<?xml version="1.0" encoding="UTF-8"?>
<icons Version="1.0">
    <propertyMap name="BOMTypeIcons">
        <item key="Product" value="SMA3_SunnyBoy.png"/>
        <item key="Device" value="SMA3_SunnyBoy.png"/>
        <item key="Module" value="SMA3_Module.png"/>
    </propertyMap>
    <primaryIcon source="SMA3_SMPRevision.png" />
        <overlayIcon source="sma3_bom_type" mapName="BOMTypeIcons" />
</icons>