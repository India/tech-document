/**
 * 
 */
package com.nov.rac.processform;

import java.util.Vector;

import com.nov.rac.processform.helper.NovObjectsHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

/**
 * @author viveksm
 * 
 */
public class NovObsoleteForm extends NovProcessForm
{
    private static final long   serialVersionUID    = 1L;    
	private Vector<TCComponent> m_dispositionTarget = new Vector<TCComponent>();
    
    public NovObsoleteForm(TCComponentForm form) throws Exception
    {
        super(form);     
    }

    protected void init() throws TCException
    {
    	super.init();
    	
    	TCComponent formComponent = null;
    	NovObjectsHelper.setObsoleteComponent(null);
    	m_dispositionTarget = NovObjectsHelper.getTargetDisposition(m_propertyMap);

    	if(m_dispositionTarget != null)
    	{
    		return;
    	}
    	
    	m_dispositionTarget = NovObjectsHelper.getDispositions(getComponent());
    	
    	try 
    	{
	    	formComponent = getComponent();
	    	formComponent.refresh();
	    	
			TCProperty dispositionProperty = formComponent.getTCProperty(NovProcessFormHelper.PROP_TARGETDISPOSITION);
			
			if(dispositionProperty != null)
			{
				dispositionProperty.setReferenceValueArray(m_dispositionTarget.toArray(new TCComponent[m_dispositionTarget.size()]));
			}
		} 
    	catch (TCException e) 
    	{
			e.printStackTrace();
		}
    	
    }

	protected String[] getPropertiesNames()
    {
        String[] propertiesNames = new String[] { NovProcessFormHelper.PROP_DRAFTEDBY, NovProcessFormHelper.PROP_DRAFTEDON,
        										    NovProcessFormHelper.PROP_APPROVEDBY, NovProcessFormHelper.PROP_APPROVEDON, 
        											NovProcessFormHelper.PROP_EXPLANATION, NovProcessFormHelper.PROP_ORACLEDESC,
        											NovProcessFormHelper.PROP_DISTRIBUTION, NovProcessFormHelper.PROP_TARGETDISPOSITION };
        
        return propertiesNames;
    }
	
}
