/**
 * 
 */
package com.nov.rac.processform;

/**
 * @author MundadaV
 *
 */
public class NovProcessFormHelper
{
    public static final String PROP_DRAFTEDBY           = "nov4_drafted_by";
    public static final String PROP_DRAFTEDON           = "nov4_drafted_on";
    public static final String PROP_APPROVEDBY          = "nov4_approved_by";
    public static final String PROP_APPROVEDON          = "nov4_approved_on";
    public static final String PROP_CHECKEDBY           = "nov4_checked_by";
    public static final String PROP_CHECKEDON           = "nov4_checked_on";
    public static final String PROP_CREATEDBY           = "nov4_created_by";
    public static final String PROP_CREATEDON           = "nov4_created_on";
    public static final String PROP_EXPLANATION         = "nov4_reason_notice";
    public static final String PROP_ORACLEDESC          = "nov4_change_desc";
    public static final String PROP_DISTRIBUTION        = "nov4_distribution";
    public static final String PROP_TARGETDISPOSITION  = "nov4_disp_target";
    
    public static final String PROP_INPROCESS           = "inprocess";
    public static final String PROP_ININVENTORY         = "ininventory";
    public static final String PROP_ASSEMBLED           = "assembled";
    public static final String PROP_INFIELD             = "infield";
    public static final String PROP_INSTRUCTION         = "dispinstruction";
    
}
