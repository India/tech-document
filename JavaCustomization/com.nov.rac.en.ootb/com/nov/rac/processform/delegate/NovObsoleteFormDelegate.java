/**
 * 
 */
package com.nov.rac.processform.delegate;

import java.util.Arrays;
import java.util.Vector;

import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.processform.helper.NovObjectsHelper;
import com.nov.rac.operations.AbstractFormOperationDelegate;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

/**
 * @author viveksm
 *
 */
public class NovObsoleteFormDelegate extends AbstractFormOperationDelegate
{
	private TCComponent 				 m_formObject 			= null;
	private Vector<CreateInObjectHelper> m_createInObjectHelper = new Vector<CreateInObjectHelper>();

	public NovObsoleteFormDelegate(TCComponent formComponent) 
	{
		m_formObject = formComponent;
	}

	@Override
	public boolean preCondition() throws TCException
	{
		String dispositionType = "targetdisposition";
		String formName = m_formObject.getType();

		CreateInObjectHelper obsoletePropertyMap = new CreateInObjectHelper(formName, CreateInObjectHelper.OPERATION_UPDATE);
		fillPanelsData(formName, "Form", getOperationInputProviders(), obsoletePropertyMap);
		obsoletePropertyMap.setTargetObject(m_formObject);
		m_createInObjectHelper.add(obsoletePropertyMap);

		Vector<TCComponent> dispositionTarget = new Vector<TCComponent>();
		TCProperty dispositionProperty = m_formObject.getTCProperty(NovProcessFormHelper.PROP_TARGETDISPOSITION);
		
		if(dispositionProperty != null)
		{
			TCComponent[] dispositions = dispositionProperty.getReferenceValueArray();
			dispositionTarget.addAll(Arrays.asList(dispositions));
		}
		
		TCComponent dispositionItem = NovObjectsHelper.getObsoleteComponent();	
				
		CreateInObjectHelper dispositionPropertyMap = new CreateInObjectHelper(dispositionType, CreateInObjectHelper.OPERATION_UPDATE);
		fillPanelsData(formName, "TargetDisposition", getOperationInputProviders(), dispositionPropertyMap);
		dispositionPropertyMap.setTargetObject(dispositionItem);
		m_createInObjectHelper.add(dispositionPropertyMap);
		
		return true;
	}

	@Override
	public boolean preAction() throws TCException 
	{
		return true;
	}

	@Override
	public boolean baseAction() throws TCException 
	{
		CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) m_createInObjectHelper.toArray( new CreateInObjectHelper[0]);
		CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		
		CreateObjectsSOAHelper.throwErrors(createInObjectHelper);
		
		return true;
	}

}
