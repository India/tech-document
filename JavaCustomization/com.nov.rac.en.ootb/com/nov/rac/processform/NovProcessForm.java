/**
 * 
 */
package com.nov.rac.processform;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.nov.rac.operations.IOperationDelegate;
import com.nov.rac.processform.factory.NovProcessFormPanelFactory;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.commands.open.OpenFormDialog;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.stylesheet.AbstractRendering;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;

/**
 * @author viveksm
 * 
 */
public abstract class NovProcessForm extends AbstractRendering
{
    private static final long   serialVersionUID    = 1L;
    private Vector<IUIPanel>    m_panels            = null;
	//private Registry       		m_registry    		= Registry.getRegistry("com.nov.rac.processform.processform");
    private Registry       		m_registry    		= Registry.getRegistry(this);
    protected IPropertyMap      m_propertyMap       = null;
    private TCComponentForm     m_masterForm;
    
    public NovProcessForm(TCComponentForm form) throws Exception
    {
        super(form);
        m_masterForm = form;
        m_panels = createUI();       
                
        init();

        loadRendering();
    }
    
    protected abstract String[] getPropertiesNames();

	protected Registry getRegistry()
    {
    	return m_registry;
    }
    
    public Vector<IUIPanel> getUIPanels()
    {
    	return m_panels;
    }
    
    protected IPropertyMap createPropertyMap()
    {
		return new SimplePropertyMap();
    }

    protected void init() throws TCException
    {
    	m_propertyMap = createPropertyMap();
        PropertyMapHelper.componentToMap(getComponent(), getPropertiesNames(), m_propertyMap);    	
    }
    
    @Override
	public void loadRendering() throws TCException
    {
        load(m_propertyMap);
    }
    
    @Override
    public void saveRendering()
    {
        String objectType = getComponent().getType();
        Object[] objects = new Object[1];
        objects[0] = getComponent();
        
        IOperationDelegate iOperationDelegate = (IOperationDelegate) getRegistry().newInstanceFor(objectType + ".SAVE_DELEGATE", objects);
        iOperationDelegate.registerOperationInputProvider(m_panels);
        try
        {
            iOperationDelegate.executeOperation();
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }

	@Override
	public boolean isObjectSavable(boolean flag)
	{
		flag = validate(m_propertyMap);
		return flag;
	}
    
    protected Vector<IUIPanel> createUI()
    {
        String formName = getComponent().getType();
        JPanel mainPanel = new JPanel(new PropertyLayout());
        NovProcessFormPanelFactory processFormPanelFactory = new NovProcessFormPanelFactory(formName, getComponent());
        m_panels = processFormPanelFactory.createPanels();
        
        for (int inx = 0; inx < m_panels.size(); ++inx)
        {
            JPanel infoPanel = new JPanel(new PropertyLayout());
            IUIPanel theUIPanel = m_panels.get(inx);            
            infoPanel.add("1.1.left.center", (JPanel)theUIPanel.getUIPanel());
            mainPanel.add("" + (inx + 1) + ".1.center.center", infoPanel);
        }
        
        JScrollPane scrollpane = new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                											 JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.setLayout(new VerticalLayout());
        this.add("unbound.bind", scrollpane);
		return m_panels;
    }
    
    protected void load(IPropertyMap propertyMap)
    {
        Vector<IUIPanel> filteredPanels = m_panels;
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof ILoadSave)
            {
                ILoadSave loadablepanel = (ILoadSave) panel;
                try
                {
                    loadablepanel.load(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    protected boolean validate(IPropertyMap propertyMap)
    {
    	boolean isValid = true;
        Vector<IUIPanel> filteredPanels = m_panels;
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof ILoadSave)
            {
                ILoadSave validatepanel = (ILoadSave) panel;
                try
                {
                	isValid = validatepanel.validate(propertyMap);
                	
                	if(isValid == false)
                	{
                		return isValid;
                	}
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
		return isValid;
    }
    //TCDECREL-4717:Start  Desc- Overridden this method to open the form in checkout mode on double clicking.
    public void displayDialog(OpenFormDialog openformdialog, boolean flag)
    {
        if(m_masterForm != null)
        {
            final TCReservationService reserserv=m_masterForm.getSession().getReservationService();
            try {
                if(!reserserv.isReserved(m_masterForm))
                    if(!(m_masterForm.getProperty("is_modifiable").length()==0))
                    reserserv.reserve(m_masterForm);
            } catch (TCException e) {
                e.printStackTrace();
            }
        
            openformdialog.addWindowListener(new WindowAdapter()
            {
                @Override
                public void windowClosed(WindowEvent e) 
                {
                    try
                    {
                        if(reserserv.isReserved(m_masterForm))
                        {
                            reserserv.unreserve(m_masterForm);
                        }
                    }
                    catch(TCException tc)
                    {
                        System.out.println(tc);
                    }
                    super.windowClosed(e);
                }
    
            });
        }
        super.displayDialog(openformdialog, flag);
    }
    //TCDECREL-4717:End
}
