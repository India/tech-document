/**
 * 
 */
package com.nov.rac.processform.helper;

import java.util.Arrays;
import java.util.Vector;

import javax.swing.table.TableModel;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;

/**
 * @author viveksm
 *
 */
public class NovObjectsHelper 
{    
	private static TCComponent m_obsoleteComponent = null;
	
	public static TCComponent getObsoleteComponent()
	{
		return m_obsoleteComponent;
	}
	
	public static void setObsoleteComponent(TCComponent obsoleteComponent)
	{
		m_obsoleteComponent = obsoleteComponent;		
	}
	
	public static Vector<TCComponent> loadTargets(TCComponent tcComponent)
    {
		TCComponentProcess currentProcess = null;
    	Vector<TCComponent> dispositionTargets = new Vector<TCComponent>();
    	
        try
        {
            AIFComponentContext[] contexts = tcComponent.whereReferenced();
            if (contexts != null)
            {
                for (int j = 0; j < contexts.length; j++)
                {
                    if (contexts[j].getComponent() instanceof TCComponentProcess)
                    {
                        currentProcess = (TCComponentProcess) contexts[j].getComponent();
                        TCComponentTask rootTask = ((TCComponentProcess) currentProcess).getRootTask();
                        TCComponent[] targets = rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
                        
                        for (int i = 0; i < targets.length; i++)
                        {
                            if (targets[i] instanceof TCComponentItem)
                            {
                            	dispositionTargets.add(targets[i]);
                            }
                        }
                        
                        break;
                    }
                }
                
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception is:" + e);
            
        }
        
        return dispositionTargets;
        
    }

    public static Vector<TCComponent> createDispositionData(TCComponent[] targetItems)
    {
      	
    	/*if(targetItemIds != null)
		{
			try
			{
				TCComponent[] dispositionData = null;
				TCUserService userService = ((TCSession)AIFUtility.getDefaultSession()).getUserService();     	
				Object objects[] = new Object[3];       	
				objects[0] = targetItemIds;
				objects[1] = targetRevIds;
				objects[2] = new Boolean(false);
				
				dispositionData = (TCComponent[])userService.call("NATOIL_createEnDispositionData", objects);
				targetComponents.addAll(Arrays.asList(dispositionData));
			}
			catch(TCException te)
			{
				te.printStackTrace();
			}
			*/
	    Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();    	
		try
		{

	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"release_status_list"};
	    	
			Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
	    	
	    	nOVDataManagementServiceHelper.getObjectsProperties(targetItems, attrList, null);
	    	
			for(int i = 0; i < targetItems.length ; i++)
			{
			    CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
			    		"targetdisposition", CreateInObjectHelper.OPERATION_CREATE);		    	
				String item_id = targetItems[i].getProperty("item_id");

				String statusName = targetItems[i].getProperty("release_status_list");
				
				formPropertyMap.setString("targetitemid", item_id);
				formPropertyMap.setString("rev_id", "");
			
				//get status name to set current status
				/*TCComponent[] relStatusList = null;
				relStatusList = ( targetItems[i].getTCProperty("release_status_list").getReferenceValueArray() );
				
				String statusName = relStatusList[0].getProperty("name");
				*/
				formPropertyMap.setString("currentstatus", statusName);
				formPropertyMap.setString("changedesc","");
				formPropertyMap.setString("dispinstruction","");
				formPropertyMap.setString("inprocess","");
				formPropertyMap.setString("ininventory","");
				formPropertyMap.setString("assembled","");
				formPropertyMap.setString("infield","");
				formPropertyMap.setString("futurestatus","");				
				vecCreateInObjectHelper.add(formPropertyMap);
	   
			}
			
	        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
	                .toArray(new CreateInObjectHelper[vecCreateInObjectHelper.size()]);
			
			//create objects
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);

		    
		    for(int j = 0; j<createInObjectHelper.length ; j++)
		    {
		    	TCComponent created = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
		    	createdorUpdatedObjs.add(created);
		    }
		   
		}
		catch(TCException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return createdorUpdatedObjs;		
    }
    
    public static Vector<TCComponent> getDispositions(TCComponent tcComponent)
    {
    	Vector<TCComponent> dispositionTarget = new Vector<TCComponent>();
    	Vector<TCComponent> targetDisposition = NovObjectsHelper.loadTargets(tcComponent);
/*    	Vector<String> itemIds = new Vector<String>();
		Vector<String> revIds = new Vector<String>();
		
    	for(int index = 0; index < targetDisposition.size(); ++index)
		{
			try 
			{
				itemIds.add(((TCComponentItem)targetDisposition.get(index)).getProperty("item_id"));
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}
			revIds.add("");
		}  
    	
    	String targetItemIds[] = itemIds.toArray(new String[itemIds.size()]);
		String targetRevIds[] = revIds.toArray(new String[revIds.size()]);
*/
    	TCComponent[] targetDispositionArray  =  targetDisposition.toArray(new TCComponent[targetDisposition.size()]);
		dispositionTarget = NovObjectsHelper.createDispositionData(targetDispositionArray);
		
		return dispositionTarget;
    }
    
    
    public static Vector<TCComponent> getTargetDisposition(IPropertyMap propMap)
    {
        Vector<TCComponent> targetDisposition = null;
        TCComponent[] targets = propMap.getTagArray(NovProcessFormHelper.PROP_TARGETDISPOSITION);
        if(targets != null && targets.length > 0)
        {
        	targetDisposition = new Vector<TCComponent>(Arrays.asList(targets));
        }
        
        return targetDisposition;
    }
    
    public static TCComponent getDispositionStatusObject(Vector<TCComponent> dispositionTarget)
    {
    	try
        {                                        
            String[] properties = {"futurestatus"};
            
            String[][] obsoleteStatus = TCComponentType.getPropertiesSet( dispositionTarget, properties);
            
            if(obsoleteStatus != null)
            {
	            for(int inx=0; inx<obsoleteStatus.length; inx++)
	            {
	            	if(obsoleteStatus[inx][0].equals("Obsolete"))
	            	{
	            		return dispositionTarget.get(inx);
	            	}
	            }
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
		return null;
    }
    
    public static TCComponent getObsoleteObject(Vector<TCComponent> dispositionTarget, TableModel tableModel)
    {        
		TCComponent[] dispositionObject = dispositionTarget.toArray(new TCComponent[dispositionTarget.size()]);
		
        for(int i=0; i<dispositionObject.length; ++i)
        {
        	String properties = tableModel.getValueAt(i, 3).toString();
            
            if(properties != null)
            {
            	if(properties.equals("Obsolete"))
            	{
            		return dispositionObject[i];
            	}
            }
        }
        
		return null;
    }
    
    public static Vector<TCComponent> getReplacementObject(Vector<TCComponent> dispositionTarget, TableModel tableModel)
    {        
    	Vector<TCComponent> replacementStatusObject = new Vector<TCComponent>();
		TCComponent[] dispositionObject = dispositionTarget.toArray(new TCComponent[dispositionTarget.size()]);
		
        for(int i=0; i<dispositionObject.length; ++i)
        {
        	String properties = tableModel.getValueAt(i, 3).toString();
            
            if(properties != null)
            {
            	if(!properties.equals("Obsolete"))
            	{
            		replacementStatusObject.add(dispositionObject[i]);
            	}
            }
        }
        
		return replacementStatusObject;
    }
}
