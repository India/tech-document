/**
 * 
 */
package com.nov.rac.processform.helper;

import com.teamcenter.rac.util.Registry;

/**
 * @author viveksm
 * 
 */
public class NovProcessFormPanelHelper
{
    Registry m_reg = Registry.getRegistry(this);
    
    public NovProcessFormPanelHelper()
    {
        
    }
    
    public String[] getProperties(String formsPanelName)
    {
        String[] panels = m_reg.getStringArray(formsPanelName + ".PANELS");
        
        return panels;
    }
}
