/**
 * 
 */
package com.nov.rac.processform.factory;

import java.util.Vector;

import com.nov.rac.processform.helper.NovProcessFormPanelHelper;
import com.nov.rac.ui.IUIPanel;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author viveksm
 * 
 */
public class NovProcessFormPanelFactory
{
    private String           m_formName      = null;
    private TCComponent      m_tcComponent   = null;
    private Registry         m_registry      = Registry.getRegistry("com.nov.rac.processform.panels.panels");
    private Vector<IUIPanel> m_subPanels     = new Vector<IUIPanel>();
    
    public NovProcessFormPanelFactory(String formName, TCComponent component)
    {
        this.m_formName = formName;
        this.m_tcComponent = component;
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public Vector<IUIPanel> createPanels()
    {
        NovProcessFormPanelHelper processFormPanelHelper = new NovProcessFormPanelHelper();
        
        String allPanels[] = processFormPanelHelper.getProperties(m_formName);
        IUIPanel panel[] = new IUIPanel[allPanels.length];
        
        Object[] objects = new Object[2];
        objects[0] = m_registry;
        objects[1] = m_tcComponent;
        
        for (int inx = 0; inx < allPanels.length; ++inx)
        {
            panel[inx] = (IUIPanel) getRegistry().newInstanceFor(allPanels[inx], objects);
            
            m_subPanels.add(panel[inx]);
        }
        
        return m_subPanels;
    }
}
