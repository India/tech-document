package com.nov.rac.processform.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.processform.helper.NovObjectsHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

public class NovDispositionPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long   serialVersionUID           = 1L;
    private TCComponent         m_tcComponent              = null;
    private Registry            m_reg                      = null;
    private Vector<TCComponent> m_dispositionTarget        = null;
    private NOVLOVPopupButton   m_inProcessPopUpButton     = null;
    private NOVLOVPopupButton   m_inInventoryPopUpButton   = null;
    private NOVLOVPopupButton   m_assembledPopUpButton     = null;
    private NOVLOVPopupButton   m_inFieldPopUpButton       = null;
    private iTextArea           m_textArea                 = null;
    private IPropertyMap        m_propertyMap              = new SimplePropertyMap();
    
    public NovDispositionPanel(Registry reg, TCComponent component)
    {
        m_reg = reg;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        TitledBorder tb = new TitledBorder("");
        tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        
        JPanel titlePanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        NOIJLabel mandatoryLabel = new NOIJLabel("*");
        mandatoryLabel.setForeground(Color.RED);
        titlePanel.add("1.1.left.center", mandatoryLabel);
        titlePanel.add("1.2.left.center", new NOIJLabel(m_reg.getString("disposition.LABEL")));
        
        JPanel infoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel leftInfoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel rightInfoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        
        leftInfoPanel.setPreferredSize(new Dimension(300, 130));
        rightInfoPanel.setPreferredSize(new Dimension(300, 110));
        infoPanel.setPreferredSize(new Dimension(600, 130));
        
        m_inProcessPopUpButton = new NOVLOVPopupButton();
        m_inProcessPopUpButton.setPreferredSize(new Dimension(120, 25));
        
        m_inInventoryPopUpButton = new NOVLOVPopupButton();
        m_inInventoryPopUpButton.setPreferredSize(new Dimension(120, 25));
        
        m_assembledPopUpButton = new NOVLOVPopupButton();
        m_assembledPopUpButton.setPreferredSize(new Dimension(120, 25));
        
        m_inFieldPopUpButton = new NOVLOVPopupButton();
        m_inFieldPopUpButton.setPreferredSize(new Dimension(120, 25));
        
        leftInfoPanel.add("1.1.left.center", new NOIJLabel(m_reg.getString("inProcess.LABEL")));
        leftInfoPanel.add("2.1.left.center", new NOIJLabel(m_reg.getString("inInventory.LABEL")));
        leftInfoPanel.add("3.1.left.center", new NOIJLabel(m_reg.getString("assembled.LABEL")));
        leftInfoPanel.add("4.1.left.center", new NOIJLabel(m_reg.getString("inField.LABEL")));
        
        leftInfoPanel.add("1.2.left.center", m_inProcessPopUpButton);
        leftInfoPanel.add("2.2.left.center", m_inInventoryPopUpButton);
        leftInfoPanel.add("3.2.left.center", m_assembledPopUpButton);
        leftInfoPanel.add("4.2.left.center", m_inFieldPopUpButton);
        
        m_textArea = new iTextArea();
        JScrollPane descScroller = new JScrollPane(m_textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                												JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        descScroller.setPreferredSize(new Dimension(280, 80));
        
        rightInfoPanel.add("1.1.left.center", new NOIJLabel(m_reg.getString("dispositionInstruction.LABEL")));
        rightInfoPanel.add("2.1.left.center", descScroller);
        
        infoPanel.add("1.1.left.center", leftInfoPanel);
        infoPanel.add("1.2.left.center", rightInfoPanel);
        
        JPanel panel = new JPanel(new PropertyLayout());
        panel.add("1.1.left.center", titlePanel);
        panel.add("2.1.left.center", infoPanel);
        
        
        panel.setBorder(tb);
        panel.setPreferredSize(new Dimension(615, 180));
        
        setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));
        this.add("1.1.left.center", panel);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
    	TCComponentListOfValues lovalues = TCComponentListOfValuesType.findLOVByName(tcSession,m_reg.getString("ECNAvailability.LABEL"));
    	m_inProcessPopUpButton.setLovComponent(lovalues);
    	m_inInventoryPopUpButton.setLovComponent(lovalues);
    	m_assembledPopUpButton.setLovComponent(lovalues);
    	m_inFieldPopUpButton.setLovComponent(lovalues);
    	m_inProcessPopUpButton.setText("N/A");
    	m_inInventoryPopUpButton.setText("N/A");
    	m_assembledPopUpButton.setText("N/A");
    	m_inFieldPopUpButton.setText("N/A");
    	
        TCComponent dispositionObject = getDispositionObject(propMap);
        
        if (dispositionObject != null)
        {
            PropertyMapHelper.componentToMap(dispositionObject, getPropertiesNames(), m_propertyMap);
            
            String inprocess = PropertyMapHelper.handleNull(m_propertyMap.getString(NovProcessFormHelper.PROP_INPROCESS));
            m_inProcessPopUpButton.setSelectedText(inprocess);           
            
            String ininventory = PropertyMapHelper.handleNull(m_propertyMap.getString(NovProcessFormHelper.PROP_ININVENTORY));
            m_inInventoryPopUpButton.setSelectedText(ininventory);            
            
            String assembled = PropertyMapHelper.handleNull(m_propertyMap.getString(NovProcessFormHelper.PROP_ASSEMBLED));
            m_assembledPopUpButton.setSelectedText(assembled);

            String infield = PropertyMapHelper.handleNull(m_propertyMap.getString(NovProcessFormHelper.PROP_INFIELD));
            m_inFieldPopUpButton.setSelectedText(infield);

            String disinstruction = PropertyMapHelper.handleNull(m_propertyMap.getString(NovProcessFormHelper.PROP_INSTRUCTION));
            m_textArea.setText(disinstruction);
        }
        
        return true;
    }
    
    private String[] getPropertiesNames()
    {
        String[] propertiesNames = new String[] { NovProcessFormHelper.PROP_INPROCESS, NovProcessFormHelper.PROP_ININVENTORY, NovProcessFormHelper.PROP_ASSEMBLED,
                                                                                        NovProcessFormHelper.PROP_INFIELD, NovProcessFormHelper.PROP_INSTRUCTION};
        
        return propertiesNames;
        
    }
    
    private TCComponent getDispositionObject(IPropertyMap propMap)
    {
    	TCComponent dispositionObject = null;
        m_dispositionTarget = NovObjectsHelper.getTargetDisposition(propMap);
        
        if(m_dispositionTarget != null)
        {
            dispositionObject = NovObjectsHelper.getDispositionStatusObject(m_dispositionTarget);
        }
 
        return dispositionObject;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
    	String dispinstruction = m_textArea.getText();
        propMap.setString(NovProcessFormHelper.PROP_INSTRUCTION, dispinstruction);

        String inprocess = m_inProcessPopUpButton.getText();
        propMap.setString(NovProcessFormHelper.PROP_INPROCESS, inprocess);
        
        String ininventory = m_inInventoryPopUpButton.getText();
        propMap.setString(NovProcessFormHelper.PROP_ININVENTORY, ininventory);
        
        String assembled = m_assembledPopUpButton.getText();
        propMap.setString(NovProcessFormHelper.PROP_ASSEMBLED, assembled);
        
        String infield = m_inFieldPopUpButton.getText();
        propMap.setString(NovProcessFormHelper.PROP_INFIELD, infield);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {    	
        return true;
    }
    
}
