package com.nov.rac.processform.panels;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.ArraySorter;
import com.teamcenter.rac.util.Registry;

public class NovAttachmentsTableModel extends AbstractTableModel
{
    private static final long   serialVersionUID = 1L;
    private Object[][]          m_data           = null;
    private Vector<TCComponent> m_selectedData   = new Vector<TCComponent>();
    private Vector<TCComponent> m_sorted         = new Vector<TCComponent>();
    private Registry            m_reg            = null;
    private String[]            m_columnNames    = null;
    private Vector<TCComponent> m_selected       = new Vector<TCComponent>();
    
    public NovAttachmentsTableModel(Vector<TCComponent> attachedData)
    {
        super();
        m_reg = Registry.getRegistry(this);
        m_columnNames = new String[] { m_reg.getString("type.STR"), m_reg.getString("name.STR") };
        initData(attachedData);
    }
    
    private void initData(Vector<TCComponent> theData)
    {
        TCComponent[] attachedComponent = new TCComponent[theData.size()];
        
        if (theData.size() > 0)
        {
            m_sorted.clear();
            for (int i = 0; i < theData.size(); i++)
            {
                attachedComponent[i] = (TCComponent) theData.get(i);
            }
            
            ArraySorter.sort(attachedComponent);
            for (int index = 0; index < attachedComponent.length; index++)
            {
                m_sorted.addElement(attachedComponent[index]);
            }
            
            m_data = new Object[m_sorted.size()][3];
            
            for (int i = 0; i < m_sorted.size(); i++)
            {
                m_selectedData.add(m_sorted.get(i));
                try
                {
                    TCComponentDataset ds = ((TCComponentDataset) m_sorted.get(i));
                    String type = ds.getProperty("object_type");
                    String name = ds.getProperty("object_name");
                    m_data[i][0] = type;
                    m_data[i][1] = name;
                }
                catch (Exception e)
                {
                    System.out.println("Could not get table info: " + e);
                }
            }
        }
        else
        {
            m_sorted = new Vector<TCComponent>();
            m_data = null;
        }
    }
    
    public String getFileRefType(TCComponentDataset dataset)
    {
        String referenceType = null;

        try
        {
            referenceType = dataset.getProperty("ref_names");
            
            if (referenceType != null && referenceType.indexOf(",") != -1)
            {
                referenceType = referenceType.split(",")[0];
            }            
            return referenceType;
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return referenceType;
    }
    
    public int getColumnCount()
    {
        return m_columnNames.length;
    }
    
    public int getRowCount()
    {
        return m_data == null ? 0 : m_data.length;
    }
    
    public String getColumnName(int col)
    {
        return m_columnNames[col];
    }
    
    public Object getValueAt(int row, int col)
    {
        return m_data[row][col];
    }
    
    public Class getColumnClass(int c)
    {
        return getValueAt(0, c).getClass();
    }
    
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
    
    public TCComponent[] getSelectedData()
    {
        TCComponent[] selectedData = new TCComponent[m_selectedData.size()];
        for (int i = 0; i < m_selectedData.size(); i++)
        {
            selectedData[i] = (TCComponent) m_selectedData.get(i);
        }
        
        return selectedData;
    }
    
    public TCComponentDataset getAttachment(int index)
    {
        return ((TCComponentDataset) m_sorted.get(index));
    }
    
    @SuppressWarnings("unchecked")
    public void addRows(Vector<TCComponentDataset> addDataset)
    {
        m_selected = (Vector<TCComponent>) m_selectedData.clone();

        for (int i = 0; i < addDataset.size(); i++)
        {
            m_selected.add(addDataset.get(i));
        }
        
        m_selectedData.removeAllElements();
        initData(m_selected);
    }
    
    @SuppressWarnings("unchecked")
    public void removeRow(int row)
    {
        m_selected = (Vector<TCComponent>) m_selectedData.clone();
        m_selected.remove(row);
        m_selectedData.removeAllElements();
        initData(m_selected);
    }
    
}
