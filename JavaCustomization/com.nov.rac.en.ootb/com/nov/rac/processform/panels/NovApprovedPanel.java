/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Dimension;

import javax.swing.JPanel;

import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author MundadaV
 * 
 */
public class NovApprovedPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long serialVersionUID        = 1L;
    private Registry          m_registry              = null;
    private NOVLOVPopupButton m_approvedByPopUpButton = null;
    private NOVLOVPopupButton m_approvedOnPopUpButton = null;
    private TCComponent       m_tcComponent           = null;
    
    public NovApprovedPanel(Registry registry, TCComponent component)
    {
        m_registry = registry;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}

    }
    
    public boolean createUI() throws TCException
    {
        JPanel infoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel leftInfoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel rightInfopanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        
        leftInfoPanel.setPreferredSize(new Dimension(300, 30));
        rightInfopanel.setPreferredSize(new Dimension(300, 30));
        
        m_approvedByPopUpButton = new NOVLOVPopupButton(){           
            
            private static final long serialVersionUID = 1L;

            @Override            
            public void setEnabled( boolean f )            
            {                
                super.setEnabled( false );            
            }        
        };
        m_approvedByPopUpButton.setPreferredSize(new Dimension(200, 21));
        m_approvedByPopUpButton.setEnabled(false);
        
        m_approvedOnPopUpButton = new NOVLOVPopupButton(){           
           
            private static final long serialVersionUID = 1L;

            @Override            
            public void setEnabled( boolean f )            
            {                
                super.setEnabled( false );            
            }        
        };
        m_approvedOnPopUpButton.setPreferredSize(new Dimension(200, 21));
        m_approvedOnPopUpButton.setEnabled(false);
        
        leftInfoPanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("approvedby.LABEL")));
        leftInfoPanel.add("1.2.left.center", m_approvedByPopUpButton);
        
        rightInfopanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("approvedon.LABEL")));
        rightInfopanel.add("1.2.left.center", m_approvedOnPopUpButton);
        
        infoPanel.add("1.1.left.center", leftInfoPanel);
        infoPanel.add("1.2.left.center", rightInfopanel);     
        infoPanel.setPreferredSize(new Dimension(630, 30));
        
        JPanel panel = new JPanel(new PropertyLayout());   
        panel.add("1.1.left.center", infoPanel);
        
        setLayout(new PropertyLayout(0, 0, 0, 0, 0, 0));
        this.add("1.1.left.center", panel);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String approvedBy = PropertyMapHelper.handleNull(propMap.getString(NovProcessFormHelper.PROP_APPROVEDBY));
        m_approvedByPopUpButton.setSelectedText(approvedBy);
        
        String approvedOn = PropertyMapHelper.handleNull(propMap.getDate(NovProcessFormHelper.PROP_APPROVEDON));
        m_approvedOnPopUpButton.setSelectedText(approvedOn);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
}
