/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

/**
 * @author MundadaV
 * 
 */
public class NovOracleDescriptionPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long serialVersionUID = 1L;
    private Registry          m_reg            = null;
    private iTextArea         m_oracleDesProp  = null;
    private TCComponent       m_tcComponent    = null;
    
    public NovOracleDescriptionPanel(Registry reg, TCComponent component)
    {
        m_reg = reg;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}

    }
    
    public boolean createUI() throws TCException
    {
        JPanel decriptionPanel = new JPanel();
        decriptionPanel.setBackground(this.getBackground());
        
        m_oracleDesProp = new iTextArea();
        
        JScrollPane descriptionPane = new JScrollPane((m_oracleDesProp));
        descriptionPane.setPreferredSize(new Dimension(590, 90));
        
        TitledBorder tb = new TitledBorder(m_reg.getString("OracleDes.LABEL"));
        tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        decriptionPanel.setBorder(tb);
        decriptionPanel.add(descriptionPane);
        
        setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));
        this.add("1.1.left.center", decriptionPanel);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String oracleDesc = PropertyMapHelper.handleNull(propMap.getString(NovProcessFormHelper.PROP_ORACLEDESC));
        m_oracleDesProp.setText(oracleDesc);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        String newDesc = m_oracleDesProp.getText();
        propMap.setString(NovProcessFormHelper.PROP_ORACLEDESC, newDesc);
        
        return true;

    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {    	
        return true;
    }
}
