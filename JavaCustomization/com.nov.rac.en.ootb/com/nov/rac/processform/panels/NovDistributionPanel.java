/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.noi.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.form.compound.component.AddressBookComponent;
import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.MessageBox;
import com.nov.rac.utilities.utils.MessageBoxHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author MundadaV
 * 
 */
public class NovDistributionPanel extends AbstractSwingUIPanel implements ActionListener, ILoadSave
{
    private static final long                               serialVersionUID      = 1L;
    private Component                                       m_comp                = null;
    private JTextField                                      m_emailAddressField   = null;
    private JButton                                         m_addEmailIdButton    = null;
    private JButton                                         m_addTarget           = null;
    private JButton                                         m_removeTarget        = null;
    private JButton                                         m_globalAddressBtn    = null;
    private Registry                                        m_reg                 = null;
    
    private JList                                           m_sourceList          = new JList();
    private JList                                           m_targetList          = new JList();
    private JScrollPane                                     m_sourceListScroll    = new JScrollPane(m_sourceList); 
    private JScrollPane                                     m_targetListScroll    = new JScrollPane(m_targetList);
    private DefaultListModel                                m_sourceListModel     = null;
    private DefaultListModel                                m_targetListModel     = null;
    
    public AbstractReferenceLOVList                         m_sourceData          = new AbstractReferenceLOVList();
    public AbstractReferenceLOVList 						m_targetData          = new AbstractReferenceLOVList();
    
    private TCComponent                                     m_tcComponent         = null;
    
    public NovDistributionPanel(Registry reg, TCComponent component)
    {
        m_reg = reg;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        TitledBorder titledBorder = new TitledBorder(m_reg.getString("selectDistribution.LABEL"));
        titledBorder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        
        m_sourceListModel = new DefaultListModel();
        m_sourceList.setModel(m_sourceListModel);
        m_sourceListScroll = new JScrollPane(m_sourceList);
        m_sourceListScroll.setPreferredSize(new Dimension(250, 120));
        
        m_targetList = new JList();
        m_targetListModel = new DefaultListModel();
        m_targetList.setModel(m_targetListModel);
        m_targetListScroll = new JScrollPane(m_targetList);
        m_targetListScroll.setPreferredSize(new Dimension(250, 120));
        
        ImageIcon plusIcon = m_reg.getImageIcon("enadded.ICON");
        ImageIcon minusIcon = m_reg.getImageIcon("enremove.ICON");
        ImageIcon addIcon = m_reg.getImageIcon("enaddr.ICON");
        
        m_addTarget = new JButton();
        m_addTarget.setPreferredSize(new Dimension(30, 20));
        m_addTarget.setIcon(plusIcon);
        m_addTarget.addActionListener(this);
        
        m_removeTarget = new JButton();
        m_removeTarget.setPreferredSize(new Dimension(30, 20));
        m_removeTarget.setIcon(minusIcon);
        m_removeTarget.addActionListener(this);
        
        JPanel addremButtonPanel = new JPanel(new PropertyLayout(5, 10, 5, 5, 50, 20));
        addremButtonPanel.add("1.1.left.center", m_addTarget);
        addremButtonPanel.add("2.1.left.center", m_removeTarget);
        
        JPanel sourcePanel = new JPanel(new PropertyLayout());
        NOIJLabel sourcLabel = new NOIJLabel(m_reg.getString("available.LABEL"));
        sourcePanel.add("1.1.center.center", sourcLabel);
        sourcePanel.add("2.1.left.top", m_sourceListScroll);
        
        JPanel targetPanel = new JPanel(new PropertyLayout());
        NOIJLabel targetLable = new NOIJLabel(m_reg.getString("selected.LABEL"));
        targetPanel.add("1.1.center.center", targetLable);
        targetPanel.add("2.1.left.top", m_targetListScroll);
        
        m_emailAddressField = new JTextField();
        m_emailAddressField.setColumns(15);
        
        m_addEmailIdButton = new JButton(plusIcon);
        m_addEmailIdButton.setPreferredSize(new Dimension(20, 20));
        m_addEmailIdButton.addActionListener(this);
        
        m_globalAddressBtn = new JButton();
        m_globalAddressBtn.setIcon(addIcon);
        m_globalAddressBtn.addActionListener(this);
        m_globalAddressBtn.setPreferredSize(new Dimension(20, 20));
        
        JPanel searchPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        
        JPanel bottomPanel = new JPanel();
        bottomPanel.setPreferredSize(new Dimension(35, 20));
        searchPanel.add("1.1.right.right", bottomPanel);
        searchPanel.add("1.2.right.right", m_emailAddressField);
        searchPanel.add("1.3.right.right", m_addEmailIdButton);
        searchPanel.add("1.4.right.right", m_globalAddressBtn);
        
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(615, 210));
        panel.setLayout(new PropertyLayout());
        panel.add("1.1.left.center", sourcePanel);
        panel.add("1.2.left.center", addremButtonPanel);
        panel.add("1.3.left.center", targetPanel);
        panel.add("2.1.left.center", bottomPanel);
        panel.add("2.2.left.center", searchPanel);
        
        panel.setBorder(titledBorder);
        setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));
        this.add("1.1.left.center", panel);
        
        return true;
    }
        
    public void actionPerformed(ActionEvent actEvt)
    {
        if (actEvt.getSource() == m_globalAddressBtn)
        {
            m_comp = this.getParent().getParent();
            AddressBookComponent adressBookComp = new AddressBookComponent(AIFUtility.getCurrentApplication()
                    .getDesktop(), m_targetData, m_targetList, 8);
            
            adressBookComp.searchField.setText(m_emailAddressField.getText());
            adressBookComp.setLocationRelativeTo(m_globalAddressBtn);
            adressBookComp.setVisible(true);
        }
        if (actEvt.getSource() == m_addEmailIdButton)
        {
            if (isValidEmail(m_emailAddressField.getText()))
            {
                int index = m_targetListModel.indexOf(m_emailAddressField.getText());
                m_comp = (JPanel) this.getParent().getParent();

                if (index >= 0)
                {
					MessageBox.post(MessageBoxHelper.getWindow(m_comp), m_reg.getString("info.TITLE"), m_reg.getString("hasMailIds.MSG") + 
																				" : " + m_emailAddressField.getText(), MessageBox.INFORMATION);	
                }
                else
                {
                    String emailIdText = m_emailAddressField.getText();
                    m_targetListModel.addElement(m_emailAddressField.getText());
                    
                    if(emailIdText != null  && emailIdText.length() > 0)
                    {
                        m_targetData.addItem(emailIdText, emailIdText);
                    }
                    
                    populateTargetList();
                }
            }
            else
            {
                m_comp = m_emailAddressField.getParent().getParent();
                
                MessageBox.post(MessageBoxHelper.getWindow(m_comp), m_reg.getString("info.TITLE"), m_reg.getString("emailIdFormat.MSG"),
                																										MessageBox.INFORMATION);

            }
        }
        if (actEvt.getSource() == m_addTarget)
        {
            Object[] selectedValues = m_sourceList.getSelectedValues();
            
            for (int i = 0; i < selectedValues.length; i++)
            {
                String value = (String) selectedValues[i];
                Object object = m_sourceData.getReferenceFromString(value);
                m_targetData.addItem(object, value);
                m_sourceData.addExclusion(value);
                populateSourceList();
                populateTargetList();
            }
        }
        if (actEvt.getSource() == m_removeTarget)
        {
            Object[] selectedValues = m_targetList.getSelectedValues();
            
            for (int i = 0; i < selectedValues.length; i++)
            {
                String value = (String) selectedValues[i];
                Object object = m_targetData.getReferenceFromString(value);
                
                if (object != null)
                {
                    m_targetData.removeItem(object, value);
                    m_targetListModel.removeElement(value);
                    m_sourceData.addItem(value, value);
                    m_sourceData.removeExclusion(value);
                    populateSourceList();
                    populateTargetList();
                }
                else
                {
                    for (i = 0; i < selectedValues.length; i++)
                    {
                        m_targetData.removeItem(selectedValues[i], (String) selectedValues[i]);
                        m_targetListModel.removeElement(selectedValues[i]);
                    }
                    
                    populateTargetList();
                }
            }
        }
    }
    
    private void populateSourceList()
    {
        String[] listValues = m_sourceData.getList();
        m_sourceList.setListData(listValues);
    }
    
    private boolean isValidEmail(String mailID)
    {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*"
                				  + "@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(mailID);
        
        if (!matcher.find())
        {
            return false;
        }
        
        return true;
    }
    
    private void populateTargetList()
    {
        String[] listValues = m_targetData.getList();
        Vector<String> targetList = new Vector<String>();
        
        for (int j = 0; listValues != null && j < listValues.length; j++)
        {
            targetList.addElement(listValues[j]);
        }
        String[] targetListStr = targetList.toArray(new String[targetList.size()]);
        
        m_targetList.setListData(targetListStr);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String[] distribution = PropertyMapHelper.handleNull(propMap.getStringArray("nov4_distribution"));
        
        if (distribution != null && distribution.length > 0)
        {
            for (int i = 0; i < distribution.length; i++)
            {
                if (!distribution[i].isEmpty() && m_targetData != null)
                {
                    m_targetData.addItem(distribution[i], distribution[i]);
                    m_sourceData.addExclusion(distribution[i]);
                }
                else
                {
                    break;
                }
            }
            
            if (m_sourceData != null && m_targetData != null)
            {
                populateSourceList();
                populateTargetList();
            }
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        int listCount = m_targetList.getModel().getSize();
        String[] selectedList = new String[listCount];
        
        for (int inx = 0; inx < listCount; inx++)
        {
            selectedList[inx] = (String) m_targetList.getModel().getElementAt(inx);
        }
        
        propMap.setStringArray(NovProcessFormHelper.PROP_DISTRIBUTION, selectedList);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
}
