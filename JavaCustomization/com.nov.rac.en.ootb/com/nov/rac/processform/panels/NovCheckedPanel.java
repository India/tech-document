/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Dimension;

import javax.swing.JPanel;

import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author MundadaV
 * 
 */
public class NovCheckedPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long serialVersionUID = 1L;
    private Registry          m_registry       = null;
    private TCComponent       m_tcComponent    = null;
    
    public NovCheckedPanel(Registry registry, TCComponent component)
    {
        m_registry = registry;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e) 
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        JPanel infoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel leftInfoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel rightInfopanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        
        leftInfoPanel.setPreferredSize(new Dimension(300, 80));
        rightInfopanel.setPreferredSize(new Dimension(300, 80));
        
        NOVLOVPopupButton popUpButton = new NOVLOVPopupButton();
        popUpButton.setPreferredSize(new Dimension(200, 21));
        popUpButton.setEnabled(false);
        
        NOVLOVPopupButton popUpButton1 = new NOVLOVPopupButton();
        popUpButton1.setPreferredSize(new Dimension(200, 21));
        popUpButton.setEnabled(false);
        
        leftInfoPanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("checkedby.LABEL")));
        leftInfoPanel.add("1.2.left.center", popUpButton);
        
        rightInfopanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("checkedon.LABEL")));
        rightInfopanel.add("1.2.left.center", popUpButton1);
        
        infoPanel.add("1.1.left.center", leftInfoPanel);
        infoPanel.add("1.2.left.center", rightInfopanel);
        infoPanel.setPreferredSize(new Dimension(630, 30));
        
        JPanel panel = new JPanel(new PropertyLayout());
        panel.add("1.1.center.center", infoPanel);
        
        setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));
        this.add("1.1.left.center", panel);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
}
