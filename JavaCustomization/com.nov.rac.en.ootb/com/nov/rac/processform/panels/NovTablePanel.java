package com.nov.rac.processform.panels;

import java.util.Vector;

import javax.swing.JTable;

import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class NovTablePanel extends JTable
{
    private static final long serialVersionUID  = 1L;
    
    public NovTablePanel()
    {
        
    }
    
    public NovTablePanel(String[] columnNames)
    {
        AIFTableModel model = new AIFTableModel(columnNames);
        
        setModel(model);
        
    }
    
    public boolean isCellEditable(int row, int column)
    {
        if (column == 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void loadTabledata(Vector<TCComponent> dispositionTarget)
    {
        try
        {
            int rowNo = 0;
           
            if (dispositionTarget != null)
            {
                for (int i = 0; i < dispositionTarget.size(); i++)
                {
                	dispositionTarget.get(i).refresh();
                    Vector<Object> dispositonVector = new Vector<Object>();
                    dispositonVector.add(Integer.toString(++rowNo));
                    
                    TCComponent dispositionObject = dispositionTarget.get(i);
                    String itemId = dispositionObject.getProperty("targetitemid");
                    dispositonVector.add(itemId);

                    if (dispositionObject != null)
                    {
                        String status = dispositionObject.getProperty("currentstatus");
                        dispositonVector.add(status);
                    }
                    
                    String futureStatus = dispositionObject.getTCProperty("futurestatus").getStringValue();
                    
                    if (futureStatus != null)
                    {
                    	dispositonVector.add(futureStatus);
                    }
                    
                    ((AIFTableModel) this.dataModel).addRow(dispositonVector);
                }
            }
        }
        catch (TCException tc)
        {
            
        }
    }    
}