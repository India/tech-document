/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.noi.NationalOilwell;
import com.noi.rac.commands.newdataset.NewDatasetDialog;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author MundadaV
 * 
 */
public class NovChangeDescAttachmentsPanel extends AbstractSwingUIPanel implements ILoadSave, ActionListener, ListSelectionListener
{
    private static final long       	serialVersionUID      = 1L;
    private Registry               	 	m_reg                 = null;
    private JScrollPane             	m_tablePane           = null;
    private JButton                 	m_attachmentsBoxPlus  = null;
    private JButton                 	m_attachmentsBoxMinus = null;
    private JButton                 	m_viewAttachment      = null;
    private TCComponent             	m_tcComponent         = null;
    private Vector<TCComponent>     	m_attachments         = new Vector<TCComponent>();
    private NovAttachmentsTableModel    m_theModel            = null;
    private ArrayList<TCComponent>  	m_tableData           = new ArrayList<TCComponent> ();
    private Vector<TCComponent>     	m_theData             = new Vector<TCComponent> ();
    private JTable                  	m_table               = new JTable();
    private int                     	m_selectedItemRow     = -1;
    
    public NovChangeDescAttachmentsPanel(Registry reg, TCComponent component)
    {
        m_reg = reg;
        this.m_tcComponent = component;
        m_theModel = getTableModel();
        setInitialData(new ArrayList<TCComponent> ());
        try 
        {
			createUI();
		} 
        catch (TCException e) 
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(615, 150));
        
        TitledBorder titledBorder = new javax.swing.border.TitledBorder(m_reg.getString("changeDescriptionAttachments.LABEL"));
        titledBorder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        panel.setBorder(titledBorder);
        
        ListSelectionModel rowSelectionModel = m_table.getSelectionModel();
        rowSelectionModel.addListSelectionListener(this);
        
        m_table.setModel(m_theModel);
        m_table.setPreferredScrollableViewportSize(new Dimension(575, 200));       
        
        m_tablePane = new JScrollPane(m_table);
        m_tablePane.setPreferredSize(new Dimension(580, 80));
        
        ImageIcon plusIcon = m_reg.getImageIcon("enadded.ICON");
        ImageIcon minusIcon = m_reg.getImageIcon("enremove.ICON");
        
        m_attachmentsBoxPlus = new JButton();
        m_attachmentsBoxPlus.setPreferredSize(new Dimension(30, 20));
        m_attachmentsBoxPlus.setIcon(plusIcon);
        
        m_attachmentsBoxMinus = new JButton();
        m_attachmentsBoxMinus.setPreferredSize(new Dimension(30, 20));
        m_attachmentsBoxMinus.setIcon(minusIcon);
        
        m_viewAttachment = new JButton(m_reg.getString("ChangeDescAttachmentsPanel.ViewSelectedDocument"));
        m_viewAttachment.setVisible(false);
        
        m_attachmentsBoxPlus.addActionListener(this);
        m_attachmentsBoxMinus.addActionListener(this);
        m_viewAttachment.addActionListener(this);
        
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;      
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(m_attachmentsBoxPlus);
        buttonPanel.add(m_attachmentsBoxMinus);
        buttonPanel.add(m_viewAttachment);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        
        panel.add(m_tablePane, gbc);
        panel.add(buttonPanel, gbc);
        
        setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));
        this.add("1.1.left.center", panel);
        
        return true;
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e)
    {
        if (e.getValueIsAdjusting())
        {
            return;
        }
        
        ListSelectionModel defaultSelectionModel = (ListSelectionModel) e.getSource();
        if (defaultSelectionModel.isSelectionEmpty())
        {
            ;
        }
        else
        {
            m_selectedItemRow = defaultSelectionModel.getMinSelectionIndex();
            m_viewAttachment.setVisible(true);
            m_viewAttachment.setEnabled(true);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent actionevent)
    {
        boolean isFormLocked = false;
        try
        {
            isFormLocked = m_tcComponent.getLogicalProperty("locked");
        }
        catch (Exception e)
        {
            
        }
        
        if (actionevent.getSource() == m_attachmentsBoxPlus && !isFormLocked)
        {
            NewDatasetDialog datasetDialog = new NewDatasetDialog(AIFUtility.getCurrentApplication().getDesktop(),
                    AIFUtility.getCurrentApplication(), m_tcComponent, true, false);
            datasetDialog.setModal(true);
            datasetDialog.setLocationRelativeTo(this);
            
            if (Toolkit.getDefaultToolkit().getScreenResolution() > 96)
            {
                datasetDialog.setSize(600, 220);
            }
            else
            {
                datasetDialog.setSize(500, 220);
            }
            
            datasetDialog.setDsName(m_reg.getString("DEFLTATTACH.NAME"));
            datasetDialog.setVisible(true);
            
            if (datasetDialog.getDataset() != null)
            {
                Vector<TCComponentDataset> addDataset = new Vector<TCComponentDataset>();
                addDataset.add(datasetDialog.getDataset());
                ((NovAttachmentsTableModel) m_table.getModel()).addRows(addDataset);
                m_table.updateUI();
            }            
        }
        else if (actionevent.getSource() == m_attachmentsBoxMinus && !isFormLocked)
        {
            if (m_table.getModel().getRowCount() > 0)
            {
                if (m_selectedItemRow >= 0 && m_selectedItemRow < m_table.getModel().getRowCount())
                {
                    TCComponentDataset attachedDataset = ((NovAttachmentsTableModel) m_table.getModel()).getAttachment(m_selectedItemRow);
                    TCComponentForm form = (TCComponentForm) m_tcComponent;
                    
                    try
                    {
                        DeleteRelationHelper delete = new DeleteRelationHelper(
                                form,
                                attachedDataset,
                                m_reg.getString("CHNGDESCDOC.TYPE"));
                        delete.deleteRelation();
                        //m_tcComponent.refresh();
                    }
                    catch (Exception e)
                    {
                        System.out.println("Could not remove attachment: " + e);
                    }
                    
                    ((NovAttachmentsTableModel) m_table.getModel()).removeRow(m_selectedItemRow);
                    m_table.updateUI();
                }
            }
        }
        else if (actionevent.getSource() == m_viewAttachment)
        {
            if (m_table.getModel().getRowCount() > 0)
            {
                if (m_selectedItemRow >= 0 && m_selectedItemRow < m_table.getModel().getRowCount())
                {
                    TCComponentDataset attachedDataset = ((NovAttachmentsTableModel) m_table.getModel()).getAttachment(m_selectedItemRow);
                    
                    try
                    {
                        String cmd = "";
                        String fetchFileType = ((NovAttachmentsTableModel) m_table.getModel()).getFileRefType(attachedDataset);
                        
                        if (null != fetchFileType && fetchFileType.length() > 0)
                        {
                            String fileName = null;
                            File[] attachedFile = attachedDataset.getFiles(fetchFileType);
                            if (attachedFile != null)
                            {
                                fileName = attachedFile[0].getAbsolutePath();
                                
                                if (fileName != null && fileName.length() > 0)
                                {
                                    cmd = "rundll32 shell32.dll,OpenAs_RunDLL " + fileName;
                                    Runtime.getRuntime().exec(cmd);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.post(m_reg.getString("Reference2ViewMissing.MSG"),
                                    m_reg.getString("ErrorDocView.MSG"), MessageBox.INFORMATION);
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        MessageBox.post(m_reg.getString("FailedonRequiredFun.MSG"), e.toString(),
                                m_reg.getString("ErrorDocView.MSG"), MessageBox.INFORMATION);
                    }
                }
            }
        }
        
    }
    
    private NovAttachmentsTableModel getTableModel()
    {
        try
        {
            m_attachments.clear();
            AIFComponentContext[] comps = m_tcComponent.getSecondary();
            for (int i = 0; i < comps.length; i++)
            {
                if ((comps[i].getComponent() instanceof TCComponentDataset)
                        && (comps[i].getContextDisplayName().equals("_ChangeDescriptionDocument_")))
                {
                    m_attachments.add((TCComponent) comps[i].getComponent());
                }
            }
            
            m_theModel = new NovAttachmentsTableModel(m_attachments);
        }
        catch (Exception e)
        {
            
        }
        
        return m_theModel;
    }
    
    private void setInitialData(ArrayList<TCComponent> arrayList)
    {
        if (m_attachments.size() > 0)
        {
            return;
        }
        
        m_tableData = arrayList;
        
        for (int i = 0; i < m_tableData.size(); i++)
        {
            m_theData.add((TCComponent) m_tableData.get(i));
        }
        
        m_theModel = new NovAttachmentsTableModel(m_theData);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {       
        m_theModel = getTableModel();
        setInitialData(new ArrayList<TCComponent> ());
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
}
