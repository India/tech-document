/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Dimension;

import javax.swing.JPanel;

import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author viveksm
 * 
 */
public class NovDraftedPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long serialVersionUID       = 1L;
    private Registry          m_registry             = null;
    private NOVLOVPopupButton m_draftedByPopUpButton = null;
    private NOVLOVPopupButton m_draftedOnPopUpButton = null;
    private TCComponent       m_tcComponent          = null;
    
    public NovDraftedPanel(Registry registry, TCComponent component)
    {
        m_registry = registry;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        JPanel infoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        JPanel leftInfoPanel = new JPanel(new PropertyLayout(16, 5, 5, 5, 5, 5));
        JPanel rightInfopanel = new JPanel(new PropertyLayout(16, 5, 5, 5, 5, 5));
        
        leftInfoPanel.setPreferredSize(new Dimension(300, 30));
        rightInfopanel.setPreferredSize(new Dimension(300, 30));

		m_draftedByPopUpButton = new NOVLOVPopupButton(){           
            
            private static final long serialVersionUID = 1L;

            @Override            
            public void setEnabled( boolean f )            
            {                
                super.setEnabled( false );            
            }        
        };
        m_draftedByPopUpButton.setPreferredSize(new Dimension(200, 21));
        m_draftedByPopUpButton.setEnabled(false);
        
        m_draftedOnPopUpButton = new NOVLOVPopupButton(){           
            
            private static final long serialVersionUID = 1L;

            @Override            
            public void setEnabled( boolean f )            
            {                
                super.setEnabled( false );            
            }        
        };
        m_draftedOnPopUpButton.setPreferredSize(new Dimension(200, 21));
        m_draftedOnPopUpButton.setEnabled(false);
        
        leftInfoPanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("draftedby.LABEL")));
        leftInfoPanel.add("1.2.left.center", m_draftedByPopUpButton);
        
        rightInfopanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("draftedon.LABEL")));
        rightInfopanel.add("1.2.left.center", m_draftedOnPopUpButton);
        
        infoPanel.add("1.1.left.center", leftInfoPanel);
        infoPanel.add("1.2.left.center", rightInfopanel);
        infoPanel.setPreferredSize(new Dimension(630, 30));
        
        JPanel panel = new JPanel(new PropertyLayout());
        panel.add("1.1.center.center", infoPanel);
        
        setLayout(new PropertyLayout(0, 0, 0, 0, 0, 0));
        this.add("1.1.left.center", panel);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String dreatedBy = PropertyMapHelper.handleNull(propMap.getString(NovProcessFormHelper.PROP_DRAFTEDBY));
        m_draftedByPopUpButton.setSelectedText(dreatedBy);
        
        String dreatedOn = PropertyMapHelper.handleNull(propMap.getDate(NovProcessFormHelper.PROP_DRAFTEDON));
        m_draftedOnPopUpButton.setSelectedText(dreatedOn);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
}
