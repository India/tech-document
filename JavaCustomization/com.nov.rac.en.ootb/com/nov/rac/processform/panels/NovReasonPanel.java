/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.MessageBoxHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.nov.rac.utilities.utils.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

/**
 * @author MundadaV
 * 
 */
public class NovReasonPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long serialVersionUID = 1L;
    private Registry          m_reg            = null;
    private iTextArea         m_reasonProp     = null;
    private TCComponent       m_tcComponent    = null;
    
    public NovReasonPanel(Registry reg, TCComponent component)
    {
        m_reg = reg;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        JPanel reasonPanel = new JPanel();
        reasonPanel.setBackground(this.getBackground());
        
        m_reasonProp = new iTextArea();
        m_reasonProp.setRequired(true);
        
        JScrollPane reasonPane = new JScrollPane((m_reasonProp));
        reasonPane.setPreferredSize(new Dimension(590, 90));
        
        TitledBorder titleBorder = new TitledBorder(m_reg.getString("reasonForNotice.LABEL"));
        titleBorder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        reasonPanel.setBorder(titleBorder);
        reasonPanel.add(reasonPane);
        
        setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));
        this.add("1.1.left.center", reasonPanel);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String reasonProp = PropertyMapHelper.handleNull(propMap.getString(NovProcessFormHelper.PROP_EXPLANATION));
        m_reasonProp.setText(reasonProp);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        String newReason = m_reasonProp.getText();
        propMap.setString(NovProcessFormHelper.PROP_EXPLANATION, newReason);
        
        return true;

    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
    	boolean isValid = true;
    	String reason = m_reasonProp.getText();
    	
    	if(reason == null || reason.equals(""))
    	{
    		Component comp	= null;
    		comp = this.getParent().getParent();
			MessageBox.post(MessageBoxHelper.getWindow(comp), "ERROR", "\"Reason For Notice\" is a mandatory field, please enter the reason",
																																	MessageBox.ERROR);  		
    		isValid = false;
    	}
    	
        return isValid;
    }
}
