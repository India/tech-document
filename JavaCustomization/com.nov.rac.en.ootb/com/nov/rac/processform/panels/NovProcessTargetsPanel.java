/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import com.nov.rac.processform.NovProcessFormHelper;
import com.nov.rac.processform.helper.NovObjectsHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.AlternateIDHelper;
import com.nov.rac.utilities.utils.MessageBox;
import com.nov.rac.utilities.utils.MessageBoxHelper;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author viveksm
 * 
 */
public class NovProcessTargetsPanel extends AbstractSwingUIPanel implements ILoadSave 
{
	private static final long 	 serialVersionUID 	  = 1L;
	private Registry 			 m_reg 				  = null;
	private AIFTableModel 		 m_tableModel 		  = null;
	private TCComponent 		 m_tcComponent 		  = null;
	private NovTablePanel 		 m_targetsTable       = new NovTablePanel();
	private Vector<TCComponent>  m_dispositionTargets = null;
	private Component 			 m_comp				  = null;

	public NovProcessTargetsPanel(Registry reg, TCComponent component) 
	{
		m_reg = reg;
		this.m_tcComponent = component;
		
		try
		{
			createUI();
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}

	public boolean createUI() throws TCException
	{
		JPanel processTargetPanel = new JPanel(new BorderLayout());
		processTargetPanel.setBackground(this.getBackground());

		String columnNames[] = { "Line No", "Item ID", "Current Status", "Future Status" };
		m_targetsTable = new NovTablePanel(columnNames);
		m_targetsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		m_tableModel = (AIFTableModel) m_targetsTable.getModel();

		JScrollPane scrollPane = new JScrollPane(m_targetsTable);
		scrollPane.setPreferredSize(new Dimension(600, 100));

		TitledBorder titledBorder = new TitledBorder(m_reg.getString("processTargets.LABEL"));
		titledBorder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		processTargetPanel.setBorder(titledBorder);
		processTargetPanel.add(scrollPane);

		setFutureColumn(m_targetsTable.getColumnModel().getColumn(3));

		setLayout(new PropertyLayout(5, 20, 5, 5, 20, 0));
		this.add("1.1.left.center", processTargetPanel);

		return true;
	}

	public AIFTableModel getTableModel() 
	{
		return m_tableModel;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		m_dispositionTargets = NovObjectsHelper.getTargetDisposition(propMap);
		
		m_targetsTable.loadTabledata(m_dispositionTargets);
		
		return true;
	}

	private void setFutureColumn(TableColumn futureColumn) 
	{
		String obsoleteLabels[] = { "", "Obsolete", "Replacement" };
		JComboBox futureComboBox = new JComboBox(obsoleteLabels);

		futureColumn.setCellEditor(new DefaultCellEditor(futureComboBox));
		futureComboBox.setSelectedIndex(0);

		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for combo box");
		futureColumn.setCellRenderer(renderer);
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException 
	{
		if (m_dispositionTargets != null) 
		{
			TCComponent tcComponent[] = m_dispositionTargets.toArray(new TCComponent[m_dispositionTargets.size()]);

			for (int inx = 0; inx < tcComponent.length; inx++) 
			{
				tcComponent[inx].getTCProperty("futurestatus").setStringValue(m_targetsTable.getModel().getValueAt(inx, 3).toString());
			}

			 propMap.setTagArray(NovProcessFormHelper.PROP_TARGETDISPOSITION, tcComponent);
		}

		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException 
	{
		boolean checkDuplicateStatus = true;

		checkDuplicateStatus = isValidate(propMap);

		return checkDuplicateStatus;
	}

	private boolean isValidate(IPropertyMap propMap) 
	{
		m_comp = this.getParent().getParent();
		boolean checkStatus = true;
		TCComponent obsoleteObject = NovObjectsHelper.getObsoleteObject(m_dispositionTargets, m_targetsTable.getModel());
		NovObjectsHelper.setObsoleteComponent(obsoleteObject);

		if(m_dispositionTargets != null)
		{
			TCComponent tcComponent[] = m_dispositionTargets.toArray(new TCComponent[m_dispositionTargets.size()]);
		
			List<String> statuses = new ArrayList<String>();
		
			for(int inx=0; inx<tcComponent.length; inx++)
			{
				String obsolete = m_targetsTable.getModel().getValueAt(inx,3).toString();
		
				if(obsolete == null || obsolete.equals(" "))
				{
					MessageBox.post(MessageBoxHelper.getWindow(m_comp), "ERROR", "Status value can not be empty for any targets", 
																														MessageBox.ERROR);		
					checkStatus = false;
					return checkStatus;
		
				}
				else
				{
					if(obsolete.equals("Obsolete") && statuses.contains(obsolete))
					{
						MessageBox.post(MessageBoxHelper.getWindow(m_comp), "ERROR", "Please choose only one target as Obsolete status", 
																																MessageBox.ERROR);		
						checkStatus = false;
						return checkStatus;
					}
					else
					{
						statuses.add(obsolete);
					}
				}
			}
		
		 if(!statuses.contains("Obsolete") && checkStatus == true)
		 {
			 MessageBox.post(MessageBoxHelper.getWindow(m_comp), "ERROR", "Please choose at least one target as Obsolete status", 
					 																										MessageBox.ERROR);				
			 checkStatus = false;
			 return checkStatus;
		 }
		 }

		 checkStatus = validateOracleId(propMap);

		 return checkStatus;
	}

	private boolean validateOracleId(IPropertyMap propMap) 
	{
		m_comp = this.getParent().getParent();
		boolean isValid = true;
		m_dispositionTargets = NovObjectsHelper.getTargetDisposition(propMap);
		
		Vector<TCComponent> targetItems = NovObjectsHelper.loadTargets(m_tcComponent);
		Vector<TCComponent> replacementObjects = NovObjectsHelper.getReplacementObject(m_dispositionTargets,m_targetsTable.getModel());
		String replacementObjsIds[] = new String[replacementObjects.size()];

		try 
		{
			for (int index = 0; index < replacementObjects.size(); ++index) 
			{
				replacementObjsIds[index] = replacementObjects.get(index).getProperty("targetitemid");
			}

			List<String> replacementList = Arrays.asList(replacementObjsIds);
			List<String> oracleIdsList = new ArrayList<String>();
			
			for (int inx = 0; inx < targetItems.size(); ++inx) 
			{
				String objectId = targetItems.get(inx).getProperty("item_id");

				if (replacementList.contains(objectId)) 
				{
					String[] alIds = AlternateIDHelper.getAlternateIds(targetItems.get(inx));
					//TCProperty property = targetItems.get(inx).getTCProperty("altid_list");
					//String propertyValue = property.toString();
					if(null != alIds && alIds.length > 0)
					{
						oracleIdsList.addAll(Arrays.asList(alIds));
					}
					else
					{
						oracleIdsList.add("");
					}
				}
			}

			if (oracleIdsList.contains("") ) 
			{
				String messageString = null;
				List<String> emptyList = new ArrayList<String>();
				emptyList.add("");

				List<String> noEmptyList = new ArrayList<String>();
				noEmptyList.addAll(oracleIdsList);
				noEmptyList.removeAll(emptyList);

				if (noEmptyList.isEmpty()) 
				{				
					messageString = "Disposition objects have no oracle ID";
				} 
				else 
				{
					messageString = "Some disposition objects have no oracle ID";
				}
				
				isValid = getObsoleteAltIdValidation(targetItems, messageString);

				return isValid;
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}

		return isValid;
	}
	
	private boolean getObsoleteAltIdValidation(Vector<TCComponent> targetItems, String messageString)
	{
		boolean isValid = true;
		String itemId = null;
		//String propertyValue = null;
		TCComponent obsoleteObject = NovObjectsHelper.getObsoleteObject(m_dispositionTargets, m_targetsTable.getModel()); 
		try 
		{
			itemId = obsoleteObject.getProperty("targetitemid");

			for(int inx=0; inx<targetItems.size(); ++inx)
			{
				String objectId = targetItems.get(inx).getProperty("item_id");
				
				if(itemId.equals(objectId))
				{
					String[] altIds = AlternateIDHelper.getAlternateIds(targetItems.get(inx));
					
					//TCProperty property = targetItems.get(inx).getTCProperty("altid_list");
					//propertyValue = property.toString();
					
					//if(propertyValue.length() > 0 && !propertyValue.equals(""))
					if(null != altIds && altIds.length > 0)
					{
						MessageBox.post(MessageBoxHelper.getWindow(m_comp), "ERROR", messageString, MessageBox.ERROR);
						isValid = false;
						return isValid;
					}
				}
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		return isValid;
	}
}
