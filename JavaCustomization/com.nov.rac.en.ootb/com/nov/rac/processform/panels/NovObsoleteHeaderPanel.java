/**
 * 
 */
package com.nov.rac.processform.panels;

import java.awt.Dimension;

import javax.swing.JPanel;

import com.noi.rac.en.util.components.NOIJLabel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractSwingUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/**
 * @author MundadaV
 *
 */
public class NovObsoleteHeaderPanel extends AbstractSwingUIPanel implements ILoadSave
{
    private static final long serialVersionUID 		 = 1L;
    private Registry          m_registry             = null;
    private TCComponent       m_tcComponent          = null;
    
    public NovObsoleteHeaderPanel(Registry registry, TCComponent component)
    {
        m_registry = registry;
        this.m_tcComponent = component;
        
        try 
        {
			createUI();
		} 
        catch (TCException e)
        {
			e.printStackTrace();
		}
    }
    
    public boolean createUI() throws TCException
    {
        JPanel topPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        topPanel.add("1.1.left.center", new NOIJLabel(m_registry.getString("EngineeringObsolete.LABEL")));
        topPanel.setPreferredSize(new Dimension(650, 30));
        
        JPanel panel = new JPanel(new PropertyLayout());
        panel.add("1.1.center.center", topPanel);
        
        
        setLayout(new PropertyLayout(0, 0, 0, 0, 0, 0));
        this.add("1.1.left.center", panel);
        
        return true;
    
    }

	@Override
	public boolean load(IPropertyMap propMap) throws TCException
	{
		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException
	{
		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException
	{
		return true;
	}
    
}
