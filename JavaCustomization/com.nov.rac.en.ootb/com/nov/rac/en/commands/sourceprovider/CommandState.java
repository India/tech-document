package com.nov.rac.en.commands.sourceprovider;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.AbstractSourceProvider;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.SessionChangedEvent;
import com.teamcenter.rac.kernel.SessionChangedListener;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;

	public class CommandState extends AbstractSourceProvider implements SessionChangedListener
	{
	  public CommandState()
	  {
		  super();
			m_session = (TCSession) AIFUtility.getDefaultSession();
		    m_session.addSessionChangeListener(this);
	  }
	
	
	public final static String MY_STATE = "com.nov.rac.en.commands.sourceprovider.active";
	public final static String ENABLED = "ENABLED";
	public final static String DISABLE = "DISABLE";
	public final static String ATTRIBUTE_SPLIT_CHAR = ">";
  
  TCSession m_session=null;

  @Override
  public void dispose()
  {
  }

  @Override
  public String[] getProvidedSourceNames()
  {
    return new String[] { MY_STATE };
  }

  @Override
  public Map getCurrentState() 
  {
    Map map = new HashMap(1);
    String value = getStatus();
    
    map.put(MY_STATE, value);

    return map;
  }

  private String getStatus()
  {
	String value=DISABLE;
	TCPreferenceService prefService = m_session.getPreferenceService();
     
    String[] m_users_profile = prefService.getStringArray(TCPreferenceService.TC_preference_site, "NOV4_Allowed_Users_For_MBC");
  
    for(int i=0;i<m_users_profile.length;i++)
    {
    	String[] m_SeparatorList=null;
    	m_SeparatorList = Pattern.compile(ATTRIBUTE_SPLIT_CHAR, Pattern.LITERAL).split(m_users_profile[i]);
		if(m_session.getUserName().equals(m_SeparatorList[0]) && m_session.getRole().toString().equals(m_SeparatorList[1]))
		{
			value=ENABLED;
			break;
		}
	}

	return value;
  }

	@Override
	public void sessionChanged(SessionChangedEvent arg0)
	{
		final Map map = new HashMap(1);
		String value = getStatus();
		
		map.put(MY_STATE, value);
		Display.getDefault().syncExec( new Runnable()
		{
			public void run() 
			{
				fireSourceChanged(0, map);
			}
		}
		);
		
	}

}