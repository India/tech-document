package com.nov.rac.en.commands;

import java.awt.Frame;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JFrame;

import com.noi.rac.en.util.components.NOVEnDispositionPanel;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentSite;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCSiteInfo;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVENPrintCommand  extends AbstractAIFCommand{

	private TCSession session;
	private Registry reg=Registry.getRegistry(this);
	public NOVENPrintCommand(Frame parent, TCSession session)
	{
		this.reg=Registry.getRegistry(this);
		showMsg();
	}

	public NOVENPrintCommand(Frame parent, InterfaceAIFComponent[] targets)
	{
		if(targets!=null)
		{
			try
			{
				if(targets.length >0)
				{	
					if (targets[0] instanceof TCComponentForm) 
					{
						this.reg=Registry.getRegistry(this);
						ecnEroform =(TCComponentForm)targets[0];
						if (ecnEroform.getType().equalsIgnoreCase(reg.getString("engPreReleaseForm.TYPE"))
								||ecnEroform.getType().equalsIgnoreCase(reg.getString("engNoticeForm.TYPE"))
								|| ecnEroform.getType().equalsIgnoreCase(reg.getString("engSupersedeForm.TYPE")) 
								|| ecnEroform.getType().equalsIgnoreCase(reg.getString("engStopForm.TYPE")) 
								) 
						{
							AIFComponentContext[] contexts=ecnEroform.whereReferenced();			
							if(contexts!=null)
							{
								for(int j=0;j<contexts.length;j++)
								{
									if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
									{
										currentProcess=(TCComponentProcess)contexts[j].getComponent();
										
										TCComponentTask rootTask=((TCComponentProcess)currentProcess).getRootTask();
										TCComponent[] processTargets=rootTask.getAttachments(
												                          TCAttachmentScope.LOCAL, 
												                          TCAttachmentType.TARGET);
										for(int i=0;i<processTargets.length;i++)
										{
											if(processTargets[i] instanceof TCComponentItemRevision)
											{
												vTargetComp.addElement(((TCComponentItemRevision)processTargets[i]).getItem());
											}
										}
										processPUID = rootTask.getUid();
									}
								}
							}
							if(/*(currentProcess==null)&&*/
								( (ecnEroform.getTCProperty(reg.getString("propEprtargetsdisposition.PROP"))!= null ) 
								&& (ecnEroform.getTCProperty(reg.getString("propEprtargetsdisposition.PROP")).getReferenceValueArray()).length>0) )
							{
								Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList
										(ecnEroform.getTCProperty(reg.getString("propEprtargetsdisposition.PROP")).getReferenceValueArray()));
								TCComponentItemRevisionType type=(TCComponentItemRevisionType) 
								ecnEroform.getSession().getTypeService().getTypeComponent(reg.getString("itemRevision.TYPE"));
								for(int k=0;k<tempReference.size();k++)
								{					
									String targetId=(tempReference.get(k).getTCProperty(reg.getString("targetItemId.PROP"))).getStringValue();			
									String revid=(tempReference.get(k).getTCProperty(reg.getString("revisionId.PROP"))).getStringValue();
									TCComponent comp=type.findRevision(targetId, revid);
									if(comp!=null)
										vTargetComp.addElement(((TCComponentItemRevision)comp).getItem());
								}	
								//((NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition")).setDispData(this);
							}		
							
							session=ecnEroform.getSession();
							String formname = ecnEroform.getProperty(reg.getString("objectName.PROP"));
							TCComponentSite site = ecnEroform.getSession().getCurrentSite();
							TCSiteInfo info = site.getSiteInfo();
							String database = info.getSiteName();
							alcprint("enPrint",formname,database);	
						}
						else
						{
							showMsg();
						}	
					}
					else
					{
						showMsg();
					}
				}
				else
				{
					showMsg();
				}
			}
			catch(Exception e1)
			{
				MessageBox.post(e1);
			}
		}
	}
	
	private void showMsg()
	{
		MessageBox.post(reg.getString("showMsg.MSG"), reg.getString("warning.NAME"), MessageBox.WARNING);
	}

	public void alcprint(String uri,String ecnname,String db) throws TCException, IOException {

		boolean groupFlag = false;
		TCPreferenceService prefServ = session.getPreferenceService();
		String server = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGALCServices_server" );
		String port = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGALCServices_port" );
		String context = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGALCServices_context" );
		
		String cmd = "http://"+server+":"+port+context+uri+"?ecn="+ecnname+"&db="+db;
		
		if(ecnEroform.getType().equalsIgnoreCase( reg.getString("engNoticeForm.TYPE")) )
		{
			groupFlag = true;
			cmd = cmd + "&processpuid="+processPUID+"&requestname="+"EN";
		}
		else if(ecnEroform.getType().equalsIgnoreCase( reg.getString("engSupersedeForm.TYPE")) )
		{
			cmd = cmd + "&processpuid="+processPUID+"&requestname="+"SUPERSEDE";
		}
		else if(ecnEroform.getType().equalsIgnoreCase( reg.getString("engStopForm.TYPE")) )
		{
			boolean m_bIsSTOP = false;
			boolean m_bIsCanelSTOP = false;
			String sProcessName = null;
			TCSession session = (TCSession)AIFUtility.getDefaultSession();	
			String[] grpStrArray = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
							reg.getString("STOPProcesses.PREF") );
			try
			{
				AIFComponentContext[] contexts=ecnEroform.whereReferenced();			
				if(contexts!=null)
				{
					for(int j=0;j<contexts.length;j++)
					{
						if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
						{
							TCComponentProcess currentProcess1 =(TCComponentProcess)contexts[j].getComponent();
							TCComponentTask rootTask=((TCComponentProcess)currentProcess1).getRootTask();
							sProcessName = rootTask.toString();					
						}
					}
				}
				if(sProcessName == null)
					return;
			}
			catch (TCException e)
			{
				e.printStackTrace();
				System.out.println("exception while getting where reference for stop form");
				return;
			}			
			for(int i=0; i< grpStrArray.length; i++ )
			{
				String[] sStopPrefVal = grpStrArray[i].split("=");
				
				String[] sPrefWorkFlowNames = sStopPrefVal[1].split(",");
				for(int j=0; j< sPrefWorkFlowNames.length; j++ )
				{	
					if( sPrefWorkFlowNames[j].contains( sProcessName ) )
					{			
						if(sStopPrefVal[0].contentEquals("STOP") )
						{
							m_bIsSTOP = true;
							m_bIsCanelSTOP = false;
							break;
						}
						else if(sStopPrefVal[0].contentEquals("CANCEL-STOP") )
						{
							m_bIsCanelSTOP = true;
							m_bIsSTOP = false;
							break;
						}
					}
				}
			}
			if(m_bIsCanelSTOP == true){
				cmd = cmd + "&processpuid="+processPUID+"&requestname="+"CANCELSTOP";
			}
			else if(m_bIsSTOP == true){
				cmd = cmd + "&processpuid="+processPUID+"&requestname="+"STOP";
			}
		}
		else if(ecnEroform.getType().equalsIgnoreCase(reg.getString("engPreReleaseForm.TYPE")))
		{
			groupFlag = true;
			String strTargetRevs = getTargetPrereleasedRev();
			if(! (strTargetRevs.equalsIgnoreCase("&baselinerev=")) )
				cmd = cmd + "&processpuid="+processPUID+"&requestname="+"EPR"+strTargetRevs;
			else
				cmd = cmd + "&processpuid="+processPUID+"&requestname="+"EPR";
		}
		boolean missFlag = false;
		String grp = "RS";
		String strOwnGrp = ecnEroform.getProperty("owning_group").toString();
		
		String[] grpStrArray = prefServ.getStringArray(TCPreferenceService.TC_preference_site,reg.getString("MissionGroup.NAME"));

		for(int index=0; grpStrArray !=null && index< grpStrArray.length; index++)
		{
			if( strOwnGrp.equalsIgnoreCase( grpStrArray[index]) )
			{
				grp="Mission";
				missFlag = true;
				break;
			}
		}
		if(groupFlag == true)
			cmd = cmd + "&group="+grp;
		if(ecnEroform.getType().equalsIgnoreCase( reg.getString("engNoticeForm.TYPE")) )
		{
			String version = ecnEroform.getProperty("nov4_en_version").toString();
			if(version.equalsIgnoreCase("1"))
			{
				if(missFlag == false)
				{
					cmd = cmd + "&version="+3.2;
				}
				else
				{
					cmd = cmd + "&version="+"3.2_2";
				}

			}
			else
			{
				cmd = cmd + "&version="+3.1;
			}
		}
		Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+cmd);
	}
	
	public String getTargetPrereleasedRev()
	{
		String strBaselineRev = "&baselinerev=";
		String strTargetRevs = "";// item_id, ";;", prev_rev_id";
		int nTargetComp = vTargetComp.size();
		
		for(int index=0;index<nTargetComp;index++)
		{
			try 
			{
				TCComponentItem compTargetItem = vTargetComp.get(index);
				TCComponent[] compTargetRev = compTargetItem.getRelatedComponents(reg.getString("revisionList.PROP"));
				String strRevId = "";
				String strItemId = compTargetItem.getProperty(reg.getString("itemId.PROP"));
				for(int i=0; compTargetRev!=null &&i< compTargetRev.length; i++ )
				{
					String itemRevId = compTargetRev[i].getProperty(reg.getString("itemRevId.PROP"));
					if(itemRevId.indexOf(reg.getString("preRelStat.TYPE"))!=-1)
					{
						strRevId = itemRevId;
					}
				}
				if(strRevId.length()>0)
				{
					strTargetRevs = strTargetRevs+(strBaselineRev+strItemId+";;"+strRevId);
				}
			}
			catch (TCException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return strTargetRevs;
	}
	
	
	String processPUID = null;
	TCComponentProcess currentProcess = null;
	TCComponentForm ecnEroform = null;
	Vector<TCComponentItem> vTargetComp = new Vector<TCComponentItem>();
}
