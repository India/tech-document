package com.nov.rac.en.commands;

import java.awt.Frame;

import com.nov.rac.utilities.common.NOVChangeManagementServiceInfo;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentRole;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVCloseECRCommand  extends AbstractAIFCommand{

	private	boolean invalidSel	= false;
	private boolean groupMismatch  = false;
	TCUserService 	userservice;
	TCSession		session;
	public NOVCloseECRCommand(Frame parent, TCSession session)
	{
		//this.reg=Registry.getRegistry(this);
		this.session		= session;
		this.userservice 	= session.getUserService();
	}

	public NOVCloseECRCommand(Frame parent, InterfaceAIFComponent[] targets)
	{
		//check user role to be DDC
		try
		{	
			session=(TCSession)AbstractAIFUIApplication.getCurrentApplication(parent).getSession();
			Registry reg = Registry.getRegistry("com.nov.rac.en.commands.commands");
			//Registry reg = session.getRegistry();
            //Get Role of the logged-in User
			String userRole = session.getRole().getStringProperty(TCComponentRole.PROP_ROLE_NAME);
			String userGrp = session.getGroup().getStringProperty(TCComponentGroup.PROP_GROUP_NAME);
		 
			 //5865-added admin role
            //if( !userRole.equalsIgnoreCase(reg.getString("DBAROLE.NAME"))&& !userGrp.equalsIgnoreCase(reg.getString("DBAGRP.NAME")) )
            if( !((userRole.equalsIgnoreCase(reg.getString("DBAROLE.NAME")) && userGrp.equalsIgnoreCase(reg.getString("DBAGRP.NAME"))) 
                    || userRole.equalsIgnoreCase(reg.getString("ADMROLE.NAME"))) )
            {
				// error message
				System.out.println("Run this utility with group/role=dba/DBA or Admin Role");
				MessageBox.post(parent, reg.getString("InvalidGrp.MSG"), reg.getString("Error.NAME"),
						MessageBox.ERROR);
				return;						
			}	
			if(targets!=null)
			{
				if(targets.length >0)
				{	
					for(int j=0;j<targets.length;j++)
					{
						//check if selection has object other than ECR form
						if((targets[j] instanceof TCComponentForm))
						{						
							TCComponentForm form= (TCComponentForm) targets[j];
							//System.out.println("form type is: "+form.getType());
							//System.out.println("form type from registry is: "+reg.getString("ecrForm.TYPE") );
							if(!form.getType().equalsIgnoreCase(reg.getString("ecrForm.TYPE")))
							{
								//System.out.println("form type is: "+form.getType());
								System.out.println("Invalid form type selected");
								invalidSel = true;
								break;
							}	
							else
							{
							   //TCDECREL-5865 start
							   //Check if the owning group of ECR form matches with the logged in group
							    TCComponentGroup owningGroupComp = (TCComponentGroup) form.getTCProperty(reg.getString("owningGrp.PROP") )
		                        .getReferenceValue();
		                        String owningGroup = owningGroupComp.getGroupName().toString();
		                        if(!userGrp.equalsIgnoreCase(owningGroup) && (!userRole.equalsIgnoreCase(reg.getString("DBAROLE.NAME"))))
		                        {
		                            groupMismatch = true;
		                            break;		                            
		                        }
		                        //TCDECREL-5865 end
		                
							   //check if the ECR form is already released.
								//System.out.println("rel status to check: "+reg.getString("relStatusList.PROP"));
								TCComponent[] relStatusList = 
									form.getTCProperty( reg.getString("relStatusList.PROP") ).getReferenceValueArray();
								//System.out.println("RS Closed status : "+reg.getString("closedStatus.NAME"));
								for (int z = 0; z < relStatusList.length; z++) 
								{
									//System.out.println("release status of form is: "+relStatusList[0].toString());
									if(relStatusList[0].toString().equalsIgnoreCase(reg.getString("closedStatus.NAME"))) 
									{
										System.out.println("Released ECR selected");
									    invalidSel = true;
									    break;
									}
								}
							}
						}
						else
						{
							System.out.println("Invalid type obj selected");
							invalidSel = true;
							break;
						}
					}
					if(invalidSel)
						MessageBox.post(reg.getString("InvalidSelection.MSG"), reg.getString("Error.NAME"),
								MessageBox.ERROR);
					//TCDECREL-5865 start
					else if(groupMismatch)
					    MessageBox.post(reg.getString("groupMismatch.MSG"), reg.getString("Error.NAME"),
                                MessageBox.ERROR);
                    //TCDECREL-5865 end
					else
					{
						Object inputObjs[] = new Object[1];
						TCComponent[]comps = new TCComponent[targets.length];
						for(int i=0; i<targets.length; i++)
						{
							TCComponent comp = (TCComponent) targets[i];
							comps[i] = comp;					
						}
						inputObjs[0] = comps;
						
						NOVChangeManagementServiceInfo novCloseECOService = new NOVChangeManagementServiceInfo();	
						novCloseECOService.setSelectedECO(comps);

						ServiceData ecoServiceData = novCloseECOService.close_ECO();
												
						/*Object obj = session.getUserService().call(reg.getString("closeECR.SERVERCALL"), inputObjs);
						if(obj instanceof String[])
						{
							String[] retStrs = new String[1];
							retStrs[0]= (((String [])obj)[0]);
							if(retStrs[0].equalsIgnoreCase(reg.getString("NoError.MSG")))
								MessageBox.post(reg.getString("closeECRSuccess.MSG"), reg.getString("Info.NAME"), MessageBox.INFORMATION);	
							//System.out.println("Got Strings");
						}*/
					}
				}
				else
					MessageBox.post(reg.getString("noSelection.MSG"), reg.getString("Error.NAME"), MessageBox.ERROR);	
			}
		}
		catch(TCException ex)
		{
			MessageBox.post(ex);
            return;
		}
			
	}
}