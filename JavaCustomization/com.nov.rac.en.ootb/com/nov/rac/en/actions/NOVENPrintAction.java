package com.nov.rac.en.actions;

import java.awt.Frame;


import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aif.common.actions.AbstractAIFAction;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCSession;
public class NOVENPrintAction extends AbstractAIFAction{

	public NOVENPrintAction (AbstractAIFUIApplication theApplication,
            Frame theParent, String theActionName)
    {
    	super (theApplication, theParent, theActionName);    	
    }
    public NOVENPrintAction (AbstractAIFUIApplication theApplication,String theActionName)
    {
    	super (theApplication, theActionName);    	
    }
	
	public void run() {
		try
        {
            InterfaceAIFComponent comps[] = application.getTargetComponents();
            String commandKey = getCommandKey();
            
            AbstractAIFCommand cmd =null;
            if ( comps == null )
            {
              cmd = (AbstractAIFCommand) registry.newInstanceFor (commandKey, 
                		new Object[ ]{parent,(TCSession )(application.getSession())});	
            }
            else
            {
            	cmd = (AbstractAIFCommand) registry.newInstanceFor(commandKey,
                     new Object[] {parent, comps });             
            }
            cmd.executeModal();
        }
        catch ( Exception exce )
        {
        	exce.printStackTrace();
        }
	}
}
