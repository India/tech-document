package com.nov.rac.en.actions;

import java.awt.Frame;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aif.common.actions.AbstractAIFAction;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NOVCloseECRAction extends AbstractAIFAction
{
	public NOVCloseECRAction(Registry reg, Frame parent, String actionName) {
		super(reg, parent, actionName);
		// TODO Auto-generated constructor stub
	}
	public NOVCloseECRAction (AbstractAIFUIApplication theApplication,String theActionName)
    {
    	super (theApplication, theActionName);    	
    }
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try
        {
            InterfaceAIFComponent comps[] = application.getTargetComponents();
            String commandKey = getCommandKey();
            
            AbstractAIFCommand cmd =null;
            if ( comps == null )
            {
              cmd = (AbstractAIFCommand) registry.newInstanceFor (commandKey, 
                		new Object[ ]{parent,(TCSession )(application.getSession())});	
            }
            else
            {
            	cmd = (AbstractAIFCommand) registry.newInstanceFor(commandKey,
                     new Object[] {parent, comps });             
            }
            cmd.executeModal();
        }
        catch ( Exception exce )
        {
        	exce.printStackTrace();
        }
		
	}
	
}