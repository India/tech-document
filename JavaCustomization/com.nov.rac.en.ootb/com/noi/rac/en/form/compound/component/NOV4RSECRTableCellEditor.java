package com.noi.rac.en.form.compound.component;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;

public class NOV4RSECRTableCellEditor extends DefaultCellEditor
{
	private static final long serialVersionUID = 1L;
	
	JCheckBox checkBox;
	
	public NOV4RSECRTableCellEditor( JCheckBox checkBox )
	{
		super( checkBox );
		this.checkBox = checkBox;
	}
	
	@Override
	public Component getTableCellEditorComponent( JTable jtable, Object obj,
			boolean flag, int i, int j )
	{
		return checkBox;
	}

	@Override
	public boolean stopCellEditing()
	{
		return super.stopCellEditing();
	}	
	
//
//	@Override
//	public void addCellEditorListener( CellEditorListener celleditorlistener )
//	{
//	}
//
//	@Override
//	public void cancelCellEditing()
//	{
//	}
//
//	@Override
//	public Object getCellEditorValue()
//	{
//		return null;
//	}
//
//	@Override
//	public boolean isCellEditable( EventObject eventobject )
//	{
//		return false;
//	}
//
//	@Override
//	public void removeCellEditorListener( CellEditorListener celleditorlistener )
//	{
//	}
//
//	@Override
//	public boolean shouldSelectCell( EventObject eventobject )
//	{
//		return false;
//	}
//
//	@Override
//	public boolean stopCellEditing()
//	{
//			return false;
//	}
}
