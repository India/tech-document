package com.noi.rac.en.form.compound.component;

import java.awt.event.KeyEvent;

import javax.swing.*;

import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.teamcenter.rac.util.Registry;

/**
 * @author NiuY
 * @Date:  May 2, 2007
 * @usage: The class is used to create the base component for nov 
 *         customization
 */
public abstract class BaseComponent {
	protected INOVCustomFormProperties novFormProperties;	
	protected JPanel parentPanel;
	protected boolean newrelease;
	private Registry reg;
	public abstract void initialize();
	
	
	public BaseComponent() {
	}

	public BaseComponent(INOVCustomFormProperties novFormProperties, JPanel parentPanel) {
		super();
		this.reg=Registry.getRegistry(this);
		this.novFormProperties = novFormProperties;
		this.parentPanel = parentPanel;			
		initialize();
	}
	
	public BaseComponent(INOVCustomFormProperties novFormProperties, JPanel parentPanel, boolean newrelease) {
		super();
		this.reg=Registry.getRegistry(this);
		this.novFormProperties = novFormProperties;
		this.parentPanel = parentPanel;
		this.newrelease=newrelease;
		initialize();
	}

	/**
	 * This method is used to check the  Textarea  input limitation
	 * @param KeyEvent e
	 * @param String propetyName 
	 * @param int maxLength
	 */
	protected void keyHandlerOfTextAreaLimitation(KeyEvent e, String propertyName, int maxLength) {
		if((JTextArea)e.getSource()==(JTextArea)novFormProperties.getProperty(propertyName)){
			int currentLength = ((JTextArea)novFormProperties.getProperty(propertyName)).getText().length();
			if(currentLength >= maxLength){
				JOptionPane.showMessageDialog((JTextArea)novFormProperties.getProperty(propertyName),
						reg.getString("lengthExceeded.MSG") + maxLength , 
						reg.getString("lengthExceeded.TITLE"), JOptionPane.WARNING_MESSAGE);
				String maxString = ((JTextArea)novFormProperties.getProperty(propertyName)).getText().substring(0,maxLength-1);
				((JTextArea)novFormProperties.getProperty(propertyName)).setText(maxString);
			}
		}
	}


}
