package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
//import com.noi.rac.en.form.compound.util.AddressBookComponent;
import com.noi.rac.en.NationalOilwell;
import com.noi.rac.en.util.Miscellaneous;
import com.noi.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.form.compound.component.AddressBookComponent;
import com.nov.rac.utilities.utils.NOVCustomTextField;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVEnDistributionPanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	Hashtable stringToRef = null;
	Hashtable refToString = null;
	Dimension listSize;
	private NOVCustomTextField  emailAddressField;
	private JButton emailIDSearchButton;
	
	private JButton globalAddressBtn;
	
	JList sourceList = new JList();
	JScrollPane sourceListScroll = new JScrollPane(sourceList);
	JList targetList = new JList();
	
	public JList getTargetList() {
		return targetList;
	}

	JScrollPane targetListScroll = new JScrollPane(targetList);
	
	public AbstractReferenceLOVList sourceData=null;
	public AbstractReferenceLOVList targetData=null;
	
	DefaultListModel sourceListModel;
	DefaultListModel targetListModel;
	
	private JButton addTarget;
	private JButton removeTarget;
	private TCSession session;
	
	private Registry 	reg;
	
	public NOVEnDistributionPanel()
	{
		super();
	}
	public void initUI()
	{
		reg=Registry.getRegistry(this);
		//sourceList = new JList();
		sourceListModel = new DefaultListModel();
		sourceList.setModel(sourceListModel);
		sourceListScroll = new JScrollPane(sourceList);
		sourceListScroll.setPreferredSize(new Dimension(260,120));
		
		targetList = new JList();
		targetListModel = new DefaultListModel();
		targetList.setModel(targetListModel);
		
		targetListScroll = new JScrollPane(targetList);
		targetListScroll.setPreferredSize(new Dimension(260,120));
		
		sourceList.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) 
			{
				if (!targetList.isSelectionEmpty()) 
				{
					targetList.clearSelection();
				}
			}
		});
		
		targetList.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) 
			{
				if (!sourceList.isSelectionEmpty()) 
				{
					sourceList.clearSelection();
				}
			}
		});
        
        ImageIcon plusIcon = reg.getImageIcon("enadded.ICON");
		//ImageIcon minusIcon =new ImageIcon();
		ImageIcon minusIcon = reg.getImageIcon("enremove.ICON");
		//ImageIcon minusIcon =new ImageIcon(Miscellaneous.getImageIcon(reg.getString("enremove.ICON")));
		//ImageIcon addrIcon =new ImageIcon();
		ImageIcon addrIcon = reg.getImageIcon("enaddr.ICON");
		//ImageIcon addrIcon =new ImageIcon(Miscellaneous.getImageIcon(reg.getString("enaddr.ICON")));
		
		addTarget = new JButton();
		addTarget.setIcon(plusIcon);
		addTarget.addActionListener(this);
		removeTarget = new JButton();
		removeTarget.setIcon(minusIcon);
		removeTarget.addActionListener(this);
		
		JPanel addremButtonPanel = new JPanel(new PropertyLayout(5 , 10 , 5 , 5 , 50 , 20));
		addremButtonPanel.add("1.1.left.center",addTarget);
		addremButtonPanel.add("2.1.left.center",removeTarget);
		
		JPanel sourcePanel = new JPanel(new PropertyLayout());
		NOIJLabel srcLbl = new NOIJLabel(reg.getString("AVAILABLE.LABEL"));
		sourcePanel.add("1.1.center.center",srcLbl);
		sourcePanel.add("2.1.left.top",sourceListScroll);
		
		JPanel targetPanel = new JPanel(new PropertyLayout());
		NOIJLabel tarLbl = new NOIJLabel(reg.getString("SELECTED.LABEL"));
		targetPanel.add("1.1.center.center",tarLbl);
		targetPanel.add("2.1.left.top",targetListScroll);
		
		//TCDECREL-3786:Start
        //emailAddressField = new JTextField(25);
        emailAddressField = new NOVCustomTextField(reg.getString("emailAddress.NAME"),NationalOilwell.MAXEMAILIDLEN);
        emailAddressField.setColumns(25);
        //TCDECREL-3786:End
		
		emailIDSearchButton = new JButton(plusIcon);
		emailIDSearchButton.setPreferredSize(new Dimension(20,20));
		emailIDSearchButton.addActionListener(this);
		
		globalAddressBtn = new JButton();
		globalAddressBtn.setIcon(addrIcon);
		globalAddressBtn.addActionListener(this);
		globalAddressBtn.setPreferredSize(new Dimension(20,20));
		
		JPanel searchPanel = new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		JPanel bkPanel = new JPanel();
		bkPanel.setPreferredSize(new Dimension(54,20));
		searchPanel.add("1.1.right.right",bkPanel);
		searchPanel.add("1.2.right.right",emailAddressField);
		searchPanel.add("1.3.right.right",emailIDSearchButton);
		searchPanel.add("1.4.right.right",globalAddressBtn);
		
		this.setLayout(new PropertyLayout());
		this.add("1.1.left.top", sourcePanel);
		this.add("1.2.left.top", addremButtonPanel);
		this.add("1.3.left.top", targetPanel);
		this.add("2.1.left.top", bkPanel);
		this.add("2.2.left.top", searchPanel);
		populateSourceList();
		populateTargetList();
	}

	public void setTitleBorderText(String title)
	{
		TitledBorder tb = new TitledBorder(title);
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		setBorder(tb);
	}
	
	public void actionPerformed(ActionEvent actEvt) 
	{/*
		if (actEvt.getSource() ==  globalAddressBtn )
		{
			AddressBookComponent comp = new AddressBookComponent(null ,
					emailAddressField , false);
			comp.searchField.setText(emailAddressField.getText());
            comp.setVisible(true);
		}
		if (actEvt.getSource() ==  emailIDSearchButton )
		{
			if (isValidEmail(emailAddressField.getText())) 
			{
				int objInd = targetListModel.indexOf(emailAddressField.getText());
				if (objInd>=0) 
				{
					MessageBox.post("The target list already has E-Mail ID(s) :"+ emailAddressField.getText(),"Information", MessageBox.INFORMATION);
				}
				else
				{
					targetListModel.addElement(emailAddressField.getText());
				}
			}
			else
			{
				//emailAddressField.setText("");
				MessageBox.post("Email address format should be like \"john.smith@nov.com\" .","Information", MessageBox.INFORMATION);
			}
		}
		if (actEvt.getSource() ==  addTarget ) 
		{
			Object[] objs = sourceList.getSelectedValues();
			List<Object> existComps = new ArrayList<Object>();
			for (int i = 0; i < objs.length; i++) 
			{
				int objInd = targetListModel.indexOf(objs[i].toString());
				if (objInd >= 0) 
				{
					existComps.add(objs[i].toString());
				}
				else
				{
				    targetListModel.addElement(objs[i].toString());
				}
			}
			if (existComps.size()>0) 
			{
				showErrorIfListisNotEmpty(existComps);
			}
		}
		if (actEvt.getSource() ==  removeTarget ) 
		{
			Object[] selObjs = targetList.getSelectedValues();
			for (int i = 0; i < selObjs.length; i++) 
			{
				targetListModel.removeElement(selObjs[i]);
			}
		}
	*/
		
		if (actEvt.getSource() ==  globalAddressBtn )
		{
			AddressBookComponent comp = new AddressBookComponent(AIFUtility.getCurrentApplication().getDesktop(),
                    targetData, targetList, listSize.width);
			comp.searchField.setText(emailAddressField.getText());
            comp.setVisible(true);
		}
		if (actEvt.getSource() ==  emailIDSearchButton )
		{
			if (isValidEmail(emailAddressField.getText())) 
			{
				int objInd = targetListModel.indexOf(emailAddressField.getText());
				if (objInd>=0) 
				{
					MessageBox.post(reg.getString("hasMailIds.MSG")+" : "+ emailAddressField.getText(),
							reg.getString("info.TITLE"), MessageBox.INFORMATION);
				}
				else
				{
					targetListModel.addElement(emailAddressField.getText());
					populateTargetList();
					/*String[] targetListData = new String[targetListModel.getSize()];
					for(int i=0;i<targetListModel.getSize();i++)
						targetListData[i]=(String) targetListModel.getElementAt(i);
					targetList.setListData(targetListData);
					targetList.setPreferredSize(new Dimension(listSize.width,17*(targetListModel.getSize())));    */ 
				}
			}
			else
			{
				//emailAddressField.setText("");
				MessageBox.post(reg.getString("emailIdFormat.MSG"),
						reg.getString("info.TITLE"), MessageBox.INFORMATION);
			}
		}
		if (actEvt.getSource() ==  addTarget ) 
		{
			/*Object[] objs = sourceList.getSelectedValues();
			List<Object> existComps = new ArrayList<Object>();
			for (int i = 0; i < objs.length; i++) 
			{
				int objInd = targetListModel.indexOf(objs[i].toString());
				if (objInd >= 0) 
				{
					existComps.add(objs[i].toString());
				}
				else
				{
				    targetListModel.addElement(objs[i].toString());
				}
			}
			if (existComps.size()>0) 
			{
				showErrorIfListisNotEmpty(existComps);
			}*/
			Object[] vals = sourceList.getSelectedValues();
			for (int i=0;i<vals.length;i++) {
				String str = (String)vals[i];
				Object c = sourceData.getReferenceFromString(str);
				targetData.addItem(c, str);
				sourceData.addExclusion(str);
				populateSourceList();
				populateTargetList();
				this.repaint();
			}
		}
		if (actEvt.getSource() ==  removeTarget ) 
		{
			/*Object[] selObjs = targetList.getSelectedValues();
			for (int i = 0; i < selObjs.length; i++) 
			{
				targetListModel.removeElement(selObjs[i]);
			}*/
			Object[] vals = targetList.getSelectedValues();
			for (int i=0;i<vals.length;i++) {
				String str = (String)vals[i];
				Object c = targetData.getReferenceFromString(str);
				if(c!=null)
				{
					targetData.removeItem(c,str);
					targetListModel.removeElement(str);
	//				if(str.indexOf('*')==0)
	//				{
	//				String tmpstr=str.substring(1);
	//				sourceData.removeExclusion(tmpstr);
	//				}
	//				else
					sourceData.addItem(str, str);
					sourceData.removeExclusion(str);
					populateSourceList();
					populateTargetList();
				}
				else
				{
					for ( i=0;i<vals.length;i++) {
					targetData.removeItem(vals[i], (String) vals[i]);
					targetListModel.removeElement(vals[i]);
					}
					populateTargetList();
				}
				this.repaint();
			}
		}
	}
	
	//4907-start
	public void enableDistributionPanel(boolean isEnabled)
	{
		addTarget.setEnabled(isEnabled);
		removeTarget.setEnabled(isEnabled);
		emailIDSearchButton.setEnabled(isEnabled);
		globalAddressBtn.setEnabled(isEnabled);
		
	}
	//4907-end
	
	public void disableButtons()
	{
	    addTarget.setEnabled(false);
	}
	public List getTargetTextValues() {
		ArrayList retArray = new ArrayList();
		int listCount = targetList.getModel().getSize();
		for (int i=0;i<listCount;i++) 
			retArray.add((String)targetList.getModel().getElementAt(i));
		return retArray;
	}
	public List getTargetReferenceValues() {
		ArrayList retArray = new ArrayList();
		int listCount = targetList.getModel().getSize();
		for (int i=0;i<listCount;i++) 
			retArray.add((TCComponent)stringToRef.get(targetList.getModel().getElementAt(i)));

		return retArray;
	}
	public void init(AbstractReferenceLOVList source,AbstractReferenceLOVList target,Dimension size) {
		sourceData = source;
		targetData = target;	
		listSize=size;
		initUI();        
	}
	public void setSelectedTextValues(String[] list) {


	}
	private void populateSourceList() {
		String[] listValues = sourceData.getList();		
		sourceList.setListData(listValues);
		sourceList.setPreferredSize(new Dimension(listSize.width,17*listValues.length));
	}
	private void populateTargetList() {
		String[] listValues = targetData.getList();
		String[] targetGlobalAddList = getTargetListObjects();
		int length = 0;
		Vector<String> vTargetList = new Vector<String>();
		
		for(int i=0;targetGlobalAddList!=null && i<targetGlobalAddList.length;i++)
		{
			vTargetList.addElement(targetGlobalAddList[i]);
		}
		
		for(int j=0;listValues!=null && j<listValues.length;j++)
		{
			vTargetList.addElement(listValues[j]);
		}
		String []targetListStr = vTargetList.toArray(new String[vTargetList.size()]);
		
		targetList.setListData(targetListStr);
		targetList.setPreferredSize(new Dimension(listSize.width,17*targetListStr.length));        
	}
	
	public boolean isValidEmail(String mailID)
	{
		String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*" +
                    "@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        
        Pattern p = Pattern.compile(EMAIL_PATTERN);
        Matcher m = p.matcher(mailID);

		if(!m.find())
		{
			return false;
		}		
		
		return true;
	}
	public static boolean isValidEmailAddress(String aEmailAddress){
	    if (aEmailAddress == null) return false;
	    boolean result = true;
	    try {
	      InternetAddress emailAddr = new InternetAddress(aEmailAddress);
	      if ( ! hasNameAndDomain(aEmailAddress) ) {
	        result = false;
	      }
	    }
	    catch (AddressException ex){
	      result = false;
	    }
	    return result;
	  }

	  private static boolean hasNameAndDomain(String aEmailAddress){
	    String[] tokens = aEmailAddress.split("@");
	    return 
	     tokens.length == 2 && (null!=tokens[0] && tokens[0].length()>0) &&
	     (null!=tokens[1] && tokens[1].length()>0);
	    /*Util.textHasContent(  tokens[0] ) && 
	     Util.textHasContent( tokens[1] ) ;*/
	  }

	private void showErrorIfListisNotEmpty(List<Object> existComps)
	{
		String errorStr =reg.getString("hasMailIds.MSG");
		
		for (int i = 0; i < existComps.size(); i++) 
		{
			errorStr = errorStr + existComps.get(i).toString()+",";
		}
		if (errorStr.charAt(errorStr.length()-1) == ',') 
		{
			errorStr = errorStr.substring(0, errorStr.length()-1);
		}
		
		//TODO: need to change the title of messagebox
		MessageBox.post(errorStr,reg.getString("info.TITLE"), MessageBox.INFORMATION);
		
	}
	
	public void populateSourceList(Object[] sourceObjs)
	{
		if (sourceObjs!=null) 
		{
			if (sourceListModel.getSize()>0) 
			{
				sourceListModel.clear();
			}
			for (int i = 0; i < sourceObjs.length; i++) 
			{
				if(!sourceListModel.contains(sourceObjs[i].toString()))
				{
					sourceListModel.addElement(sourceObjs[i].toString());
				}
			}	
		}
	}
	
	public String[] getAllTargetListObjects()
	{
		String[] listValues = targetData.getList();
		String[] targetGlobalAddList = getTargetListObjects();
		Vector<String> vTargetList = new Vector<String>();
		
		for(int i=0;targetGlobalAddList!=null && i<targetGlobalAddList.length;i++)
		{
			vTargetList.addElement(targetGlobalAddList[i]);
		}
		
		for(int j=0;listValues!=null && j<listValues.length;j++)
		{
			vTargetList.addElement(listValues[j]);
		}
		String []targetListStr = vTargetList.toArray(new String[vTargetList.size()]);
		
		return targetListStr;
	}
	
	public String[] getTargetListObjects()
	{
		String [] retObjs = new String[targetListModel.getSize()];
		for (int i = 0; i < targetListModel.getSize(); i++) 
		{
			retObjs[i]=targetListModel.getElementAt(i).toString();
		}
		return retObjs;
	}

	public void populateTargetList(String[] diststributionList) 
	{
		if(targetListModel==null)
		{
			targetList = new JList();
			targetListModel = new DefaultListModel();
			targetList.setModel(targetListModel);
		}
		if (diststributionList!=null && targetListModel!=null) 
		{
			for (int i = 0; i < diststributionList.length; i++) 
			{
				targetListModel.addElement(diststributionList[i]);
			}	
		}	
	}

	//Resize based on width and height
	public void setNewDimention(int width,int height)
	{
		this.setPreferredSize(new Dimension(width, height));
		sourceListScroll.setPreferredSize(new Dimension((int)(width/2.3),(int)(height/1.6)));
		targetListScroll.setPreferredSize(new Dimension((int)(width/2.3),(int)(height/1.6)));
	}
		
}