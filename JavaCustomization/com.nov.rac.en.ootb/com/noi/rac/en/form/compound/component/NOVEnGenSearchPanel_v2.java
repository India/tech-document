package com.noi.rac.en.form.compound.component;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.util.StringUtils;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.queries.General;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;
import com.noi.rac.en.customize.util.MessageBox;

public class NOVEnGenSearchPanel_v2 extends JPanel implements ActionListener
{
    private static final long serialVersionUID = 1L;
    
    private JTextField searchField;
    private JButton searchButton;
    
    private JList sourceList;
    NOIJLabel srcLbl;
    private JScrollPane sourceListScroll;
    private JList targetList;
    private JScrollPane targetListScroll;
    
    DefaultListModel sourceListModel;
    DefaultListModel targetListModel;
    
    JPanel addremButtonPanel;
    JPanel sourcePanel;
    JPanel targetPanel;
    JPanel searchPanel;
    
    private JButton addTarget;
    private JButton removeTarget;
    private String searchTypeStr;
    private TCSession tcSession;
    private Component comp = null; // Vivek
    
    private Registry reg;
    
    public NOVEnGenSearchPanel_v2(TCSession session, String searchObjType)
    {
        reg = Registry.getRegistry(this);
        tcSession = session;
        searchTypeStr = searchObjType;
        searchField = new JTextField(15);
        searchButton = new JButton();
        searchButton.setEnabled(false);
        searchButton.addActionListener(this);
        
        sourceList = new JList();
        sourceListModel = new DefaultListModel();
        sourceList.setModel(sourceListModel);
        // Scrollbar change
        // sourceList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        sourceList.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                int clickCount = e.getClickCount();
                if (clickCount == 2)
                {
                    // TCDECREL-3261
                    // addSelValToTargetList();
                    String selectedValue = (String) sourceList.getSelectedValue();
                    openECRForm(selectedValue);
                    // TCDECREL-3261
                }
            }
        });
        sourceListScroll = new JScrollPane(sourceList);
        sourceListScroll.setPreferredSize(new Dimension(375, 80));// TCDECREl-900
        
        targetList = new JList();
        targetListModel = new DefaultListModel();
        targetList.setModel(targetListModel);
        // Scrollbar change
        // targetList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        targetListScroll = new JScrollPane(targetList);
        targetListScroll.setPreferredSize(new Dimension(375, 80));// TCDECREl-900
        
        sourceList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (!targetList.isSelectionEmpty())
                {
                    targetList.clearSelection();
                }
            }
        });
        
        targetList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (!sourceList.isSelectionEmpty())
                {
                    sourceList.clearSelection();
                }
                
                // TCDECREL-3261
                if (e.getClickCount() == 2)
                {
                    String selectedValue = (String) targetList.getSelectedValue();
                    openECRForm(selectedValue);
                }
                // TCDECREL-3261
            }
        });
        
        // TODO: need to change the path if we are integrating in other plugin
        ImageIcon plusIcon = reg.getImageIcon("enadded.ICON");
        ImageIcon minusIcon = reg.getImageIcon("enremove.ICON");
        
        addTarget = new JButton();
        addTarget.setIcon(plusIcon);
        addTarget.addActionListener(this);
        removeTarget = new JButton();
        removeTarget.setIcon(minusIcon);
        removeTarget.addActionListener(this);
        
        addremButtonPanel = new JPanel(new PropertyLayout(5, 10, 5, 5, 30, 5));// TCDECREl-900
        addremButtonPanel.add("1.1.left.center", addTarget);
        addremButtonPanel.add("2.1.left.center", removeTarget);
        
        sourcePanel = new JPanel(new PropertyLayout());
        srcLbl = new NOIJLabel();
        sourcePanel.add("1.1.center.center", srcLbl);
        sourcePanel.add("2.1.left.top", sourceListScroll);
        
        targetPanel = new JPanel(new PropertyLayout());
        NOIJLabel tarLbl = new NOIJLabel(reg.getString("SELECTED.LABEL"));
        targetPanel.add("1.1.center.center", tarLbl);
        targetPanel.add("2.1.left.top", targetListScroll);
    }
    
    private void openECRForm(String selectedValue) // TCDECREL-3261
    {
        Object[] objects = new Object[1];
        TCComponent[] theComponent = null;
        
        try
        {
            TCUserService userService = tcSession.getUserService();
            String[] inputValues = { selectedValue, "RS ECR Form" };
            objects[0] = inputValues;
            theComponent = (TCComponent[]) userService.call("NATOIL_find_form", objects);
            
            if(theComponent.length > 0)
            {
                objects = new Object[2];
                objects[0] = AIFUtility.getActiveDesktop();
                objects[1] = (InterfaceAIFComponent) theComponent[0];
                AbstractAIFCommand openCmd = tcSession.getOpenCommand(objects);
                openCmd.executeModal();
               }
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
        catch (Exception e2)
        {
            e2.printStackTrace();
        }
    } // TCDECREL-3261
    
    public void addUICtrl(boolean isECRSearchPanel)
    {
        searchPanel = new JPanel(new PropertyLayout());
        
        if (!isECRSearchPanel)
        {
            searchPanel.add("1.1.left.top", searchField);
            searchPanel.add("1.2.left.top", searchButton);
        }
        else
        {
            NOIJLabel ecrLabel = new NOIJLabel(reg.getString("ECRsNumber.LABLE"));
            searchPanel.add("1.1.left.center", ecrLabel);
            searchPanel.add("1.2.left.top", searchField);
            searchPanel.add("1.3.left.top", searchButton);
        }
        
        this.setLayout(new PropertyLayout());
        this.add("1.1.left.top", searchPanel);
        this.add("2.1.left.top", sourcePanel);
        this.add("2.2.left.top", addremButtonPanel);
        this.add("2.3.left.top", targetPanel);
        
    }
    
    public void setControlState(boolean isReleased)
    {
        searchButton.setEnabled(!isReleased);
//        sourceList.setEnabled(!isReleased);
//        targetList.setEnabled(!isReleased);  //  TCDECREL-3261
        addTarget.setEnabled(!isReleased);
        removeTarget.setEnabled(!isReleased);
        searchField.setEnabled(!isReleased);
    }
    
    public void setSearchBtnText(String searchBtnText)
    {
        searchButton.setText(searchBtnText);
    }
    
    public void setSourceLabel(String sourceLabel)
    {
        srcLbl.setText(sourceLabel);
    }
    
    public void setSearchFieldText(String searchFieldText)
    {
        searchField.setText(searchFieldText);
    }
    
    public void setSearchFieldToolTip(String searchFieldToolTipText)
    {
        searchField.setToolTipText(searchFieldToolTipText);
    }
    
    public void setTitleBorderText(String title)
    {
        TitledBorder tb = new TitledBorder(title);
        tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        setBorder(tb);
    }
    
    public JTextField getSearchField()
    {
        return searchField;
    }
    
    public void setSearchButtonState(boolean state)
    {
        searchButton.setEnabled(state);
    }
    
    public void actionPerformed(ActionEvent actEvt)
    {
        if (actEvt.getSource() == addTarget)
        {
            addSelValToTargetList();
        }
        if (actEvt.getSource() == removeTarget)
        {
            Object[] selObjs = targetList.getSelectedValues();
            for (int i = 0; i < selObjs.length; i++)
            {
                targetListModel.removeElement(selObjs[i]);
                if ((!sourceListModel.contains(selObjs[i].toString())))
                {
                    sourceListModel.addElement(selObjs[i].toString());
                }
            }
        }
        if (actEvt.getSource() == searchButton
                && searchButton.getText().equalsIgnoreCase(reg.getString("SEARCHBUTTON.LABEL")))
        {
            executeQuery();
        }
        if (actEvt.getSource() == searchButton
                && searchButton.getText().equalsIgnoreCase(reg.getString("ECRSEARCHBTN.LABEL")))
        {
            if (searchField.getText().equalsIgnoreCase("*"))
            {
                return;
            }
            
            String strEcrNumber = searchField.getText();
            
            String[] inputValues = { strEcrNumber, "RS ECR Form" };
            Object inputObjs[] = new Object[1];
            inputObjs[0] = inputValues;
            TCUserService usrService = tcSession.getUserService();
            
            try
            {
                TCComponent[] foundECRForms = (TCComponent[]) usrService.call("NATOIL_find_form", inputObjs);
                
                if (foundECRForms.length == 0)
                {
                    strEcrNumber = reg.getString("ECRsNumber.LABLE") + searchField.getText();
                    inputValues[0] = strEcrNumber;
                    foundECRForms = (TCComponent[]) usrService.call("NATOIL_find_form", inputObjs);
                    
                    if (foundECRForms.length == 0)
                    {
                        comp = (JPanel) this.getParent().getParent(); // Vivek
                        // strEcrNumber = "For both " +searchField.getText()+
                        // " and " +strEcrNumber + " " + reg.getString(
                        // "NOOBJFOUND.MSG" );
                        MessageBox.post(getWindow(comp), reg.getString("NOOBJFOUND.INFO"),
                                reg.getString("NOOBJFOUND.MSG"), MessageBox.INFORMATION); // Vivek
                        searchButton.setEnabled(false);
                        searchField.setText(reg.getString("ECRsNumberToolTip.TXT"));
                        return;
                    }
                }
                if (foundECRForms.length > 1)
                {
                    comp = this.getParent().getParent();
                    strEcrNumber += " " + reg.getString("DUPLICATEECR.MSG"); // Vivek
                    MessageBox.post(getWindow(comp), reg.getString("DUPLICATEECR.INFO"), strEcrNumber,
                            MessageBox.INFORMATION); // Vivek
                    searchButton.setEnabled(false);
                    searchField.setText(reg.getString("ECRsNumberToolTip.TXT"));
                    return;
                }
                
                // Assuming only one ECR will be found
                String releaseStatus = foundECRForms[0].getProperty("release_status_list");
                if (!releaseStatus.equalsIgnoreCase(reg.getString("WITHDRAWN.MSG"))
                        && !releaseStatus.equalsIgnoreCase(reg.getString("REJECTED.MSG")))
                {
                    String ecrNumber = foundECRForms[0].getProperty("object_name");
                    addToTargetList(ecrNumber);
                }
            }
            catch (TCException tce)
            {
                tce.printStackTrace();
            }
        }
    }
    
    private void addToTargetList(String strValue)
    {
        if (targetListModel.contains(strValue))
        {
            comp = (JPanel) this.getParent().getParent(); // Vivek
            String errorStr = strValue + reg.getString("AlreadyObjects.MSG");
            MessageBox.post(getWindow(comp), reg.getString("Info.MSG"), errorStr, MessageBox.INFORMATION); // Vivek
            return;
        }
        
        targetListModel.addElement(strValue);
    }
    
    private void addSelValToTargetList()
    {
        Object[] objs = sourceList.getSelectedValues();
        List<Object> existComps = new ArrayList<Object>();
        for (int i = 0; i < objs.length; i++)
        {
            int objInd = targetListModel.indexOf(objs[i].toString());
            if (objInd >= 0)
            {
                existComps.add(objs[i].toString());
            }
            else
            {
                if (!targetListModel.contains(objs[i].toString()))
                {
                    sourceListModel.removeElement(objs[i].toString());
                }
                targetListModel.addElement(objs[i].toString());
            }
        }
        if (existComps.size() > 0)
        {
            showErrorIfListisNotEmpty(existComps);
        }
    }
    
    private void showErrorIfListisNotEmpty(List<Object> existComps)
    {
        comp = (JPanel) this.getParent().getParent(); // Vivek
        String errorStr = reg.getString("AlreadyObjects.MSG");
        
        for (int i = 0; i < existComps.size(); i++)
        {
            errorStr = errorStr + existComps.get(i).toString() + ",";
        }
        if (errorStr.charAt(errorStr.length() - 1) == ',')
        {
            errorStr = errorStr.substring(0, errorStr.length() - 1);
        }
        
        // TODO: need to change the title of messagebox
        MessageBox.post(getWindow(comp), (reg.getString("Info.MSG")), errorStr, MessageBox.INFORMATION); // Vivek
        
    }
    
    private void executeQuery()
    {
        if (searchTypeStr != null && tcSession != null)
        {
            
            String[] names = { (reg.getString("Type.TYPE")), (reg.getString("Name.TYPE")) };
            String searchStr = searchField.getText();
            
            if (searchTypeStr.equals(reg.getString("TC_Project.TYPE")))
            {
                // Aparna commented to enhance the search speed
                /*
                 * if (searchStr.endsWith("*")) /*if (searchStr.endsWith("*")) {
                 * searchStr = searchStr.substring(0,searchStr.length()-1); }
                 */
                try
                {
                    // TCComponent[] projComps = getSearchResult(searchStr);
                    TCUserService us = tcSession.getUserService();
                    Object[] obj = new Object[1];
                    obj[0] = searchStr;
                    TCComponent[] projComps = (TCComponent[]) us.call("NATOIL_projectsQuery", obj);
                    // TCComponent[] projComps = getSearchResult(searchStr);
                    
                    if ((projComps != null) && (projComps.length > 0))
                    {
                        populateSourceList(projComps);
                    }
                    else
                    {
                        // Vivek Start
                        comp = (JPanel) this.getParent().getParent();
                        MessageBox.post(getWindow(comp), reg.getString("Info.MSG"), reg.getString("NoObjectFound.MSG"),
                                MessageBox.INFORMATION);
                        // Vivek End
                    }
                }
                catch (TCException te)
                {
                    System.out.println(te);
                }
            }
            else if (searchStr.trim().length() > 0)
            {
                String[] values = { searchTypeStr, searchStr.trim() };
                TCComponent[] searchResults = General.execute(tcSession, names, values);
                
                if (searchResults.length > 0)
                {
                    populateSourceList(searchResults);
                }
                else
                {
                    // Vivek Start
                    comp = (JPanel) this.getParent().getParent();
                    MessageBox.post(getWindow(comp), reg.getString("Info.MSG"), reg.getString("NoObjectFound.MSG"),
                            MessageBox.INFORMATION);
                    // Vivek End
                }
            }
        }
    }
    
    public void populateSourceList(Object[] sourceObjs)
    {
        if (sourceObjs != null)
        {
            if (sourceListModel.getSize() > 0)
            {
                sourceListModel.clear();
            }
            for (int i = 0; i < sourceObjs.length; i++)
            {
                if (!sourceListModel.contains(sourceObjs[i].toString()))
                {
                    sourceListModel.addElement(sourceObjs[i].toString());
                }
            }
        }
    }
    
    // private TCComponent[] getSearchResult(String searchString)
    // {
    // TCComponent[] comps=null;
    // try
    // {
    // /*TCComponentType compType = tcSession.getTypeComponent("TC_Project");
    // TCComponent[] projs = compType.extent();
    // ArrayList myList = new ArrayList();
    // for(int i = 0; i< projs.length; i++)
    // {
    // String curProject;
    // try
    // {
    // curProject = ((TCComponent)projs[i]).getStringProperty("project_id");
    // if(curProject.toUpperCase().indexOf(searchString.toUpperCase())>=0)
    // {
    // myList.add(projs[i]);
    // }
    // }
    // catch (TCException e)
    // {
    // e.printStackTrace();
    // }
    // }
    // int finalSize = myList.size();
    // if(finalSize > 0)
    // {
    // TCComponent[] result = new TCComponent[myList.size()];
    // for(int j = 0; j < myList.size(); j++)
    // {
    // result[j] = (TCComponent) myList.get(j);
    // }
    // return result;
    // }*/
    // TCUserService userService = tcSession.getUserService();
    // Object[] objs = new Object[1];
    // objs[0] = searchString;
    // comps = (TCComponent[])userService.call("NATOIL_projectsQuery", objs);
    //
    // /*TCComponentQueryType qryType =
    // (TCComponentQueryType)tcSession.getTypeComponent("ImanQuery");
    // TCComponentQuery qry =
    // (TCComponentQuery)qryType.find("NOV_ProjectsQuery");
    // String[] name = new String[2];
    // name[0]= "Projects";
    // name[1]= "Projects Name";
    // String [] value = new String[2];
    // value[0]= "NOV_ProjectsQuery";
    // value[1]= searchString;
    // comps = qry.execute(name, value);*/
    // }
    // catch (TCException e1)
    // {
    // e1.printStackTrace();
    // }
    // return comps;
    // }
    
    public String[] getTargetListObjects()
    {
        String[] retObjs = new String[targetListModel.getSize()];
        for (int i = 0; i < targetListModel.getSize(); i++)
        {
            retObjs[i] = targetListModel.getElementAt(i).toString();
        }
        return retObjs;
    }
    
    public void populateTargetList(String[] projects)
    {
        if (projects != null)
        {
            for (int i = 0; i < projects.length; i++)
            {
                if (!targetListModel.contains(projects[i].toString()))
                    targetListModel.addElement(projects[i]);
            }
        }
        
    }
    
    // Vivek Start // Getting the parent component i.e Window
    
    public Window getWindow(Component con)
    {
        Window window = null;
        
        if (con.getParent() instanceof Window)
        {
            window = (Window) con.getParent();
        }
        else
        {
            window = getWindow(con.getParent());
        }
        
        return window;
    } // Vivek End
    
}