/*
 * Created on Sep 20, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound.util;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

//import com.teamcenter.rac.find.ItemAttributesQuery;
import com.teamcenter.rac.find.QueryFormPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ItemRevisionQueryDialog extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Registry reg;
	JButton findButton = new JButton(reg.getString("Find.BUTN"));
	JButton cancelButton = new JButton(reg.getString("Cance.BUTN"));
    TCComponentQueryType qt;
    TCComponentQuery gen;	
    //ItemAttributesQuery attQ;
    QueryFormPanel qfp;
    JScrollPane scroller;
    JPanel paramPanel;
    JPanel head;
    JPanel buttons;
    JList  resultsList;
    TCComponent[] queryResults;
    Vector listItems = new Vector();
    public boolean CANCELLED=true;
    String queryName=reg.getString("ECNQuery.QRY");
    String title=reg.getString("ECNItemQry.QRY");
	
	/**
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog() throws HeadlessException {
		super();
	 reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Dialog arg0) throws HeadlessException {
		super(arg0);
		reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Dialog arg0, boolean arg1)
			throws HeadlessException {
		super(arg0, arg1);
		reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Frame arg0) throws HeadlessException {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Frame arg0, boolean arg1)
			throws HeadlessException {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Dialog arg0, String arg1)
			throws HeadlessException {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Dialog arg0, String arg1, boolean arg2)
			throws HeadlessException {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Frame arg0, String arg1)
			throws HeadlessException {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Frame arg0, String arg1, boolean arg2)
			throws HeadlessException {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryDialog(Dialog arg0, String arg1, boolean arg2,
			GraphicsConfiguration arg3) throws HeadlessException {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public ItemRevisionQueryDialog(Frame arg0, String arg1, boolean arg2,
			GraphicsConfiguration arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public void setQueryName(String s,String t) { queryName = s; title = t; }
	
	public void display(TCSession session) {
        try {
            qt = (TCComponentQueryType)session.getTypeComponent(reg.getString("ImanQuery.QRY"));
            gen = (TCComponentQuery)qt.find(queryName);
            //attQ = new ItemAttributesQuery(gen);
            /*qfp = new QueryFormPanel(attQ);
            //scroller = new JScrollPane();
            //scroller.setPreferredSize(new Dimension(380,102));
            qfp.setPreferredSize(new Dimension(363,200));*/
            paramPanel = new JPanel(new BorderLayout());
            head = new JPanel();
            buttons = new JPanel();
            findButton.addActionListener(this);
            cancelButton.addActionListener(this);
            buttons.add(findButton);
            buttons.add(cancelButton);
            paramPanel.add(head,BorderLayout.NORTH);
            paramPanel.add(qfp,BorderLayout.CENTER);
            paramPanel.add(buttons,BorderLayout.SOUTH);
            paramPanel.setPreferredSize(new Dimension(363,220));
            //scroller.setViewportView(qfp);
            getContentPane().add(paramPanel);
            setSize(new Dimension(400,240));
            setTitle(title);
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation(d.width/2,d.height/2);
            
            show();
            
            
        }
        catch (Exception e)
        {
            System.out.println("Could not execute general query: "+e);
            e.printStackTrace();
        }
		
	}
	
	
	public void actionPerformed(ActionEvent e) {		
		if (e.getSource() == findButton) {
			CANCELLED = false;
			Vector vnames = new Vector();
			Vector vvalues = new Vector();
			qfp.getFilledEntries(vnames,vvalues);
			if (vnames.size() > 0) {
				String[] snames = new String[vnames.size()];
				String[] svalues = new String[vvalues.size()];
				for (int i=0;i<vnames.size();i++) {
					snames[i] = vnames.get(i).toString();
					svalues[i] = vvalues.get(i).toString();
					System.out.println(snames[i]+","+svalues[i]);
				}
				try {
					queryResults = gen.execute(snames,svalues); 
				}
				catch (Exception ex) {
					System.out.println("Could not execute query: "+ex);
				}
			}
			this.dispose();
		}
		else if (e.getSource() == cancelButton) {
			this.dispose();
		}
	}
	
	public TCComponent[] getQueryResults() { return queryResults; }
}
