package com.noi.rac.en.form.compound.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;

import javax.swing.JPanel;
import javax.swing.JTextField;

import com.noi.rac.en.form.compound.util.ItemRevisionHistory;
import com.noi.rac.en.util.components.NOIJLabel;
import com.teamcenter.rac.util.Registry;

public class ItemRevisionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ItemRevisionPanel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemRevisionPanel(boolean arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ItemRevisionPanel(LayoutManager arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ItemRevisionPanel(LayoutManager arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	public void createUI(ItemRevisionHistory irh) {
		
		Registry registry = Registry.getRegistry("com.noi.rac.en.form.form_locale");
		
		this.setBackground(new Color(225,225,225));
		//setBorder(new LineBorder(new java.awt.Color(0,0,0)));
		GridBagLayout gbl = new GridBagLayout();
		gbl.rowHeights = new int[] { 22 };
		gbl.columnWidths = new int[] { 20,25,50,75,50,75,50,150 };
		
		JTextField itemRevId = new JTextField();
		JTextField createDate = new JTextField();
		JTextField releaseDate = new JTextField();
		JTextField name = new JTextField();
		JTextField desc = new JTextField();
		
		itemRevId.setText(irh.revisionId);
		createDate.setText(irh.creationDate != null && irh.creationDate .length()>12 ? irh.creationDate.substring(0,11):irh.creationDate);
		releaseDate.setText(irh.dateReleased != null && irh.dateReleased.length()>12 ? irh.dateReleased.substring(0,11):irh.dateReleased);
		name.setText(irh.name);
		desc.setText("  " +irh.description);
		
		itemRevId.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
		itemRevId.setPreferredSize(new Dimension(25,21));
		itemRevId.setHorizontalAlignment(JTextField.CENTER);
		itemRevId.setEditable(false);
		createDate.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
		createDate.setPreferredSize(new Dimension(75,21));
		createDate.setHorizontalAlignment(JTextField.CENTER);
		createDate.setEditable(false);
		releaseDate.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
		releaseDate.setPreferredSize(new Dimension(75,21));
		releaseDate.setHorizontalAlignment(JTextField.CENTER);
		releaseDate.setEditable(false);
		name.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
		name.setPreferredSize(new Dimension(150,21));
		name.setHorizontalAlignment(JTextField.CENTER);
		name.setEditable(false);
		desc.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
		desc.setPreferredSize(new Dimension(425,21));
		desc.setEditable(false);

		//setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
		//setPreferredSize(new Dimension(520,44));
		setLayout(gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0,2,0,0);
        add(new NOIJLabel(registry.getString("ItemRevisionPanel.Rev")),gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(0,5,0,0);
		gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		add(itemRevId,gbc);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridx = 2;
		gbc.gridy = 0;
        add(new NOIJLabel(registry.getString("ItemRevisionPanel.Created")),gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.insets = new Insets(0,5,0,0);
		gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		add(createDate,gbc);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridx = 4;
		gbc.gridy = 0;
        add(new NOIJLabel(registry.getString("ItemRevisionPanel.Released")),gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 0;
        gbc.insets = new Insets(0,5,0,0);
		gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		add(releaseDate,gbc);
		
		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridx = 6;
		gbc.gridy = 0;
        add(new NOIJLabel(registry.getString("ItemRevisionPanel.Name")),gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 7;
        gbc.gridy = 0;
        //gbc.insets = new Insets(0,2,0,0);
		gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		add(name,gbc);

		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(0,2,0,0);
        add(new NOIJLabel(registry.getString("ItemRevisionPanel.Description")),gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.gridwidth = 6;
        gbc.insets = new Insets(0,5,0,0);
		gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
		add(desc,gbc);
		
	}
}
