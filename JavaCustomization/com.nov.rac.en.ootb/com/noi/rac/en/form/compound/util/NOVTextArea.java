/** \file 
    \brief This file contains a class NOVTextArea which creates a text area.
*/

package com.noi.rac.en.form.compound.util;
import com.teamcenter.rac.util.iTextArea;
import java.awt.Container;

/** \brief Declaration for the class NOVTextArea, it creates a text area. Basically this is used for avoiding 
  		   the up / down key problem with the iTextArea in certain conditions. 
    	
*/
public class NOVTextArea extends iTextArea
{
	/// Serial Version UID for the class.
    private static final long serialVersionUID = 1L;

	/** \brief Creates the Instance of the NOVTextArea.
	*/
    public NOVTextArea()
    {
    	super();
    }

	/** \brief Creates the Instance of the NOVTextArea.
	    \param strInitValue A default String value which will be displayed in the NOVTextArea.
 	*/
    public NOVTextArea( String strInitValue )
    {
    	super( strInitValue );
    }
 
	/** \brief Creates the Instance of the NOVTextArea.
	    \param rows Number of rows in the NOVTextArea.
	  	\param columns Number of columns in the NOVTextArea.
 	*/
    public NOVTextArea( int rows, int columns )
    {
    	super( rows,  columns );
    }
     
	/** \brief Creates the Instance of the NOVTextArea.
	    \param strInitValue A default String value which will be displayed in the NOVTextArea.
	    \param rows Number of rows in the NOVTextArea.
	  	\param columns Number of columns in the NOVTextArea.
 	*/    
    public NOVTextArea( String strInitValue, int rows, int columns )
    {
        super( strInitValue, rows, columns );
    }
   
	/** \brief Overridden method which will get the Focus Transfer Container for current control.
	 	\return Returns the Focus Transfer Container for current control.
	*/
    public Container getFocusTransferContainer()
    {
    	return this;
    }
}
