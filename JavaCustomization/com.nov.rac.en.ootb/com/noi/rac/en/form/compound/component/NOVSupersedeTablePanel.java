package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import com.teamcenter.rac.common.TCTable;

public class NOVSupersedeTablePanel extends JPanel 
{

	public NOVSupersedeTablePanel()
	{
		String[] columnNames = {"Line No","Item ID","Current Status","Future Status"};
		TCTable sSedeTable = new TCTable(columnNames);
		sSedeTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scPane = new JScrollPane(sSedeTable);
		scPane.setPreferredSize(new Dimension(600,100));
		this.add(scPane);
	}
}
