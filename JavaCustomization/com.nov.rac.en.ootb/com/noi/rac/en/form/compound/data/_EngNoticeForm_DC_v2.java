package com.noi.rac.en.form.compound.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JCheckBox;

import com.noi.rac.en.form.compound.component.NOV4ECRDistributionPanel;
import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel_v2;
import com.noi.rac.en.form.compound.component.NOVEnGenSearchPanel_v2;
import com.noi.rac.en.form.compound.util.NOVCustomFormProperties;
import com.noi.rac.en.form.compound.util.NOVTextArea;
import com.noi.rac.en.mission.helper.NOVMissionHelper_v2;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVEnDispositionPanel_v2;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.noi.util.components.AbstractReferenceLOVList;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.combobox.iComboBox;

public class _EngNoticeForm_DC_v2 extends NOVCustomFormProperties
{
    public JCheckBox mfgReq;
    public JCheckBox docControlRequired;
    public NOVLOVPopupButton drafted;
    public NOVLOVPopupButton checked;
    public NOVLOVPopupButton approved;
    public NOVLOVPopupButton created;
    public iComboBox reasonCombo;
    public JCheckBox m_isENCritical;//TCDECREL-4530
    public NOVDateButton draftedon;
    public NOVDateButton checkedon;
    public NOVDateButton approvedon;
    public NOVDateButton creationon;
    public NOVTextArea furExplanation;
    public NOVTextArea reasonForChangeDesc;
    public DateButton validationDate;
    
    public NOVLOVPopupButton ercapprovedby;
    public NOVDateButton ercapproveddate;
    //TCDECREL-3651
    public NOVLOVPopupButton me_approver;
    public NOVDateButton me_approved_date;
    //TCDECREL-3742
    public NOVLOVPopupButton ce_approver;
    public NOVDateButton ce_approved_date;
    
    NOVEnGenSearchPanel_v2 relatedECRSearchPanel;
    NOVEnGenSearchPanel_v2 projectsPanel;
    NOVEnDistributionPanel_v2 distributionPanel;
    NOV4ECRDistributionPanel ecrDistributionPanel;
    public NOVEnDispositionPanel_v2 dispDataComp;
    AbstractReferenceLOVList distSource;
    AbstractReferenceLOVList distTarget;
    AbstractReferenceLOVList ecrDistTarget;
    private Registry registry = Registry.getRegistry("com.noi.rac.en.form.form");
    
    String[] propertyNames = new String[] { "newrelease", "mfgreviewrequired", "nov4_doccontrol_required", "islocked", "lastvalidatedon",
            "draftedby", "draftedon", "checkedby", "checkedon", "approvedby", "approvedon", "reason", "nov4_is_en_critical",
            "nov4reason_desc", "relatedecr", "explanation", "projects", "distribution", "nov4manual_distributions",
            "entargetsdisposition", "nov4ercapprovedby", "nov4ercapproveddate" ,
            "nov4_me_approver","nov4_me_approved_date","nov4_ce_approver","nov4_ce_approved_date"};
    
    public _EngNoticeForm_DC_v2()
    {
        
        mfgReq = new JCheckBox();
        docControlRequired = new JCheckBox();
        drafted = new NOVLOVPopupButton();
        checked = new NOVLOVPopupButton();
        approved = new NOVLOVPopupButton();
        created = new NOVLOVPopupButton();
        reasonCombo = new iComboBox();
        reasonCombo.setMandatory(true);
        reasonCombo.setEditable(false);
        m_isENCritical = new JCheckBox();//TCDECREL-4530
        draftedon = new NOVDateButton();
        checkedon = new NOVDateButton();
        approvedon = new NOVDateButton();
        creationon = new NOVDateButton();
        furExplanation = new NOVTextArea();
        reasonForChangeDesc = new NOVTextArea();
        validationDate = new DateButton();
        ecrDistTarget = new AbstractReferenceLOVList();
        ercapprovedby = new NOVLOVPopupButton();
        ercapproveddate = new NOVDateButton();
        //TCDECREL-3651
        me_approver = new NOVLOVPopupButton();
        me_approved_date = new NOVDateButton();
        //TCDECREL-3742
        ce_approver = new NOVLOVPopupButton();
        ce_approved_date = new NOVDateButton(); 
        
        // addProperty("newrelease",newRelease );
        addProperty("mfgreviewrequired", mfgReq);
        addProperty("nov4_doccontrol_required", docControlRequired);        
        
        // addProperty("islocked", );
        addProperty("draftedby", drafted);
        addProperty("draftedon", draftedon);
        addProperty("checkedby", checked);
        addProperty("checkedon", checkedon);
        addProperty("approvedby", approved);
        addProperty("approvedon", approvedon);
        addProperty("createdby", created);
        addProperty("createdon", creationon);
        addProperty("reason", reasonCombo);
        addProperty("nov4_is_en_critical", m_isENCritical);//TCDECREL-4530
        addProperty("explanation", furExplanation);
        addProperty("nov4reason_desc", reasonForChangeDesc);
        addProperty("lastvalidatedon", validationDate);
        addProperty("nov4ercapprovedby", ercapprovedby);
        addProperty("nov4ercapproveddate", ercapproveddate);
        //TCDECREL-3651 & TCDECREL-3742
        addProperty("nov4_me_approver", me_approver);
        addProperty("nov4_me_approved_date", me_approved_date);
        addProperty("nov4_ce_approver", ce_approver);
        addProperty("nov4_ce_approved_date", ce_approved_date);
    }
    
    public String[] getPropertyNames()
    {
        return propertyNames;
    }
    
    public void setForm(TCComponent form)
    {
        if (form != null)
        {
            try
            {
                super.setForm(form);
                // get the disposition data
                TCProperty targetdispProperty = form.getTCProperty("entargetsdisposition");
                // targetdispProperty.getReferenceValueArray();
                dispDataComp = new NOVEnDispositionPanel_v2(targetdispProperty.getReferenceValueArray());
                relatedECRSearchPanel = new NOVEnGenSearchPanel_v2(form.getSession(), "");
                ;
                projectsPanel = new NOVEnGenSearchPanel_v2(form.getSession(), "TC_Project");
                ;
                
                distributionPanel = new NOVEnDistributionPanel_v2();
                ecrDistributionPanel = new NOV4ECRDistributionPanel(form);
                
                // Code added for Distribution List -------
                String currentGroup = "";
                distSource = new AbstractReferenceLOVList();
                TCComponentGroup group = form.getSession().getCurrentGroup();
                String groupname = group.getFullName();
                if (groupname.indexOf('.') >= 0)
                    currentGroup = groupname.substring(0, groupname.indexOf('.'));
                else
                    currentGroup = groupname;
                currentGroup = currentGroup + ":";
                distSource.initClassECNERO(form.getSession(), "ImanAliasList", "alName", currentGroup);
                // @harshada - Modified for test
                // distSource.initClass(getForm().getSession(),"ImanAliasList","alName");
                distTarget = new AbstractReferenceLOVList();
                // ecrDistTarget.initClassECNERO( form.getSession(),
                // "ImanAliasList", "alName", currentGroup );
                // Code added for Distribution List -------
                
                TCComponentListOfValues lovalues = TCComponentListOfValuesType.findLOVByName(form.getSession(),
                        "_RSOne_enreasonforchange_");
                // ListOfValuesInfo lovInfo = lovalues.getListOfValues();
                // Object[] lovS = lovInfo.getListOfValues();
                // Vector<Object> reasonValVac = new Vector<Object>();
                Object[] lovS = lovalues.getListOfValues().getLOVDisplayValues();
                if (lovS.length > 0)
                {
                    reasonCombo.addItem("");
                    for (int i = 0; i < lovS.length; i++)
                    {
                        reasonCombo.addItem(lovS[i]);
                    }
                }
                
                addProperty("nov4_is_en_critical", m_isENCritical);//TCDECREL-4530
                addProperty("entargetsdisposition", dispDataComp);
                addProperty("relatedecr", relatedECRSearchPanel);
                addProperty("projects", projectsPanel);
                addProperty("distribution", distributionPanel);
                addProperty("nov4manual_distributions", ecrDistributionPanel);
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            super.getFormProperties(propertyNames);
        }
    }
    
    public void enableForm()
    {
        enableProperty("mfgreviewrequired");
        enableProperty("nov4_doccontrol_required");
        enableProperty("reason");
        enableProperty("explanation");
        enableProperty("nov4reason_desc");
        enableProperty("lastvalidatedon");
        enableProperty("entargetsdisposition");
        enableProperty("relatedecr");
        enableProperty("projects");
        enableProperty("distribution");
        enableProperty("nov4manual_distributions");
    }
    
    public void clearForm()
    {
    }
    
    public void populateFormComponents()
    {
        if (getForm() == null)
            return;
        try
        {
            /*
             * TCComponentForm preEnForm = getPreviousENForm(); if
             * (preEnForm!=null) { String[] projects =
             * preEnForm.getTCProperty("projects").getStringValueArray();
             * String[] distrs =
             * preEnForm.getTCProperty("distribution").getStringValueArray();
             * String[] relatedEcr =
             * preEnForm.getTCProperty("relatedecr").getStringValueArray();
             * projectsPanel.populateSourceList(projects);
             * distributionPanel.populateSourceList(distrs);
             * relatedECRSearchPanel.populateSourceList(relatedEcr); }
             */

            /*
             * @Mohanar-TCDECREL-405 ---> TO remove mission group preference
             * boolean misGroup =
             * NOVMissionHelper.isGroup(registry.getString("misPref.NAME"
             * ),getForm()); if (misGroup){ populateMissionFields(); }
             */

            // @Mohanar-TCDECREL-405
            boolean createGroup = NOVMissionHelper_v2.isGroup(registry.getString("createPref.NAME"), getForm(), false);
            if (!createGroup)
            {
                populateMissionFields();
            }
            
            mfgReq.setSelected(((TCProperty) imanProperties.get(mfgReq)).getLogicalValue());
            docControlRequired.setSelected(((TCProperty) imanProperties.get(docControlRequired)).getLogicalValue());
            
            drafted.setSelectedText(((TCProperty) imanProperties.get(drafted)).getStringValue());
            checked.setSelectedText(((TCProperty) imanProperties.get(checked)).getStringValue());
            approved.setSelectedText(((TCProperty) imanProperties.get(approved)).getStringValue());
            
            draftedon.setDate(((TCProperty) imanProperties.get(draftedon)).getDateValue());
            checkedon.setDate(((TCProperty) imanProperties.get(checkedon)).getDateValue());
            approvedon.setDate(((TCProperty) imanProperties.get(approvedon)).getDateValue());
            
            reasonCombo.setSelectedItem(((TCProperty) imanProperties.get(reasonCombo)).getStringValue());
            m_isENCritical.setSelected(((TCProperty) imanProperties.get(m_isENCritical)).getLogicalValue());//TCDECREL-4530
            
            furExplanation.setText(((TCProperty) imanProperties.get(furExplanation)).getStringValue());
            reasonForChangeDesc.setText(((TCProperty) imanProperties.get(reasonForChangeDesc)).getStringValue());
            
            // Mohanar
            ercapprovedby.setSelectedText(((TCProperty) imanProperties.get(ercapprovedby)).getStringValue());
            ercapproveddate.setDate(((TCProperty) imanProperties.get(ercapproveddate)).getDateValue());
            //TCDECREL-3651
            me_approver.setSelectedText(((TCProperty) imanProperties.get(me_approver)).getStringValue());
            me_approved_date.setDate(((TCProperty) imanProperties.get(me_approved_date)).getDateValue());
            //TCDECREL-3742
            ce_approver.setSelectedText(((TCProperty) imanProperties.get(ce_approver)).getStringValue());
            ce_approved_date.setDate(((TCProperty) imanProperties.get(ce_approved_date)).getDateValue());
            
            String[] relatedECRs = ((TCProperty) imanProperties.get(relatedECRSearchPanel)).getStringValueArray();
            if (relatedECRs != null)
            {
                relatedECRSearchPanel.populateTargetList(relatedECRs);
            }
            
            String[] projects = ((TCProperty) imanProperties.get(projectsPanel)).getStringValueArray();
            if (projects != null)
            {
                projectsPanel.populateTargetList(projects);
            }
            /*
             * String[] diststributionList =
             * ((TCProperty)imanProperties.get(distributionPanel
             * )).getStringValueArray(); if (diststributionList!=null) {
             * distributionPanel.populateTargetList(diststributionList); }
             */

            String[] arr1 = ((TCProperty) imanProperties.get(distributionPanel)).getStringValueArray();
            for (int i = 0; i < arr1.length; i++)
            {
                distTarget.addItem(arr1[i], arr1[i]);
                distSource.addExclusion(arr1[i]);
            }
            
            distributionPanel.init(distSource, distTarget, new java.awt.Dimension(250, 100));
            
            String[] strEcrDistArr = getEcrDistributions();
            setEcrDistributions(strEcrDistArr);
            
            // disposition
            // dispDataComp.
            // validation Date
            Date validation = ((TCProperty) imanProperties.get(validationDate)).getDateValue();
            validationDate.setDate(validation);
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public String[] getEcrDistributions()
    {
        String[] strEcrDistArr = ((TCProperty) imanProperties.get(ecrDistributionPanel)).getStringValueArray();
        return strEcrDistArr;
    }
    
    public void setEcrDistributions(String[] strEcrDistArr)
    {
        ((TCProperty) imanProperties.get(ecrDistributionPanel)).setStringValueArrayData(strEcrDistArr);
        if (ecrDistTarget == null)
            ecrDistTarget = new AbstractReferenceLOVList();
        
        for (int index = 0; index < strEcrDistArr.length; index++)
        {
            ecrDistTarget.addItem(strEcrDistArr[index], strEcrDistArr[index]);
        }
        ecrDistributionPanel.init(new java.awt.Dimension(250, 100));
    }
    
    public NOV4ECRDistributionPanel getEcrDistributionPanel()
    {
        return ecrDistributionPanel;
    }
    
    private void populateMissionFields()
    {
        AIFComponentContext[] partContext;
        try
        {
            partContext = getForm().whereReferenced();
            TCComponentTask epmTask = null;
            for (int inx = 0; inx < partContext.length; inx++)
            {
                if (partContext[inx] == null || !(partContext[inx].getComponent() instanceof TCComponentTask))
                {
                    created.setSelectedText("");
                    creationon.setDate("");
                    continue;
                }
                epmTask = (TCComponentTask) partContext[inx].getComponent();
                if (epmTask == null)
                    continue;
                String processInitiater = epmTask.getProperty("owning_user");
                DateFormat df = new SimpleDateFormat(registry.getString("dateFrmt.NAME"));
                if (processInitiater != null)
                    created.setSelectedText(processInitiater);
                String date = epmTask.getProperty(registry.getString("crDate.NAME"));
                Date creationDate = new Date();
                if (date != null)
                {
                    // creationDate =
                    // df.parse(epmTask.getProperty(registry.getString("crDate.NAME")));
                    creationDate = epmTask.getDateProperty(registry.getString("crDate.NAME"));
                    creationon.setDate(creationDate);
                    break;
                }
            }
        }
        catch (TCException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
    }
    
    public void saveOnlyDispFormData()
    {
        try
        {
            TCProperty[] arr = null;
            
            // disposition data
            TCProperty prop = (TCProperty) imanProperties.get(dispDataComp);
            TCComponent[] dispcomps = dispDataComp.getDispData();
            if (dispcomps != null && dispDataComp.isEnabled())
                prop.setReferenceValueArrayData(dispcomps);
            
            arr = enabledProperty(arr, relatedECRSearchPanel);
            arr = enabledProperty(arr, projectsPanel);
            arr = enabledProperty(arr, distributionPanel);
            arr = enabledProperty(arr, ecrDistributionPanel);
            // arr = enabledProperty(arr,newRelease);
            arr = enabledProperty(arr, validationDate);
            
            //Note : This is required as this enabledProperty(...) method does not add the property 
            // if the control associated with that property is in the disabled state. And in case of 
            // Manufacturer review required Document Control required we need to enable and disable the 
            // controls based on the selection.
            if( !mfgReq.isEnabled() )
            {
                TCProperty mfgProp = (TCProperty) imanProperties.get(mfgReq);
                mfgProp.setLogicalValue(mfgReq.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, mfgReq);
            }
            
            if( !docControlRequired.isEnabled() )
            {
                TCProperty docControlProp = (TCProperty) imanProperties.get(docControlRequired);
                docControlProp.setLogicalValue(docControlRequired.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, docControlRequired);
            }

            arr = enabledProperty(arr, furExplanation);
            arr = enabledProperty(arr, reasonForChangeDesc);
            arr = enabledProperty(arr, reasonCombo);
            arr = enabledProperty(arr, m_isENCritical);//TCDECREL-4530
            arr = enabledProperty(arr, drafted);
            arr = enabledProperty(arr, checked);
            arr = enabledProperty(arr, approved);
            arr = enabledProperty(arr, draftedon);
            arr = enabledProperty(arr, checkedon);
            arr = enabledProperty(arr, approvedon);
            arr = enabledProperty(arr, dispDataComp);
            
            // @MOhanar - ERC new attributes
            arr = enabledProperty(arr, ercapprovedby);
            arr = enabledProperty(arr, ercapproveddate);
            //TCDECREL-3651
            arr = enabledProperty(arr, me_approver);
            arr = enabledProperty(arr, me_approved_date);
            //TCDECREL-3742
            arr = enabledProperty(arr, ce_approver);
            arr = enabledProperty(arr, ce_approved_date);
            
            getForm().setTCProperties(arr);
            getForm().save();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public void saveFormData()
    {
        try
        {
            TCProperty[] arr = null;
            ((TCProperty) imanProperties.get(relatedECRSearchPanel)).setStringValueArrayData(relatedECRSearchPanel
                    .getTargetListObjects());
            ((TCProperty) imanProperties.get(projectsPanel)).setStringValueArrayData(projectsPanel
                    .getTargetListObjects());
            /*
             * ((TCProperty)imanProperties.get(distributionPanel)).
             * setStringValueArrayData
             * (distributionPanel.getTargetListObjects()); String[] disArr =
             * distTarget.getList(); if (disArr != null &&
             * distributionPanel.isEnabled())
             * ((TCProperty)imanProperties.get(distributionPanel
             * )).setStringValueArray(disArr);
             */
            ((TCProperty) imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel
                    .getAllTargetListObjects());
            
            ((TCProperty) imanProperties.get(ecrDistributionPanel)).setStringValueArrayData(ecrDistributionPanel
                    .getECRDistributionsObjects());
            
            ((TCProperty) imanProperties.get(mfgReq)).setLogicalValueData(mfgReq.isSelected());
            ((TCProperty) imanProperties.get(docControlRequired)).setLogicalValueData(docControlRequired.isSelected());
            
            ((TCProperty) imanProperties.get(furExplanation)).setStringValueData(furExplanation.getText());
            ((TCProperty) imanProperties.get(reasonForChangeDesc)).setStringValueData(reasonForChangeDesc.getText());
            
            Object selObj = reasonCombo.getSelectedItem();
            if (selObj != null)
            {
                ((TCProperty) imanProperties.get(reasonCombo)).setStringValueData(selObj.toString());
            }

            ((TCProperty) imanProperties.get(m_isENCritical)).setLogicalValueData(m_isENCritical.isSelected());//TCDECREL-4530
           
            Object selObj1 = drafted.getSelectedObject();
            if (selObj1 != null)
            {
                ((TCProperty) imanProperties.get(drafted)).setStringValueData(selObj1.toString());
            }
            Object selObj2 = checked.getSelectedObject();
            if (selObj2 != null)
            {
                ((TCProperty) imanProperties.get(checked)).setStringValueData(selObj2.toString());
            }
            Object selObj3 = approved.getSelectedObject();
            if (selObj3 != null)
            {
                ((TCProperty) imanProperties.get(approved)).setStringValueData(selObj3.toString());
            }
            
            String draftedDate = draftedon.getDateString();
            if (draftedDate != null)
            {
                ((TCProperty) imanProperties.get(draftedon)).setDateValueData(draftedon.getDate());
            }
            String checkedDate = checkedon.getDateString();
            if (checkedDate != null)
            {
                ((TCProperty) imanProperties.get(checkedon)).setDateValueData(checkedon.getDate());
            }
            String approvedDate = approvedon.getDateString();
            if (approvedDate != null)
            {
                ((TCProperty) imanProperties.get(approvedon)).setDateValueData(approvedon.getDate());
            }
            
            // @MOhanar - ERC new properties
            Object selObj4 = ercapprovedby.getSelectedObject();
            if (selObj4 != null)
            {
                ((TCProperty) imanProperties.get(ercapprovedby)).setStringValueData(selObj4.toString());
            }
            String ercapprovedDate = ercapproveddate.getDateString();
            if (ercapprovedDate != null)
            {
                ((TCProperty) imanProperties.get(ercapproveddate)).setDateValueData(ercapproveddate.getDate());
            }
            // disposition data
            TCProperty prop = (TCProperty) imanProperties.get(dispDataComp);
            dispDataComp.saveDispData();
            TCComponent[] dispcomps = dispDataComp.getDispData();
            if (dispcomps != null && dispDataComp.isEnabled())
                prop.setReferenceValueArrayData(dispcomps);
            
            // validation date
            ((TCProperty) imanProperties.get(validationDate)).setDateValueData(validationDate.getDate());
            
            arr = enabledProperty(arr, relatedECRSearchPanel);
            arr = enabledProperty(arr, projectsPanel);
            arr = enabledProperty(arr, distributionPanel);
            arr = enabledProperty(arr, ecrDistributionPanel);
            // arr = enabledProperty(arr,newRelease);
            arr = enabledProperty(arr, validationDate);
            
            //Note : This is required as this enabledProperty(...) method does not add the property 
            // if the control associated with that property is in the disabled state. And in case of 
            // Manufacturer review required Document Control required we need to enable and disable the 
            // controls based on the selection.
            if( !mfgReq.isEnabled() )
            {
                TCProperty mfgProp = (TCProperty) imanProperties.get(mfgReq);
                mfgProp.setLogicalValue(mfgReq.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, mfgReq);
            }
            
            if( !docControlRequired.isEnabled() )
            {
                TCProperty docControlProp = (TCProperty) imanProperties.get(docControlRequired);
                docControlProp.setLogicalValue(docControlRequired.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, docControlRequired);
            }
            
            arr = enabledProperty(arr, furExplanation);
            arr = enabledProperty(arr, reasonForChangeDesc);
            arr = enabledProperty(arr, reasonCombo);
            arr = enabledProperty(arr, m_isENCritical);//TCDECREL-4530
            arr = enabledProperty(arr, drafted);
            arr = enabledProperty(arr, checked);
            arr = enabledProperty(arr, approved);
            arr = enabledProperty(arr, draftedon);
            arr = enabledProperty(arr, checkedon);
            arr = enabledProperty(arr, approvedon);
            arr = enabledProperty(arr, dispDataComp);
            
            // @Mohanar - ERC new attributes
            arr = enabledProperty(arr, ercapprovedby);
            arr = enabledProperty(arr, ercapproveddate);
            //TCDECREL-3651
            arr = enabledProperty(arr, me_approver);
            arr = enabledProperty(arr, me_approved_date);
            //TCDECREL-3742
            arr = enabledProperty(arr, ce_approver);
            arr = enabledProperty(arr, ce_approved_date);
            
            getForm().setTCProperties(arr);
            getForm().save();
            
            // usha - for creating EN-ECR (_ecnecr_)relation
            Object inputObjs[] = new Object[1];
            int output = 0;
            TCComponent enForm = getForm();
            inputObjs[0] = new TCComponent[] { enForm };
            TCSession session = enForm.getSession();
            TCUserService usrService = session.getUserService();
            usrService.call("NOV4_createENECRRelation", inputObjs);
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public boolean isFormSavable()
    {
        return true;
        
    }
}
