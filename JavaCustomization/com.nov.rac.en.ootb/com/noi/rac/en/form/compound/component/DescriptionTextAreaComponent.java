package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;

/**
 * @author niuy
 * @Date:  May 11, 2007
 * @usage: The class is used to 
 */
public class DescriptionTextAreaComponent extends BaseComponent  implements  KeyListener {
	private int textAreaWidth;
	private int textAreaHeight;
    private String title;
    private int maximumTextSize;
    private String propertyName;

	/**
	 * 
	 */
	public DescriptionTextAreaComponent() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param novFormProperties
	 * @param parentPanel
	 */
	public DescriptionTextAreaComponent(INOVCustomFormProperties novFormProperties,
			JPanel parentPanel) {
		super(novFormProperties, parentPanel);
		
	}
	
	public DescriptionTextAreaComponent(INOVCustomFormProperties novFormProperties,JPanel parentPanel,int textAreaWidth, int textAreaHeight, String title, int maximumTextSize, String propertyName) {
		super(novFormProperties, parentPanel);
		this.textAreaWidth = textAreaWidth;
		this.textAreaHeight = textAreaHeight;
		this.title = title;
		this.maximumTextSize = maximumTextSize;
		this.propertyName = propertyName;
		createUI();
	}

	/* (non-Javadoc)
	 * @see com.noi.rac.en.form.compound.component.BaseComponent#initialize()
	 */
	public void createUI() {
		
		parentPanel.setBorder(new javax.swing.border.TitledBorder(title));
		((JTextArea)novFormProperties.getProperty(propertyName)).setRows(9);
		((JTextArea)novFormProperties.getProperty(propertyName)).setColumns(60);
		((JTextArea)novFormProperties.getProperty(propertyName)).setFont(new Font("Dialog",java.awt.Font.BOLD,10));
		((JTextArea)novFormProperties.getProperty(propertyName)).setLineWrap(true);
		((JTextArea)novFormProperties.getProperty(propertyName)).setWrapStyleWord(true);
		((JTextArea)novFormProperties.getProperty(propertyName)).addKeyListener(this);
		JScrollPane descScroller = new JScrollPane(((JTextArea)novFormProperties.getProperty(propertyName)),
										JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
										JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		descScroller.setPreferredSize(new Dimension(textAreaWidth,textAreaHeight));
		descScroller.setMinimumSize(new Dimension(textAreaWidth,textAreaHeight));
		
		parentPanel.add(descScroller);
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
		keyHandlerOfTextAreaLimitation(e, propertyName,maximumTextSize);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {	
		//keyHandlerOfTextAreaLimitation(e, propertyName,NOVConstants.MAXIMUM_ECN_REASON_OF_CHANGE);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
		//keyHandlerOfTextAreaLimitation(e, propertyName,NOVConstants.MAXIMUM_ECN_REASON_OF_CHANGE);
		
	}

	public int getMaximumTextSize() {
		return maximumTextSize;
	}

	public void setMaximumTextSize(int maximumTextSize) {
		this.maximumTextSize = maximumTextSize;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public int getTextAreaHeight() {
		return textAreaHeight;
	}

	public void setTextAreaHeight(int textAreaHeight) {
		this.textAreaHeight = textAreaHeight;
	}

	public int getTextAreaWidth() {
		return textAreaWidth;
	}

	public void setTextAreaWidth(int textAreaWidth) {
		this.textAreaWidth = textAreaWidth;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see com.noi.rac.en.form.compound.component.BaseComponent#initialize()
	 */
	public void initialize() {
		
	}

}
