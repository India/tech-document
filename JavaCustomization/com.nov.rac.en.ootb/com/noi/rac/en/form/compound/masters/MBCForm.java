package com.noi.rac.en.form.compound.masters;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.eclipse.swt.widgets.Composite;

import com.noi.rac.en.form.compound.panels.MBCFormComposite;
import com.teamcenter.rac.kernel.TCComponentForm;

public class MBCForm extends BaseMasterForm
{
    public MBCFormComposite m_mbcFormComposite;
    public Composite m_testComposite;
    private JPanel m_panel;
    
    public MBCForm(TCComponentForm master)
    {
        super();
        masterForm = master;
        init();
    }
    
    private void init()
    {
    }
    
    public JComponent getPanel()
    {
        return m_panel;
    }
    
    public void saveForm()
    {
        
    }
    
    public void lockForm(boolean lock)
    {
        
    }
    
    public void setDisplayProperties()
    {
        
    }
    
    public ImageIcon getTabIcon()
    {
        return null;
    }
    
    public String getTip()
    {
        return null;
    }
    
}
