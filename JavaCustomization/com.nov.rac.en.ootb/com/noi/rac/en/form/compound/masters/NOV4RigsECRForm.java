/** \file 
    \brief This file contains a class NOV4_RIGSEngChangeRequestForm which works as a Controller in MVC pattern.
*/

package com.noi.rac.en.form.compound.masters;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.noi.rac.en.form.compound.data.NOV4RigsECR_DC;
import com.noi.rac.en.form.compound.panels.NOV4RigsECRForm_Panel;
import com.teamcenter.rac.kernel.TCComponentForm;

/** \brief Declaration for the class NOV4_RIGSEngChangeRequestForm, it works as Controller in MVC pattern.
 		   Creation of the Form is done using MVC pattern and this class Creates both the data( NOV4_RIGSEngChangeRequest_DC )
 		   and the view( NOV4_RIGSEngChangeRequest_Panel ) and maintains the way to communicate between them. 
*/
public class NOV4RigsECRForm extends BaseMasterForm
{
	/// Hold all the data that is required for displaying.
	NOV4RigsECR_DC rigsEngChangeRequestDC;
	
	/// This is view part in MVC pattern. Displays the data in the view.
	NOV4RigsECRForm_Panel rigsEngChangeRequestPanel;
	
	/// Name for the master form.
	String masterFormName = "";

	/** \brief Creates the Instance of the NOV4_RIGSEngChangeRequestForm.
	      	   It gets the master form and associate it with the data.	
	    \param master The master form required to be associated with the data. 	   	
	*/	
	public NOV4RigsECRForm( TCComponentForm master )
	{
		super();
		masterForm = master;
		
		try 
		{
			masterFormName = masterForm.getProperty( "object_name" );
		}
		catch (Exception e) 
		{
			;
		}
		
		rigsEngChangeRequestDC = new NOV4RigsECR_DC();
		rigsEngChangeRequestPanel = new NOV4RigsECRForm_Panel();
		
		rigsEngChangeRequestDC.setForm( masterForm );
		rigsEngChangeRequestDC.populateFormComponents();
		rigsEngChangeRequestPanel.createUI( rigsEngChangeRequestDC );
	}
	
	/** \brief Gives the panel i.e. view which displays all the components. 
	    \return Returns the panel i.e. view.  	   	
	*/		
	public JComponent getPanel() 
	{
		return rigsEngChangeRequestPanel;
	}

	/** \brief Gives Icon for the Tab. 
	    \return Returns the Icon for the Tab.
	*/			
	public ImageIcon getTabIcon() 
	{
		return null;
	}

	/** \brief Gives the tool tip. 
	    \return Returns the String for the tool tip.
	*/	
	public String getTip() 
	{
		return null;
	}

	/** \brief Locks the form for editing. 
	 	\param lock Boolean value decides if the form is to be locked or unlocked.  
	*/	
	public void lockForm( boolean lock ) {} 

	/** \brief Saves the form data. 
	*/	
	public void saveForm() 
	{
		rigsEngChangeRequestDC.saveFormData();
	}
	
	/** \brief Sets the properties to be displayed. 
	*/	
	public void setDisplayProperties(){}
}
