/*
 * Created on Sep 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound.util;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.noi.rac.en.util.ComponentWrapper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;
/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ItemRevisionQueryResultsDialog extends JDialog implements
		ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Registry  reg;
	JButton applyButton = new JButton(reg.getString("Apply.BUTN"));
    JButton reQueryButton = new JButton(reg.getString("ReQuery.BUTN"));
	JButton cancelButton = new JButton(reg.getString("Cancel.BUTN"));
    JScrollPane scroller = new JScrollPane();
    JPanel paramPanel;
    JPanel head;
    JPanel buttons;
    JList  resultsList;
    Vector listItems = new Vector();
    Vector selectedItems = new Vector();
    Hashtable compDispMap = new Hashtable();
    
    public final static int CANCELLED = 1;
    public final static int REQUERY = 2;
    public final static int APPLIED = 3;
    
    public int STATUS = CANCELLED;
	
	/**
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog() throws HeadlessException {
		super();
		reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Dialog arg0) throws HeadlessException {
		super(arg0);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Dialog arg0, boolean arg1)
			throws HeadlessException {
		super(arg0, arg1);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Frame arg0) throws HeadlessException {
		super(arg0);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Frame arg0, boolean arg1)
			throws HeadlessException {
		super(arg0, arg1);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Dialog arg0, String arg1)
			throws HeadlessException {
		super(arg0, arg1);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Dialog arg0, String arg1, boolean arg2)
			throws HeadlessException {
		super(arg0, arg1, arg2);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Frame arg0, String arg1)
			throws HeadlessException {
		super(arg0, arg1);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Frame arg0, String arg1, boolean arg2)
			throws HeadlessException {
		super(arg0, arg1, arg2);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @throws java.awt.HeadlessException
	 */
	public ItemRevisionQueryResultsDialog(Dialog arg0, String arg1,
			boolean arg2, GraphicsConfiguration arg3) throws HeadlessException {
		super(arg0, arg1, arg2, arg3);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public ItemRevisionQueryResultsDialog(Frame arg0, String arg1,
			boolean arg2, GraphicsConfiguration arg3) {
		super(arg0, arg1, arg2, arg3);
  reg=Registry.getRegistry(this);
		// TODO Auto-generated constructor stub
	}
	
	public Vector getSelected() { return selectedItems; }

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

		if (arg0.getSource() == reQueryButton) {
			STATUS = REQUERY;
			this.dispose();
		}
		else if (arg0.getSource() == applyButton) {
			STATUS = APPLIED;
			Object[] selected = resultsList.getSelectedValues();
			for (int i=0;i<selected.length;i++) {
				String sel = (String)selected[i];
				TCComponent comp = (TCComponent)compDispMap.get(sel);
				selectedItems.add(comp);
			}
			this.dispose();
		}
		else if (arg0.getSource() == cancelButton) {
			STATUS = CANCELLED;
			this.dispose();
		}
		
	}

	public void displayResults(TCComponent[] queryResults) {
		
        paramPanel = new JPanel(new BorderLayout());
        head = new JPanel();
        buttons = new JPanel();
        cancelButton.addActionListener(this);
        applyButton.addActionListener(this);
        reQueryButton.addActionListener(this);
        buttons.add(reQueryButton);
        buttons.add(applyButton);
        buttons.add(cancelButton);
        paramPanel.add(head,BorderLayout.NORTH);
        paramPanel.add(scroller,BorderLayout.CENTER);
        paramPanel.add(buttons,BorderLayout.SOUTH);
        paramPanel.setPreferredSize(new Dimension(380,400));
        
		try {
			if (queryResults.length > 0) {
				for (int i=0;i<queryResults.length;i++) {
					ComponentWrapper cw = 
						new ComponentWrapper(
							queryResults[i],
							new String[] {"item_id","current_revision_id","object_name"});
					listItems.add(cw.toString());
					compDispMap.put(cw.toString(),queryResults[i]);
				}
			}
			else
				listItems.add(new String(reg.getString("NoResults.MSG")));
				
		}
		catch (Exception e1) {
			System.out.println("Could not execute query: "+e1);
			listItems.add(new String(reg.getString("NoResults.MSG")));
		}
		resultsList = new JList(listItems);
		int listHeight =
			((listItems.size() > 20) ? listItems.size()*21 : 550);
		
		resultsList.setPreferredSize(new Dimension(380,listHeight));
        
        scroller.setViewportView(resultsList);        
        getContentPane().add(paramPanel);
        setSize(new Dimension(420,400));
        setTitle(reg.getString("ItemQryResult.MSG"));
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(d.width/2,d.height/2);
        
        show();
		
	}	
}
