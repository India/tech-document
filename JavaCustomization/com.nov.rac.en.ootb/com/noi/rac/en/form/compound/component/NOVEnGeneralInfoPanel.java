package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.mission.helper.NOVMissionHelper;
import com.noi.rac.en.mission.helper.NOVMissionHelper_v2;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
//import com.nov.rac.commands.newitem.NOVLocaleDialog;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.combobox.iComboBox;

public class NOVEnGeneralInfoPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private Registry 	reg;
	
	/*public JCheckBox mfgReq;
	public NOVLOVPopupButton drafted;
	public NOVLOVPopupButton checked;
	public NOVLOVPopupButton approved;
	public JComboBox reasonCombo;
	public NOVDateButton draftedon;
	public NOVDateButton checkedon;
	public NOVDateButton approvedon;
	public iTextArea furExplanation ;
	
	public NOVEnGenSearchPanel relatedECRSearchPanel;
	public NOVEnGenSearchPanel projectsPanel;
	public NOVEnDistributionPanel distributionPanel;*/
	
	private INOVCustomFormProperties formProps;
	JCheckBox m_mfgViewReqdCheckBox;
	JCheckBox m_docControlReqdCheckBox;
	boolean m_docControlRequired = false;
	private JCheckBox m_isENCritical;
	
	public NOVEnGeneralInfoPanel(INOVCustomFormProperties formProperties)
	{
		reg=Registry.getRegistry(this);
		this.formProps =formProperties;
		
		
		boolean mfgNotRequiredGroup = NOVMissionHelper_v2.isGroup(reg
				.getString("mfgNotRequiredGroup.NAME"), formProperties);
		
		m_docControlRequired = NOVMissionHelper_v2.isGroup(reg
				.getString("docControlReqdPref.NAME"), formProperties);
		
		JPanel mfgAndDocRequiredPanel = new JPanel(new PropertyLayout());

		m_mfgViewReqdCheckBox = ((JCheckBox) formProps.getProperty("mfgreviewrequired"));
		m_mfgViewReqdCheckBox.setText(reg.getString("mfgReviewReqd.MSG"));
		m_mfgViewReqdCheckBox.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		
		m_docControlReqdCheckBox = ( ( JCheckBox ) formProps.getProperty( "nov4_doccontrol_required" ) );
		m_docControlReqdCheckBox.setText( reg.getString( "docControlRequired.MSG" ) );
		m_docControlReqdCheckBox.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));		
		
		if( !mfgNotRequiredGroup && !m_docControlRequired )
		{
			mfgAndDocRequiredPanel.add("1.1.left.center", m_mfgViewReqdCheckBox);
			mfgAndDocRequiredPanel.add( "1.2.left.center", m_docControlReqdCheckBox );
		}
		else if( !mfgNotRequiredGroup && m_docControlRequired )
		{
			mfgAndDocRequiredPanel.add("1.1.left.center", m_mfgViewReqdCheckBox);
		}
		else if( mfgNotRequiredGroup && !m_docControlRequired )
		{
			mfgAndDocRequiredPanel.add( "1.1.left.center", m_docControlReqdCheckBox );			
		}
		
		if( !mfgNotRequiredGroup && !m_docControlRequired )
		{
			if( m_mfgViewReqdCheckBox.isSelected() )
			{
				m_mfgViewReqdCheckBox.setEnabled( true );
				m_docControlReqdCheckBox.setEnabled( false );
			}
			
			m_mfgViewReqdCheckBox.addItemListener( new ItemListener()
			{
				@Override
				public void itemStateChanged(ItemEvent itemEvent )
				{
					mfgReviewRequiredStateChanged( itemEvent );
				}
			});

			if( m_docControlReqdCheckBox.isSelected() )
			{
				m_docControlReqdCheckBox.setEnabled( true );
				m_mfgViewReqdCheckBox.setEnabled( false );
			}
			
			m_docControlReqdCheckBox.addItemListener( new ItemListener()
			{
				@Override
				public void itemStateChanged( ItemEvent itemEvent )
				{
					docControlRequiredStateChanged( itemEvent );
				}
			});
		}
		
		JPanel pplInfoPanel = buildPeopleInfoPanel();
		
		JPanel reasonForNoticePanel = new JPanel();		
		
		JPanel reasonForNoticePanelFirst = new JPanel(new GridBagLayout());
		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		gbConstraints.weightx = 0.5;
		gbConstraints.insets = new Insets(0, 0, 0, 100);
		gbConstraints.gridx = 0;
		gbConstraints.gridy = 0;
		iComboBox reasonCombo = ((iComboBox) formProps.getProperty("reason"));
		reasonCombo.setPreferredSize(new Dimension(180,21));
		
		reasonForNoticePanelFirst.add(reasonCombo, gbConstraints);

		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		gbConstraints.gridx = 1;
		gbConstraints.gridy = 0;
		
		m_isENCritical = ((JCheckBox) formProps.getProperty("nov4_is_en_critical"));
		
		m_isENCritical.setText(reg.getString("markENasCritical.TITLE"));
		m_isENCritical.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
	
		reasonForNoticePanelFirst.add(m_isENCritical, gbConstraints);
		
		//((iComboBox)formProps.getProperty("reason")).setPreferredSize(new Dimension(180,21));
		try {
			if(formProps.getForm().getTCProperty("reason").getStringValue().length() == 0)
				reasonCombo.setSelectedIndex(7);
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reasonForNoticePanel.add(new NOIJLabel(reg.getString("reasonLbl.TITLE")));
		reasonForNoticePanel.add( reasonForNoticePanelFirst );
		// TCSession tcSession =  formProps.getForm().getSession();	
		
		//TODO : Need to add ECR form Type		
		((NOVEnGenSearchPanel)formProps.getProperty("relatedecr")).setTitleBorderText(
				reg.getString("relatedECRs.TITLE"));
		
		JPanel furExpPanel = new JPanel(new PropertyLayout());
		( (iTextArea)formProps.getProperty("explanation")).setLengthLimit(1900);
		JScrollPane furExpPane = new JScrollPane(( (iTextArea)formProps.getProperty("explanation")));
		furExpPane.setPreferredSize(new Dimension(565,120));
		TitledBorder tb = new TitledBorder(reg.getString("explanation.TITLE"));
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		furExpPanel.setBorder(tb);
		
		furExpPanel.add("1.1.left.center",furExpPane);
        
		/*TCPreferenceService prefrenceSrv = formProps.getForm().getSession().getPreferenceService();		
		boolean isLocale = prefrenceSrv.isTrue(TCPreferenceService.TC_preference_all, "NOV_show_localization_button");
		if (isLocale) 
		{
			ImageIcon icon = reg.getImageIcon("locale.IMG");
			final TCProperty furExplanation = formProps.getImanProperty(formProps.getProperty("explanation"));
			final JButton localeBtn = new JButton(icon);
			localeBtn.setPreferredSize(new Dimension(20,20));
			localeBtn.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent actEvt) 
				{
					NOVLocaleDialog localeDlg = new NOVLocaleDialog(furExplanation);							
					//localeDlg.setSize(350, 325);
					int x = localeBtn.getLocationOnScreen().x- localeDlg.getWidth();
					int y = localeBtn.getLocationOnScreen().y- localeDlg.getHeight();
					localeDlg.setLocation(x, y);
					localeDlg.setVisible(true);
				}
			});
			furExpPanel.add("1.2.top.center",localeBtn);	
		}*/
		
		((NOVEnGenSearchPanel)formProps.getProperty("projects")).setTitleBorderText(
				reg.getString("selPrj.TITLE"));		
		((NOVEnDistributionPanel)formProps.getProperty("distribution")).setTitleBorderText(
				reg.getString("selDistrib.TITLE"));
		
		setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		
		if( !mfgNotRequiredGroup || !m_docControlRequired )
		{
			//this.add("1.1.left.center",((JCheckBox)formProps.getProperty("mfgreviewrequired")));
			this.add( "1.1.left.center", mfgAndDocRequiredPanel );
			this.add("2.1.left.center",pplInfoPanel);
			this.add("3.1.left.center",reasonForNoticePanel);		
			this.add("4.1.left.center",((NOVEnGenSearchPanel)formProps.getProperty("relatedecr")));
			this.add("5.1.left.center",furExpPanel);
			this.add("6.1.left.center",((NOVEnGenSearchPanel)formProps.getProperty("projects")));
			this.add("7.1.left.center",((NOVEnDistributionPanel)formProps.getProperty("distribution")));
			this.setBorder(new TitledBorder(""));
		}
		else
		{
			this.add("1.1.left.center",pplInfoPanel);
			this.add("2.1.left.center",reasonForNoticePanel);		
			this.add("3.1.left.center",((NOVEnGenSearchPanel)formProps.getProperty("relatedecr")));
			this.add("4.1.left.center",furExpPanel);
			this.add("5.1.left.center",((NOVEnGenSearchPanel)formProps.getProperty("projects")));
			this.add("6.1.left.center",((NOVEnDistributionPanel)formProps.getProperty("distribution")));
			this.setBorder(new TitledBorder(""));
		}
		
	}

	private void mfgReviewRequiredStateChanged( ItemEvent itemEvent )
	{
		if( itemEvent.getStateChange()== ItemEvent.SELECTED )
		{
			m_docControlReqdCheckBox.setEnabled( false );
		}
		else
		{
			m_docControlReqdCheckBox.setEnabled( true );
		}
	}
	
	private void docControlRequiredStateChanged( ItemEvent itemEvent )
	{
		if( itemEvent.getStateChange()== ItemEvent.SELECTED )
		{
			m_mfgViewReqdCheckBox.setEnabled( false );
		}
		else
		{
			m_mfgViewReqdCheckBox.setEnabled( true );
		}
	}

	public boolean isMandatoryFieldsFilled()
	{
		String selItem =  ((iComboBox)formProps.getProperty("reason")).getSelectedItem().toString();
		
		TCComponentListOfValues lovalues = TCComponentListOfValuesType.findLOVByName((TCSession) AIFDesktop.getActiveDesktop().getCurrentApplication().getSession(), "_RSOne_enreasonforchange_");
/*		ListOfValuesInfo lovInfo = null;
		try {
			lovInfo = lovalues.getListOfValues();
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Object[] lovS = null;
		try {
			lovS = lovalues.getListOfValues().getLOVDisplayValues();
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		boolean flag = false;
		if ( lovS.length > 0 )
		{
			for ( int i = 0; i < lovS.length; i++ )
			{
				if(lovS[i].toString().equals( selItem ))
				{
					flag = true;
					break;
				}
			}
		}

		if (selItem!=null) 
		{
			if ( (selItem.trim().length()>0) && (flag == true) )
			{
				return true;
			}
		}
		
		return false;
	}
	
	public JPanel buildPeopleInfoPanel()
	{
		JPanel pplInfoPanel = new JPanel(new PropertyLayout());
		JPanel leftpplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		JPanel rightpplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		boolean misGroup = NOVMissionHelper.isGroup(reg.getString("misPref.NAME"),formProps);
		((NOVLOVPopupButton)formProps.getProperty("draftedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("draftedby")).setPreferredSize(new Dimension(200,21));
		((NOVLOVPopupButton)formProps.getProperty("checkedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("checkedby")).setPreferredSize(new Dimension(200,21));
		((NOVLOVPopupButton)formProps.getProperty("approvedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("approvedby")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("draftedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("draftedon")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("checkedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("checkedon")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("approvedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("approvedon")).setPreferredSize(new Dimension(200,21));
        leftpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("draftedby.LABEL")));
		leftpplInfoPanel.add("1.2.left.center",((NOVLOVPopupButton)formProps.getProperty("draftedby")));
		leftpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("checkedby.LABEL")));
		leftpplInfoPanel.add("2.2.left.center",((NOVLOVPopupButton)formProps.getProperty("checkedby")));
		leftpplInfoPanel.add("3.1.left.center",new NOIJLabel(reg.getString("approvedby.LABEL")));
		leftpplInfoPanel.add("3.2.left.center",((NOVLOVPopupButton)formProps.getProperty("approvedby")));
		
		rightpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("draftedon.LABEL")));
		rightpplInfoPanel.add("1.2.left.center",((NOVDateButton)formProps.getProperty("draftedon")));
		rightpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("checkedon.LABEL")));
		rightpplInfoPanel.add("2.2.left.center",((NOVDateButton)formProps.getProperty("checkedon")));
		rightpplInfoPanel.add("3.1.left.center",new NOIJLabel(reg.getString("approvedon.LABEL")));
		rightpplInfoPanel.add("3.2.left.center",((NOVDateButton)formProps.getProperty("approvedon")));
		if (misGroup){
			addMissionPanel(leftpplInfoPanel,rightpplInfoPanel);
		}
		pplInfoPanel.add("1.1.left.center",leftpplInfoPanel);
		pplInfoPanel.add("1.2.left.center",rightpplInfoPanel);
		
		return pplInfoPanel;
	}
	
	private void addMissionPanel(JPanel leftpplInfoPanel ,JPanel rightpplInfoPanel) {
		((NOVLOVPopupButton)formProps.getProperty("createdby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("createdby")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("createdon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("createdon")).setPreferredSize(new Dimension(200,21));
		leftpplInfoPanel.add("4.1.left.center",new NOIJLabel("Created By:"));
		leftpplInfoPanel.add("4.2.left.center",((NOVLOVPopupButton)formProps.getProperty("createdby")));
		rightpplInfoPanel.add("4.1.left.center",new NOIJLabel("Creation Date:"));
		rightpplInfoPanel.add("4.2.left.center",((NOVDateButton)formProps.getProperty("createdon")));
		
		
	}

	
}