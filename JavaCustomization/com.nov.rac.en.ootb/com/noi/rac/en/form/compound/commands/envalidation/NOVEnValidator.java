package com.noi.rac.en.form.compound.commands.envalidation;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.util.components.NOVEnTargetsPanel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMViewRevision;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentBOMWindowType;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.Registry;

public class NOVEnValidator {

	public Vector<TCComponent> targets;
	public TCSession session;
	public HashMap<TCComponent, Vector<TCComponent>> targetBOMLines=new HashMap<TCComponent, Vector<TCComponent>>();
	public Vector<TCComponent> failedTargets=new Vector<TCComponent>();
	public Vector<TCComponent> otherWorkflowTargets=new Vector<TCComponent>();
	public TCComponentTask rootTask;
	public _EngNoticeForm_Panel enFormPanel;
	private NOVEnTargetsPanel targetPanel;
	private Registry reg;
	public NOVEnValidator(_EngNoticeForm_Panel enFormPanel, NOVEnTargetsPanel targetPanel) 
	{
		super();	
		reg=Registry.getRegistry(this);
		try
		{
			this.targets = enFormPanel.getEnTargets();
			this.session =enFormPanel.getSession();
			this.rootTask=enFormPanel.getProcess().getRootTask();
			this.enFormPanel=enFormPanel;
			this.targetPanel=targetPanel;
			loadTargetsBOMChildren();
			NOVEnValidationReport report=new NOVEnValidationReport(session, this);
			report.createUI();		
			//report.setSize(new Dimension(600, 600));
			//report.setLocationRelativeTo(enFormPanel);
			//report.setModal(true);
			//report.setVisible(true);
	
		}
		catch(TCException exp)
		{
			System.out.println(exp);
		}
	}	

	public void loadTargetsBOMChildren()
	{
		//get each target 
		for(int i=0;i<targets.size();i++)
		{
			//fetch the bvr
			TCComponentBOMViewRevision bvr=getBvrfromItemRev(targets.get(i));
			//traverse each target first level and get the item status
			if(bvr!=null)
				targetBOMLines.put(targets.get(i), loadBOMfirstLevelchildren((TCComponentItemRevision) targets.get(i),bvr));
			
		}
		//generate the validation report
	}
	public TCComponentBOMViewRevision getBvrfromItemRev(TCComponent targetRev)
	{
		TCComponentBOMViewRevision theBom=null;
		try
		{
			//fetch the Bvr			
			AIFComponentContext[] itemRevisionComponents = ((TCComponentItemRevision)targetRev).getChildren();
			for (int i=0;i<itemRevisionComponents.length;i++)
			{
				if (itemRevisionComponents[i].getComponent() instanceof TCComponentBOMViewRevision) 
				{		
					//TCComponentBOMViewRevision bvr=(TCComponentBOMViewRevision)itemRevisionComponents[i].getComponent();

					theBom = (TCComponentBOMViewRevision)itemRevisionComponents[i].getComponent();
					break;
				}
			}			
		}
		catch(TCException te)
		{
			System.out.println();
		}
		return theBom;
	}

	public Vector<TCComponent> loadBOMfirstLevelchildren(TCComponentItemRevision topRev, TCComponentBOMViewRevision theBOM)
	{
		Vector<TCComponent> firstLevelBomLines=new Vector<TCComponent>();
		try
		{
			TCComponentBOMWindowType bwType = 
				(TCComponentBOMWindowType)session.getTypeService().getTypeComponent("BOMWindow");
			TCComponentBOMWindow bw = bwType.create(null);
			TCComponentBOMLine topline = bw.setWindowTopLine(null,null,null,theBOM);
			
			HashMap<TCComponent, String> tempMap = enFormPanel.targetsPanel.targetsTable.futureStatMap;
			System.out.println(tempMap.size());
			String futureStatus = enFormPanel.targetsPanel.targetsTable.futureStatMap.get(topRev).toString();
			
			AIFComponentContext[] childComponents = topline.getChildren();
			for (int k=0;k<childComponents.length;k++) 
			{
				TCComponentBOMLine bl = (TCComponentBOMLine)childComponents[k].getComponent();
				TCComponentItemRevision childcomp=bl.getItemRevision();
				childcomp.refresh();
				firstLevelBomLines.add(k, childcomp);
				if(!futureStatus.equalsIgnoreCase(reg.getString("swap.TYPE")))
					validate(childcomp);
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		return firstLevelBomLines;
	}
	public void validate(TCComponentItemRevision childcomp)
	{
		try
		{
			if(!childcomp.getProperty("object_type").equalsIgnoreCase(reg.getString("DocumentsRevision.TYPE")))
			{
				if(!targets.contains(childcomp))
				{
					String status=((TCComponentItemRevision)childcomp).getItem().getProperty("release_statuses");
				
				
					/*if(futureStatus.equalsIgnoreCase("swap"))
						otherWorkflowTargets.add(childcomp);*/
					
					/*else */if(!((status.compareTo(reg.getString("MFG.TYPE"))==0)||(status.compareTo(reg.getString("ACTIVE.TYPE"))==0))
							||(status.compareTo(reg.getString("ACT.TYPE"))==0))
					{
						//check if part of other workflow
						if(isPartofOtherProcess(childcomp))
							otherWorkflowTargets.add(childcomp);
						else
						{
							if((childcomp!=null)&&!((failedTargets.contains(childcomp))||(targets.contains(childcomp))))							
							{
								failedTargets.add(childcomp);
							}

						}

					}
				}
			}
		}catch(TCException te)
		{
			System.out.println(te);
		}
	}

	public void addRDDofFailedItems()
	{
		try
		{
			for(int j=0;j<failedTargets.size();j++)
			{
				TCComponentItemRevision childcomp=(TCComponentItemRevision)failedTargets.get(j);
				if(!childcomp.getProperty("object_type").equalsIgnoreCase(reg.getString("DocumentsRevision.TYPE")))
				{
					AIFComponentContext[] rddDocComps=childcomp.getRelated(reg.getString("rdd.TYPE"));
					String RevId=childcomp.getProperty("current_revision_id");
					for(int k=0;k<rddDocComps.length;k++)
					{
						TCComponentItem rddDocItem=(TCComponentItem)rddDocComps[k].getComponent();

						TCComponentItemRevision rddRev=getItemRevisionofRevID(rddDocItem, RevId);
						if((rddRev!=null)&&!((failedTargets.contains(rddRev))||(targets.contains(rddRev))))							
						{
							failedTargets.add(rddRev);
							//then add its RDD related comps also
							addRddRevsofDocument(rddRev);

						}
					}
				}else
				{
					addRddRevsofDocument(childcomp);
				}
			}
		}catch(Exception e)
		{
			System.out.println(e);
		}

	}

	public void addRddRevsofDocument(TCComponentItemRevision rddrevDoc)
	{
		TCComponentItemRevision[] revs=null;
		try
		{
			AIFComponentContext[] rddComps=rddrevDoc.getItem().getPrimary();//Related("RelatedDefiningDocument");
			Vector<TCComponentItem> rdditems=new Vector<TCComponentItem>();
			for(int k=0;k<rddComps.length;k++)
			{
				if(rddComps[k].getContext().toString().equalsIgnoreCase(reg.getString("rdd.TYPE")))
				{
					TCComponentItemRevision rddItem=(TCComponentItemRevision)rddComps[k].getComponent();
					TCComponentItem item=rddItem.getItem();
					if(!rdditems.contains(item))rdditems.add(item);
				}
			}
			for(int k=0;k<rdditems.size();k++)
			{
				TCComponentItem rddItem=rdditems.get(k);
				String RevId=rddrevDoc.getProperty("current_revision_id");
				TCComponentItemRevision rddRev=getItemRevisionofRevID(rddItem, RevId);				
				if((rddRev!=null)&&!((failedTargets.contains(rddRev))||(targets.contains(rddRev))))							
				{
					failedTargets.add(rddRev);					
				}				
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
	}

	public TCComponentItemRevision getItemRevisionofRevID(TCComponentItem rddItem, String RevId)
	{
		TCComponentItemRevision itemRev=null;
		try
		{
			AIFComponentContext[] revItems=rddItem.getChildren("revision_list");
			for(int k=0;k<revItems.length;k++)
			{
				String revId=((TCComponentItemRevision)revItems[k].getComponent()).getProperty("current_revision_id");
				if(revId.compareTo(RevId)==0)
				{
					itemRev=(TCComponentItemRevision)revItems[k].getComponent();
					break;
				}
			}

			if(itemRev==null)
			{
				TCUserService us=session.getUserService();
				Object[] obj=new Object[2];
				obj[0]=rddItem;
				obj[1]=RevId;
				TCComponentItemRevision rev=(TCComponentItemRevision)us.call("NOV_EN_revise", obj);
				itemRev=rev;
				//NOV_EN_revise

			}
		}catch(TCException e)
		{
			System.out.println(e);

		}

		return itemRev;
	}

	public void addFaileditemstoWorkflow()
	{
		try
		{
			addRDDofFailedItems();
			int attachtypes[]=new int[failedTargets.size()];
			TCComponent comp[]=new TCComponent[failedTargets.size()];
			for(int i=0;i<failedTargets.size();i++)
			{
				attachtypes[i]=TCAttachmentType.TARGET;
				comp[i]=failedTargets.get(i);
			}			

			rootTask.addAttachments(TCAttachmentScope.GLOBAL, comp, attachtypes);

			//update EnTargets
			enFormPanel.Entargets.addAll(failedTargets);
			targetPanel.targetsTable.loadTabledata(failedTargets);
			targetPanel.targetsTable.updateUI();
			targetPanel.targetsTable.validate();
			targetPanel.validate();
			//update disposition tabs
			enFormPanel.addDispData(comp);
			enFormPanel.validate();
			enFormPanel.updateUI();
		}catch(TCException exp)
		{
			System.out.println(exp);
		}
	}
	public boolean isPartofOtherProcess(TCComponentItemRevision childcomp)
	{

		try
		{

			TCComponentItemRevision[] inprocessItemRevs = childcomp.getItem().getInProcessItemRevisions();

			if (inprocessItemRevs.length>0) 
			{
				Vector<TCComponentItemRevision> itemVec = new Vector<TCComponentItemRevision>();
				for (int i = 0; i < inprocessItemRevs.length; i++) 
				{
					itemVec.add(inprocessItemRevs[i]);	
				}
				if (itemVec.contains(childcomp)) 
				{
					return true;
				}
			}
			/*AIFComponentContext[] contexts=childcomp.whereReferenced();			
			if(contexts!=null)
			{
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
					{
						TCComponentProcess currentProcess=(TCComponentProcess)contexts[j].getComponent();		
						if(!(currentProcess==enFormPanel.getProcess()))
							return true;						
					}
				}
			}*/

		}catch(Exception e)
		{
			System.out.println("Exception is:" + e);

		}		
		return false;
	}


}