package com.noi.rac.en.form.compound.component;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.util.Miscellaneous;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.queries.General;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;
import com.noi.rac.en.customize.util.MessageBox;

public class NOVEnGenSearchPanel extends JPanel implements ActionListener, FocusListener
{
	private static final long serialVersionUID = 1L;
	
	private JTextField  searchField;
	private JButton searchButton;

	private JList sourceList;
	private JScrollPane sourceListScroll;
	private JList targetList;
	private JScrollPane targetListScroll;
	
	DefaultListModel sourceListModel;
	DefaultListModel targetListModel;
	
	private JButton addTarget;
	private JButton removeTarget;
	private String searchTypeStr;
	private TCSession tcSession;
	private Component comp = null;			//  Vivek
	
	private Registry reg;
	public NOVEnGenSearchPanel(TCSession session,String searchObjType)
	{
		reg = Registry.getRegistry(this);
		tcSession = session;
		searchTypeStr = searchObjType;
		searchField = new JTextField(15);
		searchField.setText(reg.getString("SEARCHFIELD.TXT"));
		searchField.addFocusListener(this);
		searchButton = new JButton(reg.getString("SEARCHBUTTON.LABEL"));
		searchButton.addActionListener(this);
		
		sourceList = new JList();
		sourceListModel = new DefaultListModel();
		sourceList.setModel(sourceListModel);
		sourceList.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				int clickCount = e.getClickCount();
				if (clickCount == 2) 
				{
					addSelValToTargetList();
				}
			}
		});
		sourceListScroll = new JScrollPane(sourceList);
		sourceListScroll.setPreferredSize(new Dimension(260,120));
		
		targetList = new JList();
		targetListModel = new DefaultListModel();
		targetList.setModel(targetListModel);
		targetListScroll = new JScrollPane(targetList);
		targetListScroll.setPreferredSize(new Dimension(260,120));
        
		sourceList.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) 
			{
				if (!targetList.isSelectionEmpty()) 
				{
					targetList.clearSelection();
				}
			}
		});
		
		targetList.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) 
			{
				if (!sourceList.isSelectionEmpty()) 
				{
					sourceList.clearSelection();
				}
			}
		});
		
		//TODO: need to change the path if we are integrating in other plugin
		ImageIcon plusIcon=reg.getImageIcon("enadded.ICON");		
		ImageIcon minusIcon = reg.getImageIcon("enremove.ICON");  

		addTarget = new JButton();
		addTarget.setIcon(plusIcon);
		addTarget.addActionListener(this);
		removeTarget = new JButton();
		removeTarget.setIcon(minusIcon);
		removeTarget.addActionListener(this);
		
		JPanel addremButtonPanel = new JPanel(new PropertyLayout(5 , 10 , 5 , 5 , 50 , 20));
		addremButtonPanel.add("1.1.left.center",addTarget);
		addremButtonPanel.add("2.1.left.center",removeTarget);
		
		JPanel sourcePanel = new JPanel(new PropertyLayout());
		NOIJLabel srcLbl = new NOIJLabel(reg.getString("AVAILABLE.LABEL"));
		sourcePanel.add("1.1.center.center",srcLbl);
		sourcePanel.add("2.1.left.top",sourceListScroll);
		
		JPanel targetPanel = new JPanel(new PropertyLayout());
		NOIJLabel tarLbl = new NOIJLabel(reg.getString("SELECTED.LABEL"));
		targetPanel.add("1.1.center.center",tarLbl);
		targetPanel.add("2.1.left.top",targetListScroll);
		
		JPanel searchPanel = new JPanel();
		searchPanel.add(searchField);
		searchPanel.add(searchButton);
		
		this.setLayout(new PropertyLayout());
		this.add("1.1.left.top", searchPanel);
		this.add("2.1.left.top", sourcePanel);
		this.add("2.2.left.top", addremButtonPanel);
		this.add("2.3.left.top", targetPanel);
		
	}
	public void setTitleBorderText(String title)
	{
		TitledBorder tb = new TitledBorder(title);
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		setBorder(tb);
	}
	public void actionPerformed(ActionEvent actEvt) 
	{
		if (actEvt.getSource() ==  addTarget ) 
		{
			addSelValToTargetList();
		}
		if (actEvt.getSource() ==  removeTarget ) 
		{
			Object[] selObjs = targetList.getSelectedValues();
			for (int i = 0; i < selObjs.length; i++) 
			{
				targetListModel.removeElement(selObjs[i]);
			}
		}
		if (actEvt.getSource() ==  searchButton) 
		{
			executeQuery();
		}
	}

	private void  addSelValToTargetList()
	{
		Object[] objs = sourceList.getSelectedValues();
		List<Object> existComps = new ArrayList<Object>();
		for (int i = 0; i < objs.length; i++) 
		{
			int objInd = targetListModel.indexOf(objs[i].toString());
			if (objInd >= 0) 
			{
				existComps.add(objs[i].toString());
			}
			else
			{
			    targetListModel.addElement(objs[i].toString());
			}
		}
		if (existComps.size()>0) 
		{
			showErrorIfListisNotEmpty(existComps);
		}
	}
	
	private void showErrorIfListisNotEmpty(List<Object> existComps)
	{
		String errorStr =reg.getString("AlreadyObjects.MSG");
		
		for (int i = 0; i < existComps.size(); i++) 
		{
			errorStr = errorStr + existComps.get(i).toString()+",";
		}
		if (errorStr.charAt(errorStr.length()-1) == ',') 
		{
			errorStr = errorStr.substring(0, errorStr.length()-1);
		}
		
		//TODO: need to change the title of messagebox
		comp = this.getParent().getParent();				//  Vivek
		MessageBox.post(getWindow(comp), (reg.getString("Info.MSG")), errorStr, MessageBox.INFORMATION);		//  Vivek
	}

	private void executeQuery() 
	{
		if (searchTypeStr!=null && tcSession!=null) 
		{

			String[] names = {(reg.getString("Type.TYPE")), (reg.getString("Name.TYPE"))};
			String searchStr =  searchField.getText();

			if (searchTypeStr.equals(reg.getString("TC_Project.TYPE"))) 
			{
				//Aparna commented to enhance the search speed
				/*if (searchStr.endsWith("*")) 

				/*if (searchStr.endsWith("*")) 

				{
					searchStr = searchStr.substring(0,searchStr.length()-1);

				}*/
				try
				{
					//TCComponent[] projComps = getSearchResult(searchStr);					
					TCUserService us=tcSession.getUserService();
					Object[] obj=new Object[1];
					obj[0]=searchStr;
					TCComponent[] projComps=(TCComponent[])us.call("NATOIL_projectsQuery", obj);
					//TCComponent[] projComps = getSearchResult(searchStr);			
					//TCDECREL-5625
					if (projComps != null && projComps.length >0) 
					{
						populateSourceList(projComps);	
					}
					else
					{
						//  Vivek Start
						comp = this.getParent().getParent();			
						MessageBox.post(getWindow(comp), reg.getString("Info.MSG"), reg.getString("NoObjectFound.MSG"), MessageBox.INFORMATION);		
						//  Vivek End
					}
				}
				catch(TCException te)
				{
					System.out.println(te);
				}
			}
			else if (searchStr.trim().length()>0) 
			{
				String[] values = {searchTypeStr,searchStr.trim()};
				TCComponent[] searchResults =  General.execute(tcSession, names, values);	

				if (searchResults.length >0) 
				{
					populateSourceList(searchResults);	
				}
				else
				{
					//  Vivek Start
					comp = this.getParent().getParent();			
					MessageBox.post(getWindow(comp), reg.getString("Info.MSG"), reg.getString("NoObjectFound.MSG"), MessageBox.INFORMATION);	
					//  Vivek End
				}
			}	
		}
	}

	public void populateSourceList(Object[] sourceObjs)
	{
		if (sourceObjs!=null) 
		{
			if (sourceListModel.getSize()>0) 
			{
				sourceListModel.clear();
			}
			for (int i = 0; i < sourceObjs.length; i++) 
			{
				if(!sourceListModel.contains(sourceObjs[i].toString()))
				{
					sourceListModel.addElement(sourceObjs[i].toString());
				}
			}	
		}
	}

//	private TCComponent[] getSearchResult(String searchString) 
//	{
//		TCComponent[] comps=null;
//		try
//		{
//			/*TCComponentType compType = tcSession.getTypeComponent("TC_Project");
//			TCComponent[] projs = compType.extent();
//			ArrayList myList = new ArrayList();
//			for(int i = 0; i< projs.length; i++)
//			{
//				String curProject;
//				try 
//				{
//					curProject = ((TCComponent)projs[i]).getStringProperty("project_id");
//					if(curProject.toUpperCase().indexOf(searchString.toUpperCase())>=0)
//					{
//						myList.add(projs[i]);
//					}
//				} 
//				catch (TCException e) 
//				{
//					e.printStackTrace();
//				}
//			}
//			int finalSize = myList.size();
//			if(finalSize > 0)
//			{
//				TCComponent[] result = new TCComponent[myList.size()];
//				for(int j = 0; j < myList.size(); j++)
//				{
//					result[j] = (TCComponent) myList.get(j);
//				}
//				return result;
//			}*/
//			TCUserService userService = tcSession.getUserService();
//			Object[] objs = new Object[1];
//			objs[0] = searchString;
//			comps = (TCComponent[])userService.call("NATOIL_projectsQuery", objs);
//
//			/*TCComponentQueryType qryType = (TCComponentQueryType)tcSession.getTypeComponent("ImanQuery");
//			TCComponentQuery qry = (TCComponentQuery)qryType.find("NOV_ProjectsQuery");
//			String[] name = new String[2];
//			name[0]=  "Projects";
//			name[1]= "Projects Name";
//			String [] value = new String[2];
//			value[0]= "NOV_ProjectsQuery";
//			value[1]= searchString;
//			comps = qry.execute(name, value);*/
//		}
//		catch (TCException e1) 
//		{
//			e1.printStackTrace();
//		}
//		return comps;
//	}
	
	public String[] getTargetListObjects()
	{
		String [] retObjs = new String[targetListModel.getSize()];
		for (int i = 0; i < targetListModel.getSize(); i++) 
		{
			retObjs[i]=targetListModel.getElementAt(i).toString();
		}
		return retObjs;
	}

	public void populateTargetList(String[] projects)
	{
		if (projects!=null) 
		{
			for (int i = 0; i < projects.length; i++) 
			{
				targetListModel.addElement(projects[i]);
			}	
		}
		
	}
	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(searchField))
		{
			if(searchField.getText().equalsIgnoreCase(reg.getString("SEARCHFIELD.TXT")))
			{
				searchField.setText("");
			}
		}
	}
	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(searchField))
		{
			if(searchField.getText().equalsIgnoreCase(""))
			{
				searchField.setText(reg.getString("SEARCHFIELD.TXT"));
			}
		}
	}
	
	// Vivek Start		//  Getting the parent component i.e Window
	
	public Window getWindow(Component con)
	{
		Window window = null;
		
		if(con.getParent() instanceof Window)
		{
			window = (Window) con.getParent();
		}
		else
		{
			 window = getWindow(con.getParent());
		}
		
		return window;
	}   //  Vivek End
	
}