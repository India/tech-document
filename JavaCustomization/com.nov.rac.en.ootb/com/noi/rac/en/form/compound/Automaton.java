/*
 * Created on Jul 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;


import com.noi.rac.en.form.compound.masters.IMasterForm;
import com.noi.rac.en.form.compound.masters.Nov4_SupersedeForm;
import com.noi.rac.en.form.compound.masters._EngNoticeForm_;
import com.noi.rac.en.form.compound.masters._StopForm_;
import com.noi.rac.en.form.compound.panels.ItemRevisionPanel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.noi.rac.en.form.compound.util.ItemRevisionHistory;
import com.noi.rac.en.util.Miscellaneous;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.commands.open.OpenFormDialog;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.workflow.common.TestErrorListener;

/**
 * @author hughests
 * 
 * Preferences - Java - Code Style - Code Templates
 */
public class Automaton extends AbstractTCForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Registry appReg;

	TCComponentForm masterForm;

	TCComponent item;

	LinkedList activeMasters = new LinkedList();

	LinkedList itemRevisions = new LinkedList();

	LinkedList itemRevForms = new LinkedList();

	IMasterForm theMaster;

	JPanel revisionHistoryPanel;

	JTabbedPane automatonTabbePane = new JTabbedPane();

	/**
	 * @throws java.lang.Exception
	 */
	public Automaton() throws Exception {
		super();
		appReg=Registry.getRegistry(this);
	}

	/**
	 * @param arg0
	 * @throws java.lang.Exception
	 */
	public Automaton(TCComponentForm arg0) throws Exception {
		super(arg0);
		appReg=Registry.getRegistry(this);
		item = null;

		masterForm = arg0;
		AIFComponentContext[] arr = masterForm.whereReferenced();
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].getComponent() instanceof TCComponentItem) {
				item = (TCComponent) arr[i].getComponent();
			}
		}
		if (item != null) {
			AIFComponentContext[] children = item.getChildren();
			for (int i = 0; i < children.length; i++) {
				if (children[i].getComponent() instanceof TCComponentForm
						&& (TCComponentForm) children[i].getComponent() == masterForm)
					continue;
				else if (children[i].getComponent() instanceof TCComponentItemRevision)
				{
					itemRevisions.add((TCComponentItemRevision) children[i].getComponent());
				}
			}
		}
		loadRevisionHistory();
		loadForm();
		createTabbedPane(activeMasters);
		this.revalidate();
		this.repaint();
	}

	/**
	 * create all the tab content
	 * @param forms contain all the tab UI
	 */
	private void createTabbedPane(LinkedList forms) {
		ResourceBundle rg=ResourceBundle.getBundle("com.noi.rac.en.form.form_locale");		
		Dimension maxComponentDimension = new Dimension(0,0);
		Registry reg = Registry.getRegistry(this);
		ImageIcon tipIcon = reg.getImageIcon("com/nov/images/TipOfTheDay16.png");
		for(Iterator it = forms.iterator(); it.hasNext();){
			IMasterForm currentForm = (IMasterForm)it.next();
			JComponent currentPanel =  currentForm.getPanel();
			maxSize(maxComponentDimension, currentPanel.getPreferredSize());
		}
		//maxSize(maxComponentDimension,revisionHistoryPanel.getPreferredSize());
		Dimension realDimension = new Dimension((int)(maxComponentDimension.getWidth()+10),(int)(maxComponentDimension.getHeight()+20));
		int tabNumber = 0;
		for(Iterator it = forms.iterator(); it.hasNext();){
			IMasterForm currentForm = (IMasterForm)it.next();
			tabNumber++;
			JComponent currentPanel =  currentForm.getPanel();
			JPanel masterPannel = new JPanel(new VerticalLayout(0,0,0,10,0));
			masterPannel.setBackground(new Color(225, 225, 225));
			masterPannel.setPreferredSize(realDimension);
			masterPannel.add("top.nobind.center.center",currentPanel);
			if(currentPanel instanceof _EngNoticeForm_Panel)
			{
				((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane.insertTab(currentForm.getPanelName()==null  ? appReg.getString("tabNumber.STR")+ tabNumber :rg.getString(currentForm.getPanelName()) ,
						currentForm.getTabIcon() == null ? tipIcon : currentForm.getTabIcon() , masterPannel,
								currentForm.getTip() == null ? (appReg.getString("engNoticeFormTip.STR")) : currentForm.getTip(), 0);
				//((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane.setPreferredSize(realDimension);
				((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane.setBackground(new Color(225, 225, 225));
				add("top.nobind.center.center", ((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane);
				((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane.setSelectedIndex(0);
				//((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane.setComponentAt(0, masterPannel);
				
			}else if(currentPanel instanceof _EngNoticeForm_Panel_v2)
			{
				System.out.println("rg.getString(currentForm.getPanelName() :::" + rg.getString(currentForm.getPanelName()));
				((_EngNoticeForm_Panel_v2)currentPanel).enautomatonTabbePane.insertTab(currentForm.getPanelName()==null  ? appReg.getString("tabNumber.STR")+ tabNumber :rg.getString(currentForm.getPanelName()) ,
						currentForm.getTabIcon() == null ? tipIcon : currentForm.getTabIcon() , masterPannel,
								currentForm.getTip() == null ? (appReg.getString("engNoticeFormTip.STR")) : currentForm.getTip(), 0);
				((_EngNoticeForm_Panel_v2)currentPanel).enautomatonTabbePane.setPreferredSize(realDimension);
				((_EngNoticeForm_Panel_v2)currentPanel).enautomatonTabbePane.setBackground(new Color(225, 225, 225));
				add("top.nobind.center.center", ((_EngNoticeForm_Panel_v2)currentPanel).enautomatonTabbePane);
				((_EngNoticeForm_Panel_v2)currentPanel).enautomatonTabbePane.setSelectedIndex(0);
				//((_EngNoticeForm_Panel)currentPanel).enautomatonTabbePane.setComponentAt(0, masterPannel);
			}
			
			else
			{
			automatonTabbePane.addTab(currentForm.getPanelName()==null  ? appReg.getString("tabNumber.STR")+ tabNumber :rg.getString(currentForm.getPanelName()) ,
					currentForm.getTabIcon() == null ? tipIcon : currentForm.getTabIcon() , masterPannel,
							currentForm.getTip() == null ? appReg.getString("engNoticeFormTip.STR") : currentForm.getTip());
			automatonTabbePane.setPreferredSize(realDimension);
			automatonTabbePane.setBackground(new Color(225, 225, 225));
			add("top.nobind.center.center", automatonTabbePane);
			}
		}
		
	}	

	/**
	 * 
	 * @see com.ugsolutions.rac.form.AbstractTCForm#loadForm()
	 */


	public void loadForm() throws TCException {

		boolean isLocked = false;
		//masterForm.set
		appReg = Registry.getRegistry(this);//"Automaton");
		setLayout(new VerticalLayout(0, 0, 0, 0, 0));
		JPanel imagePanel = new JPanel(new PropertyLayout(0, 0, 0, 0, 0, 0));
		// Create the other property fields of the form
		JLabel imageLabel = new JLabel();
		String type = masterForm.getType();
		//		add imagelabel to imagePanel
		imageLabel.setIcon(appReg.getImageIcon(type + ".ICON")); 
		imagePanel.add("1.1.center.center.resizable.resizable", imageLabel); 
		add("top.nobind.center.center", imagePanel);

		IMasterForm theMasterForm = null;
		try {
			try {
				boolean isLocked1 = masterForm.getLogicalProperty("Locked");
				boolean isLocked2 = masterForm.getLogicalProperty("locked");
				if (isLocked1 || isLocked2) isLocked = true;
			} catch (Exception e) {
				isLocked = false;
			}
			// new method to instance the concrete master form
			theMasterForm = getConcreteMasterForm(masterForm);
			theMaster = theMasterForm;
			activeMasters.add(theMasterForm);
			if (isLocked) {
				theMasterForm.lockForm(isLocked);
			}
		}
		catch (Exception e)
		{
			System.out.println("Could not create master class: " + e);
			}

		}

	//	create revision tab content

	/**
	 *  retrieve all the revisions history information and  draw the UI   
	 * @throws TCException
	 */
	private void loadRevisionHistory() throws TCException {

		revisionHistoryPanel = new JPanel(new GridLayout(itemRevisions.size(),
				1));
		revisionHistoryPanel.setBackground(new Color(225, 225, 225));
		revisionHistoryPanel.setBorder(null);
		for (int i = 0; i < itemRevisions.size(); i++) {
			try {
				TCComponentItemRevision rev = (TCComponentItemRevision) itemRevisions
				.get(i);
				ItemRevisionHistory revHist = new ItemRevisionHistory();
				revHist.creationDate = rev.getProperty("creation_date");
				revHist.dateReleased = rev.getProperty("date_released");
				revHist.revisionId = rev.getProperty("item_revision_id");
				revHist.name = rev.getProperty("object_name");
				revHist.description = rev.getProperty("object_desc");
				JPanel aPanel = new JPanel();
				aPanel.setBackground(new Color(225, 225, 225));
				aPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
				ItemRevisionPanel irp = new ItemRevisionPanel();
				irp.createUI(revHist);
				revisionHistoryPanel.add(aPanel);
				AIFComponentContext[] cc = rev
				.getRelated("IMAN_master_form_rev");
				if (cc.length == 1) {
					TCComponentForm revForm = (TCComponentForm) cc[0]
					                                               .getComponent();
					itemRevForms.add(revHist);
					AIFComponentContext[] arr = revForm.whereReferenced();
					aPanel.setLayout(new GridLayout(arr.length, 1));
					aPanel.add(irp);
						}
				else
				{
					aPanel.setLayout(new GridLayout(1, 1));
					aPanel.add(irp);
				}
			} catch (Exception e) {
				System.out.println("Error getting or attaching Item Revision panels... "+ e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ugsolutions.rac.form.AbstractTCForm#saveForm()
	 */
	public void saveForm()
	{		
		for (int i = 0; i < activeMasters.size(); i++)
			((IMasterForm) activeMasters.get(i)).saveForm();
	}


	private void maxSize(Dimension cmax, Dimension omax) {

		if (cmax.height < omax.height)
			cmax.height = omax.height;
		if (cmax.width < omax.width)
			cmax.width = omax.width;

	}

	private IMasterForm getConcreteMasterForm(TCComponentForm paraForm) {
		if (appReg == null)
			appReg = Registry.getRegistry(this);//, "Automaton");
		String itemType = paraForm.getType();
		IMasterForm theMasterForm = (IMasterForm) appReg.newInstanceFor(itemType, paraForm);

		return theMasterForm;
	}

	public void setFormReadWrite() {
		theMaster.setDisplayProperties();
		for (int i = 0; i < activeMasters.size(); i++)
			((IMasterForm) activeMasters.get(i)).setDisplayProperties();
	}
	public boolean isFormSavable()
	{
		boolean formSavable =false;
		for (int i = 0; i < activeMasters.size(); i++)
		{
			if (((IMasterForm) activeMasters.get(i)) instanceof _EngNoticeForm_)
			{
				return (((_EngNoticeForm_) activeMasters.get(i)).isformSavable());
			}
			if (((IMasterForm) activeMasters.get(i)) instanceof Nov4_SupersedeForm)
			{
				return (((Nov4_SupersedeForm) activeMasters.get(i)).isformSavable());
			}
			
			formSavable=((IMasterForm) activeMasters.get(i)).isFormSavable();
			
			if(!formSavable)
			{
				 break;
			}
		}
		return formSavable;

	}
	public void displayDialog(OpenFormDialog openformdialog, boolean flag)
	{		
		TCReservationService reserserv=masterForm.getSession().getReservationService();
		//reserserv.isReserved(masterForm);
		try {
			if(!reserserv.isReserved(masterForm))
				if(!(masterForm.getProperty("is_modifiable").length()==0))//isModifiable(arg0))
				reserserv.reserve(masterForm);
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		openformdialog.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosed(WindowEvent e) 
			{
				try
				{
					TCReservationService reserserv=masterForm.getSession().getReservationService();
					if(reserserv.isReserved(masterForm))
					{
						reserserv.unreserve(masterForm);
					}
				}
				catch(TCException tc)
				{
					System.out.println(tc);
				}
				super.windowClosed(e);
			}

		});
		super.displayDialog(openformdialog, flag);
	}
	public void setFormReadOnly()
	{
		//setModifiableComponents(this, false);
		System.out.println("hi");
	}
	public void postLoadForm(Object obj)
	{
		System.out.println("hi post");
	}


}
