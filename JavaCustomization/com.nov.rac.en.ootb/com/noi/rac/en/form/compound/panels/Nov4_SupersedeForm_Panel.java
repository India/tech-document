package com.noi.rac.en.form.compound.panels;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel_v2;
import com.noi.rac.en.form.compound.component.NOVSupersedeGeneralInfoPanel;
import com.noi.rac.en.form.compound.data.Nov4_SupersedeForm_DC;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOVEnDispositionPanel;
import com.noi.rac.en.util.components.NOVStopTargetsPanel;
import com.noi.rac.en.util.components.NOVSupersedeTablePanel;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;


public class Nov4_SupersedeForm_Panel extends PrintablePanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TCComponentForm Enform;
	public Vector<TCComponent> Entargets=new Vector<TCComponent>();
	private TCComponentProcess currentProcess=null;
	
	public NOVSupersedeTablePanel targetsPanel;
	public JTabbedPane enautomatonTabbePane = new JTabbedPane();
	private Registry  reg;
	public String dispErrStr = "";
	public String reasonErrStr = "";
	public HashMap<TCComponent, String> currentStatMap ;
	AIFTableModel tableModel;//Added By Sandip
	Vector<TCComponent> targetdisp;
	//TCDECREL-1586:START
	private boolean isReadOnly = false;
	//TCDECREL-1586:END
	
	public Nov4_SupersedeForm_Panel()
	{
		super();
		reg=Registry.getRegistry(this);
	}	
	public Nov4_SupersedeForm_Panel(TCComponentForm masterForm)
	{
		super();
		reg=Registry.getRegistry(this);
		Enform = masterForm;
		
	}
	
	public void initialize()
	{
		
	}
	public TCSession getSession()
	{
		return Enform.getSession();
	}

	public void saveDipsDatatoForm()
	{   
		((Nov4_SupersedeForm_DC)novFormProperties).saveOnlyDispFormData();
	}

	public void saveForm()
	{   
		((Nov4_SupersedeForm_DC)novFormProperties).saveFormData();
	}

	public void createUI(INOVCustomFormProperties sSedeDC)
	{  
		novFormProperties = sSedeDC;
		JPanel generalInfoPanel = new NOVSupersedeGeneralInfoPanel(this, novFormProperties);
		JPanel	genInfoPanel	= new JPanel(new PropertyLayout(5,5,5,5,5,5));
		genInfoPanel.add("1.1.left.center", generalInfoPanel);
		genInfoPanel.setPreferredSize(new Dimension(640, 110));
		
		//TCDECREL-1586:START
		checkSupersedeENFormStatus();
		
		//TCDECREL-1586:END		
						
		loadEnTargets();
		targetsPanel = new NOVSupersedeTablePanel(this, novFormProperties);	
		tableModel=targetsPanel.getTableModel();
		//TCDECREL-1586:START
		targetsPanel.targetsTable.setEnabled(!isReadOnly);
		//TCDECREL-1586:END
		JPanel targetPanel = new JPanel(new PropertyLayout());
		targetPanel.setPreferredSize(new Dimension(640, 200));
		targetPanel.add("1.1.center.center", targetsPanel);
		targetPanel.setBorder(new TitledBorder(""));
		
		
		JPanel reasonForStopPanel = new JPanel();
		reasonForStopPanel.setBackground(this.getBackground());
		//reasonForStopPanel.setPreferredSize(new Dimension(650,120));
		//reasonForStopPanel.setMinimumSize(new Dimension(650,120));
		iTextArea reasonProp = (iTextArea)sSedeDC.getProperty("nov4_explanation");
		reasonProp.setLineWrap(true);	
		reasonProp.setLengthLimit(1900);
		reasonProp.setRequired(true);
		
		( (iTextArea)sSedeDC.getProperty("nov4_explanation")).setLengthLimit(1900);
		//TCDECREL-1586:START
		reasonProp.setEnabled(!isReadOnly);
		//TCDECREL-1586:END
		JScrollPane stopreasonPane = new JScrollPane(( reasonProp ));
		stopreasonPane.setPreferredSize(new Dimension(615,120));
		TitledBorder tb = new TitledBorder("Reason for Supersede");
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		reasonForStopPanel.setBorder(tb);
		reasonForStopPanel.add(stopreasonPane);
		
		//TCDECREL-1586:START
		//((NOVEnDistributionPanel)sSedeDC.getProperty("nov4_distribution")).setTitleBorderText(reg.getString("SelectDistribution.MSG"));
		NOVEnDistributionPanel_v2 selectionDistribution = (NOVEnDistributionPanel_v2)sSedeDC.getProperty("nov4_distribution");
		JPanel	distPanel	= new JPanel(new PropertyLayout(5,5,5,5,5,5));
		TitledBorder tb1 = new TitledBorder(reg.getString("SelectDistribution.MSG"));
		tb1.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		distPanel.setBorder(tb1);
		distPanel.setPreferredSize(new Dimension(640,232));
		//selectionDistribution.setPreferredSize( new Dimension( 640, 200 ) );
		selectionDistribution.setNewDimention(624, 200);
		distPanel.add("1.1.center.center", selectionDistribution);
		
		selectionDistribution.setControlState( isReadOnly );

		PrintExternalAttachmentsPanel_v2 printExtPanel = new PrintExternalAttachmentsPanel_v2(
				sSedeDC, false, this, reg.getString( "PrintExternalAttachmentsPanel.Title2" ) );
		printExtPanel.setControlState( isReadOnly );
		printExtPanel.setPreferredSize( new Dimension( 640, 160 ) );
		printExtPanel.tablePane.setPreferredSize(new Dimension(620,90));
		
		ExternalAttachmentsPanel_v2 extPanel = new ExternalAttachmentsPanel_v2(sSedeDC, false, this );
		extPanel.setControlState( isReadOnly );
		extPanel.setPreferredSize( new Dimension( 640, 160 ) );
		extPanel.tablePane.setPreferredSize(new Dimension(620,90));
		//TCDECREL-1586:END

		JPanel bottomPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));		
		//bottomPanel.add("1.1.left.top",sPanel);
		bottomPanel.add("1.1.left.top",genInfoPanel);
		bottomPanel.add("2.1.left.top",targetPanel);
		bottomPanel.add("3.1.left.top",reasonForStopPanel);
		//TCDECREL-1586:START
		//bottomPanel.add("4.1.left.top",((NOVEnDistributionPanel)sSedeDC.getProperty("nov4_distribution")));
		//bottomPanel.add("4.1.left.top",(selectionDistribution));
		bottomPanel.add("4.1.left.top",(distPanel));
		
		bottomPanel.add("5.1.left.top",printExtPanel);
		bottomPanel.add("6.1.left.top",extPanel);
		//TCDECREL-1586:END
		
		JScrollPane scrollpane=new JScrollPane(bottomPanel);
		//bottomPanel.addMouseWheelListener(new ScrollForm(this));
		add(scrollpane);
	}
	
	
	public void actionPerformed(ActionEvent e) {}

	
	public HashMap<TCComponent, String> getStopCurrentStatus()
	{
		return currentStatMap;
	}
	public Vector<TCComponent> getEnTargets()
	{
		return Entargets;
	}
	public TCComponentProcess getProcess()
	{
		return currentProcess;
	}
	//TCDECREL-1586:START
	private void checkSupersedeENFormStatus()
	{
		try
		{
			TCComponent[] relStatusList = ( novFormProperties.getForm().getTCProperty( "release_status_list" ).getReferenceValueArray() );
			String isCheckedOut = novFormProperties.getForm().getTCProperty( "checked_out" ).getStringValue();

			String processStageList = novFormProperties.getForm().getProperty( "process_stage_list" );
			String[] processStageListArr = processStageList.split( "," );
			String reqdProcessStage =  reg.getString( "ProcessStage.Stage" );
			
			if( isCheckedOut.trim().length() == 0 && processStageListArr.length > 1 &&
					!processStageListArr[1].trim().startsWith( reqdProcessStage ) )
				isReadOnly = true;
			else if( isCheckedOut.trim().length() == 0 && processStageListArr.length <= 1 )
				isReadOnly = true;
			
			for( int index = 0; index < relStatusList.length; ++index )
			{
				if( relStatusList[index].getProperty( "name" ).equalsIgnoreCase( reg.getString( "ENReleaseStatus.STATUS" ) ) )
					isReadOnly = true;
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
	}
	//TCDECREL-1586:END

	public void loadEnTargets()
	{
		try
		{
			//EN Targets;//Aparna : commented to fetch the targets from dispostion
			AIFComponentContext[] contexts=Enform.whereReferenced();			
			if(contexts!=null)
			{
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
					{
						currentProcess=(TCComponentProcess)contexts[j].getComponent();
						TCComponentTask rootTask=((TCComponentProcess)currentProcess).getRootTask();
						TCComponent[] targets=rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
						for(int i=0;i<targets.length;i++)
						{
							if(targets[i] instanceof TCComponentItem)
								Entargets.add(targets[i]);
						}
						((NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition")).setDispData(this);
						break;						
					}
				}
				
			}	

			if((currentProcess==null)&&((NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition")!=null && 
					((NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition")).referncedispdata.length>0))
			{
				Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(((NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition")).referncedispdata));
				//TCComponentItemRevisionType type=(TCComponentItemRevisionType)Enform.getSession().getTypeService().getTypeComponent("ItemRevision");
				TCComponentItemType type=(TCComponentItemType)Enform.getSession().getTypeService().getTypeComponent("Item");
				for(int k=0;k<tempReference.size();k++)
				{					
					String targetId=(tempReference.get(k).getTCProperty("targetitemid")).getStringValue();			
					//String revid=(tempReference.get(k).getTCProperty("rev_id")).getStringValue();
					TCComponent comp=type.find(targetId);
					//.findRevision(targetId, revid);
					if(comp!=null)
						Entargets.add(comp);
				}	
				((NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition")).setDispData(this);
			}		


		}catch(Exception e)
		{
			System.out.println("Exception is:" + e);

		}		

	}
	public void removeDispositionData(TCComponent tobremoved)
	{
		NOVEnDispositionPanel dispnale=(NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition");
		dispnale.removeDispTab(tobremoved);
	}

	public void addDispData(TCComponent[] tobAdded)
	{
		NOVEnDispositionPanel dispnale=(NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition");
		dispnale.adddispTabs(tobAdded);
	}

	class ScrollForm implements MouseWheelListener 
	{
		JPanel mailpanelLc = new JPanel();

		ScrollForm(JPanel mainpaneld)
		{
			mailpanelLc =mainpaneld;
		}

		public void mouseWheelMoved(MouseWheelEvent e) 
		{    

			Container cont = getParentCust(mailpanelLc);
			if (cont instanceof JScrollPane) 
			{                            
				JScrollPane sc = (JScrollPane)cont;
				if (e.getWheelRotation() < 0) 
				{
					sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() -15);                                
				}
				else 
				{
					sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() + 15);
				}
			}
		}        
		public Container getParentCust(Container con)
		{    
			if (con instanceof JScrollPane)
			{        
				return con;    
			}
			else
			{
				return getParentCust(con.getParent());
			}                        
		}
	}

	public AIFTableModel getTableModel(){
		 return tableModel;
	    }
	public Vector<TCComponent> getSupdTargetDisposition()
	{
		targetdisp = new Vector<TCComponent>(Arrays.asList(((NOVEnDispositionPanel)novFormProperties.getProperty("nov4_supdtargetdisposition")).referncedispdata));
		return targetdisp;
	}

}
