package com.noi.rac.en.form.compound.commands.envalidation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;


import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.util.components.NOIJLabel;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;


public class NOVEnValidationReport extends AbstractAIFDialog implements ActionListener
{
	private TCSession session;
	private NOVEnValidator validator;
	private JTree targetsTree;
	private boolean failedButAdded;
	public JButton yesButton;
	JButton noButton;

	public NOVEnValidationReport(TCSession session, NOVEnValidator validator)
	{
		super();
		this.session = session;
		this.validator = validator;
	}
	
	public void loadTableData()
	{
		for(int i=0;i<validator.targets.size();i++)
		{
			
		}
	}

	public void createUI()
	{
		Registry reg=Registry.getRegistry(this);
		failedButAdded=false;
		setTitle(reg.getString("envalreport.TITLE"));
		DefaultMutableTreeNode rootnode=new DefaultMutableTreeNode(reg.getString("enTargets.STR"));		
		//create the bom tree for each of the targets		
		for(int i=0;i<validator.targets.size();i++)
		{
			DefaultMutableTreeNode targetNode=createTargetBOMTree(validator.targets.get(i));
			rootnode.add(targetNode);
		}
		targetsTree=new JTree(rootnode);
		targetsTree.setCellRenderer(new TargetTreeRenderer());
		targetsTree.setLargeModel(true);
		JScrollPane scrollPane=new JScrollPane(targetsTree);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(scrollPane, BorderLayout.CENTER);
		JPanel bottomPanel=new JPanel();
		bottomPanel.setLayout(new BorderLayout());
		if(validator.failedTargets.size()>0)
		{
			NOIJLabel label=new NOIJLabel();
			//label.setText("Some items failed because they are not in the MFG or ACTIVE status, \ndo you like to add them to this workflow?");
			label.setText(reg.getString("failedTargetsLbl.STR"));
			noButton=new JButton(reg.getString("okbtn.TITLE"));
			noButton.addActionListener(this);
			JPanel emptyPanel=new JPanel();			
			emptyPanel.add(noButton);
			bottomPanel.add(label, BorderLayout.NORTH);
			bottomPanel.add(emptyPanel, BorderLayout.SOUTH);
			/*yesButton=new JButton("YES");
			yesButton.addActionListener(this);
			noButton=new JButton("NO");
			noButton.addActionListener(this);
			bottomPanel.add(label, BorderLayout.NORTH);
			JPanel emptyPanel=new JPanel();
			emptyPanel.add(yesButton);
			emptyPanel.add(noButton);
			bottomPanel.add(emptyPanel, BorderLayout.SOUTH);*/
			
		}else if(failedButAdded)
		{
			NOIJLabel label=new NOIJLabel();
			label.setText(reg.getString("failedButAddedTargetsLbl.STR"));	
			bottomPanel.add(label, BorderLayout.NORTH);
			noButton=new JButton(reg.getString("okbtn.TITLE"));
			noButton.addActionListener(this);
			JPanel emptyPanel=new JPanel();			
			emptyPanel.add(noButton);
			bottomPanel.add(emptyPanel, BorderLayout.SOUTH);
		}
		else
		{
			NOIJLabel label=new NOIJLabel();
			label.setText(reg.getString("notFailedTargestLbl.STR"));	
			bottomPanel.add(label, BorderLayout.NORTH);
			noButton=new JButton(reg.getString("okbtn.TITLE"));
			noButton.addActionListener(this);
			JPanel emptyPanel=new JPanel();			
			emptyPanel.add(noButton);
			bottomPanel.add(emptyPanel, BorderLayout.SOUTH);
			
		}
		//button panel
		this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
		targetsTree.setExpandsSelectedPaths(true);
		
		int height = targetsTree.getRowCount()*50;
		if(height>600)
			height = 600;
		//user input dialog
		this.setPreferredSize(new Dimension(550, height));
		this.setModal(true);
		
		this.pack();
		this.centerToScreen(0.0, 0.0);
		this.setVisible(true);
	}

	public DefaultMutableTreeNode createTargetBOMTree(TCComponent itemrev)
	{
		//ImageIcon icon=TCTypeRenderer.getIcon(itemrev);
		Registry reg=Registry.getRegistry(this);
		DefaultMutableTreeNode targetNode=new DefaultMutableTreeNode(itemrev);		
		try
		{

			Vector<TCComponent> targetBomLines=validator.targetBOMLines.get(itemrev);
			for(int i=0;i<targetBomLines.size();i++)
			{
				TCComponent bomchild=targetBomLines.get(i);
				String treenode=bomchild.toString();
				if((validator.failedTargets.contains(bomchild))||(validator.otherWorkflowTargets.contains(bomchild)))
				{
					if(validator.targets.contains(bomchild))
					{
						treenode=treenode.concat(reg.getString("failedButAddedTargets.MSG"));
						validator.failedTargets.remove(bomchild);
						if(validator.failedTargets.size()==0)
							failedButAdded=true;
					}
					else if(validator.otherWorkflowTargets.contains(bomchild))
						treenode=treenode.concat(reg.getString("failedTargets.MSG"));
					else
						treenode=treenode.concat(reg.getString("failed.MSG"));	

				}
				else
				{
					String futureStatus = validator.enFormPanel.targetsPanel.targetsTable.futureStatMap.get(itemrev).toString();
					if(!futureStatus.equalsIgnoreCase("swap"))
					{
						String status=((TCComponentItemRevision)bomchild).getItem().getProperty("release_statuses");
						if(!((status.compareTo("MFG")==0)||(status.compareTo("ACTIVE")==0)))
						{
						treenode=treenode.concat(reg.getString("failedButAddedTargets.MSG"));
						failedButAdded=true;
						}
						else
							treenode=treenode.concat(reg.getString("passed.MSG"));
					}
					else
						treenode=treenode.concat(reg.getString("passed.MSG"));
				}
				//icon=TCTypeRenderer.getIcon(bomchild);
				targetNode.add(new DefaultMutableTreeNode(treenode));
			

			}
		}
		catch(Exception e)
		{

		}
		return targetNode;
	}
	
	class TargetTreeRenderer extends DefaultTreeCellRenderer
	{

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{			
			// TODO Auto-generated method stub
			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
					row, hasFocus);
			Registry reg = Registry.getRegistry(this);
			if(((DefaultMutableTreeNode)value).getUserObject() instanceof TCComponentItemRevision)
			setIcon(TCTypeRenderer.getIcon(((DefaultMutableTreeNode)value).getUserObject()));
			if(leaf)
			{
				if(value.toString().contains(reg.getString("failed.MSG")))
				{
				setForeground(Color.RED);
				setBackground(Color.RED);
				}
			}	
			else
			{				
				setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			}
			return this;
		}
		
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==yesButton)
		{
			validator.addFaileditemstoWorkflow();
			dispose();
			
		}else if(ae.getSource()==noButton)
		{
			dispose();
		}
	}


}
