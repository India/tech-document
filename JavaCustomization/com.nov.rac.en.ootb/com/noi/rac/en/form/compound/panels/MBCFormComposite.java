package com.noi.rac.en.form.compound.panels;

import java.awt.Toolkit;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.massbomchange.helper.NOVDataManagementServiceHelper;
import com.noi.rac.en.util.massbomchange.helper.NOVMBCFormComponents;
import com.noi.rac.en.util.massbomchange.mbcpanels.MBCAffectedAsmTablePanel;
import com.noi.rac.en.util.massbomchange.mbcpanels.MBCBasicInfoPanel;
import com.noi.rac.en.util.massbomchange.mbcpanels.MBCCompPartToReplacePanel;
import com.noi.rac.en.util.massbomchange.mbcpanels.MBCReplacementPartPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;

public class MBCFormComposite
{

    private Shell m_shell;
    private TCComponentForm m_mbcForm;
    private String m_owningUser = "";
    private String m_creationDate = "";
    private String m_owningGroup = "";
    private String m_compPartItemID = "";
    private String m_compPartAltID = "";
    private String m_compPartItemName = "";
    private String m_compPartItemStatus = "";
    private String m_compPartItemDesc = "";
    private String m_compPartItemRevID = "";
    private String m_compPartRevStatus = "";
    private String m_compPartItemOwnUser = "";
    private String m_compPartItemOwnGrp = "";
    private String m_replmntPartItemID = "";
    private String m_replmntPartAltID = "";
    private String m_replmntPartItemName = "";
    private String m_replmntPartItemStatus = "";
    private String m_replmntPartItemDesc = "";
    private String m_replmntPartItemRevID = "";
    private String m_replmntPartRevStatus = "";
    private String m_replmntPartItemOwnUser = "";
    private String m_replmntPartItemOwnGrp = "";
    private String[][] m_affectedPropValues = null;
    public Composite m_composite;
    private MBCBasicInfoPanel m_basicInfoPanel;
    private MBCCompPartToReplacePanel m_compPartRepPanel;
    private MBCReplacementPartPanel m_replacedPartPanel;
    private MBCAffectedAsmTablePanel m_affAsmPanel;

    public MBCFormComposite(TCComponentForm masterForm, Shell parentShell)
    {
        super();
        m_mbcForm = masterForm;
        m_shell = parentShell;
        createUI();
        fetchFormProperties(m_mbcForm);
    }



    public Composite createUI()
    {
    	m_shell.setSize(new Point(SWTUIHelper.convertHorizontalDLUsToPixels(m_shell,425), SWTUIHelper.convertVerticalDLUsToPixels(m_shell,400)));
        m_shell.setLayout(new GridLayout());
        ScrolledComposite m_scrolledComposite = new ScrolledComposite(m_shell, SWT.EMBEDDED |SWT.BORDER| SWT.H_SCROLL |  SWT.V_SCROLL);
		m_scrolledComposite.setAlwaysShowScrollBars(true);

		Toolkit toolkit = Toolkit.getDefaultToolkit();
	    int screenResolution = toolkit.getScreenResolution();
	    Point scrollCompSize = null;

	    if(screenResolution<=100){
	    	scrollCompSize =  new Point(SWTUIHelper.convertHorizontalDLUsToPixels(m_scrolledComposite,400), SWTUIHelper.convertVerticalDLUsToPixels(m_scrolledComposite,350));
	    }else if(screenResolution>100 && screenResolution<125){
	    	scrollCompSize =  new Point(SWTUIHelper.convertHorizontalDLUsToPixels(m_scrolledComposite,400), SWTUIHelper.convertVerticalDLUsToPixels(m_scrolledComposite,290));
	    }else if(screenResolution>125 && screenResolution<150){
	    	scrollCompSize =  new Point(SWTUIHelper.convertHorizontalDLUsToPixels(m_scrolledComposite,400), SWTUIHelper.convertVerticalDLUsToPixels(m_scrolledComposite,220));
	    }
	    m_scrolledComposite.setMinSize(scrollCompSize);
		Composite comp = new Composite(m_scrolledComposite, SWT.None);
		comp.setLayout(new GridLayout(1, false));
        m_basicInfoPanel = new MBCBasicInfoPanel(comp, SWT.NONE);
        m_compPartRepPanel = new MBCCompPartToReplacePanel(comp, SWT.NONE);
        m_replacedPartPanel = new MBCReplacementPartPanel(comp, SWT.NONE);
        m_affAsmPanel = new MBCAffectedAsmTablePanel(comp, SWT.NONE);
        m_scrolledComposite.pack();
        comp.setSize(SWTUIHelper.convertHorizontalDLUsToPixels(m_shell,425), SWTUIHelper.convertVerticalDLUsToPixels(m_shell,400)); // updated for 100 DPI
        m_scrolledComposite.setContent(comp);

        return m_composite;

    }

    private void fetchFormProperties(TCComponentForm mbcForm)
    {
        if (mbcForm.getType().equalsIgnoreCase(NOVMBCFormComponents.NOV4_MBCFORM))
        {

            NOVDataManagementServiceHelper componentItem_helper = new NOVDataManagementServiceHelper(mbcForm);
            TCComponent mbcFormComponent = componentItem_helper.getMBCFormProperties(mbcForm);
            populateMBCFormComponents(mbcFormComponent);
            displayMBCFormPropValues();
        }
    }

    private void populateMBCFormComponents(TCComponent mbcFormComponent)
    {

        populateGenericPropertyValues(mbcFormComponent);
        populateMBCFormCompPartValues(mbcFormComponent);
        populateMBCFormReplacementPartValues(mbcFormComponent);
        populateMBCFormAffectdPartValues(mbcFormComponent);

    }

    private void displayMBCFormPropValues()
    {
        displayGenericFormProps();
        displayCompPartFormProps();
        displayReplmntPartFormProps();
        displayMBCFormAffctdAssemblies();
    }

    private void populateGenericPropertyValues(TCComponent mbcFormComponent)
    {
        try
        {

            m_owningUser = mbcFormComponent.getTCProperty(NOVMBCFormComponents.ITEM_OWNING_USER).getReferenceValue()
                    .toString();
            m_creationDate = (new NOVDateButton(mbcFormComponent.getTCProperty(NOVMBCFormComponents.ITEM_CREATION_DATE).getDateValue()))
            		.getDateString();
            m_owningGroup = mbcFormComponent.getTCProperty(NOVMBCFormComponents.ITEM_OWNING_GROUP).getReferenceValue()
                    .toString();

        }
        catch (TCException e)
        {
            e.printStackTrace();
        }

    }

    private void populateMBCFormCompPartValues(TCComponent mbcFormComponent)
    {
        try
        {
            TCComponent mbcFormCompPart = mbcFormComponent.getTCProperty(NOVMBCFormComponents.NOV4_COMPONENT_PART)
                    .getReferenceValue();
            if (mbcFormCompPart != null)
            {
                m_compPartItemID = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_ITEM_ID).getStringValue();
                m_compPartAltID = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_ALT_ID).getStringValue();
                m_compPartItemName = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_NAME).getStringValue();
                m_compPartItemStatus = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_ITEM_STATUS)
                        .getStringValue();
                m_compPartItemDesc = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_DESCRIPTION)
                        .getStringValue();
                m_compPartItemRevID = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_REVISION_ID)
                        .getStringValue();
                m_compPartRevStatus = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_REVISION_STATUS)
                        .getStringValue();
                m_compPartItemOwnUser = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_OWNING_USER)
                        .getStringValue();
                m_compPartItemOwnGrp = mbcFormCompPart.getTCProperty(NOVMBCFormComponents.NOV4_OWNING_GROUP)
                        .getStringValue();
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }

    private void populateMBCFormReplacementPartValues(TCComponent mbcFormComponent)
    {
        try
        {
            TCComponent mbcFormReplmntPart = mbcFormComponent.getTCProperty(NOVMBCFormComponents.NOV4_REPLACEMENT_PART)
                    .getReferenceValue();
            if (mbcFormReplmntPart != null)
            {
                m_replmntPartItemID = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_ITEM_ID)
                        .getStringValue();
                m_replmntPartAltID = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_ALT_ID)
                        .getStringValue();
                m_replmntPartItemName = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_NAME)
                        .getStringValue();
                m_replmntPartItemStatus = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_ITEM_STATUS)
                        .getStringValue();
                m_replmntPartItemDesc = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_DESCRIPTION)
                        .getStringValue();
                m_replmntPartItemRevID = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_REVISION_ID)
                        .getStringValue();
                m_replmntPartRevStatus = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_REVISION_STATUS)
                        .getStringValue();
                m_replmntPartItemOwnUser = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_OWNING_USER)
                        .getStringValue();
                m_replmntPartItemOwnGrp = mbcFormReplmntPart.getTCProperty(NOVMBCFormComponents.NOV4_OWNING_GROUP)
                        .getStringValue();
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }

    private void populateMBCFormAffectdPartValues(TCComponent mbcFormComponent)
    {

        try
        {
            TCComponent[] mbcFormAffectedParts = mbcFormComponent.getTCProperty(
                    NOVMBCFormComponents.NOV4_AFFECTD_ASSEMBLIES).getReferenceValueArray();

            m_affectedPropValues = new String[mbcFormAffectedParts.length][NOVMBCFormComponents.AFFECTEDASSMBL_COL_COUNT];
            if (mbcFormAffectedParts.length > 0)
            {
                for (int index = 0; index < mbcFormAffectedParts.length; index++)
                {
                    m_affectedPropValues[index][0] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_ITEM_ID).getStringValue();
                    m_affectedPropValues[index][1] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_ALT_ID).getStringValue();
                    m_affectedPropValues[index][2] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_REVISION_ID).getStringValue();
                    m_affectedPropValues[index][3] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_NAME).getStringValue();
                    m_affectedPropValues[index][4] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_DESCRIPTION).getStringValue();
                    m_affectedPropValues[index][5] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_OWNING_GROUP).getStringValue();
                    m_affectedPropValues[index][6] = mbcFormAffectedParts[index].getTCProperty(
                            NOVMBCFormComponents.NOV4_ITEM_STATUS).getStringValue();
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }

    private void displayGenericFormProps()
    {

    	m_basicInfoPanel.m_txtInitiatedBy.setText(m_owningUser);
    	m_basicInfoPanel.m_txtDateInitiatedBy.setText(m_creationDate);
    	m_basicInfoPanel.m_txtOwningGrp.setText(m_owningGroup);

    }

    private void displayCompPartFormProps()
    {

    	m_compPartRepPanel.m_txtCompPartID.setText(m_compPartItemID);
    	m_compPartRepPanel.m_txtCompPartAltID.setText(m_compPartAltID);
    	m_compPartRepPanel.m_txtCompPartName.setText(m_compPartItemName);
    	m_compPartRepPanel.m_txtCompPartItemStatus.setText(m_compPartItemStatus);
    	m_compPartRepPanel.m_txtCompPartDesc.setText(m_compPartItemDesc);
    	m_compPartRepPanel.m_txtCompPartRev.setText(m_compPartItemRevID);
    	m_compPartRepPanel.m_txtCompPartRevStatus.setText(m_compPartRevStatus);
    	m_compPartRepPanel.m_txtCompPartOwner.setText(m_compPartItemOwnUser);
        m_compPartRepPanel.m_txtCompPartOwningGrp.setText(m_compPartItemOwnGrp);

    }

    private void displayReplmntPartFormProps()
    {

    	m_replacedPartPanel.m_txtReplPartID.setText(m_replmntPartItemID);
        m_replacedPartPanel.m_txtReplPartAltID.setText(m_replmntPartAltID);
        m_replacedPartPanel.m_txtReplPartName.setText(m_replmntPartItemName);
        m_replacedPartPanel.m_txtReplPartItemStatus.setText(m_replmntPartItemStatus);
        m_replacedPartPanel.m_txtReplPartDesc.setText(m_replmntPartItemDesc);
        m_replacedPartPanel.m_txtReplPartRev.setText(m_replmntPartItemRevID);
        m_replacedPartPanel.m_txtReplPartRevStatus.setText(m_replmntPartRevStatus);
        m_replacedPartPanel.m_txtReplPartOwner.setText(m_replmntPartItemOwnUser);
        m_replacedPartPanel.m_txtReplPartOwningGrp.setText(m_replmntPartItemOwnGrp);

    }

    private void displayMBCFormAffctdAssemblies()
    {
        for (int i = 0; i < m_affectedPropValues.length; i++)
        {
            Vector<Object> rowData = new Vector<Object>();
            rowData.add(m_affectedPropValues[i][0]);
            rowData.add(m_affectedPropValues[i][1]);
            rowData.add(m_affectedPropValues[i][2]);
            rowData.add(m_affectedPropValues[i][3]);
            rowData.add(m_affectedPropValues[i][4]);
            rowData.add(m_affectedPropValues[i][5]);
            rowData.add(m_affectedPropValues[i][6]);
            m_affAsmPanel.m_affctdAssmebliesTable.addRow(rowData);
        }
    }

    public Composite getComposite()
    {
        return m_composite;
    }
}
