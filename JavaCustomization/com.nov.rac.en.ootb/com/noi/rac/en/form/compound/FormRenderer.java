package com.noi.rac.en.form.compound;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.en.form.compound.masters.IMasterForm;
import com.noi.rac.en.form.compound.panels.MBCFormComposite;
import com.noi.rac.en.util.SWT_AWT_Wrapper;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;

public class FormRenderer extends AbstractTCForm
{
    private static final long serialVersionUID = 1L;
    private Registry m_appReg;
    private TCComponentForm m_masterForm;
    
    public FormRenderer() throws Exception
    {
        super();
    }
    
    public FormRenderer(TCComponentForm tcComponentform) throws Exception
    {
        super(tcComponentform);
        m_masterForm = tcComponentform;
    }
    
    public void addNotify()
    {
        super.addNotify();
        try
        {
            loadForm();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void loadForm() throws TCException
    {
        
        m_appReg = Registry.getRegistry(this);
        
        while (true)
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    setLayout(new VerticalLayout(0, 0, 0, 0, 0));
                    Shell new_shell = SWT_AWT_Wrapper.getShell(FormRenderer.this);
                    MBCFormComposite mbcFormComposite = new MBCFormComposite(m_masterForm,new_shell);

                }
            });
            break;
        }
    }
    
    @Override
    public void saveForm()
    {
        
    }
    
    private IMasterForm getConcreteMasterForm(TCComponentForm paraForm)
    {
        if (m_appReg == null)
        {
            m_appReg = Registry.getRegistry(this);
        }
        String itemType = paraForm.getType();
        IMasterForm theMasterForm = (IMasterForm) m_appReg.newInstanceFor(itemType, paraForm);
        
        return theMasterForm;
    }
}
