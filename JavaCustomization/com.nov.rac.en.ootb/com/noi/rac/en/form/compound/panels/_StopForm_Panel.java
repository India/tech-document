package com.noi.rac.en.form.compound.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel_v2;
import com.noi.rac.en.form.compound.component.NOVStopGeneralInfoPanel;
import com.noi.rac.en.form.compound.data._StopForm_DC;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVStopTargetsPanel;
import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.combobox.iComboBox;

public class _StopForm_Panel extends PrintablePanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Registry registry;
	public TCComponentForm stopform;
	public TCComponent[] referncedispdata;	
	public HashMap<TCComponent, TCComponent> targetdispMap=new HashMap<TCComponent, TCComponent>();
	public Vector<TCComponent> stopTargets=new Vector<TCComponent>();
	public HashMap<TCComponent, String> currentStatMap ; 
	private TCComponentProcess currentProcess=null;
	//JPanel mainPanel=new JPanel(new GridBagLayout());
	public NOVStopTargetsPanel targetsPanel;
	public NOVStopGeneralInfoPanel stopGenInfoPanel;
	private iTextArea stopReason;
	private iTextArea stopComment;
	private JRadioButton stopWOnPO;
	private JRadioButton stopShipment;
	private JRadioButton stopAll;
	private iTextArea stopInstruct;
	private NOVEnDistributionPanel_v2 distPanel;
	private NOVDateButton expectedResolutionDate;
	//private JRadioButton partRevised;
	//public boolean partRevisedFlag;
	private TitledBorder stopReasonTitle;
	private NOIJLabel dateLbl;
	private TitledBorder stopOrderTitle;
	private TitledBorder stopDispTitle;
	private iComboBox stopRemCombo;
	private JPanel reasonForStopPanel;//2064
	private boolean m_bIsSTOP = true;
	private boolean m_bIsCanelSTOP = true;

	private boolean isReadOnly = false;
	//TCDECREL-2707:Start
	private JPanel	distributionPanel; 
	private TitledBorder distTitleBorder;
	private JPanel stopcommentPanel;
	//TCDECREL-2707:End
//	public boolean isPartRevisedFlag() {
//		return partRevisedFlag;
//	}

	public _StopForm_Panel( TCComponent[] referncedispdata)
	{
		super();
		registry = Registry.getRegistry(this);
		currentStatMap=new HashMap<TCComponent, String>();
		this.referncedispdata=referncedispdata;
	}

	public TCComponent[] getDispData()
	{
		return referncedispdata;
	}
	
	public void initialize()
	{
		
	}
	public TCSession getSession()
	{
		return stopform.getSession();
	}
	
	public void saveDipsDatatoForm()
	{
		((_StopForm_DC)novFormProperties).dispDataComp = referncedispdata;

		if(((_StopForm_DC)novFormProperties).dispDataComp!=null)
		{
			TCProperty targetdispProperty;
			try 
			{
				targetdispProperty = ((_StopForm_DC)novFormProperties).masterform.getFormTCProperty("stoptargetdisposition");
		
				if (((_StopForm_DC)novFormProperties).dispDataComp != null && targetdispProperty!=null)
					targetdispProperty.setReferenceValueArray(referncedispdata);	
				((_StopForm_DC)novFormProperties).getForm().save();
				
			} 
			catch (TCException e) {
				e.printStackTrace();
			}		
		}
	}


	private void setStopProcessType()
	{
		// get process name
		String sProcessName = null;
		try
		{
			AIFComponentContext[] contexts=novFormProperties.getForm().whereReferenced();			
			if(contexts!=null)
			{
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
					{
						TCComponentProcess currentProcess1 =(TCComponentProcess)contexts[j].getComponent();
						TCComponentTask rootTask=((TCComponentProcess)currentProcess1).getRootTask();
						sProcessName = rootTask.toString();					
					}
				}
			}
			if(sProcessName == null)
				return;
		}
		catch (TCException e)
		{
			e.printStackTrace();
			System.out.println("exception while getting where reference for stop form");
			return;
		}
		
		TCSession session = (TCSession)AIFUtility.getDefaultSession();
		TCPreferenceService prefServ = session.getPreferenceService();		
		String[] grpStrArray = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
						registry.getString("STOPProcesses.PREF") );
		
		for(int i=0; i< grpStrArray.length; i++ )
		{
			String[] sStopPrefVal = grpStrArray[i].split("=");
			
			String[] sPrefWorkFlowNames = sStopPrefVal[1].split(",");
			for(int j=0; j< sPrefWorkFlowNames.length; j++ )
			{	
				if( sPrefWorkFlowNames[j].contains( sProcessName ) )
				{			
					if(sStopPrefVal[0].contentEquals("STOP") )
					{
						m_bIsSTOP = true;
						m_bIsCanelSTOP = false;
						break;
					}
					else if(sStopPrefVal[0].contentEquals("CANCEL-STOP") )
					{
						m_bIsCanelSTOP = true;
						m_bIsSTOP = false;
						break;
					}
				}
			}
		}
	}
	@SuppressWarnings("serial")
	public void createUI(INOVCustomFormProperties formProps)
	{
		novFormProperties = formProps;
		stopform=(TCComponentForm)novFormProperties.getForm();
		checkSupersedeENFormStatus();
		
		setStopProcessType();
		
		stopGenInfoPanel = new NOVStopGeneralInfoPanel(this, formProps);
		JPanel stopPanel1 = new JPanel(new PropertyLayout(5, 5, 5,5, 5,5));
		stopPanel1.add("1.1.center.center", stopGenInfoPanel);
		stopPanel1.setBorder(new TitledBorder(""));
		stopPanel1.setPreferredSize(new Dimension(665, 110));
		
		loadEnTargets();
		targetsPanel=new NOVStopTargetsPanel(this, novFormProperties, m_bIsSTOP);
		//TCDECREL-2064:	change the dimension value for alignment
		JPanel innerTargetPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		innerTargetPanel.add("1.1.center.center", targetsPanel);	
		TitledBorder titleTarget = new TitledBorder(registry.getString("AffectedSTOP.LBL"));
		titleTarget.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		innerTargetPanel.setBorder(titleTarget);
		innerTargetPanel.setPreferredSize(new Dimension(650, 180));
		
		JPanel topPanel1 = new JPanel(new PropertyLayout());
		topPanel1.add("1.1.center.center", /*targetsPanel*/innerTargetPanel);
		//TitledBorder titlebrder = new TitledBorder(registry.getString("AffectedSTOP.LBL"));
		//titlebrder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		//topPanel1.setBorder(titlebrder);
		topPanel1.setBorder(new TitledBorder(""));
		topPanel1.setPreferredSize(new Dimension(665, 190));
		
		
		JPanel mainPanel=new JPanel(new GridBagLayout());		
		mainPanel.setAutoscrolls(true);
//		JPanel topPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
//		topPanel.add("1.1.center.top", targetsPanel);
//		JPanel topPanel=new JPanel(new BorderLayout());
//		topPanel.setPreferredSize(new Dimension(650,180));
//		topPanel.add(targetsPanel, BorderLayout.CENTER);
		
		
//		JPanel bottomPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
//		bottomPanel.add("1.1.center.top",stopPanel1);
		
		reasonForStopPanel = new JPanel();	
		stopReason = (iTextArea)formProps.getProperty("stopreason");
		stopReason.setLineWrap(true);         //TCDECREL-3149
        stopReason.setWrapStyleWord(true);     //TCDECREL-3149
		stopReason.setLengthLimit(1900);
		stopReason.setRequired(true);
		stopReason.setEnabled( !isReadOnly );
		
		JScrollPane stopreasonPane = new JScrollPane(stopReason);
		stopreasonPane.setPreferredSize(new Dimension(620,120));
		
		stopReasonTitle = new TitledBorder(registry.getString("Reason4Stop.MSG"));
		stopReasonTitle.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		reasonForStopPanel.setBorder(stopReasonTitle);
		reasonForStopPanel.add(stopreasonPane);
		reasonForStopPanel.setPreferredSize(new Dimension(640, 162));
		
		stopcommentPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		stopComment = (iTextArea)formProps.getProperty("stopcomment");
		stopComment.setLineWrap(true);            //TCDECREL-3149
        stopComment.setWrapStyleWord(true);       //TCDECREL-3149
		stopComment.setLengthLimit(1900);
		stopComment.setEnabled( isReadOnly );
		//stopComment.setRequired(true);
		JScrollPane stopCommentPane = new JScrollPane(stopComment);
		stopCommentPane.setPreferredSize(new Dimension(610,60));
		
		stopComment.setEnabled( !isReadOnly );
		
//		partRevised = (JRadioButton)formProps.getProperty("nov4_part_revised");
//		JPanel radioButtonsPanel2 = new JPanel(new PropertyLayout());
//		JRadioButton partRevisedButton=new JRadioButton(registry.getString("PartRevised.MSG"));
//		JRadioButton partNOTRevisedButton=new JRadioButton(registry.getString("PartNotRevised.MSG"));
//		
//		if(partRevised.isSelected())
//		{
//			partRevisedButton.setSelected(true);
//		}else
//		{
//			partNOTRevisedButton.setSelected(true);
//		}
//		
//		class RadioAction implements ActionListener {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				if(e.getActionCommand().equals(registry.getString("PartRevised.MSG"))){
//					partRevisedFlag=true;
//				}else
//				{
//					partRevisedFlag=false;
//				}
//			}
//		}
		
//		RadioAction radioAction=new RadioAction(); 
//		partRevisedButton.addActionListener(radioAction);
//		partNOTRevisedButton.addActionListener(radioAction);
//		
//		
//		ButtonGroup partRevisedGroup = new ButtonGroup();
//		partRevisedGroup.add(partRevisedButton);
//		partRevisedGroup.add(partNOTRevisedButton);
		NOIJLabel blank=new NOIJLabel();
//		
//		radioButtonsPanel2.add("1.1.top.center",blank);
//		radioButtonsPanel2.add("1.2.top.center",partRevisedButton);
//		radioButtonsPanel2.add("1.3.top.center",blank);
//		radioButtonsPanel2.add("1.4.top.center",partNOTRevisedButton);
		
		stopOrderTitle = new TitledBorder(registry.getString("STOPOrderRemove.MSG"));
		stopOrderTitle.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		stopcommentPanel.setBorder(stopOrderTitle);
		stopRemCombo = (iComboBox)formProps.getProperty("nov4_stopcomment_vals");
		stopRemCombo.setPreferredSize(new Dimension(150, 21));
		stopRemCombo.setMandatory(true);
		stopcommentPanel.add("1.1.left", stopRemCombo);
		stopcommentPanel.add("2.1.center.center",stopCommentPane);
		stopcommentPanel.setPreferredSize(new Dimension(640, 125));
		stopRemCombo.setEnabled( !isReadOnly );
		
		JPanel stopInstructionsPanel = new JPanel();
		stopInstruct = ( (iTextArea)formProps.getProperty("stopinstructions"));
		stopInstruct.setLineWrap(true);       //TCDECREL-3149
        stopInstruct.setWrapStyleWord(true);  //TCDECREL-3149
		stopInstruct.setLengthLimit(2000);
		JScrollPane stopInstructionPane = new JScrollPane(stopInstruct);
		stopInstruct.setEnabled( !isReadOnly );
		
		stopInstructionPane.setPreferredSize(new Dimension(615,120));
		TitledBorder tb2 = new TitledBorder(registry.getString("STOPInstruction.MSG"));
		tb2.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		stopInstructionsPanel.setBorder(tb2);
		stopInstructionsPanel.add(stopInstructionPane);
		
		//distPanel.setLayout(new PropertyLayout(5,5,5,5,5,5));
		distPanel = ((NOVEnDistributionPanel_v2)formProps.getProperty("distribution"));	
		//distPanel.setTitleBorderText(registry.getString("SelectDistribution.MSG"));
		distPanel.setControlState( isReadOnly );
		distPanel.setNewDimention(620, 215);
		distributionPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5)); //TCDECREL-2707
		distributionPanel.add("1.1.center.center", distPanel);
		// TCDECREL-2064: Added  mandatory star at the title of distribution panel
		distTitleBorder = new TitledBorder(registry.getString("SelectDistribution.MSG")) //TCDECREL-2707
		{
			int Justification = getTitleJustification();
			public void paintBorder(Component component, Graphics g, int i,
					int j, int k, int l) 
			{
				// TODO Auto-generated method stub
				super.paintBorder(component, g, i, j, k, l);
	
				g.setColor(Color.red);
				int x = Justification*5+(getTitle().length())*5;
				byte byte0 = 4;
				byte byte1 = 3;
				byte byte2 = 2;
				g.drawLine(x - byte1, byte0, x + byte1, byte0);
				g.drawLine(x, byte0 - byte1, x, byte0 + byte1);
				g.drawLine(x - byte2, byte0 - byte2, x + byte2, byte0 + byte2);
				g.drawLine(x - byte2, byte0 + byte2, x + byte2, byte0 - byte2);
	
			}
		};
		distTitleBorder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		distributionPanel.setBorder(distTitleBorder);
		distributionPanel.setPreferredSize(new Dimension(640, 248));
		
		//distPanel.setPreferredSize(new Dimension(650, 220));
		
		JPanel stopDispositionPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));	
		TitledBorder titleBord = new TitledBorder(registry.getString("STOPDisposition.MSG"))
		{
			int Justification = getTitleJustification();
			public void paintBorder(Component component, Graphics g, int i,
					int j, int k, int l) 
			{
				// TODO Auto-generated method stub
				super.paintBorder(component, g, i, j, k, l);

				g.setColor(Color.red);
				int x = Justification*4+(getTitle().length())*6;
				byte byte0 = 4;
				byte byte1 = 3;
				byte byte2 = 2;
				g.drawLine(x - byte1, byte0, x + byte1, byte0);
				g.drawLine(x, byte0 - byte1, x, byte0 + byte1);
				g.drawLine(x - byte2, byte0 - byte2, x + byte2, byte0 + byte2);
				g.drawLine(x - byte2, byte0 + byte2, x + byte2, byte0 - byte2);

			}
		};

		titleBord.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		stopDispTitle = new TitledBorder(titleBord);
		//stopDispTitle.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		//stopDispTitle.setTitleJustification(0);
		
		
//		TitledBorder mandTitle = new TitledBorder("*");
//		mandTitle.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
//		mandTitle.setTitleJustification(2);
//		mandTitle.setTitleColor(Color.RED);
//		CompoundBorder newBorder = new CompoundBorder(mandTitle, stopDispTitle); 
//		stopDispositionPanel.setBorder(newBorder);
		JLabel lbl = new JLabel("TEST");
		stopDispositionPanel.setBorder(stopDispTitle);
		stopDispositionPanel.setPreferredSize(new Dimension(640, 70));
		
//		NOIJLabel mandLabel=new NOIJLabel();
//		  mandLabel.setText("*");
//		  mandLabel.setForeground(Color.RED);
//		  mandLabel.setFont(new Font("Dialog", java.awt.Font.BOLD, 15));
//		  mandPanel.setPreferredSize(new Dimension(600, 10));
//		  mandPanel.add(mandLabel, BorderLayout.LINE_END);
//		  stopDispositionPanel.add("1.1.right.top", mandPanel);

	 	JPanel radioButtonsPanel = new JPanel(new PropertyLayout());  
		  stopWOnPO = (JRadioButton)formProps.getProperty("stopwoandpo");
		  stopWOnPO.setText(registry.getString("STOPWOnPO.MSG"));
		  stopShipment = (JRadioButton)formProps.getProperty("stopshipment");
		  stopShipment.setText(registry.getString("STOPShipment.MSG"));
		  stopAll = (JRadioButton)formProps.getProperty("nov4_stopall");
		 // stopAll = (JRadioButton)formProps.getProperty("stopwip");
		  stopAll.setText(registry.getString("STOPAll.MSG"));
		  
		  ButtonGroup stopDispositionGroup = new ButtonGroup();
		  stopDispositionGroup.add(stopWOnPO);
		  stopDispositionGroup.add(stopShipment);
		  stopDispositionGroup.add(stopAll);
		  
		  
		  stopWOnPO.setEnabled( !isReadOnly );
		  stopShipment.setEnabled( !isReadOnly );
		  stopAll.setEnabled( !isReadOnly );
		  		  
		  radioButtonsPanel.add("1.1.center.center",stopWOnPO);
		 // radioButtonsPanel.add("1.2.left.center",blank);
		  //radioButtonsPanel.add("1.2.left.center",blank);
		  radioButtonsPanel.add("1.2.center.center",blank);
		  radioButtonsPanel.add("1.3.center.center",stopShipment);
		  radioButtonsPanel.add("1.4.center.center",blank);
		 // radioButtonsPanel.add("1.7.left.center",blank);
		  //radioButtonsPanel.add("1.6.left.center",blank);
		  radioButtonsPanel.add("1.5.center.center",stopAll);
		  radioButtonsPanel.setPreferredSize(new Dimension(620, 30));
//		  NOIJLabel mandLabel=new NOIJLabel();
//		  mandLabel.setText("*");
//		  mandLabel.setForeground(Color.RED);
//		  mandLabel.setFont(new Font("Dialog", java.awt.Font.BOLD, 15));
		  //radioButtonsPanel.add("1.10.left.center",mandLabel);
		 
		
		radioButtonsPanel.setVisible(true);
	    
	    stopDispositionPanel.add("1.1.center.center",radioButtonsPanel);
	    //stopDispositionPanel.add("2.1.left.center",radioButtonsPanel);
	    
	    JPanel datePanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		TitledBorder dateTitle = new TitledBorder("");
		tb2.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		datePanel.setBorder(dateTitle);
		JPanel dateCompPanel = new JPanel(new PropertyLayout());
		expectedResolutionDate = ((NOVDateButton)formProps.getProperty("nov4_expected_resolution"));
		expectedResolutionDate.setEnabled(true);
		expectedResolutionDate.setMandatory(true);
		expectedResolutionDate.setPreferredSize(new Dimension(250,21));
		
		expectedResolutionDate.setEnabled( !isReadOnly );
		
		dateLbl = new NOIJLabel(registry.getString("ExpectedResolutionDate.LABLE"));
		dateCompPanel.add("1.1.center.center",dateLbl);
		dateCompPanel.add("1.2.center.center",expectedResolutionDate);
		datePanel.add("1.1.center.center",dateCompPanel);
		datePanel.setPreferredSize( new Dimension(635,40));

		PrintExternalAttachmentsPanel_v2 printExtPanel = new PrintExternalAttachmentsPanel_v2(
				formProps, false, this, registry.getString( "PrintExternalAttachmentsPanel.Title2" ) );
		printExtPanel.setControlState( isReadOnly );
		printExtPanel.setPreferredSize(new Dimension( 640, 160 ));
		printExtPanel.tablePane.setPreferredSize(new Dimension(610,90));
		
		JPanel	lastPanel	= new JPanel(new PropertyLayout(5,5,5,5,5,5));
		ExternalAttachmentsPanel_v2 extPanel = new ExternalAttachmentsPanel_v2(formProps, false, this );
		extPanel.setControlState( isReadOnly );
		extPanel.setPreferredSize( new Dimension( 640, 160 ) );
		extPanel.tablePane.setPreferredSize(new Dimension(610,90));
		lastPanel.add("1.1.center.center", printExtPanel);
		lastPanel.add("2.1.center.center", extPanel);
		lastPanel.setPreferredSize(new Dimension(665, 350));
		lastPanel.setBorder(new TitledBorder(""));
		
		JPanel bottomPanel1=new JPanel(new PropertyLayout(5,5,5,5,5,5));
		
		if(m_bIsSTOP)
		{
			bottomPanel1.setPreferredSize(new Dimension(665, 720));
			bottomPanel1.add("1.1.center.center",reasonForStopPanel);
			bottomPanel1.add("2.1.center.center",stopDispositionPanel);
			bottomPanel1.add("3.1.center.center",datePanel);
			bottomPanel1.add("4.1.center.center",stopInstructionsPanel);
			//bottomPanel1.add("5.1.left.center",((NOVEnGenSearchPanel)formProps.getProperty("projects")));
			bottomPanel1.add("5.1.center.center",distributionPanel);
			if(m_bIsCanelSTOP)
			{
				bottomPanel1.setPreferredSize(new Dimension(665, 850));
				bottomPanel1.add("6.1.center.center", stopcommentPanel);
			}
		}
		else if(m_bIsCanelSTOP)
		{
			bottomPanel1.setPreferredSize(new Dimension(665, 400));
			//bottomPanel1.add("1.1.center.center",distPanel);
			bottomPanel1.add("1.1.center.center",distributionPanel);
			bottomPanel1.add("2.1.center.center", stopcommentPanel);
		}

		bottomPanel1.setBorder(new TitledBorder(""));
		
		//Main panel
		GridLayout gridLayout = new GridLayout(10, 1){
			public Dimension preferredLayoutSize(Container parent) {				
			    synchronized (parent.getTreeLock()) {
			      Insets insets = parent.getInsets();
			      int ncomponents = parent.getComponentCount();
			      int nrows = getRows();
			      int ncols = getColumns();
			      if (nrows > 0) {
			        ncols = (ncomponents + nrows - 1) / nrows;
			      } 
			      else {
			        nrows = (ncomponents + ncols - 1) / ncols;
			      }
			      int[] w = new int[ncols];
			      int[] h = new int[nrows];
			      for (int i = 0; i < ncomponents; i ++) {
			        int r = i / ncols;
			        int c = i % ncols;
			        Component comp = parent.getComponent(i);
			        Dimension d = comp.getPreferredSize();
			        if (w[c] < d.width) {
			          w[c] = d.width;
			        }
			        if (h[r] < d.height) {
			          h[r] = d.height;
			        }
			      }
			      int nw = 0;
			      for (int j = 0; j < ncols; j ++) {
			        nw += w[j];
			      }
			      int nh = 0;
			      for (int i = 0; i < nrows; i ++) {
			        nh += h[i];
			      }
			      return new Dimension(insets.left + insets.right + nw + (ncols-1)*getHgap(), 
			          insets.top + insets.bottom + nh + (nrows-1)*getVgap());
			    }
			  }

			  public Dimension minimumLayoutSize(Container parent) {				
			    synchronized (parent.getTreeLock()) {
			      Insets insets = parent.getInsets();
			      int ncomponents = parent.getComponentCount();
			      int nrows = getRows();
			      int ncols = getColumns();
			      if (nrows > 0) {
			        ncols = (ncomponents + nrows - 1) / nrows;
			      } 
			      else {
			        nrows = (ncomponents + ncols - 1) / ncols;
			      }
			      int[] w = new int[ncols];
			      int[] h = new int[nrows];
			      for (int i = 0; i < ncomponents; i ++) {
			        int r = i / ncols;
			        int c = i % ncols;
			        Component comp = parent.getComponent(i);
			        Dimension d = comp.getMinimumSize();
			        if (w[c] < d.width) {
			          w[c] = d.width;
			        }
			        if (h[r] < d.height) {
			          h[r] = d.height;
			        }
			      }
			      int nw = 0;
			      for (int j = 0; j < ncols; j ++) {
			        nw += w[j];
			      }
			      int nh = 0;
			      for (int i = 0; i < nrows; i ++) {
			        nh += h[i];
			      }
			      return new Dimension(insets.left + insets.right + nw + (ncols-1)*getHgap(), 
			          insets.top + insets.bottom + nh + (nrows-1)*getVgap());
			    }
			  }

			  public void layoutContainer(Container parent) {			   
			    synchronized (parent.getTreeLock()) {
			      Insets insets = parent.getInsets();
			      int ncomponents = parent.getComponentCount();
			      int nrows = getRows();
			      int ncols = getColumns();
			      if (ncomponents == 0) {
			        return;
			      }
			      if (nrows > 0) {
			        ncols = (ncomponents + nrows - 1) / nrows;
			      } 
			      else {
			        nrows = (ncomponents + ncols - 1) / ncols;
			      }
			      int hgap = getHgap();
			      int vgap = getVgap();
				  // scaling factors      
			      Dimension pd = preferredLayoutSize(parent);
			      double sw = (1.0 * parent.getWidth()) / pd.width;
			      double sh = (1.0 * parent.getHeight()) / pd.height;
			      // scale
			      int[] w = new int[ncols];
			      int[] h = new int[nrows];
			      for (int i = 0; i < ncomponents; i ++) {
			        int r = i / ncols;
			        int c = i % ncols;
			        Component comp = parent.getComponent(i);
			        Dimension d = comp.getPreferredSize();
			        d.width = (int) (sw * d.width);
			        d.height = (int) (sh * d.height);
			        if (w[c] < d.width) {
			          w[c] = d.width;
			        }
			        if (h[r] < d.height) {
			          h[r] = d.height;
			        }
			      }
			      for (int c = 0, x = insets.left; c < ncols; c ++) {
			        for (int r = 0, y = insets.top; r < nrows; r ++) {
			          int i = r * ncols + c;
			          if (i < ncomponents) {
			            parent.getComponent(i).setBounds(x, y, w[c], h[r]);
			          }
			          y += h[r] + vgap;
			        }
			        x += w[c] + hgap;
			      }
			    }
			  }  
		};	
		mainPanel.setLayout(gridLayout);
			
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(stopPanel1/*bottomPanel*/, gridBagConstraints);
		
	    gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(topPanel1, gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(bottomPanel1, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 3;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(lastPanel, gridBagConstraints);
		//mainPanel.add(printExtPanel, gridBagConstraints);

//		gridBagConstraints = new GridBagConstraints();
//		gridBagConstraints.anchor = GridBagConstraints.CENTER;
//		gridBagConstraints.gridx = 0;
//		gridBagConstraints.gridy = 4;
//		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
//		mainPanel.add(extPanel, gridBagConstraints);

//		JScrollPane scrollpane=new JScrollPane(mainPanel);
//		scrollpane.setPreferredSize(new Dimension(660, 1600));
//		add(scrollpane);
		
		add(mainPanel);
	}
	
	
	public void actionPerformed(ActionEvent e) {}
	
	public HashMap<TCComponent, String> getStopCurrentStatus()
	{
		return currentStatMap;
	}
	public Vector<TCComponent> getEnTargets()
	{
		return stopTargets;
	}
	public TCComponentProcess getProcess()
	{
		return currentProcess;
	}
	public void loadEnTargets()
	{
		try
		{
			//EN Targets;
			AIFComponentContext[] contexts=stopform.whereReferenced();		
			Vector<String> vtargetIds=new Vector<String>();
			Vector<String> vtargetRevIds=new Vector<String>();
			currentStatMap.clear();
			if(contexts!=null)
			{
				TCComponent[] targets = null;
				for(int j=0;j<contexts.length;j++)
				{
					if(((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess) /*&& (contexts[j].getComponent().toString().startsWith(registry.getString("STOPProcess.MSG")) )*/)
					{
						currentProcess=(TCComponentProcess)contexts[j].getComponent();
						TCComponentTask rootTask=((TCComponentProcess)currentProcess).getRootTask();
						if((rootTask.toString().contains(registry.getString("STOPProcess.MSG"))) || (rootTask.toString().contains(registry.getString("STOPCancelProcess.MSG"))))
						{
							targets =rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
							for(int i=0;i<targets.length;i++)
							{
								//if(targets[i] instanceof TCComponentItemRevision)
								if(targets[i] instanceof TCComponentItem )
								{
									stopTargets.add(targets[i]);
									TCComponentItem item = (TCComponentItem)targets[i];
									vtargetIds.add(item.getProperty("item_id"));
									TCComponentItemRevision itemRev=item.getLatestItemRevision();
									//vtargetIds.add(((TCComponentItemRevision)targets[i]).getProperty("item_id"));
									vtargetRevIds.add(itemRev.getProperty("current_revision_id"));
									//vtargetRevIds.add(((TCComponentItemRevision)targets[i]).getProperty("current_revision_id"));
								}
							}	
							break;
						}
					}
				}
				if(targets.length>0 && ((referncedispdata!=null && referncedispdata.length==0) || (referncedispdata==null)) )
				{
					String[] targetIds = new String[vtargetIds.size()];
					vtargetIds.copyInto(targetIds);
					String []  targetRevIds=new String[vtargetRevIds.size()];
					vtargetRevIds.copyInto(targetRevIds);
					referncedispdata = createDispInstances(targets);
					// Save the Form
					saveDipsDatatoForm();
					Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));
					for (int k = 0; k < stopTargets.size(); k++) 
					{
						for (int j = 0; j < tempReference.size(); j++) 
						{
							String targetId = (tempReference.get(j).getTCProperty("targetitemid")).getStringValue();
//							String itemId = ((TCComponentItemRevision) stopTargets.get(k)).getProperty("item_id");
							String itemId = ((TCComponentItem) stopTargets.get(k)).getProperty("item_id");
							if (targetId.equalsIgnoreCase(itemId)) 
							{
								targetdispMap.put(stopTargets.get(k),tempReference.get(j));
								currentStatMap.put(stopTargets.get(k), tempReference.get(j).getTCProperty("currentstatus").getStringValue());
								break;
							}
						}
					}
				}
				else if(referncedispdata.length!=0)
				{
					Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
					for(int k=0;k<vtargetIds.size();k++)
					{
						for(int j=0;j<tempReference.size();j++)
						{
							String targetId=(tempReference.get(j).getTCProperty("targetitemid")).getStringValue();
							//String itemId=((TCComponentItemRevision)stopTargets.get(k)).getProperty("item_id");
							String itemId=((TCComponentItem)stopTargets.get(k)).getProperty("item_id");
							if(targetId.equalsIgnoreCase(itemId))
							{
								targetdispMap.put(tempReference.get(j),stopTargets.get(k));
								currentStatMap.put(stopTargets.get(k), tempReference.get(j).getTCProperty("currentstatus").getStringValue());
								break;
								//tempReference.remove(j);
								//set the future status of Table data
							}

						}
					}
				}
			}		
			//This check is added - after completing STOP Process we are deleting Items from Target 
			//So for Form display we need to get hold of Items to which STOPForm is Referenced...
			//In above loop we are checking for targets attached to Process here we are checking for whereReferenced on Forms...
			if(contexts!=null && stopTargets.size()==0)
			{
				
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentItemRevision)
					{
								stopTargets.add((TCComponent)contexts[j].getComponent());
								vtargetIds.add(((TCComponentItemRevision)contexts[j].getComponent()).getProperty("item_id"));
					}
				}
				System.out.println("referncedispdata.length: "+referncedispdata.length);
				if(referncedispdata.length!=0)
				{
					Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
					for(int k=0;k<vtargetIds.size();k++)
					{
						for(int j=0;j<stopTargets.size();j++)
						{
							String targetId=(tempReference.get(j).getTCProperty("targetitemid")).getStringValue();
							//String itemId=((TCComponentItemRevision)stopTargets.get(k)).getProperty("item_id");
							String itemId=((TCComponentItem)stopTargets.get(k)).getProperty("item_id");
							if(targetId.equalsIgnoreCase(itemId))
							{
								targetdispMap.put(tempReference.get(j),stopTargets.get(k));
								//disptargetMap.put(vtargetIds.get(k), tempReference.get(j));
								currentStatMap.put(stopTargets.get(k), tempReference.get(j).getTCProperty("currentstatus").getStringValue());
								//tempReference.remove(j);
								//set the future status of Table data
							}
						}
					}
				
				}
			}

		}catch(Exception e)
		{
			System.out.println("Exception is:" + e);
			e.printStackTrace();
		}		

	}
	
	public TCComponent[] createDispInstances(TCComponent[] targetItems)
      {
        TCComponent[] comps=null;
          /*    try
            {
                  //call userExit to create the Disposition componenets and return the array
                  TCUserService us = getSession().getUserService();           
                  Object obj[]=new Object[3];         
                  obj[0]=itemIds;
                  obj[1]=itemRevIds;
                  obj[2] = new Boolean(false);
                  comps=(TCComponent[])us.call("NATOIL_createEnDispositionData", obj);
                  
                  if (comps!=null && comps.length >0) 
                  {
	                  if (comps!=null && comps.length >0) 
	                  {
	                        for (int i = 0; i < comps.length; i++) 
	                        {
	                              comps[i].getTCProperty("futurestatus").setStringValue("STOP");
	                        }
	                  }
                  }
            }catch(TCException te)
            {

            }
            return comps;
            
            */
		
		try
		{
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"release_status_list"};
	    	
			Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
	    	
	    	nOVDataManagementServiceHelper.getObjectsProperties(targetItems, attrList, null);
	    	
			for(int i = 0; i < targetItems.length ; i++)
			{
			    CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
			    		"targetdisposition", CreateInObjectHelper.OPERATION_CREATE);		    	
				String item_id = targetItems[i].getProperty("item_id");
				String revision_id = ((TCComponentItem)targetItems[i]).getLatestItemRevision().getProperty("current_revision_id");
				String statusName = targetItems[i].getProperty("release_status_list");
				
				formPropertyMap.setString("targetitemid", item_id);
				formPropertyMap.setString("rev_id", revision_id);
			

				formPropertyMap.setString("currentstatus", statusName);
				formPropertyMap.setString("futurestatus", "STOP");
				//set empty properties
				
				formPropertyMap.setString("changedesc","");
				formPropertyMap.setString("dispinstruction","");
				formPropertyMap.setString("inprocess","");
				formPropertyMap.setString("ininventory","");
				formPropertyMap.setString("assembled","");
				formPropertyMap.setString("infield","");
				
				vecCreateInObjectHelper.add(formPropertyMap);
	   
			}
			
	        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
	                .toArray(new CreateInObjectHelper[vecCreateInObjectHelper.size()]);
			
			//create objects
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		    Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();
		    
		    for(int j = 0; j<createInObjectHelper.length ; j++)
		    {
		    	TCComponent created = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
		    	createdorUpdatedObjs.add(created);
		    }
		   
		    comps = (TCComponent[]) createdorUpdatedObjs.toArray(new TCComponent[createdorUpdatedObjs.size()]);
			
		}
		catch(TCException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comps;			
      }

	public boolean checkCompulsoryFields() {
		boolean error = false;
		String errorString="";
		Font errorFont = new Font("Tahoma", Font.BOLD, 12);
		Font normalFont = new Font("Dialog", java.awt.Font.BOLD, 12);
		
		if( m_bIsSTOP == true && m_bIsCanelSTOP == false)
		{
			if(null==stopReason.getText() ||(null!=stopReason.getText() && stopReason.getText().trim().length()==0) )
			{
				error=true;
				errorString=registry.getString("Reason4Stop.MSG");
				stopReasonTitle.setTitle(errorString+" is mandatory field.");
				stopReasonTitle.setTitleFont(errorFont);
				stopReasonTitle.setTitleColor(Color.RED);
				reasonForStopPanel.repaint();//2064
			}else
			{
				// i think I should change here
				stopReasonTitle.setTitle((registry.getString("Reason4Stop.MSG")));
				stopReasonTitle.setTitleFont(normalFont);
				stopReasonTitle.setTitleColor(Color.BLUE);
				reasonForStopPanel.repaint();//2064
			}
			if(null==expectedResolutionDate.getDate())
			{
				error=true;
				errorString=registry.getString("ExpectedResolutionDate.LABLE");
				dateLbl.setText(errorString+" is mandatory field.");
				dateLbl.setForeground(Color.RED);
				dateLbl.setFont(errorFont);
			}
			else
			{
				dateLbl.setText(registry.getString("ExpectedResolutionDate.LABLE"));
				dateLbl.setForeground(Color.BLUE);
				dateLbl.setFont(normalFont);
			}
			if(!stopWOnPO.isSelected() && !stopShipment.isSelected() && !stopAll.isSelected()){
				error=true;
				errorString=registry.getString("STOPDisposition.MSG");
				stopDispTitle.setTitle(errorString+" is mandatory field.");
				stopDispTitle.setTitleFont(errorFont);
				stopDispTitle.setTitleColor(Color.RED);
			}
			//usha- 2064
//			else
//			{
//				stopDispTitle.setTitle(registry.getString("STOPDisposition.MSG"));
//				stopDispTitle.setTitleFont(normalFont);
//				stopDispTitle.setTitleColor(Color.BLUE);
//			}
		}
				
		if(distPanel.getAllTargetListObjects().length==0)
		{
			error=true;
			errorString=registry.getString("SelectDistribution.MSG");
			//TCDECREL-2707:Start
			/*distPanel.setTitleBorderText(errorString+" is mandatory field.");
			Border border=distPanel.getBorder();
			if(border instanceof TitledBorder)
			{
				TitledBorder tb=(TitledBorder)border;
				tb.setTitleFont(errorFont);
				tb.setTitleColor(Color.RED);
			}*/
			TitledBorder disttb = new TitledBorder(errorString+" is mandatory field.");
			disttb.setTitleFont(errorFont);
			disttb.setTitleColor(Color.RED);
			distributionPanel.setBorder(disttb );
			//TCDECREL-2707:End
		}
		else
		{
			//TCDECREL-2707:Start
			/*distPanel.setTitleBorderText(registry.getString("SelectDistribution.MSG"));
			distPanel.setForeground(Color.BLUE);
			distPanel.setFont(normalFont);*/
			distributionPanel.setBorder(distTitleBorder);
			//TCDECREL-2707:End
		}
		if( m_bIsCanelSTOP == true && m_bIsSTOP == false)
		{			
			String stopRemTxt = stopRemCombo.getTextField().getText();
			Vector vecRem = new Vector();
			Collections.addAll(vecRem, stopRemCombo.getItems());
			//if (null==stopComment.getText() ||(null!=stopComment.getText() && stopComment.getText().trim().length()==0)){\
			//if ( !(stopRemCombo.getSelectedIndex()>0) )
			if ( !(vecRem.indexOf(stopRemTxt) > 0))
			{
				error=true;
				errorString=registry.getString("STOPOrderRemove.MSG");
				stopOrderTitle.setTitle(errorString+" is mandatory field.");
				stopOrderTitle.setTitleFont(errorFont);
				stopOrderTitle.setTitleColor(Color.RED);
			}
			//else if(stopRemCombo.getSelectedIndex() == 1){
			else if(vecRem.indexOf(stopRemTxt) == 1){
				stopOrderTitle.setTitle(registry.getString("STOPOrderRemove.MSG"));
				stopOrderTitle.setTitleFont(normalFont);
				stopOrderTitle.setTitleColor(Color.BLUE);				
			}
			else{
				if (null==stopComment.getText() ||(null!=stopComment.getText() && stopComment.getText().trim().length()==0)){
					error=true;
					errorString=registry.getString("STOPOrderRemove.MSG");
					stopOrderTitle.setTitle(errorString+" is mandatory field.");
					stopOrderTitle.setTitleFont(errorFont);
					stopOrderTitle.setTitleColor(Color.RED);
				}
				else{
					stopOrderTitle.setTitle(registry.getString("STOPOrderRemove.MSG"));
					stopOrderTitle.setTitleFont(normalFont);
					stopOrderTitle.setTitleColor(Color.BLUE);
				}
			}
			stopcommentPanel.repaint(); //TCDECREL-2707
		}
		if(error)
		{
			JOptionPane.showMessageDialog(targetsPanel.getRootPane(), registry.getString("FillMandatoryFields.MSG"), "Cannot Proceed!", JOptionPane.ERROR_MESSAGE);			
			return false;
		}
			
		return true;
	}

	private void checkSupersedeENFormStatus()
	{
		try
		{
			TCComponent[] relStatusList = ( novFormProperties.getForm().getTCProperty( "release_status_list" ).getReferenceValueArray() );
			String isCheckedOut = novFormProperties.getForm().getTCProperty( "checked_out" ).getStringValue();

			String processStageList = novFormProperties.getForm().getProperty( "process_stage_list" );
			String[] processStageListArr = processStageList.split( "," );
			String reqdProcessStage =  registry.getString( "ProcessStage.Stage" );
			
			if( isCheckedOut.trim().length() == 0 && processStageListArr.length > 1 &&
					!processStageListArr[1].trim().startsWith( reqdProcessStage ) )
				isReadOnly = true;
			else if( isCheckedOut.trim().length() == 0 && processStageListArr.length <= 1 )
				isReadOnly = true;
			
			for( int index = 0; index < relStatusList.length; ++index )
			{
				if( relStatusList[index].getProperty( "name" ).equalsIgnoreCase( registry.getString( "ENReleaseStatus.STATUS" ) ) )
					isReadOnly = true;
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
	}

}
