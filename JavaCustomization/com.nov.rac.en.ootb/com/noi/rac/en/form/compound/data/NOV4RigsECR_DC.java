/** \file 
    \brief This file contains a class NOV4_RIGSEngChangeRequest_DC which works as a Model(data) in MVC pattern.
*/

package com.noi.rac.en.form.compound.data;

import javax.swing.JTextField;

import com.noi.rac.en.form.compound.component.NOV4ECRDistributionPanel;
import com.noi.rac.en.form.compound.component.NOV4_RIGSEcrAffectedDocPartsPanel;
import com.noi.rac.en.form.compound.util.NOVCustomFormProperties;
import com.noi.rac.en.form.compound.util.NOVTextArea;
import com.noi.rac.en.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVEnDispositionPanel_v2;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;

/** \brief Declaration for the class NOV4_RIGSEngChangeRequest_DC, it works as Model(data) in MVC pattern.
 		   Creation of the Form is done using MVC pattern and this class hold all the data that is required for
     	   displaying. This class will also take care of populating the components present in the UI and binds the
     	   panels and components to the attributes(properties).	
*/
public class NOV4RigsECR_DC extends NOVCustomFormProperties 
{
	/// This specifies the ECR Number.
	public JTextField ecrNumber;
	
	/// This specifies name of the ECR requester.
	public JTextField ecrRequester;
	
	/// This specifies the date on which the ECR is requested.
	public NOVDateButton ecrRequestDate;
	
	/// This specifies status of the ECR it can be Waiting for Approval, Pending, Change in Process, and Closed.
	public JTextField ecrStatus;
	
	/// This specifies name of the ECR requestor's department.
	public JTextField ecrRequestorDept;
	
	/// This specifies the date on which the ECR is closed.
	public NOVDateButton ecrCloseDate;
	
	/** \brief When the ECR status is �Change In Process�, this field should be left blank. 
	 		   When the ECR is at �Closed� status, the close disposition of the ECR should be
	 		   populated as �withdrawn� if initiator withdrew the request in ALC; or 
	 		   �Rejected� if the request was rejected by the engineering approver or additional approver in ALC
	 */
	public JTextField ecrCloseDisposition;	
	
	public JTextField ecrChangeResp;	
	
	/** \brief This panel holds all the disposition components.
	   		   This panel a\is also responsible for creating, adding, removing and setting the data for Disposition Tab Panels.		
	*/	
	public NOVEnDispositionPanel_v2 ecrDispositionDataComp;
	
	public NOV4_RIGSEcrAffectedDocPartsPanel ecrAffectedPartsDocPanel;
	
	/// This specifies the Product name or keywords of the product name/model if available.
	public JTextField productModel;	
	
	/// This specifies the document ID, name, and description if its specified by the initiator of the ECR.
	public JTextField relatedDocuments;	
	
	/// This specifies the Project/Serial number if specified by initiator.
	public JTextField projectSerialNumber;	
	
	/// This specifies the customer name.
	public JTextField customer;	
	
	/// This specifies name of the RIG.
	public JTextField rigName;	
	
	/// This specifies the Engineering task priority.
	public JTextField priority;	
	
	/// This specifies the reason for the change request.	
	public JTextField reasonForChange;
	
	/// This specifies the explanation for the reason of the change request.	
	public NOVTextArea explanation;
	
	/// This specifies description for this change requester.	
	public NOVTextArea changeDescription;
	
	/// This specifies Approval group for this ECR.	
	public JTextField approvalGroup;
	
	/// This specifies Name of the Engineering Approver for this ECR.	
	public JTextField engApprover;
	
	/// This specifies ECR approve date by the Engineering Approver.	
	public NOVDateButton engApproveDate ;
	
	/// This specifies the name of the additional approver if available for this ECR.	
	public JTextField additionalApprover;
	
	/// This specifies ECR additional approve date by the Additional Approver.	
	public NOVDateButton  additionalApproveDate;
	
	/// This specifies comments for this change requester.	
	public NOVTextArea comments;
	
	/// This specifies the distributions from the ALC.	
	NOV4ECRDistributionPanel distributionPanel;
	
	AbstractReferenceLOVList distributionLOVList;	
	
	/// This specifies comments for this change requester.	
	AbstractReferenceLOVList referenceLOVList;	
	
	private Registry registry ;
	 
	/// Array of String containing the names of the all properties( attributes ) of the Form.
	String[] propertyNames = new String[] { "object_name", "nov4_ecr_requester", "nov4_request_date",
			"release_status_list", "nov4_requester_dept", "nov4_close_date", "nov4_close_disposition",
			"nov4_change_resp", "nov4_ecr_affected_parts", "nov4_product_model", "nov4_related_doc",
			"nov4_project_number", "nov4_customer", "nov4_rig_name", "nov4_priority", "nov4_reason_change",
			"nov4_explanation", "nov4_chage_desc", "nov4_approval_group", "nov4_eng_approver",
			"nov4_eng_approve_date", "nov4_additional_approver", "nov4_addtional_approve_date",
			"nov4_comments", "nov4_distribution" };

	/** \brief Creates the Instance of the NOV4_RIGSEngChangeRequest_DC.
	      	   It creates the controls for the required properties and adds the properties to the form.		
	*/
	public NOV4RigsECR_DC()
	{
		ecrNumber = new JTextField();
		ecrRequester = new JTextField();
		ecrRequestDate = new NOVDateButton();
		ecrRequestDate.setDisplayFormat( "d-MMM-yyyy" );
		ecrStatus = new JTextField();
		ecrRequestorDept = new JTextField();
		ecrCloseDate = new NOVDateButton();
		ecrCloseDate.setDisplayFormat( "d-MMM-yyyy" );
		ecrCloseDisposition = new JTextField();
		ecrChangeResp = new JTextField();
		
		productModel = new JTextField();
		relatedDocuments = new JTextField();
		projectSerialNumber = new JTextField();
		customer = new JTextField();
		rigName = new JTextField();
		priority = new JTextField();
		
		reasonForChange = new JTextField();
		explanation = new NOVTextArea();
		changeDescription = new NOVTextArea();
		
		approvalGroup = new JTextField();
		engApprover = new JTextField();
		engApproveDate = new NOVDateButton();
		engApproveDate.setDisplayFormat( "d-MMM-yyyy" );	
		additionalApprover = new JTextField();
		additionalApproveDate = new NOVDateButton();
		additionalApproveDate.setDisplayFormat( "d-MMM-yyyy" );	
		
		comments = new NOVTextArea();
		registry = Registry.getRegistry(this);
	
		addProperty( "object_name", ecrNumber );
		addProperty( "nov4_ecr_requester", ecrRequester );
		addProperty( "nov4_request_date", ecrRequestDate );
		addProperty( "release_status_list", ecrStatus );
		addProperty( "nov4_requester_dept", ecrRequestorDept );
		addProperty( "nov4_close_date", ecrCloseDate );
		addProperty( "nov4_close_disposition", ecrCloseDisposition );
		addProperty( "nov4_change_resp", ecrChangeResp );
		
		addProperty( "nov4_product_model", productModel );
		addProperty( "nov4_related_doc", relatedDocuments );
		addProperty( "nov4_project_number", projectSerialNumber );
		addProperty( "nov4_customer", customer );
		addProperty( "nov4_rig_name", rigName );
		addProperty( "nov4_priority", priority );
		addProperty( "nov4_reason_change", reasonForChange );
		addProperty( "nov4_explanation", explanation );
		addProperty( "nov4_chage_desc", changeDescription );
		
		addProperty( "nov4_approval_group", approvalGroup );
		addProperty( "nov4_eng_approver", engApprover );
		addProperty( "nov4_eng_approve_date", engApproveDate );
		addProperty( "nov4_additional_approver", additionalApprover );
		addProperty( "nov4_addtional_approve_date", additionalApproveDate );
		
		addProperty( "nov4_comments", comments );
	}

	/** \brief Returns all the property names.
	   	\return Returns array of all the property names.		
	 */
	public String[] getPropertyNames()
	{
		return propertyNames; 
	}

	/** \brief Gets the properties for all the required panels for the forms and add the properties to the form.
	 	\param form The form that is required to be set.
	 */
	public void setForm( TCComponent form ) 
	{
		if (form!=null) 
		{
			try
			{
				super.setForm( form );	            
				//get the disposition data
				TCProperty targetdispProperty = form.getTCProperty( "nov4_ecr_affected_parts" );
				ecrDispositionDataComp = new NOVEnDispositionPanel_v2( targetdispProperty.getReferenceValueArray() );
			
				distributionPanel = new NOV4ECRDistributionPanel();
	
				// Code added for Distribution List -------
				String currentGroup = "";
				distributionLOVList = new AbstractReferenceLOVList();				
				TCComponentGroup group = form.getSession().getCurrentGroup();
				String groupname = group.getFullName();
				if( groupname.indexOf( '.' ) >= 0 )
					currentGroup = groupname.substring( 0, groupname.indexOf( '.' ) );
				else
					currentGroup = groupname;
				currentGroup = currentGroup + ":";
				//distributionLOVList.initClassECNERO( form.getSession(), "ImanAliasList", "alName", currentGroup );
				
				referenceLOVList = new AbstractReferenceLOVList();
	
				addProperty( "nov4_ecr_affected_parts", ecrDispositionDataComp );
				addProperty( "nov4_distribution", distributionPanel );
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}	
			super.getFormProperties( propertyNames );
		}
	}

	/** \brief Enables the component which are required to be enabled.
	 */	
	public void enableForm()
	{
		enableProperty( "nov4_ecr_affected_parts" );
	}
	
	public void clearForm() 
	{
	}

	/** \brief This method populates the properties of the form components.
	 */		
	public void populateFormComponents() 
	{
		if( getForm() == null )
			return;
		
		ecrNumber.setText( ( ( TCProperty)imanProperties.get( ecrNumber ) ).getStringValue() );
		ecrRequester.setText( ( ( TCProperty )imanProperties.get( ecrRequester ) ).getStringValue() );
		ecrRequestDate.setDate( ( ( TCProperty )imanProperties.get( ecrRequestDate ) ).getDateValue() );
		ecrRequestorDept.setText( ( ( TCProperty )imanProperties.get( ecrRequestorDept ) ).getStringValue() );
		ecrCloseDate.setDate( ( ( TCProperty )imanProperties.get( ecrCloseDate ) ).getDateValue() );
		ecrCloseDisposition.setText( ( ( TCProperty )imanProperties.get( ecrCloseDisposition ) ).getStringValue() );
		ecrChangeResp.setText( ( ( TCProperty )imanProperties.get( ecrChangeResp ) ).getStringValue() );		
				
		productModel.setText( ( ( TCProperty )imanProperties.get( productModel ) ).getStringValue() );
		projectSerialNumber.setText( ( ( TCProperty )imanProperties.get( projectSerialNumber ) ).getStringValue() );
		customer.setText( ( ( TCProperty )imanProperties.get( customer ) ).getStringValue() );
		rigName.setText( ( ( TCProperty )imanProperties.get( rigName ) ).getStringValue() );
		priority.setText( ( ( TCProperty )imanProperties.get( priority ) ).getStringValue() );

		reasonForChange.setText( ( ( TCProperty )imanProperties.get( reasonForChange ) ).getStringValue() );
		explanation.setText( ( ( TCProperty )imanProperties.get( explanation ) ).getStringValue() );
		//reasonForChangeCombo.setSelectedItem( ( ( TCProperty )imanProperties.get( reasonForChange ) ).getStringValue() );
		changeDescription.setText( ( ( TCProperty )imanProperties.get( changeDescription ) ).getStringValue() );
		
		approvalGroup.setText( ( ( TCProperty)imanProperties.get( approvalGroup ) ).getStringValue() );
		engApprover.setText( ( ( TCProperty )imanProperties.get( engApprover ) ).getStringValue() );
		engApproveDate.setDate( ( ( TCProperty )imanProperties.get( engApproveDate ) ).getDateValue() );
		additionalApprover.setText( ( ( TCProperty )imanProperties.get( additionalApprover ) ).getStringValue() );
		additionalApproveDate.setDate( ( ( TCProperty )imanProperties.get( additionalApproveDate ) ).getDateValue() );

		comments.setText( ( ( TCProperty )imanProperties.get( comments ) ).getStringValue() );			

		String[] relDocArr = ( ( TCProperty )imanProperties.get( relatedDocuments ) ).getStringValueArray();
		String relDocuments = "";
		
		for( int index = 0; index < relDocArr.length; ++index )
		{
			if( index == 0 )
			{
				relDocuments = relDocArr[index];
			}
			else
				relDocuments += "," + relDocArr[index];
		}
		
		relatedDocuments.setText( relDocuments );
		
		TCComponent[] releaseStatusList = ( ( TCProperty )imanProperties.get( ecrStatus ) ).getReferenceValueArray();
		if( releaseStatusList.length > 0 )
		{
			String relStatus = releaseStatusList[0].toString();
			if( relStatus.equalsIgnoreCase( registry.getString("RSCLOSED.ACTUALVAL") ) )
				ecrStatus.setText( registry.getString("RSCLOSED.DISPLAYVAL" ));
			else
				ecrStatus.setText( relStatus );
		}
		else
			ecrStatus.setText( registry.getString("RSCIP.DISPLAYVAL") );

		
		String[] arr1 = ( ( TCProperty )imanProperties.get( distributionPanel ) ).getStringValueArray();
		for( int index =0; index < arr1.length; index++)
		{
			distributionLOVList.addItem( arr1[index],arr1[index] );
			//referenceLOVList.addExclusion( arr1[index] );
		}
		
		distributionPanel.init( distributionLOVList, new java.awt.Dimension( 250,100 ) ); 
 	}	
}
