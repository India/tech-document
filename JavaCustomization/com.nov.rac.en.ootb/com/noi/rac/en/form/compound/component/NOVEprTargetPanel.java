package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import com.noi.rac.en.form.compound.data._EngPreReleaseForm_DC;
import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVEprTargetPanel extends JPanel 
{
	private static final long serialVersionUID = 1L;
	public TCComponentProcess currentProcess=null;
	public Vector<TCComponent> Eprtargets=new Vector<TCComponent>();
	public HashMap <String,TCComponent> eprTargetsMap = new HashMap <String,TCComponent>();
	public NOVEprTargetTable targetsTbl;
	public TCSession m_tcSession;
	private _EngPreReleaseForm_DC eprFormDC;
 protected Registry reg;
	public NOVEprTargetPanel(_EngPreReleaseForm_DC engPreReleaseFormDC) 
	{
		eprFormDC = engPreReleaseFormDC;
		m_tcSession = engPreReleaseFormDC.getForm().getSession();
		reg = Registry.getRegistry(this);
		String[] tblcolumns = reg.getString("SubColumnName.NAME").split(",");
		targetsTbl =  new NOVEprTargetTable(m_tcSession, tblcolumns,eprTargetsMap); 
		
	    targetsTbl.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	    targetsTbl.addMouseListener(new java.awt.event.MouseAdapter() 
	    {
            public void mouseClicked(java.awt.event.MouseEvent mouseEvt)
            {
                Object source = mouseEvt.getSource();
                if(source instanceof JTreeTable)
                {
                    JTreeTable treeTable = (JTreeTable)source;
                    int selrow = treeTable.getSelectedRow();
                    //int itemIdCol = targetsTbl.getColumn("Part ID").getModelIndex();
                    Object obj = treeTable.getValueAt(selrow, 0);
                    /*TreeTableNode infcomps[] = treeTable.getSelectedNodes();
                    if(infcomps.length > 0)
                        if(!infcomps[0].isNodeExpanded())
                        	targetsTbl.expandNode(infcomps[0]);
                        else
                        	targetsTbl.collapseNode(infcomps[0]);*/
                    if(mouseEvt.getClickCount() == 2)
                    {
                        TCComponent selComp = (TCComponent)targetsTbl.epr_TargetsMap.get(obj);
                        if(selComp instanceof TCComponentItemRevision)
                        {
                        	try
                        	{
                        	if(!(selComp.getProperty("object_type").contains(reg.getString("Documents.TYPE")))) 
                        	{                        			
                            com.teamcenter.rac.aif.AIFDesktop desktop = AIFUtility.getActiveDesktop();
                            NOVEprBomInfoDialog bomInfoDlg = new NOVEprBomInfoDialog(desktop, (TCComponentItemRevision)selComp);
                            bomInfoDlg.compareBOM();
                            bomInfoDlg.setVisible(true);
                        	}
                        	}
                        	catch(TCException tc)
                        	{
                        		System.out.println(tc);
                        	}
                        }
                    }
                }
            }            
        });
		JScrollPane targetTblPane = new JScrollPane(targetsTbl);
		targetTblPane.setPreferredSize(new Dimension(625,150));		
							
		this.setLayout(new PropertyLayout());		
		this.add("1.1.left.top",targetTblPane);		
	}

	public void getTargets(TCComponentForm eprform)
	{			
		try
		{
			TCProperty dispProp = ((TCProperty)eprFormDC.imanProperties.get(this));
			AIFComponentContext[] contexts=eprform.whereReferenced();			
			if(contexts!=null)
			{
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
					{
						currentProcess=(TCComponentProcess)contexts[j].getComponent();
						TCComponentTask rootTask=((TCComponentProcess)currentProcess).getRootTask();
						TCComponent[] targets=rootTask.getAttachments(
								                          TCAttachmentScope.LOCAL, 
								                          TCAttachmentType.TARGET);
						TCComponent[] references=rootTask.getAttachments(
		                          						  TCAttachmentScope.LOCAL, 
		                          						  TCAttachmentType.REFERENCE);
						for(int i=0;i<targets.length;i++)
						{
							if(targets[i] instanceof TCComponentItemRevision)
							{
								String itemid=targets[i].getProperty("item_id");		
								eprTargetsMap.put(itemid,targets[i]);
							}
						}
						for(int i=0;i<references.length;i++)
						{
							if(references[i] instanceof TCComponentItemRevision)
							{
								String itemid=references[i].getProperty("item_id");		
								eprTargetsMap.put(itemid,references[i]);
							}
						}
						targetsTbl.setProcess(currentProcess);
						//Creating Disposition instances
						if (dispProp!=null) 
						{
							TCComponent[] dispComps = dispProp.getReferenceValueArray();
							if (dispComps.length==0) 
							{
								setDispData();		
							}
							else
							{
								buildTargetIDCompMap(dispComps);
							}
						}
					}
				}
			}
			
			if (currentProcess == null) 
			{
				TCComponent[] dispComps = dispProp.getReferenceValueArray();
				if (dispComps.length>0) 
				{
					Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(dispComps));
					TCComponentItemRevisionType type=(TCComponentItemRevisionType)m_tcSession.getTypeService().getTypeComponent("ItemRevision");
					for(int k=0;k<tempReference.size();k++)
					{					
						String targetId=(tempReference.get(k).getTCProperty("targetitemid")).getStringValue();			
						String revid=(tempReference.get(k).getTCProperty("rev_id")).getStringValue();
						TCComponent comp=type.findRevision(targetId, revid);
						if(comp!=null)
							eprTargetsMap.put(targetId,comp);
					}	
				}	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void setDispData() 
	{
		try 
		{
			if (eprTargetsMap.size()>0) 
			{
				String targetIds[];//=new String[targets.size()];	
				String targetRevIds[];
				Vector<String> vtargetIds=new Vector<String>();
				Vector<String> vtargetRevIds=new Vector<String>();
				
				TCComponent targetToProcess = null;
								
				Iterator itr = eprTargetsMap.entrySet().iterator();
				while(itr.hasNext())
				{	
					Map.Entry targetMap = (Map.Entry)itr.next();
					TCComponent  target = (TCComponent) targetMap.getValue();
					if(!(target.getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE"))))
					{
						vtargetIds.add(((TCComponentItemRevision)target).getProperty("item_id"));
						vtargetRevIds.add(((TCComponentItemRevision)target).getProperty("current_revision_id"));
						
						targetToProcess = target;
					}
				}
				targetIds=new String[vtargetIds.size()];
				vtargetIds.copyInto(targetIds);
				
				targetRevIds=new String[vtargetRevIds.size()];
				vtargetRevIds.copyInto(targetRevIds);
				
				TCComponent[] targetdispComps = createDispInstances(new TCComponent[] {targetToProcess});
				updateFutureStatus(targetdispComps);
				buildTargetIDCompMap(targetdispComps);
				saveDispDataToForm(targetdispComps);
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}

	private void updateFutureStatus(TCComponent[] targetdispComps) 
	{
		if (targetdispComps!=null) 
		{
			for (int i = 0; i < targetdispComps.length; i++) 
			{
				try 
				{
					targetdispComps[i].refresh();
					String currentStatus = targetdispComps[i].getTCProperty("currentstatus").getStringValue();
					TCProperty futureStatProp = targetdispComps[i].getTCProperty("futurestatus");
					futureStatProp.setStringValue(currentStatus);					
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}
			}	
		}		
	}

	private void buildTargetIDCompMap(TCComponent[]targetdispComps) 
	{
		if (targetdispComps!=null) 
		{
			for (int i = 0; i < targetdispComps.length; i++) 
			{
				try 
				{
					targetdispComps[i].refresh();
					TCProperty preRelProp = targetdispComps[i].getTCProperty("nov4_prerelease_rev");
					String targetItemId = targetdispComps[i].getTCProperty("targetitemid").getStringValue();
					targetsTbl.currentStatMap.put(eprTargetsMap.get(targetItemId), targetdispComps[i].getProperty("currentstatus"));
					targetsTbl.futureStatMap.put(eprTargetsMap.get(targetItemId), targetdispComps[i].getProperty("futurestatus"));
					targetsTbl.m_prereleaseRevMap.put(eprTargetsMap.get(targetItemId), preRelProp!=null?preRelProp.getStringValue():null);
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}
			}	
		}		
	}

	private void saveDispDataToForm(TCComponent[] targetdispComps) 
	{
		eprFormDC.saveOnlyDispData(targetdispComps);
	}

	public TCComponent[] createDispInstances(TCComponent[] targetItems)
	{
		TCComponent[] comps=null;
		/*if(itemIds!=null)
		{
			try
			{
				//call userExit to create the Disposition componenets and return the array
				TCUserService us = m_tcSession.getUserService();       	
				Object obj[]=new Object[3];       	
				obj[0]=itemIds;
				obj[1]=revIds;
				obj[2] = new Boolean(true);
				comps=(TCComponent[])us.call("NATOIL_createEnDispositionData", obj);
			}catch(TCException te)
			{

			}
		}
		return comps;
		*/
		try
		{
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"release_status_list"};
	    	
			Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
	    	
	    	nOVDataManagementServiceHelper.getObjectsProperties(targetItems, attrList, null);
	    	
			for(int i = 0; i < targetItems.length ; i++)
			{
			    CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
			    		"targetdisposition", CreateInObjectHelper.OPERATION_CREATE);		    	
				String item_id = targetItems[i].getProperty("item_id");
				String revision_id = ((TCComponentItem)targetItems[i]).getLatestItemRevision().getProperty("current_revision_id");
				String statusName = targetItems[i].getProperty("release_status_list");
				
				formPropertyMap.setString("targetitemid", item_id);
				formPropertyMap.setString("rev_id", revision_id);
			
				//get status name to set current status
				/*TCComponent[] relStatusList = null;
				relStatusList = ( targetItems[i].getTCProperty("release_status_list").getReferenceValueArray() );
				
				String statusName = relStatusList[0].getProperty("name");
				*/
				if(statusName.isEmpty())
				{
					formPropertyMap.setString("currentstatus", "");
				}
				else
				{
					formPropertyMap.setString("currentstatus", statusName);
				}
				
				if(statusName.isEmpty())
				{
					formPropertyMap.setString("futurestatus", "");
				}
				else if(statusName.compareToIgnoreCase("ENG") == 0 || statusName.compareToIgnoreCase("ENG-V") == 0 )
				{
					formPropertyMap.setString("futurestatus", statusName);
				}
				
				//set empty properties
				
				formPropertyMap.setString("changedesc","");
				formPropertyMap.setString("dispinstruction","");
				formPropertyMap.setString("inprocess","");
				formPropertyMap.setString("ininventory","");
				formPropertyMap.setString("assembled","");
				formPropertyMap.setString("infield","");
				
				vecCreateInObjectHelper.add(formPropertyMap);
	   
			}
			
	        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
	                .toArray(new CreateInObjectHelper[vecCreateInObjectHelper.size()]);
			
			//create objects
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		    Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();
		    
		    for(int j = 0; j<createInObjectHelper.length ; j++)
		    {
		    	TCComponent created = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
		    	createdorUpdatedObjs.add(created);
		    }
		   
		    comps = (TCComponent[]) createdorUpdatedObjs.toArray(new TCComponent[createdorUpdatedObjs.size()]);
			
		}
		catch(TCException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comps;		
	}
	
	public void loadTblData()
	{
		Iterator itr = eprTargetsMap.entrySet().iterator();
		while(itr.hasNext())
		{	
			Map.Entry targetMap = (Map.Entry)itr.next();
			TCComponent  tarGet = (TCComponent) targetMap.getValue();
			targetsTbl.addTarget(tarGet);
		}
	}

	public void setTitleBorderText(String title)
	{
		TitledBorder tb = new TitledBorder(title);
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		setBorder(tb);
	}
	

}
