package com.noi.rac.en.form.compound.component;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import com.noi.rac.en.util.components.NOIJLabel;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.teamcenter.rac.aif.AbstractAIFDialog;
//import com.teamcenter.rac.aif.ApplicationDef;//TC10.1 Upgrade
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVEprBomInfoDialog extends AbstractAIFDialog 
{

	private static final long serialVersionUID = 1L;
	
	private TCComponentItemRevision latestBaselineRev;
	private TCComponentItemRevision targetRev;
	public NOVEnBomInfoPanel eprBomTablePanel ;
 private Registry reg;
	public NOVEprBomInfoDialog(Frame desktop, TCComponentItemRevision targetRevision )
	{
		super(desktop);
		reg = Registry.getRegistry(this);
		try 
		{
			this.targetRev = targetRevision;
			if (latestBaselineRev == null) 
			{
				TCComponentItemRevision []  baseLineRevs = targetRev.getItem().getReleasedItemRevisions();
				latestBaselineRev = getLatestBaseLineRev(baseLineRevs);	
				eprBomTablePanel =  new NOVEnBomInfoPanel(latestBaselineRev, targetRev);
				createUI();
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	
	public NOVEprBomInfoDialog(TCComponentItemRevision prevRevision, TCComponentItemRevision LatestRevision)
	{
	 reg = Registry.getRegistry(this);
		try 
		{				
			targetRev = LatestRevision;
			latestBaselineRev = prevRevision;
			eprBomTablePanel =  new NOVEnBomInfoPanel(latestBaselineRev, targetRev);
			createUI();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void createUI()
	{
		JButton button = new JButton(reg.getString("edit.BUTTON"));
		button.addActionListener(new ActionAdapter()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				/*ApplicationDef appDef =  AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey("com.teamcenter.rac.pse.PSEApplication");
				appDef.openApplication(new InterfaceAIFComponent[]{targetRev});*/
			    IOpenService openPerspective = PerspectiveDefHelper.getOpenService("com.teamcenter.rac.pse.PSEApplication");//TC10.1 Upgrade
                openPerspective.open(new InterfaceAIFComponent[]{targetRev});//TC10.1 Upgrade
				NOVEprBomInfoDialog.this.dispose();
			}
		});
		
		this.getContentPane().setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		JPanel lblPanel = new JPanel(new BorderLayout());
		String lblStr = "";
		if (latestBaselineRev!=null) 
		{
			lblStr = (reg.getString("BOMrepFor.LEBLE"))+targetRev.toString()+" , "+latestBaselineRev.toString();	
		}
		else
		{
			lblStr = (reg.getString("BOMfor.LEBLE"))+targetRev.toString();
		}
		
		NOIJLabel lbl = new NOIJLabel(lblStr);
		lblPanel.add(lbl, BorderLayout.WEST);
		lblPanel.add(button, BorderLayout.EAST);
		lblPanel.setPreferredSize(new Dimension(700,22));
		lblPanel.setBackground(Color.WHITE);
		JPanel legendPanel = new JPanel();
		legendPanel.setBackground(Color.WHITE);
		JPanel addRenPanel = new JPanel();
		addRenPanel.setBackground(Color.GREEN);
		addRenPanel.setBorder(new LineBorder(Color.BLACK));
		addRenPanel.setPreferredSize(new Dimension(20,16));
		NOIJLabel addRenLbl = new NOIJLabel(reg.getString("ItemAdded.MSG"));
		JPanel remRenPanel = new JPanel()
		{
			private static final long serialVersionUID = 1L;
			public void paintComponent(Graphics g )
	        {
	            super.paintComponent(g);
	            Graphics2D g2=(Graphics2D)g;
	            g2.setColor(Color.RED);
	            int midpoint = getHeight() / 2;
	            g2.setStroke(new BasicStroke(2)); 
	            g2.drawLine(0, midpoint, getWidth() - 1, midpoint);
	        }
		};
		remRenPanel.setBackground(Color.WHITE);
		remRenPanel.setBorder(new LineBorder(Color.BLACK));
		remRenPanel.setPreferredSize(new Dimension(20,16));
		NOIJLabel remRenLbl = new NOIJLabel(reg.getString("ItemRemoved.MSG"));
		/*JPanel revChRenPanel = new JPanel();
		revChRenPanel.setBackground(Color.YELLOW);
		revChRenPanel.setBorder(new LineBorder(Color.BLACK));
		revChRenPanel.setPreferredSize(new Dimension(20,16));
		NOIJLabel revChRenLbl = new NOIJLabel("Revision Change");*/
		JPanel qtyRenPanel = new JPanel()
		{
			private static final long serialVersionUID = 1L;
			public void paintComponent(Graphics g )
	        {
	            super.paintComponent(g);
	            Graphics2D g2=(Graphics2D)g;
	            g2.setColor(Color.RED);
	            int midpoint = getHeight() / 2;
	            g2.setStroke(new BasicStroke(2)); 
	            g2.drawLine(0, midpoint, getWidth() - 1, midpoint);
	        }
		};
		qtyRenPanel.setBackground(Color.WHITE);
		qtyRenPanel.setBorder(new LineBorder(Color.BLACK));
		qtyRenPanel.setPreferredSize(new Dimension(20,16));
		NOIJLabel qtyRenLbl = new NOIJLabel(reg.getString("QuantityChange.MSG"));
		legendPanel.add(addRenPanel);
		legendPanel.add(addRenLbl);
		legendPanel.add(remRenPanel);
		legendPanel.add(remRenLbl);
		//legendPanel.add(revChRenPanel);
		//legendPanel.add(revChRenLbl);
		legendPanel.add(qtyRenPanel);
		legendPanel.add(qtyRenLbl);
		this.getContentPane().setBackground(Color.WHITE);
		this.getContentPane().add("1.1.left.center",lblPanel);
		this.getContentPane().add("2.1.left.center",eprBomTablePanel);
		this.getContentPane().add("3.1.left.center",legendPanel);
		this.setTitle(reg.getString("BOMDetails.TITLE"));
		this.centerToScreen();
		this.pack();
		this.setModal(true);
	}	
	
	public static TCComponentItemRevision getLatestBaseLineRev(
			TCComponentItemRevision[] baseLineRevs) 
	{
		for (int i = 0; i < baseLineRevs.length; i++) 
		{
			try 
			{
				TCComponent[] statuses = baseLineRevs[i].getTCProperty("release_status_list").getReferenceValueArray();
				
				for (int j = 0; j < statuses.length; j++) 
				{
					if (statuses[0].toString().equalsIgnoreCase("Engineering Pre Release")) 

					{
						return baseLineRevs[i];
					}
				}
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public void compareBOM() 
	{
		eprBomTablePanel.compareBOM(false);
	}
}