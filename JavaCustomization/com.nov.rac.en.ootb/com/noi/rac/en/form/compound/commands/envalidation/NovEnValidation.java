package com.noi.rac.en.form.compound.commands.envalidation;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.noi.rac.en.util.components.NOVEnTargetsPanel_v2;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NovEnValidation extends AbstractAIFDialog 
{
	private static final long serialVersionUID = 1L;

	Vector<String> statVec = new Vector<String>();
	Vector<String> classVec = new Vector<String>();
	Vector<String> uomVec = new Vector<String>();
	Vector<String> rs1IDVec = new Vector<String>();
	Vector<String> rsOrgVec = new Vector<String>();
	Vector<String> mfgNameVec = new Vector<String>();
	Vector<String> mfgNoDupVec = new Vector<String>();
	Vector<String> accPrivVec = new Vector<String>();
	Vector<String> dispValVec = new Vector<String>();
	Map<String,Vector<String>> parentChild = new HashMap<String,Vector<String>>();
	private TCSession session;
	Registry reg;
	
	public NovEnValidation( Frame parentFrame, _EngNoticeForm_Panel_v2 enFormPanel )
	{
		super( parentFrame );
		JEditorPane editorPane = new JEditorPane();
		String ecoform = "";
		session = enFormPanel.getSession();
		reg = Registry.getRegistry(this);
		try 
		{
			ecoform = enFormPanel.Enform.getProperty(reg.getString("objName.PROP"));
		}
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		JPanel listPane = new JPanel();											// Vivek
		listPane.setLayout(new BoxLayout(listPane, BoxLayout.LINE_AXIS));		// Vivek
		
		final String htmlText = " <FONT SIZE=\"6\" COLOR=#330033><center>"+ecoform+" Validation Report</center></FONT>"+getValidationReportString(enFormPanel);
		editorPane.setContentType("text/html");
		editorPane.setText(htmlText);
		editorPane.setEditable(false);
		
		int width = ((int) parentFrame.getBounds().getWidth()) * 7;				// Vivek
		int height = ((int) parentFrame.getBounds().getHeight()) * 10;			// Vivek

		JScrollPane scPane = new JScrollPane(editorPane);
		scPane.setPreferredSize(new Dimension(width, height));					// Vivek
		this.setTitle(reg.getString("valReport.TITLE"));
		listPane.add(scPane);													// Vivek
		
		JPanel panel = new JPanel();
		JButton saveBtn = new JButton(reg.getString("saveReportBtn.LABEL"));
		
		saveBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JFileChooser fileChooser = new JFileChooser();
				int returnVal = fileChooser.showSaveDialog(NovEnValidation.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile(); //This grabs the File you typed
					String nameOfFile = "";   
		            nameOfFile = file.getPath();
		            nameOfFile =nameOfFile+".html";
		            try 
					{   
						BufferedWriter out = new BufferedWriter(new FileWriter(nameOfFile));   
						out.write(htmlText);   
						out.close();   
					} 
					catch (IOException ex1) 
					{   
						MessageBox.post(ex1);
					}					
				}
			}
		});
		
		JButton cancelBtn = new JButton(reg.getString("cancelBtn.LABEL"));
		cancelBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				NovEnValidation.this.dispose();
			}
		});
		panel.add(saveBtn);
		panel.add(cancelBtn);
		getContentPane().add(listPane,BorderLayout.CENTER);		// Vivek
		getContentPane().add(panel,BorderLayout.PAGE_END);		// Vivek
		setModal( true );
		pack();
	}


	private String getValidationReportString(_EngNoticeForm_Panel_v2 enFormPanel) 
	{
		boolean legacyFlag = true;
		String enOwnGrp = null;
//        TCPreferenceService prefSer = session.getPreferenceService();
//        String[] prefLangNames = prefSer.getStringArray(TCPreferenceService.TC_preference_site, "NOV_ValidateLegacyPart");
//        String loggedgrp = session.getCurrentGroup().toString();
//        for(int k1=0; k1<prefLangNames.length; k1++)
//        {
//        	if(prefLangNames[k1].equalsIgnoreCase(loggedgrp))
//        	{
//        		legacyFlag = false;
//        		break;
//        	}
//        }
		TCPreferenceService prefSer = session.getPreferenceService();
		String[] prefLangNames = prefSer.getStringArray(TCPreferenceService.TC_preference_site, "NOV_ValidateLegacyPart");
		try {
			enOwnGrp = enFormPanel.Enform.getProperty("owning_group");
		} catch (TCException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int k1=0; k1<prefLangNames.length; k1++)
		{
			if( (enOwnGrp != null) && (prefLangNames[k1].equalsIgnoreCase(enOwnGrp)))
			{
				legacyFlag = false;
				break;
			}
		}
		TCUserService userservice = enFormPanel.Enform.getSession().getUserService();
		Object[] inputObjs = new Object[4];
		inputObjs[0]= enFormPanel.getProcess();
		inputObjs[1]= enFormPanel.Entargets.toArray(new TCComponent[enFormPanel.Entargets.size()]);
		inputObjs[2]= enFormPanel.Enform;//usha
		inputObjs[3]= legacyFlag;//amitosh
		
		Vector<String> strvalidatationDataVec = new Vector<String>();
		
		try 
		{
			Object retObj = userservice.call(reg.getString("enValidation.SERVERCALL"), inputObjs);
			Object retObj1 = userservice.call(reg.getString("enValidation.SERVERCALL1"), inputObjs);
			if (retObj!=null && retObj1 != null ) 
			{
				if(retObj instanceof String[] && retObj1 instanceof String[] ) 
				{
					String[] arry1 = (String[])retObj;
					String[] arry2 = (String[])retObj1;
					
					strvalidatationDataVec.addAll( Arrays.asList(arry1) );
					strvalidatationDataVec.addAll( Arrays.asList(arry2) );
					
					String[] arry = strvalidatationDataVec.toArray( new String[strvalidatationDataVec.size()] );
					
					for (int i = 0; i < arry.length; i++) 
					{
						String[] strSpt = arry[i].split("=");
						for (int j = 1; j < strSpt.length; j++)
						{
							String[] childErrStrA = strSpt[j].split(">>");
							
							if (i==0) 
							{
								String[] childItems = childErrStrA[1].split(",");
								Vector<String> childItemVec = new Vector<String>(Arrays.asList(childItems)); 
								parentChild.put(childErrStrA[0],childItemVec );
							}
							//got all child info for parent target - now check for result strings							
							//usha - TCDECREL-951
							String[] errItems = new String[]{};
							if(childErrStrA.length >2 && (childErrStrA[2].length() > 0))
								errItems = childErrStrA[2].split(","); //it now stores a result string as follows:
							//	PASS-<ITEMID>, FAIL-<ITEM ID>, N/A-<ITEMID>
							//if (errItems.length>0 && (!errItems[0].equalsIgnoreCase("NoErrorItems")))
							if (errItems.length>0 )
							{
								List<String> list = Arrays.asList(errItems);	
								if(strSpt[0].equalsIgnoreCase("StatusCheck"))
								{
									statVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("Classification"))
								{
									classVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("UOM"))
								{
									uomVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("RSOneId"))
								{
									rs1IDVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("RSOrg"))
								{
									rsOrgVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("MFGName"))
								{
									mfgNameVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("MFGNumDup"))
								{
									mfgNoDupVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("AccessPrev"))
								{
									accPrivVec.addAll(list);
								}
								else if(strSpt[0].equalsIgnoreCase("Disposition"))
								{
									dispValVec.addAll(list);
								}
							}
						}
					}
				}
			}
		}
		catch (TCException e) 
		{
            MessageBox.post("Contact CET support to check if Datafile is missing from Item Revision Master Form for the following items: \n" + e.getDetailsMessage(), "Error", MessageBox.ERROR);			
		}
		
		String reportStr = "";
		
		Set<String> parentSet = parentChild.keySet();
		String[] parentStrA = parentSet.toArray(new String[parentSet.size()]);
		
		for (int i = 0; i < parentStrA.length; i++) 
		{
			String objRowDatap = getReportString(parentStrA[i],true);
			reportStr  = reportStr + objRowDatap;
			Vector<String> childernVec = parentChild.get(parentStrA[i]);
			for (int j = 0; (j < childernVec.size()) && (childernVec.get(j).length() >0) ; j++) 
			{
				String objRowData = getReportString(childernVec.get(j),false);
				reportStr  = reportStr + objRowData;
			}
		}
		
		String str = "<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Parent</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Children</B></TD>"+
	    "<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Status</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Classification</B></TD>"+
		"<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>UOM</B></TD><TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>RSOne ID</B></TD>" +
		"<TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>RSOrg</B></TD><TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>MFG Name Exists ?</B></TD>"+
		"<TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>MFG No duplicate check</B></TD>"+
		"<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Approver's privilege check</B></TD>" +
		"<TD WIDTH=\"120\"><FONT COLOR=#153E7E><B><center>Dispositions check</B></TD></TR>"+reportStr+"</table>";
		
		return str;
	}

	private String getReportString(String itemId, boolean isParent) 
	{
		String passedStr = reg.getString("passed.VAL");
		String failedStr = reg.getString("failed.VAL");
		String NAStr	 = reg.getString("NA.VAL");
		
		String passedChkStr = reg.getString("passCheck.VAL");
		String failedChkStr = reg.getString("failCheck.VAL");
			
		String retString = "";
		String parentID = " ";
		String childID = " ";
		
		String status = "";	
		String classficationCk = "";	
		String uomVal = "";	
		String rs1Fail = "";	
		String rsOrg = "";	
		String mfgName = "";	
		String mfgNnoDup = "";	
		String accprev = "";	
		String disp = "";	
		
		String	itemPassStr = passedChkStr	+ "-" + itemId;
		String  itemFailStr = failedChkStr	+ "-" + itemId;
		//String	itemNAStr	= NAStr		+ "-" + itemId;

		
		if (isParent) 
		{
			parentID = itemId;	
		}
		if (!isParent) 
		{
			childID = itemId;	
		}
		
		if(itemId!= null )
		{
			//status
			if(statVec.contains(itemPassStr))
				status = passedStr;
			else if(statVec.contains(itemFailStr))
				status = failedStr;
			else
				status = NAStr;
			//classification
			if(classVec.contains(itemPassStr))
				classficationCk = passedStr;
			else if(classVec.contains(itemFailStr))
				classficationCk = failedStr;
			else
				classficationCk = NAStr;
			
			//uom
			if(uomVec.contains(itemPassStr))
				uomVal = passedStr;
			else if(uomVec.contains(itemFailStr))
				uomVal = failedStr;
			else
				uomVal = NAStr;
			
			//rs1 id
			if(rs1IDVec.contains(itemPassStr))
				rs1Fail = passedStr;
			else if(rs1IDVec.contains(itemFailStr))
				rs1Fail = failedStr;
			else
				rs1Fail = NAStr;
			
			//rs org 
			if(rsOrgVec.contains(itemPassStr))
				rsOrg = passedStr;
			else if(rsOrgVec.contains(itemFailStr))
				rsOrg = failedStr;
			else
				rsOrg = NAStr;
			
			//mfg name
			if(mfgNameVec.contains(itemPassStr))
				mfgName = passedStr;
			else if(mfgNameVec.contains(itemFailStr))
				mfgName = failedStr;
			else
				mfgName = NAStr;
			
			//mfg no. dup
			if(mfgName.equalsIgnoreCase(failedStr) || mfgName.equalsIgnoreCase(NAStr))
				mfgNnoDup = NAStr;
			else
			{
				if(mfgNoDupVec.contains(itemPassStr))
					mfgNnoDup = passedStr;
				else if(mfgNoDupVec.contains(itemFailStr))
					mfgNnoDup = failedStr;
				else
					mfgNnoDup = NAStr;	
			}
			
			//access
			if(accPrivVec.contains(itemPassStr))
				accprev = passedStr;
			else if(accPrivVec.contains(itemFailStr))
				accprev = failedStr;
			else
				accprev = NAStr;
	
			//disp
			if(!isParent)
				disp = NAStr;
			else
			{
				if(dispValVec.contains(itemPassStr))
					disp = passedStr;
				else if(dispValVec.contains(itemFailStr))
					disp = failedStr;
				else
					disp = NAStr;
			}
		}
		
		String fonts = "<FONT COLOR=#153E7E>"; 
		if(status.equalsIgnoreCase(failedStr))
			fonts = "<FONT COLOR=#FF0000>";
		
		String fontcls = "<FONT COLOR=#153E7E>"; 
		if(classficationCk.equalsIgnoreCase(failedStr))
			fontcls = "<FONT COLOR=#FF0000>";
				
		String fontuom = "<FONT COLOR=#153E7E>"; 
		if(uomVal.equalsIgnoreCase(failedStr))
			fontuom = "<FONT COLOR=#FF0000>";
		
		String fontrs1 = "<FONT COLOR=#153E7E>"; 
		if(rs1Fail.equalsIgnoreCase(failedStr))
			fontrs1 = "<FONT COLOR=#FF0000>";
		
		String fontrsorg = "<FONT COLOR=#153E7E>"; 
		if(rsOrg.equalsIgnoreCase(failedStr))
			fontrsorg = "<FONT COLOR=#FF0000>";
				
		String fontmfgN = "<FONT COLOR=#153E7E>";
		if(mfgName.equalsIgnoreCase(failedStr))
			fontmfgN = "<FONT COLOR=#FF0000>";
		
		String fontmfgNo = "<FONT COLOR=#153E7E>";
		if(mfgNnoDup.equalsIgnoreCase(failedStr))
			fontmfgNo = "<FONT COLOR=#FF0000>";
		
		String fontacc = "<FONT COLOR=#153E7E>";
		if(accprev.equalsIgnoreCase(failedStr))
			fontacc = "<FONT COLOR=#FF0000>";
				
		String fontdisp = "<FONT COLOR=#153E7E>";
		if(disp.equalsIgnoreCase(failedStr))
				fontdisp = "<FONT COLOR=#FF0000>";
		
		retString= "<TR><TD WIDTH=\"160\">"+parentID+"</TD><TD WIDTH=\"160\">"+childID+"</TD>"+
	    "<TD WIDTH=\"160\"><center>"+fonts+status+"</TD><TD WIDTH=\"160\"><center>"+fontcls+classficationCk+"</TD>"+
		"<TD WIDTH=\"160\"><center>"+fontuom+uomVal+"</TD><TD WIDTH=\"200\"><center>"+fontrs1+rs1Fail+"</TD>" +
		"<TD WIDTH=\"200\"><center>"+fontrsorg+rsOrg+"</TD><TD WIDTH=\"200\"><center>"+fontmfgN+mfgName+"</TD>"+
		"<TD WIDTH=\"200\"><center>"+fontmfgNo+mfgNnoDup+"</TD>"+
		"<TD WIDTH=\"160\"><center>"+fontacc+accprev+"</TD>"+
		"<TD WIDTH=\"120\"><center>"+fontdisp+disp+"</TD></TR>";
		return retString;
	}

}
