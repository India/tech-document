package com.noi.rac.en.form.compound.masters;

import com.teamcenter.rac.kernel.TCComponentForm;

/**
 * @author niuy
 * @Date:  May 23, 2007
 * @usage: The class is used to 
 */
public abstract class BaseMasterForm implements IMasterForm {
	protected TCComponentForm masterForm;
    
	public String getPanelName(){
		String itemType = masterForm.getType();
		String panelName = itemType.replaceAll("NOV_", "");
		panelName.replaceAll("NOI_", "");
		panelName = panelName.replaceAll("_Master", "");
		panelName = panelName.replaceAll("_", "");
		if(panelName.equalsIgnoreCase("EngRequestOrder"))
		{
			panelName = "EngReleaseOrder";
		}

		return panelName;
	}
	
	public boolean isFormSavable()
	{
		return true;
	}
}
