package com.noi.rac.en.form.compound.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JCheckBox;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel;
import com.noi.rac.en.form.compound.component.NOVEnGenSearchPanel;
import com.noi.rac.en.form.compound.util.NOVCustomFormProperties;
import com.noi.rac.en.mission.helper.NOVMissionHelper;
import com.noi.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVEnDispositionPanel;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.combobox.iComboBox;

public class _EngNoticeForm_DC extends NOVCustomFormProperties 
{
	public JCheckBox mfgReq;
    public JCheckBox docControlRequired;
	public NOVLOVPopupButton drafted;
	public NOVLOVPopupButton checked;
	public NOVLOVPopupButton approved;
	public NOVLOVPopupButton created;
	public iComboBox reasonCombo;
    public JCheckBox m_isENCritical;//TCDECREL-4530
	public NOVDateButton draftedon;
	public NOVDateButton checkedon;
	public NOVDateButton approvedon;
	public NOVDateButton creationon;
	public iTextArea furExplanation ;
	public DateButton validationDate;
	NOVEnGenSearchPanel relatedECRSearchPanel;
	NOVEnGenSearchPanel projectsPanel;
	NOVEnDistributionPanel distributionPanel;
	public NOVEnDispositionPanel dispDataComp;	
	AbstractReferenceLOVList distSource;
	AbstractReferenceLOVList distTarget;
	 private Registry registry      = Registry
	 										.getRegistry("com.noi.rac.en.form.form");
	
	String[]   propertyNames = new String[] {"newrelease","mfgreviewrequired","nov4_doccontrol_required","islocked","lastvalidatedon",
			"draftedby","draftedon","checkedby","checkedon","approvedby",
			"approvedon","reason","nov4_is_en_critical","relatedecr","explanation","projects",
			"distribution","entargetsdisposition"};

	public _EngNoticeForm_DC()
	{

		mfgReq = new JCheckBox();
        docControlRequired = new JCheckBox();
		drafted = new NOVLOVPopupButton();
		checked = new NOVLOVPopupButton();
		approved = new NOVLOVPopupButton();
		created = new NOVLOVPopupButton();
		reasonCombo = new iComboBox();
		reasonCombo.setMandatory(true);
		reasonCombo.setEditable(false);
		m_isENCritical = new JCheckBox();
		draftedon = new NOVDateButton();
		checkedon = new NOVDateButton();
		approvedon = new NOVDateButton();
		creationon = new NOVDateButton();
		furExplanation = new iTextArea();
		validationDate=new DateButton();


		//addProperty("newrelease",newRelease );
		addProperty("mfgreviewrequired", mfgReq);
		addProperty("nov4_doccontrol_required", docControlRequired);
		//addProperty("islocked", );
		addProperty("draftedby", drafted);
		addProperty("draftedon", draftedon);
		addProperty("checkedby", checked);
		addProperty("checkedon", checkedon);
		addProperty("approvedby", approved);
		addProperty("approvedon", approvedon);
		addProperty("createdby", created);
		addProperty("createdon", creationon);
		addProperty("reason", reasonCombo);
        addProperty("nov4_is_en_critical", m_isENCritical);//TCDECREL-4530
		addProperty("explanation", furExplanation);
		addProperty("lastvalidatedon", validationDate);

	}



	public String[] getPropertyNames()
	{
		return propertyNames; 
	}

	public void setForm(TCComponent form) 
	{
		if (form!=null) 
		{
			try
			{   super.setForm(form);	            
			//get the disposition data
			TCProperty targetdispProperty=form.getTCProperty("entargetsdisposition");
			//targetdispProperty.getReferenceValueArray();
			dispDataComp=new NOVEnDispositionPanel(targetdispProperty.getReferenceValueArray());
			relatedECRSearchPanel = new NOVEnGenSearchPanel(form.getSession(),"");;
			projectsPanel = new NOVEnGenSearchPanel(form.getSession(),"TC_Project");;
		
			distributionPanel = new NOVEnDistributionPanel();

			// Code added for Distribution List -------
			String currentGroup="";
			distSource = new AbstractReferenceLOVList();				
			TCComponentGroup group=form.getSession().getCurrentGroup();
			String groupname=group.getFullName();
			if(groupname.indexOf('.')>=0)
				currentGroup=groupname.substring(0, groupname.indexOf('.'));
			else
				currentGroup=groupname;
			currentGroup=currentGroup+":";
			distSource.initClassECNERO(form.getSession(),"ImanAliasList","alName", currentGroup);
			//@harshada - Modified for test
			//distSource.initClass(getForm().getSession(),"ImanAliasList","alName");
			distTarget = new AbstractReferenceLOVList();
			// Code added for Distribution List -------
			
			TCComponentListOfValues lovalues = TCComponentListOfValuesType
			.findLOVByName(form.getSession(), "_RSOne_enreasonforchange_");
			//ListOfValuesInfo lovInfo = lovalues.getListOfValues();
			//Object[] lovS = lovInfo.getListOfValues();
			//Vector<Object> reasonValVac = new Vector<Object>();
			Object[] lovS =lovalues.getListOfValues().getLOVDisplayValues();
			if ( lovS.length > 0 )
			{
				reasonCombo.addItem("");
				for ( int i = 0; i < lovS.length; i++ )
				{
					reasonCombo.addItem(lovS[i]);
				}
			}


			addProperty("entargetsdisposition", dispDataComp);
			addProperty("relatedecr", relatedECRSearchPanel);
			addProperty("projects", projectsPanel);
			addProperty("distribution", distributionPanel);
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}	
			super.getFormProperties(propertyNames);
		}
	}

	public void enableForm()
	{
		enableProperty("mfgreviewrequired");		
		enableProperty("nov4_doccontrol_required");
		enableProperty("reason");
		enableProperty("explanation");
		enableProperty("lastvalidatedon");
		enableProperty("entargetsdisposition");
		enableProperty("relatedecr");
		enableProperty("projects");
		enableProperty("distribution");
	}
	public void clearForm() 
	{}

	public void populateFormComponents() 
	{
		if(getForm() == null) return;
		try 
		{
			/*TCComponentForm preEnForm = getPreviousENForm();
			if (preEnForm!=null) 
			{
				String[] projects = preEnForm.getTCProperty("projects").getStringValueArray();
				String[] distrs = preEnForm.getTCProperty("distribution").getStringValueArray();
				String[] relatedEcr = preEnForm.getTCProperty("relatedecr").getStringValueArray();

				projectsPanel.populateSourceList(projects);
				distributionPanel.populateSourceList(distrs);
				relatedECRSearchPanel.populateSourceList(relatedEcr);
			}*/
			boolean misGroup = NOVMissionHelper.isGroup(registry.getString("misPref.NAME"),getForm());
			if (misGroup){
				populateMissionFields();
				
			}
			mfgReq.setSelected(((TCProperty)imanProperties.get(mfgReq)).getLogicalValue());
			docControlRequired.setSelected(((TCProperty)imanProperties.get(docControlRequired)).getLogicalValue());

			drafted.setSelectedText(((TCProperty)imanProperties.get(drafted)).getStringValue());
			checked.setSelectedText(((TCProperty)imanProperties.get(checked)).getStringValue());
			approved.setSelectedText(((TCProperty)imanProperties.get(approved)).getStringValue());

			draftedon.setDate(((TCProperty)imanProperties.get(draftedon)).getDateValue());
			checkedon.setDate(((TCProperty)imanProperties.get(checkedon)).getDateValue());
			approvedon.setDate(((TCProperty)imanProperties.get(approvedon)).getDateValue());

			reasonCombo.setSelectedItem(((TCProperty)imanProperties.get(reasonCombo)).getStringValue());
            m_isENCritical.setSelected(((TCProperty) imanProperties.get(m_isENCritical)).getLogicalValue());//TCDECREL-4530

			furExplanation.setText(((TCProperty)imanProperties.get(furExplanation)).getStringValue());


			String[] relatedECRs = ((TCProperty)imanProperties.get(relatedECRSearchPanel)).getStringValueArray();
			if (relatedECRs!=null) 
			{
				relatedECRSearchPanel.populateTargetList(relatedECRs);	
			}

			String[] projects = ((TCProperty)imanProperties.get(projectsPanel)).getStringValueArray();
			if (projects!=null) 
			{
				projectsPanel.populateTargetList(projects);	
			}
			/*String[] diststributionList = ((TCProperty)imanProperties.get(distributionPanel)).getStringValueArray();
			if (diststributionList!=null) 
			{
				distributionPanel.populateTargetList(diststributionList);	
			}*/

			String[] arr1 = ((TCProperty)imanProperties.get(distributionPanel)).getStringValueArray();
			for (int i=0;i<arr1.length;i++) {
				distTarget.addItem(arr1[i],arr1[i]);
				distSource.addExclusion(arr1[i]);
			}
			
			distributionPanel.init(distSource,distTarget,new java.awt.Dimension(250,100)); 
			
			//disposition
			//dispDataComp.
			//validation Date			 
			Date validation=((TCProperty)imanProperties.get(validationDate)).getDateValue();			
			validationDate.setDate(validation);

		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private void populateMissionFields() {
		AIFComponentContext[] partContext;
		try {
			partContext = getForm().whereReferenced();
			TCComponentTask epmTask = null;
			for (int inx = 0; inx<partContext.length;inx++)
			{
				if ( partContext[inx] == null || !(partContext[inx].getComponent() instanceof TCComponentTask)){
					created.setSelectedText("");
					creationon.setDate("");
					continue;
				}
				epmTask =  (TCComponentTask)partContext[inx].getComponent();
				if (epmTask == null)
					continue;
				String processInitiater = epmTask.getProperty("owning_user");
				DateFormat df = new SimpleDateFormat(registry.getString("dateFrmt.NAME")); 
				if (processInitiater != null)
					created.setSelectedText(processInitiater);
				try {
						String date = epmTask.getProperty(registry.getString("crDate.NAME"));
						Date creationDate= new Date();
						if (date != null){
							 creationDate = df.parse(epmTask.getProperty(registry.getString("crDate.NAME")));
							 creationon.setDate(creationDate);
							 break;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
		} catch (TCException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}



	public void saveOnlyDispFormData() 
	{
		try 
		{
			TCProperty[] arr=null;		

			//disposition data
			TCProperty prop = (TCProperty)imanProperties.get(dispDataComp);			
			TCComponent[] dispcomps = dispDataComp.getDispData();
			if (dispcomps != null && dispDataComp.isEnabled())
				prop.setReferenceValueArrayData(dispcomps);		

			arr = enabledProperty(arr,relatedECRSearchPanel);
			arr = enabledProperty(arr,projectsPanel);
			arr = enabledProperty(arr,distributionPanel);
			//arr = enabledProperty(arr,newRelease);
			arr = enabledProperty(arr,validationDate);
			
            //Note : This is required as this enabledProperty(...) method does not add the property 
            // if the control associated with that property is in the disabled state. And in case of 
            // Manufacturer review required Document Control required we need to enable and disable the 
            // controls based on the selection.
            if( !mfgReq.isEnabled() )
            {
                TCProperty mfgProp = (TCProperty) imanProperties.get(mfgReq);
                mfgProp.setLogicalValue(mfgReq.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, mfgReq);
            }
            
            if( !docControlRequired.isEnabled() )
            {
                TCProperty docControlProp = (TCProperty) imanProperties.get(docControlRequired);
                docControlProp.setLogicalValue(docControlRequired.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, docControlRequired);
            }

			arr = enabledProperty(arr,furExplanation);
			arr = enabledProperty(arr,reasonCombo);
			arr = enabledProperty(arr,m_isENCritical);
			arr = enabledProperty(arr,drafted);
			arr = enabledProperty(arr,checked);
			arr = enabledProperty(arr,approved);
			arr = enabledProperty(arr,draftedon);
			arr = enabledProperty(arr,checkedon);
			arr = enabledProperty(arr,approvedon);
			arr=enabledProperty(arr,dispDataComp);
			getForm().setTCProperties(arr);
			getForm().save();
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
	}

	public void saveFormData() 
	{
		try 
		{
			TCProperty[] arr=null;
			((TCProperty)imanProperties.get(relatedECRSearchPanel)).setStringValueArrayData( relatedECRSearchPanel.getTargetListObjects());
			((TCProperty)imanProperties.get(projectsPanel)).setStringValueArrayData( projectsPanel.getTargetListObjects());
			/*((TCProperty)imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel.getTargetListObjects());

			String[] disArr = distTarget.getList();
			if (disArr != null && distributionPanel.isEnabled())
				((TCProperty)imanProperties.get(distributionPanel)).setStringValueArray(disArr);*/
			((TCProperty)imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel.getAllTargetListObjects());
			
			((TCProperty)imanProperties.get(mfgReq)).setLogicalValueData(mfgReq.isSelected());
			((TCProperty)imanProperties.get(docControlRequired)).setLogicalValueData(docControlRequired.isSelected());			

			((TCProperty)imanProperties.get(furExplanation)).setStringValueData(furExplanation.getText());

			Object selObj = reasonCombo.getSelectedItem();
			if (selObj!=null) 
			{
				((TCProperty)imanProperties.get(reasonCombo)).setStringValueData(selObj.toString());
			}

			((TCProperty)imanProperties.get(m_isENCritical)).setLogicalValueData(m_isENCritical.isSelected());

			Object selObj1 = drafted.getSelectedObject();
			if (selObj1!=null) 
			{
				((TCProperty)imanProperties.get(drafted)).setStringValueData(selObj1.toString());
			}
			Object selObj2 = checked.getSelectedObject();
			if (selObj2!=null) 
			{
				((TCProperty)imanProperties.get(checked)).setStringValueData(selObj2.toString());
			}
			Object selObj3 = approved.getSelectedObject();
			if (selObj3!=null) 
			{
				((TCProperty)imanProperties.get(approved)).setStringValueData(selObj3.toString());
			}

			String draftedDate = draftedon.getDateString();
			if (draftedDate != null) 
			{
				((TCProperty)imanProperties.get(draftedon)).setDateValueData(draftedon.getDate());	
			}
			String checkedDate = checkedon.getDateString();
			if (checkedDate != null) 
			{
				((TCProperty)imanProperties.get(checkedon)).setDateValueData(checkedon.getDate());	
			}
			String approvedDate = approvedon.getDateString();
			if (approvedDate != null) 
			{
				((TCProperty)imanProperties.get(approvedon)).setDateValueData(approvedon.getDate());	
			}

			//disposition data
			TCProperty prop = (TCProperty)imanProperties.get(dispDataComp);
			dispDataComp.saveDispData();
			TCComponent[] dispcomps = dispDataComp.getDispData();
			if (dispcomps != null && dispDataComp.isEnabled())
				prop.setReferenceValueArrayData(dispcomps);

			//validation date
			((TCProperty)imanProperties.get(validationDate)).setDateValueData(validationDate.getDate());

			arr = enabledProperty(arr,relatedECRSearchPanel);
			arr = enabledProperty(arr,projectsPanel);
			arr = enabledProperty(arr,distributionPanel);
			//arr = enabledProperty(arr,newRelease);
			arr = enabledProperty(arr,validationDate);
			
            //Note : This is required as this enabledProperty(...) method does not add the property 
            // if the control associated with that property is in the disabled state. And in case of 
            // Manufacturer review required Document Control required we need to enable and disable the 
            // controls based on the selection.
            if( !mfgReq.isEnabled() )
            {
                TCProperty mfgProp = (TCProperty) imanProperties.get(mfgReq);
                mfgProp.setLogicalValue(mfgReq.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, mfgReq);
            }
            
            if( !docControlRequired.isEnabled() )
            {
                TCProperty docControlProp = (TCProperty) imanProperties.get(docControlRequired);
                docControlProp.setLogicalValue(docControlRequired.isEnabled());
            }
            else
            {
            	arr = enabledProperty(arr, docControlRequired);
            }

			arr = enabledProperty(arr,furExplanation);
			arr = enabledProperty(arr,reasonCombo);
			arr = enabledProperty(arr,m_isENCritical);
			arr = enabledProperty(arr,drafted);
			arr = enabledProperty(arr,checked);
			arr = enabledProperty(arr,approved);
			arr = enabledProperty(arr,draftedon);
			arr = enabledProperty(arr,checkedon);
			arr = enabledProperty(arr,approvedon);
			arr=enabledProperty(arr,dispDataComp);


			getForm().setTCProperties(arr);
			getForm().save();
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
	}


	public boolean isFormSavable()
	{
		return true;
		
	}
	
	

}
