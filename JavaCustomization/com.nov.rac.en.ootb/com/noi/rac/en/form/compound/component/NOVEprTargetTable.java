package com.noi.rac.en.form.compound.component;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.Registry;

public class NOVEprTargetTable extends JTreeTable
{
	private static final long serialVersionUID = 1L;
    NOVEnTreeTableModel treeTableModel;
    public HashMap<String, TCComponent> epr_TargetsMap;
    public HashMap<TCComponent, String> currentStatMap ;
    public HashMap<TCComponent, String> futureStatMap ;
    public HashMap<TCComponent, String> m_prereleaseRevMap ;
    TCSession tcSession;
    private TCComponentProcess currentProcess;
    private Registry reg;
    //public HashMap<String, TCComponent> treeTableMap;
    public NOVEprTargetTable(TCSession session, String columnNames[], HashMap<String, TCComponent> eprTargetsMap)
    {
        super(session);
        reg = Registry.getRegistry(this);
        currentStatMap=new HashMap<TCComponent, String>();
        futureStatMap =new HashMap<TCComponent, String>();
        m_prereleaseRevMap =new HashMap<TCComponent, String>();
        tcSession = session;
        epr_TargetsMap = eprTargetsMap;
        //treeTableMap = new HashMap<String, TCComponent>();
        if(columnNames.length > 0)
        {
            treeTableModel = new NOVEnTreeTableModel(columnNames);
            setModel(treeTableModel);
        }
        for(int i = 0; i < columnNames.length; i++)
        {
            TableColumn col = new TableColumn(i);
            addColumn(col);
        }
        
        getTree().setCellRenderer(new TargetIdRenderer(this));
        getColumnModel().getColumn(1).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(2).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(3).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(4).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(5).setCellRenderer(new RootCellRenderer());
		
		getColumnModel().getColumn(2).setPreferredWidth(15);
		getColumnModel().getColumn(3).setPreferredWidth(60);
		getColumnModel().getColumn(4).setPreferredWidth(50);
        
    }

    public void setProcess(TCComponentProcess process)
    {
    	currentProcess = process;
    }
    public boolean isCellEditable(int row, int column)
    {
        return column == 4;
    }

    public void addTarget(TCComponent itemRev)
    {
        if(itemRev != null)
        {
        	try
            {
        		//String item_Id = itemRev.getProperty("item_id");
        	    if (!itemRev.getType().startsWith(reg.getString("Documents.TYPE"))) 
        	    {
        	    	NovTreeTableNode node1 = new NovTreeTableNode(itemRev);
                
                    /*AIFComponentContext contextRDD[] = itemRev.getRelated("RelatedDefiningDocument");
                    for(int i = 0; i < contextRDD.length; i++)
                    {
                        String itemID = contextRDD[i].getComponent().getProperty("item_id");
                        if (epr_TargetsMap.containsKey(itemID)) 
                        {
                        	TCComponent tccomp = epr_TargetsMap.get(itemID);
                            NovTreeTableNode nodeRDD = new NovTreeTableNode(tccomp);
                            node1.add(nodeRDD);
    					}
                    }*/
                    treeTableModel.addRoot(node1);
                    updateUI();	
				}
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
           
        }
    }

    
    
    public class NOVEnTreeTableModel extends TreeTableModel
    {
    	private static final long serialVersionUID = 1L;
		private Vector<String> columnNamesVec;

        public NOVEnTreeTableModel(String columnNames[])
        {
            super();
            columnNamesVec = new Vector<String>();
            TreeTableNode top = new TreeTableNode();
            setRoot(top);
            top.setModel(this);
            for(int i = 0; i < columnNames.length; i++)
            {
                columnNamesVec.addElement(columnNames[i]);
                modelIndexToProperty.add(columnNames[i]);
            }

        }
        public void addRoot(TreeTableNode root)
        {
            TreeTableNode top = (TreeTableNode)getRoot();
            top.add(root);
        }

        public TreeTableNode getRoot(int index)
        {
            if(((TreeTableNode)getRoot()).getChildCount() > 0)
                return (TreeTableNode)((TreeTableNode)getRoot()).getChildAt(index);
            else
                return null;
        }

        public int getColumnCount()
        {
            return columnNamesVec.size();
        }

        public String getColumnName(int column)
        {
            return (String)columnNamesVec.elementAt(column);
        }        
    }
    
    public class NovTreeTableNode extends TreeTableNode
    {

    	 private static final long serialVersionUID = 1L;
         public TCComponent context;         
    	public NovTreeTableNode(TCComponent com)
        {
            context = com;           
        }
    	
        public String getProperty(String name)
        {
        	try 
            {
	            if(name.equalsIgnoreCase(reg.getString("PartID.NAME")))
	            {
					return context.getProperty("item_id") == null ? "" : context.getProperty("item_id").toString();
	            }
	            else if(name.equalsIgnoreCase(reg.getString("PartName.NAME")))
				{
	                return context.getProperty("object_name") == null ? "" : context.getProperty("object_name").toString();
				}
	            else if(name.equalsIgnoreCase(reg.getString("PartRev.NAME")))
	            {
	                return context.getProperty("current_revision_id") == null ? "" : context.getProperty("current_revision_id").toString();
	            }
	            else if(name.equalsIgnoreCase(reg.getString("PartStartStat.NAME")))
	            {
	                /*if(context instanceof TCComponentItemRevision)
	                    return ((TCComponentItemRevision)context).getItem().getProperty("release_statuses");*/
	            	if(context instanceof TCComponentItemRevision)
					{
						return currentStatMap.get(context)== null?"" : currentStatMap.get(context);
					}
	            }
	            else if(name.equalsIgnoreCase(reg.getString("PartFutureStat.NAME"))&&(!context.getType().startsWith(reg.getString("Documents.TYPE"))))
	            {
	            	if(context instanceof TCComponentItemRevision)
					{
						return futureStatMap.get(context)== null?"" : futureStatMap.get(context);
					}
	                //return "ENG";
	            }
	            else if(name.equalsIgnoreCase(reg.getString("PreReleaseRev.NAME")))
	            {
	            	if(context instanceof TCComponentItemRevision)
					{
						return m_prereleaseRevMap.get(context)== null?"" : m_prereleaseRevMap.get(context);
					}
	            }
            }
            catch (Exception e) 
            {
					e.printStackTrace();
			}
            return "";
        }

        public String getRelation()
        {
           return " ";
        }

        public String getType()
        {
            return context.getType();
        }
        
    }

    public class RootCellRenderer extends JLabel implements TableCellRenderer
	{    
		public RootCellRenderer()
		{			
			super();
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
				setText("");
			else
			{
				setText(value.toString());
				if(isSelected)
				{
					setOpaque(true);
					setBackground(table.getSelectionBackground());
				}
				else
				{
					setBackground(table.getBackground());
				}
			}
			return this;
		}
	}
    
    public class TargetIdRenderer extends TreeTableTreeCellRenderer
	{   		

		public TargetIdRenderer(JTreeTable arg0) {
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{
			// Select the current value
			try
			{
				super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
						row, hasFocus);

				if(row==0)
				{
					/*setText("");
					setIcon(TCTypeRenderer.getTypeIcon(tcSession.getTypeComponent("Folder"), "folder"));*/
					String name;
					if (currentProcess !=null) 
					{
						name = currentProcess.getRootTask().getName();//getProperty("object_name");	
					}
					else
					{
						name = "";
					}
					setText(name);
					setIcon(TCTypeRenderer.getTypeIcon(tcSession.getTypeComponent("Job"), "Job"));
					
				
				}
				else
				{
					setText(((NovTreeTableNode)getNodeForRow(row)).context.getProperty("item_id"));
					setIcon(TCTypeRenderer.getIcon(((NovTreeTableNode)getNodeForRow(row)).context));
				}				
			}
			catch(Exception e)
			{
				System.out.println(e);

			}
			return this;
		}
	}
}