package com.noi.rac.en.form.compound.util;

import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.swing.DefaultComboBoxModel;

import com.sun.jndi.ldap.ctl.PagedResultsControl;
import com.sun.jndi.ldap.ctl.PagedResultsResponseControl;


public class LDAPConnection
{ 
//	private  static String[][] controllers ={ {"srvhoudc01","nov.com","389"},{"srvwchdc01","noi.natoil.com","389"},
//	{"srvwchdc02","noi.natoil.com","389"},{"rvsgldc01","noi.natoil.com","389"},
//	{"srvwchengdc01","engntsrv.vrc.local","3268"}};

	private  static String[][] controllers ={{"ldap.nov.com","nov.com","389"}};//srvhoudc01
	//private JList globalAddrdList;
	private String search;
//	public Vector list=new Vector();
//	public Vector name=new Vector();
	public HashMap results=new HashMap();
	public Object data[][];
	private DefaultComboBoxModel lm;
	public boolean isConnected=false;

	public LDAPConnection(String name)//, DefaultComboBoxModel lm)//, JList globalAddrdList )
	{		
		this.search=name;		
		doLogin("plmuser2", "plmnovplm");

	}   

	public HashMap getResults()
	{
		return results;		
	}

	public boolean isConnected()
	{
		return isConnected;
	}
	public void doLogin(String username, String password)
	{		
		try
		{
			for(int i = 0; i < controllers.length; i++)
			{
				authenticate(controllers[0][0], controllers[0][1], controllers[0][2], username, password);
			}

		}
		catch(Exception e)
		{
			System.out.println("Could not log in the user... "+e.toString());
		}
	}

	public void authenticate(String host, String domain, String port, String username, String password)
	{
		LdapContext ctx;
		Hashtable ldapEnv= new Hashtable();		
		String searchFilter;
		String objAttribs[];

		String urlDC = "ldap://"+host+"."+domain+":"+port+"/";		
		String userName = String.valueOf(username)+"@"+domain;
		String userPassword = password;
		ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
		ldapEnv.put(Context.SECURITY_PRINCIPAL, userName);
		ldapEnv.put(Context.SECURITY_CREDENTIALS, userPassword);
		ldapEnv.put(Context.PROVIDER_URL, urlDC);

		String searchBase="DC=NOV,DC=com";
		if(search.indexOf("*")<0)
			search="*"+search+"*";
		//searchFilter = "(&(objectClass=user)(SAMAccountName="+search+"*))";
		//else
		//searchFilter = "(&(!(objectClass=user)(objectClass=group))(SAMAccountName="+search+"))";
		searchFilter = "(&(objectClass=*)(displayName="+search+"))";
		byte[] cookie = new byte[0];		
		int pageSize = 100;

		try {
			//connection
			ctx = new InitialLdapContext(ldapEnv, null);
			isConnected=true;
			//search controls
			SearchControls srchInfo = new SearchControls();
			objAttribs = (new String[] {"displayName","mail"});
			srchInfo.setReturningAttributes(objAttribs); 
			srchInfo.setSearchScope(SearchControls.SUBTREE_SCOPE);				

			//for page results
			Control[] ctls = new Control[]{new PagedResultsControl(pageSize)};//, Control.CRITICAL)};
			ctx.setRequestControls(ctls);
			for(int i=0;i<2;i++)
			{
				do 
				{				   
					NamingEnumeration rows = ctx.search(searchBase, searchFilter, srchInfo);					
					while (rows != null && rows.hasMoreElements())
					{
						SearchResult sr = (SearchResult)rows.next();
						String displayname="";
						String mailid="";
						Attributes attrs=sr.getAttributes();
						for (NamingEnumeration enum2 = attrs.getAll(); enum2.hasMoreElements();) 
						{
							Attribute attrib = (Attribute)enum2.nextElement();						
							for (NamingEnumeration e = attrib.getAll();e.hasMoreElements();)
							{		
								if(attrib.getID().equals("displayName"))
								{							
									//name.add(e.nextElement().toString());
									displayname=e.nextElement().toString();
								}
								else
								{									
									//list.add(e.nextElement().toString());
									mailid=e.nextElement().toString();
								}
							}							
						}
						//increment the counter	
						if(mailid.length()>0)
							results.put(displayname, mailid);
					}
					// examine the response controls
					cookie = parseControls(ctx.getResponseControls());
					// pass the cookie back to the server for the next page
					ctx.setRequestControls(new Control[]{new PagedResultsControl(pageSize, cookie, Control.CRITICAL) });
				} while ((cookie != null) && (cookie.length != 0));
				searchFilter = "(&(objectClass=*)(displayName="+search+"))";
			}
			ctx.close();
		} 
		catch (NamingException e) {
			System.err.println("Paged Search failed." + e);
		}	
		catch (java.io.IOException e) {
			System.err.println("Paged Search failed." + e);
		} 
	}
//	static byte[] parseControls(Control[] controls) throws NamingException {

//	byte[] cookie = null;

//	if (controls != null)
//	{ 
//	for (int i = 0; i < controls.length; i++)
//	{
//	if (controls[i] instanceof PagedResultsResponseControl)
//	{
//	PagedResultsResponseControl prrc = (PagedResultsResponseControl)controls[i];
//	cookie = prrc.getCookie();
//	System.out.println(">>Next Page \n");
//	}
//	}
//	}

//	return (cookie == null) ? new byte[0] : cookie;
//	}

	public byte[] parseControls(Control[] controls) throws NamingException {

		byte[] cookie = null;

		if (controls != null) {

			for (int i = 0; i < controls.length; i++) {
				if (controls[i] instanceof PagedResultsResponseControl) {
					PagedResultsResponseControl prrc = (PagedResultsResponseControl)controls[i];
					cookie = prrc.getCookie();
					System.out.println(">>Next Page \n");
				}
			}
		}

		return (cookie == null) ? new byte[0] : cookie;
	}

}
