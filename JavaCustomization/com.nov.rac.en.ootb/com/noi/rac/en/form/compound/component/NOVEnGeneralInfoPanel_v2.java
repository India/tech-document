package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.form.compound.util.NOVTextArea;
import com.noi.rac.en.mission.helper.NOVMissionHelper_v2;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.combobox.iComboBox;

public class NOVEnGeneralInfoPanel_v2 extends JPanel {
	private static final long serialVersionUID = 1L;
	private Registry reg;
	public TCComponentForm Enform;// Added by Sandip
	private TCComponentProcess currentProcess = null; // Added by Sandip
	private boolean m_isReleased = false;
	JCheckBox m_mfgViewReqdCheckBox;
	JCheckBox m_docControlReqdCheckBox;
	private iComboBox reasonCombo;
	private JCheckBox m_isENCritical;
	private NOVTextArea reasonForChangeText;
	NOVTextArea changeDescrText;
	/*
	 * public JCheckBox mfgReq; public NOVLOVPopupButton drafted; public
	 * NOVLOVPopupButton checked; public NOVLOVPopupButton approved; public
	 * JComboBox reasonCombo; public NOVDateButton draftedon; public
	 * NOVDateButton checkedon; public NOVDateButton approvedon; public
	 * iTextArea furExplanation ;
	 * 
	 * public NOVEnGenSearchPanel_v2 relatedECRSearchPanel; public
	 * NOVEnGenSearchPanel_v2 projectsPanel; public NOVEnDistributionPanel_v2
	 * distributionPanel;
	 */

	private INOVCustomFormProperties formProps;
	private NOV4ECRDistributionPanel ecrDistributionPanel;
	private NOVEnGenSearchPanel_v2 relatedEcrsPanel;
	private NOVEnGenSearchPanel_v2 selectProjPanel;
	private HashMap<TCComponentForm, String> relatedECRMap = new HashMap<TCComponentForm, String>();
	JTextField ecrSearchField = null;
	JTextField projectSearchField = null;
	
	boolean m_docControlRequired = false;

	public NOVEnGeneralInfoPanel_v2(INOVCustomFormProperties formProperties,
			boolean isReleased) {
		reg = Registry.getRegistry(this);
		this.formProps = formProperties;
		m_isReleased = isReleased;
		
		boolean mfgNotRequiredGroup = NOVMissionHelper_v2.isGroup(reg
				.getString("mfgNotRequiredGroup.NAME"), formProperties);
		
		m_docControlRequired = NOVMissionHelper_v2.isGroup(reg
				.getString("docControlReqdPref.NAME"), formProperties);
		
		JPanel mfgAndDocRequiredPanel = new JPanel(new PropertyLayout());

		m_mfgViewReqdCheckBox = ((JCheckBox) formProps.getProperty("mfgreviewrequired"));
		m_mfgViewReqdCheckBox.setText(reg.getString("mfgReviewReqd.MSG"));
		m_mfgViewReqdCheckBox.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		
		m_docControlReqdCheckBox = ( ( JCheckBox ) formProps.getProperty( "nov4_doccontrol_required" ) );
		m_docControlReqdCheckBox.setText( reg.getString( "docControlRequired.MSG" ) );
		m_docControlReqdCheckBox.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));		
		
		if( !mfgNotRequiredGroup && !m_docControlRequired )
		{
			mfgAndDocRequiredPanel.add("1.1.left.center", m_mfgViewReqdCheckBox);
			mfgAndDocRequiredPanel.add( "1.2.left.center", m_docControlReqdCheckBox );
		}
		else if( !mfgNotRequiredGroup && m_docControlRequired )
		{
			mfgAndDocRequiredPanel.add("1.1.left.center", m_mfgViewReqdCheckBox);
		}
		else if( mfgNotRequiredGroup && !m_docControlRequired )
		{
			mfgAndDocRequiredPanel.add( "1.1.left.center", m_docControlReqdCheckBox );			
		}
		
		if( !mfgNotRequiredGroup && !m_docControlRequired )
		{
			if( m_mfgViewReqdCheckBox.isSelected() )
			{
				m_mfgViewReqdCheckBox.setEnabled( true );
				m_docControlReqdCheckBox.setEnabled( false );
			}
			
			m_mfgViewReqdCheckBox.addItemListener( new ItemListener()
			{
				@Override
				public void itemStateChanged(ItemEvent itemEvent )
				{
					mfgReviewRequiredStateChanged( itemEvent );
				}
			});

			if( m_docControlReqdCheckBox.isSelected() )
			{
				m_docControlReqdCheckBox.setEnabled( true );
				m_mfgViewReqdCheckBox.setEnabled( false );
			}
			
			m_docControlReqdCheckBox.addItemListener( new ItemListener()
			{
				@Override
				public void itemStateChanged( ItemEvent itemEvent )
				{
					docControlRequiredStateChanged( itemEvent );
				}
			});
		}
		
		JPanel pplInfoPanel = buildPeopleInfoPanel();

		JPanel reasonForNoticePanel = new JPanel(new PropertyLayout());
		
		JPanel reasonForNoticePanelFirst = new JPanel(new GridBagLayout());
		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		gbConstraints.weightx = 0.5;
		gbConstraints.insets = new Insets(0, 0, 0, 100);
		gbConstraints.gridx = 0;
		gbConstraints.gridy = 0;
		reasonCombo = ((iComboBox) formProps.getProperty("reason"));
		
		reasonForNoticePanelFirst.add(reasonCombo, gbConstraints);
		
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		gbConstraints.gridx = 1;
		gbConstraints.gridy = 0;

		m_isENCritical = ((JCheckBox) formProps.getProperty("nov4_is_en_critical"));
		m_isENCritical.setText(reg.getString("markENasCritical.TITLE"));
		m_isENCritical.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
	
		reasonForNoticePanelFirst.add(m_isENCritical, gbConstraints);
		
		reasonForNoticePanelFirst.setPreferredSize(new Dimension(600, 25));

		// Start --Adding code to fetch all the targets to get the revision
		// count for disposition issue By Sandip
		JPanel furExpPanel = new JPanel(new PropertyLayout());
		changeDescrText = ((NOVTextArea) formProps.getProperty("explanation"));
		changeDescrText.setRequired(true);
		changeDescrText.setLineWrap(true);
		changeDescrText.setWrapStyleWord(true);
		JScrollPane furExpPane = new JScrollPane(changeDescrText);
		furExpPane.setPreferredSize(new Dimension(810, 120));//TCDECREl-900
		
		try {

			if (formProps.getForm().getTCProperty("reason").getStringValue()
					.length() == 0) {
				// if(isLegacyRel(formProperties))
				// reasonCombo.setSelectedIndex(10);

				// else
				if (isFirstRev(formProperties))
				{
					reasonCombo.setSelectedIndex(7);
				}
				else
					reasonCombo.setSelectedIndex(0);

			}

		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		reasonForChangeText = (NOVTextArea) formProps.getProperty("nov4reason_desc");
		reasonForChangeText.setLineWrap(true);
		reasonForChangeText.setWrapStyleWord(true);
		JScrollPane reasonForChangeTextPane = new JScrollPane(reasonForChangeText);
		reasonForChangeTextPane.setPreferredSize(new Dimension(810, 60));
		TitledBorder titleBorderReason = new TitledBorder(reg.getString("reasonLbl_v2.TITLE"));
		titleBorderReason.setTitleFont(new Font("Dialog", java.awt.Font.BOLD,12));
		reasonForNoticePanel.setBorder(titleBorderReason);

		setMandatoryFields();

		String changeDesc = ( String )reasonCombo.getSelectedItem();
		if( changeDesc.equalsIgnoreCase( reg.getString( "ChangeReason.TXT" ) ) && 
			changeDescrText.getText().trim().length() <= 0 )
		{
			changeDescrText.setText( reg.getString( "ChangeReason.TXT" ) );
		}

		reasonCombo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String selItemName = ( String )reasonCombo.getSelectedItem();
				if( selItemName.equalsIgnoreCase( reg.getString( "ChangeReason.TXT" ) ) && 
					changeDescrText.getText().trim().length() <= 0 )
					changeDescrText.setText( reg.getString( "ChangeReason.TXT" ) );
				else if( !selItemName.equalsIgnoreCase( reg.getString( "ChangeReason.TXT" ) ) && 
						 changeDescrText.getText().equalsIgnoreCase( reg.getString( "ChangeReason.TXT" ) ) )
					changeDescrText.setText( "" );

				setMandatoryFields();
			}
		});

		// reasonForNoticePanel.add(new
		// NOIJLabel(reg.getString("reasonLbl.TITLE")));
		reasonForNoticePanel.add("1.1.left.center", reasonForNoticePanelFirst);
		//reasonForNoticePanel.add("1.2.left.center", m_isENCritical);
		reasonForNoticePanel.add("2.1.left.center", reasonForChangeTextPane);

		reasonForNoticePanel.setPreferredSize(new Dimension(840, 130));

		/*
		 * Sandip TitledBorder tb = null; if(isFirstRev(formProperties)) tb =
		 * new TitledBorder(reg.getString("instructionsForRelease.LABEL")); else
		 * tb = new TitledBorder(reg.getString("explanation.TITLE"));
		 * //TitledBorder tb = new
		 * TitledBorder(reg.getString("instructionsForRelease.LABEL"));
		 */

		// @Mohanar - Reverted Change done by Sandip
		TitledBorder tb = new TitledBorder(reg
				.getString("explanation_v2.TITLE"));
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		furExpPanel.setBorder(tb);

		furExpPanel.add("1.1.left.center", furExpPane);
		furExpPanel.setPreferredSize(new Dimension(840, 160));//TCDECREl-900
		// TODO : Need to add ECR form Type
		relatedEcrsPanel = ((NOVEnGenSearchPanel_v2) formProps.getProperty("relatedecr"));
		relatedEcrsPanel.addUICtrl(true);
		relatedEcrsPanel.setTitleBorderText(reg
				.getString("relatedECRs_v2.TITLE"));
		relatedEcrsPanel.setSearchBtnText(reg.getString("ECRSEARCHBTN.LABEL"));
		// relatedEcrsPanel.addECRLabelPanel( reg.getString( "ECR.LABEL" ) );
		relatedEcrsPanel.setSourceLabel(reg.getString("ECR.LABEL"));
		relatedEcrsPanel.setSearchFieldText(reg
				.getString("ECRsNumberToolTip.TXT"));
		// added by sachin aug10
		relatedEcrsPanel.setPreferredSize(new Dimension(840, 180));//TCDECREl-900

		final JTextField ecrSearchField = relatedEcrsPanel.getSearchField();
		ecrSearchField.setColumns(14);
		ecrSearchField.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent focusevent) {
				if (focusevent.getSource().equals(ecrSearchField)) {
					if (ecrSearchField.getText().equalsIgnoreCase("")) {
						ecrSearchField.setText(reg
								.getString("ECRsNumberToolTip.TXT"));
					}
				}
			}

			public void focusGained(FocusEvent focusevent) {
				if (focusevent.getSource().equals(ecrSearchField)) {
					if (ecrSearchField.getText().equalsIgnoreCase(
							reg.getString("ECRsNumberToolTip.TXT"))) {
						ecrSearchField.setText("");
					}
				}
			}
		});

		ecrSearchField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				String strSrchFieldText = ecrSearchField.getText();
				int lenght = strSrchFieldText.length();
				if (!strSrchFieldText.equalsIgnoreCase(reg
						.getString("ECRsNumberToolTip.TXT"))
						&& lenght > 0)
					relatedEcrsPanel.setSearchButtonState(true);
				else
					relatedEcrsPanel.setSearchButtonState(false);

			}
		});

		try {
			String[] relatedECRArr = (String[]) formProps.getForm()
					.getTCProperty("relatedecr").getStringValueArray();
			relatedEcrsPanel.populateTargetList(relatedECRArr);
		} catch (TCException tce) {
			tce.printStackTrace();
		}

		if (m_isReleased)
			relatedEcrsPanel.setControlState(m_isReleased);

		/*
		 * TCPreferenceService prefrenceSrv =
		 * formProps.getForm().getSession().getPreferenceService(); boolean
		 * isLocale = prefrenceSrv.isTrue(TCPreferenceService.TC_preference_all,
		 * "NOV_show_localization_button"); if (isLocale) { ImageIcon icon =
		 * reg.getImageIcon("locale.IMG"); final TCProperty furExplanation =
		 * formProps.getImanProperty(formProps.getProperty("explanation"));
		 * final JButton localeBtn = new JButton(icon);
		 * localeBtn.setPreferredSize(new Dimension(20,20));
		 * localeBtn.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent actEvt) { NOVLocaleDialog localeDlg = new
		 * NOVLocaleDialog(furExplanation); //localeDlg.setSize(350, 325); int x
		 * = localeBtn.getLocationOnScreen().x- localeDlg.getWidth(); int y =
		 * localeBtn.getLocationOnScreen().y- localeDlg.getHeight();
		 * localeDlg.setLocation(x, y); localeDlg.setVisible(true); } });
		 * furExpPanel.add("1.2.top.center",localeBtn); }
		 */

		selectProjPanel = ((NOVEnGenSearchPanel_v2) formProps.getProperty("projects"));
		selectProjPanel.addUICtrl(false);
		selectProjPanel.setTitleBorderText(reg.getString("selPrj.TITLE"));
		selectProjPanel.setSearchBtnText(reg.getString("SEARCHBUTTON.LABEL"));
		selectProjPanel.setSourceLabel(reg.getString("AVAILABLE.LABEL"));
		selectProjPanel.setSearchFieldText(reg.getString("SEARCHFIELD.TXT"));
		// added by sachin aug10
		/*
		 * System.out.println("get Pre height "+selectProjPanel.getPreferredSize(
		 * ).height);
		 * System.out.println("get Pre width "+selectProjPanel.getPreferredSize
		 * ().width);
		 */
		selectProjPanel.setPreferredSize(new Dimension(840, 180));//TCDECREl-900

		final JTextField prjSearchField = selectProjPanel.getSearchField();
		prjSearchField.setColumns(14);
		prjSearchField.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent focusevent) {
				if (focusevent.getSource().equals(prjSearchField)) {
					if (prjSearchField.getText().equalsIgnoreCase("")) {
						prjSearchField
								.setText(reg.getString("SEARCHFIELD.TXT"));
					}
				}
			}

			public void focusGained(FocusEvent focusevent) {
				if (focusevent.getSource().equals(prjSearchField)) {
					if (prjSearchField.getText().equalsIgnoreCase(
							reg.getString("SEARCHFIELD.TXT"))) {
						prjSearchField.setText("");
					}
				}
			}
		});

		prjSearchField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				String strSrchFieldText = prjSearchField.getText();
				int lenght = strSrchFieldText.length();
				if (!strSrchFieldText.equalsIgnoreCase(reg
						.getString("SEARCHFIELD.TXT"))
						&& lenght > 0)
					selectProjPanel.setSearchButtonState(true);
				else
					selectProjPanel.setSearchButtonState(false);
			}
		});

		if (m_isReleased)
			selectProjPanel.setControlState(m_isReleased);

		NOVEnDistributionPanel_v2 enDistributionPanel = (NOVEnDistributionPanel_v2) formProps
				.getProperty("distribution");
		enDistributionPanel.setTitleBorderText(reg.getString("selDistrib.TITLE"));
		enDistributionPanel.setControlState(m_isReleased);

		ecrDistributionPanel = (NOV4ECRDistributionPanel) formProps.getProperty("nov4manual_distributions");
		ecrDistributionPanel.ecrDistributionListScroll.setPreferredSize(new Dimension(220, 120));
		NOIJLabel ecrDistributionLbl = new NOIJLabel(reg.getString("ECRDIST.LABEL"));

		// UI Change in distribution panel for EN Sachin Kabade
		JPanel blankPanel = new JPanel();
		blankPanel.setPreferredSize(new Dimension(20, 0));

		JPanel ecrPanel = new JPanel(new PropertyLayout());
		ecrPanel.add("1.1.center.center", ecrDistributionLbl);
		ecrPanel.add("2.1.right.center", ecrDistributionPanel);
		enDistributionPanel.add("1.4.right.center",blankPanel);
		enDistributionPanel.add("1.5.right.center",ecrPanel);
		enDistributionPanel.setPreferredSize(new Dimension(840, 220));
		
	/*	JPanel distPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
		distPanel.add("1.1.left.center", enDistributionPanel);*/
		//distPanel.add("1.2.left.center", ecrPanel);
		/*distPanel.setBorder(new TitledBorder(""));// added on aug 16
		distPanel.setPreferredSize(new Dimension(840, 240));// added on aug 16
	*/
		setLayout(new PropertyLayout(5, 5, 5, 5, 5, 5));

		if( !mfgNotRequiredGroup || !m_docControlRequired )
		{
			//this.add("1.1.left.center", ((JCheckBox) formProps.getProperty("mfgreviewrequired")));
			this.add("1.1.left.center", mfgAndDocRequiredPanel);
			this.add("2.1.left.center", pplInfoPanel);
			this.add("3.1.left.center", reasonForNoticePanel);
			// this.add("4.1.left.center",((NOVEnGenSearchPanel_v2)formProps.getProperty("relatedecr")));
			this.add("4.1.left.center", furExpPanel);
			this.add("5.1.left.center", relatedEcrsPanel);
			this.add("6.1.left.center", ((NOVEnGenSearchPanel_v2) formProps.getProperty("projects")));
			this.add("7.1.left.center", enDistributionPanel);
			// this.setBorder(new TitledBorder(""));
			if (m_isReleased)
			{
				m_mfgViewReqdCheckBox.setEnabled(false);
				//if( m_docControlRequired )
				{
					m_docControlReqdCheckBox.setEnabled(false);
				}
			}
		} else {
			this.add("1.1.left.center", pplInfoPanel);
			this.add("2.1.left.center", reasonForNoticePanel);
			// this.add("4.1.left.center",((NOVEnGenSearchPanel_v2)formProps.getProperty("relatedecr")));
			this.add("3.1.left.center", furExpPanel);
			this.add("4.1.left.center", relatedEcrsPanel);
			this.add("5.1.left.center", ((NOVEnGenSearchPanel_v2) formProps.getProperty("projects")));
			this.add("6.1.left.center", enDistributionPanel);

		}

		if (m_isReleased) {
			// mfgViewReqdCheckBox.setEnabled( false );
			reasonForNoticePanel.setEnabled(false);
			reasonForChangeText.setEnabled(false);
			changeDescrText.setEnabled(false);
			reasonCombo.setEnabled(false);
			m_isENCritical.setEnabled(false);
			furExpPanel.setEnabled(false);
		}
	}

	private void mfgReviewRequiredStateChanged( ItemEvent itemEvent )
	{
		if( itemEvent.getStateChange()== ItemEvent.SELECTED )
		{
			m_docControlReqdCheckBox.setEnabled( false );
		}
		else
		{
			m_docControlReqdCheckBox.setEnabled( true );
		}
	}
	
	private void docControlRequiredStateChanged( ItemEvent itemEvent )
	{
		if( itemEvent.getStateChange()== ItemEvent.SELECTED )
		{
			m_mfgViewReqdCheckBox.setEnabled( false );
		}
		else
		{
			m_mfgViewReqdCheckBox.setEnabled( true );
		}
	}

	public void setMandatoryFields() {
		//usha - TCDECREL-1250
		//if (reasonCombo.getSelectedIndex() == 10) {
		String strSelObject = null;
		
		strSelObject = (String) reasonCombo.getSelectedObject(); 
		if (strSelObject != null && strSelObject.equalsIgnoreCase(reg.getString("other.reasonfornotice")) ) {
			reasonForChangeText.setRequired(true);
		} else
			reasonForChangeText.setRequired(false);

		reasonForChangeText.setVisible(false);
		reasonForChangeText.setVisible(true);

		this.validate();
	}


	// returns boolean value for first revision or not, excluding the PRE-RELEASE revisions
	private boolean isFirstRev(INOVCustomFormProperties formProperties) 
	{
		boolean isFirstRev = true;
		Enform = (TCComponentForm) formProperties.getForm();

		try {
			AIFComponentContext[] contexts = Enform.whereReferenced();
			TCComponent[] targets = null;

			if (contexts != null) {

				for (int j = 0; (j < contexts.length) && (isFirstRev == true); j++)
				{
					if ((TCComponent) contexts[j].getComponent() instanceof TCComponentProcess)
					{
						currentProcess = (TCComponentProcess) contexts[j].getComponent();
						TCComponentTask rootTask = ((TCComponentProcess) currentProcess).getRootTask();
						targets = rootTask.getAttachments(
								TCAttachmentScope.LOCAL,
								TCAttachmentType.TARGET);
						//System.out.println("got targets");
						break;
					}
				}
			}//modified the contexts & targets looping- usha

			if(targets != null)
			{
				for (int i = 0; (i < targets.length)
						&& (isFirstRev == true); i++) {
					if (targets[i] instanceof TCComponentItemRevision) {
						TCComponentItem item = (TCComponentItem) targets[i]
								.getRelatedComponent("items_tag");
						TCComponent[] itemRev = (TCComponent[]) item
								.getRelatedComponents("revision_list");

								int iNewRev = 0;
								String[] reqProprties = { "item_revision_id" };
								List<TCComponent> itemRevList = Arrays.asList(itemRev); 
								//String[][] retItemRevIds = TCComponentType.getPropertiesSet( itemRev, reqProprties );
								String[][] retItemRevIds = TCComponentType.getPropertiesSet( itemRevList, reqProprties );//TC10.1 Upgrade
								
								for( int iRev = 0; iRev < itemRev.length; ++iRev )
								{
									//String sRevId = ((TCComponentItemRevision)itemRev[iRev]).getProperty("item_revision_id");
									
									int ind = retItemRevIds[iRev][0].indexOf("PRE-RELEASE");
									if(ind < 0)
									{
										iNewRev += 1;
									}
								}
								if (iNewRev > 1)
								{
									isFirstRev = false;
									break;
								}
							}
						}
				}//targets

		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return isFirstRev;
	}

	public void setEcrDistributions(String[] strEcrDistArr)
	{
		ecrDistributionPanel = (NOV4ECRDistributionPanel) formProps
			.getProperty("nov4manual_distributions");

	}

	public boolean isMandatoryFieldsFilled()
	{
		String selItem = ((iComboBox) formProps.getProperty("reason"))
				.getSelectedItem().toString();

		TCComponentListOfValues lovalues = TCComponentListOfValuesType
				.findLOVByName((TCSession) AIFDesktop.getActiveDesktop()
						.getCurrentApplication().getSession(),
						"_RSOne_enreasonforchange_");
		/*ListOfValuesInfo lovInfo = null;
		try {
			lovInfo = lovalues.getListOfValues();
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Object[] lovS = null;
		try {
			lovS = lovalues.getListOfValues().getLOVDisplayValues();
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		boolean flag = false;
		if (lovS.length > 0) {
			for (int i = 0; i < lovS.length; i++) {
				if (lovS[i].toString().equals(selItem)) {
					flag = true;
					break;
				}
			}
		}
		if ((selItem.trim().length() > 0) && (flag == true)) {
			if (selItem.trim().length() > 0) {
				return true;
			}
		}

		return false;
	}

	public boolean isMandatoryOtherFieldFilled() {
		if ((reasonForChangeText.isRequired() && reasonForChangeText.getText()
				.trim().length() <= 0))
			return false;
		else
			return true;
	}

	public boolean isChangeDescFieldFilled() {
		if ((changeDescrText.getText().trim().length() <= 0))
			return false;
		else
			return true;
	}

	public JPanel buildPeopleInfoPanel() {
		
		JPanel pplInfoPanel = new JPanel(new PropertyLayout());
		JPanel leftpplInfoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5,
				5));
		JPanel rightpplInfoPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5,
				5));
		boolean createGroup = NOVMissionHelper_v2.isGroup(reg
				.getString("createPref.NAME"), formProps);
		boolean ercGroup = NOVMissionHelper_v2.isGroup(reg
				.getString("ercPref.NAME"), formProps);
		//TCDECREL-3651
		//TCDECREL-3742
		boolean meGroup = NOVMissionHelper_v2.isGroup(reg
				.getString("mePref.Name"), formProps);
		boolean ceGroup = NOVMissionHelper_v2.isGroup(reg
				.getString("cePref.Name"), formProps);

		Vector<String> m_propVector1 = new Vector<String>();
		Vector<String> m_propVecto2 = new Vector<String>();
		
		if(!createGroup)
		{
			m_propVector1.add("createdby");	m_propVecto2.add("createdon");
		}

		m_propVector1.add("draftedby");	m_propVecto2.add("draftedon");
		m_propVector1.add("checkedby");	m_propVecto2.add("checkedon");
		m_propVector1.add("approvedby");m_propVecto2.add("approvedon");
		
		if(!ercGroup)
		{
			m_propVector1.add("nov4ercapprovedby");	m_propVecto2.add("nov4ercapproveddate");
		}

		if(!meGroup)
		{
			m_propVector1.add("nov4_me_approver");	m_propVecto2.add("nov4_me_approved_date");
		}

		if(!ceGroup)
		{
			m_propVector1.add("nov4_ce_approver");	m_propVecto2.add("nov4_ce_approved_date");
		}

		String props1[] = m_propVector1.toArray(new String[m_propVector1.size()]);
		String props2[] = m_propVecto2.toArray(new String[m_propVecto2.size()]);
		

		((NOVLOVPopupButton) formProps.getProperty("createdby")).setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("createdby")).setPreferredSize(new Dimension(290, 21));
		((NOVDateButton) formProps.getProperty("createdon")).setEnabled(false);
		((NOVDateButton) formProps.getProperty("createdon")).setPreferredSize(new Dimension(290, 21));
		
		((NOVLOVPopupButton) formProps.getProperty("draftedby"))
				.setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("draftedby"))
				.setPreferredSize(new Dimension(290, 21));
		((NOVLOVPopupButton) formProps.getProperty("checkedby"))
				.setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("checkedby"))
				.setPreferredSize(new Dimension(290, 21));
		((NOVLOVPopupButton) formProps.getProperty("approvedby"))
				.setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("approvedby"))
				.setPreferredSize(new Dimension(290, 21));

		// Below 2 lines added by sandip
		// @Mohanar - ERC new attributes
		((NOVLOVPopupButton) formProps.getProperty("nov4ercapprovedby"))
				.setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("nov4ercapprovedby"))
				.setPreferredSize(new Dimension(290, 21));
		//TCDECREL-3651
		((NOVLOVPopupButton) formProps.getProperty("nov4_me_approver"))
			.setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("nov4_me_approver"))
			.setPreferredSize(new Dimension(290, 21));
		//TCDECREL-3742
		((NOVLOVPopupButton) formProps.getProperty("nov4_ce_approver"))
			.setEnabled(false);
		((NOVLOVPopupButton) formProps.getProperty("nov4_ce_approver"))
			.setPreferredSize(new Dimension(290, 21));

		((NOVDateButton) formProps.getProperty("draftedon")).setEnabled(false);
		((NOVDateButton) formProps.getProperty("draftedon"))
				.setPreferredSize(new Dimension(290, 21));
		((NOVDateButton) formProps.getProperty("checkedon")).setEnabled(false);
		((NOVDateButton) formProps.getProperty("checkedon"))
				.setPreferredSize(new Dimension(290, 21));
		((NOVDateButton) formProps.getProperty("approvedon")).setEnabled(false);
		((NOVDateButton) formProps.getProperty("approvedon"))
				.setPreferredSize(new Dimension(290, 21));

		// Below 2 lines added by sandip
		// @Mohanar - ERC new attributes
		((NOVDateButton) formProps.getProperty("nov4ercapproveddate"))
				.setEnabled(false);
		((NOVDateButton) formProps.getProperty("nov4ercapproveddate"))
				.setPreferredSize(new Dimension(290, 21));
		//TCDECREL-3651
		((NOVDateButton) formProps.getProperty("nov4_me_approved_date"))
			.setEnabled(false);
		((NOVDateButton) formProps.getProperty("nov4_me_approved_date"))
			.setPreferredSize(new Dimension(290, 21));
		//TCDECREL-3742
		((NOVDateButton) formProps.getProperty("nov4_ce_approved_date"))
			.setEnabled(false);
		((NOVDateButton) formProps.getProperty("nov4_ce_approved_date"))
			.setPreferredSize(new Dimension(290, 21));

		//TCDECREL-3651 & TCDECREL-3742
	
		for (int i = 0; i < props1.length; i++)
		{
			String sVertIndex = String.valueOf(i+1);

			leftpplInfoPanel.add(sVertIndex+".1.left.center", new NOIJLabel(reg.getString(props1[i]+".LABEL")));
			leftpplInfoPanel.add(sVertIndex+".2.left.center", ((NOVLOVPopupButton) formProps.getProperty(props1[i])));
			
			rightpplInfoPanel.add(sVertIndex+".1.left.center", new NOIJLabel(reg.getString(props2[i]+".LABEL")));
			rightpplInfoPanel.add(sVertIndex+".2.left.center", ((NOVDateButton) formProps.getProperty(props2[i])));			
		}

		pplInfoPanel.add("1.1.left.center", leftpplInfoPanel);
		pplInfoPanel.add("1.2.left.center", rightpplInfoPanel);

		return pplInfoPanel;
	}



	
}