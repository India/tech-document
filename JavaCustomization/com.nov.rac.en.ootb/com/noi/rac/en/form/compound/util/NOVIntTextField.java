/*================================================================================
                             Copyright (c) 2009 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : IntTextField.java
 Package Name: com.nov.rac.commands.newitem
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         2009/02/24    HarshadaM     File added in SVN
 ================================================================================*/

package com.noi.rac.en.form.compound.util;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

public class NOVIntTextField extends JTextField
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public NOVIntTextField(int defval, int size)
    {
        super("001" , size);
    }

    public NOVIntTextField(int size)
    {
        super("" , size);
    }

    protected Document createDefaultModel()
    {
        return new IntTextDocument();
    }

    public boolean isValid()
    {
        try
        {
            Integer.parseInt(getText());
            return true;
        }
        catch ( NumberFormatException e )
        {
            return false;
        }
    }

    public int getValue()
    {
        try
        {
            if ( getText().length() <= 3 )
                return Integer.parseInt(getText());
            else
                return 0;
        }
        catch ( NumberFormatException e )
        {
            return 0;
        }
    }

    class IntTextDocument extends PlainDocument
    {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public void insertString(int offs , String str , AttributeSet a )
                throws BadLocationException
        {
            if ( str == null ) return;
            String oldString = getText(0, getLength());
            String newString = oldString.substring(0, offs) + str
                    + oldString.substring(offs);
            try
            {
                Integer.parseInt(newString + "0");
                super.insertString(offs, str, a);
            }
            catch ( NumberFormatException e )
            {
            }
        }
    }
}
