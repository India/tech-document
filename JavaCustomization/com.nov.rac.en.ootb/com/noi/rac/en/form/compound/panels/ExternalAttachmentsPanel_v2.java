package com.noi.rac.en.form.compound.panels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.table.*;

import com.noi.rac.commands.newdataset.NewDatasetDialog;
import com.noi.rac.en.NationalOilwell;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.Miscellaneous;
import com.noi.rac.en.util.components.ExternalAttachmentsTableModel;
import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.teamcenter.rac.aif.AbstractAIFApplication;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;

public class ExternalAttachmentsPanel_v2 extends JPanel implements ActionListener,ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel parentPanel;
	JTable theTable = new JTable();
	JScrollPane tablePane;
	JButton attachmentsBoxPlus;
	JButton attachmentsBoxMinus;
	JButton viewAttachment;
	
	ArrayList tableData = new ArrayList();
	Vector    theData = new Vector();
	AbstractAIFApplication app=null;

	Vector attachments = new Vector();
	private Registry registry;
	ExternalAttachmentsTableModel theModel=null;
	int selectedItemRow=-1;
    INOVCustomFormProperties fp; 
    boolean isFormLocked=false;
	
	public ExternalAttachmentsPanel_v2(INOVCustomFormProperties fp,boolean locked, JPanel parentPanel ) {
		super();
		this.parentPanel = parentPanel;
		isFormLocked = locked;
		this.fp = fp;
		registry = Registry.getRegistry(this);
		try {
			//TCComponent[] comps=fp.getForm().getRelatedComponents("_CustomSubForm_");
			String[] relation={registry.getString("SUBFORM.RELATION")};
	        AIFComponentContext[] comps = fp.getForm().whereReferencedByTypeRelation(new String[0], relation);
	        for (int i=0;i<comps.length;i++) 
	        {
	            if ((comps[i].getComponent()) instanceof TCComponentDataset) 
	            {
	                attachments.add(comps[i].getComponent());
	            }
	        }
	    	theModel = new ExternalAttachmentsTableModel(attachments);
		}
		catch (Exception e) {
			
		}
		setInitialData(new ArrayList());
		createUI();	
	}


	public TableModel getTableModel() { return theTable.getModel(); }
	
    public void setInitialData(ArrayList al) {
    	
    	if (attachments.size() > 0) return;
    	
    	tableData = al;
    	for (int i=0;i<tableData.size();i++)
    		theData.add(tableData.get(i));
    	theModel = new ExternalAttachmentsTableModel(theData);
    }
	
    public void createUI() {
    	
    	registry = Registry.getRegistry(this);
    	
    	//setLayout(new GridBagLayout());
    	//setBackground(new Color(225,225,225));
    	setPreferredSize(new Dimension(845,180));
    	TitledBorder tb = new javax.swing.border.TitledBorder(registry.getString("ExternalAttachmentsPanel.ReferenceAttachments"));
    	tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
    	setBorder(tb);

        ListSelectionModel rowSM = theTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        theTable.setModel(theModel);
        theTable.setPreferredScrollableViewportSize(new Dimension(575,200));
        TableColumn col = null;
        
        col = theTable.getColumnModel().getColumn(0);
        int newwidth = 80;
        col.setPreferredWidth(newwidth);
        
        col = theTable.getColumnModel().getColumn(1);
        col.setCellEditor(null);
        newwidth = 150;
        col.setPreferredWidth(newwidth);
    	
        JTableHeader head = theTable.getTableHeader();
        head.setBackground(NationalOilwell.NOVPanelContrast);
        
        tablePane = new JScrollPane(theTable);
        
        ImageIcon plusIcon=null;
        try {
        	//Registry.getImageIconFromPath
         plusIcon = registry.getImageIcon("enadd.IMG");
        	//plusIcon = new ImageIcon(Miscellaneous.getImageIcon(registry.getString("enadd.IMG")));
        }
        catch (Exception ie) {
        	System.out.println(ie);
        }
        ImageIcon minusIcon = registry.getImageIcon("enremove.IMG");
		//ImageIcon minusIcon = new ImageIcon(Miscellaneous.getImageIcon(registry.getString("enremove.IMG")));
        
		attachmentsBoxPlus = new JButton(plusIcon);
		attachmentsBoxMinus = new JButton(minusIcon);
		viewAttachment = new JButton(registry.getString("ExternalAttachmentsPanel.ViewSelectedDocument"));
		try
		{
			isFormLocked=fp.getForm().getLogicalProperty("locked");
		}
		catch(Exception e)
		{
			
		}
		
		viewAttachment.setVisible(false);
		attachmentsBoxPlus.addActionListener(this);
		attachmentsBoxMinus.addActionListener(this);
		viewAttachment.addActionListener(this);
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		tablePane.setToolTipText(registry.getString("AdditionalInfo.MSG"));
		tablePane.setPreferredSize(new Dimension(800,80));
        this.add(tablePane,gbc);
        
        JPanel bpanel = new JPanel();
        //bpanel.setBackground(new Color(225,225,225));
        attachmentsBoxPlus.setToolTipText(registry.getString("ExternalAttachmentsPanel.AddButtonToolTip"));
        attachmentsBoxMinus.setToolTipText(registry.getString("ExternalAttachmentsPanel.RemoveButtonToolTip"));
        bpanel.add(attachmentsBoxPlus);
        bpanel.add(attachmentsBoxMinus);
        bpanel.add(viewAttachment);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        this.add(bpanel,gbc);
        //resizePanel();
    }

	public void setControlState( boolean isReleased )
	{
    	//theTable.setEnabled( !isReleased );
    	attachmentsBoxPlus.setEnabled( !isReleased );
    	attachmentsBoxMinus.setEnabled( !isReleased );
	}
    
    public void hideAttachmentsPlusMinus()
    {
    	if( attachmentsBoxPlus != null )
    	{
    		attachmentsBoxPlus.hide();
    	}
    	if( attachmentsBoxMinus != null )
    	{
    		attachmentsBoxMinus.hide();
    	}
    }

    public void actionPerformed(ActionEvent event) {
    	if (event.getSource() == attachmentsBoxPlus && !isFormLocked) {
//    		gurrama: NewDatasetDialog class ismodifed for new ecnero form
    		NewDatasetDialog dlg = new NewDatasetDialog(AIFUtility.getCurrentApplication().getDesktop(), AIFUtility.getCurrentApplication(),
    				                fp.getForm(),false);
    		dlg.setModal(true);
    		dlg.setLocationRelativeTo(this);
    		if(Toolkit.getDefaultToolkit().getScreenResolution()>96)
    		{
    		dlg.setSize(600, 220);
    		}
    		else
    		{
    			dlg.setSize(500, 220);
    		}
    		dlg.setDsName(registry.getString("DEFLTATTACH.NAME"));
    		//dlg.show();
    		dlg.setVisible(true);
    		if (dlg.getDataset() != null) {
    		    Vector v = new Vector();
    		    v.add(dlg.getDataset());
    		    ((ExternalAttachmentsTableModel)theTable.getModel()).addRows(v);
    		}
    		fp.saveFormData();
    	}
    	else if (event.getSource() == attachmentsBoxMinus && !isFormLocked)
    	{
    		if(theTable.getModel().getRowCount()>0)
    		{
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
    			{
	    			TCComponentDataset ds = 
	    				((ExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
	    			TCComponentForm form = (TCComponentForm)fp.getForm();
	    			try {
	        			AIFComponentContext[] frel = form.getRelated();
	        			AIFComponentContext[] drel = ds.getRelated();
	        			
	        			DeleteRelationHelper delete = new DeleteRelationHelper(ds,
	                            form, registry.getString("SUBFORM.RELATION"));
	                    delete.deleteRelation();
	                    
	        			fp.saveFormData();
	    			}
	    			catch (Exception e) {
	    				System.out.println("Could not remove attachment: "+e);
	    			}
	    			
	    			((ExternalAttachmentsTableModel)theTable.getModel()).removeRow(selectedItemRow);
    			}
    		}		
    	}
    	else if (event.getSource() == viewAttachment)
    	{
    		if(theTable.getModel().getRowCount()>0){
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
    			{
    				TCComponentDataset ds =((ExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
    				try 
    				{

    					String   cmd="";
    					String dstype = ds.getProperty("object_type");

    					String fetchFileType = "";
    					fetchFileType = getFileRefType(ds);
    					/*
             			else if (dstype.equalsIgnoreCase("acaddwg"))
             			{
          	   				fname = ds.getFileNames("DWG");
          	   				fetchFileType ="DWG";
             			}
    					 */
    					if(null!=fetchFileType && fetchFileType.length()>0)
    					{   
    						/* if (dstype.equalsIgnoreCase("pdf"))
              					{
           	   						fetchFileType ="PDF_Reference";
              					}
              				*/

    						System.out.println("fetchFileType ***** "+fetchFileType);
    						/*String fileName = null;
    						File[] fl =ds.getFiles(fetchFileType);
    						if(fl!=null)
    						{
    							fileName = fl[0].getAbsolutePath();

    							if (fileName!=null && fileName.length() > 0) 
    							{
    								System.out.println("File name : "+ fl[0].getAbsolutePath());
    								cmd = "rundll32 shell32.dll,OpenAs_RunDLL "+ fileName;// windPath+"\\temp\\"+newName;
    								Runtime.getRuntime().exec(cmd);
    							}
    						}*/
    						
    						//If the file name contains multiple continuous spaces then file is not opening with above logic . so using OOTB dataset open method
    						ds.open();
    					}
    					else
    					{
    						MessageBox.post(registry.getString("Reference2ViewMissing.MSG"),registry.getString("ErrorDocView.MSG"),MessageBox.INFORMATION);
    					}
    				}
    				catch (Exception e) {
    					e.printStackTrace();
    					MessageBox.post(registry.getString("FailedonRequiredFun.MSG"),e.toString(),registry.getString("ErrorDocView.MSG"),MessageBox.INFORMATION);
    				}
    			} 
    		}
    	}
		//resizePanel();
	
    }
    
    
    public String getFileRefType(TCComponentDataset dataset)
    {
    	String strRefType=null;
    	String datasetType = dataset.getType();
    	try 
    	{
    		strRefType = dataset.getProperty("ref_names");
    		
    		if( strRefType!=null && strRefType.indexOf(",")!=-1 )
    			strRefType = strRefType.split(",")[0];
    		
    		return strRefType;
    		
			/*NamedReferenceContext[] namedRefType = dataset
					.getDatasetDefinitionComponent()
					.getNamedReferenceContexts();
			
			Map<String, String> mapNamedRefType = new HashMap<String, String>();
			for (int index = 0; index < namedRefType.length; index++) 
			{
				mapNamedRefType.put(namedRefType[index].getFileTemplate(),
						namedRefType[index].getNamedReference());
			}

			TCComponent[] imanFileObj = ((TCComponentDataset) dataset)
					.getNamedReferences();
			dataset.getProperty("ref_names");
			String strFileName;
			if(imanFileObj!=null && imanFileObj.length>0)
			{
				strFileName = ((TCComponentTcFile) imanFileObj[0]).getTCProperty(
						"original_file_name").toString();
	
				if (strFileName != null && strFileName.indexOf(".")!=-1) 
				{
					if(mapNamedRefType.size()==1)
						strRefType = (String) mapNamedRefType.get("*");
					else
					{
						String strExtension = strFileName.substring(strFileName
								.indexOf("."), strFileName.length());
						strRefType = (String) mapNamedRefType.get("*" + strExtension);
					}
				}
			}*/
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strRefType;
    }
    
	public void valueChanged(ListSelectionEvent e) {
		
        if (e.getValueIsAdjusting()) return;

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
        	;
        } else {
            selectedItemRow = lsm.getMinSelectionIndex();
            viewAttachment.setVisible(true);
            viewAttachment.setEnabled(true);
        }
		
	}
    public void resizePanel() {
    	
        TableModel tm = theTable.getModel();
        int tableHeight = (tm.getRowCount()+1)*18;
        if (tm.getRowCount() <= 0){
        	tablePane.setVisible(false); 
            viewAttachment.setVisible(false);
            viewAttachment.setEnabled(false);

        }
        else {
        	tablePane.setVisible(true);
        	if(selectedItemRow >= 0 && selectedItemRow < tm.getRowCount()){
        		viewAttachment.setVisible(true);
        		viewAttachment.setEnabled(true);
        	}
        	else{
                viewAttachment.setVisible(false);
                viewAttachment.setEnabled(false);
        	}
        }
        if (tableHeight > 100) tableHeight=100;
        if (tableHeight <= 54) tableHeight+=4;
        
        tablePane.setPreferredSize(new Dimension(600,tableHeight));
        this.setPreferredSize(new Dimension(630,tableHeight+70));
       this.setMinimumSize(new Dimension(630,tableHeight+70));
       parentPanel.revalidate();
       parentPanel.repaint();
    	
    }
}
