package com.noi.rac.en.form.compound.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.noi.rac.en.NationalOilwell;
import com.noi.rac.en.customize.util.MessageBox;
import com.noi.rac.en.form.compound.panels._StopForm_Panel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.nov.rac.en.commands.NOVENPrintCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.noi.rac.en.util.components.*;

public class NOVStopGeneralInfoPanel extends JPanel 
implements ActionListener
{

	private static final long serialVersionUID = 1L;
	
	
	private INOVCustomFormProperties formProps;
 protected Registry reg;
	//2064-usha
	private JButton helpSTOP;
	private JButton	printSTOP;
	String helpUrl = "";
	private TCPreferenceService prefServ ;
	private Component comp = null;
	public _StopForm_Panel stopFormPanel;
	public NOVStopGeneralInfoPanel(_StopForm_Panel stopFormPanel, INOVCustomFormProperties formProperties)
	{
		this.formProps =formProperties;
		reg = Registry.getRegistry(this);
		this.stopFormPanel  = stopFormPanel;
		//2064 - usha
//		JLabel engSTOPOrder = new JLabel(reg.getString("EngSTOPOrder.LBL"));
//		engSTOPOrder.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		
		JPanel pplInfoPanel = buildPeopleInfoPanel();
		
		setLayout(new PropertyLayout());
		//2064 - usha
		this.add("1.1.center.center",pplInfoPanel);
		
	}
	
	public JPanel buildPeopleInfoPanel()
	{
		JPanel pplInfoPanel = new JPanel(new PropertyLayout(5, 5, 5,5, 5,5));
		JPanel leftpplInfoPanel = new JPanel(new PropertyLayout());
		JPanel rightpplInfoPanel = new JPanel(new PropertyLayout());
		leftpplInfoPanel.setPreferredSize(new Dimension(320, 55));
		rightpplInfoPanel.setPreferredSize(new Dimension(320, 55));
		
		((NOVLOVPopupButton)formProps.getProperty("draftedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("draftedby")).setPreferredSize(new Dimension(215,20));
		((NOVLOVPopupButton)formProps.getProperty("approvedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("approvedby")).setPreferredSize(new Dimension(215,20));
		((NOVDateButton)formProps.getProperty("draftedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("draftedon")).setPreferredSize(new Dimension(200,20));
		((NOVDateButton)formProps.getProperty("approvedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("approvedon")).setPreferredSize(new Dimension(200,20));
      
		leftpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("draftedby.LABEL")));
		leftpplInfoPanel.add("1.2.left.center",((NOVLOVPopupButton)formProps.getProperty("draftedby")));
		leftpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("approvedby.LABEL")));
		leftpplInfoPanel.add("2.2.left.center",((NOVLOVPopupButton)formProps.getProperty("approvedby")));
		rightpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("draftedon.LABEL")));
		rightpplInfoPanel.add("1.2.left.center",((NOVDateButton)formProps.getProperty("draftedon")));
		rightpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("approvedon.LABEL")));
		rightpplInfoPanel.add("2.2.left.center",((NOVDateButton)formProps.getProperty("approvedon")));
		
		pplInfoPanel.setPreferredSize(new Dimension(635, 55));
		pplInfoPanel.add("1.1.left.center",leftpplInfoPanel);
		pplInfoPanel.add("1.2.left.center",rightpplInfoPanel); 
		
		//usha - 2064
		helpSTOP = new JButton(reg.getString("HELPSTR.LABEL"));//"Help");
		helpSTOP.setPreferredSize(new Dimension(70, 25));
		printSTOP = new JButton( reg.getString( "PRINTSTR.LABEL" ) );
		printSTOP.setPreferredSize(new Dimension(70, 25));
		prefServ = stopFormPanel.getSession().getPreferenceService();
		helpUrl = prefServ.getString(
				TCPreferenceService.TC_preference_all,
				"NOI_NewSTOPHelp");

		helpSTOP.addActionListener(this);
		printSTOP.addActionListener( this );
		
		JPanel rightPanel=new JPanel(new PropertyLayout(5 , 5 , 5, 5 , 5 , 5));
		rightPanel.add("1.1.left.top",helpSTOP);
		rightPanel.add("1.2.left.top",printSTOP);
		rightPanel.setPreferredSize(new Dimension(150, 30));
		
		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.add(rightPanel,BorderLayout.LINE_END);
		
		panel1.setPreferredSize(new Dimension(600,30));
		JPanel panel = new JPanel(new PropertyLayout());
		
		panel.add("1.1.left.center", panel1);
		panel.add("2.1.left.center", pplInfoPanel);
		
		return panel;
	}
	
	//2064 - usha
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==helpSTOP)
        {
			if(helpUrl==null || helpUrl.length()==0)
			{
				comp = (JPanel)this.getParent().getParent();				
				MessageBox.post(getWindow(comp), 
						NationalOilwell.SEEADMIN, NationalOilwell.NOHELP, MessageBox.ERROR);
			}
			else
			{
				try 
				{
					Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+helpUrl);
				} 
				catch (Exception ex)
				{
					ex.printStackTrace();
					comp = (JPanel)this.getParent().getParent();
					MessageBox.post(getWindow(comp),
							NationalOilwell.SEEADMIN, NationalOilwell.NOHELP, MessageBox.ERROR);
				}
			}
			return;
		}
		else if( ae.getSource() == printSTOP )
		{
			Frame parent = ( Frame )AIFUtility.getActiveDesktop();
			InterfaceAIFComponent comps[] = ( InterfaceAIFComponent[] )new TCComponent[]{
				formProps.getForm() };
			NOVENPrintCommand cmd = new NOVENPrintCommand( parent, comps );
			return;
		}		
	}
	//2064- usha
	public Window getWindow(Component con)
	{
		Window window = null;
		
		if(con.getParent() instanceof Window)
		{
			window = (Window) con.getParent();
		}
		else
		{
			 window = getWindow(con.getParent());
		}
		
		return window;	
	} 

}
