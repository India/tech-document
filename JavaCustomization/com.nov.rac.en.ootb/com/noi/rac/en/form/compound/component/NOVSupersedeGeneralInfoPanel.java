package com.noi.rac.en.form.compound.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.noi.rac.en.NationalOilwell;
import com.noi.rac.en.customize.util.MessageBox;
import com.noi.rac.en.form.compound.panels.Nov4_SupersedeForm_Panel;
import com.noi.rac.en.form.compound.panels._StopForm_Panel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.nov.rac.en.commands.NOVENPrintCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVSupersedeGeneralInfoPanel extends JPanel implements ActionListener
{

	private static final long serialVersionUID = 1L;
	//2064-usha
	private JButton helpSUP;
	private JButton	printSUP;
	String helpUrl = "";
	private TCPreferenceService prefServ ;
	private Component comp = null;
	public Nov4_SupersedeForm_Panel supFormPanel;
	
	private INOVCustomFormProperties formProps;
 protected Registry reg;
	public NOVSupersedeGeneralInfoPanel(Nov4_SupersedeForm_Panel supFormPanel, INOVCustomFormProperties formProperties)
	{
		this.formProps =formProperties;
		reg = Registry.getRegistry(this);
		this.supFormPanel  = supFormPanel;	
//		JLabel engSTOPOrder = new JLabel(reg.getString("EngSUPOrder.LBL"));
//		engSTOPOrder.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		
		JPanel pplInfoPanel = buildPeopleInfoPanel();
		
		setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		
//		this.add("1.1.left.center",engSTOPOrder);
//		this.add("2.1.left.center",pplInfoPanel);
		this.add("1.1.left.center",pplInfoPanel);
		
	}
	
	public JPanel buildPeopleInfoPanel()
	{
		JPanel pplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		JPanel leftpplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		JPanel rightpplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		
		leftpplInfoPanel.setPreferredSize(new Dimension(300, 80));
		rightpplInfoPanel.setPreferredSize(new Dimension(300, 80));
		
		((NOVLOVPopupButton)formProps.getProperty("nov4_draftedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("nov4_draftedby")).setPreferredSize(new Dimension(200,21));
		((NOVLOVPopupButton)formProps.getProperty("nov4_approvedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("nov4_approvedby")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("nov4_draftedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("nov4_draftedon")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("nov4_approvedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("nov4_approvedon")).setPreferredSize(new Dimension(200,21));
      
		leftpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("draftedby.LABEL")));
		leftpplInfoPanel.add("1.2.left.center",((NOVLOVPopupButton)formProps.getProperty("nov4_draftedby")));
		leftpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("approvedby.LABEL")));
		leftpplInfoPanel.add("2.2.left.center",((NOVLOVPopupButton)formProps.getProperty("nov4_approvedby")));
		
		rightpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("draftedon.LABEL")));
		rightpplInfoPanel.add("1.2.left.center",((NOVDateButton)formProps.getProperty("nov4_draftedon")));
		rightpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("approvedon.LABEL")));
		rightpplInfoPanel.add("2.2.left.center",((NOVDateButton)formProps.getProperty("nov4_approvedon")));
		
		pplInfoPanel.add("1.1.left.center",leftpplInfoPanel);
		pplInfoPanel.add("1.2.left.center",rightpplInfoPanel);
		
		//usha - 2064
		helpSUP = new JButton(reg.getString("HELPSTR.LABEL"));//"Help");
		helpSUP.setPreferredSize(new Dimension(70,25));
		printSUP = new JButton( reg.getString( "PRINTSTR.LABEL" ) );
		printSUP.setPreferredSize(new Dimension(70,25));
		prefServ = supFormPanel.getSession().getPreferenceService();
		helpUrl = prefServ.getString(
				TCPreferenceService.TC_preference_all,				
				"NOI_NewSupersedeHelp");

		helpSUP.addActionListener(this);
		printSUP.addActionListener( this );
		
		JPanel rightPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		rightPanel.add("1.1.right.top",helpSUP);
		rightPanel.add("1.2.right.top",printSUP);
		rightPanel.setPreferredSize(new Dimension(160, 25));
		
		pplInfoPanel.setPreferredSize(new Dimension(630,60));
		
		JPanel panel1 = new JPanel(new BorderLayout());
		//panel1.add(leftPanel,BorderLayout.LINE_START);
		panel1.add(rightPanel,BorderLayout.LINE_END);
		
		panel1.setPreferredSize(new Dimension(640,30));
		//JPanel panel = new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		JPanel panel = new JPanel(new PropertyLayout());
		
		panel.add("1.1.left.center", panel1);
		panel.add("2.1.left.center", pplInfoPanel);
		
		return panel;
		
		//return pplInfoPanel;
	}
	
	
	//2064 - usha
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==helpSUP)
        {
			if(helpUrl==null || helpUrl.length()==0)
			{
				comp = (JPanel)this.getParent().getParent();
				
				MessageBox.post(getWindow(comp), 
						NationalOilwell.SEEADMIN, NationalOilwell.NOHELP, MessageBox.ERROR);
			}
			else
			{
				try 
				{
					Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+helpUrl);
				} 
				catch (Exception ex)
				{
					ex.printStackTrace();
					comp = (JPanel)this.getParent().getParent();
					MessageBox.post(getWindow(comp),
							NationalOilwell.SEEADMIN, NationalOilwell.NOHELP, MessageBox.ERROR);
				}
			}
			return;
		}
		else if( ae.getSource() == printSUP )
		{
			Frame parent = ( Frame )AIFUtility.getActiveDesktop();
			InterfaceAIFComponent comps[] = ( InterfaceAIFComponent[] )new TCComponent[]{
				formProps.getForm() };
			NOVENPrintCommand cmd = new NOVENPrintCommand( parent, comps );
			return;
		}		
	}
	//2064- usha
	public Window getWindow(Component con)
	{
		Window window = null;
		
		if(con.getParent() instanceof Window)
		{
			window = (Window) con.getParent();
		}
		else
		{
			 window = getWindow(con.getParent());
		}
		
		return window;	
	} 

}

