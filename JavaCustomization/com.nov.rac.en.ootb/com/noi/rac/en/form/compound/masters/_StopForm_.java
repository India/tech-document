package com.noi.rac.en.form.compound.masters;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.noi.rac.en.form.compound.data._StopForm_DC;
import com.noi.rac.en.form.compound.panels._StopForm_Panel;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.util.Registry;

public class _StopForm_ extends BaseMasterForm
{

	_StopForm_DC stopDC;
	_StopForm_Panel stopPanel;
	String masterFormName="";
	static boolean once=false;
	
	
	//TODO : Need to implement lockForm,setDisplayProperties
	public _StopForm_(TCComponentForm master)
	{
		super();
		
		masterForm = master;
		
		try 
		{
			masterFormName = masterForm.getProperty("object_name");
		}
		catch (Exception e) 
		{
			System.out.println(e);;
		}
		
		stopDC = new _StopForm_DC(masterForm);
		stopPanel = new _StopForm_Panel(stopDC.dispDataComp);
		
		stopDC.setForm(masterForm);
		//Create EN Panels and then populating the values		
		stopDC.populateFormComponents();
		stopPanel.createUI(stopDC);
		//if(!once)
		//{
	/*	TCReservationService reserserv=masterForm.getSession().getReservationService();
		//reserserv.isReserved(masterForm);
		try {
			if(!reserserv.isReserved(masterForm))
			reserserv.reserve(masterForm);
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//once=true;
		//}
		
		
	}
	public JComponent getPanel() 
	{
		return stopPanel;
	}

	public ImageIcon getTabIcon() 
	{
		return null;
	}

	public String getTip() 
	{
		return null;
	}

	public void lockForm(boolean lock) 
	{
		
	}

	public void saveForm() 
	{
			//stopDC.saveReviseItemDecision(stopPanel.isPartRevisedFlag());
			stopDC.saveFormData();
	}

	public void setDisplayProperties()
	{
		
	}
	
	

	@Override
	public boolean isFormSavable() {
		if(stopPanel.checkCompulsoryFields())
			return true;
		else
			return false;
	}

}
