/*
 * Created on Aug 2, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound.masters;
import javax.swing.JComponent;
/**
 * @author HughesTS
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IRevisionMasterForm {
	
	abstract JComponent getPanels();
	abstract void       saveForms();
	abstract void       lockForms(boolean lock);

}
