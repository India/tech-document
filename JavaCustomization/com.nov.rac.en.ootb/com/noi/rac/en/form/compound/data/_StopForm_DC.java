package com.noi.rac.en.form.compound.data;

import javax.swing.JRadioButton;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel_v2;
import com.noi.rac.en.form.compound.component.NOVEnGenSearchPanel;
import com.noi.rac.en.form.compound.util.NOVCustomFormProperties;
import com.noi.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.combobox.iComboBox;

public class _StopForm_DC extends NOVCustomFormProperties
{
    
    public NOVLOVPopupButton drafted;
    public NOVLOVPopupButton approved;
    public NOVDateButton draftedon;
    public NOVDateButton approvedon;
    public iTextArea stopreason;
    public iTextArea stopcomment;
    public iTextArea stopinstructions;
    NOVEnDistributionPanel_v2 distributionPanel;
    AbstractReferenceLOVList distSource;
    AbstractReferenceLOVList distTarget;
    public TCComponentForm masterform;
    public JRadioButton stopwoandpo;
    public JRadioButton stopshipment;
    public JRadioButton stopAll;
    public NOVDateButton expectedResolutionDate;
    public JRadioButton partRevised;
    public TCComponent[] dispDataComp;
    public iComboBox stop_rem_comment_combo;
    
    String[] propertyNames = new String[] { "draftedby", "draftedon", "approvedby", "approvedon", "stopreason",
            "stopcomment", "projects", "distribution", "stopwoandpo", "stopshipment", "nov4_stopall",/*
                                                                                                      * "stopwip"
                                                                                                      * ,
                                                                                                      */
            "nov4_expected_resolution", "nov4_part_revised", "stoptargetdisposition", "stopinstructions",
            "nov4_stopcomment_vals" };
    
    public _StopForm_DC(TCComponent form)
    {
        
        masterform = (TCComponentForm) form;
        // get the disposition data
        TCProperty targetdispProperty;
        try
        {
            targetdispProperty = form.getTCProperty("stoptargetdisposition");
            
            if (targetdispProperty != null)
            {
                targetdispProperty.getReferenceValueArray();
                dispDataComp = (targetdispProperty.getReferenceValueArray());
            }
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        stopAll = new JRadioButton();
        stopwoandpo = new JRadioButton();
        stopshipment = new JRadioButton();
        expectedResolutionDate = new NOVDateButton();
        partRevised = new JRadioButton();
        drafted = new NOVLOVPopupButton();
        approved = new NOVLOVPopupButton();
        draftedon = new NOVDateButton();
        approvedon = new NOVDateButton();
        stopcomment = new iTextArea();
        stopreason = new iTextArea();
        stopinstructions = new iTextArea();
        stop_rem_comment_combo = new iComboBox();
        
        addProperty("draftedby", drafted);
        addProperty("draftedon", draftedon);
        addProperty("approvedby", approved);
        addProperty("approvedon", approvedon);
        addProperty("stopreason", stopreason);
        addProperty("stopcomment", stopcomment);
        addProperty("stopinstructions", stopinstructions);
        addProperty("nov4_stopall", stopAll);
        addProperty("stopwoandpo", stopwoandpo);
        addProperty("stopshipment", stopshipment);
        addProperty("nov4_expected_resolution", expectedResolutionDate);
        addProperty("nov4_part_revised", partRevised);
        addProperty("nov4_stopcomment_vals", stop_rem_comment_combo);
    }
    
    public String[] getPropertyNames()
    {
        return propertyNames;
    }
    
    public void setForm(TCComponent form)
    {
        if (form != null)
        {
            try
            {
                super.setForm(form);
                
                distributionPanel = new NOVEnDistributionPanel_v2();
                
                // Code added for Distribution List -------
                String currentGroup = "";
                distSource = new AbstractReferenceLOVList();
                TCComponentGroup group = form.getSession().getCurrentGroup();
                String groupname = group.getFullName();
                if (groupname.indexOf('.') >= 0)
                    currentGroup = groupname.substring(0, groupname.indexOf('.'));
                else
                    currentGroup = groupname;
                currentGroup = currentGroup + ":";
                distSource.initClassECNERO(form.getSession(), "ImanAliasList", "alName", currentGroup);
                // @harshada - Modified for test
                // distSource.initClass(getForm().getSession(),"ImanAliasList","alName");
                distTarget = new AbstractReferenceLOVList();
                // Code added for Distribution List -------
                
                /*
                 * TCComponentListOfValues lovalues =
                 * TCComponentListOfValuesType .findLOVByName(form.getSession(),
                 * "_enreasonforchange_"); ListOfValuesInfo lovInfo =
                 * lovalues.getListOfValues(); Object[] lovS =
                 * lovInfo.getListOfValues(); //Vector<Object> reasonValVac =
                 * new Vector<Object>(); if ( lovS.length > 0 ) {
                 * reasonCombo.addItem(""); for ( int i = 0; i < lovS.length;
                 * i++ ) { reasonCombo.addItem(lovS[i]); } }
                 */
                TCComponentListOfValues lovalues = TCComponentListOfValuesType.findLOVByName(form.getSession(),
                        "nov4_stopcomment_vals");
                Object[] lovS = lovalues.getListOfValues().getLOVDisplayValues();
                if (lovS.length > 0)
                {
                    stop_rem_comment_combo.addItem("");
                    for (int i = 0; i < lovS.length; i++)
                    {
                        stop_rem_comment_combo.addItem(lovS[i]);
                    }
                }
                
                addProperty("distribution", distributionPanel);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            super.getFormProperties(propertyNames);
        }
    }
    
    public void enableForm()
    {
        enableProperty("stopinstructions");
        enableProperty("stopreason");
        enableProperty("stopcomment");
        
        enableProperty("nov4_stopall");
        enableProperty("stopwoandpo");
        enableProperty("stopshipment");
        enableProperty("nov4_expected_resolution");
        enableProperty("nov4_part_revised");
        enableProperty("projects");
        enableProperty("distribution");
        enableProperty("nov4_stopcomment_vals");
    }
    
    public void clearForm()
    {
    }
    
    public void populateFormComponents()
    {
        if (getForm() == null)
            return;
        try
        {
            
            stopAll.setSelected(((TCProperty) imanProperties.get(stopAll)).getLogicalValue());
            stopwoandpo.setSelected(((TCProperty) imanProperties.get(stopwoandpo)).getLogicalValue());
            stopshipment.setSelected(((TCProperty) imanProperties.get(stopshipment)).getLogicalValue());
            
            expectedResolutionDate.setDate(((TCProperty) imanProperties.get(expectedResolutionDate)).getDateValue());
            partRevised.setSelected(((TCProperty) imanProperties.get(partRevised)).getLogicalValue());
            
            drafted.setSelectedText(((TCProperty) imanProperties.get(drafted)).getStringValue());
            approved.setSelectedText(((TCProperty) imanProperties.get(approved)).getStringValue());
            
            draftedon.setDate(((TCProperty) imanProperties.get(draftedon)).getDateValue());
            approvedon.setDate(((TCProperty) imanProperties.get(approvedon)).getDateValue());
            
            stopreason.setText(((TCProperty) imanProperties.get(stopreason)).getStringValue());
            stopcomment.setText(((TCProperty) imanProperties.get(stopcomment)).getStringValue());
            stopinstructions.setText(((TCProperty) imanProperties.get(stopinstructions)).getStringValue());
            stop_rem_comment_combo.setText(((TCProperty) imanProperties.get(stop_rem_comment_combo)).getStringValue());
            
            String[] diststributionList = ((TCProperty) imanProperties.get(distributionPanel)).getStringValueArray();
            if (diststributionList != null)
            {
                distributionPanel.populateTargetList(diststributionList);
            }
            String[] arr1 = ((TCProperty) imanProperties.get(distributionPanel)).getStringValueArray();
            for (int i = 0; i < arr1.length; i++)
            {
                distTarget.addItem(arr1[i], arr1[i]);
                distSource.addExclusion(arr1[i]);
            }
            distributionPanel.init(distSource, distTarget, new java.awt.Dimension(250, 100));
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public void saveOnlyDispFormData()
    {
        try
        {
            TCProperty[] arr = null;
            
            arr = enabledProperty(arr, distributionPanel);
            arr = enabledProperty(arr, stopAll);
            arr = enabledProperty(arr, stopwoandpo);
            arr = enabledProperty(arr, stopshipment);
            arr = enabledProperty(arr, expectedResolutionDate);
            arr = enabledProperty(arr, partRevised);
            arr = enabledProperty(arr, stopreason);
            arr = enabledProperty(arr, stopcomment);
            arr = enabledProperty(arr, stopinstructions);
            
            arr = enabledProperty(arr, drafted);
            arr = enabledProperty(arr, approved);
            arr = enabledProperty(arr, draftedon);
            arr = enabledProperty(arr, approvedon);
            arr = enabledProperty(arr, stop_rem_comment_combo);
            
            getForm().setTCProperties(arr);
            getForm().save();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public void saveFormData()
    {
        try
        {
            TCProperty[] arr = null;
            
            // ((TCProperty)imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel.getTargetListObjects());
            ((TCProperty) imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel
                    .getAllTargetListObjects());
            
            ((TCProperty) imanProperties.get(stopAll)).setLogicalValueData(stopAll.isSelected());
            ((TCProperty) imanProperties.get(stopshipment)).setLogicalValueData(stopshipment.isSelected());
            ((TCProperty) imanProperties.get(stopwoandpo)).setLogicalValueData(stopwoandpo.isSelected());
            
            ((TCProperty) imanProperties.get(stopcomment)).setStringValueData(stopcomment.getText());
            ((TCProperty) imanProperties.get(stopreason)).setStringValueData(stopreason.getText());
            ((TCProperty) imanProperties.get(stopinstructions)).setStringValueData(stopinstructions.getText());
            
            String expectedResolutionDateValue = expectedResolutionDate.getDateString();
            if (expectedResolutionDateValue != null)
            {
                ((TCProperty) imanProperties.get(expectedResolutionDate)).setDateValueData(expectedResolutionDate
                        .getDate());
            }
            
            Object selObj1 = drafted.getSelectedObject();
            if (selObj1 != null)
            {
                ((TCProperty) imanProperties.get(drafted)).setStringValueData(selObj1.toString());
            }
            Object selObj3 = approved.getSelectedObject();
            if (selObj3 != null)
            {
                ((TCProperty) imanProperties.get(approved)).setStringValueData(selObj3.toString());
            }
            
            String draftedDate = draftedon.getDateString();
            if (draftedDate != null)
            {
                ((TCProperty) imanProperties.get(draftedon)).setDateValueData(draftedon.getDate());
            }
            String approvedDate = approvedon.getDateString();
            if (approvedDate != null)
            {
                ((TCProperty) imanProperties.get(approvedon)).setDateValueData(approvedon.getDate());
            }
            String remCommentComboValue = stop_rem_comment_combo.getSelectedItem().toString();
            if (remCommentComboValue != null)
            {
                ((TCProperty) imanProperties.get(stop_rem_comment_combo)).setStringValue(remCommentComboValue);
            }
            
            // disposition data
            TCProperty prop = (TCProperty) masterform.getFormTCProperty("stoptargetdisposition");
            // saveDispData();
            // TCComponent[] dispcomps = dispDataComp.getDispData();
            if (dispDataComp != null && prop != null)
                prop.setReferenceValueArrayData(dispDataComp);
            
            arr = enabledProperty(arr, stopinstructions);
            arr = enabledProperty(arr, stopAll);
            arr = enabledProperty(arr, stopshipment);
            arr = enabledProperty(arr, stopwoandpo);
            arr = enabledProperty(arr, partRevised);
            arr = enabledProperty(arr, expectedResolutionDate);
            arr = enabledProperty(arr, stopcomment);
            arr = enabledProperty(arr, stopreason);
            arr = enabledProperty(arr, drafted);
            arr = enabledProperty(arr, approved);
            arr = enabledProperty(arr, draftedon);
            arr = enabledProperty(arr, approvedon);
            arr = enabledProperty(arr, distributionPanel);
            arr = enabledProperty(arr, stop_rem_comment_combo);
            // arr=enabledProperty(arr,dispDataComp);
            getForm().setTCProperties(arr);
            getForm().save();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public boolean isFormSavable()
    {
        return true;
    }
    
    public void saveReviseItemDecision(boolean partRevisedFlag)
    {
        ((TCProperty) imanProperties.get(partRevised)).setLogicalValueData(partRevisedFlag);
        
    }
    
}
