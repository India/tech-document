package com.noi.rac.en.form.compound.util;

import java.awt.Container;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ScrollForm implements MouseWheelListener 
{
	JPanel mailpanelLc = new JPanel();

	public ScrollForm(JPanel mainpaneld)
	{
		mailpanelLc =mainpaneld;
	}

	public void mouseWheelMoved(MouseWheelEvent e) 
	{    

		Container cont = getParentCust(mailpanelLc);
		if (cont instanceof JScrollPane) 
		{                            
			JScrollPane sc = (JScrollPane)cont;
			if (e.getWheelRotation() < 0) 
			{
				sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() -15);                                
			}
			else 
			{
				sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() + 15);
			}
		}
	}        
	public Container getParentCust(Container con)
	{    
		if (con instanceof JScrollPane)
		{        
			return con;    
		}
		else
		{
			return getParentCust(con.getParent());
		}                        
	}
}
