package com.noi.rac.en.form.compound.panels;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.noi.NationalOilwell;
import com.noi.rac.en.form.compound.component.NOV4ECRDistributionPanel;
import com.noi.rac.en.form.compound.component.NOV4RIGSEcrGenInfoPanel;
import com.noi.rac.en.form.compound.component.NOV4_RIGSEcrAffectedDocPartsPanel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.form.compound.util.ScrollForm;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentSite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCSiteInfo;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOV4RigsECRForm_Panel  extends PrintablePanel
{
	private static final long serialVersionUID = 1L;
	
	private Registry regECRPanel;
	
	public TCComponentForm rigECRForm;	

	NOV4_RIGSEcrAffectedDocPartsPanel rigsECRAffectedDocPartsPanel;
	NOV4RIGSEcrGenInfoPanel rigsECRGenInfoPanel;
	ExternalAttachmentsPanel_v2 refAttachmentPanel;
	//@Mohanar
	RelatedEnPanel relENPanel;

	public NOV4RigsECRForm_Panel() {
		super();
		regECRPanel = Registry.getRegistry( this );
	}
	
	public void createUI( INOVCustomFormProperties formProperties  )
	{
		novFormProperties = formProperties;
		rigECRForm = ( TCComponentForm )novFormProperties.getForm();	
		
		JPanel genInfoPanel = createGenInfoPanel();
		//genInfoPanel.disable();
		genInfoPanel.setEnabled(false);
		
		TCSession tcSession = rigECRForm.getSession();
		rigsECRAffectedDocPartsPanel = new NOV4_RIGSEcrAffectedDocPartsPanel( this, tcSession );
		rigsECRAffectedDocPartsPanel.setPreferredSize( new Dimension( 780, 190 ) );
	
		TitledBorder titleBorder = new TitledBorder( regECRPanel.getString( "affectedDocParts.LABEL" ) );
		titleBorder.setTitleFont( new Font( "Dialog", java.awt.Font.BOLD, 12 ) );
		rigsECRAffectedDocPartsPanel.setBorder( titleBorder );
		//rigsECRAffectedDocPartsPanel.disable();
		rigsECRAffectedDocPartsPanel.setEnabled(false);
		
		rigsECRGenInfoPanel = new NOV4RIGSEcrGenInfoPanel( novFormProperties );

		JPanel ecrTopPanel = new JPanel( new PropertyLayout( 5 , 5 , 5 , 5 , 5 , 5 ) );	
		ecrTopPanel.add( "1.1.left.center", genInfoPanel );
		ecrTopPanel.add( "2.1.left.center", rigsECRAffectedDocPartsPanel );

		NOV4ECRDistributionPanel distributionPanel = ( NOV4ECRDistributionPanel )novFormProperties.getProperty( "nov4_distribution");
		distributionPanel.setTitleBorderText( regECRPanel.getString( "distributionPanel.LABEL" ));
		distributionPanel.setPreferredSize( new Dimension( 220, 160 ) );
		distributionPanel.ecrDistributionListScroll.setPreferredSize( new Dimension( 200, 110 ));
		
		//distributionPanel.disable();
		distributionPanel.setEnabled(false);
		
		refAttachmentPanel = new ExternalAttachmentsPanel_v2( novFormProperties, false, this );
		refAttachmentPanel.setPreferredSize( new Dimension( 550, 160 ) );
		refAttachmentPanel.tablePane.setPreferredSize(new Dimension(520,90));
		refAttachmentPanel.hideAttachmentsPlusMinus();
		//refAttachmentPanel.disable();
		refAttachmentPanel.setEnabled(false);

		JPanel distRefAttachPanel = new JPanel( new PropertyLayout( 5 , 5 , 5 , 5 , 5 , 5 ) );		
		distRefAttachPanel.add( "1.1.left.center", distributionPanel );
		distRefAttachPanel.add( "1.2.left.center", refAttachmentPanel );
		//distRefAttachPanel.disable();
		distRefAttachPanel.setEnabled(false);

		// @Mohanar - To add EN link in the ECR form
		relENPanel = new RelatedEnPanel(this, novFormProperties);
		relENPanel.setPreferredSize(new Dimension(500, 150));
		//relENPanel.disable();
		relENPanel.setEnabled(false);

		JPanel relatedENPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
		relatedENPanel.add("1.1.center", relENPanel);
		//relatedENPanel.disable();
		relatedENPanel.setEnabled(false);

		JPanel ecrBottomPanel = new JPanel(new PropertyLayout(0, 0, 0, 0, 0, 15));
		ecrBottomPanel.add("1.1.left.top", rigsECRGenInfoPanel);
		ecrBottomPanel.add("2.1.left.center", distRefAttachPanel);
		ecrBottomPanel.add("3.1.center", relatedENPanel);

		/*
		 * JPanel ecrBottomPanel = new JPanel( new PropertyLayout( 0 , 0 , 0 , 0
		 * , 0 , 10 ) ); ecrBottomPanel.add( "1.1.left.top", rigsECRGenInfoPanel
		 * ); ecrBottomPanel.add( "2.1.left.center", distRefAttachPanel );
		 * ecrBottomPanel.add( "3.1.left.center", distRefAttachPanel );
		 */

		GridLayout gridLayout = createGridLayout();

		JPanel ecrMainPanel = new JPanel( new GridBagLayout() );		
		ecrMainPanel.setLayout( gridLayout );
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		ecrMainPanel.add( ecrTopPanel, gridBagConstraints) ;

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		ecrMainPanel.add( ecrBottomPanel, gridBagConstraints );

		JScrollPane scrollpane = new JScrollPane( ecrMainPanel );
		ecrBottomPanel.addMouseWheelListener( new ScrollForm( this ) );
		add( scrollpane );			
	}
	
	public TCComponent[] getECRAffectedDocParts()
	{
		TCProperty targetdispProperty = null; 
		try
		{
			targetdispProperty = rigECRForm.getTCProperty( "nov4_ecr_affected_parts" );
		}catch (TCException e) {
			e.printStackTrace();
		}
		
		return targetdispProperty.getReferenceValueArray();
	}

	// @Mohanar - Added new method to fetch the related EN objects from the ECR properties.
	/*	public TCComponent[] getRelatedEn() {
		TCProperty targetdispProperty = null;
		try {
			targetdispProperty = rigECRForm.getTCProperty("nov4_related_en");
			} catch (TCException e) {
			e.printStackTrace();
		}
		
		return targetdispProperty.getReferenceValueArray();
	}*/
	
	public AIFComponentContext[] getRelatedEn() {
		AIFComponentContext[] comps=null;
		try 
		{
			String[] relation={regECRPanel.getString("ECNECR.RELATION")};
	        comps = rigECRForm.whereReferencedByTypeRelation(new String[0], relation);
		} catch (TCException e) {
			e.printStackTrace();
		}
		
		return comps;
	}
	
	
	public JPanel createGenInfoPanel()
	{
		JPanel genInfoLeftPanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );		
		
		JTextField ecrNumberText = ( JTextField )novFormProperties.getProperty( "object_name" );  
		ecrNumberText.setEnabled( false );
		ecrNumberText.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrNumberText.disable();
		
		JTextField ecrStatusText = ( JTextField )novFormProperties.getProperty( "release_status_list" );
		ecrStatusText.setEnabled( false );
		ecrStatusText.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrStatusText.disable();
		
		JTextField ecrCloseDispositionText = ( JTextField )novFormProperties.getProperty( "nov4_close_disposition" );		
		ecrCloseDispositionText.setEnabled( false );
		ecrCloseDispositionText.setPreferredSize( new Dimension( 150, 21 ) );	
		//ecrCloseDispositionText.disable();
		
		genInfoLeftPanel.add( "1.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrNumber.LABEL") ) );
		genInfoLeftPanel.add( "1.2.left.center", ecrNumberText );
		genInfoLeftPanel.add( "2.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrStatus.LABEL" ) ) );
		genInfoLeftPanel.add( "2.2.left.center", ecrStatusText );
		genInfoLeftPanel.add( "3.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrCloseDisposition.LABEL" ) ) );
		genInfoLeftPanel.add( "3.2.left.center", ecrCloseDispositionText );
		
		JPanel genInfoMiddlePanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );
		
		JTextField ecrRequesterText = ( JTextField )novFormProperties.getProperty( "nov4_ecr_requester" );		
		ecrRequesterText.setEnabled( false );
		ecrRequesterText.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrRequesterText.disable();
		
		JTextField ecrRequestorDeptText = ( JTextField )novFormProperties.getProperty( "nov4_requester_dept" );		
		ecrRequestorDeptText.setEnabled(false);
		ecrRequestorDeptText.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrRequestorDeptText.disable();

		JTextField ecrChangeRespText = ( JTextField )novFormProperties.getProperty( "nov4_change_resp" );		
		ecrChangeRespText.setEnabled( false );
		ecrChangeRespText.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrChangeRespText.disable();
		
		genInfoMiddlePanel.add( "1.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrRequester.LABEL") ) );
		genInfoMiddlePanel.add( "1.2.left.center", ecrRequesterText );
		genInfoMiddlePanel.add( "2.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrRequestorDept.LABEL" ) ) );
		genInfoMiddlePanel.add( "2.2.left.center", ecrRequestorDeptText );
		genInfoMiddlePanel.add( "3.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrChangeResp.LABEL" ) ) );
		genInfoMiddlePanel.add( "3.2.left.center", ecrChangeRespText );

		
		JPanel genInfoRightPanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );		
		
		NOVDateButton  ecrRequestDateBtn = ( NOVDateButton  )novFormProperties.getProperty( "nov4_request_date" );		
		ecrRequestDateBtn.setEnabled( false );
		ecrRequestDateBtn.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrRequestDateBtn.disable();
		
		NOVDateButton  ecrCloseDateBtn = ( NOVDateButton  )novFormProperties.getProperty( "nov4_close_date" );		
		ecrCloseDateBtn.setEnabled( false );
		ecrCloseDateBtn.setPreferredSize( new Dimension( 150, 21 ) );
		//ecrCloseDateBtn.disable();
		
		JLabel blankLeftLabel = new JLabel();
		blankLeftLabel.setPreferredSize( new Dimension( 60, 21 ) );		

		JLabel blankRightLabel = new JLabel();
		blankRightLabel.setPreferredSize( new Dimension( 15, 21 ) );		
	
		JButton printBtn = new JButton();
		printBtn.setText( regECRPanel.getString( "printECR.LABEL" ) );
		//printBtn.setPreferredSize( new Dimension( 60, 25 ) );
		
		JButton helpBtn = new JButton();
		helpBtn.setText( regECRPanel.getString( "helpECR.LABEL" ) );
		//helpBtn.setPreferredSize( new Dimension( 60, 25 ) );
		
		helpBtn.addActionListener( new ActionListener()
		{
			public void actionPerformed( ActionEvent e )
			{
				TCComponentForm ecrForm = ( TCComponentForm )novFormProperties.getForm();		
				TCPreferenceService prefServ = ecrForm.getSession().getPreferenceService();
				String helpUrl = prefServ.getString( TCPreferenceService.TC_preference_site,
						regECRPanel.getString( "NOI_NewENHelp.PREF" ) );

				if(helpUrl==null || helpUrl.length()==0)
				{
					//helpUrl = "http://srvhouplmt01:7081/cetgws/Standard-ENform.pdf";
					MessageBox.post( NationalOilwell.NOHELP,	NationalOilwell.SEEADMIN, MessageBox.ERROR );
				}
				else
				{
					try 
					{
						//Runtime.getRuntime().exec(call.toString());
						Runtime.getRuntime().exec( "rundll32 url.dll,FileProtocolHandler " + helpUrl );
					} 
					catch (Exception ex)
					{
						ex.printStackTrace();
						MessageBox.post(NationalOilwell.NOHELP,
								NationalOilwell.SEEADMIN, MessageBox.ERROR);
					}
				}
			}
		});
		
		printBtn.addActionListener( new ActionListener()
		{
			public void actionPerformed( ActionEvent e )
			{
				TCComponentForm ecrForm = ( TCComponentForm )novFormProperties.getForm();
				TCComponentSite site = ecrForm.getSession().getCurrentSite();
				try
				{
					TCSiteInfo info = site.getSiteInfo();
					String database = info.getSiteName();		
					String formname = ecrForm.getProperty( "object_name" );
					String ecrFormPUID = ecrForm.getUid();
					
					alcECRPrint( "ecrPrint", formname, database, ecrFormPUID );
				}
				catch( Exception exp )
				{
					exp.printStackTrace();
				}
			}
		});

		JPanel genInfoBtnPanel = new JPanel( new PropertyLayout() );
		genInfoBtnPanel.add( "1.1.left.center", blankRightLabel );
		genInfoBtnPanel.add( "1.2.right.center", helpBtn );
		genInfoBtnPanel.add( "1.3.right.center", printBtn );		
		
		genInfoRightPanel.add( "1.1.left.center", blankLeftLabel );	
		genInfoRightPanel.add( "1.2.left.center", genInfoBtnPanel );		
		genInfoRightPanel.add( "2.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrRequestDate.LABEL" ) ) );
		genInfoRightPanel.add( "2.2.left.center", ecrRequestDateBtn );		
		genInfoRightPanel.add( "3.1.left.center", new NOIJLabel( regECRPanel.getString( "ecrCloseDate.LABEL" ) ) );
		genInfoRightPanel.add( "3.2.left.center", ecrCloseDateBtn );

		JPanel genInfoPanel = new JPanel( new PropertyLayout() );		
		genInfoPanel.add( "1.1.left.center", genInfoLeftPanel );
		genInfoPanel.add( "1.2.left.center", genInfoMiddlePanel );		
		genInfoPanel.add( "1.3.left.center", genInfoRightPanel );
		
		return genInfoPanel;		
	}
	
//TODO: Nitin --> Make this as a common for EN and ECR form.	
	public GridLayout createGridLayout()
	{
		GridLayout gridLayout = new GridLayout( 8, 1 )
		{
			private static final long serialVersionUID = 1L;

			public Dimension preferredLayoutSize(Container parent)
			{				
				synchronized (parent.getTreeLock())
				{
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if( nrows > 0 )
					{
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else
					{
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for( int i = 0; i < ncomponents; i ++ )
					{
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getPreferredSize();
						if( w[c] < d.width )
						{
							w[c] = d.width;
						}
						if( h[r] < d.height )
						{
							h[r] = d.height;
						}
					}
					int nw = 0;
					for( int j = 0; j < ncols; j ++  )
					{
						nw += w[j];
					}
					int nh = 0;
					for( int i = 0; i < nrows; i ++ )
					{
						nh += h[i];
					}
					return new Dimension( insets.left + insets.right + nw + (ncols-1)*getHgap(), 
							insets.top + insets.bottom + nh + (nrows-1)*getVgap() );
				}
			}

			public Dimension minimumLayoutSize( Container parent )
			{				
				synchronized( parent.getTreeLock() )
				{
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if( nrows > 0 )
					{
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else
					{
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for( int i = 0; i < ncomponents; i ++ )
					{
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getMinimumSize();
						if (w[c] < d.width)
						{
							w[c] = d.width;
						}
						if (h[r] < d.height)
						{
							h[r] = d.height;
						}
					}
					int nw = 0;
					for( int j = 0; j < ncols; j ++ )
					{
						nw += w[j];
					}
					int nh = 0;
					for( int i = 0; i < nrows; i ++ )
					{
						nh += h[i];
					}
					return new Dimension( insets.left + insets.right + nw + (ncols-1)*getHgap(), 
							insets.top + insets.bottom + nh + (nrows-1)*getVgap() );
				}
			}

			public void layoutContainer( Container parent )
			{			   
				synchronized( parent.getTreeLock() )
				{
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if( ncomponents == 0 )
					{
						return;
					}
					if( nrows > 0 )
					{
						ncols = ( ncomponents + nrows - 1 ) / nrows;
					} 
					else
					{
						nrows = ( ncomponents + ncols - 1 ) / ncols;
					}
					int hgap = getHgap();
					int vgap = getVgap();
					// scaling factors      
					Dimension pd = preferredLayoutSize(parent);
					double sw = (1.0 * parent.getWidth()) / pd.width;
					double sh = (1.0 * parent.getHeight()) / pd.height;
					// scale
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++)
					{
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getPreferredSize();
						d.width = (int) (sw * d.width);
						d.height = (int) (sh * d.height);
						if (w[c] < d.width)
						{
							w[c] = d.width;
						}
						if (h[r] < d.height)
						{
							h[r] = d.height;
						}
					}
					for (int c = 0, x = insets.left; c < ncols; c ++)
					{
						for (int r = 0, y = insets.top; r < nrows; r ++)
						{
							int i = r * ncols + c;
							if (i < ncomponents)
							{
								parent.getComponent(i).setBounds(x, y, w[c], h[r]);
							}
							y += h[r] + vgap;
						}
						x += w[c] + hgap;
					}
				}
			}  
		};
		
		return gridLayout;
	}
	
}
