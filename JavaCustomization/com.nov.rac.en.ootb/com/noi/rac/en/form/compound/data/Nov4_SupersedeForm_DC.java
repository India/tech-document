package com.noi.rac.en.form.compound.data;

import javax.swing.JCheckBox;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel_v2;
import com.noi.rac.en.form.compound.component.NOVEnGenSearchPanel;
import com.noi.rac.en.form.compound.component.NOVSupersedeTablePanel;
import com.noi.rac.en.form.compound.util.NOVCustomFormProperties;
import com.noi.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVEnDispositionPanel;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.userpreferences.StartOfWeekPanel;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.combobox.iComboBox;

public class Nov4_SupersedeForm_DC extends NOVCustomFormProperties
{
    public NOVLOVPopupButton drafted;
    public NOVLOVPopupButton checked;
    public NOVLOVPopupButton approved;
    public NOVLOVPopupButton created;
    public iComboBox reasonCombo;
    public NOVDateButton draftedon;
    // public NOVDateButton nov4_draftedon;
    
    public NOVDateButton checkedon;
    public NOVDateButton approvedon;
    public NOVDateButton creationon;
    public iTextArea reasonForChangeDesc;
    public DateButton validationDate;
    NOVEnGenSearchPanel relatedECRSearchPanel;
    NOVEnGenSearchPanel projectsPanel;
    
    NOVSupersedeTablePanel targetPanel;
    
    public iTextArea furExplanation;
    // TCDECREL-1586:START
    // NOVEnDistributionPanel distributionPanel;
    NOVEnDistributionPanel_v2 distributionPanel;
    // TCDECREL-1586:END
    public NOVEnDispositionPanel dispDataComp;
    AbstractReferenceLOVList distSource;
    AbstractReferenceLOVList distTarget;
    private Registry registry = Registry.getRegistry("com.noi.rac.en.form.form");
    
    /*
     * String[] propertyNames = new String[]
     * {"nov4_draftedby","nov4_draftedon","nov4_checkedby"
     * ,"nov4_checkedon","nov4_approvedby",
     * "nov4_approvedon","nov4_reason","nov4_explanation","nov4_projects",
     * "nov4_distribution","nov4_supdtargetsdisposition"};
     */

    public TCComponent[] dispcomps; // added By Sandip
    
    /*
     * String[] propertyNames = new String[]
     * {"nov4_draftedby","nov4_checkedby","nov4_approvedby",
     * "nov4_approvedon","nov4_reason","nov4_explanation","nov4_projects",
     * "nov4_distribution","nov4_supdtargetdisposition",
     * "nov4_draftedon","nov4_checkedon" };
     */

    String[] propertyNames = new String[] { "nov4_draftedby", "nov4_checkedby", "nov4_approvedby", "nov4_approvedon",
            "nov4_explanation", "nov4_projects", "nov4_distribution", "nov4_supdtargetdisposition", "nov4_draftedon",
            "nov4_checkedon" };
    
    public Nov4_SupersedeForm_DC()
    {
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        projectsPanel = new NOVEnGenSearchPanel(tcSession, "TC_Project");
        
        furExplanation = new iTextArea();
        drafted = new NOVLOVPopupButton();
        checked = new NOVLOVPopupButton();
        approved = new NOVLOVPopupButton();
        reasonCombo = new iComboBox();
        reasonCombo.setMandatory(true);
        reasonCombo.setEditable(false);
        draftedon = new NOVDateButton();
        // nov4_draftedon = new NOVDateButton();
        
        checkedon = new NOVDateButton();
        approvedon = new NOVDateButton();
        furExplanation = new iTextArea();
        reasonForChangeDesc = new iTextArea();
        
        // addProperty("newrelease",newRelease );
        // addProperty("islocked", );
        addProperty("nov4_draftedby", drafted);
        // addProperty("draftedon", draftedon);
        addProperty("nov4_checkedby", checked);
        // addProperty("checkedon", checkedon);
        addProperty("nov4_approvedby", approved);
        addProperty("nov4_approvedon", approvedon);
        // addProperty("nov4_reason", reasonCombo);
        addProperty("nov4_explanation", furExplanation);
        addProperty("nov4_projects", projectsPanel);
        addProperty("nov4_draftedon", draftedon);
        addProperty("nov4_checkedon", checkedon);
    }
    
    public String[] getPropertyNames()
    {
        return propertyNames;
    }
    
    public void setForm(TCComponent form)
    {
        if (form != null)
        {
            try
            {
                super.setForm(form);
                targetPanel = new NOVSupersedeTablePanel();
                
                // get the disposition data
                TCProperty targetdispProperty = form.getTCProperty("nov4_supdtargetdisposition");
                if (null != targetdispProperty)
                    dispDataComp = new NOVEnDispositionPanel(targetdispProperty.getReferenceValueArray());
                
                // TCDECREL-1586:START
                // distributionPanel = new NOVEnDistributionPanel();
                distributionPanel = new NOVEnDistributionPanel_v2();
                // TCDECREL-1586:END
                
                // Code added for Distribution List -------
                String currentGroup = "";
                distSource = new AbstractReferenceLOVList();
                TCComponentGroup group = form.getSession().getCurrentGroup();
                String groupname = group.getFullName();
                if (groupname.indexOf('.') >= 0)
                    currentGroup = groupname.substring(0, groupname.indexOf('.'));
                else
                    currentGroup = groupname;
                currentGroup = currentGroup + ":";
                distSource.initClassECNERO(form.getSession(), "ImanAliasList", "alName", currentGroup);
                distTarget = new AbstractReferenceLOVList();
                // Code added for Distribution List -------
                
                if (null != dispDataComp)
                    addProperty("nov4_supdtargetdisposition", dispDataComp);
                addProperty("nov4_distribution", distributionPanel);
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            super.getFormProperties(propertyNames);
        }
    }
    
    public void clearForm()
    {
    }
    
    public void populateFormComponents()
    {
        if (getForm() == null)
            return;
        try
        { // Start - Adding By Sandip
            draftedon.setDate(((TCComponentForm) getForm()).getTCProperty("nov4_draftedon").getDateValue());
            drafted.setSelectedText(((TCComponentForm) getForm()).getTCProperty("nov4_draftedby").getStringValue());
            
            checked.setSelectedText(((TCComponentForm) getForm()).getTCProperty("nov4_checkedby").getStringValue());
            checkedon.setDate(((TCComponentForm) getForm()).getTCProperty("nov4_checkedon").getDateValue());
            
            approved.setSelectedText(((TCComponentForm) getForm()).getTCProperty("nov4_approvedby").getStringValue());
            approvedon.setDate(((TCComponentForm) getForm()).getTCProperty("nov4_approvedon").getDateValue());
            
            String[] projects = (((TCComponentForm) getForm()).getTCProperty("nov4_projects")).getStringValueArray();
            
            if (projects != null)
            {
                projectsPanel.populateTargetList(projects);
            }
            
            // End - Adding By Sandip
            
            furExplanation.setText(((TCComponentForm) getForm()).getTCProperty("nov4_explanation").getStringValue());
            
            String[] arr1 = ((TCComponentForm) getForm()).getTCProperty("nov4_distribution").getStringValueArray();
            for (int i = 0; i < arr1.length; i++)
            {
                distTarget.addItem(arr1[i], arr1[i]);
                distSource.addExclusion(arr1[i]);
            }
            
            distributionPanel.init(distSource, distTarget, new java.awt.Dimension(250, 100));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void saveOnlyDispFormData()
    {
        try
        {
            TCProperty[] arr = null;
            
            // disposition data
            TCProperty prop = (TCProperty) imanProperties.get(dispDataComp);
            TCComponent[] dispcomps = dispDataComp.getDispData();
            if (dispcomps != null && dispDataComp.isEnabled())
                prop.setReferenceValueArrayData(dispcomps);
            
            arr = enabledProperty(arr, distributionPanel);
            arr = enabledProperty(arr, furExplanation);
            arr = enabledProperty(arr, dispDataComp);
            getForm().setTCProperties(arr);
            getForm().save();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public void saveFormData()
    {
        try
        {
            TCProperty[] arr = null;
            ((TCProperty) imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel
                    .getAllTargetListObjects());
            ((TCProperty) imanProperties.get(furExplanation)).setStringValueData(furExplanation.getText());
            // disposition data
            TCProperty prop = (TCProperty) imanProperties.get(dispDataComp);
            dispDataComp.saveDispData();
            // TCComponent[] dispcomps = dispDataComp.getDispData();
            dispcomps = dispDataComp.getDispData();
            
            if (dispcomps != null && dispDataComp.isEnabled())
                prop.setReferenceValueArrayData(dispcomps);
            
            arr = enabledProperty(arr, distributionPanel);
            arr = enabledProperty(arr, furExplanation);
            arr = enabledProperty(arr, dispDataComp);
            getForm().setTCProperties(arr);
            getForm().save();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public boolean isFormSavable()
    {
        return true;
        
    }
    
}
