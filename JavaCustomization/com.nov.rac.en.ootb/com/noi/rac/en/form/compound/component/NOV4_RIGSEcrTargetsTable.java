package com.noi.rac.en.form.compound.component;

import com.noi.rac.en.form.compound.panels.NOV4RigsECRForm_Panel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.Registry;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;


public class NOV4_RIGSEcrTargetsTable extends JTreeTable
{
	private static final long serialVersionUID = 1L;
	NOVEcrTreeTableModel treeTableModel;	
	private NOV4RigsECRForm_Panel ecrFormPanel;
	private TCSession session;	
	private Registry reg;
	
	public NOV4_RIGSEcrTargetsTable( final NOV4RigsECRForm_Panel enformPanel, TCSession tcSession, String columnNames[] )
	{
		super( tcSession );		
		this.ecrFormPanel = enformPanel;
		this.session=tcSession;
		this.reg=Registry.getRegistry( this );
			
		treeTableModel = new NOVEcrTreeTableModel( columnNames );
		setModel( treeTableModel );

		for( int index = 0; index < columnNames.length; ++index )
		{
			TableColumn col = new TableColumn( index );
			addColumn( col );
		}
		getTree().setCellRenderer(new TargetIdRenderer(this));

		getColumnModel().getColumn(1).setCellRenderer( new RootCellRenderer() );
		getColumnModel().getColumn(2).setCellRenderer( new RootCellRenderer() );
		getColumnModel().getColumn(3).setCellRenderer( new RootCellRenderer() );		
		setAutoResizeMode( JTreeTable.AUTO_RESIZE_ALL_COLUMNS );

		TCComponent[] affectedDocPartsArr = ecrFormPanel.getECRAffectedDocParts();		
		Vector<TCComponent> affectedDocPartsVec = new Vector<TCComponent>();
		
		for( int index = 0; index < affectedDocPartsArr.length; ++index )
		{
			affectedDocPartsVec.add( affectedDocPartsArr[index] );
		}
		
		loadTabledata( affectedDocPartsVec );
	}

	public void loadTabledata( Vector<TCComponent> affectedDocParts )
	{
		//get the targets and load the table
		try
		{
			for(int index = 0; index < affectedDocParts.size(); ++index )
			{
				addTarget( affectedDocParts.elementAt( index ) );
			}
		}
		catch( Exception tc )
		{
			System.out.print( tc );
		}
	}

	public boolean isCellEditable( int row , int col )
	{			
		return false;
	}	
	
	public void addTarget( TCComponent affectedDocPart )
	{
		try
		{
			String[] reqdProps = { reg.getString("AFFCPARTPANEL.PROP1"), 
					reg.getString("AFFCPARTPANEL.PROP2"), /*"nov4_en_number"*/ };
			String[] retrievedProps = affectedDocPart.getProperties( reqdProps );
			
			// This is change for the 
			String[] enNumbers = affectedDocPart.getTCProperty( "nov4_en_number" ).getStringValueArray();
			
			TCComponentItemRevisionType compItemRevType = ( TCComponentItemRevisionType )session.getTypeService().getTypeComponent( "ItemRevision" );

			TCComponent targetRevItem = null;
			targetRevItem = compItemRevType.findRevision( retrievedProps[0], retrievedProps[1] );
			String targetItemType = null;
			
			if( targetRevItem != null )
				targetItemType = targetRevItem.getTCProperty( reg.getString( "objType.PROP" ) ).toString();
			else
			{
				TCComponentItemType compItemType = ( TCComponentItemType )session.getTypeService().getTypeComponent( "Item" );
				targetRevItem = compItemType.find( retrievedProps[0] );
				targetItemType = targetRevItem.getTCProperty( reg.getString( "objType.PROP" ) ).toString();
			}
		
			if( targetItemType.equalsIgnoreCase( reg.getString( "DocRev.TYPE" ) ) )
			{
            	//NovTreeTableNode node1 = new NovTreeTableNode( targetRevItem, retrievedProps[2], true );
				NovTreeTableNode node1 = new NovTreeTableNode( targetRevItem, enNumbers, true );
				treeTableModel.addRoot( node1 );		
				updateUI();
			}
			if( !targetItemType.equalsIgnoreCase( reg.getString( "DocRev.TYPE" ) ) )
			{
				//NovTreeTableNode node1 = new NovTreeTableNode( targetRevItem, retrievedProps[2], true );
				NovTreeTableNode node1 = new NovTreeTableNode( targetRevItem, enNumbers, true );
				String RevId = targetRevItem.getProperty( "current_revision_id" );
				AIFComponentContext contextRDD[] = ( targetRevItem ).getRelated( reg.getString( "rdd.TYPE" ) );
				for( int index = 0; index < contextRDD.length; ++index )
				{
					TCComponentItem rddcomp =( TCComponentItem )contextRDD[index].getComponent();
					//TCComponentItemRevision rddRev = getItemRevisionofRevID( rddcomp, RevId );
					TCComponentItemRevision rddRev = rddcomp.getLatestItemRevision();
					if( rddRev != null )
					{
						NovTreeTableNode nodeRDD = new NovTreeTableNode( rddRev, enNumbers, false  );
						node1.add(nodeRDD);
					}					
				}		
				treeTableModel.addRoot( node1 );		
				updateUI();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public TCComponentItemRevision getItemRevisionofRevID( TCComponentItem rddItem, String RevId )
	{
		TCComponentItemRevision itemRev = null;
		try
		{
			AIFComponentContext[] revItems=rddItem.getChildren( "revision_list" );
			for( int index = 0; index < revItems.length; ++index )
			{
				String revId = ( ( TCComponentItemRevision )revItems[index].getComponent()).getProperty( "current_revision_id" );
				if( revId.compareTo( RevId ) == 0 )
				{
					itemRev=( TCComponentItemRevision )revItems[index].getComponent();
					break;
				}
			}
		}catch( TCException e )
		{
			System.out.println( e );

		}
		if( itemRev == null )
		{}

		return itemRev;
	}

	private class NOVEcrTreeTableModel extends TreeTableModel
	{
		private static final long serialVersionUID = 1L;
		
		private Vector<Object> columnNamesVec;

		public NOVEcrTreeTableModel( String columnNames[] )
		{
			super();
			columnNamesVec = new Vector<Object>();
			TreeTableNode top = new TreeTableNode();
			setRoot( top );
			top.setModel(this);
			for(int i = 0; i < columnNames.length; i++)
			{
				columnNamesVec.addElement( columnNames[i] );
				modelIndexToProperty.add( columnNames[i] );
			}
		}
		
		public void addRoot( TreeTableNode root )
		{
			TreeTableNode top = ( TreeTableNode )getRoot();
			top.add( root );
		}

		public TreeTableNode getRoot( int index )
		{
			if(((TreeTableNode)getRoot()).getChildCount() > 0)
				return (TreeTableNode)((TreeTableNode)getRoot()).getChildAt(index);
			else
				return null;
		}

		public int getColumnCount()
		{
			return columnNamesVec.size();
		}

		public String getColumnName(int column)
		{
			return (String)columnNamesVec.elementAt(column);
		}

		public void removeNode( int row )
		{
			((TreeTableNode)getRoot()).remove(getNodeForRow(row));
		}
	}

	private class NovTreeTableNode extends TreeTableNode
	{
		private static final long serialVersionUID = 1L;
		public TCComponent context;
		//public HashMap<TCComponent, String> targetItemEnNumberMap = new HashMap<TCComponent, String>();
		public HashMap<TCComponent, String[]> targetItemEnNumberMap = new HashMap<TCComponent, String[]>();

//		public NovTreeTableNode( TCComponent com, String strEnNumber, Boolean dispEnNumber )
//		{
//			context = com;
//			if( dispEnNumber )
//			{
//				targetItemEnNumberMap.put( com, strEnNumber );
//			}
//		}

		public NovTreeTableNode( TCComponent com, String[] strEnNumbers, Boolean dispEnNumber )
		{
			context = com;
			if( dispEnNumber )
			{
				targetItemEnNumberMap.put( com, strEnNumbers );
			}
		}
		
//		public String getProperty( String name )
//		{
//			try 
//			{
//				if( name.equalsIgnoreCase( reg.getString( "ItemID.TYPE" ) ) )
//					return context.getProperty( "item_id" ) == null ? "" : context.getProperty( "item_id" ).toString();
//				if( name.equalsIgnoreCase( reg.getString( "ItemName.TYPE" ) ) )
//					return context.getProperty( "object_name" ) == null ? "" : context.getProperty( "object_name" ).toString();
//				if( name.equalsIgnoreCase( reg.getString( "Rev.TYPE" ) ) )
//					return context.getProperty( "current_revision_id" ) == null ? "" : context.getProperty( "current_revision_id" ).toString();
//				if( name.equalsIgnoreCase( reg.getString( "EnNumber.TYPE" ) ) )
//				{
//					String enNumber = targetItemEnNumberMap.get( context );
//						return( ( enNumber == null || enNumber.length() <= 0 ) ? "" : enNumber ); 
//				}
//			}
//			catch( Exception e ) 
//			{
//				e.printStackTrace();
//			}
//			return "";
//		}

		public String getProperty( String name )
		{
			try 
			{
				if( name.equalsIgnoreCase( reg.getString( "ItemID.TYPE" ) ) )
				{
					String itemID = context.getProperty( "item_id" );
					return ( itemID == null ? "" : itemID );
				}
				if( name.equalsIgnoreCase( reg.getString( "ItemName.TYPE" ) ) )
				{
					String objectName = context.getProperty( "object_name" ); 
					return ( objectName == null ? "" : objectName );
				}
				if( name.equalsIgnoreCase( reg.getString( "Rev.TYPE" ) ) )
				{
					String curRevID = context.getProperty( "current_revision_id" );
					return ( curRevID == null ? "" : curRevID );
				}
				if( name.equalsIgnoreCase( reg.getString( "EnNumber.TYPE" ) ) )
				{
					String[] enNumbers = targetItemEnNumberMap.get( context );
					String enNumberQuamaSep = "";
					
					for( int index = 0; index < enNumbers.length; ++index )
					{
						if( index == 0 )
							enNumberQuamaSep = enNumbers[index];
						else		
							enNumberQuamaSep += "," + enNumbers[index];
					}
					
					return( ( enNumberQuamaSep == null || enNumberQuamaSep.trim().length() <= 0 ) ? "" : enNumberQuamaSep ); 
				}
			}
			catch( Exception e ) 
			{
				e.printStackTrace();
			}
			return "";
		}		
		
		public String getRelation()
		{			
			return " ";
		}

		public String getType()
		{
			return context.getType();
		}
	}
	
	class TargetIdRenderer extends TreeTableTreeCellRenderer
	{   		
		private static final long serialVersionUID = 1L;

		public TargetIdRenderer(JTreeTable arg0)
		{
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{
			
			// Select the current value
			try
			{
				super.getTreeCellRendererComponent( tree, value, sel, expanded, leaf, row, hasFocus );		

				if( row == 0 )
				{
					setText( reg.getString( "affectedDocParts.LABEL" ) );
				}
				else
				{
					setText( ( ( NovTreeTableNode )getNodeForRow( row ) ).context.getProperty( "item_id" ) );
					setIcon( TCTypeRenderer.getIcon( ( ( NovTreeTableNode )getNodeForRow( row ) ).context ) );
				}	
			}
			catch( Exception e )
			{
				System.out.println( e );

			}
			return this;
		}
	}	
	
	class RootCellRenderer extends JLabel implements TableCellRenderer
	{    
		private static final long serialVersionUID = 1L;

		public RootCellRenderer()
		{			
			super();
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
			{
				setText( reg.getString( "" ) );
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			else
			{
				setText(value.toString());
				if(isSelected)
				{
					setOpaque(true);
					setBackground(table.getSelectionBackground());
					setForeground( Color.WHITE );
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
			}
			return this;
		}
	}

}
