package com.noi.rac.en.form.compound.data;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel;
import com.noi.rac.en.form.compound.component.NOVEnGenSearchPanel;
import com.noi.rac.en.form.compound.component.NOVEprTargetPanel;
import com.noi.rac.en.form.compound.util.NOVCustomFormProperties;
import com.noi.util.components.AbstractReferenceLOVList;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.iTextArea;

public class _EngPreReleaseForm_DC extends NOVCustomFormProperties
{
	public NOVEnGenSearchPanel projectsPanel;
	public NOVEnDistributionPanel distributionPanel;
	public iTextArea eprComments;
	public NOVEprTargetPanel targetPanel;
	public NOVLOVPopupButton drafted;
	public NOVDateButton draftedon;
	public NOVLOVPopupButton approved;
	public NOVDateButton approvedon;
	AbstractReferenceLOVList distSource;
	AbstractReferenceLOVList distTarget;
	
	String[]   propertyNames = new String[] { "projects","distribution","prereleasecomments", 
			"eprtargetsdisposition","draftedby","draftedon","approvedby",
			"approvedon"
			};
	public _EngPreReleaseForm_DC()
	{
		TCSession tcSession =  (TCSession) AIFUtility.getDefaultSession();
		projectsPanel = new NOVEnGenSearchPanel(tcSession,"TC_Project");
		distributionPanel = new NOVEnDistributionPanel();
		eprComments = new iTextArea();
		eprComments.setLengthLimit(1900);
		drafted = new NOVLOVPopupButton();
		approved = new NOVLOVPopupButton();
		draftedon = new NOVDateButton();
		approvedon = new NOVDateButton();
		
		addProperty("draftedby", drafted);
		addProperty("draftedon", draftedon);
		addProperty("approvedby", approved);
		addProperty("approvedon", approvedon);
		addProperty("projects", projectsPanel);
		addProperty("distribution", distributionPanel);
		addProperty("prereleasecomments", eprComments);		
	}
	
	@Override
	public String[] getPropertyNames() 
	{
		return propertyNames; 
	}

	@Override
	public void setForm(TCComponent form) 
	{
		
		if (form!=null) 
		{
			try
	        {   super.setForm(form);
		        //String[] props = form.getPropertyNames();
		        targetPanel = new NOVEprTargetPanel(this);
		        addProperty("eprtargetsdisposition", targetPanel);
		        super.getFormProperties(propertyNames);
		        targetPanel.getTargets((TCComponentForm)this.getForm());
		        targetPanel.loadTblData();
		        
		        String currentGroup="";
				distSource = new AbstractReferenceLOVList();				
				TCComponentGroup group=form.getSession().getCurrentGroup();
				String groupname=group.getFullName();
				if(groupname.indexOf('.')>=0)
					currentGroup=groupname.substring(0, groupname.indexOf('.'));
				else
					currentGroup=groupname;
				currentGroup=currentGroup+":";
				distSource.initClassECNERO(form.getSession(),"ImanAliasList","alName", currentGroup);
				distTarget = new AbstractReferenceLOVList();
				distributionPanel = new NOVEnDistributionPanel();
				
				
				//addProperty("eprtargetsdisposition", targetPanel);
				addProperty("distribution", distributionPanel);
				super.getFormProperties(propertyNames);
				
	        }
	        catch ( Exception e )
	        {
	            e.printStackTrace();
	        }	
		}
	}
	
	@Override
	public void populateFormComponents() 
	{
	    if(getForm() == null) return;    
	    
	    String[] projects = ((TCProperty)imanProperties.get(projectsPanel)).getStringValueArray();
	    if (projects!=null) 
	    {
	    	projectsPanel.populateTargetList(projects);	
		}
	    String[] diststributionList = ((TCProperty)imanProperties.get(distributionPanel)).getStringValueArray();
	    if (diststributionList!=null) 
	    {
	    	distributionPanel.populateTargetList(diststributionList);	
		}
	    
	    String[] arr1 = ((TCProperty)imanProperties.get(distributionPanel)).getStringValueArray();
		for (int i=0;i<arr1.length;i++) {
			distTarget.addItem(arr1[i],arr1[i]);
			distSource.addExclusion(arr1[i]);
		}
		distributionPanel.init(distSource,distTarget,new java.awt.Dimension(250,100));
	    
	    eprComments.setText(((TCProperty)imanProperties.get(eprComments)).getStringValue());
	    
		drafted.setSelectedText(((TCProperty)imanProperties.get(drafted)).getStringValue());
		approved.setSelectedText(((TCProperty)imanProperties.get(approved)).getStringValue());

		draftedon.setDate(((TCProperty)imanProperties.get(draftedon)).getDateValue());
		approvedon.setDate(((TCProperty)imanProperties.get(approvedon)).getDateValue());
	}
	
	
	@Override
	public void saveFormData()
	{
		try 
		{
			TCProperty[] arr=null;
			((TCProperty)imanProperties.get(projectsPanel)).setStringValueArrayData( projectsPanel.getTargetListObjects());
			//((TCProperty)imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel.getTargetListObjects());
			
			/*String[] disArr = distTarget.getList();
			if (disArr != null && distributionPanel.isEnabled())
				((TCProperty)imanProperties.get(distributionPanel)).setStringValueArray(disArr);*/
			((TCProperty)imanProperties.get(distributionPanel)).setStringValueArrayData(distributionPanel.getAllTargetListObjects());
			
			((TCProperty)imanProperties.get(eprComments)).setStringValueData(eprComments.getText());
			
			Object selObj1 = drafted.getSelectedObject();
			if (selObj1!=null) 
			{
				((TCProperty)imanProperties.get(drafted)).setStringValueData(selObj1.toString());
			}
			Object selObj3 = approved.getSelectedObject();
			if (selObj3!=null) 
			{
				((TCProperty)imanProperties.get(approved)).setStringValueData(selObj3.toString());
			}

			String draftedDate = draftedon.getDateString();
			if (draftedDate != null) 
			{
				((TCProperty)imanProperties.get(draftedon)).setDateValueData(draftedon.getDate());	
			}
			String approvedDate = approvedon.getDateString();
			if (approvedDate != null) 
			{
				((TCProperty)imanProperties.get(approvedon)).setDateValueData(approvedon.getDate());	
			}

			arr = enabledProperty(arr,drafted);
			arr = enabledProperty(arr,approved);
			arr = enabledProperty(arr,draftedon);
			arr = enabledProperty(arr,approvedon);
			arr = enabledProperty(arr,projectsPanel);
			arr = enabledProperty(arr,distributionPanel);
			arr = enabledProperty(arr,eprComments);
			
			getForm().setTCProperties(arr);
			getForm().save();
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
	}

	public void saveOnlyDispData(TCComponent[] targetdispComps) 
	{
		TCProperty[] arr=null;		
		TCProperty dispProp = ((TCProperty)imanProperties.get(targetPanel));
		if (dispProp!=null)
		{
			try 
			{
				dispProp.setReferenceValueArray(targetdispComps);
				
				arr = enabledProperty(arr,projectsPanel);
				arr = enabledProperty(arr,distributionPanel);
				arr = enabledProperty(arr,eprComments);
				arr = enabledProperty(arr,targetPanel);
				
				arr = enabledProperty(arr,drafted);
				arr = enabledProperty(arr,approved);
				arr = enabledProperty(arr,draftedon);
				arr = enabledProperty(arr,approvedon);
				
				getForm().setTCProperties(arr);
				getForm().save();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
	}
}
