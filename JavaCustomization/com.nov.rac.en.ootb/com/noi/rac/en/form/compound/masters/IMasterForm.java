/*
 * Created on Aug 2, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound.masters;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
/**
 * @author HughesTS
 *
 * this interface define the common function of masre form
 */
public interface IMasterForm {
	
	abstract JComponent getPanel();

	abstract void       saveForm();
	
	abstract void       lockForm(boolean lock);
	
	abstract void       setDisplayProperties();
	
	public  ImageIcon getTabIcon();

	public String getPanelName();

	public String getTip();
	
	public boolean isFormSavable();

}
