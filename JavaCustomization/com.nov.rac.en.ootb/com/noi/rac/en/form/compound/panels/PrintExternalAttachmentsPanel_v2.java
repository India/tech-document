
package com.noi.rac.en.form.compound.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import com.noi.NationalOilwell;
import com.noi.rac.commands.newdataset.NewDatasetDialog;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.Miscellaneous;
import com.noi.rac.en.util.components.PrintExternalAttachmentsTableModel;
import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.teamcenter.rac.aif.AbstractAIFApplication;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class PrintExternalAttachmentsPanel_v2 extends JPanel implements ActionListener,ListSelectionListener {

	private JPanel parentPanel;
	private Registry registry;
	JTable theTable = new JTable();
	JScrollPane tablePane;
	JButton attachmentsBoxPlus;
	JButton attachmentsBoxMinus;
	JButton viewAttachment;
	ArrayList tableData = new ArrayList();
	Vector    theData = new Vector();
	AbstractAIFApplication app=null;

	Vector attachments = new Vector();
	
	PrintExternalAttachmentsTableModel theModel=null;
	int selectedItemRow=-1;
    INOVCustomFormProperties fp; 
    boolean isFormLocked=false;
    String strBorderedTitle;    
	
	public PrintExternalAttachmentsPanel_v2(INOVCustomFormProperties fp,boolean locked, JPanel parentPanel, 
			String strBorderedTitle )
	{
		super();
		this.parentPanel = parentPanel;
		isFormLocked = locked;
		this.fp = fp;
		this.strBorderedTitle = strBorderedTitle;
		try {
			 AIFComponentContext[] comps=fp.getForm().getSecondary();//("_ChangeDescriptionDocument_");			 
	        for (int i=0;i<comps.length;i++) 
	        {
	        	//comps[i].getContextDisplayName().equals("_ChangeDescriptionDocument_");
	            if ((comps[i].getComponent() instanceof TCComponentDataset)&&(comps[i].getContextDisplayName().equals("_ChangeDescriptionDocument_"))) 
	            {
	                attachments.add(comps[i].getComponent());
	            }
	        }
	    	theModel = new PrintExternalAttachmentsTableModel(attachments);
		}
		catch (Exception e) {
			
		}
		setInitialData(new ArrayList());
		createUI();	
	}


	public TableModel getTableModel() { return theTable.getModel(); }
	
    public void setInitialData(ArrayList al) {
    	
    	if (attachments.size() > 0) return;
    	
    	tableData = al;
    	for (int i=0;i<tableData.size();i++)
    		theData.add(tableData.get(i));
    	theModel = new PrintExternalAttachmentsTableModel(theData);
    }
	
    public void createUI() {
    	
    	registry = Registry.getRegistry(this);
    	
    	//setLayout(new GridBagLayout());
    	//setBackground(new Color(225,225,225));
    	setPreferredSize(new Dimension(845,180));
    	TitledBorder tb;
    	//if(((JCheckBox)fp.getProperty("newrelease")).isSelected())
    	tb = new javax.swing.border.TitledBorder( strBorderedTitle );
    	/*else
    	{
    		tb = new javax.swing.border.TitledBorder(registry.getString("PrintExternalAttachmentsPanel.Title2"));
    	}*/
    	tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
    	setBorder(tb);

        ListSelectionModel rowSM = theTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        theTable.setModel(theModel);
        theTable.setPreferredScrollableViewportSize(new Dimension(575,200));
        TableColumn col = null;
        
        col = theTable.getColumnModel().getColumn(0);
        int newwidth = 125;
        col.setPreferredWidth(newwidth);
        
        col = theTable.getColumnModel().getColumn(1);
        col.setCellEditor(null);
        newwidth = 250;
        col.setPreferredWidth(newwidth);
    	
        JTableHeader head = theTable.getTableHeader();
        head.setBackground(NationalOilwell.NOVPanelContrast);
        
        tablePane = new JScrollPane(theTable);
        
        ImageIcon plusIcon=null;
        try {
           plusIcon = registry.getImageIcon("enadd.IMG");
        	//plusIcon = new ImageIcon(Miscellaneous.getImageIcon(registry.getString("enadd.IMG")));
        }
        catch (Exception ie) {
        	System.out.println(ie);
        }
        
		ImageIcon  minusIcon = registry.getImageIcon("enremove.IMG");
		//ImageIcon minusIcon = new ImageIcon(Miscellaneous.getImageIcon(registry.getString("enremove.IMG")));
        
		attachmentsBoxPlus = new JButton(plusIcon);
		attachmentsBoxMinus = new JButton(minusIcon);
		viewAttachment = new JButton(registry.getString("PrintExternalAttachmentsPanel.ViewSelectedDocument"));
		try
		{
			isFormLocked=fp.getForm().getLogicalProperty("locked");
		}
		catch(Exception e)
		{
			
		}
		try
		{
			isFormLocked=fp.getForm().getLogicalProperty("locked");
		}
		catch(Exception e)
		{
			
		}
		//attachmentsBoxPlus.setEnabled(((JTable)fp.getProperty("itemrevs")).isEnabled() && !isFormLocked);
		//attachmentsBoxMinus.setEnabled(((JTable)fp.getProperty("itemrevs")).isEnabled() && !isFormLocked);
		//viewAttachment.setEnabled(((JTable)fp.getProperty("itemrevs")).isEnabled());
		viewAttachment.setVisible(false);
		attachmentsBoxPlus.addActionListener(this);
		attachmentsBoxMinus.addActionListener(this);
		viewAttachment.addActionListener(this);
       // this.add(tablePane);
        //this.add(attachmentsBoxPlus);
        //this.add(attachmentsBoxMinus);
        //this.add(viewAttachment);
        
        setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
		gbc.gridy = 0;
		tablePane.setToolTipText(registry.getString("PrintExternalAttachmetnsPanel.TablePaneToolTip"));
		tablePane.setPreferredSize(new Dimension(800,80));
        this.add(tablePane,gbc);
        
        JPanel bpanel = new JPanel();
        //bpanel.setBackground(new Color(225,225,225));
        attachmentsBoxPlus.setToolTipText(registry.getString("PrintExternalAttachmentsPanel.AddButtonToolTip"));
        attachmentsBoxMinus.setToolTipText(registry.getString("PrintExternalAttachmentsPanel.RemoveButtonToolTip"));
        bpanel.add(attachmentsBoxPlus);
        bpanel.add(attachmentsBoxMinus);
        bpanel.add(viewAttachment);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        this.add(bpanel,gbc);
        //resizePanel();
    }
    
    public void setControlState( boolean isReleased )
	{
       	//theTable.setEnabled( !isReleased );
    	attachmentsBoxPlus.setEnabled( !isReleased );
    	attachmentsBoxMinus.setEnabled( !isReleased );
    }

    public void actionPerformed(ActionEvent event)
    {
    	if (event.getSource() == attachmentsBoxPlus && !isFormLocked) 
    	{
    		NewDatasetDialog dlg = 
    			new NewDatasetDialog(AIFUtility.getCurrentApplication().getDesktop(),
    					AIFUtility.getCurrentApplication(),//fp.getForm().getSession().getCurrentApplication(),
    					fp.getForm(), true);
    		dlg.setModal(true);
    		dlg.setLocationRelativeTo(this);
    		if(Toolkit.getDefaultToolkit().getScreenResolution()>96)
    		{
    		dlg.setSize(600, 220);
    		}
    		else
    		{
    			dlg.setSize(500, 220);
    		}
    		dlg.setDsName(registry.getString("DEFLTATTACH.NAME"));
    		dlg.setVisible(true);
    		if (dlg.getDataset() != null)
    		{
    		    Vector v = new Vector();
    		    v.add(dlg.getDataset());
    		    ((PrintExternalAttachmentsTableModel)theTable.getModel()).addRows(v);
    		}
    		fp.saveFormData();
    		
    	}
    	else if (event.getSource() == attachmentsBoxMinus && !isFormLocked)
    	{
    		if(theTable.getModel().getRowCount()>0)
    		{
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
    			{
	    			TCComponentDataset ds = ((PrintExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
	    			TCComponentForm form = (TCComponentForm)fp.getForm();
	    			try
	    			{
	        			DeleteRelationHelper delete = new DeleteRelationHelper(
	                            form,ds,registry.getString("CHNGDESCDOC.TYPE"));
	                    delete.deleteRelation();
						fp.saveFormData();   //  Vivek
	    			}
	    			catch (Exception e) {
	    				System.out.println("Could not remove attachment: "+e);
	    			}
	    			
	    			((PrintExternalAttachmentsTableModel)theTable.getModel()).removeRow(selectedItemRow);
    			}
    		}		
    	}
    	else if (event.getSource() == viewAttachment)
    	{
    		if(theTable.getModel().getRowCount()>0)
    		{
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
	    		{
    				TCComponentDataset ds =((PrintExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
		    			try 
		    			{
			             
			               String   cmd="";
			               String dstype = ds.getProperty("object_type");
			               
			               String fetchFileType = "";
			               fetchFileType = getFileRefType(ds);
			               if(null!=fetchFileType && fetchFileType.length()>0)
			               {   
			            	   System.out.println("fetchFileType ***** "+fetchFileType);
			            	   String fileName = null;
			            	   File[] fl =ds.getFiles(fetchFileType);
			            	   if(fl!=null)
			            	   {
									fileName = fl[0].getAbsolutePath();
									
									if (fileName!=null && fileName.length() > 0) 
									{
										System.out.println("File name : "+ fl[0].getAbsolutePath());
										cmd = "rundll32 shell32.dll,OpenAs_RunDLL "+ fileName;// windPath+"\\temp\\"+newName;
										Runtime.getRuntime().exec(cmd);
									}
			            	   }
			               }
			               else
			               {
			            	   MessageBox.post(registry.getString("Reference2ViewMissing.MSG"),registry.getString("ErrorDocView.MSG"),MessageBox.INFORMATION);
			               }
	    			}
	    			catch (Exception e) {
	    				e.printStackTrace();
	    				MessageBox.post(registry.getString("FailedonRequiredFun.MSG"),e.toString(),registry.getString("ErrorDocView.MSG"),MessageBox.INFORMATION);
	    			}
	    		} 
    		}
    	
    		/*
    		if(theTable.getModel().getRowCount()>0){
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
	    			{
		    			TCComponentDataset ds =((PrintExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
		    			try {
			               TCPreferenceService prefServ = fp.getForm().getSession().getPreferenceService();
			               String server = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGWSServices_server" );
			               String port = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGWSServices_port" );
			               String context = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGWSServices_context" );
			               String[] fname=null;
			               String   cmd="";
			               String   url="";
			               String dstype = ds.getProperty("object_type");
			               if (dstype.equalsIgnoreCase("msword")) 
			                   fname = ds.getFileNames("word");
			               else if (dstype.equalsIgnoreCase("mswordx")) 
			                   fname = ds.getFileNames("word");
			               else if (dstype.equalsIgnoreCase("pdf")) 
			            	   fname = ds.getFileNames("PDF");
			               else if (dstype.equalsIgnoreCase("acaddwg")) 
			            	   fname = ds.getFileNames("DWG");
			               else if (dstype.equalsIgnoreCase("image")) 
			            	   fname = ds.getFileNames("Image");
			               else if (dstype.equalsIgnoreCase("msexcel")) 
			            	   fname = ds.getFileNames("excel");
		
			               String newName = StringUtils.replaceSpecialChars(fname[0]);
			               url = "http://"+server+":"+port+context+"FileStreamer/"+ds.getUid()+"/"+ds.getProperty("timestamp")+"/"+newName;
			               Miscellaneous.writeHTTPStreamToTemp(url,newName);	   
			               String portalPath = WindowsRegistry.getWindowsPathToPortal();
			               String windPath=portalPath.replaceAll("/", "\\");
			               cmd = "rundll32 shell32.dll,OpenAs_RunDLL "+windPath+"\\temp\\"+newName;
			               
			               Runtime.getRuntime().exec(cmd);
	    			}
	    			catch (Exception e) {
	    				MessageBox.post("Could not execute required function:",e.toString(),"Error viewing document",MessageBox.INFORMATION);
	    			}
	    		} 
    		}
    	*/}
		//resizePanel();
	
    }
    
    public String getFileRefType(TCComponentDataset dataset)
    {
    	String strRefType=null;
    	String datasetType = dataset.getType();
    	try 
    	{
    		strRefType = dataset.getProperty("ref_names");
    		
    		if( strRefType!=null && strRefType.indexOf(",")!=-1 )
    			strRefType = strRefType.split(",")[0];
    		
    		return strRefType;
		
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strRefType;
    }
    
	public void valueChanged(ListSelectionEvent e) {
		
        if (e.getValueIsAdjusting()) return;

        ListSelectionModel lsm =
            (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
        	;
        } else {
            selectedItemRow = lsm.getMinSelectionIndex();
            viewAttachment.setVisible(true);
            viewAttachment.setEnabled(true);
        }
		
	}
    public void resizePanel() {
    	
        TableModel tm = theTable.getModel();
        int tableHeight = (tm.getRowCount()+1)*18;
        if (tm.getRowCount() <= 0){
        	tablePane.setVisible(false); 
            viewAttachment.setVisible(false);
            viewAttachment.setEnabled(false);

        }
        else {
        	tablePane.setVisible(true);
        	if(selectedItemRow >= 0 && selectedItemRow < tm.getRowCount()){
        		viewAttachment.setVisible(true);
        		viewAttachment.setEnabled(true);
        	}
        	else{
                viewAttachment.setVisible(false);
                viewAttachment.setEnabled(false);
        	}
        }
        if (tableHeight > 100) tableHeight=100;
        if (tableHeight <= 54) tableHeight+=4;
        
        tablePane.setPreferredSize(new Dimension(600,tableHeight));
        this.setPreferredSize(new Dimension(630,tableHeight+75));
        this.setMinimumSize(new Dimension(630,tableHeight+75));
        parentPanel.revalidate();
        parentPanel.repaint();
    	
    }
}
