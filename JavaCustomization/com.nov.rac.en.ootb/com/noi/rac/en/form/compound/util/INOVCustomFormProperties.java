/*
 * Created on Jul 15, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound.util;
import java.awt.Component;
import java.util.Hashtable;

import javax.swing.JComponent;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;
/**
 * @author hughests
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface INOVCustomFormProperties {
	
	abstract JComponent 	getProperty(String name);
    abstract void       	addProperty(String name,Component comp);
    abstract void           disableProperty(String name);
    abstract void           disableProperties(String[] names);
    abstract void           disableAllProperties();    
    abstract void       	populateFormComponents();
    abstract void       	saveFormData();
    abstract void 			addImanProperty(JComponent comp,TCProperty prop);
    abstract TCProperty	getImanProperty(JComponent comp);
    abstract void           setForm(TCComponent form);
    abstract TCComponent  getForm();
    abstract String[]       getPropertyNames();
    abstract Hashtable      getComponents();
    abstract Hashtable      getImanProperties();
    abstract void           clearForm();
}
