package com.noi.rac.en.form.compound.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel;
import com.noi.rac.en.form.compound.component.NOVEnGenSearchPanel;
import com.noi.rac.en.form.compound.component.NOVEprTargetPanel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.noi.rac.en.util.components.NOVLOVPopupButton;
//import com.nov.rac.commands.newitem.NOVLocaleDialog;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

public class _EngPreReleaseForm_Panel extends PrintablePanel 
{

	private static final long serialVersionUID = 1L;
	public TCComponentForm eprform;
	private Registry  reg;
	TCComponentItem targetItem = null;
	private INOVCustomFormProperties formProps;
	
	public _EngPreReleaseForm_Panel()
	{
		super();
		reg=Registry.getRegistry(this);
	}
	public void createUI(INOVCustomFormProperties form_Props) 
	{
		formProps = form_Props;
		setLayout(new PropertyLayout());
		
		JPanel pplInfoPanel = buildPeopleInfoPanel();
		pplInfoPanel.setBorder(new TitledBorder(""));
		NOVEprTargetPanel targetPanel = (NOVEprTargetPanel) formProps.getProperty("eprtargetsdisposition");		
		targetPanel.setTitleBorderText(reg.getString("PrelBOMRelTarget.MSG"));
		NOVEnGenSearchPanel searchPanel = (NOVEnGenSearchPanel) formProps.getProperty("projects");
		searchPanel.setTitleBorderText(reg.getString("SelectProject.MSG"));
		NOVEnDistributionPanel distributionPanel = (NOVEnDistributionPanel)formProps.getProperty("distribution");
		distributionPanel.setTitleBorderText(reg.getString("SelectDistribution.MSG"));
		iTextArea eprComments =  (iTextArea)formProps.getProperty("prereleasecomments");
		
		JPanel eprCommentsPanel = new JPanel(new PropertyLayout());
		JScrollPane eprCommentsPane = new JScrollPane(eprComments);
		eprCommentsPane.setPreferredSize(new Dimension(575,150));
		TitledBorder tb = new TitledBorder(reg.getString("EPRComments.MSG"));
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		eprCommentsPanel.setBorder(tb);		
				
		eprCommentsPanel.add("1.1.left.center",eprCommentsPane);
		
	/*	TCPreferenceService prefrenceSrv = form_Props.getForm().getSession().getPreferenceService();
		
		boolean isLocale = prefrenceSrv.isTrue(TCPreferenceService.TC_preference_all, "NOV_show_localization_button");
		if (isLocale) 
		{
			ImageIcon icon = reg.getImageIcon("locale.IMG");
			final TCProperty prerelComments = formProps.getImanProperty(formProps.getProperty("prereleasecomments"));
			final JButton localeBtn = new JButton(icon);
			localeBtn.setPreferredSize(new Dimension(20,20));
			localeBtn.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent actEvt) 
				{
					NOVLocaleDialog localeDlg = new NOVLocaleDialog(prerelComments);						
					//localeDlg.setSize(350, 325);
					int x = localeBtn.getLocationOnScreen().x- localeDlg.getWidth();
					int y = localeBtn.getLocationOnScreen().y- localeDlg.getHeight();
					localeDlg.setLocation(x, y);
					localeDlg.setVisible(true);
				}
			});
			eprCommentsPanel.add("1.2.top.center",localeBtn);	
		}		*/
		
		add("1.1.left.center",pplInfoPanel);
		add("2.1.left.center",targetPanel);
		add("3.1.left.center",searchPanel);
		add("4.1.left.center",distributionPanel);
		add("5.1.left.center",eprCommentsPanel);
		
	}
	
	public JPanel buildPeopleInfoPanel()
	{
		JPanel pplInfoPanel = new JPanel(new PropertyLayout());
		JPanel leftpplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		JPanel rightpplInfoPanel = new JPanel(new PropertyLayout(5,5,5,5,5,5));
		
		((NOVLOVPopupButton)formProps.getProperty("draftedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("draftedby")).setPreferredSize(new Dimension(190,21));
		((NOVLOVPopupButton)formProps.getProperty("approvedby")).setEnabled(false);
		((NOVLOVPopupButton)formProps.getProperty("approvedby")).setPreferredSize(new Dimension(190,21));
		((NOVDateButton)formProps.getProperty("draftedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("draftedon")).setPreferredSize(new Dimension(200,21));
		((NOVDateButton)formProps.getProperty("approvedon")).setEnabled(false);
		((NOVDateButton)formProps.getProperty("approvedon")).setPreferredSize(new Dimension(200,21));
        leftpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("Draftedby.LABLE")));
		leftpplInfoPanel.add("1.2.left.center",((NOVLOVPopupButton)formProps.getProperty("draftedby")));
		leftpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("Approvedby.LABLE")));
		leftpplInfoPanel.add("2.2.left.center",((NOVLOVPopupButton)formProps.getProperty("approvedby")));
		
		rightpplInfoPanel.add("1.1.left.center",new NOIJLabel(reg.getString("DraftedDate.LABLE")));
		rightpplInfoPanel.add("1.2.left.center",((NOVDateButton)formProps.getProperty("draftedon")));
		rightpplInfoPanel.add("2.1.left.center",new NOIJLabel(reg.getString("Approvedon.LABLE")));
		rightpplInfoPanel.add("2.2.left.center",((NOVDateButton)formProps.getProperty("approvedon")));
		
		pplInfoPanel.add("1.1.left.center",leftpplInfoPanel);
		pplInfoPanel.add("1.2.left.center",rightpplInfoPanel);
		
		return pplInfoPanel;
	}

	
	public void setForm(TCComponentForm form)
	{
		eprform=form;	
	}
}
