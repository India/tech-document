package com.noi.rac.en.form.compound.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import com.noi.rac.en.NationalOilwell;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.RelatedEnTableModel;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.editproperties.EditPropertiesDialog;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RelatedEnPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private Registry registry;
	private int selectedItemRow = -1;
	
	public RelatedEnTableModel theModel = null;
	public TCTable relatedENTable = new TCTable();
	INOVCustomFormProperties novFormProperties;
	NOV4RigsECRForm_Panel ECRPanel;
	JScrollPane tablePane;
	EditPropertiesDialog propCmd = null;

	public RelatedEnPanel(NOV4RigsECRForm_Panel ecrPanel,INOVCustomFormProperties novFormProperties) {

		super();
		this.novFormProperties = novFormProperties;
		ECRPanel = ecrPanel;
		createUI();
	}

	private void createUI() {
		// TODO Auto-generated method stub

		registry = Registry.getRegistry(this);
		setPreferredSize(new Dimension(500, 80));
		TitledBorder tb = new javax.swing.border.TitledBorder(registry.getString("RelatedEnPanel.RelatedEns"));
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		setBorder(tb);

		theModel = new RelatedEnTableModel(ECRPanel);
		relatedENTable.setModel(theModel);
		relatedENTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
		TableColumn col = null;
		col = relatedENTable.getColumnModel().getColumn(0);
		
		relatedENTable.addMouseListener( new MouseAdapter()
			{
				public void mouseClicked( MouseEvent e )
				{
					if( e.getClickCount() == 2 )
					{
						openENForm();
					}
				}
			}
		);

		int newwidth = 233;
		col.setPreferredWidth(newwidth);

		col = relatedENTable.getColumnModel().getColumn(1);
		col.setCellEditor(null);
		newwidth = 235;
		col.setPreferredWidth(newwidth);

		JTableHeader head = relatedENTable.getTableHeader();
		head.setBackground(NationalOilwell.NOVPanelContrast);

		tablePane = new JScrollPane( relatedENTable );
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		tablePane.setPreferredSize(new Dimension(470, 100));
		this.add(tablePane, gbc);

	}

	private void openENForm()
	{
		selectedItemRow = relatedENTable.getSelectedRow();
			AIFComponentContext[] enProp = ECRPanel.getRelatedEn();
		TCComponent enForm = ( TCComponent )enProp[selectedItemRow].getComponent();

		Object[] args = new Object[2];
		args[0] = AIFUtility.getActiveDesktop();
		args[1] = (InterfaceAIFComponent)enForm;
		TCSession session = (TCSession) AIFUtility.getDefaultSession();	
		AbstractAIFCommand openCmd;
		try
		{
			openCmd = session.getOpenCommand(args);
			openCmd.executeModal();
		}
		catch( Exception exception )
		{
			exception.printStackTrace();
		}

	}
	
	public TableModel getTableModel()
	{
		return relatedENTable.getModel();
	}

}
