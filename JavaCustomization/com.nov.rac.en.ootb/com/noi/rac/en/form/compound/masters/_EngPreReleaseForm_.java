package com.noi.rac.en.form.compound.masters;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import com.noi.rac.en.form.compound.data._EngPreReleaseForm_DC;
import com.noi.rac.en.form.compound.panels._EngPreReleaseForm_Panel;
import com.teamcenter.rac.kernel.TCComponentForm;

public class _EngPreReleaseForm_ extends BaseMasterForm
{

	_EngPreReleaseForm_DC eprDC;
	_EngPreReleaseForm_Panel eprPanel;
	String masterFormName="";
	
	public _EngPreReleaseForm_(TCComponentForm master)
	{
		super();
		masterForm = master;
		
		try 
		{
			masterFormName = masterForm.getProperty("object_name");
		}
		catch (Exception e) 
		{
			;
		}
		
		eprDC = new _EngPreReleaseForm_DC();
		eprPanel = new _EngPreReleaseForm_Panel();
		
		eprDC.setForm(masterForm);
		eprDC.populateFormComponents();
		eprPanel.setForm(masterForm);
		eprPanel.createUI(eprDC);
		
		
	}
	public JComponent getPanel() 
	{
		return eprPanel;
	}

	public ImageIcon getTabIcon() 
	{
		return null;
	}

	public String getTip() 
	{
		return null;
	}

	public void lockForm(boolean lock) 
	{
		
	}

	public void saveForm() 
	{
		eprDC.saveFormData();
	}

	public void setDisplayProperties()
	{
		
	}
	@Override
	public boolean isFormSavable() {
		// TODO Override this method to implement form related validations
		return true;
	}
	
}