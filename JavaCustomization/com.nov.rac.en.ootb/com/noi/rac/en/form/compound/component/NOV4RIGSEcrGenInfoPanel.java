/** \file 
    \brief This file contains a class NOV4RIGSEcrGenInfoPanel which which will be used by NOV4_RIGSEngChangeRequestPanel .
*/

package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.form.compound.util.NOVTextArea;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVDateButton;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

/** \brief Declaration for the class NOV4RIGSEcrGenInfoPanel, it will create the and sets the properties for following properties of the 
 		   RIG-ECR form.
		   Product Model, Project/Serial #, Rig Name, Related Documents, Customer, Priority,
		   Reason for Change Panel,
		   Change Description Panel,
		   ECRApprovalGroupPanel,
		   Comments,
		   Distribution Panel,
		   Reference Attachments,
*/
public class NOV4RIGSEcrGenInfoPanel extends JPanel
{
	///Serial version UID for the serialization.
	private static final long serialVersionUID = 1L;
	
	///Registry for getting the values which are required to be displayed.
	private Registry reg;
	
	///INOVCustomFormProperties required for getting the form properties and setting their values.
	private INOVCustomFormProperties ecrFormProps;

	/** \brief Creates the Instance of the NOV4RIGSEcrGenInfoPanel.
	      	   It creates different panels and positions them at appropriate place.
	    \param formProperties A form from which we can retrieve the properties and associate them with the components.	   		
	*/
	public NOV4RIGSEcrGenInfoPanel( INOVCustomFormProperties formProperties )
	{
		reg = Registry.getRegistry( this );
		this.ecrFormProps = formProperties;

		JPanel ecrInfoPanel = createECRInfoPanel();
		
		JPanel reasonForChangePanel = new JPanel( new PropertyLayout() );
		JTextField reasonForChangeText = ( JTextField )( ecrFormProps.getProperty( "nov4_reason_change" ) );
		reasonForChangeText.setPreferredSize( new Dimension( 200, 21 ) );
		NOVTextArea explanationText = ( NOVTextArea )ecrFormProps.getProperty( "nov4_explanation" );
		//explanationText.setLengthLimit( 1700 );
		explanationText.setLineWrap(true);
		explanationText.setWrapStyleWord(true);
		JScrollPane explanationPane = new JScrollPane( explanationText );
		explanationPane.setPreferredSize( new Dimension( 759, 50 ) );
		TitledBorder titleBorderReason = new TitledBorder( reg.getString( "reasonForChange.LABEL" ) );
		titleBorderReason.setTitleFont( new Font( "Dialog", java.awt.Font.BOLD, 12 ) );
		reasonForChangePanel.setBorder( titleBorderReason );
		reasonForChangeText.setEnabled( false );
		explanationText.setEnabled( false );
		reasonForChangePanel.add( "1.1.left.center", reasonForChangeText );
		reasonForChangePanel.add( "2.1.left.center", explanationPane );
		
		JPanel changeDescriptionPanel = new JPanel( new PropertyLayout() );
		NOVTextArea changeDescriptionText = ( NOVTextArea )ecrFormProps.getProperty( "nov4_chage_desc" );
		//changeDescriptionText.setLengthLimit( 1700 );
		changeDescriptionText.setLineWrap(true);
		changeDescriptionText.setWrapStyleWord(true);
		JScrollPane changeDescriptionPane = new JScrollPane( changeDescriptionText );
		changeDescriptionPane.setPreferredSize( new Dimension( 759, 50 ) );
		TitledBorder titleBorderDesc = new TitledBorder( reg.getString( "changeDescription.LABEL" ) );
		titleBorderDesc.setTitleFont( new Font( "Dialog", java.awt.Font.BOLD, 12 ) );
		changeDescriptionPanel.setBorder( titleBorderDesc );
		changeDescriptionPanel.add( "1.1.left.center", changeDescriptionPane );
		changeDescriptionText.setEnabled( false );
		
		JPanel approvalGroupPanel = createApprovalGroupPanel();

		JPanel commentsPanel = new JPanel( new PropertyLayout() );
		NOVTextArea commentText = ( NOVTextArea )ecrFormProps.getProperty( "nov4_comments" );
		//commentText.setLengthLimit(1700);
		commentText.setLineWrap(true);
		commentText.setWrapStyleWord(true);
		JScrollPane commentTextPane = new JScrollPane( commentText );
		commentTextPane.setPreferredSize( new Dimension( 759, 50 ) );
		TitledBorder titleBorderComments = new TitledBorder( reg.getString( "comments.LABEL" ) );
		titleBorderComments.setTitleFont( new Font( "Dialog", java.awt.Font.BOLD, 12 ) );
		commentsPanel.setBorder( titleBorderComments );
		commentText.setEnabled( false );
		commentsPanel.add( "1.1.left.center", commentTextPane );
		
		setLayout(new PropertyLayout( 5 , 5 , 5 , 5 , 5 , 5 ) );
		
		this.add( "1.1.left.center", ecrInfoPanel );
		this.add( "2.1.left.center", reasonForChangePanel );		
		this.add( "3.1.left.center", changeDescriptionPanel );
		this.add( "4.1.left.center", approvalGroupPanel );
		this.add( "5.1.left.center", commentsPanel );
		//this.setBorder( new TitledBorder( "" ) );
	}

	/** \brief Creates the ECRInfoPanel.
	      	   It contains general information about ECR such as productModel, relatedDocuments, projectSerialNumber, customer, rigName, priority
      	   	   and displays them at proper positions. 
	    \return returns the ECRInfoPanel.	   		
	*/	
	public JPanel createECRInfoPanel()
	{
		JPanel ecrInfoPanel = new JPanel( new PropertyLayout() );

		JPanel ecrInfoLeftPanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );
		JPanel ecrInfoRightPanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );
		
		JTextField productModelText = ( JTextField )( ecrFormProps.getProperty( "nov4_product_model" ) );  
		productModelText.setEnabled(false);
		productModelText.setPreferredSize( new Dimension( 270, 21 ) );
		
		JTextField relatedDocumentsText = ( JTextField )( ecrFormProps.getProperty( "nov4_related_doc" ) );		
		relatedDocumentsText.setEnabled(false);
		relatedDocumentsText.setPreferredSize( new Dimension( 270, 21 ) );
		
		JTextField projectSerialNumberText = ( JTextField )ecrFormProps.getProperty( "nov4_project_number" );		
		projectSerialNumberText.setEnabled(false);
		projectSerialNumberText.setPreferredSize( new Dimension( 270, 21 ) );

		JTextField customerText = ( JTextField )ecrFormProps.getProperty( "nov4_customer" );		
		customerText.setEnabled(false);
		customerText.setPreferredSize( new Dimension( 270, 21 ) );
		
		JTextField rigNameText = ( JTextField )ecrFormProps.getProperty( "nov4_rig_name" );		
		rigNameText.setEnabled(false);
		rigNameText.setPreferredSize( new Dimension( 270, 21 ) );
		
		// Removed priority field display - Aug 02 Sachin
		
		/*JTextField priorityText = ( JTextField )ecrFormProps.getProperty( "nov4_priority" );		
		priorityText.setEnabled(false);
		priorityText.setPreferredSize( new Dimension( 270, 21 ) );*/
		
		ecrInfoLeftPanel.add( "1.1.left.center", new NOIJLabel( reg.getString( "productModel.LABEL") ) );
		ecrInfoLeftPanel.add( "1.2.left.center", productModelText );
		ecrInfoLeftPanel.add( "2.1.left.center",new NOIJLabel(reg.getString("projectSerialNumber.LABEL")));
		ecrInfoLeftPanel.add( "2.2.left.center", projectSerialNumberText );
		ecrInfoLeftPanel.add( "3.1.left.center", new NOIJLabel( reg.getString( "rigName.LABEL" ) ) );
		ecrInfoLeftPanel.add( "3.2.left.center", rigNameText );
		
		ecrInfoRightPanel.add( "1.1.left.center", new NOIJLabel( reg.getString("relatedDocuments.LABEL" ) ) );
		ecrInfoRightPanel.add( "1.2.left.center", relatedDocumentsText );		
		ecrInfoRightPanel.add( "2.1.left.center", new NOIJLabel(reg.getString( "customer.LABEL" ) ) );
		ecrInfoRightPanel.add( "2.2.left.center", customerText );
		
		// Removed priority field display - Aug 02 Sachin
		
		/*
		ecrInfoRightPanel.add( "3.1.left.center", new NOIJLabel( reg.getString( "priority.LABEL" ) ) );
		ecrInfoRightPanel.add( "3.2.left.center", priorityText );
		*/
		
		ecrInfoPanel.add( "1.1.left.center", ecrInfoLeftPanel );
		ecrInfoPanel.add( "1.2.left.left", ecrInfoRightPanel );
		
		return ecrInfoPanel;
	}
	
	/** \brief Creates the ECRInfoPanel.
	   		   It contains Approval Group information about ECR such as approvalGroup, engApprover, engApproveDate, additionalApprover, additionalApproveDate
  	   		   and displays them at proper positions. 
		\return returns the Approval Group Panel for the ECR.	   		
	*/		
	public JPanel createApprovalGroupPanel()
	{
		JPanel approvalGroupPanel = new JPanel( new PropertyLayout() );

		TitledBorder approvalGroupTitledBorder = new TitledBorder( reg.getString( "approvalGroup.LABEL" ) );
		approvalGroupTitledBorder.setTitleFont( new Font( "Dialog", java.awt.Font.BOLD, 12 ) );
		approvalGroupPanel.setBorder( approvalGroupTitledBorder );
		
		JTextField approvalGroupText = ( JTextField )ecrFormProps.getProperty( "nov4_approval_group" );		
		approvalGroupText.setEnabled( false );
		approvalGroupText.setPreferredSize( new Dimension( 242, 21 ) );
	
		JPanel approvalGroupLeftPanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );
		JPanel approvalGroupRightPanel = new JPanel( new PropertyLayout( 5, 5, 5, 5, 5, 5 ) );
		
		JTextField engApproverText = ( JTextField )ecrFormProps.getProperty( "nov4_eng_approver" );  
		engApproverText.setEnabled(false);
		engApproverText.setPreferredSize( new Dimension( 242, 21 ) );
		
		NOVDateButton engApproveDateBtn = ( NOVDateButton )ecrFormProps.getProperty( "nov4_eng_approve_date" );		
		engApproveDateBtn.setEnabled(false);
		engApproveDateBtn.setPreferredSize( new Dimension( 242, 21 ) );
		
		JTextField additionalApproverText = ( JTextField )ecrFormProps.getProperty( "nov4_additional_approver" );		
		additionalApproverText.setEnabled( false );
		additionalApproverText.setPreferredSize( new Dimension( 242, 21 ) );
		
		NOVDateButton additionalApproveDateBtn = ( NOVDateButton )ecrFormProps.getProperty( "nov4_addtional_approve_date" );		
		additionalApproveDateBtn.setEnabled( false );
		additionalApproveDateBtn.setPreferredSize( new Dimension( 242, 21 ) );

		approvalGroupLeftPanel.add( "1.1.left.center", new NOIJLabel( reg.getString( "engApprover.LABEL") ) );
		approvalGroupLeftPanel.add( "1.2.left.center", engApproverText );
		approvalGroupLeftPanel.add( "2.1.left.center", new NOIJLabel( reg.getString( "additionalApprover.LABEL" ) ) );
		approvalGroupLeftPanel.add( "2.2.left.center", additionalApproverText );

		approvalGroupRightPanel.add( "1.1.left.center", new NOIJLabel( reg.getString( "engApproveDate.LABEL") ) );
		approvalGroupRightPanel.add( "1.2.left.center", engApproveDateBtn );
		approvalGroupRightPanel.add( "2.1.left.center", new NOIJLabel(reg.getString( "additionalApproveDate.LABEL" ) ) );
		approvalGroupRightPanel.add( "2.2.left.center", additionalApproveDateBtn );		
		
		approvalGroupPanel.add( "1.1.left.center", approvalGroupText );
		approvalGroupPanel.add( "2.1.left.center", approvalGroupLeftPanel );
		approvalGroupPanel.add( "2.2.left.center", approvalGroupRightPanel );		
		
		return approvalGroupPanel;
	}
}
