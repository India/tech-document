package com.noi.rac.en.form.compound.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.noi.rac.en.form.compound.component.NOVEnGeneralInfoPanel;
import com.noi.rac.en.form.compound.data._EngNoticeForm_DC;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOVEnDispositionPanel;
import com.noi.rac.en.util.components.NOVEnDispositionTabPanel;
import com.noi.rac.en.util.components.NOVEnTargetsPanel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class _EngNoticeForm_Panel extends PrintablePanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TCComponentForm Enform;
	public Vector<TCComponent> Entargets=new Vector<TCComponent>();
	private TCComponentProcess currentProcess=null;
	public NOVEnTargetsPanel targetsPanel;
	public NOVEnGeneralInfoPanel enGenInfoPanel;
	public JTabbedPane enautomatonTabbePane = new JTabbedPane();
	private Registry  reg;
	public String dispErrStr = "";
	public String reasonErrStr = "";

	public _EngNoticeForm_Panel()
	{
		super();
		reg=Registry.getRegistry(this);
	}


	public void initialize()
	{

	}
	public TCSession getSession()
	{
		return Enform.getSession();
	}

	public void saveDipsDatatoForm()
	{
		((_EngNoticeForm_DC)novFormProperties).saveOnlyDispFormData();
	}

	public void saveForm()
	{
		((_EngNoticeForm_DC)novFormProperties).saveFormData();
	}

	public void createUI(INOVCustomFormProperties formProps)
	{
		novFormProperties = formProps;
		Enform=(TCComponentForm)novFormProperties.getForm();

		enGenInfoPanel = new NOVEnGeneralInfoPanel(formProps);

		loadEnTargets();
		targetsPanel=new NOVEnTargetsPanel(this, novFormProperties);	
		((NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition")).createUI();

		ExternalAttachmentsPanel extPanel = new ExternalAttachmentsPanel(formProps, false, this);
		
		PrintExternalAttachmentsPanel printExtPanel = new PrintExternalAttachmentsPanel(formProps, false, this);
		
		JPanel mainPanel=new JPanel(new GridBagLayout());		
		JPanel topPanel=new JPanel(new BorderLayout());	

		topPanel.add(targetsPanel, BorderLayout.NORTH);

		JPanel bottomPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));		
		bottomPanel.add("1.1.left.top",enGenInfoPanel);
		bottomPanel.add("2.1.left.top",printExtPanel);
		bottomPanel.add("3.1.left.top",extPanel);
		
		
		//Main panel
		GridLayout gridLayout = new GridLayout(10, 1){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Dimension preferredLayoutSize(Container parent) {				
				synchronized (parent.getTreeLock()) {
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if (nrows > 0) {
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else {
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++) {
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getPreferredSize();
						if (w[c] < d.width) {
							w[c] = d.width;
						}
						if (h[r] < d.height) {
							h[r] = d.height;
						}
					}
					int nw = 0;
					for (int j = 0; j < ncols; j ++) {
						nw += w[j];
					}
					int nh = 0;
					for (int i = 0; i < nrows; i ++) {
						nh += h[i];
					}
					return new Dimension(insets.left + insets.right + nw + (ncols-1)*getHgap(), 
							insets.top + insets.bottom + nh + (nrows-1)*getVgap());
				}
			}

			public Dimension minimumLayoutSize(Container parent) {				
				synchronized (parent.getTreeLock()) {
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if (nrows > 0) {
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else {
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++) {
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getMinimumSize();
						if (w[c] < d.width) {
							w[c] = d.width;
						}
						if (h[r] < d.height) {
							h[r] = d.height;
						}
					}
					int nw = 0;
					for (int j = 0; j < ncols; j ++) {
						nw += w[j];
					}
					int nh = 0;
					for (int i = 0; i < nrows; i ++) {
						nh += h[i];
					}
					return new Dimension(insets.left + insets.right + nw + (ncols-1)*getHgap(), 
							insets.top + insets.bottom + nh + (nrows-1)*getVgap());
				}
			}

			public void layoutContainer(Container parent) {			   
				synchronized (parent.getTreeLock()) {
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if (ncomponents == 0) {
						return;
					}
					if (nrows > 0) {
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else {
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int hgap = getHgap();
					int vgap = getVgap();
					// scaling factors      
					Dimension pd = preferredLayoutSize(parent);
					double sw = (1.0 * parent.getWidth()) / pd.width;
					double sh = (1.0 * parent.getHeight()) / pd.height;
					// scale
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++) {
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getPreferredSize();
						d.width = (int) (sw * d.width);
						d.height = (int) (sh * d.height);
						if (w[c] < d.width) {
							w[c] = d.width;
						}
						if (h[r] < d.height) {
							h[r] = d.height;
						}
					}
					for (int c = 0, x = insets.left; c < ncols; c ++) {
						for (int r = 0, y = insets.top; r < nrows; r ++) {
							int i = r * ncols + c;
							if (i < ncomponents) {
								parent.getComponent(i).setBounds(x, y, w[c], h[r]);
							}
							y += h[r] + vgap;
						}
						x += w[c] + hgap;
					}
				}
			}  
		};	
		mainPanel.setLayout(gridLayout);
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(topPanel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(bottomPanel, gridBagConstraints);

		JScrollPane scrollpane=new JScrollPane(mainPanel);
		bottomPanel.addMouseWheelListener(new ScrollForm(this));
		add(scrollpane);	
	}

	public void actionPerformed(ActionEvent e) {}

	public boolean isMandatoryFieldsFilled()
	{
		int tabCount = enautomatonTabbePane.getTabCount();
		dispErrStr =  new String();
		reasonErrStr = new String();
		for (int i = 0; i < tabCount; i++) 
		{
			Component comp = enautomatonTabbePane.getComponentAt(i);
			if (comp instanceof NOVEnDispositionTabPanel) 
			{
				if (!((NOVEnDispositionTabPanel)comp).isMandatoryFieldsFilled()) 
				{
					dispErrStr = dispErrStr + enautomatonTabbePane.getTitleAt(i)+",";	
				}
			}
		}

		try {
			if (!enGenInfoPanel.isMandatoryFieldsFilled()) 
			{
				reasonErrStr = reg.getString("ReasonForNoticeMandatory.MSG");	
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if ((reasonErrStr.trim().length()==0) && (dispErrStr.trim().length()==0) ) 
		{
			return true;
		}

		return false;
	}

	public Vector<TCComponent> getEnTargets()
	{
		return Entargets;
	}
	public TCComponentProcess getProcess()
	{
		return currentProcess;
	}
	public void loadEnTargets()
	{
		try
		{
			//EN Targets;//Aparna : commented to fetch the targets from dispostion
			AIFComponentContext[] contexts=Enform.whereReferenced();			
			if(contexts!=null)
			{
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
					{
						currentProcess=(TCComponentProcess)contexts[j].getComponent();
						TCComponentTask rootTask=((TCComponentProcess)currentProcess).getRootTask();
						TCComponent[] targets=rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
						for(int i=0;i<targets.length;i++)
						{
							if(targets[i] instanceof TCComponentItemRevision)
								Entargets.add(targets[i]);
						}
						((NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition")).setDispData(this);
						break;						
					}
				}
				
			}	

			if((currentProcess==null)&&(((NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition")).referncedispdata.length>0))
			{
				Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(((NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition")).referncedispdata));
				TCComponentItemRevisionType type=(TCComponentItemRevisionType)Enform.getSession().getTypeService().getTypeComponent("ItemRevision");
				for(int k=0;k<tempReference.size();k++)
				{					
					String targetId=(tempReference.get(k).getTCProperty("targetitemid")).getStringValue();			
					String revid=(tempReference.get(k).getTCProperty("rev_id")).getStringValue();
					TCComponent comp=type.findRevision(targetId, revid);
					if(comp!=null)
						Entargets.add(comp);
				}	
				((NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition")).setDispData(this);
			}		


		}catch(Exception e)
		{
			System.out.println("Exception is:" + e);

		}		

	}
	public void removeDispositionData(TCComponent tobremoved)
	{
		NOVEnDispositionPanel dispnale=(NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition");
		dispnale.removeDispTab(tobremoved);
	}

	public void addDispData(TCComponent[] tobAdded)
	{
		NOVEnDispositionPanel dispnale=(NOVEnDispositionPanel)novFormProperties.getProperty("entargetsdisposition");
		dispnale.adddispTabs(tobAdded);
	}


//	private void setMfgReviewCondition(boolean flag)
//	{
//		try
//		{}catch(Exception e)
//		{
//			System.out.println("Exception is:" + e);
//
//		}		
//
//	}
	class ScrollForm implements MouseWheelListener 
	{
		JPanel mailpanelLc = new JPanel();

		ScrollForm(JPanel mainpaneld)
		{
			mailpanelLc =mainpaneld;
		}

		public void mouseWheelMoved(MouseWheelEvent e) 
		{    

			Container cont = getParentCust(mailpanelLc);
			if (cont instanceof JScrollPane) 
			{                            
				JScrollPane sc = (JScrollPane)cont;
				if (e.getWheelRotation() < 0) 
				{
					sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() -15);                                
				}
				else 
				{
					sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() + 15);
				}
			}
		}        
		public Container getParentCust(Container con)
		{    
			if (con instanceof JScrollPane)
			{        
				return con;    
			}
			else
			{
				return getParentCust(con.getParent());
			}                        
		}
	}


}
