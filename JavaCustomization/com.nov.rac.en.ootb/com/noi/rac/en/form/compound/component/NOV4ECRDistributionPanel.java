package com.noi.rac.en.form.compound.component;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import com.noi.rac.en.util.components.AbstractReferenceLOVList;
import com.nov.rac.utilities.utils.QueryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;

public class NOV4ECRDistributionPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	Dimension listSize;
	TCSession tcSession=null;
	JList ecrDistributionList = new JList();
	TCComponent form=null;
	public JScrollPane ecrDistributionListScroll = new JScrollPane( ecrDistributionList );		
	
	public AbstractReferenceLOVList ecrDistributionData = null;		
	DefaultListModel ecrDistributionModel;		
	
	public NOV4ECRDistributionPanel(TCComponent form)
	{
		super();
		this.form = form;
		this.tcSession=form.getSession();
	}
	public NOV4ECRDistributionPanel()
	{
		super();
		
	}
	
	public void initUI()
	{
		ecrDistributionList  = new JList();
		ecrDistributionModel = new DefaultListModel();
		ecrDistributionList.setModel( ecrDistributionModel );
		//MOhanar
		//ecrDistributionList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		ecrDistributionListScroll = new JScrollPane( ecrDistributionList );
		ecrDistributionListScroll.setPreferredSize( new Dimension( 172,120 ) );        

		JPanel ecrDistributionPanel = new JPanel( new PropertyLayout() );
		//NOIJLabel ecrDistributionLbl = new NOIJLabel( reg.getString( "ECRDIST.LABEL" ) );
		//ecrDistributionPanel.add( "1.1.center.center", ecrDistributionLbl );
		ecrDistributionPanel.add( "1.1.center.center", ecrDistributionListScroll );
		
		this.setLayout(new PropertyLayout());
		this.add( "1.1.center.center", ecrDistributionPanel );				
		populateECRDistributionList();
		ecrDistributionList.setEnabled( false );
	}

	public void setTitleBorderText(String title)
	{
		TitledBorder tb = new TitledBorder(title);
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		setBorder(tb);
	}
	
	public void init( Dimension size )
	{
		AbstractReferenceLOVList emailList =new AbstractReferenceLOVList();
		TCProperty propObjs =null;
		try {
			String[] strEcrNumber= form.getTCProperty("relatedecr").getStringValueArray();
			
			
			for(int index=0;index<strEcrNumber.length;index++){
				 String[] ipEntries = { "Name", "Type" };
	    		 String[] ipValues1 = { strEcrNumber[index], "RS ECR Form" };
	    		 String query = "General...";
	    		 int resultsType = 0;

	    		TCComponent[] foundECRForms = QueryUtils.executeSOAQuery(ipEntries,
	    					                      ipValues1, query, resultsType);
				propObjs = foundECRForms[0].getTCProperty( "nov4_distribution" );
				if(propObjs.getStringValueArray()!=null){
					String[] emailids=propObjs.getStringValueArray();
					for(int j=0; j<emailids.length && emailids[j]!=null ;j++)
						emailList.addItem(emailids[j],emailids[j]);
					
				}	
			}
			
			ecrDistributionData = emailList;
			listSize = size;
			initUI(); 
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		       
	}
	public void init( AbstractReferenceLOVList source, Dimension size )
	{
		ecrDistributionData = source;
		listSize = size;
		initUI();        
	}

	private void populateECRDistributionList()
	{
		if( ecrDistributionData != null )
		{
			String[] listValues = ecrDistributionData.getList();
			ecrDistributionList.setListData( listValues );
			ecrDistributionList.validate();
			//ecrDistributionList.setPreferredSize( new Dimension( listSize.width, 17*listValues.length ) );
		}
	}
	
	//Resize based on width and height
	public void setNewDimention( int width,int height )
	{
		this.setPreferredSize( new Dimension( width, height ) );
		ecrDistributionListScroll.setPreferredSize(
				new Dimension( ( int )( width / 2.3 ), ( int )( height / 1.6 ) ) );
	}
	
	public String[] getECRDistributionsObjects()
	{
		String[] strEcrDistArr = ecrDistributionData.getList();
		
		return strEcrDistArr;
	}
}