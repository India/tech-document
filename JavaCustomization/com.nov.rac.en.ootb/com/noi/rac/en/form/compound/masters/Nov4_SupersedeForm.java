package com.noi.rac.en.form.compound.masters;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumnModel;

import com.noi.rac.en.form.compound.data.Nov4_SupersedeForm_DC;
import com.noi.rac.en.form.compound.panels.Nov4_SupersedeForm_Panel;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
//import com.teamcenter.tc.kernel.icctstubs.booleanSeq_tHelper;//TC10.1 Upgrade

public class Nov4_SupersedeForm extends BaseMasterForm
{
	Nov4_SupersedeForm_DC sSedeDC;
	Nov4_SupersedeForm_Panel sSedePanel;
	String masterFormName="";
	static boolean once=false;
	//static boolean flag = true;
	AIFTableModel tableModel;//Added By Sandip
	TCComponent[] dispcomps;
	TCSession session =  (TCSession) AIFUtility.getDefaultSession();
	public Nov4_SupersedeForm(TCComponentForm master)
	{
		super();
		masterForm = master;
		
		try 
		{
			masterFormName = masterForm.getProperty("object_name");
			
		}
		catch (Exception e) 
		{
			System.out.println(e);;
		}
		
		sSedeDC = new Nov4_SupersedeForm_DC();
		sSedePanel = new Nov4_SupersedeForm_Panel(masterForm);
		sSedeDC.setForm(masterForm);
		sSedeDC.populateFormComponents();
		sSedePanel.createUI(sSedeDC);
		tableModel=sSedePanel.getTableModel();
		
	}
	public JComponent getPanel() 
	{
		return sSedePanel;
	}

	public ImageIcon getTabIcon() 
	{
		return null;
	}

	public String getTip() 
	{
		return null;
	}

	public void lockForm(boolean lock) 
	{
		
	}

	public void saveForm() 
	{ 
		sSedeDC.saveFormData();
	}

	
	public void setDisplayProperties()
	{
		
	}
	
	public AIFTableModel getTableModel(){
		 return tableModel;
	    }
	
	public boolean isRSOne()
	 {   
		boolean isRsOne = true;
		int		x = 0;
	    
	    try
	     {
		  if(dispcomps!=null)
		  {
	    	for(int i=0; i<dispcomps.length;i++){
	    		
	    		TCComponentItemType type = (TCComponentItemType)session.getTypeService().getTypeComponent("Item");
		
      	    TCComponentItem compitem=type.find(dispcomps[i].getProperty("targetitemid"));

      	    TCComponent[] altids = compitem.getReferenceListProperty("altid_list");
	 
      	    for(int j=0;j<altids.length;j++)
      	    {
      	    	TCComponent comp = altids[j].getTCProperty("idcontext").getReferenceValue();
      	    	
	    	        if (comp.getProperty("idcxt_name").equalsIgnoreCase("RSOne"))
	    	        {
	    	           x++;
	    	        }
	    
      	    }
	    	}
	    	if(dispcomps.length != x)
	    	{
	    		isRsOne = false;
	    	}
	    }
		  
	    }catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return isRsOne;
	 }
	
	public TCComponent updateFutureStatus(TCComponent targetdispComps, String futureValue ) 

    {
		   if (targetdispComps!=null) 

          {    
                      try 
                      {
                    	  	targetdispComps.refresh();
                            TCProperty futureStatProp = targetdispComps.getTCProperty("futurestatus");                            
                            futureStatProp.setStringValue(futureValue);
                            String futureStatus=targetdispComps.getTCProperty("futurestatus").getStringValue();
                     } 
                      catch (TCException e) 
                      {
                            e.printStackTrace();
                      }
          }
		   return targetdispComps;
    }
	public boolean isformSavable()
	{
		String consErrStr = "";	
		boolean flag = true;
		
		dispcomps = sSedeDC.dispDataComp.getDispData();

/*          if(isRSOne() && ((!(tableModel.getValueAt(0, 4).equals("Scrap") || tableModel.getValueAt(0, 4).equals("Rework")) && !(tableModel.getValueAt(1, 4).equals("Scrap") || tableModel.getValueAt(1, 4).equals("Rework")))))
	      {
        	consErrStr =  "Please choose any one from the dropdown for the part selected to be Superseded : Scrap or Rework";
		    flag = false;            
	      }
        else if(!isRSOne() && (flag) && ((tableModel.getValueAt(0, 4).equals("Scrap") || tableModel.getValueAt(0, 4).equals("Rework")) || (tableModel.getValueAt(1, 4).equals("Scrap") || tableModel.getValueAt(1, 4).equals("Rework"))))
	      {
        	consErrStr =  "Scrap or Rework for a part can only be selected only when both the Parts have RSOne ID";
		    flag = false;
	      }*/
        
        if(tableModel.getValueAt(0, 3).equals("Superseded")&& tableModel.getValueAt(1, 3).equals("Superseded") && (flag))
		   {
        	consErrStr =  "Please choose only one as Supersede Part";
			 flag = false;
		   }
        if(tableModel.getValueAt(0, 3).equals(" ") && tableModel.getValueAt(1, 3).equals(" ")&& (flag))
		   {
        	consErrStr =  "Please choose one part as Supersede part";
			 flag = false;			 
		   }
        
		if((dispcomps!=null) && (flag))
        {   
		    for(int i=0; i<dispcomps.length;i++)
		      {
				 try {
					 if(((TCComponent)dispcomps[i]).getProperty("targetitemid").equals(tableModel.getValueAt(i, 1)))
					    {    
						   dispcomps[i]= updateFutureStatus((TCComponent)dispcomps[i],tableModel.getValueAt(i, 3).toString());
					    }
					   
/*					   if(tableModel.getValueAt(i, 4).equals("Scrap") && tableModel.getValueAt(i, 3).equals("Superseded") && ((TCComponent)dispcomps[i]).getProperty("targetitemid").equals(tableModel.getValueAt(i, 1)))
					   {   
						   dispcomps[i]= updateFutureStatus((TCComponent)dispcomps[i],"Scrap");
					   }
					   
					   if(tableModel.getValueAt(i, 4).equals("Rework") && tableModel.getValueAt(i, 3).equals("Superseded") && ((TCComponent)dispcomps[i]).getProperty("targetitemid").equals(tableModel.getValueAt(i, 1)))
					   {    
						   dispcomps[i]= updateFutureStatus((TCComponent)dispcomps[i],"Rework");
						   
					   }*/			   
						   
				     } catch (TCException e1) {
					     // TODO Auto-generated catch block
					     e1.printStackTrace();
				     }
		      }
		}

		if(flag)
		{
			String reasonForSupersede = sSedeDC.furExplanation.getText();
			if(reasonForSupersede == null || reasonForSupersede.trim().isEmpty())
			{
				consErrStr =  "\"Reason for Supersede\" is a mandatory field, please enter the reason";
				sSedeDC.furExplanation.setText("");
				flag = false;
			}
		}
		
		if(flag)
		{
			return true;
		}
		else
		{
			//MessageBox.post(consErrStr,"Can not Proceed!!!",MessageBox.WARNING);
			JOptionPane.showMessageDialog(sSedePanel.getRootPane(), consErrStr, "Cannot Proceed!!!", JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}
}
