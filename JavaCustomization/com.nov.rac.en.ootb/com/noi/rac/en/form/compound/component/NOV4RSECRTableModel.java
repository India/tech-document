package com.noi.rac.en.form.compound.component;

import java.util.Vector;

import com.teamcenter.rac.common.TCTableModel;

public class NOV4RSECRTableModel extends TCTableModel
{
	private static final long serialVersionUID = 1L;

	String[] tableColumnNames;
	Object[][] rowData;
	Boolean isAnyCellEditable;
	int editableCellIndex = 0;
	
	public NOV4RSECRTableModel( String[] tableColumnNames, Boolean isCellEditable,
			int editableCellIndex )
	{
		this.tableColumnNames = tableColumnNames;
		isAnyCellEditable = isCellEditable;
		this.editableCellIndex = editableCellIndex;
	}
	
	public void setRowData( Object[][] rowData )
	{
		this.rowData = rowData;
	}
	
	public Object[][] getSelectedRowData()
	{
		return getRowData( true );		
	}
	
	public Object[][] getUnSelectedRowData()
	{
		return getRowData( false ); 
	}
	
	private Object[][] getRowData( Boolean isSelected )
	{
		int rowDataSize = rowData.length;
		int columnSize = tableColumnNames.length;
		
		Vector<Object[]> data = new Vector<Object[]>();
		for( int index = 0; index < rowDataSize; ++index )
		{
			Object[] tempData = new Object[columnSize]; 
			if( isSelected && ( ( Boolean )getValueAt( index, 0 ) ==  true ) )
			{
				for( int columnIndex = 0; columnIndex < columnSize; ++columnIndex )
				{
					tempData[columnIndex] = rowData[index][columnIndex]; 
				}
				data.add( tempData );
			}
			else if( !isSelected && ( ( Boolean )getValueAt( index, 0 ) ==  false ) )
			{
				for( int columnIndex = 0; columnIndex < columnSize; ++columnIndex )
				{
					tempData[columnIndex] = rowData[index][columnIndex]; 
				}
				data.add( tempData );
			}
		}		

		Object[][] selRowData = new Object[data.size()][columnSize];		
		for( int selRowDataIndex = 0; selRowDataIndex < data.size(); ++selRowDataIndex )
		{
			selRowData[selRowDataIndex] = data.elementAt( selRowDataIndex ); 
		}
		
		return selRowData;
	}
	
	public Object[] getRowData( int rowIndex )
	{
		int columnCount = getColumnCount();
		Object[] rowData = new Object[columnCount];
		
		for( int index =0; index < columnCount; ++index )
		{
			rowData[index] = getValueAt( rowIndex, index );
		}
		
		return rowData;
	}			
	
	public int getColumnCount()
	{
		return tableColumnNames.length;
	}
	
	public int getRowCount()
	{
		return rowData.length;
	}
		 
	@Override
	public String getColumnName( int column )
	{
		return tableColumnNames[column];
	}			
	
	public Object getValueAt( int rowIndex, int columnIndex )
	{
		return rowData[rowIndex][columnIndex];
	}			
	
	public Class<?> getColumnClass( int columnIndex )
	{
		return rowData[0][columnIndex].getClass();
	}

    public boolean isCellEditable( int rowIndex, int columnIndex )
    {
    	if( isAnyCellEditable && ( columnIndex == editableCellIndex ) )
    		return true;
    	
    	return false;
    }	
    
    public void setValueAt( Object value, int row, int col )
    {
    	rowData[row][col] = value;
        fireTableCellUpdated( row, col );
    }  
}
