package com.noi.rac.en.form.compound.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.noi.rac.en.form.compound.component.NOVEnGeneralInfoPanel_v2;
import com.noi.rac.en.form.compound.data._EngNoticeForm_DC_v2;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.form.compound.util.ScrollForm;
import com.noi.rac.en.util.components.NOVEnDispositionPanel_v2;
import com.noi.rac.en.util.components.NOVEnDispositionTabPanel_v2;
import com.noi.rac.en.util.components.NOVEnTargetsPanel_v2;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class _EngNoticeForm_Panel_v2 extends PrintablePanel
{
	private static final long serialVersionUID = 1L;
	
	public TCComponentForm Enform;
	public Vector<TCComponent> Entargets=new Vector<TCComponent>();
	private TCComponentProcess currentProcess=null;
	public NOVEnTargetsPanel_v2 targetsPanel;
	public NOVEnGeneralInfoPanel_v2 enGenInfoPanel;
	public JTabbedPane enautomatonTabbePane = new JTabbedPane();
	private Registry  reg;
	public String dispErrStr = "";
	public String reasonErrStr = "";
	public String otherRaasonExplField = "";
	public String changeDescField = "";	
	public boolean isReadOnly = false;

	public _EngNoticeForm_Panel_v2()
	{
		super();
		reg=Registry.getRegistry(this);
	}


	public void initialize()
	{

	}
	public TCSession getSession()
	{
		return Enform.getSession();
	}

	public void saveDipsDatatoForm()
	{
		((_EngNoticeForm_DC_v2)novFormProperties).saveOnlyDispFormData();
	}

	public void saveForm()
	{
		((_EngNoticeForm_DC_v2)novFormProperties).saveFormData();
	}

	public void createUI(INOVCustomFormProperties formProps)
	{
		novFormProperties = formProps;
		Enform=(TCComponentForm)novFormProperties.getForm();

		checkENFormStatus();
		
		loadEnTargets();
		targetsPanel=new NOVEnTargetsPanel_v2(this, novFormProperties, isReadOnly );

		// Performance Issue Nitin : default Status is set at the form creation.
		((NOVEnDispositionPanel_v2)novFormProperties.getProperty("entargetsdisposition")).setTargetStatus();
		enGenInfoPanel = new NOVEnGeneralInfoPanel_v2( formProps, isReadOnly );
		
		ExternalAttachmentsPanel_v2 extPanel = new ExternalAttachmentsPanel_v2(formProps, false, this );
		//extPanel.setPreferredSize( new Dimension( 740, 180 ) );
		// changes for Reference Attachment section (height and width) 
		//extPanel.tablePane.setPreferredSize(new Dimension(715,100));
		extPanel.setControlState( isReadOnly );
		
		PrintExternalAttachmentsPanel_v2 printExtPanel = new PrintExternalAttachmentsPanel_v2(
			formProps, false, this, reg.getString( "PrintExternalAttachmentsPanel.Title2" ) );
		printExtPanel.setControlState( isReadOnly );
		
		JPanel mainPanel=new JPanel(new GridBagLayout());		
		JPanel topPanel=new JPanel(new BorderLayout());	

		topPanel.add(targetsPanel, BorderLayout.NORTH);

		JPanel bottomPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));		
		bottomPanel.add("1.1.left.top",enGenInfoPanel);
		bottomPanel.add("2.1.left.top",printExtPanel);
		bottomPanel.add("3.1.left.top",extPanel);
		
		//Main panel
		GridLayout gridLayout = new GridLayout(10, 1){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Dimension preferredLayoutSize(Container parent) {				
				synchronized (parent.getTreeLock()) {
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if (nrows > 0) {
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else {
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++) {
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getPreferredSize();
						if (w[c] < d.width) {
							w[c] = d.width;
						}
						if (h[r] < d.height) {
							h[r] = d.height;
						}
					}
					int nw = 0;
					for (int j = 0; j < ncols; j ++) {
						nw += w[j];
					}
					int nh = 0;
					for (int i = 0; i < nrows; i ++) {
						nh += h[i];
					}
					return new Dimension(insets.left + insets.right + nw + (ncols-1)*getHgap(), 
							insets.top + insets.bottom + nh + (nrows-1)*getVgap());
				}
			}

			public Dimension minimumLayoutSize(Container parent) {				
				synchronized (parent.getTreeLock()) {
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if (nrows > 0) {
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else {
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++) {
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getMinimumSize();
						if (w[c] < d.width) {
							w[c] = d.width;
						}
						if (h[r] < d.height) {
							h[r] = d.height;
						}
					}
					int nw = 0;
					for (int j = 0; j < ncols; j ++) {
						nw += w[j];
					}
					int nh = 0;
					for (int i = 0; i < nrows; i ++) {
						nh += h[i];
					}
					return new Dimension(insets.left + insets.right + nw + (ncols-1)*getHgap(), 
							insets.top + insets.bottom + nh + (nrows-1)*getVgap());
				}
			}

			public void layoutContainer(Container parent) {			   
				synchronized (parent.getTreeLock()) {
					Insets insets = parent.getInsets();
					int ncomponents = parent.getComponentCount();
					int nrows = getRows();
					int ncols = getColumns();
					if (ncomponents == 0) {
						return;
					}
					if (nrows > 0) {
						ncols = (ncomponents + nrows - 1) / nrows;
					} 
					else {
						nrows = (ncomponents + ncols - 1) / ncols;
					}
					int hgap = getHgap();
					int vgap = getVgap();
					// scaling factors      
					Dimension pd = preferredLayoutSize(parent);
					double sw = (1.0 * parent.getWidth()) / pd.width;
					double sh = (1.0 * parent.getHeight()) / pd.height;
					// scale
					int[] w = new int[ncols];
					int[] h = new int[nrows];
					for (int i = 0; i < ncomponents; i ++) {
						int r = i / ncols;
						int c = i % ncols;
						Component comp = parent.getComponent(i);
						Dimension d = comp.getPreferredSize();
						d.width = (int) (sw * d.width);
						d.height = (int) (sh * d.height);
						if (w[c] < d.width) {
							w[c] = d.width;
						}
						if (h[r] < d.height) {
							h[r] = d.height;
						}
					}
					for (int c = 0, x = insets.left; c < ncols; c ++) {
						for (int r = 0, y = insets.top; r < nrows; r ++) {
							int i = r * ncols + c;
							if (i < ncomponents) {
								parent.getComponent(i).setBounds(x, y, w[c], h[r]);
							}
							y += h[r] + vgap;
						}
						x += w[c] + hgap;
					}
				}
			}  
		};	
		mainPanel.setLayout(gridLayout);
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(topPanel, gridBagConstraints);

		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.CENTER;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = GridBagConstraints.HORIZONTAL | GridBagConstraints.VERTICAL;
		mainPanel.add(bottomPanel, gridBagConstraints);
		
		mainPanel.setPreferredSize(new Dimension(mainPanel.getPreferredSize().width, mainPanel.getPreferredSize().height+20));
		JScrollPane scrollpane=new JScrollPane(mainPanel);
		bottomPanel.addMouseWheelListener(new ScrollForm(this));
		add(scrollpane);	
	}

	private void checkENFormStatus()
	{
		try
		{
			TCComponent[] relStatusList = ( novFormProperties.getForm().getTCProperty( "release_status_list" ).getReferenceValueArray() );
			String isCheckedOut = novFormProperties.getForm().getTCProperty( "checked_out" ).getStringValue();

			String processStageList = novFormProperties.getForm().getProperty( "process_stage_list" );
			String[] processStageListArr = processStageList.split( "," );
			String reqdProcessStage =  reg.getString( "ProcessStage.Stage" );
			
			//Mohanar : If the form is checked in  
			if( isCheckedOut.trim().length() == 0 && processStageListArr.length > 1 &&
					!processStageListArr[1].trim().startsWith( reqdProcessStage ) )
				isReadOnly = true;
			else if( isCheckedOut.trim().length() == 0 && processStageListArr.length <= 1 )
				isReadOnly = true;
			
			for( int index = 0; index < relStatusList.length; ++index )
			{
				if( relStatusList[index].getProperty( "name" ).equalsIgnoreCase( reg.getString( "ENReleaseStatus.STATUS" ) ) )
					isReadOnly = true;
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
	}
	
	public void disableEnFormComponents()
	{
		//enGenInfoPanel
	}
	
	public void createDispTabPanel( TCComponent targetItemRev, boolean isReleased )
	{
		NOVEnDispositionPanel_v2 panel = ( ( NOVEnDispositionPanel_v2 )novFormProperties.getProperty( "entargetsdisposition" ) );
		panel.createUI( this, targetItemRev, isReleased );
	}
	
	public void actionPerformed(ActionEvent e) {}

	public boolean isMandatoryFieldsFilled()
	{
		int tabCount = enautomatonTabbePane.getTabCount();
		dispErrStr =  new String();
		reasonErrStr = new String();
		for (int i = 0; i < tabCount; i++) 
		{
			Component comp = enautomatonTabbePane.getComponentAt(i);
			if (comp instanceof NOVEnDispositionTabPanel_v2) 
			{
				if (!((NOVEnDispositionTabPanel_v2)comp).isMandatoryFieldsFilled()) 
				{
					dispErrStr = dispErrStr + enautomatonTabbePane.getTitleAt(i)+",";	
				}
			}
		}

		try {
				if (!enGenInfoPanel.isMandatoryFieldsFilled()) 
				{
					reasonErrStr = reg.getString( "ReasonForNoticeMandatory.MSG" );	
				}
				else if( !enGenInfoPanel.isMandatoryOtherFieldFilled() )
				{
					reasonErrStr = reg.getString( "FurtherExpalnationMandatory.MSG" );
				}
				else if( !enGenInfoPanel.isChangeDescFieldFilled() )
				{
					reasonErrStr = reg.getString( "ChangeDescMandatory.MSG" );
				}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if ((reasonErrStr.trim().length()==0) && (dispErrStr.trim().length()==0) ) 
		{
			return true;
		}

		return false;
	}

	public Vector<TCComponent> getEnTargets()
	{
		return Entargets;
	}
	
	public TCComponentProcess getProcess()
	{
		return currentProcess;
	}
	
	public void loadEnTargets()
	{
		try
		{
			NOVEnDispositionPanel_v2 dispPanel = ((NOVEnDispositionPanel_v2)novFormProperties.getProperty(
					"entargetsdisposition" ) );

			//EN Targets;//Aparna : commented to fetch the targets from dispostion
			AIFComponentContext[] contexts=Enform.whereReferenced();
			//usha - modified the contexts, targets looping
			TCComponent[] targets = null;
			if(contexts!=null)
			{
				for(int j=0;j<contexts.length;j++)
				{
					if((TCComponent)contexts[j].getComponent() instanceof TCComponentProcess)
					{
						currentProcess=(TCComponentProcess)contexts[j].getComponent();
						TCComponentTask rootTask=((TCComponentProcess)currentProcess).getRootTask();
						targets=rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
						break;
					}
				}
			}
			if(targets != null)
			{
				for(int i=0;i<targets.length;i++)
				{
					if(targets[i] instanceof TCComponentItemRevision)
						Entargets.add(targets[i]);
				}
				//((NOVEnDispositionPanel_v2)novFormProperties.getProperty("entargetsdisposition")).setDispData(this );
				dispPanel.createMappedData( this );
			}

			if( ( currentProcess == null ) && ( dispPanel.referncedispdata.length > 0 ) )
			{
				TCComponent[] tempReference = dispPanel.referncedispdata;
				String[] reqTCProprties = { "targetitemid", "rev_id" };
				//TCProperty[][] retTCProp = TCComponentType.getTCPropertiesSet( tempReference, reqTCProprties );
				List<TCComponent> tempReferenceList = Arrays.asList(tempReference);//TC10.1 Upgrade
				TCProperty[][] retTCProp = TCComponentType.getTCPropertiesSet( tempReferenceList, reqTCProprties );//TC10.1 Upgrade
				TCComponentItemRevisionType type=(TCComponentItemRevisionType)Enform.getSession().getTypeService().getTypeComponent("ItemRevision");
				for(int index = 0; index < tempReference.length; ++index )
				{					
					String targetId = ( retTCProp[index][0].getStringValue() );			
					String revid = ( retTCProp[index][1].getStringValue() );
					TCComponent comp = type.findRevision( targetId, revid );
					if(comp!=null)
						Entargets.add(comp);
				}	
				dispPanel.createMappedData( this );
			}		
		}
		catch(Exception e)
		{
			System.out.println("Exception is:" + e);

		}		
	}
	
	//Commented by Usha 22/06/2011
	public void removeDispositionData(TCComponent tobremoved)
	{
		NOVEnDispositionPanel_v2 dispnale=(NOVEnDispositionPanel_v2)novFormProperties.getProperty("entargetsdisposition");
		dispnale.removeDispTab(tobremoved);
	}

	public void addDispData(TCComponent[] tobAdded)
	{
		NOVEnDispositionPanel_v2 dispnale=(NOVEnDispositionPanel_v2)novFormProperties.getProperty("entargetsdisposition");
		dispnale.adddispTabs(tobAdded);
	}

	public void setEcrDistributions( String[] strEcrDistArr )
	{
		((_EngNoticeForm_DC_v2)novFormProperties).setEcrDistributions( strEcrDistArr );
		enGenInfoPanel.setEcrDistributions( strEcrDistArr );
	}
	
	public String[] getEcrDistributions()
	{
		return ((_EngNoticeForm_DC_v2)novFormProperties).getEcrDistributions();
	}	

//	private void setMfgReviewCondition(boolean flag)
//	{
//		try
//		{}catch(Exception e)
//		{
//			System.out.println("Exception is:" + e);
//
//		}		
//
//	}
//	class ScrollForm implements MouseWheelListener 
//	{
//		JPanel mailpanelLc = new JPanel();
//
//		ScrollForm(JPanel mainpaneld)
//		{
//			mailpanelLc =mainpaneld;
//		}
//
//		public void mouseWheelMoved(MouseWheelEvent e) 
//		{    
//
//			Container cont = getParentCust(mailpanelLc);
//			if (cont instanceof JScrollPane) 
//			{                            
//				JScrollPane sc = (JScrollPane)cont;
//				if (e.getWheelRotation() < 0) 
//				{
//					sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() -15);                                
//				}
//				else 
//				{
//					sc.getVerticalScrollBar().setValue(sc.getVerticalScrollBar().getValue() + 15);
//				}
//			}
//		}        
//		public Container getParentCust(Container con)
//		{    
//			if (con instanceof JScrollPane)
//			{        
//				return con;    
//			}
//			else
//			{
//				return getParentCust(con.getParent());
//			}                        
//		}
//	}


}
