package com.noi.rac.en.form.compound.masters;


import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.noi.rac.en.form.compound.data._EngNoticeForm_DC;
import com.noi.rac.en.form.compound.data._EngNoticeForm_DC_v2;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
public class _EngNoticeForm_ extends BaseMasterForm
{

	public _EngNoticeForm_DC enDC;
	private Registry  reg;
	_EngNoticeForm_Panel enPanel;
	_EngNoticeForm_Panel_v2 enPanel_V2;
	public _EngNoticeForm_DC_v2 enDC_v2;
	String masterFormName="";
	String form_version=""; // added by sachin
	static boolean once=false;

	//TODO : Need to implement lockForm,setDisplayProperties
	public _EngNoticeForm_(TCComponentForm master)
	{
		super();
		reg=Registry.getRegistry(this);
		masterForm = master;

		try 
		{
			String[] reqProps = { "object_name", "nov4_en_version" };
			String[] retProps = masterForm.getProperties( reqProps );
			masterFormName = retProps[0];
			form_version = retProps[1];
		}
		catch (Exception e) 
		{
			System.out.println(e);;
		}

		//System.out.println("form version "+form_version);
		if(form_version == ""){
		enDC = new _EngNoticeForm_DC();
		enPanel = new _EngNoticeForm_Panel();

		enDC.setForm(masterForm);
		//Create EN Panels and then populating the values		
		enDC.populateFormComponents();
		enPanel.createUI(enDC);		

		}else{
			enDC_v2 = new _EngNoticeForm_DC_v2();
			enPanel_V2 = new _EngNoticeForm_Panel_v2();
			enDC_v2.setForm(masterForm);
			//Create EN Panels and then populating the values		
			enDC_v2.populateFormComponents();
			enPanel_V2.createUI(enDC_v2);	
		}
		/*else{
			new _EngNoticeForm_v2(master);
		}*/

			

	}
	public JComponent getPanel() 
	{
		if(form_version == "")
		return enPanel;
		else
			return enPanel_V2;
	}

	public ImageIcon getTabIcon() 
	{
		return null;
	}

	public String getTip() 
	{
		return null;
	}

	public void lockForm(boolean lock) 
	{

	}

	public void saveForm() 
	{
		if(form_version == "")
		enDC.saveFormData();
		else
			enDC_v2.saveFormData();
	}

	public void setDisplayProperties()
	{

	}

	public boolean isformSavable()
	{
		if(form_version == "")
		{
		if(enPanel.isMandatoryFieldsFilled())
			return true;
		else
		{
			String consErrStr = "";
			if (enPanel.dispErrStr.trim().length()>0) 
			{
				char endChr = enPanel.dispErrStr.charAt(enPanel.dispErrStr.length()-1);
				
				if (endChr == ',') 
				{
					enPanel.dispErrStr = enPanel.dispErrStr.substring(0, enPanel.dispErrStr.length()-1);
				}
			}
			if ( (enPanel.dispErrStr.trim().length()>0) && (enPanel.reasonErrStr.trim().length()==0)) 
			{
				consErrStr = reg.getString("MandatoryChangeDescError.MSG")+enPanel.dispErrStr;
			}
			else if ( (enPanel.dispErrStr.trim().length()==0) && (enPanel.reasonErrStr.trim().length()>0)) 
			{
				consErrStr = reg.getString("MandatoryReason4NoticeError.MSG");
			}
			else if ( (enPanel.dispErrStr.trim().length()>0) && (enPanel.reasonErrStr.trim().length()>0)) 
			{
				consErrStr = reg.getString("MandatoryReason4NoticeChangeDescError.MSG")+enPanel.dispErrStr;
			} 
			MessageBox.post( consErrStr, reg.getString( "Mandatory.MSG" ), MessageBox.WARNING );
			return false;
			}
		}else{
			
			if(enPanel_V2.isMandatoryFieldsFilled())
				return true;
			else
			{
				String consErrStr = "";
				if (enPanel_V2.dispErrStr.trim().length()>0) 
				{
					char endChr = enPanel_V2.dispErrStr.charAt(enPanel_V2.dispErrStr.length()-1);
					
					if (endChr == ',') 
					{
						enPanel_V2.dispErrStr = enPanel_V2.dispErrStr.substring(0, enPanel_V2.dispErrStr.length()-1);
					}
				}
				if ( (enPanel_V2.dispErrStr.trim().length()>0) && (enPanel_V2.reasonErrStr.trim().length()==0)) 
				{
					consErrStr = reg.getString("MandatoryChangeDescError.MSG") + enPanel_V2.dispErrStr;
				}
				else if ( (enPanel_V2.dispErrStr.trim().length()==0) && (enPanel_V2.reasonErrStr.trim().length()>0)) 
				{
					consErrStr = enPanel_V2.reasonErrStr;
				}
				else if ( (enPanel_V2.dispErrStr.trim().length()>0) && (enPanel_V2.reasonErrStr.trim().length()>0)) 
				{
					consErrStr = enPanel_V2.reasonErrStr + enPanel_V2.dispErrStr;
				} 
			MessageBox.post(consErrStr,reg.getString("Mandatory.MSG"),MessageBox.WARNING);
			return false;
			}
			
		}
	}
	
	@Override
	public boolean isFormSavable() {
		// TODO Override this method to implement form related validations
		return true;
	}

}