/*
 * Created on Jul 15, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.form.compound.util;
import java.awt.Component;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.swing.JComponent;

import com.teamcenter.rac.common.lov.LOVPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;

/**
 * @author hughests
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class NOVCustomFormProperties implements INOVCustomFormProperties {

	public Hashtable 			components = new Hashtable();
	public Hashtable           imanProperties = new Hashtable();
	TCComponent				thisForm;
	
	public void setForm(TCComponent form) {
		thisForm = form;
	//try {
	//	thisForm.refresh();
	//} catch (TCException e) {
	//	e.printStackTrace();
	//}
	}
	public TCComponent getForm() { return thisForm; }
	
	public void addProperty(String name,Component comp) {
		components.put(name.toLowerCase(),comp);
	}
	
	public void addProperty(String name,TCComponent[] comp) {
		components.put(name.toLowerCase(),comp);
	}
	public JComponent getProperty(String name) {
		return ((JComponent)components.get(name.toLowerCase()));
	}
	public void disableProperty(String name) {
		JComponent prop = ((JComponent)components.get(name.toLowerCase()));
		if (prop != null) {
			prop.setFocusable(false);
			prop.setEnabled(false);
			if (prop instanceof LOVPopupButton)
				prop.setEnabled(false);
		}
	}
	/*public void makeFormReadOnly()
	{
		for (Enumeration e=components.keys();e.hasMoreElements();)
		{
			String cVal = (String)e.nextElement();
			JComponent prop = ((JComponent)components.get(cVal.toLowerCase()));
			disableComponent(prop);
		}
	}
	public void disableComponent(Component prop)
	{
		if(prop instanceof JPanel)
		{
			Component[] comp = ((JPanel)prop).getComponents();   
			for(int i=0;i<comp.length;i++)
			{
				if(comp[i] instanceof JPanel)
					disableComponent(comp[i]);
					else
						comp[i].setEnabled(false);
			}
				
		}else
		prop.setEnabled(false);
	}*/
	public void enableProperty(String name) {
		JComponent prop = ((JComponent)components.get(name.toLowerCase()));
		if (prop != null) {
			prop.setFocusable(true);
			prop.setEnabled(true);
			if (prop instanceof LOVPopupButton)
				prop.setEnabled(true);
		}
	}
	public void disableProperties(String[] names) {
		for (int i=0;i<names.length;i++)
			disableProperty(names[i]);
	}
	public void disableAllProperties() {
		for (Enumeration e=components.keys();e.hasMoreElements();) {
			String cVal = (String)e.nextElement();
			disableProperty(cVal);
		}
	}	
	public void addImanProperty(JComponent comp, TCProperty prop) {
		imanProperties.put(comp,prop);
	}
	public TCProperty getImanProperty(JComponent comp) {
		return ((TCProperty)imanProperties.get(comp));
	}
	public TCProperty[] enabledProperty(TCProperty[] arr,JComponent comp) {
		
		if (comp.isEnabled() && comp.isFocusable()) {
			TCProperty[] newarr;
			if (arr != null) 
			    newarr = new TCProperty[arr.length+1];
			else
				newarr = new TCProperty[1];
			
		    for (int i=0;i<((arr == null) ? 0 : arr.length);i++) {
			    newarr[i] = arr[i];
		    }
			    
		    newarr[((arr == null) ? 0 : arr.length)] = (TCProperty)imanProperties.get(comp);
			return newarr;
		}
		return arr;
	}
	
	public void getFormProperties(String[] propertyNames) {
		
		if (thisForm == null) return;
		
		try {
			TCProperty[] formProperties;
			formProperties = thisForm.getTCProperties(propertyNames);
			for (int i=0;i<formProperties.length;i++) {
				String name = formProperties[i].getPropertyName();
				if (getProperty(name) == null) continue;

				JComponent component = getProperty(name);
				addImanProperty(component,formProperties[i]);
			}
		}
		catch (Exception e) {
			System.out.println("Could not get form properties: "+e.getMessage());
			e.printStackTrace();
		}
	}
		
	public Hashtable getComponents() { return components; }
	public Hashtable getImanProperties() { return imanProperties; }
	
    public   void       	populateFormComponents() {}
    public   void        	saveFormData() {}
    public   String[]       getPropertyNames() { return (new String[]{""});} 
    public   void           clearForm() {}
	
    public int getYear(Date date){
    	GregorianCalendar calendar = new GregorianCalendar();
    	calendar.setTime(date);
    	
    	return (calendar.get(Calendar.YEAR)-1900);
    }
	
}
