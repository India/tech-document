package com.noi.rac.en.form.compound.component;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

import com.noi.rac.en.form.compound.panels.NOV4RigsECRForm_Panel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOV4_RIGSEcrAffectedDocPartsPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private Registry reg;
	private TCSession tcSession; 
	
	NOV4RigsECRForm_Panel novECRPanel;
	NOV4_RIGSEcrTargetsTable ecrAffectedDocPartsTable;

	public NOV4_RIGSEcrAffectedDocPartsPanel( NOV4RigsECRForm_Panel ecrPanel, TCSession tcSession )
	{
		reg = Registry.getRegistry( this );
		novECRPanel = ecrPanel;
		this.tcSession = tcSession;
		createUI();
	}
	
	private void createUI()
	{
		createAffectedDocPartsTable( );
	}
	
	private void createAffectedDocPartsTable()
	{
		String tableColumnNames[] = { reg.getString( "ItemID.TYPE" ), reg.getString( "Rev.TYPE" ),
				reg.getString( "ItemName.TYPE" ), reg.getString( "EnNumber.TYPE" ) };
	
		ecrAffectedDocPartsTable = new NOV4_RIGSEcrTargetsTable( novECRPanel, tcSession, tableColumnNames );
		ecrAffectedDocPartsTable.setAutoResizeMode( TCTable.AUTO_RESIZE_OFF );	

		TableColumn col = ecrAffectedDocPartsTable.getColumnModel().getColumn(0);
		col.setPreferredWidth( 240 );
		col = ecrAffectedDocPartsTable.getColumnModel().getColumn(1);
		col.setPreferredWidth( 80 );	
		col = ecrAffectedDocPartsTable.getColumnModel().getColumn(2);
		col.setPreferredWidth( 240 );	
		col = ecrAffectedDocPartsTable.getColumnModel().getColumn(3);
		col.setPreferredWidth( 200 );	
		
		ecrAffectedDocPartsTable.setBackground( Color.WHITE );
		ecrAffectedDocPartsTable.setVisible( true );
		ecrAffectedDocPartsTable.setEnabled( false );
		
		JScrollPane targetTblPane = new JScrollPane( ecrAffectedDocPartsTable );
		targetTblPane.setPreferredSize( new Dimension( 763, 150 ) );
							
		this.setLayout( new PropertyLayout() );
		this.add( "1.1.left.top", targetTblPane );
		this.validate();
	}
}
