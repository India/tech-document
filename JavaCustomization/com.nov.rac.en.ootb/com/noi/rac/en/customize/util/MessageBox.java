package com.noi.rac.en.customize.util;

import java.awt.Frame;
import java.awt.Window;
import javax.swing.JOptionPane;

public class MessageBox																	//    Vivek Start
{
    public static final int INFORMATION = JOptionPane.INFORMATION_MESSAGE;
    public static final int WARNING = JOptionPane.WARNING_MESSAGE;
    public static final int ERROR = JOptionPane.ERROR_MESSAGE;
	
    public MessageBox( Frame parent, String msgBoxTitle, String message, int type )
    {
		createMessageBox( parent, msgBoxTitle, message, type );
    }
    
    public MessageBox( Window parent, String msgBoxTitle, String message, int type )
    {
		createMessageBox( parent, msgBoxTitle, message, type );
    }
   
    private void createMessageBox(Window parent, String msgBoxTitle,
			String message, int type) 
    {
    	JOptionPane.showMessageDialog(parent, message, msgBoxTitle, type);
    }

	public static MessageBox post( Frame window, String s1, String s2, int i )
    {
    	MessageBox messagebox = new MessageBox( window, s1,  s2, i );
        return messagebox;
    }
    
    public static MessageBox post( Window window, String s1, String s2, int i )
    {
    	MessageBox messagebox = new MessageBox( window, s1,  s2, i );
        return messagebox;
    }
    
    private void createMessageBox( Frame parent, String msgBoxTitle, String message, int type )
    {
    	JOptionPane.showMessageDialog(parent, message, msgBoxTitle, type);
    }  

}

//  Vivek End