package com.noi.rac.en.customize.util;

import javax.swing.border.Border;

/**
 * @author hughests
 * This is class  is used to define  all the  Constants
 */
public final class NOVConstants {

	public static final String LISTOFVALUES = "ListOfValues";
	public static final String LOV_NAME = "lov_name";
	public static final String ITEM_TYPE = null;
	public static final String DOCUMENT_CATEGORY = null;
	public static final String NOI_POM_GROUP = "NOI_POMGroup_LOV";
	public static final String GROUP_NAMES = "Group Names";
	public static final String SELECT_PROJECT_TITLE = "Select Projects";
	public static final String PROPERTY_PROJECTS = "projects";
	public static final String SELECT_DISTRIBUTION_TITLE = "Select Distribution";
	public static final String PROPERTY_DISTRIBUTION = "distribution";
	public static final String ERO_SCHEDULING = "ERO Scheduling";
	public static final String TYPE_OF_RELEASE = "Type Of Release";
	public static final String TYPE_OF_CHANGE = "Type Of Change";
	public static final String ECN_SCHEDULING = "ECN Scheduling";
	public static final int MAXIMUM_ECN_CHANGE_DESCRIPTION = 1900;
	public static final int MAXIMUM_ERD_ADDITIONAL_DESCRIPTION = 1900;
	public static final int MAXIMUM_ECN_REASON_OF_CHANGE = 1900;
	//public static final String LENGTH_EXCEEDED_TITLE = "Length Exceeded";
//	public static final String LENGTH_EXCEEDED_DETAIL = "Maximum characters exceeded: ";
	public static final String MRP_ENTRY = "MRP Entry";
	public static final String TITLE_ERO_REASON_OF_RELEASE = "Reason for Release";
	public static final int MAXIMUM_ERO_RESON_OF_RELEASE = 500;
	public static final int MAXIMUM_ERO_INSTRUCTIONS = 1900;
	public static final String TITLE_ERO_INSTRUCTIONS_OR_SUGGESTIONS = "Instructions/Suggestions";
	public static final String N_SLASH_A = "N/A";
	public static final String ECNQUERY = "ECNQuery";
	public static final String ERD_ITEM_QUERY = "ERD Item Query";
	public static final String TYPE_PROJECT_NAME_HERE = "Type project name here";
	public static final String PROPERTY_PRODUCTLINE = "productlinearray";
	public static final String SELECT_PRODUCTLINE_TITLE = "Select ProductLine";
	public static final String PROPERTY_MFGLOCATION = "mfglocation";
	public static final String SELECT_MFGLOCATION_TITLE = "Select MFGLocations";
	public static final String SELECT__ALTTERNATE_MFGLOCATION_TITLE = "Select Alternate Locations";
}
