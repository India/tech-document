package com.noi.rac.en.mission.helper;

import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;

public class NOVMissionHelper 
{
	public static boolean isGroup(String pref,INOVCustomFormProperties fp) 
	{
			TCSession session = (TCSession)AIFUtility.getDefaultSession();
			TCPreferenceService prefServ = session.getPreferenceService();
			String[] grpStrArray = prefServ.getStringArray(
	                TCPreferenceService.TC_preference_site,pref);
			String currentUsrGrp = null;
			if (fp == null || fp.getForm() == null)
				return false;
				try {
					AIFComponentContext[] partContext = fp.getForm().whereReferenced();
					TCComponentProcess enProcess = null;
					TCComponentItemRevision itemRevision = null;
					boolean diffGroup = false;
					for (int inx = 0; inx<partContext.length;inx++)
					{
						if ( partContext[inx] == null || !(partContext[inx].getComponent() instanceof TCComponentProcess))
							continue;
						enProcess =  (TCComponentProcess)partContext[inx].getComponent();
						TCComponent[] targets= enProcess.getRootTask().getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
						for(int i=0;i<targets.length;i++)
						{
							if(targets[i] == null || !(targets[i] instanceof TCComponentItemRevision))
								continue;
							itemRevision = (TCComponentItemRevision)targets[i];
							if (itemRevision == null)
								continue;

							if (currentUsrGrp!= null )
							{
								if (!currentUsrGrp.equalsIgnoreCase(itemRevision.getItem().getProperty("owning_group")))
									diffGroup = true;
							
							}
							currentUsrGrp = itemRevision.getItem().getProperty("owning_group");
						}
						if (diffGroup && enProcess != null){
							currentUsrGrp = enProcess.getProperty("owning_group");
						}
						
					}
					if (currentUsrGrp == null)
						currentUsrGrp = fp.getForm().getProperty("owning_group").toString();
					for(int inx = 0;grpStrArray != null && inx< grpStrArray.length; inx++ )
			         {
			             if(currentUsrGrp.equals(grpStrArray[inx]))
			             {
			             	return true;
			             }
			         }
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return false;
		}
	
	public static boolean isGroup(String pref,TCComponent form) {
		
		TCSession session = (TCSession)AIFUtility.getDefaultSession();
		TCPreferenceService prefServ = session.getPreferenceService();
		String[] grpStrArray = prefServ.getStringArray(
                TCPreferenceService.TC_preference_site,pref);
		String objectOwnerGrp = null;
		if (form == null)
			return false;
			try {
				AIFComponentContext[] context = form.whereReferenced();
				TCComponentProcess enProcess = null;
				TCComponentItemRevision itemRevision = null;
				boolean diffGroup = false;
				for (int inx = 0; inx<context.length;inx++)
				{
					if ( context[inx] == null || !(context[inx].getComponent() instanceof TCComponentProcess))
						continue;
					enProcess =  (TCComponentProcess)context[inx].getComponent();
					TCComponent[] targets= enProcess.getRootTask().getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
					for(int i=0;i<targets.length;i++)
					{
						if(targets[i] == null || !(targets[i] instanceof TCComponentItemRevision))
							continue;
						itemRevision = (TCComponentItemRevision)targets[i];
						if (itemRevision == null)
							continue;
						if (objectOwnerGrp!= null )
						{
							if (!objectOwnerGrp.equalsIgnoreCase(itemRevision.getItem().getProperty("owning_group")))
								diffGroup = true;
						
						}
						objectOwnerGrp = itemRevision.getItem().getProperty("owning_group");
					}
					if (diffGroup && enProcess != null){
						objectOwnerGrp = enProcess.getProperty("owning_group");
					}
					
				}
				if (objectOwnerGrp == null)
					objectOwnerGrp = form.getProperty("owning_group").toString();
				for(int inx = 0;grpStrArray != null && inx< grpStrArray.length; inx++ )
		         {
		             if(objectOwnerGrp.equals(grpStrArray[inx]))
		             {
		             	return true;
		             }
		         }
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return false;
	}
	
	public static boolean isGroup(String pref,TCComponent itemRev,boolean lLogical) {
		
		TCSession session = (TCSession)AIFUtility.getDefaultSession();
		TCPreferenceService prefServ = session.getPreferenceService();
		String[] grpStrArray = prefServ.getStringArray(
                TCPreferenceService.TC_preference_site,pref);
		String objectOwnerGrp = null;
		TCComponentItemRevision itemRevision= null;
		if (itemRev== null)
			return false;
		if (itemRev instanceof TCComponentItemRevision)
		{
			itemRevision = (TCComponentItemRevision)itemRev;
			try {
				objectOwnerGrp = itemRevision.getItem().getProperty("owning_group");
				for(int inx = 0;grpStrArray != null && objectOwnerGrp != null &&  inx< grpStrArray.length; inx++ )
		         {
		             if(objectOwnerGrp.equals(grpStrArray[inx]))
		             {
		             	return true;
		             }
		         }
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

}
