package com.noi.rac.en.util;

import com.teamcenter.rac.common.lov.LOVPopupButton;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;

//Referenced classes of package com.ugsolutions.rac.common.lov:
//         LOVPanel, LOVObject

public class NOVLOVPopUpButton extends LOVPopupButton 
 //implements PropertyChangeListener
{

 public NOVLOVPopUpButton(TCSession racsession, String s)
 {     
             super(racsession, s);
        
 }


 


 public Dimension getPreferredSize()
 {
     Dimension dimension = super.getPreferredSize();
     Icon icon = getIcon();
     int i;
     if(icon !=null)
     i= icon.getIconWidth();
     else
    	 i=5;
     dimension.width = i;
     
     return dimension;
 }



}