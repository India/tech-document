package com.noi.rac.en.util;

import java.util.Vector;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

//TCDECREL-5689 Start: Search Provider to get related Minor Revisions
public class RelatedMinorRevisionSearchProvider implements INOVSearchProvider2 
{
	TCComponent[] m_targetComponents = null;
	String[] m_puidOfRDD = null;
	
	public RelatedMinorRevisionSearchProvider(TCComponent[] targetsAttached) 
	{
		setTargetComponents(targetsAttached);
	}
	@Override
	public String getBusinessObject() 
	{
		return null;
	}
	
	@Override
	public boolean validateInput() 
	{
		m_puidOfRDD = getSelectedRddUid();

		if(m_puidOfRDD != null && m_puidOfRDD.length > 0)
        	return true;	
        else
        	return false;
	}

	@Override
	public QuickBindVariable[] getBindVariables() 
	{
		QuickBindVariable[] bindVariables = new QuickBindVariable[3];
        
        // Relation type
        bindVariables[0] = new QuickBindVariable();
        bindVariables[0].nVarType = POM_string;
        bindVariables[0].nVarSize = 1;
        bindVariables[0].strList = new String[] { "RelatedDefiningDocument" };
        
        // puids of RDDs of target minor revisions 
        bindVariables[1] = new QuickBindVariable();
        bindVariables[1].nVarType = POM_typed_reference;
        bindVariables[1].nVarSize = m_puidOfRDD.length;
        bindVariables[1].strList = m_puidOfRDD;
     
      	// Alternate id, idcontext
        bindVariables[2] = new QuickBindVariable();
        bindVariables[2].nVarType = POM_string;
        bindVariables[2].nVarSize = 1;
        bindVariables[2].strList = new String[] {"RSOne"}; 
  
        return bindVariables;		
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo()
	{
		QuickHandlerInfo[] allHandlerInfo = new QuickHandlerInfo[3];
    
		// Parent handler to pull part family of attached targets
	    allHandlerInfo[0] = new QuickHandlerInfo();
	    allHandlerInfo[0].handlerName = "NOVSRCH-get-partfamily";
	    allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
	    allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5, 6 };
	    allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 3, 4, 5, 6 };	   
			
	    // Handler to get release status
		allHandlerInfo[1] = new QuickHandlerInfo();
		allHandlerInfo[1].handlerName ="NOVSRCH-get-release-status";
		allHandlerInfo[1].listBindIndex = new int[0];
		allHandlerInfo[1].listReqdColumnIndex = new int[] {2};
		allHandlerInfo[1].listInsertAtIndex = new int[] {7};

		// Handler to get part alternate ids from minor revs
		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[2].handlerName ="NOVSRCH-get-item-alt-ids-from-revision";
		allHandlerInfo[2].listBindIndex = new int[]{3};
		allHandlerInfo[2].listReqdColumnIndex = new int[] {2};
		allHandlerInfo[2].listInsertAtIndex = new int[] {8}; 

	    return allHandlerInfo;		
	}
		
	//Method to get the selected rdd from part attached to EN form as target
	private String[] getSelectedRddUid()
	{
		TCComponent[] attachedTargets = getTargetComponents();		
		Vector<String> vectorOfPuids = new Vector<String>();

		for( int index = 0; index < attachedTargets.length; index++ )
		{
			TCComponent rdd = getRDDForComponent(attachedTargets[index]);
			if (rdd != null)
			{		
				vectorOfPuids.add(rdd.getUid());
			}			
		}

		return vectorOfPuids.toArray(new String[vectorOfPuids.size()]);
	}
	
	public void setTargetComponents(TCComponent[] targetComponents)
	{
		this.m_targetComponents = targetComponents;
	}
	
	public TCComponent[] getTargetComponents()
	{
		return m_targetComponents;
	}
	
	public static TCComponent getRDDForComponent(TCComponent targetComponent)
    {
        TCComponent[] rdd = null;
        try
        {
            if (targetComponent.getType().equalsIgnoreCase("Documents Revision"))
            {
                TCComponent item = ((TCComponentItemRevision) targetComponent).getItem();
                
                if (item != null)
                {
                    rdd = new TCComponent[] { item };
                }
            }
            else
            {
                rdd = targetComponent.getRelatedComponents("RelatedDefiningDocument");
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        if (rdd != null && rdd.length > 0)
        {
            return rdd[0];
        }
        return null;
    }
}
//TCDECREL-5689 End