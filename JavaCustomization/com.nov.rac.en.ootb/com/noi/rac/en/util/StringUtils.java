package com.noi.rac.en.util;

public class StringUtils {

	public static boolean in(String str,String[] arr)  {
		
		for (int i=0;i<arr.length;i++) {
			if (str.equals(arr[i]))
				return true;
		}
		return false;
	}
	
	public static boolean inIgnoreCase(String str,String[] arr)  {
		
		for (int i=0;i<arr.length;i++) {
			if (str.equalsIgnoreCase(arr[i]))
				return true;
		}
		return false;
	}
	public static String replaceSpecialChars(String in) {
		String out = new String();
		
		in = in.replaceAll(" ","_");
		out = in.replaceAll("\\\\","_");
		
		
		return out;
	}
}
