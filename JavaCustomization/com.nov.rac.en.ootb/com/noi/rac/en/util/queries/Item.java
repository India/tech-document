/*
 * Created on May 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.queries;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
/*import com.ugsolutions.rac.kernel.*;*/
/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Item {

    public static TCComponent[] execute(TCSession session,String[] names,String[] values) 
    {
        Registry reg = Registry.getRegistry("com/noi/rac/en/util/queries/queries");
        try {
            TCComponentQueryType qt = 
                   (TCComponentQueryType)session.getTypeComponent(reg.getString("ImanQuery.QRY"));
            TCComponentQuery gen = (TCComponentQuery)qt.find(reg.getString("Item.QRY"));
            if (gen != null) {
                return (gen.execute(names,values));
            }
        }
        catch (Exception e)
        {
            System.out.println("Could not execute general query: "+e);
        }
        return (TCComponent[])null;
    }
}
