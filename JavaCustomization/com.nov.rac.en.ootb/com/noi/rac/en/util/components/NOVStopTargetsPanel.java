package com.noi.rac.en.util.components;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.noi.rac.en.form.compound.commands.envalidation.NOVEnValidator;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.form.compound.panels._StopForm_Panel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVStopTargetsPanel  extends JPanel
{

	
	public NOVStopTargetsTable targetsTable;
	public JLabel lastValidationDate=new NOIJLabel();
	public Date validationDate;
	public JButton validateButton;
	public _StopForm_Panel stopFormPanel;
	private INOVCustomFormProperties novFormProperties;
	private Registry reg;
	public NOVStopTargetsPanel(_StopForm_Panel stopFormPanel,
			INOVCustomFormProperties novFormProperties,
			boolean bIsStop )
	{
		this.novFormProperties=novFormProperties;
		setLayout(new PropertyLayout());
		this.stopFormPanel=stopFormPanel;
		reg = Registry.getRegistry(this);
		/*validateButton=new JButton("Validate");
		validateButton.addActionListener(this);*/
		//top Panel
//		JPanel topPanel=new JPanel();
//		//topPanel.add(new NOIJLabel(reg.getString("Affected.LVL") ));
//		topPanel.add("1.1.left.center", new NOIJLabel(reg.getString("AffectedSTOP.LBL") ));
//		topPanel.setPreferredSize(new Dimension(110, 20));
		//topPanel.add(validateButton);
		
		//get the last saved validation date;
	/*	DateButton valid=(DateButton)novFormProperties.getProperty("lastvalidatedon");
		SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
		if(valid.getDate()!=null)
		{
        String validDateString = sdfDestination.format(valid.getDate()); 
        lastValidationDate.setText(validDateString);
		}
		topPanel.add(lastValidationDate);*/
		//add(topPanel, BorderLayout.NORTH);
		String columnNames[];
		if(bIsStop)
			columnNames=reg.getString("columnNamesSTOP.LIST").split(",");
		else
			columnNames=reg.getString("columnNamesCANCEL_STOP.LIST").split(",");
		targetsTable=new NOVStopTargetsTable(stopFormPanel, stopFormPanel.getSession(), columnNames);
		targetsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane=new JScrollPane(targetsTable);
		scrollPane.setPreferredSize(new Dimension(625,140));// TCDECREL-2064	change dimension for alignment
		//add(scrollPane, BorderLayout.CENTER);	
		
		//this.add("1.1.left.center",topPanel);	
		this.add("1.1.center.center",scrollPane);	
		
		//check if the form is checked out to enable validate button
		/*try
		{
		if(enFormPanel.Enform.getTCProperty("checked_out_date").getDateValue()==null)
			validateButton.setEnabled(false);
		else
			validateButton.setEnabled(true);
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}*/
	}
	/*public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==validateButton)
		{
			NOVEnValidator validator=new NOVEnValidator(enFormPanel, this);
			//get the time stamp of validation and save it in the form variable
			Calendar calendar=Calendar.getInstance();
			validationDate=calendar.getTime();
			((DateButton)novFormProperties.getProperty("lastvalidatedon")).setDate(validationDate);
			 SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy hh:mm");             
             String validDateString = sdfDestination.format(validationDate); 
             lastValidationDate.setText(validDateString);   
			
		}
	}*/


}
