/*
 * Created on Sep 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JRadioButton;

import com.noi.rac.en.NationalOilwell;

/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOVJRadioButton extends JRadioButton {

	/**
	 * 
	 */
	public NOVJRadioButton() {
		super();
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 */
	public NOVJRadioButton(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOVJRadioButton(String arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 */
	public NOVJRadioButton(Action arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 */
	public NOVJRadioButton(Icon arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOVJRadioButton(Icon arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOVJRadioButton(String arg0, Icon arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public NOVJRadioButton(String arg0, Icon arg1, boolean arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOVB12ptFont);
		setBackground(NationalOilwell.NOVPanelBG);
	}

}
