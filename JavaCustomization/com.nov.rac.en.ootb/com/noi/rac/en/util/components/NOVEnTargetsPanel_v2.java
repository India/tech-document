package com.noi.rac.en.util.components;
import java.awt.BorderLayout;
         
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import com.noi.rac.en.customize.util.MessageBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import com.noi.NationalOilwell;
//import com.noi.rac.en.form.compound.commands.envalidation.NOVEnValidator_v2;
import com.noi.rac.en.form.compound.commands.envalidation.NovEnValidation;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.mission.helper.NOVMissionHelper_v2;
import com.noi.rac.en.util.components.NOVECNTargetsTable_v2.NovTreeTableNode;
import com.nov.rac.en.commands.NOVENPrintCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMViewRevision;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentBOMWindowType;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.treetable.TreeTableNode;
//import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;


public class NOVEnTargetsPanel_v2 extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	public NOVECNTargetsTable_v2 targetsTable;
	public JLabel lastValidationDate=new NOIJLabel();
	public Date validationDate;
	public JButton validateButton;
	public JButton oracleNotify;
	public JButton helpEn;
	public JButton printEn;	
	public JButton addBtn;
	public JButton remBtn;
	public _EngNoticeForm_Panel_v2 enFormPanel;
	protected INOVCustomFormProperties novFormProperties;
	private TCPreferenceService prefServ ;
	String helpUrl = "";
	private Registry reg = Registry.getRegistry(this);
	private boolean m_isReleased = false;
	TCComponentTask	enRootTask	= null;
	boolean	isLockedPropVal = true;
	private Component comp = null;
	private TCSession session	= null;
	public NOVEnTargetsPanel_v2(_EngNoticeForm_Panel_v2 enFormPanel, INOVCustomFormProperties novFormProperties, 
			boolean isReleased )
	{
		boolean statusGroup = NOVMissionHelper_v2.isGroup(reg.getString("statusPref.NAME"),novFormProperties);
		boolean noValGroup = NOVMissionHelper_v2.isGroup(reg.getString("noValPref.NAME"),novFormProperties);
		boolean noOraGroup = NOVMissionHelper_v2.isGroup(reg.getString("noOraPref.NAME"),novFormProperties);
		this.novFormProperties=novFormProperties;
		setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		this.enFormPanel=enFormPanel;
		m_isReleased = isReleased;
		validateButton=new JButton(reg.getString("VALIDATE.LABEL"));
		if(enFormPanel.getProcess()==null)
			validateButton.setEnabled(false);
		validateButton.addActionListener(this);
		
		oracleNotify = new JButton(reg.getString("NOTIFYORACLE.LABEL"));
		oracleNotify.addActionListener(this);
		
		helpEn = new JButton(reg.getString("HELPSTR.LABEL"));//"Help");
		printEn = new JButton( reg.getString( "PRINTSTR.LABEL" ) );	
		
		helpEn.addActionListener(this);
		printEn.addActionListener( this );
		session = enFormPanel.getSession();
		prefServ = session.getPreferenceService();
		helpUrl = prefServ.getString(
		TCPreferenceService.TC_preference_site, reg.getString("NOI_NewENHelp.PREF"));
		
		boolean bIsOracleButtonEnable = false;
		TCComponentProcess enProcess = enFormPanel.getProcess();
		try 
		{			
			if(enFormPanel.getProcess()!=null && enFormPanel.Enform != null)
			{
				String sChangeType = enFormPanel.Enform.getTCProperty("nov4_change_type").getStringValue();
				String[] editableTypes=reg.getStringArray("EnableNotifyOracleButton.CHANGETYPE");
				if((sChangeType == null || sChangeType.isEmpty()) || (Arrays.asList(editableTypes).contains(sChangeType)))
				{
					enRootTask = enProcess.getRootTask();
					TCComponent[] targets = enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
					for(int i=0;i<targets.length;i++)
					{
						if(targets[i] instanceof TCComponentItemRevision)
						{
							if(((TCComponentItemRevision)targets[i]).getItem().getProperty("release_status_list").contains("ACTIVE")
									|| ((TCComponentItemRevision)targets[i]).getItem().getProperty("release_status_list").contains("MFG")								)
							{
								bIsOracleButtonEnable = true;
								break;
							}
						}
					}
				}
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		oracleNotify.setEnabled(bIsOracleButtonEnable);
	
		JPanel leftPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		leftPanel.add("1.1.left.center",new NOIJLabel(reg.getString("ENGNOTICETGTS.LABEL")));
		if (!noOraGroup)
		{
			leftPanel.add("1.2.left.bottom",oracleNotify);
		}
		if (!noValGroup)
		{
			if (!noOraGroup)
				leftPanel.add("1.3.left.bottom",validateButton);
			else
				leftPanel.add("1.2.left.bottom",validateButton);
		}
		JPanel rightPanel=new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		rightPanel.add("1.1.right.bottom",helpEn);
		rightPanel.add("1.2.right.bottom",printEn);
	
		String columnNames[] ;
			
		if(!statusGroup){
			columnNames = new String[5] ;

			columnNames[0]=reg.getString("PartID.TYPE");
			columnNames[1]=reg.getString("PartName.TYPE");
			columnNames[2]=reg.getString("Rev.TYPE");
			columnNames[3]=reg.getString("StartStatus.TYPE");
			columnNames[4]=reg.getString("FutureStatus.TYPE");
		}
			
		else{
			columnNames = new String[3] ;
			columnNames[0]=reg.getString("PartID.TYPE");
			columnNames[1]=reg.getString("PartName.TYPE");
			columnNames[2]=reg.getString("Rev.TYPE");
			
		}
		
		targetsTable=new NOVECNTargetsTable_v2(enFormPanel, enFormPanel.getSession(), columnNames, m_isReleased );
		targetsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		JScrollPane scrollPane=new JScrollPane(targetsTable);
		scrollPane.setPreferredSize(new Dimension(820,100));
		
		JPanel targetsPanel1	= new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		JPanel addRemBtnPanel	= new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		
		ImageIcon addIcon = reg.getImageIcon("enadd.IMG");
		ImageIcon minusIcon =reg.getImageIcon("enremove.IMG");

		addBtn = new JButton(addIcon);
		addBtn.addActionListener(this);	
		addBtn.setToolTipText(reg.getString("addToTargetsToolTip.STR"));
		addBtn.setEnabled(false);
		remBtn	= new JButton(minusIcon);
		remBtn.addActionListener(this);	
		remBtn.setToolTipText(reg.getString("rmvTargetToolTip.STR"));
		remBtn.setEnabled(false);
		
		if(enFormPanel.isReadOnly == false)
		{
			addBtn.setEnabled(true);
			remBtn.setEnabled(true);
		}

		//Added for disabling add/remove btns based on  site preference- NOV_EnableAddRemoveGroups
		String[] enableGrpNames = prefServ.getStringArray(
				TCPreferenceService.TC_preference_site,
				reg.getString("NOV_EnableAddRemoveGroups.PREF") );
				//"NOV_EnableAddRemoveGroups");	
		boolean	enableAddRem	= false;
		//enableAddRem = NOVMissionHelper_v2.isGroup(reg.getString("statusPref.NAME"));
		if(enableGrpNames!= null && enableGrpNames.length>0)
		{
			//if(enableGrpNames[0].equalsIgnoreCase("All"))
			if(enableGrpNames[0].equalsIgnoreCase(reg.getString("allGroups.VAL")) )
			{
				enableAddRem = true;
			}
			else
			{
				String currentUserGrpName = session.getCurrentGroup().toString();
				for(int i=0; i<enableGrpNames.length; ++i)
				{
					if(currentUserGrpName.contains(enableGrpNames[i]))
					{
						enableAddRem = true;
						break;
					}
				}
			}
		}//enableGrpNames
		//if the preference is not found or if the preference holds no values or
		//if current user group is not in enable groups list,
		//disable add & remove buttons...
		if(!enableAddRem)
		{
			addBtn.setEnabled(false);
			remBtn.setEnabled(false);
		}
		
		addRemBtnPanel.add("1.1", addBtn);
		addRemBtnPanel.add("1.2", remBtn);
		targetsPanel1.add("1.1.center.top",scrollPane);
		targetsPanel1.add("2.1.center.top",addRemBtnPanel);
		targetsPanel1.setPreferredSize(new Dimension(840,155));
		
		TitledBorder tb = new TitledBorder("");		
		targetsPanel1.setBorder(tb);
		
		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.add(leftPanel,BorderLayout.LINE_START);
		panel1.add(rightPanel,BorderLayout.LINE_END);
		panel1.setPreferredSize(new Dimension(840,45));
		JPanel panel = new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		panel.add("1.1", panel1);
		panel.add("2.1", targetsPanel1);
		
		this.add("1.1.left.top",panel);
		//this.add("2.1.right.top",targetsPanel1);
		
		if( m_isReleased )
			setControlState();
		}

    public void setControlState()
	{
    	//validateButton.setEnabled( !m_isReleased );
    	//oracleNotify.setEnabled( !m_isReleased );
	}
	
	public void actionPerformed(ActionEvent ae)     
	{
		Object 	addRemRetObj = null;
		Object 	addRemInputObjs[] = new Object[3];
		int		addRemFlag		= 0;
		if(ae.getSource()==validateButton)
		{		
			validateDocumentInBOM();
			NovEnValidation valDlg = new NovEnValidation(
            		AIFUtility.getCurrentApplication().getDesktop(), enFormPanel);
			
			
			
			valDlg.setVisible( true );
		}
		
		if(ae.getSource()==helpEn)
        {
			if(helpUrl==null || helpUrl.length()==0)
			{
				comp = (JPanel)this.getParent().getParent();
				//helpUrl = "http://srvhouplmt01:7081/cetgws/Standard-ENform.pdf";
				MessageBox.post(getWindow(comp), 
						NationalOilwell.SEEADMIN, NationalOilwell.NOHELP, MessageBox.ERROR);
			}
			else
			{
				try 
				{
					Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+helpUrl);
				} 
				catch (Exception ex)
				{
					ex.printStackTrace();
					comp = (JPanel)this.getParent().getParent();
					MessageBox.post(getWindow(comp),
							NationalOilwell.SEEADMIN, NationalOilwell.NOHELP, MessageBox.ERROR);
				}
			}
			return;
		}
		else if( ae.getSource() == printEn )
		{
			Frame parent = ( Frame )AIFUtility.getActiveDesktop();
			InterfaceAIFComponent comps[] = ( InterfaceAIFComponent[] )new TCComponent[]{
				 novFormProperties.getForm() };
			NOVENPrintCommand cmd = new NOVENPrintCommand( parent, comps );
			return;
		}		
		else if(ae.getSource()==oracleNotify)
		{
			try 
			{
				TCComponentProcess enProcess = enFormPanel.getProcess();
				TCComponentTask enRootTask = enProcess.getRootTask();
				String processUID = enRootTask.getUid();
				
				TCComponent[] targets = enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
				String[] reqTCProprties = { "isOracleNotify" };
				//TCProperty[][] retTCProp = TCComponentType.getTCPropertiesSet( targets, reqTCProprties );
				List<TCComponent> targetList = Arrays.asList(targets);//TC10.1 Upgrade
				TCProperty[][] retTCProp = TCComponentType.getTCPropertiesSet( targetList, reqTCProprties );//TC10.1 Upgrade
				
				for( int index = 0; index < targets.length; ++index )
				{
					if( targets[index] instanceof TCComponentForm )
					{
						if( retTCProp[index][0].getIntValue() == 0 )
						{
							//call userExit to create the Disposition components and return the array
							TCUserService us = enFormPanel.getSession().getUserService();       	
							Object obj[]=new Object[3];       	
							obj[0]=targets.length;
							obj[1]=targets;
							obj[2]=processUID;
							us.call("NATOIL_OracleNotify", obj);
							
							oracleNotify.setEnabled(false);
						}
					}
				}
			} 
			catch (TCException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}		
		}
		//usha, for add-remove targets
		else if(ae.getSource()== addBtn)
		{
			//saving the form before adding target - TCDECREL-2051
			enFormPanel.saveForm();
			//fetching isLocked value in both listeners- because cache value is carried to next add/remove
			try {
				isLockedPropVal = enFormPanel.Enform.getLogicalProperty(reg.getString("islocked.PROP"));
			} catch (TCException e) {
				e.printStackTrace();
			}
			if(isLockedPropVal == true)
			{
				comp = (JPanel)this.getParent().getParent();
				MessageBox.post(getWindow(comp),"ERROR", reg.getString("formIsLocked.MSG"),  MessageBox.ERROR);
				return;
			}
			TCComponentProcess enProcess = enFormPanel.getProcess();
			try
			{
				enRootTask = enProcess.getRootTask();
				String processUID = enRootTask.getUid();	
				TCComponent[] targets = enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
				
				TCComponentItemRevision targetToAdd = null;
				//open search dialog
				NOVAddENTargetSearchDialog searchDialog = new  NOVAddENTargetSearchDialog(AIFUtility.getCurrentApplication().getDesktop(),this);
				searchDialog.setLocationByPlatform(true);
				searchDialog.setSize(900,550);
				searchDialog.setVisible(true);

			} catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
		else if(ae.getSource()== remBtn)
		{
			//saving the form before removing target - TCDECREL-2051
			enFormPanel.saveForm();
			//fetching isLocked value in both listeners- because cache value is carried to next add/remove
			comp = (JPanel)this.getParent().getParent();
			try {
				isLockedPropVal = enFormPanel.Enform.getLogicalProperty(reg.getString("islocked.PROP"));
			} catch (TCException e) {
				e.printStackTrace();
			}
			if(isLockedPropVal == true)
			{
				MessageBox.post(getWindow(comp),"ERROR",reg.getString("formIsLocked.MSG"),  MessageBox.ERROR);
				return;
			}
 			TCComponentProcess enProcess = enFormPanel.getProcess();
			try
			{
				enRootTask = enProcess.getRootTask();
				String processUID = enRootTask.getUid();
				
				if(enFormPanel.targetsPanel.targetsTable.getSelectedRowCount() <1)
				{
					MessageBox.post(getWindow(comp),"ERROR", reg.getString("selectPartToRemove.MSG"), MessageBox.ERROR);				
					return;
				}
				if(enFormPanel.targetsPanel.targetsTable.getSelectedRowCount() >1)
				{
					MessageBox.post(getWindow(comp),"ERROR", reg.getString("selectSinglePartToRemove.MSG"),  MessageBox.ERROR);
					return;
				}
				int selRow= enFormPanel.targetsPanel.targetsTable.getSelectedRow();		
				if(selRow == 0)
				{
					MessageBox.post(getWindow(comp),"ERROR", reg.getString("selectPartToRemove.MSG"), MessageBox.ERROR);
					return;
				}
				TCComponent[] targets = enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
				
				TreeTableNode selNodes[] = enFormPanel.targetsPanel.targetsTable.getSelectedNodes();
				Object objItem = enFormPanel.targetsPanel.targetsTable.getValueAt(selRow, 0);
				Object objRev = enFormPanel.targetsPanel.targetsTable.getValueAt(selRow, 2);
				TCComponentItemRevision targetToRemove = null;
				
				for(int i=0; (i<targets.length) ; i++)
				{			
					if( (targets[i] instanceof TCComponentItemRevision)&& 
							( targets[i].getStringProperty("item_id").equalsIgnoreCase(objItem.toString()) ))	
					{
						targetToRemove = (TCComponentItemRevision) targets[i];
						break;
					}			
	 			}
				//close disp tab
				if(enFormPanel.enautomatonTabbePane.getComponentCount()>1)
				{
					NOVEnDispositionTabPanel_v2 dispPanel=(NOVEnDispositionTabPanel_v2)enFormPanel.enautomatonTabbePane.getComponentAt( 1 );
					if(dispPanel != null)
					{
						dispPanel.dispcomp.refresh();
						enFormPanel.enautomatonTabbePane.remove( dispPanel );
					}
				}

				//call user service			
				TCUserService userservice = enFormPanel.Enform.getSession().getUserService();		
				addRemInputObjs[0] = (	TCComponent)enRootTask;
				addRemInputObjs[1] = new TCComponent[] {targetToRemove};
				addRemInputObjs[2] = addRemFlag;
				addRemRetObj = (Object[]) userservice.call("NATOIL_AddRemoveTargets_EN", addRemInputObjs);
				
				if(addRemRetObj!= null)
				{		
					//String selObjType = targetToRemove.getType();
					TCComponent[] removeList = (TCComponent[])addRemRetObj;
					int	removeCnt	= removeList.length;				
					
					//enRootTask.removeAttachments(removeList);
					if(removeCnt==1)
					{	
						if(removeList[0].getType().startsWith(reg.getString("DocsItem.TYPE")))
						{
							this.enFormPanel.Entargets.remove(removeList[0]);
						}else
						{
							TreeTableNode selNode = (TreeTableNode)selNodes[0];
							targetsTable.removeTarget(targetToRemove, selNode);
						}
					}
					if(removeCnt>1) 
					{
						for(int j=0; j<removeCnt; j++)
						{
							//if RDD, remove from targets list - no need to remove from UI 
							if(removeList[j].getType().startsWith(reg.getString("DocsItem.TYPE")))
							{
								this.enFormPanel.Entargets.remove(removeList[j]);
								continue;
							}
							//if not doc, find and remove the node from UI -- in turn removes RDD also
							//and remove from targets list.
							for(int k=1; k< targetsTable.treeTableModel.getRowCount(); k++)
							{
								TCComponentItemRevision compAtNode= null;	
								TreeTableNode treeNode = targetsTable.getNodeForRow(k);
								if (treeNode instanceof NovTreeTableNode) 
									compAtNode = (TCComponentItemRevision)((NovTreeTableNode)treeNode).context;
								if(compAtNode != null && removeList[j] == compAtNode)
								{
									targetsTable.removeTarget(compAtNode, treeNode);
								}
							}
						}				
					}//if remove cnt is more than 1	
					enFormPanel.Enform.save();
					enFormPanel.Enform.refresh();
					targetsTable.updateUI();
				}//if return not null
			} catch (TCException e) 
			{
				//throw error if all targets are being removed
				MessageBox.post(getWindow(comp),"ERROR", e.getDetailsMessage(),  MessageBox.ERROR);
			}
		}
		if( (addRemRetObj != null) && (addRemRetObj.toString().length()> 0))
		{
			MessageBox.post(getWindow(comp),"Info", reg.getString("Removed.MSG"), MessageBox.INFORMATION);
		}

	}
	
	private void validateDocumentInBOM() {

		Vector<TCComponent> targets = enFormPanel.Entargets;
		String itemsWithDoc = "";
		String msg = "";
		String partID = null;
		for (int i = 0; i < targets.size(); i++) {
			TCComponentBOMViewRevision bvr= getBvrfromItemRev(targets.get(i));
			if(bvr!=null)
				if(isDOcumentOnBOM((TCComponentItemRevision) targets.get(i),bvr))
				{
					partID = (targets.get(i).toDisplayString());
					itemsWithDoc = itemsWithDoc + partID + "\n";
				}
		}
		msg =  reg.getString("docOnBOM.MSG")+"\n\n"+itemsWithDoc;
		if(itemsWithDoc != "" )
		{
			comp = (JPanel)this.getParent().getParent();
			com.noi.rac.en.customize.util.MessageBox.post(getWindow(comp), reg.getString("docOnBOM.TITLE"), msg, MessageBox.WARNING);
		}
	}
	
	private TCComponentBOMViewRevision getBvrfromItemRev(TCComponent targetRev)
	{
		TCComponentBOMViewRevision theBom=null;
		try
		{
			//fetch the Bvr			
			AIFComponentContext[] itemRevisionComponents = ((TCComponentItemRevision)targetRev).getChildren();
			for (int i=0;i<itemRevisionComponents.length;i++)
			{
				if (itemRevisionComponents[i].getComponent() instanceof TCComponentBOMViewRevision) 
				{		
					theBom = (TCComponentBOMViewRevision)itemRevisionComponents[i].getComponent();
					break;
				}
			}			
		}
		catch(TCException te)
		{
			System.out.println();
		}
		return theBom;
	}
	
	private boolean isDOcumentOnBOM(TCComponentItemRevision topRev, TCComponentBOMViewRevision theBOM)
	{
		Vector<TCComponent> firstLevelBomLines=new Vector<TCComponent>();
		boolean hasDoc = false;
		try
		{
			TCComponentBOMWindowType bwType = 
				(TCComponentBOMWindowType)session.getTypeService().getTypeComponent("BOMWindow");
			TCComponentBOMWindow bw = bwType.create(null);
			TCComponentBOMLine topline = bw.setWindowTopLine(null,null,null,theBOM);

			AIFComponentContext[] childComponents = topline.getChildren();
			for (int k=0;k<childComponents.length;k++) 
			{
				TCComponentBOMLine bl = (TCComponentBOMLine)childComponents[k].getComponent();
				TCComponentItemRevision childcomp=bl.getItemRevision();
			
				if(childcomp.getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE")))
				{
					hasDoc = true;
					break;
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		return hasDoc;
	}

	public Window getWindow(Component con)
	{
		Window window = null;
		
		if(con.getParent() instanceof Window)
		{
			window = (Window) con.getParent();
		}
		else
		{
			 window = getWindow(con.getParent());
		}
		
		return window;	
	} 
	
}
