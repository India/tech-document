package com.noi.rac.en.util.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.util.Miscellaneous;
import com.teamcenter.rac.util.Registry;




public class MultiSelectAddressPanel extends JPanel implements ActionListener {

	JList sourceList = new JList();
	JScrollPane sourceListScroll = new JScrollPane(sourceList);
	JList targetList = new JList();
	JScrollPane targetListScroll = new JScrollPane(targetList);

	JButton addTarget;
	JButton removeTarget;

	Hashtable stringToRef = null;
	Hashtable refToString = null;


	
	Dimension listSize;

	private JButton addressbookButton;
	private JTextField mailId;
	private JButton plusButton;	
	//private boolean locked;



	public MultiSelectAddressPanel() {
		super();
		init(new java.awt.Dimension(250,100));
	}

	public void init(Dimension size) {
		
		//this.locked=locked;
		listSize = size;
		initUI();        
	}


	public void setEnabled(boolean enabled) {
		sourceList.setEnabled(enabled);
		targetList.setEnabled(enabled);
		addTarget.setEnabled(enabled);
		removeTarget.setEnabled(enabled);
	}
	public void actionPerformed(ActionEvent e)
	{}

	public boolean isValidEmail(String mailID)
	{
		Pattern p = Pattern.compile(".+@.+\\.[a-zA-Z]+");
		Matcher m = p.matcher(mailID);
		if(!m.find())
		{
			//System.out.println("Email address format should be like \"john.smith@nov.com\" .");
			return false;
		}
		//Checks for email addresses starting with inappropriate symbols like dots or @ signs.
		p = Pattern.compile("^\\.|^\\@");
		m = p.matcher(mailID);
		if (m.find())
		{
			//System.err.println("Email addresses don't start with dots or @ signs.");
			return false;
		}

		//Checks for email addresses that start with www. and prints a message if it does.
		p = Pattern.compile("^www\\.");
		m = p.matcher(mailID);
		if (m.find())
		{
			//System.out.println("Email addresses don't startwith \"www.\", only web pages do.");
			return false;
		}

		p = Pattern.compile("[^A-Za-z0-9\\.\\@_\\-~#]+");
		m = p.matcher(mailID);
		if(m.find())
		{
			//System.out.println("");
			return false;
		}
		return true;

	}

	public Dimension getSize() { return listSize; }

	public List getTargetTextValues() {
		ArrayList retArray = new ArrayList();
		int listCount = targetList.getModel().getSize();
		for (int i=0;i<listCount;i++) 
			retArray.add((String)targetList.getModel().getElementAt(i));
		return retArray;
	}
	public List getTargetReferenceValues() {/*
		ArrayList retArray = new ArrayList();
		int listCount = targetList.getModel().getSize();
		for (int i=0;i<listCount;i++) 
			retArray.add((TCComponent)stringToRef.get(targetList.getModel().getElementAt(i)));

		return retArray;
	*/
		return null;
		}
	public void setSelectedTextValues(String[] list) {


	}
	private void populateSourceList() {/*
		String[] listValues = sourceData.getList();		
		sourceList.setListData(listValues);
		sourceList.setPreferredSize(new Dimension(listSize.width,17*listValues.length));
	*/}
	private void populateTargetList() {/*
		String[] listValues = targetData.getList();
		targetList.setListData(listValues);
		targetList.setPreferredSize(new Dimension(listSize.width,17*listValues.length));        
	*/}

	public void initUI() 
	{
		Registry reg = Registry.getRegistry(this);
		GridBagConstraints gbc;
		GridBagLayout gbl = new GridBagLayout();

		gbl.columnWidths = new int[] { listSize.width, 64, listSize.width };
		gbl.rowHeights = new int[] { 17, listSize.height-17 };

		setLayout(gbl);
		TitledBorder tb = new javax.swing.border.TitledBorder(reg.getString("titledBorder.STR"));
		tb.setTitleFont(new Font(reg.getString("dialogFont.STR"), java.awt.Font.BOLD, 12));
		setBorder(tb);

		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		add(new NOIJLabel(reg.getString("availableLbl.STR")),gbc);
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 2;
		add(new NOIJLabel(reg.getString("selectedLbl.STR")),gbc);

		sourceListScroll.setPreferredSize(new Dimension(listSize.width,listSize.height-17));
		sourceListScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sourceListScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		gbc = new GridBagConstraints();
		gbc.gridy = 1;
		populateSourceList();
		populateTargetList();
		sourceListScroll.setToolTipText(reg.getString("tooltip.STR"));
		add(sourceListScroll,gbc);

		JPanel buttonPanel = new JPanel(new GridBagLayout());
		//buttonPanel.setBackground(new java.awt.Color(225,225,225));
		buttonPanel.setPreferredSize(new Dimension(64,listSize.height-17));
		ImageIcon addArrow = reg.getImageIcon("enadd.IMG");
		ImageIcon minusIcon =reg.getImageIcon("enremove.IMG");
		
		addTarget = new JButton(addArrow);
		addTarget.addActionListener(this);	
		addTarget.setToolTipText(reg.getString("addTargetToolTip.STR"));
		addTarget.setPreferredSize(new Dimension(45,21));
		addTarget.setBackground(new Color(192,192,192));
		

		removeTarget = new JButton(minusIcon);
		removeTarget.addActionListener(this);
		removeTarget.setToolTipText(reg.getString("rmvTargetToolTip.STR"));
		removeTarget.setBackground(new Color(192,192,192));
		removeTarget.setPreferredSize(new Dimension(45,21));
		
		gbc = new GridBagConstraints();
		buttonPanel.add(addTarget,gbc);
		gbc = new GridBagConstraints();
		gbc.gridy = 1;
		buttonPanel.add(removeTarget,gbc);

		gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(buttonPanel,gbc);

		targetListScroll.setPreferredSize(new Dimension(listSize.width,listSize.height-17));
		targetListScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		targetListScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		gbc = new GridBagConstraints();
		gbc.gridx = 2;
		gbc.gridy = 1;
		add(targetListScroll,gbc);


	}
	


}
