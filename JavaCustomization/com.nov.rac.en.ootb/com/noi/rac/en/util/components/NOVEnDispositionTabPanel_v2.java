package com.noi.rac.en.util.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import com.noi.rac.en.form.compound.component.NOVEnBomInfoPanel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
//import com.teamcenter.rac.aif.ApplicationDef; //TC10.1 Upgrade
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCClassificationService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMViewRevision;
import com.teamcenter.rac.kernel.TCComponentICO;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.kernel.ics.ICSKeyLov;
import com.teamcenter.rac.kernel.ics.ICSProperty;
import com.teamcenter.rac.kernel.ics.ICSPropertyDescription;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2010_04.DataManagement.LocalizedPropertyValuesList;

public class NOVEnDispositionTabPanel_v2 extends JPanel{
	private static final long serialVersionUID = 1L;	
	
	private TCComponent targetItemRev;
	private NOVEnBomInfoPanel bomInfoPanel;
	public NOVLOVPopupButton inProcesslov = new NOVLOVPopupButton();
	public NOVLOVPopupButton inInventorylov = new NOVLOVPopupButton();
	public NOVLOVPopupButton assembledlov = new NOVLOVPopupButton();
	public NOVLOVPopupButton inFieldlov = new NOVLOVPopupButton();
	public JTextArea txtDispInstrux;
	public TCComponent dispcomp;
	NOVENDispositionPanelData paneldata;
	public boolean newRelease=false;
	_EngNoticeForm_Panel_v2 enPanel = null;
	private TCSession session;
	private TCTable bomTable; 
	boolean hasBOM=false;
	public JButton viewBOMBtn;
	public JButton refreshBOMBtn;
	public JButton classifyBtn;
	JButton searchButton;	
	private Registry reg;
	protected boolean misGroup = false;
	public JTextField ecrNumberText;
	JButton copyDisposition;
	private boolean isClassifiable = false;
	public TCClassificationService classificationService;

	public TCComponent[] referncedispdata;
	public static int targetindex;
	public int index;
	public boolean isCopyAll;
	private boolean isBOMCompared = false;
	
	public NOVEnDispositionTabPanel_v2(_EngNoticeForm_Panel_v2 argENPanel,TCSession session, TCComponent dispcomp, boolean newRelease, 
			TCComponent targetItemRev2, TCComponent[] referncedispdata, int targetindex)
	{
		this.dispcomp=dispcomp;
		this.newRelease=newRelease;
		this.session=session;
		this.targetItemRev = targetItemRev2;
		reg = Registry.getRegistry(this);
		paneldata=new NOVENDispositionPanelData(dispcomp, newRelease);
		enPanel = argENPanel; 
		
		this.referncedispdata=referncedispdata;
		isCopyAll = false;
		this.index=targetindex;
		
		classificationService = session.getClassificationService();
	}
	
	public void createUI( boolean isRelased )
	{
		//create the top panel based on newRelease
		//create the BomPanel
		JPanel mainPanel = new JPanel();
		JPanel middlePanel=new JPanel();
		JPanel changePanel=new JPanel();
		JPanel classificationPanel = new JPanel( new PropertyLayout( 5 , 10 , 5 , 5 , 5 , 5 ) );
		JPanel bomPanel=new JPanel( new PropertyLayout( 5 , 10 , 5 , 5 , 5 , 5 )  );
		javax.swing.JLabel lblChageDesc = new JLabel();
		//ImageIcon icon = reg.getImageIcon("locale.IMG");
		boolean addOnlyChangePanel	= false;
		//Mohanar
		JPanel copyDispPanel = new JPanel();
		
		try
		{
			isClassifiable = classificationService.isObjectClassifiable( targetItemRev );
		}
		catch( TCException tce )
		{
			tce.printStackTrace();
		}
	
		//prefrenceSrv = dispcomp.getSession().getPreferenceService();		
		//boolean isLocale = prefrenceSrv.isTrue(TCPreferenceService.TC_preference_all, "NOV_show_localization_button");
		
		if(!newRelease)
		{
			JPanel disposition = new JPanel(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
			JLabel lblDisposition = new JLabel(reg.getString("DISPOSITION.LABEL"));
			lblDisposition.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblInProcess = new JLabel(reg.getString("INPROCESS.LABEL"));
			lblInProcess.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblInventry = new JLabel(reg.getString("ININVENTORY.LABEL"));
			lblInventry.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblAssembled = new JLabel(reg.getString("ASSEMBLED.LABEL"));
			lblAssembled.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblInField = new JLabel(reg.getString("INFIELD.LABEL"));
			lblInField.setFont(new Font("Dialog",java.awt.Font.BOLD,12));

			//Mohanar - To reove Change Desc Label
			//lblChageDesc.setText(reg.getString("CHANGEDESC.LABEL"));
			
			disposition.add("1.1.left.top", lblDisposition);
			disposition.add("2.1.left.top", lblInProcess);
			disposition.add("2.2.left.top", inProcesslov);
			disposition.add("3.1.left.top", lblInventry);
			disposition.add("3.2.left.top", inInventorylov);
			disposition.add("4.1.left.top", lblAssembled);
			disposition.add("4.2.left.top", assembledlov);
			disposition.add("5.1.left.top", lblInField);
			disposition.add("5.2.left.top", inFieldlov);

			JPanel dispInstructions = new JPanel();
			//p3Panel.setLayout(new VerticalLayout(2 , 2 , 2 , 2 , 2));
			dispInstructions.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
			JLabel lblDispInstrux = new JLabel(reg.getString("DISP_INS.LABEL"));
			lblDispInstrux.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			txtDispInstrux = new JTextArea(30,30);
			JScrollPane scDispInstrux = new JScrollPane(txtDispInstrux);
			scDispInstrux.setPreferredSize(new Dimension(321,110));
			
						
			dispInstructions.add("1.1.left.top",lblDispInstrux);
			dispInstructions.add("2.1.left.top",scDispInstrux);
			
			middlePanel.add(disposition);
			middlePanel.add(dispInstructions);
			
			//Mohanar
			copyDisposition = new JButton(reg.getString("COPYDISPBUTTON.LABEL"));
			copyDisposition.addActionListener(new ActionListener() {

				//@Override
				public void actionPerformed(ActionEvent arg0) {
					isCopyAll = true;
					targetindex=index;
					NOVEnDispositionPanel_v2 dispPanel = new NOVEnDispositionPanel_v2( referncedispdata );
					//dispPanel.setDispData(enPanel, isCopyAll);
					String futureStatus = dispPanel.targetStatusMap.get( targetItemRev );
					copyDispDataToAllParts( enPanel, futureStatus );

					//dispPanel.setDispData( enPanel );
					
					// Performance Issue Nitin : No need to save the disposition data as copyDispDataToAllParts 
					// will copy the disposition data to all parts.
					//dispPanel.saveDispData();
				}
			});
			copyDispPanel.add(copyDisposition);
		}

		final JPanel classifyPanel = new JPanel( new PropertyLayout( 5 , 10 , 5 , 5 , 5 , 5 ) );
		JLabel lblClassified = new JLabel();
		lblClassified.setFont( new Font( "Dialog",java.awt.Font.BOLD,12 ) );
		
		classifyBtn = new JButton( reg.getString( "CLASSIFYBUTTON.LABEL" ) );
		classifyBtn.addActionListener( new ActionAdapter()
		{
			public void actionPerformed( ActionEvent arg0 ) 
			{
				enPanel.saveForm();
				//ApplicationDef appDef = AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey(reg.getString("CLASSIFICATIONApps.APPS"));
				//appDef.openApplication(new InterfaceAIFComponent[]{targetItemRev});
				IOpenService openPerspective = PerspectiveDefHelper.getOpenService(reg.getString("CLASSIFICATIONApps.APPS"));
                openPerspective.open(new InterfaceAIFComponent[]{targetItemRev}); //TC10.1 Upgrade
			}
		});
		
		final ImageIcon expandIcon = reg.getImageIcon( "expand.IMG" );
		final ImageIcon collapseIcon = reg.getImageIcon( "collapse.IMG" );
	
		if( isClassifiable ) 
		{
			classifyPanel.setLayout( new BoxLayout( classifyPanel, BoxLayout.Y_AXIS ) );
			
			final JScrollPane pane = new JScrollPane( classifyPanel );
			pane.setPreferredSize( new Dimension( 325, 120 ) );
			pane.setVisible( false );

			JPanel headingPanel = new JPanel( new PropertyLayout( 5 , 5 , 5 , 5 , 5 , 5 ) );
			//JLabel blankLabel = new JLabel();
			//blankLabel.setPreferredSize( new Dimension( 325, 20 ) );
			headingPanel.add( "1.1.left.top", lblClassified );
			//headingPanel.add( "1.2.left.top", blankLabel );
			headingPanel.add( "1.2.left.top", classifyBtn );

			classificationPanel.add( "1.1.left.top", headingPanel );
			
			boolean isClassified = false;
			String classificationTxt = "";
			
			String className = "";
			try
			{
				isClassified = classificationService.isObjectClassified( targetItemRev );
				
				if( isClassified )
				{
					className = targetItemRev.getProperty( "ics_subclass_name" );
					
					classificationTxt = reg.getString( "CLASSIFICATION.LABEL" ) + " : " + targetItemRev.getProperty( "object_string" ) +
						" " + reg.getString( "ISCLASSIFIEDIN.LABEL" ) + " " +  className;	
					
//					TCComponentType type =   session.getTypeComponent( "_NOV_IC_Store_" );
//					String[] classNameArr = null; 
//					String[] classIDArr = null; 
//                	TCComponent[] comps =  type.extent();
//					for (int i = 0; i < comps.length; i++) 
//					{
//						classNameArr = comps[i].getTCProperty( "CP_attr" ).getStringValueArray();
//						classIDArr = comps[i].getTCProperty( "CID_attr" ).getStringValueArray();
//					}
				
					//JLabel clsHierarchyName = new JLabel( className );
					//classificationPanel.add( "2.1.top.center", clsHierarchyName );
				
					final JToggleButton tBtnClassify = new JToggleButton( collapseIcon );
					tBtnClassify.setPreferredSize( new Dimension( 20,20 ) );
					tBtnClassify.doClick();
					
					JLabel lblUnits = new JLabel(reg.getString("UNITS.LABEL"));
					lblUnits.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
					
					final JComboBox unitsCombo = new JComboBox();
					unitsCombo.setPreferredSize(new Dimension(100, 20));
					unitsCombo.addItem("Metric");
					unitsCombo.addItem("Imperial");
					addClassificationData( classifyPanel, unitsCombo.getSelectedIndex() );
					pane.setVisible(true);
					tBtnClassify.setToolTipText(reg.getString( "COLLAPSETOOLTIP.TXT" ) );
					tBtnClassify.setIcon(collapseIcon);
					NOVEnDispositionTabPanel_v2.this.revalidate();
					
					tBtnClassify.addItemListener( new ItemListener() 
					{
						public void itemStateChanged( ItemEvent ie ) 
						{
							if( ie.getStateChange()== ItemEvent.DESELECTED )
							{
								pane.setVisible( false );
								tBtnClassify.setToolTipText( reg.getString( "EXPANDTOOLTIP.TXT"  ) );
								tBtnClassify.setIcon( expandIcon );
								NOVEnDispositionTabPanel_v2.this.revalidate();
							}
							else
					{
								addClassificationData( classifyPanel, unitsCombo.getSelectedIndex() );
								pane.setVisible(true);
								tBtnClassify.setToolTipText(reg.getString( "COLLAPSETOOLTIP.TXT" ) );
								tBtnClassify.setIcon( collapseIcon );
								NOVEnDispositionTabPanel_v2.this.revalidate();
							}
						}
					});
					unitsCombo.addActionListener(new ActionListener() {
						
						public void actionPerformed(ActionEvent e) {
							addClassificationData( classifyPanel, unitsCombo.getSelectedIndex() );
							NOVEnDispositionTabPanel_v2.this.revalidate();
						}
					});
					
					JPanel showClsPanel = new JPanel( new PropertyLayout( 10 , 5 , 5 , 5 , 5 , 5 ) );
					showClsPanel.add( "1.1.left.top", tBtnClassify );
					showClsPanel.add( "1.2.left.top", pane );
					showClsPanel.add( "1.3.left.top", lblUnits );
					showClsPanel.add( "1.4.right.top", unitsCombo );
					classificationPanel.add( "2.1.left.top", showClsPanel );
				}
				else
				{
					classificationTxt = reg.getString( "CLASSIFICATION.LABEL" ) + " : " + targetItemRev.getProperty( "object_string" ) +
						" " + reg.getString( "NOTCLASSIFIED.LABEL" );
				}
			} 
			catch( TCException tce )
			{
				tce.printStackTrace();
			}
			lblClassified.setText( classificationTxt );
		}

		viewBOMBtn =  new JButton( reg.getString( "VIEWBOMBUTTON.LABEL" ) );
		viewBOMBtn.addActionListener( new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				enPanel.saveForm();
				//ApplicationDef appDef = AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey(reg.getString("PSEApps.APPS"));
				//appDef.openApplication(new InterfaceAIFComponent[]{ targetItemRev } );
				IOpenService openPerspective = PerspectiveDefHelper.getOpenService(reg.getString("PSEApps.APPS"));
                openPerspective.open(new InterfaceAIFComponent[]{targetItemRev});//TC10.1 Upgrade
			}
		});

		JLabel bomLabel = new JLabel(reg.getString( "BOM.LABEL" ) );
		bomLabel.setFont( new Font( "Dialog",java.awt.Font.BOLD,12 ) );
		JLabel blankBomLabel = new JLabel();
		blankBomLabel.setPreferredSize( new Dimension( 350, 20 ) );
		
		JPanel bomHeadingPanel = new JPanel( new PropertyLayout( 5 , 5 , 5 , 5 , 5 , 5 ) );
		bomHeadingPanel.add( "1.1.left.top", bomLabel );
		bomHeadingPanel.add( "1.2.left.top", blankBomLabel );
		bomHeadingPanel.add( "1.3.left.top", viewBOMBtn );
		bomPanel.add( "1.1.left.top", bomHeadingPanel );
		
		if( hasBOM )
		{
			final JToggleButton tBtnBOM = new JToggleButton( expandIcon );
			tBtnBOM.setPreferredSize( new Dimension( 20,20 ) );
			tBtnBOM.setToolTipText(reg.getString( "EXPANDTOOLTIP.TXT" ) );
			
			JPanel showBOMPanel = new JPanel( new PropertyLayout( 5 , 5 , 5 , 5 , 5 , 5 ) );
			showBOMPanel.add( "1.1.left.top", tBtnBOM );
			
			final JScrollPane sctblIntendedBOM = new JScrollPane( bomTable );
			bomTable.setAutoResizeMode( JTable.AUTO_RESIZE_ALL_COLUMNS );
			sctblIntendedBOM.setVisible( false );
			sctblIntendedBOM.setPreferredSize( new Dimension( 500, 120 ) );
			
			tBtnBOM.addItemListener( new ItemListener() 
			{
				public void itemStateChanged( ItemEvent ie ) 
				{
					if( ie.getStateChange()== ItemEvent.DESELECTED )
					{
						tBtnBOM.setToolTipText( reg.getString( "EXPANDTOOLTIP.TXT"  ) );
						tBtnBOM.setIcon( expandIcon );
						sctblIntendedBOM.setVisible( false );
						NOVEnDispositionTabPanel_v2.this.revalidate();
					}
					else
					{
						if (!isBOMCompared) 
						{
							bomInfoPanel.compareBOM( true );
							isBOMCompared = true;
						}
						tBtnBOM.setToolTipText(reg.getString( "COLLAPSETOOLTIP.TXT" ) );
						tBtnBOM.setIcon( collapseIcon );
						bomTable.setAutoResizeMode( JTable.AUTO_RESIZE_ALL_COLUMNS );
						sctblIntendedBOM.setVisible( true );
						NOVEnDispositionTabPanel_v2.this.revalidate();
					}
				}
			});
			
			showBOMPanel.add( "1.2.left.top", sctblIntendedBOM );
			bomPanel.add( "2.1.left.top", showBOMPanel );	
		}
		
		changePanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
		
		lblChageDesc.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		
		changePanel.add("1.1.left.top",lblChageDesc);
		
			try {
				if( this.targetItemRev.getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE ")))
				{
					addOnlyChangePanel = true;
					mainPanel.setPreferredSize(new Dimension(600, 180));
					mainPanel.setLayout(new PropertyLayout(1 , 2 , 1 , 1 , 1 , 1));
				}
				else
					mainPanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
			} catch (TCException e) {
				e.printStackTrace();
			}
		
		if(newRelease)
		{
			mainPanel.add("1.1.left.top", changePanel);
			if(!addOnlyChangePanel)
			{
				if(!misGroup)
				{
					mainPanel.add( "2.1.left.top", classificationPanel );
					mainPanel.add( "3.1.left.top", bomPanel );
				}
			}
		}
		else
		{
			mainPanel.add("1.1.left.top", changePanel);
			
			if(!addOnlyChangePanel)
			{
				mainPanel.add("2.1.left.top", middlePanel);
		
				// @Mohanar
				mainPanel.add("3.1.left", copyDispPanel);

				if (!misGroup) {
					mainPanel.add( "4.1.left.top", classificationPanel );
					mainPanel.add("5.1.left.top", bomPanel);
				}
			}
		}

		JScrollPane mainPSc = new JScrollPane(mainPanel);
		this.add(mainPSc);
		loadData();
		
		if( !newRelease && isRelased )
		{
			inProcesslov.setEnabled( false );
			inInventorylov.setEnabled( false );
			assembledlov.setEnabled( false );
			inFieldlov.setEnabled( false );
			txtDispInstrux.setEnabled( false );
			copyDisposition.setEnabled( false );
		}
	}
	
	public String[] getDispValues( String futureStatus )
	{
		String inProcess = inProcesslov.getText();
		String inInventory = inInventorylov.getText();
		String assembled = assembledlov.getText();
		String infield = inFieldlov .getText();
		String dispInstruction = txtDispInstrux.getText();
		//usha
		try {
			String currStatus	= dispcomp.getStringProperty("currentstatus");
			if(currStatus == null)
				futureStatus = "";
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] dispDataArr = new String[]{ dispInstruction, inProcess, inInventory, assembled, infield, futureStatus }; 
		return dispDataArr;
	}
	
	public void copyDispDataToAllParts(_EngNoticeForm_Panel_v2 enPanel, String futureStatus )
	{
		this.enPanel = enPanel;
		String dispPropArr[] = new String[]{ "dispinstruction", "inprocess", "ininventory", "assembled", "infield", "futurestatus" };
		try{
				Vector<TCComponent> targetsVec = enPanel.getEnTargets(); 
				for( int j=0; j < targetsVec.size(); ++j )
				{
					boolean isDocRev = isDocRevision(  targetsVec.get( j ) );
					if( !isDocRev )
					{	
						referncedispdata[j].setProperties( dispPropArr, getDispValues( futureStatus ) );
				    }
				}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}	

	public boolean isDocRevision(TCComponent targetItemRev) {
		boolean flag = false;
		try{
		if(targetItemRev.getProperty("object_type").toString().equalsIgnoreCase("Documents Revision"))
			flag = true;
		else
			flag = false;
		}catch(TCException exc){
			exc.printStackTrace();
		}
		return flag;
		
	}
	public static String getClsPropValue(TCSession session, TCComponentICO icoObj, String propertyName, String languageCode) throws Exception{
		
		   DataManagementService dmService = DataManagementService.getService(session);

        com.teamcenter.services.rac.core._2010_04.DataManagement.PropertyInfo NamePropertyInfo[] = new com.teamcenter.services.rac.core._2010_04.DataManagement.PropertyInfo[1];
	       NamePropertyInfo[0] = new com.teamcenter.services.rac.core._2010_04.DataManagement.PropertyInfo();
	       NamePropertyInfo[0].object = icoObj;
	       NamePropertyInfo[0].propsToget = new com.teamcenter.services.rac.core._2010_04.DataManagement.NameLocaleStruct[1];
	       NamePropertyInfo[0].propsToget[0] = new com.teamcenter.services.rac.core._2010_04.DataManagement.NameLocaleStruct();
	       NamePropertyInfo[0].propsToget[0].name = propertyName;
	       NamePropertyInfo[0].propsToget[0].locales =new String[]{languageCode};
	                    
	       LocalizedPropertyValuesList response = dmService.getLocalizedProperties(NamePropertyInfo);
	       ServiceData sd = response.partialErrors;
	       if (sd.sizeOfPartialErrors()>0){
	    	   throw new Exception("Fail to getLocalizedProperties using SOA API :");
	       }
	       com.teamcenter.services.rac.core._2010_04.DataManagement.LocalizedPropertyValuesInfo propValueInfo = response.output[0];
	       com.teamcenter.services.rac.core._2010_04.DataManagement.NameValueLocaleStruct propertyValue = propValueInfo.propertyValues[0];
	       String value = propertyValue.values[0];
	       return value;
	}
	public void addClassificationData( JPanel classifyPanel, int selectedUnit )
	{
		try 
		{
			// Get classification attributes from the revision object
			classifyPanel.removeAll();
			Map<String, String> classAttrs = new HashMap<String,String>();
			int baseUnitSystem = 0;
			
			TCComponentICO[] icoObjs = ( ( TCComponentItemRevision )targetItemRev ).getClassificationObjects();
			if (icoObjs == null || icoObjs.length<1){
				return;
			}
			TCComponentICO icoObj = icoObjs[0];
			ICSProperty[] icsProperties = icoObj.getICSProperties(true);
    		TCProperty[] tcproperties = icoObj.getAllTCProperties();
			int arrlen = tcproperties.length;
			for (int i = 0; i < arrlen; i++) {
				if(tcproperties[i].toString().equalsIgnoreCase(reg.getString("metric.NAME"))){
					baseUnitSystem = 0;
					break;
				}
				else if(tcproperties[i].toString().equalsIgnoreCase(reg.getString("nonmetric.NAME"))){
					baseUnitSystem = 1;
					break;
				}
			}
			
			// get attribute values
			ICSPropertyDescription[] icsPropDesc = icoObj.getICSPropertyDescriptors();

			Map<String, String> attrIDToValueMap = new HashMap<String,String>();
			for (ICSProperty icsProperty: icsProperties){
				attrIDToValueMap.put(icsProperty.getId()+"",icsProperty.getValue());
			}
			
			for (int i=0;i<icsPropDesc.length;i++) {
				try {
					icsPropDesc[i].setActiveUnitsystem(selectedUnit);
					String attrDatatype = icsPropDesc[i].getFormat().getDisplayString();
					//boolean  isMandatory = cp[i].isMandatory();
					String attrDefaultAnnotation = icsPropDesc[i].getAnnotation();
					ICSKeyLov lov = icsPropDesc[i].getFormat().getKeyLov();
					String attrName = icsPropDesc[i].getName();
					String attrID = icsPropDesc[i].getId()+"";
					String key = (String)attrIDToValueMap.get(attrID);
					String attrValue = "";
					
					if (attrName.equalsIgnoreCase("SHORT NAME"))
						continue;
									
					if (lov != null) {
						attrValue = lov.getValueOfKey(key);
					}
					else {
						if(baseUnitSystem == selectedUnit)
						{
							attrValue = (String)attrIDToValueMap.get(attrID);
						}
						else
						{
							ICSProperty[] icsproperty = classificationService.convertAttributeValue(attrID, icsPropDesc[i].getAltUnit(), key, icsPropDesc[i].getUnit(), icsPropDesc[i].getFormat().getFormat());
							attrValue = icsproperty[0].getValue();
						}
						if (attrDatatype.contains("INTEGER") ||attrDatatype.contains("REAL") || attrDatatype.contains("DOUBLE")) {
							while (attrValue != null && attrValue.endsWith("0") && attrValue.contains("."))
								attrValue = attrValue.substring(0,attrValue.length()-1);
							if (attrValue != null && (attrValue.length() > 0) && attrValue.charAt(attrValue.length()-1) == '.')
								attrValue = attrValue.substring(0,attrValue.length()-1);
							while (attrValue != null && attrValue.startsWith("0", 0)  && attrValue.length() > 1) 
								attrValue = attrValue.substring(1);
						}
					}
					if (attrValue == null) {
						attrValue = "";
					}
					
					if (attrDefaultAnnotation != null && !attrDefaultAnnotation.equals("") && attrValue != null && attrValue.length() > 0 )
    				{
    					attrValue = attrDefaultAnnotation+" "+attrValue;
    				}
										
					System.out.println("attrName="+attrName);
					System.out.println("attrValue="+attrValue);

					classAttrs.put(attrName, attrValue);

//					if(isMandatory)
//					{
//						String val = "<html><b><font=8>" + attrName + " = " + attrValue + "</font><font=8 COLOR=#FF0000>"+" *"+"</font></b>";
//						classifyPanel.add( new JLabel( val ) );
//					}
//					else
//					{
//						String val = "<html><b><font=8>" + attrName + " = " + attrValue + "</font></b>";
//						classifyPanel.add( new JLabel( val ) );
//					}
				}
				catch (Exception e) {
					System.out.println("Error getting technical attributes: "+e);
				}
			}
			
			// add values
			Set<String> keySet = classAttrs.keySet();
			Object[] keyObjs =  keySet.toArray();
			
			/// First add only attributes which has values
			for (int i = 0; i < keyObjs.length; i++) 
			{
				Object valObj = classAttrs.get(keyObjs[i]);
				if( valObj.toString().trim().length() > 0 )
				{
					String val = reg.getString( "XMLFont.TXT" ) + keyObjs[i]+" = " + valObj+reg.getString( "XMLEndFont.TXT" );
					classifyPanel.add( new JLabel( val ) );
				}
			}
			/// Then add attributes which has no values.
			for (int i = 0; i < keyObjs.length; i++) 
			{
				Object valObj = classAttrs.get(keyObjs[i]);
				if( valObj.toString().trim().length() <= 0 )
				{
					String val = reg.getString( "XMLFont.TXT" ) + keyObjs[i] + " = " + valObj + reg.getString( "XMLEndFont.TXT" );
					classifyPanel.add( new JLabel( val ) );
				}
			}

		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}

	public void setTargetItemRev(TCComponent targetItemRevision)
	{
		targetItemRev = targetItemRevision;
	}

	public boolean isMandatoryFieldsFilled()
	{
		return true;
	}
	
	public void loadData()
	{		
		String enOwnGrp = null;
		try 
		{
			enOwnGrp = enPanel.Enform.getProperty("owning_group");

			if( !newRelease )
			{
				txtDispInstrux.setText(paneldata.getStrdispInstruction());
			}
			
			String sDefaultValue = null;
			String sLOVName = null;
			
			if(enOwnGrp.contains(reg.getString( "PCG.Group" )))
			{		
				sLOVName = reg.getString( "ENDisposition_PCG.LOV" );
				sDefaultValue = reg.getString( "PCGDefault.STATUS" );					
			}
			else
			{	
				sLOVName = reg.getString( "ENDisposition.LOV" );								
				sDefaultValue = reg.getString( "NA.STATUS" );				
			}	
			TCComponentListOfValues lovalues = TCComponentListOfValuesType.findLOVByName(session,sLOVName);
			inProcesslov.setLovComponent(lovalues);	
			inFieldlov.setLovComponent(lovalues);
			inInventorylov.setLovComponent(lovalues);
			assembledlov.setLovComponent(lovalues);
			
			setDispositiondata(sDefaultValue);
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
	}
	private void setDispositiondata(String sDeafultValue)
	{
		String curInProcess = paneldata.getStrinProcess();
		String curInField = paneldata.getStrinfield();
		String curInInventory = paneldata.getStrinInventory();
		String curpurchased = paneldata.getStrassembled();
		
		if( (curInProcess == null) || (curInProcess.trim().length() <=0))
		{
			curInProcess = sDeafultValue;		
		}		

		if( (curInField == null) || (curInField.trim().length() <=0))
		{
			curInField = sDeafultValue;		
		}	

		if( (curInInventory == null) || (curInInventory.trim().length() <=0))
		{
			curInInventory = sDeafultValue;		
		}

		if( (curpurchased == null) || (curpurchased.trim().length() <=0))
		{
			curpurchased = sDeafultValue;		
		}
		
		inProcesslov.setText( curInProcess);
		inFieldlov.setText( curInField);
		inInventorylov.setText( curInInventory);
		assembledlov.setText(curpurchased);
	}
	public void saveData( String futureStatus )
	{
		try
		{
			//usha
			if(dispcomp.getStringProperty("currentstatus") == null)
				futureStatus = "";
			if(newRelease)
			{
				String[] strReqdPropArr = new String[]{ "futurestatus" };
				String[] strValueArr = new String[]{ futureStatus};
				paneldata.setDispClassData( strReqdPropArr, strValueArr );
			}else
			{
				String inProcess=inProcesslov.getText();
				String inInventory=inInventorylov.getText();
				String assembled=assembledlov.getText();
				String infield=inFieldlov .getText();
				String dispInstruction=txtDispInstrux.getText();
				/*paneldata.setDispClassData( chnageReason, inProcess, inInventory, assembled, infield,
						dispInstruction, futureStatus );*/
				paneldata.setDispClassData( inProcess, inInventory, assembled, infield,
						dispInstruction, futureStatus );
			}
		}
		catch(Exception te)
		{
			System.out.println(te);
		}
	}

	public void saveFutureStatus( String futureStatus )
	{
		try
		{
			//usha
			if(dispcomp.getStringProperty("currentstatus") == null)
				futureStatus = "";
			if( newRelease )
			{
				String[] strReqdPropArr = new String[]{ "futurestatus" };
				String[] strValueArr = new String[]{ futureStatus };	
				paneldata.setDispClassData( strReqdPropArr, strValueArr );
			}
		}
		catch(Exception te)
		{
			System.out.println(te);
		}
	}

	public void createBOMPanel( TCComponent revcomp )
	{
		if(revcomp!=null)
		{
			try
			{			
				AIFComponentContext[] itemRevisionComponents = ( ( TCComponentItemRevision )revcomp ).getChildren();
				for( int i = 0; i < itemRevisionComponents.length; ++i )
				{
					if( itemRevisionComponents[i].getComponent() instanceof TCComponentBOMViewRevision ) 
					{					
						hasBOM = true;
						break;
					}
				}	
				if( hasBOM )
				{
					//get the previous Revision;
					AIFComponentContext[] revItems = ( ( TCComponentItemRevision ) revcomp ).getItem().getChildren( "revision_list" );
					TCComponentItemRevision prevrev = null;
					if( revItems.length > 1 )
					{
						prevrev = getPreiviousRev( revItems, ( TCComponentItemRevision )revcomp, "date_released" );
					}
					bomInfoPanel = new NOVEnBomInfoPanel( prevrev, (TCComponentItemRevision) revcomp );
					//bomInfoPanel.compareBOM( true );
					bomInfoPanel.setVisible( false );
					bomTable=bomInfoPanel.bomInfoTable;
				}
			}
			catch(TCException tc)
			{
				System.out.println(tc);
			}
		}
	}

	public TCComponentItemRevision getPreiviousRev( AIFComponentContext[] allrevs,
			TCComponentItemRevision curRev, String strDateProperty )
	{
		TCComponentItemRevision prevRev = null;
		Date prevDate = null;
		try
		{
			// If current revision is released.
			Date curRevDate = curRev.getDateProperty( strDateProperty );
			if( curRevDate == null )
			{
				/// If current revision is not released.
				curRevDate = curRev.getDateProperty( "creation_date" );
			}
			
			prevRev = ( TCComponentItemRevision )allrevs[0].getComponent();	
			prevDate = prevRev.getDateProperty( strDateProperty );
			for( int index = 1; index < allrevs.length; ++index )
			{
				Date compareRelDate = ( ( TCComponentItemRevision )allrevs[index].getComponent() ).getDateProperty( strDateProperty );
				
				if( ( compareRelDate != null ) && ( compareRelDate.before( curRevDate ) ) && 
					( prevDate != null ) && ( compareRelDate.after( prevDate ) ) )
				{
					prevDate = compareRelDate;
					prevRev=( TCComponentItemRevision )allrevs[index].getComponent();
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		
		return ( ( prevDate != null ) && ( curRev != prevRev ) ? prevRev : null );
	}

//	public TCComponentItemRevision getPreiviousRev( AIFComponentContext[] allrevs,
//			TCComponentItemRevision curRev, String strDateProperty )
//	{
//		TCComponentItemRevision prevRev = null;
//		try
//		{
//			Date curRevRelDate = curRev.getDateProperty( strDateProperty );
//			prevRev = ( TCComponentItemRevision )allrevs[0].getComponent();	
//			Date prevDate = prevRev.getDateProperty( strDateProperty );
//			for( int index = 1; index < allrevs.length; ++index )
//			{
//				Date compareRelDate = ( ( TCComponentItemRevision )allrevs[index].getComponent() ).getDateProperty( strDateProperty );
//				if( ( compareRelDate.before( curRevRelDate ) ) && ( compareRelDate.after( prevDate ) ) )
//				{
//					prevDate = compareRelDate;
//					prevRev=( TCComponentItemRevision )allrevs[index].getComponent();
//				}
//			}
//		}
//		catch(TCException tc)
//		{
//			System.out.println(tc);
//		}
//		return prevRev;
//	}

	class NOVENDispositionPanelData
	{		
		//TCProperty chnageReason;
		//TCProperty itemId;
		TCProperty inProcess;
		TCProperty inInventory;
		TCProperty assembled;
		TCProperty infield;
		TCProperty dispInstruction;
		TCProperty futureStatus;
		TCProperty currentStatus;
		TCProperty relatedEcrs;
		

		String strinProcess;
		String strinInventory;
		String strassembled;
		String strinfield;
		String strdispInstruction;
		String stritemId;
		String strfutureStatus;
		String strcurrentStatus;
		//String[] strRelatedEcrs;
		//TCComponent targetRev;
		//TCComponent dispcomp;

		public NOVENDispositionPanelData(TCComponent dispcomp, boolean newRelease) {			

			try
			{
				String dispProps[] = dispcomp.getProperties(new String[]{"currentstatus", "futurestatus"});
				dispProps = dispcomp.getProperties(new String[]{"currentstatus", "futurestatus",
							                                        "inprocess", "ininventory", "assembled", "infield",
							                                        "dispinstruction" });
				
				strinProcess=dispProps[2];
				strinInventory=dispProps[3];;
				strassembled=dispProps[4];
				strinfield=dispProps[5];
				strdispInstruction=dispProps[6];
				
				
				strcurrentStatus=dispProps[0];
				strfutureStatus=dispProps[1];
				
			}
			catch ( TCException tcEx )
			{
				tcEx.printStackTrace();
			}
		}

		public String getStrcurrentStatus() {
			return strcurrentStatus;
		}

		/*public String getStrchnageReason() {
			return strchnageReason;
		}*/

		public String getStrinProcess() {
			return strinProcess;
		}

		public String getStrinInventory() {
			return strinInventory;
		}

		public String getStrassembled() {
			return strassembled;
		}

		public String getStrinfield() {
			return strinfield;
		}

		public String getStrdispInstruction() {
			return strdispInstruction;
		}	

		public String getStrfutureStatus() {
			return strfutureStatus;
		}	

		public void setDispClassData(String strinProcess, String strinInventory,
				String strassembled, String strinfield,
				String strdispInstruction,
				String strfutureStatus) {

			try
			{
				
				Object inputObjs[] = new Object[3];
				inputObjs[0] = new TCComponent[]{dispcomp};
				inputObjs[1] = new String[]{"inprocess", "ininventory",
						                    "assembled", "infield", "dispinstruction",
						                    "futurestatus"};
				inputObjs[2] = new String[]{strinProcess, strinInventory, 
						                    strassembled, strinfield, strdispInstruction,
						                    strfutureStatus};
				
				TCUserService usrService = session.getUserService();
				int returnVal = (Integer)usrService.call("NATOIL_update_pom_string_props", inputObjs );
				if ( returnVal != 0 )
				{
					throw new Exception("Could not update target disposition data");
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		public void setDispClassData( String[] reqdPropArr, String[] strValueArr  )
			{
			try
				{
				
				Object inputObjs[] = new Object[3];
				inputObjs[0] = new TCComponent[]{dispcomp};
				inputObjs[1] = reqdPropArr;
				inputObjs[2] = strValueArr;
				TCUserService usrService = session.getUserService();
				int returnVal = (Integer)usrService.call("NATOIL_update_pom_string_props", inputObjs );
				if ( returnVal != 0 )
				{
					throw new Exception("Could not update target disposition data");
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}
}




