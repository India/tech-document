/*
 * Created on Sep 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.noi.rac.en.NationalOilwell;
import com.teamcenter.rac.util.DateButton;

/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOVDateButton extends DateButton {

	/**
	 * 
	 */
	public NOVDateButton() {
		super();
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
		setBorder(new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.LOWERED));
	}

	/**
	 * @param arg0
	 */
	public NOVDateButton(Date arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
		setBorder(new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.LOWERED));
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public NOVDateButton(Date arg0, String arg1, boolean arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
		setBorder(new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.LOWERED));
	}

	/**
	 * @param arg0
	 */
	public NOVDateButton(SimpleDateFormat arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
		setBorder(new javax.swing.border.EtchedBorder(javax.swing.border.EtchedBorder.LOWERED));
	}

}
