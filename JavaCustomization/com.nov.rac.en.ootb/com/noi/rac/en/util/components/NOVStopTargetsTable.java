package com.noi.rac.en.util.components;

import java.util.HashMap;
import java.util.Vector;

import com.noi.rac.en.form.compound.panels._StopForm_Panel;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NOVStopTargetsTable extends TCTable{

	private _StopForm_Panel stopformPanel;
	public NOVStopTargetsTable(_StopForm_Panel stopformPanel, TCSession session, String[] columNames)
	{
		super();
		setSession(session);
		AIFTableModel tableModel = new AIFTableModel(columNames);
		setModel(tableModel);
		this.stopformPanel=stopformPanel;
		loadTabledata(stopformPanel.getEnTargets(),stopformPanel.getStopCurrentStatus());
	}

	public void loadTabledata(Vector<TCComponent> TargetstoBAdded,HashMap<TCComponent, String> TargetcurrentStatMap)
	{
		//get the targets and load the table
		try
		{			
			Vector<TCComponent> enTargets=TargetstoBAdded;
			TCComponent[] dispData=stopformPanel.getDispData();
			
			for(int i=0;i<enTargets.size();i++)
			{
				Vector<Object> obj=new Vector<Object>();
				
				//ItemID
				TCComponent comp=enTargets.get(i);
				String itemId=comp.getProperty("item_id")==null?"":comp.getProperty("item_id");
				obj.add(itemId);

				//Item Name
				String itemName=comp.getProperty("object_name")==null?"":comp.getProperty("object_name");
				obj.add(itemName);
			
				//future status
				String status=dispData[i].getTCProperty("futurestatus").toString();
				obj.add(status);
				
				
				this.dataModel.addRow(createLine(obj));
			}
		}
		catch(TCException tc)
		{

		}

	}

	public void saveTable()
	{

	}
	public boolean isCellEditable(int row , int col )
	{
		return false;
	}	

}



