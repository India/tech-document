package com.noi.rac.en.util.components;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.util.ArraySorter;
import com.teamcenter.rac.util.Registry;

/*import com.ugsolutions.iman.kernel.IMANComponent;
 import com.ugsolutions.iman.kernel.IMANComponentDataset;
 import com.ugsolutions.util.ArraySorter;*/

public class ExternalAttachmentsTableModel extends AbstractTableModel {

	private Object[] references;
	private Object[][] data;
	private Vector extAttVector = new Vector();
	private Vector sorted = new Vector();
	Registry reg;
	private String[] columnNames ;

	public ExternalAttachmentsTableModel(Vector theData) {
		super();
		reg= Registry.getRegistry(this);
		columnNames = new String[]{ reg.getString("type.STR"), reg.getString("name.STR") };
		initData(theData);
	}

	private void initData(Vector theData) {

		TCComponent[] forSort = new TCComponent[theData.size()];		
		
		if (theData.size() > 0) {
			for (int i = 0; i < theData.size(); i++)
				forSort[i] = (TCComponent) theData.get(i);

			ArraySorter.sort(forSort);
			for (int index = 0; index < forSort.length; index++)
				sorted.addElement(forSort[index]);
			references = new Object[sorted.size()];
			data = new Object[sorted.size()][3];

			for (int i = 0; i < sorted.size(); i++) {

				extAttVector.add(sorted.get(i));
				try {
					TCComponentDataset ds = ((TCComponentDataset) sorted.get(i));
					String type = ds.getProperty(reg.getString("objType.PROP"));
					String name = ds.getProperty(reg.getString("objName.PROP"));
					data[i][0] = type;
					data[i][1] = name;
				} catch (Exception e) {
					System.out.println("Could not get table info: " + e);
				}
			}
		} else {
			sorted = new Vector();
			data = null;
		}

	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data == null ? 0 : data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public TCComponent[] getSelectedData() {
		TCComponent[] arr = new TCComponent[extAttVector.size()];
		for (int i = 0; i < extAttVector.size(); i++)
			arr[i] = (TCComponent) extAttVector.get(i);
		return arr;
	}

	public TCComponentDataset getAttachment(int index) {
		return ((TCComponentDataset) sorted.get(index));
	}

	public void addRows(Vector items) {

		Vector newV;

		newV = (Vector) extAttVector.clone();
		for (int i = 0; i < items.size(); i++)
			newV.add(items.get(i));

		extAttVector.removeAllElements();
		initData(newV);
	}

	public void removeRow(int row) {

		Vector newV = (Vector) extAttVector.clone();
		newV.remove(row);
		extAttVector.removeAllElements();
		initData(newV);
	}

}
