package com.noi.rac.en.util.components;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import com.teamcenter.rac.util.Registry;
public class YesNoDialog extends JDialog implements ActionListener {

	int result=-1;
	JPanel thePanel = new JPanel();
	JLabel label = null;
	JButton yes = null;
	JButton no = null;
	public static int YES = 1;
	public static int NO = 0;
	private Registry reg;
	public YesNoDialog(String msg) {
		
		label = new JLabel(msg);
		thePanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.BOTH;
		thePanel.add(label,gbc);
		reg = Registry.getRegistry(this);
		
		yes = new JButton(reg.getString("Yes.BUTN"));
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.LINE_END;
		thePanel.add(yes,gbc);

		no = new JButton(reg.getString("No.BUTN"));
		gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.LINE_START;
		thePanel.add(no,gbc);
		
	}
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == yes)
			result = 1;
		else
			result = 0;
		
		this.setVisible(false);
	}
	public int getResult() {
		return result;
	}
	
}
