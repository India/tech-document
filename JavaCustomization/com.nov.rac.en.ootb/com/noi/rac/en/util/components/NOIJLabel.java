/*
 * Created on Apr 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOIJLabel extends JLabel {
    
    private static Font labelFont = new Font("Dialog",java.awt.Font.BOLD,12);
    
    public NOIJLabel() { super(); setFont(labelFont); }
    public NOIJLabel(String s) { super(s); setFont(labelFont); }
    public NOIJLabel(String s, Icon icon, int i){super(s, icon, i);setFont(labelFont);}

}
