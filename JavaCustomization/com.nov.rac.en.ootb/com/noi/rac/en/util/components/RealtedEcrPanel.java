
package com.noi.rac.en.util.components;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.noi.rac.en.util.Miscellaneous;
import com.noi.rac.en.util.NovSearchableList;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;


public class RealtedEcrPanel extends JPanel implements ActionListener, FocusListener{

 	/**
	 * 
	 */
    public static final Cursor busyCursor = new Cursor(Cursor.WAIT_CURSOR);
    public static final Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);
	private Registry reg;
	JList sourceList;
	JTextField  searchField;
	JButton searchProjectButton;
    JScrollPane sourceListScroll;
    JList targetList;
    JScrollPane targetListScroll;
    
    JButton addTarget;
    JButton removeTarget;

    Hashtable stringToRef = null;
    Hashtable refToString = null;
    
	
    Dimension listSize;
    private int  selectedIndex = -1;
    /**
     * 
     */
    public RealtedEcrPanel() {
        super();
        reg = Registry.getRegistry(this);
        init(new java.awt.Dimension(250,100));
    }
    public void actionPerformed(ActionEvent e) {
    	// TODO Auto-generated method stub
    	
    }

    public void init(Dimension size) {       
        listSize = size;
        sourceList = new NovSearchableList();
        sourceListScroll = new JScrollPane(sourceList);
        targetList = new JList();
        targetListScroll = new JScrollPane(targetList);
        initUI();        
    }
  

	
	public void setEnabled(boolean enabled) {
    	sourceList.setEnabled(enabled);
    	targetList.setEnabled(enabled);
    	addTarget.setEnabled(enabled);
    	removeTarget.setEnabled(enabled);
    }
    

	public Dimension getSize() { return listSize; }
    
  
    public void setSelectedTextValues(String[] list) {
        
        
    }
   
    
    public void initUI() {       
    	
    	
    	sourceList.addFocusListener(this);
        GridBagConstraints gbc;
        GridBagLayout gbl = new GridBagLayout();
        
        gbl.columnWidths = new int[] { listSize.width, 64, listSize.width };
        gbl.rowHeights = new int[] { 17, listSize.height-17 };
        
        setLayout(gbl);
		//setBackground(new java.awt.Color(225,225,225));
		TitledBorder tb = new javax.swing.border.TitledBorder(reg.getString("RelatedECRs.LABLE"));
		tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
		setBorder(tb);
        
        gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        add(searchfieldAndAvailablePanel(),gbc);
        gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = 2;
        gbc.insets = new Insets(0,0,2,0);
        add(new NOIJLabel(reg.getString("Selected.LABLE")),gbc);
        
        sourceListScroll.setPreferredSize(new Dimension(listSize.width,listSize.height-17));
        sourceListScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        sourceListScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        gbc = new GridBagConstraints();
        gbc.gridy = 1;
        add(sourceListScroll,gbc);
        
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        //buttonPanel.setBackground(new java.awt.Color(225,225,225));
        buttonPanel.setPreferredSize(new Dimension(64,listSize.height-17));
        ImageIcon plusIcon  =  reg.getImageIcon("enadd.IMG");		
		ImageIcon minusIcon =  reg.getImageIcon("enremove.IMG");  
        addTarget = new JButton(plusIcon);
        addTarget.setPreferredSize(new Dimension(45,21));
        addTarget.addActionListener(this);
        
        
        removeTarget = new JButton(minusIcon);
        removeTarget.setPreferredSize(new Dimension(45,21));
        removeTarget.addActionListener(this);        
        gbc = new GridBagConstraints();
        buttonPanel.add(addTarget,gbc);
        gbc = new GridBagConstraints();
        gbc.gridy = 1;
        gbc.insets = new Insets(1,0,0,0);
        buttonPanel.add(removeTarget,gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(buttonPanel,gbc);
        
        targetListScroll.setPreferredSize(new Dimension(listSize.width,listSize.height-17));
        targetListScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        targetListScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        add(targetListScroll,gbc);
        
    }
    
    /**
	 * @return
	 */
	private JPanel searchfieldAndAvailablePanel() {
		//Registry registry = Registry.getRegistry("com.noi.rac.form.form_locale");
		
		JPanel schAvailPanel = new JPanel(new GridBagLayout());
		//schAvailPanel.setBackground(new java.awt.Color(225,225,225));
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new Insets(0,0,2,0);
		searchField = new JTextField();
		searchField.setText(reg.getString("TypeECRNo.MSG"));
		searchField.setPreferredSize(new Dimension(130,21));
		searchField.addFocusListener(this);
		
		schAvailPanel.add(searchField,gridBagConstraints);
		
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.anchor = GridBagConstraints.NORTHEAST;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.insets = new Insets(0,2,2,0);
		searchProjectButton = new JButton(reg.getString("Search.BUTN"));
		searchProjectButton.setPreferredSize(new Dimension(75,21));
		schAvailPanel.add(searchProjectButton,gridBagConstraints);		
		searchProjectButton.addActionListener(this);
		
		return schAvailPanel;
	}


	public JList getTargetList() {
		return targetList;
	}

	public void setTargetList(JList targetList) {
		this.targetList = targetList;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
	 */
	public void focusGained(FocusEvent e) {
		if(e.getSource().equals(searchField)){
			searchField.setText("");
		}
		else if(e.getSource().equals(sourceList)){
			selectedIndex = sourceList.getSelectedIndex();
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
	 */
	public void focusLost(FocusEvent e) {
	}
	
}
