package com.noi.rac.en.util.components;


import java.util.HashMap;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.noi.rac.en.form.compound.panels.Nov4_SupersedeForm_Panel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.mission.helper.NOVMissionHelper;
import com.noi.rac.en.util.components.NOVECNTargetsTable.NOVEnTreeTableModel;
import com.noi.rac.en.util.components.NOVECNTargetsTable.NovTreeTableNode;
import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.Registry;

public class NOVEnDispositionPanel extends JPanel {
	private _EngNoticeForm_Panel enPanel;
	private Nov4_SupersedeForm_Panel ssPanel;
	private Vector<TCComponent> targets;		
	public TCComponent[] referncedispdata;	
	public HashMap<TCComponent, TCComponent> targetdispMap=new HashMap<TCComponent, TCComponent>();
	public HashMap<TCComponent, TCComponent> disptargetMap=new HashMap<TCComponent, TCComponent>();
	public HashMap<TCComponent, String> targetStatusMap=new HashMap<TCComponent, String>();
    private Registry reg;

	public NOVEnDispositionPanel(TCComponent[] referncedispdata)
	{		
		this.referncedispdata=referncedispdata;
		this.reg=Registry.getRegistry(this);
	}

	public void createUI()
	{	
		try
		{			
			createDispTabPanels(referncedispdata);
			add(enPanel.enautomatonTabbePane);


		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void setDispData(_EngNoticeForm_Panel enPanel)
	{
		this.enPanel=enPanel;	
		targets=enPanel.getEnTargets();
		//disposition data initially
		try
		{
			if((referncedispdata.length==0)&&(targets.size()>0)) //Create the Disposition instances for the targets
			{
				String targetIds[];//=new String[targets.size()];	
				String targetRevIds[];
				Vector<String> vtargetIds=new Vector<String>();
				Vector<String> vtargetRevIds=new Vector<String>();
				for(int k=0;k<targets.size();k++)
				{
					//if(!(targets.get(k).getProperty("object_type").equalsIgnoreCase("Documents Revision")))
					//{
					vtargetIds.add(((TCComponentItemRevision)targets.get(k)).getProperty("item_id"));
					vtargetRevIds.add(((TCComponentItemRevision)targets.get(k)).getProperty("current_revision_id"));
					//}
				}  
				/*targetIds=new String[vtargetIds.size()];
				vtargetIds.copyInto(targetIds);

				targetRevIds=new String[vtargetRevIds.size()];
				vtargetRevIds.copyInto(targetRevIds);
				*/
				
				
				System.out.println ( "Inside setDispData(_EngNoticeForm_Panel enPanel)");
				TCComponent[] targetsToProcess = targets.toArray(new TCComponent[targets.size()]);
				referncedispdata=createDispInstances(targetsToProcess);
				//Save the Form
				enPanel.saveDipsDatatoForm();				
				//Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
				for(int k=0;k<targets.size();k++)
				{
					for(int j=0;j<referncedispdata.length;j++)
					{
						referncedispdata[j].refresh();
						String dispObjItemID = referncedispdata[j].getProperties(new String[]{"targetitemid"})[0];
						String itemId=((TCComponentItemRevision)targets.get(k)).getProperty("item_id");
						if(dispObjItemID.equalsIgnoreCase(itemId))
						{
							targetdispMap.put(referncedispdata[j],targets.get(k));
							disptargetMap.put(targets.get(k), referncedispdata[j]);
							//tempReference.remove(j);
							//set the future status of Table data
						}
					}
				}
			}
			else
			{
				//Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
				for(int k=0;k<targets.size();k++)
				{
					for(int j=0;j<referncedispdata.length;j++)
					{
						referncedispdata[j].refresh();
						String dispObjItemID = referncedispdata[j].getProperties(new String[]{"targetitemid"})[0];
						String itemId=((TCComponentItemRevision)targets.get(k)).getProperty("item_id");
						if(dispObjItemID.equalsIgnoreCase(itemId))
						{
							targetdispMap.put(referncedispdata[j],targets.get(k));
							disptargetMap.put(targets.get(k), referncedispdata[j]);
							//tempReference.remove(j);
							//set the future status of Table data
						}

						}
				}
			}

		}
		catch(TCException tc)
		{
			tc.printStackTrace();
		}
	}
	public void setDispData(Nov4_SupersedeForm_Panel enPanel)
	{
		this.ssPanel=enPanel;	
		targets=ssPanel.getEnTargets();
		//disposition data initially
		try
		{
			if((referncedispdata.length==0)&&(targets.size()>0)) //Create the Disposition instances for the targets
			{
				String targetIds[];//=new String[targets.size()];	
				String targetRevIds[];
				Vector<String> vtargetIds=new Vector<String>();
				Vector<String> vtargetRevIds=new Vector<String>();
				for(int k=0;k<targets.size();k++)
				{
					//if(!(targets.get(k).getProperty("object_type").equalsIgnoreCase("Documents Revision")))
					//{
					vtargetIds.add(((TCComponentItem)targets.get(k)).getProperty("item_id"));
					vtargetRevIds.add("");
					//}
				}  
				targetIds=new String[vtargetIds.size()];
				vtargetIds.copyInto(targetIds);

				targetRevIds=new String[vtargetRevIds.size()];
				vtargetRevIds.copyInto(targetRevIds);
				System.out.println ( "Inside setDispData(_EngNoticeForm_Panel enPanel)");
				
				TCComponent[] targetList =  targets.toArray(new TCComponent[targets.size()]);
				referncedispdata=createDispInstances1(targetList);
				//Save the Form
				ssPanel.saveDipsDatatoForm();				
				//Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
				for(int k=0;k<targets.size();k++)
				{
					for(int j=0;j<referncedispdata.length;j++)
					{
						referncedispdata[j].refresh();
						String dispObjItemID = referncedispdata[j].getProperties(new String[]{"targetitemid"})[0];
						String itemId=((TCComponentItem)targets.get(k)).getProperty("item_id");
						if(dispObjItemID.equalsIgnoreCase(itemId))
						{
							targetdispMap.put(referncedispdata[j],targets.get(k));
							disptargetMap.put(targets.get(k), referncedispdata[j]);
							//tempReference.remove(j);
							//set the future status of Table data
						}
					}
				}
			}
			else
			{
				//Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
				for(int k=0;k<targets.size();k++)
				{
					for(int j=0;j<referncedispdata.length;j++)
					{
						referncedispdata[j].refresh();
						String dispObjItemID = referncedispdata[j].getProperties(new String[]{"targetitemid"})[0];
						String itemId=((TCComponentItem)targets.get(k)).getProperty("item_id");
						if(dispObjItemID.equalsIgnoreCase(itemId))
						{
							targetdispMap.put(referncedispdata[j],targets.get(k));
							disptargetMap.put(targets.get(k), referncedispdata[j]);
							//tempReference.remove(j);
							//set the future status of Table data
						}

					}
				}
			}
			//TODO: Nitin Check This			
//			if( oldDispositionPanel == null )
//			{
//				oldDispositionPanel = new NOVEnDispositionTabPanel( enPanel, enPanel.getSession(),
//					null, false );
//			}

		}
		catch(TCException tc)
		{
			tc.printStackTrace();
		}
	}
	public TCComponent[] getDispData()
	{
		return referncedispdata;
	}
	public TCComponent[] createDispInstances(TCComponent[] targetItems)
	{
		TCComponent[] comps=null;
		/*if(itemIds!=null)
		{
			try
			{
				//call userExit to create the Disposition componenets and return the array
				TCUserService us=enPanel.getSession().getUserService();       	
				Object obj[]=new Object[3];       	
				obj[0]=itemIds;
				obj[1]=revIds;
				obj[2] = new Boolean(false);
				comps=(TCComponent[])us.call("NATOIL_createEnDispositionData", obj);
			}catch(TCException te)
			{
				te.printStackTrace();
			}
		}
		return comps;
		*/
		try
		{
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"release_status_list"};
	    	
			Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
	    	
	    	nOVDataManagementServiceHelper.getObjectsProperties(targetItems, attrList, null);
	    	
			for(int i = 0; i < targetItems.length ; i++)
			{
			    CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
			    		"targetdisposition", CreateInObjectHelper.OPERATION_CREATE);		    	
				String item_id = targetItems[i].getProperty("item_id");
				String revision_id = ((TCComponentItem)targetItems[i]).getLatestItemRevision().getProperty("current_revision_id");
				String statusName = targetItems[i].getProperty("release_status_list");
				
				formPropertyMap.setString("targetitemid", item_id);
				formPropertyMap.setString("rev_id", revision_id);
			
				//get status name to set current status
				/*TCComponent[] relStatusList = null;
				relStatusList = ( targetItems[i].getTCProperty("release_status_list").getReferenceValueArray() );
				
				String statusName = relStatusList[0].getProperty("name");
				*/
				formPropertyMap.setString("currentstatus", statusName);
				//set empty properties
				
				formPropertyMap.setString("changedesc","");
				formPropertyMap.setString("dispinstruction","");
				formPropertyMap.setString("inprocess","");
				formPropertyMap.setString("ininventory","");
				formPropertyMap.setString("assembled","");
				formPropertyMap.setString("infield","");
				formPropertyMap.setString("futurestatus","");
				
				vecCreateInObjectHelper.add(formPropertyMap);
	   
			}
			
	        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
	                .toArray(new CreateInObjectHelper[vecCreateInObjectHelper.size()]);
			
			//create objects
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		    Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();
		    
		    for(int j = 0; j<createInObjectHelper.length ; j++)
		    {
		    	TCComponent created = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
		    	createdorUpdatedObjs.add(created);
		    }
		   
		    comps = (TCComponent[]) createdorUpdatedObjs.toArray(new TCComponent[createdorUpdatedObjs.size()]);
			
		}
		catch(TCException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comps;		
	}
	public TCComponent[] createDispInstances1(TCComponent[] targetItems)
		{
		TCComponent[] comps=null;
		/*if(itemIds!=null)
			{
			/*try
			{
				//call userExit to create the Disposition componenets and return the array
				TCUserService us=ssPanel.getSession().getUserService();       	
				Object obj[]=new Object[3];       	
				obj[0]=itemIds;
				obj[1]=revIds;
				obj[2] = new Boolean(false);
				comps=(TCComponent[])us.call("NATOIL_createEnDispositionData", obj);
				
				

				
			}catch(TCException te)
				{
				te.printStackTrace();
			}
		}*/
		try
		{
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"release_status_list"};
	    	
			Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
	    	
	    	nOVDataManagementServiceHelper.getObjectsProperties(targetItems, attrList, null);
	    	
			for(int i = 0; i < targetItems.length ; i++)
			{
			    CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
			    		"targetdisposition", CreateInObjectHelper.OPERATION_CREATE);		    	
				String item_id = targetItems[i].getProperty("item_id");
				String revision_id = ((TCComponentItem)targetItems[i]).getLatestItemRevision().getProperty("current_revision_id");
				String statusName = targetItems[i].getProperty("release_status_list");
				
				formPropertyMap.setString("targetitemid", item_id);
				formPropertyMap.setString("rev_id", "");
			
				//get status name to set current status
				/*TCComponent[] relStatusList = null;
				relStatusList = ( targetItems[i].getTCProperty("release_status_list").getReferenceValueArray() );
				
				String statusName = relStatusList[0].getProperty("name");
				*/
				formPropertyMap.setString("currentstatus", statusName);
				formPropertyMap.setString("changedesc","");
				formPropertyMap.setString("dispinstruction","");
				formPropertyMap.setString("inprocess","");
				formPropertyMap.setString("ininventory","");
				formPropertyMap.setString("assembled","");
				formPropertyMap.setString("infield","");
				formPropertyMap.setString("futurestatus","");
				
				vecCreateInObjectHelper.add(formPropertyMap);
	   
			}
			
	        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
	                .toArray(new CreateInObjectHelper[vecCreateInObjectHelper.size()]);
			
			//create objects
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		    Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();
		    
		    for(int j = 0; j<createInObjectHelper.length ; j++)
		    {
		    	TCComponent created = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
		    	createdorUpdatedObjs.add(created);
		    }
		   
		    comps = (TCComponent[]) createdorUpdatedObjs.toArray(new TCComponent[createdorUpdatedObjs.size()]);
			
			return comps;				
	
			
		}
		catch(TCException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comps;		

	}

	public void createDispTabPanels(TCComponent[] dispcomps)
	{
		try
		{
			for(int k=0;k<dispcomps.length;k++)
			{
				if(targetdispMap.get(dispcomps[k])!=null)
				{
					if(!(targetdispMap.get(dispcomps[k]).getProperty("object_type").equalsIgnoreCase(reg.getString("docsRev.TYPE"))))
					{
						boolean newRelease=isNewRelease(targetdispMap.get(dispcomps[k]));
						NOVEnDispositionTabPanel dispPanel=new NOVEnDispositionTabPanel(enPanel,enPanel.getSession(), dispcomps[k], newRelease);
						dispPanel.createBOMPanel(targetdispMap.get(dispcomps[k]));
						dispPanel.misGroup=NOVMissionHelper.isGroup(reg.getString("misPref.NAME"),targetdispMap.get(dispcomps[k]),true);
						dispPanel.createUI();
						dispPanel.setTargetItemRev(targetdispMap.get(dispcomps[k]));
						String itemId=(dispcomps[k].getTCProperty("targetitemid")).getStringValue();
						ImageIcon typeicon=TCTypeRenderer.getIcon(targetdispMap.get(dispcomps[k]));
						enPanel.enautomatonTabbePane.addTab(itemId, typeicon, dispPanel);
					}
				}
			}
			setTargetStatus();
		}catch(TCException tc)
		{
			tc.printStackTrace();

		}
	}

	public void adddispTabs(TCComponent[] newdispData)
	{
		//update disp data		
		try
		{		
			//get the Target Item IDs
			String itemIds[];	
			String targetRevIds[];
			Vector<String> vtargetIds=new Vector<String>();
			Vector<String> vtargetRevIds=new Vector<String>();
			Vector<TCComponent>newTargets=new Vector<TCComponent>();
			for(int i=0;i<newdispData.length;i++)
			{		
				//if(!(newdispData[i].getProperty("object_type").equalsIgnoreCase("Documents Revision")))
				//{
				vtargetIds.add(((TCComponentItemRevision)newdispData[i]).getProperty("item_id"));
				vtargetRevIds.add(((TCComponentItemRevision)newdispData[i]).getProperty("current_revision_id"));
				newTargets.add(newdispData[i]);
				//}

			}
			//Create the disp instances for the new Targets
			/*itemIds=new String[vtargetIds.size()];
			vtargetIds.copyInto(itemIds);
			targetRevIds=new String[vtargetRevIds.size()];
			vtargetRevIds.copyInto(targetRevIds);
			
			*/
			System.out.println ( "Inside adddispTabs(TCComponent[] newdispData)");
			TCComponent[] newdispInstances=createDispInstances(newdispData);
			for(int k=0;k<newdispInstances.length;k++)
			{
				targetdispMap.put(newdispInstances[k],newTargets.get(k));
				disptargetMap.put(newTargets.get(k), newdispInstances[k]);
			}
			//Update Disp Info
			TCComponent[] updateddispData=new TCComponent[referncedispdata.length+newdispInstances.length];		
			int k=0, i=0;
			for(k=0;k<referncedispdata.length;k++)
			{
				updateddispData[k]=referncedispdata[k];
			}
			for(;k<(referncedispdata.length+newdispInstances.length);k++)
			{
				updateddispData[k]=newdispInstances[i++];
			}
			referncedispdata=updateddispData;
			//save form
			enPanel.saveDipsDatatoForm();
			//Create new Disp Tabs
			createDispTabPanels(newdispInstances);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void removeDispTab(TCComponent tobRemoved)
	{
		TCComponent dispComptoBremoved=null;
		try {
			if(!(tobRemoved.getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE"))))
			{
				String item_id=tobRemoved.getProperty("item_id");
				//remove the tab panel also;
				for(int k=0;k<enPanel.enautomatonTabbePane.getTabCount();k++)
				{
					String title=enPanel.enautomatonTabbePane.getTitleAt(k);
					if(title.equalsIgnoreCase(item_id))
					{
						TCComponentItem item = ((TCComponentItemRevision)tobRemoved).getItem();
						String status = item.getProperty("release_status_list");
						if (status.equals(reg.getString("SUS.TYPE"))) 
						{
							String currentStat = status;
							String initialStat = disptargetMap.get(tobRemoved).getTCProperty("currentstatus").getStringValue();
							TCUserService userService =  tobRemoved.getSession().getUserService();
							Object[] objs =  new Object[3];
							objs[0]=item;
							objs[1]=currentStat;
							objs[2]=initialStat;
							TCComponent comp = (TCComponent)userService.call("NATOIL_setInitialStatus", objs);
							comp.refresh();
							//System.out.println("After server call :"+comp.getProperty("release_status_list"));
						}
						NOVEnDispositionTabPanel dispPanel=(NOVEnDispositionTabPanel)enPanel.enautomatonTabbePane.getComponentAt(k);
						dispComptoBremoved=dispPanel.dispcomp;
						enPanel.enautomatonTabbePane.remove(k);
						break;
					}

				}
			}
			dispComptoBremoved=disptargetMap.get(tobRemoved);
			if(dispComptoBremoved!=null)
			{
				int i=0;		
				TCComponent[] updateddispData=new TCComponent[referncedispdata.length-1];
				for(int k=0;k<referncedispdata.length;k++)
				{
					if(referncedispdata[k]!=dispComptoBremoved)
						updateddispData[i++]=referncedispdata[k];
				}
				//dispComptoBremoved.delete();
				referncedispdata=updateddispData;
				//save form
				enPanel.saveDipsDatatoForm();
				setTargetStatus();
			}

		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}

	}
	public void saveDispData()
	{
		if(enPanel!=null)
		{
			getTargetStatus();
			for(int k=0;k<enPanel.enautomatonTabbePane.getTabCount();k++)
			{
				if(enPanel.enautomatonTabbePane.getComponentAt(k) instanceof NOVEnDispositionTabPanel)
				{
					NOVEnDispositionTabPanel dispPanel=(NOVEnDispositionTabPanel)enPanel.enautomatonTabbePane.getComponentAt(k);
					TCComponent targetcomp=targetdispMap.get(dispPanel.dispcomp);
					try {
						if(!(targetcomp.getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE"))))
						{
							String status=targetStatusMap.get(targetcomp);			
							dispPanel.saveData(status);
						}
					} catch (TCException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public boolean isNewRelease(TCComponent comp)
	{
		//Aparna: modified to check the no of revs for new release instead of rev value
		try
		{
			if(comp instanceof TCComponentItemRevision)
			{
				//String revid=((TCComponentItemRevision)comp).getProperty("current_revision_id");
				int revscount=((TCComponentItemRevision)comp).getItem().getChildren( "revision_list" ).length;
				//if((revid.equalsIgnoreCase("A"))||(revid.equalsIgnoreCase("01")))
				if(revscount>1)
					return false;
				else
					return true;
			}

		}catch(TCException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public void getTargetStatus()
	{		
		if(enPanel!=null)
		{
			NOVEnTreeTableModel treeModel=(NOVEnTreeTableModel)enPanel.targetsPanel.targetsTable.getModel();
			int childcount=((TreeTableNode)treeModel.getRoot()).getChildCount();			
			for(int k=0;k<childcount;k++)
			{											
				NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)treeModel.getRoot()).getChildAt(k);
				TCComponent targetcomp=treeNode.context;
				//TCComponent comp=((NovTreeTableNode)enPanel.targetsPanel.targetsTable.getNodeForRow(k)).context;
				String status=enPanel.targetsPanel.targetsTable.futureStatMap.get(targetcomp);//enPanel.targetsPanel.targetsTable.getValueAt(k, 4).toString();
				targetStatusMap.put(targetcomp, status);
				String currentstatus=treeNode.getProperty("Starting Status");
			}
		}
	}

	public void setTargetStatus()
	{
		try
		{
			if(enPanel != null)
			{
				enPanel.targetsPanel.targetsTable.futureStatMap.clear();
				enPanel.targetsPanel.targetsTable.currentStatMap.clear();
				NOVEnTreeTableModel treeModel=(NOVEnTreeTableModel)enPanel.targetsPanel.targetsTable.getModel();
				int childcount=((TreeTableNode)treeModel.getRoot()).getChildCount();			
				for(int k=childcount-1;k>=0;k--)
				{	
					NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)treeModel.getRoot()).getChildAt(k);
					TCComponent targetcomp=treeNode.context;
					TCComponent dispcomp=disptargetMap.get(targetcomp);
					if(dispcomp != null)
					{
						enPanel.targetsPanel.targetsTable.currentStatMap.put(targetcomp, dispcomp.getTCProperty("currentstatus").getStringValue());
						String status=treeNode.getProperty("Starting Status");//enPanel.targetsPanel.targetsTable.getValueAt(k, 3).toString();
						if((status.compareTo(reg.getString("MFG.TYPE"))==0)||((status.compareTo(reg.getString("ACTIVE.TYPE"))==0))||((status.compareTo(reg.getString("SUS.TYPE"))==0)))
						{					
							enPanel.targetsPanel.targetsTable.futureStatMap.put(targetcomp, reg.getString("MFG.TYPE"));					
						}
						else
						{					
							String fstatus=dispcomp.getTCProperty("futurestatus").getStringValue();
							if((fstatus==null)||(fstatus.length()==0))
								enPanel.targetsPanel.targetsTable.futureStatMap.put(targetcomp, reg.getString("MFG.TYPE"));
							else
								enPanel.targetsPanel.targetsTable.futureStatMap.put(targetcomp, fstatus);					
						}
					}
				}
			}
		}catch(TCException tc)
		{
			tc.printStackTrace();
		}
	}
}
