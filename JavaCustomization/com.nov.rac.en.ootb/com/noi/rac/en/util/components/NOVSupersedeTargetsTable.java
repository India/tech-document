package com.noi.rac.en.util.components;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.noi.rac.en.form.compound.panels.Nov4_SupersedeForm_Panel;
import com.noi.rac.en.form.compound.panels._StopForm_Panel;

import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;

public class NOVSupersedeTargetsTable extends TCTable implements ActionListener{

	private Nov4_SupersedeForm_Panel supdformPanel;
	private TCSession session;
	String comboSelectedItem = "";
	//AIFTableModel tableModel;//Added By Sandip
	Vector<TCComponent> dispositionTarget;
	
	
	public NOVSupersedeTargetsTable(Nov4_SupersedeForm_Panel supdformPanel, TCSession session, String[] columNames)
	{
		super();
		setSession(session);
		AIFTableModel tableModel = new AIFTableModel(columNames);
		
		setModel(tableModel);
		
		this.supdformPanel=supdformPanel;
		this.session=session;
		//tableColumnModel = this.getColumnModel();
		dispositionTarget = supdformPanel.getSupdTargetDisposition();
		loadTabledata(supdformPanel.getEnTargets());
		/*try{
		for(int i=0; i<dispositionTarget.length;i++)
		{
			dispositionTarget[i].save();
		}
		}catch(TCException tce){
			
		}*/
	}

	public void loadTabledata(Vector<TCComponent> TargetstoBAdded)
	{
		//get the targets and load the table
		//get the targets and load the table
		try
		{			
			Vector<TCComponent> enTargets=TargetstoBAdded;
			int rowNo=getRowCount();
			
			//TCComponent targetObj=enTargets.get(i);
			
			String[] targetIds = new String[enTargets.size()];
			String[] targetRevIds = new String[enTargets.size()];
			
			for(int i=0;i<enTargets.size();i++){
				
				TCComponent targetObj=enTargets.get(i);
				targetIds[i]=targetObj.getProperty("item_id");
				targetRevIds[i]="01"; 
				
			}
			
			//TCComponent[] dispositionTarget = createDispInstances(targetIds, targetRevIds);
			
			//dispositionTarget = createDispInstances(targetIds, targetRevIds);
			//Save the Form
			//supdformPanel.saveDipsDatatoForm();	
			//Check Null condition 
			//TCProperty targetdispProperty=form.getTCProperty("nov4_supdtargetdisposition");
			if(dispositionTarget!=null)
			{
			 // for(int i=0;i<enTargets.size();i++)
			 for(int i=0;i<dispositionTarget.size();i++)
			  { 
				dispositionTarget.get(i).refresh();
				Vector<Object> obj=new Vector<Object>();
				
				obj.add(Integer.toString(++rowNo));
				
				//TCComponent comp=enTargets.get(i);
				TCComponent dispositionObject=dispositionTarget.get(i);
				
				String itemId=dispositionObject.getProperty("targetitemid");
				
				//Item Id
				obj.add(itemId);
										
				//Item Name
				//String itemName=dispositionObject.getProperty("object_name");
				//obj.add(itemName);
				
				//Current Status
					if(dispositionObject != null)
					{
						//status= ((TCComponentItemRevision)context).getItem().getProperty("release_statuses");
						String status=dispositionObject.getProperty("currentstatus");
						
							obj.add(status);
					}
					
					//String currentStatus = dispositionObject.getProperty("currentstatus")==null?"":dispositionObject.getProperty("currentstatus");
					//obj.add(currentStatus);
					//Added Sandip
					//String toGetScrapOrReworkIndex[] = {"","Scrap","Rework"};
					//String futureStatus=dispositionObject.getProperty("futurestatus");
					String futureStatus=dispositionObject.getTCProperty("futurestatus").getStringValue();
					//int scraporreworkposition=0;
/*					for (int j=0;j<toGetScrapOrReworkIndex.length;j++) 
					{
						if(toGetScrapOrReworkIndex[j]==futureStatus) {
							scraporreworkposition = j;
						break;
						}
					}*/
					//Ended Sandip
					
					//String futureStatus=dispositionObject.getProperty("futurestatus");
					
					 if(futureStatus==null || futureStatus.equals(""))
					 {
				       setFutureColumn(this, this.getColumnModel().getColumn(3));
				       //obj.add("Superseded");
					 }else if(futureStatus.equals("Superseded") /*|| futureStatus.equals("Scrap") || futureStatus.equals("Rework")*/)
					   {
						 obj.add("Superseded");
						 //obj.add(futureStatus);
					   }

/*					 if(futureStatus==null || futureStatus.equals(""))
					 {
						 setScrapOrReworkColumn(this, this.getColumnModel().getColumn(4),scraporreworkposition);
					 }else if( futureStatus.equals("Scrap") || futureStatus.equals("Rework"))
					   {
						 obj.add(futureStatus);
					   }*/
				     //setScrapOrReworkColumn(this, this.getColumnModel().getColumn(4));
				     
				     //obj.add("");
				     //obj.add("Scrap/Rework");
				this.dataModel.addRow(createLine(obj));
				
			}
			
		} //End Check Null condition 
		}
		catch(TCException tc)
		{

		}

	}

	
	/**
	  * This method will return the array of instance of disposition object.
	  *
	  * @param String[] itemIds, String revIds.
	  * @return TCComponent[]
	  * @throws com.teamcenter.rac.kernel.TCException
	  */
	
	public TCComponent[] createDispInstances(String[] itemIds, String[] revIds)
	{
		TCComponent[] comps=null;
		if(itemIds!=null)
		{ 
			try
			{
				//call userExit to create the Disposition componets and return the array
				TCUserService us=supdformPanel.getSession().getUserService();  
				
				Object obj[]=new Object[3];       	
				obj[0]=itemIds;
				obj[1]=revIds;
				obj[2] = new Boolean(false);
				comps=(TCComponent[])us.call("NATOIL_createEnDispositionData", obj);
				
			}catch(TCException te)
			{
				te.printStackTrace();
			}
		}
		return comps;
	}
	
	/**
	  * This method will set the dropdown in future column.
	  *
	  * @param JTable table, TableColumn futureColumn.
	  * @return void
	  * 
	  */
	
	public void setFutureColumn(JTable table, TableColumn futureColumn) {
		String supersedeLabels[] = {"", "Superseded"};
		JComboBox futureComboBox = new JComboBox(supersedeLabels);
		
		futureColumn.setCellEditor(new DefaultCellEditor(futureComboBox));
		futureComboBox.setSelectedIndex(0);

		DefaultTableCellRenderer renderer =
			new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for combo box");
		futureColumn.setCellRenderer(renderer);
		
		//futureComboBox.addActionListener(this);
		
	}
	
	/**
	  * This method will set the dropdown in Scrap/Rework column.
	  *
	  * @param JTable table, TableColumn scrapOrReworkColumn.
	  * @return void
	  * 
	  */
	public void setScrapOrReworkColumn(JTable table, TableColumn scrapOrReworkColumn, int position) {
		String scraporreworkLabels[] = {"","Scrap","Rework"}; 
		JComboBox ScrapOrReworkComboBox = new JComboBox(scraporreworkLabels);
		
		scrapOrReworkColumn.setCellEditor(new DefaultCellEditor(ScrapOrReworkComboBox));
		//ScrapOrReworkComboBox.addActionListener(this);
	
		DefaultTableCellRenderer renderer =
			new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for combo box");
		ScrapOrReworkComboBox.setSelectedIndex(position); // this "position" index will decide which option will display when open the page.
		scrapOrReworkColumn.setCellRenderer(renderer);
				
		//ScrapOrReworkComboBox.addActionListener(this);
	}
	
	public void saveTable()
	{
		//To choose only one Supersede
		
		/*if(this.getModel().getValueAt(0, 3).equals("Superseded")&& this.getModel().getValueAt(1, 3).equals("SuperSeded"))
		{
			JOptionPane.showMessageDialog(null, "Please choose only one Supersede Part");
	        
		}*/
		
	}
	
	public boolean isCellEditable(int row , int col )
	{ 
		
		//Below condition will make 3rd and 4th column editable...for dropdown purpose.
		if( col == 3 || col == 4)
		 { 
			return true;
					
		 }else{
				return false;
			  }
	}	

	/*public boolean isFutureStatusEditable(int row)
	{
		String curretnStatus=getModel().getValueAt(row, 4).toString();
		//if((curretnStatus.compareTo("ACTIVE")==0)||(curretnStatus.compareTo("STOP")==0))
		if((curretnStatus.compareTo("MFG")==0)||((curretnStatus.compareTo("ACTIVE")==0))||((curretnStatus.compareTo("SUS")==0)))
			return false;
		return true;
	}*/
	
	/**
	  * This method will listen if the future's column dropdown is selected.
	  *
	  * @param ActionEvent e.
	  * @return void
	  * 
	  */
	 public void actionPerformed(ActionEvent e) {
		 
	        JComboBox cb = (JComboBox)e.getSource();
	        comboSelectedItem = (String)cb.getSelectedItem();
	       
	    }
	 
	 boolean isSupersede(String superSede){
		 
		 if(superSede.equalsIgnoreCase("Superseded"))
			 return true;
		 else return false;
		 
	 }
	 
	 
	 
	 
  /* public AIFTableModel getTableModel(){
	 return tableModel;
    }*/
}