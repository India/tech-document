/*
 * Created on Sep 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import com.noi.rac.en.NationalOilwell;
import com.teamcenter.rac.common.lov.LOVPopupButton;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCSession;

/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOVLOVPopupButton extends LOVPopupButton {

	/**
	 * 
	 */
	public NOVLOVPopupButton() {
		super();
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
		
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOVLOVPopupButton(TCComponentListOfValues arg0, String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public NOVLOVPopupButton(TCSession arg0, String arg1, String arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOVLOVPopupButton(TCSession arg0, String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		setFont(NationalOilwell.NOV10ptFont);
		setBackground(NationalOilwell.NOVActiveFieldBG);
	}
	
	@Override
	public void setText(String s)
	{
	    // TODO Auto-generated method stub
	    super.setText(s);
	    if(s != null && s.length() > 0)
            setToolTipText(s);
	    else
            setToolTipText(null);
	}

}
