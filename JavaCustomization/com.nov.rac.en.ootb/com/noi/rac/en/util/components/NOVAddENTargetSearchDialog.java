package com.noi.rac.en.util.components;

//import com.nov.quicksearch.services.INOVSearchProvider;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.noi.NationalOilwell;
import com.noi.rac.en.customize.util.MessageBox;
import com.noi.rac.en.util.ENUtility;
import com.noi.rac.en.util.RelatedMinorRevisionSearchProvider;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.lov.HierarchicalLOVComponent;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.combobox.iComboBox;

public class NOVAddENTargetSearchDialog extends AbstractAIFDialog implements
		INOVSearchProvider {

	private NOVEnTargetsPanel_v2 targetsPanel;
	TCSession 		session;
	Registry 		registry = Registry.getRegistry(this);
	
	TCTree 						itemsearchTree;
	JButton 					addBtn;
	JButton 					cancelBtn;
	JLabel 						searchStatusLbl;
	JTextField 					itemIDtxt;
	JTextField 					itemNametxt;
	iComboBox 					itemTypeCmb;
	iComboBox 					groupCmb;
	JButton 					searchBtn;
	iComboBox 					lifeCycleCmb;
	HierarchicalLOVComponent 	partSubTypeCmb;
	JProgressBar progressBar;
	boolean 				m_isMissionGrp 	= false; 
		
	//TCDECREL-5689: Added a Table
	RelatedMinorRevision_TCTable m_relatedMinorRevTable = null;	
	//Added a vector for puid of minor rev already attached as target
	Vector<String> m_targetMinorRevsPuid = new Vector<String>();
	//TCDECREL-5689 Stop
	
	protected 	String      m_sItemId     	= null;
	protected 	String      m_sAltID     	= null;
	TCTable itemSearchTbl;
	String[] objTYPE= null;
	int	iTblRowCnt	= 0; 
	private Component comp = null;
	
	Map<Integer , String>  itemInfo = new HashMap<Integer , String>();
	//TCDECREL-5689: Map of puids of target minor revisions
	Map<Integer , String>  m_minorRevInfo = new HashMap<Integer , String>();
	
	public NOVAddENTargetSearchDialog(Frame parentFrame,NOVEnTargetsPanel_v2 novEnTargetsPanelV2) 
	{
		super(parentFrame);
		this.targetsPanel = novEnTargetsPanelV2;
		
		session = (TCSession)AIFUtility.getDefaultSession();

		TCPreferenceService prefServ = session.getPreferenceService();
        String[] missionGrps =prefServ.getStringArray(TCPreferenceService.TC_preference_all, 
        		registry.getString( "misPref.NAME"));
        				//"_NOV_Mission_item_creation_groups_" );
        		
        String enOwnGrp = null;
		try {
			enOwnGrp = targetsPanel.enFormPanel.Enform.getProperty("owning_group");
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        for(int k=0; k<missionGrps.length; k++)
        {
        	if(missionGrps[k].equalsIgnoreCase(enOwnGrp))
        	{
        		m_isMissionGrp = true;
        		break;
        	}
        }
		createUI();
	}

	private void createUI()
	{
		//TCDECREL-5689 Start
		//Show part/doc search table title
		JPanel searchTablePanel = new JPanel();
		searchTablePanel.setLayout(new BorderLayout(0, 0));
		searchTablePanel.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
		JLabel searchPartDocLabel = new JLabel(); 		
		searchPartDocLabel.setText(registry.getString("searchTable.Title"));		
		Font font = searchPartDocLabel.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		searchPartDocLabel.setFont(boldFont);
		searchTablePanel.add(searchPartDocLabel,BorderLayout.LINE_START);
		
		
		//Single selection mode label
 		JPanel singleSelectionLabelPanel = new JPanel(new BorderLayout(0, 0));
 		JLabel singleSelectionLabel = new JLabel(); 
 		singleSelectionLabelPanel.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
 		singleSelectionLabel.setText(registry.getString("singleSelection.LABEL"));
 		singleSelectionLabel.setForeground(Color.RED);
		singleSelectionLabelPanel.add(singleSelectionLabel,BorderLayout.LINE_START);
		//TCDECREL-5689 Stop
		
		JPanel searchCriPanel = new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));     
		JLabel itemIdLbl = new JLabel(registry.getString("itemIdOrAlternateId.Name"));

			//Prepare Search Status in RED BOLD 	
		searchStatusLbl = new JLabel(" ");
		searchStatusLbl.setFont(new Font(searchStatusLbl.getFont().getFamily(), Font.BOLD, searchStatusLbl.getFont().getSize())); 
		searchStatusLbl.setForeground(Color.RED);
		searchStatusLbl.setPreferredSize(new Dimension(300,20));
		searchStatusLbl.setAlignmentX(CENTER_ALIGNMENT);
		searchStatusLbl.setAlignmentY(CENTER_ALIGNMENT);
		
		itemIDtxt = new JTextField(20);
		itemIDtxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
            	//requestFocus(true);
            	int keyCode = ke.getKeyCode();
                if ( keyCode == KeyEvent.VK_ENTER )
                {
                	search_PopulateItems();
                	progressBar.setValue(0);
    				Rectangle rect = progressBar.getBounds();
                	progressBar.paintImmediately( 0,0, rect.width , rect.height );
                }
            }
           });
		itemNametxt = new JTextField(11);
		itemTypeCmb = new iComboBox(new String[]{"Documents","Engineering","Non-Engineering"});
		itemTypeCmb.setSelectedItem("");
		
		TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName(session, registry.getString("DH_Lifecycle.LOV"));
		
		String[] lovS = null;
		try {
			lovS = lovValues.getListOfValues().getStringListOfValues();
		} catch (TCException e) {
			e.printStackTrace();
		}
		Vector<String> lifecycleVals = new Vector<String>();
		lifecycleVals.add("");
		for (int i = 0; i < lovS.length; i++) 
		{
			lifecycleVals.add(lovS[i]);
		}
		String[] lifecycleValueArr = lifecycleVals.toArray(new String[lifecycleVals.size()]);
		lifeCycleCmb = new iComboBox(lifecycleValueArr);
		
		JLabel partSubTypeLbl = new JLabel(registry.getString("partSubType.Name"));
		
		TCComponentListOfValues typeLov = TCComponentListOfValuesType
        .findLOVByName((TCSession) AIFUtility.getDefaultSession(), NationalOilwell.DHITEMTYPELOV);
		partSubTypeCmb = new HierarchicalLOVComponent(typeLov , null , false);	
		partSubTypeLbl.setVisible(false);
		partSubTypeCmb.setVisible(false);
		
		groupCmb = new iComboBox();
		
		try 
		{
			
			TCComponentGroup[] usersGrp = session.getUser().getGroups();
			for (int i = 0; i < usersGrp.length; i++) 
			{
				groupCmb.addItem(usersGrp[i]);	
			}			
		} 
		catch (TCException e2)
		{
			e2.printStackTrace();
		}
		searchBtn = new JButton();
		ImageIcon searchIcon = registry.getImageIcon("search.ICON");
		searchBtn.setIcon(searchIcon);
		searchBtn.setPreferredSize(new Dimension(20,20));
		
		searchBtn.addMouseListener(new MouseAdapter() 
		{
			public void mouseReleased(MouseEvent mouseevent) 
			{
				search_PopulateItems();
				progressBar.setValue(0);
				Rectangle rect = progressBar.getBounds();
            	progressBar.paintImmediately( 0,0, rect.width , rect.height );
			}
		});
		searchCriPanel.add("1.1.center.center",itemIdLbl);
		searchCriPanel.add("1.2.center.center",itemIDtxt);
		searchCriPanel.add("1.3.center.center",searchBtn);

		//Search table column headers
		String[] colNames = null;
		colNames=registry.getString("columnNamesADD.LIST").split(","); 
		itemSearchTbl = new TCTable(session,colNames);
		Dimension dmn = itemSearchTbl.getSize();
		itemSearchTbl.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		TableColumn col = itemSearchTbl.getColumnModel().getColumn(0);
		col.setPreferredWidth( 70 );
		col = itemSearchTbl.getColumnModel().getColumn(1);
		col.setPreferredWidth( 60 );	
		col = itemSearchTbl.getColumnModel().getColumn(2);
		col.setPreferredWidth( 20 );	
		col = itemSearchTbl.getColumnModel().getColumn(3);
		col.setPreferredWidth( 130 );	
		col = itemSearchTbl.getColumnModel().getColumn(4);
		col.setPreferredWidth( 160 );
		col = itemSearchTbl.getColumnModel().getColumn(5);
		col.setPreferredWidth( 30 );	
		
		itemSearchTbl.getColumnModel().getColumn(0).setCellRenderer(new SearchResultTableCellRenderer());
		
		//TCDECREL-5689
		itemSearchTbl.getTableHeader().setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		
		itemSearchTbl.setName("searchResult.Name");
		itemSearchTbl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane treePane = new JScrollPane(itemSearchTbl);
		treePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		JPanel resultPanel = new JPanel(new BorderLayout());
		resultPanel.add(treePane, BorderLayout.CENTER);

		JPanel statusPanel = new JPanel(new PropertyLayout());
		statusPanel.add("1.1.center.center",searchStatusLbl);
		

	
		JPanel btnPanel = new JPanel();
		addBtn = new JButton(registry.getString("addToTargets.Name"));
		addBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				boolean	targetAlreadyExists	= false;
				boolean	isValidType			= false;
				
				int selRowsIndex[] = NOVAddENTargetSearchDialog.this.itemSearchTbl.getSelectedRows();				
				
				//TCDECREL-5689: Get selected row indices from Related Minor Revision Table
				int selRowsRelatedMinorRevTable[] = null;
				if( null != m_relatedMinorRevTable )
				{
					selRowsRelatedMinorRevTable = NOVAddENTargetSearchDialog.this.m_relatedMinorRevTable.getSelectedRowsIndices();
				}
				
				//TCDECREL-5689: Warning dialog if nothing is selected from both tables
				if( (selRowsIndex== null || selRowsIndex.length == 0)
						&&
						( selRowsRelatedMinorRevTable == null || selRowsRelatedMinorRevTable.length == 0) )
				{			
					comp = (JPanel)targetsPanel.getParent().getParent();
					MessageBox.post(NOVAddENTargetSearchDialog.this,"Warning",
							registry.getString("noPartSelected.MSG"),MessageBox.WARNING);
					return;
				}
				
				if(selRowsIndex.length > 1)
				{
					comp = (JPanel)targetsPanel.getParent().getParent();
					MessageBox.post(NOVAddENTargetSearchDialog.this,"Warning",
							registry.getString("selectOneItem.Name"),MessageBox.WARNING);
					return;
				}

				TCComponent selcomp = null;		

				//TCDECREL-5689: Vector of Item Ids
				Vector<String> vectorOfItemIds = new Vector<String>();
				//TCDECREL-5689: Vector of TCComponent to populate selected parts
				Vector<TCComponent> targetVector = new Vector<TCComponent>();
				try
				{
					if(selRowsIndex.length != 0)
					{
						String selPuid	= itemInfo.get(Integer.valueOf(selRowsIndex[0]));
						selcomp = session.stringToComponent(selPuid);
					
						TCComponentItem item = (TCComponentItem)selcomp;

						//usha
						String selObjType = selcomp.getType();
						if(selObjType.startsWith(registry.getString("DocsItem.TYPE")) ||
								selObjType.startsWith(registry.getString("Nov4PartItem.TYPE")) ||
								selObjType.startsWith(registry.getString("NonEngItem.TYPE")) )
						{
							isValidType = true;
							//TCDECREL-5689: Add part from Part/Doc Search Table to target vector
							targetVector.add(item);
							vectorOfItemIds.add(item.getProperty("item_id"));
							
						}
						if(!isValidType)
						{
							comp = (JPanel)targetsPanel.getParent().getParent();
							MessageBox.post(NOVAddENTargetSearchDialog.this,"Warning",
									registry.getString("INVALIDTYPE.MSG"),MessageBox.WARNING);
							return;	
						}
						//usha
						NOVAddENTargetSearchDialog.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					}
					
					//TCDECREL-5689: Adding selected related minor revisions to target vector
					if(selRowsRelatedMinorRevTable != null)
					{
						for(int index = 0; index < selRowsRelatedMinorRevTable.length; index++)
						{
							
							TCComponent selRowComp = null;
							int selRowIndex = 0;	
				
							selRowIndex = selRowsRelatedMinorRevTable[index];
							String selPuid	= m_minorRevInfo.get(Integer.valueOf(selRowIndex));
							selRowComp = session.stringToComponent(selPuid);
							
							TCComponentItemRevision itemRev = (TCComponentItemRevision)selRowComp; 
							//Check if Related Minor Revision Table and Part/Doc Search Table contain the same selected part
							if(!targetVector.isEmpty())
							{
								if(targetVector.get(0).getProperty("item_id").equalsIgnoreCase(itemRev.getProperty("item_id")))
								{
									continue;
								}		
								targetVector.add(itemRev);
								vectorOfItemIds.add(itemRev.getProperty("item_id"));
							}
							else
							{
								targetVector.add(itemRev);
								vectorOfItemIds.add(itemRev.getProperty("item_id"));
							}							
						}
					}

					TCComponent[] targets = null;
					targets = targetsPanel.enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);					
					

					targetAlreadyExists = false;
					String targetCompId = null;
					for(int i=0; i< targets.length; i++)
					{					
						targetCompId = targets[i].getProperty("item_id");
						//TCDECREL-5689: Avoid adding revision of same part as target											
						if(vectorOfItemIds.contains(targets[i].getProperty("item_id")))
						{
							targetAlreadyExists = true;
							break;
						}
					}
					if(targetAlreadyExists ==true)
					{
						comp = (JPanel)targetsPanel.getParent().getParent();
						MessageBox.post(NOVAddENTargetSearchDialog.this,"Warning",
								(targetCompId)+" , "+registry.getString("targetExists.msg"),MessageBox.WARNING);
						NOVAddENTargetSearchDialog.this.setCursor(Cursor.getDefaultCursor());
						return;
					}
										
					if( (targetsPanel!=null) && (targetVector.size() != 0) && (targetAlreadyExists == false))
					{	
						TCComponent[] trgtArr = targetVector.toArray(new TCComponent[targetVector.size()]);
						addItemToTarget(targetsPanel, trgtArr);
					}
					NOVAddENTargetSearchDialog.this.setCursor(Cursor.getDefaultCursor());
				} 
				catch (TCException e1) 
				{
					e1.printStackTrace();
				}
			}		
		});
		cancelBtn = new JButton(registry.getString("cancel.Name"));
		cancelBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				NOVAddENTargetSearchDialog.this.dispose();
			}
		});
		btnPanel.setLayout(new  PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		btnPanel.add("1.1.center.center", addBtn);
		btnPanel.add("1.2.center.center", cancelBtn);
		

	    progressBar = new JProgressBar();
	    
		JPanel progressPanel = new JPanel(new PropertyLayout());
		progressPanel.add("1.1.center.center", progressBar);
		
		//JPanel contentPanel = new JPanel(new PropertyLayout());

		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		
		//TCDECREL-5689: Related minor revision panel displayed only if valid EN group
		if(ENUtility.isValidENGroupForMinorRevisionTable())
		{
			JPanel relatedMinorRevisionPanel = createMinorRevisionPanel();
			addBtn.setToolTipText(registry.getString("addButtonToolTip.STR"));
			if(relatedMinorRevisionPanel != null)
			{
				contentPanel.add(relatedMinorRevisionPanel);
			}			
		}
		//TCDECREL-5689 Stop
	
		contentPanel.add(searchCriPanel);		
		//TCDECREL-5689: Added two new labels for search table
		contentPanel.add(searchTablePanel);
		contentPanel.add(singleSelectionLabelPanel);
		
		contentPanel.add(resultPanel);
		contentPanel.add(statusPanel);		
		
		//contentPanel.add(new Separator());
	
		contentPanel.add(progressBar);
		//contentPanel.add(new Separator());
		contentPanel.add(btnPanel);
		
			
		this.getContentPane().add(contentPanel);
		this.setResizable(true);
		this.setTitle(registry.getString("itemSearch.Title"));
		this.pack();
		this.setModal(true);
	}//createUI 
	
	//TCDECREL-5689 Creating the Related Minor Revision Panel
	public JPanel createMinorRevisionPanel()
	{		
		//Added a button "Show Related Minor Revisions"
		JPanel showRelatedMinorRevPanel = new JPanel(new PropertyLayout(5 , 5 , 5 , 5 , 15 , 5));
		JButton showRelatedMinorRevsBtn = new JButton();
		showRelatedMinorRevsBtn.setText(registry.getString("showMinorRevButton.LABEL"));
		showRelatedMinorRevsBtn.setToolTipText(registry.getString("showMinorRevButtonToolTip.STR"));
		showRelatedMinorRevsBtn.addMouseListener(new MouseAdapter() 
		{
			public void mouseReleased(MouseEvent mouseevent) 
			{				
				//Search and populate related minor revisions		
				try 
				{
					m_relatedMinorRevTable.clear();
					m_relatedMinorRevTable.clearCheckboxHeader();
					populateRelatedMinorRevisionTable(targetsPanel);
				} 
				catch (TCException e2) 
				{
					e2.printStackTrace();
					MessageBox.post(NOVAddENTargetSearchDialog.this,"ERROR", e2.getDetailsMessage(),  MessageBox.ERROR);
				}				
			}
		});		
		showRelatedMinorRevPanel.add("1.1.center.center", showRelatedMinorRevsBtn);
		
		//Related minor revision table title
 		JPanel relatedMinorRevPanel = new JPanel(new BorderLayout(0, 0));
		JLabel relatedMinorRevLabel = new JLabel(); 
		relatedMinorRevPanel.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
		relatedMinorRevLabel.setText(registry.getString("relatedMinorRevisionTable.TITLE"));
		Font font = relatedMinorRevLabel.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		relatedMinorRevLabel.setFont(boldFont);
		relatedMinorRevPanel.add(relatedMinorRevLabel,BorderLayout.LINE_START);
		
		//Checkbox selection label
 		JPanel checkboxSelectionLabelPanel = new JPanel(new BorderLayout(0, 0));
 		JLabel checkboxSelectionLabel = new JLabel(); 
 		checkboxSelectionLabelPanel.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
 		checkboxSelectionLabel.setText(registry.getString("checkboxSelection.LABEL"));
 		checkboxSelectionLabel.setForeground(Color.RED);
 		checkboxSelectionLabelPanel.add(checkboxSelectionLabel,BorderLayout.LINE_START);
		
		//Related minor revision table header
		String[] columnNames = null;
		columnNames = registry.getString("columnNamesRelatedMinorRevs.LIST").split(","); 

		//Creating related minor revisions table
		m_relatedMinorRevTable = new RelatedMinorRevision_TCTable(session, columnNames);
		m_relatedMinorRevTable.setCheckboxHeaderState();
		
		JScrollPane treePaneRelatedMinors = new JScrollPane(m_relatedMinorRevTable);
		treePaneRelatedMinors.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		JPanel relatedMinorRevisionPanel = new JPanel(new BorderLayout());
		relatedMinorRevisionPanel.add(treePaneRelatedMinors, BorderLayout.CENTER);		
		
		//Assembling all the components
		JPanel minorRevisionPanel = new JPanel();
		minorRevisionPanel.setLayout(new BoxLayout(minorRevisionPanel, BoxLayout.Y_AXIS));
		
		minorRevisionPanel.add(showRelatedMinorRevPanel);
		minorRevisionPanel.add(relatedMinorRevPanel);		
		minorRevisionPanel.add(checkboxSelectionLabelPanel);
		minorRevisionPanel.add(relatedMinorRevisionPanel);
		
		return minorRevisionPanel;
	}

	//TCDECREL-5689 Start
	private void populateRelatedMinorRevisionTable(NOVEnTargetsPanel_v2 targetsPanel) throws TCException 
	{
		TCComponent[] targets = null;
		targets = targetsPanel.enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);		
		TCComponent[] minorTargets = getMinorRevsFromTargets(targets);	
		//Populate vector for filtering the search results later
		for(int index = 0; index < minorTargets.length; index++ )
		{
			m_targetMinorRevsPuid.add(minorTargets[index].getUid());
		}
		search_related_minor_revisions(minorTargets);
	}

	//Search for related unreleased minor revisions
	private void search_related_minor_revisions(TCComponent[] targetsAttached)
	{
		  iTblRowCnt = 0; 
		  m_minorRevInfo.clear();
		  
		  INOVSearchProvider searchService = new RelatedMinorRevisionSearchProvider(targetsAttached);
		  INOVSearchResult searchResult = null;
		  
	        try
	        {
	            if (searchService != null)
	            {
	                 if ( (searchService instanceof INOVSearchProvider2) && 
	                	   (((INOVSearchProvider2)searchService).validateInput() == false)
	                	 )
	                 {
	                	 MessageBox.post(this,"Info", registry.getString("noMinorRevisions.MSG"), MessageBox.INFORMATION);                	
	                     return;
	                 }	                 
	                 searchResult = NOVSearchExecuteHelperUtils.execute(searchService, 0);
	            }
	        }
	        catch (Exception exception)
	        {
	            exception.printStackTrace();
	        }
	        if (searchResult == null)
	        {
	            return;
	        }
	        
	        INOVResultSet theResultSet = searchResult.getResultSet();
	
	        //Populate the table with search results
	        for (int i = 0; i < searchResult.getResponse().nRows; i++)
            {     
            	  Vector<String> itemData = new Vector<String>();
            	
            	  String objPUID = theResultSet.getRowData().elementAt(9*i);
                  String objNAME = theResultSet.getRowData().elementAt(9*i+1);
                  String objDesc = theResultSet.getRowData().elementAt(9*i+2);
                  String revID = theResultSet.getRowData().elementAt(9*i+3);
                  String objID = theResultSet.getRowData().elementAt(9*i+5);
                  String releaseStatus = theResultSet.getRowData().elementAt(9*i+7);
                  String altid = theResultSet.getRowData().elementAt(9*i+8);
                  
                  if(!isMinorRev(revID) || releaseStatus.equalsIgnoreCase("Released") || m_targetMinorRevsPuid.contains(objPUID))
                  {
                	  continue;
                  }
 
                  itemData.add("false");
                  itemData.add(objID);
                  itemData.add(altid);
                  itemData.add(revID);
                  itemData.add(objNAME);
                  itemData.add(objDesc);
                
                  m_relatedMinorRevTable.addRow(itemData);
                 
                  //Add to m_minorRevInfo HashMap
                  m_minorRevInfo.put(Integer.valueOf(iTblRowCnt), objPUID);
                  iTblRowCnt++;
            }
	        if(iTblRowCnt > 0)
	        {
	        	m_relatedMinorRevTable.setCheckboxHeaderState();
	        	m_relatedMinorRevTable.updateUI();
	        }
	        else
	        {
	        	MessageBox.post(this,"Info", registry.getString("noMinorRevisions.MSG"), MessageBox.INFORMATION);
	        }
	}
		
	//Check if revision is minor
	private boolean isMinorRev(String revID)
	{
		boolean isRevMinor = false;
		if(revID.contains("."))
		{
			String tempStr = revID.substring(revID.lastIndexOf(".")+1, revID.length());		
			try
			{
				  int num = Integer.parseInt(tempStr);
				  // is an integer!
				  isRevMinor = true;
			} 
			catch (NumberFormatException e) 
			{
				  // not an integer!
				  isRevMinor = false;
			}
		}
		return isRevMinor;
	}
	
	//Filter to get only minor revisions from multiple targets
	private TCComponent[] getMinorRevsFromTargets(TCComponent[] targets) throws TCException
	{
		TCComponent[] minorRevTargets = new TCComponent[]{};
		Vector<TCComponent> trgtVec = new Vector<TCComponent>();
		for(int count = 0; count < targets.length; count++ )
		{
			if(targets[count] instanceof TCComponentItemRevision)
			{
				String revisionID = targets[count].getProperty("item_revision_id");
				if(isMinorRev(revisionID))
				{
					trgtVec.add(targets[count]);					
				}
			}			
		}	
		minorRevTargets = trgtVec.toArray(new TCComponent[trgtVec.size()]);
		return minorRevTargets;
	}	
	//TCDECREL-5689 Stop
	
	
	//TCDECREL-5689: Modified to take TCComponent[] as input instead of TCComponent 
	private void addItemToTarget(NOVEnTargetsPanel_v2 panel, TCComponent[] selComponents) 
	{
		TCUserService userservice = session.getUserService();	
		Object 	addRemRetObj 	= null;
		Object 	addRemInputObjs[] = new Object[3];
		int		addRemFlag		= 1;
		addRemInputObjs[0] = (TCComponent)(panel.enRootTask);
		
		//TCDECREL-5689: Modified to take tccomp[] as input
		addRemInputObjs[1] = selComponents;
		
		addRemInputObjs[2] = addRemFlag;
			
		try 
		{
			NOVAddENTargetSearchDialog.this.dispose();
			addRemRetObj = userservice.call("NATOIL_AddRemoveTargets_EN", addRemInputObjs);		
		} 
		catch (TCException e) 
		{
			MessageBox.post(this,"ERROR",
					e.getDetailsMessage(),  MessageBox.ERROR);
		}
		if(addRemRetObj != null) 
		{
			TCComponent[] newTargets = (TCComponent[]) addRemRetObj;
			int		newTrgtsCnt	= newTargets.length;
			
			try
			{		
				AIFComponentContext[] cxtObjs =  panel.enRootTask.getChildren();
				for (int i = 0; i < cxtObjs.length; i++) 
				{
					if (cxtObjs[i].getContext().equals(registry.getString("PSEUDOFOLDER.NAME"))) 
					{
						((TCComponent)cxtObjs[i].getComponent()).refresh();	
					}
				}
				panel.enRootTask.refresh();	
				for(int i=0; i<newTrgtsCnt; i++)
				{
					panel.enFormPanel.Entargets.add(newTargets[i]);
				}
				if( (newTrgtsCnt>0))
				{
					for (int i = 0; i < newTargets.length; i++) 
				   {
						String	selObjType = null;
						selObjType = newTargets[i].getTCProperty("object_type").toString();
						if(selObjType.startsWith(registry.getString("DocsItem.TYPE")))
						{
							panel.targetsTable.addDocTarget(newTargets[i]);
						}
						else
						{
							panel.targetsTable.addTarget( newTargets[i]);
						}
				   }

				   panel.enFormPanel.Enform.save();
				   panel.enFormPanel.Enform.refresh();
				   panel.targetsTable.updateUI();
				}						
			} 
			catch (TCException e)
			{
				e.printStackTrace();;
			}	

			if(newTrgtsCnt>0)
			{
				MessageBox.post(this,"Info",
						registry.getString("Added.MSG"), MessageBox.INFORMATION);
			}
			else
			{
				MessageBox.post(this,"Info",
						registry.getString("NotAdded.MSG"), MessageBox.INFORMATION);
			}
		} 
	}
	public String getBusinessObject()
	{
		return null;
	}

	public QuickSearchService.QuickBindVariable[] getBindVariables()
	{
		//Prepare input fields - substitute * for empty values
		m_sItemId = itemIDtxt.getText();
		
		m_sAltID = m_sItemId;
        if (m_sAltID == null || m_sAltID.isEmpty())
        	m_sAltID = "*";
 
        //String m_sGrpID = "";
			
        //Construct Bind Variables
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[12];
        
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[]{m_sItemId};
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[] {"*"}; //item name
        
        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[]{"*"};//item type
        
                                
        bindVars[3] = new QuickSearchService.QuickBindVariable();
        bindVars[3].nVarType = POM_date;
        bindVars[3].nVarSize = 2;
        bindVars[3].strList = new String[] {"*","*"};//m_sCreatedAfter,m_sCreatedBefore
        
        bindVars[4] = new QuickSearchService.QuickBindVariable();
        bindVars[4].nVarType = POM_string;
        bindVars[4].nVarSize = 1;
        bindVars[4].strList = new String[] {"*"};//desc
        
        bindVars[5] = new QuickSearchService.QuickBindVariable();
        bindVars[5].nVarType = POM_string;
        bindVars[5].nVarSize = 1;
        bindVars[5].strList = new String[] {"*"};//release status
        
        bindVars[6] = new QuickSearchService.QuickBindVariable();
        bindVars[6].nVarType = POM_string;
        bindVars[6].nVarSize = 1;
        bindVars[6].strList = new String[] {"*"};//m_sOwningUser
  
        
        bindVars[7] = new QuickSearchService.QuickBindVariable();
        bindVars[7].nVarType = POM_typed_reference;
        bindVars[7].nVarSize = 1;
        bindVars[7].strList = new String[] {""};
        //bindVars[7].strList = new String[] {m_sGrpID};
        
        
        
        bindVars[8] = new QuickSearchService.QuickBindVariable();
        bindVars[8].nVarType = POM_string;
        bindVars[8].nVarSize = 1;
        if(!m_isMissionGrp)
        	bindVars[8].strList = new String[] {"RSOne"};
        else
        	bindVars[8].strList = new String[] {"JDE"};
        
        bindVars[9] = new QuickSearchService.QuickBindVariable();
        bindVars[9].nVarType = POM_string;
        bindVars[9].nVarSize = 1;
        bindVars[9].strList = new String[] {"*"};//m_sLastModifiedUser
        
        //Rakesh K - Start - Send Alternate ID selection
        bindVars[10] = new QuickSearchService.QuickBindVariable();
        bindVars[10].nVarType = POM_string;
        bindVars[10].nVarSize = 1;
        bindVars[10].strList = new String[] {m_sAltID};
        
        bindVars[11] = new QuickSearchService.QuickBindVariable();
        bindVars[11].nVarType = POM_string;
        bindVars[11].nVarSize = 1;
        bindVars[11].strList = new String[] { "*" };

        return bindVars;
        
  }
//getBindVariables

	
	public QuickSearchService.QuickHandlerInfo[] getHandlerInfo()
	{
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[4];
		
		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
		allHandlerInfo[0].listBindIndex = new int[] {1,2,3,4,5,6,7,8,10,11,12};
		allHandlerInfo[0].listReqdColumnIndex = new int[]{1,2,3,5}; //object_name , item_id, object_desc
		allHandlerInfo[0].listInsertAtIndex = new int[] {2,1,3,4}; //Item Id, Item Name
		
		
		allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
		//allHandlerInfo[1].handlerName = "NOVSRCH-get-item-latest-revs";
		allHandlerInfo[1].handlerName ="NOVSRCH-get-nov4part-latest-revs";
		allHandlerInfo[1].listBindIndex = new int[0];
//		allHandlerInfo[1].listBindIndex = new int[1];
//		allHandlerInfo[1].listBindIndex[0] = 0;
		//allHandlerInfo[1].listBindIndex = new int[0];
		allHandlerInfo[1].listReqdColumnIndex = new int[] {1,2};
		allHandlerInfo[1].listInsertAtIndex = new int[] {5,6 }; //latest rev id, release status
		
		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[2].handlerName = "NOVSRCH-get-alternate-ids";
		allHandlerInfo[2].listBindIndex = new int[] {9};
		allHandlerInfo[2].listReqdColumnIndex = new int[] {1};
		allHandlerInfo[2].listInsertAtIndex = new int[] {7};
		
		allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[3].handlerName = "NOVSRCH-get-nov4part-master-props";
		allHandlerInfo[3].listBindIndex = new int[0];
		allHandlerInfo[3].listReqdColumnIndex = new int[] {1};
		allHandlerInfo[3].listInsertAtIndex = new int[] {8};
		
		return allHandlerInfo;
	}//getHandlerInfo
	
	private void search_PopulateItems()
	{
		itemInfo.clear();
		itemSearchTbl.removeAllRows();
		m_sItemId = itemIDtxt.getText();
        try
        {     searchStatusLbl.setVisible(true);
              searchStatusLbl.setText("");
              String itemId = itemIDtxt.getText();           
              if(itemId.trim().length() >=3)
              {
            	  //if wild card exists in search string, remove the wild cards and verify string length.
            	  //it should be min. 3 characters long.- Else throw error.
            	  char[] arr = itemId.trim().toCharArray();
            	  String inputId = "";
            	  for(int i=0; i<arr.length; i++)
            	  {
            		  if(arr[i] != '*')
            		  {
            			  inputId = inputId + arr[i];
            		  }
            	  }
            	  if(inputId.length()< 3)
            	  {
            		  comp = (JPanel)targetsPanel.getParent().getParent();
					  MessageBox.post(this,"ERROR",
							  registry.getString("searchWithMinInput.MSG"), MessageBox.ERROR);
            		  return;
            	  }            	  
              }
              if(itemId.trim().length()<3)
              {
            	  comp = (JPanel)targetsPanel.getParent().getParent();
				  MessageBox.post(this,"ERROR",
						  registry.getString("searchWithMinInput.MSG"), MessageBox.ERROR);
            	  return;
              }
              
              INOVSearchProvider searchService = this;
              INOVSearchResult searchResult = null;
              INOVResultSet theResSet = null;
              try
              {
                    searchResult = NOVSearchExecuteHelperUtils.execute(searchService, -2);
              }
              catch (Exception e) 
                {
                  System.out.println("Exception in search: "+e.getMessage()); 
            	  e.printStackTrace();
                }
            
              if ( searchResult != null)
              {
                    theResSet = (INOVResultSet) searchResult.getResultSet();
              }
              
                     
              if(searchResult != null && searchResult.getResponse().nRows > 0)
              {
            	  
            	  iTblRowCnt	= 0; 
            	  objTYPE = new String[searchResult.getResponse().nRows];
            	  //progressBar.setVisible(true);

            	  //progressBar.enable();
            	  progressBar.setMinimum(0);
            	  progressBar.setMaximum(searchResult.getResponse().nRows);
            	  Rectangle rect = progressBar.getBounds();
            	  //progressBar.paintImmediately( 0,0, rect.width , rect.height );
          	  
                    for (int i = 0; i < searchResult.getResponse().nRows; i++)
                    {
                    	progressBar.setValue(i);
                    	//System.out.println("val"+i);
                    	progressBar.paintImmediately( 0,0, rect.width , rect.height );
                    	 
                    	  Vector<String> itemData = new Vector<String>();
                          String objPUID = theResSet.getRowData().elementAt(9*i);
                          String objID = theResSet.getRowData().elementAt(9*i+1);
                          String objNAME = theResSet.getRowData().elementAt(9*i+2);
                          String objType = theResSet.getRowData().elementAt(9*i+3);
                          String objDesc = theResSet.getRowData().elementAt(9*i+4);
                          String revID = theResSet.getRowData().elementAt(9*i+5);
                          String relStatus = theResSet.getRowData().elementAt(9*i+6);
                          //System.out.println("Rel Status for "+objID+"/"+revID+ " is "+relStatus);
 //                    String relStatus1 = theResSet.getRowData().elementAt(8*i+7);
                          String altid = theResSet.getRowData().elementAt(9*i+7);
                          String rsOneType = theResSet.getRowData().elementAt(9*i+8);
                                                
                          itemData.add(objID);
                          itemData.add(altid);
                          itemData.add(revID);
                          itemData.add(objNAME);
                          itemData.add(objDesc);
                          itemData.add(relStatus);
                          //itemData.add(altid);
                          
                          if( ((relStatus == null)) ||( (relStatus != null) &&  (!(relStatus.equalsIgnoreCase("Engineering Pre Release")))) )
                          {         
                        	  itemSearchTbl.addRow(itemData);
                        	  //itemInfo.put(objPUID, itemData);
                        	  itemInfo.put(Integer.valueOf(iTblRowCnt), objPUID);
                        	  if(rsOneType!= null && rsOneType.equalsIgnoreCase("Purchased (Inventory Item)"))
                        		  objType = "Purchased Part";
                        	  objTYPE[iTblRowCnt] = objType + " Revision";
                        	  iTblRowCnt++;
                          }
                    }
                    if(itemSearchTbl.getRowCount() <1)
                    {
                    	searchStatusLbl.setVisible(true);
                    	searchStatusLbl.setText(registry.getString("searchStatusNoItemFound.Name"));
                    }
                    else
                    {
                    	searchStatusLbl.setForeground(Color.BLUE);
                    	searchStatusLbl.setVisible(true);
                    	searchStatusLbl.setText(iTblRowCnt+" Item(s) Found");
                    }
              }
              else
              {
            	    searchStatusLbl.setForeground(Color.RED);
                    searchStatusLbl.setVisible(true);
                    //searchStatusLbl.setText("No Item(s) found. Change your search criteria");
                    searchStatusLbl.setText(registry.getString("searchStatusNoItemFound.Name"));
              }
              //progressBar.setVisible(false);
             // progressBar.disable();
              
        }catch(Exception e)
        {
              e.printStackTrace();
        }

	}//search_PopulateItems
	class SearchResultTableCellRenderer extends JLabel implements TableCellRenderer
	{

	   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
	            boolean hasFocus, int row, int column) 
	   {
		  this.setIcon(TCTypeRenderer.getTypeIcon(objTYPE[row], null));
		  this.setText(value.toString());
		  if (isSelected) 
		  {
			  setOpaque(true);
			  this.setBackground(table.getSelectionBackground());
		      this.setForeground(table.getSelectionForeground());
		  }
		  else
		  {
			  this.setBackground(null);
			  this.setForeground(null);
			  
		  }
	      	      
	      return this;
	   }
	}
	
	public Window getWindow(Component con)
	{
		Window window = null;
		
		if(con.getParent() instanceof Window)
		{
			window = (Window) con.getParent();
		}
		else
		{
			 window = getWindow(con.getParent());
		}
		
		return window;	
	} 

}
