/*
 * Created on Apr 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JTextField;

/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOINonEditableTextField extends JTextField {
    
    private static Font labelFont = new Font("Dialog",java.awt.Font.BOLD,10);
    
    public NOINonEditableTextField() { 
        super(); 
        setFont(labelFont); 
        setMargin(new Insets(0,15,0,5));
        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
        setEditable(false);
        setMinimumSize(new Dimension(125,20));
        setPreferredSize(new Dimension(125,20));
    }
    public NOINonEditableTextField(String s) { 
        super(s); 
        setFont(labelFont); 
        setMargin(new Insets(0,15,0,5)); 
        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
        setEditable(false);
        setMinimumSize(new Dimension(125,20));
        setPreferredSize(new Dimension(50,20));
    }

}
