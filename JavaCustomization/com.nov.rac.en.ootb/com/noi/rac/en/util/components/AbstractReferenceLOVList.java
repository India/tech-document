/*
 * Created on Dec 29, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Vector;

import org.eclipse.swt.widgets.List;

import com.noi.rac.en.util.queries.General;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ArraySorter;
import com.teamcenter.rac.util.Registry;


/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AbstractReferenceLOVList {

    Hashtable stringToRef = new Hashtable();
    Hashtable refToString = new Hashtable();
    Vector    orderedVector = new Vector();
    ArrayList excludeList = new ArrayList();
    private Registry reg;
    
    final int FORM=0;
    final int POM_QUERY=1;
    /**
     * 
     */
    public AbstractReferenceLOVList() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(TCComponent[] arr, String attName) {
    	ArraySorter.sort(arr, true);
    	for(int index =0;index<arr.length;index++)
    		orderedVector.addElement(arr[index]);
        populateHashtables(orderedVector,attName);
    }

    public void init(TCSession s,String object, String attName) {
    	this.reg=Registry.getRegistry(this);
        try {
            TCComponent[] arr =
                General.execute(s,new String[] {reg.getString("type.STR")},new String[] {object});
            if (arr.length > 0) {
            	ArraySorter.sort(arr, true);
            	for(int index =0;index<arr.length;index++)
            		orderedVector.addElement(arr[index]);
                populateHashtables(orderedVector,attName);
            }
            else {
                System.out.println("AbstractReferenceLOVList: No values found for type \""+object+"\"\n");
            }
        }
        catch (Exception e) {
            System.out.println("Could not retrieve values for AbstractReferenceLOVList: "+e);
        }
        
    }
    public void initClass(TCSession s,String object, String attName) {
        
        try {
        	TCComponent[] arr = 
        		s.getClassService().findByClass(object,attName,"*");
            if (arr.length > 0) {
            	ArraySorter.sort(arr, true);
            	for(int index =0;index<arr.length;index++)
            		orderedVector.addElement(arr[index]);        	
                populateHashtables(orderedVector,attName);
            }
            else {
                System.out.println("AbstractReferenceLOVList: No values found for type \""+object+"\"\n");
            }
        }
        catch (Exception e) {
            System.out.println("Could not retrieve values for AbstractReferenceLOVList: "+e);
        }
        
    }
 public void initClassECNERO(TCSession s,String object, String attName, String matchstr) {
        
        try {
        	TCComponent[] arr = 
        		s.getClassService().findByClass(object,attName, matchstr+"*");
            if (arr.length > 0) {
            	ArraySorter.sort(arr, true);
            	for(int index =0;index<arr.length;index++)
            		orderedVector.addElement(arr[index]);         	
            	populateHashtables(orderedVector,attName);
            }
            else {
                System.out.println("AbstractReferenceLOVList: No values found for type \""+object+"\"\n");
            }
        }
        catch (Exception e) {
            System.out.println("Could not retrieve values for AbstractReferenceLOVList: "+e);
        }
        
    }
    public void init(TCSession s,String query, String[] qryParms, String[] qryParmVals,String attName) {
        
        try {
            TCComponentQueryType qt = 
                (TCComponentQueryType)s.getTypeComponent(reg.getString("qry.TYPE"));
            TCComponentQuery gen = (TCComponentQuery)qt.find(query);
            if (gen != null) {
                TCComponent[] arr = gen.execute(qryParms,qryParmVals);
                if (arr.length > 0) {
                	ArraySorter.sort(arr, true);
                	for(int index =0;index<arr.length;index++)
                		orderedVector.addElement(arr[index]);
                    populateHashtables(orderedVector,attName);
                }
                else
                    System.out.println("AbstractReferenceLOVList: No values found for query \""+query+"\"\n");
            }            
        }
        catch (Exception e) {
            System.out.println("Could not retrieve values for AbstractReferenceLOVList: "+e);
        }
    }
    
    private void populateHashtables(TCComponent[] arr,String attName) {
        
        try {
            for (int i=0;i<arr.length;i++) {
                String val = arr[i].getProperty(attName);
                refToString.put(arr[i],val);
                stringToRef.put(val,arr[i]);
            }
        }
        catch (Exception e) {
            System.out.println("Could not populate AbstractReferenceLOVList hashtables: "+e);
        }
        
    }
    private void populateHashtables(Vector vec,String attName) {
        
        try {
            for (int i=0;i<vec.size();i++) {
            	TCComponent comp = (TCComponent)vec.get(i);
                String val = comp.getProperty(attName);
                refToString.put(comp,val);
                stringToRef.put(val,comp);
            }
        }
        catch (Exception e) {
            System.out.println("Could not populate AbstractReferenceLOVList hashtables: "+e);
        }
        
    }
        private void populateHashtablesEcnero(Vector vec,String attName) {
            
            try {
                for (int i=0;i<vec.size();i++) {
                	TCComponent comp = (TCComponent)vec.get(i);
                    String val = comp.getProperty(attName);
                    String dispval=val.substring(val.indexOf(':'), val.length()-1);
                    refToString.put(comp,dispval);
                    stringToRef.put(dispval,comp);
                }
            }
        catch (Exception e) {
            System.out.println("Could not populate AbstractReferenceLOVList hashtables: "+e);
        }
        
    }
    
    public String getStringFromReference(TCComponent i) {
    	if (i == null) return "";
        return (String)refToString.get(i);
    }
    public Object getReferenceFromString(String s) {
    	if (s == null) return null;
        return (Object)stringToRef.get(s);
    }
    
     
    public void addExclusion(String s) {
        excludeList.add(s);
    }
    
    public void removeExclusion(String s) {
        excludeList.remove(s);
    }
    public void addItem(Object c,String s) {
        refToString.put(c,s);
        stringToRef.put(s,c);
    }
    public void removeItem(Object c,String s) {
        refToString.remove(c);
        stringToRef.remove(s);
    }
    public void removeItem(String compStr,String s) {
        refToString.remove(compStr);
        stringToRef.remove(s);
    }
    public String[] getList() {
        
        Enumeration e = stringToRef.keys();
        ArrayList firstPass = new ArrayList();
        
        while(e.hasMoreElements()) {
            String val = (String)e.nextElement();
            boolean exclude = false;
            if (excludeList.size() > 0) {
                for (int j=0;j<excludeList.size();j++) {
                    if (val.equalsIgnoreCase((String)excludeList.get(j)) )
                        exclude = true;
                }
            }
            if (exclude)
                ;
            else {
                firstPass.add(val);
            }
        }
        String[] retArr = new String[firstPass.size()];
        for (int i=0;i<firstPass.size();i++) 
            retArr[i] = (String)firstPass.get(i);
        
        Arrays.sort(retArr);
        
        return retArr;
    }
    public LinkedList getLinkedList() {
        
        Enumeration e = stringToRef.keys();
        ArrayList firstPass = new ArrayList();
        
        while(e.hasMoreElements()) {
            String val = (String)e.nextElement();
            boolean exclude = false;
            if (excludeList.size() > 0) {
                for (int j=0;j<excludeList.size();j++) {
                    if (val.equalsIgnoreCase((String)excludeList.get(j)) )
                        exclude = true;
                }
            }
            if (exclude)
                ;
            else {
                firstPass.add(val);
            }
        }
        LinkedList retList = new LinkedList();
        for (int i=0;i<firstPass.size();i++) 
            retList.add(firstPass.get(i));
        
        return retList;
    }

    public TCComponent[] getReferences() {
    	
        Enumeration e = stringToRef.keys();
        ArrayList firstPass = new ArrayList();
        
        while(e.hasMoreElements()) {
            String val = (String)e.nextElement();
            boolean exclude = false;
            if (excludeList.size() > 0) {
                for (int j=0;j<excludeList.size();j++) {
                    if (val.equalsIgnoreCase((String)excludeList.get(j)) )
                        exclude = true;
                }
            }
            if (exclude)
                ;
            else {
                firstPass.add(val);
            }
        }
        TCComponent[] retArr = new TCComponent[firstPass.size()];
        for (int i=0;i<firstPass.size();i++) 
        	retArr[i] = (TCComponent)stringToRef.get((String)firstPass.get(i));
        
        return retArr;
    }
    
    public Hashtable getStringToRefTable() { return stringToRef; }
    public Hashtable getRefToStringTable() { return refToString; }
    public Vector    getOrderedVector()    { return orderedVector; }


	


}
