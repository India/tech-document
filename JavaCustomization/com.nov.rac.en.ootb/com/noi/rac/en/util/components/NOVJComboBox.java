package com.noi.rac.en.util.components;

import java.awt.Graphics;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

//import com.teamcenter.rac.icctstubs.booleanSeq_tHelper;
import com.teamcenter.rac.util.Painter;

public class NOVJComboBox extends JComboBox {

	private boolean required=false;;
	public NOVJComboBox() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NOVJComboBox(Object[] arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NOVJComboBox(Vector arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public NOVJComboBox(ComboBoxModel arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public void setText(String value) {
		for (int i=0;i<this.getItemCount();i++) {
			String item = (String)this.getItemAt(i);
			if (item.equals(value)) {
				this.setSelectedItem(item);
			}
		}
	}
	public String getText() {
		String str = new String((String)this.getSelectedItem());
		return str;
	}
	public void setRequired(boolean required)
	{
		this.required=required;
	}
	public boolean isRequired()
	{
		return this.required;
	}

	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		 if(required)
	            Painter.paintIsRequired(this, g);

	}
	
	
}
