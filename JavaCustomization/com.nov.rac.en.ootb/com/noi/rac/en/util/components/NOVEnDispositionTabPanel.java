package com.noi.rac.en.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import com.noi.rac.en.form.compound.component.NOVEnBomInfoPanel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
//import com.teamcenter.rac.aif.ApplicationDef; //TC10.1 Upgrade
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMViewRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

public class NOVEnDispositionTabPanel extends JPanel
{
	private TCComponent targetItemRev;
	private NOVEnBomInfoPanel bomInfoPanel;
	public iTextArea txtChangeDesc;
	public NOVLOVPopupButton inProcesslov = new NOVLOVPopupButton();
	public NOVLOVPopupButton inInventorylov = new NOVLOVPopupButton();
	public NOVLOVPopupButton assembledlov = new NOVLOVPopupButton();
	public NOVLOVPopupButton inFieldlov = new NOVLOVPopupButton();
	public JTextArea txtDispInstrux;
	public TCComponent dispcomp;
	NOVENDispositionPanelData paneldata;
	public boolean newRelease=false;
	_EngNoticeForm_Panel enPanel = null;
	private TCSession session;
	private TCTable bomTable; 
	boolean hasBOM=false;
	public JButton viewBOMBtn;
	public JButton refreshBOMBtn;
	public JButton classifyBtn;
	private Registry reg;
	protected boolean misGroup = false;
	public NOVEnDispositionTabPanel(_EngNoticeForm_Panel argENPanel,TCSession session, TCComponent dispcomp, boolean newRelease)
	{
		this.dispcomp=dispcomp;
		this.newRelease=newRelease;
		this.session=session;
		reg = Registry.getRegistry(this);
		paneldata=new NOVENDispositionPanelData(dispcomp, newRelease);
		enPanel = argENPanel; 
	}
	
	public void createUI()
	{
		//create the top panel based on newRelease
		//create the BomPanel
		JPanel mainPanel = new JPanel();
		JPanel middlePanel=new JPanel();
		JPanel changePanel=new JPanel();
		JPanel bomPanel=new JPanel();
		javax.swing.JLabel lblChageDesc = new JLabel();
		ImageIcon icon = reg.getImageIcon("locale.IMG");
		//TCPreferenceService prefrenceSrv = dispcomp.getSession().getPreferenceService();		
		//boolean isLocale = prefrenceSrv.isTrue(TCPreferenceService.TC_preference_all, "NOV_show_localization_button");

		
		if(!newRelease)
		{
			JPanel disposition = new JPanel(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
			JLabel lblDisposition = new JLabel(reg.getString("DISPOSITION.LABEL"));
			lblDisposition.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblInProcess = new JLabel(reg.getString("INPROCESS.LABEL"));
			lblInProcess.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblInventry = new JLabel(reg.getString("ININVENTORY.LABEL"));
			lblInventry.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblAssembled = new JLabel(reg.getString("ASSEMBLED.LABEL"));
			lblAssembled.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			JLabel lblInField = new JLabel(reg.getString("INFIELD.LABEL"));
			lblInField.setFont(new Font("Dialog",java.awt.Font.BOLD,12));

			lblChageDesc.setText(reg.getString("CHANGEDESC.LABEL"));
			
			disposition.add("1.1.left.top", lblDisposition);
			disposition.add("2.1.left.top", lblInProcess);
			disposition.add("2.2.left.top", inProcesslov);
			disposition.add("3.1.left.top", lblInventry);
			disposition.add("3.2.left.top", inInventorylov);
			disposition.add("4.1.left.top", lblAssembled);
			disposition.add("4.2.left.top", assembledlov);
			disposition.add("5.1.left.top", lblInField);
			disposition.add("5.2.left.top", inFieldlov);

			JPanel dispInstructions = new JPanel();
			//p3Panel.setLayout(new VerticalLayout(2 , 2 , 2 , 2 , 2));
			dispInstructions.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
			JLabel lblDispInstrux = new JLabel(reg.getString("DISP_INS.LABEL"));
			lblDispInstrux.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			txtDispInstrux = new JTextArea(30,30);
			JScrollPane scDispInstrux = new JScrollPane(txtDispInstrux);
			scDispInstrux.setPreferredSize(new Dimension(321,110));
			
						
			dispInstructions.add("1.1.left.top",lblDispInstrux);
			dispInstructions.add("2.1.left.top",scDispInstrux);
			/*if (isLocale) 
			{
				final JButton dispInxlocaleBtn = new JButton(icon);
				dispInxlocaleBtn.setPreferredSize(new Dimension(20,20));
				try 
				{
					dispInxlocaleBtn.addActionListener(new ActionListener() 
					{
						public void actionPerformed(ActionEvent actEvt) 
						{
							NOVLocaleDialog localeDlg = null;
							try 
							{
								localeDlg = new NOVLocaleDialog(dispcomp.getTCProperty("dispinstruction"));
							}
							catch (TCException e) 
							{
								e.printStackTrace();
							}							
							//localeDlg.setSize(350, 325);
							int x = dispInxlocaleBtn.getLocationOnScreen().x- localeDlg.getWidth();
							int y = dispInxlocaleBtn.getLocationOnScreen().y- localeDlg.getHeight();
							localeDlg.setLocation(x, y);
							localeDlg.setVisible(true);
						}
					});
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				dispInstructions.add("2.2.left.top",dispInxlocaleBtn);	
			}*/
			middlePanel.add(disposition);
			middlePanel.add(dispInstructions);
		}
		else
		{
			//@Harshada -  modified for label from "Reason for Release" to "Instructions for Release"
			lblChageDesc.setText(reg.getString("INSFORRELEASE.LABEL"));
		}

		classifyBtn = new JButton(reg.getString("CLASSIFYBUTTON.LABEL"));
		classifyBtn.addActionListener(new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				enPanel.saveForm();
				//ApplicationDef appDef =  AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey(reg.getString("CLASSIFICATIONApps.APPS"));
				//appDef.openApplication(new InterfaceAIFComponent[]{targetItemRev});
				IOpenService openPerspective = PerspectiveDefHelper.getOpenService(reg.getString("CLASSIFICATIONApps.APPS"));
                openPerspective.open(new InterfaceAIFComponent[]{targetItemRev}); //TC10.1 Upgrade
			}
		});
		
		viewBOMBtn =  new JButton(reg.getString("VIEWBOMBUTTON.LABEL"));
		viewBOMBtn.addActionListener(new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				enPanel.saveForm();
				//ApplicationDef appDef =  AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey(reg.getString("PSEApps.APPS"));
				//appDef.openApplication(new InterfaceAIFComponent[]{targetItemRev});
				IOpenService openPerspective = PerspectiveDefHelper.getOpenService(reg.getString("PSEApps.APPS"));
                openPerspective.open(new InterfaceAIFComponent[]{targetItemRev});//TC10.1 Upgrade
			}
		});
		
		if(hasBOM)
		{
			ImageIcon refreshIcon =reg.getImageIcon("refresh.IMG");
			refreshBOMBtn = new JButton(refreshIcon);
			refreshBOMBtn.setPreferredSize(new Dimension(20,22));
			refreshBOMBtn.setToolTipText(reg.getString("REFRESHBOM.TOOLTIP"));
			refreshBOMBtn.addActionListener(new ActionAdapter()
			{
				public void actionPerformed(ActionEvent arg0) 
				{
					if (bomInfoPanel!=null) 
					{
						bomInfoPanel.bomInfoTable.removeAllRows();
						bomInfoPanel.bomInfoTable.revalidate();
						bomInfoPanel.compareBOM(true);
					}
				}
			});
			
			bomPanel.setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
			JPanel bomBtnPanel = new JPanel(new BorderLayout());
			
			JLabel lblIntendedBOM = new JLabel(reg.getString("BOM.LABEL"));
			lblIntendedBOM.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			bomBtnPanel.setPreferredSize(new Dimension(550,30));
			bomBtnPanel.add(BorderLayout.WEST,lblIntendedBOM);
			JPanel btnPnl = new JPanel();
			btnPnl.add(refreshBOMBtn);
			btnPnl.add(viewBOMBtn);
			btnPnl.add(classifyBtn);
			
			bomBtnPanel.add(BorderLayout.EAST,btnPnl);
			bomPanel.add("1.1.left.top",bomBtnPanel);
			bomTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			JScrollPane sctblIntendedBOM = new JScrollPane(bomTable);
			//sctblIntendedBOM.setPreferredSize(new Dimension(550,110));
			//@Harshada - Modified for checking more number of rows in A table
			if(bomTable.getRowCount()>5)
				sctblIntendedBOM.setPreferredSize(new Dimension(550,210));
			else
				sctblIntendedBOM.setPreferredSize(new Dimension(550,100));
			
			bomPanel.add("2.1.left.top",sctblIntendedBOM);
		}
		else
		{
			
			bomPanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
			JLabel lblIntendedBOM = new JLabel(reg.getString("NOBOM.LABEL"));
			lblIntendedBOM.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
			
			JPanel bomBtnPanel = new JPanel(new BorderLayout());
			bomBtnPanel.setPreferredSize(new Dimension(550,30));
			bomBtnPanel.add(BorderLayout.WEST,lblIntendedBOM);
			JPanel btnPnl = new JPanel();
			btnPnl.add(viewBOMBtn);
			btnPnl.add(classifyBtn);
			bomBtnPanel.add(BorderLayout.EAST,btnPnl);
			bomPanel.add("1.1.left.top",bomBtnPanel);
			//bomPanel.add("1.1.left.top",lblIntendedBOM);
		}

		changePanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
		
		lblChageDesc.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		txtChangeDesc = new iTextArea(30,30);
		//@Harshada - modified for removing mandetory field's requirement from Tab Panel 
		//txtChangeDesc.setRequired(true);
		JScrollPane scChangeDesc = new JScrollPane(txtChangeDesc);
		scChangeDesc.setPreferredSize(new Dimension(525,100));
		
		changePanel.add("1.1.left.top",lblChageDesc);
		changePanel.add("2.1.left.top",scChangeDesc);
		/*if (isLocale) 
		{
			final JButton localeBtn = new JButton(icon);
			localeBtn.setPreferredSize(new Dimension(20,20));
			try 
			{
				localeBtn.addActionListener(new ActionListener() 
				{
					public void actionPerformed(ActionEvent actEvt) 
					{
						NOVLocaleDialog localeDlg = null;
						try 
						{
							localeDlg = new NOVLocaleDialog(dispcomp.getTCProperty("changedesc"));
						}
						catch (TCException e) 
						{
							e.printStackTrace();
						}							
						//localeDlg.setSize(350, 325);
						int x = localeBtn.getLocationOnScreen().x- localeDlg.getWidth();
						int y = localeBtn.getLocationOnScreen().y- localeDlg.getHeight();
						localeDlg.setLocation(x, y);
						localeDlg.setVisible(true);
					}
				});
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			changePanel.add("2.2.left.top",localeBtn);	
		}*/
		
		mainPanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
		//mainPanel.setPreferredSize(new Dimension(550, 330));
		if(newRelease)
		{
			mainPanel.add("1.1.left.top", changePanel);	
			if (!misGroup)
				mainPanel.add("2.1.left.top", bomPanel);
		}
		else
		{
			mainPanel.add("1.1.left.top", changePanel);
			mainPanel.add("2.1.left.top", middlePanel);
			if (!misGroup)
				mainPanel.add("3.1.left.top", bomPanel);
		}

		JScrollPane mainPSc = new JScrollPane(mainPanel);
		this.add(mainPSc);
		loadData();
	}

	public void setTargetItemRev(TCComponent targetItemRevision)
	{
		targetItemRev = targetItemRevision;
	}

	public boolean isMandatoryFieldsFilled()
	{
		String changedesc =  txtChangeDesc.getText();
		
		/*if (changedesc.trim().length()>0) 
		{
			return true;
		}*/
		return true;
	}
	public void loadData()
	{		
		if(newRelease)
		{
			txtChangeDesc.setText(paneldata.getStrchnageReason());
		}else
		{
			try
			{
				String enOwnGrp = enPanel.Enform.getProperty("owning_group");

				txtChangeDesc.setText(paneldata.getStrchnageReason());
				txtDispInstrux.setText(paneldata.getStrdispInstruction());

				String sDefaultValue = null;
				String sLOVName = null;

				if(enOwnGrp.contains(reg.getString( "PCG.Group" )))
				{		
					sLOVName = reg.getString( "ENDisposition_PCG.LOV" );
					sDefaultValue = reg.getString( "PCGDefault.STATUS" );					
				}
				else
				{	
					sLOVName = reg.getString( "ENDisposition.LOV" );								
					sDefaultValue = reg.getString( "NA.STATUS" );				
				}	
				TCComponentListOfValues lovalues = TCComponentListOfValuesType.findLOVByName(session,sLOVName);
				inProcesslov.setLovComponent(lovalues);	
				inFieldlov.setLovComponent(lovalues);
				inInventorylov.setLovComponent(lovalues);
				assembledlov.setLovComponent(lovalues);

				setDispositiondata(sDefaultValue);
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
	}
	private void setDispositiondata(String sDeafultValue)
	{
		String curInProcess = paneldata.getStrinProcess();
		String curInField = paneldata.getStrinfield();
		String curInInventory = paneldata.getStrinInventory();
		String curpurchased = paneldata.getStrassembled();
		
		if( (curInProcess == null) || (curInProcess.trim().length() <=0))
		{
			curInProcess = sDeafultValue;		
		}		

		if( (curInField == null) || (curInField.trim().length() <=0))
		{
			curInField = sDeafultValue;		
		}	

		if( (curInInventory == null) || (curInInventory.trim().length() <=0))
		{
			curInInventory = sDeafultValue;		
		}

		if( (curpurchased == null) || (curpurchased.trim().length() <=0))
		{
			curpurchased = sDeafultValue;		
		}
		
		inProcesslov.setText( curInProcess);
		inFieldlov.setText( curInField);
		inInventorylov.setText( curInInventory);
		assembledlov.setText(curpurchased);
	}

	public void saveData(String futureStatus)
	{
		try
		{
			if(newRelease)
			{
				String chnageReason=txtChangeDesc.getText();
				paneldata.setDispClassData(chnageReason, futureStatus);
			}else
			{
				String chnageReason=txtChangeDesc.getText();
				String inProcess=inProcesslov.getText();
				String inInventory=inInventorylov.getText();
				String assembled=assembledlov.getText();
				String infield=inFieldlov .getText();
				String dispInstruction=txtDispInstrux.getText();
				paneldata.setDispClassData(chnageReason, inProcess, inInventory, assembled, infield, dispInstruction, futureStatus);
			}
		}
		catch(Exception te)
		{
			System.out.println(te);
		}
	}


	public void createBOMPanel(TCComponent revcomp)
	{
		if(revcomp!=null)
		{
		try
		{			
			/*if(!newRelease)
			{*/			
			AIFComponentContext[] itemRevisionComponents = ((TCComponentItemRevision)revcomp).getChildren();
			for (int i=0;i<itemRevisionComponents.length;i++)
			{
				if (itemRevisionComponents[i].getComponent() instanceof TCComponentBOMViewRevision) 
				{					
					hasBOM=true;
					break;
				}
			}	
			if(hasBOM)
			{
				//get the previous Revision;
				AIFComponentContext[] revItems=((TCComponentItemRevision)revcomp).getItem().getChildren("revision_list");
				TCComponentItemRevision prevrev=null;
				/*if(revItem.length>1)
					{*/
				prevrev=getPreiviousRev(revItems, (TCComponentItemRevision)revcomp);
				bomInfoPanel = new NOVEnBomInfoPanel(prevrev, (TCComponentItemRevision)revcomp);
				bomInfoPanel.compareBOM(true);
				bomInfoPanel.setVisible(false);
				bomTable=bomInfoPanel.bomInfoTable;
				//}
			}
			//}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		}
	}

	public TCComponentItemRevision getPreiviousRev(AIFComponentContext[] allrevs, TCComponentItemRevision rev)
	{
		TCComponentItemRevision prevRev=null;
		try
		{
			Date curRevDate=rev.getDateProperty("creation_date");
			//Date currentdate=DateFormat.
			prevRev=(TCComponentItemRevision)allrevs[0].getComponent();	
			Date prevDate=prevRev.getDateProperty("creation_date");
			for(int k=1;k<allrevs.length;k++)
			{
				Date compareDate=((TCComponentItemRevision)allrevs[k].getComponent()).getDateProperty("creation_date");
				if((compareDate.before(curRevDate))&&(compareDate.after(prevDate)))
				{
					prevDate=compareDate;
					prevRev=(TCComponentItemRevision)allrevs[k].getComponent();
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		return prevRev;
	}

	class NOVENDispositionPanelData
	{		
		TCProperty chnageReason;
		TCProperty inProcess;
		TCProperty inInventory;
		TCProperty assembled;
		TCProperty infield;
		TCProperty dispInstruction;
		//TCProperty itemId;
		TCProperty futureStatus;
		TCProperty currentStatus;

		String strchnageReason;
		String strinProcess;
		String strinInventory;
		String strassembled;
		String strinfield;
		String strdispInstruction;
		String stritemId;
		String strfutureStatus;
		String strcurrentStatus;
		//TCComponent dispcomp;

		public NOVENDispositionPanelData(TCComponent dispcomp, boolean newRelease) {			

			try
			{
				String dispProps[] = dispcomp.getProperties(new String[]{"changedesc", "currentstatus", "futurestatus"});
				if(newRelease)
				{
					strchnageReason = dispProps[0];
				}else
				{
					dispProps = dispcomp.getProperties(new String[]{"changedesc", "currentstatus", "futurestatus",
							                                        "inprocess", "ininventory", "assembled", "infield",
							                                        "dispinstruction" });
					strchnageReason = dispProps[0];
					strinProcess=dispProps[3];
					strinInventory=dispProps[4];;
					strassembled=dispProps[5];
					strinfield=dispProps[6];
					strdispInstruction=dispProps[7];;	
				}
				strcurrentStatus=dispProps[1];
				strfutureStatus=dispProps[2];
			}
			catch ( TCException tcEx )
			{
				tcEx.printStackTrace();
			}
		}

		public String getStrcurrentStatus() {
			return strcurrentStatus;
		}

		public String getStrchnageReason() {
			return strchnageReason;
		}

		public String getStrinProcess() {
			return strinProcess;
		}

		public String getStrinInventory() {
			return strinInventory;
		}

		public String getStrassembled() {
			return strassembled;
		}

		public String getStrinfield() {
			return strinfield;
		}

		public String getStrdispInstruction() {
			return strdispInstruction;
		}	

		public String getStrfutureStatus() {
			return strfutureStatus;
		}	

		public void setDispClassData(String strchnageReason,
				String strinProcess, String strinInventory,
				String strassembled, String strinfield,
				String strdispInstruction,
				String strfutureStatus) {

			try
			{
				//
				/*chnageReason.setStringValue(strchnageReason);
				inProcess.setStringValue(strinProcess);
				inInventory.setStringValue(strinInventory);
				assembled.setStringValue(strassembled);
				infield.setStringValue(strinfield);
				dispInstruction.setStringValue(strdispInstruction);

				futureStatus.setStringValue(strfutureStatus);
				TCProperty[] prop;
				if(currentStatus.isModifiable())
				{
					prop=new TCProperty[8];
					prop[7]=currentStatus;
				}
				else
					prop=new TCProperty[7];
				prop[0]=chnageReason;
				prop[1]=inProcess;
				prop[2]=inInventory;
				prop[3]=assembled;
				prop[4]=infield;
				prop[5]=dispInstruction;			
				prop[6]=futureStatus;				
				dispcomp.setTCProperties(prop);	
				dispcomp.lock();
				dispcomp.save();
				dispcomp.unlock();*/
				Object inputObjs[] = new Object[3];
				inputObjs[0] = new TCComponent[]{dispcomp};
				inputObjs[1] = new String[]{"changedesc", "inprocess", "ininventory",
						                    "assembled", "infield", "dispinstruction",
						                    "futurestatus"};
				inputObjs[2] = new String[]{strchnageReason, strinProcess, strinInventory, 
						                    strassembled, strinfield, strdispInstruction,
						                    strfutureStatus};
				TCUserService usrService = session.getUserService();
				int returnVal = (Integer)usrService.call("NATOIL_update_pom_string_props", inputObjs );
				if ( returnVal != 0 )
				{
					throw new Exception("Could not update target disposition data");
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		public void setDispClassData(String strchnageReason,				
				String strfutureStatus) {

			try
			{
				//
				/*chnageReason.setStringValue(strchnageReason);
				futureStatus.setStringValue(strfutureStatus);
				//currentStatus.setStringValue(strcurrentStatus);
				TCProperty[] prop;
				if(currentStatus.isModifiable())
				{
					prop=new TCProperty[3];
					prop[2]=currentStatus;
				}
				else
					prop=new TCProperty[2];				
				prop[0]=chnageReason;							
				prop[1]=futureStatus;
				//prop[2]=currentStatus;
				dispcomp.setTCProperties(prop);	
				dispcomp.lock();
				dispcomp.save();
				dispcomp.unlock();*/
				
				Object inputObjs[] = new Object[3];
				inputObjs[0] = new TCComponent[]{dispcomp};
				inputObjs[1] = new String[]{"futurestatus","changedesc"};
				inputObjs[2] = new String[]{strfutureStatus, strchnageReason};
				TCUserService usrService = session.getUserService();
				int returnVal = (Integer)usrService.call("NATOIL_update_pom_string_props", inputObjs );
				if ( returnVal != 0 )
				{
					throw new Exception("Could not update target disposition data");
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}
}




