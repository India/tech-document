package com.noi.rac.en.util.components;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import com.noi.NationalOilwell;
import com.noi.rac.en.form.compound.commands.envalidation.NOVEnValidator;
import com.noi.rac.en.form.compound.data._EngNoticeForm_DC;
import com.noi.rac.en.form.compound.data._StopForm_DC;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.mission.helper.NOVMissionHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVEnTargetsPanel extends JPanel implements ActionListener{
	
	public NOVECNTargetsTable targetsTable;
	public JLabel lastValidationDate=new NOIJLabel();
	public Date validationDate;
	public JButton validateButton;
	public JButton oracleNotify;
	public JButton helpEn;
	private StringBuffer                  call            = new StringBuffer("cmd /c start ");
	public _EngNoticeForm_Panel enFormPanel;
	protected INOVCustomFormProperties novFormProperties;
	private TCPreferenceService prefServ ;
	String helpUrl = "";
	private Registry reg = Registry.getRegistry(this);
	public NOVEnTargetsPanel(_EngNoticeForm_Panel enFormPanel, INOVCustomFormProperties novFormProperties)
	{
		boolean misGroup = NOVMissionHelper.isGroup(reg.getString("misPref.NAME"),novFormProperties);
		boolean totGroup = NOVMissionHelper.isGroup(reg.getString("totPref.NAME"),novFormProperties);
		boolean noValOraGroup = NOVMissionHelper.isGroup(reg.getString("noValOraPref.NAME"),novFormProperties);
		this.novFormProperties=novFormProperties;
		setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));
		this.enFormPanel=enFormPanel;
		validateButton=new JButton(reg.getString("VALIDATE.LABEL"));
		if(enFormPanel.getProcess()==null)
			validateButton.setEnabled(false);
		validateButton.addActionListener(this);
		
		oracleNotify = new JButton(reg.getString("NOTIFYORACLE.LABEL"));
		oracleNotify.addActionListener(this);
		
		helpEn = new JButton(reg.getString("HELPSTR.LABEL"));//"Help");
		helpEn.addActionListener(this);
		prefServ = enFormPanel.getSession().getPreferenceService();
		helpUrl = prefServ.getString(
		TCPreferenceService.TC_preference_site, reg.getString("NOI_NewENHelp.PREF"));
		
		boolean bIsOracleButtonEnable = false;
		TCComponentProcess enProcess = enFormPanel.getProcess();
		TCComponentTask enRootTask;
		try 
		{
			if(enFormPanel.getProcess()!=null && enFormPanel.Enform != null)
			{
				String sChangeType = enFormPanel.Enform.getTCProperty("nov4_change_type").getStringValue();
				String[] editableTypes=reg.getStringArray("EnableNotifyOracleButton.CHANGETYPE");
				if((sChangeType == null || sChangeType.isEmpty()) || Arrays.asList(editableTypes).contains(sChangeType))
				{
					enRootTask = enProcess.getRootTask();
					TCComponent[] targets=enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
					for(int i=0;i<targets.length;i++)
					{
						if(targets[i] instanceof TCComponentItemRevision)
						{
							if(((TCComponentItemRevision)targets[i]).getItem().getProperty("release_status_list").contains("ACTIVE"))
							{
								bIsOracleButtonEnable = true;
								break;
							}
						}
					}
				}
			}			
		} catch (TCException e) {
			// TODO Auto-generated catch blockSt
			e.printStackTrace();
		}
		
		oracleNotify.setEnabled(bIsOracleButtonEnable);
		
		//top Panel
		JPanel topPanel=new JPanel();
		topPanel.add(new NOIJLabel(reg.getString("ENGNOTICETGTS.LABEL")));
		if (!(misGroup || noValOraGroup))
		{
			topPanel.add(oracleNotify);
			topPanel.add(validateButton);
		}
		topPanel.add(helpEn);
		
		//get the last saved validation date;
		DateButton valid=(DateButton)novFormProperties.getProperty("lastvalidatedon");
		SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
		if(valid.getDate()!=null)
		{
        String validDateString = sdfDestination.format(valid.getDate()); 
        lastValidationDate.setText(validDateString);
		}
		topPanel.add(lastValidationDate);		
		String columnNames[] ;
		if (misGroup || totGroup){
			columnNames = new String[3] ;
			columnNames[0]=reg.getString("PartID.TYPE");
			columnNames[1]=reg.getString("PartName.TYPE");
			columnNames[2]=reg.getString("Rev.TYPE");
			
		}
		else{
			columnNames = new String[5] ;

			columnNames[0]=reg.getString("PartID.TYPE");
			columnNames[1]=reg.getString("PartName.TYPE");
			columnNames[2]=reg.getString("Rev.TYPE");
			columnNames[3]=reg.getString("StartStatus.TYPE");
			columnNames[4]=reg.getString("FutureStatus.TYPE");
			
			
		}
		//String columnNames[]={reg.getString("PartID.TYPE"), reg.getString("PartName.TYPE"), reg.getString("Rev.TYPE"), reg.getString("StartStatus.TYPE"), reg.getString("FutureStatus.TYPE")};
		
		targetsTable=new NOVECNTargetsTable(enFormPanel, enFormPanel.getSession(), columnNames);
		targetsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane=new JScrollPane(targetsTable);
		scrollPane.setPreferredSize(new Dimension(630,150));
		
		this.add("1.1.left.top",topPanel);	
		this.add("2.1.center.center",scrollPane);	
		
		TitledBorder tb = new TitledBorder("");		
		scrollPane.setBorder(tb);
		
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==validateButton)
		{
			new NOVEnValidator(enFormPanel, this);
			//get the time stamp of validation and save it in the form variable
			Calendar calendar=Calendar.getInstance();
			validationDate=calendar.getTime();
			((DateButton)novFormProperties.getProperty("lastvalidatedon")).setDate(validationDate);
			 SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy hh:mm");             
             String validDateString = sdfDestination.format(validationDate); 
             lastValidationDate.setText(validDateString);   
			
		}
		
		if(ae.getSource()==helpEn)
        {
			if(helpUrl==null || helpUrl.length()==0)
			{
				//helpUrl = "http://srvhouplmt01:7081/cetgws/Standard-ENform.pdf";
				MessageBox.post(NationalOilwell.NOHELP,
						NationalOilwell.SEEADMIN, MessageBox.ERROR);
			}
			else
			{
				try 
				{
					//Runtime.getRuntime().exec(call.toString());
					Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+helpUrl);
				} 
				catch (Exception ex)
				{
					ex.printStackTrace();
					MessageBox.post(NationalOilwell.NOHELP,
							NationalOilwell.SEEADMIN, MessageBox.ERROR);
				}
			}
			return;
		}
		
		else if(ae.getSource()==oracleNotify)
		{
			TCProperty isOracleNotifyProperty;
			try 
			{
				
				TCComponentProcess enProcess = enFormPanel.getProcess();
				TCComponentTask enRootTask = enProcess.getRootTask();
				TCComponentProcess currentJOB = enProcess.getCurrentJob();
				String processUID = enRootTask.getUid();
				
				TCComponent[] targets=enRootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
				
				for(int i=0;i<targets.length;i++)
				{
					if(targets[i] instanceof TCComponentForm)
					{
						if(targets[i].getTCProperty("isOracleNotify").getIntValue() == 0)
						{
							//call userExit to create the Disposition components and return the array
							TCUserService us = enFormPanel.getSession().getUserService();       	
							Object obj[]=new Object[3];       	
							obj[0]=targets.length;
							obj[1]=targets;
							obj[2]=processUID;
							us.call("NATOIL_OracleNotify", obj);
							
							oracleNotify.setEnabled(false);
						}
					}
				}
			} 
			catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
	}

}
