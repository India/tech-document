package com.noi.rac.en.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableColumnModel;

import com.noi.rac.en.form.compound.commands.envalidation.NOVEnValidator;
import com.noi.rac.en.form.compound.panels.Nov4_SupersedeForm_Panel;
import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.noi.rac.en.form.compound.panels._StopForm_Panel;
import com.noi.rac.en.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.en.util.components.NOVSupersedeTargetsTable;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVSupersedeTablePanel  extends JPanel
{

	
	public NOVSupersedeTargetsTable targetsTable;
	public JLabel lastValidationDate=new NOIJLabel();
	public Date validationDate;
	public JButton validateButton;
	public Nov4_SupersedeForm_Panel stopFormPanel;
	private INOVCustomFormProperties novFormProperties;
	private Registry reg;
	
	AIFTableModel tableModel;//Added By Sandip
	
	public NOVSupersedeTablePanel(Nov4_SupersedeForm_Panel stopFormPanel, INOVCustomFormProperties novFormProperties)
	{
		this.novFormProperties=novFormProperties;
		setLayout(new PropertyLayout());
		this.stopFormPanel=stopFormPanel;
		reg = Registry.getRegistry(this);
		/*validateButton=new JButton("Validate");
		validateButton.addActionListener(this);*/
		//top Panel
		JPanel topPanel=new JPanel(new BorderLayout());
		//topPanel.add(new NOIJLabel(reg.getString("Affected.LVL") ));
		//usha - 2064
		topPanel.add(new NOIJLabel(reg.getString("AffectedSUP.LBL") ), BorderLayout.LINE_START);
		//topPanel.setPreferredSize(new Dimension(620, 20));
		//topPanel.add(validateButton);
		
		//get the last saved validation date;
	/*	DateButton valid=(DateButton)novFormProperties.getProperty("lastvalidatedon");
		SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
		if(valid.getDate()!=null)
		{
        String validDateString = sdfDestination.format(valid.getDate()); 
        lastValidationDate.setText(validDateString);
		}
		topPanel.add(lastValidationDate);*/
		//add(topPanel, BorderLayout.NORTH);
		
		String columnNames[]=reg.getString("columnNames1.LIST").split(",");
		targetsTable=new NOVSupersedeTargetsTable(stopFormPanel, stopFormPanel.getSession(), columnNames);
		
		tableModel=(AIFTableModel) targetsTable.getModel();
		
		targetsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane=new JScrollPane(targetsTable);
		scrollPane.setPreferredSize(new Dimension(620,155));
		scrollPane.setVisible(true);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.add(scrollPane);
		bottomPanel.setPreferredSize(new Dimension(620, 160));
		//add(scrollPane, BorderLayout.CENTER);	
		
		this.add("1.1.left.top",topPanel);	
		this.add("2.1.center.center",bottomPanel);	
		
		//check if the form is checked out to enable validate button
		/*try
		{
		if(enFormPanel.Enform.getTCProperty("checked_out_date").getDateValue()==null)
			validateButton.setEnabled(false);
		else
			validateButton.setEnabled(true);
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}*/
	}
	/*public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==validateButton)
		{
			NOVEnValidator validator=new NOVEnValidator(enFormPanel, this);
			//get the time stamp of validation and save it in the form variable
			Calendar calendar=Calendar.getInstance();
			validationDate=calendar.getTime();
			((DateButton)novFormProperties.getProperty("lastvalidatedon")).setDate(validationDate);
			 SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy hh:mm");             
             String validDateString = sdfDestination.format(validationDate); 
             lastValidationDate.setText(validDateString);   
			
		}
	}*/

	public AIFTableModel getTableModel(){
		 return tableModel;
	    }

}