package com.noi.rac.en.util.components;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.noi.rac.en.form.compound.panels.NOV4RigsECRForm_Panel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RelatedEnTableModel implements TableModel {

	private Object[][] data;
	Registry reg;
	private String[] columnNames;
	NOV4RigsECRForm_Panel ECRPanel;

	

	public RelatedEnTableModel(NOV4RigsECRForm_Panel ecrPanel) {
		super();
		reg = Registry.getRegistry(this);
		ECRPanel = ecrPanel;
		columnNames = new String[] { reg.getString("EN.STR"),reg.getString("ReleaseStatus.STR") };
		initData();
	} 

	private void initData() {
		// TODO Auto-generated method stub
		AIFComponentContext[] ecrRelatedEnArr = ECRPanel.getRelatedEn();
		//String[] reqdRowData = { "object_name", "process_stage_list" };
		String[] reqdRowData = { "object_name", "release_status_list" };
		Object[][] retrievedRowData = new Object[ecrRelatedEnArr.length][reqdRowData.length];

		for (int index = 0; index < ecrRelatedEnArr.length; ++index) {
			try {
				String status = "";
				retrievedRowData[index][0] = ecrRelatedEnArr[index].getComponent().getProperty("object_name");
				if(ecrRelatedEnArr[index].getComponent().getProperty("release_status_list").equalsIgnoreCase("Released") )
				{
					status = reg.getString( "Complete.stage" );
				}
				else if(ecrRelatedEnArr[index].getComponent().getProperty("process_stage_list") != null)
				{
					status = reg.getString( "InProgress.stage" ); 
				}
				retrievedRowData[index][1] = status;
				//retrievedRowData[index][1] = ecrRelatedEnArr[index].getComponent().getProperty("release_status_list");
				//retrievedRowData[index][1] = ecrRelatedEnArr[index].getComponent().getProperty("process_stage_list");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();}						
		}
		this.data = retrievedRowData;
		
	}

	/*public void initData() {
		try {
			TCComponent[] ecrRelatedEnArr = ECRPanel.getRelatedEn();
			//String[] reqdRowData = { "nov4_en_number", "nov4_item_id" };
			String[] reqdRowData = { "nov4_en_number", "release_status_list" };
			String[][] retrievedProp = TCComponentType.getPropertiesSet(ecrRelatedEnArr, reqdRowData);
			Object[][] retrievedRowData = new Object[ecrRelatedEnArr.length][reqdRowData.length];

			for (int index = 0; index < ecrRelatedEnArr.length; ++index) {

				retrievedRowData[index][0] = retrievedProp[index][0];
				retrievedRowData[index][1] = retrievedProp[index][1];
			}
			this.data = retrievedRowData;

		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data == null ? 0 : data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

}
