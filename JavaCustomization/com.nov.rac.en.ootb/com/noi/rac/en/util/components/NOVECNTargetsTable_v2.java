package com.noi.rac.en.util.components;

import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.noi.rac.en.mission.helper.NOVMissionHelper_v2;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

import java.awt.Color;
import java.awt.Component;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class NOVECNTargetsTable_v2 extends JTreeTable
{
	private static final long serialVersionUID = 1L;
	NOVEnTreeTableModel treeTableModel;	
	private _EngNoticeForm_Panel_v2 enformPanel;
	private TCSession session;	
	public HashMap<TCComponent, String> futureStatMap ; 
	public HashMap<TCComponent, String> currentStatMap ; 
	private Vector<TCComponent> TargetsTobremoved=new Vector<TCComponent>();
	private Registry reg;
	Boolean m_isReleased = false;
	boolean m_isMissionGrp = false; 

	String strObjectType = null;
	String strDocRevType = null;
	String strNov4PartRev = null;
	String strPurchasedPartRev = null;
	String strNovEngRev = null;
	String strRddType = null;

	public NOVECNTargetsTable_v2( final _EngNoticeForm_Panel_v2 enformPanel, TCSession tcSession,
			String columnNames[], boolean isReleased )
	{
		super(tcSession);		
		this.enformPanel=enformPanel;
		this.session=tcSession;
		this.reg=Registry.getRegistry(this);
		m_isReleased = isReleased;
			
		strObjectType = reg.getString( "objType.PROP" );
		strDocRevType = reg.getString( "DocRev.TYPE" );
		strNov4PartRev = reg.getString( "Nov4PartRev.TYPE" );
		strPurchasedPartRev = reg.getString( "PurchasedRev.TYPE" );
		strNovEngRev = reg.getString( "NonEngRev.TYPE" );
		strRddType = reg.getString( "rdd.TYPE" );
			
		futureStatMap = new HashMap<TCComponent, String>();
		currentStatMap=new HashMap<TCComponent, String>();
		treeTableModel = new NOVEnTreeTableModel(columnNames);
		setModel(treeTableModel);

		for(int i = 0; i < columnNames.length; i++)
		{
			TableColumn col = new TableColumn(i);
			addColumn(col);
		}
		getTree().setCellRenderer(new TargetIdRenderer(this));
		//set Renderer and editor for "future status" column
		//RootCellRenderer
		getColumnModel().getColumn(1).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(2).setCellRenderer(new RootCellRenderer());
		if (getColumnModel().getColumnCount()>3){
			getColumnModel().getColumn(3).setCellRenderer(new RootCellRenderer());
			//getColumnModel().getColumn(4).setCellRenderer(new RootCellRenderer());
			//1839-check if EN owning group belongs to Mission group. If yes, add that combo box.
			
			try
			{
				TCPreferenceService prefServ = session.getPreferenceService();
		        String[] missionGrps =prefServ.getStringArray(TCPreferenceService.TC_preference_all, 
		        		reg.getString( "misPref.NAME"));
		        				//"_NOV_Mission_item_creation_groups_" );
		        		
		        String enOwnGrp = enformPanel.Enform.getProperty("owning_group");
		        for(int k=0; k<missionGrps.length; k++)
		        {
		        	if(missionGrps[k].equalsIgnoreCase(enOwnGrp))
		        	{
		        		m_isMissionGrp = true;
		        		break;
		        	}
		        }
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JComboBox ststuscombo = null;
			TableColumn col5=getColumnModel().getColumn(4);
			if(m_isMissionGrp)
			{		
				//String[] status =  {"Standard", "Cancelled", "Feature", "ENG", "Type5" };
				String[] status =  {reg.getString( "CANCELLED.TYPE" ), reg.getString( "CONTROL.TYPE" ), 
									reg.getString( "CUSTOM.TYPE" ), reg.getString( "DISCONT.TYPE" ), 
									reg.getString( "ENGINEERING.TYPE" ), reg.getString( "FEATURE.TYPE" ), 
									reg.getString( "INTRO.TYPE" ), reg.getString( "MARKETING.TYPE" ), 
									reg.getString( "NONSTD.TYPE" ), reg.getString( "PHASEOUT.TYPE" ), 
									reg.getString( "STANDARD.TYPE" ), reg.getString( "VOID.TYPE" )
									};
				col5.setCellRenderer(new ComboboxCellRenderer(status));            
				ststuscombo=new JComboBox(status);		
			}
			else
			{
				String[] status =	{ reg.getString( "MFG.TYPE" ), reg.getString( "SWAP.TYPE" ) };
				col5.setCellRenderer(new ComboboxCellRenderer(status));            
				ststuscombo=new JComboBox(status);
			}
			
			if( m_isReleased )
				ststuscombo.setEnabled( false );
			col5.setCellEditor(new ComboboxCellEditor(ststuscombo));	
			setAutoResizeMode(JTreeTable.AUTO_RESIZE_ALL_COLUMNS);	
		}
		loadTabledata( enformPanel.Entargets );
		addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent mouseEvt)
			{
				Object source = mouseEvt.getSource();
				int selRow = ((NOVECNTargetsTable_v2)source).getSelectedRow();
				/*if(SwingUtilities.isRightMouseButton(mouseEvt) && (mouseEvt.getClickCount() == 1)&& selRow >0)
				{
					JPopupMenu popupMenu = new JPopupMenu();
					JMenuItem item = new JMenuItem();
					String initialText = "<html><b><font=8 color=red>Remove Target</font></b>";
					item.setText(initialText);
					item.addActionListener(new ActionListener() {

						public void actionPerformed(ActionEvent e )
						{				
							//Commented by Usha - 22/06/2011
							//Confirmation dialog
							int row=NOVECNTargetsTable_v2.this.getSelectedRow();
							if(row!=-1)
							{
								int rows=((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).getChildCount();//NOVECNTargetsTable.this.getRowCount();
								if(rows==1)
								{
									//PlatformUI.
									MessageBox.post(reg.getString("deleteTargets.MSG"), reg.getString("warning.STR"), MessageBox.WARNING);
									return;
								}
								int response = JOptionPane.showConfirmDialog(null,
										reg.getString("response.MSG"),reg.getString("confirm.STR"),
										JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
								if ( response == JOptionPane.YES_OPTION )
								{
									getTree().getSelectionPath();								
									removeTargetsandClenup(row);
								}
							}
						}
					});
					popupMenu.add(item);
					popupMenu.show((java.awt.Component)source, mouseEvt.getX(), mouseEvt.getY());
				}*/
					
				if( mouseEvt.getClickCount() == 2 && selRow > 0 )
				{
					TCComponent targetItemRev = ( ( NovTreeTableNode )NOVECNTargetsTable_v2.this.getNodeForRow( selRow ) ).context;
					enformPanel.createDispTabPanel( targetItemRev, m_isReleased );
				}	
			}			 
		});

	}
    
	public void removeTargetsandClenup(int row)
	{
		try
		{
			//User exit for the remove action  
			TCComponent remTarget=((NovTreeTableNode)NOVECNTargetsTable_v2.this.getNodeForRow(row)).context;//(TCComponent)getValueAt(row, 1);
			deleteTargets((TCComponentItemRevision)remTarget);
			if(TargetsTobremoved.size()==enformPanel.getEnTargets().size())
			{
				TargetsTobremoved.removeAllElements();
				MessageBox.post(reg.getString("removeTargets.MSG"), reg.getString("warning.STR"), MessageBox.WARNING);
				return;
			}
			//Remove from the Process
			TCComponent[] remattchemnts=new TCComponent[TargetsTobremoved.size()];
			TargetsTobremoved.copyInto(remattchemnts);
			TCComponentTask roottask=enformPanel.getProcess().getRootTask();
			roottask.removeAttachments(remattchemnts);
			//Remove row from the table and remove it from the vector also	
			int childcount=((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).getChildCount();			
			for(int k=childcount-1;k>=0;k--)
			{	
				//TCComponent tobRemoved=((NovTreeTableNode)NOVECNTargetsTable_v2.this.getNodeForRow(i)).context;				
				NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).getChildAt(k);
				TCComponent tobRemoved=treeNode.context;
				if(TargetsTobremoved.contains(tobRemoved))
				{	
					if(!(tobRemoved.getProperty(strObjectType)
							.equalsIgnoreCase(strDocRevType)))
					{
						((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).remove(treeNode);
						updateUI();
					}
				}
			}
			//Remove from the Targets vector
			Vector<TCComponent> enTargets=NOVECNTargetsTable_v2.this.enformPanel.getEnTargets();
			enTargets.removeAll(TargetsTobremoved);
			//remove the disposition tab for the respective target
			for(int k=0;k<TargetsTobremoved.size();k++)
			{
				//if(!(TargetsTobremoved.get(k).getProperty("object_type").equalsIgnoreCase("Documents Revision")))
				//{
					NOVECNTargetsTable_v2.this.enformPanel.removeDispositionData(TargetsTobremoved.get(k));
				//}
			}
			TargetsTobremoved.removeAllElements();
		}
		catch(TCException exp)
		{
			System.out.println(exp);
		}
	}

	public void deleteTargets(TCComponentItemRevision childcomp)
	{
		try
		{
			TargetsTobremoved.add(childcomp);						
			if(!childcomp.getProperty(
					strObjectType).equalsIgnoreCase(strDocRevType))
			{
				AIFComponentContext[] rddComps=childcomp.getRelated(strRddType);
				String RevId=childcomp.getProperty(reg.getString("currRevId.PROP"));
				for(int k=0;k<rddComps.length;k++)
				{
					TCComponentItem rddItem=(TCComponentItem)rddComps[k].getComponent();
					TCComponentItemRevision rddRev=getItemRevisionofRevID(rddItem, RevId);
					if((rddRev!=null)&&!(TargetsTobremoved.contains(rddRev)))							
					{
						if(enformPanel.Entargets.contains(rddRev))
						{
							//then add its RDD related comps also
							if(addRddRevsofDocument(rddRev, true))
								TargetsTobremoved.add(rddRev);
						}
					}
				}
			}else
			{
				addRddRevsofDocument(childcomp, false);
			}


		}
		catch(TCException te)
		{
			System.out.println(te);
		}
	}
	public boolean addRddRevsofDocument(TCComponentItemRevision rddrev, boolean check)
	{
		//TCComponentItemRevision[] revs=null;
		try
		{
			AIFComponentContext[] rddComps=rddrev.getItem().getPrimary();//Related("RelatedDefiningDocument");
			for(int k=0;k<rddComps.length;k++)
			{
				if(rddComps[k].getContext().toString().equalsIgnoreCase(strRddType))
				{
					TCComponentItemRevision rddItem=(TCComponentItemRevision)rddComps[k].getComponent();			
					if((rddItem!=null)&&!(TargetsTobremoved.contains(rddItem)))							
					{						
						if(enformPanel.Entargets.contains(rddItem))
						{
							if(check)
								return false;
							TargetsTobremoved.add(rddItem);
						}
					}
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		return true;
	}

	public void loadTabledata( Vector<TCComponent> enTargets )
	{
		//get the targets and load the table
		try
		{
			strObjectType = reg.getString( "objType.PROP" );
			strDocRevType = reg.getString( "DocRev.TYPE" );
			strNov4PartRev = reg.getString( "Nov4PartRev.TYPE" );
			strPurchasedPartRev = reg.getString( "PurchasedRev.TYPE" );
			strNovEngRev = reg.getString( "NonEngRev.TYPE" );
			strRddType = reg.getString( "rdd.TYPE" );
			
			String[] reqTCProprties = { strObjectType };
			TCComponentItemRevision[] itemRevArr = ( TCComponentItemRevision[] )enTargets.toArray( new TCComponentItemRevision[enTargets.size()] );
			//TCProperty[][] retTCProp = TCComponentType.getTCPropertiesSet( itemRevArr, reqTCProprties );
			List<TCComponentItemRevision> itemRevList = Arrays.asList(itemRevArr);//TC10.1 Upgrade
			TCProperty[][] retTCProp = TCComponentType.getTCPropertiesSet( itemRevList, reqTCProprties );//TC10.1 Upgrade
			
			
			String[] reqCurRevId = { "current_revision_id" };
			//String[][] retCurRevId = TCComponentType.getPropertiesSet(  itemRev, reqCurRevId );
			String[][] retCurRevId = TCComponentType.getPropertiesSet(  itemRevList, reqCurRevId );//TC10.1 Upgrade
			
			for( int index = 0; index < enTargets.size(); index++ )
			{
//				TCComponentItemRevision itemRev = (TCComponentItemRevision)enTargets.get( index );
//				String itemtype = ( ( TCComponentItemRevision )itemRev ).getTCProperty(	strObjectType ).toString();
				String itemtype = retTCProp[index][0].toString(); 	
				if(itemtype.equalsIgnoreCase( strDocRevType ) )
				{
					String[] types = { strNov4PartRev, strPurchasedPartRev, strNovEngRev };
	                String[] relations = { strRddType };
	                AIFComponentContext[] contextComp = itemRevArr[index].getItem().whereReferencedByTypeRelation(types, relations);
	                boolean isInTargetList = false; 
	                
	                for( int indexTarget = 0; indexTarget < contextComp.length; ++indexTarget )
	                {
	                	isInTargetList = enTargets.contains( ( TCComponent )contextComp[indexTarget].getComponent() );
	                	if( isInTargetList )
	                		break;
	                }
	                
	                if( !isInTargetList )
	                {
	                	NovTreeTableNode node1 = new NovTreeTableNode( itemRevArr[index] );
			    		treeTableModel.addRoot( node1 );		
	    				updateUI();
	                }
				}
				
				if(!itemtype.equalsIgnoreCase( strDocRevType ) )
				{
					NovTreeTableNode node1 = new NovTreeTableNode( itemRevArr[index] );
					//String RevId = itemRevArr[index].getProperty( "current_revision_id" );
					AIFComponentContext contextRDD[] = itemRevArr[index].getRelated( strRddType );
					for( int indexRdd = 0; indexRdd < contextRDD.length; ++indexRdd )
					{
						TCComponentItem rddcomp = (TCComponentItem)contextRDD[indexRdd].getComponent();
						AIFComponentContext[] itemRevisions = (AIFComponentContext[] ) (rddcomp.getRelated("revision_list"));
						//TCComponentItemRevision rddRev = getItemRevisionofRevID( rddcomp, retCurRevId[index][0] );
						for( int rddinx = 0; rddinx < itemRevisions.length; ++rddinx )
						{
							if(enformPanel.Entargets.contains( itemRevisions[rddinx].getComponent() ) )
							{
								NovTreeTableNode nodeRDD = new NovTreeTableNode((TCComponent)(itemRevisions[rddinx].getComponent() )); 
								node1.add(nodeRDD);
								break;
							}
						}
									
					}			
					treeTableModel.addRoot(node1);		
					updateUI();
				}
			}
		}
		catch(Exception tcExeption )
		{
			tcExeption.printStackTrace();
		}
	}

	public boolean isCellEditable(int row , int col )
	{			
		if(col==4)
		{
			return isFutureStatusEditable(row);				
		}
		return false;
	}	

	/*public boolean isTopFutureToSWAP()
	{
		boolean topIsSwap = false;
		String futureStatus=getModel().getValueAt(0, 3).toString();	
	}*/
	
	public boolean isFutureStatusEditable(int row)
	{
		String curretnStatus=getModel().getValueAt(row, 3).toString();	
		
		if(!m_isMissionGrp)
		{
			if((curretnStatus.compareTo(reg.getString("MFG.TYPE"))==0)||((curretnStatus.compareTo(reg.getString("ACTIVE.TYPE"))==0))||((curretnStatus.compareTo(reg.getString("SUS.TYPE"))==0)))
				return false;
			return true;
		}
		else
			return true;
	}

	public TCComponentItemRevision getItemRevisionofRevID(TCComponentItem rddItem, String RevId)
	{
		TCComponentItemRevision itemRev=null;
		try
		{
			AIFComponentContext[] revItems = rddItem.getChildren( "revision_list" );
			
			for(int k=0;k<revItems.length;k++)
			{
				String revId = ((TCComponentItemRevision)revItems[k].getComponent()).getProperty( "current_revision_id" );
				if(revId.compareTo(RevId)==0)
				{
					itemRev=(TCComponentItemRevision)revItems[k].getComponent();
					break;
				}
			}
		}
		catch(TCException e)
		{
			System.out.println(e);
		}
		if(itemRev==null)
		{
		}

		return itemRev;
	}


	public class NOVEnTreeTableModel extends TreeTableModel
	{
		private Vector<Object> columnNamesVec;

		public NOVEnTreeTableModel(String columnNames[])
		{
			super();
			columnNamesVec = new Vector<Object>();
			TreeTableNode top = new TreeTableNode();
			setRoot(top);
			top.setModel(this);
			for(int i = 0; i < columnNames.length; i++)
			{
				columnNamesVec.addElement(columnNames[i]);
				modelIndexToProperty.add(columnNames[i]);
			}

		}
		public void addRoot(TreeTableNode root)
		{
			TreeTableNode top = (TreeTableNode)getRoot();
			top.add(root);
		}

		public TreeTableNode getRoot(int index)
		{
			if(((TreeTableNode)getRoot()).getChildCount() > 0)
				return (TreeTableNode)((TreeTableNode)getRoot()).getChildAt(index);
			else
				return null;
		}

		public int getColumnCount()
		{
			return columnNamesVec.size();
		}

		public String getColumnName(int column)
		{
			return (String)columnNamesVec.elementAt(column);
		}
		public void removeNode(int row)
		{
			//getNodeForRow(row).remove(arg0);
			((TreeTableNode)getRoot()).remove(getNodeForRow(row));
		}
	}

	public class NovTreeTableNode extends TreeTableNode
	{

		private static final long serialVersionUID = 1L;
		public TCComponent context;

		public NovTreeTableNode(TCComponent com)
		{
			context = com;
		}
		//@Override
		public String getProperty(String name)
		{
			try 
			{
				String status="";
				if(name.equalsIgnoreCase(reg.getString("PartID.TYPE")))
				{
					String strItemId = context.getProperty("item_id");
					return strItemId == null ? "" : strItemId.toString();
				}
				if(name.equalsIgnoreCase(reg.getString("PartName.TYPE")))
				{
					String strObjectName = context.getProperty("object_name");
					return strObjectName == null ? "" : strObjectName.toString();
				}
				if(name.equalsIgnoreCase(reg.getString("Rev.TYPE")))
				{
					String strCurRevId = context.getProperty( "current_revision_id" );
					return strCurRevId == null ? "" : strCurRevId.toString();
				}
				if(name.equalsIgnoreCase(reg.getString("StartStatus.TYPE")))
				{
					if(context instanceof TCComponentItemRevision)
					{
						//status= ((TCComponentItemRevision)context).getItem().getProperty("release_statuses");
						status=currentStatMap.get(context)== null?"" : currentStatMap.get(context);
						return status;
					}
				}
				if(name.equalsIgnoreCase(reg.getString("FutureStatus.TYPE")))
				{			
					if(context.getType().equalsIgnoreCase("Documents Revision"))
					{
							String[] types =
			                { strNov4PartRev, strPurchasedPartRev, strNovEngRev };
			                String[] relations =
			                { strRddType };
			                AIFComponentContext[] contextComp = ((TCComponentItemRevision)context).getItem()
			                        .whereReferencedByTypeRelation(types,
			                                relations);
			                if ( contextComp.length == 0 )
			                {
			                	validate();
			                	return "";
			                }
					}
					//fetch the saved future status for the remaining
					if(context instanceof TCComponentItemRevision)
					{
						//usha
						if(!m_isMissionGrp)
						{
							status= ((TCComponentItemRevision)context).getItem().getProperty("release_statuses");
							if(status == null)
							{
								//System.out.println("No status on part--> legacy part.  need to set future status NULL");
								validate();
								return "";
							}

							if((status.compareTo(reg.getString("MFG.TYPE"))==0)||((status.compareTo(reg.getString("ACTIVE.TYPE"))==0))||((status.compareTo(reg.getString("SUS.TYPE"))==0)))
							{							
								validate();
								return new String(reg.getString("MFG.TYPE"));
							}
						}
						else //mission group
						{
							status=futureStatMap.get(context)== null?reg.getString( "STANDARD.TYPE" ) : futureStatMap.get(context);
							if(status == null)
							{
								validate();
								return new String(reg.getString( "STANDARD.TYPE" ));
							}
							else
							{
								validate();
								return status;
							}
						}

					}			

				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return "";
		}

		public String getRelation()
		{			
			return " ";
		}

		public String getType()
		{
			return context.getType();
		}

	}
	class TargetIdRenderer extends TreeTableTreeCellRenderer
	{   		

		public TargetIdRenderer(JTreeTable arg0) {
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{
			// Select the current value
			try
			{
				super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
						row, hasFocus);

				if(row==0)
				{
					if(enformPanel.getProcess()!=null)
					{
						String name=enformPanel.getProcess().getRootTask().getName();//getProperty("object_name");
						setText(name);
					}
					setIcon(TCTypeRenderer.getTypeIcon(session.getTypeComponent("Job"), "Job"));					
				}
				else
				{
					setText(((NovTreeTableNode)getNodeForRow(row)).context.getProperty("item_id"));
					setIcon(TCTypeRenderer.getIcon(((NovTreeTableNode)getNodeForRow(row)).context));
				}				
			}
			catch(Exception e)
			{
				System.out.println(e);

			}
			return this;
		}
	}	
	public class ComboboxCellEditor extends DefaultCellEditor 
	{
		public ComboboxCellEditor(JComboBox comboBox) 
		{
			super( comboBox);
		}

		//@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) 
		{
			String itemType="";
			try
			{
				if (getNodeForRow(row) instanceof NovTreeTableNode ) 
				{
					itemType=((NovTreeTableNode)getNodeForRow(row)).context.getProperty(strObjectType);	
				}
				if(itemType.compareToIgnoreCase(strDocRevType)==0 ||itemType.equals("") )
				{
					return null;
				}
			}catch(TCException tc)
			{
				System.out.println(tc);
			}
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}

		//@Override
		public boolean stopCellEditing() {
			int row=getEditingRow();
			if(row>=0)
				futureStatMap.put(((NovTreeTableNode)getNodeForRow(row)).context, (String)getCellEditorValue());
			//clearSelection();
			return super.stopCellEditing();
		}


	}

	class ComboboxCellRenderer extends JComboBox implements
	TableCellRenderer
	{    

		public ComboboxCellRenderer(String[] status)
		{			
			super(status);
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
				return null;
			String itemType="";
			try
			{
				itemType=((NovTreeTableNode)getNodeForRow(row)).context.getProperty(strObjectType);				
			}catch(TCException tc)
			{
				System.out.println(tc);
			}
			if(itemType.compareToIgnoreCase(strDocRevType)==0)
			{
				if(isSelected)
				{
					setBackground(table.getSelectionBackground());
					setForeground( Color.WHITE );
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
				return null;
			}
			else
			{
				if(isSelected)
				{
					setBackground(table.getSelectionBackground());
					setForeground( Color.WHITE );
				}
				else                
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
				// Select the current value
				if (!(value.toString().trim().length()>0)) 
				{
					value = ComboboxCellRenderer.this.getItemAt(0);
				}
			}
			setSelectedItem(value);
			return this;
		}
	}

	class RootCellRenderer extends JLabel implements
	TableCellRenderer
	{    

		public RootCellRenderer()
		{			
			super();
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
			{
				setText("");
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			else
			{
				setText(value.toString());
				if(isSelected)
				{
					setOpaque(true);
					setBackground(table.getSelectionBackground());
					setForeground( Color.WHITE );
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
			}
			return this;
		}
	}

	public void addTarget(TCComponent itemRev)
	{
		try
		{
			String itemtype=((TCComponentItemRevision)itemRev).getTCProperty(strObjectType).toString();
			if(!itemtype.equalsIgnoreCase(strDocRevType))
			{
				NovTreeTableNode node1 = new NovTreeTableNode(itemRev);
				//enformPanel.Entargets.add(itemRev);
				//9th jan
				AIFComponentContext contextRDD[] = itemRev.getRelated( strRddType );
				for( int indexRdd = 0; indexRdd < contextRDD.length; ++indexRdd )
				{
					TCComponentItem rddcomp = (TCComponentItem)contextRDD[indexRdd].getComponent();
					AIFComponentContext[] itemRevisions = (AIFComponentContext[] ) (rddcomp.getRelated("revision_list"));
					for( int rddinx = 0; rddinx < itemRevisions.length; ++rddinx )
					{
						if(enformPanel.Entargets.contains( itemRevisions[rddinx].getComponent() ) )
						{
							NovTreeTableNode nodeRDD = new NovTreeTableNode((TCComponent)(itemRevisions[rddinx].getComponent() )); 
							node1.add(nodeRDD);
							break;
						}
					}
								
				}			
				//9th jan
				treeTableModel.addRoot(node1);		
				//updateUI();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//return;
	}

	public void removeTarget(TCComponent itemRev, TreeTableNode selNode ) 
	{
		enformPanel.Entargets.remove(itemRev);
		((TreeTableNode)treeTableModel.getRoot()).remove(selNode);
		updateUI();
	}
	public void addDocTarget(TCComponent docRev)
	{
		if(!isRDDofTargetItem(docRev))
		{
			NovTreeTableNode nodeDoc = new NovTreeTableNode(docRev);
			//enformPanel.Entargets.add(docRev);
			treeTableModel.addRoot(nodeDoc);		
			//updateUI();
		}
		//return;
	}
	private boolean isRDDofTargetItem(TCComponent itemRev) 
	{
		try 
		{
			TCComponentItem targetItem =  ((TCComponentItemRevision)itemRev).getItem();
					String[] types =
			    { strNov4PartRev, strPurchasedPartRev, strNovEngRev };
			    String[] relations =
			    { strRddType };
			    AIFComponentContext[] contextComp = targetItem
				    .whereReferencedByTypeRelation(types,
					    relations);	
			for (int i = 0; i < contextComp.length; i++) 
			{
				InterfaceAIFComponent infComp =  contextComp[i].getComponent();	
				if (infComp instanceof TCComponentItemRevision) 
				{
					if(enformPanel.Entargets.contains(infComp)) 
					{
						return true;
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
}

