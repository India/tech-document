/*
 * Created on Sep 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.LayoutManager;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.noi.rac.en.NationalOilwell;
/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOVTextArea extends JPanel {

	JTextArea jta;
	JScrollPane scroller;
	int lineHeight;
	int charWidth;
	String fieldText;
	
	/**
	 * 
	 */
	public NOVTextArea() {
		super();
		// TODO Auto-generated constructor stub
        init();		
	}

	/**
	 * @param arg0
	 */
	public NOVTextArea(boolean arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
        init();		
	}

	/**
	 * @param arg0
	 */
	public NOVTextArea(LayoutManager arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
        init();		
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOVTextArea(LayoutManager arg0, boolean arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
        init();		
	}

	private void init() {

		setBackground(NationalOilwell.NOVPanelBG);
		jta = new JTextArea();
		jta.setLineWrap(true);
		jta.setWrapStyleWord(true);
		scroller = new JScrollPane();
		scroller.setBackground(NationalOilwell.NOVPanelBG);
		Font thisFont = jta.getFont();
		FontMetrics fm = getFontMetrics(thisFont);
		lineHeight = fm.getHeight();
		int[] widths = fm.getWidths();
		charWidth = 0;
		for (int i=0;i<widths.length;i++) {
			if (widths[i] > charWidth)
				charWidth = widths[i];
		}
	}
	public void setLineWrap(boolean wrap) {
		jta.setLineWrap(wrap);
	}
	
	public void setRows(int rows) {		
		jta.setRows(rows);
	}
	
	public void setColumns(int columns) {
		jta.setColumns(columns);
	}
	
	public void setText(String str) {
		fieldText = str;
		jta.setText(str);
	}
	public String getText() {
		return(jta.getText());
	}
	public void calculateSize() {
		
		int cols = jta.getColumns();
		int lines = jta.getRows();
		int totalLines = (((fieldText.length() == 0) ? cols : fieldText.length())/cols)+10;
		jta.setRows(totalLines);
		scroller.setPreferredSize(new Dimension(cols*charWidth+22,lines*lineHeight+2));
		scroller.setViewportView(jta);
		add(scroller);
		
	}
}
