package com.noi.rac.en.util.components;

import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;


public class NOVECNTargetsTable extends JTreeTable
{
	private static final long serialVersionUID = 1L;
	NOVEnTreeTableModel treeTableModel;	
	private _EngNoticeForm_Panel enformPanel;
	private TCSession session;	
	public HashMap<TCComponent, String> futureStatMap ; 
	public HashMap<TCComponent, String> currentStatMap ; 
	private Vector<TCComponent> TargetsTobremoved=new Vector<TCComponent>();
	private Registry reg;
	public NOVECNTargetsTable( final _EngNoticeForm_Panel enformPanel, TCSession tcSession, String columnNames[])
	{
		super(tcSession);		
		this.enformPanel=enformPanel;
		this.session=tcSession;
		this.reg=Registry.getRegistry(this);
			
		futureStatMap = new HashMap<TCComponent, String>();
		currentStatMap=new HashMap<TCComponent, String>();;
		treeTableModel = new NOVEnTreeTableModel(columnNames);
		setModel(treeTableModel);

		for(int i = 0; i < columnNames.length; i++)
		{
			TableColumn col = new TableColumn(i);
			addColumn(col);
		}
		getTree().setCellRenderer(new TargetIdRenderer(this));
		//set Renderer and editor for "future status" column
		//RootCellRenderer
		getColumnModel().getColumn(1).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(2).setCellRenderer(new RootCellRenderer());
		if (getColumnModel().getColumnCount()>3){
			getColumnModel().getColumn(3).setCellRenderer(new RootCellRenderer());
			//getColumnModel().getColumn(4).setCellRenderer(new RootCellRenderer());
			String[] status={reg.getString( "MFG.TYPE" ), reg.getString( "SWAP.TYPE" ) };			
			TableColumn col5=getColumnModel().getColumn(4);     
			col5.setCellRenderer(new ComboboxCellRenderer(status));            
			JComboBox ststuscombo=new JComboBox(status);          
			ststuscombo.setSelectedIndex(1);
			col5.setCellEditor(new ComboboxCellEditor(ststuscombo));	
			setAutoResizeMode(JTreeTable.AUTO_RESIZE_ALL_COLUMNS);	
		}
		loadTabledata(enformPanel.Entargets);
		/*addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent mouseEvt)
			{
				Object source = mouseEvt.getSource();
				int selRow = ((NOVECNTargetsTable)source).getSelectedRow();
				if(SwingUtilities.isRightMouseButton(mouseEvt) && (mouseEvt.getClickCount() == 1)&& selRow >0)
				{
					JPopupMenu popupMenu = new JPopupMenu();
					JMenuItem item = new JMenuItem();
					String initialText = "<html><b><font=8 color=red>Remove Target</font></b>";
					item.setText(initialText);
					item.addActionListener(new ActionListener() {

						public void actionPerformed(ActionEvent e )
						{				
							//Confirmation dialog
							int row=NOVECNTargetsTable.this.getSelectedRow();
							if(row!=-1)
							{
								int rows=((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).getChildCount();//NOVECNTargetsTable.this.getRowCount();
								if(rows==1)
								{
									//PlatformUI.
									MessageBox.post(reg.getString("deleteTargets.MSG"), reg.getString("warning.STR"), MessageBox.WARNING);
									return;
								}
								int response = JOptionPane.showConfirmDialog(null,
										reg.getString("response.MSG"),reg.getString("confirm.STR"),
										JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
								if ( response == JOptionPane.YES_OPTION )
								{
									getTree().getSelectionPath();								
									removeTargetsandClenup(row);
								}
							}
						}
					});
					popupMenu.add(item);
					popupMenu.show((java.awt.Component)source, mouseEvt.getX(), mouseEvt.getY());
				}
		
				if( mouseEvt.getClickCount() == 2 && selRow > 0 )
				{
					TCComponent targetItemRev = ( ( NovTreeTableNode )NOVECNTargetsTable.this.getNodeForRow( selRow ) ).context;
					enformPanel.createDispTabPanel( targetItemRev );
				}	
			}			 
		});*/

	}
	public void removeTargetsandClenup(int row)
	{
		try
		{
			//User exit for the remove action  
			TCComponent remTarget=((NovTreeTableNode)NOVECNTargetsTable.this.getNodeForRow(row)).context;//(TCComponent)getValueAt(row, 1);
			deleteTargets((TCComponentItemRevision)remTarget);
			if(TargetsTobremoved.size()==enformPanel.getEnTargets().size())
			{
				TargetsTobremoved.removeAllElements();
				MessageBox.post(reg.getString("removeTargets.MSG"), reg.getString("warning.STR"), MessageBox.WARNING);
				return;
			}
			//Remove from the Process
			TCComponent[] remattchemnts=new TCComponent[TargetsTobremoved.size()];
			TargetsTobremoved.copyInto(remattchemnts);
			TCComponentTask roottask=enformPanel.getProcess().getRootTask();
			roottask.removeAttachments(remattchemnts);
			//Remove row from the table and remove it from the vector also	
			int childcount=((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).getChildCount();			
			for(int k=childcount-1;k>=0;k--)
			{	
				//TCComponent tobRemoved=((NovTreeTableNode)NOVECNTargetsTable.this.getNodeForRow(i)).context;				
				NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).getChildAt(k);
				TCComponent tobRemoved=treeNode.context;
				if(TargetsTobremoved.contains(tobRemoved))
				{	
					if(!(tobRemoved.getProperty(reg.getString("objType.PROP"))
							.equalsIgnoreCase(reg.getString("docsRev.TYPE"))))
					{
						((TreeTableNode)((NOVEnTreeTableModel)getModel()).getRoot()).remove(treeNode);
						updateUI();
					}
				}
			}
			//Remove from the Targets vector
			Vector<TCComponent> enTargets=NOVECNTargetsTable.this.enformPanel.getEnTargets();
			enTargets.removeAll(TargetsTobremoved);
			//remove the disposition tab for the respective target
			for(int k=0;k<TargetsTobremoved.size();k++)
			{
				//if(!(TargetsTobremoved.get(k).getProperty("object_type").equalsIgnoreCase("Documents Revision")))
				//{
					NOVECNTargetsTable.this.enformPanel.removeDispositionData(TargetsTobremoved.get(k));
				//}
			}
			TargetsTobremoved.removeAllElements();
		}
		catch(TCException exp)
		{
			System.out.println(exp);
		}
	}

	public void deleteTargets(TCComponentItemRevision childcomp)
	{
		try
		{
			TargetsTobremoved.add(childcomp);						
			if(!childcomp.getProperty(
					reg.getString("objType.PROP")).equalsIgnoreCase(reg.getString("docsRev.TYPE")))
			{
				AIFComponentContext[] rddComps=childcomp.getRelated(reg.getString("rdd.TYPE"));
				String RevId=childcomp.getProperty(reg.getString("currRevId.PROP"));
				for(int k=0;k<rddComps.length;k++)
				{
					TCComponentItem rddItem=(TCComponentItem)rddComps[k].getComponent();
					TCComponentItemRevision rddRev=getItemRevisionofRevID(rddItem, RevId);
					if((rddRev!=null)&&!(TargetsTobremoved.contains(rddRev)))							
					{
						if(enformPanel.Entargets.contains(rddRev))
						{
							//then add its RDD related comps also
							if(addRddRevsofDocument(rddRev, true))
								TargetsTobremoved.add(rddRev);
						}
					}
				}
			}else
			{
				addRddRevsofDocument(childcomp, false);
			}


		}
		catch(TCException te)
		{
			System.out.println(te);
		}
	}
	public boolean addRddRevsofDocument(TCComponentItemRevision rddrev, boolean check)
	{
		//TCComponentItemRevision[] revs=null;
		try
		{
			AIFComponentContext[] rddComps=rddrev.getItem().getPrimary();//Related("RelatedDefiningDocument");
			for(int k=0;k<rddComps.length;k++)
			{
				if(rddComps[k].getContext().toString().equalsIgnoreCase(reg.getString("rdd.TYPE")))
				{
					TCComponentItemRevision rddItem=(TCComponentItemRevision)rddComps[k].getComponent();			
					if((rddItem!=null)&&!(TargetsTobremoved.contains(rddItem)))							
					{						
						if(enformPanel.Entargets.contains(rddItem))
						{
							if(check)
								return false;
							TargetsTobremoved.add(rddItem);
						}
					}
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		return true;
	}

	public void loadTabledata(Vector<TCComponent> enTargets)
	{
		//get the targets and load the table
		try
		{
			for(int i=0;i<enTargets.size();i++)
			{
				TCComponentItemRevision rev=(TCComponentItemRevision)enTargets.get(i);
				addTarget(rev);
			}
		}
		catch(Exception tc)
		{

		}
	}

	public boolean isCellEditable(int row , int col )
	{			
		if(col==4)
		{
			return isFutureStatusEditable(row);				
		}
		return false;
	}	

	/*public boolean isTopFutureToSWAP()
	{
		boolean topIsSwap = false;
		String futureStatus=getModel().getValueAt(0, 3).toString();	
	}*/
	
	public boolean isFutureStatusEditable(int row)
	{
		String curretnStatus=getModel().getValueAt(row, 3).toString();		
		if((curretnStatus.compareTo(reg.getString("MFG.TYPE"))==0)||((curretnStatus.compareTo(reg.getString("ACTIVE.TYPE"))==0))||((curretnStatus.compareTo(reg.getString("SUS.TYPE"))==0)))
			return false;
		return true;
	}
	public void addTarget(TCComponent itemRev)
	{
		try
		{
			String itemtype=((TCComponentItemRevision)itemRev).getTCProperty(
					reg.getString("objType.PROP")).toString();
			if(itemtype.equalsIgnoreCase(reg.getString("DocRev.TYPE")))
			{
				String[] types =
                { reg.getString("Nov4PartRev.TYPE"), reg.getString("PurchasedRev.TYPE"),
                        reg.getString("NonEngRev.TYPE") };
                String[] relations =
                { reg.getString("rdd.TYPE") };
                AIFComponentContext[] contextComp = ((TCComponentItemRevision)itemRev).getItem()
                        .whereReferencedByTypeRelation(types,
                                relations);
                if ( contextComp.length == 0 )
                {
                	NovTreeTableNode node1 = new NovTreeTableNode(itemRev);
    				//String RevId=itemRev.getProperty("current_revision_id");
    				treeTableModel.addRoot(node1);		
    				updateUI();
                }
			}
			if(!itemtype.equalsIgnoreCase(reg.getString("DocRev.TYPE")))
			{
				NovTreeTableNode node1 = new NovTreeTableNode(itemRev);
				String RevId=itemRev.getProperty("current_revision_id");
				AIFComponentContext contextRDD[] = itemRev.getRelated(reg.getString("rdd.TYPE"));
				for(int i = 0; i < contextRDD.length; i++)
				{
					TCComponentItem rddcomp=(TCComponentItem)contextRDD[i].getComponent();
					TCComponentItemRevision rddRev=getItemRevisionofRevID(rddcomp, RevId);
					if(enformPanel.Entargets.contains(rddRev))
					{
						NovTreeTableNode nodeRDD = new NovTreeTableNode(rddRev);
						node1.add(nodeRDD);
					}
				}		
				treeTableModel.addRoot(node1);		
				updateUI();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public TCComponentItemRevision getItemRevisionofRevID(TCComponentItem rddItem, String RevId)
	{
		TCComponentItemRevision itemRev=null;
		try
		{
			AIFComponentContext[] revItems=rddItem.getChildren("revision_list");
			for(int k=0;k<revItems.length;k++)
			{
				String revId=((TCComponentItemRevision)revItems[k].getComponent()).getProperty("current_revision_id");
				if(revId.compareTo(RevId)==0)
				{
					itemRev=(TCComponentItemRevision)revItems[k].getComponent();
					break;
				}
			}
		}catch(TCException e)
		{
			System.out.println(e);

		}
		if(itemRev==null)
		{

		}

		return itemRev;
	}


	public class NOVEnTreeTableModel extends TreeTableModel
	{
		private Vector<Object> columnNamesVec;

		public NOVEnTreeTableModel(String columnNames[])
		{
			super();
			columnNamesVec = new Vector<Object>();
			TreeTableNode top = new TreeTableNode();
			setRoot(top);
			top.setModel(this);
			for(int i = 0; i < columnNames.length; i++)
			{
				columnNamesVec.addElement(columnNames[i]);
				modelIndexToProperty.add(columnNames[i]);
			}

		}
		public void addRoot(TreeTableNode root)
		{
			TreeTableNode top = (TreeTableNode)getRoot();
			top.add(root);
		}

		public TreeTableNode getRoot(int index)
		{
			if(((TreeTableNode)getRoot()).getChildCount() > 0)
				return (TreeTableNode)((TreeTableNode)getRoot()).getChildAt(index);
			else
				return null;
		}

		public int getColumnCount()
		{
			return columnNamesVec.size();
		}

		public String getColumnName(int column)
		{
			return (String)columnNamesVec.elementAt(column);
		}
		public void removeNode(int row)
		{
			//getNodeForRow(row).remove(arg0);
			((TreeTableNode)getRoot()).remove(getNodeForRow(row));
		}
	}

	public class NovTreeTableNode extends TreeTableNode
	{

		private static final long serialVersionUID = 1L;
		public TCComponent context;

		public NovTreeTableNode(TCComponent com)
		{
			context = com;
		}
		@Override
		public String getProperty(String name)
		{
			try 
			{
				String status="";
				if(name.equalsIgnoreCase(reg.getString("PartID.TYPE")))

					return context.getProperty("item_id") == null ? "" : context.getProperty("item_id").toString();

				if(name.equalsIgnoreCase(reg.getString("PartName.TYPE")))
					return context.getProperty("object_name") == null ? "" : context.getProperty("object_name").toString();
				if(name.equalsIgnoreCase(reg.getString("Rev.TYPE")))
					return context.getProperty("current_revision_id") == null ? "" : context.getProperty("current_revision_id").toString();
				if(name.equalsIgnoreCase(reg.getString("StartStatus.TYPE")))
				{
					if(context instanceof TCComponentItemRevision)
					{
						//status= ((TCComponentItemRevision)context).getItem().getProperty("release_statuses");
						status=currentStatMap.get(context)== null?"" : currentStatMap.get(context);
						return status;
					}
				}
				if(name.equalsIgnoreCase(reg.getString("FutureStatus.TYPE")))
				{			
					if(context.getType().equalsIgnoreCase("Documents Revision"))
					{
							String[] types =
			                { reg.getString("Nov4PartRev.TYPE"), reg.getString("PurchasedRev.TYPE"),
			                        reg.getString("NonEngRev.TYPE") };
			                String[] relations =
			                { reg.getString("rdd.TYPE") };
			                AIFComponentContext[] contextComp = ((TCComponentItemRevision)context).getItem()
			                        .whereReferencedByTypeRelation(types,
			                                relations);
			                if ( contextComp.length == 0 )
			                {
			                	validate();
			                	return "";
			                }
					}
					//fetch the saved future status for the remaining
					if(context instanceof TCComponentItemRevision)
					{
						status= ((TCComponentItemRevision)context).getItem().getProperty("release_statuses");
						if((status.compareTo(reg.getString("MFG.TYPE"))==0)||((status.compareTo(reg.getString("ACTIVE.TYPE"))==0))||((status.compareTo(reg.getString("SUS.TYPE"))==0)))
						{							
							validate();
							return new String(reg.getString("MFG.TYPE"));
						}
						else
						{
							validate();
							return futureStatMap.get(context)== null?"" : futureStatMap.get(context);
						}
					}			

				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return "";
		}

		public String getRelation()
		{			
			return " ";
		}

		public String getType()
		{
			return context.getType();
		}

	}
	class TargetIdRenderer extends TreeTableTreeCellRenderer
	{   		

		public TargetIdRenderer(JTreeTable arg0) {
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{
			// Select the current value
			try
			{
				super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
						row, hasFocus);

				if(row==0)
				{
					if(enformPanel.getProcess()!=null)
					{
						String name=enformPanel.getProcess().getRootTask().getName();//getProperty("object_name");
						setText(name);
					}
					setIcon(TCTypeRenderer.getTypeIcon(session.getTypeComponent("Job"), "Job"));					
				}
				else
				{
					setText(((NovTreeTableNode)getNodeForRow(row)).context.getProperty("item_id"));
					setIcon(TCTypeRenderer.getIcon(((NovTreeTableNode)getNodeForRow(row)).context));
				}				
			}
			catch(Exception e)
			{
				System.out.println(e);

			}
			return this;
		}
	}	
	public class ComboboxCellEditor extends DefaultCellEditor 
	{
		public ComboboxCellEditor(JComboBox comboBox) 
		{
			super( comboBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) 
		{
			String itemType="";
			try
			{
				if (getNodeForRow(row) instanceof NovTreeTableNode ) 
				{
					itemType=((NovTreeTableNode)getNodeForRow(row)).context.getProperty("object_type");	
				}
				if(itemType.compareToIgnoreCase(reg.getString("docsRev.TYPE"))==0 ||itemType.equals("") )
				{
					return null;
				}
			}catch(TCException tc)
			{
				System.out.println(tc);
			}
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}

		@Override
		public boolean stopCellEditing() {
			int row=getEditingRow();
			if(row>=0)
				futureStatMap.put(((NovTreeTableNode)getNodeForRow(row)).context, (String)getCellEditorValue());
			//clearSelection();
			return super.stopCellEditing();
		}


	}

	class ComboboxCellRenderer extends JComboBox implements
	TableCellRenderer
	{    

		public ComboboxCellRenderer(String[] status)
		{			
			super(status);
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
				return null;
			String itemType="";
			try
			{
				itemType=((NovTreeTableNode)getNodeForRow(row)).context.getProperty("object_type");				
			}catch(TCException tc)
			{
				System.out.println(tc);
			}
			if(itemType.compareToIgnoreCase(reg.getString("docsRev.TYPE"))==0)
			{
				if(isSelected)
				{
					setBackground(table.getSelectionBackground());
					setForeground(table.getForeground());
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
				return null;
			}
			else
			{
				if(isSelected)
				{
					setBackground(table.getSelectionBackground());
					setForeground(table.getForeground());
				}
				else                
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
				// Select the current value
				if (!(value.toString().trim().length()>0)) 
				{
					value = ComboboxCellRenderer.this.getItemAt(0);
				}
			}
			setSelectedItem(value);
			return this;
		}
	}

	class RootCellRenderer extends JLabel implements
	TableCellRenderer
	{    

		public RootCellRenderer()
		{			
			super();
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
			{
				setText("");
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			else
			{
				setText(value.toString());
				if(isSelected)
				{
					setOpaque(true);
					setBackground(table.getSelectionBackground());
					setForeground(table.getForeground());
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
			}
			return this;
		}
	}

}

