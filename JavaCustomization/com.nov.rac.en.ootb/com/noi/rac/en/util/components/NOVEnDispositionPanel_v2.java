package com.noi.rac.en.util.components;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import com.noi.rac.en.form.compound.panels._EngNoticeForm_Panel_v2;
import com.noi.rac.en.mission.helper.NOVMissionHelper_v2;
import com.noi.rac.en.util.components.NOVECNTargetsTable_v2.NOVEnTreeTableModel;
import com.noi.rac.en.util.components.NOVECNTargetsTable_v2.NovTreeTableNode;
import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.Registry;

public class NOVEnDispositionPanel_v2 extends JPanel
{
	private static final long serialVersionUID = 1L;	
	
	private _EngNoticeForm_Panel_v2 enPanel;
	private Vector<TCComponent> targets = new Vector<TCComponent>();		
	public TCComponent[] referncedispdata;	
	public HashMap<TCComponent, TCComponent> targetdispMap=new HashMap<TCComponent, TCComponent>();
	public HashMap<TCComponent, TCComponent> disptargetMap=new HashMap<TCComponent, TCComponent>();
	public HashMap<TCComponent, String> targetStatusMap=new HashMap<TCComponent, String>();
    private Registry reg;

    TCComponent curTargetItemRev;
    
	public NOVEnDispositionPanel_v2(TCComponent[] referncedispdata)
	{		
		this.referncedispdata=referncedispdata;
		this.reg=Registry.getRegistry(this);
	}

	public void createUI( _EngNoticeForm_Panel_v2 enPanel, TCComponent newTargetItemRev, boolean isReleased )
	{	
		try
		{	
			String[] reqNewTargetItemProps = { "object_type", "item_id" };
			String[] newTargetItemProps = newTargetItemRev.getProperties( reqNewTargetItemProps );
			//in case of document targets, check if it is standalone Docs or RDD
			//if( isRevRDD(newTargetItemRev)  )
			if( isRevRDD(newTargetItemRev) || newTargetItemProps[0].equalsIgnoreCase(reg.getString("DocRev.TYPE")) )
			{
				return;
			}

			if( curTargetItemRev != newTargetItemRev ) 
			{
				if( curTargetItemRev == null )
				{
					curTargetItemRev = newTargetItemRev;
					createDispTabPanels( newTargetItemRev, isReleased );
				}
				else if( !( newTargetItemProps[1].toString().equalsIgnoreCase(
						curTargetItemRev.getProperty( "item_id" ).toString() ) ) )
				{
					curTargetItemRev = newTargetItemRev;
					saveDispData();
					NOVEnDispositionTabPanel_v2 dispPanel=(NOVEnDispositionTabPanel_v2)enPanel.enautomatonTabbePane.getComponentAt( 1 );
					dispPanel.dispcomp.refresh();
					enPanel.enautomatonTabbePane.remove( dispPanel );
					if( enPanel.enautomatonTabbePane.getTabCount() > 1 )
						enPanel.enautomatonTabbePane.remove(1);
					createDispTabPanels( newTargetItemRev, isReleased );
				}
				enPanel.enautomatonTabbePane.setSelectedIndex( 1 );
			}
			
			if( enPanel.enautomatonTabbePane.getComponentCount() > 1 )
				enPanel.enautomatonTabbePane.setSelectedIndex( 1 );
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public TCComponent[] getDispData()
	{
		return referncedispdata;
	}
	public TCComponent[] createDispInstances(TCComponent[] targetItems)
	{
		TCComponent[] comps=null;
		/*if(itemIds!=null)
		{
			try
			{
				//call userExit to create the Disposition componenets and return the array
				TCUserService us=enPanel.getSession().getUserService();       	
				Object obj[]=new Object[3];       	
				obj[0]=itemIds;
				obj[1]=revIds;
				obj[2] = new Boolean(false);
				comps=(TCComponent[])us.call("NATOIL_createEnDispositionData", obj);
			}catch(TCException te)
			{
				te.printStackTrace();
			}
		}
		return comps;
		*/
		try
		{
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"release_status_list"};
	    	
			Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
	    	
	    	nOVDataManagementServiceHelper.getObjectsProperties(targetItems, attrList, null);
	    	
			for(int i = 0; i < targetItems.length ; i++)
			{
			    CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
			    		"targetdisposition", CreateInObjectHelper.OPERATION_CREATE);		    	
				String item_id = targetItems[i].getProperty("item_id");
				String revision_id = ((TCComponentItem)targetItems[i]).getLatestItemRevision().getProperty("current_revision_id");
				String statusName = targetItems[i].getProperty("release_status_list");
				
				formPropertyMap.setString("targetitemid", item_id);
				formPropertyMap.setString("rev_id", revision_id);
			
				//get status name to set current status
				/*TCComponent[] relStatusList = null;
				relStatusList = ( targetItems[i].getTCProperty("release_status_list").getReferenceValueArray() );
				
				String statusName = relStatusList[0].getProperty("name");
				*/
				formPropertyMap.setString("currentstatus", statusName);
				vecCreateInObjectHelper.add(formPropertyMap);
	   
			}
			
	        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
	                .toArray(new CreateInObjectHelper[vecCreateInObjectHelper.size()]);
			
			//create objects
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		    Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();
		    
		    for(int j = 0; j<createInObjectHelper.length ; j++)
		    {
		    	TCComponent created = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
		    	createdorUpdatedObjs.add(created);
		    }
		   
		    comps = (TCComponent[]) createdorUpdatedObjs.toArray(new TCComponent[createdorUpdatedObjs.size()]);
			
		}
		catch(TCException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return comps;		
	}

	// returns boolean value for first revision or not, excluding the PRE-RELEASE revisions
	private boolean isFirstRev(TCComponent inItemRev)
	{
		boolean isFirstRev = true;

		try
		{
			TCComponentItem item = (TCComponentItem) inItemRev.getRelatedComponent("items_tag");
			TCComponent[] itemRev = (TCComponent[]) item .getRelatedComponents("revision_list");

			int iNewRev = 0;
			for (int iRev=0; iRev<itemRev.length; iRev++)
			{
				String sRevId = ((TCComponentItemRevision)itemRev[iRev]).getProperty("item_revision_id");
				
				int ind = sRevId.indexOf("PRE-RELEASE");
				if(ind < 0)
				{
					iNewRev += 1;
				}
			}
			if (iNewRev > 1) 
			{
				isFirstRev = false;
			}

		}
		catch (TCException e)
		{
			e.printStackTrace();
		}
		return isFirstRev;
	}

	public void createDispTabPanels( TCComponent targetItemRev, boolean isReleased )
	{
		try
		{
			String[] targetItemProp = { "object_type", "item_id" };
			String[] retTargetItemProp = targetItemRev.getProperties( targetItemProp );
			
			if( retTargetItemProp[0].toString().equalsIgnoreCase( "Documents Revision" ) )
			{
				// Performance Issue Nitin : If target is Document we are not creating the disposition tab 
				// so no need to check if it is RDD or not.  
				//if( isRevRDD( targetItemRev ) )
				return;		
			}

			String[] targetDispProp = { "targetitemid" };
			List<TCComponent> referncedispdataList = Arrays.asList(referncedispdata);
			//String[][] retTargetDispProp = TCComponentType.getPropertiesSet( referncedispdata, targetDispProp ); 
			String[][] retTargetDispProp = TCComponentType.getPropertiesSet( referncedispdataList, targetDispProp );//TC10.1 Upgrade
			
			for( int index = 0; index < referncedispdata.length; ++index )
			{
				if( retTargetDispProp[index][0].equalsIgnoreCase( retTargetItemProp[1] ) )
				{
					boolean newRelease = isFirstRev( targetItemRev );
					NOVEnDispositionTabPanel_v2 dispPanel = new NOVEnDispositionTabPanel_v2( enPanel, enPanel.getSession(),
							referncedispdata[index], newRelease, targetItemRev ,referncedispdata,index );
					dispPanel.createBOMPanel( targetItemRev );
//					dispPanel.misGroup = NOVMissionHelper_v2.isGroup( reg.getString("misPref.NAME"),
//							targetItemRev, true );
					dispPanel.createUI( isReleased );
					dispPanel.setTargetItemRev( targetItemRev );
					String itemId = retTargetItemProp[1].toString();
					ImageIcon typeicon=TCTypeRenderer.getIcon( targetItemRev );
					enPanel.enautomatonTabbePane.addTab( itemId, typeicon, dispPanel);
					
					break;
				}
			}
			
			setTargetStatus();
		}
		catch(TCException tc)
		{
			tc.printStackTrace();

		}
	}
	

	public void adddispTabs(TCComponent[] newdispData)
	{
		//update disp data		
		try
		{		
			//get the Target Item IDs
			String itemIds[];	
			String targetRevIds[];
			Vector<String> vtargetIds=new Vector<String>();
			Vector<String> vtargetRevIds=new Vector<String>();
			Vector<TCComponent>newTargets=new Vector<TCComponent>();
			
			for(int i=0;i<newdispData.length;i++)
			{		
				//if(!(newdispData[i].getProperty("object_type").equalsIgnoreCase("Documents Revision")))
				//{
				vtargetIds.add(((TCComponentItemRevision)newdispData[i]).getProperty("item_id"));
				vtargetRevIds.add(((TCComponentItemRevision)newdispData[i]).getProperty("current_revision_id"));
				newTargets.add(newdispData[i]);
				//}

			}
			//Create the disp instances for the new Targets
			itemIds=new String[vtargetIds.size()];
			vtargetIds.copyInto(itemIds);
			targetRevIds=new String[vtargetRevIds.size()];
			vtargetRevIds.copyInto(targetRevIds);
			System.out.println ( "Inside adddispTabs(TCComponent[] newdispData)");
			
			TCComponent[] newdispInstances=createDispInstances(newdispData);
			for(int k=0;k<newdispInstances.length;k++)
			{
				targetdispMap.put(newdispInstances[k],newTargets.get(k));
				disptargetMap.put(newTargets.get(k), newdispInstances[k]);
			}
			//Update Disp Info
			TCComponent[] updateddispData=new TCComponent[referncedispdata.length+newdispInstances.length];		
			int k=0, i=0;
			for(k=0;k<referncedispdata.length;k++)
			{
				updateddispData[k]=referncedispdata[k];
			}
			for(;k<(referncedispdata.length+newdispInstances.length);k++)
			{
				updateddispData[k]=newdispInstances[i++];
			}
			referncedispdata=updateddispData;
			//save form
			enPanel.saveDipsDatatoForm();
			//Create new Disp Tabs
			//createDispTabPanels(newdispInstances);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void removeDispTab(TCComponent tobRemoved)
	{
		TCComponent dispComptoBremoved=null;
		try {
			if(!(tobRemoved.getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE"))))
			{
				String item_id=tobRemoved.getProperty("item_id");
				//remove the tab panel also;
				for(int k=0;k<enPanel.enautomatonTabbePane.getTabCount();k++)
				{
					String title=enPanel.enautomatonTabbePane.getTitleAt(k);
					if(title.equalsIgnoreCase(item_id))
					{
						TCComponentItem item = ((TCComponentItemRevision)tobRemoved).getItem();
						String status = item.getProperty("release_status_list");
						if (status.equals(reg.getString("SUS.TYPE"))) 
						{
							String currentStat = status;
							String initialStat = disptargetMap.get(tobRemoved).getTCProperty("currentstatus").getStringValue();
							TCUserService userService =  tobRemoved.getSession().getUserService();
							Object[] objs =  new Object[3];
							objs[0]=item;
							objs[1]=currentStat;
							objs[2]=initialStat;
							TCComponent comp = (TCComponent)userService.call("NATOIL_setInitialStatus", objs);
							comp.refresh();
							//System.out.println("After server call :"+comp.getProperty("release_status_list"));
						}
						NOVEnDispositionTabPanel_v2 dispPanel=(NOVEnDispositionTabPanel_v2)enPanel.enautomatonTabbePane.getComponentAt(k);
						dispComptoBremoved=dispPanel.dispcomp;
						enPanel.enautomatonTabbePane.remove(k);
						break;
					}

				}
			}
			dispComptoBremoved=disptargetMap.get(tobRemoved);
			if(dispComptoBremoved!=null)
			{
				int i=0;		
				TCComponent[] updateddispData=new TCComponent[referncedispdata.length-1];
				for(int k=0;k<referncedispdata.length;k++)
				{
					if(referncedispdata[k]!=dispComptoBremoved)
						updateddispData[i++]=referncedispdata[k];
				}
				//dispComptoBremoved.delete();
				referncedispdata=updateddispData;
				//save form
				enPanel.saveDipsDatatoForm();
				setTargetStatus();
			}

		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}

	}
	
	public void saveDispData()
	{
		if(enPanel!=null)
		{
			boolean isStatusSet = false;
			getTargetStatus();
			for( int index = 0; index < referncedispdata.length; ++index )
			{
				boolean newRelease = isNewRelease( targetdispMap.get( referncedispdata[index] ) );
				NOVEnDispositionTabPanel_v2 dispPanel = new NOVEnDispositionTabPanel_v2( enPanel, enPanel.getSession(),
						referncedispdata[index], newRelease , targetdispMap.get(referncedispdata[index]),referncedispdata,index);
				try
				{
					if( !( targetdispMap.get( dispPanel.dispcomp ).getProperty( "object_type" ).equalsIgnoreCase(
						reg.getString( "DocRev.TYPE" ) ) ) )
					{
						String status = targetStatusMap.get( targetdispMap.get( dispPanel.dispcomp ) );
						
						if( enPanel.enautomatonTabbePane.getComponentCount() > 1 )
						{
							NOVEnDispositionTabPanel_v2 openedDispPanel =( NOVEnDispositionTabPanel_v2)enPanel.enautomatonTabbePane.getComponentAt( 1 );
							if( openedDispPanel.isCopyAll )
							{
								String dispPropArr[] = new String[]{ "dispinstruction","inprocess","ininventory","assembled","infield" };
								String retDispPropArr[] = openedDispPanel.dispcomp.getProperties( dispPropArr );
								dispPanel.dispcomp.setProperties( dispPropArr, retDispPropArr );
								dispPanel.dispcomp.setProperty( "futurestatus", status );
							}
							else
							{
								 if( openedDispPanel.dispcomp.getTCProperty( "targetitemid" ).getStringValue().equalsIgnoreCase( 
									 dispPanel.dispcomp.getTCProperty( "targetitemid" ).getStringValue() ) )
								 {
									 openedDispPanel.saveData( status );
									 openedDispPanel.dispcomp.refresh();
								 }
							}
						}
						else
						{
							dispPanel.dispcomp.setProperty( "futurestatus", status );
						}
						isStatusSet = true;
					}
				}
				catch (TCException e)
				{
					e.printStackTrace();
				}
			}
			
			if( !isStatusSet )
			{
				for( int index = 0; index < referncedispdata.length; ++index )
				{
					boolean newRelease = isNewRelease( targetdispMap.get( referncedispdata[index] ) );
					NOVEnDispositionTabPanel_v2 dispPanel = new NOVEnDispositionTabPanel_v2( enPanel, enPanel.getSession(),
						referncedispdata[index], newRelease , targetdispMap.get(referncedispdata[index]),referncedispdata,index);
					try
					{	
						if(!( targetdispMap.get(dispPanel.dispcomp).getProperty("object_type").equalsIgnoreCase(reg.getString("DocRev.TYPE"))))
						{
							String status = targetStatusMap.get(targetdispMap.get(dispPanel.dispcomp));
//							dispPanel.saveFutureStatus( status );
							if( newRelease )
							{
								dispPanel.dispcomp.setProperty( "futurestatus", status );
							}
						}
					}
					catch( TCException e )
					{
						e.printStackTrace();
					}
				}
			}
		}
	}

	public boolean isNewRelease(TCComponent comp)
	{
		//Aparna: modified to check the no of revs for new release instead of rev value
		try
		{
			if(comp instanceof TCComponentItemRevision)
			{
				//String revid=((TCComponentItemRevision)comp).getProperty("current_revision_id");
				//int revscount=((TCComponentItemRevision)comp).getItem().getChildren( "revision_list" ).length;
				int revscount=((AIFComponentContext[])(((TCComponentItemRevision)comp).getItem().getRelated("revision_list"))).length;
				//if((revid.equalsIgnoreCase("A"))||(revid.equalsIgnoreCase("01")))
				if(revscount>1)
					return false;
				else
					return true;
			}

		}catch(TCException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public void getTargetStatus()
	{		
		if(enPanel!=null)
		{
			NOVEnTreeTableModel treeModel=(NOVEnTreeTableModel)enPanel.targetsPanel.targetsTable.getModel();
			int childcount=((TreeTableNode)treeModel.getRoot()).getChildCount();			
			for(int k=0;k<childcount;k++)
			{											
				NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)treeModel.getRoot()).getChildAt(k);
				TCComponent targetcomp=treeNode.context;
				//TCComponent comp=((NovTreeTableNode)enPanel.targetsPanel.targetsTable.getNodeForRow(k)).context;
				String status=enPanel.targetsPanel.targetsTable.futureStatMap.get(targetcomp);//enPanel.targetsPanel.targetsTable.getValueAt(k, 4).toString();
				targetStatusMap.put(targetcomp, status);
				String currentstatus=treeNode.getProperty("Starting Status");
			}
		}
	}

	public void createMappedData( _EngNoticeForm_Panel_v2 enPanel )
	{
		String[] reqStrItemID = { "item_id" };
		try
		{
			this.enPanel = enPanel;
			targets = enPanel.getEnTargets();
			List<TCComponent> targetsList = new ArrayList<TCComponent>(targets);
			/*String[][] targetItemIDArr = TCComponentType.getPropertiesSet(
					targets.toArray( new TCComponent[targets.size()] ), reqStrItemID );*/
			String[][] targetItemIDArr = TCComponentType.getPropertiesSet(targetsList, reqStrItemID );//TC10.1 Upgrade
				
			String[] reqStrRefTargetItemID = { "targetitemid" };
			List<TCComponent> referncedispdataList = Arrays.asList(referncedispdata);
			//String[][] refTargetItemIDArr = TCComponentType.getPropertiesSet( referncedispdata,
				    //reqStrRefTargetItemID );
			String[][] refTargetItemIDArr = TCComponentType.getPropertiesSet( referncedispdataList,
                    reqStrRefTargetItemID );//TC10.1 Upgrade
		
			for( int targetIndex = 0; targetIndex < targets.size(); ++targetIndex )
			{
				for( int refIndex = 0; refIndex < referncedispdata.length; ++refIndex )
				{
					if( refTargetItemIDArr[refIndex][0].equalsIgnoreCase( targetItemIDArr[targetIndex][0] ) )
					{
						disptargetMap.put( targets.get( targetIndex ), referncedispdata[refIndex] );
						targetdispMap.put( referncedispdata[refIndex], targets.get( targetIndex ) );
						break;
					}
				}
			}
		}
		catch( TCException tce )
		{
			tce.printStackTrace();
		}
	}
	
	public void setTargetStatus()
	{
		try
		{
			if(enPanel != null)
			{
				enPanel.targetsPanel.targetsTable.futureStatMap.clear();
				enPanel.targetsPanel.targetsTable.currentStatMap.clear();
				NOVEnTreeTableModel treeModel = ( NOVEnTreeTableModel )enPanel.targetsPanel.targetsTable.getModel();
				int childcount = ( ( TreeTableNode )treeModel.getRoot()).getChildCount();			
				for( int index = childcount-1; index >= 0; --index )
				{	
					NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)treeModel.getRoot()).getChildAt( index );
					TCComponent targetcomp = treeNode.context;
					TCComponent dispcomp = disptargetMap.get( targetcomp );
					//TCDECREL-1484 - explicitly removing target from targetstable if
					//disposition is not present for it.
					if(dispcomp == null)
					{
						TreeTableNode selNode = (TreeTableNode)treeNode;
						enPanel.targetsPanel.targetsTable.removeTarget(targetcomp, selNode);
						continue;
					}
					if(dispcomp.getTCProperty("currentstatus").getStringValue() != null)
						enPanel.targetsPanel.targetsTable.currentStatMap.put( targetcomp, dispcomp.getTCProperty("currentstatus").getStringValue());				
					String status=treeNode.getProperty(reg.getString("StartStatus.TYPE"));//enPanel.targetsPanel.targetsTable.getValueAt(k, 3).toString();
					//usha
					String fstatus = "";
					if(status.length() >0)
					{
						//if( !(NOVMissionHelper_v2.isGroup( "_NOV_Mission_item_creation_groups_",
						if( !(NOVMissionHelper_v2.isGroup( reg.getString( "misPref.NAME"),
								targetcomp, true )))//1839-usha
						{
							if((status.compareTo("ENG")==0))
							{
								fstatus = dispcomp.getTCProperty("futurestatus").getStringValue();
								if((fstatus==null)||(fstatus.length()==0))
								{
									fstatus = reg.getString("MFG.TYPE");
								}
							}
							else
								fstatus = reg.getString("MFG.TYPE");
						}
						else //if MIssion group//1839-usha
						{
							fstatus = dispcomp.getTCProperty("futurestatus").getStringValue();
							if((fstatus==null)||(fstatus.length()==0)||(status.compareTo(reg.getString( "ARCHIVED.TYPE" ))==0))
							{
								fstatus = reg.getString( "STANDARD.TYPE" );
							}
							//status;
						}
					}
					//1839-usha
					else
					{
						if(NOVMissionHelper_v2.isGroup( reg.getString("misPref.NAME"),
								targetcomp, true ))
						{
							fstatus = dispcomp.getTCProperty("futurestatus").getStringValue();
							if((fstatus==null)||(fstatus.length()==0))
							{
								fstatus = reg.getString( "STANDARD.TYPE" );
							}							
						}
						else
							fstatus = reg.getString("MFG.TYPE");
					}
					enPanel.targetsPanel.targetsTable.futureStatMap.put(targetcomp, fstatus);
					String fstatus1 = treeNode.getProperty(reg.getString("FutureStatus.TYPE"));
					//1839-usha
					JComboBox combo= null;
					if( !(NOVMissionHelper_v2.isGroup( reg.getString("misPref.NAME"),
							targetcomp, true )))//1839-usha
					{
						String[] stat = {reg.getString( "MFG.TYPE" ), reg.getString( "SWAP.TYPE" ) };
						combo=new JComboBox(stat);
					}
					else
					{
						//String[] stat1 = {"Standard", "Cancelled", "Feature", "ENG", "Type5" };
						String[] stat1 =  {reg.getString( "CANCELLED.TYPE" ), reg.getString( "CONTROL.TYPE" ), 
								reg.getString( "CUSTOM.TYPE" ), reg.getString( "DISCONT.TYPE" ), 
								reg.getString( "ENGINEERING.TYPE" ), reg.getString( "FEATURE.TYPE" ), 
								reg.getString( "INTRO.TYPE" ), reg.getString( "MARKETING.TYPE" ), 
								reg.getString( "NONSTD.TYPE" ), reg.getString( "PHASEOUT.TYPE" ), 
								reg.getString( "STANDARD.TYPE" ), reg.getString( "VOID.TYPE" )
								};
						combo=new JComboBox(stat1);
					}
					//combo.setSelectedItem(fstatus);
					((NovTreeTableNode)((TreeTableNode)treeModel.getRoot()).getChildAt( index )).setProperty("Future Status", combo);
					//System.out.println("Future status set to "+fstatus);
				}
			};
		}catch(TCException tc)
		{
			tc.printStackTrace();
		}
	}
	public boolean isRevRDD(TCComponent newTargetItemRev)
	{
		try {
			if( newTargetItemRev.getProperty("object_type").toString().equalsIgnoreCase(
					reg.getString("DocRev.TYPE"))) 
			{
				String[] relation={"RelatedDefiningDocument"};
				AIFComponentContext[] contexts = newTargetItemRev.whereReferencedByTypeRelation(new String[0], relation);
				if(contexts!=null && contexts.length>0)
				{
					return true;					
				}
				else
					return false;
			}
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
