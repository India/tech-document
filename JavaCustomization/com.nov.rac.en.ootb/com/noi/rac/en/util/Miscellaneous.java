package com.noi.rac.en.util;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;

import com.teamcenter.rac.aif.AIFPortal;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.AIFSessionManager;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TcServiceGatewayCorbaConnection;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

/**
 * @author hughests
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Miscellaneous 
{
	static int vcount=0;
	
	public static void addDesktopLinks(AIFComponentContext[] components) {
		
		try {
			TCPreferenceService svc=null;
			String server=null;
			String port=null;
			String context=null;
			if (components != null) {
				svc = ((TCComponentDataset)components[0].getComponent()).getSession().getPreferenceService();
			}
			server = svc.getString(TCPreferenceService.TC_preference_site,"CETGWSServices_server");
			port = svc.getString(TCPreferenceService.TC_preference_site,"CETGWSServices_port");
			context = svc.getString(TCPreferenceService.TC_preference_site,"CETGWSServices_context");
			
			for (int i = 0; i < components.length; i++) {
				TCComponentDataset ds = (TCComponentDataset)components[i].getComponent();
				String dsname = ds.getProperty("object_name");
				String dstype = ds.getType();
				String id = null;
				if (components[i].getParentComponent() instanceof TCComponentItemRevision) {
					TCComponentItemRevision rev = (TCComponentItemRevision)components[i].getParentComponent();
					TCComponentItem item = rev.getItem();
					id = item.getProperty("item_id");
				}
				// String lnkName = "";
				// String fileName = "";
				String url="URL=http://"+server+":"+port+context;
				if (id != null) {
					// fileName = id + ".URL";
					url = url+"ProcessExport.jsp?ItemID="+id+"&Type="+dstype+"&DSName="+dsname;
				}
				else {
					// fileName = dsname + ".URL";
					url = url+"EmbeddedStreamer/"+ds.getUid()+"/viewable";
				}
				/*
				lnkName = "C:/documents and settings/"+System.getProperty("user.name")+"/Desktop/"+fileName;
				PrintWriter pw = new PrintWriter(new FileWriter(lnkName));
				pw.println("[InternetShortcut]");
                pw.println(url);
                pw.close();
                */
                StringSelection stringSelection = new StringSelection( url );
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents( stringSelection, null );
                
			}
		}
		catch (Exception e) {
				System.out.println("Error creating shortcut: "+e);
		}
		
	}
	
	public static byte[] getImageIcon(String str )
    {
       
            byte[] retval = null;
    		try {
    			InputStream is =  Miscellaneous.class.getClassLoader().getResourceAsStream(str);
    			//ClassLoader.getSystemResourceAsStream("platform:/plugin/com.nov.rac.smdl/com/noi/rac/form/images/add_16.png");
    			//InputStream is = this.getClass().getResourceAsStream("com/noi/rac/form/images/add_16.png");
    			is.toString();
    			retval = new byte[is.available()];
    			int lastRead = 0;
    			int bRead = 0;
    			int total = is.available();
    			int toRead = is.available();
    			while (lastRead < total) {
    				bRead = is.read(retval, lastRead, toRead);
    				lastRead += bRead;
    				toRead = total - lastRead;
    			}
    			/*FileOutputStream fos = new FileOutputStream(dtdFile);
    			fos.write(retval);
    			fos.close();*/
    		} catch (Exception e) {
    			MessageBox.post("Unable to export file (" + str + ") : " + e,
    					"Error!! Export file", 3);
        }
        return retval;
    }
	
	public static byte[] getIcon(String str) 
	{
		byte[] retval = null;
		try {
			InputStream is = ClassLoader.getSystemResourceAsStream(str);
			retval = new byte[is.available() + 1];
			int lastRead = 0;
			int bRead = 0;
			int total = is.available();
			int toRead = is.available();
			while (lastRead < total) {
				bRead = is.read(retval, lastRead, toRead);
				lastRead += bRead;
				toRead = total - lastRead;
			}
		} catch (Exception e) {
			System.out.println("Miscellaneous.getIcon Could not get Icon (" + str + ") : " + e);
		}

		return retval;
	}

	public static void dumpDatasetsForAMP1E(AIFComponentContext[] components)
	{

		String fileList = "";
		LinkedList allFiles = new LinkedList();
		Hashtable allFilesNames = new Hashtable();
		//int fileCount=0;
		
		try {
			for (int i = 0; i < components.length; i++) {
				TCComponentItemRevision component = null;
				if (components[i].getComponent() instanceof TCComponentItemRevision) {
					component = (TCComponentItemRevision) components[i]
							.getComponent();
				} else if (components[i].getComponent() instanceof TCComponentItem) {
					component = ((TCComponentItem) components[i]
							.getComponent()).getLatestItemRevision();
				}
				if (component != null) {
					AIFComponentContext[] related = component.getChildren();
					TCComponentDataset selected = null;
					for (int j = 0; j < related.length; j++) {
						if (related[j].getComponent() instanceof TCComponentDataset) {
							TCComponentDataset ds = ((TCComponentDataset) related[j]
									.getComponent());
							String dstype = ds.getReferenceProperty(
									"dataset_type").getProperty(
									"datasettype_name");
							if (dstype.equalsIgnoreCase("acaddwg")) {
								selected = ds;
							}
						}
					}

					File[] racFiles = selected.getFiles("DWG");
					if (i == 0) {
						fileList = racFiles[0].getAbsolutePath();

						System.out.println(racFiles[0].getName());
						System.out.println(racFiles[0].getPath());
						fileList = fileList.substring(0, fileList
								.lastIndexOf("\\"));
					}
					for (int j = 0; j < racFiles.length; j++) {
						allFiles.add(racFiles[j]);
						allFilesNames.put(racFiles[j], component.getProperty("item_id"));
					}
					
				}
			}

			boolean materialfileexists = new File(
					"y:\\amp1e\\dxf\\material.dat").exists();
			if (materialfileexists = true) {
				boolean materialfiledelete = new File(
						"y:\\amp1e\\dxf\\material.dat").delete();
			}
			String outFile = fileList + "\\amp1e.fileList";
			boolean filedeleted = new File(WindowsRegistry.getWindowsPathToPortal()+"/temp/amp1e.fileList")
					.delete();
			FileOutputStream fos = new FileOutputStream(outFile, false);
			for (int i = 0; i < allFiles.size(); i++) {
				File file = (File) allFiles.get(i);
				String filename2 = file.getName();
				filename2 = filename2.substring(0, filename2.indexOf("."))
						.substring(0, 5);
				boolean tempfileexists = new File(fileList + "\\" + filename2 + i + ".dwg").exists();
					if(tempfileexists == true){
						boolean tempfiledelete = new File(fileList + "\\" + filename2 + i + ".dwg").delete();
					}
				File file2 = new File(fileList + "\\" + filename2 + i + ".dwg");
				boolean success = file.renameTo(file2);
				String path = allFilesNames.get(file) + "\t"
						+ file2.getAbsolutePath() + "\r\n";
				fos.write(path.getBytes());
			}
			fos.close();

			if (allFiles.size() > 0) {
				try {
					TCSession session = (TCSession) (components[0]
							.getComponent()).getSession();
					TCPreferenceService prefServ = session
							.getPreferenceService();

					Runtime run = Runtime.getRuntime();
					String amp1eDir = prefServ.getString(
							TCPreferenceService.TC_preference_user,
							"AMP1E_LSEdir");
					String command = amp1eDir + "\\bin\\smdxf.exe /F "
							+ amp1eDir + "\\temp\\amp1e.fileList /L "
							+ amp1eDir + "\\bin\\smdxf.lsp";
					Process p = run.exec(command);
					p.waitFor();

				} catch (Exception e1) {
					System.err.println("Could not exec SMDXF: " + e1);
				}
			}

		} catch (Exception e) {
			System.err
					.println("Could not get Autocad Files for SMDSF processing: "
							+ e);
		}
	}


	
	//---------------------------------------------------------------------------------------
	/* Created on: 02/28/08 By Tejas
	 * This method is called when user right click on a dataset. It creates a link based on 
	 * which database they login to and put that link to a clipboard. This method is written
	 * to create newlink that points to flex portal. It will create a link based on what ever 
	 * the ItemID and dataset type.
	 * Example --> ItemID:D33328000-pro-001 DatasetType:MSWord Database: WCH
	 * New Link --> "http://cetws.wch.nov.com:7070/cetgws/ProcessExport.jsp?ItemID=D33328000-pro-001&Type=MSWord&FLD=temp
	 */
	
	
	//-------------------------------------------------------------------------------------------
	/* Created on: 02/28/08 By Tejas
	 * This method is called when user right click on a dataset. It creates a pdf file of a selected
	 * dataset and create a PDF dataset into the same revision and put the generated PDF file into
	 * the PDF dataset. Currently, it cannot generate PDF for more than 15MB file due to the 
	 * Adobe LiveCycle PDF webservices BLOB object limitation.
	 */

	//================================================================================================
	/* Created on: 02/28/08 By Tejas
	 * This method is gets files from dataset.
	 */
	public static File[] getFilesFromDataset(TCComponentDataset dataset) throws TCException, IOException
	{
		File[] racFiles = null;
		if(dataset.getType().equalsIgnoreCase("MSWord"))
		{
			racFiles = dataset.getFiles("word");
		}
		else if(dataset.getType().equalsIgnoreCase("ACADDWG"))
		{
			racFiles = dataset.getFiles("DWG");
		}
		else if(dataset.getType().equalsIgnoreCase("MSVisio"))
		{
			racFiles = dataset.getFiles("MSVisio");
		}
		else if(dataset.getType().equalsIgnoreCase("MSExcel"))
		{
			racFiles = dataset.getFiles("excel");
		}
		else if(dataset.getType().equalsIgnoreCase("MSPowerPoint"))
		{
			racFiles = dataset.getFiles("powerpoint");
		}
		else if(dataset.getType().equalsIgnoreCase("Image"))
		{
			racFiles = dataset.getFiles("Image");
			
			ArrayList filelist = new ArrayList();
			for(int zz = 0; zz < racFiles.length; zz++)
			{
				String fileName = racFiles[zz].getName();
				String ext = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
				if(ext.equalsIgnoreCase("bmp") ||
						ext.equalsIgnoreCase("gif") ||
						ext.equalsIgnoreCase("tif") ||
						ext.equalsIgnoreCase("tiff") ||
						ext.equalsIgnoreCase("jpg") ||
						ext.equalsIgnoreCase("jpeg"))
				{
					filelist.add(racFiles[zz]);
				}
			}
				
			if(filelist.size() > 0)
			{
				racFiles = null;
				racFiles = new File[filelist.size()];
				for(int tt = 0; tt < filelist.size(); tt++)
				{
					racFiles[tt] = (File) filelist.get(tt);
				}
			}
		}
		return racFiles;
	}

	

	public static String getTemplateFooter() {
		String Template = "<w:sectPr><w:pgSz w:w=\"12240\" w:h=\"15840\"/><w:pgMar w:top=\"1440\" w:right=\"1800\" w:bottom=\"1440\" w:left=\"1800\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\"/><w:cols w:space=\"720\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></wx:sect></w:body></w:wordDocument>";

		return Template;

	}

	public static String stringNVL(String in, String nvl) {
		if (in == null)
			return nvl;
		else
			return in;
	}

	

	public static String getLastActionUser(LinkedList list, String action) {/*

		AuditInfo lastRelease = null;

		for (int i = 0; i < list.size(); i++) {
			String type;
			AuditInfo item = (AuditInfo) list.get(i);
			if (item.status.equalsIgnoreCase(action))
				lastRelease = item;
		}
		if (lastRelease != null)
			return (lastRelease.user);
		else
			return "*";
	*/
		return "";}

	public static String getLastReleaseUser(LinkedList list) {
		return getLastActionUser(list, "Release");
	}

	public static String getLastSignoffUser(LinkedList list) {
		return getLastActionUser(list, "perform-signoffs");
	}

    public static void copyFile(String src,String dstdir,String dstname) {
    	try {
        	File puidPath = new File(dstdir);
        	puidPath.mkdirs();
        	String outputPath = dstdir+dstname;
        	String htmPath=null;
        	File input = new File(src);
        	File output = new File(outputPath);
        	FileInputStream fin = new FileInputStream(input);
        	FileOutputStream fout = new FileOutputStream(output);
        	FileOutputStream fhtm=null;
        	FileChannel inc = fin.getChannel();
        	FileChannel out = fout.getChannel();
        	FileChannel htmCh=null;
        	
       		inc.transferTo(0,inc.size(),out);
        	inc.close();
        	out.close();
        	fin.close();
        	fout.close();    		
    	}
    	catch (Exception e) {
    		System.out.println("Could not copy file "+src+" to "+dstdir+dstname+": "+e);
    	}
    }
	public static boolean writeHTTPStreamToTemp(String url,String filename) {
		boolean retval = false;
		
		try {
			URL theUrl = new URL(url);
			InputStream theStream = theUrl.openStream();
			String tempDir=System.getenv("temp");
			File testFile = new File( tempDir+"/"+filename);//WindowsRegistry.getWindowsPathToPortal()+"/temp/"+filename);
			if (testFile.exists())
				testFile.delete();
			FileOutputStream aFile = new FileOutputStream( tempDir+"/"+filename);//WindowsRegistry.getWindowsPathToPortal()+"/temp/"+filename);
			byte[] theData = new byte[4096];
			int bytesWritten=0;
			int bytesRead=0;
			while ( (bytesRead=theStream.read(theData,0,4096)) > 0) {
				aFile.write(theData,0,bytesRead);
				bytesWritten += bytesRead;
			}
			aFile.close();
			return true;
		}
		catch (Exception e) {
			System.out.println("Could not get file from stream: "+e);
		}
		return retval; 
		
	}

	/*
	public static boolean PostToCETJMSQueue(TCSession session, String message) {

		String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";

		QueueConnectionFactory qconFactory;
		QueueConnection qcon;
		QueueSession qsession;
		QueueSender qsender;
		javax.jms.Queue queue;
		TextMessage msg;
		try {
			
			TCPreferenceService svc = session.getPreferenceService();
			String jms_url = svc
					.getString(TCPreferenceService.TC_preference_all,
							"CETGJMS_Server");
			String jms_factory = svc.getString(
					TCPreferenceService.TC_preference_all,
					"CETGJMS_Factory");
			String jms_queue = svc.getString(
					TCPreferenceService.TC_preference_all, "CETGJMS_Queue");
			
			Hashtable env = new Hashtable();
			env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
			env.put(Context.PROVIDER_URL, jms_url);
			InitialContext ic = new InitialContext(env);
			qconFactory = (QueueConnectionFactory) ic.lookup(jms_factory);
			qcon = qconFactory.createQueueConnection();
			qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			queue = (javax.jms.Queue) ic.lookup(jms_queue);
			qsender = qsession.createSender(queue);
			msg = qsession.createTextMessage();
			qcon.start();
			msg.setText(message);
			qsender.send(msg);
			qsender.close();
			qsession.close();
			qcon.close();
			return true;

		} catch (Exception e) {
			try {
			NOVMail.sendEmail(
					new String[] {"cetsupport@nov.com"}, 
					null, "stan.hughes@nov.com", "Error posting to JMS queue", 
					message, null, "smtp.nov.com");
			}
			catch (Exception ee) {
				System.out.println("Error sending failure notification via email"+ee);
			}
			System.out.println("Error posting message to JMS Queue: " + e);
		}
		return false;
		
	}
	*/
    public static TCSession login(AIFPortal portal,TCSession racSession,String servername,String marker,int port,String username,String password,String group) {
    	
    	System.out.println("Getting site_specific");
        Registry registry = Registry.getRegistry("site_specific");
    	System.out.println("Getting registry vals");
        String transportInUse = registry.getString 
        ("portalCommunicationTransport", "iiop" );
    	System.out.println("Getting site_specific");
    	
        try {
        	System.out.println("Opening portal");
          portal = new AIFPortal(false);
          // Get the Session Manager
      	System.out.println("Getting the session manager");
          AIFSessionManager sessionManager = portal.getKernel().getSessionManager();
          // From the session manager, create a new session (i.e TCSession)
      	System.out.println("Getting a session");
          racSession = (TCSession) sessionManager.getASession(
              "com.ugsolutions.rac.kernel.TCSession", null);
          //Try to login
      
      	System.out.println("Getting serverConnection");
          TcServiceGatewayCorbaConnection serverConnection = 
        	  new TcServiceGatewayCorbaConnection ( "localhost", servername);
       
      	System.out.println("Setting marker name");
          serverConnection.setMarkerServerName(marker);
      	System.out.println("Setting the port");
          serverConnection.setPort ( port );
      	System.out.println("Connecting...");
          
          //serverConnection.connect();TC10.1 Upgrade
      	System.out.println("logging in");
          
          racSession.login(serverConnection,username,password,group,"");
          System.out.println("Successfully logged in");
          return racSession;
        }
        catch (Exception ex) {
          ex.printStackTrace();
          System.out.println(ex);
          System.exit(1);
        }
        return null;
      }
	
}
