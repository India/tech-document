package com.noi.rac.en.util;

import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.noi.rac.en.customize.util.NOVConstants;

/**
 * @author niuy
 * @Date:  Jul 20, 2007
 * @usage: The class is used to 
 */
public class NovSearchableList extends JList {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7531137945338012060L;
	private SearchField searchField;
    private int currentIndex  = -1;
    private ArrayList itemList;
      public NovSearchableList() {
        super();
        setModel (new SelectModel());
        searchField = new SearchField ();
    }
    public NovSearchableList(ArrayList itemList) {
        super();
        this.itemList=itemList;
        setModel (new SelectModel());
        searchField = new SearchField ();
    }

   	public void setModel (ListModel m) {
        if (! (m instanceof SelectModel))
            throw new IllegalArgumentException();
        super.setModel (m);
    }

    public void addItem (Object o) {
        ((SelectModel)getModel()).addElement (o);
        
    }

    public JTextField getSearchField() {
        return searchField;
    }

     // inner class to provide filtered model
    class SelectModel extends AbstractListModel {
        
        /**
		 * 
		 */
		private static final long serialVersionUID = -4534901708728416484L;
		public SelectModel() {
            super();
            
        }
        public Object getElementAt (int index) {
            if (index < itemList.size())
                return itemList.get (index);
            else
                return null;
        }
        public int getSize() {
        	if(itemList ==null)
        		return  0;
            else
            	return itemList.size();
        }
        public void addElement (Object o) {
            itemList.add (o);
        }
        private void reSearch() {
            String term = getSearchField().getText().toLowerCase();
           if(itemList != null && itemList.size()>0 && term != null && term.trim().length()>0 && !term.equalsIgnoreCase(NOVConstants.TYPE_PROJECT_NAME_HERE)  ){
            for (int i=0; i<itemList.size(); i++){
            	String currentItem = (itemList.get(i).toString()).toLowerCase();
                if (currentItem.startsWith(term)){
                	currentIndex = i;
                	break;
                }
            } 
            }
            setSelectedIndex(currentIndex);
     		ensureIndexIsVisible(currentIndex);
         };
    }

    // inner class provides filter-by-keystroke field
    class SearchField extends JTextField implements DocumentListener {
        /**
		 * 
		 */
		private static final long serialVersionUID = -2583541529008563795L;
		public SearchField () {
            super();
            getDocument().addDocumentListener (this);
        }
        public void changedUpdate (DocumentEvent e) { ((SelectModel)getModel()).reSearch(); }
        public void insertUpdate (DocumentEvent e) {((SelectModel)getModel()).reSearch(); }
        public void removeUpdate (DocumentEvent e) {((SelectModel)getModel()).reSearch(); }
    }

	public void setItemList(ArrayList itemList) {
		this.itemList = itemList;
        setModel (new SelectModel());
	}
	public void setListData(Object[] listValues){
		ArrayList cuArrayList = new ArrayList();
		for(int i = 0; i <listValues.length; i++){
			cuArrayList.add(listValues[i]);
		}
		this.itemList = cuArrayList;
		setModel (new SelectModel());
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	
}
