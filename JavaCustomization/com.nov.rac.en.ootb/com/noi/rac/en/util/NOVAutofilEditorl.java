/*================================================================================
                     Copyright (c) 2009 National Oilwell Varco
                     Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVAutofilEditor.java
 Package Name: com.noi.rac.commands.newrsitem
 ================================================================================
 Modification Log
 ================================================================================
 Revision       Date         Author        Description  
 1.0            2009/02/02   harshadam     Initial Creation
 1.1            2009/02/03   varunk        Code Cleanup     
 1.2            2009/02/03   harshadam     Review & Code Formatting                      
 ================================================================================*/
package com.noi.rac.en.util;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.text.*;

import com.teamcenter.rac.form.textfield.FormTextField;

/**
 * NOVAutofilEditorl : class NOVAutofilEditorl extends JTextField *
 * 
 */
public class NOVAutofilEditorl extends FormTextField
{
    /**
     * 
     */
    private static final long serialVersionUID  = 1L;
    protected List            data              = null;
    protected BasicComboPopup popupmenu         = null;
    private JComboBox         popcombobox       = null;
    private boolean           keyEvent          = false;
    public boolean            keyEventToNeglect = false;
    private Vector            treeNodeInfo      = new Vector();

    /**
     * NOVDocument : class NOVDocument extends PlainDocument
     * 
     * 
     */
    class NOVDocument extends PlainDocument
    {
        private static final long serialVersionUID = 1L;

        /**
         * replace 
         * 
         * @param start : integer 
         * @param end : integer
         * @param str : string
         * @param attrbset : AttributeSet
         * @throws BadLocationException
         */
        public void replace(int start, int end, String str,
                AttributeSet attrbset) throws BadLocationException
        {
            super.remove(start, end);
            insertString(start, str, attrbset);
        }

        /**
         * remove 
         * 
         * @param i : integer
         * @param j : integer
         * @throws BadLocationException
         */
        public void remove(int i, int j) throws BadLocationException
        {
            if (popupmenu != null)
            {
                popupmenu.hide();
            }
            super.remove(i, j);
        }

        /**
         * insertString
         * 
         * @param  offset : int
         * @param  str : string
         * @param attributeset :
         *                  {@link AttributeSet}
         * @throws BadLocationException         
         */
        public void insertString(int offset, String str,
                AttributeSet attributeset) throws BadLocationException
        {
            if (popupmenu != null)
            {
                popupmenu.hide();
            }
            if (keyEventToNeglect)
            {
                super.insertString(0, str, attributeset);
                return;
            }
            if (str == null || str.equals(""))
                return;
            Vector list = compareStr(getText(0, offset) + str);
            int len = (offset + str.length()) - 1;
            if (list.size() == 0)
            {
                String s2 = setstr(str);
                super.remove(0, getLength());
                super.insertString(0, s2, attributeset);
                return;
            }
            String s2 = setstr(list.get(0).toString());
            super.remove(0, getLength());
            super.insertString(0, s2, attributeset);
            addPopupMenu(list);
            setSelectionStart(len + 1);
            setSelectionEnd(getLength());
        }
    }

    /**
     * addPopupMenu 
     * @param v : Vector
     */
    public void addPopupMenu(Vector v)
    {
        popcombobox = new JComboBox(v.toArray());
        popupmenu = new BasicComboPopup(popcombobox);
        popcombobox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent me)
            {
                if (!keyEvent)
                {
                    setText(setstr(popcombobox.getSelectedItem().toString()));
                    setFocusable(false);
                    setFocusable(true);
                    requestFocus(true);
                    popupmenu.hide();
                }
                keyEvent = false;
            }
        });
        if (v.size() < 5)
        {
            int height = v.size() * 18;
            popupmenu.setPreferredSize(new Dimension(300, height));
        }
        else
        {
            popupmenu.setPreferredSize(new Dimension(300, 100));
        }
        int x = 0;
        try
        {
            if (((JTextField) this).getUI().modelToView(((JTextField) this), 0) != null)
            {
                x = ((JTextField) this).getUI().modelToView(
                        ((JTextField) this), 0).x;
                popupmenu.show(this, x, this.getHeight());
            }
        }
        catch (BadLocationException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * NOVAutofilEditorl : constructor NOVAutofilEditorl
     * 
     * @param list : List 
     * @param argtreeNodeInfo : Vector 
     */
    public NOVAutofilEditorl(List list, Vector argtreeNodeInfo)
    {
        super();
        treeNodeInfo = argtreeNodeInfo;
        setDocument(new NOVDocument());
        addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_DOWN)
                {
                    keyEvent = true;
                    int index = popcombobox.getSelectedIndex();
                    if (!(popcombobox.getItemCount() == index + 1))
                    {
                        popcombobox.setSelectedItem(popcombobox
                                .getItemAt(index + 1));
                    }
                }
                else if (keyCode == KeyEvent.VK_UP)
                {
                    keyEvent = true;
                    int index = popcombobox.getSelectedIndex();
                    if (!(popcombobox.getSelectedIndex() == 0))
                    {
                        popcombobox.setSelectedIndex(index - 1);
                    }
                }
                else if (keyCode == KeyEvent.VK_ENTER)
                {
                    keyEvent = true;
                    if (popupmenu.isVisible())
                    {
                        String node = popcombobox.getSelectedItem().toString();
                        setText(node);
                    }
                    if (popupmenu != null)
                    {
                        popupmenu.hide();
                    }
                    System.out
                            .println("popcombobox.getSelectedItem().toString(): "
                                    + popcombobox.getSelectedItem().toString());
                }
                else if (keyCode == KeyEvent.VK_TAB)
                {
                    keyEvent = true;
                    if (popupmenu != null)
                    {
                        popupmenu.hide();
                    }
                }
            }
        });
        /**
         * addFocusListener 
         */
        addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e)
            {
                if (popupmenu != null)
                {
                    String node = popcombobox.getSelectedItem().toString();
                    setText(node);
                    popupmenu.hide();
                }
            }
        });
        data = new LinkedList(list);
    }

    /**
     * setstr
     * @param str : string 
     * @return strActualText
     */
    public String setstr(String str)
    {
        String strActualText = "";
        String node = str;
        int startIndex = node.lastIndexOf("->");
        if (startIndex != -1)
            strActualText = node.substring(startIndex + 2, node.length());
        return strActualText;
    }

    /**
     * compareStr
     * @param str :string
     * @return tempVector
     */
    private Vector compareStr(String str)
    {
        int vSize = treeNodeInfo.size();
        Vector tempVector = new Vector();
        tempVector.clear();
        for (int index = 0; index < vSize; index++)
        {
            String node = (String) treeNodeInfo.get(index);
            int startIndex = node.lastIndexOf("->");
            if (startIndex != -1)
            {
                if (node.toLowerCase().startsWith(str.toLowerCase(),
                        startIndex + 2))
                    tempVector.addElement(node);
            }
        }
        return tempVector;
    }
    public Vector findMatches() {
    	
    	return(compareStr(getText()));
    }
    public String getComboBoxSelection() {
    	return (String)popcombobox.getSelectedItem();
    }
}
