package com.noi.rac.en.util;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import com.teamcenter.rac.util.Registry;

public class WindowsRegistry {

	private static final String REGQUERY_UTIL = "reg query ";
	private static final String REGSTR_TOKEN = "REG_SZ";
	// private static final String REGDWORD_TOKEN = "REG_DWORD";

	private static final String GET_EXCEL_PATH = REGQUERY_UTIL +
	"\"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Extensions\" /v xls";
	private static final String GET_LSE0910_PATH_32 = REGQUERY_UTIL +
	"\"HKLM\\Software\\UGS\\iMAN Portal LSE\\V9.1.2.0\" /v Path";
	private static final String GET_PORTAL2007_PATH_32 = REGQUERY_UTIL +
	"\"HKLM\\Software\\UGS\\Teamcenter\\10_0\" /v TC_ROOT";
	// private static final String GET_LSE0910_PATH_64 = REGQUERY_UTIL +
	// "\"HKLM\\Software\\Wow6432Node\\UGS\\iMAN Portal LSE\\V9.1.2.0\" /v Path";
	private static final String GET_AutoCad_Mgr_Version = REGQUERY_UTIL + "\"HKLM\\Software\\UGS\\AutoCAD Manager\" /s";
	private static final String GET_OOTB_Viewer_path = REGQUERY_UTIL + "\"HKLM\\Software\\UGS\\VisViewSEV\" /s";
	public static boolean CheckOotbViewer() {
		try {
			Process process = Runtime.getRuntime().exec(GET_OOTB_Viewer_path);
			StreamReader reader = new StreamReader(process.getInputStream());
			reader.start();
			process.waitFor();
			reader.join();

			String result = reader.getResult();
			if((null==result)||(result.length()==0))
				return false;
			else
				return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	public static String getExcelPath() {
		try {
			Process process = Runtime.getRuntime().exec(GET_EXCEL_PATH);
			StreamReader reader = new StreamReader(process.getInputStream());

			reader.start();
			process.waitFor();
			reader.join();

			String result = reader.getResult();
			int p = result.indexOf(REGSTR_TOKEN);

			if (p == -1)
				return null;

			return result.substring(p + REGSTR_TOKEN.length()).trim();
		}
		catch (Exception e) {
			return null;
		}
	}
	public static String getAutoCadMgrVersion()
	{
		try
		{ 			 
			// String lowACADVer=null;
			Process process = Runtime.getRuntime().exec(GET_AutoCad_Mgr_Version);
			StreamReader reader = new StreamReader(process.getInputStream());

			reader.start();
			process.waitFor();
			reader.join();

			String result = reader.getResult();

			int p = result.indexOf("HKEY_LOCAL_MACHINE\\Software\\UGS\\AutoCAD Manager\\3.0.1");

			if(p>=0)
			{
				return "3.0.1";
			}
			else if (p == -1)
			{
				p = result.indexOf("HKEY_LOCAL_MACHINE\\Software\\UGS\\AutoCAD Manager\\2.1");
				if (p>=0)
				{
					return "2.1.9";
				}
			}
			return null;
			//return result.substring(p + 5).trim();
		}
		catch (Exception e) {
			return null;
		}
	}
	static class StreamReader extends Thread {
		private InputStream is;
		private StringWriter sw;

		StreamReader(InputStream is) {
			this.is = is;
			sw = new StringWriter();
		}

		public void run() {
			try {
				int c;
				while ((c = is.read()) != -1)
					sw.write(c);
			}
			catch (IOException e) { ; }
		}

		String getResult() {
			return sw.toString();
		}
	}
	public static String getTCEVersion() {
		Registry registry1 = Registry.getRegistry("version");
		String tcVersion = registry1.getString("InternalVersion");
		return tcVersion;
	}
	public static String getWindowsPathToPortal() {
		try {
			String regQuery = "";
			if (getTCEVersion().toLowerCase().indexOf("v10") == 0) 
			    regQuery = GET_PORTAL2007_PATH_32;
			else
				regQuery = GET_LSE0910_PATH_32;
			
			Process process = Runtime.getRuntime().exec(regQuery);
			StreamReader reader = new StreamReader(process.getInputStream());

			reader.start();
			process.waitFor();
			reader.join();

			String result = reader.getResult();
			int p = result.indexOf(REGSTR_TOKEN);

			if (p == -1)
				return null;

			return result.substring(p + REGSTR_TOKEN.length()).trim();
		}
		catch (Exception e) {
			return null;
		}		  
	}
}
