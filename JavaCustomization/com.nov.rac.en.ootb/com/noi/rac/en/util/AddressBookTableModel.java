package com.noi.rac.en.util;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import com.teamcenter.rac.util.Registry;
/*import com.ugsolutions.rac.kernel.IMANComponent;
import com.ugsolutions.rac.kernel.IMANComponentDataset;
import com.ugsolutions.util.ArraySorter;*/

public class AddressBookTableModel extends AbstractTableModel {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Object[][] addrdata;	
	private Vector sorted= new Vector();
	private Registry reg;
	private String[] columnNames = null;
	
	public AddressBookTableModel(HashMap addrData)//, Vector Name) 
	{
		super();
         reg=Registry.getRegistry(this);
         columnNames = reg.getString("columnNames.LIST").split(",");
		
		setData(addrData);//, Name);
	}
	public AddressBookTableModel()
	{
		super();
		reg=Registry.getRegistry(this);
		columnNames = reg.getString("columnNames.LIST").split(",");
		addrdata=null;
	}
	private void setData(HashMap addrData)//, Vector Name)
	{		
		addrdata = new Object[addrData.size()][2];		
		if(addrData.size()>0)
		{			
			/***************sort the mail ids before displaying******************/
			
			List mapKeys = new ArrayList(addrData.keySet());
			List mapValues = new ArrayList(addrData.values());
			addrData.clear();
			//TreeSet sortedSet = new TreeSet(mapValues);
			TreeSet sortedSet = new TreeSet(mapKeys);
			Object[] sortedArray = sortedSet.toArray();
			int size = sortedArray.length; 
			for (int i=0; i<size; i++)
			{ 
				//addrData.put(mapKeys.get(mapValues.indexOf(sortedArray[i])), sortedArray[i]); 
				//addrdata[i][0]=mapKeys.get(mapValues.indexOf(sortedArray[i]));
				addrdata[i][0]=sortedArray[i];
				addrdata[i][1]=mapValues.get(mapKeys.indexOf(sortedArray[i]));
			}
//			Iterator keys=addrData.keySet().iterator();
//			int i=0;
//			while(keys.hasNext())
//			{
//				Object name=keys.next();
//				addrdata[i][0]=name;
//				addrdata[i][1]=addrData.get(name);
//				i++;
//			}
//	
			
		}else
		{
			addrdata=null;
		}
		
	}
    public int getColumnCount() { return columnNames.length; }
    public int getRowCount() { return addrdata == null? 0: addrdata.length; }
    public String getColumnName(int col) { return columnNames[col]; }
    public Object getValueAt(int row, int col) { return addrdata[row][col]; }
    public Class getColumnClass(int c) { return getValueAt(0,c).getClass(); }
    public boolean isCellEditable(int row,int col) { return false; } 

}