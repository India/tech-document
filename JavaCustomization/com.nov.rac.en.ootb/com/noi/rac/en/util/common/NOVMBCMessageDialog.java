package com.noi.rac.en.util.common;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;//TC10.1 Upgrade

//import com.teamcenter.rac.common.views.SWTResourceManager;

public class NOVMBCMessageDialog extends MessageDialog
{
    private static String m_message1;
    private static String m_message2;
    
    public NOVMBCMessageDialog(Shell parentShell, String dialogTitle, Image dialogTitleImage, String dialogMessage,
            int dialogImageType, String[] dialogButtonLabels, int defaultIndex)
    {
        super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels,
                defaultIndex);
    }
    
    public static boolean openWarningAndQuestion(Shell parent, String title, String message, String message1,
            String message2)
    {
        NOVMBCMessageDialog dialog = new NOVMBCMessageDialog(parent, title, null, message, WARNING, new String[] {
                IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL }, 1);
        dialog.setShellStyle(dialog.getShellStyle() | SWT.SHEET);
        m_message1 = message1;
        m_message2 = message2;
        return dialog.open() == 0;
    }
    
    protected Control createCustomArea(Composite parent)
    {
        super.createCustomArea(parent);
        Label label1 = new Label(parent, SWT.READ_ONLY);
        Label label2 = new Label(parent, SWT.READ_ONLY);
        
        label1.setForeground(SWTResourceManager.getColor(255, 0, 0));//TC10.1 Upgrade(org.eclipse.wb.swt.SWTResourceManager)
        
        label1.setText("\t" + m_message1);
        label2.setText("\t" + m_message2);
        
        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.heightHint = 25;
        gridData.verticalIndent = -2;
        label1.setLayoutData(gridData);
        return parent;
    }
    
}
