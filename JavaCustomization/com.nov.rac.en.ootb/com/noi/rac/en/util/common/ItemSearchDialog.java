/* ============================================================
  
  File description: 

  Filename: ItemSearchDialog.java 
  Module  : com/noi/rac/en/util/common

  Description :
  This class will be invoked when we select search button in Mass BOM Change dialog.
  This will display the search results when user enters Item Id or Alternate Id
  
  Date         Developers    		Description
  25-11-2011   Rajyalaxmi   	  	Initial Creation

  $HISTORY
  ============================================================ */
package com.noi.rac.en.util.common;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

/**
 * @author gander
 * 
 */
public class ItemSearchDialog extends AbstractSWTDialog implements INOVSearchProvider
{
    
    TCTree itemsearchTree;
    TCTable itemsearchTable;
    Button searchBtn;
    Text itemIdText;
    Label searchStatusLbl;
    Button btnOk;
    Button btnCancel;
    ProgressBar pb;
    String itemToSearch = null;
    String[] objTYPE = null;
    
    TCSession session;
    boolean isItem2Change;
    MassBOMChangeDialog massBOMChDlg;
    TCComponentItemRevision itemRevtoSet;
    TCComponentItem itemToSet;
    Map<Integer, String> itemInfo = new HashMap<Integer, String>();
    String sItemId = null;
    String sAltID = null;
    public static final int POM_date = 2;
    public static final int POM_string = 8;
    public static final int POM_typed_reference = 9;
    String searchLabelTxt = null;
    INOVSearchResult searchResults = null;
    boolean invokeSrch = true;
    
    protected Registry reg = Registry.getRegistry(this);
    private Vector<String> m_itemSearchData;
    private int m_searchResultRowsCount;
    private int m_whereUsedItemsColCount;
    private TCComponentItemType m_tcCompType;
    private Map<String, String> m_itemSearchRevStatusMap = new HashMap<String, String>();
    private Map<String, String> m_itemSearchItemTypeMap = new HashMap<String, String>();
    
    public ItemSearchDialog(Shell parentShell) throws TCException, IOException
    {
        super(AIFUtility.getActiveDesktop().getShell());
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        session = (TCSession) AIFUtility.getDefaultSession();
        m_tcCompType = (TCComponentItemType) session.getTypeComponent("item");
    }
    
    public ItemSearchDialog(MassBOMChangeDialog massBOMChangeDlg, String itmSearch) throws TCException, IOException
    {
        super(massBOMChangeDlg.getShell());
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        session = (TCSession) AIFUtility.getDefaultSession();
        m_tcCompType = (TCComponentItemType) session.getTypeComponent("item");
        massBOMChDlg = massBOMChangeDlg;
        this.itemToSearch = itmSearch;
        createDialogArea(new Shell());
        search_PopulateItems(true);
        
    }
    
    public ItemSearchDialog(MassBOMChangeDialog massBOMChangeDlg, boolean isItemToChange) throws TCException,
            IOException
    {
        super(massBOMChangeDlg.getShell());
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        massBOMChDlg = massBOMChangeDlg;
        isItem2Change = isItemToChange;
        
        if (isItem2Change == true)
        {
            itemToSearch = massBOMChDlg.txtItemChange.getText();
            searchLabelTxt = reg.getString("CompItemTxt");
        }
        else
        {
            itemToSearch = massBOMChDlg.txtRepItem.getText();
            searchLabelTxt = reg.getString("ReplItemTxt");
        }
        session = (TCSession) AIFUtility.getDefaultSession();
        m_tcCompType = (TCComponentItemType) session.getTypeComponent("item");
    }
    
    public ItemSearchDialog(MassBOMChangeDialog massBOMChangeDlg, boolean isItemToChange, boolean invokePerfSearch)
            throws TCException, IOException
    {
        super(massBOMChangeDlg.getShell());
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        massBOMChDlg = massBOMChangeDlg;
        isItem2Change = isItemToChange;
        invokeSrch = invokePerfSearch;
        
        if (isItem2Change == true)
        {
            itemToSearch = massBOMChDlg.txtItemChange.getText();
            searchLabelTxt = reg.getString("CompItemTxt");
        }
        else
        {
            itemToSearch = massBOMChDlg.txtRepItem.getText();
            searchLabelTxt = reg.getString("ReplItemTxt");
        }
        session = (TCSession) AIFUtility.getDefaultSession();
        m_tcCompType = (TCComponentItemType) session.getTypeComponent("item");
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        
        parent.setLayout(new GridLayout(1, false));
        GridData gd_parent = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_parent.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(parent, 490);
        gd_parent.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(parent, 150);
        parent.setLayoutData(gd_parent);
        
        Composite itemSrchComp = new Composite(parent, SWT.NONE);
        itemSrchComp.setLayout(new GridLayout(3, false));
        GridData gd_itemSrchComp = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_itemSrchComp.heightHint = SWTUIHelper.convertHorizontalDLUsToPixels(itemSrchComp, 20);
        gd_itemSrchComp.widthHint = SWTUIHelper.convertVerticalDLUsToPixels(itemSrchComp, 250);
        itemSrchComp.setLayoutData(gd_itemSrchComp);
        
        Label lblItemId = new Label(itemSrchComp, SWT.NONE);
        lblItemId.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblItemId.setText(searchLabelTxt);
        
        itemIdText = new Text(itemSrchComp, SWT.BORDER);
        GridData gd_itemIdText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_itemIdText.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(itemSrchComp, 100);
        itemIdText.setLayoutData(gd_itemIdText);
        if (itemToSearch != null) // && itemToSearch.length() > 0 )
        {
            itemIdText.setText(itemToSearch);
        }
        
        Image searchIcon = reg.getImage("search.ICON");
        searchBtn = new Button(itemSrchComp, SWT.NONE);
        GridData gd_searchBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_searchBtn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(itemSrchComp, 20);
        gd_searchBtn.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(itemSrchComp, 12);
        searchBtn.setLayoutData(gd_searchBtn);
        searchBtn.setImage(searchIcon);
        searchBtn.setToolTipText(reg.getString("searchIcon.TOOLTIP"));
        
        itemIdText.addListener(SWT.DefaultSelection, new Listener()
        {
            // @Override
            public void handleEvent(Event e)
            {
                search_PopulateItems(true);
                
            }
        });
        
        searchBtn.addSelectionListener(new SelectionListener()
        {
            
            // @Override
            public void widgetSelected(SelectionEvent event)
            {
                search_PopulateItems(true);
                
            }
            
            // @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
        String[] attrNames = reg.getStringArray("ItemSearchResColumns", ",");
        AIFTableModel itemsearchTablemodel = new AIFTableModel(attrNames);
        itemsearchTable = new TCTable();
        itemsearchTable.setSession(session);
        itemsearchTable.setModel(itemsearchTablemodel);
        
        itemsearchTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        itemsearchTable.getTableHeader().setReorderingAllowed(false);
        itemsearchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        itemsearchTable.getColumnModel().getColumn(0).setCellRenderer(new CustomItemIdCellRenderer());
        itemsearchTable.getColumnModel().getColumn(2).setCellRenderer(new CustomTableCellRenderer());
        
        itemsearchTable.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    int selIndex = itemsearchTable.getSelectedRow();
                    Object selectedItemObj = itemsearchTable.dataModel.getValueAt(selIndex, 0);
                    
                    TCComponent selcomp = null;
                    try
                    {
                        
                        if (selIndex != -1)
                        {
                            /*
                             * String selPuid =
                             * itemInfo.get(Integer.valueOf(selIndex)); selcomp
                             * = session.stringToComponent(selPuid);
                             */
                            if (m_tcCompType != null)
                            {
                                selcomp = m_tcCompType.find(selectedItemObj.toString());
                            }
                        }
                        
                        if (selcomp == null)
                            return;
                        if (massBOMChDlg != null)
                        {
                            if (selcomp instanceof TCComponentItemRevision || selcomp instanceof TCComponentItem)
                            {
                                if (selcomp instanceof TCComponentItem)
                                {
                                    try
                                    {
                                        itemToSet = (TCComponentItem) selcomp;
                                        itemRevtoSet = ((TCComponentItem) selcomp).getLatestItemRevision();
                                    }
                                    catch (TCException e1)
                                    {
                                        e1.printStackTrace();
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        itemToSet = ((TCComponentItemRevision) selcomp).getItem();
                                    }
                                    catch (TCException e1)
                                    {
                                        e1.printStackTrace();
                                    }
                                    itemRevtoSet = (TCComponentItemRevision) selcomp;
                                }
                                if (isItem2Change)
                                {
                                    
                                    getShell().getDisplay().syncExec(new Runnable()
                                    {
                                        public void run()
                                        {
                                            massBOMChDlg.itemToChange = itemRevtoSet;
                                            massBOMChDlg.searchForwhereUsedItems(itemRevtoSet);
                                            
                                            // if
                                            // (massBOMChDlg.whereUsedItems.length
                                            // <= 0)
                                            // {
                                            // MessageBox.post(reg.getString("compItemNotUsedInAnyAsm"),"Warning",MessageBox.WARNING);
                                            //
                                            // }
                                            // else
                                            // {
                                            massBOMChDlg.otherGroupItemsTable.removeAllRows();
                                            massBOMChDlg.populateWhereUsedTable(itemRevtoSet, false);
                                            try
                                            {
                                                massBOMChDlg.txtItemChange.setText(itemToSet.getProperty(
                                                        reg.getString("itemIDProp")).toString());
                                                massBOMChDlg.txtItemChange.setEditable(false);
                                            }
                                            catch (TCException e)
                                            {
                                                // TODO Auto-generated catch
                                                // block
                                                e.printStackTrace();
                                            }
                                            if (massBOMChDlg.whereUsedItems.length > 0)
                                            {
                                                getShell().getDisplay().syncExec(new Runnable()
                                                {
                                                    public void run()
                                                    {
                                                        ItemSearchDialog.this.getShell().dispose();
                                                    }
                                                });
                                            }
                                        }
                                        
                                        // }
                                    });
                                }
                                else
                                {
                                    getShell().getDisplay().syncExec(new Runnable()
                                    {
                                        public void run()
                                        {
                                            try
                                            {
                                                massBOMChDlg.txtRepItem.setText(itemToSet.getProperty(
                                                        reg.getString("itemIDProp")).toString());
                                            }
                                            catch (TCException e)
                                            {
                                                // TODO Auto-generated catch
                                                // block
                                                e.printStackTrace();
                                            }
                                            massBOMChDlg.replacementItem = itemRevtoSet;
                                            massBOMChDlg.txtRepItem.setEditable(false);
                                        }
                                    });
                                    getShell().getDisplay().syncExec(new Runnable()
                                    {
                                        public void run()
                                        {
                                            ItemSearchDialog.this.getShell().dispose();
                                        }
                                    });
                                }
                            }
                        }
                    }
                    catch (TCException e2)
                    {
                        // TODO Auto-generated catch block
                        e2.printStackTrace();
                    }
                    
                }
            }
        });
        
        JScrollPane searchResPane = new JScrollPane(itemsearchTable);
        GridData grid_data1 = new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1);
        grid_data1.heightHint = 150;
        
        Composite seachResContainer = new Composite(parent, SWT.NONE);
        seachResContainer.setLayout(new FillLayout(SWT.HORIZONTAL));
        GridData gd_seachResContainer = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_seachResContainer.heightHint = 220;
        gd_seachResContainer.widthHint = 350;
        seachResContainer.setLayoutData(gd_seachResContainer);
        SWTUIUtilities.embed(seachResContainer, searchResPane, true);
        
        searchStatusLbl = new Label(parent, SWT.NONE);
        GridData gd_searchStatusLbl = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        gd_searchStatusLbl.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(parent, 300);
        searchStatusLbl.setLayoutData(gd_searchStatusLbl);
        
        Label horizonSeperator = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
        GridData gd_label = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
        gd_label.widthHint = 600;
        horizonSeperator.setLayoutData(gd_label);
        
        pb = new ProgressBar(parent, SWT.SMOOTH | SWT.APPLICATION_MODAL);
        GridData gd_pb = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
        gd_pb.widthHint = 600;
        pb.setLayoutData(gd_pb);
        pb.setVisible(false);
        
        Composite compositeBttn = new Composite(parent, SWT.NONE);
        GridLayout gl_compositeBttn = new GridLayout(3, false);
        gl_compositeBttn.marginRight = 0;
        gl_compositeBttn.marginTop = 0;
        compositeBttn.setLayout(gl_compositeBttn);
        GridData gd_compositeBttn = new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1);
        gd_compositeBttn.heightHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 20);
        compositeBttn.setLayoutData(gd_compositeBttn);
        
        btnOk = new Button(compositeBttn, SWT.NONE);
        GridData gd_btnOk = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_btnOk.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 40);
        gd_btnOk.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(compositeBttn, 12);
        btnOk.setLayoutData(gd_btnOk);
        btnOk.setText(reg.getString("OkBtnTxt"));
        
        btnOk.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent event)
            {
                
                int selIndex = itemsearchTable.getSelectedRow();
                Object selectedItemObj = itemsearchTable.dataModel.getValueAt(selIndex, 0);
                
                // Set<Integer> keySet1 = itemInfo.keySet();
                // Integer[] keyStrA = keySet1.toArray(new
                // Integer[keySet1.size()]);
                TCComponent selcomp = null;
                try
                {
                    if (selIndex != -1)
                    {
                        /*
                         * String selPuid =
                         * itemInfo.get(Integer.valueOf(selIndex)); selcomp =
                         * session.stringToComponent(selPuid);
                         */
                        if (m_tcCompType != null)
                        {
                            selcomp = m_tcCompType.find(selectedItemObj.toString());
                        }
                    }
                    if (selcomp == null)
                    {
                        if (isItem2Change)
                            MessageBox.post(reg.getString("selCompItem.WARN"), "Warning", MessageBox.WARNING);
                        else
                            MessageBox.post(reg.getString("selReplItem.WARN"), "Warning", MessageBox.WARNING);
                        return;
                        // return ;
                    }
                    if (massBOMChDlg != null)
                    {
                        if (selcomp instanceof TCComponentItemRevision || selcomp instanceof TCComponentItem)
                        {
                            if (selcomp instanceof TCComponentItem)
                            {
                                try
                                {
                                    itemToSet = (TCComponentItem) selcomp;
                                    itemRevtoSet = ((TCComponentItem) selcomp).getLatestItemRevision();
                                }
                                catch (TCException e1)
                                {
                                    e1.printStackTrace();
                                }
                            }
                            else
                            {
                                try
                                {
                                    itemToSet = ((TCComponentItemRevision) selcomp).getItem();
                                }
                                catch (TCException e)
                                {
                                    e.printStackTrace();
                                }
                                itemRevtoSet = (TCComponentItemRevision) selcomp;
                            }
                            if (isItem2Change)
                            {
                                massBOMChDlg.itemToChange = itemRevtoSet;
                                massBOMChDlg.searchForwhereUsedItems(itemRevtoSet);
                                
                                // if (massBOMChDlg.whereUsedItems.length <= 0)
                                // {
                                // MessageBox.post(reg.getString("compItemNotUsedInAnyAsm"),"Warning",MessageBox.WARNING);
                                //
                                // }
                                // else
                                // {
                                massBOMChDlg.otherGroupItemsTable.removeAllRows();
                                massBOMChDlg.populateWhereUsedTable(itemRevtoSet, false);
                                try
                                {
                                    massBOMChDlg.txtItemChange.setText(itemToSet.getProperty(
                                            reg.getString("itemIDProp")).toString());
                                }
                                catch (TCException e)
                                {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                massBOMChDlg.txtItemChange.setEditable(false);
                                if (massBOMChDlg.whereUsedItems.length > 0)
                                    ItemSearchDialog.this.getShell().dispose();
                            }// isItem2Change
                            else
                            {
                                try
                                {
                                    massBOMChDlg.txtRepItem.setText(itemToSet.getProperty(reg.getString("itemIDProp"))
                                            .toString());
                                }
                                catch (TCException e)
                                {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                massBOMChDlg.txtRepItem.setEditable(false);
                                massBOMChDlg.replacementItem = itemRevtoSet;
                                
                                ItemSearchDialog.this.getShell().dispose();
                            }
                            
                        }
                        
                    }
                    // }
                }
                catch (TCException e2)
                {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
            }
            
        });
        
        btnCancel = new Button(compositeBttn, SWT.NONE);
        GridData gd_btnCancel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_btnCancel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 40);
        gd_btnCancel.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(compositeBttn, 12);
        btnCancel.setLayoutData(gd_btnCancel);
        btnCancel.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                getShell().dispose();
            }
        });
        btnCancel.setText(reg.getString("cancelBtnTxt"));
        
        if (itemIdText.getText().trim().length() > 0)
        {
            search_PopulateItems(invokeSrch);
        }
        
        return parent;
    }
    
    /**
     * Create contents of the button bar.
     * 
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
        /*
         * createButton(parent, IDialogConstants.OK_ID,
         * IDialogConstants.OK_LABEL, true); createButton(parent,
         * IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
         */
    }
    
    protected void configureShell(Shell newShell)
    {
        super.configureShell(newShell);
        newShell.setText(reg.getString("ItemSrchDialogHeader"));
    }
    
    public INOVSearchResult performSearch(String srchCriteria)
    {
        
        itemInfo.clear();
        sItemId = srchCriteria;
        INOVSearchResult searchResult = null;
        try
        {
            String itemId = sItemId;
            
            if (itemId.trim().length() >= 3)
            {
                // if wild card exists in search string, remove the wild cards
                // and verify string length.
                // it should be min. 3 characters long.- Else throw error.
                char[] arr = itemId.trim().toCharArray();
                String inputId = "";
                for (int i = 0; i < arr.length; i++)
                {
                    if (arr[i] != '*')
                    {
                        inputId = inputId + arr[i];
                    }
                }
                if (inputId.length() < 3)
                {
                    MessageBox.post(reg.getString("searchWithMinInput.MSG"), "ERROR", MessageBox.ERROR);
                    return null;
                }
            }
            if (itemId.trim().length() < 3)
            {
                MessageBox.post(reg.getString("searchWithMinInput.MSG"), "ERROR", MessageBox.ERROR);
                return null;
            }
            
            INOVSearchProvider searchService = this;
            try
            {
                searchResult = NOVSearchExecuteHelperUtils.execute(searchService, -2);
                this.searchResults = searchResult;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return searchResult;
        
    }
    
    private void search_PopulateItems(boolean invokePerformSrch)
    {
        INOVSearchResult searchResult = null;
        INOVResultSet theResSet = null;
        searchStatusLbl.setText("");
        itemsearchTable.removeAllRows();
        m_itemSearchRevStatusMap.clear();
        m_itemSearchItemTypeMap.clear();
        if (invokePerformSrch)
        {
            searchResult = performSearch(itemIdText.getText());
        }
        else
        {
            searchResult = this.searchResults;
        }
        if (searchResult != null && searchResult.getResponse().nRows > 0)
        {
            massBOMChDlg.progressBar.setVisible(true);
            massBOMChDlg.progressBar.setSelection(50);
            theResSet = (INOVResultSet) searchResult.getResultSet();
            m_itemSearchData = theResSet.getRowData();
            m_searchResultRowsCount = theResSet.getRows();
            m_whereUsedItemsColCount = theResSet.getCols();
            
            objTYPE = new String[searchResult.getResponse().nRows];
            pb.setVisible(true);
            pb.setMinimum(0);
            pb.setMaximum(searchResult.getResponse().nRows);
            System.out.println("Found" + searchResult.getResponse().nRows + "objects");
            for (int i = 0; i < searchResult.getResponse().nRows; i++)
            {
                String itemSearchStatusValue = null;
                pb.setSelection(i);
                int rowIndex = m_whereUsedItemsColCount * i;
                Vector<String> rowData = new Vector<String>();
                String objPUID = m_itemSearchData.elementAt(rowIndex);
                String objID = m_itemSearchData.elementAt(rowIndex + 1);
                String objNAME = m_itemSearchData.elementAt(rowIndex + 2);
                String objtype = m_itemSearchData.elementAt(rowIndex + 3);
                
                String objDESC = m_itemSearchData.elementAt(rowIndex + 4);
                String group = m_itemSearchData.elementAt(rowIndex + 5);
                String rev = m_itemSearchData.elementAt(rowIndex + 6);
                String itemStatus = m_itemSearchData.elementAt(rowIndex + 7);
                String altId = m_itemSearchData.elementAt(rowIndex + 8);
                String rsOneType = m_itemSearchData.elementAt(rowIndex + 9);
                String revRelStatus = m_itemSearchData.elementAt(rowIndex + 10);
                String revProcessName = m_itemSearchData.elementAt(rowIndex + 11);
                
                rowData.add(objID);
                rowData.add(altId);
                rowData.add(rev);
                rowData.add(objNAME);
                rowData.add(objDESC);
                rowData.add(group);
                rowData.add(itemStatus);
                
                if ((revRelStatus != null) && (revRelStatus.length() > 0))
                {
                    itemSearchStatusValue = reg.getString("itemRevReleaseStatus");
                }
                else if ((revProcessName != null) && (revProcessName.length() > 0))
                {
                    itemSearchStatusValue = reg.getString("itemRevInprocessStatus");
                }
                else
                {
                    itemSearchStatusValue = reg.getString("itemRevWorkingStatus");
                }
                
                m_itemSearchRevStatusMap.put(objID, itemSearchStatusValue);
                itemsearchTable.addRow(rowData);
                if (rsOneType != null && rsOneType.equalsIgnoreCase("Purchased (Inventory Item)"))
                {
                    objtype = "Purchased Part";
                }
                m_itemSearchItemTypeMap.put(objID, objtype);
                objTYPE[i] = objtype;
                itemInfo.put(Integer.valueOf(i), objPUID);
                
            }
            if (itemsearchTable.getRowCount() < 1)
            {
                searchStatusLbl.setForeground(new org.eclipse.swt.graphics.Color(this.getShell().getDisplay(), 255, 0,
                        0));
                searchStatusLbl.setText(reg.getString("srchStatus"));
            }
            else
            {
                String status = "Found " + searchResult.getResponse().nRows + " NOV4Parts and Non-Engineering Parts.";
                searchStatusLbl.setForeground(new org.eclipse.swt.graphics.Color(this.getShell().getDisplay(), 0, 0,
                        205));
                searchStatusLbl.setText(status);
            }
            
        }
        else
        {
            itemIdText.setFocus();
        }
        
        if (searchResult != null && searchResult.getResponse().nRows == 0)
        {
            searchStatusLbl.setForeground(new org.eclipse.swt.graphics.Color(this.getShell().getDisplay(), 255, 0, 0));
            searchStatusLbl.setText(reg.getString("srchStatus"));
        }
        pb.setVisible(false);
        massBOMChDlg.progressBar.setSelection(90);
        massBOMChDlg.progressBar.setVisible(false);
        
    }
    
    public QuickSearchService.QuickBindVariable[] getBindVariables()
    {
        
        if (sItemId == null || sItemId.isEmpty())
            sItemId = "*";
        
        if (sAltID == null || sAltID.isEmpty())
            sAltID = "*";
        
        // Construct Bind Variables
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[12];
        
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[] { sItemId };
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[] { "*" };
        
        // To pass the type which you want in the search results
        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 2;
        bindVars[2].strList = new String[] { "Nov4Part", "Non-Engineering" };
        
        bindVars[3] = new QuickSearchService.QuickBindVariable();
        bindVars[3].nVarType = POM_date;
        bindVars[3].nVarSize = 2;
        bindVars[3].strList = new String[] { "*", "*" };
        
        bindVars[4] = new QuickSearchService.QuickBindVariable();
        bindVars[4].nVarType = POM_string;
        bindVars[4].nVarSize = 1;
        bindVars[4].strList = new String[] { "*" };
        
        bindVars[5] = new QuickSearchService.QuickBindVariable();
        bindVars[5].nVarType = POM_string;
        bindVars[5].nVarSize = 1;
        bindVars[5].strList = new String[] { "*" };
        
        bindVars[6] = new QuickSearchService.QuickBindVariable();
        bindVars[6].nVarType = POM_string;
        bindVars[6].nVarSize = 1;
        bindVars[6].strList = new String[] { "*" };
        
        bindVars[7] = new QuickSearchService.QuickBindVariable();
        bindVars[7].nVarType = POM_typed_reference;
        bindVars[7].nVarSize = 1;
        bindVars[7].strList = new String[] { "" };
        
        //4672 Passing both JDE and RSOne as bind var
        bindVars[8] = new QuickSearchService.QuickBindVariable();
        bindVars[8].nVarType = POM_string;
        bindVars[8].nVarSize = 2;
        bindVars[8].strList = new String[] { "RSOne","JDE" };
        
        bindVars[9] = new QuickSearchService.QuickBindVariable();
        bindVars[9].nVarType = POM_string;
        bindVars[9].nVarSize = 1;
        bindVars[9].strList = new String[] { "*" };
        
        bindVars[10] = new QuickSearchService.QuickBindVariable();
        bindVars[10].nVarType = POM_string;
        bindVars[10].nVarSize = 1;
        bindVars[10].strList = new String[] { sAltID };
        
        bindVars[11] = new QuickSearchService.QuickBindVariable();
        bindVars[11].nVarType = POM_string;
        bindVars[11].nVarSize = 1;
        bindVars[11].strList = new String[] { "*" };
        return bindVars;
        
    }
    
    public QuickSearchService.QuickHandlerInfo[] getHandlerInfo()
    {
        // QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new
        // QuickSearchService.QuickHandlerInfo[2];
        //
        // allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        // allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
        // allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8,
        // 10, 11 };
        // allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 5 }; //
        // object_name
        // // , item_id,Description,PUid
        // allHandlerInfo[0].listInsertAtIndex = new int[] { 2, 1, 3 ,4 }; //
        // Item Id, Item_name, description, type
        //
        // allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        // allHandlerInfo[1].handlerName = "NOVSRCH-get-alternate-ids";
        // allHandlerInfo[1].listBindIndex = new int[] {9};
        // allHandlerInfo[1].listReqdColumnIndex = new int[] {1};
        // allHandlerInfo[1].listInsertAtIndex = new int[] {5};
        
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = null;
        allHandlerInfo = new QuickSearchService.QuickHandlerInfo[5];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 10, 11,12 };
        // allHandlerInfo[0].listReqdColumnIndex = new int[0];
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 5, 7 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 2, 1, 3, 4, 5 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-nov4part-latest-revs";
        allHandlerInfo[1].listBindIndex = new int[0];
        // allHandlerInfo[1].listBindIndex[0] = 0;
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1, 2, 7 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 6, 10, 11 };
        
        allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-release-status";
        allHandlerInfo[2].listBindIndex = new int[0];
        allHandlerInfo[2].listReqdColumnIndex = new int[] { 2 };
        allHandlerInfo[2].listInsertAtIndex = new int[] { 7 };
        
        allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-alternate-ids";
        allHandlerInfo[3].listBindIndex = new int[] { 9 };
        allHandlerInfo[3].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[3].listInsertAtIndex = new int[] { 8 };
        
        allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-nov4part-master-props";
        allHandlerInfo[4].listBindIndex = new int[0];
        allHandlerInfo[4].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[4].listInsertAtIndex = new int[] { 9 };
        
        return allHandlerInfo;
    }
    
    public String getBusinessObject()
    {
        return sItemId;
    }
    
    class CustomItemIdCellRenderer extends JLabel implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            Object itemObj = table.getValueAt(row, 0);// Fetching ItemID Object
            String itemTypeValue = m_itemSearchItemTypeMap.get(itemObj.toString());
            this.setIcon(TCTypeRenderer.getTypeIcon(itemTypeValue, reg.getString("itemName")));
            this.setText(value.toString());
            if (isSelected)
            {
                setOpaque(true);
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            }
            else
            {
                this.setBackground(null);
                this.setForeground(null);
                
            }
            
            return this;
        }
    }
    
    class CustomTableCellRenderer extends JLabel implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            
            ImageIcon status_icon = null;
            Object itemObj = table.getValueAt(row, 0);
            String revStatusValue = m_itemSearchRevStatusMap.get(itemObj.toString());
            if(revStatusValue != null)
            {
                if (revStatusValue.equalsIgnoreCase(reg.getString("itemRevReleaseStatus")))
                {
                    status_icon = reg.getImageIcon("release_status.ICON");
                }
                else if (revStatusValue.equalsIgnoreCase(reg.getString("itemRevInprocessStatus")))
                {
                    status_icon = reg.getImageIcon("inprocess.ICON");
                }
                this.setIcon(status_icon);
            }
            this.setText(value.toString());
            this.setHorizontalTextPosition(SwingConstants.LEADING);
            if (isSelected)
            {
                setOpaque(true);
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            }
            else
            {
                this.setBackground(null);
                this.setForeground(null);
                
            }
            
            return this;
        }
    }
    
}
