/* ============================================================

  File description:

  Filename: MassBOMChangeDialog.java
  Module  : com/noi/rac/en/util/common

  Description :
  This class will be invoked when we select "Mass BOM Change" option under NOV Tools menu.
  This class constructs the Mass BOM Change dialog where a user can do the following :
  1) Search for a component item using the search button beside to the Component item text field.
  2) To display the Where used report table and Item from other groups table
  3) Select an action "Replace" or "Delete"
  4) To proceed with the selected action (Replace/Delete)

  Date         Developers           Description
  25-11-2011   Rajyalaxmi           Initial Creation

  $HISTORY
  ============================================================ */
package com.noi.rac.en.util.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.noi.rac.en.util.massbomchange.helper.NOVDataManagementServiceHelper;
import com.noi.rac.en.util.massbomchange.helper.NOVMBCFormComponents;
import com.noi.rac.en.util.massbomchange.helper.NOVMBCPolicy;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.rac.utilities.common.NOVDataManagementServiceInfo;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.deleteHelper.CreateRelationObjectHelper;
import com.nov.rac.utilities.services.statusHelper.ApplyStatusSOAHelper;
import com.nov.rac.utilities.utils.AlternateIDHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.MassBOMEditInputInfo;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.MassBOMEditOutput;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.MassBOMEditResponse;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCComponentRevisionRuleType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;
import com.teamcenter.rac.workflow.commands.newprocess.NewProcessCommand;
import com.teamcenter.soa.client.model.ErrorStack;

public class MassBOMChangeDialog extends AbstractSWTDialog implements SelectionListener
{
    
    Composite rdBtnContainer;
    Button itemChSrBtn;
    Button repItemSrBtn;
    Button replItemStrcBtn;
    Button delItemStrcBtn;
    Button initChangeBtn;
    Button cancelBtn;
    // 2134 - usha
    Button resetBtn;
    public Text txtItemChange;
    Text txtRepItem;
    String selItemId;
    JTable whrUsedTable;
    TCTable otherGroupItemsTable;
    TCComponentItemRevision itemToChange;
    TCComponentItemRevision replacementItem;
    TCComponentItem selCompItem;
    TCSession session;
    TCComponent[] whereUsedItems;
    List<Integer> nonEditableRowList = new ArrayList<Integer>();
    Map<String, TCComponentItemRevision> whrUsedItemsMap = new HashMap<String, TCComponentItemRevision>();
    Set<String> whrUsdItemSelected = new HashSet<String>();
    Registry registry = Registry.getRegistry(this);
    CheckBoxHeader rendererComponent;
    CheckBoxHeader chkBoxHeader;
    CustomItemListener itemlistener;
    ProgressBar progressBar;
    MassBOMChangeDialog mbcDlg;
    String[][] propertyValues;
    
    Label lblItemChange;
    short actionCmd = 0;
    private TCComponentItemRevision[] m_whereUsedItems = null;
    AIFTableModel m_whrUsedtblmodel;
    private TableRowSorter<AIFTableModel> m_whrUsedTableSorter;
    private Map<String, String> m_whrUsedtblItemRevStatusMap = new HashMap<String, String>();
    private Map<String, String> m_otherGroupTableItemrevStatusMap = new HashMap<String, String>();
    
    public MassBOMChangeDialog(Shell parentShell)
    {
        super(AIFUtility.getActiveDesktop().getShell());
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        session = (TCSession) AIFUtility.getDefaultSession();
        createDialogArea(parentShell);
        mbcDlg = this;
        
    }
    
    public MassBOMChangeDialog(Shell parentShell, TCComponentItem compItem)
    {
        super(AIFUtility.getActiveDesktop().getShell());
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        selCompItem = compItem;
        session = (TCSession) AIFUtility.getDefaultSession();
        try
        {
            if (compItem instanceof TCComponentItem)
            {
                itemToChange = compItem.getLatestItemRevision();
                selItemId = compItem.getProperty(registry.getString("itemIDProp"));
            }
            
        }
        catch (TCException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        mbcDlg = this;
    }
    
    protected Control createDialogArea(final Composite parent)
    {
        parent.setLayout(new GridLayout(1, false));
        GridData gd_parent = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_parent.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(parent, 510);
        gd_parent.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(parent, 370);
        parent.setLayoutData(gd_parent);
        
        Image searchIcon = registry.getImage("search.ICON");
        
        Composite itemSrComp = new Composite(parent, SWT.NONE);
        GridData gd_itemSrComp = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        // gd_itemSrComp.heightHint = 50;
        gd_itemSrComp.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(itemSrComp, 500);
        itemSrComp.setLayoutData(gd_itemSrComp);
        GridLayout gl_itemSrComp = new GridLayout(3, false);
        gl_itemSrComp.marginLeft = SWTUIHelper.convertHorizontalDLUsToPixels(itemSrComp, 150);
        itemSrComp.setLayout(gl_itemSrComp);
        
        lblItemChange = new Label(itemSrComp, SWT.NONE);
        GridData gd_lblItemChange = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        // gd_lblItemChange.widthHint = 86;
        lblItemChange.setLayoutData(gd_lblItemChange);
        lblItemChange.setText(registry.getString("lblItemChangeTxt"));
        
        txtItemChange = new Text(itemSrComp, SWT.BORDER);
        GridData gd_txt = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_txt.widthHint = 200;
        txtItemChange.setLayoutData(gd_txt);
        
        txtItemChange.addListener(SWT.DefaultSelection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                ItemSearchDialog ISD = null;
                boolean openISD = false;
                try
                {
                    ISD = new ItemSearchDialog(mbcDlg, true, false);
                }
                catch (TCException ex1)
                {
                    ex1.printStackTrace();
                }
                catch (IOException ex2)
                {
                    ex2.printStackTrace();
                }
                INOVSearchResult searchResult = null;
                if (txtItemChange.getEditable())
                    searchResult = ISD.performSearch(txtItemChange.getText());
                else
                {
                    if (openISD == false)
                        openISD = true;
                }
                int noOfObj = 0;
                if (searchResult != null)
                {
                    noOfObj = searchResult.getResultSet().getRows();
                }
                if (noOfObj == 1)
                {
                    TCComponent enteredComp = null;
                    try
                    {
                        String objPUID = searchResult.getResultSet().getRowData().elementAt(0);
                        enteredComp = session.stringToComponent(objPUID);
                        itemToChange = ((TCComponentItem) enteredComp).getLatestItemRevision();
                        mbcDlg.txtItemChange.setText(enteredComp.getProperty(registry.getString("itemIDProp"))
                                .toString());
                        
                    }
                    catch (TCException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (mbcDlg.whrUsedTable != null && mbcDlg.whrUsedTable.getRowCount() > 0)
                        m_whrUsedtblmodel.removeAllRows();// mbcDlg.whrUsedTable.removeAllRows();
                    if (mbcDlg.whrUsdItemSelected != null && mbcDlg.whrUsdItemSelected.size() > 0)
                        mbcDlg.whrUsdItemSelected.clear();
                    if (mbcDlg.otherGroupItemsTable != null && mbcDlg.otherGroupItemsTable.getRowCount() > 0)
                        mbcDlg.otherGroupItemsTable.removeAllRows();
                    populateWhereUsedTable(itemToChange, true);
                    mbcDlg.txtItemChange.setEditable(false);
                }
                else
                {
                    if (openISD == false)
                        openISD = true;
                }
                if (openISD)
                {
                    if (mbcDlg.whrUsedTable != null && mbcDlg.whrUsedTable.getRowCount() > 0)
                        m_whrUsedtblmodel.removeAllRows();// mbcDlg.whrUsedTable.removeAllRows();
                    if (mbcDlg.whrUsdItemSelected != null)
                        mbcDlg.whrUsdItemSelected.clear();
                    if (mbcDlg.otherGroupItemsTable != null && mbcDlg.otherGroupItemsTable.getRowCount() > 0)
                        mbcDlg.otherGroupItemsTable.removeAllRows();
                    ISD.open();
                }
            }
        });
        
        if (selCompItem != null)
        {
            txtItemChange.setText(selItemId);
            // mbcDlg.txtItemChange.setEditable(false);
        }
        
        itemChSrBtn = new Button(itemSrComp, SWT.PUSH);
        GridData gd_itemChSrBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_itemChSrBtn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(itemSrComp, 20);
        gd_itemChSrBtn.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(itemSrComp, 12);
        itemChSrBtn.setLayoutData(gd_itemChSrBtn);
        itemChSrBtn.setImage(searchIcon);
        itemChSrBtn.setToolTipText(registry.getString("itemSrchIcon.TOOLTIP"));
        itemChSrBtn.addSelectionListener(this);
        
        session = (TCSession) AIFUtility.getDefaultSession();
        
        Composite whrUsedComp = new Composite(parent, SWT.NONE);
        
        GridLayout whrUsedCompGL = new GridLayout();
        whrUsedCompGL.marginLeft = 5;
        GridData gd_whrUsedComp = new GridData(SWT.LEFT, SWT.LEFT, false, false, 1, 1);
        whrUsedComp.setLayoutData(gd_whrUsedComp);
        whrUsedComp.setLayout(whrUsedCompGL);
        
        Label lblWhrUsed = new Label(parent, SWT.NONE);
        lblWhrUsed.setText(registry.getString("lblWhrUsedTxt"));
        // SACHIN
        createWhereUsedTable();
        createWhereUsedTableSorter();
        assignColumnRenderer();
        setColumnWidth();
        
        // whrUsedTable.addMouseListener(new MouseAdapter()
        // {
        // public void mouseClicked(MouseEvent msEvt)
        // {
        // JTable table = (JTable)msEvt.getSource();
        // if(table.getSelectedColumn()==0)
        // {
        // Object cellVal = table.getValueAt(table.getSelectedRow(),
        // table.getSelectedColumn());
        // if (cellVal instanceof Boolean)
        // {
        // String itemID = (String)table.getValueAt(table.getSelectedRow(),
        // table.getSelectedColumn()+1);
        //
        // if(((Boolean)cellVal).booleanValue())
        // {
        // System.out.println(" Selected Item : " + itemID);
        // if (itemID!=null)
        // {
        // System.out.println(" Adding Item : " + itemID);
        // whrUsdItemSelected.add(itemID);
        // }
        // }
        // else
        // {
        // System.out.println(" DeSelected Item : " + itemID);
        // if (whrUsdItemSelected.contains(itemID))
        // {
        // System.out.println(" Removing Item : " + itemID);
        // whrUsdItemSelected.remove(itemID);
        // }
        // }
        // }
        // }
        // }
        // });
        //
        
        JScrollPane whrUserPane = new JScrollPane(whrUsedTable);
        GridData grid_data1 = new GridData(GridData.FILL, GridData.FILL, true, true, 1, 50);
        GridLayout gridLayout = new GridLayout();
        
        Composite whrUsedContainer = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
        
        whrUsedContainer.setLayout(gridLayout);
        whrUsedContainer.setLayoutData(grid_data1);
        SWTUIUtilities.embed(whrUsedContainer, whrUserPane, false);
        
        Label lblOthrGrpItems = new Label(parent, SWT.NONE);
        lblOthrGrpItems.setText(registry.getString("lblOthrGrpItemsTxt"));
        String[] attrNames1 = registry.getStringArray("othrGrpItemsTblAttr", ",");// {" ","Item ID","Rev","Name","Description","Group","ItemStatus"};
        AIFTableModel othrGrpItemstblmodel = new AIFTableModel(attrNames1);
        otherGroupItemsTable = new TCTable();
        otherGroupItemsTable.setSession(session);
        otherGroupItemsTable.setModel(othrGrpItemstblmodel);
        otherGroupItemsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        
        otherGroupItemsTable.getColumnModel().getColumn(1).setCellRenderer(new CustomOtherGroupTableCellRenderer());
        
        otherGroupItemsTable.getTableHeader().setReorderingAllowed(true);
        JScrollPane othrGrpItemsPane = new JScrollPane(otherGroupItemsTable);
        
        Composite othrGrpItemsContainer = new Composite(parent, SWT.EMBEDDED);
        GridData gd_othrGrpItemsContainer = new GridData(GridData.FILL, GridData.FILL, true, true, 1, 25);
        othrGrpItemsContainer.setLayout(gridLayout);
        othrGrpItemsContainer.setLayoutData(gd_othrGrpItemsContainer);
        
        SWTUIUtilities.embed(othrGrpItemsContainer, othrGrpItemsPane, false);
        
        Composite rdBtnContainer = new Composite(parent, SWT.NONE);
        GridData gd_rdBtnContainer = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        gd_rdBtnContainer.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(rdBtnContainer, 380);
        rdBtnContainer.setLayoutData(gd_rdBtnContainer);
        GridLayout gl_rdBtnContainer = new GridLayout(3, false);
        gl_rdBtnContainer.marginLeft = SWTUIHelper.convertHorizontalDLUsToPixels(rdBtnContainer, 50);
        rdBtnContainer.setLayout(gl_rdBtnContainer);
        
        replItemStrcBtn = new Button(rdBtnContainer, SWT.RADIO);
        replItemStrcBtn.setText(registry.getString("replItemStrcBtnTxt"));
        replItemStrcBtn.addSelectionListener(this);
        
        txtRepItem = new Text(rdBtnContainer, SWT.BORDER);
        txtRepItem.setEnabled(false);
        txtRepItem.setLayoutData(gd_txt);
        
        txtRepItem.addListener(SWT.DefaultSelection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                ItemSearchDialog ISD = null;
                try
                {
                    ISD = new ItemSearchDialog(mbcDlg, false);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                ISD.open();
            }
        });
        
        repItemSrBtn = new Button(rdBtnContainer, SWT.PUSH);
        GridData gd_repItemSrBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_repItemSrBtn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(rdBtnContainer, 20);
        gd_repItemSrBtn.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(rdBtnContainer, 12);
        repItemSrBtn.setLayoutData(gd_repItemSrBtn);
        repItemSrBtn.setImage(searchIcon);
        repItemSrBtn.setEnabled(false);
        repItemSrBtn.addSelectionListener(this);
        
        delItemStrcBtn = new Button(rdBtnContainer, SWT.RADIO);
        delItemStrcBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
        delItemStrcBtn.setText(registry.getString("delItemStrcBtnTxt"));
        delItemStrcBtn.addSelectionListener(this);
        new Label(rdBtnContainer, SWT.NONE);
        
        Label horizontalSeperator = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
        GridData gd_horizontalSeperator = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
        horizontalSeperator.setLayoutData(gd_horizontalSeperator);
        
        progressBar = new ProgressBar(parent, SWT.INDETERMINATE | SWT.APPLICATION_MODAL);
        GridData gd_progressBar = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
        gd_progressBar.heightHint = 20;
        progressBar.setLayoutData(gd_progressBar);
        progressBar.setVisible(false);
        
        Composite compositeBttn = new Composite(parent, SWT.NONE);
        GridData gd_compositeBttn = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
        gd_compositeBttn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 200);
        compositeBttn.setLayoutData(gd_compositeBttn);
        GridLayout gl_compositeBttn = new GridLayout(3, false);
        gl_compositeBttn.marginLeft = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 30);
        gl_compositeBttn.marginBottom = 0;
        compositeBttn.setLayout(gl_compositeBttn);
        
        initChangeBtn = new Button(compositeBttn, SWT.NONE);
        initChangeBtn.setText(registry.getString("initChangeBtnTxt"));
        GridData gd_initChangeBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_initChangeBtn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 80);
        initChangeBtn.setLayoutData(gd_initChangeBtn);
        initChangeBtn.addSelectionListener(this);
        
        cancelBtn = new Button(compositeBttn, SWT.NONE);
        cancelBtn.setText(registry.getString("cancelBtnTxt"));
        GridData gd_cancelBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_cancelBtn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 35);
        cancelBtn.setLayoutData(gd_cancelBtn);
        cancelBtn.addSelectionListener(this);
        
        resetBtn = new Button(compositeBttn, SWT.NONE);
        // resetBtn.setText(registry.getString("resetBtnTxt"));
        resetBtn.setText("Reset");
        GridData gd_resetBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_resetBtn.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(compositeBttn, 35);
        resetBtn.setLayoutData(gd_resetBtn);
        resetBtn.addSelectionListener(this);
        
        if (selCompItem != null)
        {
            try
            {
                populateWhereUsedTable(selCompItem.getLatestItemRevision(), true);
                if (whrUsedTable != null && whrUsedTable.getRowCount() > 0)
                    mbcDlg.txtItemChange.setEditable(false);
            }
            catch (TCException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return parent;
        
    }
    
    private void createWhereUsedTable()
    {
        
        String[] attrNames = registry.getStringArray("whrUsedTableAttr", ",");
        m_whrUsedtblmodel = new AIFTableModel(attrNames);
        whrUsedTable = new JTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int column)
            {
                if (column == 0)// && (!nonEditableRowList.contains(row)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
            public Class getColumnClass(int column)
            {
                return getValueAt(0, column).getClass();
            }
            
            // This is to remove the sorting on columns and also to remove
            // defaults TC renderer behavior on column header of table.
            /*
             * @Override protected void addMouseListenerToHeaderInTable() { }
             */
        };
        // whrUsedTable.setSession(session);
        
        whrUsedTable.setModel(m_whrUsedtblmodel);
        whrUsedTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        //whrUsedTable.addMouseListener(new CustomMouseListener()); // TCDECREL-3828: commented
        whrUsedTable.getTableHeader().setReorderingAllowed(false);
        whrUsedTable.doLayout();
    }
    
    private void createWhereUsedTableSorter()
    {
        m_whrUsedTableSorter = new TableRowSorter<AIFTableModel>(m_whrUsedtblmodel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                if (column == 0) // added this logic to avoid sorting for the
                    // first column of table.
                {
                    return;
                }
                
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                whrUsedTable.setRowSorter(this);
                this.sort();
                
                System.out.println("Running custom Sorting: Column " + column + order);
                
            }
        };
        
        /*
         * List<SortKey> keys = new ArrayList<SortKey>(); SortKey sortKey = new
         * SortKey(m_whrUsedtblmodel.findColumn("Item ID"),
         * SortOrder.ASCENDING); keys.add(sortKey);
         * m_whrUsedTableSorter.setSortKeys(keys);
         */
        whrUsedTable.setRowSorter(m_whrUsedTableSorter);
        m_whrUsedTableSorter.setSortable(0, false);
    }
    
    private void assignColumnRenderer()
    {
        TableColumn tabCol = whrUsedTable.getColumnModel().getColumn(0);
        tabCol.setResizable(false);
        tabCol.setPreferredWidth(10);
        tabCol.setCellEditor(whrUsedTable.getDefaultEditor(Boolean.class));
        //tabCol.setCellRenderer(whrUsedTable.getDefaultRenderer(Boolean.class));
        tabCol.setCellRenderer(new CustomCheckBoxRenderer()); // TCDECREL-3828: added class CustomCheckBoxRenderer
        itemlistener = new CustomItemListener();
        chkBoxHeader = new CheckBoxHeader(itemlistener);
        rendererComponent = chkBoxHeader.rendererComponent;
        tabCol.setHeaderRenderer(chkBoxHeader);
        
        whrUsedTable.getColumnModel().getColumn(3).setCellRenderer(new CustomTableCellRenderer());
    }
    
    private class CustomCheckBoxRenderer extends DefaultTableCellRenderer 
    {
        private static final long serialVersionUID = 1L;
        JCheckBox m_checkBox = new JCheckBox();
        
        public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected,
                boolean isFocused, int row, int col)
        {
            if (value instanceof Boolean)
            {
                Boolean marked = (Boolean) value;
                String itemID = (String) jtable.getValueAt(row, col + 1);
                
                m_checkBox.setBackground(Color.white);
                m_checkBox.setLayout(new FlowLayout(FlowLayout.CENTER));
                if (marked.booleanValue())
                {
                    m_checkBox.setSelected(true);
                    if (itemID != null)
                    {
                        whrUsdItemSelected.add(itemID);
                    }
                }
                else
                {
                    m_checkBox.setSelected(false);
                    if (whrUsdItemSelected.contains(itemID))
                    {
                        whrUsdItemSelected.remove(itemID);
                    }
                }
                
                m_checkBox.setHorizontalAlignment(SwingConstants.CENTER);
                
                rendererComponent.removeItemListener(itemlistener);
                if (rendererComponent instanceof JCheckBox)
                {
                    int checkedCount = 0;
                    
                    boolean[] flags = new boolean[jtable.getRowCount()];
                    for (int i = 0; i < jtable.getRowCount(); i++)
                    {
                        flags[i] = ((Boolean) jtable.getValueAt(i, 0)).booleanValue();
                        if (flags[i])
                        {
                            checkedCount++;
                        }
                    }
                    if (checkedCount == jtable.getRowCount())
                    {
                        ((JCheckBox) rendererComponent).setSelected(true);
                    }
                    if (checkedCount != jtable.getRowCount())
                    {
                        ((JCheckBox) rendererComponent).setSelected(false);
                    }
                }
                
                rendererComponent.addItemListener(itemlistener);
                jtable.getTableHeader().repaint();
            }
            return m_checkBox;
        }
    }
    
    private void setColumnWidth()
    {
        whrUsedTable.getColumn(m_whrUsedtblmodel.getColumnIdentifier(0)).setWidth(100);
        // whrUsedTable.getColumn(whrUsedTable.getColumnIdentifier(0)).setWidth(100);
    }
    
    /**
     * Create contents of the button bar.
     * 
     * @param parent
     */
    // @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
    }
    
    protected void configureShell(Shell newShell)
    {
        super.configureShell(newShell);
        newShell.setText(registry.getString("MassBOMDialogHeader"));
    }
    
    // @Override
    public void widgetSelected(SelectionEvent selEvent)
    {
        if (selEvent.getSource() == itemChSrBtn)
        {
            ItemSearchDialog ISD = null;
            boolean openISD = false;
            try
            {
                ISD = new ItemSearchDialog(this, true, false);
                
            }
            catch (TCException ex1)
            {
                ex1.printStackTrace();
            }
            catch (IOException ex2)
            {
                ex2.printStackTrace();
            }
            INOVSearchResult searchResult = null;
            if (txtItemChange.getEditable())
            {
                searchResult = ISD.performSearch(txtItemChange.getText());
            }
            else
            {
                openISD = true;
            }
            
            int noOfObj = 0;
            if (searchResult != null)
            {
                noOfObj = searchResult.getResultSet().getRows();
            }
            if (noOfObj == 1)
            {
                TCComponent enteredComp = null;
                try
                {
                    String objPUID = searchResult.getResultSet().getRowData().elementAt(0);
                    enteredComp = session.stringToComponent(objPUID);
                    itemToChange = ((TCComponentItem) enteredComp).getLatestItemRevision();
                    mbcDlg.txtItemChange.setText(enteredComp.getProperty(registry.getString("itemIDProp")).toString());
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (this.whrUsedTable != null && this.whrUsedTable.getRowCount() > 0)
                {
                    m_whrUsedtblmodel.removeAllRows();// this.whrUsedTable.removeAllRows();
                }
                if (whrUsdItemSelected != null && whrUsdItemSelected.size() > 0)
                {
                    whrUsdItemSelected.clear();
                }
                if (this.otherGroupItemsTable != null && this.otherGroupItemsTable.getRowCount() > 0)
                {
                    this.otherGroupItemsTable.removeAllRows();
                }
                populateWhereUsedTable(itemToChange, true);
                mbcDlg.txtItemChange.setEditable(false);
            }
            else
            {
                openISD = true;
            }
            if (openISD)
            {
                /*
                 * if (whrUsdItemSelected != null && whrUsdItemSelected.size() >
                 * 0) whrUsdItemSelected.clear(); if (this.whrUsedTable != null
                 * && this.whrUsedTable.getRowCount() > 0)
                 * this.whrUsedTable.removeAllRows(); if
                 * (this.otherGroupItemsTable != null &&
                 * this.otherGroupItemsTable.getRowCount() > 0)
                 * this.otherGroupItemsTable.removeAllRows();
                 */
                ISD.open();
            }
        }
        else if (selEvent.getSource() == repItemSrBtn)
        {
            ItemSearchDialog ISD = null;
            try
            {
                ISD = new ItemSearchDialog(this, false);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            ISD.open();
        }
        else if (selEvent.getSource() == replItemStrcBtn)
        {
            actionCmd = 1;
            txtRepItem.setEnabled(true);
            txtRepItem.setEditable(true);
            repItemSrBtn.setEnabled(true);
        }
        else if (selEvent.getSource() == delItemStrcBtn)
        {
            actionCmd = 2;
            repItemSrBtn.setEnabled(false);
            txtRepItem.setText("");
            txtRepItem.setEnabled(false);
            repItemSrBtn.setEnabled(false);
            replacementItem = null;
        }
        else if (selEvent.getSource() == initChangeBtn)
        {
            
            boolean isValid = validateInput();
            boolean isWhereUsedItemCheckedout = validateWhereUsedItemsForCheckedOut();
            if (isValid && !isWhereUsedItemCheckedout && (session != null))
            {
                String resStr = null;
                
                if (actionCmd == 1)
                {
                    
                    try
                    {
                        resStr = registry.getString("MBC1.MSG") + " "
                        + itemToChange.getTCProperty("item_id").getStringValue() + " "
                        + registry.getString("repPart.MSG") + " " + registry.getString("MBC1.MSG") + " "
                        + replacementItem.getTCProperty("item_id").getStringValue() + " "
                        + registry.getString("MBC2.MSG") + "\n" + registry.getString("MBCwarn.MSG") + "\n\n"
                        + " " + registry.getString("MBC3.MSG");
                    }
                    catch (TCException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else
                {
                    try
                    {
                        resStr = registry.getString("MBC1.MSG") + " "
                        + itemToChange.getTCProperty("item_id").getStringValue() + " "
                        + registry.getString("remPart.MSG") + " " + registry.getString("MBC2.MSG") + "\n"
                        + registry.getString("MBCwarn.MSG") + "\n\n" + " " + registry.getString("MBC3.MSG");
                    }
                    catch (TCException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                
                NOVConfirmDialog confirmDlg = new NOVConfirmDialog(this.getShell(),
                        registry.getString("confirmDlg.TITLE"), resStr);
                confirmDlg.open();
                if (confirmDlg.isOkayClicked())
                {
                	if(m_whereUsedItems.length >= 100)
                	{
                	    boolean bomStatus =NOVMBCMessageDialog.openWarningAndQuestion(this.getShell(), "Warning",
                	                    registry.getString("largeBOMWarningMsg1"), registry.getString("largeBOMWarningMsg2"),registry.getString("largeBOMWarningMsg3") ); //TCDECREL-4912
                		if(bomStatus)
                		{
                			processCommand();
                		}
                	}
                	else
                	{
                		processCommand();
                	}
                }
            }
            
        }
        else if (selEvent.getSource() == cancelBtn)
        {
            this.getShell().dispose();
        }
        else if (selEvent.getSource() == resetBtn)
        {
            this.itemToChange = null;
            this.replacementItem = null;
            
            // this.txtRepItem.setEditable(true);
            this.txtItemChange.setEditable(true);
            
            this.txtRepItem.setText("");
            this.txtItemChange.setText("");
            this.txtRepItem.setEnabled(false);
            this.repItemSrBtn.setEnabled(false);
            // this.txtRepItem.clearSelection();
            // this.txtItemChange.clearSelection();
            
            this.whrUsedTable.setRowSorter(null);
            this.m_whrUsedtblmodel.removeAllRows();
            this.whereUsedItems = null;
            this.whrUsdItemSelected.clear();
            this.whrUsedItemsMap.clear();
            this.otherGroupItemsTable.clear();
            this.chkBoxHeader.rendererComponent.setSelected(false);
            this.replItemStrcBtn.setSelection(false);
            this.delItemStrcBtn.setSelection(false);
            this.m_whrUsedtblItemRevStatusMap.clear();
            this.m_otherGroupTableItemrevStatusMap.clear();
            
        }
        
    }

	private void processCommand() {progressBar.setVisible(true);
    progressBar.setSelection(10);
    InterfaceAIFComponent[] successfulAsms = callSOA();
    
    if (successfulAsms == null || successfulAsms.length < 1)
    {
        progressBar.setVisible(false);
    }
    
    if (successfulAsms != null && successfulAsms.length > 0)
    {
        progressBar.setSelection(50);
        // Excluding the item revisions which are already in
        // workflow
        Vector<InterfaceAIFComponent> targetAsms = new Vector<InterfaceAIFComponent>();
        Vector<InterfaceAIFComponent> inProcessAsms = new Vector<InterfaceAIFComponent>(); //5169
        for (int i = 0; i < successfulAsms.length; ++i)
        {
            progressBar.setSelection(50 + i);
            TCComponentProcess itemRevProcess = null;
            String revProcess = "";
            try
            {
                revProcess = ((TCComponentItemRevision) successfulAsms[i]).getProperty("process_stage");
                
            }
            catch (Exception e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            if (revProcess.length() < 1)
            {
                targetAsms.add(successfulAsms[i]);
            }
            //5169 Collect Asms which are in workflow process.
            else
            {
                inProcessAsms.add(successfulAsms[i]);                                
            }
            // try {
            // itemRevProcess =
            // ((TCComponentItemRevision)successfulAsms[i]).getCurrentJob();
            //
            // } catch (Exception e) {
            // // TODO Auto-generated catch block
            // e.printStackTrace();
            // }
            
        }
        
        System.out.println("The output is " + successfulAsms);
        progressBar.setVisible(false);
        //5169 Display message to the user -> inProcess Asms
        if (inProcessAsms.size() > 0)
        {
            StringBuffer errorStrBuf = new StringBuffer();
            errorStrBuf.append(registry.getString("NoTargetItems.MSG")+" Affected items are : ");
            Iterator itr = inProcessAsms.iterator();
            while(itr.hasNext())
            {
                try
                {
                    errorStrBuf.append(((TCComponentItemRevision) itr.next()).getProperty("item_id"));
                    errorStrBuf.append(" ");
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }                            
                
            }
            MessageBox.post(errorStrBuf.toString(), registry.getString("NoTargetItems.Title"), MessageBox.WARNING); 
            
        }
        if (targetAsms.size() > 0)
        {
            NewProcessCommand processCmd = new NewProcessCommand(AIFUtility.getActiveDesktop(),
                    AIFUtility.getCurrentApplication(),
                    targetAsms.toArray(new InterfaceAIFComponent[targetAsms.size()]));
        }
        //5169 : Commented out the warning message which gets displayed only when all targetAsms are in workflow process. 
//        else
//        {
//            MessageBox.post(registry.getString("NoTargetItems.MSG"),
//                    registry.getString("NoTargetItems.Title"), MessageBox.WARNING);
//        }
        this.getShell().dispose();
    }}
    
    private TCComponentItemRevision[] callSOA()
    {
        NOV4ChangeManagementService cmService = NOV4ChangeManagementService.getService(session);
        TCComponentItemRevision[] successfulAsmRevs = null;
        TCComponentItemRevision[] failureAsmRevs = null; //5169
        MassBOMEditInputInfo bomEditInputInfo[] = new MassBOMEditInputInfo[1];
        bomEditInputInfo[0] = new MassBOMEditInputInfo();
        bomEditInputInfo[0].selectedItemRevs = m_whereUsedItems;// (TCComponentItemRevision[])
        // getWhereUsedItems();
        bomEditInputInfo[0].componentItem = itemToChange;
        bomEditInputInfo[0].replacementItem = this.replacementItem;
        bomEditInputInfo[0].actionCmd = this.actionCmd;
        if (bomEditInputInfo[0].selectedItemRevs.length > 0)
        {
            MassBOMEditResponse mberesponse = cmService.massBOMEdit(bomEditInputInfo);
            
            //5169 : Commented out so that failureItemRevs from the set can reported. 
            //if (mberesponse.serviceData.sizeOfPartialErrors() == 0)
            //{
                MassBOMEditOutput[] mbeoutput = mberesponse.massBOMEditOutputList;
                TCComponentForm mbcForm = mbeoutput[0].mbcForm;
                
                // 5169 : Collect both failureItemRevs and successfulItemRevs from SOA response.
                if( mbeoutput.length > 0 )
                {
                    failureAsmRevs = mbeoutput[0].failureItemRevs;
                    successfulAsmRevs = mbeoutput[0].successfulItemRevs;
                }
                
                if (mbeoutput.length > 0 && mbeoutput[0].successfulItemRevs.length > 0)
                {
                    // User service to generate MassBOM Form name
                    TCUserService userservice = session.getUserService();
                   
                    /* Object[] inputObjs = new Object[1];
                    inputObjs[0] = mbcForm;*/
                    try
                    {
                    	NOVDataManagementServiceInfo novGetNextValueService = new NOVDataManagementServiceInfo();
                    	 
                        setDatatoService(novGetNextValueService, "MBC_SEQ",  "",  "");
                        
                        // SOA call to generate MassBOM Form name
                        String strMassBOMFormName = novGetNextValueService.getNextValue();               		
                        
                        mbcForm.setStringProperty("object_name", strMassBOMFormName);
                        //userservice.call(registry.getString("generateMassBOMFormName.SERVERCALL"), inputObjs);                     
                        
                    }
                    
                   
                    
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }
                   
                    
                    if (successfulAsmRevs.length > 0)
                    {
                        Map<Integer, TCComponentItemRevision[]> mbcComponentsMap = new HashMap<Integer, TCComponentItemRevision[]>();
                        mbcComponentsMap.put(new Integer(NOVMBCFormComponents.ASSEMBLIES_KEY), successfulAsmRevs);
                        createRelationMBCFormAffectedAssms(mbcComponentsMap, mbcForm);
                        
//                        Object[] objStatusInputs = new Object[3];
//                        objStatusInputs[0] = mbcForm;
//                        objStatusInputs[1] = registry.getString("itemReleasedProp");
//                        objStatusInputs[2] = registry.getString("objectName");
//                        try
//                        {
//                            userservice.call(registry.getString("objectSetNewStatus.SERVERCALL"), objStatusInputs);
//                        }
//                        catch (TCException e)
//                        {
//                            e.printStackTrace();
//                        }
                        
                        // Replace userservice "NOV_object_setNewStatus" with SOA
                        ApplyStatusSOAHelper.replaceStatus(mbcForm, registry.getString("itemReleasedProp"));
                        
                    }
                    
                }
                // 5169 : Print the message and list all failedItemRevs to the user. 
                if (mbeoutput.length > 0 && mbeoutput[0].failureItemRevs.length > 0)
                {
                    StringBuffer errorStrBuf = new StringBuffer();
                    errorStrBuf.append("Mass BOM Change Operation failed for these items :");
                    for (int i = 0; i < mbeoutput[0].failureItemRevs.length; i++)
                    {
                        try
                        {
                            errorStrBuf.append(mbeoutput[0].failureItemRevs[i].getTCProperty("item_id").toString());
                            errorStrBuf.append(" ");
                        }
                        catch (TCException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        
                    }
                    if (mberesponse.serviceData.sizeOfPartialErrors() != 0)
                    {
                        ErrorStack errorStack = mberesponse.serviceData.getPartialError(0);
                        String[] errors = errorStack.getMessages();
                        errorStrBuf.append("\n");
                        errorStrBuf.append(errors[0]);                        
                    }
                    MessageBox.post(errorStrBuf.toString(), registry.getString("errorInMassBOMOperation.MSG"), MessageBox.ERROR);                
                }
                
              //5169 : Commented out so that failureItemRevs from the set can reported.   
//            if (mberesponse.serviceData.sizeOfPartialErrors() != 0)
//            {
//                ErrorStack errorStack = mberesponse.serviceData.getPartialError(0);
//                String[] errors = errorStack.getMessages();
//                MessageBox.post(errors[0] + " ", registry.getString("errorInMassBOMOperation.MSG"), MessageBox.ERROR);
//            }
        }
        return successfulAsmRevs;
    }
    
    /**
     * Set Input data to the service
     */   
    
    void setDatatoService(NOVDataManagementServiceInfo novGetNextValueService,String strCounterName,
 		   													String strPrefixExp, String strPostfixExp)
    {
 		int theScope = TCPreferenceService.TC_preference_site;	

 		novGetNextValueService.setContext("NAME");
 		novGetNextValueService.setCounterName(strCounterName);
 		novGetNextValueService.setDefaultCounterStartValue(1);
 		novGetNextValueService.setPrefix(strPrefixExp);
 		novGetNextValueService.setPostfix(strPostfixExp);
		novGetNextValueService.setAlternateIDContext(new String[] {""});
		
    }
    
    private boolean validateWhereUsedItemsForCheckedOut()
    {
        
        boolean isCheckedOut = false;
        m_whereUsedItems = (TCComponentItemRevision[]) getWhereUsedItems();
        for (int i = 0; i < m_whereUsedItems.length; i++)
        {
            try
            {
                String checkedOutValue = m_whereUsedItems[i].getProperty("checked_out");
                if (checkedOutValue.equalsIgnoreCase("Y"))
                {
                    isCheckedOut = true;
                    MessageBox.post(registry.getString("allAssembliesNotCheckedIn.MSG"), "Warning", MessageBox.WARNING);
                    break;
                }
                if (validateBOMRevForCheckedOut(m_whereUsedItems[i]))
                {
                    isCheckedOut = true;
                    break;
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            
        }
        
        return isCheckedOut;
        
    }
    
    public boolean validateBOMRevForCheckedOut(TCComponent revComponent)
    {
        InterfaceAIFComponent[] aifComponent = null;
        boolean isBOMRevCheckedOut = false;
        try
        {
            aifComponent = (revComponent).getTCProperty(NOVMBCFormComponents.STRUCTURE_REVISIONS)
            .getReferenceValueArray();
            for (int i = 0; i < aifComponent.length; i++)
            {
                String bomRevCheckedOutValue = aifComponent[i].getProperty(NOVMBCFormComponents.CHECKED_OUT);
                if (bomRevCheckedOutValue.equalsIgnoreCase("Y"))
                {
                    isBOMRevCheckedOut = true;
                    MessageBox.post(registry.getString("allAssembliesNotCheckedIn.MSG"), "Warning", MessageBox.WARNING);
                    break;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return isBOMRevCheckedOut;
    }
    
    private boolean validateInput()
    {
        boolean validateFlag = false;
        if (itemToChange == null)
        {
            MessageBox.post(registry.getString("selCompItem.WARN"), "Warning", MessageBox.WARNING);
        }
        else if (actionCmd == 0)
        {
            MessageBox.post(registry.getString("selAction.WARN"), "Warning", MessageBox.WARNING);
        }
        else if (actionCmd == 1 && replacementItem == null)
        {
            MessageBox.post(registry.getString("selReplItem.WARN"), "Warning", MessageBox.WARNING);
        }
        else if (actionCmd == 1 && txtItemChange.getText().trim().equals(txtRepItem.getText().trim()))
        {
            MessageBox.post(registry.getString("compAndReplItemsSame.MSG"), "Warning", MessageBox.WARNING);
        }
        else if ((actionCmd == 2 || actionCmd == 1)
                && ((whrUsdItemSelected == null) || (whrUsdItemSelected.size() < 1)))
        {
            MessageBox.post(registry.getString("selWhrUsed.WARN"), "Warning", MessageBox.WARNING);
        }
        else
        {
            validateFlag = true;
        }
        return validateFlag;
        
    }
    
    private void createRelationMBCFormAffectedAssms(Map affectedAsmMap, TCComponent mbcFormComp)
    {
        
        TCComponentItemRevision assemblyItemRevs[] = (TCComponentItemRevision[]) affectedAsmMap.get(new Integer(
                NOVMBCFormComponents.ASSEMBLIES_KEY));
        
        try
        {
            
            CreateRelationObjectHelper creRelationObjectHelper[] = new CreateRelationObjectHelper[assemblyItemRevs.length];
            for (int i = 0; i < assemblyItemRevs.length; i++)
            {
                creRelationObjectHelper[i] = new CreateRelationObjectHelper(
                        CreateRelationObjectHelper.OPERATION_CREATE_RELATION);
                creRelationObjectHelper[i].setPrimaryObject(assemblyItemRevs[i]);
                creRelationObjectHelper[i].setSecondaryObject(mbcFormComp);
                creRelationObjectHelper[i].setRelationName(NOVMBCFormComponents.RELATEDECN);
            }
            
            CreateObjectsSOAHelper.createRelationObjects(creRelationObjectHelper);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    public TCComponent[] getWhereUsedItems()
    {
        Vector<TCComponent> whrUsedItemsVec = new Vector<TCComponent>();
        TCComponentItemRevision[] comps = new TCComponentItemRevision[whrUsdItemSelected.size()];
        Object[] whrObs = whrUsdItemSelected.toArray();
        for (int i = 0; i < whrUsdItemSelected.size(); i++)
        {
            whrUsedItemsVec.add(whrUsedItemsMap.get(whrObs[i]));
            comps[i] = (TCComponentItemRevision) whrUsedItemsMap.get(whrObs[i]);
        }
        return comps;
    }
    
    public void searchForwhereUsedItems(TCComponentItemRevision itemRev)
    {
        TCComponentRevisionRuleType revRuleType;
        try
        {
            revRuleType = (TCComponentRevisionRuleType) itemRev.getSession().getTypeComponent("RevisionRule");
            
            TCComponent[] revRules = revRuleType.extent();
            TCComponentRevisionRule revRule = null;
            TCComponentRevisionRule defaultRevRule = null;
            String defRevRule = registry.getString("defaultRevRule");
            for (int i = 0; i < revRules.length; i++)
            {
                if (revRules[i].getProperty(registry.getString("itemNameProp")).equalsIgnoreCase(defRevRule))
                {
                    defaultRevRule = new TCComponentRevisionRule();
                    defaultRevRule = (TCComponentRevisionRule) revRules[i];
                    break;
                }
            }
            if (revRule == null)
            {
                revRule = defaultRevRule;
            }
            
            NOVDataManagementServiceHelper dmhelper = new NOVDataManagementServiceHelper(itemRev);
            // NOV4ChangeManagementService cmService =
            // NOV4ChangeManagementService.getService(session);
            NOVMBCPolicy mbcPolicy = dmhelper.createMBCPolicy();
            if (whereUsedItems != null)
            {
                whereUsedItems = null;
            }
            whereUsedItems = dmhelper.getWhereUsed(itemRev, mbcPolicy, revRule);
            if (whereUsedItems.length < 1)
            {
                MessageBox.post(registry.getString("compItemNotUsedInAnyAsm"), "Warning", MessageBox.WARNING);
                mbcDlg.txtItemChange.setEditable(true);
            }
            propertyValues = new String[whereUsedItems.length][9];
            for (int itemRevIndex = 0; itemRevIndex < whereUsedItems.length; itemRevIndex++)
            {
                propertyValues[itemRevIndex][0] = (String) whereUsedItems[itemRevIndex].getTCProperty(
                "item_revision_id").getPropertyValue();
                propertyValues[itemRevIndex][1] = (String) whereUsedItems[itemRevIndex].getProperty("release_statuses");
                propertyValues[itemRevIndex][2] = (String) whereUsedItems[itemRevIndex].getProperty("process_stage");
                TCComponent itemTag = whereUsedItems[itemRevIndex].getTCProperty("items_tag").getReferenceValue();
                propertyValues[itemRevIndex][3] = (String) itemTag.getTCProperty("item_id").getPropertyValue();
                String[] altIds = AlternateIDHelper.getAlternateIds(itemTag);
                String altIdCommaSaparated = AlternateIDHelper.getSingleString(altIds, ", ");
                propertyValues[itemRevIndex][4] = altIdCommaSaparated;
                propertyValues[itemRevIndex][5] = (String) itemTag.getProperty("release_statuses");
                propertyValues[itemRevIndex][6] = (String) itemTag.getTCProperty("object_name").getPropertyValue();
                propertyValues[itemRevIndex][7] = (String) itemTag.getTCProperty("object_desc").getPropertyValue();
                propertyValues[itemRevIndex][8] = (String) itemTag.getProperty("owning_group");
            }
            // WhereUsedResponse whrUsedResponse =
            // cmService.performWhereUsed(itemRev, revRule, 1, false);
            // ItemRevWithProps[] itemRevswithAttrValues =
            // whrUsedResponse.itemRevsList;
            // propertyValues = new String[itemRevswithAttrValues.length][];
            // whereUsedItems = new TCComponent[itemRevswithAttrValues.length];
            // for ( int itemRevIndex=0; itemRevIndex <
            // itemRevswithAttrValues.length; itemRevIndex++ )
            // {
            // whereUsedItems[itemRevIndex] =
            // itemRevswithAttrValues[itemRevIndex].itemRev;
            // propertyValues[itemRevIndex] =
            // itemRevswithAttrValues[itemRevIndex].attributes;
            // }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    public void populateWhereUsedTable(TCComponentItemRevision itemRev, boolean invokeSrchForWhrUsed)
    {
        try
        {
            // usha - fix for invalid pop-up messages on whereused item
            // selections
            whrUsedTable.setRowSorter(null);
            m_whrUsedtblmodel.removeAllRows();
            m_whrUsedtblItemRevStatusMap.clear();
            m_otherGroupTableItemrevStatusMap.clear();
            whrUsedTable.setRowSelectionAllowed(false);
            if (invokeSrchForWhrUsed)
            {
                // usha - fix for Select All checkbox
                ((JCheckBox) (whrUsedTable.getTableHeader().getColumnModel().getColumn(0).getHeaderRenderer()))
                .setSelected(false);
                searchForwhereUsedItems(itemRev);
            }
            otherGroupItemsTable.setName("Other");
            whrUsedTable.setName("Own");
            
            for (int i = 0; i < whereUsedItems.length; i++)
            {
                // propertyValues: REV ID, REV REL STATUS, REV PROCESS STAGE,
                // ITEM ID, ALT ID, RELEASE STATUS OF ITEM, OBJ NAME, OBJ DESC,
                // OWN GRP
                String statusValue = null;
                String owning_group = "";
                if (propertyValues[i][8] != null)
                    owning_group = propertyValues[i][8];
                Vector<Object> rowData = new Vector<Object>();
                
                if (owning_group.equals(session.getCurrentGroup().toString()))
                {
                    rowData.add(new Boolean(false));
                    TCComponentItemRevision latestRev = ((TCComponentItemRevision) whereUsedItems[i]);
                    
                    String itemId = "";
                    if (propertyValues[i][3] != null)
                        itemId = propertyValues[i][3];
                    String altId = "";
                    if (propertyValues[i][4] != null)
                        altId = propertyValues[i][4];
                    String revId = "";
                    if (propertyValues[i][0] != null)
                        revId = propertyValues[i][0];
                    String itemName = "";
                    if (propertyValues[i][6] != null)
                        itemName = propertyValues[i][6];
                    String itemDesc = "";
                    if (propertyValues[i][7] != null)
                        itemDesc = propertyValues[i][7];
                    String itemStatus = "";
                    if (propertyValues[i][5] != null)
                        itemStatus = propertyValues[i][5];
                    rowData.add(itemId);
                    rowData.add(altId);
                    rowData.add(revId);
                    rowData.add(itemName);
                    rowData.add(itemDesc);
                    rowData.add(owning_group);
                    rowData.add(itemStatus);
                    String releaseStats = "";
                    if (propertyValues[i][1] != null)
                        releaseStats = propertyValues[i][1];
                    String itemRevProcess = "";
                    if (propertyValues[i][2] != null)
                        itemRevProcess = propertyValues[i][2];
                    
                    if ((releaseStats != null) && (releaseStats.length() > 0))
                    {
                        statusValue = registry.getString("itemRevReleaseStatus");
                    }
                    else if ((itemRevProcess != null) && (itemRevProcess.length() > 0))
                    {
                        statusValue = registry.getString("itemRevInprocessStatus");
                    }
                    else
                    {
                        statusValue = registry.getString("itemRevWorkingStatus");
                    }
                    // whrUsedTable.addRow(rowData);
                    m_whrUsedtblItemRevStatusMap.put(itemId, statusValue);
                    m_whrUsedtblmodel.addRow(rowData);
                    whrUsedItemsMap.put(itemId, (TCComponentItemRevision) whereUsedItems[i]);
                }
                else
                {
                    // rowData.add(new Boolean(false));
                    String otherGroupTableItemRevStatusVal = null;
                    TCComponentItemRevision latestRev = ((TCComponentItemRevision) whereUsedItems[i]);
                    String itemId = "";
                    if (propertyValues[i][3] != null)
                        itemId = propertyValues[i][3];
                    String revId = "";
                    if (propertyValues[i][0] != null)
                        revId = propertyValues[i][0];
                    String itemName = "";
                    if (propertyValues[i][6] != null)
                        itemName = propertyValues[i][6];
                    String itemDesc = "";
                    if (propertyValues[i][7] != null)
                        itemDesc = propertyValues[i][7];
                    String itemStatus = "";
                    if (propertyValues[i][5] != null)
                        itemStatus = propertyValues[i][5];
                    rowData.add(itemId);
                    rowData.add(revId);
                    rowData.add(itemName);
                    rowData.add(itemDesc);
                    rowData.add(owning_group);
                    rowData.add(itemStatus);
                    String releaseStats = "";
                    if (propertyValues[i][1] != null)
                        releaseStats = propertyValues[i][1];
                    String itemRevProcess = "";
                    if (propertyValues[i][2] != null)
                        itemRevProcess = propertyValues[i][2];
                    if ((releaseStats != null) && (releaseStats.length() > 0))
                    {
                        otherGroupTableItemRevStatusVal = registry.getString("itemRevReleaseStatus");
                    }
                    else if ((itemRevProcess != null) && (itemRevProcess.length() > 0))
                    {
                        otherGroupTableItemRevStatusVal = registry.getString("itemRevInprocessStatus");
                    }
                    else
                    {
                        otherGroupTableItemRevStatusVal = registry.getString("itemRevWorkingStatus");
                    }
                    m_otherGroupTableItemrevStatusMap.put(itemId, otherGroupTableItemRevStatusVal);
                    otherGroupItemsTable.addRow(rowData);
                }
                
            }
            if (whereUsedItems.length >= 1 && whrUsedTable.getRowSorter() == null)
            {
                whrUsedTable.setRowSorter(m_whrUsedTableSorter);
                m_whrUsedTableSorter.setSortable(0, false);
                m_whrUsedTableSorter.sort();
                
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    // @Override
    public void widgetDefaultSelected(SelectionEvent arg0)
    {
        // TODO Auto-generated method stub
        
    }
    
    private class CustomItemListener implements ItemListener
    {
        // @Override
        public void itemStateChanged(ItemEvent e)
        {
            Object source = e.getSource();
            System.out.println("object state changed: " + source.toString());
            if (source instanceof AbstractButton == false)
                return;
            boolean checked = e.getStateChange() == ItemEvent.SELECTED;
            for (int x = 0, y = whrUsedTable.getRowCount(); x < y; x++)
            {
                whrUsedTable.setValueAt(new Boolean(checked), x, 0);
                String itemID = (String) whrUsedTable.getValueAt(x, 1);
                if (checked)
                {
                    if (itemID != null)
                    {
                        whrUsdItemSelected.add(itemID);
                    }
                }
                else
                {
                    if (whrUsdItemSelected.contains(itemID))
                    {
                        whrUsdItemSelected.remove(itemID);
                    }
                }
            }
            whrUsedTable.updateUI();
        }
    }
    
    public CheckBoxHeader getRendererComponent()
    {
        return rendererComponent;
    }
    
    public void setRendererComponent(CheckBoxHeader rendererComponent)
    {
        // rendererComponent.setText("Check All");
        this.rendererComponent = rendererComponent;
    }
    
    class CustomTableCellRenderer extends JLabel implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            ImageIcon status_icon = null;
            Object itemObj = table.getValueAt(row, 1);// Fetching ItemID Object
            String whrUsedtblItemRevStatusVal = m_whrUsedtblItemRevStatusMap.get(itemObj.toString());
            this.setText(value.toString());
            this.setHorizontalTextPosition(SwingConstants.LEADING);
            
            if (whrUsedtblItemRevStatusVal.equalsIgnoreCase(registry.getString("itemRevReleaseStatus")))
            {
                status_icon = registry.getImageIcon("release_status.ICON");
            }
            else if (whrUsedtblItemRevStatusVal.equalsIgnoreCase(registry.getString("itemRevInprocessStatus")))
            {
                status_icon = registry.getImageIcon("inprocess.ICON");
            }
            this.setIcon(status_icon);
            if (isSelected)
            {
                setOpaque(true);
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            }
            else
            {
                this.setBackground(null);
                this.setForeground(null);
                
            }
            return this;
            
        }
    }
    
    class CustomOtherGroupTableCellRenderer extends JLabel implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            ImageIcon status_icon = null;
            Object itemObj = table.getValueAt(row, 0);// Fetching ItemID Object
            String otherGroupTableItemRevStatusVal = m_otherGroupTableItemrevStatusMap.get(itemObj.toString());
            this.setText(value.toString());
            this.setHorizontalTextPosition(SwingConstants.LEADING);
            if (otherGroupTableItemRevStatusVal.equalsIgnoreCase(registry.getString("itemRevReleaseStatus")))
            {
                status_icon = registry.getImageIcon("release_status.ICON");
            }
            else if (otherGroupTableItemRevStatusVal.equalsIgnoreCase(registry.getString("itemRevInprocessStatus")))
            {
                status_icon = registry.getImageIcon("inprocess.ICON");
            }
            
            this.setIcon(status_icon);
            if (isSelected)
            {
                setOpaque(true);
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            }
            else
            {
                this.setBackground(null);
                this.setForeground(null);
                
            }
            return this;
            
        }
    }
    
}
