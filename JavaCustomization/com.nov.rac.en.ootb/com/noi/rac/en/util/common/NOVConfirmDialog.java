 /* ============================================================
   
   File description: 

   Filename: MassBOMChangeDialog.java 
   Module  : com/noi/rac/en/util/common

   Description :
   This class implements SWT message box for warning with warning icon.
   
   Date         Developers    		Description
   08-02-2011   Rajyalaxmi   	  	Initial Creation

   $HISTORY
   ============================================================ */
package com.noi.rac.en.util.common;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.StyledEditorKit.StyledTextAction;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.nov.rac.utilities.utils.SWTUIHelper;
//import com.teamcenter.rac.common.views.SWTResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;//TC10.1 Upgrade
import com.teamcenter.rac.util.ConfirmDialog;
import com.teamcenter.rac.util.Registry;

public class NOVConfirmDialog extends Dialog {

	public NOVConfirmDialog(Shell shell) {
        super(shell);
       // this.shell = shell;
        isOkClicked = false;
        int i = 2096 | Window.getDefaultOrientation();
        setShellStyle(i);
	}
	
    public NOVConfirmDialog(Shell shell, String s, String s1)
    {
        this(shell);
        //this.shell = shell;
        setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.APPLICATION_MODAL);
        title = s;
        text = s1;
        msg = new StyledText(shell, SWT.NONE); 
    }
    
    public NOVConfirmDialog(Shell shell, String s, String s1, boolean flag)
    {
        this(shell, s, s1);
       // this.shell = shell;
        msg = new StyledText(shell, SWT.NONE); 
        setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.APPLICATION_MODAL);
        if(flag)
            setShellStyle(getShellStyle() | 65536);
    }
    
    protected Control createContents(Composite composite)
    {
        super.createContents(composite);
        Composite composite1 = (Composite)getDialogArea();
        composite1.setLayout(new GridLayout(3, false));
        composite1.setEnabled(false);
		GridData gd_composite1 = new GridData(SWT.FILL , SWT.FILL, true, true, 1, 1);
		gd_composite1.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(composite1,400);
		gd_composite1.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(composite1,60);
		composite1.setLayoutData(gd_composite1);
        getShell().setImage(getImage());
        getShell().setText(title);
        Image warning_icon = reg.getImage("warning.ICON");
        
        new Label(composite1, SWT.NONE);
		Label lblNewLabel = new Label(composite1, SWT.IMAGE_PNG | SWT.NONE);
		lblNewLabel.setImage(warning_icon);
		GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
		lblNewLabel.setLayoutData(gd_lblNewLabel);
        
		Label lblWarning = new Label(composite1, SWT.NONE);
		lblWarning.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD));//TC10.1 Upgrade(import org.eclipse.wb.swt.SWTResourceManager)
		lblWarning.setForeground(SWTResourceManager.getColor(255, 0, 0));//TC10.1 Upgrade(import org.eclipse.wb.swt.SWTResourceManager)
		GridData gd_lblWarning = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lblWarning.setLayoutData(gd_lblWarning);
		lblWarning.setText(reg.getString("WARN.TXT"));
		new Label(composite1, SWT.NONE);
		new Label(composite1, SWT.NONE);
		
		StyledText waniningMsg = new StyledText(composite1, SWT.READ_ONLY);
		waniningMsg.setBackground(composite.getBackground());
		
		waniningMsg.setText(text);
		int operation = 0;
		if(text.contains(reg.getString("repPart.MSG"))) operation = 1;
		if(text.contains(reg.getString("remPart.MSG"))) operation = 2;
		//1 --> replacement operation
		//2 --> remove operation
		StyleRange style1 = new StyleRange();
		if(operation ==1 )
		{
			style1.start = 0;
			int repIndex = text.indexOf(reg.getString("repPart.MSG"), 0);
			style1.length = repIndex;
			style1.fontStyle = SWT.BOLD;
			
			StyleRange style2 = new StyleRange();
			style2.start = text.indexOf(reg.getString("repPart.MSG"))
						+ reg.getString("repPart.MSG").length();
			style2.length = text.indexOf(reg.getString("MBC2.MSG")) 
				- text.indexOf(reg.getString("repPart.MSG"))
				-1
				-reg.getString("repPart.MSG").length();
			style2.fontStyle = SWT.BOLD;
			
			StyleRange style3 = new StyleRange();
			style3.start=text.indexOf(reg.getString("MBCwarn.MSG"));
			style3.length = reg.getString("MBCwarn.MSG").length();
			style3.fontStyle = SWT.BOLD;
			
			waniningMsg.setStyleRange(style2);
			waniningMsg.setStyleRange(style3);
		}
		if(operation ==2 )
		{
			style1.start = 0;
			int remIndex = text.indexOf(reg.getString("remPart.MSG"), 0);
			style1.length = remIndex;
			style1.fontStyle = SWT.BOLD;
			
			StyleRange style3 = new StyleRange();
			style3.start=text.indexOf(reg.getString("MBCwarn.MSG"));
			style3.length = reg.getString("MBCwarn.MSG").length();
			style3.fontStyle = SWT.BOLD;
			waniningMsg.setStyleRange(style3);
			
		}
		//style1.background = composite.getBackground();
		waniningMsg.setStyleRange(style1);
        return composite1;
    }
    
    private Image getImage()
    {
        ImageDescriptor imagedescriptor = AbstractUIPlugin.imageDescriptorFromPlugin("com.teamcenter.rac.aifrcp", "icons/defaultapplication_16.png");
        if(imagedescriptor != null)
            return imagedescriptor.createImage();
        else
            return null;
    }
    
    public boolean isOkayClicked()
    {
        return isOkClicked;
    }
    
    protected void buttonPressed(int i)
    {
        close();
        isOkClicked = i == 2;
    }
    
    protected void createButtonsForButtonBar(Composite composite)
    {
        createButton(composite, 2, IDialogConstants.YES_LABEL, false);
        createButton(composite, 3, IDialogConstants.NO_LABEL, true);
    }
    
    public static int prompt(final Shell shell, final String s, final String s1)
    {
        int i = 3;
        if(Thread.currentThread() == shell.getDisplay().getThread())
        {
            ConfirmDialog confirmdialog = new ConfirmDialog(shell, s, s1);
            confirmdialog.open();
            if(confirmdialog.isOkayClicked())
                i = 2;
        } else
        {
            final ArrayList arraylist = new ArrayList(1);
           // final List responseList;
          shell.getDisplay().syncExec(new Runnable() {
        	  List responseList = null;
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				NOVConfirmDialog confirmdialog1 = new NOVConfirmDialog(shell, s, s1);
              confirmdialog1.open();
              responseList = arraylist ; 
              if(confirmdialog1.isOkayClicked())
                  responseList.add(Integer.valueOf(2));
              else
                  responseList.add(Integer.valueOf(3));
              
				
			}
		});
            i = ((Integer)arraylist.get(0)).intValue();
        }
        return i;
    }
    
    //public Shell shell;
    public  String title;
    public  String text;
    public  StyledText msg; 
    public boolean isOkClicked;
    protected Registry reg = Registry.getRegistry(this);

}
