/*
 * Created on Apr 15, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.util;
import java.util.Calendar;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ComponentWrapper {
    AIFComponentContext comp;
    TCComponentItemRevision compR;
    String[]            toStringElements=null;
    boolean             useDisplayFormat=false;
    
    public ComponentWrapper(TCComponent c) { 
    	comp = new AIFComponentContext(null,c,null); 
    	useDisplayFormat = true;
    }
    public ComponentWrapper(TCComponent c,String[] toStringElements) { 
    	comp = new AIFComponentContext(null,c,null);
    	if (c instanceof TCComponentItemRevision)
    		compR = (TCComponentItemRevision)c;
    	this.toStringElements = toStringElements;
    	useDisplayFormat = true;
    }
    public ComponentWrapper(AIFComponentContext comp) { this.comp = comp;}
    public ComponentWrapper(AIFComponentContext comp,String[] toStringElements)
    {
    	this.comp = comp;
    	this.toStringElements = toStringElements;
    }
    public ComponentWrapper(AIFComponentContext comp,TCComponentItemRevision compR,String[] toStringElements)
    {
    	this.comp = comp;
    	this.compR = compR;
    	this.toStringElements = toStringElements;
    	
    }
    public AIFComponentContext getComponent() { return comp; }
    public TCComponentItemRevision getComponentR() { return compR; }
    public String toString() {
    	String returnVal="";
    	
    	if (toStringElements == null) {
           try {
              return (comp.getComponent()).getProperty("object_name");
           }
           catch (Exception e) {
              return comp.toString();
           }
    	}
    	else {
    		for (int i=0;i<toStringElements.length;i++) {
    			try {
    				if(toStringElements[i].equals("item_id"))
    				returnVal += comp.getComponent().getProperty(toStringElements[i]);
    				if(toStringElements[i].equals("object_name")) {
    					if (useDisplayFormat)
    						returnVal += " ";
   						returnVal += comp.getComponent().getProperty(toStringElements[i]);
    				}
    				if(toStringElements[i].equals("object_desc")){
    					returnVal += comp.getComponent().getProperty(toStringElements[i]);
    				} 
    				if (toStringElements[i].equals("current_revision_id")){
    					if (useDisplayFormat)
    						returnVal += "/";
    					returnVal += compR.getProperty(toStringElements[i]);
   					}
    				if (toStringElements[i].equals("release_statuses")){
    					returnVal += compR.getProperty(toStringElements[i]);
    					}
    				if (toStringElements[i].equals("date_released")){
    					returnVal += compR.getProperty(toStringElements[i]);
    					}
    				if (toStringElements[i].equals("checked_out_user")){
    					returnVal += comp.getComponent().getProperty(toStringElements[i]);
    					}
    				if (toStringElements[i].equals("ID")){
    					returnVal += comp.getComponent().getProperty(toStringElements[i]);
    					}
    				if (toStringElements[i].equals("checked_out_date")){
    					
    					java.util.Date currentDate = ((TCComponent)comp.getComponent()).getTCProperty(toStringElements[i]).getDateValue();
    					Calendar cal= Calendar.getInstance();
    					cal.setTime(currentDate);
    					returnVal += cal.get(Calendar.YEAR)+""+cal.get(Calendar.MONTH)+""+cal.get(Calendar.DAY_OF_MONTH);
    					}
    				
    			}
    			catch (Exception e) {
    				; // nothing to append!
    			}
    		}
    	}
    	return returnVal;
    }
}
