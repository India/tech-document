 /* ============================================================
   
   File description: 

   Filename: NOVMBCPolicy.java 
   Module  : com/noi/rac/en/util/massbomchange/helper

   Date         Developers    		Description
   25-11-2011   Rajyalaxmi   	  	Initial Creation

   $HISTORY
   ============================================================ */
package com.noi.rac.en.util.massbomchange.helper;

import com.teamcenter.soa.common.ObjectPropertyPolicy;
import com.teamcenter.soa.common.PolicyProperty;
import com.teamcenter.soa.common.PolicyType;

public class NOVMBCPolicy{
	
	private ObjectPropertyPolicy m_mbcPolicy = null;
	
	public NOVMBCPolicy() {
		m_mbcPolicy = new ObjectPropertyPolicy();
	}

	public void addPropertiesPolicy(String policyType, NOVMBCPolicyProperty[] mbcPolicyProps) {
		
		PolicyType mbcPolicyType = new PolicyType(policyType);
		
		for(int iCnt = 0; iCnt < mbcPolicyProps.length ; iCnt++ ){
			PolicyProperty policyProp = new PolicyProperty(mbcPolicyProps[iCnt].get_propertyName());
			
			if(mbcPolicyProps[iCnt].get_propertyModifier() != null){
				policyProp.setModifier(mbcPolicyProps[iCnt].get_propertyModifier(), true);
			}
			
			mbcPolicyType.addProperty(policyProp);
		}		
		
		m_mbcPolicy.addType(mbcPolicyType);
		
	}

	public ObjectPropertyPolicy get_mbcPolicy() {
		return m_mbcPolicy;
	}
	
	
	
}
