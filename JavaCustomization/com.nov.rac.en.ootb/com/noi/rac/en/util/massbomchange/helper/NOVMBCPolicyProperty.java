 /* ============================================================
   
   File description: 

   Filename: NOVMBCPolicyProperty.java 
   Module  : com/noi/rac/en/util/massbomchange/helper

   Date         Developers    		Description
   25-11-2011   Rajyalaxmi   	  	Initial Creation

   $HISTORY
   ============================================================ */
package com.noi.rac.en.util.massbomchange.helper;

public class NOVMBCPolicyProperty {
	
	private String m_propertyName = null;
	
	private String m_propertyModifier = null;
	
	public NOVMBCPolicyProperty (String name,String modifier){
		
		m_propertyName = name;
		
		m_propertyModifier = modifier;
	}

	public String get_propertyName() {
		return m_propertyName;
	}

	public String get_propertyModifier() {
		return m_propertyModifier;
	}

}
