/* ============================================================
  
  File description: 

  Filename: NOVDataManagementServiceHelper.java 
  Module  : com/noi/rac/en/util/massbomchange/helper

  Date         Developers    		Description
  25-11-2011   Rajyalaxmi   	  	Initial Creation

  $HISTORY
  ============================================================ */
package com.noi.rac.en.util.massbomchange.helper;

import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core.SessionService;
import com.teamcenter.services.rac.core._2007_01.DataManagement;
import com.teamcenter.services.rac.core._2007_01.DataManagement.WhereUsedOutput;
import com.teamcenter.services.rac.core._2007_01.DataManagement.WhereUsedParentInfo;
import com.teamcenter.soa.common.PolicyProperty;

public class NOVDataManagementServiceHelper
{
    
    private TCSession m_tcSession;
    
    private String m_strCurrentSOAPolicy = null;
    
    private NOVMBCPolicy m_mbcPolicy;
    private NOVMBCPolicy m_mbcFormPolicy;
    
    public NOVDataManagementServiceHelper(TCComponent object)
    {
        m_tcSession = object.getSession();
    }
    
    public TCComponent[] getWhereUsed(TCComponent Object, NOVMBCPolicy mbcPolicy, TCComponent revRule)
    {
        
        TCComponent[] parentAsm = null;
        
        TCComponent[] inputObjs = new TCComponent[1];
        inputObjs[0] = Object;
        
        // Get Session SOA Policy
        getCurrentSOAPolicy();
        
        SessionService service = SessionService.getService(m_tcSession);
        
        service.setObjectPropertyPolicy(mbcPolicy.get_mbcPolicy());
        
        DataManagementService DMService = DataManagementService.getService(m_tcSession);
        
        DataManagement.WhereUsedResponse response = DMService.whereUsed(inputObjs, 1, false, revRule);
        
        System.out.println("The errors are : " + response.serviceData.sizeOfPartialErrors());
        
        int noOfErrors = response.serviceData.sizeOfPartialErrors();
        if (noOfErrors == 0)
        {
            WhereUsedOutput[] wsOutput = response.output;
            
            WhereUsedParentInfo[] parentInfo = wsOutput[0].info;
            parentAsm = new TCComponent[parentInfo.length];
            
            for (int i = 0; i < parentInfo.length; i++)
            {
                parentAsm[i] = parentInfo[i].parentItemRev;
            }
        }
        setCurrentSOAPolicy();
        
        return parentAsm;
    }
    
    private void setCurrentSOAPolicy()
    {
        try
        {
            if (m_strCurrentSOAPolicy != null)
            {
                SessionService service = SessionService.getService(m_tcSession);
                
                boolean bStatus = service.setObjectPropertyPolicy(m_strCurrentSOAPolicy);
                
                System.out.println(bStatus);
            }
        }
        catch (ServiceException e)
        {
            e.printStackTrace();
        }
    }
    
    private void getCurrentSOAPolicy()
    {
        this.m_strCurrentSOAPolicy = m_tcSession.getSoaConnection().getCurrentObjectPropertyPolicy();
    }
    
    public NOVMBCPolicy createMBCPolicy()
    {
        
        if (m_mbcPolicy == null)
        {
            m_mbcPolicy = new NOVMBCPolicy();
            // Add Property policy for Parent Item Revision object
            NOVMBCPolicyProperty parentRevPolicyProps[] = new NOVMBCPolicyProperty[6];
            parentRevPolicyProps[0] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_REVISION_ID,
                    PolicyProperty.AS_ATTRIBUTE);
            parentRevPolicyProps[1] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_RELEASE_STATUS,
                    PolicyProperty.AS_ATTRIBUTE);
            parentRevPolicyProps[2] = new NOVMBCPolicyProperty(NOVMBCFormComponents.PROCESS_STAGE,
                    PolicyProperty.AS_ATTRIBUTE);
            parentRevPolicyProps[3] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEMS_TAG,
                    PolicyProperty.WITH_PROPERTIES);
            parentRevPolicyProps[4] = new NOVMBCPolicyProperty(NOVMBCFormComponents.CHECKED_OUT,
                    PolicyProperty.AS_ATTRIBUTE);
            parentRevPolicyProps[5] = new NOVMBCPolicyProperty(NOVMBCFormComponents.STRUCTURE_REVISIONS,
                    PolicyProperty.WITH_PROPERTIES);
            
            NOVMBCPolicyProperty parentItemPolicyProps[] = new NOVMBCPolicyProperty[6];
            parentItemPolicyProps[0] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_ID,
                    PolicyProperty.AS_ATTRIBUTE);
            parentItemPolicyProps[1] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_ALT_ID,
                    PolicyProperty.AS_ATTRIBUTE);
            parentItemPolicyProps[2] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_REV_RELEASE_STATUS,
                    PolicyProperty.AS_ATTRIBUTE);
            parentItemPolicyProps[3] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_OBJECT_NAME,
                    PolicyProperty.AS_ATTRIBUTE);
            parentItemPolicyProps[4] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_DESCRIPTION,
                    PolicyProperty.AS_ATTRIBUTE);
            parentItemPolicyProps[5] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_OWNING_GROUP,
                    PolicyProperty.AS_ATTRIBUTE);
            
            NOVMBCPolicyProperty parentBOMViewPolicyProps[] = new NOVMBCPolicyProperty[1];
            parentBOMViewPolicyProps[0] = new NOVMBCPolicyProperty(NOVMBCFormComponents.BOMVIEW_CHECKED_OUT,
                    PolicyProperty.AS_ATTRIBUTE);
            
            m_mbcPolicy.addPropertiesPolicy("ItemRevision", parentRevPolicyProps);
            m_mbcPolicy.addPropertiesPolicy("Item", parentItemPolicyProps);
            m_mbcPolicy.addPropertiesPolicy("BOMView Revision", parentBOMViewPolicyProps);
            
        }
        
        return m_mbcPolicy;
        
    }
    
    public NOVMBCPolicy createMBCFormPolicy()
    {
        
        if (m_mbcFormPolicy == null)
        {
            m_mbcFormPolicy = new NOVMBCPolicy();
            
            NOVMBCPolicyProperty mbcFormPolicyProps[] = new NOVMBCPolicyProperty[6];
            mbcFormPolicyProps[0] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_OWNING_USER,
                    PolicyProperty.AS_ATTRIBUTE);
            mbcFormPolicyProps[1] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_CREATION_DATE,
                    PolicyProperty.AS_ATTRIBUTE);
            mbcFormPolicyProps[2] = new NOVMBCPolicyProperty(NOVMBCFormComponents.ITEM_OWNING_GROUP,
                    PolicyProperty.AS_ATTRIBUTE);
            mbcFormPolicyProps[3] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_COMPONENT_PART,
                    PolicyProperty.WITH_PROPERTIES);
            mbcFormPolicyProps[4] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_REPLACEMENT_PART,
                    PolicyProperty.WITH_PROPERTIES);
            mbcFormPolicyProps[5] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_AFFECTD_ASSEMBLIES,
                    PolicyProperty.WITH_PROPERTIES);
            m_mbcFormPolicy.addPropertiesPolicy(NOVMBCFormComponents.NOV4_MBCFORM, mbcFormPolicyProps);
            
            NOVMBCPolicyProperty snapShotPolicyProps[] = new NOVMBCPolicyProperty[9];
            snapShotPolicyProps[0] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_ITEM_ID,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[1] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_ALT_ID,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[2] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_NAME,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[3] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_ITEM_STATUS,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[4] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_DESCRIPTION,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[5] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_REVISION_ID,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[6] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_REVISION_STATUS,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[7] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_OWNING_USER,
                    PolicyProperty.AS_ATTRIBUTE);
            snapShotPolicyProps[8] = new NOVMBCPolicyProperty(NOVMBCFormComponents.NOV4_OWNING_GROUP,
                    PolicyProperty.AS_ATTRIBUTE);
            m_mbcFormPolicy.addPropertiesPolicy(NOVMBCFormComponents.NOV4_ITEMREVSNAPSHOT, snapShotPolicyProps);
        }
        return m_mbcFormPolicy;
    }
    
    public TCComponent getMBCProperties(TCComponent compObject, String[] attrProperties, NOVMBCPolicy mbcPolicy)
    {
        
        TCComponent plainObject = null;
        TCComponent[] inputObjs = new TCComponent[1];
        inputObjs[0] = compObject;
        // Get Session SOA Policy
        getCurrentSOAPolicy();
        SessionService service = SessionService.getService(m_tcSession);
        service.setObjectPropertyPolicy(mbcPolicy.get_mbcPolicy());
        DataManagementService DMService = DataManagementService.getService(m_tcSession);
        ServiceData serviceData = DMService.getProperties(inputObjs, attrProperties);
        if (serviceData.sizeOfPlainObjects() == 1)
        {
            plainObject = serviceData.getPlainObject(0);
        }
        setCurrentSOAPolicy();
        return plainObject;
    }
    
    public TCComponent[] getMBCProperties(TCComponent[] compObject, String[] attrProperties, NOVMBCPolicy mbcPolicy)
    {
        
        TCComponent[] plainObjects = new TCComponent[compObject.length];
        
        // Get Session SOA Policy
        getCurrentSOAPolicy();
        SessionService service = SessionService.getService(m_tcSession);
        service.setObjectPropertyPolicy(mbcPolicy.get_mbcPolicy());
        DataManagementService DMService = DataManagementService.getService(m_tcSession);
        ServiceData serviceData = DMService.getProperties(compObject, attrProperties);
        for (int i = 0; i < compObject.length; i++)
        {
            plainObjects[i] = serviceData.getPlainObject(i);
        }
        /*
         * if (serviceData.sizeOfPlainObjects() == 1) { plainObject =
         * serviceData.getPlainObject(0); }
         */
        setCurrentSOAPolicy();
        return plainObjects;
    }
    
    public static String[] getAttributeList()
    {
        
        String[] itemAttributeList = { NOVMBCFormComponents.ITEM_REVISION_ID,
                NOVMBCFormComponents.ITEM_REV_RELEASE_STATUS, NOVMBCFormComponents.ITEMS_TAG };
        return itemAttributeList;
    }
    
    public static String[] getMBCFormAttributeList()
    {
        String[] mbcFormAttributeList = { NOVMBCFormComponents.ITEM_OWNING_USER,
                NOVMBCFormComponents.ITEM_CREATION_DATE, NOVMBCFormComponents.ITEM_OWNING_GROUP,
                NOVMBCFormComponents.NOV4_COMPONENT_PART, NOVMBCFormComponents.NOV4_REPLACEMENT_PART,
                NOVMBCFormComponents.NOV4_AFFECTD_ASSEMBLIES };
        
        return mbcFormAttributeList;
    }
    
    public TCComponent getMBCFormProperties(TCComponentForm mbcForm)
    {
        TCComponent mbcFormSOAPlainObject = new TCComponent();
        NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(mbcForm);
        String[] mbcFormAttributes = getMBCFormAttributeList();
        NOVMBCPolicy mbcFormPolicy = dmSOAHelper.createMBCFormPolicy();
        mbcFormSOAPlainObject = dmSOAHelper.getMBCProperties(mbcForm, mbcFormAttributes, mbcFormPolicy);
        return mbcFormSOAPlainObject;
    }
    
}
