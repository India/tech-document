package com.noi.rac.en.util.massbomchange.mbcpanels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;//TC10.1 Upgrade

import com.nov.rac.utilities.utils.SWTUIHelper;
//import com.teamcenter.rac.common.views.SWTResourceManager;//TC10.1 Upgrade
import com.teamcenter.rac.util.Registry;

public class MBCCompPartToReplacePanel extends Composite
{
    public Text m_txtCompPartAltID;
    public Text m_txtCompPartID;
    public Text m_txtCompPartName;
    public Text m_txtCompPartItemStatus;
    public Text m_txtCompPartDesc;
    public Text m_txtCompPartRev;
    public Text m_txtCompPartRevStatus;
    public Text m_txtCompPartOwner;
    public Text m_txtCompPartOwningGrp;
    
    private Label m_lblCompPartId;
    private Label m_lblCompPartAltID;
    private Label m_lblCompPartName;
    private Label m_lblCompPartItemStatus;
    private Label m_lblCompPartDesc;
    private Label m_lblCompPartRev;
    private Label m_lblCompPartRevStatus;
    private Label m_lblCompPartOwner;
    private Label m_lblCompPartOwningGrp;
    private Registry m_compPartReg;
    private Color m_backgroundColor;
    
    public MBCCompPartToReplacePanel(Composite parent, int style)
    {
        super(parent, style);
        setLayout(new GridLayout(1, true));
        setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_compPartReg = Registry.getRegistry(this);
        createUI(this);
        setLabels();
        setTxtBackgroundColor();
        setEditability();
    }
    
    private void createUI(Composite parent)
    {
        Font fontBold = SWTResourceManager.getFont(m_compPartReg.getString("fontTahoma.NAME"), 8, SWT.BOLD);//TC10.1 Upgrade(org.eclipse.wb.swt.SWTResourceManager)
        Composite mainComposite = new Composite(parent, SWT.NONE);
        GridLayout gl_mainComposite = new GridLayout(1, false);
        mainComposite.setLayout(gl_mainComposite);
        m_backgroundColor = mainComposite.getBackground();
        Group grpComponentPartTo = new Group(mainComposite, SWT.NONE);
        GridData gd_grpComponentPartTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        grpComponentPartTo.setLayoutData(gd_grpComponentPartTo);
        GridLayout gl_grpComponentPartTo = new GridLayout(4, false);
        grpComponentPartTo.setLayout(gl_grpComponentPartTo);
        grpComponentPartTo.setText(m_compPartReg.getString("componentPartToReplace.LABEL"));
        grpComponentPartTo.setFont(fontBold);
        
        m_lblCompPartId = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblComponentPartId = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblComponentPartId.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartId,70);
        gdLblComponentPartId.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartId,12);
        m_lblCompPartId.setLayoutData(gdLblComponentPartId);
        
        m_txtCompPartID = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartID = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartID.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartID,110);
        gdTxtCompPartID.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartID,12);
        m_txtCompPartID.setLayoutData(gdTxtCompPartID);
        
        m_lblCompPartAltID = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblComponentPartAltID = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblComponentPartAltID.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartAltID,70);
        gdLblComponentPartAltID.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartAltID,12);
        m_lblCompPartAltID.setLayoutData(gdLblComponentPartAltID);
        
        m_txtCompPartAltID = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartAltID = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartAltID.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartAltID,110);
        gdTxtCompPartAltID.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartAltID,12);
        m_txtCompPartAltID.setLayoutData(gdTxtCompPartAltID);
        
        m_lblCompPartName = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartName = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartName.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartName,70);
        gdLblCompPartName.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartName,12);
        m_lblCompPartName.setLayoutData(gdLblCompPartName);
        
        m_txtCompPartName = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartName = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartName.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartName,110);
        gdTxtCompPartName.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartName,12);
        m_txtCompPartName.setLayoutData(gdTxtCompPartName);
        
        m_lblCompPartItemStatus = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartItemStatus = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartItemStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartItemStatus,70);
        gdLblCompPartItemStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartItemStatus,12);
        m_lblCompPartItemStatus.setLayoutData(gdLblCompPartItemStatus);
        
        m_txtCompPartItemStatus = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartItemStatus = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartItemStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartItemStatus,110);
        gdTxtCompPartItemStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartItemStatus,12);
        m_txtCompPartItemStatus.setLayoutData(gdTxtCompPartItemStatus);
        
        m_lblCompPartDesc = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartDesc = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartDesc.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartDesc,70);
        gdLblCompPartDesc.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartDesc,12);
        m_lblCompPartDesc.setLayoutData(gdLblCompPartDesc);
        
        m_txtCompPartDesc = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartDesc = new GridData(GridData.BEGINNING, GridData.CENTER, true, false, 3, 1);
        gdTxtCompPartDesc.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartDesc,303);
        gdTxtCompPartDesc.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartDesc,12);
        m_txtCompPartDesc.setLayoutData(gdTxtCompPartDesc);
        
        m_lblCompPartRev = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartRev = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartRev.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartRev,70);
        gdLblCompPartRev.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartRev,12);
        m_lblCompPartRev.setLayoutData(gdLblCompPartRev);
        
        m_txtCompPartRev = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartRev = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartRev.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartRev,110);
        gdTxtCompPartRev.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartRev,12);
        m_txtCompPartRev.setLayoutData(gdTxtCompPartRev);
        
        m_lblCompPartRevStatus = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartRevStatus = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartRevStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartRevStatus,70);
        gdLblCompPartRevStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartRevStatus,12);
        m_lblCompPartRevStatus.setLayoutData(gdLblCompPartRevStatus);
        
        m_txtCompPartRevStatus = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartRevStatus = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartRevStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartRevStatus,110);
        gdTxtCompPartRevStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartRevStatus,12);
        m_txtCompPartRevStatus.setLayoutData(gdTxtCompPartRevStatus);
        
        m_lblCompPartOwner = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartOwner = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartOwner.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartOwner,70);
        gdLblCompPartOwner.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartOwner,12);
        m_lblCompPartOwner.setLayoutData(gdLblCompPartOwner);
        
        m_txtCompPartOwner = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartOwner = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartOwner.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartOwner,110);
        gdTxtCompPartOwner.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartOwner,12);
        m_txtCompPartOwner.setLayoutData(gdTxtCompPartOwner);
        
        m_lblCompPartOwningGrp = new Label(grpComponentPartTo, SWT.NONE);
        GridData gdLblCompPartOwningGrp = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblCompPartOwningGrp.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblCompPartOwningGrp,70);
        gdLblCompPartOwningGrp.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblCompPartOwningGrp,12);
        m_lblCompPartOwningGrp.setLayoutData(gdLblCompPartOwningGrp);
        
        m_txtCompPartOwningGrp = new Text(grpComponentPartTo, SWT.BORDER);
        GridData gdTxtCompPartOwningGrp = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtCompPartOwningGrp.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtCompPartOwningGrp,110);
        gdTxtCompPartOwningGrp.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtCompPartOwningGrp,12);
        m_txtCompPartOwningGrp.setLayoutData(gdTxtCompPartOwningGrp);
        
    }
    
    private void setLabels()
    {
        m_lblCompPartId.setText(m_compPartReg.getString("partID.LABEL"));
        m_lblCompPartAltID.setText(m_compPartReg.getString("altID.LABEL"));
        m_lblCompPartName.setText(m_compPartReg.getString("name.LABEL"));
        m_lblCompPartItemStatus.setText(m_compPartReg.getString("itemStatus.LABEL"));
        m_lblCompPartDesc.setText(m_compPartReg.getString("description.LABEL"));
        m_lblCompPartRev.setText(m_compPartReg.getString("revision.LABEL"));
        m_lblCompPartRevStatus.setText(m_compPartReg.getString("revisionStatus.LABEL"));
        m_lblCompPartOwner.setText(m_compPartReg.getString("owner.LABEL"));
        m_lblCompPartOwningGrp.setText(m_compPartReg.getString("owningGroup.LABEL"));
    }
    
    private void setTxtBackgroundColor()
    {
    	m_txtCompPartAltID.setBackground(m_backgroundColor);
        m_txtCompPartID.setBackground(m_backgroundColor);
        m_txtCompPartName.setBackground(m_backgroundColor);
        m_txtCompPartItemStatus.setBackground(m_backgroundColor);
        m_txtCompPartDesc.setBackground(m_backgroundColor);
        m_txtCompPartRev.setBackground(m_backgroundColor);
        m_txtCompPartRevStatus.setBackground(m_backgroundColor);
        m_txtCompPartOwner.setBackground(m_backgroundColor);
        m_txtCompPartOwningGrp.setBackground(m_backgroundColor);
    	
    }
    
    private void setEditability() 
    { 
    	  m_txtCompPartAltID.setEditable(false);
          m_txtCompPartID.setEditable(false);
          m_txtCompPartName.setEditable(false);
          m_txtCompPartItemStatus.setEditable(false);
          m_txtCompPartDesc.setEditable(false);
          m_txtCompPartRev.setEditable(false);
          m_txtCompPartRevStatus.setEditable(false);
          m_txtCompPartOwner.setEditable(false);
          m_txtCompPartOwningGrp.setEditable(false);
    }
     

}
