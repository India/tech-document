package com.noi.rac.en.util.massbomchange.mbcpanels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;//TC10.1 Upgrade

import com.nov.rac.utilities.utils.SWTUIHelper;
//import com.teamcenter.rac.common.views.SWTResourceManager;
import com.teamcenter.rac.util.Registry;

public class MBCReplacementPartPanel extends Composite
{
    public Text m_txtReplPartAltID;
    public Text m_txtReplPartID;
    public Text m_txtReplPartName;
    public Text m_txtReplPartItemStatus;
    public Text m_txtReplPartDesc;
    public Text m_txtReplPartRev;
    public Text m_txtReplPartRevStatus;
    public Text m_txtReplPartOwner;
    public Text m_txtReplPartOwningGrp;
    
    private Label m_lblReplPartId;
    private Label m_lblReplPartAltID;
    private Label m_lblReplPartName;
    private Label m_lblReplPartItemStatus;
    private Label m_lblReplPartDesc;
    private Label m_lblReplPartRev;
    private Label m_lblReplPartRevStatus;
    private Label m_lblReplPartOwner;
    private Label m_lblReplPartOwningGrp;
    private Registry m_replPartReg;
    private Color m_backgroundColor;
    
    public MBCReplacementPartPanel(Composite parent, int style)
    {
        super(parent, style);
        setLayout(new GridLayout(1, true));
        setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_replPartReg = Registry.getRegistry(this);
        createUI(this);
        setLabels();
        setTxtBackgroundColor();
        setEditability();
    }
    
    private void createUI(Composite parent)
    {
        Font fontBold = SWTResourceManager.getFont(m_replPartReg.getString("fontTahoma.NAME"), 8, SWT.BOLD);//TC10.1 Upgrade(org.eclipse.wb.swt.SWTResourceManager)
        Composite mainComposite = new Composite(parent, SWT.NONE);
        GridLayout gl_mainComposite = new GridLayout(1, false);
        mainComposite.setLayout(gl_mainComposite);
        m_backgroundColor = mainComposite.getBackground();
        Group grpReplPartTo = new Group(mainComposite, SWT.NONE);
        GridData gd_grpReplPartTo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        grpReplPartTo.setLayoutData(gd_grpReplPartTo);
        GridLayout gl_grpReplPartTo = new GridLayout(4, false);
        grpReplPartTo.setLayout(gl_grpReplPartTo);
        grpReplPartTo.setText(m_replPartReg.getString("replacementPartSurvivor.LABEL"));
        grpReplPartTo.setFont(fontBold);
        
        m_lblReplPartId = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartId = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartId.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartId,70);
        gdLblReplPartId.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartId,12);
        m_lblReplPartId.setLayoutData(gdLblReplPartId);
        
        m_txtReplPartID = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartID = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartID.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartID,110);
        gdTxtReplPartID.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartID,12);
        m_txtReplPartID.setLayoutData(gdTxtReplPartID);
        
        m_lblReplPartAltID = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartAltID = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartAltID.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartAltID,70);
        gdLblReplPartAltID.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartAltID,12);
        m_lblReplPartAltID.setLayoutData(gdLblReplPartAltID);
        
        m_txtReplPartAltID = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartAltID = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartAltID.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartAltID,110);
        gdTxtReplPartAltID.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartAltID,12);
        m_txtReplPartAltID.setLayoutData(gdTxtReplPartAltID);
        
        m_lblReplPartName = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartName = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartName.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartName,70);
        gdLblReplPartName.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartName,12);
        m_lblReplPartName.setLayoutData(gdLblReplPartName);
        
        m_txtReplPartName = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartName = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartName.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartName,110);
        gdTxtReplPartName.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartName,12);
        m_txtReplPartName.setLayoutData(gdTxtReplPartName);
        
        m_lblReplPartItemStatus = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartItemStatus = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartItemStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartItemStatus,70);
        gdLblReplPartItemStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartItemStatus,12);
        m_lblReplPartItemStatus.setLayoutData(gdLblReplPartItemStatus);
        
        m_txtReplPartItemStatus = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartItemStatus = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartItemStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartItemStatus,110);
        gdTxtReplPartItemStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartItemStatus,12);
        m_txtReplPartItemStatus.setLayoutData(gdTxtReplPartItemStatus);
        
        m_lblReplPartDesc = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartDesc = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartDesc.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartDesc,70);
        gdLblReplPartDesc.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartDesc,12);
        m_lblReplPartDesc.setLayoutData(gdLblReplPartDesc);
        
        m_txtReplPartDesc = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartDesc = new GridData(GridData.BEGINNING, GridData.CENTER, true, false, 3, 1);
        gdTxtReplPartDesc.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartDesc,303);
        gdTxtReplPartDesc.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartDesc,12);
        m_txtReplPartDesc.setLayoutData(gdTxtReplPartDesc);
        
        m_lblReplPartRev = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartRev = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartRev.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartRev,70);
        gdLblReplPartRev.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartRev,12);
        m_lblReplPartRev.setLayoutData(gdLblReplPartRev);
        
        m_txtReplPartRev = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartRev = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartRev.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartRev,110);
        gdTxtReplPartRev.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartRev,12);
        m_txtReplPartRev.setLayoutData(gdTxtReplPartRev);
        
        m_lblReplPartRevStatus = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartRevStatus = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartRevStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartRevStatus,70);
        gdLblReplPartRevStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartRevStatus,12);
        m_lblReplPartRevStatus.setLayoutData(gdLblReplPartRevStatus);
        
        m_txtReplPartRevStatus = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartRevStatus = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartRevStatus.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartRevStatus,110);
        gdTxtReplPartRevStatus.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartRevStatus,12);
        m_txtReplPartRevStatus.setLayoutData(gdTxtReplPartRevStatus);
        
        m_lblReplPartOwner = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartOwner = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartOwner.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartOwner,70);
        gdLblReplPartOwner.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartOwner,12);
        m_lblReplPartOwner.setLayoutData(gdLblReplPartOwner);
        
        m_txtReplPartOwner = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartOwner = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartOwner.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartOwner,110);
        gdTxtReplPartOwner.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartOwner,12);
        m_txtReplPartOwner.setLayoutData(gdTxtReplPartOwner);
        
        m_lblReplPartOwningGrp = new Label(grpReplPartTo, SWT.NONE);
        GridData gdLblReplPartOwningGrp = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblReplPartOwningGrp.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblReplPartOwningGrp,70);
        gdLblReplPartOwningGrp.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblReplPartOwningGrp,12);
        m_lblReplPartOwningGrp.setLayoutData(gdLblReplPartOwningGrp);
        
        m_txtReplPartOwningGrp = new Text(grpReplPartTo, SWT.BORDER);
        GridData gdTxtReplPartOwningGrp = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtReplPartOwningGrp.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtReplPartOwningGrp,110);
        gdTxtReplPartOwningGrp.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtReplPartOwningGrp,12);
        m_txtReplPartOwningGrp.setLayoutData(gdTxtReplPartOwningGrp);
        
    }
    
    private void setLabels()
    {
        m_lblReplPartId.setText(m_replPartReg.getString("partID.LABEL"));
        m_lblReplPartAltID.setText(m_replPartReg.getString("altID.LABEL"));
        m_lblReplPartName.setText(m_replPartReg.getString("name.LABEL"));
        m_lblReplPartItemStatus.setText(m_replPartReg.getString("itemStatus.LABEL"));
        m_lblReplPartDesc.setText(m_replPartReg.getString("description.LABEL"));
        m_lblReplPartRev.setText(m_replPartReg.getString("revision.LABEL"));
        m_lblReplPartRevStatus.setText(m_replPartReg.getString("revisionStatus.LABEL"));
        m_lblReplPartOwner.setText(m_replPartReg.getString("owner.LABEL"));
        m_lblReplPartOwningGrp.setText(m_replPartReg.getString("owningGroup.LABEL"));
    }
    
    private void setTxtBackgroundColor()
    {
        m_txtReplPartAltID.setBackground(m_backgroundColor);
        m_txtReplPartID.setBackground(m_backgroundColor);
        m_txtReplPartName.setBackground(m_backgroundColor);
        m_txtReplPartItemStatus.setBackground(m_backgroundColor);
        m_txtReplPartDesc.setBackground(m_backgroundColor);
        m_txtReplPartRev.setBackground(m_backgroundColor);
        m_txtReplPartRevStatus.setBackground(m_backgroundColor);
        m_txtReplPartOwner.setBackground(m_backgroundColor);
        m_txtReplPartOwningGrp.setBackground(m_backgroundColor);
    }
    
    
    private void setEditability() 
    {
    	m_txtReplPartAltID.showSelection();
    	m_txtReplPartAltID.setEditable(false);
        m_txtReplPartID.setEditable(false);
        m_txtReplPartName.setEditable(false);
        m_txtReplPartItemStatus.setEditable(false);
        m_txtReplPartDesc.setEditable(false);
        m_txtReplPartRev.setEditable(false);
        m_txtReplPartRevStatus.setEditable(false);
        m_txtReplPartOwner.setEditable(false);
        m_txtReplPartOwningGrp.setEditable(false);
    }
     

}
