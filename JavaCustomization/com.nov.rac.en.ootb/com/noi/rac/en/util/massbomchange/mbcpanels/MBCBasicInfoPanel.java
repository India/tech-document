package com.noi.rac.en.util.massbomchange.mbcpanels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.Registry;

public class MBCBasicInfoPanel extends Composite
{
    
    private Label m_lblDateInitiated;
    private Label m_lblOwningGroup;
    private Label m_lblInitiatedBy;
    private Registry m_basicInfoPanelReg;
    public Text m_txtDateInitiatedBy;
    public Text m_txtInitiatedBy;
    public Text m_txtOwningGrp;
    private Color m_backgroundColor;
    
    public MBCBasicInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        setLayout(new GridLayout(4, true));
        setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_basicInfoPanelReg = Registry.getRegistry(this);
        createUI(this);
        setLabels();
        setTxtBackgroundColor();
        setEditability();
    }
    
    private void createUI(Composite parent)
    {
        Composite mainComposite = new Composite(parent, SWT.NONE);
        GridLayout gl_mainComposite = new GridLayout(4, true);
        mainComposite.setLayout(gl_mainComposite);
        m_backgroundColor = mainComposite.getBackground();
        Group grpInfo = new Group(mainComposite, SWT.NONE);
        GridData gdGroupInfo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        grpInfo.setLayoutData(gdGroupInfo);
        GridLayout gl_grpComponentPartTo = new GridLayout(4, false);
        grpInfo.setLayout(gl_grpComponentPartTo);
        
        m_lblInitiatedBy = new Label(grpInfo, SWT.NONE);
        GridData gdLblInitiatedBy = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblInitiatedBy.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblInitiatedBy,70);
        gdLblInitiatedBy.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblInitiatedBy,12);
        m_lblInitiatedBy.setLayoutData(gdLblInitiatedBy);
        
        m_txtInitiatedBy = new Text(grpInfo, SWT.BORDER);
        GridData gdTxtInitiatedBy = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtInitiatedBy.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtInitiatedBy,110);
        gdTxtInitiatedBy.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtInitiatedBy,12);
        m_txtInitiatedBy.setLayoutData(gdTxtInitiatedBy);
        
        m_lblDateInitiated = new Label(grpInfo, SWT.NONE);
        GridData gdLblDateInitiated = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblDateInitiated.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblDateInitiated,70);
        gdLblDateInitiated.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblDateInitiated,12);
        m_lblDateInitiated.setLayoutData(gdLblDateInitiated);
        
        m_txtDateInitiatedBy = new Text(grpInfo, SWT.BORDER);
        GridData gdTxtDateInitiatedBy = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtDateInitiatedBy.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtDateInitiatedBy,110);
        gdTxtDateInitiatedBy.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtDateInitiatedBy,12);
        m_txtDateInitiatedBy.setLayoutData(gdTxtDateInitiatedBy);
        
        m_lblOwningGroup = new Label(grpInfo, SWT.NONE);
        GridData gdLblOwningGroup = new GridData(GridData.END, GridData.CENTER, true, false);
        gdLblOwningGroup.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_lblOwningGroup,70);
        gdLblOwningGroup.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_lblOwningGroup,12);
        m_lblOwningGroup.setLayoutData(gdLblOwningGroup);
        
        m_txtOwningGrp = new Text(grpInfo, SWT.BORDER);
        GridData gdTxtOwningGrp = new GridData(GridData.BEGINNING, GridData.CENTER, true, false);
        gdTxtOwningGrp.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_txtOwningGrp,110);
        gdTxtOwningGrp.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_txtOwningGrp,12);
        m_txtOwningGrp.setLayoutData(gdTxtOwningGrp);
        
        
    }
    
    private void setLabels()
    {
        m_lblInitiatedBy.setText(m_basicInfoPanelReg.getString("initiatedBy.LABEL"));
        m_lblDateInitiated.setText(m_basicInfoPanelReg.getString("dateInitiated.LABEL"));
        m_lblOwningGroup.setText(m_basicInfoPanelReg.getString("owningGroup.LABEL"));
    }
    
    private void setTxtBackgroundColor()
    {
    	m_txtInitiatedBy.setBackground(m_backgroundColor);
    	m_txtDateInitiatedBy.setBackground(m_backgroundColor);
    	m_txtOwningGrp.setBackground(m_backgroundColor);
    }
    
    
     private void setEditability() 
     { 
    	 m_txtDateInitiatedBy.setEditable(false);
    	 m_txtOwningGrp.setEditable(false);
    	 m_txtInitiatedBy.setEditable(false); 
     }
     
}
