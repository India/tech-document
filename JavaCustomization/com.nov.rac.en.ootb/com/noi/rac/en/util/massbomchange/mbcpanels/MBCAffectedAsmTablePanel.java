package com.noi.rac.en.util.massbomchange.mbcpanels;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;//TC10.1 Upgrade

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
//import com.teamcenter.rac.common.views.SWTResourceManager;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class MBCAffectedAsmTablePanel extends Composite
{
    
    public TCTable m_affctdAssmebliesTable;
    private Registry m_affctdAssmebliesReg;
    
    public MBCAffectedAsmTablePanel(Composite parent, int style)
    {
        super(parent, style);
        setLayout(new GridLayout(1, true));
        setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        m_affctdAssmebliesReg = Registry.getRegistry(this);
        createUI(this);
    }
    
    private void createUI(Composite parent)
    {
        
        Font fontBold = SWTResourceManager.getFont(m_affctdAssmebliesReg.getString("fontTahoma.NAME"), 8, SWT.BOLD);//TC10.1 Upgrade(org.eclipse.wb.swt.SWTResourceManager)
        Label lblaffectedAssemblies = new Label(parent, SWT.NONE);
        lblaffectedAssemblies.setText(m_affctdAssmebliesReg.getString("affectedAssemblies.LABEL"));
        lblaffectedAssemblies.setFont(fontBold);
        
        String[] affectedAssembliesColumnNames = m_affctdAssmebliesReg.getStringArray("affectedAssembliesColumns");
        AIFTableModel othrGrpItemstblmodel = new AIFTableModel(affectedAssembliesColumnNames);
        m_affctdAssmebliesTable = new TCTable();
        
        TCSession session = (TCSession)AIFUtility.getDefaultSession();
        m_affctdAssmebliesTable.setSession(session);
        m_affctdAssmebliesTable.setModel(othrGrpItemstblmodel);
        m_affctdAssmebliesTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_affctdAssmebliesTable.getTableHeader().setReorderingAllowed(true);
        
        JScrollPane othrGrpItemsPane = new JScrollPane(m_affctdAssmebliesTable);
        GridLayout gridLayout = new GridLayout();
        
        GridData gd_table = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        gd_table.heightHint =SWTUIHelper.convertVerticalDLUsToPixels(parent,85); 
        gd_table.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(parent,390);
        
        Composite othrGrpItemsContainer = new Composite(parent, SWT.EMBEDDED);
        GridData gd_othrGrpItemsContainer = new GridData(GridData.FILL, GridData.FILL, true, true, 1, 25);
        othrGrpItemsContainer.setLayout(gridLayout);
        othrGrpItemsContainer.setLayoutData(gd_othrGrpItemsContainer);
        othrGrpItemsContainer.setLayoutData(gd_table);
        
        SWTUIUtilities.embed(othrGrpItemsContainer, othrGrpItemsPane, false);
    }
}
