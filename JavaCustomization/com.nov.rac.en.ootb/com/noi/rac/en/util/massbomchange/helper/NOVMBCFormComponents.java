package com.noi.rac.en.util.massbomchange.helper;

public class NOVMBCFormComponents
{
    
    public final static String ITEM_REVISION_ID = "item_revision_id";
    public final static String ITEM_REV_RELEASE_STATUS = "release_statuses";
    public final static String ITEMS_TAG = "items_tag";
    public final static String ITEM_ID = "item_id";
    public final static String ITEM_ALT_ID = "altid_list";
    public final static String ITEM_OBJECT_NAME = "object_name";
    public final static String ITEM_RELEASE_STATUS = "release_statuses";
    public final static String ITEM_DESCRIPTION = "object_desc";
    public final static String ITEM_OWNING_USER = "owning_user";
    public final static String ITEM_OWNING_GROUP = "owning_group";
    public final static String ITEM_CREATION_DATE = "creation_date";
    public final static String NOV4_ITEM_ID = "nov4_item_id";
    public final static String NOV4_ALT_ID = "nov4_alt_id";
    public final static String NOV4_NAME = "nov4_name";
    public final static String NOV4_ITEM_STATUS = "nov4_item_status";
    public final static String NOV4_DESCRIPTION = "nov4_description";
    public final static String NOV4_REVISION_ID = "nov4_revision_id";
    public final static String NOV4_REVISION_STATUS = "nov4_revision_status";
    public final static String NOV4_OWNING_USER = "nov4_owning_user";
    public final static String NOV4_OWNING_GROUP = "nov4_owning_group";
    public final static String NOV4_COMPONENT_PART = "nov4_component_part";
    public final static String NOV4_REPLACEMENT_PART = "nov4_replacement_part";
    public final static String NOV4_AFFECTD_ASSEMBLIES = "nov4_affected_assemblies";
    public final static String NOV4_MBCFORM = "Nov4MassBomChgForm";
    public final static String NOV4_ITEMREVSNAPSHOT = "Nov4ItemRevSnapshot";
    public final static String RELATEDECN = "RelatedECN";
    public final static String PROCESS_STAGE = "process_stage";
    public final static String OBJECT_TYPE = "object_type";
    public final static String CHECKED_OUT = "checked_out";
    public final static String STRUCTURE_REVISIONS = "structure_revisions";
    public final static String BOMVIEW_CHECKED_OUT = "checked_out";
    public final static short COMPONENT_ITEM_KEY = 1;
    public final static short REPLACEMENT_ITEM_KEY = 2;
    public final static short ASSEMBLIES_KEY = 3;
    public final static short AFFECTEDASSMBL_COL_COUNT = 7;
    
}
