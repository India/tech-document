 /* ============================================================
   
   File description: 

   Filename: MassBOMChangeHandler.java 
   Module  : com/noi/rac/en/util/massbomchange/handler

   MassBOMChangeHandler is invoked when user selects 'Mass BOM Change' 
   option from custom 'NOVTools' menu 

   Date         Developers    		Description
   25-11-2011   Rajyalaxmi   	  	Initial Creation

   $HISTORY
   ============================================================ */


package com.noi.rac.en.util.massbomchange.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.en.util.common.MassBOMChangeDialog;
import com.teamcenter.rac.aif.AbstractAIFApplication;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class MassBOMChangeHandler extends AbstractHandler 
{
	TCSession session = null;
	Registry registry = Registry.getRegistry(this);
    public MassBOMChangeHandler()
    {
    	super();
    }
	 
	//@Override
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{		
		session = (TCSession) AIFUtility.getDefaultSession();
		String groupName = null;
		try {
			groupName = session.getCurrentGroup().getGroupName();
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if ( groupName.contains("3C") == true )
		{
			MessageBox.post(registry.getString("invalidGroup"),"Warning",MessageBox.WARNING);
		}
		else
		{
			AbstractAIFApplication app = AIFUtility.getCurrentApplication();
			InterfaceAIFComponent selComp = app.getTargetComponent();
			
			if ( selComp != null & selComp instanceof TCComponentItem)
			{    
			    MassBOMChangeDialog mbomch = new MassBOMChangeDialog(new Shell(), (TCComponentItem)selComp);	
				mbomch.open();				
			}
			else
			{
				MassBOMChangeDialog mbomch = new MassBOMChangeDialog(new Shell());		
				mbomch.open();
			}
		}
		System.out.println("i am in handler");
		return null;
	}
}
	


