/*================================================================================
                     Copyright (c) 2009 National Oilwell Varco
                     Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVComboBox.java
 Package Name: com.noi.rac.commands.newrsitem
 ================================================================================
 Modification Log
 ================================================================================
 Revision       Date         Author        Description  
 1.0            2009/02/04   harshadam     Initial Creation
 1.1            2009/02/05   harshadam     Code Cleanup,Formatting     
 ================================================================================*/
package com.noi.rac.en.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/**
 * @author harshadam
 *
 */
public class NOVComboBox extends JComboBox
{
    private static final long serialVersionUID = 1L;

    public NOVComboBox(Vector arg0)
    {
        super(arg0);
        setEditor(new ComboBoxEditorExample(arg0, (String) arg0.get(0)));
        setEditable(true);
        if (getItemCount() > 5)
            setMaximumRowCount(5);
        else
            setMaximumRowCount(getItemCount());
    }

    public void panelFocusLost()
    {
        if (NOVComboBox.this.isPopupVisible())
        {
            NOVComboBox.this
                    .setSelectedItem(NOVComboBox.this.getSelectedItem());
            NOVComboBox.this.getEditor().setItem(
                    NOVComboBox.this.getSelectedItem());
            NOVComboBox.this.hidePopup();
        }
    }

    class ComboBoxEditorExample implements ComboBoxEditor
    {
        Vector     vector;
        ImagePanel panel;

        /**
         * 
         * @param m
         * @param defaultChoice
         */
        public ComboBoxEditorExample(Vector m, String defaultChoice)
        {
            vector = m;
            panel = new ImagePanel(defaultChoice);
        }

        /**
         * 
         */
        public void setItem(Object anObject)
        {
            if (anObject != null)
            {
                //System.out.println("setItem....." + anObject.toString());
                if (vector.indexOf(anObject.toString()) != -1)
                {
                    panel.setText(anObject.toString());
                }
            }
        }

        public Component getEditorComponent()
        {
            return panel;
        }

        public Object getItem()
        {
            return panel.getText();
        }

        public void selectAll()
        {
            panel.selectAll();
        }

        public void addActionListener(ActionListener l)
        {}

        public void removeActionListener(ActionListener l)
        {}

        /**
         * We create our own inner class to handle setting and
         * repainting the text.
         */
        class ImagePanel extends JPanel implements KeyListener
        {
            /**
             * 
             */
            private static final long serialVersionUID = 1L;
            int                       compCnt;
            String[]                  items;
            Vector                    vItems           = new Vector();
            boolean                   doNotSetText     = true;
            JLabel                    imageIconLabel;
            NOVTextField              textField;
            boolean                   UOMComboFlag     = true;
            boolean                   UOWComboFlag     = false;

            /**
             * 
             * @param initialEntry
             */
            public ImagePanel(String initialEntry)
            {
                UOMComboFlag = true;
                UOWComboFlag = false;
                compCnt = NOVComboBox.this.getModel().getSize();
                if (compCnt > 0)
                {
                    items = new String[compCnt];
                    for (int j = 0; j < compCnt; j++)
                    {
                        items[j] = NOVComboBox.this.getModel().getElementAt(j)
                                .toString();
                        vItems.add(j, items[j]);
                    }
                }
                setLayout(new BorderLayout());
                textField = new NOVTextField(initialEntry, UOMComboFlag,
                        UOWComboFlag, NOVComboBox.this);
                textField.setColumns(45);
                textField.setBorder(new BevelBorder(BevelBorder.LOWERED));
                add(textField, BorderLayout.WEST);
            }

            private Component getModel()
            {
                return null;
            }

            /**
             * 
             * setText
             * 
             * @param s
             * 
             */
            public void setText(String s)
            {
                if (doNotSetText)
                {
                    textField.removeKeyListener(ImagePanel.this);
                    textField.setText("");
                    textField.setText(s);
                    textField.addKeyListener(ImagePanel.this);
                }
                else
                    doNotSetText = true;
            }

            public String getText()
            {
                return (textField.getText());
            }

            public void selectAll()
            {
                textField.selectAll();
            }

            public void addActionListener(ActionListener l)
            {
                textField.addKeyListener(this);
            }

            /**
             * 
             * removeActionListener
             * @param l
             * void
             *
             */
            public void removeActionListener(ActionListener l)
            {
                textField.removeActionListener(l);
            }

            /**
             * 
             */
            public void keyPressed(KeyEvent e)
            {
                int keycode = e.getKeyCode();
                //System.out.println("keyPressed ..." + textField.getText());
                if (keycode == KeyEvent.VK_ENTER)
                {
                    if (NOVComboBox.this.isPopupVisible())
                    {
                        if (textField.getSelectedText() != null)
                        {
                            textField.setSelectedTextColor(Color.BLACK);
                            textField.setSelectionColor(Color.WHITE);
                            textField.setSelectionStart(textField
                                    .getSelectionStart());
                            textField.setSelectionEnd(textField
                                    .getSelectionEnd());
                        }
                        NOVComboBox.this.hidePopup();
                    }
                }
                if (keycode == KeyEvent.VK_TAB)
                {
                    if (NOVComboBox.this.isPopupVisible())
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
            }

            /*//**
             * keyReleased
             */
            public void keyReleased(KeyEvent e)
            {
                String key = Character.toString(e.getKeyChar());
                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_TAB)
                {
                    if (NOVComboBox.this.isPopupVisible())
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
                else if (keyCode == KeyEvent.VK_ENTER)
                {
                    if (NOVComboBox.this.isPopupVisible())
                        NOVComboBox.this.setSelectedItem(NOVComboBox.this
                                .getSelectedItem());
                    if (NOVComboBox.this.isPopupVisible())
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
                else if (keyCode == KeyEvent.VK_DOWN)
                {
                    int index = NOVComboBox.this.getSelectedIndex();
                    if (!(NOVComboBox.this.getItemCount() == index))
                        NOVComboBox.this.setSelectedItem(NOVComboBox.this
                                .getSelectedItem());
                }
                else if (keyCode == KeyEvent.VK_UP)
                {
                    if (!(NOVComboBox.this.getSelectedIndex() == 0))
                        NOVComboBox.this.setSelectedItem(NOVComboBox.this
                                .getSelectedItem());
                }
                else if ((e.getKeyChar() >= 'A' && e.getKeyChar() <= 'Z')
                        || (e.getKeyChar() >= 'a' && e.getKeyChar() <= 'z'))
                {
                    String toCompare = "";
                    if (null != textField.getText()
                            && textField.getText().length() > 1)
                        toCompare = textField.getText().toString();
                    else
                        toCompare = key;
                    // System.out.println("Key to compare if: " + toCompare);
                    int i;
                    for (i = 0; i < items.length; i++)
                    {
                        if (items[i].toLowerCase().startsWith(
                                toCompare.toLowerCase()))
                        {
                            NOVComboBox.this.removeAllItems();
                            String[] sItems = getCompToRefillCombo(toCompare);
                            for (int k = 0; k < sItems.length; k++)
                                NOVComboBox.this.addItem(sItems[k]);
                            NOVComboBox.this.setSelectedItem(items[i]);
                            if (sItems.length > 1)
                            {
                                int length = items[i].length();
                                textField.setSelectedTextColor(Color.WHITE);
                                textField.setSelectionColor(Color.blue);
                                textField.setSelectionStart(toCompare.length());
                                textField.setSelectionEnd(length);
                            }
                            if (sItems.length < 5)
                                NOVComboBox.this
                                        .setMaximumRowCount(sItems.length);
                            else
                                NOVComboBox.this.setMaximumRowCount(5);
                            NOVComboBox.this.showPopup();
                            break;
                        }
                    }
                    if (i == items.length)
                    {
                        if (NOVComboBox.this.isPopupVisible())
                        {
                            NOVComboBox.this.hidePopup();
                        }
                    }
                }
                else if ((e.getKeyChar() == '\b' || e.getKeyCode() == KeyEvent.VK_DELETE)
                        && (textField.getText().length() >= 1))
                {
                    //  System.out.println(e.getKeyChar() + textField.getText());
                    String toCompare = "";
                    // String key = Character.toString(e.getKeyChar());
                    if (textField.getText().length() >= 1)
                        toCompare = textField.getText().toString();
                    //   System.out.println("Key to compare keyReleased: "
                    //  + toCompare);
                    int i;
                    for (i = 0; i < items.length; i++)
                    {
                        if (items[i].toLowerCase().startsWith(
                                toCompare.toLowerCase()))
                        {
                            // selectedItem = items[i];
                            NOVComboBox.this.removeAllItems();
                            String[] sItems = getCompToRefillCombo(toCompare);
                            for (int k = 0; k < sItems.length; k++)
                            {
                                doNotSetText = false;
                                NOVComboBox.this.addItem(sItems[k]);
                            }
                            if (sItems.length < 5)
                                NOVComboBox.this
                                        .setMaximumRowCount(sItems.length);
                            else
                                NOVComboBox.this.setMaximumRowCount(5);
                            NOVComboBox.this.showPopup();
                            return;
                        }
                    }
                    if (i == items.length)
                    {
                        if (NOVComboBox.this.isPopupVisible())
                        {
                            NOVComboBox.this.hidePopup();
                        }
                    }
                }
                else
                {
                    if (NOVComboBox.this.isPopupVisible())
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
                if (textField.getText().length() == 0)
                {
                    NOVComboBox.this.removeAllItems();
                    for (int j = 0; j < items.length; j++)
                    {
                        doNotSetText = false;
                        NOVComboBox.this.addItem(items[j]);
                    }
                    NOVComboBox.this.setMaximumRowCount(5);
                    NOVComboBox.this.showPopup();
                }
                // System.out.println("keyReleased...." + textField.getText());
            }

            public String[] getCompToRefillCombo(String toCompare)
            {
                List list = new LinkedList();
                for (int i = 0; i < items.length; i++)
                {
                    if (items[i].toLowerCase().startsWith(
                            toCompare.toLowerCase()))
                    {
                        list.add(items[i]);
                    }
                }
                String[] strReturn = (String[]) list.toArray(new String[list
                        .size()]);
                return strReturn;
            }

            /**
             * keyTyped
             */
            public void keyTyped(KeyEvent e)
            {
            //System.out.println("Key Typed...." + e.getKeyChar());
            }
        }
    }
}
