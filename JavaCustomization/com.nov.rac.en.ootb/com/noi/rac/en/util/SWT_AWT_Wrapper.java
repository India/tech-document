package com.noi.rac.en.util;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SWT_AWT_Wrapper
{
    public static Canvas m_canvas = null;
    public Shell m_shell = null;
    public final static String MASSBOMFORM = "Mass BOM Change Form";
    
    public static Shell getShell(JPanel panel)
    {
    	
    	m_canvas = new Canvas();
    	createUI();
        JTabbedPane tabbedPane = new JTabbedPane();
        panel.add("top.nobind.center.center", tabbedPane);
        tabbedPane.addTab(MASSBOMFORM, m_canvas);
        return SWT_AWT.new_Shell(Display.getDefault(), m_canvas);
    }
    
    private static void createUI(){
    	 Toolkit toolkit = Toolkit.getDefaultToolkit();
         int screenResolution = toolkit.getScreenResolution();
         Dimension dimension = null;
         if(screenResolution<=100){
        	 dimension = new Dimension(620, 675);
         }else if(screenResolution>100 && screenResolution<125){
        	 dimension =new Dimension(825, 750);
         }else if(screenResolution>125 && screenResolution<150){
        	 dimension = new Dimension(1030, 720);
         }
         m_canvas.setSize(dimension);
    }
    
}
