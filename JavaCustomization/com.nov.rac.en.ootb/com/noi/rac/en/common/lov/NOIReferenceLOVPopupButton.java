/*
 * Created on Mar 17, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.en.common.lov;

import java.util.Hashtable;

import com.teamcenter.rac.common.lov.LOVPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.ListOfValuesInfo;



/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NOIReferenceLOVPopupButton extends LOVPopupButton {

	Hashtable refToString = new Hashtable();
	Hashtable stringToRef = new Hashtable();
	TCComponent[] lovItemComponents;
	TCSession session;
	String      LOVName="";
	
	/**
	 * 
	 */
	public NOIReferenceLOVPopupButton() {
		super();
		// TODO Auto-generated constructor stub
		LOVName = "noargs";

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOIReferenceLOVPopupButton(TCComponentListOfValues arg0,
			String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		session = arg0.getSession();
		LOVName = arg1;
		populateHashtables();
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public NOIReferenceLOVPopupButton(TCSession arg0, String arg1, String arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
		session = arg0;
		LOVName = arg1;
		populateHashtables(arg2);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NOIReferenceLOVPopupButton(TCSession arg0, String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		session = arg0;
		LOVName = arg1;
		
		populateHashtables();
	}
	
	//TC 10.1 Upgrade
    //Commenting this method as it is not used any where else	
/*	public void setFilteredLOVComponent(NOIReferenceLOVPopupButton src,String query,String qryParam) {
		
		TCComponent srcComponent = src.getSelectedReferenceValue();
		String selectedVal = src.getSelectedTextValue(srcComponent);
		if (srcComponent != null) {
			try {
				session = srcComponent.getSession();
				TCComponentQueryType qt = 
					(TCComponentQueryType)session.getTypeComponent("ImanQuery");
				TCComponentQuery gen = (TCComponentQuery)qt.find(query);
				if (gen != null) {
					TCComponent[] filtered = gen.execute(new String[]{qryParam},
														new String[]{selectedVal});
					TCComponentListOfValues lov = new TCComponentListOfValues();
					if (filtered != null && filtered.length > 0) {
					    lov.insertValues(filtered,0);
					    this.setLovComponent(lov);
					}
				}
			}
			catch (Exception e) {
				System.out.println("Could not populate LOV ("+srcComponent.toString()+"): "+e);
			}
         }			
	}*/
	
	public void setLOVComponentByName(TCSession session,String name) {
		super.setLOVComponentByName(session,name);
		populateHashtables();		
	}
	
	private void populateHashtables()
	{
	    
        try
        {
        	int lovitem=0;
        	
        	TCComponentListOfValues lovc = getLovComponent();
        	if (lovc != null) {
        		ListOfValuesInfo lovi = getLovComponent().getListOfValues();
        		if (lovi != null) {
        			TCComponent[] lov = lovi.getTagListOfValues();
        			if (lov != null) {
        	        	for (lovitem=0;lovitem<lov.length;lovitem++) {
        	   	            TCProperty nameprop = lov[lovitem].getTCProperty("Name");
        	   	            String val = nameprop.getStringValue();
        	   	            if (nameprop == null || val == null) 
        	   	            	System.out.println("nameprop or val == null");
        	   	            refToString.put(lov[lovitem],val);
        	   	            stringToRef.put(val,lov[lovitem]);
        	   	            //System.out.println(LOVName+">> "+val+"["+lov[lovitem].toString()+"]");
        	        	}
        			}
        		}
        	}
            
        }
        catch(Exception ex)
        {
        	System.out.println(LOVName+">> Error populating hashtable(1): "+ex);
        } 
	}
	private void populateHashtables(String columnName)
	{
        try
        {
        	int lovitem=0;
        	TCComponent[] lov = getLovComponent().getListOfValues().getTagListOfValues();
        	for (lovitem=0;lovitem<lov.length;lovitem++) {
   	            TCProperty nameprop = lov[lovitem].getTCProperty(columnName);
   	            String val = nameprop.getStringValue();
   	            if (nameprop == null || val == null) 
   	            	System.out.println("nameprop or val == null");
   	            refToString.put(lov[lovitem],val);
   	            stringToRef.put(val,lov[lovitem]);
   	            //System.out.println(LOVName+">> "+val+"["+lov[lovitem].toString()+"]");
        	}
        }
        catch(Exception ex)
        {
        	System.out.println(LOVName+">> Error populating hashtable(2): "+ex);
        } 
	    
	}
	public TCComponent getSelectedReferenceValue()
	{
		String sVal = getText();
		if (sVal != null) {
			TCComponent val = (TCComponent)stringToRef.get(getText());
			if (val == null) {
				;//System.out.println(LOVName+" ** Could not find component for value ["+getText()+"]");
			}
			return ((TCComponent)stringToRef.get(getText()));
		}
		else
			return null;
	}

	public String getSelectedTextValue(TCComponent inp)
	{
	    if (inp == null) {
	    	;//System.out.println("getSelectedTextValue() inp == null");
	        return null;
	    }
	    String val = (String)refToString.get(inp);
		if (val == null) {
			;//System.out.println(LOVName+" ** Could not find string val for component");
		}
	    return ((String)refToString.get(inp));
	}
}
