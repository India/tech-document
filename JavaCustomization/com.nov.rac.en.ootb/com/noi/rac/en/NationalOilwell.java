package com.noi.rac.en;

/**
* <p>Description: Static variables for Site</p>
* @author Mark Hoover
* @version $Change: 9 $ $DateTime: 2005/01/07 21:03:44 $
*/
//
// /////////////////////////////////////////////////////
// Copyright: Copyright (c) 2004-2005
// Company: Hoover & Nebrig, Inc
// Source control info
// $File: //depot/Natoil/java/com/noi/NationalOilwell.java $
// $Revision:   1.0  $
// $Date:   Sep 18 2007 13:59:40  $
// $Author:   hughests  $
// /////////////////////////////////////////////////////
//

import java.awt.Color;
import java.awt.Font;

public class NationalOilwell
{
   public final static boolean DEBUG = true;
   public final static String DSTEMPLATE_USERSERVICE = "NATOIL_getDatasetTemplatesUS";
   public final static String DSCREATE_USERSERVICE = "NATOIL_createDatasetUS";
   public final static String NR_USERSERVICE = "NATOIL_isNamedRefOkUS" ;
   public final static String NEXTREV_USERSERVICE = "NATOIL_getNextRevIdUS";
   public final static String NEXTALPHAREV_USERSERVICE = "NATOIL_getNextAlphaRevIdUS";
   public final static String REVISE_ITEM_USERSERVICE = "NATOIL_reviseItemUS";
   public final static String CREATE_ITEM_USERSERVICE = "NATOIL_createItemV2US";
   public final static String SAVE_AS_ITEM_USERSERVICE = "NATOIL_itemSaveAsV1US";
   public final static String CREATE_PRODUCT_FOLDERS = "NATOIL_createProductFoldersUS";
  // public final static String NEXT_ID_USERSERVICE = "NATOIL_getNextIdUS";
   public final static String NEXT_SEQNO_USERSERVICE = "NATOIL_getNextSeqnoUS";
   //public final static String NEXT_SEQNOV2_USERSERVICE = "NATOIL_getNextSeqnoV2US";
   public final static String NEXT_SHEETNO_USERSERVICE = "NATOIL_getNextSheetUS";
   public final static String COPY_FOLDER_CONTENTS = "NATOIL_copyFolderContentsUS";
   public final static String COPY_ITEM_ATTACHMENTS = "NATOIL_copyItemAttachmentsUS";
   public final static String COPY_BOM = "NATOIL_copybomUS";
   public final static String REMOVE_RELATION = "NATOIL_removeRelationUS";
   public final static String ADD_REVISION_GROUP_FORMS = "NATOIL_AddRevisionGroupFormsUS";
   public final static String CLASSIFY_ITEM_USERSERVICE = "NOV_classifyObjectUS";

   public final static String BADEXT = " is not a valid file extension for the dataset type ";
   public final static String COOKIENAME = "natoil";
   public final static String DATASETTYPELOV = "ValidDatasetTypes_LOV";
   public final static String DOCTYPELOV = "ValidDocumentTypes_LOV";
   public final static String ITEMTYPELOV = "ValidItemTypes_LOV";
   public final static String _MILESTONES_ = "_Milestones_";
   public final static String NOFILE = "File cannot be located: ";
   public final static String NOHELP = "Help file cannot be located.";
   public final static String NOIMANDATA = "Cannot determine IMAN_DATA directory by preference.";
   public final static String NOREVASSIGN = "Cannot assign rev without a base number.";
   public final static String NOSEQNOASSIGN = "Cannot assign seqno without a base number and rev.";
   public final static String NOSHEETASSIGN = "Cannot assign sheet without a base number and rev and sequence number.";
   public final static String NUMBERONLYCHARS = "0123456789";
   public final static String NOSUBTYPE = " is missing the subtypes List of Values.\nSee your TCEng Administrator.";
   public final static String BADDOCTYPE = " is not a valid document type.";
   public final static String BADSUBTYPE = " is not a valid subtype.";
   public final static String OKREVCHARS = "0123456789ABCDEFGHJKLMNPRTUVWXY";
   public final static String DOCUMENTTYPE = "Documents";
   public final static String STANDARDPRODUCTDESIGNATOR = "S";
   public final static String SEEADMIN = "See your TCEng Administrator";

   public final static String NODSCREATE = "You may only create datasets under Item Revisions.";
   public final static String SINGLEREVONLY = "You must select a single Item Revision.";

   public final static int MAXBASELEN = 15;
   public final static int MAXDESCLEN = 132;
   public final static int MAXNAMELEN = 32;
   public final static int MAXREVLEN  = 2;
   public final static int MAXSEQNOLEN = 3;
   public final static int MAXSHEETLEN = 2;
   public final static int MAXEMAILIDLEN = 254;

   public final static String PRODUCTITEMTYPE = "Product";
   public final static String NUMBEREDPRODUCTITEM = "Numbered Product";
   public final static String PRODUCTCATEGORYITEMTYPE = "ProductCategory";
   
   public final static String PRODUCTITEMTYPETEXT = "S - Product";
   public final static String SUBTYPETYPELOVPATTERN = "_SubType_LOV";

   public final static int NEW_ITEM = 0;
   public final static int SAVE_AS = 1;
   public final static int REVISION = 2;
   public final static int COPY = 4;
   
   public final static Color NOVPanelBG = new Color(225,225,225);
   public final static Color NOVPanelContrast = new Color(173,173,173);
   public final static Color NOVActiveFieldBG = Color.WHITE;
   public final static Font  NOVB12ptFont = new Font("Dialog",java.awt.Font.BOLD,12);
   public final static Font  NOVB10ptFont = new Font("Dialog",java.awt.Font.BOLD,10);
   public final static Font  NOV12ptFont = new Font("Dialog",java.awt.Font.PLAIN,12);
   public final static Font  NOV10ptFont = new Font("Dialog",java.awt.Font.PLAIN,10);
   
   
   public final static String SIM_GROUP = ".SIM";
   public final static String DHT_GROUP = ".DHT";
   public final static String RS_MECHANICAL = ".RS - Mechanical";
   public final static String SHP_DHT = "33 - AIR.DHT";
   public final static String CON_TOT = "60 - CON.Texas Oil Tools";
   public final static String SHP_RPG = "22 - SHP.Rig Package Group";
   public final static String WLY_SHAFFER = "45 - WLY.Shaffer";
   public final static String SERVICE = "82 - Field Engineering.Service";
   public final static String SHP_WGB = "99 - WGB.Mission";
   public final static String TUBOSCOPE = "2B � HR.Tuboscope";
   
   public final static String DefaltTypeByDocCatg = "_DefaultTypesByDocCategory_LOV";

   public NationalOilwell()
   {
   }
}