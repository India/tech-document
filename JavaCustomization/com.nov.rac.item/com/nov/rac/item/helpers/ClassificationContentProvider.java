package com.nov.rac.item.helpers;

import java.util.ArrayList;

import org.eclipse.jface.fieldassist.ContentProposal;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.swt.widgets.Control;

public class ClassificationContentProvider extends SimpleContentProposalProvider
{

    public ClassificationContentProvider(String[] proposals)
    {
        super(proposals);
        this.proposals = proposals;
    }
    

    private IControlContentAdapter controlContentAdapter;
    private String[] proposals;
    private IContentProposalProvider proposalProvider;
    private Control control;
    private IContentProposal contentProposals[];
    
    @Override
    public IContentProposal[] getProposals(String contents, int position)
    {
        ArrayList<ContentProposal> list = new ArrayList<ContentProposal>();
        String[] filteredProposal;
        for (int i = 0; i < proposals.length; i++)
        {
            filteredProposal = proposals[i].split("->");
            for(int inx=1; inx< filteredProposal.length; inx++)
            {
                if (filteredProposal[inx].length() >= contents.length()
                        && filteredProposal[inx].substring(0, contents.length()).equalsIgnoreCase(contents))
                    list.add(new ContentProposal(proposals[i]));
            }
           
        }
            
        
        return (IContentProposal[]) list.toArray(new IContentProposal[list.size()]);
   
    }
    
    
  
    

    
}
