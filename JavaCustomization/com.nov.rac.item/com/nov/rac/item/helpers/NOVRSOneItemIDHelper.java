package com.nov.rac.item.helpers;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.nov.cet.rsone.iteminterface.ItemInterfaceResponse;
import com.nov.cet.rsone.iteminterface.ItemUpdate;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEData;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataLastUpdate;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataManufacturer;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTargetOrgs;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributes;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute1;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute2;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute3;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute4;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute5;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute6;
import com.nov.cet.rsone.iteminterface.ItemUpdateTCEDataTechnicalAttributesTechAttribute7;
import com.nov.rac.item.panels.rsone.RSOneCreateDocumentDocCategoryPanel;
import com.nov.rac.item.panels.rsone.RSOneDocumentSimpleInfoPanel;
import com.teamcenter.rac.kernel.TCComponentSite;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCSiteInfo;
import com.teamcenter.rac.util.Registry;

public class NOVRSOneItemIDHelper
{

    public static String getOracleId()
    {
        String timeStamp = new SimpleDateFormat("MMddHHmmss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }
	
	/*public static String getOracleId() throws Exception
    {
		
		TCPreferenceService prefSer1 = session.getPreferenceService();
		String oracleid =null;
		String fullGroup = session.getCurrentGroup().getFullName();
		String resp=null;
		
        
		
		ItemUpdate item = new ItemUpdate();
		ItemUpdateTCEData data = new ItemUpdateTCEData();
		ItemUpdateTCEDataLastUpdate upd = new ItemUpdateTCEDataLastUpdate();
		ItemUpdateTCEDataTechnicalAttributes atts = new ItemUpdateTCEDataTechnicalAttributes();
        atts.setTechAttribute1(new ItemUpdateTCEDataTechnicalAttributesTechAttribute1());
        atts.setTechAttribute2(new ItemUpdateTCEDataTechnicalAttributesTechAttribute2());
        atts.setTechAttribute3(new ItemUpdateTCEDataTechnicalAttributesTechAttribute3());
        atts.setTechAttribute4(new ItemUpdateTCEDataTechnicalAttributesTechAttribute4());
        atts.setTechAttribute5(new ItemUpdateTCEDataTechnicalAttributesTechAttribute5());
        atts.setTechAttribute6(new ItemUpdateTCEDataTechnicalAttributesTechAttribute6());
        atts.setTechAttribute7(new ItemUpdateTCEDataTechnicalAttributesTechAttribute7());
		data.setTechnicalAttributes(atts);
		
		data.setItemID(oracleid);
	    data.setDocumentCategory(m_createDOCCatagotyPanel.m_textDocCategory.getText());
		
		try {
			item.setBaseEN("NA");
			item.setEventType("CREATE");
			item.setIndependent(true);
			item.setLanguageCode("US");
			item.setProcessID("NA");
			item.setRequestor("NA");
            TCComponentSite site = session.getCurrentSite();
            TCSiteInfo info = site.getSiteInfo();
			String database = info.getSiteName();
			upd.setSourceSystem(database);
			upd.setTimeStamp(Calendar.getInstance());
			String user = System.getProperty("user.name");
			upd.setUser(user);
			String strTCEDatabase = null;
			//Get the group names from preference to send Item create to new schema
            String prefGrpNames[] = null;
            String splitStrs[] = fullGroup.split("\\.");
            TCPreferenceService prefSer = session.getPreferenceService();
            if ( prefSer != null )
            {
            	prefGrpNames = prefSer.getStringArray(TCPreferenceService.TC_preference_site, registry.getString("otherRSoneInstances.NAME"));
            }
            
            for ( int inx = 0; prefGrpNames != null && inx < prefGrpNames.length; inx ++ )
            {
           	 for ( int jnx = 0; splitStrs != null && jnx < splitStrs.length; jnx ++ )
           	 {
	 	            if( prefGrpNames[inx].equals(splitStrs[jnx]) )
	 	            {
	 	            	strTCEDatabase = splitStrs[jnx];
	 	            	break;
	 	            }
           	 }
           	 if ( strTCEDatabase != null )
           	 {
           		 break;
           	 }
            }
            if ( strTCEDatabase != null )
            {
	           	 item.setRSONE_INSTANCE(strTCEDatabase);
            }
            else
            {               
                 String prefstring = prefSer.getString(TCPreferenceService.TC_preference_site, registry.getString("isitProduction.NAME"));
                 if(prefstring != null && prefstring.length() > 0 && prefstring.equalsIgnoreCase("YES"))
                	 item.setRSONE_INSTANCE(registry.getString("Pruduction.NAME"));
                 else
                	 throw new Exception((registry
      						.getString("nonPRODinPROD.MSG")));
            }
          
			
		
        	data.setItemDecription(m_RSOneDocumentSimplePanel.m_descriptionText.getText()); 
            data.setItemName(m_RSOneDocumentSimplePanel.m_comboName.getText());                   	
  
			ItemInterfaceResponse iir = new ItemInterfaceResponse();
			
		//	iir = p.process(item);
			
			resp = iir.getResult();
		
			}
			catch (Exception e) {
				System.out.println(e);
	            System.out.println("Could not add item to RSOne... " + e);
	        	throw e;
			}
		
		return resp;
		

		
    }*/
    
	private static RSOneDocumentSimpleInfoPanel m_RSOneDocumentSimplePanel;
	private  static RSOneCreateDocumentDocCategoryPanel m_createDOCCatagotyPanel;
    private static TCSession session=null;
    protected static Registry registry= null;
    
}
