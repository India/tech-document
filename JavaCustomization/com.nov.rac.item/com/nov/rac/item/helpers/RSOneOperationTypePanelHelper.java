package com.nov.rac.item.helpers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.nov.NationalOilwell;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RSOneOperationTypePanelHelper
{
    public static Map<String, String> getPartsTypeLov(String argType) throws TCException
    {
        TCComponentListOfValues engLOVs[] = null;
        TCComponentListOfValuesType lovType = (TCComponentListOfValuesType) getSession().getTypeComponent(
                NationalOilwell.ListOfValues);
        if (argType.equals(getRegistry().getString("operationTypePanel.Engineering")))
        {
            engLOVs = lovType.find(NationalOilwell.LOV_NOVEngineeringParts);
        }
        else if (argType.equals(getRegistry().getString("operationTypePanel.NonEngineering")))
        {
            engLOVs = lovType.find(NationalOilwell.NOVNon_EngineeringPartsLOV);
        }
        if (engLOVs != null && engLOVs.length > 0)
        {
            Object objs[] = engLOVs[0].getListOfValues().getListOfValues();
            String objd[] = engLOVs[0].getListOfValues().getDescriptions();
            
            fillMapForTypeMapping(objs, objd, argType);
            
            return m_OracleItemTypeCodeMap;
        }
        else
        {
            throw (new TCException(getRegistry().getString("EngLOVMissing.ERROR")));
        }
    }
    
    private static void fillMapForTypeMapping(Object[] objs, String[] desc, String argType) throws TCException
    {
        m_OracleTCEEngTypeMap = new HashMap<String, String>();
        m_OracleItemTypeCodeMap = new HashMap<String, String>();
        int noOfLOVs = objs.length;
        
        for (int i = 0; i < noOfLOVs; i++)
        {
            String sLovValue = (String) objs[i];
            if (!sLovValue.isEmpty())
            {
                boolean isValidLovValue = checkPreferenceValue(sLovValue);
                
                if (isValidLovValue)
                {
                    mapPartToTypeAndCode(sLovValue,argType,desc,i);                 
                }
            }
        }
        
        if (m_OracleTCEEngTypeMap.size() < 0)
        {
            throw (new TCException(getRegistry().getString("DelimterDatainCodenLOVMismatch.MSG")));
        }
    }
    
    private static void mapPartToTypeAndCode(String sLovValue, String argType, String[] desc, int i)
    {
        String sOracleTypeKeySet = sLovValue;
        String sTCETypeValueSet = null;
        
        sTCETypeValueSet = mapToPartOrPurchasePart(sOracleTypeKeySet, argType);
        m_OracleTCEEngTypeMap.put(sOracleTypeKeySet, sTCETypeValueSet);
        
        if (null != desc)
        {
            m_OracleItemTypeCodeMap.put(sOracleTypeKeySet, desc[i]);
        }
    }

    private static String mapToPartOrPurchasePart(String sOracleTypeKeySet, String argType)
    {
        String sTCETypeValueSet = null;
        if (argType.equals(getRegistry().getString("operationTypePanel.Engineering")))
        {
            String[] partList = getRegistry().getStringArray("partList.NAME");
            if (Arrays.asList(partList).contains(sOracleTypeKeySet))
            {
                sTCETypeValueSet = "Part";
            }
            else
            {
                sTCETypeValueSet = "Purchased Part";
            }
        }
        else if (argType.equals(getRegistry().getString("operationTypePanel.NonEngineering")))
        {
            String[] partList = getRegistry().getStringArray("non-engPartList.NAME");
            if (Arrays.asList(partList).contains(sOracleTypeKeySet))
            {
                sTCETypeValueSet = "Non-Engineering";
            }
            else
            {
                sTCETypeValueSet = "Non-Engg Purchased Part";
            }
        }
        return sTCETypeValueSet;
    }
    
    private static Boolean checkPreferenceValue(String sLovValue)
    {
        boolean removeMatch = true;
        
        TCPreferenceService prefSer = getSession().getPreferenceService();
        
        String[] prefValues = prefSer.getStringValuesAtLocation(getRegistry().getString("HiddenRSOneEngTypePref.NAME"),
                TCPreferenceLocation.GROUP_LOCATION);
        
        prefValues = (prefValues == null) ? new String[0]:prefValues;
        
        String[] RSOneNXTypes = prefSer.getStringValuesAtLocation(
                getRegistry().getString("NXRSOneItemTypesToSkipPref.NAME"), TCPreferenceLocation.OVERLAY_LOCATION);
        
        RSOneNXTypes = (RSOneNXTypes == null) ? new String[0]:RSOneNXTypes;
        
        if (Arrays.asList(prefValues).contains(sLovValue) || Arrays.asList(RSOneNXTypes).contains(sLovValue))
        {
            removeMatch = false;
        }
        return removeMatch;
    }
    
    private static TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
    }
    
    private static Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    public static String getPartType()
    {
        if(m_lovName != null)
        {
            try
            {
                getPartsTypeLov(m_lovName);
                setLoveName(null);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
            return m_OracleTCEEngTypeMap.get(selectedItemType);
        }
        if(m_OracleTCEEngTypeMap != null)
        {    
            return m_OracleTCEEngTypeMap.get(selectedItemType);
        }
        else
        {
            return "";
        }
    }
    
    public static String getSelectedItemType()
    {
        return selectedItemType;
    }

    public static void setSelectedItemType(String selectedItemType)
    {
        RSOneOperationTypePanelHelper.selectedItemType = selectedItemType;
    }
    
    public static Map<String, String> getOracleItemTypeCodeMap()
    {
        return m_OracleItemTypeCodeMap;
    }
    
    public static String getSelectedPartType()
    {
        return selectedPartType;
    }
    
    public static void setLoveName(String lovName)
    {
        m_lovName = lovName;
    }

    public static void setSelectedPartType(String selectedPartType)
    {
        RSOneOperationTypePanelHelper.selectedPartType = selectedPartType;
    }
    
    private static String m_lovName = null;
    private static Map<String, String> m_OracleTCEEngTypeMap;
    private static Map<String, String> m_OracleItemTypeCodeMap;
    private static String selectedItemType = null;
    
    private static String selectedPartType = null;

   
    
}
