package com.nov.rac.item.helpers;



import java.util.LinkedList;
import java.util.List;




public class ClassTreeNode
{
	private String value;
	private List<ClassTreeNode> childrenofnode = new LinkedList<ClassTreeNode>();
	public ClassTreeNode(final String rootValue)
	 {
		  value = rootValue;
	 }
	 public final String getValue()
	 {
		 return value;
	 }
	 public final List<ClassTreeNode> getChildrenList()
	 {
		 return childrenofnode;
	 }
}

