package com.nov.rac.item.helpers;

import java.util.HashMap;
import java.util.Map;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;

public class RSOneDashPropertyMap
{
    private static HashMap<String, IPropertyMap> dashMap = new HashMap<String, IPropertyMap>();
    private static IPropertyMap propertyMap = new SimplePropertyMap();
    private static Map<String, IPropertyMap> dashPropertyMap = new HashMap<String, IPropertyMap>();
    
    public static void addDash(String dashNumber)
    {
        dashMap.put(dashNumber, propertyMap);
    }
    
    public void setPropertyMap(IPropertyMap propertyMap)
    {
        this.propertyMap = propertyMap;
    }
    
    public static Map<String, IPropertyMap> getDashPropertyMap()
    {
        return dashPropertyMap;
    }
    
    public static void setDashPropertyMap(Map<String, IPropertyMap> l_dashPropertyMap)
    {
        dashPropertyMap = l_dashPropertyMap;
    }
}
