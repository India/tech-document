package com.nov.rac.item.helpers;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinition;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinitionType;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;

public class NOVDatasetTypeUIPanelHelper
{
    
    public NOVDatasetTypeUIPanelHelper()
    {
        
    }
    
    public static TCComponentListOfValues getDatasetTypes() throws TCException
    {
        TCSession l_session = (TCSession) AIFUtility.getDefaultSession();
                
        if (m_datasetTypes == null)
        {
            m_datasetTypes = TCComponentListOfValuesType.findLOVByName(l_session,
                    NationalOilwell.DATASETTYPELOV);
        }
        return m_datasetTypes;
    }
    
    public static String getExtension(String nameOfFile)
    {
        int dot = nameOfFile.lastIndexOf('.');
        int len = nameOfFile.length();
        if (dot > 0 && dot < len)
            return nameOfFile.substring(dot);
        else
            return "";
    }
    
    public static boolean isNamedRefValid(IPropertyMap propMap) throws TCException
    {
        TCSession l_session = (TCSession) AIFUtility.getDefaultSession();
        TCUserService l_service = l_session.getUserService();
        Boolean bOk = new Boolean(false);
        Object objs[] = new Object[2];
        objs[0] = getExtension(propMap.getString(NationalOilwell.DATASET_FILE_PATH));
        objs[1] = propMap.getString(NationalOilwell.DATASET_TYPE);
        try
        {
            bOk = (Boolean) l_service.call(NationalOilwell.NR_USERSERVICE, objs);
        }
        catch (TCException ex)
        {
            ex.printStackTrace();
        }
        return bOk.booleanValue();
    }
    
    public static TCComponentDatasetDefinition getDatasetDefinition(String datasetType,TCSession session)
    {
        TCComponentDatasetDefinition theDatasetDefinition = null;
        TCComponentDatasetDefinitionType datasetDefinitionType;
        try
        {
            datasetDefinitionType = (TCComponentDatasetDefinitionType) session.getTypeComponent("DatasetType");
            if(datasetDefinitionType != null)
            {
                theDatasetDefinition = datasetDefinitionType.find(datasetType);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
     
        return theDatasetDefinition;
    }
    
    private static TCComponentListOfValues m_datasetTypes = null;
}
