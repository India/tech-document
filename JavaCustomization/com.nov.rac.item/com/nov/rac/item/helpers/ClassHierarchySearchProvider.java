package com.nov.rac.item.helpers;


import java.util.Locale;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;



import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class ClassHierarchySearchProvider implements INOVSearchProvider
{
    
	public ClassHierarchySearchProvider(final SimplePropertyMap map)
    {
    	  m_dataMap = map;
    }
    
    @Override
	public final String getBusinessObject() 
    {     
        return null;
    }
    
    @Override
	public final QuickBindVariable[] getBindVariables()
    {
    	QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[1];
    	
    	String[] currentLocale = getCurrentLocale();

		if(currentLocale.length>0)
		{
			allBindVars[0] = new QuickSearchService.QuickBindVariable();
			allBindVars[0].nVarType = POM_string;
			allBindVars[0].nVarSize = currentLocale.length;
			allBindVars[0].strList = currentLocale;
			
			
		}	
    	return allBindVars;
    }
   
	@Override
	public final QuickHandlerInfo[] getHandlerInfo() 
    {
         QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[4];
         
         allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
         allHandlerInfo[0].handlerName = "NOVSRCH-get-classification-hierarchy";
         allHandlerInfo[0].listReqdColumnIndex = new int[] {1, 2, 3, 4, 5 };
         allHandlerInfo[0].listInsertAtIndex = new int[]   {1, 2, 3, 4, 5}; 
         allHandlerInfo[0].listBindIndex = new int[] {};
         
         allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
         allHandlerInfo[1].handlerName = "NOVSRCH-get-classification-hierarchy-childlocale";
         allHandlerInfo[1].listReqdColumnIndex = new int[] {1, 2};
         allHandlerInfo[1].listInsertAtIndex = new int[]   {2, 7};
         allHandlerInfo[1].listBindIndex =  new int[] {1};
         
         allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
         allHandlerInfo[2].handlerName = "NOVSRCH-get-classification-hierarchy-parentlocale";
         allHandlerInfo[2].listReqdColumnIndex = new int[] {1, 2};
         allHandlerInfo[2].listInsertAtIndex = new int[]   {5, 8};
         allHandlerInfo[2].listBindIndex =  new int[] {1};          

		 allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
         allHandlerInfo[3].handlerName = "NOVSRCH-get-classification-hierarchy-leafquery";
         allHandlerInfo[3].listReqdColumnIndex = new int[] {1, 2};
         allHandlerInfo[3].listInsertAtIndex = new int[]   {5, 9};
         allHandlerInfo[3].listBindIndex =  new int[] {1};
         
         
         return allHandlerInfo;
    }
	
	private String[] getCurrentLocale() 
	 {
			// TODO Auto-generated method stub
	    	Locale locale = Locale.FRANCE;
	    	String[] currentLanguage = new String[1];
	    	currentLanguage[0]= locale.toString();
			return currentLanguage;
	}
	private SimplePropertyMap m_dataMap= new SimplePropertyMap();
    
    
}
