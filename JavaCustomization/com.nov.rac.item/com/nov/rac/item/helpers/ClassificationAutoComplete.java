package com.nov.rac.item.helpers;

import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.text.FindReplaceDocumentAdapterContentProposalProvider;
import org.eclipse.swt.widgets.Control;

public class ClassificationAutoComplete
{
    public ClassificationAutoComplete(Control control, IControlContentAdapter controlContentAdapter, String proposals[])
    {
        proposalProvider = new ClassificationContentProvider(proposals);
        proposalProvider.setFiltering(true);
        adapter = new ContentProposalAdapter(control, controlContentAdapter, proposalProvider, null, null);
        adapter.setPropagateKeys(true);
        adapter.setProposalAcceptanceStyle(2);
    }

    public void setProposals(String proposals[])
    {
        proposalProvider.setProposals(proposals);
    }

    private ClassificationContentProvider proposalProvider;
    private ContentProposalAdapter adapter;
}
