package com.nov.rac.item.helpers;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.widgets.Control;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.common.GroupUtils;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NewItemCreationHelper
{
    
    /**
     * Given the current logged in group, get the Business Group name.
     * 
     * @param strCurrentGroup
     * @return Business Group Name
     */
    public static String getBusinessGroup()
    {
        return GroupUtils.getBusinessGroupName();
        // return GroupUtils.getBusinessGroupName();//TC10.1 Change to get
        // operations according to the respective group
        // return GroupUtils.getBusinessGroupName();//TC10.1 Change to get
        // operations according to the respective group
    }
    
    /**
     * Gives selected TCComponent if it is item or item revision else returns
     * null
     * 
     * @return target component
     */
    
    public static TCComponent getTargetComponent()
    {
        TCComponent targetComponent = null;
        TCComponent selectedComponent = getSelectedContainer();
        
        if (selectedComponent instanceof TCComponentItem || selectedComponent instanceof TCComponentItemRevision)
        {
            targetComponent = selectedComponent;
        }
        
        return targetComponent;
    }
    
    /**
     * sets selected TCComponent //by sudarshan for 6576
     */
    
    public static void setTargetComponent(TCComponent targetComponent)
    {
        if (targetComponent instanceof TCComponentItem || targetComponent instanceof TCComponentItemRevision)
        {
            setSelectedContainer(targetComponent);
        }
    }
    
    /**
     * 
     * @return current logged in group
     */
    public static String getCurrentGroup()
    {
        return GroupUtils.getCurrentGroupName();
        // return GroupUtils.getCurrentGroupName();//TC10.1 Change to get
        // operations according to the respective group
        // return GroupUtils.getCurrentGroupName();//TC10.1 Change to get
        // operations according to the respective group
    }
    
    public static String[] getConfiguration(String context)
    {
        Registry registry = Registry.getRegistry("com.nov.rac.item.panels.panels");
        return getConfiguration(context, registry);
    }
    
    public static String[] getConfiguration(String context, Registry registry)
    {
        String[] configParams;
        String defaultPanelContext = null;
        
        String currentGroup = NewItemCreationHelper.getCurrentGroup();
        
        defaultPanelContext = currentGroup + NationalOilwell.DOTSEPARATOR + context;
        
        // read default configuration to be created for current logged in group
        configParams = registry.getStringArray(defaultPanelContext, null, null);
        
        int noOfGroups = currentGroup.split("\\.").length;
        
        // This is to iterate over full current group name to get context,
        // if no default panels for current logged in full group name found.
        // It will remove lower most subgroup from group name string
        // and will check for context & so on
        for (int inx = 1; configParams == null && inx < noOfGroups; inx++)
        {
            currentGroup = currentGroup.substring(currentGroup.indexOf(".") + 1);
            
            defaultPanelContext = currentGroup + NationalOilwell.DOTSEPARATOR + context;
            configParams = registry.getStringArray(defaultPanelContext, null, null);
        }
        
        // if no default configuration for current logged in group,
        // read default configuration to be created for current business
        // unit
        if (configParams == null)
        {
            defaultPanelContext = NewItemCreationHelper.getBusinessGroup() + NationalOilwell.DOTSEPARATOR + context;
            configParams = registry.getStringArray(defaultPanelContext, null, null);
        }
        
        // if no default configuration for current business unit,
        // read default configuration to be created
        if (configParams == null)
        {
            defaultPanelContext = context;
            configParams = registry.getStringArray(defaultPanelContext, null, null);
        }
        return configParams;
    }
    
    public static String getSelectedOperation()
    {
        return m_selectedOperation;
    }
    
    public static void setSelectedOperation(String operationName)
    {
        m_selectedOperation = operationName;
    }
    
    /**
     * Gets the container. Gives the selected folder if applicable, otherwise
     * gives the home folder
     * 
     * @return the container
     */
    public static TCComponent getContainer()
    {
        
        TCComponent targetComponent = null;
        TCComponent selectedComponent = getSelectedContainer();
        
        if (selectedComponent instanceof TCComponentFolder)
        {
            targetComponent = selectedComponent;
        }
        if (null == targetComponent)
        {
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            try
            {
                targetComponent = tcSession.getUser().getHomeFolder();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        return targetComponent;
        
    }
    
    /**
     * sets the component selected by Search button
     * 
     * @param selectedComponent
     */
    public static void setSelectedComponent(TCComponent selectedComponent)
    {
        m_selectedComponent = selectedComponent;
    }
    
    /**
     * Gives component which is selected using Search Button
     * 
     * @return TCComponent
     */
    public static TCComponent getSelectedComponent()
    {
        return m_selectedComponent;
    }
    
    /**
     * Disable the right mouse button click on given swt control
     */
    public static void disableRightClick(Control control)
    {
        control.addMenuDetectListener(new MenuDetectListener()
        {
            
            @Override
            public void menuDetected(MenuDetectEvent paramEvent)
            {
                paramEvent.doit = false;
            }
        });
        
    }
    
    /**
     * Validate target componetnt
     * 
     * @throws TCException
     */
    public static void validateTargetComponent() throws TCException
    {
        // InterfaceAIFComponent[] selectedComponents =
        // (InterfaceAIFComponent[])
        // AifrcpPlugin.getSelectionMediatorService().getTargetComponents();
        InterfaceAIFComponent[] selectedComponents = (InterfaceAIFComponent[]) AIFUtility.getTargetComponents();// TC10.1
                                                                                                                // Upgrade
        
        if (selectedComponents.length == 1 && selectedComponents != null)
        {
            if (!(selectedComponents[0] instanceof TCComponentItem
                    || selectedComponents[0] instanceof TCComponentItemRevision || selectedComponents[0] instanceof TCComponentFolder))
            {
                throw new TCException("Please select single valid component for New Item Creation.");
            }
        }
        else
        {
            throw new TCException("Please select single valid component for New Item Creation.");
        }
    }
    
    public static TCComponent getSelectedContainer()
    {
        return m_selectedContainer;
    }
    
    public static void setSelectedContainer(TCComponent m_selectedContainer)
    {
        NewItemCreationHelper.m_selectedContainer = m_selectedContainer;
    }
    
    public static String getSelectedType()
    {
        return selectedType;
    }
    
    public static void setSelectedType(String string)
    {
        selectedType = string;
    }
    
    public static String getPrefix()
    {
        return prefix;
    }
    
    public static void setPrefix(String string)
    {
        prefix = string;
    }
    
    public static void Initializer()
    {
        m_selectedOperation = "";
        m_selectedComponent = null;
        m_selectedContainer = null;
        selectedType = "Part";
        prefix = null;
    }
    
    private static String m_selectedOperation = "";
    private static TCComponent m_selectedComponent = null;
    private static TCComponent m_selectedContainer = null;
    private static String selectedType = "Part";
    private static String prefix = null;
    
}