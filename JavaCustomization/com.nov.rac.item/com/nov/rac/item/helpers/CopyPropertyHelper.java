package com.nov.rac.item.helpers;

public class CopyPropertyHelper
{
    private String[] propertyName;
    private Object propValue;
    private String displayName;
    
    public String[] getPropertyName()
    {
        return propertyName;
    }
    public void setPropertyName(String[] propertyName)
    {
        this.propertyName = propertyName;
    }
    public Object getPropValue()
    {
        return propValue;
    }
    public void setPropValue(String propValue)
    {
        this.propValue = propValue;
    }
    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    public void setProperties(String[] propertyName, Object propertyValue, String displayName )
    {
        this.propertyName = propertyName;
        this.propValue = propertyValue;
        this.displayName = displayName;
    }
    
    
}
