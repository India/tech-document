package com.nov.rac.item.helpers;

/**
 * The Class DocCategoryUIModel.
 */
public class DocCategoryUIModel
{
    
    /** The document category. */
    private String documentCategory;
    
    /** The document type. */
    private String documentType;
    
    private boolean docCategoryTextEnabl;
    /**
     * Gets the document category.
     *
     * @return the document category
     */
    public String getDocumentCategory()
    {
        return documentCategory;
    }
    
    /**
     * Sets the document category.
     *
     * @param documentCategory the new document category
     */
    public void setDocumentCategory(String documentCategory)
    {
        this.documentCategory = documentCategory;
    }
    
    /**
     * Gets the document type.
     *
     * @return the document type
     */
    public String getDocumentType()
    {
        return documentType;
    }
    
    /**
     * Sets the document type.
     *
     * @param documentType the new document type
     */
    public void setDocumentType(String documentType)
    {
        this.documentType = documentType;
    }
    
    
    
    public boolean isDocCategoryTextEnabled()
    {
		return docCategoryTextEnabl;
	}

	public void setDocCategoryTextEnable(boolean docCategoryTextEnabl)
	{
		this.docCategoryTextEnabl = docCategoryTextEnabl;
	}

}
