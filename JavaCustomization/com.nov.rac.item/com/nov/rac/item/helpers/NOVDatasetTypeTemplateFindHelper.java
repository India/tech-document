package com.nov.rac.item.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.item.helpers.DatasetTemplatesTree.TreeNode;

public class NOVDatasetTypeTemplateFindHelper
{
    private static NOVDatasetTypeTemplateFindHelper instance = null;
    private static INOVResultSet resultset = null;
    
    private NOVDatasetTypeTemplateFindHelper()
    {
        invokeService();
    }
    
    public static NOVDatasetTypeTemplateFindHelper getInstance()
    {
        if (instance == null)
        {
            instance = new NOVDatasetTypeTemplateFindHelper();
        }
        
        return instance;
        
    }
    
    private static void invokeService()
    {
        INOVSearchResult searchResult = null;
        DatasetTemplatesSearchProvider m_searchService = new DatasetTemplatesSearchProvider();
        try
        {
            if ((m_searchService instanceof INOVSearchProvider2)
                    && (((INOVSearchProvider2) m_searchService).validateInput() == true))
            {
                searchResult = NOVSearchExecuteHelperUtils.execute(m_searchService, INOVSearchProvider.LOAD_ALL);
                resultset = searchResult.getResultSet();
                Vector<String> rowdata = resultset.getRowData();
                
                tree = new DatasetTemplatesTree(rowdata.get(PARENTUID_COLUMN));
                tree.rootNode.setData(rowdata.get(PARENT_COLUMN), rowdata.get(PARENTUID_COLUMN));
                for (int rowCount = 0; rowCount < resultset.getRows(); rowCount++)
                {
                    int index = resultset.getCols() * rowCount;
                    
                    TreeNode childNode = new TreeNode(rowdata.get(index + NAME_COLUMN), rowdata.get(index + TYPE_COLUMN),
                            rowdata.get(index + UID_COLUMN), rowdata.get(index + PARENTUID_COLUMN), rowdata.get(index
                                    + PARENT_COLUMN));
                    tree.rootNode.add(childNode);
                }
            }
           
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    public Map<String , String> getDatasetTemplate(String datasetName, String currentGroup, String docCategory, String docType)
    {
        
        Map<String, String> datasetTemplateList = new HashMap<String, String>();
        this.currentGroup = currentGroup;
        this.docCategory = docCategory;
        this.docType = docType;
        if(tree != null )
        {
            datasetTemplateList = getDatasetTemplate(tree.rootNode, datasetName);
        }
        
        return datasetTemplateList;
        
    }
    
    private Map<String, String> getDatasetTemplate(TreeNode rootNode, String nodeName)
    {
        Map<String, String> datasetTemplateList = new HashMap<String, String>();
        
        TreeNode node = tree.find(rootNode, nodeName);
        if (node != null)
        {
            TreeNode groupNode = tree.find(node, currentGroup);
            if (groupNode != null)
            {
                getTemplateList(datasetTemplateList, groupNode);
            }
            else
            {
                getTemplateList(datasetTemplateList, node);
            }
            
        }
        
        return datasetTemplateList;
    }
    
    /**
     * @param datasetTemplateList
     * @param groupNode
     */
    private void getTemplateList(Map<String, String> datasetTemplateList, TreeNode node)
    {
        TreeNode docNode = tree.find(node, docCategory);
        if (docNode != null)
        {
            TreeNode docTypeNode = tree.find(docNode, docType);
            if (docTypeNode != null)
            {
                for (String leafNode : docTypeNode.childNodes.keySet())
                {
                    if (!docTypeNode.childNodes.get(leafNode).type.contains("Folder"))
                    {
                        datasetTemplateList.put(leafNode , docTypeNode.childNodes.get(leafNode).uid);
                    }
                }
            }
            else
            {
                for (String leafNode : docNode.childNodes.keySet())
                {
                    if (!docNode.childNodes.get(leafNode).type.contains("Folder"))
                    {
                        datasetTemplateList.put(leafNode , docNode.childNodes.get(leafNode).uid);
                    }
                }
                // datasetTemplateList.addAll(docNode.childNodes.keySet());
            }
        }
        else
        {
            for (String leafNode : node.childNodes.keySet())
            {
                if (!node.childNodes.get(leafNode).type.contains("Folder"))
                {
                    datasetTemplateList.put(leafNode , node.childNodes.get(leafNode).uid);
                }
            }
            // datasetTemplateList.addAll(node.childNodes.keySet());
        }
    }
    
    private String currentGroup, docCategory;
    private static int UID_COLUMN = 0;
    private static int NAME_COLUMN = 1;
    private static int TYPE_COLUMN = 2;
    private static int PARENT_COLUMN = 3;
    private static int PARENTUID_COLUMN = 4;
    private static DatasetTemplatesTree tree;
    private String docType;
}
