package com.nov.rac.item.helpers;

import java.util.ArrayList;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCComponentUser;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;

public class DatasetTemplatesSearchProvider implements INOVSearchProvider2
{

    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public QuickBindVariable[] getBindVariables() {
        QuickBindVariable[] bindVariables = new QuickBindVariable[1];
        
        // Relation type
        bindVariables[0] = new QuickBindVariable();
        bindVariables[0].nVarType = POM_typed_reference;
        bindVariables[0].nVarSize = 1;
        
        bindVariables[0].strList = new String[] { puidOfFolder };
        return bindVariables;
    }

    @Override
    public QuickHandlerInfo[] getHandlerInfo() {
        QuickHandlerInfo[] allHandlerInfo = new QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-folder-contents";
        allHandlerInfo[0].listBindIndex = new int[] { 1 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4};
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 3, 4};
        return allHandlerInfo;
    }
    
    @Override
    public boolean validateInput()
    {
        puidOfFolder = getDatasetTemplatesUid();
        if (puidOfFolder == null)
        {
            return false;
        }
        return true;
    }
    
    public String getDatasetTemplatesUid()
    {
        TCSession tcSession =  (TCSession) AIFUtility.getDefaultSession();
        TCComponentQueryType qt;
        String uid = null;
        TCComponentFolder homeFolder = null;
        try
        {
            
            qt = (TCComponentQueryType)tcSession.getTypeComponent("ImanQuery");
            TCComponentQuery gen = (TCComponentQuery)qt.find("Admin - Employee Information");
            if (gen != null) {
                TCComponent[] arr = gen.execute(new String[]{"Person Name","User ID"},new String[]{"infodba","infodba"});
                if(arr[0] instanceof TCComponentUser)
                {
                    TCProperty homeFolderProperty = arr[0].getTCProperty("my_home_folder");
                    homeFolder = (TCComponentFolder) homeFolderProperty.getReferenceValueArray()[0];
                }
                
               AIFComponentContext[] chidrenContext = homeFolder.getChildren();
               for(int i =0; i<chidrenContext.length;i++)
               {
                   InterfaceAIFComponent component = chidrenContext[i].getComponent();
                   if(component instanceof TCComponentFolder)
                   {
                       TCComponentFolder folder = (TCComponentFolder) component;
                       String name = folder.getProperty("object_name");
                       if(name.equalsIgnoreCase("Dataset Templates"))
                       {
                           uid = folder.getUid();
                       }
                   }
               }
               
                System.out.println(arr);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
            
        return uid;
    }
    
    private String puidOfFolder;
    
}
