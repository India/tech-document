package com.nov.rac.item.helpers;

import com.richclientgui.toolbox.validation.validator.IFieldValidator;

public class MandatoryLabelFieldValidator implements IFieldValidator {
    
    private boolean isValidValue = false;
    
    public void setValid(boolean decision)
    {
        isValidValue = decision;
    }

    @Override
    public String getErrorMessage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getWarningMessage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isValid(Object content) 
    {
        return isValidValue;
    }

    @Override
    public boolean warningExist(Object arg0) {
        // TODO Auto-generated method stub
        return false;
    }
}

