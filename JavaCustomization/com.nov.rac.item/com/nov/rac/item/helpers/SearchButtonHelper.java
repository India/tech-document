package com.nov.rac.item.helpers;

import javax.swing.ListSelectionModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.item.itemsearch.ISearchPropertiesProvider;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.utilities.utils.SWTUIHelper;

public class SearchButtonHelper
{
    public static SearchButtonComponent getSerachButton(Composite composite,GridData searchBtnGridData,String[] searchItemTypes)
    {
        SearchButtonComponent searchButton = new SearchButtonComponent(composite, SWT.TOGGLE);
        searchBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
                NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
        String operationContext = NewItemCreationHelper.getSelectedOperation();
        searchButton.setLayoutData(searchBtnGridData);
        
        searchButton.setProperty(ISearchPropertiesProvider.ITEM_TYPES, searchItemTypes);
        searchButton.setProperty(ISearchPropertiesProvider.SEARCH_CONTEXT, "NID");
        searchButton.setProperty(ISearchPropertiesProvider.OPERATION_CONTEXT,operationContext);
        searchButton.setProperty(ISearchPropertiesProvider.SELECTION_MODE, ListSelectionModel.SINGLE_SELECTION);
        
        //searchButton.setProperty(ISearchPropertiesProvider.SELECTION_MODE,ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        return searchButton;
    }
}
