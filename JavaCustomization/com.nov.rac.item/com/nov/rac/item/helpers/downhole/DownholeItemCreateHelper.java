package com.nov.rac.item.helpers.downhole;

import com.nov.NationalOilwell;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;

public class DownholeItemCreateHelper
{
	
	public static boolean isItemIdEditableItemTypes(String itemType)
    {
        return PreferenceHelper.isStringExistInPref(itemType, NationalOilwell.PREF_DHL_ITEMID_EDITABLE_ITEM_TYPES, TCPreferenceLocation.OVERLAY_LOCATION);
    }

}
