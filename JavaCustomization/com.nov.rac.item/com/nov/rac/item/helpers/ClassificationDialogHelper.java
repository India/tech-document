package com.nov.rac.item.helpers;

import java.util.List;

import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.classification.common.G4MInClassDialog;
import com.teamcenter.rac.classification.common.tree.G4MAutoScrollingJTree;
import com.teamcenter.rac.classification.common.tree.G4MTreeNode;
import com.teamcenter.rac.classification.icm.services.IClassificationLogicService;
import com.teamcenter.rac.common.AbstractTCApplication;
import com.teamcenter.rac.util.OSGIUtil;


public class ClassificationDialogHelper
{
    public static void getClassificationData()
    {
        m_app = ClassificationDialogHelper.getClassificationApp();
        
        m_compContext = new G4MInClassDialog(m_app);
        m_classificationtree = m_compContext.getTree().getTree();
        int childCnt = m_classificationtree.getModel().getChildCount(m_classificationtree.getModel().getRoot());
        for (int index=childCnt-1;index>=0;index--)
        {
            G4MTreeNode node1 = (G4MTreeNode)m_classificationtree.getModel().getChild(m_classificationtree.getModel().getRoot(), index);
           
                m_compContext.getTree().setRootNode(node1, true);
                m_compContext.getTree().repaint();
                m_compContext.getTree().refresh(node1.getNodeName(), true, true);
              
                m_classificationtree =  m_compContext.getTree().getTree();
            
        }
    }
    
    public static AbstractAIFUIApplication getClassificationApp()
    {
        m_app = (AbstractAIFUIApplication) OSGIUtil.getService(AifrcpPlugin.getDefault(),
                IClassificationLogicService.class);
        
        if (!(m_app instanceof AbstractTCApplication))
        {
            List<AbstractAIFUIApplication> appList = AIFUtility.getActiveDesktop().getApplications();
            for (int i = 0; i < appList.size(); i++)
            {
                if (appList.get(i) instanceof AbstractTCApplication)
                {
                    m_app = (AbstractTCApplication) appList.get(i);
                    break;
                }
            }
        }
        return m_app;
    }
    
    private static AbstractAIFUIApplication m_app;
    private static G4MInClassDialog m_compContext;
    private static G4MAutoScrollingJTree m_classificationtree;
}
