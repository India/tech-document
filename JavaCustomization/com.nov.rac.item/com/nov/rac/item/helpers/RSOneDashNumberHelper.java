package com.nov.rac.item.helpers;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.item.itemsearch.IdentifierSearchProvider;

public class RSOneDashNumberHelper
{

/*    public Vector<String> getTargetItemDashNumbers(String altId)
    {
        Vector<String> dashListVector = getTargetItemIds(altId);
        Vector<String> dashNumberList = getDashNumbers(dashListVector);
        
        return dashNumberList;
    }*/
    
    public static String[] getAllDashNumbers(String altId)
    {
        Vector<String> dashListVector = getTargetItemIds(altId);
        
        m_allDashNumbers = getDashNumberList(dashListVector);
        
        return m_allDashNumbers;
    }
    
    private static Vector<String> getTargetItemIds(String altId)
    {
        Vector<String> dashListVector = new Vector<String>();
        
        // example : get '1218123646' from 1218123646-001@RSOne
        altId = altId.split(DASH_STR)[0];
        
        try
        {
            IdentifierSearchProvider altIdSearchProvider = new IdentifierSearchProvider(altId + "*");
            INOVResultSet results = altIdSearchProvider.execute(INOVSearchProvider.INIT_LOAD_ALL);
            
            dashListVector = results.getRowData();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return dashListVector;
    }
    
/*    private Vector<String> getDashNumbers(Vector<String> dashListVector)
    {
        Vector<String> vdashNumbers = new Vector<String>();
        
        for (int i = 0; i < dashListVector.size(); i++)
        {
            if (dashListVector.get(i).contains(DASH_STR))
            {
                String dashNumber = dashListVector.get(i).split(DASH_STR)[1];
                vdashNumbers.add(dashNumber);
            }
        }
        
        return vdashNumbers;
    }*/
    
    private static String[] getDashNumberList(Vector<String> dashListVector)
    {
        // TODO Auto-generated method stub
        String[] dashNumberArray;
        // first remove puid entries from vector if any
        for (int i = 0; i < dashListVector.size(); i++)
        {
            if (!dashListVector.get(i).contains("-"))
            {
                dashListVector.remove(i);
            }
        }
        // list needs to be listed in sorted order with new dash number to be
        // added in it.
        SortedSet<String> dashNumberSet = new TreeSet<String>();
        // separate itemid string from dash number
        for (int i = 0; i < dashListVector.size(); i++)
        {
            if (dashListVector.get(i).contains(DASH_STR))
            {
                String dashNumber = dashListVector.get(i).split(DASH_STR)[1];
                dashNumberSet.add(dashNumber);
            }
        }
        
        dashNumberSet = addNewDashNumber(dashNumberSet);
        dashNumberArray = dashNumberSet.toArray(new String[dashNumberSet.size()]);
        return dashNumberArray;
    }
    
    private static SortedSet<String> addNewDashNumber(SortedSet<String> dashNumberSet)
    {
        int missingNum = 0;
        // TODO Auto-generated method stub
        if (Integer.valueOf(dashNumberSet.first()) != 1)
        {
            missingNum = 1;
        }
        else if (Integer.valueOf(dashNumberSet.last()) == dashNumberSet.size())
        {
            missingNum = Integer.valueOf(dashNumberSet.last()) + 1;
        }
        else
        {
            //findFirstMissingDashNumber expects array to start with 0
            dashNumberSet.add("000");
            String[] dashNumberArray = dashNumberSet.toArray(new String[dashNumberSet.size()]);
            missingNum = findFirstMissingDashNumber(dashNumberArray, 0, dashNumberArray.length - 1);
        }
        
        if (missingNum < LEAST_FOUR_DIGIT_NUMBER)
        {
            m_missingNumStr = String.format("%03d", missingNum);
        }
        else if (missingNum > MAX_THREE_DIGIT_NUMBER && missingNum < LEAST_FIVE_DIGIT_NUMBER)
        {
            m_missingNumStr = String.format("%04d", missingNum);
        }
        
        dashNumberSet.add(m_missingNumStr);
        
        return dashNumberSet;
    }
    
    private static int findFirstMissingDashNumber(String[] array, int startIndex, int endIndex)
    {
        if (startIndex > endIndex)
        {
            return endIndex + 1;
        }
        
        if (startIndex != Integer.valueOf(array[startIndex]))
        {
            return startIndex;
        }
        
        int midIndex = (startIndex + endIndex) / 2;
        
        if (Integer.valueOf(array[midIndex]) > midIndex)
        {
            return findFirstMissingDashNumber(array, startIndex, midIndex);
        }
        else
        {
            return findFirstMissingDashNumber(array, midIndex + 1, endIndex);
        }
    } 
    
    public static String getMissingDashNumber()
    {
        return m_missingNumStr;
    }
    
    public static Vector<String> getExistingDashNumbers()
    {
        Vector<String> existingDashNumbers = new Vector<String>();
        
        if(m_allDashNumbers != null)
        {
            for(String theDashNumber : m_allDashNumbers)
            {
                if(!(m_missingNumStr.compareTo(theDashNumber) == 0))
                {
                    existingDashNumbers.add(theDashNumber);
                }
            }
        }
        return existingDashNumbers;
    }

    private static String m_missingNumStr = null;
    private static String[] m_allDashNumbers = null;
    private static final String DASH_STR = "-";
    
    private static final int MAX_THREE_DIGIT_NUMBER = 999;
    private static final int LEAST_FOUR_DIGIT_NUMBER = 1000;
    private static final int LEAST_FIVE_DIGIT_NUMBER = 10000;
}
