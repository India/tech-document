package com.nov.rac.item.helpers;

import java.text.DecimalFormat;

import com.nov.NationalOilwell;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.nov.rac.utilities.common.NOVDataManagementServiceInfo;
import com.nov.rac.utilities.common.PreferenceUtils;

public class GenerateItemId {
	public static TCSession getSession() {
		return (TCSession) AIFUtility.getCurrentApplication().getSession();
	}

	public static String assignItemId() {
		TCSession session = getSession();
		Object objs[] = new Object[5];
		String newIdStr = null;
		TCPreferenceService prefServ = session.getPreferenceService();
		String[] grpStrArray = prefServ.getStringArray(
				TCPreferenceService.TC_preference_site,
				"NOV_mission_groups_sharing_id_counter");
		String strGroup = null;
		String currentUsrGrp = session.getCurrentGroup().toString();
		for (int inx = 0; grpStrArray != null && inx < grpStrArray.length; inx++) {
			if (currentUsrGrp.equals(grpStrArray[inx])) {
				strGroup = "5D";
				break;
			} else if (currentUsrGrp != null
					&& currentUsrGrp.indexOf(".") != -1) {
				String missionGrp = currentUsrGrp.substring(0,
						currentUsrGrp.indexOf("."));
				if (missionGrp.equals(grpStrArray[inx])) {
					strGroup = "5D";
					break;
				}

			}
		}
		// String strGroup = getGroup().substring(0,2);
		//objs[0] = "";
		//objs[1] = strGroup != null ? strGroup : ""; /* pszGroupCode */
		//objs[2] = 01; /* iRev */
		//objs[3] = 0; /* iSeqno */
		//objs[4] = 0; /* iSheet */
		
		String strTypeCode = "";
		String strGroupCode = strGroup != null ? strGroup : "";
		int iSeqNo = 0;

		String strCounterName = null;
		String strTempItemType = null;
		String strPrefixExp = null;
		String strPostfixExp = null;
		
		try {
			
			strTempItemType = tempItemType(strGroupCode, strTypeCode);

			String strTempGroupCode = tempGroupCode(strGroupCode);

			strCounterName = strTempItemType + strTempGroupCode + "_itemid";

			strPrefixExp = strTempItemType + strGroupCode;
			
			
			if( iSeqNo != 0)
	 		{
	 			strPostfixExp =String.format("%03d", iSeqNo);  			
	 		}	 		
	 		
	 		NOVDataManagementServiceInfo novGetNextValueService = new NOVDataManagementServiceInfo();
	 		
	 		setDatatoService(novGetNextValueService, strCounterName,  strPrefixExp,  strPostfixExp, strGroupCode);			
		 		
	 		//SOA call to get next id	
			//Integer newId = (Integer) session.getUserService().call(
					//NationalOilwell.NEXT_ID_USERSERVICE, objs);
			
			Integer newId = Integer.parseInt(novGetNextValueService.getNextValue()); 
			
			DecimalFormat fmt = new DecimalFormat("50000000");
			newIdStr = fmt.format(newId.longValue());

		} catch (Exception ex) {
			ex.printStackTrace();
			MessageBox.post("Could not assign Item Id",
					NationalOilwell.SEEADMIN, MessageBox.ERROR);
		}
		return newIdStr;
	}
	
	/**
	    * Set Input data to the service
	    */   
	   
	   static void setDatatoService(NOVDataManagementServiceInfo novGetNextValueService,String strCounterName,
			   													String strPrefixExp, String strPostfixExp,String strGroupCode)
	   {
			int theScope = TCPreferenceService.TC_preference_site;	

			novGetNextValueService.setContext("ITEM_ID");
			novGetNextValueService.setCounterName(strCounterName);
			novGetNextValueService.setDefaultCounterStartValue(1000000);
			novGetNextValueService.setPrefix(strPrefixExp);
			novGetNextValueService.setPostfix(strPostfixExp);

			PreferenceUtils PrefObj = new PreferenceUtils();

			boolean bJDEGroupDesicion = PrefObj.isPrefValueExist(strGroupCode, "NOV_JDE_check_creation_groups",theScope);
			if(bJDEGroupDesicion) 
			{
				novGetNextValueService.setAlternateIDContext(new String[] {"JDE"});
			}else
			{		
				novGetNextValueService.setAlternateIDContext(new String[] {""});
			}
	   }
	 
	   /**
	    * Compute TempItemType
	    */
	   static String tempItemType(String strGroupCode,String strTypeCode )
	   {
			String strTempItemType;

			if (strGroupCode.equals("M") || strGroupCode.equals("4A"))
			{
				if (strTypeCode.equals("D") || strTypeCode.equals("M")
						|| strTypeCode.equals("P"))
					strTempItemType = "M";
				else
					strTempItemType = strTypeCode;

			} else
				strTempItemType = strTypeCode;

			return strTempItemType;
		   
	   }
	   
	   /**
	    * Compute temp group code
	    */
	   
	   static String tempGroupCode(String strGroupCode)
	   {
			PreferenceUtils PrefObj = new PreferenceUtils();

			String strTempGroupCode;

			// GroupCode "2B,86(Present in the Pref)";
			int theScope = TCPreferenceService.TC_preference_site;

			boolean bPrefValuepresent = PrefObj.isPrefValueExist(strGroupCode,
					"NOV_Legacy_Item_Create_Tuboscope_Groups", theScope);

			if (bPrefValuepresent) {
				strTempGroupCode = "2B";
			} else {
				strTempGroupCode = strGroupCode;
			}

			return strTempGroupCode;
	   }
	   
	

}
