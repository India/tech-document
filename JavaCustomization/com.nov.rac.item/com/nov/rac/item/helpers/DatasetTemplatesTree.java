package com.nov.rac.item.helpers;

import java.util.HashMap;
import java.util.Map;

public class DatasetTemplatesTree
{
    public static TreeNode rootNode;
    
    public DatasetTemplatesTree()
    {
        rootNode = new TreeNode();
    }
    
    public DatasetTemplatesTree(String rootUID)
    {
        this();
        rootNode.uid = rootUID;
    }
    
    public TreeNode find(String theName)
    {
        return find(rootNode, theName);
    }
    
    public TreeNode find(TreeNode startNode, String theName)
    {
        // This function will search for the node at root and one level below.
        // It will not perform a full tree search.
        TreeNode theNode = null;
        
        // Check if given string matches with root or levels below.
        if (startNode.name.compareTo(theName) == 0)
        {
            return startNode;
        }
        else
        {
            theNode = startNode.childNodes.get(theName);
        }
        
        return theNode;
    }
    
    public static class TreeNode
    {
        public String name;
        public String type;
        public String uid;
        public String pUID;
        public String pName;
        
        // Store Name Vs TreeNode
        public Map<String, TreeNode> childNodes = new HashMap<String, DatasetTemplatesTree.TreeNode>();
        
        public TreeNode(String theName, String theType, String theUID, String thePUID, String thePName)
        {
            name = theName;
            type = theType;
            uid = theUID;
            pUID = thePUID;
            pName = thePName;
        }
        
        public TreeNode()
        {
            
        }
        
        public void setData(String theName, String theUID)
        {
            name = theName;
            uid = theUID;
        }
        
        public void add(TreeNode childNode)
        {
            if (childNode.pUID.equals(rootNode.uid))
            {
                childNodes.put(childNode.name, childNode);
            }
            else
            {
                addSubNode(childNodes, childNode);
            }
            
        }
        
        private void addSubNode(Map<String, TreeNode> childNodes, TreeNode childNode)
        {
            
            if (childNodes.containsKey(childNode.pName) && childNodes.get(childNode.pName).uid.equals(childNode.pUID))
            {
                
                childNodes.get(childNode.pName).childNodes.put(childNode.name, childNode);
            }
            else
            {
                for (String name : childNodes.keySet())
                {
                    if (!childNodes.get(name).childNodes.isEmpty())
                    {
                        addSubNode(childNodes.get(name).childNodes, childNode);
                    }
                }
            }
        }
    }
}
