package com.nov.rac.item.helpers;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.fieldassist.TextContentAdapter;

import com.teamcenter.rac.kernel.TCException;
 public class ClassificationHelper
    {
        private static Map<String, String> classificationMap = new HashMap<String, String>();
        private static Map<String, String> classificationUIDMap = new HashMap<String, String>();

        public static Map<String, String> getClassificationUIDMap()
        {
            return classificationUIDMap;
        }

        public static void setClassificationUIDMap(Map<String, String> classificationUIDMap)
        {
            ClassificationHelper.classificationUIDMap = classificationUIDMap;
        }

        public static Map<String, String> getClassificationMap()
        {
            return classificationMap;
        }

        public static void setClassificationMap(Map<String, String> classificationMap)
        {
            ClassificationHelper.classificationMap = classificationMap;
        }
        
        public static String[] createClassificationHierarchy(String rootClassName) throws TCException
        {

            ClassHierarchyCreator classhierarchycreator = new ClassHierarchyCreator();
            
                try
                {
                    classhierarchycreator.createStructure(rootClassName);
                }
                catch (Exception e)
                {
                    throw new TCException(e);
                }
             
           
            String[] cp_array = (String[])classhierarchycreator.getLinearStructure().toArray(new String[0]);
            String [] classID_array = (String[]) classhierarchycreator.getClassID().toArray(new String[0]);
            String[] classpuid_array = (String[]) classhierarchycreator.getClassPuid().toArray(new String[0]);
            Map<String, String> classiMap = new HashMap<String, String>();
            Map<String, String> classUidMap = new HashMap<String, String>();
           
            String[] classificationArray = new String[cp_array.length];
            for(int inx=0; inx < cp_array.length; inx++)
            {
               // m_classText.setData(cp_array[inx],classID_array[inx]);
                
               // ClassificationHelper clasHelper = new ClassificationHelper();
                if(cp_array[inx].contains("->"))
                {
                    classificationArray[inx] = cp_array[inx].substring(cp_array[inx].lastIndexOf(">")+1);
                }
                
                classiMap.put(classificationArray[inx],classID_array[inx]);
                classUidMap.put(classificationArray[inx], classpuid_array[inx]);
                
            }
            ClassificationHelper.setClassificationMap(classiMap);
            ClassificationHelper.setClassificationUIDMap(classUidMap);
            return cp_array;
        }

    }