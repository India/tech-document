package com.nov.rac.item.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

import com.nov.NationalOilwell;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinition;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinitionType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.Registry;

public class DatasetHelper
{
    
    public DatasetHelper(TCComponent dataset, String path) 
    {
        session = (TCSession) AIFUtility.getDefaultSession();
        registry = getRegistry();
        m_dataset = (TCComponentDataset) dataset;
        m_path = path;
//        try
//        {
//            updateDataset((TCComponentDataset) dataset, path);
//        }
//        catch (TCException exception)
//        {
//            MessageBox.post(exception);
//        }
    }
    
    Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    public void updateDataset() throws TCException
    {
        updateDataset(m_dataset,m_path);
    }
    private void updateDataset(TCComponentDataset dataset, String path) throws TCException 
    {
        
        File file = new File(path);
        String tempfiletype = dataset.getType();
        m_datasetFileAbsolutePath = file.getAbsolutePath();
        m_datasetType = dataset.getType();
//        if (!isNamedRefValid())
//        {
//            throw new TCException(getExtension() + registry.getString("Dataset.BADEXT") + m_datasetType);
//        }
//        else
//        {
            String[] filePathNames = new String[1];
            String[] fileTypes = new String[1];
            String[] fileSubTypes = new String[1];
            String[] namedReferences = new String[1];
            String dataSetNameS = dataset.getProperty("object_name");
            
            if (tempfiletype.equals(registry.getString("XML.NAME")))
            {
                fileTypes[0] = (registry.getString("Text.NAME"));
            }
            else
            {
                fileTypes[0] = tempfiletype;
            }
            
            fileSubTypes[0] = dataset.getSubType();
            File modNamedRefFile = getNamedRefFileSameAsDatasetName(file, path, dataSetNameS);
            
            namedReferences = getNamedRefOfDataSet(session, tempfiletype);
            filePathNames[0] = modNamedRefFile.getAbsolutePath();
            if (filePathNames[0] != null && fileTypes[0] != null && fileSubTypes[0] != null
                    && namedReferences[0] != null)
            {
                try
                {
                    dataset.setFiles(filePathNames, fileTypes, fileSubTypes, new String[] { namedReferences[namedReferences.length - 1] }, 20);
                }
                catch (TCException e)
                {
                    dataset.removeFiles(namedReferences[1]);
                    throw e ;
                }
            }
      //  }
    }
    
    public File getNamedRefFileSameAsDatasetName(File inputFile, String originalFilePath, String datasetName)
    {
        File outputFile = null;
        try
        {
            Registry clientReg = Registry.getRegistry("client_specific");            
          
            Pattern thePattern = Pattern.compile("[`\\@\\$\\&\\(\\{\\[\\|\\>\\\\\\:\\~\\!\\#\\^\\*\\]\\,\\}\\)\\<\\/]");
            datasetName = thePattern.matcher(datasetName).replaceAll(".");

            int originalFileNameStartIndex = originalFilePath.lastIndexOf(File.separator);
            String newFilePath = originalFilePath.substring(0, originalFileNameStartIndex);
            
            String originalFileExtn = originalFilePath.substring(originalFilePath.lastIndexOf("."));
            
            newFilePath = clientReg.getString((registry.getString("TCExportDir.NAME"))) + "\\" + datasetName
                    + originalFileExtn;
            
            outputFile = new File(newFilePath);
            if (outputFile.exists())
                outputFile.delete();
            
            copyFiles(inputFile, outputFile);
            
            System.out.println("Done....");
        }
        catch (FileNotFoundException e)
        {
            System.err.println("FileStreamsTest: " + e);
        }
        catch (IOException e)
        {
            System.err.println("FileStreamsTest: " + e);
        }
        
        return outputFile;
    }
    
    public void copyFiles(File inputFile, File outputFile) throws IOException
    {
        int BUFF_SIZE = 100000;
        byte[] buffer = new byte[BUFF_SIZE];
        InputStream in = null;
        OutputStream out = null;
        try
        {
            in = new FileInputStream(inputFile);
            out = new FileOutputStream(outputFile);
            while (true)
            {
                synchronized (buffer)
                {
                    int amountRead = in.read(buffer);
                    if (amountRead <= 0)
                    {
                        break;
                    }
                    out.write(buffer, 0, amountRead);
                }
            }
        }
        finally
        {
            if (in != null)
            {
                in.close();
            }
            if (out != null)
            {
                out.close();
            }
        }
    }
    
    private String[] getNamedRefOfDataSet(TCSession tcsession, String datasettype)
    {
        String namedRef[] = null;
        
        try
        {
            TCComponentDatasetDefinitionType tccompdatasetdefitype = (TCComponentDatasetDefinitionType) tcsession
                    .getTypeComponent(registry.getString("DatasetType.NAME"));
            
            TCComponentDatasetDefinition datasetdef = tccompdatasetdefitype.find(datasettype);
            
            namedRef = datasetdef.getNamedReferences();
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return namedRef;
    }
    
    public boolean isNamedRefValid() throws TCException
    {
        Boolean bOk = new Boolean(false);
        Object objs[] = new Object[2];
        objs[0] = getExtension(); /* pszExt */
        objs[1] = m_datasetType; /* pszDatasetType */
        try
        {
            TCUserService l_service = session.getUserService();
            bOk = (Boolean) l_service.call(NationalOilwell.NR_USERSERVICE, objs);
        }
        catch (TCException ex)
        {
        }
        return bOk.booleanValue();
    }
    
    public String getExtension()
    {
        int dot = m_datasetFileAbsolutePath.lastIndexOf('.');
        int len = m_datasetFileAbsolutePath.length();
        if (dot > 0 && dot < len)
            return m_datasetFileAbsolutePath.substring(dot);
        else
            return "";
    }
    
    private String m_datasetFileAbsolutePath = "";
    private String m_datasetType = "";
    private Registry registry = null;
    private TCSession session = null;
    private TCComponentDataset m_dataset = null;
    private String m_path = "";
    
}
