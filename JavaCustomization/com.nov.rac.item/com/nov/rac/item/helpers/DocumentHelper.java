package com.nov.rac.item.helpers;

import com.nov.NationalOilwell;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class DocumentHelper
{
    
    public static TCComponentListOfValues getDocumentTypes(String docCategorySelected) throws TCException

    {
        
        TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
        
        TCComponentListOfValues m_docTypes = TCComponentListOfValuesType.findLOVByName(session, docCategorySelected
                + NationalOilwell.SUBTYPE_LOV);
        //System.out.println("Lov Name is  " + docCategorySelected + NationalOilwell.SUBTYPE_LOV);
        return m_docTypes;
        
    }
    
    public static TCComponentListOfValues getDocCategory(String inputLOV)
    {
        
        TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
        
        TCComponentListOfValues m_docCategory = TCComponentListOfValuesType.findLOVByName(session,inputLOV);
       
        return m_docCategory;
        
    }
    
    public static boolean hasContent(String string)
    {
        if (null == string || string.isEmpty() || "".equals(string.trim()))
            return false;
        return true;
    }

    public static boolean hasContent(TCComponent tag)
    {
        if (null == tag )
            return false;
        return true;
    }
    
}