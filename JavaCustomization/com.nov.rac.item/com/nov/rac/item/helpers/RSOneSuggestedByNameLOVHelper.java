package com.nov.rac.item.helpers;

import java.util.Hashtable;
import java.util.Vector;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ArraySorter;

public class RSOneSuggestedByNameLOVHelper
{

    public static void init(TCSession s,String query, String[] qryParms, String[] qryParmVals,String attName) {
        
        try {
            TCComponentQueryType qt = 
                (TCComponentQueryType)s.getTypeComponent("ImanQuery");
            TCComponentQuery gen = (TCComponentQuery)qt.find(query);
            if (gen != null) {
                TCComponent[] arr = gen.execute(qryParms,qryParmVals);
                if (arr.length > 0) {
                    ArraySorter.sort(arr, true);
                    for(int index =0;index<arr.length;index++)
                        orderedVector.addElement(arr[index]);
                    populateHashtables(orderedVector,attName);
                }
                else
                    System.out.println("AbstractReferenceLOVList: No values found for query \""+query+"\"\n");
            }            
        }
        catch (Exception e) {
            System.out.println("Could not retrieve values for AbstractReferenceLOVList: "+e);
        }
    }
    
    private static void populateHashtables(Vector<TCComponent> vec,String attName) {
        
        try {
            for (int i=0;i<vec.size();i++) {
                TCComponent comp = (TCComponent)vec.get(i);
                String val = comp.getProperty(attName);
                refToString.put(comp,val);
                stringToRef.put(val,comp);
            }
        }
        catch (Exception e) {
            System.out.println("Could not populate AbstractReferenceLOVList hashtables: "+e);
        }
        
    }
    
    public static String[] getSuggestedNameValues(TCSession s ,String query, String[] qryParms, String[] qryParmVals,String attName)
    {
        refToString.clear();
        init(s,query,qryParms,qryParmVals,attName);
        String[] suggestedNameValues = new String[refToString.size()];
        suggestedNameValues = refToString.values().toArray(suggestedNameValues);
        return suggestedNameValues;
    }
    
   private static Vector<TCComponent>    orderedVector = new Vector<TCComponent>();
   static Hashtable<String,Object> stringToRef = new Hashtable<String,Object>();
   static Hashtable<Object,String> refToString = new Hashtable<Object,String>();
    
}
