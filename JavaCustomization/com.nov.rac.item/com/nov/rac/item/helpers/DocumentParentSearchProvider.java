package com.nov.rac.item.helpers;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.kernel.TCComponent;

public class DocumentParentSearchProvider  implements INOVSearchProvider2
{
    
    @Override
    public String getBusinessObject()
    {
        return null;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        QuickBindVariable[] bindVariables = new QuickBindVariable[2];
        
        // Relation type
        bindVariables[0] = new QuickBindVariable();
        bindVariables[0].nVarType = POM_string;
        bindVariables[0].nVarSize = 1;
        bindVariables[0].strList = new String[] { NationalOilwell.RELATED_DOCUMENT};
        
        // Selected RDD
        bindVariables[1] = new QuickBindVariable();
        bindVariables[1].nVarType = POM_typed_reference;
        bindVariables[1].nVarSize = 1;
        bindVariables[1].strList = new String[] { getSelectedRddUid() };
        
        return bindVariables;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickHandlerInfo[] allHandlerInfo = new QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-partfamily";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1 };
        
        return allHandlerInfo;
    }
    
    private String getSelectedRddUid()
    {
        String puid = null;
        
        TCComponent rdd = NewItemCreationHelper.getTargetComponent();
        
        if (rdd != null)
        {
            puid = rdd.getUid();
        }
        return puid;
    }
    
    @Override
    public boolean validateInput()
    {
        boolean isValid = true;
        
        String puidOfRDD = getSelectedRddUid();
        
        if (puidOfRDD == null)
        {
            isValid = false;
        }
        return isValid;
    }
    
    public INOVResultSet executeQuery(INOVSearchProvider theSearchProvider)
    {
        INOVSearchProvider m_searchService = theSearchProvider;// new PartFamilySearchProvider();
        INOVResultSet resultSet = null;
        
        if (m_searchService != null)
        {
            if ((m_searchService instanceof INOVSearchProvider2)
                    && (((INOVSearchProvider2) m_searchService).validateInput() == true))
            {
                
                INOVSearchResult searchResult;
                
                try
                {
                    searchResult = NOVSearchExecuteHelperUtils.execute(m_searchService, INOVSearchProvider.LOAD_ALL);
                    resultSet = searchResult.getResultSet();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            
        }
        return resultSet;
    }
    
}
