package com.nov.rac.item.helpers;


import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Vector;

import org.eclipse.core.commands.ExecutionException;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class ClassHierarchyCreator 
{
	public final Object createStructure(String rootClassId) throws ExecutionException, TCException, MissingResourceException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException
	{
		System.out.println("In classifiction");
		SimplePropertyMap dummyMap = new SimplePropertyMap();
		
		INOVSearchProvider searchService = (INOVSearchProvider) new ClassHierarchySearchProvider(dummyMap);
		
		try 
		{	
			
			ClassTreeNode nextElementNode;
			Date now1 = new Date ( );
			String rootClassName = null;
			
			System.out.println ("classification string start:"+
		    		DateFormat.getDateTimeInstance ( DateFormat.LONG, DateFormat.LONG ).format(now1) );		
			INOVSearchResult searchResult = NOVSearchExecuteHelperUtils.searchSOACallServer(searchService,INOVSearchProvider.LOAD_ALL);
			createAllMaps(searchResult);
			m_valueNodeMap = createValueNode(m_childIdParentId,m_classIdName);
		
			//rootClassId = getRoot(m_childIdParentId);
			//rootClassId = getRoot(rootClassName,m_classIdName);
			
			rootClassName = getClassName(rootClassId,m_classIdName);
			
			nextElementNode = m_valueNodeMap.get(rootClassId);
			m_linearStructure = appendString(m_linearStructure, rootClassName,nextElementNode,m_classIDCFlag);
	
			Date now2 = new Date ( );
			System.out.println ("classification string end:"+
		    		DateFormat.getDateTimeInstance ( DateFormat.LONG, DateFormat.LONG ).format(now2) );		
			
		}
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public final String getClassName(String rootClassId,final Map<String, String> classIDName)
	{
		
        String strVal = null;
        for(Map.Entry entry: classIDName.entrySet()){
           if(rootClassId.equals(entry.getKey())){
        	   strVal = (String) entry.getValue();
               break;
           }
       }
        return strVal;
	}
	
	public final Map<String, ClassTreeNode> createValueNode(final Map<String, String> childIDParentIDMap,final Map<String,String> classIDNameMap) 
	{
		 	Map<String, ClassTreeNode> valueNodeMap = new HashMap<String, ClassTreeNode>();
			Collection<String> children = childIDParentIDMap.keySet();
			
		    // Cloning here as any changes made to values will directly affect the map
			String rootVal = getRoot(childIDParentIDMap);
		    ClassTreeNode root = new ClassTreeNode(rootVal);
		    
		    // Put the root node into value map as it will not be present in the list of children
		    valueNodeMap.put(rootVal, root);
		    
		    // Populate all children into valueNode map
		    for (String child : children) {
		      ClassTreeNode valueNode = new ClassTreeNode(child);
		      valueNodeMap.put(child, valueNode);

		    }

		    // Now the map contains all nodes in the tree. Iterate through all the children and associate them with their parents
		    for (String child : children) {
		      ClassTreeNode childNode = valueNodeMap.get(child);
		      String parent = childIDParentIDMap.get(child);
		      
		      ClassTreeNode parentNode = valueNodeMap.get(parent);
		      
		      if(parentNode != null)
		      	parentNode.getChildrenList().add(childNode);
		      
		    }
		    
		    return valueNodeMap;
		   
	 } 
	public final List<String> appendString(List<String> result,final String stringToAppend, final ClassTreeNode element,final Map<String,String> classCFlagMap)
	 {
		String currentChild = null;
		for(Iterator<ClassTreeNode> itr = element.getChildrenList().iterator();itr.hasNext();)
		{
			int storageClassCFlag = 16;
			String resultString = null;
			ClassTreeNode nextNode = itr.next();
			currentChild = m_classIdName.get(nextNode.getValue()); 
			resultString = stringToAppend + "->" + currentChild;
			if(!nextNode.getChildrenList().isEmpty() && (Integer.parseInt(classCFlagMap.get(nextNode.getValue())) & storageClassCFlag)==0)
			{
				result = appendString(result, resultString, nextNode,classCFlagMap);
			}
			else 
			{		
				if((Integer.parseInt(classCFlagMap.get(nextNode.getValue())) & storageClassCFlag)!=0)
				{
					result.add(resultString);
					classID.add(nextNode.getValue());
					classPuid.add(m_classIDPuid.get(nextNode.getValue()));
				}
				
				if(!nextNode.getChildrenList().isEmpty()) {
					result = appendString(result, resultString, nextNode,classCFlagMap);
				}
				
			}
			
			 //System.out.println(resultString);
	     }
		return result;
	 }
	public final void createAllMaps(final INOVSearchResult searchResult) throws TCException 
	{
		m_rowCount = searchResult.getResultSet().getRows();
		m_colCount = searchResult.getResultSet().getCols();
		m_rowData = searchResult.getResultSet().getRowData();
		
    	Locale locale = Locale.FRANCE;
    	String currentLanguage;
    	currentLanguage = locale.toString();
    	
		
		for(int i = 0; i<m_rowCount; i++)
		{
			StringBuffer strBuf = new StringBuffer();
			
			StringBuffer strBufParent = new StringBuffer();
			
			int requiredRow = i*m_colCount;
			
			//for(int j = 0; j<m_colCount;j++)
			{
				/*m_childIdParentId.put(m_rowData.get(requiredRow + ), m_rowData.get((requiredRow +j)+3));
				m_classIDCFlag.put(m_rowData.get((requiredRow +j) + 2 ), m_rowData.get((requiredRow +j)+4));
				m_classIDPuid.put(m_rowData.get((requiredRow+j) + 2),m_rowData.get(requiredRow));
				m_classIdName.put(m_rowData.get((requiredRow +j) + 2 ), m_rowData.get((requiredRow +j)+0));
				m_classIdName.put(m_rowData.get((requiredRow +j) + 3 ), m_rowData.get((requiredRow +j)+1));
				*/
				
				m_childIdParentId.put(m_rowData.get((requiredRow) + 1 ), m_rowData.get((requiredRow)+ 4));
				m_classIDCFlag.put(m_rowData.get((requiredRow) + 1 ), m_rowData.get((requiredRow)+ 3));
				m_classIDPuid.put(m_rowData.get((requiredRow) + 1),m_rowData.get(requiredRow));
				
				//parent
				if(m_classIdName.containsKey(m_rowData.get((requiredRow) + 4 )))
				{
					if(m_rowData.get((requiredRow)+8).equals(currentLanguage))
					{
						m_classIdName.put(m_rowData.get((requiredRow) + 4 ), m_rowData.get((requiredRow)+5));
					}
				}
				else
				{
					m_classIdName.put(m_rowData.get((requiredRow) + 4 ), m_rowData.get((requiredRow)+5));
				}

				//chils
				if(m_classIdName.containsKey(m_rowData.get((requiredRow) + 1 )))
				{
					if(m_rowData.get((requiredRow)+7).equals(currentLanguage))
					{
						m_classIdName.put(m_rowData.get((requiredRow) + 1 ), m_rowData.get((requiredRow)+2));
					}
				}
				else
				{
					m_classIdName.put(m_rowData.get((requiredRow) + 1 ), m_rowData.get((requiredRow)+2));
				}



					if(m_rowData.get((requiredRow)+7).equals(currentLanguage))
					{
						m_classIDNameLocale.put(m_rowData.get((requiredRow) + 1),  m_rowData.get((requiredRow)+2));
					}
					
					if( m_rowData.get((requiredRow)+8).equals(currentLanguage))
					{
						m_classIDNameLocale.put(m_rowData.get((requiredRow) + 4),  m_rowData.get((requiredRow)+5));
					}

				//strBuf.append(m_rowData.get((requiredRow) + 1 )).append(" ").append(m_rowData.get((requiredRow)+2));
				//System.out.println(strBuf);


				
				//strBufParent.append(m_rowData.get((requiredRow) + 4 )).append(" ").append(m_rowData.get((requiredRow)+5));
				//System.out.println(strBufParent);			

				//strBuf.append(m_rowData.get(requiredRow + j)).append(" ");
				//break;
			}
			//System.out.println(strBuf);
			
		}
		
		//printMap();
		
	}
	private void printMap()
	{

        for(Map.Entry entry: m_classIdName.entrySet()){
        	StringBuffer strBuf = new StringBuffer();
        	
        	strBuf.append(entry.getKey()).append(" ").append(entry.getValue());
                System.out.println(strBuf);

        }		
	}
	
	public final String getRoot(final Map<String, String> childParentMap)
	 {
		 Collection<String> children = childParentMap.keySet();
		 Collection<String> values = childParentMap.values();
		 Collection<String> clonedValues = new HashSet<String>();
		 for (String value : values) {
			 clonedValues.add(value);
		 }
		 clonedValues.removeAll(children);
		 String rootValue = clonedValues.iterator().next();
		 return rootValue;
	 }	
	
	public final String getRoot(String rootClassName,final Map<String, String> classIDName)
	{
		
        String strKey = null;
        for(Map.Entry entry: classIDName.entrySet()){
           if(rootClassName.equals(entry.getValue())){
               strKey = (String) entry.getKey();
               break; //breaking because its one to one map
           }
       }
        return strKey;
	}


	public List<String> getLinearStructure() {
		return m_linearStructure;
	}

	
	public  List<String> getClassID() {
		return classID;
	}


	public  List<String> getClassPuid() {
		return classPuid;
	}
	
	private Map<String, ClassTreeNode> m_valueNodeMap;
	private List<String> m_linearStructure = new LinkedList<String>();
	
	private Map<String, String> m_classIDCFlag = new HashMap<String, String>();
	private Map<String, String> m_classIdName = new HashMap<String, String>();
	private Map<String, String> m_childIdParentId = new HashMap<String, String>();
	private Map<String, String> m_classIDPuid = new HashMap<String, String>();
	
	private Map<String, String> m_classIDNameLocale = new HashMap<String, String>();
	
	private List<String> classID = new LinkedList<String>();
	private List<String> classPuid = new LinkedList<String>();

	int m_rowCount;
	
	int m_colCount;
	Vector<String> m_rowData;
}
