package com.nov.rac.item.helpers;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.utilities.manufacturerhelper.NOVManufacturerHelper;

public class ManufacturerHelper
{
    public static Vector<String> getActiveManifacturersVector()
    {
        NOVManufacturerHelper novManufacturerHelper = new NOVManufacturerHelper();
       
        novManufacturerHelper.setMfgStatus("Y");
        novManufacturerHelper.setMfgID("*");
        INOVResultSet theResSet = null;
        try
        {
            theResSet = novManufacturerHelper.execute(INOVSearchProvider.INIT_LOAD_ALL);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        Vector<String> rowData = theResSet.getRowData();
        Vector<String> objectNames = new Vector<String>();
        
        final int colCount = theResSet.getCols();
        
        for (int i = 0; i < theResSet.getRows(); i++)
        {
            objectNames.add(rowData.elementAt((colCount * i) + 1));
        }
        
        return objectNames;
    }
    
    public static String[] getActiveManifacturersArray()
    {
        Vector<String> vManufacturerNames = getActiveManifacturersVector();
        
        String[] aManufacturerNames  = vManufacturerNames.toArray(new String[vManufacturerNames.size()]);
        
        return aManufacturerNames;
    }
}

