package com.nov.rac.item.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NOVRSOneRevisionIDHelper
{

    public static String getOracleId()
    {
        String timeStamp = new SimpleDateFormat("MMddHHmmss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }
     
}
