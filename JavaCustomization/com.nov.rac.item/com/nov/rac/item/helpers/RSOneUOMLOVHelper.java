package com.nov.rac.item.helpers;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

import com.nov.NationalOilwell;
import com.nov.rac.utilities.utils.LOVUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RSOneUOMLOVHelper
{
    
    public static Vector<String> getUOMLOVValues()
    {
        String sUOMLOVName = NationalOilwell.UOMLOV;
        if (getSession() != null)
        {
            try
            {
                uomvector.clear();
                
                Object uomObjs[] = null;
                
                TCComponentListOfValuesType lovType = (TCComponentListOfValuesType) getSession().getTypeComponent(
                        "ListOfValues");
                TCComponentListOfValues uomLOVs[] = lovType.find(sUOMLOVName);
                if (uomLOVs != null && uomLOVs.length > 0)
                {
                    uomObjs = uomLOVs[0].getListOfValues().getListOfValues();
                    
                    //uomvector.add(0, "");
                    
                    String desc[] = uomLOVs[0].getListOfValues().getDescriptions();
                    if (desc != null && desc.length > 0)
                    {
                        int noOfUOms = desc.length;
                        
                        checkLoggedInGroup();
                        
                        setUOMVector(uomObjs, desc, noOfUOms, flag, isSTPGrp);
                    }
                    
                }
                else
                    throw (new TCException(getRegistry().getString("UOMLOVMissing.ERROR")));
                
            }
            catch (TCException ex)
            {
                ex.printStackTrace();
            }
        }
        if (uomvector.isEmpty())
        {
            uomvector.add(0, "");
        }
        
        return uomvector;
        
    }

    private static void checkLoggedInGroup()
    {
        String loggedgrp = getSession().getCurrentGroup().toString();
        
        TCPreferenceService prefServ = getSession().getPreferenceService();
       /* String[] grpStrArray = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
                getRegistry().getString("pcgPref.NAME"));*/
        String[] grpStrArray = prefServ.getStringValues(getRegistry().getString("pcgPref.NAME"));
        
       /* String[] RSOneJDEGrps = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
                getRegistry().getString("RSoneJDEUOMPref.NAME"));*/
        String[] RSOneJDEGrps = prefServ.getStringValues(getRegistry().getString("RSoneJDEUOMPref.NAME"));
        flag = false;
        isSTPGrp = false;
        
        for (int inx = 0; grpStrArray != null && inx < grpStrArray.length; inx++)
        {
            if (loggedgrp.equals(grpStrArray[inx]))
            {
                flag = true;
                break;
            }
        }
        
        if (Arrays.asList(RSOneJDEGrps).contains(loggedgrp))
        {
            isSTPGrp = true;
        }
    }

    private static void setUOMVector(Object[] uomObjs, String[] desc, int noOfUOms, boolean flag, boolean isSTPGrp)
    {
        for (int index = 0, cnt = -1; index < noOfUOms; index++)
        {
            if (isSTPGrp == true)
            {
                if (desc[index].indexOf(getRegistry().getString("STPUOM.NAME")) != -1)
                {
                    cnt++;
                    uomvector.add(cnt, uomObjs[index].toString());
                }
            }
            else
            {
                if ((flag == false)
                        && ((desc[index].indexOf(getRegistry().getString("RSONE.NAME")) != -1) || (desc[index]
                                .trim().length() == 0)))
                {
                    cnt++;
                    uomvector.add(cnt, uomObjs[index].toString());
                    
                }
                if ((flag == true)
                        && ((desc[index].indexOf(getRegistry().getString("WLY.NAME")) != -1) || (desc[index]
                                .trim().length() == 0)))
                {
                    cnt++;
                    uomvector.add(cnt, uomObjs[index].toString());
                    
                }
            }
        }
    }
    
    public static Vector<String> getRSOneOrgLOVValues()
    {
        Vector<String> vector = new Vector<String>();
        
        try
        {
            TCComponentListOfValues values = LOVUtils.getLOVComponent(NationalOilwell.RSOne_ORG_LOV, getSession());
            
            String[] orgValues = LOVUtils.getLovStringValues(values);
            
            Collections.addAll(vector, orgValues);
            
            vector.add(0, "");
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return vector;
    }

    
    private static Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    protected static TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
        
    }
    
    private static Vector<String> uomvector = new Vector<String>();
    private static boolean flag;
    private static boolean isSTPGrp;

}
