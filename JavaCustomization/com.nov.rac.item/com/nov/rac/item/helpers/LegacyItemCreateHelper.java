package com.nov.rac.item.helpers;

import com.nov.NationalOilwell;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;

public class LegacyItemCreateHelper
{   
    public static TCComponentListOfValues getItemType()
    {
        
        TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
        
        TCComponentListOfValues m_itemType = TCComponentListOfValuesType.findLOVByName(session,
                NationalOilwell.ITEMTYPELOV);
        return m_itemType;
        
    }
    
    public static boolean hasContent(String string)
    {
        if (null == string || string.isEmpty() || "".equals(string.trim()))
            return false;
        return true;
    }

    public static boolean hasContent(TCComponent tag)
    {
        if (null == tag )
            return false;
        return true;
    }

    public static boolean isRDRDDGroup()
    {
        return PreferenceHelper.isGroupExistInPref(NewItemCreationHelper.getCurrentGroup(), "NOV_RDRDD_GROUPS", TCPreferenceLocation.OVERLAY_LOCATION);
    }
    
    public static boolean isDualItemCreationGroup()
    {
        return PreferenceHelper.isGroupExistInPref(NewItemCreationHelper.getCurrentGroup(), NationalOilwell.PREF_NOV_RS_DUAL_ITEM_CREATION_GROUPS, TCPreferenceLocation.OVERLAY_LOCATION);
    }
    
    public static boolean isLegacyWithRDGroup()
    {
        return PreferenceHelper.isGroupExistInPref(NewItemCreationHelper.getCurrentGroup(), "NOV_Legacy_Legacy_Item_Create_With_RD", TCPreferenceLocation.OVERLAY_LOCATION);
    }
    
    public static boolean isTuboscope()
    {
        String groupName = getShortGroupName(NewItemCreationHelper.getCurrentGroup());
        return PreferenceHelper.isGroupExistInPref(groupName, "NOV_Legacy_Item_Create_Tuboscope_Groups", TCPreferenceLocation.OVERLAY_LOCATION);
    }

	public static boolean isUOMMandatoryGroup() 
	{
		return PreferenceHelper.isGroupExistInPref(NewItemCreationHelper.getCurrentGroup(), "NOV_Legacy_Mandatory_UOM_Groups", TCPreferenceLocation.OVERLAY_LOCATION);
	}

	public static boolean isENGStatus() 
	{
		return PreferenceHelper.isGroupExistInPref(NewItemCreationHelper.getCurrentGroup(), "NOV_LegacyItemCreate_ENG_Status", TCPreferenceLocation.OVERLAY_LOCATION);
	}
	
    public static String getSelectedRelation()
    {
        return selectedRelation;
    }

    public static void setSelectedRelation(String selectedRelation)
    {
        LegacyItemCreateHelper.selectedRelation = selectedRelation;
    }
    
    private static String getShortGroupName(String groupName)
    {
        String returnValue = "";
        
        int index = groupName.indexOf("-");
        if(index > -1)
        {
            String allGroups = (String) groupName.subSequence(0, index);
            if(allGroups != null)
            {
               returnValue = allGroups.trim();
            }
        }
       
         return returnValue;
    }
	
	private static String selectedRelation = null;

}