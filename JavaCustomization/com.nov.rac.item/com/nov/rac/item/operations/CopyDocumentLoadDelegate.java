package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class CopyDocumentLoadDelegate extends AbstractLoadDelegate
{
    
    public CopyDocumentLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.item.operations.ILoadDelegate#loadComponent(com.teamcenter
     * .rac.kernel.TCComponent)
     */

    public void loadComponent(TCComponent targetObject) throws TCException
    {
        TCComponentItem targetItem = null;
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        if (null != targetItem)
        {
            
            IPropertyMap documentPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetItem, new String[] { "item_id", "object_name", "object_desc",
                    NationalOilwell.DOCUMENTS_CATEGORY, NationalOilwell.DOCUMENTS_TYPE }, documentPropertyMap);
            
            TCComponent docMasterForm = targetItem.getRelatedComponent("IMAN_master_form");
            
            IPropertyMap docMasterFormPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(docMasterForm, new String[] { NationalOilwell.PULL_DRAWING },
                    docMasterFormPropertyMap);
            
            documentPropertyMap.setCompound("IMAN_master_form", docMasterFormPropertyMap);
            
            IPropertyMap datasetPropertyMap = new SimplePropertyMap();
            TCComponent latestDocRevision = targetItem.getLatestItemRevision();
            TCComponent[] datasetList = latestDocRevision.getRelatedComponents("IMAN_specification");
            
            if (null != datasetList && 0 < datasetList.length)
            {
                for (TCComponent dataset : datasetList)
                {
                    if (dataset instanceof TCComponentDataset)
                    {
                        PropertyMapHelper.componentToMap(dataset, new String[] { "object_type" }, datasetPropertyMap);
                        break;
                    }
                }
            }
            
            loadPanelsData(targetItem.getType(), getInputProviders(), documentPropertyMap);
            
            //TCDECREL-7122 Commented if condition to update document data even if it doen't have dataset
            //if (!datasetPropertyMap.isEmpty())
            //{
                loadPanelsData(NationalOilwell.DATASET_BO_TYPE, getInputProviders(), datasetPropertyMap);
            //}
        }
    }

    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent( (TCComponent) inputObject);
    }
}
