package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class CreateLegacyPartLoadDelegate extends AbstractLoadDelegate
{

    public CreateLegacyPartLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }

    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        if(inputObject != null && inputObject instanceof IPropertyMap)
        {
            loadPanelsData(NationalOilwell.PART_BO_TYPE, getInputProviders(), (IPropertyMap)inputObject);
            loadPanelsData(NationalOilwell.DOCUMENT_TYPE, getInputProviders(), (IPropertyMap)inputObject);
            loadPanelsData(NationalOilwell.DATASET_BO_TYPE, getInputProviders(), (IPropertyMap)inputObject);
        }
    }
    
}
