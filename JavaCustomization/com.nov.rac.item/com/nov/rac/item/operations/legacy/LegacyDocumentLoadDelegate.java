package com.nov.rac.item.operations.legacy;

import java.util.List;
import java.util.Vector;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.item.helpers.DocumentParentSearchProvider;
import com.nov.rac.item.operations.AbstractLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class LegacyDocumentLoadDelegate extends AbstractLoadDelegate
{
    
    public LegacyDocumentLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent((TCComponent) inputObject);
    }
    
    public void loadComponent(TCComponent targetObject) throws TCException
    {
        
        TCComponentItem targetItem = null;
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        if (null != targetItem)
        {
            String itemId = targetItem.getProperty(NationalOilwell.ITEM_ID);
            INOVResultSet result = null;
            
            DocumentParentSearchProvider searchService = new DocumentParentSearchProvider();
            result = searchService.executeQuery(searchService);
            
            if (result != null && result.getRows() > 0) // Document is attached
                                                        // to some part then load the part's data
            {
                Vector<String> rowData = result.getRowData();
                
                String puid = rowData.get(0);
                
                TCSession tcSession = (TCSession) AIFUtility
                        .getDefaultSession();
                
                TCComponent tcComponentRev = tcSession.stringToComponent(puid);
                
                TCComponent tcComponent = ((TCComponentItemRevision) tcComponentRev)
                        .getItem();
                
                IPropertyMap partPropertyMap = new SimplePropertyMap();
                PropertyMapHelper.componentToMap(tcComponent, 
                                                 new String[] { NationalOilwell.OBJECT_TYPE, 
                                                                NationalOilwell.NOV_UOM,
                                                                NationalOilwell.OBJECT_NAME,
                                                                NationalOilwell.OBJECT_DESC }, 
                                                 partPropertyMap
                                                );
                
                IPropertyMap masterFormPropertyMap = new SimplePropertyMap();
                TCComponent masterForm = tcComponent
                        .getRelatedComponent("IMAN_master_form");
                String[] properties = getRegistry().getStringArray(
                        tcComponent.getType() + ".loadProperties");
                if (properties != null)
                {
                    PropertyMapHelper.componentToMap(masterForm, 
                                                     properties,
                                                     masterFormPropertyMap
                                                    );
                    
                }
                
                partPropertyMap.setCompound("IMAN_master_form",
                        masterFormPropertyMap);
                IPropertyMap documentPropertyMap = new SimplePropertyMap();
                IPropertyMap datasetPropertyMap = new SimplePropertyMap();
                
                PropertyMapHelper.componentToMap(targetItem, 
                                                 new String[] { NationalOilwell.DOCUMENTS_CATEGORY,
                                                                NationalOilwell.DOCUMENTS_TYPE }, 
                                                 documentPropertyMap
                                                );
                
                IPropertyMap docMasterFormPropertyMap = new SimplePropertyMap();
                TCComponent docMasterForm = targetItem
                        .getRelatedComponent("IMAN_master_form");
                PropertyMapHelper.componentToMap(docMasterForm,
                                                 new String[] { NationalOilwell.SEQUENCE },
                                                 docMasterFormPropertyMap
                                                );
                
                documentPropertyMap.setCompound("IMAN_master_form",
                        docMasterFormPropertyMap);
                TCComponent[] docRevisionList = targetItem
                        .getRelatedComponents("revision_list");
                TCComponent[] datasetList = docRevisionList[0]
                        .getRelatedComponents("IMAN_specification");
                
                if (null != datasetList && 0 < datasetList.length)
                {
                    PropertyMapHelper.componentToMap(datasetList[0],
                                                     new String[] { NationalOilwell.OBJECT_TYPE },
                                                     datasetPropertyMap
                                                    );
                }
                
                loadPanelsData(tcComponent.getType(), getInputProviders(),
                        partPropertyMap);
                
                if (!documentPropertyMap.isEmpty())
                {
                    loadPanelsData("DocumentsWithPart", getInputProviders(),
                            documentPropertyMap);
                }
                
                if (!datasetPropertyMap.isEmpty())
                {
                    loadPanelsData(NationalOilwell.DATASET_BO_TYPE,
                            getInputProviders(), datasetPropertyMap);
                }
                
            }
            else   // Document is not attached to any part then load the document's data
            {
                IPropertyMap documentPropertyMap = new SimplePropertyMap();
                PropertyMapHelper.componentToMap(targetItem, 
                                                 new String[] {NationalOilwell.OBJECT_TYPE,
                                                               NationalOilwell.OBJECT_NAME, 
                                                               NationalOilwell.ITEM_ID,
                                                               NationalOilwell.DOCUMENTS_CATEGORY,
                                                               NationalOilwell.DOCUMENTS_TYPE }, 
                                                 documentPropertyMap
                                                );
                
                IPropertyMap masterFormPropertyMap = new SimplePropertyMap();
                TCComponent masterForm = targetItem
                        .getRelatedComponent("IMAN_master_form");
                PropertyMapHelper.componentToMap(masterForm,
                        new String[] { NationalOilwell.SEQUENCE,
                                NationalOilwell.BASE_NUMBER },
                        masterFormPropertyMap);
                
                documentPropertyMap.setCompound("IMAN_master_form",
                        masterFormPropertyMap);
                
                IPropertyMap datasetPropertyMap = new SimplePropertyMap();
                
                TCComponent latestDocRevision = targetItem
                        .getLatestItemRevision();
                TCComponent[] datasetList = latestDocRevision
                        .getRelatedComponents("IMAN_specification");
                
                if (null != datasetList && 0 < datasetList.length)
                {
                    for (TCComponent dataset : datasetList)
                    {
                        if (dataset instanceof TCComponentDataset)
                        {
                            PropertyMapHelper.componentToMap(dataset,
                                    new String[] { "object_type" },
                                    datasetPropertyMap);
                            break;
                        }
                    }
                }
                
                loadPanelsData(targetItem.getType(), getInputProviders(),
                        documentPropertyMap);
                
                if (!datasetPropertyMap.isEmpty())
                {
                    loadPanelsData(NationalOilwell.DATASET_BO_TYPE,
                            getInputProviders(), datasetPropertyMap);
                }
            }
            
        }
        
    }
    
    private Registry getRegistry()
    {
        m_registry = Registry
                .getRegistry("com.nov.rac.item.operations.legacy.legacy");
        
        return m_registry;
    }
    
    private Registry m_registry;
}
