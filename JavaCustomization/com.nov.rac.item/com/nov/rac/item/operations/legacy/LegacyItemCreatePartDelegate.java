package com.nov.rac.item.operations.legacy;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.AbstractOperationDelegate;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.item.operations.propertyValidators.ValidationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LegacyItemCreatePartDelegate extends AbstractOperationDelegate
{
    public LegacyItemCreatePartDelegate(Shell theShell, String objectType, String datasetType, boolean isLegacyItem)
    {
        super(theShell);
        
        m_objectType = objectType;
        m_datasetBusinessObjectName = datasetType;
        
        m_isLegacySelected = isLegacyItem;
        m_registry = getRegistry();
        
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry(this);
    }
    
    public void setObjectType(String objectType)
    {
        m_objectType = objectType;
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        m_shouldProcessDataset = true;
        m_shouldProcessDocument = true;
        m_partPropertyMap = new SOAPropertyMap(m_objectType);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(
                m_partPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(m_objectType, getOperationInputProviders(),
                m_partPropertyMap);
        
        IPropertyModifier partPropertiesModifier = getModifier(m_objectType);
        partPropertiesModifier.modifyProperties(m_partPropertyMap);
        
        m_documentPropertyMap = new SOAPropertyMap(
                NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper documentCompoundHelper = new CreateInCompoundHelper(
                m_documentPropertyMap);
        documentCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        documentCompoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE,
                getOperationInputProviders(), m_documentPropertyMap);
        
        m_datasetAllPropertyMap = new SOAPropertyMap(
                m_datasetBusinessObjectName);
        
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE,
                getOperationInputProviders(), m_datasetAllPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = { m_partPropertyMap,
                m_documentPropertyMap, m_datasetAllPropertyMap };
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(
                getRegistry(), NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        if (!isDocumentContainsRequiredFields(m_documentPropertyMap))
        {
            m_shouldProcessDocument = false;
        }
        else
        {
            IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
            docPropertiesModifier.modifyProperties(m_documentPropertyMap);
        }
        
        if (!isDatasetContainsRequiredFields(m_datasetAllPropertyMap))
        {
            m_shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(m_documentPropertyMap, m_datasetAllPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(m_datasetAllPropertyMap);
        }
        
        return true;
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        m_relType = LegacyItemCreateHelper.getSelectedRelation();
        boolean shouldApplyStatus = LegacyItemCreateHelper.isENGStatus();
        
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        
        TCComponentItem selectedItem = (TCComponentItem) m_partPropertyMap.getComponent();
        IPropertyMap revisionPropertyMap = m_partPropertyMap
                .getCompound("revision");
        String revId = revisionPropertyMap
                .getString(NationalOilwell.ITEMREVISIONID);
        
        m_revisionItem = ValidationHelper.getRev(selectedItem , revId); //m_revisionItem = null if part doesn't exist
                                                                        //m_revisionItem = latestRev if revision doesn't exist
                                                                        //m_revisionItem = revision if revision exist
        
        if (selectedItem == null)                       // selected item = null if part exists
        {
            processPart(shouldApplyStatus, createInputMap);
            
        }
        else if(!m_shouldProcessDocument)
        {
            if (ValidationHelper.isRevExists(selectedItem, revId))
            {
                throw new TCException(m_registry.getString("PartExists.Error"));
            }
            else
            {
                revisePart(createInputMap);
            }
        }
        if (m_shouldProcessDocument)
        {
        	if(m_documentPropertyMap.getComponent() != null)
        	{
        		throw new TCException(m_registry.getString("PartExists.Error"));
        	}
        	else
        	{
        		processDocument(createInputMap);
        	}
        }
       
        
        if (m_shouldProcessDataset)
        {
            processDataset(createInputMap);
        }
        
        m_createItemResponse = CreateItemSOAHelper.createItem(createInputMap);
        
        if (m_shouldProcessDataset)
        {

			TCComponent selectedTemplate = m_datasetAllPropertyMap
					.getTag("selectedTemplate");
			
			if (null == selectedTemplate) {
				CompoundObjectHelper[] createItemResponseDataset = m_createItemResponse
						.getCompoundObject(CreateItemResponseHelper.DATASET);

				TCComponent dataset = createItemResponseDataset[0].getComponent();				
				
				String path = m_datasetAllPropertyMap
						.getString("datasetFilePath");
				if (!path.equals("")) {
					try {
						DatasetHelper datasetHelper = new DatasetHelper(
								dataset, path);
						datasetHelper.updateDataset();
					} catch (TCException exception) {
						stack.addErrors(exception);
						throw exception;
					}
				}

			}
		}
        
        return true;
    }

    /**
     * @param createInputMap
     * @throws TCException
     */
    private void revisePart(Map<String, OperationInputHelper[]> createInputMap)
            throws TCException
    {
        OperationInputHelper revisionOperationInput = new OperationInputHelper();
        
        
        IPropertyMap revisionPropertyMap = m_partPropertyMap
                .getCompound("revision");
        
        revisionPropertyMap.setComponent(m_revisionItem);
        
        m_partPropertyMap.setCompound("revision", revisionPropertyMap);
        revisionOperationInput.setObjectProperties(m_partPropertyMap);
        
        revisionOperationInput
                .setOperationMode(OperationInputHelper.OPERATION_REVISE_OBJECT);
        
        
        createInputMap.put(CreateItemSOAHelper.PART,
                new OperationInputHelper[] {revisionOperationInput});
    }

    /**
     * @param shouldApplyStatus
     * @param createInputMap
     */
    private void processPart(boolean shouldApplyStatus,
            Map<String, OperationInputHelper[]> createInputMap)
    {
        OperationInputHelper operationInput = new OperationInputHelper();
        operationInput.setObjectProperties(m_partPropertyMap);
        operationInput
                .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        operationInput.setContainerInfo(
                NewItemCreationHelper.getContainer(), "contents", true);
        
        createInputMap.put(CreateItemSOAHelper.PART,
                new OperationInputHelper[] {operationInput});
        
        // to apply status to items
        if (shouldApplyStatus)
        {
            OperationInputHelper statusOperationInput = new OperationInputHelper();
            statusOperationInput
                    .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            IPropertyMap statusPropertyMap = new SOAPropertyMap(
                    "ReleaseStatus");
            statusPropertyMap.setString("name", "ENG");
            statusOperationInput.setObjectProperties(statusPropertyMap);
            
            createInputMap.put(CreateItemSOAHelper.STATUS,
                    new OperationInputHelper[] {statusOperationInput});
        }
    }

    /**
     * @param createInputMap
     */
    private void processDataset(
            Map<String, OperationInputHelper[]> createInputMap)
    {
    	
    	  OperationInputHelper datasetOperationInput = new OperationInputHelper();

          
          TCComponent selectedTemplate = m_datasetAllPropertyMap.getTag("selectedTemplate");
          if(null != selectedTemplate)
          {
          	datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
          	m_datasetAllPropertyMap.setComponent(selectedTemplate);
     
          }
          else
          {
          	datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
          }
          datasetOperationInput.setObjectProperties(m_datasetAllPropertyMap);
          datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
          
          
      /*  OperationInputHelper datasetOperationInput = new OperationInputHelper();
        datasetOperationInput.setObjectProperties(m_datasetAllPropertyMap);
        datasetOperationInput
                .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        datasetOperationInput.setContainerInfo(
                NewItemCreationHelper.getContainer(), "contents", true);*/
        
        createInputMap.put(CreateItemSOAHelper.DATASET,
                new OperationInputHelper[] {datasetOperationInput});
    }

    /**
     * @param createInputMap
     */
    private void processDocument(
            Map<String, OperationInputHelper[]> createInputMap)
    {
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        documentOperationInput.setObjectProperties(m_documentPropertyMap);
        documentOperationInput
                .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        documentOperationInput.setContainerInfo(
                NewItemCreationHelper.getContainer(), "contents", true);
        
        createInputMap.put(CreateItemSOAHelper.DOCUMENT,
                new OperationInputHelper[] {documentOperationInput});
        
            if (m_relType == null)
            {
                m_relType = NationalOilwell.RELATED_DOCUMENT;
            }
            OperationInputHelper relationOperationInput = new OperationInputHelper();
            IPropertyMap relationPropertyMap = new SOAPropertyMap(m_relType);
            relationPropertyMap.setTag("primary_object", m_revisionItem);
            relationOperationInput.setObjectProperties(relationPropertyMap);
            relationOperationInput
                    .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            createInputMap.put(CreateItemSOAHelper.PART_DOC_RELATION,
                    new OperationInputHelper[] {relationOperationInput});
    }
    
    private boolean isDocumentContainsRequiredFields(IPropertyMap document)
    {
        return (hasContent(document.getString("DocumentsCAT")) || hasContent(document.getString("DocumentsType")));
    }
    
    private boolean isDatasetContainsRequiredFields(IPropertyMap datasetPropertyMap)
    {
        return (hasContent(datasetPropertyMap.getString("dataset_type")));
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    @Override
    public boolean postAction() throws TCException
    {
        return true;
    }

	/** The m_registry. */
    protected Registry m_registry = null;
    private boolean m_shouldProcessDataset, m_shouldProcessDocument;
    private String m_objectType = "Nov4Part";
    private String m_datasetBusinessObjectName = "";
    private IPropertyMap m_partPropertyMap;
    private IPropertyMap m_documentPropertyMap;
    private IPropertyMap m_datasetAllPropertyMap = null;
    private Boolean m_isLegacySelected = false;
    private String m_relType = null;
    private TCComponentItemRevision m_revisionItem = null;
    protected  CreateItemResponseHelper m_createItemResponse ;
}
