package com.nov.rac.item.operations.legacy;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.AbstractOperationDelegate;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class LegacyItemCreateDelegate extends AbstractOperationDelegate implements ISubscriber
{
    
    private AbstractOperationDelegate m_nextLevelDelegate = null;
    
    public LegacyItemCreateDelegate(Shell theShell)
    {
        super(theShell);
        m_registry = getRegistry();
        registerSubscriber();
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        // return
        // Registry.getRegistry("com.nov.rac.item.operations.legacy.legacy");
        return Registry.getRegistry(this);
    }
    
    @Override
    public boolean executeOperation()
    {
        if (m_lovSelection != null)
        {
            getNextLevelDelegate();
        }
        
        return super.executeOperation();
    }
    
    private void getNextLevelDelegate()
    {
        StringBuffer theStringBuffer = new StringBuffer();
        theStringBuffer.append(m_lovSelection).append(NationalOilwell.DOTSEPARATOR).append("NEXT_DELEGATE");
        
        String[] contextNames = NewItemCreationHelper.getConfiguration(theStringBuffer.toString(), getRegistry());
        
        Object[] objectArguments = new Object[4];
        objectArguments[0] = getShell();
        objectArguments[1] = m_objectType;
        objectArguments[2] = m_datasetBusinessObjectName;
        objectArguments[3] = m_isLegacyItem;
        // objectArguments[4] = m_relationName;
        
        if (contextNames != null && contextNames.length > 0)
        {
            try
            {
                m_nextLevelDelegate = (AbstractOperationDelegate) Instancer.newInstanceEx(contextNames[0],
                        objectArguments);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        m_nextLevelDelegate.registerOperationInputProvider(getOperationInputProviders());
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        boolean retValue = false;
        
        if (m_nextLevelDelegate != null)
        {
            retValue = m_nextLevelDelegate.preCondition();
        }
        return retValue;
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        boolean retValue = false;
        
        if (m_nextLevelDelegate != null)
        {
            retValue = m_nextLevelDelegate.preAction();
        }
        
        return retValue;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        boolean retValue = false;
        
        if (m_nextLevelDelegate != null)
        {
            retValue = m_nextLevelDelegate.baseAction();
        }
        
        return retValue;
    }
    
    @Override
    public boolean postAction() throws TCException
    {
        boolean retValue = false;
        
        if (m_nextLevelDelegate != null)
        {
            retValue = m_nextLevelDelegate.postAction();
        }
        
        return retValue;
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber(NationalOilwell.ITEM_TYPE, this);
        controller.registerSubscriber(NationalOilwell.DATASET_TYPE, this);
        controller.registerSubscriber(NationalOilwell.LEGACY_DATASET_TYPE, getSubscriber());
        controller.registerSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT, this);
        controller.registerSubscriber(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT, this);
        controller.registerSubscriber(NationalOilwell.LEGACY_RD_OR_RDD_SELECTED_MSG, this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String propertyName = event.getPropertyName();
        
        if (propertyName.equals(NationalOilwell.LEGACY_DATASET_TYPE))
        {
            m_datasetBusinessObjectName = (String) event.getNewValue();
        }
        else if (propertyName.equals(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT))
        {
            String selectedOperation = event.getNewValue().toString();
            getLegacyPrefix(selectedOperation);
            m_objectType = m_registry.getString(selectedOperation + ".BOTYPE");
            
            m_lovSelection = selectedOperation;
            NewItemCreationHelper.setSelectedOperation(selectedOperation);
        }
        else if (propertyName.compareTo(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT) == 0)
        {
            m_isLegacyItem = (Boolean) event.getNewValue();
        }
        else if (propertyName.compareTo(NationalOilwell.LEGACY_RD_OR_RDD_SELECTED_MSG) == 0)
        {
            m_relationName = (String) event.getNewValue();
            System.out.println(event.getNewValue());
        }
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    private void getLegacyPrefix(String itemType)
    {
        String prefix = "";
        
        StringBuffer theStringBuffer = new StringBuffer();
        theStringBuffer.append(itemType).append(NationalOilwell.DOTSEPARATOR).append("ID_PREFIX");
        
        String[] contextNames = NewItemCreationHelper.getConfiguration(theStringBuffer.toString(), getRegistry());
        
        if (contextNames != null && contextNames.length > 0)
        {
            prefix = contextNames[0] + getGroupModiferId();
        }
        
        NewItemCreationHelper.setPrefix(prefix);
        
    }
    
    private String getGroupModiferId()
    {
        String groupId = "";
        
        System.out.println(NewItemCreationHelper.getCurrentGroup());
        
        String currentLoggedInGorup = NewItemCreationHelper.getCurrentGroup();
        
        if (currentLoggedInGorup != null)
        {
            groupId = currentLoggedInGorup.substring(0, 2);
        }
        
        return groupId;
    }
    
    @Override
    public void cleanUp()
    {
        super.cleanUp();
        unRegisterSubscriber();
    }
    
    private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.unregisterSubscriber(NationalOilwell.ITEM_TYPE, this);
        controller.unregisterSubscriber(NationalOilwell.DATASET_TYPE, this);
        controller.unregisterSubscriber(NationalOilwell.LEGACY_DATASET_TYPE, getSubscriber());
        controller.unregisterSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.LEGACY_RD_OR_RDD_SELECTED_MSG, this);
    }
    
    /** The m_registry. */
    protected Registry m_registry = null;
    private String m_objectType = "Nov4Part";
    private String m_datasetBusinessObjectName = "";
    private String m_relationName = "RelatedDocuments";
    private String m_lovSelection = null;
    private boolean m_isLegacyItem = false;
}
