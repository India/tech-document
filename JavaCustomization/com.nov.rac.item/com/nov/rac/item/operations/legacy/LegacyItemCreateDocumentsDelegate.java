package com.nov.rac.item.operations.legacy;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.AbstractOperationDelegate;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateItemResponse;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LegacyItemCreateDocumentsDelegate extends AbstractOperationDelegate
{
    
    public LegacyItemCreateDocumentsDelegate(Shell theShell, String objectType, String datasetType, boolean isLegacyItem)
    {
        super(theShell);
        
        m_objectType = objectType;
        datasetBusinessObjectName = datasetType;
        
        m_isLegacySelected = isLegacyItem;
        m_registry = getRegistry();
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        // return
        // Registry.getRegistry("com.nov.rac.item.operations.legacy.legacy");
        return Registry.getRegistry(this);
    }
    
    public void setObjectType(String objectType)
    {
        m_objectType = objectType;
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        shouldProcessDataset = true;
        shouldProcessDocument = true;
        
        /*if (m_objectType == null || m_objectType.trim().isEmpty())
        {
            m_objectType = "Documents";
        }*/
        
        documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper documentCompoundHelper = new CreateInCompoundHelper(documentPropertyMap);
        documentCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        documentCompoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), documentPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = {documentPropertyMap};
        
        // validates all required field in selected operation
        IPropertyValidator documentsPropertiesValidator = getValidator(getRegistry(), NewItemCreationHelper.getSelectedOperation());
        documentsPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        datasetAllPropertyMap = new SOAPropertyMap(datasetBusinessObjectName);
        
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), datasetAllPropertyMap);
        
        if (!isDocumentContainsRequiredFields(documentPropertyMap))
        {
            shouldProcessDocument = false;
        }
        else if(!m_isLegacySelected)
        {
             IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
             docPropertiesModifier.modifyProperties(documentPropertyMap);
        }
        
        if (!isDatasetContainsRequiredFields(datasetAllPropertyMap))
        {
            shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(documentPropertyMap, datasetAllPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(datasetAllPropertyMap);
        }
        return true;
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        documentOperationInput.setObjectProperties(documentPropertyMap);
        documentOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        documentOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        OperationInputHelper datasetOperationInput = new OperationInputHelper();

        
        TCComponent selectedTemplate = datasetAllPropertyMap.getTag("selectedTemplate");
        if(null != selectedTemplate)
        {
        	datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
        	datasetAllPropertyMap.setComponent(selectedTemplate);
   
        }
        else
        {
        	datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        }
        
        datasetOperationInput.setObjectProperties(datasetAllPropertyMap);
        datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        
       /* OperationInputHelper relationOperationInput = new OperationInputHelper();
        IPropertyMap relationPropertyMap = new SOAPropertyMap("RelatedDocuments");
        relationOperationInput.setObjectProperties(relationPropertyMap);
        relationOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);*/
        
        if (shouldProcessDocument)
        {
        	if(documentPropertyMap.getComponent() != null)
        	{
        		throw new TCException(m_registry.getString("PartExists.Error"));
        	}
        	else
        	{
        		 createInputMap.put(CreateItemSOAHelper.DOCUMENT, new OperationInputHelper[] { documentOperationInput });
        	}
           
           // createInputMap.put(CreateItemSOAHelper.PART_DOC_RELATION, new OperationInputHelper[] { relationOperationInput });
            
        }
        
        if (shouldProcessDataset)
        {
            createInputMap.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
        }
        CreateItemResponseHelper createItemResponse = CreateItemSOAHelper.createItem(createInputMap);
        
        
        if (shouldProcessDataset)
        {

			if (null == selectedTemplate) {
				CompoundObjectHelper[] createItemResponseDataset = createItemResponse
						.getCompoundObject(CreateItemResponseHelper.DATASET);

				TCComponent dataset = createItemResponseDataset[0].getComponent();				
				
				String path = datasetAllPropertyMap
						.getString("datasetFilePath");
				if (!path.equals("")) {
					try {
						DatasetHelper datasetHelper = new DatasetHelper(
								dataset, path);
						datasetHelper.updateDataset();
					} catch (TCException exception) {
						stack.addErrors(exception);
						throw exception;
					}
				}

			}
		}
        
        
        return true;
    }
    
    private boolean isDocumentContainsRequiredFields(SOAPropertyMap document)
    {
        return (hasContent(document.getString("DocumentsCAT")) || hasContent(document.getString("DocumentsType")));
    }
    
    private boolean isDatasetContainsRequiredFields(SOAPropertyMap datasetPropertyMap)
    {
        return (hasContent(datasetPropertyMap.getString("dataset_type")));
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    
    /** The m_registry. */
    protected Registry m_registry = null;
    private boolean shouldProcessDataset, shouldProcessDocument;
    private String m_objectType = "Documents";
    private String datasetBusinessObjectName = "";
    private SOAPropertyMap documentPropertyMap;
    private SOAPropertyMap datasetAllPropertyMap = null;
    private Boolean m_isLegacySelected = false;
    
}
