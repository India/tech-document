package com.nov.rac.item.operations.legacy;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.utilities.services.createFoldersHelper.CreateFolderSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class LegacyItemCreateNumberedProductDelegate extends LegacyItemCreatePartDelegate{

	public LegacyItemCreateNumberedProductDelegate(Shell theShell,
			String objectType, String datasetType, boolean isLegacyItem) {
		super(theShell, objectType, datasetType, isLegacyItem);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean baseAction() throws TCException {
		
	  super.baseAction();
	  
	  CompoundObjectHelper[] createItemResponseArray = m_createItemResponse
              .getCompoundObject(CreateItemResponseHelper.PART);
      
      if(createItemResponseArray != null && createItemResponseArray.length >0)
      {
         TCComponent part = createItemResponseArray[0].getComponent();
         
         TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
         TCComponentListOfValues productFolderLOV = LOVUtils.getLOVComponent(NationalOilwell.PRODUCT_FOLDERS_LOV , session );
        
         String[] folders = LOVUtils.getLovStringValues(productFolderLOV);
         
         CreateFolderSOAHelper.createProductFolders(part , folders);
      }
      
	  return true;
	}
	
}
