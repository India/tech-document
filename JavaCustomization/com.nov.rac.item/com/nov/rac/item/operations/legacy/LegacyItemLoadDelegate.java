package com.nov.rac.item.operations.legacy;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.item.operations.AbstractLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LegacyItemLoadDelegate extends AbstractLoadDelegate
{

    public LegacyItemLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }

    public void loadComponent(TCComponent targetObject) throws TCException
    {
    
        TCComponentItem targetItem = null;
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        if (null != targetItem)
        {
            
            IPropertyMap partPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap( targetItem, 
                                              new String[] { NationalOilwell.OBJECT_TYPE,
                                                             NationalOilwell.ITEM_ID, 
                                                             NationalOilwell.NOV_UOM,
                                                             NationalOilwell.OBJECT_NAME, 
                                                             NationalOilwell.OBJECT_DESC }, 
                                              partPropertyMap
                                             );
            
            IPropertyMap masterFormPropertyMap = new SimplePropertyMap();
            TCComponent masterForm = targetItem.getRelatedComponent("IMAN_master_form");
            
            String[] properties = getRegistry().getStringArray(targetItem.getType()+".loadProperties");
            if(properties != null)
            {
                PropertyMapHelper.componentToMap( masterForm, 
                                                  properties,
                                                  masterFormPropertyMap
                                                );

            }
            partPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
            
            loadPanelsData(targetItem.getType(), getInputProviders(),
                    partPropertyMap);
            
        }
        
    }
    
    private Registry getRegistry() {
        m_registry = Registry.getRegistry("com.nov.rac.item.operations.legacy.legacy");

        return m_registry;
    }
    
    
    private Registry m_registry;
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent( (TCComponent) inputObject);
    }
    
}
