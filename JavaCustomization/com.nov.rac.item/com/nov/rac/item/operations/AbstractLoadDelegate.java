package com.nov.rac.item.operations;

import java.util.List;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public abstract class AbstractLoadDelegate implements ILoadDelegate
{
    List<IUIPanel> inputProviders ;
    public AbstractLoadDelegate(List<IUIPanel> inputProviders)
    {
        this.inputProviders = inputProviders;
    }
    
    public void loadPanelsData(String itemType, List<IUIPanel> panels, IPropertyMap propertyMap)
    {
    
        List<IUIPanel> filteredPanels = OperationHelper.getFilteredPanels(itemType, panels);
        
        for (IUIPanel panel : filteredPanels)
        {
            if (null != propertyMap && panel instanceof ILoadSave)
            {
                ILoadSave loadablepanel = (ILoadSave) panel;
                try
                {
                		loadablepanel.load(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * Gets the operation input providers.
     * 
     * @return the operation input providers
     */
    public final List<IUIPanel> getInputProviders()
    {
        return inputProviders;
    }
}
