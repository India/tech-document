package com.nov.rac.item.operations;

import java.util.List;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public interface ILoadDelegate
{
	  void loadComponent(Object inputObject) throws TCException;
	  
	  List<IUIPanel> getInputProviders();
}
