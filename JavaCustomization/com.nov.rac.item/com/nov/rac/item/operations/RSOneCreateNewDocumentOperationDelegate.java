package com.nov.rac.item.operations;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreateNewDocumentOperationDelegate extends AbstractOperationDelegate implements ISubscriber
{
    /** The document property map. */
    protected IPropertyMap m_documentPropertyMap;
    
    /** The dataset business object name. */
    private String m_datasetBusinessObjectName = "";
    
    protected boolean m_shouldProcessDataset = true;
    
    protected TCComponentItem m_documentObject;
    
    /** This Map holds all the saved properties of dataset. */
    protected IPropertyMap m_datasetAllPropertyMap = null;
    
    private CreateItemResponseHelper theResponse;
    
    public RSOneCreateNewDocumentOperationDelegate(Shell theShell)
    {
        super(theShell);
        registerSubscriber();
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        m_shouldProcessDataset = true;
        
        m_documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(m_documentPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), m_documentPropertyMap);
        
        m_datasetAllPropertyMap = new SOAPropertyMap(m_datasetBusinessObjectName);
        
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), m_datasetAllPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = { m_documentPropertyMap, m_datasetAllPropertyMap };
        
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        
        // This modification is for 6600(RsoneLeagcyDoc)
        if (!NewItemCreationHelper.getSelectedOperation().equalsIgnoreCase("CreateLegacyDocument"))
        {
        	setItemID(m_documentPropertyMap);
        	
           // m_documentPropertyMap.setString("item_id", NOVRSOneItemIDHelper.getOracleId());
            
            modifyProperties(NationalOilwell.DOCUMENT_TYPE, m_documentPropertyMap);
        }
        // This modification is for 6600(RsoneLeagcyDoc)
        
        if (!isDatasetContainsRequiredFields(m_datasetAllPropertyMap))
        {
            m_shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(m_documentPropertyMap, m_datasetAllPropertyMap);
            
            // This modification is for 6600(RsoneLeagcyDoc)
            if (!NewItemCreationHelper.getSelectedOperation().equalsIgnoreCase("CreateLegacyDocument"))
            {
            	modifyProperties(NationalOilwell.DATASET_BO_TYPE, m_datasetAllPropertyMap);
               /* IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
                datasetPropertiesModifier.modifyProperties(m_datasetAllPropertyMap);*/
            }
            // This modification is for 6600(RsoneLeagcyDoc)
        }
        
        return true;
    }
    
    protected void setItemID(IPropertyMap propertyMap)
    {
    	propertyMap.setString("item_id", NOVRSOneItemIDHelper.getOracleId());
    }

    protected void modifyProperties(String boType, IPropertyMap propertyMap) 
	{
		IPropertyModifier propertyModifier = getModifier(boType);
		propertyModifier.modifyProperties(propertyMap);
	}
    
    private boolean isDatasetContainsRequiredFields(IPropertyMap datasetPropertyMap)
    {
        return (hasContent(datasetPropertyMap.getString("dataset_type")));
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        documentOperationInput.setObjectProperties(m_documentPropertyMap);
        documentOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        addContainerInfo(documentOperationInput);
        
        OperationInputHelper datasetOperationInput = new OperationInputHelper();
        datasetOperationInput.setObjectProperties(m_datasetAllPropertyMap);
        datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        Map<String, OperationInputHelper[]> createItemInput = new HashMap<String, OperationInputHelper[]>();
        
        createItemInput.put(CreateItemSOAHelper.DOCUMENT, new OperationInputHelper[] { documentOperationInput });
        if (m_shouldProcessDataset)
        {
            createItemInput.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
        }
        
        theResponse = CreateItemSOAHelper.createItem(createItemInput);
        
        theResponse.throwErrors();
        
        return true;
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    protected void addContainerInfo(OperationInputHelper operationInput)
    {
        operationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.RSONE_DATASET_TYPE))
        {
            m_datasetBusinessObjectName = (String) event.getNewValue();
        }
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.RSONE_DATASET_TYPE, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    @Override
    public boolean postAction() throws TCException
    {
        if (m_shouldProcessDataset)
        {

            TCComponent selectedTemplate = m_datasetAllPropertyMap
                    .getTag("selectedTemplate");
            
            if (null == selectedTemplate) {
                CompoundObjectHelper[] createItemResponseDataset = theResponse
                        .getCompoundObject(CreateItemResponseHelper.DATASET);

                TCComponent dataset = createItemResponseDataset[0].getComponent();              
                
                String path = m_datasetAllPropertyMap
                        .getString("datasetFilePath");
                if (!path.equals("")) {
                    try {
                        DatasetHelper datasetHelper = new DatasetHelper(
                                dataset, path);
                        datasetHelper.updateDataset();
                    } catch (TCException exception) {
                        stack.addErrors(exception);
                        throw exception;
                    }
                }

            }
        }
        
        return true;
    }
    
}
