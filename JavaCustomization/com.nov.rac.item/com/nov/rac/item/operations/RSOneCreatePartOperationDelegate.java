package com.nov.rac.item.operations;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.ClassificationHelper;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneCreatePartOperationDelegate extends AbstractOperationDelegate implements ISubscriber
{

    public RSOneCreatePartOperationDelegate(Shell theShell)
    {
        super(theShell);
        registerSubscriber();
        m_registry = getRegistry();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.operations");
    }

    @Override
    public boolean preCondition() throws TCException
    {
        shouldProcessDataset = true;
        shouldProcessDocument = true;
        
        if (itemType == null || itemType.trim().isEmpty())
        {
            itemType = "Nov4Part";
        }
        
        //it does not contain dash property maps
        partMap = new SOAPropertyMap(itemType);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(partMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(itemType, getOperationInputProviders(), partMap);
        // to get all the dash propertyMaps
        
        IPropertyMap[] dashMapArray = partMap.getCompoundArray("dashPropertyMap");
        partMap.setCompoundArray("dashPropertyMap", new IPropertyMap[0]);
        requiredPartMaps =  preparePartMaps(partMap, dashMapArray);
        documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper documentCompoundHelper = new CreateInCompoundHelper(documentPropertyMap);
        documentCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        documentCompoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), documentPropertyMap);
        
        
        datasetAllPropertyMap = new SOAPropertyMap(datasetBusinessObjectName);
        
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), datasetAllPropertyMap);
        
        //validates all the fields
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        IPropertyMap[] arrayOfIPropertyMap = { datasetAllPropertyMap };
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        partPropertiesValidator.validateProperties(requiredPartMaps);

        if (!isDocumentContainsRequiredFields(documentPropertyMap))
        {
            shouldProcessDocument = false;
        }
        else
        {
            copyProperties(requiredPartMaps[0], documentPropertyMap);
            IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
            docPropertiesModifier.modifyProperties(documentPropertyMap);
        }
        
        if (!isDatasetContainsRequiredFields(datasetAllPropertyMap))
        {
            shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(documentPropertyMap, datasetAllPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(datasetAllPropertyMap);
        }
        
        m_statusMsg = m_registry.getString("Container.Message")+NewItemCreationHelper.getContainer();
        
        return true;
    }
    
    private IPropertyMap[] copyPropertiesFromDash(IPropertyMap[] dashMapArray, IPropertyMap[] partMapArray)
    {
        IPropertyMap[] returnValue = partMapArray;
        
        for (int inx = 0; inx < dashMapArray.length; inx++ )
        {
            dashMapArray[inx].setType(partMapArray[inx].getType());
            partMapArray[inx].addAll(dashMapArray[inx]);
        }
        
        return returnValue; 
    }

    protected boolean isDatasetContainsRequiredFields(IPropertyMap datasetPropertyMap)
    {
        return (hasContent(datasetPropertyMap.getString("dataset_type")));
    }

    protected void copyProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        // fill item related attributes for document
        String itemId = inputPropertyMap.getString("item_id");
        String oracleId = itemId.split("-")[0];
        outputPropertyMap.setString("item_id", oracleId);
        
        String itemName = inputPropertyMap.getString("object_name");
        outputPropertyMap.setString("object_name", itemName);
        
        String itemDesc = inputPropertyMap.getString("object_desc");
        outputPropertyMap.setString("object_desc", itemDesc);
        
       /* IPropertyMap itemRevisionPropertyMap = inputPropertyMap.getCompound("revision");
        if (itemRevisionPropertyMap != null)
        {
            String revId = itemRevisionPropertyMap.getString("item_revision_id");
            IPropertyMap outputItemRevisionPropertyMap = outputPropertyMap.getCompound("revision");
            if (outputItemRevisionPropertyMap != null)
            {
                outputItemRevisionPropertyMap.setString("item_revision_id", revId);
                outputPropertyMap.setCompound("revision", outputItemRevisionPropertyMap);
            }
        }*/
        
    }
    
    protected boolean isDocumentContainsRequiredFields(IPropertyMap documentPropertyMap)
    {
        return (hasContent(documentPropertyMap.getString("DocumentsCAT")) || hasContent(documentPropertyMap
                .getString("DocumentsType")));
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        ClassificationHelper classiHelper = new ClassificationHelper();
        classificationMap = ClassificationHelper.getClassificationMap();
        classificationUIdMap = ClassificationHelper.getClassificationUIDMap();
        OperationInputHelper[] operationInput = new OperationInputHelper[requiredPartMaps.length];
        OperationInputHelper[] alternateIDOperationInput = new OperationInputHelper[requiredPartMaps.length];
        OperationInputHelper[] alternateRevIDOperationInput = new OperationInputHelper[requiredPartMaps.length];
        List<OperationInputHelper> alternateIDCreateList = new ArrayList<OperationInputHelper>();
        OperationInputHelper[] alternateIDCreateInput = new OperationInputHelper[2*requiredPartMaps.length];
        OperationInputHelper[] statusOperationInput = new OperationInputHelper[requiredPartMaps.length];
        OperationInputHelper[] classificationInput = new OperationInputHelper[requiredPartMaps.length];
        
        for (int inx = 0; inx < operationInput.length; inx++)
        {
            operationInput[inx] = new OperationInputHelper();
            operationInput[inx].setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            operationInput[inx].setObjectProperties(requiredPartMaps[inx]);
            operationInput[inx].setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
            
            // alternate id for item
            alternateIDOperationInput[inx] = new OperationInputHelper();
            alternateIDOperationInput[inx].setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            IPropertyMap identifierPropertyMap = new SOAPropertyMap("Identifier");
            createPropMapForIdentifier(identifierPropertyMap, requiredPartMaps[inx].getString("item_id"));
            alternateIDOperationInput[inx].setObjectProperties(identifierPropertyMap);
            
            alternateIDCreateList.add( alternateIDOperationInput[inx]);
            
          //alternate id for item rev
            alternateRevIDOperationInput[inx] = new OperationInputHelper();
            alternateRevIDOperationInput[inx].setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            IPropertyMap identifierRevPropertyMap = new SOAPropertyMap("IdentifierRev");
            createPropMapForIdentifierRev(identifierRevPropertyMap, requiredPartMaps[inx].getString("item_id"));
            alternateRevIDOperationInput[inx].setObjectProperties(identifierRevPropertyMap);
            
            alternateIDCreateList.add(  alternateRevIDOperationInput[inx]);
            
            //to apply status to items
            statusOperationInput[inx] = new OperationInputHelper();
            
            statusOperationInput[inx].setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            IPropertyMap statusPropertyMap = new SOAPropertyMap("ReleaseStatus");
            
            statusPropertyMap.setString("name", "ENG");

            statusOperationInput[inx].setObjectProperties(statusPropertyMap);
            
            //to classify items
            classificationInput[inx] = new OperationInputHelper();
            classificationInput[inx].setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            IPropertyMap classificationPropertyMap = new SOAPropertyMap();
            createPropMapForClassification(classificationPropertyMap, requiredPartMaps[inx] );
            classificationInput[inx].setObjectProperties(classificationPropertyMap);
           
        }
        
        
        alternateIDCreateInput = alternateIDCreateList.toArray(alternateIDCreateInput);
        
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        createInputMap.put(CreateItemSOAHelper.PART, operationInput);
        
        createInputMap.put(CreateItemSOAHelper.ALTERNATE_ID, alternateIDCreateInput);
        
        createInputMap.put(CreateItemSOAHelper.CLASSIFICATION,  classificationInput);
        
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        documentOperationInput.setObjectProperties(documentPropertyMap);
        documentOperationInput
                .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        documentOperationInput.setContainerInfo(
                NewItemCreationHelper.getContainer(), "contents", true);
        
        OperationInputHelper relationOperationInput = new OperationInputHelper();
        IPropertyMap relationPropertyMap = new SOAPropertyMap("RelatedDefiningDocument");
        relationOperationInput.setObjectProperties(relationPropertyMap);
        relationOperationInput
                .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
     
        OperationInputHelper datasetOperationInput = new OperationInputHelper();

        TCComponent selectedTemplate = null;
        
        if(datasetAllPropertyMap != null)
        {
            selectedTemplate = datasetAllPropertyMap.getTag("selectedTemplate");
            
            if(null != selectedTemplate)
            {
            	datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
            	datasetAllPropertyMap.setComponent(selectedTemplate);
       
            }
            else
            {
            	datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            }
            
            datasetOperationInput.setObjectProperties(datasetAllPropertyMap);
            datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        }

        createInputMap.put(CreateItemSOAHelper.STATUS, statusOperationInput);
        
        
        if(shouldProcessDocument)
        {
            createInputMap.put(CreateItemSOAHelper.DOCUMENT,
                    new OperationInputHelper[] { documentOperationInput });
            createInputMap.put(CreateItemSOAHelper.PART_DOC_RELATION,
                    new OperationInputHelper[] { relationOperationInput });
           
        }
        
        if(shouldProcessDataset)
        {
            createInputMap.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
        }
           
        
        createItemResponse = CreateItemSOAHelper.createItem(createInputMap);
        
        return true;
    }
    
    protected IPropertyMap[] preparePartMaps(IPropertyMap partMap, IPropertyMap[] dashMapArray)
    {
        IPropertyMap[] returnValue = dashMapArray;
        
        for (IPropertyMap currentMap : dashMapArray)
        {
           // currentMap.setType(partMap.getType());
            currentMap.addAll(partMap);
        }
        
/*        for(int inx = 0; inx < dashMapArray.length; inx ++)
        {
            returnValue[inx] = new SOAPropertyMap(partMap.getType());
            dashMapArray[inx].addAll(partMap);
        }*/
        
        return returnValue;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.RSONE_DATASET_TYPE))
        {
            datasetBusinessObjectName = (String) event.getNewValue();
        }
        else if(event.getPropertyName().equals(NationalOilwell.RSONE_ITEM_TYPE_EVENT))
        {
            itemType = event.getNewValue().toString();
        }
        else if(event.getPropertyName().equals(NationalOilwell.SEND_TO_CLASSIFICATION))
        {
            sendToClassification = (Boolean) event.getNewValue();
        }
       
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.RSONE_DATASET_TYPE, getSubscriber());
        controller.registerSubscriber(NationalOilwell.RSONE_ITEM_TYPE_EVENT, this);
        controller.registerSubscriber(NationalOilwell.SEND_TO_CLASSIFICATION, this);
    }
    
    private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.unregisterSubscriber(NationalOilwell.RSONE_DATASET_TYPE, getSubscriber());
        controller.unregisterSubscriber(NationalOilwell.RSONE_ITEM_TYPE_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.SEND_TO_CLASSIFICATION, this);
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    protected void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    private IPropertyMap createPropMapForIdentifier(IPropertyMap identifierPropertyMap, String itemId)
    {
         TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("RSOne");
        // properties tobe set here
        identifierPropertyMap.setString("object_name", itemId );
        //identifierPropertyMap.setString("object_desc", "CreateItemSOA-AlternateID");
        identifierPropertyMap.setString("idfr_id", itemId );
        identifierPropertyMap.setTag("idcontext", idContext );
        identifierPropertyMap.setTag("suppl_context", null );
        //identifierPropertyMap.boolProps.put("disp_default", new BigInteger("1") );
        //identifierPropertyMap.boolProps.put("isAlternateID", new BigInteger("0") );
        return identifierPropertyMap;
    }
    
//***********    alternate ID Rev property map   ****************/
    
    private IPropertyMap createPropMapForIdentifierRev(IPropertyMap identifierRevPropertyMap, String itemId)
    {
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("RSOne");
        
        identifierRevPropertyMap.setString("object_name", itemId );
        identifierRevPropertyMap.setString("idfr_id", "01" );
        identifierRevPropertyMap.setTag("idcontext", idContext );
        //identifierRevPropertyMap.tagProps.put("suppl_context", null );
        //identifierRevPropertyMap.boolProps.put("isAlternateID", new BigInteger("0") );
        return identifierRevPropertyMap;
    }
    
    private IPropertyMap createPropMapForClassification(IPropertyMap classificationPropertyMap, IPropertyMap requiredPartMaps)
    {
        String className = requiredPartMaps.getString(NationalOilwell.ICS_subclass_name);
    /*    String classUid = classificationUIdMap.get(className);
        TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
        String unitSystem = null;
        try
        {
            TCComponent  theComponent = theSession.stringToComponent(classUid);
            unitSystem = theComponent.getProperty("unit_system");
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        
        
        classificationPropertyMap.setString("cid", classificationMap.get(className));
        classificationPropertyMap.setInteger("AttrID",new Integer("-630"));
        classificationPropertyMap.setStringArray("AttrValues",new String[] {"0","1","2"} );
        return classificationPropertyMap;
    }
    
    @Override
    public boolean postAction() throws TCException
    {
       
        if (sendToClassification)
        {
            
            CompoundObjectHelper[] createItemResponseArray = createItemResponse
                    .getCompoundObject(CreateItemResponseHelper.PART);
            
            
            Map<String, TCComponent[]> childComponentMap = createItemResponseArray[0].getChildComponents();
            
            TCComponent[] revisionItem = childComponentMap.get("revision");
            
            if(revisionItem!=null && revisionItem.length > 0)
            {
                TCComponentItemRevision itemRev = (TCComponentItemRevision) revisionItem[0];
                
                IPerspectiveDef persDef = PerspectiveDefHelper
                
                .getPerspectiveDef("com.teamcenter.rac.classification.icm.ClassificationPerspective");
                
                InterfaceAIFComponent[] component = new InterfaceAIFComponent[] { itemRev };
                
                persDef.openPerspective(component);
            }
         
        }
        if (shouldProcessDataset)
        {

			TCComponent selectedTemplate = datasetAllPropertyMap
					.getTag("selectedTemplate");
			
			if (null == selectedTemplate) {
				CompoundObjectHelper[] createItemResponseDataset = createItemResponse
						.getCompoundObject(CreateItemResponseHelper.DATASET);

				TCComponent dataset = createItemResponseDataset[0].getComponent();				
				
				String path = datasetAllPropertyMap
						.getString("datasetFilePath");
				if (!path.equals("")) {
					try {
						DatasetHelper datasetHelper = new DatasetHelper(
								dataset, path);
						datasetHelper.updateDataset();
					} catch (TCException exception) {
						stack.addErrors(exception);
						throw exception;
					}
				}

			}
		}
        
        m_statusMsg = m_statusMsg + "\n" +  m_registry.getString("completed.message");
        
        return true;
    }
    
    @Override
    protected void cleanUp()
    {
        super.cleanUp();
        unRegisterSubscriber();
        
    }
    
 /*   @Override
    protected void showMessage(String s)
    {
        if(m_statusMsg != null)
        {
            s = m_statusMsg;
        }
        super.showMessage(s);
    }*/
    
    protected String itemType = null;
    protected IPropertyMap documentPropertyMap;
    protected IPropertyMap datasetAllPropertyMap;
    protected IPropertyMap partMap;
    protected boolean shouldProcessDataset, shouldProcessDocument;
    protected IPropertyMap[] requiredPartMaps;
    private TCComponentItem item;
    protected String datasetBusinessObjectName;
    protected boolean sendToClassification = false;
    protected Map<String,String> classificationMap = new HashMap<String, String>();
    private Map<String,String> classificationUIdMap = new HashMap<String, String>();
    protected String selectedCid = null;
    
    protected CreateItemResponseHelper createItemResponse;
    protected String m_statusMsg = null;
    protected Registry m_registry;
    
}
