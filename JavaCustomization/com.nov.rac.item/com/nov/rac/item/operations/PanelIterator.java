package com.nov.rac.item.operations;

import com.nov.rac.propertymap.IPropertyMap;

public interface PanelIterator
{
    public IPropertyMap getPanelsData();
    public void addItemType(String itemType);
}
