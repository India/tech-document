package com.nov.rac.item.operations;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

/**
 * The Class CreateRelatedDefiningDocumentOperationDelegate.
 */
public class CreateRelatedDefiningDocumentOperationDelegate extends CreateNewDocumentOperationDelegate
{
    /**
     * Instantiates a new creates the related defining document operation
     * delegate.
     * 
     * @param theShell
     *            the the shell
     */
    public CreateRelatedDefiningDocumentOperationDelegate(Shell theShell)
    {
        super(theShell);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.item.operations.CreateNewDocumentOperationDelegate#baseAction
     * ()
     */
    @Override
    public boolean baseAction() throws TCException
    {
        
        super.baseAction();
        TCComponent partRevision = NewItemCreationHelper.getTargetComponent();
        if (partRevision != null && partRevision instanceof TCComponentItemRevision)
        {
            partRevision.add("RelatedDefiningDocument", documentObject);
            partRevision.lock();
            partRevision.save();
            partRevision.unlock();
        }
        return true;
        
    }
}
