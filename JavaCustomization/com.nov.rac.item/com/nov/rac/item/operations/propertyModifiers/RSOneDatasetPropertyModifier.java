package com.nov.rac.item.operations.propertyModifiers;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;

public class RSOneDatasetPropertyModifier implements IPropertyModifier {

	public RSOneDatasetPropertyModifier() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void modifyProperties(IPropertyMap propertyMap) {
		String itemId = propertyMap.getString("object_name");
		if (itemId != null) {
			propertyMap.setString("object_name", itemId);
		}
		
		propertyMap.setString(NationalOilwell.DOCUMENTS_CATEGORY,"");
	    propertyMap.setString(NationalOilwell.DOCUMENTS_TYPE, "");
	}
	

}
