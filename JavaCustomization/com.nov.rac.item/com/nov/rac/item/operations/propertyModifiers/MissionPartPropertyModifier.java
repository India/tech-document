package com.nov.rac.item.operations.propertyModifiers;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;



public class MissionPartPropertyModifier implements IPropertyModifier
{

    public MissionPartPropertyModifier()
    {
    }
    
    @Override
    public void modifyProperties(IPropertyMap propertyMap)
    {
      String itemId=  propertyMap.getString("item_id");
      
      if(itemId!= null)
      {
          String currentGroup = NewItemCreationHelper.getCurrentGroup();
          
          String groupId= currentGroup.substring(0,2);
          
          String modifiedId = itemId + DELIMETER + groupId;
          
          propertyMap.setString("item_id",modifiedId);
      }
    }
    
    
    private static final String DELIMETER = ":";
}
