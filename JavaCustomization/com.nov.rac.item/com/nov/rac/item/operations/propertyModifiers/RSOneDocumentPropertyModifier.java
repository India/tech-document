package com.nov.rac.item.operations.propertyModifiers;

import com.nov.rac.propertymap.IPropertyMap;

public class RSOneDocumentPropertyModifier implements IPropertyModifier {

	public RSOneDocumentPropertyModifier() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void modifyProperties(IPropertyMap propertyMap) 
	 {
	        IPropertyMap docPropMap = propertyMap;
			String itemId = docPropMap.getString("item_id");
			String docCategory = docPropMap.getString("DocumentsCAT");
			if (itemId != null && docCategory != null) {
				String modifiedId = itemId + DOC_CATEGORY_DELIMETER + docCategory;
				docPropMap.setString("item_id", modifiedId);
			}
	
			/*docPropMap.setString("dataset_type", "");
			docPropMap.setString("datasetFilePath", "");
			docPropMap.setString("selectedTemplate", "");
			propertyMap.addAll(docPropMap);*/
		}

    private static final String DOC_CATEGORY_DELIMETER = "-";
	}


