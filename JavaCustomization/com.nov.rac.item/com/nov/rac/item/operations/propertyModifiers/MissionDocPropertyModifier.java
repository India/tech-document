package com.nov.rac.item.operations.propertyModifiers;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;

public class MissionDocPropertyModifier implements IPropertyModifier
{
    
    public MissionDocPropertyModifier()
    {
    }
    
    @Override
    public void modifyProperties(IPropertyMap propertyMap)
    {
        String itemId = propertyMap.getString("item_id");
        String docCategory = propertyMap.getString("DocumentsCAT");
        boolean sFlag = itemId.contains(":");
        if (itemId != null && docCategory != null)
        {
            String currentGroup = NewItemCreationHelper.getCurrentGroup();
            String groupId = currentGroup.substring(0, 2);
            if (sFlag)
            {
                itemId = itemId.split(":")[0];
            }
            String modifiedId = itemId + DOC_CATEGORY_DELIMETER + docCategory + GROUP_DELIMETER + groupId;
            propertyMap.setString("item_id", modifiedId);
        }
    }
    
    private static final String GROUP_DELIMETER = ":";
    private static final String DOC_CATEGORY_DELIMETER = "-";
}
