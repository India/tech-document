package com.nov.rac.item.operations.propertyModifiers.downhole;

import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.propertymap.IPropertyMap;

public class DatasetPropertyModifier implements IPropertyModifier {

	@Override
	public void modifyProperties(IPropertyMap propertyMap) {
		String itemId = propertyMap.getString("object_name");
		int length = itemId.length() - 3;
		String reqdItemId = itemId.substring(0, length);
        if (itemId != null)
        {
            propertyMap.setString("object_name", reqdItemId);
        }
	}

}
