package com.nov.rac.item.operations.propertyModifiers.legacy;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.ValidationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.teamcenter.rac.kernel.TCComponentItem;

public class LegacyDocumentPropertyModifier implements IPropertyModifier
{
    
    public LegacyDocumentPropertyModifier()
    {
    }
    
    @Override
    public void modifyProperties(IPropertyMap propertyMap)
    {
        String itemId = propertyMap.getString("item_id");
        String modifiedId;
        String docCategory = propertyMap.getString("DocumentsCAT");
        IPropertyMap masterPropMap = propertyMap.getCompound("IMAN_master_form");
        String sequence = PropertyMapHelper.handleNull(masterPropMap.getString("Sequence"));
        
        if (itemId != null && docCategory != null)
        {
        	if(LegacyItemCreateHelper.isTuboscope())
        	{
        		modifiedId = NewItemCreationHelper.getPrefix() + "0" + itemId + DELIMETER + docCategory + DELIMETER + sequence;
        	}
        	else
        	{
        		 modifiedId = NewItemCreationHelper.getPrefix() + itemId + DELIMETER + docCategory + DELIMETER + sequence;
        	}
           
            propertyMap.setString("item_id", modifiedId);
            
            if (!isItemIDUnique(propertyMap))
            {
                // To set existing part to part property map
                String partBoType = propertyMap.getType();
                TCComponentItem selectedItem = ValidationHelper.getTCComponent(
                        propertyMap.getString(NationalOilwell.ITEM_ID), partBoType);
                propertyMap.setComponent(selectedItem);
            }
        }
        
    }
    
    private boolean isItemIDUnique(IPropertyMap propertyMap)
    {
        boolean isUniqueId = false;
        isUniqueId = ValidationHelper.isIDAlreadyExist(propertyMap,
                propertyMap.getType());
        return isUniqueId;
    }
    private static final String DELIMETER = "-";
    
}
