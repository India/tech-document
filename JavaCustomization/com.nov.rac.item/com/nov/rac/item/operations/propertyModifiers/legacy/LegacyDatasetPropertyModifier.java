package com.nov.rac.item.operations.propertyModifiers.legacy;

import com.nov.NationalOilwell;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.propertymap.IPropertyMap;

public class LegacyDatasetPropertyModifier implements IPropertyModifier
{
    
    public LegacyDatasetPropertyModifier()
    {
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void modifyProperties(IPropertyMap propertyMap)
    {
        String itemId = propertyMap.getString("object_name");
        if (itemId != null)
        {
            propertyMap.setString("object_name", itemId);
        }
    }
}
