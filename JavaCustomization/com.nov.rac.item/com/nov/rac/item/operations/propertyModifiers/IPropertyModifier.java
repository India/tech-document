package com.nov.rac.item.operations.propertyModifiers;

import com.nov.rac.propertymap.IPropertyMap;

public interface IPropertyModifier
{
    
    public void modifyProperties(IPropertyMap propertyMap);
    
}
