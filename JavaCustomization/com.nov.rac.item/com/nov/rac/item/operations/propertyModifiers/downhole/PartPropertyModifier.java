package com.nov.rac.item.operations.propertyModifiers.downhole;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.propertymap.IPropertyMap;

public class PartPropertyModifier implements IPropertyModifier {

	@Override
	public void modifyProperties(IPropertyMap propertyMap)
	{
		String itemId = propertyMap.getString("item_id");
		String currentGroup = NewItemCreationHelper.getCurrentGroup();
        String group = currentGroup.substring(0, 2);
        
        //if (itemId != null || itemId != "")
        if (itemId.compareTo("") != 0)
        {
            String modifiedId = itemId + DELIMETER + group;
            propertyMap.setString("item_id", modifiedId);
        }
	}
	
	private static final String DELIMETER = ":";

}
