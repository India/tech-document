package com.nov.rac.item.operations.propertyModifiers.legacy;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.ValidationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCComponentItem;

public class LegacyPartPropertyModifier implements IPropertyModifier
{
    
    @Override
    public void modifyProperties(IPropertyMap propertyMap)
    {
        String itemId = propertyMap.getString(NationalOilwell.ITEM_ID);
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        Integer legacy = masterPropertyMap.getInteger(NationalOilwell.LEGACY);
        if (legacy != 1
                && !(propertyMap.getType().equalsIgnoreCase(NationalOilwell.PART_BO_TYPE) && LegacyItemCreateHelper
                        .isTuboscope()))
        {
            if (itemId != null)
            {
                String modifiedId = NewItemCreationHelper.getPrefix() + itemId;
                propertyMap.setString(NationalOilwell.ITEM_ID, modifiedId);
            }
        }
        
        if (!isItemIDUnique(propertyMap))
        {
            // To set existing part to part property map
            String partBoType = propertyMap.getType();
            TCComponentItem selectedItem = ValidationHelper.getTCComponent(
                    propertyMap.getString(NationalOilwell.ITEM_ID), partBoType);
            propertyMap.setComponent(selectedItem);
        }
    }
    
    // to check whether the item ID is already existing or not
    private boolean isItemIDUnique(IPropertyMap propertyMap)
    {
        boolean isUniqueId = false;
        isUniqueId = ValidationHelper.isIDAlreadyExist(propertyMap, propertyMap.getType());
        return isUniqueId;
    }
    
}
