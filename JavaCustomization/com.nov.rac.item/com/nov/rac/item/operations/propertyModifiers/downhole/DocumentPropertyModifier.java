package com.nov.rac.item.operations.propertyModifiers.downhole;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;

public class DocumentPropertyModifier implements IPropertyModifier 
{

	@Override
	public void modifyProperties(IPropertyMap propertyMap) 
	{
		String itemId = propertyMap.getString("item_id");
		boolean sFlag = itemId.contains(":");
		
        String docCategory = propertyMap.getString("DocumentsCAT");
        String currentGroup = NewItemCreationHelper.getCurrentGroup();
        String groupId = currentGroup.substring(0, 2);
        
        if (itemId != null && docCategory != null)
        {
        	if (sFlag)
            {
                itemId = itemId.split(":")[0];
            }
        	String modifiedId = itemId + DELIMETER_1 + docCategory + DELIMETER_2 + groupId;
            propertyMap.setString("item_id", modifiedId);
        }
    }
    
    private static final String DELIMETER_1 = "-";
    private static final String DELIMETER_2 = ":";

}
