package com.nov.rac.item.operations.propertyModifiers;

import com.nov.rac.propertymap.IPropertyMap;

public class MissionDatasetPropertyModifier implements IPropertyModifier
{
    
    @Override
    public void modifyProperties(IPropertyMap propertyMap)
    {
        String itemId = propertyMap.getString("object_name");
        if(itemId!= null)
        {
            int index = itemId.indexOf(GROUP_DELIMETER);
            String modifiedId = itemId.substring(0,index);
            propertyMap.setString("object_name", modifiedId);   
        } 
    }
    
    private static final String GROUP_DELIMETER = ":";
}
