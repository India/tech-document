package com.nov.rac.item.operations;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.nov.rac.item.operations.RSOneCreateNewDocumentOperationDelegate;

public class RSOneRelatedDefiningDocumentOperationDelegate extends RSOneCreateNewDocumentOperationDelegate 
{
	public RSOneRelatedDefiningDocumentOperationDelegate(Shell theShell) 
	{
		super(theShell);
	}

	@Override
	protected void addContainerInfo(OperationInputHelper operationInput)
	{
	      TCComponent targetObject = NewItemCreationHelper.getTargetComponent();
	      TCComponentItemRevision targetItem = null;
			
	     if(targetObject instanceof TCComponentItemRevision)
	   {
	        targetItem = (TCComponentItemRevision) targetObject;
	   }		
	   operationInput.setContainerInfo(targetItem , "RelatedDefiningDocument", true);
	}
}
