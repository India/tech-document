package com.nov.rac.item.operations;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCException;

public class RSOneLegacyPartWithOracleIdOperationDelegate extends AbstractOperationDelegate implements ISubscriber
{
    
    public RSOneLegacyPartWithOracleIdOperationDelegate(Shell theShell)
    {
        super(theShell);
        registerSubscriber();
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        if (itemType == null || itemType.trim().isEmpty())
        {
            itemType = "Nov4Part";
        }
        
        partPropertyMap = new SOAPropertyMap(itemType);
        CreateInCompoundHelper partCompoundHelper = new CreateInCompoundHelper(partPropertyMap);
        partCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        partCompoundHelper.addCompound("ItemRevision", "revision");
        fillPanelsData(itemType, getOperationInputProviders(), partPropertyMap);
        
        documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper docCompoundHelper = new CreateInCompoundHelper(documentPropertyMap);
        docCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        docCompoundHelper.addCompound("ItemRevision", "revision");
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), documentPropertyMap);
        
        datasetPropertyMap = new SOAPropertyMap(datasetBusinessObjectName);
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), datasetPropertyMap);
        
        IPropertyMap[] dashPropertyMap = partPropertyMap.getCompoundArray("dashPropertyMap");
        partPropertyMap.addAll(dashPropertyMap[0]);
        
        IPropertyMap[] arrayOfIPropertyMap = { partPropertyMap, datasetPropertyMap };
        // IPropertyMap[] arrayOfIPropertyMap = { partPropertyMap,
        // documentPropertyMap, datasetPropertyMap };
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        if (!isDocumentContainsRequiredFields(documentPropertyMap))
        {
            shouldProcessDocument = false;
        }
        else
        {
            copyProperties(partPropertyMap, documentPropertyMap);
            
            IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
            docPropertiesModifier.modifyProperties(documentPropertyMap);
        }
        
        if (!isDatasetContainsRequiredFields(datasetPropertyMap))
        {
            shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(documentPropertyMap, datasetPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(datasetPropertyMap);
        }
        
        return true;
    }
    
    private boolean isDocumentContainsRequiredFields(SOAPropertyMap documentPropertyMap)
    {
        return (hasContent(documentPropertyMap.getString("DocumentsCAT")) || hasContent(documentPropertyMap
                .getString("DocumentsType")));
    }
    
    private boolean isDatasetContainsRequiredFields(SOAPropertyMap datasetPropertyMap)
    {
        return (hasContent(datasetPropertyMap.getString("dataset_type")));
    }
    
    private void copyProperties(IPropertyMap inputPropertyMap, SOAPropertyMap outputPropertyMap)
    {
        // fill item related atrributes for document
        String itemId = m_oldOtemId;
      //  String oracleId = itemId.split("-")[0];
        outputPropertyMap.setString("item_id", itemId);
        
        String itemName = inputPropertyMap.getString("object_name");
        outputPropertyMap.setString("object_name", itemName);
        
        String itemDesc = inputPropertyMap.getString("object_desc");
        outputPropertyMap.setString("object_desc", itemDesc);
        
        /*
         * IPropertyMap itemRevisionPropertyMap =
         * inputPropertyMap.getCompound("revision"); if (itemRevisionPropertyMap
         * != null) { String revId =
         * itemRevisionPropertyMap.getString("item_revision_id"); IPropertyMap
         * outputItemRevisionPropertyMap =
         * outputPropertyMap.getCompound("revision"); if
         * (outputItemRevisionPropertyMap != null) {
         * outputItemRevisionPropertyMap.setString("item_revision_id", revId);
         * outputPropertyMap.setCompound("revision",
         * outputItemRevisionPropertyMap); } }
         */
        
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
    	
    	String alternateId = partPropertyMap.getString(NationalOilwell.ITEM_ID);
        String objectName = partPropertyMap.getString(NationalOilwell.OBJECT_NAME);
        
        // this is to maintain item_id of legacy part.
        partPropertyMap.setString(NationalOilwell.ITEM_ID, m_oldOtemId);
        
        OperationInputHelper operationInput = new OperationInputHelper();
        operationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        operationInput.setObjectProperties(partPropertyMap);
        operationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        createInputMap.put(CreateItemSOAHelper.PART, new OperationInputHelper[] { operationInput });
        
        // alternate id for item
        OperationInputHelper alternateIDOperationInput = new OperationInputHelper();
        alternateIDOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap identifierPropertyMap = new SOAPropertyMap("Identifier");
       // createPropMapForIdentifier(identifierPropertyMap, partPropertyMap.getString("item_id"));
        //createPropMapForIdentifier(identifierPropertyMap, NOVRSOneItemIDHelper.getOracleId() + "-001");
        createPropMapForIdentifier(identifierPropertyMap, m_oldOtemId, objectName);
        alternateIDOperationInput.setObjectProperties(identifierPropertyMap);
        
      //alternate id for item rev
        OperationInputHelper alternateIDRevOperationInput = new OperationInputHelper();
        alternateIDRevOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap identifierRevPropertyMap = new SOAPropertyMap("IdentifierRev");
      //  createPropMapForIdentifierRev(identifierRevPropertyMap, partPropertyMap.getString("item_id"));
       // createPropMapForIdentifierRev(identifierRevPropertyMap, NOVRSOneItemIDHelper.getOracleId() + "-001");
        createPropMapForIdentifierRev(identifierRevPropertyMap, m_oldOtemId, objectName);
        alternateIDRevOperationInput.setObjectProperties(identifierRevPropertyMap);
        
        //to apply status to items
        OperationInputHelper statusOperationInput = new OperationInputHelper();
        
        statusOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap statusPropertyMap = new SOAPropertyMap("ReleaseStatus");
        
        statusPropertyMap.setString("name", "LEGACY");

        statusOperationInput.setObjectProperties(statusPropertyMap);

        createInputMap.put(CreateItemSOAHelper.STATUS, new OperationInputHelper[] { statusOperationInput });
        
        createInputMap.put(CreateItemSOAHelper.ALTERNATE_ID, new OperationInputHelper[]{alternateIDOperationInput,alternateIDRevOperationInput});
        
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        documentOperationInput.setObjectProperties(documentPropertyMap);
        documentOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        documentOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        OperationInputHelper datasetOperationInput = new OperationInputHelper();
        datasetOperationInput.setObjectProperties(datasetPropertyMap);
        datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        if (shouldProcessDocument)
        {
            createInputMap.put(CreateItemSOAHelper.DOCUMENT, new OperationInputHelper[] { documentOperationInput });
            // to attach RDD to item
            OperationInputHelper relationOperationInput = new OperationInputHelper();
            IPropertyMap relationPropertyMap = new SOAPropertyMap("RelatedDefiningDocument");
            relationOperationInput.setObjectProperties(relationPropertyMap);
            relationOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            
            createInputMap.put(CreateItemSOAHelper.PART_DOC_RELATION,
                    new OperationInputHelper[] { relationOperationInput });
            
        }
        
        if (shouldProcessDataset)
        {
            createInputMap.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
        }
        CreateItemSOAHelper.createItem(createInputMap);
        
        return true;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.RSONE_DATASET_TYPE))
        {
            datasetBusinessObjectName = (String) event.getNewValue();
        }
        else if (event.getPropertyName().equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL))
        {
            m_oldOtemId = event.getNewValue().toString();
        }
        else if (event.getPropertyName().equals(NationalOilwell.RSONE_ITEM_TYPE_EVENT))
        {
            itemType = event.getNewValue().toString();
        }
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.RSONE_DATASET_TYPE, this);
        controller.registerSubscriber(NationalOilwell.RSONE_ITEM_TYPE_EVENT, this);
        controller.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, this);
    }
    
    private IPropertyMap createPropMapForIdentifier(IPropertyMap identifierPropertyMap, String alternateId, String objectName)
    {
         TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("RSOne");
        // properties tobe set here
        identifierPropertyMap.setString("object_name", objectName );
        //identifierPropertyMap.setString("object_desc", "CreateItemSOA-AlternateID");
        identifierPropertyMap.setString("idfr_id", alternateId );
        identifierPropertyMap.setTag("idcontext", idContext );
        identifierPropertyMap.setTag("suppl_context", null );
        //identifierPropertyMap.boolProps.put("disp_default", new BigInteger("1") );
        //identifierPropertyMap.boolProps.put("isAlternateID", new BigInteger("0") );
        return identifierPropertyMap;
    }
    
    private IPropertyMap createPropMapForIdentifierRev(IPropertyMap identifierRevPropertyMap, String alternateId, String objectName)
    {
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("RSOne");
        
        identifierRevPropertyMap.setString("object_name", objectName );
        identifierRevPropertyMap.setString("idfr_id", "01" );
        identifierRevPropertyMap.setTag("idcontext", idContext );
        //identifierRevPropertyMap.tagProps.put("suppl_context", null );
        //identifierRevPropertyMap.boolProps.put("isAlternateID", new BigInteger("0") );
        return identifierRevPropertyMap;
    }
    
    private IPropertyMap createPropMapForClassification(IPropertyMap classificationPropertyMap)
    {
        classificationPropertyMap.setString("cid", "642401");
        classificationPropertyMap.setInteger("AttrID",new Integer("-630"));
        classificationPropertyMap.setStringArray("AttrValues",new String[] {"1"} );
        
        return classificationPropertyMap;
    }
    
    
    private String itemType = "Nov4Part";
    private String m_oldOtemId = null;
    private IPropertyMap partPropertyMap;
    private SOAPropertyMap documentPropertyMap;
    private SOAPropertyMap datasetPropertyMap;
    private String datasetBusinessObjectName;
    private boolean shouldProcessDocument = true;
    private boolean shouldProcessDataset = true;
}
