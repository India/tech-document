package com.nov.rac.item.operations;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

/**
 * The Class CreateRelatedDocumentOperationDelegate.
 */
public class CreateRelatedDocumentOperationDelegate extends CreateNewDocumentOperationDelegate
{
    
    /**
     * Instantiates a new creates the related document operation delegate.
     * 
     * @param theShell
     *            the the shell
     */
    public CreateRelatedDocumentOperationDelegate(Shell theShell)
    {
        super(theShell);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.item.operations.CreateNewDocumentOperationDelegate#baseAction
     * ()
     */
    @Override
    public boolean baseAction() throws TCException
    {
        
        super.baseAction();
        if (null != documentObject)
        {
            TCComponent partRevision = NewItemCreationHelper.getTargetComponent();
            if (partRevision != null && partRevision instanceof TCComponentItemRevision)
            {
                partRevision.add("RelatedDocuments", documentObject);
                partRevision.lock();
                partRevision.save();
                partRevision.unlock();
            }
        }
        return true;
        
    }
    
}
