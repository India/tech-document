package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class RelatedDefiningDocumentLoadDelegate extends AbstractLoadDelegate
{
    public RelatedDefiningDocumentLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    public void loadComponent(TCComponent targetObject) throws TCException
    {
        if (targetObject instanceof TCComponentItemRevision)
        {
            TCComponentItem theItem = ((TCComponentItemRevision) targetObject).getItem();
            
            IPropertyMap propertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(theItem, new String[] { "item_id", "object_name", "object_desc" },
                    propertyMap);
            
            IPropertyMap revisionPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetObject, new String[] { "item_revision_id" }, revisionPropertyMap);
            propertyMap.setCompound("revision", revisionPropertyMap);
            
            loadPanelsData(NationalOilwell.DOCUMENT_TYPE, getInputProviders(), propertyMap);
        }
    }
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
    	loadComponent( (TCComponent) inputObject);
    }
}
