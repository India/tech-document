package com.nov.rac.item.operations;

import java.util.List;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public interface IOperationDelegate
{
    
    /* Perform Validation on the input values.*/
    public boolean preCondition() throws TCException;
 
    public boolean preAction() throws TCException; 

    /* Create all required objects.*/
    public boolean baseAction() throws TCException;   

    /* Post processing after object creation. */
    public boolean postAction() throws TCException;   
    
   /* Executes Operations listed above */
    public boolean executeOperation() ;

    /* registers panels contributing data for operation*/
    public void registerOperationInputProvider(List<IUIPanel> inputProviders);
    
}
