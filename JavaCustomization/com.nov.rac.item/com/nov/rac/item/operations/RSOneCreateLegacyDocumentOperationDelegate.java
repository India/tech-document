package com.nov.rac.item.operations;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.propertymap.IPropertyMap;

public class RSOneCreateLegacyDocumentOperationDelegate extends RSOneCreateNewDocumentOperationDelegate{

	public RSOneCreateLegacyDocumentOperationDelegate(Shell theShell) 
	{
		super(theShell);
	}
	
	@Override
	protected void modifyProperties(String boType, IPropertyMap propertyMap)
	{
		
	}
	
	protected void setItemID(IPropertyMap propertyMap)
    {
		
    }
}
