package com.nov.rac.item.operations;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.statusHelper.ApplyStatusSOAHelper;
import com.nov.rac.utilities.userservices.NOV4CopyNamedReferences;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov4.services.rac.custom.NOV4DataManagementService;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.client.model.ErrorStack;

public class CreateNewPartOperationDelegate extends AbstractOperationDelegate implements ISubscriber
{
    protected boolean shouldProcessDataset = true;
    protected boolean shouldProcessDocument = true;
    
    /** This Map holds all the saved properties of dataset. */
    private IPropertyMap datasetAllPropertyMap = null;
    
    public CreateNewPartOperationDelegate(Shell theShell)
    {
        super(theShell);
        // this.m_shell = theShell;
        registerSubscriber();
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        // hack : empty itemType wil not allow creation of CreateInObjectHelper
        if (itemType == null || itemType.trim().isEmpty())
        {
            itemType = NationalOilwell.PART_BO_TYPE;
        }
        
        shouldProcessDataset = true;
        shouldProcessDocument = true;
        
        partPropertyMap = new CreateInObjectHelper(itemType, CreateInObjectHelper.OPERATION_CREATE);
        CreateInCompoundHelper partCompoundHelper = new CreateInCompoundHelper(partPropertyMap);
        partCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        partCompoundHelper.addCompound("ItemRevision", "revision");
        fillPanelsData(itemType, getOperationInputProviders(), partPropertyMap);
        
        documentPropertyMap = new CreateInObjectHelper(NationalOilwell.DOCUMENT_TYPE,
                CreateInObjectHelper.OPERATION_CREATE);
        CreateInCompoundHelper docCompoundHelper = new CreateInCompoundHelper(documentPropertyMap);
        docCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        docCompoundHelper.addCompound("ItemRevision", "revision");
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), documentPropertyMap);
        
        datasetAllPropertyMap = new SimplePropertyMap();
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), datasetAllPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = { partPropertyMap, documentPropertyMap, datasetAllPropertyMap };
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        IPropertyModifier partPropertiesModifier = getModifier(itemType);
        partPropertiesModifier.modifyProperties(partPropertyMap);
        
        if (isDocumentContainsRequiredFields(documentPropertyMap))
        {
            shouldProcessDocument = false;
        }
        else
        {
            copyProperties(partPropertyMap, documentPropertyMap);
            IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
            docPropertiesModifier.modifyProperties(documentPropertyMap);
        }
        
        if (shouldProcessDocument)
        {
            datasetPropertyMap = new CreateInObjectHelper(datasetBusinessObjectName,
                    CreateInObjectHelper.OPERATION_CREATE);
            copyDatasetProperties(datasetAllPropertyMap, datasetPropertyMap);
            
            if (datasetPropertyMap.isEmpty())
            {
                shouldProcessDataset = false;
            }
            else
            {
                copyDocProperties(documentPropertyMap, datasetPropertyMap);
                
                IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
                datasetPropertiesModifier.modifyProperties(datasetPropertyMap);
            }
        }
        
        return true;
    }
    
    private boolean isDocumentContainsRequiredFields(CreateInObjectHelper document)
    {
        return !(hasContent(document.getString("DocumentsCAT")) || hasContent(document.getString("DocumentsType")));
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        System.out.println("Create Part preaction called...");
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        
        ArrayList<CreateInObjectHelper> inputObjectList = new ArrayList<CreateInObjectHelper>();
        inputObjectList.add(partPropertyMap);
        
        if (shouldProcessDocument)
        {
            inputObjectList.add(documentPropertyMap);
            if (shouldProcessDataset)
                inputObjectList.add(datasetPropertyMap);
        }
        CreateInObjectHelper[] inputObjectsArray = inputObjectList.toArray(new CreateInObjectHelper[inputObjectList
                .size()]);
        // CreateObjectsSOAHelper.createUpdateObjects(inputObjectsArray,
        // new ControlInfo[] { ControlInfo.ROLL_BACK_FAIL_FAST });
        CreateObjectsSOAHelper.createUpdateObjects(inputObjectsArray);
        
        // Added for TCDECREL-6363
        CreateObjectsSOAHelper.throwErrors(inputObjectsArray);
        ExceptionHelper.validateObjectCreation(inputObjectsArray, stack);
        
        // ******************************* Apply status to Newly Created Part ::
        // start
        TCComponentItem partObject = (TCComponentItem) CreateObjectsSOAHelper
                .getCreatedOrUpdatedRootObject(partPropertyMap);
        if (partObject != null)// KLOC220
        {
            processPart(inputObjectsArray, partObject);
        }
        return true;
    }
    
    private void processPart(CreateInObjectHelper[] inputObjectsArray, TCComponentItem partObject) throws TCException
    {
        TCComponentItemRevision partRevObject = partObject.getLatestItemRevision();
        
        applyStatus(partObject, NationalOilwell.STATUS_STANDARD);
        // ******************************* Apply status to Newly Created Part ::
        // end
        
        ArrayList<TCComponent> components = new ArrayList<TCComponent>();
        components.add(partObject);
        
        if (shouldProcessDocument)
        {
            TCComponentItemRevision partRevision = partObject.getLatestItemRevision();
            TCComponentItem documentObject = (TCComponentItem) CreateObjectsSOAHelper
                    .getCreatedOrUpdatedRootObject(documentPropertyMap);
            if (null != documentObject)
            {
                processDocument(components, partRevision, documentObject);
            }
        }
        handleErrors(inputObjectsArray);
        // ******* alternate ID Code : Start ***************//
        
        // Kishor ******* alternate ID Code : Start ***************//
        createAlternateIDForPart(partObject, partRevObject);
        // ******* alternate ID Code : End *****************//
        
        // adding newly created objects to container/folder
        addObjectToFolder(components);
    }
    
    private void processDocument(ArrayList<TCComponent> components, TCComponentItemRevision partRevision,
            TCComponentItem documentObject) throws TCException
    {
        components.add(documentObject);
        TCComponentItemRevision documentRevision = documentObject.getLatestItemRevision();
        documentRevision.refresh();
        
        partRevision.add("RelatedDefiningDocument", documentObject);
        if (shouldProcessDataset)
        {
            // TCComponentItemRevision documentRevision =
            // documentObject.getLatestItemRevision();
            // documentRevision.refresh();
            TCComponent datasetObject = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(datasetPropertyMap);
            if (datasetObject != null)
            {
                processDataset(documentRevision, datasetObject);
            }
        }
        partRevision.lock();
        partRevision.save();
        partRevision.unlock();
    }
    
    protected void processDataset(TCComponentItemRevision documentRevision, TCComponent datasetObject)
            throws TCException
    {
        documentRevision.add("IMAN_specification", datasetObject);
        
        TCComponent selectedTemplate = datasetAllPropertyMap.getTag("selectedTemplate");
        if (null == selectedTemplate)
        {
            String path = datasetAllPropertyMap.getString("datasetFilePath");
            if (!path.equals(""))
            {
                try
                {
                    DatasetHelper datasetHelper = new DatasetHelper(datasetObject, path);
                    datasetHelper.updateDataset();
                }
                catch (TCException exception)
                {
                    stack.addErrors(exception);
                    throw exception;
                }
            }
            
        }
        else
        {
            
            Map<TCComponent, TCComponent[]> templateToDataSetMap = new HashMap<TCComponent, TCComponent[]>();
            templateToDataSetMap.put(selectedTemplate, new TCComponent[] { datasetObject });
            TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
            NOV4DataManagementService dmService = NOV4DataManagementService.getService(session);
            
            dmService.copyNamedReferences(templateToDataSetMap);
            
        }
        
    }
    
    protected void createAlternateIDForPart(TCComponentItem partObject, TCComponentItemRevision partRevObject)
            throws TCException
    {
        
        // ******* alternate ID Code : Start ***************//
        
        // Kishor ******* alternate ID Code : Start ***************//
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("JDE");
        
        IPropertyMap altIDProp = new SimplePropertyMap("Identifier");
        
        altIDProp.setString("object_name", partPropertyMap.getString(NationalOilwell.OBJECT_NAME));
        altIDProp.setTag("altid_of", partObject);
        altIDProp.setTag("idcontext", idContext);
        altIDProp.setString("idfr_id", partPropertyMap.getString(NationalOilwell.ITEM_ID).split(":")[0]);
        
        IPropertyMap altIDRevProp = new SimplePropertyMap("IdentifierRev");
        
        altIDRevProp.setString("object_name", partPropertyMap.getString(NationalOilwell.OBJECT_NAME));
        altIDRevProp.setTag("altid_of", partRevObject);
        altIDRevProp.setTag("idcontext", idContext);
        altIDRevProp.setString("idfr_id", partPropertyMap.getCompound("revision").getString("item_revision_id"));
        
        CreateAlternateIDHelper createAltHelper = new CreateAlternateIDHelper(altIDProp, altIDRevProp);
        createAltHelper.createAlternateID();
        
        // Kishor ******* alternate ID Code : Start ***************//
        partObject.refresh();
        partRevObject.refresh();
        // ******* alternate ID Code : End *****************//
    }
    
    protected void addObjectToFolder(ArrayList<TCComponent> components) throws TCException
    {
        TCComponentFolder parentFolder = (TCComponentFolder) NewItemCreationHelper.getContainer();
        parentFolder.add("contents", components, 0);
        parentFolder.refresh();
    }
    
    protected void addObjectToFolder(TCComponent component) throws TCException
    {
        
        TCComponentFolder parentFolder = (TCComponentFolder) NewItemCreationHelper.getContainer();
        parentFolder.add("contents", component, 0);
        parentFolder.refresh();
    }
    
    private void copyProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        // fill item related atrributes for document
        String itemId = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("item_id", itemId);
        
        String itemName = inputPropertyMap.getString("object_name");
        outputPropertyMap.setString("object_name", itemName);
        
        String itemDesc = inputPropertyMap.getString("object_desc");
        outputPropertyMap.setString("object_desc", itemDesc);
        
        IPropertyMap itemRevisionPropertyMap = inputPropertyMap.getCompound("revision");
        if (itemRevisionPropertyMap != null)
        {
            String revId = itemRevisionPropertyMap.getString("item_revision_id");
            IPropertyMap outputItemRevisionPropertyMap = outputPropertyMap.getCompound("revision");
            if (outputItemRevisionPropertyMap != null)
            {
                outputItemRevisionPropertyMap.setString("item_revision_id", revId);
                outputPropertyMap.setCompound("revision", outputItemRevisionPropertyMap);
            }
        }
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    private void handleErrors(CreateInObjectHelper[] object)
    {
        
        ArrayList<String> errorMessages = new ArrayList<String>();
        
        for (int i = 0; i < object.length; i++)
        {
            for (ErrorStack errorStack : object[i].getErrors())
            {
                Collections.addAll(errorMessages, errorStack.getMessages());
            }
        }
        
        if (errorMessages.size() > 0)
        {
            String[] errorStrings = new String[1];
            errorStrings = errorMessages.toArray(errorStrings);
            
            MessageBox.post(new TCException(errorStrings));
        }
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String propertyName = event.getPropertyName();
        if (propertyName.equals(NationalOilwell.ITEM_TYPE))
        {
            itemType = (String) event.getNewValue();
        }
        if (propertyName.equals(NationalOilwell.DATASET_TYPE))
        {
            datasetBusinessObjectName = (String) event.getNewValue();
        }
        
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        // IController controller =
        // ControllerFactory.getInstance().getController(""+m_shell.hashCode());
        controller.registerSubscriber(NationalOilwell.ITEM_TYPE, this);
        controller.registerSubscriber(NationalOilwell.DATASET_TYPE, this);
    }
    
    private void copyDatasetProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("dataset_type");
        // outputPropertyMap.setString(PropertyMapHelper.handleNull("object_name",
        // itemName));
        PropertyMapHelper.setString("object_name", itemName, outputPropertyMap);
    }
    
    protected void applyStatus(TCComponentItem item, String status)
    {
        ApplyStatusSOAHelper.applyStatus(item, status);
    }
    
    private String itemType = "Nov4Part";
    private String datasetBusinessObjectName = "";
    protected CreateInObjectHelper partPropertyMap;
    protected CreateInObjectHelper documentPropertyMap;
    protected CreateInObjectHelper datasetPropertyMap;
}