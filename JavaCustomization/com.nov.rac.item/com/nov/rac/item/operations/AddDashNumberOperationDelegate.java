package com.nov.rac.item.operations;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class AddDashNumberOperationDelegate extends RSOneCreatePartOperationDelegate
{
    
    public AddDashNumberOperationDelegate(Shell theShell)
    {
        super(theShell);
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        setTargetComponent();
        shouldProcessDataset = true;
        shouldProcessDocument = true;
        
        if (itemType == null || itemType.trim().isEmpty())
        {
            itemType = "Nov4Part";
        }
        
        // it does not contain dash property maps
        partMap = new SOAPropertyMap(itemType);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(partMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(itemType, getOperationInputProviders(), partMap);
        
        // to get all the dash propertyMaps
        IPropertyMap[] dashMapArray = partMap.getCompoundArray("dashPropertyMap");
        partMap.setCompoundArray("dashPropertyMap", new IPropertyMap[0]);
        requiredPartMaps = preparePartMaps(partMap, dashMapArray);
        
        documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper documentCompoundHelper = new CreateInCompoundHelper(documentPropertyMap);
        documentCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        documentCompoundHelper.addCompound("ItemRevision", "revision");
        
        // validates all the fields
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(requiredPartMaps);
        
        shouldProcessDocument = false;
        shouldProcessDataset = false;
        
        m_statusMsg = m_registry.getString("Container.Message") + NewItemCreationHelper.getContainer();
        
        return true;
    }
    
    private void setTargetComponent() throws TCException
    {
        TCComponent targetComp = NewItemCreationHelper.getTargetComponent();
        
        if (targetComp == null)
        {
            targetComp = NewItemCreationHelper.getSelectedComponent();
        }
        
        if (targetComp instanceof TCComponentItem)
        {
            m_targetItem = (TCComponentItem) targetComp;
        }
        
        if (targetComp instanceof TCComponentItemRevision)
        {
            m_targetItem = ((TCComponentItemRevision) targetComp).getItem();
        }
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        super.baseAction();
        
        CompoundObjectHelper[] compoundObjects = createItemResponse.getCompoundObject("part");
        
        for(CompoundObjectHelper object : compoundObjects)
        {
            if(object.getComponent() instanceof TCComponentItem)
            {
                TCComponentItem part = (TCComponentItem) object.getComponent();
                
                if(part.getLatestItemRevision() instanceof TCComponentItemRevision && getDocuments() != null)
                {
                    TCComponentItemRevision partRevision = part.getLatestItemRevision();
                    partRevision.setRelated("RelatedDefiningDocument", getDocuments());
                }
            }
        }
        return true;
    }
    
    private TCComponent[] getDocuments() throws TCException
    {
        TCComponent[] documentList = null;
        
        if (m_targetItem != null)
        {
            TCComponentItemRevision partRevisionList = m_targetItem.getLatestItemRevision();
            documentList = partRevisionList.getRelatedComponents("RelatedDefiningDocument");
        }
        
        return documentList;
    }
    
    private TCComponentItem m_targetItem;
}
