package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyPartLoadDelegate extends AbstractLoadDelegate
{
    
    /**
     * Instantiates a new copy part load delegate.
     * 
     * @param inputProviders
     *            the input providers
     */
    public RSOneCopyPartLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    public void loadComponent(TCComponent targetObject) throws TCException
    {
        TCComponentItem targetItem = null;
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        if (null != targetItem)
        {
            IPropertyMap partPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetItem, new String[] { NationalOilwell.ICS_subclass_name,
                    NationalOilwell.OBJECT_NAME, NationalOilwell.OBJECT_DESC, NationalOilwell.ITEM_ID },
                    partPropertyMap);
            
            IPropertyMap masterFormPropertyMap = new SimplePropertyMap();
            TCComponent masterForm = targetItem.getRelatedComponent("IMAN_master_form");
            PropertyMapHelper.componentToMap(masterForm, new String[] { NationalOilwell.RSONE_ORG,
                    NationalOilwell.RSONE_UOM, NationalOilwell.RSONE_LOTCONTROL, NationalOilwell.RSONE_SERIALIZE,
                    NationalOilwell.RSONE_QAREQUIRED, NationalOilwell.RSONE_ENGCONTROLLED,
                    NationalOilwell.RSONE_ITEMTYPE }, masterFormPropertyMap);
            
            partPropertyMap.setComponent(targetItem); // to check/uncheck the
                                                      // clone defining document
                                                      // check box
            
            partPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
            
            IPropertyMap documentPropertyMap = new SimplePropertyMap();
            TCComponentItemRevision partRevisionList = targetItem.getLatestItemRevision();
            TCComponent[] documentList = partRevisionList.getRelatedComponents("RelatedDefiningDocument");
            
            if (null != documentList && 0 < documentList.length)
            {
                PropertyMapHelper.componentToMap(documentList[0], new String[] { NationalOilwell.DOCUMENTS_CATEGORY,
                        NationalOilwell.DOCUMENTS_TYPE }, documentPropertyMap);
                
                TCComponent[] docRevisionList = documentList[0].getRelatedComponents("revision_list");
                TCComponent[] datasetList = docRevisionList[0].getRelatedComponents("IMAN_specification");
                
                if (null != datasetList && 0 < datasetList.length)
                {
                    PropertyMapHelper.componentToMap(datasetList[0], new String[] { NationalOilwell.OBJECT_TYPE },
                            documentPropertyMap);
                }
            }
            
            loadPanelsData(targetItem.getType(), getInputProviders(), partPropertyMap);
            
            if (!documentPropertyMap.isEmpty())
            {
               
                loadPanelsData(NationalOilwell.DOCUMENT_TYPE, getInputProviders(), documentPropertyMap);
                loadPanelsData(NationalOilwell.DATASET_BO_TYPE, getInputProviders(), documentPropertyMap);
            }
            
        }
        
    }
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent((TCComponent) inputObject);
    }
    
}
