package com.nov.rac.item.operations;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyPartOperationDelegate extends AbstractOperationDelegate implements ISubscriber{

    public RSOneCopyPartOperationDelegate(Shell theShell) {
		super(theShell);
		registerSubscribers();
	}
	
	private void registerSubscribers()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.CLONE_DOC_SELECTION_EVENT, this);
    }

	@Override
	public boolean preCondition() throws TCException {

        if (itemType == null || itemType.trim().isEmpty())
        {
            itemType = "Nov4Part";
        }
        
        m_partPropertyMap = new SOAPropertyMap(itemType);
        CreateInCompoundHelper partCompoundHelper = new CreateInCompoundHelper(m_partPropertyMap);
        partCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        partCompoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(itemType, getOperationInputProviders(), m_partPropertyMap);
        
        m_documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper docCompoundHelper = new CreateInCompoundHelper(m_documentPropertyMap);
        docCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        docCompoundHelper.addCompound("ItemRevision", "revision");
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), m_documentPropertyMap);
        
        m_datasetPropertyMap = new SOAPropertyMap(m_datasetBusinessObjectName);
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), m_datasetPropertyMap);
        
        String selectedBase = m_partPropertyMap.getString(NationalOilwell.ITEM_ID);  // to validate the selected base field
        
        IPropertyMap[] dashMapArray = m_partPropertyMap.getCompoundArray("dashPropertyMap");
        String itemId = dashMapArray[0].getString(NationalOilwell.ITEM_ID);
        
        m_partPropertyMap.setCompoundArray("dashPropertyMap", new IPropertyMap[0]);
   /*     IPropertyMap[] partMapArray = new SOAPropertyMap[dashMapArray.length];
        partMapArray = preparePartMaps(partMap, partMapArray);
        */
       // requiredPartMaps = copyPropertiesFromDash(dashMapArray,partMapArray);
        m_requiredPartMap =  preparePartMaps(m_partPropertyMap, dashMapArray);
        
      
        m_requiredPartMap[0].setString(NationalOilwell.ITEM_ID, selectedBase);
        
        IPropertyMap[] arrayOfIPropertyMap = { m_requiredPartMap[0] , m_datasetPropertyMap};
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        m_requiredPartMap[0].setString(NationalOilwell.ITEM_ID, itemId);   //to assign generated item id
        
        if (!isDocumentContainsRequiredFields(m_documentPropertyMap))
        {
            m_shouldProcessDocument = false;
        }
        else
        {
            copyProperties(m_requiredPartMap[0], m_documentPropertyMap);
            IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
            docPropertiesModifier.modifyProperties(m_documentPropertyMap);
        }
        
        if (!isDatasetContainsRequiredFields(m_datasetPropertyMap))
        {
            m_shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(m_documentPropertyMap, m_datasetPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(m_datasetPropertyMap);
        }
        
		return true;
	}
	
	private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    

	@Override
	public boolean preAction() throws TCException {
		return true;
	}

	@Override
    public boolean baseAction() throws TCException
    {
        TCComponent targetComponent = NewItemCreationHelper
                .getTargetComponent();
        
        if (targetComponent instanceof TCComponentItemRevision)
        {
            targetComponent = ((TCComponentItemRevision) NewItemCreationHelper
                    .getTargetComponent()).getItem();
        }
        
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        
        processPart(targetComponent, createInputMap);
        
        
        if(m_shouldProcessDocument)
        {
            processDocument(targetComponent, createInputMap);
        }
        
        if(m_shouldProcessDataset)
        {
            processDataset(createInputMap);
        }
           
        CreateItemResponseHelper createItemResponse = CreateItemSOAHelper.createItem(createInputMap);
        
        attachDocument(createItemResponse);
        
        return true;
    }

    /**
     * @param targetComponent
     * @param createInputMap
     */
    private void processPart(TCComponent targetComponent,
            Map<String, OperationInputHelper[]> createInputMap)
    {
        m_requiredPartMap[0].setComponent(targetComponent);
        OperationInputHelper operationInput = new OperationInputHelper();
        operationInput
                .setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
        operationInput.setObjectProperties(m_requiredPartMap[0]);
        operationInput.setContainerInfo(NewItemCreationHelper.getContainer(),
                "contents", true);
        
        
        createInputMap.put(CreateItemSOAHelper.PART,
                new OperationInputHelper[] { operationInput });
    }

    /**
     * @param createInputMap
     */
    private void processDataset(
            Map<String, OperationInputHelper[]> createInputMap)
    {
        OperationInputHelper datasetOperationInput = new OperationInputHelper();

        
        TCComponent selectedTemplate = m_datasetPropertyMap.getTag("selectedTemplate");
        if(null != selectedTemplate)
        {
            datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
            m_datasetPropertyMap.setComponent(selectedTemplate);
        }
        else
        {
            datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        }
        datasetOperationInput.setObjectProperties(m_datasetPropertyMap);
        datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        createInputMap.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
    }

    /**
     * @param targetComponent
     * @param createInputMap
     * @throws TCException
     */
    protected void processDocument(TCComponent targetComponent,
            Map<String, OperationInputHelper[]> createInputMap)
            throws TCException
    {
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        
        if(m_cloneDocSelected)
        {
            TCComponentItemRevision targetItemRev = ((TCComponentItem) targetComponent).getLatestItemRevision();
            
            TCComponent document = targetItemRev.getRelatedComponent(NationalOilwell.RELATED_DEFINING_DOCUMENT);
            
           
            m_documentPropertyMap.setComponent(document);
            documentOperationInput.setObjectProperties(m_documentPropertyMap);
            documentOperationInput
                    .setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
            documentOperationInput.setContainerInfo(
                    NewItemCreationHelper.getContainer(), "contents", true);
            
        }
        else
        {
            documentOperationInput.setObjectProperties(m_documentPropertyMap);
            documentOperationInput
                    .setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
            documentOperationInput.setContainerInfo(
                    NewItemCreationHelper.getContainer(), "contents", true);
            
        }
        
        createInputMap.put(CreateItemSOAHelper.DOCUMENT,
                new OperationInputHelper[] { documentOperationInput });
    }
	
	
	private void attachDocument(CreateItemResponseHelper createItemResponse) throws TCException
    {
        if (m_shouldProcessDocument)
        {
            addOrRemoveDoc(createItemResponse);
        }
                
    }

    /**
     * @param createItemResponse
     * @throws TCException
     */
    protected void addOrRemoveDoc(CreateItemResponseHelper createItemResponse)
            throws TCException
    {
        CompoundObjectHelper[] createItemResponsePart = createItemResponse
                .getCompoundObject(CreateItemResponseHelper.PART);
        
        CompoundObjectHelper[] createItemResponseDocument = createItemResponse
                .getCompoundObject(CreateItemResponseHelper.DOCUMENT);
        
        if ((createItemResponsePart != null && createItemResponsePart.length > 0 ) && (createItemResponseDocument != null
                && createItemResponseDocument.length > 0))
        {
            TCComponentItem createdPart = (TCComponentItem) createItemResponsePart[0]
                    .getComponent();
            
            TCComponentItemRevision itemRev = createdPart
                    .getLatestItemRevision();
            TCComponent attachedDocuments = itemRev
                    .getRelatedComponent(NationalOilwell.RELATED_DEFINING_DOCUMENT);
            
            TCComponent document = createItemResponseDocument[0]
                    .getComponent();
            
            itemRev.remove(NationalOilwell.RELATED_DEFINING_DOCUMENT,
                    attachedDocuments);
            
            itemRev.add(NationalOilwell.RELATED_DEFINING_DOCUMENT, document);
            itemRev.lock();
            itemRev.save();
            itemRev.unlock();
        }
    }
	
	
	  @Override
      public void update(PublishEvent event)
      {
          if(event.getPropertyName().equals(NationalOilwell.CLONE_DOC_SELECTION_EVENT))
          {
              m_cloneDocSelected = (Boolean) event.getNewValue();
          }
          
      }
	  
	  private boolean isDatasetContainsRequiredFields(IPropertyMap datasetPropertyMap)
	    {
	        return (hasContent(datasetPropertyMap.getString("dataset_type")));
	    }

	    private void copyProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
	    {
	        // fill item related attributes for document
	        String itemId = inputPropertyMap.getString("item_id");
	        String oracleId = itemId.split("-")[0];
	        outputPropertyMap.setString("item_id", oracleId);
	        
	        String itemName = inputPropertyMap.getString("object_name");
	        outputPropertyMap.setString("object_name", itemName);
	        
	        String itemDesc = inputPropertyMap.getString("object_desc");
	        outputPropertyMap.setString("object_desc", itemDesc);
	        
	    }
	    
	    private boolean isDocumentContainsRequiredFields(IPropertyMap documentPropertyMap)
	    {
	        return (hasContent(documentPropertyMap.getString("DocumentsCAT")) || hasContent(documentPropertyMap
	                .getString("DocumentsType")));
	    }
	    
	    private IPropertyMap[] preparePartMaps(IPropertyMap partMap, IPropertyMap[] dashMapArray)
	    {
	        IPropertyMap[] returnValue = dashMapArray;
	        
	        for (IPropertyMap currentMap : dashMapArray)
	        {
	            currentMap.addAll(partMap);
	        }
	        
	        return returnValue;
	    }
	    
	    
		private String itemType = "Nov4Part";
	    private IPropertyMap m_partPropertyMap;
	    protected IPropertyMap m_documentPropertyMap;
	    private IPropertyMap m_datasetPropertyMap;
	    private String m_datasetBusinessObjectName;
	    protected boolean m_shouldProcessDocument = true;
	    private boolean m_shouldProcessDataset = true;
        protected boolean m_cloneDocSelected = false;
        private IPropertyMap[] m_requiredPartMap;
}
