package com.nov.rac.item.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;


public class ItemTypePanelIterator implements PanelIterator
{
    
    public ItemTypePanelIterator(ArrayList<IUIPanel> panels, String itemType)
    {
        m_panels = (ArrayList<IUIPanel>) panels.clone();
        addItemType(itemType);
    }

    @Override
    public void addItemType(String itemType)
    {
        m_itemType = itemType;
        filterPanelsToVisitForItemType();
    }
    
    public IPropertyMap getPanelsData()
    {
      
       //IPropertyMap propertyMap = new SimplePropertyMap();
        
        //to remove the hard code
        IPropertyMap propertyMap = new CreateInObjectHelper(m_itemType,CreateInObjectHelper.OPERATION_CREATE);
        
       Iterator it = m_panels.iterator();
        while(it.hasNext())
        {
            IUIPanel panel= (IUIPanel) it.next();
            if(panel instanceof ILoadSave)
            {
                ILoadSave saveablepanel = (ILoadSave)panel;
                try
                {
                    saveablepanel.save(propertyMap);
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        
        
        return propertyMap;
    }
    
    protected void filterPanelsToVisitForItemType()
    {
        //read the configuration for m_itemType
    	 Registry registry = Registry.getRegistry("com.nov.rac.item.operations.operations");
    	 registry = Registry.getRegistry("com.nov.rac.item.panels.panels");
         String[] panelNames = NewItemCreationHelper.getConfiguration(m_itemType+NationalOilwell.DOTSEPARATOR+"DATA_PANELS_LIST",registry);
         
         if(panelNames!= null)
         {
             List<String> names =  Arrays.asList(panelNames);
             
             //keep only panels which are configured to visit for the specified type
             Iterator it = m_panels.iterator();
             while(it.hasNext())
             {
                 IUIPanel panel= (IUIPanel) it.next();
                 String panelName = panel.getClass().getSimpleName();
                 if(!names.contains(panelName))
                 {
                     //m_panels.remove(panelName);
                     it.remove();
                 }
             }
         }
    }
    
    protected ArrayList<IUIPanel> m_panels; 
    protected String m_itemType;
}
