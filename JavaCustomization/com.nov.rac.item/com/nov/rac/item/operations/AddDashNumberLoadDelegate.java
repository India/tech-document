package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class AddDashNumberLoadDelegate extends AbstractLoadDelegate
{

    public AddDashNumberLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent((TCComponent) inputObject);
    }

    public void loadComponent(TCComponent targetObject) throws TCException
    {
        IPropertyMap masterFormPropertyMap = null;
        
        TCComponentItem targetItem = GeneralItemUtils.getItemFromTCComponent(targetObject);
        
        TCComponent masterForm = targetItem.getRelatedComponent("IMAN_master_form");

        IPropertyMap partPropertyMap = new SimplePropertyMap();
        PropertyMapHelper.componentToMap(targetItem, getItemMapProperties(), partPropertyMap);
        partPropertyMap.setComponent(targetItem);
        
        masterFormPropertyMap = new SimplePropertyMap();
        PropertyMapHelper.componentToMap(masterForm, getMasterFormMapProperties(), masterFormPropertyMap);
        
        partPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
        loadPanelsData(targetItem.getType(), getInputProviders(), partPropertyMap);
    }
    
    private String[] getItemMapProperties()
    {
        String[] itemMapProperties = { NationalOilwell.ICS_subclass_name, NationalOilwell.OBJECT_NAME,
                NationalOilwell.OBJECT_DESC, NationalOilwell.ITEM_ID };
        
        return itemMapProperties;
    }
    
    private String[] getMasterFormMapProperties()
    {
        String[] masterFormMapProperties = { NationalOilwell.RSONE_ORG, NationalOilwell.RSONE_UOM,
                NationalOilwell.RSONE_LOTCONTROL, NationalOilwell.RSONE_SERIALIZE, NationalOilwell.RSONE_QAREQUIRED,
                NationalOilwell.RSONE_ENGCONTROLLED, NationalOilwell.RSONE_ITEMTYPE };
        
        return masterFormMapProperties;
    }
}
