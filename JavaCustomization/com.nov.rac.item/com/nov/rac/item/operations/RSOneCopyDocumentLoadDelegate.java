package com.nov.rac.item.operations;

import java.util.List;
import java.util.Vector;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.PartFamilySearchProvider;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class RSOneCopyDocumentLoadDelegate extends AbstractLoadDelegate
{
    
    public RSOneCopyDocumentLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.item.operations.ILoadDelegate#loadComponent(com.teamcenter
     * .rac.kernel.TCComponent)
     */
    
    public void loadComponent(TCComponent targetObject) throws TCException
    {
        TCComponentItem targetItem = null;
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        if (null != targetItem)
        {
            NewItemCreationHelper.setTargetComponent(targetItem);
            
            IPropertyMap documentPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetItem, new String[] { NationalOilwell.OBJECT_NAME,
                    NationalOilwell.ITEM_ID, NationalOilwell.DOCUMENTS_CATEGORY, NationalOilwell.DOCUMENTS_TYPE },
                    documentPropertyMap);
            
            setRDDTag(documentPropertyMap);
            
            TCComponent latestDocRevision = targetItem.getLatestItemRevision();
            TCComponent[] datasetList = latestDocRevision.getRelatedComponents("IMAN_specification");
            
            if (null != datasetList && 0 < datasetList.length)
            {
                for (TCComponent dataset : datasetList)
                {
                    if (dataset instanceof TCComponentDataset)
                    {
                        PropertyMapHelper.componentToMap(dataset, new String[] { "object_type" }, documentPropertyMap);
                        break;
                    }
                }
            }
           
            loadPanelsData(targetItem.getType(), getInputProviders(), documentPropertyMap);
            if (!documentPropertyMap.isEmpty())
            {
                loadPanelsData(NationalOilwell.DATASET_BO_TYPE, getInputProviders(), documentPropertyMap);
            }
        }
    }
    
    private void setRDDTag(IPropertyMap propertyMap) throws TCException
    {
        INOVResultSet result = null;
        
        PartFamilySearchProvider searchService = new PartFamilySearchProvider();
        result = searchService.executeQuery(searchService);
        
        Vector<String> rowData = result.getRowData();
        
        if (result != null && result.getRows() > 0)
        {
            String puid = rowData.get(0);
            
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            
            TCComponent tcComponent = tcSession.stringToComponent(puid);
            
            propertyMap.setTag("RelatedDefiningDocument", tcComponent);
            
        }
        else
        {
            propertyMap.setTag("RelatedDefiningDocument", null);
        }
    }
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent((TCComponent) inputObject);
    }
}
