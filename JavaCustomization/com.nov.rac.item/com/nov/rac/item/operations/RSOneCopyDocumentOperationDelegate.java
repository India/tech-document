package com.nov.rac.item.operations;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyDocumentOperationDelegate extends
AbstractOperationDelegate
{
    /** The document property map. */
    protected IPropertyMap m_documentPropertyMap;
    
    protected TCComponentItem m_documentObject;

    public RSOneCopyDocumentOperationDelegate(Shell theShell)
    {
        super(theShell);
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        m_documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(
                m_documentPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData( NationalOilwell.DOCUMENT_TYPE,
                        getOperationInputProviders(), 
                        m_documentPropertyMap);

        IPropertyMap[] arrayOfIPropertyMap = { m_documentPropertyMap };

        IPropertyValidator partPropertiesValidator = getValidator( NewItemCreationHelper.getSelectedOperation() );
        
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        m_documentPropertyMap.setString("item_id", NOVRSOneItemIDHelper.getOracleId());
        
        IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
        docPropertiesModifier.modifyProperties(m_documentPropertyMap);

        return true;
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }

    @Override
    public boolean baseAction() throws TCException
    {
        setTargetObject(m_documentPropertyMap);
        
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        
        documentOperationInput.setObjectProperties(m_documentPropertyMap);
        documentOperationInput
                .setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
        documentOperationInput.setContainerInfo(
                NewItemCreationHelper.getContainer(), "contents", true);
        
        Map<String, OperationInputHelper[]> createItemInput = new HashMap<String, OperationInputHelper[]>();
        
        // as of now setting mode as "PART" as SOA clones only the part
        createItemInput.put(CreateItemSOAHelper.DOCUMENT,
                new OperationInputHelper[] { documentOperationInput }); 
        
        CreateItemResponseHelper theResponse = CreateItemSOAHelper.createItem(createItemInput);
        
        theResponse.throwErrors();

        return true;
    }
    
    private void setTargetObject(IPropertyMap m_documentPropertyMap2)
    {
        if(NewItemCreationHelper.getTargetComponent() != null)
        {
            m_documentPropertyMap.setComponent(NewItemCreationHelper.getTargetComponent());
        }
        else
        {
            m_documentPropertyMap.setComponent(NewItemCreationHelper.getSelectedComponent());
        }
    }
    
}
