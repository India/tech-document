package com.nov.rac.item.operations;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.userservices.NOV4CopyNamedReferences;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov4.services.rac.custom.NOV4DataManagementService;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

/**
 * The Class CreateNewDocumentOperationDelegate.
 */
public class CreateNewDocumentOperationDelegate extends AbstractOperationDelegate implements ISubscriber
{
    
    /** The document property map. */
    protected CreateInObjectHelper documentPropertyMap;
    
    /** The dataset property map. */
    private CreateInObjectHelper datasetPropertyMap;
    
    /** The dataset business object name. */
    private String datasetBusinessObjectName = "";
    
    protected TCComponentItem documentObject;
    
    private boolean shouldProcessDataset = true;
    
    /** This Map holds all the saved properties of dataset. */
    private IPropertyMap datasetAllPropertyMap = null;
    
    //private Shell m_shell;
    
    /**
     * Instantiates the NewDocumentOperationDelegate.
     * 
     * @param theShell
     *            the the shell
     */
    public CreateNewDocumentOperationDelegate(Shell theShell)
    {
        super(theShell);
       // m_shell = theShell;
        registerSubscriber();
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.item.operations.AbstractOperationDelegate#preCondition()
     */
    @Override
    public boolean preCondition() throws TCException
    {
    	shouldProcessDataset = true;
        documentPropertyMap = new CreateInObjectHelper(NationalOilwell.DOCUMENT_TYPE,
                CreateInObjectHelper.OPERATION_CREATE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(documentPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), documentPropertyMap);
        
        datasetAllPropertyMap = new SimplePropertyMap();
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), datasetAllPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = {documentPropertyMap,datasetAllPropertyMap};
        
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
       
        
        IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
        docPropertiesModifier.modifyProperties(documentPropertyMap);
        
        
        datasetPropertyMap = new CreateInObjectHelper(datasetBusinessObjectName, CreateInObjectHelper.OPERATION_CREATE);
        copyDatasetProperties(datasetAllPropertyMap, datasetPropertyMap);
        
        if (datasetPropertyMap.isEmpty())
        {
            shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(documentPropertyMap, datasetPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(datasetPropertyMap);
        }
        return true;
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.item.operations.AbstractOperationDelegate#preAction()
     */
    @Override
    public boolean preAction() throws TCException
    {
        
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.item.operations.AbstractOperationDelegate#baseAction()
     */
    @Override
    public boolean baseAction() throws TCException
    {
        CreateInObjectHelper[] input;
        if (shouldProcessDataset)
            input = new CreateInObjectHelper[] { documentPropertyMap, datasetPropertyMap };
        else
            input = new CreateInObjectHelper[] { documentPropertyMap };
        
       // CreateObjectsSOAHelper.createUpdateObjects(input, new ControlInfo[] { ControlInfo.ROLL_BACK_FAIL_FAST });
        CreateObjectsSOAHelper.createUpdateObjects(input);
        
        //Added for TCDECREL-6363
        CreateObjectsSOAHelper.throwErrors(input);
        
        documentObject = (TCComponentItem) CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(input[0]);
        ExceptionHelper.validateObjectCreation(input, stack);
        
        if (shouldProcessDataset)
        {
            if (null != documentObject)
            {
            	//KLOC219
                processDataset(input);
            }
        }
        
        // adding newly created objects to container/folder KLOC219
        addObjectToFolder();

        return true;
    }

	private void processDataset(CreateInObjectHelper[] input)throws TCException
	{
			
		TCComponent datasetObject = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(input[1]);
		TCComponentItemRevision documentRevision = documentObject.getLatestItemRevision();
		if (datasetObject != null)
		{
		    documentRevision.add("IMAN_specification", datasetObject);
		    TCComponent selectedTemplate = datasetAllPropertyMap.getTag("selectedTemplate");
		    if (null == selectedTemplate)
		    {
		        String path = datasetAllPropertyMap.getString("datasetFilePath");
		        if (!path.equals(""))
		        {
		            try
		            {
		                DatasetHelper datasetHelper = new DatasetHelper(datasetObject, path);
		                datasetHelper.updateDataset();
		            }
		            catch (TCException exception)
		            {
		                stack.addErrors(exception);
		                throw exception;
		            }
		        }
		    }
		    else
		    {
		        attachTemplateToDataset(datasetObject, selectedTemplate);
		    }
		}
	}

	private void attachTemplateToDataset(TCComponent datasetObject,TCComponent selectedTemplate) throws TCException
	{
		NOV4CopyNamedReferences userService = new NOV4CopyNamedReferences();
		userService.addArgument(new Object[] { selectedTemplate });
		userService.addArgument(new Object[] { datasetObject });
		Object[] theStatus = (Object[]) userService.invokeUserService();
		
		for (Object status : theStatus)
		{
		    System.out.println("Template Attachment status " + status.toString());
		}
	}

	private void addObjectToFolder() throws TCException 
	{
		TCComponentFolder parentFolder = (TCComponentFolder)NewItemCreationHelper.getContainer();
        parentFolder.add("contents", documentObject, 0 );
        parentFolder.refresh();
	}
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.framework.communication.ISubscriber#update(com.nov.rac.framework
     * .communication.PublishEvent)
     */
    @Override
    public void update(PublishEvent event)
    {
        Object datasetBusinessObject = event.getNewValue();
        if (null != datasetBusinessObject)
            datasetBusinessObjectName = datasetBusinessObject.toString();
        else
            datasetBusinessObjectName = "";
        
    }
    
    /**
     * Gets the subscriber.
     * 
     * @return the subscriber
     */
    ISubscriber getSubscriber()
    {
        return (ISubscriber) this;
    }
    
    /**
     * Register subscriber.
     */
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        //IController controller = ControllerFactory.getInstance().getController(""+m_shell.hashCode());
        controller.registerSubscriber(NationalOilwell.DATASET_TYPE, getSubscriber());
    }
    
    private void copyDatasetProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("dataset_type");
        //outputPropertyMap.setString("object_name", itemName);
        PropertyMapHelper.setString("object_name", itemName, outputPropertyMap);
    }
}
