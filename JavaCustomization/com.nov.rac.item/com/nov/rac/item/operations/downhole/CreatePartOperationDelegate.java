package com.nov.rac.item.operations.downhole;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.ClassificationHelper;
import com.nov.rac.item.helpers.DatasetHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.AbstractOperationDelegate;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CompoundObjectHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class CreatePartOperationDelegate extends AbstractOperationDelegate implements ISubscriber{

	public CreatePartOperationDelegate(Shell theShell) {
		super(theShell);
		m_registry = getRegistry();
		registerSubscriber();
	}
	
	/**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
        //return Registry.getRegistry(this);
    }

	@Override
	public boolean preCondition() throws TCException
	{
		
		m_shouldProcessDataset = true;
        m_shouldProcessDocument = true;
        
        if (m_itemType == null || m_itemType.trim().isEmpty())
        {
            m_itemType = NationalOilwell.PART_BO_TYPE;
        }
        
        m_partPropertyMap = new SOAPropertyMap(m_itemType);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(m_partPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(m_itemType, getOperationInputProviders(), m_partPropertyMap);
        
        IPropertyModifier partPropertiesModifier = getModifier(m_itemType);
        partPropertiesModifier.modifyProperties(m_partPropertyMap);
        
        m_documentPropertyMap = new SOAPropertyMap(NationalOilwell.DOCUMENT_TYPE);
        CreateInCompoundHelper documentCompoundHelper = new CreateInCompoundHelper(m_documentPropertyMap);
        documentCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        documentCompoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(NationalOilwell.DOCUMENT_TYPE, getOperationInputProviders(), m_documentPropertyMap);
        
        m_datasetAllPropertyMap = new SOAPropertyMap(m_datasetBusinessObjectName);
        
        fillPanelsData(NationalOilwell.DATASET_BO_TYPE, getOperationInputProviders(), m_datasetAllPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = {m_partPropertyMap,m_documentPropertyMap,m_datasetAllPropertyMap};
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(getRegistry(), NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        if (!isDocumentContainsRequiredFields(m_documentPropertyMap))
        {
            m_shouldProcessDocument = false;
        }
        else
        {
        	copyProperties(m_partPropertyMap, m_documentPropertyMap);
        	
            IPropertyModifier docPropertiesModifier = getModifier(NationalOilwell.DOCUMENT_TYPE);
            docPropertiesModifier.modifyProperties(m_documentPropertyMap);
        }
        
        if (!isDatasetContainsRequiredFields(m_datasetAllPropertyMap))
        {
            m_shouldProcessDataset = false;
        }
        else
        {
            copyDocProperties(m_documentPropertyMap, m_datasetAllPropertyMap);
            
            IPropertyModifier datasetPropertiesModifier = getModifier(NationalOilwell.DATASET_BO_TYPE);
            datasetPropertiesModifier.modifyProperties(m_datasetAllPropertyMap);
        }
        
		return true;
	}
	
	private boolean isDocumentContainsRequiredFields(SOAPropertyMap document)
    {
        return (hasContent(document.getString("DocumentsCAT")) || hasContent(document.getString("DocumentsType")));
    }
	
	private boolean isDatasetContainsRequiredFields(SOAPropertyMap datasetPropertyMap)
    {
        return (hasContent(datasetPropertyMap.getString("dataset_type")));
    }
	
	protected void copyProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        // fill item related attributes for document
        String itemId = inputPropertyMap.getString("item_id");
        String oracleId = itemId.split("-")[0];
        outputPropertyMap.setString("item_id", oracleId);
        
        String itemName = inputPropertyMap.getString("object_name");
        outputPropertyMap.setString("object_name", itemName);
        
        String itemDesc = inputPropertyMap.getString("object_desc");
        outputPropertyMap.setString("object_desc", itemDesc);
        
        /*if(NewItemCreationHelper.getCurrentGroup().equals("3C - ReedHycalog.DHT"))
        {
	        IPropertyMap masterPropertyMap = inputPropertyMap
					.getCompound("IMAN_master_form");
			if (masterPropertyMap != null)
			{
				String[] Site = masterPropertyMap.getStringArray("Sites");
				outputPropertyMap.getCompound("IMAN_master_form").setStringArray("Sites", Site);
			}
        }*/
    }
    
    private void copyDocProperties(IPropertyMap inputPropertyMap, IPropertyMap outputPropertyMap)
    {
        String itemName = inputPropertyMap.getString("item_id");
        outputPropertyMap.setString("object_name", itemName);
    }
    
    private IPropertyMap createPropMapForIdentifier(IPropertyMap identifierPropertyMap, String itemId)
    {
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("JDE");
        
        // properties to be set here
        identifierPropertyMap.setString("object_name", itemId );
        identifierPropertyMap.setString("idfr_id", itemId );
        identifierPropertyMap.setTag("idcontext", idContext );
        identifierPropertyMap.setTag("suppl_context", null );
        
        return identifierPropertyMap;
    }
	
	private IPropertyMap createPropMapForIdentifierRev(IPropertyMap identifierRevPropertyMap, String itemId)
    {
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("JDE");
        
        identifierRevPropertyMap.setString("object_name", itemId );
        identifierRevPropertyMap.setString("idfr_id", "01" );
        identifierRevPropertyMap.setTag("idcontext", idContext );
        
        return identifierRevPropertyMap;
    }
	
	/*private IPropertyMap createPropMapForClassification(IPropertyMap classificationPropertyMap, IPropertyMap requiredPartMaps)
    {
        String className = requiredPartMaps.getString(NationalOilwell.ICS_subclass_name);
        
        classificationPropertyMap.setString("cid", classificationCIDMap.get(className));
        classificationPropertyMap.setInteger("AttrID",new Integer("-630"));
        classificationPropertyMap.setStringArray("AttrValues",new String[] {"0","1","2"} );
        return classificationPropertyMap;
    }*/
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.DH_DATASET_TYPE))
        {
            m_datasetBusinessObjectName = (String) event.getNewValue();
        }
        
        if (event.getPropertyName().equals(NationalOilwell.SELECTEDCLASS))
        {
            String selPath = (String) event.getNewValue();
            
            boolean contains = selPath.contains(MANUFACTURING);
            
            if (contains == true)
            {
            	m_itemType = NationalOilwell.NON_ENGINEERING;
            }
            
        }
       
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.DH_DATASET_TYPE, getSubscriber());
        
        controller.registerSubscriber(NationalOilwell.SELECTEDCLASS, getSubscriber());
    }
    
    private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.unregisterSubscriber(NationalOilwell.DH_DATASET_TYPE, getSubscriber());
        
        controller.unregisterSubscriber(NationalOilwell.SELECTEDCLASS, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }

	@Override
	public boolean preAction() throws TCException
	{
		return true;
	}

	@Override
	public boolean baseAction() throws TCException {
		OperationInputHelper operationInput = new OperationInputHelper();
		
		/*// Start : Classification
		classificationCIDMap = ClassificationHelper.getClassificationMap();
        classificationUIDMap = ClassificationHelper.getClassificationUIDMap();
        OperationInputHelper classificationInput = new OperationInputHelper();
        // End : Classification*/
		
		// Start : alternate id
		OperationInputHelper alternateIDOperationInput = new OperationInputHelper();
        OperationInputHelper alternateRevIDOperationInput = new OperationInputHelper();
        List<OperationInputHelper> alternateIDCreateList = new ArrayList<OperationInputHelper>();
        OperationInputHelper[] alternateIDCreateInput = new OperationInputHelper[2];
        // End : alternate id
        
        operationInput.setObjectProperties(m_partPropertyMap);
        operationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        operationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        // Start : alternate id for item
        alternateIDOperationInput = new OperationInputHelper();
        alternateIDOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap identifierPropertyMap = new SOAPropertyMap("Identifier");
        
        /*String itemId = partPropertyMap.getString("item_id");
        int reqItemIdLen = itemId.length() - 3;
        String reqItemId = itemId.substring(0, reqItemIdLen);*/
        
        createPropMapForIdentifier(identifierPropertyMap, m_partPropertyMap.getString("item_id"));
        //createPropMapForIdentifier(identifierPropertyMap, reqItemId);
        alternateIDOperationInput.setObjectProperties(identifierPropertyMap);
        
        alternateIDCreateList.add(alternateIDOperationInput);
        // End : alternate id for item
        
        // Start : alternate id for item rev
        alternateRevIDOperationInput = new OperationInputHelper();
        alternateRevIDOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap identifierRevPropertyMap = new SOAPropertyMap("IdentifierRev");
        createPropMapForIdentifierRev(identifierRevPropertyMap, m_partPropertyMap.getString("item_id"));
        //createPropMapForIdentifierRev(identifierRevPropertyMap, reqItemId);
        alternateRevIDOperationInput.setObjectProperties(identifierRevPropertyMap);
        
        alternateIDCreateList.add(alternateRevIDOperationInput);
        // End : alternate id for item rev
        
        /*// Start : to classify items
        classificationInput = new OperationInputHelper();
        classificationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap classificationPropertyMap = new SOAPropertyMap();
        createPropMapForClassification(classificationPropertyMap, m_partPropertyMap);
        classificationInput.setObjectProperties(classificationPropertyMap);
        // End : to classify items*/
        
        alternateIDCreateInput = alternateIDCreateList.toArray(alternateIDCreateInput);
        
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        
        createInputMap.put(CreateItemSOAHelper.PART, new OperationInputHelper[] { operationInput });
        
        createInputMap.put(CreateItemSOAHelper.ALTERNATE_ID, alternateIDCreateInput);
        
        /*createInputMap.put(CreateItemSOAHelper.CLASSIFICATION,   new OperationInputHelper[] { classificationInput });*/
        
        OperationInputHelper documentOperationInput = new OperationInputHelper();
        documentOperationInput.setObjectProperties(m_documentPropertyMap);
        documentOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        documentOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        OperationInputHelper datasetOperationInput = new OperationInputHelper();
      
        
	    // Start : For Named References (File Template)
		TCComponent selectedTemplate = m_datasetAllPropertyMap
				.getTag("selectedTemplate");
		if (null != selectedTemplate) {
			datasetOperationInput
					.setOperationMode(OperationInputHelper.OPERATION_SAVEAS_OBJECT);
			m_datasetAllPropertyMap.setComponent(selectedTemplate);
	
		} else {
			datasetOperationInput
					.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
		}
		// End : For Named References (File Template)
		  datasetOperationInput.setObjectProperties(m_datasetAllPropertyMap);
        datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        OperationInputHelper relationOperationInput = new OperationInputHelper();
        IPropertyMap relationPropertyMap = new SOAPropertyMap("RelatedDefiningDocument");
        relationOperationInput.setObjectProperties(relationPropertyMap);
        relationOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        
        if (m_shouldProcessDocument)
        {
            createInputMap.put(CreateItemSOAHelper.DOCUMENT, new OperationInputHelper[] { documentOperationInput });
            createInputMap.put(CreateItemSOAHelper.PART_DOC_RELATION, new OperationInputHelper[] { relationOperationInput });
            
        }
        
        if (m_shouldProcessDataset)
        {
            createInputMap.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
        }
        theResponse = CreateItemSOAHelper.createItem(createInputMap);
        
        return true;
	}
	
	@Override
	public boolean postAction() throws TCException
	{
		// Start : For Named References (Import File)

		if (m_shouldProcessDataset) {

			TCComponent selectedTemplate = m_datasetAllPropertyMap
					.getTag("selectedTemplate");

			if (null == selectedTemplate) {
				CompoundObjectHelper[] createItemResponseDataset = theResponse
						.getCompoundObject(CreateItemResponseHelper.DATASET);

				TCComponent dataset = createItemResponseDataset[0]
						.getComponent();

				String path = m_datasetAllPropertyMap
						.getString("datasetFilePath");
				if (!path.equals("")) {
					try {
						DatasetHelper datasetHelper = new DatasetHelper(
								dataset, path);
						datasetHelper.updateDataset();
					} catch (TCException exception) {
						stack.addErrors(exception);
						throw exception;
					}
				}

			}
		}

		// End : For Named References (Import File)

		return true;
	}
	
	@Override
    protected void cleanUp()
    {
        super.cleanUp();
        unRegisterSubscriber();
        
    }
	
	protected Registry m_registry = null;
	private String m_itemType = null;
    private SOAPropertyMap m_documentPropertyMap;
    private SOAPropertyMap m_datasetAllPropertyMap;
    private SOAPropertyMap m_partPropertyMap;
    private boolean m_shouldProcessDataset;
    private boolean m_shouldProcessDocument;
    private String m_datasetBusinessObjectName;
    private final static String MANUFACTURING = "Manufacturing";
    private CreateItemResponseHelper theResponse;
    private Map<String,String> classificationCIDMap = new HashMap<String, String>();
    private Map<String,String> classificationUIDMap = new HashMap<String, String>();

}
