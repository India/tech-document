package com.nov.rac.item.operations.downhole;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.AbstractOperationDelegate;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class CreatePartForRDDOperationDelegate extends
		AbstractOperationDelegate 
{

	public CreatePartForRDDOperationDelegate(Shell theShell) 
	{
		super(theShell);
		m_registry = getRegistry();
		m_targetObject = NewItemCreationHelper.getTargetComponent();
	}
	
	/**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }

	@Override
	public boolean preCondition() throws TCException 
	{
		if (m_itemType == null || m_itemType.trim().isEmpty())
        {
            m_itemType = "Nov4Part";
        }
        
        m_partPropertyMap = new SOAPropertyMap(m_itemType);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(m_partPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(m_itemType, getOperationInputProviders(), m_partPropertyMap);
        
        IPropertyModifier partPropertiesModifier = getModifier(m_itemType);
        partPropertiesModifier.modifyProperties(m_partPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = {m_partPropertyMap};
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(getRegistry(), NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
		
		return true;
	}

	@Override
	public boolean preAction() throws TCException 
	{
		return true;
	}

	@Override
	public boolean baseAction() throws TCException 
	{
		TCComponentItem targetItem = null;
		
		if(m_targetObject instanceof TCComponentItem)
		{
			targetItem = (TCComponentItem) m_targetObject;
		}
		else if(m_targetObject instanceof TCComponentItemRevision)
		{
			targetItem = ((TCComponentItemRevision) m_targetObject).getItem();
		}
		
		OperationInputHelper partOperationInput = new OperationInputHelper();
		partOperationInput.setObjectProperties(m_partPropertyMap);
		partOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);	
		partOperationInput.setContainerInfo(NewItemCreationHelper.getContainer() , "contents", true);
		
		IPropertyMap documentPropertyMap = new SOAPropertyMap(CreateItemSOAHelper.DOCUMENT);
		documentPropertyMap.setComponent(targetItem);
		
		OperationInputHelper documentOperationInput = new OperationInputHelper();
		documentOperationInput.setOperationMode(OperationInputHelper.OPERATION_UPDATE_OBJECT);
		documentOperationInput.setObjectProperties(documentPropertyMap);
        
        OperationInputHelper relationOperationInput = new OperationInputHelper();
        IPropertyMap relationPropertyMap = new SOAPropertyMap("RelatedDefiningDocument");
        relationOperationInput.setObjectProperties(relationPropertyMap);
        relationOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        
        Map<String, OperationInputHelper[]> createItemInput = new HashMap<String, OperationInputHelper[]>();
        
        createItemInput.put(CreateItemSOAHelper.PART, new OperationInputHelper[] { partOperationInput });
        createItemInput.put(CreateItemSOAHelper.DOCUMENT, new OperationInputHelper[] { documentOperationInput });
        createItemInput.put(CreateItemSOAHelper.PART_DOC_RELATION, new OperationInputHelper[] { relationOperationInput });
		
        CreateItemResponseHelper theResponse = CreateItemSOAHelper.createItem(createItemInput);
        
        theResponse.throwErrors();
        
        return true;
	}

	private Registry m_registry = null;
	private String m_itemType = null;
	private TCComponent m_targetObject = null;
	protected IPropertyMap m_partPropertyMap;

}
