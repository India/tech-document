package com.nov.rac.item.operations.downhole;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.utilities.services.createItemHelper.CreateItemResponseHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class CreateRDDOperationDelegate extends CreateNewDocumentOperationDelegate 
{

	public CreateRDDOperationDelegate(Shell theShell) 
	{
		super(theShell);
		m_targetObject = NewItemCreationHelper.getTargetComponent();
		registerSubscriber();
	}

	@Override
	public void update(PublishEvent event) 
	{
		super.update(event);
		if(event.getPropertyName().equals(NationalOilwell.SELECTED_ITEM_MODIFY_EVENT))
		{
			if(NewItemCreationHelper.getSelectedComponent() != null)
			{
				m_targetObject = NewItemCreationHelper.getSelectedComponent();
			}
		}
	}

	@Override
	public boolean baseAction() throws TCException 
	{
		TCComponentItem targetItem = null;
		
		if(m_targetObject instanceof TCComponentItem)
		{
			targetItem = (TCComponentItem) m_targetObject;
		}
		else if(m_targetObject instanceof TCComponentItemRevision)
		{
			targetItem = (TCComponentItem) m_targetObject;
		}
		
		IPropertyMap partPropertyMap = new SOAPropertyMap(CreateItemSOAHelper.PART);
		partPropertyMap.setComponent(targetItem);
		
		OperationInputHelper documentOperationInput = new OperationInputHelper();
		documentOperationInput.setObjectProperties(m_documentPropertyMap);
		documentOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);	
		documentOperationInput.setContainerInfo(NewItemCreationHelper.getContainer() , "contents", true);
		
		OperationInputHelper partOperationInput = new OperationInputHelper();
		partOperationInput.setOperationMode(OperationInputHelper.OPERATION_UPDATE_OBJECT);
		partOperationInput.setObjectProperties(partPropertyMap);
		
		OperationInputHelper datasetOperationInput = new OperationInputHelper();
        datasetOperationInput.setObjectProperties(m_datasetAllPropertyMap);
        datasetOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        datasetOperationInput.setContainerInfo(NewItemCreationHelper.getContainer(), "contents", true);
        
        OperationInputHelper relationOperationInput = new OperationInputHelper();
        IPropertyMap relationPropertyMap = new SOAPropertyMap("RelatedDefiningDocument");
        relationOperationInput.setObjectProperties(relationPropertyMap);
        relationOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        
        Map<String, OperationInputHelper[]> createItemInput = new HashMap<String, OperationInputHelper[]>();
        
        createItemInput.put(CreateItemSOAHelper.PART, new OperationInputHelper[] { partOperationInput });
        createItemInput.put(CreateItemSOAHelper.DOCUMENT, new OperationInputHelper[] { documentOperationInput });
        createItemInput.put(CreateItemSOAHelper.PART_DOC_RELATION, new OperationInputHelper[] { relationOperationInput });
        
        if (m_shouldProcessDataset)
        {
            createItemInput.put(CreateItemSOAHelper.DATASET, new OperationInputHelper[] { datasetOperationInput });
        }
        
        CreateItemResponseHelper theResponse = CreateItemSOAHelper.createItem(createItemInput);
        
        theResponse.throwErrors();
        
        return true;
		
	}
	
	@Override
	public boolean postAction() throws TCException 
	{
		unRegisterSubscriber();
		return true;
	}

	private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber(NationalOilwell.SELECTED_ITEM_MODIFY_EVENT , this);
    }
	
	private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.unregisterSubscriber(NationalOilwell.SELECTED_ITEM_MODIFY_EVENT , this);
    }

	private TCComponent m_targetObject = null;
}
