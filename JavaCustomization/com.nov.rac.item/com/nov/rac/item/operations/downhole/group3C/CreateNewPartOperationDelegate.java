package com.nov.rac.item.operations.downhole.group3C;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.item.operations.downhole.CreatePartOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;

public class CreateNewPartOperationDelegate extends CreatePartOperationDelegate {

	public CreateNewPartOperationDelegate(Shell theShell)
	{
		super(theShell);
	}
	
	@Override
	protected void copyProperties(IPropertyMap inputPropertyMap,
			IPropertyMap outputPropertyMap)
	{
		super.copyProperties(inputPropertyMap, outputPropertyMap);
		
		IPropertyMap masterPropertyMap = inputPropertyMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null)
		{
			String[] Site = masterPropertyMap.getStringArray("Sites");
			outputPropertyMap.getCompound("IMAN_master_form").setStringArray("Sites", Site);
		}
	}

}
