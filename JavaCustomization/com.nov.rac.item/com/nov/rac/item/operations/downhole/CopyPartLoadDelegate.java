package com.nov.rac.item.operations.downhole;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.item.operations.AbstractLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class CopyPartLoadDelegate extends AbstractLoadDelegate 
{

	public CopyPartLoadDelegate(List<IUIPanel> inputProviders) 
	{
		super(inputProviders);
	}

	@Override
	public void loadComponent(Object inputObject) throws TCException 
	{
		loadComponent( (TCComponent) inputObject);
	}
	
	 public void loadComponent(TCComponent targetObject) throws TCException
    {
    
        TCComponentItem targetItem = null;
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        
        if (null != targetItem)
        {
            
            IPropertyMap partPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap( targetItem, 
                                              new String[] { NationalOilwell.TYPE,
            												 NationalOilwell.ICS_SUBCLASS_NAME,
                                                             NationalOilwell.ITEM_ID,
                                                             NationalOilwell.NOV_UOM,
                                                             NationalOilwell.OBJECT_NAME,
                                                             NationalOilwell.OBJECT_DESC }, 
                                              partPropertyMap
                                             );
            
            IPropertyMap masterFormPropertyMap = new SimplePropertyMap();
            TCComponent masterForm = targetItem.getRelatedComponent("IMAN_master_form");
            PropertyMapHelper.componentToMap( masterForm, 
                                              new String[] { NationalOilwell.SITES,
                                                             NationalOilwell.NAME2 },
                                              masterFormPropertyMap
                                             );
            
            partPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
            
            IPropertyMap documentPropertyMap = new SimplePropertyMap();
            TCComponent[] partRevisionList = targetItem.getRelatedComponents("revision_list");
            TCComponent[] documentList = partRevisionList[0].getRelatedComponents("RelatedDefiningDocument");
            
            IPropertyMap datasetPropertyMap = new SimplePropertyMap();
            
            if (null != documentList && 0 < documentList.length)
            {
                
                PropertyMapHelper.componentToMap( documentList[0], 
                                                  new String[] { NationalOilwell.DOCUMENTS_CATEGORY, 
                                                                 NationalOilwell.DOCUMENTS_TYPE }, 
                                                  documentPropertyMap
                                                 );
                
                TCComponent docMasterForm = documentList[0].getRelatedComponent("IMAN_master_form");
                
                IPropertyMap docMasterFormPropertyMap = new SimplePropertyMap();
                PropertyMapHelper.componentToMap( docMasterForm, 
                                                  new String[] { NationalOilwell.PULL_DRAWING }, 
                                                  docMasterFormPropertyMap
                                                 );
                
                documentPropertyMap.setCompound("IMAN_master_form", docMasterFormPropertyMap);
                
                TCComponent[] docRevisionList = documentList[0].getRelatedComponents("revision_list");
                TCComponent[] datasetList = docRevisionList[0].getRelatedComponents("IMAN_specification");
                
                if (null != datasetList && 0 < datasetList.length)
                {
                    PropertyMapHelper.componentToMap( datasetList[0], 
                                                      new String[] { NationalOilwell.OBJECT_TYPE },
                                                      datasetPropertyMap
                                                     );
                }
            }
            
            loadPanelsData(targetItem.getType(), getInputProviders(), partPropertyMap);
            
            if (!documentPropertyMap.isEmpty())
            {
                loadPanelsData(NationalOilwell.DOCUMENT_TYPE, getInputProviders(), documentPropertyMap);
            }
            
            if (!datasetPropertyMap.isEmpty())
            {
                loadPanelsData(NationalOilwell.DATASET_BO_TYPE, getInputProviders(), datasetPropertyMap);
            }
            
        }
    }

}
