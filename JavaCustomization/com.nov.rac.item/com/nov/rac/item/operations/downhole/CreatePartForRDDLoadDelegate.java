package com.nov.rac.item.operations.downhole;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.item.operations.AbstractLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class CreatePartForRDDLoadDelegate extends AbstractLoadDelegate 
{

	public CreatePartForRDDLoadDelegate(List<IUIPanel> inputProviders) 
	{
		super(inputProviders);
	}

	@Override
	public void loadComponent(Object inputObject) throws TCException 
	{
		loadComponent( (TCComponent) inputObject);
	}
	
	public void loadComponent(TCComponent targetObject) throws TCException
	{
		TCComponentItem targetItem = null;
	    if (targetObject instanceof TCComponentItem)
	    {
	        targetItem = (TCComponentItem) targetObject;
	    }
	    
	    if (targetObject instanceof TCComponentItemRevision)
	    {
	        targetItem = ((TCComponentItemRevision) targetObject).getItem();
	    }
	    
	    if (null != targetItem)
	    {
	        
	        IPropertyMap partPropertyMap = new SimplePropertyMap();
	        PropertyMapHelper.componentToMap( targetItem, 
	                                          new String[] { NationalOilwell.OBJECT_NAME,
	                                                         NationalOilwell.OBJECT_DESC }, 
	                                          partPropertyMap
	                                         );
	        
	        IPropertyMap masterFormPropertyMap = new SimplePropertyMap();
	        TCComponent masterForm = targetItem.getRelatedComponent("IMAN_master_form");
	        PropertyMapHelper.componentToMap( masterForm, 
	                                          new String[] { NationalOilwell.SITES },
	                                          masterFormPropertyMap
	                                         );
	        
	        partPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
	        
	        loadPanelsData(NationalOilwell.PART_BO_TYPE, getInputProviders(), partPropertyMap);
	    }
	}

}
