package com.nov.rac.item.operations;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class RSOneRelatedDocumentOperationDelegate extends
        RSOneCreateNewDocumentOperationDelegate implements ISubscriber
{
    
    public RSOneRelatedDocumentOperationDelegate(Shell theShell)
    {
        super(theShell);
    }
    
    @Override
    protected void addContainerInfo(OperationInputHelper operationInput)
    {
        TCComponent targetObject = NewItemCreationHelper.getTargetComponent();
        TCComponentItemRevision targetItem = null;
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = (TCComponentItemRevision) targetObject;
        }
        else if(targetObject instanceof TCComponentItem)
        {
            try
            {
                targetItem = ((TCComponentItem) targetObject).getLatestItemRevision();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        operationInput.setContainerInfo(targetItem, "RelatedDocuments", true);
    }
    
}
