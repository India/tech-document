package com.nov.rac.item.operations;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.ClassificationHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.services.createItemHelper.CreateItemSOAHelper;
import com.nov.rac.utilities.services.createItemHelper.OperationInputHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdToRsoneLegacyPartOPerationDelegate extends AbstractOperationDelegate implements ISubscriber
{
    private String m_itemType = "Nov4Part";
    private IPropertyMap m_partPropertyMap;
    
    private Map<String, String> m_classificationMap = new HashMap<String, String>();
    private boolean m_sendToClassification = false;
    
    TCComponentItem m_itemTag = null;
    TCComponentItemRevision m_itemRevTag = null;
    
    private String m_oldOtemId = null;
    
    public AssignOracleIdToRsoneLegacyPartOPerationDelegate(Shell theShell)
    {
        super(theShell);
        // TODO Auto-generated constructor stub
        registerSubscriber();
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        if (m_itemType == null || m_itemType.trim().isEmpty())
        {
            m_itemType = "Nov4Part";
        }
        
        m_partPropertyMap = new SOAPropertyMap(m_itemType);
        CreateInCompoundHelper partCompoundHelper = new CreateInCompoundHelper(m_partPropertyMap);
        
        partCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
        partCompoundHelper.addCompound("ItemRevision", "revision");
        
        fillPanelsData(m_itemType, getOperationInputProviders(), m_partPropertyMap);
        
        IPropertyMap[] arrayOfIPropertyMap = { m_partPropertyMap };
        
        // validates all required field in selected operation
        IPropertyValidator partPropertiesValidator = getValidator(NewItemCreationHelper.getSelectedOperation());
        partPropertiesValidator.validateProperties(arrayOfIPropertyMap);
        
        return true;
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        TCComponent targetComp = NewItemCreationHelper.getTargetComponent();
        getItemAndRevTags(targetComp);
        
        m_partPropertyMap.setComponent(m_itemTag);
        
        // While assigning oracle id to legacy part, legacy part id should not
        // be modified.
        String alternateId = m_partPropertyMap.getString(NationalOilwell.ITEM_ID);
        String objectName = m_partPropertyMap.getString(NationalOilwell.OBJECT_NAME);
        
        // this is to maintain item_id of legacy part.
        m_partPropertyMap.setString(NationalOilwell.ITEM_ID, m_oldOtemId);
        
        OperationInputHelper operationInput = new OperationInputHelper();
        operationInput.setOperationMode(OperationInputHelper.OPERATION_UPDATE_OBJECT);
        
        operationInput.setObjectProperties(m_partPropertyMap);
        
        Map<String, OperationInputHelper[]> createInputMap = new HashMap<String, OperationInputHelper[]>();
        createInputMap.put(CreateItemSOAHelper.PART, new OperationInputHelper[] { operationInput });
        
        // alternate id for item
        OperationInputHelper alternateIDOperationInput = new OperationInputHelper();
        alternateIDOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        
        IPropertyMap identifierPropertyMap = new SOAPropertyMap("Identifier");
        
        createPropMapForIdentifier(identifierPropertyMap, alternateId, objectName, m_itemTag);
        
        alternateIDOperationInput.setObjectProperties(identifierPropertyMap);
        
        // alternate id for item rev
        OperationInputHelper alternateIDRevOperationInput = new OperationInputHelper();
        alternateIDRevOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        
        IPropertyMap identifierRevPropertyMap = new SOAPropertyMap("IdentifierRev");
        
        createPropMapForIdentifierRev(identifierRevPropertyMap, alternateId, objectName, m_itemRevTag);
        
        alternateIDRevOperationInput.setObjectProperties(identifierRevPropertyMap);
        
        createInputMap.put(CreateItemSOAHelper.ALTERNATE_ID, new OperationInputHelper[] { alternateIDOperationInput,
                alternateIDRevOperationInput });
        
        // to classify
        m_classificationMap = ClassificationHelper.getClassificationMap();
        
        OperationInputHelper classificationInput = new OperationInputHelper();
        classificationInput = new OperationInputHelper();
        
        classificationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap classificationPropertyMap = new SOAPropertyMap();
        
        createPropMapForClassification(classificationPropertyMap, m_partPropertyMap);
        classificationInput.setObjectProperties(classificationPropertyMap);
        
        createInputMap.put(CreateItemSOAHelper.CLASSIFICATION, new OperationInputHelper[] { classificationInput });
        // to apply status to items
        OperationInputHelper statusOperationInput = new OperationInputHelper();
        
        statusOperationInput.setOperationMode(OperationInputHelper.OPERATION_CREATE_OBJECT);
        IPropertyMap statusPropertyMap = new SOAPropertyMap("ReleaseStatus");
        
        statusPropertyMap.setString("name", "LEGACY");
        
        statusOperationInput.setObjectProperties(statusPropertyMap);
        
        createInputMap.put(CreateItemSOAHelper.STATUS, new OperationInputHelper[] { statusOperationInput });
        
        CreateItemSOAHelper.createItem(createInputMap);
        
        return true;
    }
    
    private void getItemAndRevTags(TCComponent targetComponent)
    {
        try
        {
            if (targetComponent instanceof TCComponentItem)
            {
                m_itemTag = (TCComponentItem) targetComponent;
                m_itemRevTag = ((TCComponentItem) targetComponent).getLatestItemRevision();
            }
            else if (targetComponent instanceof TCComponentItemRevision)
            {
                m_itemRevTag = (TCComponentItemRevision) targetComponent;
                m_itemTag = m_itemRevTag.getItem();
            }
        }
        catch (TCException te)
        {
            te.printStackTrace();
        }
    }
    
    private IPropertyMap createPropMapForClassification(IPropertyMap classificationPropertyMap,
            IPropertyMap partPropertyMap)
    {
        String className = partPropertyMap.getString(NationalOilwell.ICS_subclass_name);
        
        classificationPropertyMap.setString("cid", m_classificationMap.get(className));
        classificationPropertyMap.setInteger("AttrID", new Integer("-630"));
        classificationPropertyMap.setStringArray("AttrValues", new String[] { "0", "1", "2" });
        
        return classificationPropertyMap;
    }
    
    private IPropertyMap createPropMapForIdentifier(IPropertyMap identifierPropertyMap, String alternateId,
            String objectName, TCComponent objectTag)
    {
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("RSOne");
        
        identifierPropertyMap.setString("object_name", objectName);
        identifierPropertyMap.setString("idfr_id", alternateId);
        
        identifierPropertyMap.setTag("idcontext", idContext);
        identifierPropertyMap.setTag("suppl_context", null);
        
        identifierPropertyMap.setTag("altid_of", m_itemTag);
        
        return identifierPropertyMap;
    }
    
    private IPropertyMap createPropMapForIdentifierRev(IPropertyMap identifierRevPropertyMap, String alternateId,
            String objectName, TCComponent objectTag)
    {
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("RSOne");
        
        identifierRevPropertyMap.setString("object_name", objectName);
        identifierRevPropertyMap.setString("idfr_id", "01");
        
        identifierRevPropertyMap.setTag("idcontext", idContext);
        identifierRevPropertyMap.setTag("altid_of", m_itemRevTag);
        
        return identifierRevPropertyMap;
    }
    
    @Override
    public boolean postAction() throws TCException
    {
        // TODO Auto-generated method stub
        if (m_sendToClassification && m_itemRevTag != null)
        {
            IPerspectiveDef persDef = PerspectiveDefHelper
            
            .getPerspectiveDef("com.teamcenter.rac.classification.icm.ClassificationPerspective");
            
            InterfaceAIFComponent[] component = new InterfaceAIFComponent[] { m_itemRevTag };
            
            persDef.openPerspective(component);
        }
        
        return true;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.SEND_TO_CLASSIFICATION))
        {
            m_sendToClassification = (Boolean) event.getNewValue();
        }
        
        if (event.getPropertyName().equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL))
        {
            m_oldOtemId = event.getOldValue().toString();
        }
        
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.SEND_TO_CLASSIFICATION, this);
        controller.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, this);
    }
    
    private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.unregisterSubscriber(NationalOilwell.SEND_TO_CLASSIFICATION, this);
    }
    
    @Override
    protected void cleanUp()
    {
        // TODO Auto-generated method stub
        super.cleanUp();
        unRegisterSubscriber();
    }
    
}
