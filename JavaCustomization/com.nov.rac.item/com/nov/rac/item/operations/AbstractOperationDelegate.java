package com.nov.rac.item.operations;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyModifiers.IPropertyModifier;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.InterfaceExceptionStack;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractOperationDelegate implements IOperationDelegate
{
    protected ExceptionStack stack = new ExceptionStack();
    
    public AbstractOperationDelegate(Shell theShell)
    {
        m_shell = theShell;
    }
    
    @Override
    abstract public boolean preCondition() throws TCException;
    
    @Override
    abstract public boolean preAction() throws TCException;
    
    @Override
    abstract public boolean baseAction() throws TCException;
    
    @Override
    public boolean postAction() throws TCException
    {
        // final TCComponent container = NewItemCreationHelper.getContainer();
        // if (null != container)
        // {
        // Display.getDefault().asyncExec(new Runnable()
        // {
        //
        // @Override
        // public void run()
        // {
        // try
        // {
        // container.refresh();
        // }
        // catch (TCException e)
        // {
        // e.printStackTrace();
        // }
        //
        // }
        // });
        // }
        return true;
    }
    
    protected void runWithProgress(final IProgressMonitor monitor)
    {
        final Registry registry = Registry.getRegistry(this);
        monitor.beginTask(registry.getString("Operation.MESSAGE"), 10000);
        Display.getDefault().syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    monitor.subTask(registry.getString("Precondition.MESSAGE"));
                    success = preCondition();
                    if (success)
                    {
                        monitor.subTask(registry.getString("CreateObject.MESSAGE"));
                        success = preAction();
                        
                        if (success)
                        {
                            monitor.subTask("Executing baseAction");
                            success = baseAction();
                            if (success)
                            {
                                monitor.subTask("Executing postAction");
                                success = postAction();
                            }
                        }
                    }
                    monitor.done();
                }
                catch (TCException exception)
                {
                    monitor.setCanceled(true);
                    monitor.done();
                    
                    success = false;
                    int[] errorSeverities;
                    try
                    {
                        errorSeverities = stack.getErrorSeverities();
                    }
                    catch (NullPointerException e)
                    {
                        errorSeverities = null;
                    }
                    if (errorSeverities != null && errorSeverities.length > 0
                            && errorSeverities[0] != InterfaceExceptionStack.SEVERITY_ERROR)
                    {
                        success = true;
                        TCException excep = new TCException(stack.getErrorCodes(), stack.getErrorSeverities(), stack
                                .getErrorStack());
                        MessageBox.post(m_shell, null, excep, true, "Information", MessageBox.INFORMATION, true, null);
                    }
                    else
                    {
                       // MessageBox.post(exception); TCDECREL-6750
                    	 MessageBox.post(null, exception, registry.getString("Error.TITLE_MSG"), MessageBox.ERROR);
                        
                    }
                }
                finally
                {
                    refreshContainer();
                }
            }
        });
    }
    
    @Override
    public boolean executeOperation()
    {
        stack.clear(); // To clear the error stack captured in previous
                       // executeOperation() call
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if (display != null)
        {
            display.syncExec((new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        createProgressBar();
                    }
                    catch (InvocationTargetException exp)
                    {
                        Throwable theCause = exp.getCause();
                        MessageBox.post(theCause);
                    }
                    catch (InterruptedException exp)
                    {
                        Throwable theCause = exp.getCause();
                        MessageBox.post(theCause);
                    }
                }
            }));
        }
        
        if (success)
        {
            cleanUp();
        }
        
        return success;
    }
    
    protected void cleanUp()
    {
    }
    
    /**
     * Gets the filtered panels.
     * 
     * @param itemType
     *            the item type - by which given panels will be filtered
     * @param panels
     *            the panels
     * @return the filtered panels
     */
    protected final List<IUIPanel> getFilteredPanels(String itemType, List<IUIPanel> panels)
    {
        /*
         * String operationName = NewItemCreationHelper.getSelectedOperation();
         * StringBuffer contextString = new StringBuffer();
         * contextString.append(
         * itemType).append(NationalOilwell.DOTSEPARATOR).append
         * ("DATA_PANELS_LIST"); String[] panelNames =
         * NewItemCreationHelper.getConfiguration(contextString.toString());
         * if(panelNames == null && operationName != null &&
         * !operationName.isEmpty()) { contextString.insert(0, operationName +
         * NationalOilwell.DOTSEPARATOR); panelNames =
         * NewItemCreationHelper.getConfiguration(contextString.toString()); }
         * List<IUIPanel> panelList = new ArrayList<IUIPanel>(); if (panelNames
         * != null) { List<String> names = Arrays.asList(panelNames); for
         * (IUIPanel panel : panels) { if
         * (names.contains(panel.getClass().getSimpleName())) {
         * panelList.add(panel); } } } return panelList;
         */
        
        /*
         * String operationName = NewItemCreationHelper.getSelectedOperation();
         * StringBuffer contextString = new StringBuffer(); if(operationName !=
         * null && !operationName.isEmpty()) {
         * contextString.append(operationName
         * ).append(NationalOilwell.DOTSEPARATOR); }
         * contextString.append(itemType
         * ).append(NationalOilwell.DOTSEPARATOR).append("DATA_PANELS_LIST");
         * String[] panelNames =
         * NewItemCreationHelper.getConfiguration(contextString.toString());
         * List<IUIPanel> panelList = new ArrayList<IUIPanel>(); if (panelNames
         * != null) { List<String> names = Arrays.asList(panelNames); for
         * (IUIPanel panel : panels) { if
         * (names.contains(panel.getClass().getSimpleName())) {
         * panelList.add(panel); } } } return panelList;
         */
        
        String operationName = NewItemCreationHelper.getSelectedOperation();
        StringBuffer contextString = new StringBuffer();
        
        if (operationName != null && !operationName.isEmpty())
        {
            contextString.append(operationName).append(NationalOilwell.DOTSEPARATOR);
        }
        
        contextString.append(itemType).append(NationalOilwell.DOTSEPARATOR).append("DATA_PANELS_LIST");
        
        String[] panelNames = NewItemCreationHelper.getConfiguration(contextString.toString());
        
        if (panelNames == null)
        {
            contextString = new StringBuffer();
            contextString.append(itemType).append(NationalOilwell.DOTSEPARATOR).append("DATA_PANELS_LIST");
            panelNames = NewItemCreationHelper.getConfiguration(contextString.toString());
        }
        
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        
        return panelList;
        
    }
    
    /**
     * Fill panels data into given property map.
     * 
     * @param itemType
     *            the item type
     * @param panels
     *            the panels
     * @param propertyMap
     *            the property map
     */
    protected final void fillPanelsData(String itemType, List<IUIPanel> panels, IPropertyMap propertyMap)
    {
        
        List<IUIPanel> filteredPanels = getFilteredPanels(itemType, panels);
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof ILoadSave)
            {
                ILoadSave saveablepanel = (ILoadSave) panel;
                try
                {
                    saveablepanel.save(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    protected IPropertyModifier getModifier(String itemType)
    {
        IPropertyModifier modifier = null;
        StringBuffer context = new StringBuffer();
        context.append(itemType).append(NationalOilwell.DOTSEPARATOR).append("PROPERTY_MODIFIER");
        
        Registry registry = Registry.getRegistry("com.nov.rac.item.operations.propertyModifiers.propertyModifiers");
        String[] classNames = NewItemCreationHelper.getConfiguration(context.toString(), registry);
        
        if (classNames.length > 0)
        {
            
            try
            {
                modifier = (IPropertyModifier) Instancer.newInstanceEx(classNames[0]);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return modifier;
    }
    
    @Override
    public void registerOperationInputProvider(List<IUIPanel> inputProviders)
    {
        m_OperationInputProviders = inputProviders;
    }
    
    /**
     * Gets the operation input providers.
     * 
     * @return the operation input providers
     */
    protected final List<IUIPanel> getOperationInputProviders()
    {
        return m_OperationInputProviders;
    }
    
    protected IPropertyValidator getValidator(String selectedOperation)
    {
        Registry registry = Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
        
        return getValidator(registry, selectedOperation);
    }
    
    protected IPropertyValidator getValidator(Registry theRegistry, String selectedOperation)
    {
        IPropertyValidator validator = null;
        StringBuffer context = new StringBuffer();
        context.append(selectedOperation).append(NationalOilwell.DOTSEPARATOR).append("PROPERTY_VALIDATOR");
        
        Registry registry = theRegistry; // Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
        String[] classNames = NewItemCreationHelper.getConfiguration(context.toString(), registry);
        
        if (classNames.length > 0)
        {
            try
            {
                
                validator = (IPropertyValidator) Instancer.newInstanceEx(classNames[0]);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return validator;
    }
    
    protected void refreshContainer()
    {
        final TCComponent container = NewItemCreationHelper.getContainer();
        if (null != container)
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                
                @Override
                public void run()
                {
                    try
                    {
                        container.refresh();
                    }
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }
                    
                }
            });
        }
    }
    
    protected Shell getShell()
    {
        return m_shell;
    }
    
    /**
     * @throws InvocationTargetException
     * @throws InterruptedException
     */
    protected void createProgressBar() throws InvocationTargetException, InterruptedException
    {
        /*
         * success = preCondition(); if(success) {
         */
        ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(m_shell);
        progressMonitorDialog.run(true, false, new IRunnableWithProgress()
        {
            @Override
            public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
            {
                runWithProgress(monitor);
            }
        });
    }
    
    protected List<IUIPanel> m_OperationInputProviders = null;
    private Shell m_shell;
    protected boolean success = false;
}