package com.nov.rac.item.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class OperationHelper
{
    /**
     * Gets the filtered panels.
     * 
     * @param itemType
     *            the item type - by which given panels will be filtered
     * @param panels
     *            the panels
     * @return the filtered panels
     */
    public static List<IUIPanel> getFilteredPanels(String itemType, List<IUIPanel> panels)
    {
        
        String operationName = NewItemCreationHelper.getSelectedOperation();
        StringBuffer contextString = new StringBuffer();
        
        if (operationName != null && !operationName.isEmpty())
        {
            contextString.append(operationName).append(NationalOilwell.DOTSEPARATOR);
        }
        
        contextString.append(itemType).append(NationalOilwell.DOTSEPARATOR).append("DATA_PANELS_LIST");
        
        String[] panelNames = NewItemCreationHelper.getConfiguration(contextString.toString());
        
        if (panelNames == null)
        {
            contextString = new StringBuffer();
            contextString.append(itemType).append(NationalOilwell.DOTSEPARATOR).append("DATA_PANELS_LIST");
            panelNames = NewItemCreationHelper.getConfiguration(contextString.toString());
        }
        
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        
        return panelList;
    }
    
    public static ILoadDelegate getLoadDelegate(Registry registry, List<IUIPanel> panels)
    {
        ILoadDelegate l_loadDelegate = null;
        String loadContext = null;
        loadContext = NewItemCreationHelper.getSelectedOperation() + ".LOAD_DELEGATE";
        String strOperationName[] = NewItemCreationHelper.getConfiguration(loadContext, registry);
        if (strOperationName == null)
        {
            TCComponentItem targetItem = null;
            TCComponent targetComponent = NewItemCreationHelper.getTargetComponent();
            if (targetComponent instanceof TCComponentItem)
            {
                targetItem = (TCComponentItem) targetComponent;
            }
            if (targetComponent instanceof TCComponentItemRevision)
            {
                try
                {
                    targetItem = ((TCComponentItemRevision) targetComponent).getItem();
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
            
            String itemType = targetItem.getType();
            loadContext = itemType + ".LOAD_DELEGATE";
            strOperationName = NewItemCreationHelper.getConfiguration(loadContext, registry);
        }
        
        String requiredOperationName = null;
        
        if (strOperationName != null && strOperationName.length > 0)
        {
            requiredOperationName = strOperationName[0];
            
            Object[] oprParams = new Object[1];
            oprParams[0] = panels;
            
            try
            {
                l_loadDelegate = (ILoadDelegate) Instancer.newInstanceEx(requiredOperationName, oprParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return l_loadDelegate;
    }
    
    public static IOperationDelegate getSaveDelegate(Registry registry, Object[] constructorArgs)
    {
        IOperationDelegate l_saveDelegate = null;
        
        String saveContext = NewItemCreationHelper.getSelectedOperation() + ".OPERATION";
        String strOperationName[] = NewItemCreationHelper.getConfiguration(saveContext, registry);
        
        String requiredOperationName = null;
        
        if (strOperationName != null && strOperationName.length > 0)
        {
            requiredOperationName = strOperationName[0];
            
            try
            {
                l_saveDelegate = (IOperationDelegate) Instancer.newInstanceEx(requiredOperationName, constructorArgs);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return l_saveDelegate;
    }
}
