package com.nov.rac.item.operations;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.DeepCopyDataHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.copyitemhelper.CopyItemHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class CopyDocumentOperationDelegate extends CreateNewDocumentOperationDelegate
{

	private TCComponent[] m_datasetList = null;

	public CopyDocumentOperationDelegate(Shell theShell)
	{
		super(theShell);
	}

	@Override
	public boolean preCondition() throws TCException
	{
		super.preCondition();
		m_datasetList  = documentPropertyMap.getCompound(NationalOilwell.REVISION).getTagArray(NationalOilwell.IMAN_SPECIFICATION);
		if (m_datasetList != null)
		{
			documentPropertyMap.getCompound(NationalOilwell.REVISION)
			                       .setTagArray(NationalOilwell.IMAN_SPECIFICATION , new TCComponent [0]);
		}
		return true;
	}

	@Override
	public boolean baseAction() throws TCException
	{
		if(m_datasetList != null)
		{
			IPropertyMap itemPropMap = createInputObjPropMap();
			DeepCopyDataHelper[] secondaryObjects = filterSecondaryObjecs();
			CopyItemHelper copyItemHelper = new CopyItemHelper(itemPropMap, secondaryObjects);
			TCComponent item = copyItemHelper.copyItem();
			
			 // adding newly created objects to container/folder
	        TCComponentFolder parentFolder = (TCComponentFolder)NewItemCreationHelper.getContainer();
	        parentFolder.add("contents", item, 0 );
	        parentFolder.refresh();
		}
		else
		{
			super.baseAction();
		}
		return true;
	}

	private IPropertyMap createInputObjPropMap() throws TCException 
	{
		IPropertyMap inputObject = new SimplePropertyMap();
		inputObject.setComponent(getItemRevOfSelectedItem());
		
		inputObject.setString( NationalOilwell.ITEM_ID , documentPropertyMap.getString(NationalOilwell.ITEM_ID));
		inputObject.setString( NationalOilwell.OBJECT_NAME , documentPropertyMap.getString(NationalOilwell.OBJECT_NAME));
		inputObject.setString( NationalOilwell.ITEM_REVISION_ID, 
				               documentPropertyMap.getCompound(NationalOilwell.REVISION).getString(NationalOilwell.ITEM_REVISION_ID));
		
		return inputObject;
	}
	
	private DeepCopyDataHelper[] filterSecondaryObjecs() 
	{
		List<DeepCopyDataHelper> list = new ArrayList<DeepCopyDataHelper>();
		TCComponentDataset dataset = null;
		
		for (int index = 0; m_datasetList != null && index < m_datasetList.length; index++) 
		{
			if (m_datasetList[index] instanceof TCComponentDataset)
			{
				dataset = (TCComponentDataset) m_datasetList[index];

				if (dataset.getType().toString().equalsIgnoreCase("PDF")|| dataset.getType().toString().equalsIgnoreCase("TIFF"))
				{
					continue;
				}
				
				String datasetName = documentPropertyMap.getString(NationalOilwell.ITEM_ID);
				datasetName = datasetName.substring(0, datasetName.indexOf(":"));
				
				DeepCopyDataHelper deepCopyData = new DeepCopyDataHelper(dataset);
				
				deepCopyData.setProperty(DeepCopyDataHelper.NEW_NAME, datasetName);
				deepCopyData.setProperty(DeepCopyDataHelper.RELATION_TYPE_NAME, NationalOilwell.IMAN_SPECIFICATION);
				deepCopyData.setProperty(DeepCopyDataHelper.COPY_ACTION, DeepCopyDataHelper.COPYACTION_CopyAsObject);
				
				deepCopyData.setProperty(DeepCopyDataHelper.IS_TARGET_PRIMARY, true);
				deepCopyData.setProperty(DeepCopyDataHelper.IS_REQUIRED, false);
				deepCopyData.setProperty(DeepCopyDataHelper.COPY_RELATIONS, false);

				list.add(deepCopyData);
			}
		}
		return (DeepCopyDataHelper[]) list.toArray(new DeepCopyDataHelper[list.size()]);
	}
	
	private TCComponentItemRevision getItemRevOfSelectedItem() throws TCException
	{	
		TCComponent selectedObject = NewItemCreationHelper.getTargetComponent();	
		TCComponentItemRevision selectedItemRev = null;
		if (selectedObject == null)	
		{
			selectedObject = NewItemCreationHelper.getSelectedComponent();
		}	
		if(selectedObject instanceof TCComponentItem)
		{			
			selectedItemRev = ((TCComponentItem) selectedObject).getLatestItemRevision();
		}
		else if(selectedObject instanceof TCComponentItemRevision)
		{
			TCComponentItem item = ((TCComponentItemRevision) selectedObject).getItem();
			selectedItemRev = item.getLatestItemRevision();
		}				
		
		return selectedItemRev;
	}
	
}
