package com.nov.rac.item.operations.propertyValidators;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LegacyItemCreateValidator implements IPropertyValidator
{
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap
                    && (null != typeValue && (typeValue.equals(NationalOilwell.PART_BO_TYPE))))
            
            {
                validatePart(propertyMap, stack);
            }
            
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }
    
    private void validatePart(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        
        // Rupali : TypeLovPopupButton is pending for validation
        
        if (!LegacyItemCreateHelper.hasContent(masterPropertyMap.getString(NationalOilwell.BASE_NUMBER)))
        {
            TCException e = new TCException("Base Number is mandatory field");
            stack.addErrors(e);
        }
        
        IPropertyMap revisionPropertyMap = propertyMap.getCompound("revision");
        if (!LegacyItemCreateHelper.hasContent(revisionPropertyMap.getString(NationalOilwell.ITEMREVISIONID)))
        {
            TCException e = new TCException("Revision ID is mandatory field");
            stack.addErrors(e);
        }
    }
    
    private Registry m_registry;
    
}
