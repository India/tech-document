package com.nov.rac.item.operations.propertyValidators.downhole.group33;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.item.operations.propertyValidators.downhole.DocumentPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;

public class DocumentValidator extends DocumentPropertyValidator
{

	@Override
	protected void validateDocument(IPropertyMap propertyMap,
			ExceptionStack stack)
	{
		super.validateDocument(propertyMap, stack);
		
		if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.OBJECT_NAME)))
        {
            TCException e = new TCException(m_registry.getString("DH_Name_Mandatory"));
            stack.addErrors(e);
        }
		
	}
	
}
