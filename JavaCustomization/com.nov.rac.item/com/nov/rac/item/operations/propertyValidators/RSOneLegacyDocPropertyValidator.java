package com.nov.rac.item.operations.propertyValidators;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneLegacyDocPropertyValidator implements IPropertyValidator
{
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap && null != typeValue && typeValue.equals(NationalOilwell.DOCUMENT_TYPE))
            {
                validateDocument(propertyMap, stack);
            }
            if (null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE)
                    && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
            {
                validateDataset(propertyMap, stack);
            }
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        //1st condition for RSOneDefiningDoc Operation
        else if ((NewItemCreationHelper.getSelectedOperation()
				.equalsIgnoreCase("CreateLegacyDocument")) && !isItemIDAlreadyExist(arrayOfPropertMap) )
        {
            throw (new TCException(m_registry.getString("TryNewID.MSG")));
        }
        
        return true;
    }
    
    protected void validateDocument(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        
        if (!hasContent(propertyMap.getString(NationalOilwell.OBJECT_NAME)))
        {
            TCException e = new TCException(m_registry.getString("Item_Name_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)))
        {
            TCException e = new TCException(m_registry.getString("Doc_Category_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!hasContent(propertyMap.getCompound("IMAN_master_form").getString(NationalOilwell.BASE_NUMBER)))
        {
            TCException e = new TCException(m_registry.getString("Base_Number_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!hasContent(propertyMap.getCompound(NationalOilwell.REVISION).getString(NationalOilwell.ITEM_REVISION_ID)))
        {
            TCException e = new TCException(m_registry.getString("Item_Rev_Mandatory"));
            stack.addErrors(e);
        }
    }
    
    private void validateDataset(IPropertyMap propertyMap, ExceptionStack stack) throws TCException
    {
        m_registry = getRegistry();
        
        // TCDECREL-6775 give message if file template is invalid
        if (DocumentHelper.hasContent(propertyMap.getString("selectedTemplate")))
        {
            TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
            stack.addErrors(e);
        }
        
        if (DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
        {
            // KLOC238
            ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
        }
    }
    
    private boolean isItemIDAlreadyExist(IPropertyMap[] arrayOfPropertMap)
    {
        boolean isUniqueId = false;
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            
            if (null != propertyMap && null != typeValue && typeValue.equals(NationalOilwell.DOCUMENT_TYPE))
            {
            	isUniqueId = ValidationHelper.isIDAlreadyExist(propertyMap, typeValue);
            }
        }
        
        return isUniqueId;
    }
    
    protected Registry m_registry;
}