package com.nov.rac.item.operations.propertyValidators.legacy;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.item.operations.propertyValidators.ValidationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.nov.rac.utilities.utils.ValidateHelper;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;
import com.teamcenter.rac.util.Registry;

public class LegacyItemCreateNov4PartValidator implements IPropertyValidator
{
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap && (null != typeValue && (typeValue.equals(NationalOilwell.PART_BO_TYPE))))
            
            {
                validatePart(propertyMap, stack);
            }
            
            if (null != propertyMap && (null != typeValue && (typeValue.equals(NationalOilwell.DOCUMENT_TYPE))))
            {
                validateDocuments(propertyMap, stack);
            }
            if (null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE)
                    && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
            {
                validateDataset(propertyMap, stack);
            }
            
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        else
        {
            IPropertyMap partPropertyMap = getPartPropertyMap(arrayOfPropertMap);
            if(isPartExists(partPropertyMap) && docHasContent)
            {
                // validation for RD and RDD
                validateRDRDD(partPropertyMap);
            }
        }
        
        return true;
    }

    private boolean isPartExists(IPropertyMap propertyMap)
    {
        boolean isPartExists = false;
        if(propertyMap.getComponent() != null)
        {
            isPartExists = true;
        }
        return isPartExists;
    }

    /**
     * @param arrayOfPropertMap
     * @return
     */
    private IPropertyMap getPartPropertyMap(IPropertyMap[] arrayOfPropertMap)
    {
        IPropertyMap partPropertyMap = null;
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            
            if (null != typeValue
                    && typeValue.equals(NationalOilwell.PART_BO_TYPE))
            {
                partPropertyMap = propertyMap;
                break;
            }
        }
        return partPropertyMap;
    }

    //To Validate and get confirmation to create RD or RDD
    private void validateRDRDD(IPropertyMap partPropertyMap) throws TCException
    {
        String relationSelected = LegacyItemCreateHelper.getSelectedRelation();
        if(relationSelected != null && LegacyItemCreateHelper.isRDRDDGroup())
        {
            if(relationSelected.equalsIgnoreCase(NationalOilwell.RELATED_DEFINING_DOCUMENT))
            {
                checkExistingRDD(partPropertyMap);
            }
           
            MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), m_registry.getString("Legacy.confirmation.title"), null,
                    m_registry.getString("Legacy.RDRDDConfirmation1")+relationSelected+m_registry.getString("Legacy.RDRDDConfirmation2"), MessageDialog.CONFIRM, new String[] { "Continue",
                  "Cancel" }, 0);
            int returnCode = dialog.open();
            // returns code 1 for cancel
            if(returnCode == 1)
            {
                throw new TCException(m_registry.getString("Legacy.CancelAction.msg "));
            }
        }
    }

    // To check whether RDD is exist in the existing part or not
    private void checkExistingRDD(IPropertyMap partPropertyMap)
            throws TCException
    {
        boolean rddExist = false;
        rddExist = ValidationHelper.isRDDExist(partPropertyMap);
        
        if(rddExist)
        {
            throw new TCException(m_registry.getString("RDD_Already_Exists_Error"));
        }
    }
    
    
    private void validatePart(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        
        String currentLoggedInGroup = NewItemCreationHelper.getCurrentGroup();
        boolean isUOMMandatory = PreferenceHelper.isGroupExistInPref(currentLoggedInGroup, "NOV_Legacy_Mandatory_UOM_Groups", TCPreferenceLocation.OVERLAY_LOCATION);
        
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        
        if (!LegacyItemCreateHelper.hasContent(masterPropertyMap.getString(NationalOilwell.BASE_NUMBER)))
        {
            TCException e = new TCException(m_registry.getString("BaseNumber_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.OBJECT_NAME)))
        {
            TCException e = new TCException(m_registry.getString("ItemName_Mandatory"));
            stack.addErrors(e);
        }
        
        
        if(isUOMMandatory && !LegacyItemCreateHelper.hasContent(propertyMap.getTag(NationalOilwell.NOV_UOM))) 
		{ 
		    TCException e = new TCException(m_registry.getString("LegacyUOM_Mandatory"));
		    stack.addErrors(e); 
		}
 
        
        IPropertyMap revisionPropertyMap = propertyMap.getCompound("revision");
        
        if (!LegacyItemCreateHelper.hasContent(revisionPropertyMap.getString(NationalOilwell.ITEMREVISIONID)))
        {
            TCException e = new TCException(m_registry.getString("ItemRevision_Mandatory"));
            stack.addErrors(e);
        }
    }
    
    private void validateDocuments(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        IPropertyMap masterFormPropertyMap = propertyMap.getCompound("IMAN_master_form");
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)))
        {
            docHasContent = true;
        }
        else
        {
            docHasContent = false;
        }
        
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_CATEGORY))
                && LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_TYPE))
                && !LegacyItemCreateHelper.hasContent(masterFormPropertyMap.getString(NationalOilwell.SEQUENCE)))
        {
            TCException e = new TCException(m_registry.getString("Sequence_Mandatory"));
            stack.addErrors(e);
        }
        
    }
    
    private void validateDataset(IPropertyMap propertyMap, ExceptionStack stack) throws TCException
    {
        m_registry = getRegistry();
        
        // TCDECREL-6775 give message if file template is invalid
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString("selectedTemplate")))
        {
            TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
            stack.addErrors(e);
            
        }
        
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
        {
            // KLOC239
            ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
        }
        
    }
    
    
    private Registry m_registry;

    boolean itemIdExists = false;
    
    private boolean docHasContent = false;
}
