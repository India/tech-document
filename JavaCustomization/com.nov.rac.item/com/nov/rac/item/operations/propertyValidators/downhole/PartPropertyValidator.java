package com.nov.rac.item.operations.propertyValidators.downhole;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class PartPropertyValidator implements IPropertyValidator {

	private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap && (null != typeValue && (typeValue.equals(NationalOilwell.PART_BO_TYPE))))
            
            {
                validatePart(propertyMap, stack);
            }
            
            /*if (null != propertyMap && (null != typeValue && (typeValue.equals(NationalOilwell.DOCUMENT_TYPE))))
            {
                validateDocuments(propertyMap, stack);
            }
            
            if (null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE)
                    && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
            {
                validateDataset(propertyMap, stack);
            }*/
            
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }
    
    protected void validatePart(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.ICS_SUBCLASS_NAME)))
        {
            TCException e = new TCException(m_registry.getString("DH_Class_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.ITEM_ID)))
        {
            TCException e = new TCException(m_registry.getString("DH_ItemId_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.OBJECT_DESC)))
        {
            TCException e = new TCException(m_registry.getString("DH_Description_Mandatory"));
            stack.addErrors(e);
        }
        
    }
    
    /*private void validateDocuments(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_CATEGORY))
                && LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_TYPE))
                && !LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.SEQUENCE)))
        {
            TCException e = new TCException(m_registry.getString("Sequence_Mandatory"));
            stack.addErrors(e);
        }
        
    }
    
    private void validateDataset(IPropertyMap propertyMap, ExceptionStack stack) throws TCException
    {
        m_registry = getRegistry();
        
        // TCDECREL-6775 give message if file template is invalid
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString("selectedTemplate")))
        {
            TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
            stack.addErrors(e);
            
        }
        
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
        {
            // KLOC239
            ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
        }
        
    }*/
    
    protected Registry m_registry;

}
