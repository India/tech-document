package com.nov.rac.item.operations.propertyValidators.legacy;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.operations.propertyValidators.IPropertyValidator;
import com.nov.rac.item.operations.propertyValidators.ValidationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LegacyItemCreateDocumentsValidator implements IPropertyValidator
{
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap && null != typeValue && typeValue.equals(NationalOilwell.DOCUMENT_TYPE))
            {
                validateDocument(propertyMap, stack);
            }
            
            if (null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE)
                    && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
            {
                validateDataset(propertyMap, stack);
            }
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
        
    }
    
    private void validateDocument(IPropertyMap propertyMap, ExceptionStack stack)
    {
        m_registry = getRegistry();
        
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        
        if (!LegacyItemCreateHelper.hasContent(masterPropertyMap.getString(NationalOilwell.BASE_NUMBER)))
        {
            TCException e = new TCException(m_registry.getString("BaseNumber_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.OBJECT_NAME)))
        {
            TCException e = new TCException(m_registry.getString("ItemName_Mandatory"));
            stack.addErrors(e);
        }
        
        /*
         * if
         * (!LegacyItemCreateHelper.hasContent(propertyMap.getTag(NationalOilwell
         * .NOV_UOM))) { TCException e = new
         * TCException(m_registry.getString("LegacyUOM_Mandatory"));
         * stack.addErrors(e); }
         */
        
        IPropertyMap revisionPropertyMap = propertyMap.getCompound("revision");
        
        if (!LegacyItemCreateHelper.hasContent(revisionPropertyMap.getString(NationalOilwell.ITEMREVISIONID)))
        {
            TCException e = new TCException(m_registry.getString("ItemRevision_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)))
        {
            TCException e = new TCException(m_registry.getString("DocCategory_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DOCUMENTS_TYPE)))
        {
            TCException e = new TCException(m_registry.getString("DocType_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!LegacyItemCreateHelper.hasContent(masterPropertyMap.getString(NationalOilwell.SEQUENCE)))
        {
            TCException e = new TCException(m_registry.getString("Sequence_Mandatory"));
            stack.addErrors(e);
        }
        
    }
    
    private void validateDataset(IPropertyMap propertyMap, ExceptionStack stack) throws TCException
    {
        m_registry = getRegistry();
        
        // TCDECREL-6775 give message if file template is invalid
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString("selectedTemplate")))
        {
            TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
            stack.addErrors(e);
            
        }
        
        if (LegacyItemCreateHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
        {
            // KLOC238
            ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
        }
        
    }
    
    private Registry m_registry;
    
}