package com.nov.rac.item.operations.propertyValidators;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NOVDatasetTypeUIPanelHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.DatasetFileHelper.FileExtensionHelper;
import com.nov.rac.utilities.services.validationhelper.ValidateValues;
import com.nov.rac.utilities.services.validationhelper.ValidationResultsHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.explorer.common.TCComponentSearch;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class ValidationHelper
{
    
    public static void validateFileRelatedIssues(IPropertyMap propertyMap,
            ExceptionStack stack) throws TCException
    {
        Registry m_registry = Registry
                .getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
        
        String pathGivenByUser = propertyMap
                .getString(NationalOilwell.DATASET_FILE_PATH);
        File file = new File(pathGivenByUser);
        if (!file.isFile()) // path is not valid then show message -
                            // TCDECREL-6770
        {
            TCException e = new TCException(
                    m_registry.getString("Import_Field_Path_Message"));
            stack.addErrors(e);
        }
        else
        { // if path is valid then check for extension
            String datasetType = propertyMap
                    .getString(NationalOilwell.DATASET_TYPE);
            FileExtensionHelper fileExtensionHelper = new FileExtensionHelper();
            String m_datasetFileAbsolutePath = file.getAbsolutePath();
            String extension = NOVDatasetTypeUIPanelHelper
                    .getExtension(m_datasetFileAbsolutePath);
            
            boolean isExtensionValid = fileExtensionHelper.isValidExtension(
                    datasetType, extension);
            
            if (!isExtensionValid)
            {
                TCException e = new TCException(extension + " "
                        + m_registry.getString("File_Extension_Message") + " "
                        + datasetType.toString());
                stack.addErrors(e);
            }
        }
    }
    
    public static boolean isIDAlreadyExist(IPropertyMap propertyMap,
            String typeValue)
    {
        boolean isUniqueId = false;
        
        IPropertyMap validatePropertyMap = new SimplePropertyMap();
        
        validatePropertyMap.addAll(propertyMap);
        validatePropertyMap.setString("object_type", typeValue);
        
        ValidateValues validateValues = new ValidateValues();
        
        validateValues.setClientId("abc");
        validateValues.setOperationType(ValidateValues.CREATE);
        validateValues.setType(typeValue);
        validateValues.setPropertyValues(validatePropertyMap);
        
        Map<String, ValidationResultsHelper> validationResults = validateValues
                .validate();
        
        ValidationResultsHelper validationResultsHelper = validationResults
                .get("abc");
        
        isUniqueId = validationResultsHelper.isIdUnique();
        
        return isUniqueId;
    }
    
    public static boolean isRDDExist(IPropertyMap propertyMap) throws TCException
    {
        boolean isRDDExist = false;
        TCComponentItem selectedItem = (TCComponentItem) propertyMap.getComponent();
        IPropertyMap revisionPropertyMap = propertyMap
                .getCompound("revision");
        String revId = revisionPropertyMap
                .getString(NationalOilwell.ITEMREVISIONID);
        TCComponentItemRevision selectedItemRevision = null;
        
        selectedItemRevision = getRev(selectedItem,revId);
        
        if(selectedItemRevision != null)
        {
            TCComponent[] documentList = null;
            try
            {
                documentList = selectedItemRevision
                        .getRelatedComponents("RelatedDefiningDocument");
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
            if (null != documentList && 0 < documentList.length)
            {
                isRDDExist = true;
            }
        }
      
        return isRDDExist;
    }
    
    public static TCComponentItem getTCComponent(String itemId, String itemType)
    {
        TCComponentItem selectedItem = null;
        TCSession tcSession =  (TCSession) AIFUtility.getDefaultSession();
        TCComponentQueryType qt;
        try
        {
            qt = (TCComponentQueryType)tcSession.getTypeComponent("ImanQuery");
            TCComponentQuery gen = (TCComponentQuery)qt.find("Item...");
            if (gen != null) 
            {
                TCComponent[] arr = gen.execute(new String[]{"Item ID", "Type"},new String[]{itemId, itemType });
                if(arr != null && arr.length > 0)
                {
                    if(arr[0] instanceof TCComponentItem)
                    {
                        selectedItem = (TCComponentItem) arr[0];
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return selectedItem;
    }
    
    /*public static TCComponentItemRevision getRevisionItem(IPropertyMap propertyMap) throws TCException
    {
        TCComponentItemRevision itemRev = null;
        TCComponentItem targetItem = (TCComponentItem) propertyMap
                .getComponent();
        if(targetItem != null)
        {
            itemRev = isRevExists(propertyMap, targetItem);
            
            if(itemRev == null)
            {
            	itemRev = targetItem.getLatestItemRevision();
            }
        }
       
        return itemRev;
    } */

	public static boolean isRevExists(TCComponentItem part,
            String revisionId) throws TCException {

		boolean revExists = false;
		
		if(part != null)
		{
		    TCComponentItemRevision existingRev = findItemForRevId(part, revisionId);
		    if(existingRev != null)
		    {
		        revExists = true;
		    }
		}
		
		return revExists;
	}
	
    public static TCComponentItemRevision getRev(TCComponentItem part,
            String revisionId) throws TCException
    {
        
        TCComponentItemRevision existingRev = null;
        
        if (part != null)
        {
            existingRev = findItemForRevId(part, revisionId);
            
            if(existingRev == null)
            {
                existingRev = part.getLatestItemRevision();
            }
        }
        
        return existingRev;
    }
    
    private static TCComponentItemRevision findItemForRevId(
            TCComponentItem part, String revisionId) throws TCException
    {
        TCComponentItemRevision itemRev = null;
        TCComponent[] revisionlist = part
                .getReferenceListProperty("revision_list");
        
        TCComponentType.getPropertiesSet(Arrays.asList(revisionlist), new String[] {NationalOilwell.ITEMREVISIONID});
        
        for (TCComponent revision : revisionlist)
        {
            TCComponentItemRevision revisionItem = (TCComponentItemRevision) revision;
            String revId = revisionItem
                    .getProperty(NationalOilwell.ITEMREVISIONID);
            if (revId.equals(revisionId))
            {
                itemRev = revisionItem;
                break;
            }
        }
        return itemRev;
    }
}
