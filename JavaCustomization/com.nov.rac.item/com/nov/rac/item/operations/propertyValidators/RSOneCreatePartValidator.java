package com.nov.rac.item.operations.propertyValidators;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneCreatePartValidator implements IPropertyValidator
{
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap
                    && (null != typeValue && (typeValue.equals(NationalOilwell.PART_BO_TYPE) || typeValue
                            .equals(NationalOilwell.NON_ENGINEERING))))
            
            {
                validatePart(propertyMap, stack);
            }
            
            if (null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE)
                    && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
            {
                validateDataset(propertyMap, stack);
            }
            
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }
    
    protected void validatePart(IPropertyMap propertyMap, ExceptionStack stack)
    
    {
        m_registry = getRegistry();
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        
        if (!DocumentHelper.hasContent(masterPropertyMap.getString(NationalOilwell.RSONE_ITEMTYPE)))
        {
            TCException e = new TCException(m_registry.getString("Item_Type_Mandatory"));
            stack.addErrors(e);
        }
        if (!DocumentHelper.hasContent(masterPropertyMap.getStringArray(NationalOilwell.RSONE_ORG)[0]))
        {
            TCException e = new TCException(m_registry.getString("ORG_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.OBJECT_NAME)))
        {
            TCException e = new TCException(m_registry.getString("Name_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getString("ics_subclass_name")))
        {
            TCException e = new TCException(m_registry.getString("class_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(masterPropertyMap.getString(NationalOilwell.RSONE_UOM)))
        {
            TCException e = new TCException(m_registry.getString("UOM_Mandatory"));
            stack.addErrors(e);
        }
        
        //Tushar added if else
        //Commented if time being and added new if
//        if(masterPropertyMap.getLogical(NationalOilwell.RSONE_LOTCONTROL) == null
//            || masterPropertyMap.getLogical(NationalOilwell.RSONE_SERIALIZE) == null
//            || masterPropertyMap.getLogical(NationalOilwell.RSONE_QAREQUIRED) == null)
//        {
//            
//            TCException e = new TCException(m_registry.getString("traceability_NA_Check"));
//            stack.addErrors(e);
//        }
        if(masterPropertyMap.getLogical("Traceability_45WLY_Check_NA"))
        {
            TCException e = new TCException(m_registry.getString("traceability_NA_Check"));
          stack.addErrors(e);
        }
        else if(!(masterPropertyMap.getLogical(NationalOilwell.RSONE_LOTCONTROL))
                && !(masterPropertyMap.getLogical(NationalOilwell.RSONE_SERIALIZE))
                && !(masterPropertyMap.getLogical(NationalOilwell.RSONE_QAREQUIRED)))
        {
            TCException e = new TCException(m_registry.getString("traceability_Mandatory"));
            stack.addErrors(e);
        }
        
    }
    
    private void validateDataset(IPropertyMap propertyMap, ExceptionStack stack) throws TCException
    {
        m_registry = getRegistry();
        
        // TCDECREL-6775 give message if file template is invalid
        if (DocumentHelper.hasContent(propertyMap.getString("selectedTemplate")))
        {
            TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
            stack.addErrors(e);
            
        }
        
        if (DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
        {
            // KLOC239
            ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
        }
        
    }
    
    protected Registry m_registry;
    
}
