package com.nov.rac.item.operations.propertyValidators;

import com.nov.rac.item.helpers.DocumentHelper;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;

public class RSOneRelatedDocPropertyValidator extends RSOneDocPropertyValidator{
	
	@Override
	protected void validateDocument(IPropertyMap propertyMap,
			ExceptionStack stack) {
		super.validateDocument(propertyMap, stack);
		
		if (!DocumentHelper.hasContent(propertyMap
                .getString(NationalOilwell.ITEM_ID))) {
            TCException e = new TCException(
                    m_registry.getString("Selected_Base_Mandatory"));
            stack.addErrors(e);
        }
	}

}
