package com.nov.rac.item.operations.propertyValidators;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public interface IPropertyValidator
{
    
    /**
     * Validate properties.
     * 
     * @param IPropertyMap[]
     * @return true, if successful
     * @throws TCException
     * 
     */
    public boolean validateProperties(IPropertyMap[] propertyMap) throws TCException;
}
