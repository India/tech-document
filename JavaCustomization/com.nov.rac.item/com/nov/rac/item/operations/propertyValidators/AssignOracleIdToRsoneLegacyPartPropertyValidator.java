package com.nov.rac.item.operations.propertyValidators;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class AssignOracleIdToRsoneLegacyPartPropertyValidator implements IPropertyValidator
{
    private Registry m_registry;
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        // TODO Auto-generated method stub
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap
                    && (null != typeValue && (typeValue.equals(NationalOilwell.PART_BO_TYPE))))
            {
                validatePart(propertyMap, stack);
            }
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }

        return true;
    }
    
private void validatePart(IPropertyMap propertyMap, ExceptionStack stack)
    
    {
        m_registry = getRegistry();
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        if (!DocumentHelper.hasContent(masterPropertyMap.getString(NationalOilwell.RSONE_ITEMTYPE)))
        {
            TCException e = new TCException(m_registry.getString("Type_Mandatory"));
            stack.addErrors(e);
        }       

        if (!DocumentHelper.hasContent(masterPropertyMap.getStringArray(NationalOilwell.RSONE_ORG)[0]))
        {
            TCException e = new TCException(m_registry.getString("ORG_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.ICS_subclass_name)))
        {
            TCException e = new TCException(m_registry.getString("class_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(masterPropertyMap.getString(NationalOilwell.RSONE_UOM)))
        {
            TCException e = new TCException(m_registry.getString("UOM_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!(masterPropertyMap.getLogical(NationalOilwell.RSONE_LOTCONTROL))
                && !(masterPropertyMap.getLogical(NationalOilwell.RSONE_SERIALIZE))
                && !(masterPropertyMap.getLogical(NationalOilwell.RSONE_QAREQUIRED)))
        {
            TCException e = new TCException(m_registry.getString("traceability_Mandatory"));
            stack.addErrors(e);
        }
    }
}
