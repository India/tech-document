package com.nov.rac.item.operations.propertyValidators;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyDocPropertyValidator extends RSOneDocPropertyValidator
{
    
    @Override
    protected void validateDocument(IPropertyMap propertyMap, ExceptionStack stack)
    {
        super.validateDocument(propertyMap, stack);
        
        if (!hasContent(propertyMap
                .getString(NationalOilwell.ITEM_ID))) {
            TCException e = new TCException(
                    m_registry.getString("Doc_ID_Mandatory"));
            stack.addErrors(e);
        }
    }
}
