package com.nov.rac.item.operations.propertyValidators;

import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyPartPropertyValidator extends RSOneCreatePartValidator{

	@Override
	protected void validatePart(IPropertyMap propertyMap, ExceptionStack stack) {
		
		super.validatePart(propertyMap, stack);
		
		if (!DocumentHelper.hasContent(propertyMap.getString("item_id")))
        {
            TCException e = new TCException(m_registry.getString("Selected_Base_Mandatory"));
            stack.addErrors(e);
        }
		
	}
}
