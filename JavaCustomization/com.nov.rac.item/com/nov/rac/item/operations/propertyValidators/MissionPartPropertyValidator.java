package com.nov.rac.item.operations.propertyValidators; 

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class MissionPartPropertyValidator implements IPropertyValidator
{
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    /**
     * Validate properties.
     * 
     * @param propertyMap
     * @return true, if successful
     * @throws TCException
     * 
     */
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for(IPropertyMap  propertyMap: arrayOfPropertMap)
		{
        	String typeValue = propertyMap.getType();
			 if(null != propertyMap && (null != typeValue && 
			(typeValue.equals(NationalOilwell.PART_BO_TYPE)  || typeValue.equals(NationalOilwell.NON_ENGINEERING) )))
			
		     {
				 validatePart(propertyMap, stack);
		     }
			 if(null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE) && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
			 {
				 validateDataset(propertyMap, stack);
			 }
		}
		
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
           throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }

	private void validatePart(IPropertyMap propertyMap,ExceptionStack stack) 
			
	{
		m_registry = getRegistry();
		IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        
        if (!DocumentHelper.hasContent(masterPropertyMap.getString(NationalOilwell.MIS_TYPE))) 
        {
            TCException e = new TCException(m_registry.getString("Item_Type_Mandatory"));
            stack.addErrors(e);
        }
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.ITEM_ID)))
        {
            TCException e = new TCException(m_registry.getString("Item_ID_Mandatory"));
            stack.addErrors(e);
        }
        if (!DocumentHelper.hasContent(propertyMap.getTag(NationalOilwell.NOV_UOM)))
        {
            TCException e = new TCException(m_registry.getString("UOM_Mandatory"));
            stack.addErrors(e);
        }
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.OBJECT_NAME))) 
        {
            TCException e = new TCException(m_registry.getString("Item_Name_Mandatory"));
            stack.addErrors(e);
        }
		
		
	}
	
	private void validateDataset(IPropertyMap propertyMap,ExceptionStack stack) throws TCException 
	{
		 m_registry = getRegistry();
		 
		 //TCDECREL-6775 give message if file template is invalid
		 if (DocumentHelper.hasContent(propertyMap.getString("selectedTemplate")))
	     {
			 TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
		     stack.addErrors(e);
			
	     }
		 
		 if (DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
	     {
			 // KLOC239 
			 ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
	     }
		 
	}


    private Registry m_registry ;
    
}
