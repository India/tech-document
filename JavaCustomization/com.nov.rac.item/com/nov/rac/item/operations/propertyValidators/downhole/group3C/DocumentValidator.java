package com.nov.rac.item.operations.propertyValidators.downhole.group3C;

import com.nov.NationalOilwell;
import com.nov.rac.item.operations.propertyValidators.downhole.DocumentPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;

public class DocumentValidator extends DocumentPropertyValidator
{

	@Override
	protected void validateDocument(IPropertyMap propertyMap,
			ExceptionStack stack)
	{
		super.validateDocument(propertyMap, stack);
		
		IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        if ((masterPropertyMap.getStringArray(NationalOilwell.SITES)).length == 0)
        {
            TCException e = new TCException(m_registry.getString("DH_Site_Mandatory"));
            stack.addErrors(e);
        }
		
	}
	
}
