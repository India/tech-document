package com.nov.rac.item.operations.propertyValidators;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneLegacyPartWithOracleIDPropertyValidator implements IPropertyValidator
{
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.operations.propertyValidators.propertyValidators");
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            if (null != propertyMap
                    && (null != typeValue && (typeValue.equals(NationalOilwell.PART_BO_TYPE) || typeValue
                            .equals(NationalOilwell.NON_ENGINEERING))))
            
            {
                validatePart(propertyMap, stack);
            }
            if (null != propertyMap && null != propertyMap.getString(NationalOilwell.DATASET_TYPE)
                    && propertyMap.getString(NationalOilwell.DATASET_TYPE).length() > 0)
            {
                validateDataset(propertyMap, stack);
            }
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        else if (!isItemIDAlreadyExist(arrayOfPropertMap))
        {
            throw (new TCException(m_registry.getString("TryNewID.MSG")));
        }
        return true;
    }
    
    private void validatePart(IPropertyMap propertyMap, ExceptionStack stack)
    
    {
        m_registry = getRegistry();
        IPropertyMap masterPropertyMap = propertyMap.getCompound("IMAN_master_form");
        if (!DocumentHelper.hasContent(masterPropertyMap.getString(NationalOilwell.RSONE_ITEMTYPE)))
        {
            TCException e = new TCException(m_registry.getString("Type_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getCompound("IMAN_master_form").getString(NationalOilwell.RSONE_BASE_NUMBER)))
        {
            TCException e = new TCException(m_registry.getString("Base_Number_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getCompound("revision").getString(NationalOilwell.ITEM_REVISION_ID)))
        {
            TCException e = new TCException(m_registry.getString("Item_Rev_Mandatory"));
            stack.addErrors(e);
        }
       /* 
        if (!DocumentHelper.hasContent(propertyMap.getCompound("IMAN_master_form").getString(NationalOilwell.RSONE_ORG)))
        {
            TCException e = new TCException(m_registry.getString("ORG_Mandatory"));
            stack.addErrors(e);
        }*/
        
        if (!DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.ICS_subclass_name)))
        {
            TCException e = new TCException(m_registry.getString("class_Mandatory"));
            stack.addErrors(e);
        }
        
        if (!DocumentHelper.hasContent(propertyMap.getCompound("IMAN_master_form").getString(NationalOilwell.RSONE_UOM)))
        {
            TCException e = new TCException(m_registry.getString("UOM_Mandatory"));
            stack.addErrors(e);
        }
    }
    
    private void validateDataset(IPropertyMap propertyMap, ExceptionStack stack) throws TCException
    {
        m_registry = getRegistry();
        
        if (DocumentHelper.hasContent(propertyMap.getString("selectedTemplate")))
        {
            TCException e = new TCException(m_registry.getString("Invalid_File_Template"));
            stack.addErrors(e);
        }
        
        if (DocumentHelper.hasContent(propertyMap.getString(NationalOilwell.DATASET_FILE_PATH)))
        {
            ValidationHelper.validateFileRelatedIssues(propertyMap, stack);
        }
    }
    
    private boolean isItemIDAlreadyExist(IPropertyMap[] arrayOfPropertMap)
    {
        boolean isUniqueId = false;
        
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String typeValue = propertyMap.getType();
            
            if (null != propertyMap && null != typeValue && typeValue.equals(NationalOilwell.PART_BO_TYPE))
            {
            	isUniqueId = ValidationHelper.isIDAlreadyExist(propertyMap, typeValue);
            }
        }
        
        return isUniqueId;
    }
    
    private Registry m_registry;
    
}
