package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class RSOneRelatedDocumentLoadDelegate extends AbstractLoadDelegate{

	public RSOneRelatedDocumentLoadDelegate(List<IUIPanel> inputProviders) 
	{
		super(inputProviders);
	}
	
	public void loadComponent(TCComponent targetObject) throws TCException 
	{
		TCComponentItem targetItem = null;
		
        if (targetObject instanceof TCComponentItem)
        {
            targetItem = (TCComponentItem) targetObject;
        }
        
        if (targetObject instanceof TCComponentItemRevision)
        {
            targetItem = ((TCComponentItemRevision) targetObject).getItem();
        }
        if (null != targetItem)
        {
        	IPropertyMap propertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetItem, new String[] { NationalOilwell.ITEM_ID, NationalOilwell.OBJECT_NAME, NationalOilwell.OBJECT_DESC },
                    propertyMap);
            
            String itemId = PropertyMapHelper.handleNull(propertyMap.getString(NationalOilwell.ITEM_ID));
            String itemIdWithoutDashNo = itemId.split(m_dashNoSeperator)[0];
            
            if(itemIdWithoutDashNo != null)
            {
            	propertyMap.setString(NationalOilwell.ITEM_ID, itemIdWithoutDashNo);
            }
            
            loadPanelsData(NationalOilwell.DOCUMENT_TYPE, getInputProviders(), propertyMap);
        }
	}

	@Override
	public void loadComponent(Object inputObject) throws TCException 
	{
		loadComponent( (TCComponent) inputObject);
	}
	
	private static final String m_dashNoSeperator = "-";
}
