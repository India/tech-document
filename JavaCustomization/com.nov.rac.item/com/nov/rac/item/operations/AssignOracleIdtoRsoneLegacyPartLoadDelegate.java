package com.nov.rac.item.operations;

import java.util.List;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdtoRsoneLegacyPartLoadDelegate extends AbstractLoadDelegate
{
    /**
     * Instantiates a new copy part load delegate.
     * 
     * @param inputProviders
     *            the input providers
     */
    
    public AssignOracleIdtoRsoneLegacyPartLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    @Override
    public void loadComponent(Object inputObject) throws TCException
    {
        loadComponent((TCComponent) inputObject);
    }
    
    public void loadComponent(TCComponent targetObject) throws TCException
    {
        
        IPropertyMap masterFormPropertyMap = null;
        TCComponentItem targetItem = GeneralItemUtils.getItemFromTCComponent(targetObject);
        
        // check if object getting loaded is same as initial selected object
        TCComponent selectedTargetComp = NewItemCreationHelper.getTargetComponent();
        TCComponent masterForm = targetItem.getRelatedComponent("IMAN_master_form");
        
        if (selectedTargetComp == targetObject && null != targetItem)
        {
            IPropertyMap selectedPartPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetItem, getItemMapProperties(), selectedPartPropertyMap);
            selectedPartPropertyMap.setComponent(targetItem);
            
            masterFormPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(masterForm, getMasterFormMapProperties(), masterFormPropertyMap);
            
            selectedPartPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
            loadPanelsData(targetItem.getType(), getInputProviders(), selectedPartPropertyMap);
        }
        else if (selectedTargetComp != targetObject && null != targetItem)
        {
            IPropertyMap basePartPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(targetItem, getItemMapProperties(), basePartPropertyMap);
            
            basePartPropertyMap.setString(NationalOilwell.OBJECT_NAME,
                    selectedTargetComp.getProperty(NationalOilwell.OBJECT_NAME));
            basePartPropertyMap.setString(NationalOilwell.OBJECT_DESC,
                    selectedTargetComp.getProperty(NationalOilwell.OBJECT_DESC));
            
            //alternate id needs to be displayed as itemId in Selected Base.
            //for legacy part Item Id and Alt Id can be different.
            String altId = targetItem.getProperty("altid_list");
            if (altId != null && !altId.equals(""))
            {
                // example : get '1218123646' from 1218123646-001@RSOne
                altId = altId.split("@")[0];
                basePartPropertyMap.setString(NationalOilwell.ITEM_ID, altId);
            }
            
            basePartPropertyMap.setComponent(targetItem);
            
            masterFormPropertyMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(masterForm, getMasterFormMapProperties(), masterFormPropertyMap);
            
            basePartPropertyMap.setCompound("IMAN_master_form", masterFormPropertyMap);
            loadPanelsData(targetItem.getType(), getInputProviders(), basePartPropertyMap);
        }
        
    }
    
    private String[] getItemMapProperties()
    {
        String[] itemMapProperties = { NationalOilwell.ICS_subclass_name, NationalOilwell.OBJECT_NAME,
                NationalOilwell.OBJECT_DESC, NationalOilwell.ITEM_ID };
        
        return itemMapProperties;
    }
    
    private String[] getMasterFormMapProperties()
    {
        String[] masterFormMapProperties = { NationalOilwell.RSONE_ORG, NationalOilwell.RSONE_UOM,
                NationalOilwell.RSONE_LOTCONTROL, NationalOilwell.RSONE_SERIALIZE, NationalOilwell.RSONE_QAREQUIRED,
                NationalOilwell.RSONE_ENGCONTROLLED, NationalOilwell.RSONE_ITEMTYPE };
        
        return masterFormMapProperties;
    }
    
}
