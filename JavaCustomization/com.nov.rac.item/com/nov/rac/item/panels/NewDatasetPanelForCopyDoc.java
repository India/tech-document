package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class NewDatasetPanelForCopyDoc extends NewDatasetPanel
{

	public NewDatasetPanelForCopyDoc(Composite parent, int style) throws TCException 
	{
		super(parent, style);
	}

	@Override
	public boolean createUI() throws TCException
	{
		super.createUI();
		registerSubscriber();
		return true;
	}
	
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
    	if(m_propMap == null)
    	{
    		m_propMap = new SimplePropertyMap();
    		m_propMap.addAll(propMap);
    	}
    	
        super.load(m_propMap);
    	return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException 
    {
    	if(m_isCreateDoc)
    	{
    		super.save(propMap);
    	}
    	
    	return true;
    }
	
	@Override
	public void update(PublishEvent event) 
	{
		super.update(event);
		
    	if(event.getPropertyName().equals(NationalOilwell.COPY_DOC))
    	{
    		m_isCreateDoc = false;
    		Display.getDefault().asyncExec(new Runnable()
    		{
    			public void run()
    			{

    				try
    				{
    					load(m_propMap);
    				}
    				catch (TCException e) 
    				{
    					e.printStackTrace();
    				}
    			}
    		});
    	}
    	if(event.getPropertyName().equals(NationalOilwell.CREATE_DOCUMENT))
    	{
    		m_isCreateDoc = true;
    	}
	}

	private void registerSubscriber()
	{
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.registerSubscriber(NationalOilwell.CREATE_DOCUMENT, getSubscriber());
        l_controller.registerSubscriber(NationalOilwell.COPY_DOC, getSubscriber());
	}
	
	@Override
	public void dispose() 
	{
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.unregisterSubscriber(NationalOilwell.CREATE_DOCUMENT, getSubscriber());
        l_controller.unregisterSubscriber(NationalOilwell.COPY_DOC, getSubscriber());
        super.dispose();
	}
	
	private IPropertyMap m_propMap = null;
	private boolean m_isCreateDoc = false;
}
