/* ============================================================
   
   	File description: 

  	 Filename: NewDocCategoryUIPanel2.java
  	 Module  : <name of the module/folder where this file resides> 

   	<Short Description of the file> 

   	Date         Developers    Description
   	02-08-2012   snehac    Initial creation

   	$HISTORY
 ============================================================ */
package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;


import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

/**
 * The Class NewDocCategoryUIPanel2.
 */
public class NewDocCategoryUIPanel2 extends SimpleNewDocCategoryUIPanel1 implements IPublisher, ISubscriber
{
    
    /**
     * Instantiates a new doc category ui panel2.
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public NewDocCategoryUIPanel2(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite documentComposite = getComposite();
        documentComposite.setLayout(new GridLayout(1, true));
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        documentComposite.setLayoutData(gd_panel);
        createDocumentHeader(documentComposite);
        createRadioBtnComposite(documentComposite);
        subComposite = SWTUIHelper.createRowComposite(documentComposite, 3, false);
        
        createDocCategoryRow(subComposite);
        createDocTypeRow(subComposite);
        createPullDocRow(subComposite);
        // TCDECREL-6370 disable right mouse click
        disableRightClick();
        
        
        if (m_btnRadioCopyDoc.getSelection())
        {
            super.toggleState(!m_btnRadioCopyDoc.getSelection());
        }
        
        
        
//        Composite m_docCategoryComposite = getComposite();
//        GridLayout gridLayout = new GridLayout(1, true);
//        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
//        m_docCategoryComposite.setLayout(gridLayout);
//        m_docCategoryComposite.setLayoutData(gd_panel);
//        // for creating composite having 2 radio buttons
//        createRadioBtnComposite(m_docCategoryComposite);
//        
//        super.createUI();
//        
//        if (m_btnRadioCopyDoc.getSelection())
//        {
//            super.toggleState(!m_btnRadioCopyDoc.getSelection());
//        }
        
        UIHelper.makeMandatory(m_textDocCategory);
        UIHelper.makeMandatory(m_docTypeLovPopupButton.getPopupButton());
        
        
       // m_docCategoryComposite.pack();
        
        addingListenerOnCopyDoc();
        addingListenerOnCreateNewDoc();
        registerSubscriber();
        
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#reload()
     */
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    /**
     * adding listener on Radio button Copy Doc.
     */
    private void addingListenerOnCopyDoc()
    {
        
        m_btnRadioCopyDoc.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                widgetsSelectedRBCopyDoc();
            }
        });
        
    }
    
    /**
     * adding listener on Radio Create New Doc.
     */
    private void addingListenerOnCreateNewDoc()
    {
        m_btnRadioCreateNewDoc.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                widgetSelectedRBCreateNewDoc();
                
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.CREATE_DOCUMENT, null, null);
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        controller.publish(event);
                    }
                });
                
            }
        });
    }
    
    /**
     * if event occurs on Copy Doc radio button then this function is called.
     */
    private void widgetsSelectedRBCopyDoc()
    {
        
        if (m_btnRadioCopyDoc.getSelection())
        {
            
            super.toggleDocTypeButton(!m_btnRadioCopyDoc.getSelection());
            super.toggleState(!m_btnRadioCopyDoc.getSelection());
            super.clear();
            TCComponent selectedComponent;
            selectedComponent = NewItemCreationHelper.getTargetComponent();
            if (null == selectedComponent)
            {
                selectedComponent = NewItemCreationHelper.getSelectedComponent();
            }
            if (null != selectedComponent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.COPY_DOC, null, null);
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        controller.publish(event);
                    }
                });
            }
            
        }
        
    }
    
    /**
     * if event occurs on Create New Doc radio button then this function is
     * called.
     */
    private void widgetSelectedRBCreateNewDoc()
    {
        if (m_btnRadioCreateNewDoc.getSelection())
        {
            super.toggleDocTypeButton(!m_btnRadioCreateNewDoc.getSelection());
            super.toggleState(m_btnRadioCreateNewDoc.getSelection());
            super.clear();
            
        }
        
    }
    
    /**
     * creating composite and adding radio buttons on it.
     * 
     * @param docCategoryComposite
     *            the doc category composite
     */
    private void createRadioBtnComposite(Composite docCategoryComposite)
    {
        
        // new Label(docCategoryComposite, SWT.NONE);
        // // SWT.BORDER to test the changes
        Composite radioBtnComposite = new Composite(docCategoryComposite, SWT.NONE);
        GridLayout radioComplayout = new GridLayout(2,true);
        radioComplayout.marginHeight = 0;
        radioComplayout.marginWidth = 0;
        radioBtnComposite.setLayout(radioComplayout);
        radioBtnComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_RADIOBUTTON_COMPOSITE, 1));
        
        radioBtnComposite.setLayout(new GridLayout(2, true));
        
        //TCDECREL-6781
        //radioBtnComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
        radioBtnComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        
        m_btnRadioCopyDoc = new Button(radioBtnComposite, SWT.RADIO);
        m_btnRadioCopyDoc.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        
        m_btnRadioCopyDoc.setText(m_registry.getString("copyDocRB.NAME", "Key Not Found "));
        m_btnRadioCopyDoc.setSelection(true);
        
        m_btnRadioCreateNewDoc = new Button(radioBtnComposite, SWT.RADIO);
        
        //TCDECREL-6781
        //m_btnRadioCreateNewDoc.setLayoutData(new GridData(SWT.LEFT, SWT.NONE, true, false, 1, 1));
        m_btnRadioCreateNewDoc.setLayoutData(new GridData(SWT.RIGHT, SWT.NONE, true, false, 1, 1));
        
        m_btnRadioCreateNewDoc.setText(m_registry.getString("newDocRB.NAME", "Key Not Found "));
        
        //TCDECREL-6781
        new Label(docCategoryComposite, SWT.NONE);
        
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TCDECREL-7122: Commented null check to update values when table selection is changed. 
        // Previously it was retaining value for the first selected object
        
//    	if(m_propMap == null)
//    	{
    		m_propMap = new SimplePropertyMap();
    		m_propMap.addAll(propMap);
//    	}
    	super.load(m_propMap);
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#save(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        
        if (m_btnRadioCopyDoc.getSelection())
        {
            handleCopyDocOperation(propMap);
        }
        
        return true;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.COPY_DOC))
        {
            try
            {
                if (null != m_propMap)
                {
                    load(m_propMap);
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.COPY_DOC, this);
    }
    
    @Override
    public void dispose()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.unregisterSubscriber(NationalOilwell.COPY_DOC, this);
        super.dispose();
    }
    
    private void handleCopyDocOperation(IPropertyMap propMap) throws TCException
    {
        // 1. get target items latest rev.
        // 2. get all attached dataset
        // 3. setTagArray to revision prop map
        
        TCComponentItemRevision docRev = getItemRevOfSelectedItem();
        TCComponent[] datasetList = docRev.getRelatedComponents(NationalOilwell.IMAN_SPECIFICATION);
        if (datasetList != null && datasetList.length > 0)
        {
            propMap.getCompound(NationalOilwell.REVISION).setTagArray(NationalOilwell.IMAN_SPECIFICATION, datasetList);
        }
    }
    
    private TCComponentItemRevision getItemRevOfSelectedItem() throws TCException
    {
        TCComponent selectedObject = NewItemCreationHelper.getTargetComponent();
        TCComponentItemRevision selectedItemRev = null;
        if (selectedObject == null)
        {
            selectedObject = NewItemCreationHelper.getSelectedComponent();
        }
        if (selectedObject instanceof TCComponentItem)
        {
            selectedItemRev = ((TCComponentItem) selectedObject).getLatestItemRevision();
        }
        else if (selectedObject instanceof TCComponentItemRevision)
        {
            TCComponentItem item = ((TCComponentItemRevision) selectedObject).getItem();
            selectedItemRev = item.getLatestItemRevision();
        }
        
        return selectedItemRev;
    }
    
    /** The m_btn radio copy doc. */
    private Button m_btnRadioCopyDoc;
    
    /** The m_btn radio create new doc. */
    private Button m_btnRadioCreateNewDoc;
    
    /** The m_registry. */
    private Registry m_registry = null;
    
    private IPropertyMap m_propMap = null;
    private final static int HORIZONTAL_SPAN_FOR_RADIOBUTTON_COMPOSITE = 4;


    
}