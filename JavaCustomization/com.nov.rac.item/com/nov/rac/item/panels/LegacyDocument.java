package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

public class LegacyDocument  extends NewDocumentInfoPanel {

	public LegacyDocument(Composite parent, int style) {
		super(parent, SWT.NONE);
        
	}
	
	@Override
	public boolean createUI() throws TCException {
		super.createUI();
		m_revisionText.setText("01");
		m_revisionText.setEnabled(true);
		m_revisionText.setEditable(true);
		return true;
	}

	

}
