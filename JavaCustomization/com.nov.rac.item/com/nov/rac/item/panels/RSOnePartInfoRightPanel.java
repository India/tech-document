package com.nov.rac.item.panels;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Vector;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.NOVClassificationDialog;
import com.nov.rac.item.dialog.NOVLocaleDialog;
import com.nov.rac.item.dialog.RSOneClassificationDialog;
import com.nov.rac.item.helpers.ClassHierarchyCreator;
import com.nov.rac.item.helpers.ClassificationAutoComplete;
import com.nov.rac.item.helpers.ClassificationHelper;
import com.nov.rac.item.helpers.CopyPropertyHelper;
import com.nov.rac.item.helpers.MandatoryLabelFieldValidator;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.item.helpers.RSOneUOMLOVHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class RSOnePartInfoRightPanel extends AbstractUIPanel implements ISubscriber, ILoadSave, IPublisher
{

    public RSOnePartInfoRightPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        m_session = getSession();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    protected TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
        
    }
    
	protected GridData getSubCompositeLayoutData()
    {
        
        return  new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
    }
    
    protected GridLayout getSubCompositeLayout()
    {
        GridLayout l_glDocPanel = new GridLayout(7, true);
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginHeight = 0;
        
        return l_glDocPanel;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1);
        l_composite.setLayoutData(gridData);
        GridLayout gridLayout = new GridLayout(4, true);
       // gridLayout.marginWidth = 0;
        l_composite.setLayout(gridLayout);
        
        createCopyAllRow(l_composite);
        
        createOrgRow(l_composite);
        
        createNameRow(l_composite);
        
        createClassRow(l_composite);
        
        createSendToClassificationRow(l_composite);
        
        createUOMRow(l_composite);
        
        createDescRow(l_composite);
        
        createTraceabilityRow(l_composite);
        
        setInitialValues();
        
        registerProperties();
        
        createUIPost();
        
        return true;
    }
    

    private void createCopyAllRow(Composite l_composite)
    {
        new Label(l_composite, SWT.NONE);
        
        m_copyAllBtn = new Button(l_composite, SWT.CHECK);
        m_copyAllBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 3, 1));
        m_copyAllBtn.setText(m_registry.getString("RSOnePartInfoPanel.copyall"));
        m_copyAllBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
            	if(m_copyAllBtn.getSelection())
            	{
            		int msgCode = getMessage(m_registry.getString("confirmation.msg"), m_registry.getString("copyAll.msg"), SWT.YES|SWT.NO|SWT.ICON_QUESTION);
                	
                	if(msgCode == SWT.YES)
    	            {
    	                final IController controller = ControllerFactory.getInstance().getDefaultController();
    	                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_ALL_PROPERTY, NationalOilwell.RSONE_ORG,  m_orgCombo.getText());
    	                controller.publish(event);
    	                
    	            }
                	
                	m_copyAllBtn.setSelection(false);
            	}
            	
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
        
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
    	org.eclipse.swt.widgets.MessageBox dialog = new org.eclipse.swt.widgets.MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
      /* private void createOrgRow(Composite l_composite) throws TCException
    {
        
        m_copyAllBtn = new Button(l_composite, SWT.CHECK);
        m_copyAllBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 7, 1));
        m_copyAllBtn.setText(m_registry.getString("RSOnePartInfoPanel.copyall"));
        m_copyAllBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_ALL_PROPERTY, NationalOilwell.RSONE_ORG,  m_orgCombo.getText());
                controller.publish(event);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
        
    }*/

    private void createOrgRow(Composite l_composite) throws TCException
    {
        Composite l_orgComposite = new Composite(l_composite, SWT.NONE);
        l_orgComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        GridLayout gl_orgLabel = new GridLayout(2, false);
        gl_orgLabel.marginTop = 0;
        gl_orgLabel.marginWidth = 0;
        l_orgComposite.setLayout(gl_orgLabel);
        
        m_orgCheckBox = new Button(l_orgComposite, SWT.CHECK);
        m_orgCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_orgCheckBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                
                CopyPropertyHelper copyProperty = new CopyPropertyHelper();
                copyProperty.setProperties(new String[]{"IMAN_master_form", NationalOilwell.RSONE_ORG}, new String[]{m_orgCombo.getText()}, m_registry.getString("RSOnePartInfoPanel.org"));
                
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, copyProperty,  null);
                controller.publish(event);
                m_orgCheckBox.setSelection(false);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
        
        m_orgLabel = new Label(l_orgComposite, SWT.NONE);
        m_orgLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_orgLabel.setText(m_registry.getString("RSOnePartInfoPanel.org"));
        
        Composite l_orgComboComposite = new Composite(l_composite, SWT.NONE);
        l_orgComboComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout l_glDocPanel = new GridLayout(7, true);  
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginTop = 0;
        l_orgComboComposite.setLayout(l_glDocPanel);
        TCComponentListOfValuesType lovType = (TCComponentListOfValuesType) getSession().getTypeComponent(
                "ListOfValues");
        TCComponentListOfValues rsOneOrgLov[] = lovType.find( NationalOilwell.RSOne_ORG_LOV);
        
        m_orgCombo = new Combo(l_orgComboComposite, SWT.NONE);
        m_orgCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        
        UIHelper.setAutoComboArray(m_orgCombo, LOVUtils.getLovStringValues(rsOneOrgLov[0]));
        
        Label empty2 = new Label(l_orgComboComposite, SWT.NONE);
        //empty2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }

    private void createNameRow(Composite l_composite)
    {
        Composite l_nameComposite = new Composite(l_composite, SWT.NONE);
        l_nameComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false,1, 1));
        GridLayout gl_nameLabel = new GridLayout(2, false);
        gl_nameLabel.marginTop = 0;
        gl_nameLabel.marginWidth = 0;
        l_nameComposite.setLayout(gl_nameLabel);
        
        m_nameCheckBox = new Button(l_nameComposite, SWT.CHECK);
        m_nameCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_nameCheckBox.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                CopyPropertyHelper copyProperty = new CopyPropertyHelper();
                copyProperty.setProperties(new String[]{NationalOilwell.OBJECT_NAME}, m_nameCombo.getText(), m_registry.getString("RSOnePartInfoPanel.name"));
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, copyProperty, null);
                controller.publish(event);
                m_nameCheckBox.setSelection(false);
            }

          
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
        m_NameLabel = new Label(l_nameComposite, SWT.NONE);
        m_NameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_NameLabel.setText(m_registry.getString("RSOnePartInfoPanel.name"));
        
        Composite l_partNameComposite = new Composite(l_composite, SWT.NONE);
        l_partNameComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
        gridLayout.marginTop = 0;
        l_partNameComposite.setLayout(gridLayout);
       
        m_nameCombo = new Combo(l_partNameComposite, SWT.NONE);
        //m_nameCombo.setTextLimit(NationalOilwell.NAME_TEXT_LIMIT);
        m_nameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        String groupName = m_session.getGroup().toString();
        String[] suggestedValues = RSOneSuggestedByNameLOVHelper.getSuggestedNameValues(m_session,
                "_SuggestedNamesByGroupQuery_", new String[] { m_registry.getString("Group.NAME") },
                new String[] { groupName }, m_registry.getString("Name.NAME"));
        m_nameCombo.setItems(suggestedValues);
        int length = PreferenceHelper.getParsedPrefValueForGroup(
				"NOV_ObjectName_Length_For_Groups",
				TCPreferenceService.TC_preference_site, groupName, "|");
        m_nameCombo.setTextLimit(length);
        new AutoCompleteField(m_nameCombo, new ComboContentAdapter(), suggestedValues);
        if (showLocalizeButton())
        {
            nameLocalePopupButton = new Button(l_partNameComposite, SWT.NONE);
            GridData m_localepopupgd = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            nameLocalePopupButton.setLayoutData(m_localepopupgd);
            m_localepopupgd.horizontalIndent = 8;
            Image localeImage = new Image(l_composite.getDisplay(), m_registry.getImage("novlocalize.ICON"), SWT.NONE);
            nameLocalePopupButton.setImage(localeImage);
            nameLocalePopupButton.addSelectionListener(new SelectionListener()
            {
                
                @Override
                public void widgetSelected(SelectionEvent arg0)
                {
                    
                    NOVLocaleDialog localeDialog = new NOVLocaleDialog(nameLocalePopupButton.getShell(), "Name",
                            m_nameCombo.getText());
                    localeDialog.open();
                    localeDialog.setMasterLocaleText(m_registry.getString("RSOnePartInfoPanel.name"));
                     
                  /*  TCComponent component = NewItemCreationHelper.getTargetComponent();
                    
                    try
                    {
                        String name = component.getStringProperty("object_name");
                        component.setStringProperty("object_name", m_nameCombo.getText());
                        
                        LocalizePopupDialog dialog = new LocalizePopupDialog(getComposite().getShell(), "object_name",
                                component);
                        dialog.open();
                        component.setStringProperty("object_name", name);
                    }
                    catch (TCException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }*/
                    
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent arg0)
                {
                    
                }
            });
            
        }
        
        else
        {
            new Label(l_partNameComposite, SWT.NONE);
        }
    }
    
    private void createClassRow(Composite l_composite) throws TCException
    {
        Composite l_classComposite = new Composite(l_composite, SWT.NONE);
        l_classComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        GridLayout gl_classLabel = new GridLayout(2, false);
        gl_classLabel.marginHeight = 0;
        gl_classLabel.marginWidth = 0;
        l_classComposite.setLayout(gl_classLabel);
        
        m_classCheckBox = new Button(l_classComposite, SWT.CHECK);
        m_classCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_classCheckBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                CopyPropertyHelper copyProperty = new CopyPropertyHelper();
                copyProperty.setProperties(new String[]{NationalOilwell.ICS_subclass_name}, m_classText.getText(), m_registry.getString("RSOnePartInfoPanel.class"));
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, copyProperty, null );
                controller.publish(event);
                m_classCheckBox.setSelection(false);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        });
        
        m_ClassLabel = new Label(l_classComposite, SWT.NONE);
        m_ClassLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_ClassLabel.setText(m_registry.getString("RSOnePartInfoPanel.class"));
        
        Composite l_partClassComposite = new Composite(l_composite, SWT.NONE);
        l_partClassComposite.setLayoutData(getSubCompositeLayoutData());
        //setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        //GridLayout gridLayout = new GridLayout(7, true);
        //gridLayout.marginWidth = 0;
        l_partClassComposite.setLayout(getSubCompositeLayout());
        
        m_classText = new Text(l_partClassComposite, SWT.BORDER);
        m_classText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        
        addModifyListener(m_classText);
        
        Composite m_composite = new Composite(l_partClassComposite, SWT.NONE);
        GridData m_classCompgd = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_composite.setLayout(new GridLayout(1, true));
        m_composite.setLayoutData(m_classCompgd);
        m_classCompgd.horizontalIndent = 5;
        m_classButton = new Button(m_composite, SWT.TOGGLE);
        GridData m_classgd = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        m_classButton.setLayoutData(m_classgd);
        Image classImage = new Image(l_composite.getDisplay(), m_registry.getImage("refresh.Icon"), SWT.NONE);
        m_classButton.setImage(classImage);
       
        m_classButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                RSOneClassificationDialog classificationDialog = new RSOneClassificationDialog(m_classButton.getShell(),m_classText );
                Point btnLocation = m_classButton.toDisplay(m_classButton.getLocation());
                Point btnSize = m_classButton.getSize();
                classificationDialog.setLocation(btnLocation, btnSize);
                classificationDialog.open();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });

    }

    /**
     * @throws TCException 
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws NoSuchMethodException 
     * @throws MissingResourceException 
     * @throws ExecutionException 
     * 
     */
    public boolean createUIPost() throws TCException 
    {
        ClassHierarchyCreator classhierarchycreator = new ClassHierarchyCreator();
        String rootClassId = "MDI"; // Class ID for "Rig Solutions";
        
            try
            {
                classhierarchycreator.createStructure(rootClassId);
            }
            catch (Exception e)
            {
                throw new TCException(e);
            }
         
       
        String[] cp_array = (String[])classhierarchycreator.getLinearStructure().toArray(new String[0]);
        String [] classID_array = (String[]) classhierarchycreator.getClassID().toArray(new String[0]);
        String[] classpuid_array = (String[]) classhierarchycreator.getClassPuid().toArray(new String[0]);
        Map<String, String> classiMap = new HashMap<String, String>();
        Map<String, String> classUidMap = new HashMap<String, String>();
        new ClassificationAutoComplete(m_classText, new TextContentAdapter(), cp_array);
        String[] classificationArray = new String[cp_array.length];
        for(int inx=0; inx < cp_array.length; inx++)
        {
           // m_classText.setData(cp_array[inx],classID_array[inx]);
            
           // ClassificationHelper clasHelper = new ClassificationHelper();
            if(cp_array[inx].contains("->"))
            {
            	classificationArray[inx] = cp_array[inx].substring(cp_array[inx].lastIndexOf(">")+1);
            }
            
            classiMap.put(classificationArray[inx],classID_array[inx]);
            classUidMap.put(classificationArray[inx], classpuid_array[inx]);
            
        }
        ClassificationHelper.setClassificationMap(classiMap);
        ClassificationHelper.setClassificationUIDMap(classUidMap);
        
        return true;
    }
    
    
    private void addModifyListener(Text m_classText2)
    {
        m_classText2.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                String value = m_classText.getText();
             /*   IController theController = ControllerFactory.getInstance().getDefaultController();
                theController.publish(new PublishEvent(getpublisher(), NationalOilwell.CLASSIFICATION_EVENT , m_classText.getData(m_classText.getText()), ""));*/
                if(value.contains("->"))
                {
                    value = value.substring(value.lastIndexOf(">")+1);
                    m_classText.setText(value);
                }
               
            }
        });
        
    }
    
    
    protected IPublisher getpublisher()
    {
        // TODO Auto-generated method stub
        return this;
    }

    private void createSendToClassificationRow(Composite l_composite)
    {
        Label empty = new Label(l_composite, SWT.NONE);
        empty.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        
        Composite l_partClassComposite = new Composite(l_composite, SWT.NONE);
        l_partClassComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        GridLayout gridLayout = new GridLayout(4, false);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        l_partClassComposite.setLayout(gridLayout);
        
        m_sendToClassificationBtn = new Button(l_partClassComposite, SWT.CHECK);
        m_sendToClassificationBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1));
        m_sendToClassificationBtn.setText(m_registry.getString("RSOnePartInfoPanel.sendToClassification"));
        new Label(l_partClassComposite, SWT.NONE);
        m_sendToClassificationBtn.setEnabled(false);
        
        m_sendToClassificationBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.SEND_TO_CLASSIFICATION, m_sendToClassificationBtn.getSelection(), null);
                controller.publish(event);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private void createUOMRow(Composite l_composite)
    {
        Composite l_uomComposite = new Composite(l_composite, SWT.NONE);
        l_uomComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        GridLayout gl_uomLabel = new GridLayout(2, false);
        gl_uomLabel.marginHeight = 0;
        gl_uomLabel.marginWidth = 0;
        l_uomComposite.setLayout(gl_uomLabel);
        
        m_uomCheckBox = new Button(l_uomComposite, SWT.CHECK);
        m_uomCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_uomCheckBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                CopyPropertyHelper copyProperty = new CopyPropertyHelper();
                copyProperty.setProperties(new String[]{"IMAN_master_form", NationalOilwell.RSONE_UOM}, m_uomCombo.getText(), m_registry.getString("RSOnePartInfoPanel.uom"));
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, copyProperty, null);
                controller.publish(event);
                m_uomCheckBox.setSelection(false);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
        m_uomLabel = new Label(l_uomComposite, SWT.NONE);
        m_uomLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_uomLabel.setText(m_registry.getString("RSOnePartInfoPanel.uom"));
        
        
        Composite l_partUOMComposite = new Composite(l_composite, SWT.NONE);
        l_partUOMComposite.setLayoutData(getSubCompositeLayoutData());
        /*new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1)
         * GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
*/      l_partUOMComposite.setLayout(getSubCompositeLayout());
        
        m_uomCombo = new Combo(l_partUOMComposite, SWT.NONE);
        m_uomCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        m_uomCombo.setText("  ");
        UIHelper.setAutoComboArray(m_uomCombo, (Vector<String>)RSOneUOMLOVHelper.getUOMLOVValues());
        
        new Label(l_partUOMComposite, SWT.NONE);
    }
    
    private void createDescRow(Composite l_composite)
    {
        Composite l_descComposite = new Composite(l_composite, SWT.NONE);
        l_descComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 3));
        GridLayout gl_descLabel = new GridLayout(2, false);
        gl_descLabel.marginHeight = 0;
        gl_descLabel.marginWidth = 0;
        l_descComposite.setLayout(gl_descLabel);
        
        m_descCheckBox = new Button(l_descComposite, SWT.CHECK);
        m_descCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_descCheckBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                CopyPropertyHelper copyProperty = new CopyPropertyHelper();
                copyProperty.setProperties( new String[]{NationalOilwell.OBJECT_DESC}, m_descText.getText(), m_registry.getString("RSOnePartInfoPanel.desc"));
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, copyProperty, null);
                controller.publish(event);
                m_descCheckBox.setSelection(false);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
        
        m_descLabel = new Label(l_descComposite, SWT.NONE);
        m_descLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_descLabel.setText(m_registry.getString("RSOnePartInfoPanel.desc"));
        
        Composite l_partDescComposite = new Composite(l_composite, SWT.NONE);
        l_partDescComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 3));
        GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight=0;
        l_partDescComposite.setLayout(gridLayout);
        
        m_descText = new Text(l_partDescComposite, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        
		UIHelper.limitCharCountInTextWithPopUpMsg(m_descText,
				NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
				m_registry.getString("ExceedingDescTextLimit.msg"));
		
        m_descText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 3));
        
        UIHelper.enableSelectAll(m_descText);
        
        if (showLocalizeButton())
        {
            descLocalePopupButton = new Button(l_partDescComposite, SWT.NONE);
            GridData m_localepopupgd = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            descLocalePopupButton.setLayoutData(m_localepopupgd);
            m_localepopupgd.horizontalIndent = 8;
            Image localeImage = new Image(l_composite.getDisplay(), m_registry.getImage("novlocalize.ICON"), SWT.NONE);
            descLocalePopupButton.setImage(localeImage);
            descLocalePopupButton.addSelectionListener(new SelectionListener()
            {
                
                @Override
                public void widgetSelected(SelectionEvent arg0)
                {
                    NOVLocaleDialog localeDialog = new NOVLocaleDialog(descLocalePopupButton.getShell());
                    localeDialog.open();
                    localeDialog.setMasterLocaleText(m_registry.getString("RSOnePartInfoPanel.desc"));
                }
                
                @Override
                public void widgetDefaultSelected(SelectionEvent arg0)
                {
                    // TODO Auto-generated method stub
                    
                }
            });
        }
        
        else
        {
            new Label(l_partDescComposite, SWT.NONE);
        }
            
       new Label(l_partDescComposite, SWT.NONE);
       new Label(l_partDescComposite, SWT.NONE);
      //  new Label(l_composite, SWT.NONE);
      //  new Label(l_composite, SWT.NONE);
      //  new Label(l_composite, SWT.NONE);
    }
    
    private void createTraceabilityRow(Composite l_composite)
    {
        m_Traceabilitycomposite = new Composite(l_composite, SWT.Hide);
        GridData m_gd = new GridData(SWT.LEFT, SWT.CENTER, true, false,1, 1);
        m_Traceabilitycomposite.setLayoutData(m_gd);
        GridLayout gl_traceabilityLabel = new GridLayout(3, false);
        gl_traceabilityLabel.marginTop = 0;
        gl_traceabilityLabel.marginWidth = 0;
        m_Traceabilitycomposite.setLayout(gl_traceabilityLabel);
        
        m_traceabilityCheckBox = new Button(m_Traceabilitycomposite, SWT.CHECK);
        m_traceabilityCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_traceabilityCheckBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
            	publishTraceabilityCopyEvent(new String[]{"IMAN_master_form", NationalOilwell.RSONE_LOTCONTROL}, m_lotControlledButton.getSelection());
            	publishTraceabilityCopyEvent(new String[]{"IMAN_master_form", NationalOilwell.RSONE_SERIALIZE}, m_serializedButton.getSelection());
            	publishTraceabilityCopyEvent(new String[]{"IMAN_master_form", NationalOilwell.RSONE_QAREQUIRED}, m_NAButton.getSelection());
            	m_traceabilityCheckBox.setSelection(false);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });

        m_traceabilityLabel = new Label(m_Traceabilitycomposite, SWT.NONE);
        GridData tr_gd = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
        tr_gd.horizontalIndent = 0;
        m_traceabilityLabel.setLayoutData(tr_gd);
        m_traceabilityLabel.setText(m_registry.getString("RSOnePartInfoPanel.traceability"));
       
        m_helpIcon = new Label(m_Traceabilitycomposite, SWT.NONE);
        GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
        gd.horizontalIndent = 5;
        m_helpIcon.setLayoutData(gd);
         
        Image classImage = new Image(l_composite.getDisplay(), m_registry.getImage("help.Icon"), SWT.NONE);
        m_helpIcon.setImage(classImage);
        m_Traceabilitycomposite.pack();
        m_helpIcon.addMouseListener(new MouseListener()
        {
            
            @Override
            public void mouseUp(MouseEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
            
            @Override
            public void mouseDown(MouseEvent arg0)
            {
                String helpURL = "cmd /c start "+m_registry.getString("traceabilityHelp.LINK");
                try {
                    Runtime.getRuntime().exec(helpURL);
                } catch (IOException e1) {
                    
                    e1.printStackTrace();
                    MessageBox.post( NationalOilwell.NOHELP , NationalOilwell.SEEADMIN , MessageBox.ERROR );
                }
            }
            
            @Override
            public void mouseDoubleClick(MouseEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        Composite l_checkBoxcomposite = new Composite(l_composite, SWT.NONE);
        GridData l_gdCheckBox = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
        l_checkBoxcomposite.setLayoutData(l_gdCheckBox);
        l_gdCheckBox.horizontalIndent = 20;
        GridLayout gridLayout = new GridLayout(3, false);
        gridLayout.marginWidth = 0;
        gridLayout.marginTop = 0;
        l_checkBoxcomposite.setLayout(gridLayout);
        m_lotControlledButton = new Button(l_checkBoxcomposite, SWT.CHECK);
        m_lotControlledButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_lotControlledButton.setText(m_registry.getString("RSOnePartInfoPanel.lotControlled"));
        
        m_lotControlledButton.addListener(SWT.Selection, listener);
        
        m_serializedButton = new Button(l_checkBoxcomposite, SWT.CHECK);
        m_serializedButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_serializedButton.setText(m_registry.getString("RSOnePartInfoPanel.serialized"));
        m_serializedButton.addListener(SWT.Selection, listener);
       
        m_NAButton = new Button(l_checkBoxcomposite, SWT.CHECK);
        m_NAButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_NAButton.setText(m_registry.getString("RSOnePartInfoPanel.NA"));
        m_NAButton.addListener(SWT.Selection, listener);
    }
    
    Listener listener = new Listener()
    {
        
        @Override
        public void handleEvent(Event event)
        {
            enableDisableButtons();
        }
    };
    
    private void enableDisableButtons()
    {
        boolean isLotControlled = m_lotControlledButton.getSelection();
        boolean isSerialized = m_serializedButton.getSelection();
        boolean isNA = m_NAButton.getSelection();
        
    	if(isLotControlled || isSerialized || isNA)
    	{
    	    m_labelFieldValidator.setValid(true);
    	    moveTraceabilityLabel();
    	}
    	else
    	{
    	    m_labelFieldValidator.setValid(false);
    	    moveTraceabilityLabel();
    	}
    	
    	if(isLotControlled || isSerialized)
    	{
    	    m_NAButton.setEnabled(false);
            m_NAButton.setSelection(false); 
    	}
    	else
    	{
    	    m_NAButton.setEnabled(true);
    	}
    	
        if(isNA)
        {
            m_lotControlledButton.setEnabled(false);
            m_lotControlledButton.setSelection(false);
            
            m_serializedButton.setEnabled(false);
            m_serializedButton.setSelection(false);
        }
        else
        {
            m_lotControlledButton.setEnabled(true);
            m_serializedButton.setEnabled(true);
        }
    }
    
    protected void moveTraceabilityLabel()
    {
        Rectangle rect = m_traceabilityLabel.getBounds();
        m_traceabilityLabel.setLocation(rect.x + 1, rect.y);
        m_traceabilityLabel.setLocation(rect.x, rect.y);
    }
    
    private void publishTraceabilityCopyEvent(String[] chainPropertyName, Object propertyValue)
    {
    	IController controller = ControllerFactory.getInstance().getDefaultController();
    	CopyPropertyHelper copyProperty = new CopyPropertyHelper();
        copyProperty.setProperties( chainPropertyName, propertyValue, m_registry.getString("RSOnePartInfoPanel.traceability"));
        PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, copyProperty, null);
        controller.publish(event);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    public boolean showLocalizeButton()
    {
        TCPreferenceService prefServ = m_session.getPreferenceService();
        Boolean isPrefExist = prefServ.getLogicalValue("NOV_show_localization_button");
        if (isPrefExist != null)
            isLocale = isPrefExist;
        return isLocale;
        
    }
    
    ISubscriber getSubscriber()
    {
        return (ISubscriber) this;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        final PublishEvent event1 = event;
        if(event.getPropertyName().equals(NationalOilwell.SELECTEDCLASS))
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                
                @Override
                public void run()
                {
                    m_classText.setText(event1.getNewValue().toString());
                }
            });
          
        }
        
        if(event.getPropertyName().equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL))
        {
            m_nameCombo.setEnabled(false);
            m_NAButton.setSelection(true);
            m_serializedButton.setSelection(false);
            m_lotControlledButton.setSelection(false);
            enableDisableButtons();
        }
        
    }
    
    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.registerSubscriber(NationalOilwell.SELECTEDCLASS, getSubscriber());
        l_controller.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL, getSubscriber());
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_classText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ICS_subclass_name)));
        m_descText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_DESC)));
        m_nameCombo.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_NAME)));
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
       
        m_orgCombo.setText(PropertyMapHelper.handleNullForArray(masterPropertyMap.getStringArray(NationalOilwell.RSONE_ORG))[0]);
        
        m_uomCombo.setText(PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.RSONE_UOM)));
        m_lotControlledButton.setSelection(masterPropertyMap.getLogical(NationalOilwell.RSONE_LOTCONTROL));
        m_serializedButton.setSelection(masterPropertyMap.getLogical(NationalOilwell.RSONE_SERIALIZE));
        m_NAButton.setSelection(masterPropertyMap.getLogical(NationalOilwell.RSONE_QAREQUIRED));
        
        enableDisableButtons();
        
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        setItemProperties(propMap);
        
        setMasterFormProperties(propMap);
        return true;
    }

    private void setItemProperties(IPropertyMap propMap) throws TCException
    {
        propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.OBJECT_NAME), m_nameCombo.getText().trim());
        propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.OBJECT_DESC), m_descText.getText().trim());
        propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.ICS_subclass_name), m_classText.getText().trim());
       
        if(m_classText.getText().trim()!="")
        {
            propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.ICS_classified), "Y");
        }
      //  propMap.setString("nov4rsone_uom", m_uomCombo.getText());
    }
    
    
    private void setMasterFormProperties(IPropertyMap propMap)
    {
       
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            setTraceability(masterPropertyMap);
            masterPropertyMap.setStringArray(NationalOilwell.RSONE_ORG, new String[]{m_orgCombo.getText().trim()});
          //  masterPropertyMap.setString(NationalOilwell.RSONE_ORG, m_orgCombo.getText());
            masterPropertyMap.setString(NationalOilwell.RSONE_UOM, m_uomCombo.getText());
            
           // masterPropertyMap.setStringArray("nov4_mis_serialize", serialiseOption);
            propMap.setCompound("IMAN_master_form", masterPropertyMap);
        }
        
        /*IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            revisionPropertyMap.setString("item_revision_id", "");
            propMap.setCompound("revision", revisionPropertyMap);
        }*/
    }

    private void setTraceability(IPropertyMap masterPropertyMap)
    {
            masterPropertyMap.setLogical(NationalOilwell.RSONE_LOTCONTROL, m_lotControlledButton.getSelection());
            
            masterPropertyMap.setLogical(NationalOilwell.RSONE_SERIALIZE, m_serializedButton.getSelection());
            masterPropertyMap.setLogical(NationalOilwell.RSONE_QAREQUIRED, m_NAButton.getSelection());
    }
    

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
  
    @Override
    public void dispose()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(NationalOilwell.SELECTEDCLASS, this);
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL, this);
        super.dispose();
    }
    
    public void setInitialValues()
    {
        m_orgCombo.select(0);
        m_nameCombo.setText("");
        m_classText.setText("");
        m_uomCombo.setText("");
        m_descText.setText("");
        m_lotControlledButton.setSelection(false);
        m_serializedButton.setSelection(false);
        m_NAButton.setSelection(false);
        m_lotControlledButton.setEnabled(true);
        m_serializedButton.setEnabled(true);
        m_NAButton.setEnabled(true);
    }
    
    protected MandatoryLabelFieldValidator m_labelFieldValidator = new MandatoryLabelFieldValidator();
    protected Registry m_registry;
    protected Label m_orgLabel;
    protected Label m_NameLabel;
    protected Label m_ClassLabel;
    protected Combo m_orgCombo;
    protected Combo m_nameCombo;
    protected Text m_classText;
    protected Button m_classButton;
    protected Button m_sendToClassificationBtn;
    protected Label m_uomLabel;
    protected Combo m_uomCombo;
    protected Label m_descLabel;
    protected Text m_descText;
    protected Label m_traceabilityLabel;
    protected Button m_lotControlledButton;
    protected Button m_serializedButton;
    protected Button m_NAButton;
    protected TCSession m_session;
    private boolean isLocale = false;
    protected Button nameLocalePopupButton;
    protected Button descLocalePopupButton;
    NOVClassificationDialog classificationDialog;
    private Composite l_composite;
    
    protected Label m_mandatoryIcon, m_helpIcon;
    protected Composite m_Traceabilitycomposite;
    
    protected Button m_orgCheckBox;
    protected Button m_nameCheckBox;
    protected Button m_classCheckBox;
    protected Button m_uomCheckBox;
    protected Button m_descCheckBox;
    protected Button m_traceabilityCheckBox;
    protected Button m_copyAllBtn;
    protected static String itemType;
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    //protected IPropertyMap subpPropertyMap;
   // protected static Vector<String> rsOneOrgLovValues;
    
    public static final int FIELD_ORG = 1;
    public static final int FIELD_CLASS_TEXT = 2;
    public static final int FIELD_UOM = 3;
    public static final int FIELD_TRACABILITY = 4;
    public static final int FIELD_NAME = 5;
    
    
    
}
