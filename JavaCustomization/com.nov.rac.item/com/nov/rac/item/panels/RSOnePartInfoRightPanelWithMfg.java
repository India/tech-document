package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.item.dialog.NOVRSOneMfgDialog;
import com.nov.rac.item.helpers.ManufacturerHelper;
import com.nov.rac.item.helpers.RSOneOperationTypePanelHelper;
import com.nov.rac.item.panels.rsone.RSOneCreateRightPartInfoPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOnePartInfoRightPanelWithMfg extends RSOneCreateRightPartInfoPanel
{

    public RSOnePartInfoRightPanelWithMfg(Composite parent, int style)
    {
        super(parent, style);
       
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        // TODO Auto-generated method stub
        super.createUI();
        
        Composite l_composite = getComposite();
      
        createMfgRow(l_composite);
        
        createMfgNumberRow(l_composite);
        
        createRequestMfgRow(l_composite);
        
        return true;
    }

    private void createMfgRow(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        
        GridLayout gl_mfgLabel = new GridLayout(2, false);
        gl_mfgLabel.marginTop = 0;
        gl_mfgLabel.marginWidth = 0;
        l_composite.setLayout(gl_mfgLabel);
        
        m_mfgCheckBox = new Button(l_composite, SWT.CHECK);
        m_mfgCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        
        m_mfgLabel = new Label(l_composite, SWT.NONE);
        m_mfgLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        m_mfgLabel.setText(m_registry.getString("RSOnePartInfoPanel.mfg"));
        
        Composite l_mfgComboComposite = new Composite(parent, SWT.NONE);
        l_mfgComboComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout l_glDocPanel = new GridLayout(7, true);  
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginTop = 0;
        l_mfgComboComposite.setLayout(l_glDocPanel);
        
        m_mfgCombo = new Combo(l_mfgComboComposite, SWT.NONE);
        m_mfgCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 6, 1));
        ManufacturerHelper novManufacturerHelper = new ManufacturerHelper();
        try {
//			UIHelper.setAutoComboArray(m_mfgCombo, novManufacturerHelper.execute(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      //  UIHelper.setAutoComboArray(m_mfgCombo, new String[]{"UNSPECIFIED"});
        m_mfgCombo.select(0);
        new Label(l_mfgComboComposite, SWT.NONE);
    }
    
    private void createMfgNumberRow(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        GridLayout gl_mfgLabel = new GridLayout(2, false);
        gl_mfgLabel.marginTop = 0;
        gl_mfgLabel.marginWidth = 0;
        l_composite.setLayout(gl_mfgLabel);
        
        
        m_mfgNumberCheckBox = new Button(l_composite, SWT.CHECK);
        m_mfgNumberCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        
        m_mfgHashLabel = new Label(l_composite, SWT.NONE);
        m_mfgHashLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        m_mfgHashLabel.setText(m_registry.getString("RSOnePartInfoPanel.mfghash"));
        
        Composite l_mfgComboComposite = new Composite(parent, SWT.NONE);
        l_mfgComboComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout l_glDocPanel = new GridLayout(7, true);  
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginTop = 0;
        l_mfgComboComposite.setLayout(l_glDocPanel);
        
        m_mfgHashText = new Text(l_mfgComboComposite, SWT.BORDER | SWT.READ_ONLY);
        m_mfgHashText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 6, 1));
       
        new Label(l_mfgComboComposite, SWT.NONE);
        
    }
    
    private void createRequestMfgRow(Composite parent)
    {
        new Label(parent, SWT.NONE);
        
        Composite l_mfgComboComposite = new Composite(parent, SWT.NONE);
        l_mfgComboComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout l_glDocPanel = new GridLayout(7, true);  
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginTop = 0;
        l_mfgComboComposite.setLayout(l_glDocPanel);
        
        m_requestMfgBtn = new Button(l_mfgComboComposite, SWT.NONE);
        m_requestMfgBtn.setText(m_registry.getString("RSOnePartInfoPanel.requestMfg")); 
        m_requestMfgBtn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 6, 1));
        m_requestMfgBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                NOVRSOneMfgDialog mfgDialog = new NOVRSOneMfgDialog(m_requestMfgBtn.getShell());
                mfgDialog.open();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
        new Label(l_mfgComboComposite, SWT.NONE);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        IPropertyMap revisionPropMap = propMap.getCompound("revision");
        if(RSOneOperationTypePanelHelper.getSelectedPartType().equalsIgnoreCase(NationalOilwell.PART_BO_TYPE))
        {
            m_mfgCombo.setText(PropertyMapHelper.handleNullForArray(revisionPropMap.getStringArray(NationalOilwell.MANUFACTURER_NAME))[0]);
            m_mfgHashText.setText(PropertyMapHelper.handleNullForArray(revisionPropMap.getStringArray(NationalOilwell.MANUFACTURER_PARTNUMBER))[0]);
        }
        else
        {
            m_mfgCombo.setText(PropertyMapHelper.handleNullForArray(revisionPropMap.getStringArray(NationalOilwell.NOV4_MFG_NAME))[0]);
            m_mfgHashText.setText(PropertyMapHelper.handleNullForArray(revisionPropMap.getStringArray(NationalOilwell.NOV4_MFG_NUMBER))[0]);
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        
        IPropertyMap revisionPropMap = propMap.getCompound("revision");
        
        if(RSOneOperationTypePanelHelper.getSelectedPartType().equalsIgnoreCase(NationalOilwell.PART_BO_TYPE))
        {
            revisionPropMap.setStringArray(NationalOilwell.MANUFACTURER_NAME, new String[] {m_mfgCombo.getText().trim()});
            revisionPropMap.setStringArray(NationalOilwell.MANUFACTURER_PARTNUMBER,new String[] { m_mfgHashText.getText().trim()});
        }
        else
        {
            revisionPropMap.setStringArray(NationalOilwell.NOV4_MFG_NAME,new String[] {m_mfgCombo.getText().trim()});
            revisionPropMap.setStringArray(NationalOilwell.NOV4_MFG_NUMBER, new String[] { m_mfgHashText.getText().trim()});
        }
        
        return true;
    }
    
    
    private Label m_mfgLabel;
    private Combo m_mfgCombo;
    private Label m_mfgHashLabel;
    private Text m_mfgHashText;
    private Button m_requestMfgBtn;
    protected Button m_mfgCheckBox;
    protected Button m_mfgNumberCheckBox;
}
