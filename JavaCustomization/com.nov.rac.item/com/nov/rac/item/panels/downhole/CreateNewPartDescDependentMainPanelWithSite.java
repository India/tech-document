package com.nov.rac.item.panels.downhole;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class CreateNewPartDescDependentMainPanelWithSite extends
		CreateNewPartMainPanelWithSite {

	public CreateNewPartDescDependentMainPanelWithSite(Composite parent,
			int style) {
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException {
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);

		super.createClassRow(l_composite);

		super.createItemIDRow(l_composite);

		super.createCompositeRow(l_composite);

		super.createNameRow(l_composite);

		super.createName2Row(l_composite);

		super.createDescriptionRow(l_composite);

		createUIPost();

		super.registerSubscriber();

		return true;
	}

	@Override
	public boolean createUIPost() throws TCException 
	{
		super.createUIPost();

		m_textName.setEnabled(false);
		m_textName2.setEnabled(false);

		return true;
	}

	@Override
	protected void addModifyListenerOnTextDescription() {
		m_textDescription.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent modifyevent) {
				String sDescVal = m_textDescription.getText();
				int iDescLength = sDescVal.length();

				if (sDescVal.startsWith(" ") || sDescVal.startsWith("\r\n")) {
					m_textDescription.setText(sDescVal.trim());
				}

				if (iDescLength > DESCRIPTION_TEXT_LIMIT) {

					MessageBox.post(
							m_registry.getString("DescriptionError.MESSAGE"),
							"Error!", MessageBox.WARNING);

					sDescVal = sDescVal.substring(0,
							Math.min(iDescLength, DESCRIPTION_TEXT_LIMIT));

					m_textDescription.setText(sDescVal);
				}

				populateNameAndName2(sDescVal.trim());
			}
		});
	}

	private void populateNameAndName2(String sDescValue) {
		String sNameFieldVal = "";
		String sName2FieldVal = "";
		int iDescLength = sDescValue.length();

		String[] sDescWithNewline = sDescValue.split("\n");
		if (sDescWithNewline != null && sDescWithNewline.length > 1) {
			sNameFieldVal = sDescWithNewline[0];
			sName2FieldVal = sDescWithNewline[1];

			int iNameLength = sNameFieldVal.length();
			if (iNameLength > NAME_TEXT_LIMIT) {
				sName2FieldVal = sNameFieldVal.substring(NAME_TEXT_LIMIT,
						iNameLength);
				sNameFieldVal = sNameFieldVal.substring(0, NAME_TEXT_LIMIT);
			}
			if (sName2FieldVal.length() > NAME_TEXT_LIMIT) {
				sName2FieldVal = sName2FieldVal.substring(0, NAME_TEXT_LIMIT);
			}
		} else {
			sNameFieldVal = sDescValue.substring(0,
					Math.min(iDescLength, NAME_TEXT_LIMIT));
			if (iDescLength > NAME_TEXT_LIMIT) {
				sName2FieldVal = sDescValue.substring(NAME_TEXT_LIMIT,
						Math.min(iDescLength, NAME2_TEXT_LIMIT));
			}
		}

		if ((m_textName.getText()) != null) {
			sNameFieldVal = sNameFieldVal.replaceAll("[\\r]", " ");
			m_textName.setText(sNameFieldVal);
		}

		if ((m_textName2.getText()) != null) {
			sName2FieldVal = sName2FieldVal.replaceAll("[\\r]", " ");
			m_textName2.setText(sName2FieldVal.trim());
		}
	}

	private static final Integer NAME_TEXT_LIMIT = 30;

	private static final Integer NAME2_TEXT_LIMIT = 60;
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;

}
