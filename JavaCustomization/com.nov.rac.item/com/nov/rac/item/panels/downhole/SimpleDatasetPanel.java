package com.nov.rac.item.panels.downhole;

import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.panels.NewDatasetPanel;
import com.teamcenter.rac.kernel.TCException;

public class SimpleDatasetPanel extends NewDatasetPanel
{
    
    public SimpleDatasetPanel(Composite parent, int style) throws TCException
    {
        super(parent, style);
    }
    
    @Override
    protected PublishEvent getPublishEventForDatasetType()
    {
        return getPublishEvent(NationalOilwell.DH_DATASET_TYPE, "Prev Val", m_textDatasetType.getText());
    }
 
    
    
    @Override
    public boolean createUI() throws TCException
    {
        // TODO Auto-generated method stub
        super.createUI();
        setEnable(false);
        return true;
    }
}
