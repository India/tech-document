package com.nov.rac.item.panels.downhole;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class CreateNewDocumentDescDependentMainPanel extends
		SimpleCreateNewDocumentMainPanel {

	public CreateNewDocumentDescDependentMainPanel(Composite parent, int style) {
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);

		super.createItemIDRow(l_composite);

		super.createRevisionRow(l_composite);

		super.createNameRow(l_composite);

		super.createDescriptionRow(l_composite);

		createUIPost();
		
		super.registerSubscriber();
		
		return true;
	}
	
	@Override
	public boolean createUIPost() throws TCException
	{
		super.createUIPost();
		
		m_textName.setEditable(false);
		
		return true;
	}
	
	@Override
	protected void addModifyListenerOnTextDescription()
	{
		m_textDescription.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent modifyevent) {
				String sDescVal = m_textDescription.getText();
				int iDescLength = sDescVal.length();
				
				if (sDescVal.startsWith(" ") || sDescVal.startsWith("\r\n")) {
					m_textDescription.setText(sDescVal.trim());
				}
				
				if (iDescLength > DESCRIPTION_TEXT_LIMIT) {

					MessageBox.post(m_registry.getString("DescriptionError.MESSAGE"),
							"Error!", MessageBox.WARNING);
					
					sDescVal = sDescVal.substring(0, Math.min(iDescLength, DESCRIPTION_TEXT_LIMIT));

		            m_textDescription.setText(sDescVal);
				}
				
				populateName(sDescVal.trim());
			}
		});
	}

	private void populateName(String sDescValue) {
		String sNameFieldVal = "";
		int iDescLength = sDescValue.length();

		String[] sDescWithNewline = sDescValue.split("\n");
		if (sDescWithNewline != null && sDescWithNewline.length > 1) {
			sNameFieldVal = sDescWithNewline[0];

			int iNameLength = sNameFieldVal.length();
			if (iNameLength > NAME_TEXT_LIMIT) {
				sNameFieldVal = sNameFieldVal.substring(0, NAME_TEXT_LIMIT);
			}
		} else {
			sNameFieldVal = sDescValue.substring(0, Math.min(iDescLength, NAME_TEXT_LIMIT));
		}

		if ((m_textName.getText()) != null) {
			sNameFieldVal = sNameFieldVal.replaceAll("[\\r]", " ");
			m_textName.setText(sNameFieldVal);
		}

	}
	
	private static final Integer NAME_TEXT_LIMIT = 30;
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;

}
