package com.nov.rac.item.panels.downhole;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.nov.rac.utilities.utils.UOMHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

//This panel will have all Create New Part Info along with "Revision" and "UOM".
public class CreateNewPartMainPanelWithoutSite extends
		SimpleCreateNewPartMainPanel {

	public CreateNewPartMainPanelWithoutSite(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public boolean createUI() throws TCException {
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);

		super.createClassRow(l_composite);

		super.createItemIDRow(l_composite);

		createRevisionRow(l_composite);

		super.createNameRow(l_composite);

		super.createName2Row(l_composite);

		super.createDescriptionRow(l_composite);

		createUIPost();

		super.registerSubscriber();

		return true;
	}

	protected void createRevisionRow(Composite mainComposite) {
		Label lblRevision = new Label(mainComposite, SWT.NONE);
		lblRevision.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblRevision.setText(m_registry.getString("lblRevision.NAME",
				"Key Not Found"));

		m_textRevision = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtRevision = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		m_textRevision.setLayoutData(gd_txtRevision);

		Label lblUOM = new Label(mainComposite, SWT.NONE);
		lblUOM.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
		lblUOM.setText(m_registry.getString("lblUOM.NAME", "Key Not Found"));

		m_comboUOM = new Combo(mainComposite, SWT.NONE);
		GridData gd_comboUOM = new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1);
		m_comboUOM.setLayoutData(gd_comboUOM);

		UIHelper.setAutoComboCollection(m_comboUOM,
				POPULATE_DATA.getUOMLOVValues());

		new Label(mainComposite, SWT.NONE);

	}
	
	/** The Constant POPULATE_DATA. */
	private final static PopulateData POPULATE_DATA;

	static {
		POPULATE_DATA = new PopulateData();
	}

	/**
	 * The Class PopulateData.
	 */
	private static class PopulateData {

		/** The session. */
		private static TCSession session;

		/**
		 * Gets the session.
		 * 
		 * @return the session
		 */
		protected TCSession getSession() {
			return (TCSession) AIFUtility.getCurrentApplication().getSession();

		}

		/**
		 * Instantiates a new populate data.
		 */
		public PopulateData() {
			session = getSession();
		}

		/**
		 * Gets the uOMLOV values.
		 * 
		 * @return the uOMLOV values
		 */
		private Vector<Object> getUOMLOVValues() {
			Vector<Object> uomvector = new Vector<Object>();
			uomvector.add("");
			TCComponentListOfValues lovvalues = TCComponentListOfValuesType
					.findLOVByName(session, NationalOilwell.UOMLOV);
			if (lovvalues != null) {
				ListOfValuesInfo lovInfo = null;
				try {
					lovInfo = lovvalues.getListOfValues();
					Object[] lovS = lovInfo.getListOfValues();
					String[] lovDesc = lovInfo.getDescriptions();
					if (lovS.length > 0) {
						for (int i = 0; i < lovS.length; i++) {
							String[] splitDesc = null;
							if (lovDesc != null && lovDesc[i] != null) {
								splitDesc = lovDesc[i].split(",");
							}
							getGroupUOM(splitDesc, uomvector, lovS[i]);
						}
					}
				} catch (TCException e) {
					e.printStackTrace();

				}

			}
			return uomvector;
		}
	}

	public static void getGroupUOM(String[] splitDesc,
			Vector<Object> uomvector, Object lovS) {
		for (int inx = 0; inx < splitDesc.length; inx++) {
			if (splitDesc != null
					&& splitDesc[inx].trim().equalsIgnoreCase(
							NationalOilwell.UOM_FILTER_DHT)
					|| splitDesc[inx].trim().equalsIgnoreCase("")) {
				uomvector.add(lovS);
				break;
			}
		}
	}

	@Override
	public boolean createUIPost() throws TCException {
		super.createUIPost();

		// To set Revision value
		m_textRevision.setText(NationalOilwell.DEFAULT_REV_ID);
		m_textRevision.setEditable(false);

		// To set UOM value
		UIHelper.makeMandatory(m_comboUOM, false);
		int index = m_comboUOM.indexOf(UOM_EACH);
		m_comboUOM.select(index);

		return true;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		super.load(propMap);

		// item properties
		TCComponent uomTag = propMap.getTag(NationalOilwell.NOV_UOM);

		String uomUnit = "";
		if (uomTag != null) {
			uomUnit = uomTag.getProperty("unit");
		}

		m_comboUOM.setText(uomUnit);

		// item revision properties
		IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
		if (revisionPropertyMap != null) {
			m_textRevision.setText(PropertyMapHelper
					.handleNull(revisionPropertyMap
							.getString(NationalOilwell.ITEMREVISIONID)));
		}

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		super.save(propMap);

		// item properties
		propMap.setTag(NationalOilwell.NOV_UOM,
				UOMHelper.getUOMObjectForUnit(m_comboUOM.getText()));

		// item revision properties
		IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
		if (revisionPropertyMap != null) {
			revisionPropertyMap.setString(NationalOilwell.ITEMREVISIONID,
					m_textRevision.getText());
			propMap.setCompound("revision", revisionPropertyMap);
		}

		return true;
	}

	private Text m_textRevision;

	private Combo m_comboUOM;

	private static final String UOM_EACH = "Each";
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;

}
