package com.nov.rac.item.panels.downhole.group1B;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.downhole.SimpleCreateNewPartDocumentPanel;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class CreateRDDDocumentPanel extends SimpleCreateNewPartDocumentPanel {

	public CreateRDDDocumentPanel(Composite parent, int style)
	{
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		super.createUI();
		
		createUIPost();
		
		return true;
	}
	
	public boolean createUIPost() throws TCException
	{
		boolean returnValue = true;

		UIHelper.makeMandatory(m_textDocCategory);
		UIHelper.makeMandatory(m_docTypeLovPopupButton.getPopupButton());

		return returnValue;
	}

}
