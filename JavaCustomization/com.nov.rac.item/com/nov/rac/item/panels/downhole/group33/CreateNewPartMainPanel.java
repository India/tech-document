package com.nov.rac.item.panels.downhole.group33;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.downhole.CreateNewPartMainPanelWithoutSite;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class CreateNewPartMainPanel extends CreateNewPartMainPanelWithoutSite {

	public CreateNewPartMainPanel(Composite parent, int style)
	{
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);
		
		super.createClassRow(l_composite);

		super.createItemIDRow(l_composite);

		super.createRevisionRow(l_composite);

		super.createNameRow(l_composite);

		super.createName2Row(l_composite);

		super.createDescriptionRow(l_composite);

		createUIPost();
		
		super.registerSubscriber();
		
		return true;
	}
	
	@Override
	public boolean createUIPost() throws TCException
	{
		super.createUIPost();
		
		UIHelper.makeMandatory(m_textName);
		
		return true;
	}
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;

}
