package com.nov.rac.item.panels.downhole;

import static com.nov.rac.item.helpers.DocumentHelper.getDocCategory;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.item.panels.SimpleDocumentCreatePanel;
import com.teamcenter.rac.kernel.TCComponentListOfValues;

public class SimpleCreateNewPartDocumentPanel extends SimpleDocumentCreatePanel {

	public SimpleCreateNewPartDocumentPanel(Composite parent, int style) {
		super(parent, style);
	}
	
	@Override
	protected TCComponentListOfValues getDocCategoryLOV() {
		return getDocCategory(NationalOilwell.DOC_TYPE_LOV_FOR_DHL);
	}
	
//	@Override
//	protected GridLayout getUILayout() {
//		return new GridLayout(COMPOSITE_GRID_LAYOUT, true);
//	}
//	
//	@Override
//	protected GridData getGridDataForDocCategory() {
//		return new GridData(SWT.FILL, SWT.TOP, true, false, GRID_DATA_HORIZONTAL_SPAN, 1);
//	}
//	
//	@Override
//	protected GridData getGridDataForDocTypeButton() {
//		return new GridData(SWT.FILL, SWT.CENTER, true, false, GRID_DATA_HORIZONTAL_SPAN, 1);
//	}
//	
//	private static final Integer COMPOSITE_GRID_LAYOUT = 5;
//	private static final Integer GRID_DATA_HORIZONTAL_SPAN = 3;


}