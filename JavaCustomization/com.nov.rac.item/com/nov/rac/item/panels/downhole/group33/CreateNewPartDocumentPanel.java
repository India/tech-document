package com.nov.rac.item.panels.downhole.group33;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.downhole.SimpleCreateNewPartDocumentPanel;

public class CreateNewPartDocumentPanel extends SimpleCreateNewPartDocumentPanel
{
    
    public CreateNewPartDocumentPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
}
