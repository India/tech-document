package com.nov.rac.item.panels.downhole.group3C;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.item.panels.downhole.SimpleCreateNewPartDocumentPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class CreateNewPartDocumentPanel extends SimpleCreateNewPartDocumentPanel {

	public CreateNewPartDocumentPanel(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public boolean createUI() throws TCException {

		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(5, false));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);

		super.createUI();

		createPullDocRow(subComposite);

		return true;
	}

	private void createPullDocRow(Composite parent) {

		Label m_lblPullDoc = new Label(parent, SWT.NONE);
		m_lblPullDoc.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		m_lblPullDoc.setText(m_registry.getString("pullLabel.NAME",
				"Key Not Found"));
		m_comboPullDoc = new Combo(parent, SWT.NONE);

		GridData gd_PullDoc = new GridData(SWT.FILL, SWT.CENTER, true, false, 2,
				1);
		gd_PullDoc.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(
				m_comboPullDoc, NationalOilwell.COMBO_FIELD_SIZE);

		m_comboPullDoc.setLayoutData(gd_PullDoc);

		// String[] values = m_registry.getStringArray("pullComboBox.VALUE");
		String[] values = new String[] { "yes", "no" };
		UIHelper.setAutoComboArray(m_comboPullDoc, values);
		setSelectedValueInCombo(m_comboPullDoc,
				m_registry.getString("pullComboBox.DEFAULTVALUE"));
		
		//new Label(parent, SWT.NONE);
	}

	private void setSelectedValueInCombo(Combo combo, String value) {
		int l_index = combo.indexOf(value);
		if (l_index != -1) {
			combo.select(l_index);
		}
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException
	{
		super.load(propMap);
		
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {
			if (masterPropertyMap.getLogical(NationalOilwell.PULL_DRAWING)) {
				int l_index = m_comboPullDoc.indexOf("yes");
				if (l_index != -1) {
					m_comboPullDoc.select(l_index);
				}
			} else {
				int l_index = m_comboPullDoc.indexOf("no");
				m_comboPullDoc.select(l_index);
			}

		}

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException
	{
		super.save(propMap);
		
		// master form properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {
			masterPropertyMap.setLogical(NationalOilwell.PULL_DRAWING,
					UIHelper.getBoolean(m_comboPullDoc.getText()));
			propMap.setCompound("IMAN_master_form", masterPropertyMap);
		}
		
		return true;
	}

	/** The m_ combo pull doc. */
	private Combo m_comboPullDoc;
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;
	    
    private static final Integer GRID_DATA_HORIZONTAL_SPAN = 3;

}
