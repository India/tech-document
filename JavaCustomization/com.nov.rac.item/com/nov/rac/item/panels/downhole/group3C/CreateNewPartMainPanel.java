package com.nov.rac.item.panels.downhole.group3C;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.downhole.CreateNewPartDescDependentMainPanelWithSite;
import com.teamcenter.rac.kernel.TCException;

public class CreateNewPartMainPanel extends CreateNewPartDescDependentMainPanelWithSite {

	public CreateNewPartMainPanel(Composite parent, int style)
	{
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{	
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);
		
		super.createClassRow(l_composite);

		super.createItemIDRow(l_composite);

		super.createCompositeRow(l_composite);

		super.createNameRow(l_composite);

		super.createName2Row(l_composite);

		super.createDescriptionRow(l_composite);

		super.createUIPost();
		
		super.registerSubscriber();
		
		return true;
	}
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;

}
