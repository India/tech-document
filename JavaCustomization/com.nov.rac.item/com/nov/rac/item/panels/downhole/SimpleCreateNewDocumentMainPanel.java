package com.nov.rac.item.panels.downhole;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.common.NOVDataManagementServiceInfo;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SimpleCreateNewDocumentMainPanel extends AbstractUIPanel implements
		ILoadSave, IPublisher, ISubscriber {

	public SimpleCreateNewDocumentMainPanel(Composite parent, int style) {
		super(parent, style);
		m_registry = getRegistry();
		m_session = getSession();
	}

	/**
	 * Gets the registry.
	 * 
	 * @return the registry
	 */
	protected Registry getRegistry() {
		return Registry.getRegistry("com.nov.rac.item.panels.panels");
	}

	protected TCSession getSession() {
		return (TCSession) AIFUtility.getCurrentApplication().getSession();

	}

	@Override
	public boolean createUI() throws TCException {
		final Composite mainComposite = getComposite();
		mainComposite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		mainComposite.setLayoutData(gd_panel);

		createItemIDRow(mainComposite);

		createRevisionRow(mainComposite);

		createNameRow(mainComposite);

		createDescriptionRow(mainComposite);

		createUIPost();
		
		registerSubscriber();

		return true;
	}

	protected void createItemIDRow(Composite mainComposite) {
		Label lblItemID = new Label(mainComposite, SWT.NONE);
		lblItemID.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblItemID.setText(m_registry.getString("lblItemID.NAME",
				"Key Not Found"));

		m_textItemID = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtItemID = new GridData(SWT.FILL, SWT.CENTER, true, false,
				GRID_DATA_HORIZONTAL_SPAN, 1);
		m_textItemID.setLayoutData(gd_txtItemID);
		
		m_textItemID.setTextLimit(ITEM_ID_TEXT_LIMIT);

		m_btnAssignItemID = new Button(mainComposite, SWT.NONE);
		GridData gd_btnAssignItemID = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		m_btnAssignItemID.setText(m_registry.getString("btnAssignItemID.NAME",
				"Key Not Found"));
		gd_btnAssignItemID.horizontalIndent = SWTUIHelper
				.convertHorizontalDLUsToPixels(m_btnAssignItemID,
						HORIZONTAL_INDENT_ITEMID);
		m_btnAssignItemID.setLayoutData(gd_btnAssignItemID);

		addSelectionListenerOnBtnAssignItemID();

	}

	private void addSelectionListenerOnBtnAssignItemID() {
		m_btnAssignItemID.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent se) {
				onAssignItemIDButtonClick();
			}
		});
	}

	private void onAssignItemIDButtonClick()
	{
		/*String itemId = m_textItemID.getText();
		if (itemId == null || itemId.compareTo("") == 0) 
		{*/
			NOVDataManagementServiceInfo novGetNextValueService = new NOVDataManagementServiceInfo();

			String strPrefixExp = "";
			String strCounterName = strPrefixExp + "_itemid";
			String strPostfixExp = "";
			String strGroupCode = "";
			String newId;
			if (strPrefixExp.compareTo("") == 1) {
				strGroupCode = strPrefixExp.substring(1);
			}

			setDatatoService(novGetNextValueService, strCounterName,
					strPrefixExp, strPostfixExp, strGroupCode);

			newId = novGetNextValueService.getNextValue();

			m_textItemID.setText(newId);
		/*} else {
			MessageBox.post(m_registry.getString("baseIdAssigned.MSG"),
					"Invalid input(s)", MessageBox.ERROR);
		}*/
	}

	/**
	 * Set Input data to the service
	 */

	void setDatatoService(NOVDataManagementServiceInfo novGetNextValueService,
			String strCounterName, String strPrefixExp, String strPostfixExp,
			String strGroupCode) {
		int theScope = TCPreferenceService.TC_preference_site;

		novGetNextValueService.setContext("ITEM_ID");
		novGetNextValueService.setCounterName(strCounterName);
		novGetNextValueService
				.setDefaultCounterStartValue(NationalOilwell.DEFAULT_COUNTER_START_VALUE);
		novGetNextValueService.setPrefix(strPrefixExp);
		novGetNextValueService.setPostfix(strPostfixExp);

		PreferenceUtils PrefObj = new PreferenceUtils();

		boolean bJDEGroupDesicion = PrefObj.isPrefValueExist(strGroupCode,
				"NOV_JDE_check_creation_groups", theScope);
		if (bJDEGroupDesicion) {
			novGetNextValueService
					.setAlternateIDContext(new String[] { "JDE" });
		} else {
			novGetNextValueService.setAlternateIDContext(new String[] { "" });
		}
	}

	protected void createRevisionRow(Composite mainComposite) {
		Label lblRevision = new Label(mainComposite, SWT.NONE);
		lblRevision.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblRevision.setText(m_registry.getString("lblRevision.NAME",
				"Key Not Found"));

		m_textRevision = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtRevision = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		m_textRevision.setLayoutData(gd_txtRevision);

		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);
	}
	
	protected void createNameRow(Composite mainComposite)
	{
		Label lblName = new Label(mainComposite, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1));
		lblName.setText(m_registry.getString("lblName.NAME", "Key Not Found"));

		m_textName = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtName = new GridData(SWT.FILL, SWT.CENTER, true, false,
				GRID_DATA_HORIZONTAL_SPAN, 1);
		m_textName.setLayoutData(gd_txtName);
		
		m_textName.setTextLimit(NAME_TEXT_LIMIT);

		new Label(mainComposite, SWT.NONE);
	}
	
	protected void createDescriptionRow(Composite mainComposite) {
		Label lblDescription = new Label(mainComposite, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
				true, 1, GRID_DATA_VERTICAL_SPAN));
		lblDescription.setText(m_registry.getString("lblDescription.NAME",
				"Key Not Found"));

		m_textDescription = new Text(mainComposite, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL | SWT.WRAP);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, true,
				true, GRID_DATA_HORIZONTAL_SPAN, GRID_DATA_VERTICAL_SPAN);
		m_textDescription.setLayoutData(gd_txtDescription);
		
		addModifyListenerOnTextDescription();

		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);

	}
	
	protected void addModifyListenerOnTextDescription()
	{
		m_textDescription.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent modifyevent) {
				String sDescVal = m_textDescription.getText();
				int iDescLength = sDescVal.length();
				
				if (sDescVal.startsWith(" ") || sDescVal.startsWith("\r\n")) {
					m_textDescription.setText(sDescVal.trim());
				}
				
				if (iDescLength > DESCRIPTION_TEXT_LIMIT) {

					MessageBox.post(m_registry.getString("DescriptionError.MESSAGE"),
							"Error!", MessageBox.WARNING);
					
					sDescVal = sDescVal.substring(0, Math.min(iDescLength, DESCRIPTION_TEXT_LIMIT));

		            m_textDescription.setText(sDescVal);
				}
			}
		});
	}
	
	public boolean createUIPost() throws TCException
	{
		boolean returnValue = true;
		
		// To set Item ID
		UIHelper.makeMandatory(m_textItemID);
		UIHelper.convertToUpperCase(m_textItemID);
		//m_textItemID.setEditable(false);
		
		// To set Revision value
		m_textRevision.setText(NationalOilwell.DEFAULT_REV_ID);
		m_textRevision.setEditable(false);
		
		// To set Name
	    UIHelper.convertToUpperCase(m_textName);
		
		// To set Description
		UIHelper.makeMandatory(m_textDescription);
		UIHelper.convertToUpperCase(m_textDescription);

		return returnValue;
	}
	
	@Override
	public boolean load(IPropertyMap propMap) throws TCException
	{
		// item properties
		
		/*m_textItemID.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.ITEM_ID)));*/
		
		m_textName.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_NAME)));

		m_textDescription.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_DESC)));

		// item revision properties
		IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
		if (revisionPropertyMap != null) {
			m_textRevision.setText(PropertyMapHelper
					.handleNull(revisionPropertyMap
							.getString(NationalOilwell.ITEMREVISIONID)));
		}

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException
	{
		// item properties
		propMap.setString(NationalOilwell.ITEM_ID, m_textItemID.getText());
		propMap.setString(NationalOilwell.OBJECT_NAME, m_textName.getText());
		propMap.setString(NationalOilwell.OBJECT_DESC,
				m_textDescription.getText());

		// item revision properties
		IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
		if (revisionPropertyMap != null) {
			revisionPropertyMap.setString(NationalOilwell.ITEMREVISIONID,
					m_textRevision.getText());
			propMap.setCompound("revision", revisionPropertyMap);
		}

		return true;
	}

	@Override
	public IPropertyMap getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reload() throws TCException {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void update(PublishEvent event)
	{
		if(event.getPropertyName().equals(NationalOilwell.SELECTED_ITEM_MODIFY_EVENT))
        {
			String selectedItem = (String) event.getNewValue();
			
			int index = selectedItem.lastIndexOf(":");
			String item = "";
			
			if (index > 0)
			{
				item = selectedItem.substring(0, index);
			}
			m_textItemID.setText(item);	
	
			//int len = selectedItem.length();
			//String item = selectedItem.substring(0, len - 3);
        }
		
        if(event.getPropertyName().equals(NationalOilwell.ONAPPLY_OBJECT_CREATED))
        {
            m_textItemID.setText("");
        }
	}

	protected void registerSubscriber()
	{
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();
		
		controller.registerSubscriber(NationalOilwell.SELECTED_ITEM_MODIFY_EVENT, this);
		controller.registerSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED, this);
	}

	protected void unRegisterSubscriber() {
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();
		
		controller.unregisterSubscriber(NationalOilwell.SELECTED_ITEM_MODIFY_EVENT, this);
		controller.unregisterSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED, this);
	}

	@Override
	public void dispose() {
		unRegisterSubscriber();
		super.dispose();
	}

	/** The m_registry. */
	protected Registry m_registry = null;

	private TCSession m_session;
	
	private Text m_textItemID;

	private Button m_btnAssignItemID;

	private static final Integer HORIZONTAL_INDENT_ITEMID = 5;

	private Text m_textRevision;
	
	protected Text m_textName;
	
	private static final Integer NAME_TEXT_LIMIT = 30;
	
	private static final Integer ITEM_ID_TEXT_LIMIT = 25;
	
	protected Text m_textDescription;
	
	protected static final Integer DESCRIPTION_TEXT_LIMIT = 240;
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;
	
	private static final Integer GRID_DATA_HORIZONTAL_SPAN = 3;
	
	private static final Integer GRID_DATA_VERTICAL_SPAN = 3;

}
