package com.nov.rac.item.panels.downhole.group3C;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.teamcenter.rac.kernel.TCException;

public class CreateRDDMainPanel extends CopyDocumentMainPanel {

	public CreateRDDMainPanel(Composite parent, int style) {
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		super.createUI();
		
		m_btnSite.setVisible(false);
		
		m_selectedItemText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent arg0) {
				 IController controller = ControllerFactory.getInstance().getDefaultController();
	                PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.SELECTED_ITEM_MODIFY_EVENT, m_selectedItemText.getText(), null);
	                controller.publish(event);
				
			}
		});
		return true;
		
	}
	
	private IPublisher getPublisher() 
	{
		return this;
	}

}
