package com.nov.rac.item.panels.downhole.group3C;

import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class CreateNewPartForRDDPanel extends CreateNewPartMainPanel {

	public CreateNewPartForRDDPanel(Composite parent, int style) {
		super(parent, style);
	}
	
	@Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		super.load(propMap);
		
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		
        if ((masterPropertyMap.getStringArray(NationalOilwell.SITES)).length != 0)
        {
        	//UIHelper.makeRequired(m_listSite);
        	moveField(m_listSite);
        }
        
        return true;
	}

}
