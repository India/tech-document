package com.nov.rac.item.panels.downhole.group3C;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.downhole.NOVSiteDialog;
import com.nov.rac.item.panels.downhole.CreateNewDocumentDescDependentMainPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class CreateNewDocumentMainPanel extends
		CreateNewDocumentDescDependentMainPanel {

	public CreateNewDocumentMainPanel(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public boolean createUI() throws TCException
	{
		
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);
		
		super.createItemIDRow(l_composite);

		super.createRevisionRow(l_composite);

		createSiteRow(l_composite);

		super.createNameRow(l_composite);

		super.createDescriptionRow(l_composite);

		createUIPost();

		registerSubscriber();

		return true;
	}

	private void createSiteRow(final Composite mainComposite)
			throws TCException {
		Label lblSite = new Label(mainComposite, SWT.NONE);
		lblSite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true,
				1, GRID_DATA_VERTICAL_SPAN));
		lblSite.setText(m_registry.getString("lblSite.NAME", "Key Not Found"));

		m_listSite = new List(mainComposite, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL | SWT.WRAP);
		GridData gd_txtItemID = new GridData(SWT.FILL, SWT.FILL, true, true, GRID_DATA_HORIZONTAL_SPAN,
				GRID_DATA_VERTICAL_SPAN);
		m_listSite.setLayoutData(gd_txtItemID);

		m_btnSite = new Button(mainComposite, SWT.NONE);
		GridData gd_btnSite = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		m_btnSite
				.setText(m_registry.getString("btnSite.NAME", "Key Not Found"));
		gd_btnSite.horizontalIndent = SWTUIHelper
				.convertHorizontalDLUsToPixels(m_btnSite,
						HORIZONTAL_INDENT_SITE);
		m_btnSite.setLayoutData(gd_btnSite);

		addSelectionListenerOnBtnSite(mainComposite);

		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);

	}

	private void addSelectionListenerOnBtnSite(final Composite mainComposite) {
		m_btnSite.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent selectionEvent) {
				Point shellLocation = m_btnSite.getShell().getLocation();
				Point buttonLocation = m_btnSite.getLocation();

				Point requiredDialogLocation = new Point(shellLocation.x
						+ buttonLocation.x + LOCATION_X, shellLocation.y
						+ buttonLocation.y + LOCATION_Y);

				NOVSiteDialog siteDialog = new NOVSiteDialog(m_btnSite
						.getShell(), SWT.NONE);

				siteDialog.setInitialLocation(requiredDialogLocation);
				siteDialog.create();
				siteDialog.open();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent selectionEvent) {
			}
		});
	}
	
	@Override
	public boolean createUIPost() throws TCException
	{
		super.createUIPost();
		
		UIHelper.makeMandatory(m_listSite);
		m_listSite.setEnabled(false);
		
		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		super.save(propMap);

		// item master properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {

			masterPropertyMap.setStringArray(NationalOilwell.SITES,
					m_listSite.getItems());

			propMap.setCompound("IMAN_master_form", masterPropertyMap);
		}

		return true;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		super.load(propMap);

		// item master properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {
			m_listSite.setItems(PropertyMapHelper.handleNull(masterPropertyMap
					.getStringArray(NationalOilwell.SITES)));
		}

		return true;
	}

	@Override
	public void update(PublishEvent event) {
		
		super.update(event);
		
		if (event.getPropertyName().compareTo(
				NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT) == 0) {
			String[] site = (String[]) event.getNewValue();

			m_listSite.setItems(site);

			//UIHelper.makeRequired(m_listSite);
			moveField(m_listSite);

		}
	}
	
	protected void moveField(Control field)
    {
        Rectangle rect = field.getBounds();
        field.setLocation(rect.x + 1, rect.y);
        field.setLocation(rect.x, rect.y);
    }

	protected void registerSubscriber() {
		
		super.registerSubscriber();
		
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();

		controller.registerSubscriber(
				NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT, this);
	}

	protected void unRegisterSubscriber() {
		
		super.unRegisterSubscriber();
		
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();

		controller.unregisterSubscriber(
				NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT, this);
	}

	@Override
	public void dispose() {
		unRegisterSubscriber();
		super.dispose();
	}

	protected List m_listSite;

	protected Button m_btnSite;

	private static final Integer HORIZONTAL_INDENT_SITE = 5;
	
    private static final Integer COMPOSITE_GRID_LAYOUT = 5;
    
    private static final Integer GRID_DATA_HORIZONTAL_SPAN = 3;
    
    private static final Integer GRID_DATA_VERTICAL_SPAN = 3;
    
    private static final Integer LOCATION_X = 15;
	
	private static final Integer LOCATION_Y = 87;

}
