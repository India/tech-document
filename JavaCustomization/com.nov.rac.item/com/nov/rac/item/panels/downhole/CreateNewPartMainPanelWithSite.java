package com.nov.rac.item.panels.downhole;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.downhole.NOVSiteDialog;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.nov.rac.utilities.utils.UOMHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

// This panel will have all Create New Part Info along with "Revision", "UOM" and "Site".
public class CreateNewPartMainPanelWithSite extends
		SimpleCreateNewPartMainPanel {

	public CreateNewPartMainPanelWithSite(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public boolean createUI() throws TCException {
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);

		super.createClassRow(l_composite);

		super.createItemIDRow(l_composite);

		createCompositeRow(l_composite);

		super.createNameRow(l_composite);

		super.createName2Row(l_composite);

		super.createDescriptionRow(l_composite);

		createUIPost();

		registerSubscriber();

		return true;
	}

	protected void createCompositeRow(Composite mainComposite) {
		createLeftComposite(mainComposite);
		createRightComposite(mainComposite);

	}

	private void createLeftComposite(Composite mainComposite) {
		Composite l_composite = new Composite(mainComposite, SWT.NONE);
		GridLayout gl = new GridLayout(2, true);
		gl.marginWidth = 0;
		l_composite.setLayout(gl);

		GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
				2, 1);
		l_composite.setLayoutData(gd_l_composite);

		createRev(l_composite);

		createUOM(l_composite);

	}

	private void createRev(Composite l_composite) {
		Label lblRevision = new Label(l_composite, SWT.NONE);
		lblRevision.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblRevision.setText(m_registry.getString("lblRevision.NAME",
				"Key Not Found"));

		m_textRevision = new Text(l_composite, SWT.BORDER);
		GridData gd_txtRevision = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		m_textRevision.setLayoutData(gd_txtRevision);
	}

	private void createUOM(Composite l_composite) {

		Label lblUOM = new Label(l_composite, SWT.NONE);
		lblUOM.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1));
		lblUOM.setText(m_registry.getString("lblUOM.NAME", "Key Not Found"));

		m_comboUOM = new Combo(l_composite, SWT.NONE);
		GridData gd_comboUOM = new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1);
		m_comboUOM.setLayoutData(gd_comboUOM);

		UIHelper.setAutoComboCollection(m_comboUOM,
				POPULATE_DATA.getUOMLOVValues());

	}

	protected void createRightComposite(Composite mainComposite) {

		Composite l_composite = new Composite(mainComposite, SWT.NONE);
		l_composite.setLayout(new GridLayout(RIGHT_COMPOSITE_COLUMNS, true));
		GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
				RIGHT_COMPOSITE_HORIZONTAL_SPAN, 1);
		l_composite.setLayoutData(gd_l_composite);
		gd_l_composite.horizontalIndent = HORIZONTAL_INDENT_SITE;

		Label lblSite = new Label(l_composite, SWT.NONE);
		lblSite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true,
				1, SITE_LABEL_VERTICAL_SPAN));
		lblSite.setText(m_registry.getString("lblSite.NAME", "Key Not Found"));

		m_listSite = new List(l_composite, SWT.V_SCROLL | SWT.BORDER
				| SWT.MULTI);

		GridData data = new GridData(GridData.FILL_BOTH);
		data.heightHint = LIST_NUMBER_OF_ROWS * (m_listSite.getItemHeight()); 															
		data.widthHint = SWTUIHelper.getStringWidth(LIST_WIDTH_HINT, m_listSite); 
		m_listSite.setLayoutData(data);

		m_btnSite = new Button(l_composite, SWT.NONE);
		GridData gd_btnSite = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		m_btnSite
				.setText(m_registry.getString("btnSite.NAME", "Key Not Found"));
		gd_btnSite.horizontalIndent = SWTUIHelper
				.convertHorizontalDLUsToPixels(m_btnSite,
						HORIZONTAL_INDENT_SITE);
		m_btnSite.setLayoutData(gd_btnSite);

		addSelectionListenerOnBtnSite(mainComposite);

	}

	private void addSelectionListenerOnBtnSite(final Composite mainComposite) {
		m_btnSite.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent selectionEvent) {
				Point shellLocation = m_btnSite.getShell().getLocation();
				Point buttonLocation = m_btnSite.getLocation();

				Point requiredDialogLocation = new Point(shellLocation.x
						+ buttonLocation.x + LOCATION_X, shellLocation.y
						+ buttonLocation.y + LOCATION_Y);

				NOVSiteDialog siteDialog = new NOVSiteDialog(m_btnSite
						.getShell(), SWT.NONE);

				siteDialog.setInitialLocation(requiredDialogLocation);
				siteDialog.create();
				siteDialog.open();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent selectionEvent) {
			}
		});
	}

	/** The Constant POPULATE_DATA. */
	private final static PopulateData POPULATE_DATA;

	static {
		POPULATE_DATA = new PopulateData();
	}

	/**
	 * The Class PopulateData.
	 */
	private static class PopulateData {

		/** The session. */
		private static TCSession session;

		/**
		 * Gets the session.
		 * 
		 * @return the session
		 */
		protected TCSession getSession() {
			return (TCSession) AIFUtility.getCurrentApplication().getSession();

		}

		/**
		 * Instantiates a new populate data.
		 */
		public PopulateData() {
			session = getSession();
		}

		/**
		 * Gets the uOMLOV values.
		 * 
		 * @return the uOMLOV values
		 */
		private Vector<Object> getUOMLOVValues() {
			Vector<Object> uomvector = new Vector<Object>();
			uomvector.add("");
			TCComponentListOfValues lovvalues = TCComponentListOfValuesType
					.findLOVByName(session, NationalOilwell.UOMLOV);
			if (lovvalues != null) {
				ListOfValuesInfo lovInfo = null;
				try {
					lovInfo = lovvalues.getListOfValues();
					Object[] lovS = lovInfo.getListOfValues();
					String[] lovDesc = lovInfo.getDescriptions();
					if (lovS.length > 0) {
						for (int i = 0; i < lovS.length; i++) {
							String[] splitDesc = null;
							if (lovDesc != null && lovDesc[i] != null) {
								splitDesc = lovDesc[i].split(",");
							}
							getGroupUOM(splitDesc, uomvector, lovS[i]);
						}
					}
				} catch (TCException e) {
					e.printStackTrace();

				}

			}
			return uomvector;
		}
	}

	public static void getGroupUOM(String[] splitDesc,
			Vector<Object> uomvector, Object lovS) {
		for (int inx = 0; inx < splitDesc.length; inx++) {
			if (splitDesc != null
					&& splitDesc[inx].trim().equalsIgnoreCase(
							NationalOilwell.UOM_FILTER_DHT)
					|| splitDesc[inx].trim().equalsIgnoreCase("")) {
				uomvector.add(lovS);
				break;
			}
		}
	}

	@Override
	public boolean createUIPost() throws TCException {
		super.createUIPost();

		// To set Revision value
		m_textRevision.setText(NationalOilwell.DEFAULT_REV_ID);
		m_textRevision.setEditable(false);

		// To set UOM value
		UIHelper.makeMandatory(m_comboUOM, false);
		int index = m_comboUOM.indexOf(UOM_EACH);
		m_comboUOM.select(index);

		// To set Site value
		UIHelper.makeMandatory(m_listSite);

		return true;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		super.load(propMap);

		// item properties
		TCComponent uomTag = propMap.getTag(NationalOilwell.NOV_UOM);

		String uomUnit = "";
		if (uomTag != null) {
			uomUnit = uomTag.getProperty("unit");
		}

		m_comboUOM.setText(uomUnit);

		// item revision properties
		IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
		if (revisionPropertyMap != null) {
			m_textRevision.setText(PropertyMapHelper
					.handleNull(revisionPropertyMap
							.getString(NationalOilwell.ITEMREVISIONID)));
		}

		// item master properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {
			m_listSite.setItems(PropertyMapHelper.handleNull(masterPropertyMap
					.getStringArray(NationalOilwell.SITES)));
		}

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		super.save(propMap);

		// item properties
		propMap.setTag(NationalOilwell.NOV_UOM,
				UOMHelper.getUOMObjectForUnit(m_comboUOM.getText()));

		// item revision properties
		IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
		if (revisionPropertyMap != null) {
			revisionPropertyMap.setString(NationalOilwell.ITEMREVISIONID,
					m_textRevision.getText());
			propMap.setCompound("revision", revisionPropertyMap);
		}

		// item master properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {

			masterPropertyMap.setStringArray(NationalOilwell.SITES,
					m_listSite.getItems());

			propMap.setCompound("IMAN_master_form", masterPropertyMap);
		}

		return true;
	}
	
	@Override
	public void update(PublishEvent event)
	{
		super.update(event);
		
		if (event.getPropertyName().compareTo(
				NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT) == 0)
		{
			String[] site = (String[]) event.getNewValue();

			m_listSite.setItems(site);

			//UIHelper.makeRequired(m_listSite);
			moveField(m_listSite);
			
		}
	}
	
	protected void moveField(Control field)
    {
        Rectangle rect = field.getBounds();
        field.setLocation(rect.x + 1, rect.y);
        field.setLocation(rect.x, rect.y);
    }

	@Override
	protected void registerSubscriber()
	{
		super.registerSubscriber();
		
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();
		
		controller.registerSubscriber(
				NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT, this);
	}
	
	@Override
	protected void unRegisterSubscriber() 
	{
		super.unRegisterSubscriber();
		
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();

		controller.unregisterSubscriber(
				NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT, this);
	}
	
	@Override
	public void dispose()
	{
		unRegisterSubscriber();
		super.dispose();
	}

	private Text m_textRevision;

	private Combo m_comboUOM;

	private static final String UOM_EACH = "Each";

	private Button m_btnSite;

	private static final Integer HORIZONTAL_INDENT_SITE = 5;
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;
	
	private static final Integer LIST_NUMBER_OF_ROWS = 3;
	
	private static final Integer LIST_WIDTH_HINT = 10;
	
	private static final Integer RIGHT_COMPOSITE_COLUMNS = 3;
	
	private static final Integer RIGHT_COMPOSITE_HORIZONTAL_SPAN = 3;
	
	private static final Integer SITE_LABEL_VERTICAL_SPAN = 3;
	
	private static final Integer LOCATION_X = 255;
	
	private static final Integer LOCATION_Y = 150;

}
