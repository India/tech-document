package com.nov.rac.item.panels.downhole;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.downhole.ClassificationDialog;
import com.nov.rac.item.helpers.ClassHierarchyCreator;
import com.nov.rac.item.helpers.ClassificationAutoComplete;
import com.nov.rac.item.helpers.ClassificationHelper;
import com.nov.rac.item.helpers.downhole.DownholeItemCreateHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.common.NOVDataManagementServiceInfo;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

// This panel will have Create New Part Info except "Revision", "UOM" and "Site".
public class SimpleCreateNewPartMainPanel extends AbstractUIPanel implements
		ILoadSave, IPublisher, ISubscriber {

	public SimpleCreateNewPartMainPanel(Composite parent, int style) {
		super(parent, style);
		m_registry = getRegistry();
		m_session = getSession();
	}

	/**
	 * Gets the registry.
	 * 
	 * @return the registry
	 */
	private Registry getRegistry() {
		return Registry.getRegistry("com.nov.rac.item.panels.panels");
	}

	private TCSession getSession() {
		return (TCSession) AIFUtility.getCurrentApplication().getSession();

	}

	@Override
	public boolean createUI() throws TCException {
		final Composite mainComposite = getComposite();
		mainComposite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		mainComposite.setLayoutData(gd_panel);

		createClassRow(mainComposite);

		createItemIDRow(mainComposite);

		createNameRow(mainComposite);

		createName2Row(mainComposite);

		createDescriptionRow(mainComposite);

		createUIPost();

		registerSubscriber();

		return true;
	}

	protected void createClassRow(Composite mainComposite) {
		Label lblClass = new Label(mainComposite, SWT.NONE);
		lblClass.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblClass.setText(m_registry.getString("lblClass.NAME", "Key Not Found"));

		m_textClass = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtClass = new GridData(SWT.FILL, SWT.CENTER, true, false,
				GRID_DATA_HORIZONTAL_SPAN, 1);
		m_textClass.setLayoutData(gd_txtClass);

		Composite m_composite = new Composite(mainComposite, SWT.NONE);
		GridData m_classCompgd = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1);
		m_composite.setLayout(new GridLayout(1, true));
		m_composite.setLayoutData(m_classCompgd);
		m_classCompgd.horizontalIndent = HORIZONTAL_INDENT_CLASS;
		m_btnClass = new Button(m_composite, SWT.TOGGLE);
		GridData m_classgd = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		m_btnClass.setLayoutData(m_classgd);
		Image classImage = new Image(mainComposite.getDisplay(),
				m_registry.getImage("refresh.Icon"), SWT.NONE);
		m_btnClass.setImage(classImage);

		addSelectionListenerOnBtnClass();

		addModifyListenerOnTextClass();

	}

	private void addSelectionListenerOnBtnClass() {
		m_btnClass.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent selectionEvent) {

				ClassificationDialog classificationDialog = new ClassificationDialog(
						m_btnClass.getShell(), m_textClass);
				Point btnLocation = m_btnClass.toDisplay(m_btnClass
						.getLocation());
				Point btnSize = m_btnClass.getSize();
				classificationDialog.setLocation(btnLocation, btnSize);
				classificationDialog.open();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent selectionEvent) {

			}
		});
	}

	private void addModifyListenerOnTextClass() {
		m_textClass.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent modifyEvent) {
				String sClass = m_textClass.getText();

				if (sClass.compareTo("") == 0) {
					m_btnAssignItemID.setEnabled(false);
				} else {
					m_btnAssignItemID.setEnabled(true);
				}

				if (DownholeItemCreateHelper.isItemIdEditableItemTypes(sClass)) {
					m_textItemID.setEditable(true);
				} else {
					m_textItemID.setEditable(false);
				}
				
				if(sClass.contains("->"))
                {
					sClass = sClass.substring(sClass.lastIndexOf(">")+1);
                    m_textClass.setText(sClass);
                }
			}

		});
	}

	protected void createItemIDRow(Composite mainComposite) {
		Label lblItemID = new Label(mainComposite, SWT.NONE);
		lblItemID.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblItemID.setText(m_registry.getString("lblItemID.NAME",
				"Key Not Found"));

		m_textItemID = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtItemID = new GridData(SWT.FILL, SWT.CENTER, true, false,
				GRID_DATA_HORIZONTAL_SPAN, 1);
		m_textItemID.setLayoutData(gd_txtItemID);

		m_textItemID.setTextLimit(ITEM_ID_TEXT_LIMIT);

		m_btnAssignItemID = new Button(mainComposite, SWT.NONE);
		GridData gd_btnAssignItemID = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		m_btnAssignItemID.setText(m_registry.getString("btnAssignItemID.NAME",
				"Key Not Found"));
		gd_btnAssignItemID.horizontalIndent = SWTUIHelper
				.convertHorizontalDLUsToPixels(m_btnAssignItemID,
						HORIZONTAL_INDENT_ITEMID);
		m_btnAssignItemID.setLayoutData(gd_btnAssignItemID);

		addSelectionListenerOnBtnAssignItemID();

	}

	private void addSelectionListenerOnBtnAssignItemID() {
		m_btnAssignItemID.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent se) {
				onAssignItemIDButtonClick();
			}
		});
	}

	private void onAssignItemIDButtonClick() {

		NOVDataManagementServiceInfo novGetNextValueService = new NOVDataManagementServiceInfo();

		String strPrefixExp = "";
		String strCounterName = strPrefixExp + "_itemid";
		String strPostfixExp = "";
		String strGroupCode = "";
		String newId;
		if (!strPrefixExp.isEmpty()) {
			strGroupCode = strPrefixExp.substring(1);
		}

		setDatatoService(novGetNextValueService, strCounterName, strPrefixExp,
				strPostfixExp, strGroupCode);

		newId = novGetNextValueService.getNextValue();

		m_textItemID.setText(newId);
	}

	/**
	 * Set Input data to the service
	 */

	void setDatatoService(NOVDataManagementServiceInfo novGetNextValueService,
			String strCounterName, String strPrefixExp, String strPostfixExp,
			String strGroupCode) {
		int theScope = TCPreferenceService.TC_preference_site;

		novGetNextValueService.setContext("ITEM_ID");
		novGetNextValueService.setCounterName(strCounterName);
		novGetNextValueService
				.setDefaultCounterStartValue(NationalOilwell.DEFAULT_COUNTER_START_VALUE);
		novGetNextValueService.setPrefix(strPrefixExp);
		novGetNextValueService.setPostfix(strPostfixExp);

		PreferenceUtils PrefObj = new PreferenceUtils();

		boolean bJDEGroupDesicion = PrefObj.isPrefValueExist(strGroupCode,
				"NOV_JDE_check_creation_groups", theScope);
		if (bJDEGroupDesicion) {
			novGetNextValueService
					.setAlternateIDContext(new String[] { "JDE" });
		} else {
			novGetNextValueService.setAlternateIDContext(new String[] { "" });
		}
	}

	protected void createNameRow(Composite mainComposite) {
		Label lblName = new Label(mainComposite, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1));
		lblName.setText(m_registry.getString("lblName.NAME", "Key Not Found"));

		m_textName = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtName = new GridData(SWT.FILL, SWT.CENTER, true, false,
				GRID_DATA_HORIZONTAL_SPAN, 1);
		m_textName.setLayoutData(gd_txtName);

		m_textName.setTextLimit(NAME_TEXT_LIMIT);

		new Label(mainComposite, SWT.NONE);
	}

	protected void createName2Row(Composite mainComposite) {
		Label lblName2 = new Label(mainComposite, SWT.NONE);
		lblName2.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblName2.setText(m_registry.getString("lblName2.NAME", "Key Not Found"));

		m_textName2 = new Text(mainComposite, SWT.BORDER);
		GridData gd_txtName2 = new GridData(SWT.FILL, SWT.CENTER, true, false,
				GRID_DATA_HORIZONTAL_SPAN, 1);
		m_textName2.setLayoutData(gd_txtName2);

		m_textName2.setTextLimit(NAME_TEXT_LIMIT);

		new Label(mainComposite, SWT.NONE);
	}

	protected void createDescriptionRow(Composite mainComposite) {
		Label lblDescription = new Label(mainComposite, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
				true, 1, GRID_DATA_VERTICAL_SPAN));
		lblDescription.setText(m_registry.getString("lblDescription.NAME",
				"Key Not Found"));

		m_textDescription = new Text(mainComposite, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL | SWT.WRAP);
		GridData gd_txtDescription = new GridData(SWT.FILL, SWT.FILL, true,
				true, GRID_DATA_HORIZONTAL_SPAN, GRID_DATA_VERTICAL_SPAN);
		m_textDescription.setLayoutData(gd_txtDescription);

		addModifyListenerOnTextDescription();

		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);
		new Label(mainComposite, SWT.NONE);

	}

	protected void addModifyListenerOnTextDescription() {
		m_textDescription.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent modifyevent) {
				String sDescVal = m_textDescription.getText();
				int iDescLength = sDescVal.length();

				if (sDescVal.startsWith(" ") || sDescVal.startsWith("\r\n")) {
					m_textDescription.setText(sDescVal.trim());
				}

				if (iDescLength > DESCRIPTION_TEXT_LIMIT) {

					MessageBox.post(
							m_registry.getString("DescriptionError.MESSAGE"),
							"Error!", MessageBox.WARNING);

					sDescVal = sDescVal.substring(0,
							Math.min(iDescLength, DESCRIPTION_TEXT_LIMIT));

					m_textDescription.setText(sDescVal);
				}
			}
		});
	}

	public boolean createUIPost() throws TCException {
		boolean returnValue = true;

		// To set Class
		UIHelper.makeMandatory(m_textClass);
		//m_textClass.setEditable(false);

		// To set Item ID
		UIHelper.makeMandatory(m_textItemID);
		UIHelper.convertToUpperCase(m_textItemID);
		m_textItemID.setEditable(false);

		// To set Name and Name2
		UIHelper.convertToUpperCase(m_textName);
		UIHelper.convertToUpperCase(m_textName2);

		// To set Description
		UIHelper.makeMandatory(m_textDescription);
		UIHelper.convertToUpperCase(m_textDescription);
		UIHelper.enableSelectAll(m_textDescription);
		
		autoCompleteClassification();

		return returnValue;
	}
	
	public boolean autoCompleteClassification() throws TCException 
	{
		ClassHierarchyCreator classhierarchycreator = new ClassHierarchyCreator();
        String rootClassId= "DHT";
        
        try
        {
            classhierarchycreator.createStructure(rootClassId);
        }
        catch (Exception e)
        {
            throw new TCException(e);
        }
         
       
        String[] cp_array = (String[])classhierarchycreator.getLinearStructure().toArray(new String[0]);
        String [] classID_array = (String[]) classhierarchycreator.getClassID().toArray(new String[0]);
        String[] classpuid_array = (String[]) classhierarchycreator.getClassPuid().toArray(new String[0]);
        Map<String, String> classiMap = new HashMap<String, String>();
        Map<String, String> classUidMap = new HashMap<String, String>();
        new ClassificationAutoComplete(m_textClass, new TextContentAdapter(), cp_array);
        String[] classificationArray = new String[cp_array.length];
        for(int inx=0; inx < cp_array.length; inx++)
        {
            if(cp_array[inx].contains("->"))
            {
            	classificationArray[inx] = cp_array[inx].substring(cp_array[inx].lastIndexOf(">")+1);
            }
            
            classiMap.put(classificationArray[inx],classID_array[inx]);
            classUidMap.put(classificationArray[inx], classpuid_array[inx]);
            
        }
        ClassificationHelper.setClassificationMap(classiMap);
        ClassificationHelper.setClassificationUIDMap(classUidMap);
        
        return true;
	}

	@Override
	public boolean reload() throws TCException {
		return false;
	}

	@Override
	public IPropertyMap getData() {
		return null;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		// item properties
		m_textClass.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.ICS_SUBCLASS_NAME)));

		m_textName.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_NAME)));

		m_textDescription.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_DESC)));

		// item master properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {

			m_textName2.setText(PropertyMapHelper.handleNull(masterPropertyMap
					.getString(NationalOilwell.NAME2)));
		}

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		// item properties

		propMap.setString(
				PropertyMapHelper.handleNull(NationalOilwell.ICS_SUBCLASS_NAME),
				m_textClass.getText().trim());

		if (m_textClass.getText().trim() != "") {
			propMap.setString(PropertyMapHelper
					.handleNull(NationalOilwell.ICS_classified), "Y");
		}

		propMap.setString(NationalOilwell.ITEM_ID, m_textItemID.getText());

		propMap.setString(NationalOilwell.OBJECT_NAME, m_textName.getText());

		propMap.setString(NationalOilwell.OBJECT_DESC,
				m_textDescription.getText());

		// item master properties
		IPropertyMap masterPropertyMap = propMap
				.getCompound("IMAN_master_form");
		if (masterPropertyMap != null) {

			masterPropertyMap.setString(NationalOilwell.NAME2,
					m_textName2.getText());

			propMap.setCompound("IMAN_master_form", masterPropertyMap);
		}

		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException {
		return false;
	}

	@Override
	public void update(PublishEvent event) {
		final PublishEvent event1 = event;

		if (event.getPropertyName().equals(NationalOilwell.SELECTEDCLASS)) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					String selPath = event1.getNewValue().toString();

					if (selPath.contains("->")) {
						String selStr = selPath.substring(selPath
								.lastIndexOf(">") + 1);

						m_textClass.setText(selStr);
					}

					// m_textClass.setText(event1.getNewValue().toString());
				}
			});
		}

		if (event.getPropertyName().equals(
				NationalOilwell.ONAPPLY_OBJECT_CREATED)) {
			m_textItemID.setText("");
		}
	}

	protected void registerSubscriber() {
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();

		controller.registerSubscriber(NationalOilwell.SELECTEDCLASS, this);
		controller.registerSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED,
				this);
	}

	protected void unRegisterSubscriber() {
		IController controller = ControllerFactory.getInstance()
				.getDefaultController();

		controller.unregisterSubscriber(NationalOilwell.SELECTEDCLASS, this);
		controller.unregisterSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED,
				this);
	}

	@Override
	public void dispose() {
		unRegisterSubscriber();
		super.dispose();
	}

	/** The m_registry. */
	protected Registry m_registry = null;

	private TCSession m_session;

	public Text m_textClass;

	private Button m_btnClass;

	private Text m_textItemID;

	private Button m_btnAssignItemID;

	protected List m_listSite;

	protected Text m_textName;

	protected Text m_textName2;

	protected Text m_textDescription;

	private static final Integer HORIZONTAL_INDENT_ITEMID = 5;
	
	private static final Integer HORIZONTAL_INDENT_CLASS = 5;

	private static final Integer NAME_TEXT_LIMIT = 30;

	private static final Integer ITEM_ID_TEXT_LIMIT = 25;

	protected static final Integer DESCRIPTION_TEXT_LIMIT = 240;
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;
	
    private static final Integer GRID_DATA_HORIZONTAL_SPAN = 3;
	
	private static final Integer GRID_DATA_VERTICAL_SPAN = 3;
}