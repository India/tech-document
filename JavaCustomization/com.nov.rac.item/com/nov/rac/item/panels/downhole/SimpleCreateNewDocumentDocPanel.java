package com.nov.rac.item.panels.downhole;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class SimpleCreateNewDocumentDocPanel extends SimpleCreateNewPartDocumentPanel
{
    
    public SimpleCreateNewDocumentDocPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
        
        Composite l_composite = getComposite();
       
        createSecureButtonRow(subComposite);
        
        createUIPost();
        
        return returnValue;
    }
    
   
    
    private void createSecureButtonRow(Composite parent)
    {
        
        Label lblInfo = new Label(parent, SWT.NONE);
        lblInfo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, HORIZONATL_SPAN_FOR_SECURE_LABEL, 1));
        lblInfo.setText(m_registry.getString("lblInfo.NAME", "Key Not Found"));
        FontData fontData = lblInfo.getFont().getFontData()[0];
        fontData.setStyle(SWT.BOLD);
        Font font = new Font(lblInfo.getParent().getDisplay(), new FontData(fontData.getName(), FONT_SIZE_FOR_SECURE_TEXT, SWT.NONE));
        lblInfo.setFont(font);
        
        lblInfo.setForeground(getComposite().getDisplay().getSystemColor(SWT.COLOR_RED));
        
        Label lblSecure = new Label(parent, SWT.NONE);
        
        lblSecure.setText(m_registry.getString("lblSecure.NAME", "Key Not Found"));
        
        Composite radioComposite = new Composite(parent, SWT.BORDER);
        radioComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1));
        GridLayout gl_secureRow = new GridLayout(2, true);
        radioComposite.setLayout(gl_secureRow);
        
        m_radioBtnTrue = new Button(radioComposite, SWT.RADIO);
        m_radioBtnTrue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, VERTICAL_SPAN_FOR_SECURE_RADIOCOMPOSITE));
        m_radioBtnTrue.setText(m_registry.getString("radioBtnTrue.NAME", "Key Not Found"));
        
        m_radioBtnFalse = new Button(radioComposite, SWT.RADIO);
        GridData gd_false = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, VERTICAL_SPAN_FOR_SECURE_RADIOCOMPOSITE);
        gd_false.widthHint = WIDTH_HINT_FOR_RADIO;
        m_radioBtnFalse.setLayoutData(gd_false);
        
        m_radioBtnFalse.setText(m_registry.getString("radioBtnFalse.NAME", "Key Not Found"));
        m_radioBtnFalse.setSelection(true);
        
    }
    
    public boolean createUIPost() throws TCException
    {
        boolean returnValue = true;
        
        UIHelper.makeMandatory(m_textDocCategory);
        UIHelper.makeMandatory(m_docTypeLovPopupButton.getPopupButton());
        
        return returnValue;
    }
    
   
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        boolean returnValue = super.load(propMap);
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            boolean secureValue = masterPropertyMap.getLogical(NationalOilwell.SECURE);
            m_radioBtnTrue.setSelection(secureValue);
            m_radioBtnFalse.setSelection(!secureValue);
        }
        return returnValue;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        boolean returnValue = super.save(propMap);
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        
        if (masterPropertyMap != null)
        {
            masterPropertyMap.setLogical(NationalOilwell.SECURE, m_radioBtnTrue.getSelection());
        }
        
        return returnValue;
    }
    
    private Button m_radioBtnTrue;
    private Button m_radioBtnFalse;
    private final static int FONT_SIZE_FOR_SECURE_TEXT = 8;
    private final static int VERTICAL_SPAN_FOR_SECURE_RADIOCOMPOSITE = 4;
    private final static int HORIZONATL_SPAN_FOR_SECURE_LABEL = 3;
    private final static int WIDTH_HINT_FOR_RADIO = 60;
	
	private static final Integer COMPOSITE_GRID_LAYOUT = 5;
	private static final Integer LABEL_INFO_HORIZONTAL_SPAN = 5;
	private static final Integer RADIO_BOTTON_VERTICAL_SPAN = 6;
	private static final Integer DOC_CATEGORY_HORIZONTAL_SPAN = 3;
	private static final Integer DOC_TYPE_HORIZONTAL_SPAN = 3;
}
