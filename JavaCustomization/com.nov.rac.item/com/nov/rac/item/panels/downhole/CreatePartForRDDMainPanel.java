package com.nov.rac.item.panels.downhole;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.teamcenter.rac.kernel.TCException;

public class CreatePartForRDDMainPanel extends SimpleCreateNewPartMainPanel
{
    
    public CreatePartForRDDMainPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // item properties
        m_textName.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_NAME)));
        
        m_textDescription.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_DESC)));
        
        // item master properties
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            m_listSite.setItems(PropertyMapHelper.handleNull(masterPropertyMap.getStringArray(NationalOilwell.SITES)));
        }
        
        if ((masterPropertyMap.getStringArray(NationalOilwell.SITES)).length != 0)
        {
            // UIHelper.makeRequired(m_listSite);
            moveField(m_listSite);
        }
        
        return true;
    }
    
    protected void moveField(Control field)
    {
        Rectangle rect = field.getBounds();
        field.setLocation(rect.x + 1, rect.y);
        field.setLocation(rect.x, rect.y);
    }
    
}
