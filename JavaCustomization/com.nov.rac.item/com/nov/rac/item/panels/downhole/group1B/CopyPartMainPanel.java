package com.nov.rac.item.panels.downhole.group1B;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.item.panels.downhole.CreateNewPartDescDependentMainPanelWithoutSite;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.teamcenter.rac.kernel.TCException;

public class CopyPartMainPanel extends CreateNewPartDescDependentMainPanelWithoutSite {

	public CopyPartMainPanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		final Composite l_composite = getComposite();
		l_composite.setLayout(new GridLayout(COMPOSITE_GRID_LAYOUT, true));

		GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		l_composite.setLayoutData(gd_panel);
		
		createSelectedItemRow(l_composite);
		
		boolean returnValue = super.createUI();
		
		return returnValue;
	}
	
	private void createSelectedItemRow(Composite l_composite)
    {
		Label lblSelectedItem = new Label(l_composite, SWT.NONE);
		lblSelectedItem.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1));
		lblSelectedItem.setText(m_registry.getString("lblSelectedItem.NAME",
				"Key Not Found"));
        
        m_selectedItemText = new Text(l_composite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedItemText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, GRID_DATA_HORIZONTAL_SPAN, 1));
        
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        
        String [] searchItemTypes = { NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(l_composite, searchBtnGridData, searchItemTypes);
        
    }
	
	@Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        
        m_selectedItemText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        
        return true;
    }
	
    private Text m_selectedItemText;
    
    private static final Integer COMPOSITE_GRID_LAYOUT = 5;
	
    private static final Integer GRID_DATA_HORIZONTAL_SPAN = 3;
}
