package com.nov.rac.item.panels;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DocCategoryUIModel;
import com.nov.rac.item.helpers.NOVDatasetTypeTemplateFindHelper;
import com.nov.rac.item.helpers.NOVDatasetTypeUIPanelHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.contentadapter.TextboxContentAdapter;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.NIDLovPopupDialog;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.controls.SWTLovPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NewDatasetPanel extends AbstractUIPanel implements ILoadSave, ISubscriber, IPublisher
{
    private Composite m_subComposite;
    
    public NewDatasetPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        m_appReg = getRegistry();
        m_session = (TCSession) AIFUtility.getDefaultSession();
    }
    
    Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    // Start Dataset Type
    private void createDatasetTypeRow(final Composite parent) throws TCException
    {
        Label l_lblDatasetType = new Label(parent, SWT.NONE);
        // l_lblDatasetType.setLayoutData(getLblDatasetTypeLayout());
        l_lblDatasetType.setText(m_appReg.getString("DatasetTypeUIPanel.DatasetType"));
        
        m_textDatasetType = new Text(parent, SWT.BORDER);
        m_textDatasetType.setToolTipText(m_appReg.getString("DatasetType.ToolTip"));
        m_textDatasetType.setLayoutData(getTextDatasetTypeLayout());
        
        String[] lovValues = LOVUtils.getLovStringValues(NOVDatasetTypeUIPanelHelper.getDatasetTypes());
        new AutoCompleteField(m_textDatasetType, new TextboxContentAdapter(Arrays.asList(lovValues)), lovValues);
        m_textDatasetType.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent e)
            {
                try
                {
                    onDatasetTypeTextModified(e);
                }
                catch (TCException e1)
                {
                    e1.printStackTrace();
                }
            }
        });
        CreateLovPopupButton(parent);
    }
    
    protected GridData getLblDatasetTypeLayout()
    {
        return new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
    }
    
    protected GridData getTextDatasetTypeLayout()
    {
        return new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
    }
    
    // End Dataset Type
    
    // Start File Template
    private void createFileTemplateRow(final Composite parent)
    {
        Label l_lblFileTemplate = new Label(parent, SWT.NONE);
        l_lblFileTemplate.setLayoutData(getLblFileTemplateLayout());
        l_lblFileTemplate.setText(m_appReg.getString("DatasetTypeUIPanel.FileTemplate"));
        
        m_comboFileTemplate = new Combo(parent, SWT.NONE);
        m_comboFileTemplate.setLayoutData(getComboFileTemplateLayout());
        m_comboFileTemplate.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent e)
            {
                m_textImportFile.setText("");
            }
        });
    }
    
    protected GridData getLblFileTemplateLayout()
    {
        return new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
    }
    
    protected GridData getComboFileTemplateLayout()
    {
        return new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
    }
    
    // End File Template
    
    // Start Import File
    private void createImportFileRow(final Composite parent)
    {
        Label l_lblImportFile = new Label(parent, SWT.NONE);
        // l_lblImportFile.setLayoutData(getLblImportFileLayout());
        l_lblImportFile.setText(m_appReg.getString("DatasetTypeUIPanel.ImportFile"));
        
        m_textImportFile = new Text(parent, SWT.BORDER);
        m_textImportFile.setLayoutData(getTextImportFileLayout());
        
        m_browseButton = new Button(parent, SWT.PUSH);
        GridData gd_browseButton = getBrowseButtonLayout();
         gd_browseButton.horizontalIndent = SWTUIHelper
         .convertHorizontalDLUsToPixels(m_browseButton,
         HORIZONTAL_INDENT_BROWSE_BUTTON);
        m_browseButton.setLayoutData(gd_browseButton);
        m_browseButton.getBackground();
        m_browseButton.setText("...");
        m_browseButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent e)
            {
                onBrowseButtonWidgetSelected(e);
            }
        });
    }
    
    protected GridData getLblImportFileLayout()
    {
        return new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
    }
    
    protected GridData getTextImportFileLayout()
    {
        return new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
    }
    
    protected GridData getBrowseButtonLayout()
    {
        return new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
    }
    
    // End Import File
    
    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        // IController l_controller =
        // ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        l_controller.registerSubscriber(NationalOilwell.DOCCATEGORY_TYPE, getSubscriber());
        l_controller.registerSubscriber(NationalOilwell.COPY_DOC, getSubscriber());
        l_controller.registerSubscriber("NewLOVValueSelected", getSubscriber());
        l_controller.registerSubscriber(NationalOilwell.CLONE_DOC_SELECTION_EVENT, getSubscriber());
        l_controller.registerSubscriber("object_name", new ISubscriber()
        {
            @Override
            public void update(PublishEvent event)
            {
                onUpdateObjectName(event);
            }
        });
    }
    
    private void onUpdateObjectName(PublishEvent event)
    {
        Object objectName = event.getNewValue();
        if (null != objectName)
        {
            m_objectName = objectName.toString();
        }
    }
    
    private void onBrowseButtonWidgetSelected(SelectionEvent e)
    {
        final Composite current = getComposite();
        FileDialog dialog = new FileDialog(current.getShell(), SWT.NULL);
        dialog.open();
        m_comboFileTemplate.setText("");
        String selectedFile = dialog.getFileName();
        if (!selectedFile.isEmpty())
        {
            m_importFileAbsolutePath = dialog.getFilterPath() + File.separator + selectedFile;
            m_textImportFile.setText(m_importFileAbsolutePath);
        }
    }
    
    protected void onDatasetTypeTextModified(ModifyEvent e) throws TCException
    {
        Object source = (Object) (e.getSource());
        if (m_textDatasetType.equals(source))
        {
            if (!m_textDatasetType.getText().equals(""))
            {
                ControllerFactory.getInstance().getDefaultController()
                // .publish(getPublishEvent(NationalOilwell.DATASET_TYPE,
                // "Prev Val", m_textDatasetType.getText()));
                        .publish(getPublishEventForDatasetType());
                
                List<String> lovList = Arrays.asList(LOVUtils.getLovStringValues(NOVDatasetTypeUIPanelHelper
                        .getDatasetTypes()));
                if (lovList.contains(m_textDatasetType.getText()))
                {
                    m_comboFileTemplate.removeAll();
                    buildFileTemplateCombo();
                    setEnable(m_textDatasetType.isEnabled());
                }
            }
            else
            {
                m_comboFileTemplate.setText("");
                m_textImportFile.setText("");
                m_comboFileTemplate.setEnabled(false);
                m_textImportFile.setEnabled(false);
                m_browseButton.setEnabled(false);
            }
        }
    }
    
    protected PublishEvent getPublishEventForDatasetType()
    {
        return getPublishEvent(NationalOilwell.DATASET_TYPE, "Prev Val", m_textDatasetType.getText());
    }
    
    protected void setEnable(boolean enable)
    {
        m_textDatasetType.setEnabled(enable);
        m_textImportFile.setEnabled(enable);
        m_comboFileTemplate.setEnabled(enable);
        m_browseButton.setEnabled(enable);
        m_dataSetLovPopupButton.getPopupButton().setEnabled(enable);
    }
    
    protected void buildFileTemplateCombo()
    {
        if (m_comboFileTemplate.getSelectionIndex() < 0)
        {
            m_textImportFile.setText("");
            TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
            String currentGroup = null;
            try
            {
                currentGroup = theSession.getCurrentGroup().getGroupName();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            NOVDatasetTypeTemplateFindHelper l_panelHelper = NOVDatasetTypeTemplateFindHelper.getInstance();
            m_datasetTemplateMap = l_panelHelper.getDatasetTemplate(m_textDatasetType.getText(), currentGroup,
                    m_propDocCategory, m_propDocType);
            String[] templates = new String[m_datasetTemplateMap.size()];
            templates = (String[]) m_datasetTemplateMap.keySet().toArray(templates);
            m_comboFileTemplate.setItems(templates);
            m_comboFileTemplate.select(0);
        }
    }
    
    private void CreateLovPopupButton(Composite l_composite) throws TCException
    {
        TCComponent l_tcComponent = null;
        
        Button l_button = new Button(l_composite, SWT.NONE);
        GridData gd_l_button = getLovPopupButtonLayout();
        gd_l_button.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(l_button,
                HORIZONTAL_INDENT_LOV_POPUP_BUTTON);
        l_button.setLayoutData(gd_l_button);
        
        l_button.setToolTipText(m_appReg.getString("LovButton.ToolTip"));
        Image l_dataSetLovPopupButtonImage = new Image(l_composite.getDisplay(),
                m_appReg.getImage("DatasetTypeUIPanel.LOVButtonImage"), SWT.NONE);
        
        final NIDLovPopupDialog l_lovpopupdialog = new NIDLovPopupDialog(l_composite.getShell(), SWT.NONE,
                NOVDatasetTypeUIPanelHelper.getDatasetTypes(), "Default");
        
        m_dataSetLovPopupButton = new SWTLovPopupButton(l_button, l_lovpopupdialog,
                NOVDatasetTypeUIPanelHelper.getDatasetTypes(), "Default", l_tcComponent);
        m_dataSetLovPopupButton.setIcon(l_dataSetLovPopupButtonImage);
        l_lovpopupdialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(final PropertyChangeEvent propertychangeevent)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        m_textDatasetType.setText(propertychangeevent.getNewValue().toString());
                    }
                });
            }
        });
    }
    
    protected GridData getLovPopupButtonLayout()
    {
        return new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
    }
    
    protected PublishEvent getPublishEvent(String property, Object prevValue, Object newValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property, newValue, null);
        event.setNewValue(newValue);
        return event;
    }
    
    IPublisher getPublisher()
    {
        return this;
    }
    
    ISubscriber getSubscriber()
    {
        return (ISubscriber) this;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(getUILayout());
        
        createDatasetHeader(l_composite);
        m_subComposite = SWTUIHelper.createRowComposite(l_composite, NUM_OF_COLUMNS_FOR_DATASET_SUBCOMPOSITE, false);
       
       
        createDatasetTypeRow(m_subComposite);
        createFileTemplateRow(m_subComposite);
        createImportFileRow(m_subComposite);
        registerProperties();
        setEnable(true);
        disableRightClick(); // TCDECREl-6370
        return true;
    }
    
    protected GridLayout getUILayout()
    {
        return new GridLayout(1, true);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        if (m_propMap == null)
        {
            m_propMap = new SimplePropertyMap();
            m_propMap.addAll(propMap);
        }
        m_textDatasetType.setText(PropertyMapHelper.handleNull(propMap.getString("object_type")));
        return true;
    }
    
    protected void createDatasetHeader(final Composite datasetComposite) throws TCException
    {
        
        Label l_lblDatasetHeader = new Label(datasetComposite, SWT.NONE);
        l_lblDatasetHeader.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                HORIZONTAL_SPAN_FOR_DATASET_HEADER, 1));
        l_lblDatasetHeader.setText(m_appReg.getString("DatasetTypeUIPanel.Header", "Key Not Found"));// "Doc Category"
        
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        String datasetTypeText = m_textDatasetType.getText();
        if (null != datasetTypeText && (!"".equals(datasetTypeText.trim())))
        {
            
            TCComponent datasetDefinition = NOVDatasetTypeUIPanelHelper
                    .getDatasetDefinition(datasetTypeText, m_session);
            propMap.setString("dataset_type", datasetDefinition.toString());
            propMap.setString("object_name", m_objectName);
            propMap.setString("datasetFilePath", m_textImportFile.getText());
            String template = m_comboFileTemplate.getText();
            if (template != null && !template.equals(""))
            {
                String uid = (String) m_datasetTemplateMap.get(template);
                TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
                TCComponent selectedTemplate = theSession.stringToComponent(uid);
                propMap.setTag("selectedTemplate", selectedTemplate);
            }
            
        }
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    public boolean reset()
    {
        m_textDatasetType.setText("");
        m_textImportFile.setText("");
        return true;
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.COPY_DOC))
        {
            try
            {
                load(m_propMap);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        else if (event.getPropertyName().equals(NationalOilwell.CLONE_DOC_SELECTION_EVENT))
        {
            {
                boolean m_iscloneDocSelected = (Boolean) event.getNewValue();
                if (m_propMap != null)
                {
                    if (m_iscloneDocSelected)
                    {
                        try
                        {
                            load(m_propMap);
                            
                            // disableFields();
                        }
                        catch (TCException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                
            }
        }
        else
        {
            final DocCategoryUIModel docCategoryUIModel = (DocCategoryUIModel) event.getNewValue();
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    if (!docCategoryUIModel.getDocumentCategory().equals(""))
                    {
                        m_propDocCategory = docCategoryUIModel.getDocumentCategory();
                        m_textDatasetType.setEnabled(docCategoryUIModel.isDocCategoryTextEnabled());
                        m_dataSetLovPopupButton.getPopupButton().setEnabled(
                                docCategoryUIModel.isDocCategoryTextEnabled());
                    }
                    else
                    {
                        m_textDatasetType.setText("");
                        m_comboFileTemplate.setText("");
                        setEnable(false);
                    }
                    if (!docCategoryUIModel.getDocumentType().equals(""))
                    {
                        m_propDocType = docCategoryUIModel.getDocumentType();
                    }
                }
            });
        }
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public void dispose()
    {
        
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        // IController l_controller =
        // ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        l_controller.unregisterSubscriber(NationalOilwell.DOCCATEGORY_TYPE, getSubscriber());
        l_controller.unregisterSubscriber("NewLOVValueSelected", getSubscriber());
        l_controller.unregisterSubscriber("object_name", getSubscriber());
        l_controller.unregisterSubscriber(NationalOilwell.COPY_DOC, getSubscriber());
        l_controller.unregisterSubscriber(NationalOilwell.CLONE_DOC_SELECTION_EVENT, getSubscriber());
        
        super.dispose();
    }
    
    private void disableRightClick()
    {
        
        NewItemCreationHelper.disableRightClick(m_textDatasetType);
        NewItemCreationHelper.disableRightClick(m_textImportFile);
        NewItemCreationHelper.disableRightClick(m_comboFileTemplate);
    }
    
    private TCSession m_session = null;
    private Registry m_appReg = null;
    protected Text m_textDatasetType;
    protected Text m_textImportFile;
    protected Combo m_comboFileTemplate;
    protected Button m_browseButton;
    String m_propDocCategory = "";
    protected String m_propDocType = "";
    protected SWTLovPopupButton m_dataSetLovPopupButton;
    private String m_importFileAbsolutePath = "";
    private String m_objectName = "";
    private TCComponent[] m_templateList = null;
    protected IPropertyMap m_propMap = null;
    private static final Integer HORIZONTAL_INDENT_LOV_POPUP_BUTTON = 5;
    private static final Integer HORIZONTAL_INDENT_BROWSE_BUTTON = 5;
    private final static int NUM_OF_COLUMNS_FOR_DATASET_SUBCOMPOSITE = 3;
    private final static int HORIZONTAL_SPAN_FOR_DATASET_HEADER = 5;
    
    protected Map m_datasetTemplateMap = new HashMap<String, String>();
}
