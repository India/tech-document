package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class SimpleSelectedBasePanel extends AbstractUIPanel implements ILoadSave
{
    
    public SimpleSelectedBasePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_composite = getComposite();
        
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_gdDocComposite.horizontalIndent = HORIZONTAL_INDENT_FOR_COMPOSITE;
        l_composite.setLayoutData(l_gdDocComposite);
        l_composite.setLayout(new GridLayout(NUM_OF_COLUMNS_FOR_COMPOSITE, false));
        
        createSelectedBaseRow(l_composite);
        
        return true;
    }
    
    private void createSelectedBaseRow(Composite l_composite)
    {
        m_selectedBaseLbl = new Label(l_composite, SWT.NONE);
        m_selectedBaseLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        m_selectedBaseLbl.setText(m_registry.getString("operationTypePanel.selectedBase"));
        
        m_selectedBaseText = new Text(l_composite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedBaseText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        UIHelper.makeMandatory(m_selectedBaseText);
        
        GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        /*
         * SearchButtonComponent searchButton = new
         * SearchButtonComponent(selectedBaseComposite, SWT.TOGGLE); GridData
         * searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false,
         * 2, 1); searchBtnGridData.horizontalIndent =
         * SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
         * NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
         * searchButton.setLayoutData(searchBtnGridData);
         * searchButton.setSearchItemType(new String[] {
         * NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING });
         */
        
        String[] searchItemTypes = { NationalOilwell.PART_BO_TYPE, NationalOilwell.NON_ENGINEERING };
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(l_composite, searchBtnGridData,
                searchItemTypes);
        
        // Dummy Label
        GridData blankLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_DUMMY_LABEL, 1);
        new Label(l_composite, SWT.NONE).setLayoutData(blankLabel);
        new Label(l_composite, SWT.NONE).setLayoutData(blankLabel);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_selectedBaseText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(NationalOilwell.ITEM_ID, m_selectedBaseText.getText().trim());
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    private final static int HORIZONTAL_INDENT_FOR_COMPOSITE = 5;
    private final static int NUM_OF_COLUMNS_FOR_COMPOSITE = 7;
    private final static int HORIZONTAL_SPAN_FOR_DUMMY_LABEL = 2;
    
    private Label m_selectedBaseLbl;
    private Text m_selectedBaseText;
    private Registry m_registry = null;
}
