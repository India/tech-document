package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

/**
 * The Class CopyDocumentPanel.
 */
public class CopyDocumentPanel extends NewDocumentInfoPanel
{
    
    /**
     * Instantiates a new copy document panel.
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public CopyDocumentPanel(Composite parent, int style)
    {
        super(parent, style);
        m_appReg = getRegistry();
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.item.panels.NewDocumentInfoPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        Composite copyDocInfoPanel = getComposite();
        GridData l_gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        copyDocInfoPanel.setLayoutData(l_gd_panel);
        copyDocInfoPanel.setLayout(new GridLayout(1, true));
        createSelectedItemRow(copyDocInfoPanel);
        boolean returnValue = super.createUI();
        m_revisionText.setEnabled(true);
        m_revisionText.setEditable(true);
        
        return returnValue;
    }
    
    /**
     * Creates the selected item row.
     * 
     * @param copyDocInfoPanel
     *            the copy doc info panel
     */
    private void createSelectedItemRow(Composite copyDocInfoPanel)
    {
        GridData l_gd_panel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        copyDocInfoPanel.setLayoutData(l_gd_panel);
        copyDocInfoPanel.setLayout(new GridLayout(5, true));
        
        m_selectedItemLabel = new Label(copyDocInfoPanel, SWT.NONE);
        m_selectedItemLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_selectedItemLabel.setText(m_appReg.getString("CopyDocInfoPanel.SelectedItem"));
        m_selectedItemText = new Text(copyDocInfoPanel, SWT.BORDER);
        m_selectedItemText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
        m_selectedItemText.setEnabled(false);
        
        GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        // new Label(copyDocInfoPanel, SWT.NONE);
        TCComponent targetItem = NewItemCreationHelper.getTargetComponent();
      /*  SearchButtonComponent searchButton = new SearchButtonComponent(copyDocInfoPanel, SWT.TOGGLE);
        GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		searchBtnGridData.horizontalIndent=SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON );
        searchButton.setLayoutData(searchBtnGridData);
        searchButton.setSearchItemType(new String[] { NationalOilwell.DOCUMENT_TYPE });*/
        
        String [] searchItemTypes = {NationalOilwell.DOCUMENT_TYPE};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(copyDocInfoPanel, searchBtnGridData, searchItemTypes);
        
        if(targetItem==null)
        {
        	searchButton.setVisible(true);
        }
        else
        {
        	searchButton.setVisible(false);
        }
        	
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.item.panels.SimpleNewDocumentInfoPanel#load(com.nov.rac.
     * propertymap.IPropertyMap)
     */
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
    	m_selectedItemText.setText(PropertyMapHelper.handleNull(propMap.getString("item_id")));
        super.load(propMap);
        m_itemIdText.setText("");
        return true;
    }
    
    /** The m_selected item text. */
    private Text m_selectedItemText;
    
    /** The m_selected item label. */
    private Label m_selectedItemLabel;
    
}
