package com.nov.rac.item.panels.factory;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.item.dialog.AbstractDialogButtonBar;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class PanelFactory
{
    private PanelFactory()
    {
        m_registry = Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    /**
     * Get singleton instance of Attribute Factory.
     * 
     * @return
     */
    
    public static PanelFactory getInstance()
    {
        if (m_factory == null)
        {
            m_factory = new PanelFactory();
        }
        
        return m_factory;
    }
    
    public void setParentComposite(Composite parentComposite)
    {
        m_ParentComposite = parentComposite;
    }
    
    public ArrayList<IUIPanel> createPanels(Composite parent, String panelType)
    {
        return createPanels(parent, panelType, false);
    }
    
    public ArrayList<IUIPanel> createPanels(Composite parent, String panelType, boolean withSeparator)
    {
        String[] panelNames;
        
        panelNames = NewItemCreationHelper.getConfiguration(panelType);
        
        return createPanels(parent, panelNames, withSeparator);
    }
    
    public ArrayList<IUIPanel> createPanels(Composite parent, String[] panelNames, boolean withSeparator)
    {
        return instantiatePanels(parent, panelNames, withSeparator);
    }
    
    private ArrayList<IUIPanel> instantiatePanels(Composite parent, String[] panelNames, boolean withSeparator)
    {
        ArrayList<IUIPanel> panels = new ArrayList<IUIPanel>();
        IUIPanel panel = null;
        
        // create panels from panel names
        Object[] params = new Object[2];
        params[0] = parent;
        params[1] = SWT.NONE;
        
        for (String panelName : panelNames)
        {
            
            panel = createPanel(panelName, params);
            if (panel != null)
            {
                try
                {
                    panel.createUI();
                    if (withSeparator)
                    {
                        addSeparator(parent);
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
                
                panels.add(panel);
            }
            
        }
        
        //KLOC2124 Pushed the logic in a new method
        setMandatoryFields(panels);
        
        return panels;
        
    }
    // TCDECREL-6756 Setting mandatory fields  
    private void setMandatoryFields(ArrayList<IUIPanel> panels)
    {
    	for (IUIPanel subPanel : panels)
        {          
            try 
            {
				subPanel.createUIPost();
			} 
            catch (TCException e) 
            {
				e.printStackTrace();
			}                        
        }     
	}
      
    public AbstractDialogButtonBar createButtonBar(Composite parent,String type)
    {
        
        String[] panelNames = NewItemCreationHelper.getConfiguration(type);
        AbstractDialogButtonBar panel = null;
        if (panelNames.length > 0)
        {
            Object[] params = new Object[2];
            params[0] = parent;
            params[1] = SWT.NONE;
            
            String[] configParams = NewItemCreationHelper.getConfiguration(panelNames[0] + NationalOilwell.DOTSEPARATOR
                    + CLASS_NAME);
            if (configParams.length > 0)
            {
                String className = configParams[0];
              
                try
                {
                    panel = (AbstractDialogButtonBar) Instancer.newInstanceEx(className, params);
                    panel.pack();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return panel;
    }
    
    /*
     * private String[] getConfiguration(String context) { String[]
     * configParams; String defaultPanelContext = null; String currentGroup =
     * NewItemCreationHelper.getCurrentGroup(); defaultPanelContext =
     * currentGroup + NationalOilwell.DOTSEPARATOR + context; // read default
     * panels to be created from registry for group configParams =
     * m_registry.getStringArray(defaultPanelContext, null); // if no default
     * panels for logged in group, // read default panels to be created from
     * registry for current bussiness // unit if (configParams == null) {
     * defaultPanelContext = NewItemCreationHelper.getBusinessGroup() +
     * NationalOilwell.DOTSEPARATOR + context; configParams =
     * m_registry.getStringArray(defaultPanelContext, null); } // if no default
     * panels for current bussiness unit, // read default panels to be created
     * from registry if (configParams == null) { defaultPanelContext = context;
     * configParams = m_registry.getStringArray(defaultPanelContext, null); }
     * return configParams; }
     */
    
    private IUIPanel createPanel(String panelName, Object[] params)
    {
        
        String[] configParams = NewItemCreationHelper.getConfiguration(panelName + NationalOilwell.DOTSEPARATOR
                + CLASS_NAME);
        String className = configParams[0];
        // IUIPanel panel =(IUIPanel)(m_registry.newInstanceFor(className,
        // params));
        IUIPanel panel = null;
        try
        {
            panel = (IUIPanel) Instancer.newInstanceEx(className, params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return panel;
    }
    
    private void addSeparator(Composite parent)
    {
        Label separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
        separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    }
    
    private static PanelFactory m_factory = null;
    private Registry m_registry;
    private Composite m_ParentComposite = null;
    private final String DEFAULT_PANELS = "DEFAULT_PANELS";
    private final String DEFAULT_BUTTONBAR = "DEFAULT.BUTTONBAR";
    private final String CLASS_NAME = "CLASS";
    
}
