package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneLegacyBaseRevFieldPanel extends AbstractUIPanel implements ILoadSave, IPublisher
{
    
    public RSOneLegacyBaseRevFieldPanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_composite = getComposite();
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, HORIZONTAL_SPAN_FOR_COMPOSITE, 1));
        l_composite.setLayout(new GridLayout(NUM_OF_COLUMNS_FOR_BASE_REV_PANEL, true));
        
        createBaseRevRow(l_composite);
        
        UIHelper.makeMandatory(m_baseText);
        UIHelper.makeMandatory(m_revText);
        
        return true;
    }
    
    private void createBaseRevRow(Composite l_composite)
    {
        Composite baseComposite = new Composite(l_composite, SWT.NONE);
        baseComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_BASE_COMPOSITE,
                1));
        baseComposite.setLayout(new GridLayout(NUM_OF_COLUMNS_FOR_BASE_COMPOSITE, false));
        
        m_baseNameLabel = new Label(baseComposite, SWT.NONE);
        m_baseNameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        m_baseNameLabel.setText(m_registry.getString("Base.NAME"));
        
        m_baseText = new Text(baseComposite, SWT.BORDER);
        GridData l_gdBaseText = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_BASE_TEXT, 1);
        m_baseText.setLayoutData(l_gdBaseText);
        m_baseText.setTextLimit(BASE_FIELD_TEXT_LIMIT);
        
        m_revLabel = new Label(/* revComposite */baseComposite, SWT.NONE);
        GridData l_gdRevLabel = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
        l_gdRevLabel.horizontalIndent = HORIZONTAL_INDENT;
        m_revLabel.setLayoutData(l_gdRevLabel);
        m_revLabel.setText(m_registry.getString("Rev.NAME"));
        
        m_revText = new Text(baseComposite, SWT.BORDER);
        GridData l_gdRevText = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        l_gdRevText.widthHint = WIDTH_FOR_REV_TEXT;
        m_revText.setLayoutData(l_gdRevText);
        m_revText.setTextLimit(2);
        
        // Dummy Label
        GridData blankLabel = new GridData(SWT.FILL, SWT.CENTER, false, false, HORIZONTAL_SPAN_FOR_DUMMY_LABEL, 1);
        new Label(l_composite, SWT.NONE).setLayoutData(blankLabel);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_baseText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        m_revText.setText(PropertyMapHelper.handleNull(propMap.getCompound("revision").getString(
                NationalOilwell.ITEM_REVISION_ID)));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(NationalOilwell.ITEM_ID, m_baseText.getText().trim());
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            masterPropertyMap.setString(NationalOilwell.RSONE_BASE_NUMBER, m_baseText.getText().trim());
        }
        
        IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            revisionPropertyMap.setString("item_revision_id", m_revText.getText());
            propMap.setCompound("revision", revisionPropertyMap);
        }
        
        publishEvents();
        
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, m_baseText.getText().trim(),
                null);
        
        controller.publish(event);
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    // Tushar start
    private final static int HORIZONTAL_INDENT = 20;
    private final static int HORIZONTAL_SPAN_FOR_COMPOSITE = 5;
    private final static int NUM_OF_COLUMNS_FOR_BASE_REV_PANEL = 5;
    private final static int BASE_FIELD_TEXT_LIMIT = 32;
    private final static int HORIZONTAL_SPAN_FOR_BASE_COMPOSITE = 3;
    private final static int NUM_OF_COLUMNS_FOR_BASE_COMPOSITE = 6;
    private final static int NUM_OF_COLUMNS_FOR_REV_COMPOSITE = 7;
    private final static int WIDTH_FOR_REV_TEXT = 30;
    private final static int HORIZONTAL_SPAN_FOR_DUMMY_LABEL = 2;
    private final static int HORIZONTAL_SPAN_FOR_BASE_TEXT = 3;
    // Tushar end
    
    /** The m_name label. */
    private Label m_baseNameLabel;
    
    /** The m_description label. */
    private Label m_revLabel;
    
    /** The m_comboName ComboBox. */
    public Text m_baseText;
    
    /** The m_description Text. */
    public Text m_revText;
    
    /** The m_registry. */
    protected Registry m_registry;
}
