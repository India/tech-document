package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIDSelectedPanel extends AssignOracleIdPanel
{
    
    public AssignOracleIDSelectedPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    public boolean createUI() throws TCException
    {
        boolean returnCode = super.createUI();
        
        super.m_assignOracleIdBtn.setSelection(true);
        
        return returnCode;
    }
    
}
