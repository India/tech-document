package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreatePartDocCategoryPanel extends RSOneSimpleDocCategoryPanel

{
    
    public RSOneCreatePartDocCategoryPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite l_docComposite = getComposite();
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_docComposite.setLayoutData(l_gdDocComposite);
        l_docComposite.setLayout(new GridLayout(5, true));
        
        createEmptyPanel(l_docComposite);
        
        createDocCategoryRow(l_docComposite);
        
        createDocTypeRow(l_docComposite);
        
        createDatasetTypeRow(l_docComposite);
        
        createFileTemplateRow(l_docComposite);
        
        createImportFileRow(l_docComposite);
        
        return true;
    }
    
    protected void createEmptyPanel(Composite l_composite)
    {
        Composite emptyComposite = new Composite(l_composite, SWT.None);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 5);
        emptyComposite.setLayoutData(gridData);
        emptyComposite.setLayout(new GridLayout(1, true));
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
