package com.nov.rac.item.panels;

import static com.nov.rac.item.helpers.DocumentHelper.getDocCategory;
import static com.nov.rac.item.helpers.DocumentHelper.getDocumentTypes;
import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.awt.AWTException;
import java.awt.Robot;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DocCategoryUIModel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.contentadapter.TextboxContentAdapter;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.NIDLovPopupDialog;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.controls.SWTLovPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class SimpleDocumentCreatePanel extends AbstractUIPanel implements ILoadSave, IPublisher, ISubscriber
{
    
    public SimpleDocumentCreatePanel(Composite parent, int style)
    {
        super(parent, style);
        
        m_registry = getRegistry();
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite mainComposite = getComposite();
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        mainComposite.setLayoutData(gd_panel);
        mainComposite.setLayout(getUILayout());
        
        createDocumentHeader(mainComposite);
        subComposite = SWTUIHelper.createRowComposite(mainComposite, 3, false);
        
        createDocCategoryRow(subComposite);
        createDocTypeRow(subComposite);
        
        addListners();
        
        return true;
    }
    
    private void addListners()
    {
        addFocusListenerOnDocCategoryText(m_textDocCategory);
        addModifyListenerOnDocCategoryText(m_textDocCategory);
    }
    
    protected TCComponentListOfValues getDocCategoryLOV()
    {
        return getDocCategory(NationalOilwell.DOCUMENT_TYPES_LOV);
    }
    
    protected GridLayout getUILayout()
    {
        return new GridLayout(1, true);
    }
    
    /**
     * Creates the doc category row.
     * 
     * @param parent
     *            the document composite
     * @throws TCException
     *             the tC exception
     */
    protected void createDocCategoryRow(final Composite parent) throws TCException
    {
        
        Label lblDocCategory = new Label(parent, SWT.NONE);
        lblDocCategory.setText(m_registry.getString("docCategory.NAME", "Key Not Found"));// "Doc Category"
        
        Composite createRowComposite = SWTUIHelper.createRowComposite(parent, 2, false);
        createRowComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
        
        m_textDocCategory = new Text(createRowComposite, SWT.BORDER);
        GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_textDocCategory.setLayoutData(gd_text);
        m_textDocCategory.setToolTipText(m_registry.getString("docCategoryEditor.TOOLTIP"));
        
        String[] lovValues = LOVUtils.getLovStringValues(getDocCategoryLOV());
        new AutoCompleteField(m_textDocCategory, new TextboxContentAdapter(Arrays.asList(lovValues)), lovValues);
        
        createDocCategoryLovPopupButton(createRowComposite);
        
    }
    
    protected void createDocumentHeader(final Composite documentComposite) throws TCException
    {
        
        Label l_lblRDDHeader = new Label(documentComposite, SWT.NONE);
        l_lblRDDHeader.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        l_lblRDDHeader.setText(m_registry.getString("docType.Header", "Key Not Found"));// "Doc Category"
        
    }
    
    /**
     * Creates the doc type row.
     * 
     * @param parent
     *            the document composite
     * @throws TCException
     *             the tC exception
     */
    protected void createDocTypeRow(final Composite parent) throws TCException
    {
        
        Label lblDocType = new Label(parent, SWT.NONE);
        lblDocType.setText(m_registry.getString("docType.NAME", "Key Not Found"));
        createDocTypeLovPopupButton(parent);
    }
    
    /**
     * Creates the doc category lov popup button.
     * 
     * @param current
     *            the current
     * @throws TCException
     *             the tC exception
     */
    private void createDocCategoryLovPopupButton(Composite current) throws TCException
    {
        
        TCComponent l_tcComponent = null;
        
        m_BtnDocCategory = new Button(current, SWT.NONE);
        GridData gd_btnDocCategory = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
        m_BtnDocCategory.setToolTipText(m_registry.getString("docCategoryPopup.TOOLTIP", "Key Not Found "));
        gd_btnDocCategory.horizontalIndent = HORIZONTAL_INDENT_BTN_DOC_CATEGORY;
        m_BtnDocCategory.setLayoutData(gd_btnDocCategory);
        
        m_LovPopupButtonImage = new Image(current.getDisplay(),
                m_registry.getImage("NewDocCategoryUIPanel1.LOVButtonImage"), SWT.NONE);
        
        final NIDLovPopupDialog docCategoryDialog = new NIDLovPopupDialog(current.getShell(), SWT.NONE,
                getDocCategoryLOV(), "Default");
        
        docCategoryDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    isModifiedByLOV = true;
                    setTextForDocCategoryAndDocType(propertychangeevent.getNewValue().toString());
                    
                }
            }
        });
        
        m_docCategoryPopupButton = new SWTLovPopupButton(m_BtnDocCategory, docCategoryDialog, getDocCategoryLOV(), "",
                l_tcComponent);
        
        m_docCategoryPopupButton.setIcon(m_LovPopupButtonImage);
    }
    
    /**
     * Sets the text for doc category and doc type.
     * 
     * @param value
     *            the new text for doc category and doc type
     */
    private void setTextForDocCategoryAndDocType(String value)
    {
        if (hasContent(value) && value.length() > (HIPHEN_POSITION + 1))
        {
            final String textValue = value.substring(0, HIPHEN_POSITION).trim();
            final String buttonCaption = value.substring(HIPHEN_POSITION + 1).trim();
            
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    if (m_textDocCategory.isEnabled()) // TCDECREL-6566
                    {
                        setTextForDocCategoryEnabled(textValue, buttonCaption);
                        
                        Display.getDefault().asyncExec(new Runnable()
                        {
                            public void run()
                            {
                                try
                                {
                                    if (m_textDocCategory.setFocus() && m_textDocCategory.isFocusControl())
                                    {
                                        Robot robot = new Robot();
                                        robot.keyPress(java.awt.event.KeyEvent.VK_INSERT);
                                        robot.keyRelease(java.awt.event.KeyEvent.VK_INSERT);
                                    }
                                    
                                }
                                catch (AWTException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    
                }
            });
        }
    }
    
    protected void setTextForDocCategoryEnabled(String textValue, String buttonCaption)
    {
        m_textDocCategory.setText(textValue);
        m_BtnDocType.setText(buttonCaption);
        m_BtnDocType.setEnabled(true);
        setDocTypeLovComponent(m_BtnDocType, textValue);
        publishDocCategory_Type();
        m_textDocCategory.setSelection(m_textDocCategory.getText().length());
        moveDocTypeButton();
    }
    
    /**
     * Sets the doc type lov component.
     * 
     * @param m_btnDocType
     *            the m_btn doc type
     * @param docCategoryLovValueSelected
     *            the doc category lov value selected
     */
    protected void setDocTypeLovComponent(Button btnDocType, final String docCategoryLovValueSelected)
    {
        
        try
        {
            docTypeDialog.setLovComponent(getDocumentTypes(docCategoryLovValueSelected));
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    protected GridData getGridDataForDocCategory()
    {
        return new GridData(SWT.FILL, SWT.TOP, true, false, 4, 1);
    }
    
    protected void publishDocCategory_Type()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        DocCategoryUIModel docCategoryUIModel = new DocCategoryUIModel();
        
        docCategoryUIModel.setDocumentCategory(m_textDocCategory.getText());
        docCategoryUIModel.setDocumentType(m_BtnDocType.getText());
        docCategoryUIModel.setDocCategoryTextEnable(m_textDocCategory.isEnabled());
        
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.DOCCATEGORY_TYPE, docCategoryUIModel,
                null);
        
        controller.publish(event);
        
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    /**
     * Fire a m_ButDocType move event.
     */
    protected void moveDocTypeButton()
    {
        Rectangle rect = m_BtnDocType.getBounds();
        m_BtnDocType.setLocation(rect.x + 1, rect.y);
        m_BtnDocType.setLocation(rect.x, rect.y);
    }
    
    /**
     * Creates the DocTypeLOVPopupButton.
     * 
     * @param documentComposite
     *            the document composite
     * @throws TCException
     *             the tC exception
     */
    private void createDocTypeLovPopupButton(Composite documentComposite) throws TCException
    {
        
        TCComponent l_tcComponent = null;
        m_BtnDocType = new Button(documentComposite, SWT.NONE);
        m_BtnDocType.setToolTipText(m_registry.getString("docTypePopup.TOOLTIP", "Key Not Found "));
        m_BtnDocType.setEnabled(false);
        
        docTypeDialog = new NIDLovPopupDialog(documentComposite.getShell(), SWT.NONE, getDocCategoryLOV(), "Default");
        m_docTypeLovPopupButton = new SWTLovPopupButton(m_BtnDocType, docTypeDialog, getDocCategoryLOV(), "Default",
                l_tcComponent);
        GridData gd_DocType = getGridDataForDocTypeButton();
        
        gd_DocType.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_textDocCategory, 140);
        m_docTypeLovPopupButton.getPopupButton().setLayoutData(gd_DocType);
        m_docTypeLovPopupButton.setIcon(m_LovPopupButtonImage);
        
        docTypeDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(final PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        public void run()
                        {
                            m_docTypeLovPopupButton.setText(propertychangeevent.getNewValue().toString());
                            docTypeDialog.setLovPopupButton((m_docTypeLovPopupButton));
                            publishDocCategory_Type();
                        }
                    });
                }
            }
        });
        
    }
    
    protected GridData getGridDataForDocTypeButton()
    {
        return new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
    }
    
    private void addFocusListenerOnDocCategoryText(final Text textDocCategory)
    {
        m_textDocCategory.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent focusEvent)
            {
                if (null != m_textDocCategory.getText())
                {
                    String docCategoryValue = m_textDocCategory.getText();
                    int docCategoryValueLength = docCategoryValue.length();
                    if (docCategoryValueLength > 0 && docCategoryValueLength < 3)
                    {
                        m_textDocCategory.setText("");
                        
                    }
                }
            }
            
            @Override
            public void focusGained(FocusEvent focusEvent)
            {
                
            }
        });
    }
    
    private void addModifyListenerOnDocCategoryText(final Text textDocCategory)
    {
        m_textDocCategory.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                if (isModifiedByLOV)
                {
                    isModifiedByLOV = false;
                    return;
                }
                
                String value = m_textDocCategory.getText();
                if (null != value && value.length() > 0)
                {
                    if (value.indexOf('-') == HIPHEN_POSITION && value.length() > HIPHEN_POSITION)
                    {
                        isModifiedByLOV = true;
                        setTextForDocCategoryAndDocType(m_textDocCategory.getText());
                    }
                    else if (value.length() > 3)
                    {
                        
                        toggleDocTypeButton(false);
                        clear();
                    }
                    else
                    {
                        if (value.length() <= 3)
                        {
                            if (!value.equals(m_textDocCategory.getText().toUpperCase()))
                            {
                                m_textDocCategory.setText(m_textDocCategory.getText().toUpperCase());
                            }
                            m_textDocCategory.setSelection(m_textDocCategory.getText().length());
                            toggleDocTypeButton(false);
                            resetDocType();
                        }
                    }
                }
                else
                {
                    toggleDocTypeButton(false);
                    resetDocType();
                }
                moveDocTypeButton();
            }
        });
    }
    
    protected void toggleDocTypeButton(boolean flag)
    {
        if (null != m_BtnDocType)
        {
            m_BtnDocType.setEnabled(flag);
            
        }
        
    }
    
    protected void resetDocType()
    {
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        publishDocCategory_Type();
    }
    
    protected void clear()
    {
        if (null != this.m_textDocCategory)
            this.m_textDocCategory.setText("");
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        publishDocCategory_Type();
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_textDocCategory.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)));
        m_BtnDocType.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.DOCUMENTS_TYPE)));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        PropertyMapHelper.setString(NationalOilwell.DOCUMENTS_CATEGORY, m_textDocCategory.getText(), propMap);
        PropertyMapHelper.setString(NationalOilwell.DOCUMENTS_TYPE, m_BtnDocType.getText(), propMap);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    public boolean reset()
    {
        m_textDocCategory.setText("");
        
        return true;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        // TODO Auto-generated method stub
        
    }
    
    /** The m_registry. */
    protected Registry m_registry = null;
    
    /** The doc type dialog. */
    NIDLovPopupDialog docTypeDialog;
    
    protected Text m_textDocCategory;
    
    /** The m_ btn doc category. */
    protected Button m_BtnDocCategory;
    
    /** The m_ lov popup button image. */
    private Image m_LovPopupButtonImage;
    
    /** The is modified by lov. */
    private boolean isModifiedByLOV = false;
    
    /** The m_doc category popup button. */
    protected SWTLovPopupButton m_docCategoryPopupButton;
    
    /** The m_doc type lov popup button. */
    protected SWTLovPopupButton m_docTypeLovPopupButton;
    
    /** The Constant HIPHEN_POSITION. */
    private static final int HIPHEN_POSITION = 4;
    
    /** The m_ btn doc type. */
    protected Button m_BtnDocType;
    
    private static final Integer HORIZONTAL_INDENT_BTN_DOC_CATEGORY = 5;
    
    protected Composite subComposite;
    
}
