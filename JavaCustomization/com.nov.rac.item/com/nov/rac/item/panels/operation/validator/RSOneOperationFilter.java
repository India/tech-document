package com.nov.rac.item.panels.operation.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.NationalOilwell;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdentifier;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class RSOneOperationFilter extends AbstractOperationFilter
{
    public String[] filter(TCComponent selectedItem, String[] operations) throws TCException
    {

    	TCComponent targetItem = selectedItem;
        String[] finalOperations = operations;
        
        TCComponent masterForm = null;
        
        if (targetItem != null && targetItem instanceof TCComponentItemRevision)
        {
           finalOperations = GenericOperationFilter.filterCreateRDDOperation(selectedItem, operations);
        }
        
        if (targetItem != null && targetItem instanceof TCComponentItem)
        {
            masterForm = targetItem.getRelatedComponent("IMAN_master_form");
        }
        else if (targetItem != null && targetItem instanceof TCComponentItemRevision)
        {
            TCComponentItemRevision targetItemRev = (TCComponentItemRevision) targetItem;
            masterForm = targetItemRev.getItem().getRelatedComponent("IMAN_master_form");
        }
        // Remove Assign Oracle Id to Legacy Part if 1. Selected Item is not
        // legacy part
        // or 2. Selected Item is a Legacy Part With Oracle Id
        if (masterForm != null && !isLegacyPart(masterForm))
        {
            finalOperations = removeOperationFromList(operations,
                    NationalOilwell.OPERATION_ASSIGN_ORACLEID_TO_LEGACY_PART);
        }
        else if (isLegacyPartWithOracleId(targetItem, masterForm))
        {
            finalOperations = removeOperationFromList(operations,
                    NationalOilwell.OPERATION_ASSIGN_ORACLEID_TO_LEGACY_PART);
        }
        
        // AddDashNUmber only if selected object has oracle id
        if (!hasOracleId(targetItem))
        {
            finalOperations = removeOperationFromList(operations, NationalOilwell.OPERATION_ADD_DASH_NUMBERS);
        }
        
        return finalOperations;
    }
    
    private boolean hasOracleId(TCComponent targetItem)
    {
        // TODO Auto-generated method stub
        if (targetItem != null
                && (targetItem instanceof TCComponentItem || targetItem instanceof TCComponentItemRevision))
        {
            try
            {
                TCComponentIdentifier altid_tag = (TCComponentIdentifier) targetItem.getRelatedComponent("altid_list");
                if (altid_tag != null)
                {
                    return true;
                }
            }
            catch (TCException ex)
            {
                ex.printStackTrace();
            }
        }
        return false;
    }
    
    private boolean isLegacyPart(TCComponent masterForm)
    {
        try
        {
            if (masterForm != null)
            {
                String baseNumber = masterForm.getProperty("Base_Number");
                if (baseNumber != null && !baseNumber.equals(""))
                {
                    // this is a legacy part
                    return true;
                }
            }
        }
        catch (TCException te)
        {
            te.printStackTrace();
        }
        
        return false;
    }
    
    private boolean isLegacyPartWithOracleId(TCComponent targetItem, TCComponent masterForm)
    {
        if (isLegacyPart(masterForm))
        {
            if (hasOracleId(targetItem))
            {
                return true;
            }
        }
        return false;
    }
    
    private String[] removeOperationFromList(String[] operations, String operationName)
    {
        String[] finalOperations = operations;
        List<String> list = new ArrayList<String>(Arrays.asList(operations));
        list.remove(operationName);
        finalOperations = list.toArray(new String[0]);
        return finalOperations;
    }
    
}
