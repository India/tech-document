package com.nov.rac.item.panels.operation.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.NationalOilwell;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class DownholeOperationFilter extends AbstractOperationFilter 
{

	@Override
    public String[] filter(TCComponent selectedItem,String[] operations) throws TCException
    {
		TCComponentItemRevision itemRev = null;
		String[] finalOperations = operations;
		
        if (null != selectedItem && selectedItem instanceof TCComponentItem)
        {
        	itemRev = ((TCComponentItem) selectedItem).getLatestItemRevision();
        }
        
        else if (selectedItem instanceof TCComponentItemRevision)
        {
        	itemRev = (TCComponentItemRevision) selectedItem;
        }
        if (null != itemRev)
        {    
           AIFComponentContext[]  secondaryObjects =  itemRev.getRelated(NationalOilwell.RELATED_DEFINING_DOCUMENT);          
                          
           if((null != secondaryObjects && secondaryObjects.length >0 ) || isRevReleased(itemRev) )
           {
               List<String> list = new ArrayList<String>(Arrays.asList(operations));
               list.remove(NationalOilwell.CreateRDDOperation);
               finalOperations = list.toArray(new String[0]);
               return finalOperations;
           } 
           
       }
       return finalOperations;
    }

   
   private boolean isRevReleased(TCComponentItemRevision itemRev)
   {
		boolean isReleased =false;
		TCComponent[] relStatusList;
		try 
		{
			relStatusList = ( itemRev.getTCProperty("release_status_list").getReferenceValueArray() );
			
			for( int index = 0; index < relStatusList.length; ++index )
			{
				if( relStatusList[index].getProperty("name").equalsIgnoreCase("Released") )
				{
					isReleased = true;
					break;
				}
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}

		return isReleased;
	}
}
