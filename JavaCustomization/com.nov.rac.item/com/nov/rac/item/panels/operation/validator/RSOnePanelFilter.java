package com.nov.rac.item.panels.operation.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.item.helpers.PartFamilySearchProvider;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class RSOnePanelFilter extends AbstractOperationFilter
{
    
    @Override
    public String[] filter(TCComponent selectedItem, String[] panelNames) throws TCException
    {
        TCComponent targetItem = selectedItem;
        
        String[] finalPanelNames = panelNames;
        
        INOVResultSet result = null;
        
        if (targetItem != null)
        {
            PartFamilySearchProvider searchService = new PartFamilySearchProvider();
            result = searchService.executeQuery(searchService);
            
            if (result.getRows() > 0)
            {
                finalPanelNames = addpanel(finalPanelNames);
            }
        }
        
        return finalPanelNames;
    }
    
/*    private INOVResultSet executeQuery()
    {
        INOVSearchProvider2 m_searchService = new PartFamilySearchProvider();
        INOVResultSet resultSet = null;
        
        if (m_searchService != null)
        {
            if ((m_searchService instanceof INOVSearchProvider2)
                    && (((INOVSearchProvider2) m_searchService).validateInput() == true))
            {
                
                INOVSearchResult searchResult;
                
                try
                {
                    searchResult = NOVSearchExecuteHelperUtils.execute(m_searchService, INOVSearchProvider2.LOAD_ALL);
                    resultSet = searchResult.getResultSet();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            
        }
        return resultSet;
    }*/
    
    private String[] addpanel(String[] panelNames)
    {
    	List<String> panelList = new ArrayList<String>(Arrays.asList(panelNames));
        
        panelList.add("CloneADefiningPartPanel");
        
        String[] finalPanelNames = panelList.toArray(new String[panelList.size()]);
        
        return finalPanelNames;
    }
    
}
