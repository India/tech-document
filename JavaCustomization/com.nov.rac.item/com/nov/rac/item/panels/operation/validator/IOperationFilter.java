package com.nov.rac.item.panels.operation.validator;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;


public interface IOperationFilter
{
    public String[] filter(TCComponent selectedItem,String[] operations) throws TCException;
}
