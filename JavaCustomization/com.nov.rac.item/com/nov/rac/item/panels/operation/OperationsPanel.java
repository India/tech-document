package com.nov.rac.item.panels.operation;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.panels.operation.validator.OperationFilterHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.fieldassist.NovAutoCompleteField;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsPanel.
 */
public class OperationsPanel extends AbstractUIPanel implements IPublisher, ISubscriber
{
    /** The Constant OPERATIONS_SELECTOR. */
    private static final OperationsSelector OPERATIONS_SELECTOR;
    static
    {
        OPERATIONS_SELECTOR = new OperationsSelector();
    }
    
    /**
     * The Class OperationsSelector.
     */
    private static class OperationsSelector
    {
        
        /**
         * Gets the registry.
         * 
         * @return the registry
         */
        protected Registry getRegistry()
        {
            return Registry.getRegistry("com.nov.rac.item.panels.operation.operation");
        }
        
        /**
         * Instantiates a new operations selector.
         */
        public OperationsSelector()
        {
        }
        
        /**
         * Creates the key string.
         * 
         * @param groupName
         *            the group name
         * @param selectedItemName
         *            the selected item name
         * @param constantString
         *            the constant string
         * @return the string
         */
        private String createKeyString(String groupName, String selectedItemName, String constantString)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (null != groupName)
            {
                stringBuilder.append(groupName);
                stringBuilder.append(NationalOilwell.DOTSEPARATOR);
            }
            if (null != selectedItemName)
            {
                stringBuilder.append(selectedItemName);
                stringBuilder.append(NationalOilwell.DOTSEPARATOR);
            }
            stringBuilder.append(constantString);
            return stringBuilder.toString();
        }
        
        /**
         * Gets the selected item name.
         * 
         * @return the selected item name
         */
        private String getSelectedItemName()
        {
            TCComponent targetItem = NewItemCreationHelper.getTargetComponent();
            String selectedItem = null;
            // if nothing is selected
            if (targetItem != null)
            {
                selectedItem = targetItem.getType();
            }
            return selectedItem;
        }
        
        /**
         * Gets the operations.
         * 
         * @return the operations
         */
        protected String[] getOperations()
        {
            String[] operations = null;
            String selectedItem = getSelectedItemName();
            
            // read the operations for logged in group and for the selected
            // Item
            String currentGroupName = NewItemCreationHelper.getCurrentGroup();
            String registryKey = createKeyString(currentGroupName, selectedItem, NationalOilwell.OPERATIONS);
            Registry l_registry = getRegistry();
            operations = l_registry.getStringArray(registryKey, NationalOilwell.COMMASEPARATOR, null);
            // if no operations list found for logged in group
            // read the operations list for current business unit and for the
            // selected item
            if (operations == null)
            {
                String businessGroupName = NewItemCreationHelper.getBusinessGroup();
                operations = l_registry.getStringArray(
                        createKeyString(businessGroupName, selectedItem, NationalOilwell.OPERATIONS),
                        NationalOilwell.COMMASEPARATOR, null);
                
                // if no operations list found for current business unit
                // read the default operations list
                if (operations == null)
                {
                    operations = l_registry.getStringArray(NationalOilwell.OPERATIONS, NationalOilwell.COMMASEPARATOR,
                            null);
                    
                }
            }
            TCComponent targetItem = NewItemCreationHelper.getTargetComponent();
            operations = OperationFilterHelper.getFinalListOfOperation(targetItem, operations);
            return operations;
        }
        
        /**
         * Gets the operation display name.
         * 
         * @param operationActualName
         *            the operation actual name
         * @return the operation display name
         */
        /*
         * public String getOperationDisplayName(String operationActualName) {
         * Registry l_registry = getRegistry(); return
         * l_registry.getString(operationActualName +
         * NationalOilwell.DOTSEPARATOR + NationalOilwell.NAME); }
         */
        
        public String getOperationDisplayName(String operationActualName)
        {
            Registry l_registry = getRegistry();
            String operationNameContext = operationActualName + ".Name";
            String operationDisplayName[] = NewItemCreationHelper.getConfiguration(operationNameContext, l_registry);
            
            return operationDisplayName[0];
        }
    }
    
    /**
     * Instantiates a new operations panel.
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public OperationsPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        registerSubscriber();
        m_registry = getRegistry();
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.OPERATION_CHANGE_EVENT, this);
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.operation.operation");
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        Composite operationsPanel = getComposite();
        operationsPanel.setLayout(new GridLayout(5, /* true */false));
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, /* true */false, false, 1, 1);
        operationsPanel.setLayoutData(gd_panel);
        GridData gd_label = new GridData(SWT.RIGHT, SWT.CENTER, false, /* false */true, 1, 1);
        m_oparationsLabel = new Label(operationsPanel, SWT.CENTER);
        m_oparationsLabel.setText(m_registry.getString("Operation.Label"));
        m_oparationsLabel.setLayoutData(gd_label);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, /* true */false, true, 3, 1);
        m_combo = new Combo(operationsPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
        m_combo.setVisibleItemCount(NationalOilwell.COMBO_DEFAULT_VISIBLE_ITEMS);// TCDECREL-6755
        gridData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_combo, NationalOilwell.COMBO_FIELD_SIZE);
        m_combo.setLayoutData(gd_combo);
        
        // m_combo.addSelectionListener(new SelectionListener()
        // {
        //
        // @Override
        // public void widgetSelected(SelectionEvent arg0)
        // {
        // System.out.println("****** Inside selectionListener ***** widgetSelected");
        // operationChanged();
        //
        // }
        //
        // @Override
        // public void widgetDefaultSelected(SelectionEvent arg0)
        // {
        // System.out.println("****** Inside selectionListener ***** widgetDefaultSelected");
        // // TODO Auto-generated method stub
        //
        // }
        // });
        m_combo.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent e)
            {
                List<String> comboValues = Arrays.asList(m_combo.getItems());
                if (comboValues.contains(m_combo.getText()))
                {
                    operationChanged();
                }
                
            }
        });
        
        initializeComboValues();
        // TCDECREL-6370 disable right mouse click
        disableRightClick();
        
        // Layout Framework commented
        // new Label(operationsPanel, SWT.NONE);
        
        return true;
    }
    
    /**
     * Initialize combo values.
     * 
     * @throws TCException
     *             the tC exception
     */
    protected void initializeComboValues() throws TCException
    {
        String[] operations = OPERATIONS_SELECTOR.getOperations();
        if (operations != null)
        {
            String[] operationDisplayNames = new String[operations.length];
            for (int i = 0; i < operations.length; i++)
            {
                operationDisplayNames[i] = OPERATIONS_SELECTOR.getOperationDisplayName(operations[i]);
                m_combo.setData(operationDisplayNames[i], operations[i]);
            }
            // m_combo.setItems(operationDisplayNames);
            new NovAutoCompleteField(m_combo, operationDisplayNames);
            // UIHelper.setAutoComboArray(m_combo, operationDisplayNames);
        }
        else
        {
            throw new TCException(NationalOilwell.NOOPERATION);
        }
        
    }
    
    private void operationChanged()
    {
        if (!selectedOperation.equals(m_combo.getText()))
        {
            IController theController = ControllerFactory.getInstance().getDefaultController();
            // .getController(""+getComposite().getShell().hashCode());
            String actualName = (String) m_combo.getData(m_combo.getText());
            NewItemCreationHelper.setSelectedOperation(actualName);
            theController.publish(new PublishEvent(this, NationalOilwell.OPERATION, actualName, ""));
            selectedOperation = m_combo.getText();
        }
    }
    
    private void disableRightClick()
    {
        NewItemCreationHelper.disableRightClick(m_combo);
    }
    
    /**
     * Widget selected_ operations combo.
     * 
     * @param e
     *            the e
     */
    public void widgetSelected_OperationsCombo(ModifyEvent e)
    {
        operationChanged();
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#load(com.nov.rac.propertymap.IPropertyMap)
     * @Override public boolean load(IPropertyMap propMap) throws TCException {
     * return true; } (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#save(com.nov.rac.propertymap.IPropertyMap)
     * @Override public boolean save(IPropertyMap propMap) throws TCException {
     * return true; } (non-Javadoc)
     * @see
     * com.nov.rac.ui.ILoadSave#validate(com.nov.rac.propertymap.IPropertyMap)
     * @Override public ErrorStack validate(IPropertyMap propMap) throws
     * TCException { return null; }
     */
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#reload()
     */
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.framework.communication.IPublisher#getData()
     */
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String propertyName = event.getPropertyName();
        
        if (propertyName.equals(NationalOilwell.OPERATION_CHANGE_EVENT))
        {
            String operationName = (String) event.getNewValue();
            String operationDisplayName = OPERATIONS_SELECTOR.getOperationDisplayName(operationName);
            m_combo.setData(operationDisplayName, operationName);
            m_combo.setItem(0, operationDisplayName);
        }
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    private void unregisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(NationalOilwell.OPERATION_CHANGE_EVENT, this);
    }
    
    private Label m_oparationsLabel;
    protected Combo m_combo;
    protected Registry m_registry;
    private String selectedOperation = "";
    
}
