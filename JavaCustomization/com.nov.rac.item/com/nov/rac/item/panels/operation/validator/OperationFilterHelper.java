package com.nov.rac.item.panels.operation.validator;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class OperationFilterHelper 
{
  
    public  static  String[] getFinalListOfOperation(TCComponent selectedItem,String[] operations) 
    {
        return  filterOperation(selectedItem,operations);
    }
    
    private static  String[] filterOperation(TCComponent selectedItem,String[] operations )
    {
        Registry registry = Registry.getRegistry("com.nov.rac.item.panels.operation.validator.validator");
        IOperationFilter l_operationValidate = null;
        String[] finalOperations = operations;
       
        if(null != selectedItem)
        {
            String selectedItemType =selectedItem.getType();
            String operationValidatorContext = selectedItemType + ".OPERATION_FILTER";
            String validatorName[] = NewItemCreationHelper.getConfiguration(operationValidatorContext, registry);
            String requiredValidatorName = null;
            
            if(validatorName != null && validatorName.length > 0)
            {
                requiredValidatorName = validatorName[0];
                try
                {
                    l_operationValidate = (IOperationFilter) Instancer.newInstanceEx(requiredValidatorName);
                    if(null != l_operationValidate)
                    {
                        finalOperations =  l_operationValidate.filter(selectedItem, operations);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        return finalOperations;
    }
   
}
