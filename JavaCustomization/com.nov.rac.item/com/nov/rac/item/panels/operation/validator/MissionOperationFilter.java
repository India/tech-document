package com.nov.rac.item.panels.operation.validator;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class MissionOperationFilter extends AbstractOperationFilter
{

   @Override
   public String[] filter(TCComponent selectedItem,String[] operations) throws TCException
   {
	   String[] finalOperations = GenericOperationFilter.filterCreateRDDOperation(selectedItem, operations);
           
       return finalOperations;
   }

}
