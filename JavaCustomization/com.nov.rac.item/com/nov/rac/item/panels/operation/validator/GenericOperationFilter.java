package com.nov.rac.item.panels.operation.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.NationalOilwell;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class GenericOperationFilter 
{
	public GenericOperationFilter()
	{
		
	}
	
	public static String[] filterCreateRDDOperation(TCComponent selectedItem,String[] operations) throws TCException
	   {
	       TCComponent targetItem = selectedItem;
	       String[] finalOperations = operations;
	       
	       // if nothing is selected
	       if (targetItem != null && targetItem instanceof TCComponentItemRevision)
	       {
	           TCComponentItemRevision itemRev = (TCComponentItemRevision) targetItem;           
	           AIFComponentContext[]  secondaryObjects =  itemRev.getRelated(NationalOilwell.RELATED_DEFINING_DOCUMENT);          
	                          
	           if((null != secondaryObjects && secondaryObjects.length >0 ) || isRevReleased(itemRev) )
	           {
	               List<String> list = new ArrayList<String>(Arrays.asList(operations));
	               list.remove(NationalOilwell.CreateRDDOperation);
	               finalOperations = list.toArray(new String[0]);
	               return finalOperations;
	           } 
	       }
	       return finalOperations;
	   }
	
	private static boolean isRevReleased(TCComponentItemRevision itemRev){
		
		boolean isReleased =false;
		TCComponent[] relStatusList;
		try 
		{
			relStatusList = ( itemRev.getTCProperty("release_status_list").getReferenceValueArray() );
			
			for( int index = 0; index < relStatusList.length; ++index )
			{
				if( relStatusList[index].getProperty("name").equalsIgnoreCase("Released") )
				{
					isReleased = true;
					break;
				}
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}

		return isReleased;
	}
}
