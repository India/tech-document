package com.nov.rac.item.panels.operation;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.teamcenter.rac.kernel.TCException;

public class DualItemOperationsPanel extends OperationsPanel 
{

	public DualItemOperationsPanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		if(LegacyItemCreateHelper.isDualItemCreationGroup())
		{
			final Composite l_composite = getComposite();
			l_composite.setLayout(new GridLayout(5, true));
	
			GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
			l_composite.setLayoutData(gd_panel);
			
			createRadioButtonRow(l_composite);
		}
		
		boolean returnValue = super.createUI();
		
		return returnValue;
	}
	
	private void createRadioButtonRow(Composite l_composite)
    {
		m_btnNonRSOne = new Button(l_composite, SWT.RADIO);
        m_btnNonRSOne.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
        m_btnNonRSOne.setText(m_registry.getString("btnNonRSOne.NAME",
				"Key Not Found"));
        
        addListenerForNonRSOneItem();
        
        new Label(l_composite, SWT.NONE);
        
        m_btnRSOne = new Button(l_composite, SWT.RADIO);
        m_btnRSOne.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
        m_btnRSOne.setText(m_registry.getString("btnRSOne.NAME",
				"Key Not Found"));
        
        m_btnRSOne.setSelection(true);
        
        addListenerForRSOne();
    }
	
	private void addListenerForNonRSOneItem()
	{
		m_btnNonRSOne.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent selectionEvent)
			{
				m_combo.setEnabled(false);
				
				IController controller = ControllerFactory.getInstance().getDefaultController();
                
                PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.MSG_CREATE_NON_RSONE_ITEM_EVENT, null, null);
                
                controller.publish(event);
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent selectionEvent)
			{

			}
		});
	}
	
	private void addListenerForRSOne()
	{
		m_btnRSOne.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent selectionEvent)
			{
				m_combo.setEnabled(true);
				
				IController controller = ControllerFactory.getInstance().getDefaultController();
                
                PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.MSG_CREATE_RSONE_ITEM_EVENT, null, null);
                
                controller.publish(event);
                
                //m_btnNonRSOne.dispose();
                //m_btnRSOne.dispose();
                
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent selectionEvent)
			{

			}
		});
	}
	
	private IPublisher getPublisher()
    {
        return this;
    }
	
	private Button m_btnNonRSOne;
	private Button m_btnRSOne;

}
