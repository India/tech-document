package com.nov.rac.item.panels.operation.validator;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class PanelListFilterHelper 
{
  
    public  static  String[] getFinalListOfPanels(TCComponent selectedItem,String[] panelNames) 
    {
        return  filterPanel(selectedItem,panelNames);
    }
    
    private static  String[] filterPanel(TCComponent selectedItem,String[] panelNames)
    {
        Registry registry = Registry.getRegistry("com.nov.rac.item.panels.operation.validator.validator");
        IOperationFilter l_panelValidate = null;
        String l_selectedOperation = NewItemCreationHelper.getSelectedOperation();
        String[] finalPanels = panelNames;
       
        if(null != selectedItem)
        {
            String selectedItemType =selectedItem.getType();
            String panelValidatorContext = l_selectedOperation + "." + selectedItemType + ".PANEL_LIST_FILTER";
            String validatorName[] = NewItemCreationHelper.getConfiguration(panelValidatorContext, registry);
            String requiredValidatorName = null;
            
            if(validatorName != null && validatorName.length > 0)
            {
                requiredValidatorName = validatorName[0];
                try
                {
                	l_panelValidate = (IOperationFilter) Instancer.newInstanceEx(requiredValidatorName);
                	
                    if(null != l_panelValidate)
                    {
                    	finalPanels =  l_panelValidate.filter(selectedItem, panelNames);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        return finalPanels;
    }
   
}
