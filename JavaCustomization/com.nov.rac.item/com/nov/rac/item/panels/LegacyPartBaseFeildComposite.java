package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LegacyPartBaseFeildComposite extends RSOneSimpleEnggNonEnggTypePanel
{
    public LegacyPartBaseFeildComposite(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        // TODO Auto-generated method stub
        boolean returnValue = super.createUI();
        
        Composite l_composite = getComposite();
        
        createEmptyPanel(l_composite);
        
        m_baseNameLabel = new Label(l_composite, SWT.NONE);
        m_baseNameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 3, 1));
        m_baseNameLabel.setText(m_registry.getString("LegacyPartBaseFeildComposite.Base"));
        
        Composite l_rightPanel = new Composite(l_composite, SWT.NONE);
        l_rightPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
        l_rightPanel.setLayout(new GridLayout(6, false));
        
        createRightPanel(l_rightPanel);
        UIHelper.makeMandatory(m_baseNameText);
        UIHelper.makeMandatory(m_revisionText);
        
        return returnValue;
    }
    
    private void createEmptyPanel(Composite l_composite)
    {
        Composite emptyComposite = new Composite(l_composite, SWT.None);
        emptyComposite.setLayout(new GridLayout(1, true));
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
        emptyComposite.setLayoutData(gridData);
    }
    
    private void createRightPanel(Composite b_rightPanel)
    {
        
        m_baseNameText = new Text(b_rightPanel, SWT.BORDER);
        m_baseNameText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1));
        
        m_revisionLabel = new Label(b_rightPanel, SWT.NONE);
        m_revisionLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        m_revisionLabel.setText(m_registry.getString("LegacyPartBaseFeildComposite.Revision"));
        
        m_revisionText = new Text(b_rightPanel, SWT.BORDER);
        m_revisionText.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        
    }
    
    /** The m_name label. */
    private Label m_baseNameLabel;
    
    /** The m_description label. */
    private Label m_revisionLabel;
    
    /** The m_comboName ComboBox. */
    public Text m_baseNameText;
    
    /** The m_description Text. */
    public Text m_revisionText;
    
    /** The m_registry. */
    protected Registry m_registry;
}
