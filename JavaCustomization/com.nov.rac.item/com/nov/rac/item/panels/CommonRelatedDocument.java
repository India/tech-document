package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class CommonRelatedDocument extends NewDocumentInfoPanel
{
    
    public CommonRelatedDocument(Composite parent, int style)
    {
        super(parent, style);
        
    }
    
    private void createSelectedItemRow(Composite relatedDocPanel)
    {
        GridData l_gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        relatedDocPanel.setLayoutData(l_gd_panel);
        relatedDocPanel.setLayout(new GridLayout(5, true));
        Label l_itemIdLabel;
        l_itemIdLabel = new Label(relatedDocPanel, SWT.NONE);
        l_itemIdLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        l_itemIdLabel.setText(m_appReg.getString("RelatedDocument.SelectedItem"));
        
        m_selecteditemText = new Text(relatedDocPanel, SWT.BORDER);
        m_selecteditemText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        m_selecteditemText.setEditable(false);
        
        // TCDECREL-6756 commented
//        UIHelper.makeMandatory(m_selecteditemText);
        
        new Label(relatedDocPanel, SWT.NONE);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_relatedDocPanel = getComposite();
        GridData l_gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_relatedDocPanel.setLayoutData(l_gd_panel);
        l_relatedDocPanel.setLayout(new GridLayout(1, true));
        createSelectedItemRow(l_relatedDocPanel);
        super.createUI();
        m_itemIdText.setEnabled(false);
        m_assignButton.setVisible(false);
        m_revisionText.setText("");
        m_revisionText.setEnabled(true);
        m_revisionText.setEditable(true);
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        m_selecteditemText.setText(PropertyMapHelper.handleNull(propMap.getString("item_id")));
        return true;
    }
    
    // TCDECREL-6756 Start Red asterisk issue
    @Override
    public boolean createUIPost() throws TCException
    {
        super.createUIPost();
        UIHelper.makeMandatory(m_selecteditemText);
		return false;
    }
    // TCDECREL-6756 End
      
    private Text m_selecteditemText;
}
