package com.nov.rac.item.panels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.RSOneDashTableDialog;
import com.nov.rac.item.helpers.CopyPropertyHelper;
import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.item.helpers.RSOneDashPropertyMap;
import com.nov.rac.item.helpers.RSOneOperationTypePanelHelper;
import com.nov.rac.item.panels.rsone.RSOneCreateRightPartInfoPanel;
import com.nov.rac.item.panels.rsone.RSOneDashNumberListPanel;
import com.nov.rac.item.panels.rsone.RSOneDashNumberPanel;
import com.nov.rac.item.panels.rsone.RSOnePurchasePartLeftPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneSimplePartInfoPanel extends AbstractUIPanel implements ILoadSave, ISubscriber, IPublisher
{
    private Composite partInfoPanel;
    protected RSOnePartInfoRightPanel m_RSOnePartInfoRightPanel;
    protected Map<String, IPropertyMap> dashPropertyMap = new HashMap<String, IPropertyMap>();
    private IPropertyMap initialPropertyMap = null;
    protected RSOneDashNumberPanel l_leftPanel;
    protected String currentSelectedDash = null;
    protected String[] dashNumbers = null;
    private String selectedType = null;
    private Registry m_registry = null;

	public RSOneSimplePartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        selectedType = RSOneOperationTypePanelHelper.getPartType();
    }
	
	 
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    
    @Override
    public boolean createUI() throws TCException
    {
        partInfoPanel = getComposite();
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        partInfoPanel.setLayoutData(gd_panel);
        partInfoPanel.setLayout(new GridLayout(5, true));
        
        createLeftPanel(partInfoPanel);
        
        createRightPanel(partInfoPanel);
        
        createInitialPropertyMap();
        
        IPropertyMap propertyMap = new SOAPropertyMap(NationalOilwell.PART_BO_TYPE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(propertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        m_RSOnePartInfoRightPanel.save(propertyMap);
        
        dashPropertyMap.put("001", propertyMap);
        
        currentSelectedDash = "001";
        
        registerSubscriber();
        
        return true;
    }

    /**
     * 
     */
    private void createInitialPropertyMap()
    {
        initialPropertyMap = new SOAPropertyMap(NationalOilwell.PART_BO_TYPE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(initialPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
    }
    
    protected void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber(NationalOilwell.MSG_DASH_NUMBER_MODIFY_EVENT, this);
        controller.registerSubscriber(NationalOilwell.MODIFY_PANEL_DONE_BUTTON_SELECTION, this);
        controller.registerSubscriber(NationalOilwell.MSG_DASH_NUMBER_TABLE_EVENT, this);
        controller.registerSubscriber(NationalOilwell.DASH_PROPERTYMAP_MODIFY_EVENT, this);
        controller.registerSubscriber(NationalOilwell.RSONE_COPY_PROPERTY, this);
        controller.registerSubscriber(NationalOilwell.RSONE_COPY_ALL_PROPERTY, this);
        controller.registerSubscriber(NationalOilwell.PART_TYPE_CHANGE_EVENT, this);
        controller.registerSubscriber(NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT, this);
    }
    
    protected void createLeftPanel(final Composite partInfoPanel)
    {
    	//To work purchase part reloading for non-engineerin part also : 8090
        if(selectedType != null && !selectedType.equalsIgnoreCase(NationalOilwell.PART) && !selectedType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
        {
            l_leftPanel = new RSOnePurchasePartLeftPanel(partInfoPanel, SWT.NONE);
        }
        else
        {
            l_leftPanel = new RSOneDashNumberPanel(partInfoPanel, SWT.NONE);
        }
        
        try
        {
            l_leftPanel.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    protected void createLeftPanelWithoutButtons(final Composite partInfoPanel)
    {
    	//To work purchase part reloading for non-engineerin part also : 8090
        if(selectedType != null && !selectedType.equalsIgnoreCase(NationalOilwell.PART) && !selectedType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
        {
            l_leftPanel = new RSOnePurchasePartWithListLeftPanel(partInfoPanel, SWT.NONE);
        }
        else
        {
            l_leftPanel = new RSOneDashNumberListPanel(partInfoPanel, SWT.NONE);
        }
        
        try
        {
            l_leftPanel.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    protected void createRightPanel(Composite partInfoPanel) throws TCException
    {
    	//To work purchase part reloading for non-engineerin part also : 8090
        if(selectedType != null && !selectedType.equalsIgnoreCase(NationalOilwell.PART) && !selectedType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
        {
            if(dashNumbers!=null && dashNumbers.length > 1)
            {
                m_RSOnePartInfoRightPanel = new RSOnePartInfoRightPanelWithMfg(partInfoPanel, SWT.NONE);
            }
            else
            {
                m_RSOnePartInfoRightPanel = new RSOnePartInfoRightPanelWithMfgWithoutCheckBox(partInfoPanel, SWT.NONE);
            }
            
        }
        else
        {
            if (dashNumbers != null && dashNumbers.length > 1)
            {
                m_RSOnePartInfoRightPanel = new RSOneCreateRightPartInfoPanel(partInfoPanel, SWT.NONE);
            }
            else
            {
                m_RSOnePartInfoRightPanel = new RSOnePartInfoRightPanelWithoutCheckBox(partInfoPanel, SWT.NONE);
            }
        }
        
        m_RSOnePartInfoRightPanel.createUI();
       
        m_RSOnePartInfoRightPanel.getComposite().getParent().redraw();
  }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        m_RSOnePartInfoRightPanel.load(propMap);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
      //  m_RSOnePartInfoRightPanel.save(propMap);
        IPropertyMap propertyMap = dashPropertyMap.get(currentSelectedDash);
        m_RSOnePartInfoRightPanel.save(propertyMap);
        String oracleId = NOVRSOneItemIDHelper.getOracleId();
        for(String dashNumber : dashPropertyMap.keySet())
        {
            dashPropertyMap.get(dashNumber).setString("item_id", oracleId + "-" + dashNumber);
        }
        
     //   propertyMap.setString("item_id", oracleId+"-"+currentSelectedDash);
        dashPropertyMap.put(currentSelectedDash, propertyMap);
        IPropertyMap[] propMapArray = new SOAPropertyMap[dashPropertyMap.size()];
        propMap.setCompoundArray("dashPropertyMap", dashPropertyMap.values().toArray(propMapArray));
        
        //dashMapSaved = true;
        
        return true;
    }
    
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    /*
     * protected Label m_dashLable; protected Registry m_registry; protected
     * Button m_modifyButton; protected Button m_tableButton;
     */
    @Override
    public void update(final PublishEvent event)
    {
        if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.MSG_DASH_NUMBER_MODIFY_EVENT))
        {
            saveCurrentSelectedDash();
            
            m_RSOnePartInfoRightPanel.dispose();
            m_RSOnePartInfoRightPanel = new RSOneModifyPanel(partInfoPanel, SWT.NONE);
            RSOneModifyPanel modifyPanel = (RSOneModifyPanel) m_RSOnePartInfoRightPanel;
            
            try
            {
                m_RSOnePartInfoRightPanel.createUI();
                modifyPanel.loadList(event.getNewValue());
                partInfoPanel.layout();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.MODIFY_PANEL_DONE_BUTTON_SELECTION))
        {
            m_RSOnePartInfoRightPanel.dispose();
            dashNumbers = (String[]) event.getNewValue();
            Vector<String> modifiedDashSet = new Vector<String>(Arrays.asList(dashNumbers)); 
            String[] dashNumberArray = new String[dashPropertyMap.size()];
            
            dashNumberArray = dashPropertyMap.keySet().toArray(dashNumberArray);
            
            Vector<String> dashNumbersInMap = new Vector<String>(Arrays.asList(dashNumberArray)); 
            if(!modifiedDashSet.containsAll(dashNumbersInMap))
            {
            	 for (String dashNumberInMap : dashNumbersInMap)
                 {
            		 if(!modifiedDashSet.contains(dashNumberInMap))
            		 {
            			 dashPropertyMap.remove(dashNumberInMap);
            		 }
                 }
            }
            		
            for (String dashNumber : dashNumbers)
            {
            	
                if (dashPropertyMap.get(dashNumber) == null)
                {
                    IPropertyMap partPropertyMap = new SOAPropertyMap(NationalOilwell.PART_BO_TYPE);
                    CreateInCompoundHelper partCompoundHelper = new CreateInCompoundHelper(partPropertyMap);
                    partCompoundHelper.addCompound("MasterForm", "IMAN_master_form");
                    partCompoundHelper.addCompound("ItemRevision", "revision");
                    
                    partPropertyMap.addAll(initialPropertyMap);
                   
                    dashPropertyMap.put(dashNumber, partPropertyMap);
                }
            }
       
            try
            {
                createRightPanel(partInfoPanel);
                partInfoPanel.layout();
               
                m_RSOnePartInfoRightPanel.load(dashPropertyMap.get(currentSelectedDash));
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            // partInfoPanel.layout();
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.DASH_PROPERTYMAP_MODIFY_EVENT))
        {
            try
            {
            	String oldValue = event.getOldValue().toString();
            	IPropertyMap partPropertyMap = dashPropertyMap.get(oldValue);
            	if(partPropertyMap != null)
            	{
            		m_RSOnePartInfoRightPanel.save(partPropertyMap);
            	}
            	else
            	{
            		m_RSOnePartInfoRightPanel.save(initialPropertyMap);
            	}
                
                dashPropertyMap.put(oldValue, partPropertyMap);
                String newValue = event.getNewValue().toString();
                currentSelectedDash = newValue;
                if (dashPropertyMap.get(newValue) != null)
                {
                    load(dashPropertyMap.get(newValue));
                }
                else
                {
                    dashPropertyMap.put(newValue, initialPropertyMap);
                    load(initialPropertyMap);
                }
                
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.RSONE_COPY_PROPERTY))
        {
            CopyPropertyHelper copyProperty = (CopyPropertyHelper) event.getNewValue();
            String[] chainPropertyName = copyProperty.getPropertyName();
            Object propertyValue = copyProperty.getPropValue();
            String displayName = copyProperty.getDisplayName();
            copyProperty(chainPropertyName, propertyValue, displayName);
        }
        
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.RSONE_COPY_ALL_PROPERTY))
        {
            copyAllProperties();
        }
        
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.MSG_DASH_NUMBER_TABLE_EVENT))
        {
        	saveCurrentSelectedDash();
            
            RSOneDashPropertyMap.setDashPropertyMap(dashPropertyMap);
            
            RSOneDashTableDialog m_dashDialog = new RSOneDashTableDialog(this.getComposite().getShell(), SWT.BORDER
                    | SWT.NO_TRIM);
            m_dashDialog.open();
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT))
        {
            IPropertyMap[] l_propMapArray = (IPropertyMap[]) event.getNewValue();
            
            Set<String> dashNumbers = dashPropertyMap.keySet();
            List<String> sortedList = new ArrayList<String>(dashNumbers);
            Collections.sort(sortedList);
            
            String [] l_dashNumbers = sortedList.toArray(new String[sortedList.size()]);
            for (int inx = 0; inx < l_propMapArray.length; inx++)
            {
                dashPropertyMap.get(l_dashNumbers[inx]).addAll(l_propMapArray[inx]);
            }
            try
            {
                m_RSOnePartInfoRightPanel.load(dashPropertyMap.get(currentSelectedDash));
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }

        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.PART_TYPE_CHANGE_EVENT))
        {
           selectedType = event.getNewValue().toString();
           IPropertyMap dashPanelMap = new SimplePropertyMap();
           int returnCode = 0 ;
           try
            {
        	 //To work purchase part reloading for non-engineerin part also : 8090
               if(selectedType.equalsIgnoreCase("purchased part") || selectedType.equalsIgnoreCase("Non-Engg Purchased Part"))
               {
                   returnCode = getMessage(m_registry.getString("confirmation.msg"),
                           m_registry.getString("retainValues.msg"), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
                   if (returnCode == SWT.YES)
                   {
                        m_RSOnePartInfoRightPanel.save(dashPropertyMap.get(currentSelectedDash));
                    
                   }
               }
               
               else
               {
                   m_RSOnePartInfoRightPanel.save(dashPropertyMap.get(currentSelectedDash));
               }
               
               l_leftPanel.save(dashPanelMap);
               
                m_RSOnePartInfoRightPanel.dispose();
                l_leftPanel.dispose();
                
                createLeftPanel(partInfoPanel);
                createRightPanel(partInfoPanel);
                
                partInfoPanel.layout();
                
                if(selectedType.equalsIgnoreCase("purchased part"))
                {
                    if (returnCode == SWT.NO)
                    {
                        
                        for (String key : dashPropertyMap.keySet())
                        {
                            IPropertyMap propMap = dashPropertyMap.get(key);
                            propMap.addAll(initialPropertyMap);
                            dashPropertyMap.put(key, propMap);
                            
                        }
                    }
                }
               l_leftPanel.load(dashPanelMap); 
               m_RSOnePartInfoRightPanel.load(dashPropertyMap.get(currentSelectedDash));
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }


	private void saveCurrentSelectedDash() 
	{
		IPropertyMap propertyMap = dashPropertyMap.get(currentSelectedDash);
		try 
		{
			m_RSOnePartInfoRightPanel.save(propertyMap);
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		
		dashPropertyMap.put(currentSelectedDash, propertyMap);
	}
    
    private int getMessage(String theText, String theMessage, int style)
    {
        MessageBox dialog = new MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }

	@Override
	public IPropertyMap getData() 
	{
		return null;
	}
        
    private void copyProperty(String[] chainPropertyName, Object propertyValue, String displayName)
    {
    	if(!chainPropertyName[chainPropertyName.length-1].equalsIgnoreCase(NationalOilwell.RSONE_SERIALIZE) &&
    			!chainPropertyName[chainPropertyName.length-1].equalsIgnoreCase(NationalOilwell.RSONE_QAREQUIRED))
    			{
    				getMessage(m_registry.getString("information.msg"), displayName+" "+m_registry.getString("copy.msg")+" "+displayName+" attribute.", SWT.ICON_INFORMATION | SWT.OK);
    			}
        
        Collection<IPropertyMap> mapList = dashPropertyMap.values();
        
        for (IPropertyMap propertyMap : mapList)
        {
        	IPropertyMap propMap = null;
			try 
			{
				propMap = TableUtils.getChainPropertyMap(propertyMap, chainPropertyName);
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}
    		PropertyMapHelper.addPropertyValueToMap(chainPropertyName[chainPropertyName.length-1],propertyValue,propMap);
        }
    }
    private void copyAllProperties()
    {
        Collection<IPropertyMap> mapList = dashPropertyMap.values();
        for (IPropertyMap propertyMap : mapList)
        {
            try
            {
                m_RSOnePartInfoRightPanel.save(propertyMap);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        getMessage(m_registry.getString("success.msg"),
                m_registry.getString("copyAllSuccess.msg"), SWT.ICON_WARNING
                        | SWT.OK);
    }
    
    @Override
    public void dispose()
    {
        m_RSOnePartInfoRightPanel.dispose();
        l_leftPanel.dispose();
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.unregisterSubscriber(NationalOilwell.MSG_DASH_NUMBER_MODIFY_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.MODIFY_PANEL_DONE_BUTTON_SELECTION, this);
        theController.unregisterSubscriber(NationalOilwell.MSG_DASH_NUMBER_TABLE_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.DASH_PROPERTYMAP_MODIFY_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.RSONE_COPY_ALL_PROPERTY, this);
        theController.unregisterSubscriber(NationalOilwell.RSONE_COPY_PROPERTY, this);
        theController.unregisterSubscriber(NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.PART_TYPE_CHANGE_EVENT, this);
        super.dispose();
    }
}