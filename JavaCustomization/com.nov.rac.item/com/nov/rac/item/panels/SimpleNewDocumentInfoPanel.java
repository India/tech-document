package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DocumentHelper;
import com.nov.rac.item.helpers.GenerateItemId;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.InterfaceExceptionStack;
import com.teamcenter.rac.util.Registry;

public class SimpleNewDocumentInfoPanel extends AbstractUIPanel implements ILoadSave, IPublisher, ISubscriber
{
    public SimpleNewDocumentInfoPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        m_appReg = getRegistry();
    }
    
    Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private void createItemIdRow(Composite docInfoPanel)
    {
        Label l_itemIdLabel;
        l_itemIdLabel = new Label(docInfoPanel, SWT.NONE);
        l_itemIdLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_itemIdLabel.setText(m_appReg.getString("NewDocumentInfoPanel.ItemId"));
        
        m_itemIdText = new Text(docInfoPanel, SWT.BORDER);
        m_itemIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
        m_itemIdText.setTextLimit(25);
        UIHelper.convertToUpperCase(m_itemIdText);
        
        m_assignButton = new Button(docInfoPanel, SWT.NONE);
        GridData assignBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        assignBtnGridData.horizontalIndent=SWTUIHelper.convertHorizontalDLUsToPixels(m_assignButton,NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON );
        m_assignButton.setLayoutData(assignBtnGridData);
        m_assignButton.setText(m_appReg.getString("NewDocumentInfoPanel.AssignButton"));
        m_assignButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionevent)
            {
                String l_objectId = GenerateItemId.assignItemId();
                IController l_controller = ControllerFactory.getInstance().getDefaultController();
               // IController l_controller = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
                l_controller.publish(new PublishEvent(getPublisher(), "object_name", l_objectId, null));
                m_itemIdText.setText(l_objectId);
            }
        });
    }
    
    private void createNameRow(Composite docInfoPanel)
    {
        Label l_nameLabel;
        l_nameLabel = new Label(docInfoPanel, SWT.NONE);
        l_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_nameLabel.setText(m_appReg.getString("NewDocumentInfoPanel.Name"));
        
        m_nameText = new Text(docInfoPanel, SWT.BORDER);
        m_nameText.setTextLimit(30);
        m_nameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
        m_nameText.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                UIHelper.convertToUpperCase(m_nameText);
            }
        });
        new Label(docInfoPanel, SWT.NONE);
    }
    
    private void createRevisionRow(Composite docInfoPanel)
    {
        Label l_revisionLabel;
        l_revisionLabel = new Label(docInfoPanel, SWT.NONE);
        l_revisionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_revisionLabel.setText(m_appReg.getString("NewDocumentInfoPanel.Revision"));
        
        m_revisionText = new Text(docInfoPanel, SWT.BORDER);
        m_revisionText.setTextLimit(NationalOilwell.REV_TEXT_LIMIT);
        m_revisionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        m_revisionText.setText("01");
        new Label(docInfoPanel, SWT.NONE);
        new Label(docInfoPanel, SWT.NONE);
        new Label(docInfoPanel, SWT.NONE);
    }
    
    private void createDescription(Composite docInfoPanel)
    {
        Label l_descriptionLabel;
        l_descriptionLabel = new Label(docInfoPanel, SWT.NONE);
        l_descriptionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 3));
        l_descriptionLabel.setText(m_appReg.getString("NewDocumentInfoPanel.Description"));
        
        m_descriptionText = new Text(docInfoPanel, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
        
        //Tushar Start TCDECREL-6822
        m_descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3));
        //Tushar End
        
        m_descriptionText.setTextLimit(240);
        m_descriptionText.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                UIHelper.convertToUpperCase(m_descriptionText);
            }
        });
        new Label(docInfoPanel, SWT.NONE);
        new Label(docInfoPanel, SWT.NONE);
    }
    
    private void setEditable(boolean enable)
    {
//        m_revisionText.setEnabled(enable);
    	m_revisionText.setEditable(enable);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_itemIdText.setText(PropertyMapHelper.handleNull(propMap.getString("item_id").split(":")[0]));
        m_nameText.setText(PropertyMapHelper.handleNull(propMap.getString("object_name")));
        m_descriptionText.setText(PropertyMapHelper.handleNull(propMap.getString("object_desc")));
        
        // item revision properties
        IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            m_revisionText.setText(PropertyMapHelper.handleNull(revisionPropertyMap.getString("item_revision_id")));
        }
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        PropertyMapHelper.setString("item_id", m_itemIdText.getText().trim(), propMap);
        PropertyMapHelper.setString("object_name", m_nameText.getText().trim(), propMap);
        PropertyMapHelper.setString("object_desc", m_descriptionText.getText().trim(), propMap);
        
        // item revision properties
        IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            PropertyMapHelper.setString("item_revision_id", m_revisionText.getText().trim(), revisionPropertyMap);
            PropertyMapHelper.setString("object_name", m_nameText.getText().trim(), revisionPropertyMap);
            propMap.setCompound("revision", revisionPropertyMap);
        }
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        if (!DocumentHelper.hasContent(propMap.getString("item_id")))
        {
            TCException e = new TCException("Item Id is mandatory field");
            stack.addErrors(e);
        }
        if (!DocumentHelper.hasContent(propMap.getString("object_name")))
        {
            TCException e = new TCException("Item Name is mandatory field");
            stack.addErrors(e);
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            InterfaceExceptionStack intStack = stack;
            TCException newExp = new TCException(intStack.getErrorSeverities(), intStack.getErrorCodes(),
                    intStack.getErrorStack());
            throw newExp;
        }
        return true;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_docInfoPanel = getComposite();
        GridData l_gd_panel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        l_docInfoPanel.setLayoutData(l_gd_panel);
        l_docInfoPanel.setLayout(new GridLayout(5, true));
        createItemIdRow(l_docInfoPanel);
        createRevisionRow(l_docInfoPanel);
        createNameRow(l_docInfoPanel);
        createDescription(l_docInfoPanel);
        setEditable(false);
        registerProperties();
        disableRightClick(); //TCDECREl-6370
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    ISubscriber getSubscriber()
    {
        return (ISubscriber) this;
    }
    
    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        //IController l_controller = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        l_controller.registerSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED, getSubscriber());
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.ONAPPLY_OBJECT_CREATED))
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    if (new Boolean(event.getNewValue().toString()))
                    {
                        if (m_itemIdText.isEnabled())
                        {
                            m_itemIdText.setText("");
                        }
                    }
                }
            });
        }
    }
    
    @Override
    public void dispose()
    {
    	IController l_controller = ControllerFactory.getInstance().getDefaultController();
        //IController l_controller = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        l_controller.unregisterSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED, getSubscriber());
        super.dispose();
    }
    
    private void disableRightClick()
    {
    	
    	NewItemCreationHelper.disableRightClick( m_itemIdText );
    	NewItemCreationHelper.disableRightClick( m_nameText );
    	NewItemCreationHelper.disableRightClick( m_descriptionText );
    	NewItemCreationHelper.disableRightClick( m_revisionText );
    }
    
    protected Registry m_appReg;
    protected Text m_itemIdText, m_nameText, m_descriptionText, m_revisionText;
    protected Button m_assignButton;
}
