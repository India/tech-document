package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;


import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyDocDocPanel extends SimpleDocumentCreatePanel
{
    
    public RSOneCopyDocDocPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        
        UIHelper.makeMandatory(m_textDocCategory);
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return super.load(propMap);
    }
}
