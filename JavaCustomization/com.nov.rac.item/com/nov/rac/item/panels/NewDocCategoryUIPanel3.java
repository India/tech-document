/* ============================================================
   
   	File description: 

  	 Filename: NewDocCategoryUIPanel3.java
  	 Module  : <name of the module/folder where this file resides> 

   	<Short Description of the file> 

   	Date         Developers    Description
   	17-08-2012   snehac    Initial creation

   	$HISTORY
 ============================================================ */
package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;


import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

/**
 * The Class NewDocCategoryUIPanel3.
 */
public class NewDocCategoryUIPanel3 extends SimpleNewDocCategoryUIPanel1 implements IPublisher, ISubscriber
{
    
    /**
     * Instantiates a new doc category ui panel3.
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public NewDocCategoryUIPanel3(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        final Composite documentComposite = getComposite();
        documentComposite.setLayout(new GridLayout(1, true));
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        documentComposite.setLayoutData(gd_panel);
        createDocumentHeader(documentComposite);
        createRadioBtnComposite(documentComposite);
        subComposite = SWTUIHelper.createRowComposite(documentComposite, 3, false);
        
        createDocCategoryRow(subComposite);
        createDocTypeRow(subComposite);
        createPullDocRow(subComposite);
        // TCDECREL-6370 disable right mouse click
        disableRightClick();
        
        
        if (m_btnRadioCopyRDD.getSelection())
        {
            super.toggleState(!m_btnRadioCopyRDD.getSelection());
        }
        
        addingListenerOnCopyDoc();
        addingListenerOnCreateNewDoc();
        registerSubscriber();
        
        
        
        
        return true;
        
        
        
        
        
        
//        Composite m_docCategoryComposite = getComposite();
//        GridLayout docCategoryLayout = new GridLayout(1, true);
//        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
//        m_docCategoryComposite.setLayout(docCategoryLayout);
//        m_docCategoryComposite.setLayoutData(gd_panel);
//        // for creating composite having 2 radio buttons
//       
//        
//        super.createUI();
//        createRadioBtnComposite(m_docCategoryComposite);
//        

    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#load(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TCDECREL-7122: Commented null check to update values when table selection is changed. 
        // Previously it was retaining value for the first selected object
        
//    	if(m_propMap == null)
//    	{
    		m_propMap = new SimplePropertyMap();
    		m_propMap.addAll(propMap);
//    	}
    	super.load(m_propMap);
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#save(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        if (m_btnRadioCreateNewRDD.getSelection())
        {
            super.save(propMap);
        }
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.ui.ILoadSave#validate(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#reload()
     */
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    /**
     * adding listener on Radio button Copy Doc.
     */
    private void addingListenerOnCopyDoc()
    {
        
        m_btnRadioCopyRDD.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                widgetsSelectedRBCopyDoc();
                publishCopyDocRBEvent();
            }
        });
        
    }
    
    /**
     * adding listener on Radio Create New Doc.
     */
    private void addingListenerOnCreateNewDoc()
    {
        m_btnRadioCreateNewRDD.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                widgetSelectedRBCreateNewDoc();
                publishCreateNewDocRBEvent();
            }
        });
    }
    
    /**
     * if event occurs on Copy Doc radio button then this function is called.
     */
    private void widgetsSelectedRBCopyDoc()
    {
        if (m_btnRadioCopyRDD.getSelection())
        {
            super.toggleDocTypeButton(!m_btnRadioCopyRDD.getSelection());
            super.toggleState(!m_btnRadioCopyRDD.getSelection());
            super.clear();
            TCComponent selectedComponent = NewItemCreationHelper.getTargetComponent();
            if (null == selectedComponent)
            {
                selectedComponent = NewItemCreationHelper.getSelectedComponent();
            }
            if (null != selectedComponent)
            {
                
                publishDocCopyDoc();
            }
        }
        
    }
    
    /**
     * if event occurs on Create New Doc radio button then this function is
     * called.
     */
    private void widgetSelectedRBCreateNewDoc()
    {
        if (m_btnRadioCreateNewRDD.getSelection())
        {
            super.toggleDocTypeButton(!m_btnRadioCreateNewRDD.getSelection());
            super.toggleState(m_btnRadioCreateNewRDD.getSelection());
            super.clear();
        }
        
    }
    
    /**
     * creating composite and adding radio buttons on it.
     * 
     * @param docCategoryComposite
     *            the doc category composite
     */
    private void createRadioBtnComposite(Composite docCategoryComposite)
    {
        // new Label(docCategoryComposite, SWT.NONE);
        // SWT.BORDER to test the changes
        Composite radioBtnComposite = new Composite(docCategoryComposite, SWT.NONE);
        GridLayout radioComplayout = new GridLayout(2,true);
        radioComplayout.marginHeight = 0;
        radioComplayout.marginWidth = 0;
        radioBtnComposite.setLayout(radioComplayout);
        
        radioBtnComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        radioBtnComposite.setLayout(new GridLayout(2, true));
        
        //TCDECREL-6781
        //radioBtnComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
        radioBtnComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        
        m_btnRadioCopyRDD = new Button(radioBtnComposite, SWT.RADIO);
        m_btnRadioCopyRDD.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        
        m_btnRadioCopyRDD.setText(m_registry.getString("copyRDDRB.NAME", "Key Not Found "));
        m_btnRadioCopyRDD.setSelection(true);
        
        m_btnRadioCreateNewRDD = new Button(radioBtnComposite, SWT.RADIO);
        
        //TCDECREL-6781
        //m_btnRadioCreateNewRDD.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_btnRadioCreateNewRDD.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
        
        m_btnRadioCreateNewRDD.setText(m_registry.getString("newRDDRB.NAME", "Key Not Found "));
        
        //TCDECREL-6781
        new Label(docCategoryComposite, SWT.NONE);
        
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.COPY_DOC))
        {
            try
            {
                if (null != m_propMap)
                {
                    load(m_propMap);
                    
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
    }
    
    private void publishDocCopyDoc()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.COPY_DOC, null, null);
        controller.publish(event);
    }
    
    private void publishCreateNewDocRBEvent()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.CREATE_NEW_RDD, null, null);
        controller.publish(event);
    }
    
    private void publishCopyDocRBEvent()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.COPY_RDD, null, null);
        controller.publish(event);
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.COPY_DOC, this);
    }
    
    @Override
    public void dispose()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.unregisterSubscriber(NationalOilwell.COPY_DOC, this);
        super.dispose();
    }
    
    /** The m_btn radio copy rdd. */
    private Button m_btnRadioCopyRDD;
    
    /** The m_btn radio create new rdd. */
    private Button m_btnRadioCreateNewRDD;
    
    /** The m_registry. */
    private Registry m_registry = null;
    
    private IPropertyMap m_propMap = null;
    
}
