package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.rsone.RSOneCreateRightPartInfoPanel;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOnePartInfoRightPanelWithoutCheckBox extends RSOneCreateRightPartInfoPanel
{

    public RSOnePartInfoRightPanelWithoutCheckBox(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
        
        m_copyAllBtn.setVisible(false);
        m_orgCheckBox.dispose();
        m_nameCheckBox.dispose();
        m_classCheckBox.dispose();
        m_descCheckBox.dispose();
        m_uomCheckBox.dispose();
        m_traceabilityCheckBox.dispose();
        m_sendToClassificationBtn.setEnabled(true);
        
        return returnValue;
    }
    
}
