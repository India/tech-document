package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.item.panels.rsone.RSOneDashNumberListPanel;
import com.teamcenter.rac.kernel.TCException;

public class RSOnePurchasePartWithListLeftPanel extends RSOneDashNumberListPanel 
{

	public RSOnePurchasePartWithListLeftPanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	 @Override
	    public boolean createUI() throws TCException
	    {
	       super.createUI();
	       
	       new Label(l_leftPanel, SWT.NONE);
	       new Label(l_leftPanel, SWT.NONE);
	       new Label(l_leftPanel, SWT.NONE);
	       
	       return true;
	    }

}
