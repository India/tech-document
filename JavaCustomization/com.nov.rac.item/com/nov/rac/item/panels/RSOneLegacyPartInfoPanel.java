package com.nov.rac.item.panels;

import java.util.Vector;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.item.helpers.RSOneUOMLOVHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RSOneLegacyPartInfoPanel extends AbstractUIPanel implements ILoadSave
{
    public RSOneLegacyPartInfoPanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
        m_session = getSession();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    protected static TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_legacyComposite = getComposite();
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1);
        l_legacyComposite.setLayoutData(gd_panel);
        l_legacyComposite.setLayout(new GridLayout(4, true));
        
        createPartNameRow(l_legacyComposite);
        
        createPartBaseRevRow(l_legacyComposite);
        
        createPartUOMRow(l_legacyComposite);
        
        createPartDescriptionRow(l_legacyComposite);
        
        UIHelper.makeMandatory(m_nameCombo, false);
        UIHelper.makeMandatory(m_revText);
        UIHelper.makeMandatory(m_uomCombo);
        UIHelper.makeMandatory(m_baseText);
        
        return true;
    }
    
    private void createPartNameRow(Composite l_legacyComposite)
    {
        m_nameLabel = new Label(l_legacyComposite, SWT.NONE);
        m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_nameLabel.setText(m_registry.getString("Name.NAME"));
        
        Composite l_nameComposite = new Composite(l_legacyComposite, SWT.NONE);
        l_nameComposite.setLayoutData(getSubCompositeLayoutData());
        
        l_nameComposite.setLayout(getSubCompositeLayout());
        
        m_nameCombo = new Combo(l_nameComposite, SWT.NONE);
        m_nameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        // m_nameCombo.setTextLimit(NationalOilwell.NAME_TEXT_LIMIT);
        
        String groupName = m_session.getGroup().toString();
        String[] suggestedValues = RSOneSuggestedByNameLOVHelper.getSuggestedNameValues(m_session,
                "_SuggestedNamesByGroupQuery_", new String[] { m_registry.getString("Group.NAME") },
                new String[] { groupName }, m_registry.getString("Name.NAME"));
        m_nameCombo.setItems(suggestedValues);
        new AutoCompleteField(m_nameCombo, new ComboContentAdapter(), suggestedValues);
        int length = PreferenceHelper.getParsedPrefValueForGroup("NOV_ObjectName_Length_For_Groups",
                TCPreferenceService.TC_preference_site, groupName, "|");
        m_nameCombo.setTextLimit(length);
        
        new Label(l_nameComposite, SWT.NONE);
        
    }
    
    private void createPartBaseRevRow(Composite l_rightComposite)
    {
        m_baseLabel = new Label(l_rightComposite, SWT.NONE);
        GridData l_gdBaseLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        l_gdBaseLabel.horizontalIndent = 0;
        m_baseLabel.setLayoutData(l_gdBaseLabel);
        m_baseLabel.setText(m_registry.getString("Base.NAME"));
        
        Composite l_baseComposite = new Composite(l_rightComposite, SWT.NONE);
        GridData l_gdBase = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
        l_baseComposite.setLayoutData(l_gdBase);
        GridLayout L_glBase = new GridLayout(7, true);
        L_glBase.marginWidth = 0;
        L_glBase.marginHeight = 0;
        l_baseComposite.setLayout(L_glBase);
        
        m_baseText = new Text(l_baseComposite, SWT.BORDER);
        GridData l_gdBaseText = new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1);
        l_gdBaseText.widthHint = 200;
        m_baseText.setLayoutData(l_gdBaseText);
        m_baseText.setTextLimit(32);
        
        m_revLabel = new Label(l_baseComposite, SWT.NONE);
        GridData l_gdRevLabel = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
        l_gdRevLabel.horizontalIndent = 10;
        m_revLabel.setLayoutData(l_gdRevLabel);
        m_revLabel.setText(m_registry.getString("Rev.NAME"));
        
        m_revText = new Text(l_baseComposite, SWT.BORDER);
        GridData l_gdRevText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_gdRevText.widthHint = 20;
        m_revText.setLayoutData(l_gdRevText);
        m_revText.setTextLimit(2);
        
        new Label(l_baseComposite, SWT.NONE);
    }
    
    private void createPartUOMRow(Composite l_legacyComposite)
    {
        m_uomLabel = new Label(l_legacyComposite, SWT.NONE);
        m_uomLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_uomLabel.setText(m_registry.getString("UOM.NAME"));
        
        Composite l_uomComposite = new Composite(l_legacyComposite, SWT.NONE);
        l_uomComposite.setLayoutData(getSubCompositeLayoutData());
        
        l_uomComposite.setLayout(getSubCompositeLayout());
        
        m_uomCombo = new Combo(l_uomComposite, SWT.NONE);
        m_uomCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        
        UIHelper.setAutoComboArray(m_uomCombo, (Vector<String>) RSOneUOMLOVHelper.getUOMLOVValues());
        
        new Label(l_uomComposite, SWT.NONE);
    }
    
    private void createPartDescriptionRow(Composite l_legacyComposite)
    {
        m_descLabel = new Label(l_legacyComposite, SWT.NONE);
        m_descLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 3));
        m_descLabel.setText(m_registry.getString("Description.NAME"));
        
        Composite l_descComposite = new Composite(l_legacyComposite, SWT.NONE);
        l_descComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3));
        
        l_descComposite.setLayout(getSubCompositeLayout());
        
        m_descText = new Text(l_descComposite, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        GridData l_gdDescText = new GridData(SWT.FILL, SWT.FILL, true, true, 6, 3);
        
        m_descText.setLayoutData(l_gdDescText);
        
        UIHelper.enableSelectAll(m_descText);
        
        UIHelper.enableSelectAll(m_descText);
        
        UIHelper.limitCharCountInTextWithPopUpMsg(m_descText, NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
                m_registry.getString("ExceedingDescTextLimit.msg"));
        
        new Label(l_descComposite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        new Label(l_descComposite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        new Label(l_descComposite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_baseText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        
        m_revText.setText(PropertyMapHelper.handleNull(propMap.getCompound("revision").getString(
                NationalOilwell.ITEM_REVISION_ID)));
        IPropertyMap partInfoPropertyMap = propMap.getCompoundArray("dashPropertyMap")[0];
        if (partInfoPropertyMap != null)
        {
            m_nameCombo
                    .setText(PropertyMapHelper.handleNull(partInfoPropertyMap.getString(NationalOilwell.OBJECT_NAME)));
            m_uomCombo.setText(PropertyMapHelper.handleNull(partInfoPropertyMap.getCompound("IMAN_master_form")
                    .getString(NationalOilwell.RSONE_UOM)));
            m_descText
                    .setText(PropertyMapHelper.handleNull(partInfoPropertyMap.getString(NationalOilwell.OBJECT_DESC)));
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(NationalOilwell.ITEM_ID, m_baseText.getText().trim());
        propMap.setString(NationalOilwell.OBJECT_NAME, m_nameCombo.getText().trim());
        // propMap.setTag(NationalOilwell.RSONE_UOM,
        // UOMHelper.getUOMObject(m_uomCombo.getText().trim()));
        propMap.setString(NationalOilwell.OBJECT_DESC, m_descText.getText().trim());
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            masterPropertyMap.setString(NationalOilwell.RSONE_UOM, m_uomCombo.getText().trim());
            masterPropertyMap.setString(NationalOilwell.RSONE_BASE_NUMBER, m_baseText.getText().trim());
            
            propMap.setCompound("IMAN_master_form", masterPropertyMap);
        }
        setRevisionProperties(propMap);
        
        return true;
    }
    
    private void setRevisionProperties(IPropertyMap propMap)
    {
        IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            revisionPropertyMap.setString("item_revision_id", m_revText.getText());
            propMap.setCompound("revision", revisionPropertyMap);
        }
    }
    
    // Added methods for layout setting
    protected GridData getSubCompositeLayoutData()
    {
        return new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1);
    }
    
    protected GridLayout getSubCompositeLayout()
    {
        GridLayout l_glDocPanel = new GridLayout(/* 7 */8, false);
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginTop = 0;
        
        return l_glDocPanel;
    }
    
    // Added methods for layout setting
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    protected Registry m_registry;
    protected TCSession m_session;
    
    private Label m_nameLabel;
    private Label m_baseLabel;
    private Label m_revLabel;
    private Label m_uomLabel;
    private Label m_descLabel;
    
    private Text m_baseText;
    private Text m_revText;
    private Text m_descText;
    
    private Combo m_nameCombo;
    private Combo m_uomCombo;
}
