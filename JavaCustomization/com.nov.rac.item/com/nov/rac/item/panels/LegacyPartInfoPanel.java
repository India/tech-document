package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

/**
 * The Class LegacyPartInfoPanel - Inherits PartInfoPanel for Legacy Part creation.
 */
public class LegacyPartInfoPanel extends PartInfoPanel
{

    /**
     * Instantiates a new legacy part info panel.
     *
     * @param parent the parent
     * @param style the style
     */
    public LegacyPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    /* (non-Javadoc)
     * @see com.nov.rac.item.panels.PartInfoPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        boolean result= super.createUI();
        m_revisionText.setEditable(true);
        return result;
    }
    
}
