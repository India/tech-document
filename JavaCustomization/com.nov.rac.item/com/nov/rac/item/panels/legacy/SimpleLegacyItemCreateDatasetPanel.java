package com.nov.rac.item.panels.legacy;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.panels.NewDatasetPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class SimpleLegacyItemCreateDatasetPanel extends NewDatasetPanel
{
    
    public SimpleLegacyItemCreateDatasetPanel(Composite parent, int style) throws TCException
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        // TODO Auto-generated method stub
        super.createUI();
        setEnable(true);
        registerSubscriber();
        return true;
    }
    
   
   
    
    @Override
    protected PublishEvent getPublishEventForDatasetType()
    {
    // TODO Auto-generated method stub
    return getPublishEvent(NationalOilwell.LEGACY_DATASET_TYPE, "Prev Val",
    m_textDatasetType.getText());
    }
    
    
    
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return super.load(propMap);
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT, this);
        controller.registerSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT, this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().compareTo(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT) == 0)
        {
            Boolean isSelected = new Boolean((Boolean) event.getNewValue());
            
            if (isSelected)
            {
                setEnable(true);
            }
        }
        else if (event.getPropertyName().compareTo(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT) == 0)
        {
            reset();
        }
    }
    
    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        super.dispose();
    }
    
    private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.unregisterSubscriber(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT, this);
    }
}
