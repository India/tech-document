package com.nov.rac.item.panels.legacy;

import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.panels.SimpleDocumentCreatePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SimpleLegacyItemCreateDocPnl extends SimpleDocumentCreatePanel
{
    
    public SimpleLegacyItemCreateDocPnl(Composite parent, int style)
    {
        super(parent, style);
        m_legacyRegistry = getLegacyRegistry();
    }
    
    // legacy panels registry
    private Registry getLegacyRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.legacy.legacy");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean isCreateUI = false;
        isCreateUI = super.createUI();
        mainComposite = getComposite();
        
        createSeqNoRow(subComposite);
        
        createDocSelectionRow(mainComposite);
        
        registerSubscriber();
        
        return isCreateUI;
    }
    
    @Override
    public boolean createUIPost() throws TCException
    {
        addListenersToRDRDDRadioButtons();
        return true;
    }
    
    private void createDocSelectionRow(Composite legacyOtherGrpDocPnlComposite) throws TCException
    {
        Composite rowComposite = SWTUIHelper.createRowComposite(legacyOtherGrpDocPnlComposite, 2, true);
        rowComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        GridData gd_rowComposite = (GridData) rowComposite.getLayoutData();
        // gd_rowComposite.widthHint = 0;
        gd_rowComposite.heightHint = 0;
        m_radBtnDefiningDoc = new Button(rowComposite, SWT.RADIO);
        m_radBtnDefiningDoc.setText(getRegistry().getString("radBtnDefiningDoc.NAME", "Key Not Found"));
        m_radBtnDefiningDoc.setSelection(true);
        
        m_radBtnDefiningDoc.setVisible(false);
        
        m_radBtnRelatedDoc = new Button(rowComposite, SWT.RADIO);
        m_radBtnRelatedDoc.setText(getRegistry().getString("radBtnRelatedDoc.NAME", "Key Not Found"));
        m_radBtnRelatedDoc.setVisible(false);
        m_radBtnRelatedDoc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
    }
    
    /**
     * Creates the Seq No row.
     * 
     * @param legacyDbaGrpDocPnlComposite
     *            the composite for legacy document panel for DBA group
     * @throws TCException
     *             the tC exception
     */
    private void createSeqNoRow(final Composite legacyDbaGrpDocPnlComposite) throws TCException
    {
        
        Label m_lblSeqNo = new Label(legacyDbaGrpDocPnlComposite, SWT.NONE);
        m_lblSeqNo.setText(getRegistry().getString("lblSeqNo.NAME", "Key Not Found"));
        
        m_textSeqNo = new Text(legacyDbaGrpDocPnlComposite, SWT.BORDER);
        m_textSeqNo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        
        // To set Text Limit
        m_textSeqNo.setTextLimit(NationalOilwell.SEQ_TEXT_LIMIT);
        
        addVerifyListenerOnSeqNoTxt();
        
        m_btnAssignSeqNo = new Button(legacyDbaGrpDocPnlComposite, SWT.NONE);
        m_btnAssignSeqNo.setText(super.m_registry.getString("btnAssignSeqNo.NAME", "Key Not Found"));
        
        addSelectionListenerOnAssignSeqNoBtn();
        
        new Label(legacyDbaGrpDocPnlComposite, SWT.NONE);
        new Label(legacyDbaGrpDocPnlComposite, SWT.NONE);
        new Label(legacyDbaGrpDocPnlComposite, SWT.NONE);
    }
    
    private void addSelectionListenerOnAssignSeqNoBtn()
    {
        m_btnAssignSeqNo.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent se)
            {
                if (base == "" || base == null || rev == "" || rev == null)
                {
                    MessageBox.post(m_registry.getString("btnAssignSeqNo.MSG"), "Invalid input(s)", MessageBox.ERROR);
                    
                }
                else
                {
                    m_textSeqNo.setText("001");
                    // onSeqNoAssignButtonClick();
                }
            }
        });
    }
    
    private void addVerifyListenerOnSeqNoTxt()
    {
        m_textSeqNo.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_textSeqNo);
            }
        });
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            m_textSeqNo.setText(PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.SEQUENCE)));
            
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            PropertyMapHelper.setString(NationalOilwell.SEQUENCE, m_textSeqNo.getText(), masterPropertyMap);
        }
        return true;
    }
    
    public boolean reset()
    {
        super.reset();
        
        m_textSeqNo.setText("");
        
        return true;
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT, this);
        controller.registerSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT, this);
        controller.registerSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT, this);
        
        controller.registerSubscriber(NationalOilwell.MSG_BASE_NUMBER_TEXT_MODIFY_EVENT, this);
        
        controller.registerSubscriber(NationalOilwell.MSG_REV_TEXT_MODIFY_EVENT, this);
        
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().compareTo(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT) == 0)
        {
            Boolean isSelected = new Boolean((Boolean) event.getNewValue());
            Boolean legacyHasRDGroup = LegacyItemCreateHelper.isLegacyWithRDGroup();
            String[] itemTypes = m_legacyRegistry.getStringArray("VARIATION_REQUIRED_ITEM_TYPES");
            if (isSelected)
            {
                disableDocPanel(legacyHasRDGroup, itemTypes);
                
                if (itemType.equals(NationalOilwell.D_DOCUMENTS))
                {
                    m_btnAssignSeqNo.setEnabled(false);
                    m_textSeqNo.setEditable(false);
                }
            }
            else
            {
                enableDocPanel(legacyHasRDGroup, itemTypes);
            }
        }
        else if (event.getPropertyName().compareTo(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT) == 0)
        {
            reset();
            
            itemType = (String) event.getNewValue();
            
            showRDRDDGroup = LegacyItemCreateHelper.isRDRDDGroup();
            
            String[] itemTypes = m_legacyRegistry.getStringArray("VARIATION_REQUIRED_ITEM_TYPES");
            
            boolean typeMatched = isTypeMatching(itemType, itemTypes);
            
            if (showRDRDDGroup && typeMatched)
            {
                showRDRDD(true);
                LegacyItemCreateHelper.setSelectedRelation(NationalOilwell.RELATED_DEFINING_DOCUMENT);
            }
            else
            {
                showRDRDD(false);
                LegacyItemCreateHelper.setSelectedRelation(NationalOilwell.RELATED_DOCUMENT);
            }
            
            if (NewItemCreationHelper.getTargetComponent() != null)
            {
                m_radBtnRelatedDoc.setSelection(true);
                m_radBtnDefiningDoc.setSelection(false);
            }
            if (itemType.compareTo(NationalOilwell.D_DOCUMENTS) == 0)
            {
                // UIHelper.makeMandatory(m_docCategoryPopupButton.getPopupButton());
                UIHelper.makeMandatory(m_textDocCategory);
                UIHelper.makeMandatory(m_docTypeLovPopupButton.getPopupButton());
                
                m_textDocCategory.setEnabled(true);
                m_docCategoryPopupButton.getPopupButton().setEnabled(true);
                m_textSeqNo.setEnabled(true);
                m_btnAssignSeqNo.setEnabled(true);
            }
            else
            {
                // Make it unMandatory
                
            }
            
        }
        
        else if (event.getPropertyName().compareTo(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT) == 0)
        {
            reset();
        }
        
        else if (event.getPropertyName().compareTo(NationalOilwell.MSG_BASE_NUMBER_TEXT_MODIFY_EVENT) == 0)
        {
            base = (String) event.getNewValue();
        }
        
        else if (event.getPropertyName().compareTo(NationalOilwell.MSG_REV_TEXT_MODIFY_EVENT) == 0)
        {
            rev = (String) event.getNewValue();
        }
    }
    
    private void enableDocPanel(Boolean legacyHasRDGroup, String[] itemTypes)
    {
        if (itemType != null)
        {
            boolean isTypeMatching = isTypeMatching(itemType, itemTypes);
            
            if (legacyHasRDGroup && isTypeMatching)
            {
                showRDRDD(true);
            }
            else
            {
                m_textDocCategory.setEnabled(true);
                m_docCategoryPopupButton.getPopupButton().setEnabled(true);
                
                enableRDRDD(true);
            }
            
            m_textSeqNo.setEnabled(true);
            m_btnAssignSeqNo.setEnabled(true);
        }
    }
    
    private void disableDocPanel(Boolean legacyHasRDGroup, String[] itemTypes)
    {
        if (itemType != null && !itemType.equals(NationalOilwell.D_DOCUMENTS))
        {
            boolean typeMatched = isTypeMatching(itemType, itemTypes);
            
            if (legacyHasRDGroup && typeMatched)
            {
                showRDRDD(false);
            }
            else
            {
                m_textDocCategory.setText("");
                m_textDocCategory.setEnabled(false);
                
                m_docCategoryPopupButton.getPopupButton().setEnabled(false);
                m_docTypeLovPopupButton.getPopupButton().setText("");
                
                m_docTypeLovPopupButton.getPopupButton().setEnabled(false);
                m_textSeqNo.setText("");
                enableRDRDD(false);
            }
            
            m_textSeqNo.setEnabled(false);
            m_btnAssignSeqNo.setEnabled(false);
        }
    }
    
    private void enableRDRDD(boolean enable)
    {
        m_radBtnDefiningDoc.setEnabled(enable);
        m_radBtnRelatedDoc.setEnabled(enable);
    }
    
    private void showRDRDD(boolean visible)
    {
        
        Composite parentComposite = m_radBtnDefiningDoc.getParent();
        
        GridData gd_parentComposite = (GridData) parentComposite.getLayoutData();
        gd_parentComposite.heightHint = 15;
        forceLayout(parentComposite.getShell());
        m_radBtnDefiningDoc.setVisible(visible);
        
        m_radBtnRelatedDoc.setVisible(visible);
    }
    
    @Override
    protected void setTextForDocCategoryEnabled(String textValue, String buttonCaption)
    {
        m_textDocCategory.setText(textValue);
        m_BtnDocType.setText(buttonCaption);
        m_BtnDocType.setEnabled(true);
        m_textSeqNo.setText("001");
        setDocTypeLovComponent(m_BtnDocType, textValue);
        publishDocCategory_Type();
        m_textDocCategory.setSelection(m_textDocCategory.getText().length());
        moveDocTypeButton();
    }
    
    @Override
    protected void resetDocType()
    {
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        if (hasContent(this.m_textSeqNo.getText()))
            this.m_textSeqNo.setText("");
        publishDocCategory_Type();
    }
    
    private void doTextValidation(VerifyEvent event, Text m_tempText)
    {
        for (char c : event.text.toCharArray())
            if (!Character.isDigit(c))
            {
                event.doit = false;
            }
    }
    
    private void unregisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.unregisterSubscriber(NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT, this);
        
        controller.unregisterSubscriber(NationalOilwell.MSG_BASE_NUMBER_TEXT_MODIFY_EVENT, this);
        
        controller.unregisterSubscriber(NationalOilwell.MSG_REV_TEXT_MODIFY_EVENT, this);
    }
    
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    /**
     * checks if selected type is present in specified types or not
     * 
     * @param type
     * @param itemTypes
     * @return isMatching
     */
    private boolean isTypeMatching(String type, String[] itemTypes)
    {
        boolean isMatching = false;
        
        for (String itemType : itemTypes)
        {
            if (type.equalsIgnoreCase(itemType))
            {
                isMatching = true;
                break;
            }
        }
        
        return isMatching;
    }
    
    private void addListenersToRDRDDRadioButtons()
    {
        m_radBtnDefiningDoc.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                if (m_radBtnDefiningDoc.isVisible() && m_radBtnDefiningDoc.getSelection())
                {
                    LegacyItemCreateHelper.setSelectedRelation(NationalOilwell.RELATED_DEFINING_DOCUMENT);
                    publishEvent(NationalOilwell.RELATED_DEFINING_DOCUMENT);
                }
            }
        });
        
        m_radBtnRelatedDoc.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                if (m_radBtnRelatedDoc.isVisible() && m_radBtnRelatedDoc.getSelection())
                {
                    LegacyItemCreateHelper.setSelectedRelation(NationalOilwell.RELATED_DOCUMENT);
                    publishEvent(NationalOilwell.RELATED_DOCUMENT);
                }
            }
        });
        
    }
    
    private void publishEvent(String relationName)
    {
        IController thController = ControllerFactory.getInstance().getDefaultController();
        
        PublishEvent publishevent = new PublishEvent(getPublisher(), NationalOilwell.LEGACY_RD_OR_RDD_SELECTED_MSG,
                relationName, null);
        
        thController.publish(publishevent);
    }
    
    protected void forceLayout(Shell shell)
    {
        Control[] children = shell.getChildren();
        if (children != null)
        {
            for (Control control : children)
            {
                if (control instanceof Composite)
                {
                    forceLayout(((Composite) control));
                }
            }
        }
        shell.layout();
    }
    
    protected void forceLayout(Composite parent)
    {
        Control[] children = parent.getChildren();
        if (children != null)
        {
            for (Control control : children)
            {
                if (control instanceof Composite)
                {
                    forceLayout(((Composite) control));
                }
            }
        }
        parent.layout();
    }
    
    private Text m_textSeqNo;
    
    private Button m_btnAssignSeqNo;
    
    private String base = null;
    
    private String rev = null;
    
    private Button m_radBtnDefiningDoc = null;
    private Button m_radBtnRelatedDoc = null;
    
    boolean showRDRDDGroup = false;
    
    private Composite mainComposite;
    private Composite legacyDocSelComposite = null;
    
    private String itemType = null;
    
    private Registry m_legacyRegistry;
}
