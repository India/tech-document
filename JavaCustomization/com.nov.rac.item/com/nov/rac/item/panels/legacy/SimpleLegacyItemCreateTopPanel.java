package com.nov.rac.item.panels.legacy;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.common.NOVDataManagementServiceInfo;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.utils.LegacyItemCreateLovPopupDialog;
import com.nov.rac.utilities.utils.UIHelper;
import com.nov.rac.utilities.utils.UOMHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.controls.SWTLovPopupButton;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;


public class SimpleLegacyItemCreateTopPanel extends AbstractUIPanel implements
        ILoadSave, IPublisher, ISubscriber
{
    
	public SimpleLegacyItemCreateTopPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        m_legacyregistry = getLegacyRegistry();
        m_isUOMMandatory = false;
    }
    
    //legacy panels registry
    private Registry getLegacyRegistry() 
    {
		return Registry.getRegistry("com.nov.rac.item.panels.legacy.legacy");
	}

	/**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    protected TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite legacyComposite = getComposite();
        legacyComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_MAIN_COMPOSITE, true));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        legacyComposite.setLayoutData(gd_panel);
        
        createGroupRow(legacyComposite);
        createTypeRow(legacyComposite);
        createBaseRow(legacyComposite);
        
        createRevRow(legacyComposite);
        createNameRow(legacyComposite);
        createDescRow(legacyComposite);
        
        addSelectionListenerOnItemTypeLovPopupBtn();
        
        registerSubscriber();
        
        return true;
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        controller.registerSubscriber(
                NationalOilwell.MSG_APPLY_BTN_SELECTION_EVENT, this);
        
    }

    private void createGroupRow(Composite legacyComposite)
    {
        
        /*Label m_lblGroup = new Label(legacyComposite, SWT.NONE);
        m_lblGroup.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1));
        m_lblGroup.setText(m_registry.getString("lblGroup.NAME",
                "Key Not Found"));
        
        
        Label m_lblGrupID = new Label(legacyComposite, SWT.NONE);
        
        m_lblGrupID.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_GORUP_ID_LABEL,
                1));
        m_lblGrupID.setText(NewItemCreationHelper.getCurrentGroup());
        
       /* 
        Label m_lbl = new Label(legacyComposite, SWT.NONE);
        GridData m_gd_lbl = new GridData(SWT.FILL, SWT.CENTER, true, false, 4,
                1);
        m_lbl.setLayoutData(m_gd_lbl);*/
    	
    	Label m_lblGroup = new Label(legacyComposite, SWT.NONE);
        m_lblGroup.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        m_lblGroup.setText(m_registry.getString("lblGroup.NAME",
                "Key Not Found"));
        
        
        Label m_lblGrupID = new Label(legacyComposite, SWT.NONE);
        
        m_lblGrupID.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_GORUP_ID_LABEL,
                1));
        m_lblGrupID.setText(NewItemCreationHelper.getCurrentGroup());
        
       /* 
        Label m_lbl = new Label(legacyComposite, SWT.NONE);
        GridData m_gd_lbl = new GridData(SWT.FILL, SWT.CENTER, true, false, 4,
                1);
        m_lbl.setLayoutData(m_gd_lbl);*/

        
    }
    
    private void createTypeRow(Composite legacyComposite) throws TCException
    {
        Label m_lblType = new Label(legacyComposite, SWT.NONE);
        m_lblType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        m_lblType
                .setText(m_registry.getString("lblType.NAME", "Key Not Found"));
        
        createTypeLovPopupButton(legacyComposite);
        new Label(legacyComposite, SWT.NONE);
        new Label(legacyComposite, SWT.NONE);
        
        m_btnCheckLegacy = new Button(legacyComposite, SWT.CHECK);
        m_btnCheckLegacy.setText(m_registry.getString("btnCheckLegacy.NAME",
                "Key Not Found"));
        
        addSelectionListenerOnLegacyCheckBtn();
        
    }
    
    private void createBaseRow(Composite legacyComposite)
    {
        Label m_lblBase = new Label(legacyComposite, SWT.NONE);
        m_lblBase.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        m_lblBase
                .setText(m_registry.getString("lblBase.NAME", "Key Not Found"));
        
        m_textBase = new Text(legacyComposite, SWT.BORDER);
        GridData m_gd_txtBase = new GridData(SWT.FILL, SWT.CENTER, true, false,
                HORIZONTAL_SPAN_FOR_BASE_TEXT, 1);
        m_textBase.setLayoutData(m_gd_txtBase);
        
        // To set Text Limit
        m_textBase.setTextLimit(NationalOilwell.BASE_TEXT_LIMIT);
        
        UIHelper.convertToUpperCase(m_textBase);
        
        m_btnAssignBase = new Button(legacyComposite, SWT.NONE);
        GridData m_gd_btnAssignBase = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        m_btnAssignBase.setText(m_registry.getString("btnAssignBase.NAME",
                "Key Not Found"));
        m_gd_btnAssignBase.horizontalIndent = ASSIGN_BTN_HORIZONTAL_INDENT;
        m_btnAssignBase.setLayoutData(m_gd_btnAssignBase);
        
        addModifyListenerOnBaseTxt();
        
        // Rupali : SOA call to assign Base Number
        addSelectionListenerOnAssignBaseBtn();
        
    }
    
    private void addModifyListenerOnBaseTxt()
    {
        m_textBase.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                String base = m_textBase.getText();
                
                final IController controller = ControllerFactory.getInstance()
                        .getDefaultController();
                
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_BASE_NUMBER_TEXT_MODIFY_EVENT,
                        base, null);
                
                controller.publish(event);
                
            }
            
        });
    }
    
    private void addSelectionListenerOnAssignBaseBtn()
    {
        m_btnAssignBase.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent se)
            {
                String itemType = m_itemTypeLovPopupButton.getPopupButton()
                        .getText();
                if (!itemType.equals(""))
                {
                    onBaseAssignButtonClick();
                }
                else
                {
                    MessageBox.post(m_registry.getString("btnAssignBase.MSG"),
                            "Invalid input(s)", MessageBox.ERROR);
                }
            }
        });
    }
    
    private void onBaseAssignButtonClick()
    {
        String baseText = m_textBase.getText();
        if(baseText == null || baseText.equals(""))
        {
            NOVDataManagementServiceInfo novGetNextValueService = new NOVDataManagementServiceInfo();
            
            String strPrefixExp = NewItemCreationHelper.getPrefix();
            String strCounterName = strPrefixExp + "_itemid";
            String strPostfixExp = "";
            String strGroupCode = "";
            String newId;
            if (!strPrefixExp.equals(""))
            {
                strGroupCode = strPrefixExp.substring(1);
            }
            
            setDatatoService(novGetNextValueService, strCounterName, strPrefixExp,
                    strPostfixExp, strGroupCode);
            
            newId = novGetNextValueService.getNextValue();
            
            m_textBase.setText(newId);
        }
        else
        {
            MessageBox.post(m_registry.getString("baseIdAssigned.MSG"),
                    "Invalid input(s)", MessageBox.ERROR);
        }
    }
    
    /**
     * Set Input data to the service
     */
    
    void setDatatoService(NOVDataManagementServiceInfo novGetNextValueService,
            String strCounterName, String strPrefixExp, String strPostfixExp,
            String strGroupCode)
    {
        int theScope = TCPreferenceService.TC_preference_site;
        
        novGetNextValueService.setContext("ITEM_ID");
        novGetNextValueService.setCounterName(strCounterName);
        novGetNextValueService.setDefaultCounterStartValue(DEFAULT_COUNTER_START_VALUE);
        novGetNextValueService.setPrefix(strPrefixExp);
        novGetNextValueService.setPostfix(strPostfixExp);
        
        PreferenceUtils PrefObj = new PreferenceUtils();
        
        boolean bJDEGroupDesicion = PrefObj.isPrefValueExist(strGroupCode,
                "NOV_JDE_check_creation_groups", theScope);
        if (bJDEGroupDesicion)
        {
            novGetNextValueService
                    .setAlternateIDContext(new String[] { "JDE" });
        }
        else
        {
            novGetNextValueService.setAlternateIDContext(new String[] { "" });
        }
    }
    
    private void createRevRow(Composite legacyComposite)
    {
        Label m_lblRev = new Label(legacyComposite, SWT.NONE);
        m_lblRev.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        m_lblRev.setText(m_registry.getString("lblRev.NAME", "Key Not Found"));
        
        m_textRev = new Text(legacyComposite, SWT.BORDER);
        m_textRev.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
                false, 1, 1));
        
        // To set Text Limit
        m_textRev.setTextLimit(NationalOilwell.REV_TEXT_LIMIT);
        m_btnAssignRev = new Button(legacyComposite, SWT.NONE);
        GridData m_gd_btnAssignRev = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        m_btnAssignRev.setText(m_registry.getString("btnAssignRev.NAME",
                "Key Not Found"));
        m_gd_btnAssignRev.horizontalIndent = ASSIGN_BTN_HORIZONTAL_INDENT;
        m_btnAssignRev.setLayoutData(m_gd_btnAssignRev);
        
        Label m_lblUOM = new Label(legacyComposite, SWT.NONE);
        m_lblUOM.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1));
        m_lblUOM.setText(m_registry.getString("lblUOM.NAME", "Key Not Found"));
        
        m_comboUOM = new Combo(legacyComposite, SWT.NONE);
        m_comboUOM.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
        		true, 1, 1));
        UIHelper.setAutoComboCollection(m_comboUOM,
                POPULATE_DATA.getUOMLOVValues());
        
        new Label(legacyComposite, SWT.NONE);
        
        addVerifyListenerOnRevTxt();
        
        addModifyListenerOnRevTxt();
        
        // Rupali : Need to change after SOA development
        addSelectionListenerOnAssignRevBtn();
        
    }
    
    private void addModifyListenerOnRevTxt()
    {
        m_textRev.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                String rev = m_textRev.getText();
                
                final IController controller = ControllerFactory.getInstance()
                        .getDefaultController();
                
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_REV_TEXT_MODIFY_EVENT, rev, null);
                
                controller.publish(event);
                
            }
            
        });
    }
    
    private void addSelectionListenerOnAssignRevBtn()
    {
        m_btnAssignRev.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent se)
            {
                String base = m_textBase.getText();
                if (!base.equals(""))
                {
                    m_textRev.setText(NationalOilwell.DEFAULT_REV_ID);
                    // onRevAssignButtonClick();
                }
                else
                {
                    MessageBox.post(m_registry.getString("btnAssignRev.MSG"),
                            "Invalid input(s)", MessageBox.ERROR);
                }
            }
        });
    }
    
    private void addVerifyListenerOnRevTxt()
    {
        m_textRev.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_textRev);
            }
        });
    }
    
    private void createNameRow(Composite legacyComposite)
    {
        Label m_lblName = new Label(legacyComposite, SWT.NONE);
        m_lblName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        m_lblName
                .setText(m_registry.getString("lblName.NAME", "Key Not Found"));
        
        m_comboName = new Combo(legacyComposite, SWT.NONE);
        GridData m_gd_comboName = new GridData(SWT.FILL, SWT.CENTER, true,
                false, HORIZONTAL_SPAN_FOR_NAME_COMBO, 1);
        m_comboName.setLayoutData(m_gd_comboName);
        
        // To set Text Limit
        m_comboName.setTextLimit(NationalOilwell.NAME_TEXT_LIMIT);
        
        addVerifyListenerOnNameCombo();
        
        new Label(legacyComposite, SWT.NONE);
        
    }

    private void addVerifyListenerOnNameCombo()
    {
        m_comboName.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent e)
            {
                String itemType = m_itemTypeLovPopupButton.getPopupButton().getText();
                if (itemType.equals(NationalOilwell.D_DOCUMENTS) || itemType.equals(NationalOilwell.M_PART) || itemType.equals(NationalOilwell.N_NUMBERED_PRODUCT) || itemType.equals(NationalOilwell.P_PURCHASED_PART))
                {
                   e.text = e.text.toUpperCase();
                }
            }
        });
    }
    
    private void createDescRow(Composite legacyComposite)
    {
        Label m_lblDesc = new Label(legacyComposite, SWT.NONE);
        m_lblDesc.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        m_lblDesc
                .setText(m_registry.getString("lblDesc.NAME", "Key Not Found"));
        
        m_textDesc = new Text(legacyComposite, SWT.BORDER);
        GridData m_gd_txtDesc = new GridData(SWT.FILL, SWT.CENTER, true, false,
                HORIZONTAL_SPAN_FOR_DESC_TEXT, 1);
        m_textDesc.setLayoutData(m_gd_txtDesc);
        
        // To set Text Limit
        m_textDesc.setTextLimit(NationalOilwell.DESC_TEXT_LIMIT);
        
        UIHelper.convertToUpperCase(m_textDesc);
    }
    
    private void createTypeLovPopupButton(Composite legacyComposite)
            throws TCException
    {
        
        TCComponent l_tcComponent = null;
        m_btnType = new Button(legacyComposite, SWT.NONE);
        m_btnType.setToolTipText(m_registry.getString("itemTypePopup.TOOLTIP",
                "Key Not Found "));
        
        m_typeDialog = new LegacyItemCreateLovPopupDialog(
                legacyComposite.getShell(), SWT.NONE, LegacyItemCreateHelper.getItemType(), "Default");
        m_itemTypeLovPopupButton = new SWTLovPopupButton(m_btnType, m_typeDialog,
        		LegacyItemCreateHelper.getItemType(), "Default", l_tcComponent);
        GridData gd_itemType = new GridData(SWT.FILL, SWT.CENTER, true, false,
                2, 1);
        
        m_itemTypeLovPopupButton.getPopupButton().setLayoutData(gd_itemType);
        
        m_lovPopupButtonImage = new Image(
                legacyComposite.getDisplay(),
                m_registry
                        .getImage("SimpleLegacyItemCreatePanel1.LOVButtonImage"),
                SWT.NONE);
        
        m_itemTypeLovPopupButton.setIcon(m_lovPopupButtonImage);
        
        m_typeDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(
                    final PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                final String l_type = (String) propertychangeevent.getNewValue();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        public void run()
                        {
                            m_itemTypeLovPopupButton
                                    .setText(propertychangeevent.getNewValue()
                                            .toString());
                            processLovButtonSelection();
                            makeUOMMandatoryOrOptional(l_type);
                            moveItemTypeButton();
                            enableDisableControls();
                            
                        }
                    });
                }
            }
        });
        
    }

    /* 
     * To make UOM field mandatory or Non Mandatory based on the preference &
     * selected item type
     */
    
    private void makeUOMMandatoryOrOptional(String type) 
    {
		m_isUOMMandatory = LegacyItemCreateHelper.isUOMMandatoryGroup();
		
    	if(m_isUOMMandatory)
    	{
    		String[] itemTypes = m_legacyregistry.getStringArray("VARIATION_REQUIRED_ITEM_TYPES");
    		
    		boolean isMatchFound = isTypeMatching(type, itemTypes);
    		
    		if(isMatchFound)
			{
    			UIHelper.makeMandatory(m_comboUOM, false);
				m_comboUOM.select(m_comboUOM.indexOf(m_legacyregistry.getString("UOMEA.NAME")));
			}
    	}
	}
    
    /**
     * checks if selected type is present in specified types or not  
     * @param type
     * @param itemTypes
     * @return isMatching
     */
    private boolean isTypeMatching(String type, String[] itemTypes) 
    {
    	boolean isMatching = false;
    	
    	 for (String eachItemType : itemTypes) 
         {
             if (type.equalsIgnoreCase(eachItemType))
             {
            	 isMatching = true;
                 break;
             }
         }
    	 
		return isMatching;
	}

	private void enableDisableControls()
    {
        m_textBase.setEnabled(true);
        m_btnAssignBase.setEnabled(true);
        m_textRev.setEnabled(true);
        m_btnAssignRev.setEnabled(true);
    }
    
    /** The Constant POPULATE_DATA. */
    private final static PopulateData POPULATE_DATA;
    
    static
    {
        POPULATE_DATA = new PopulateData();
    }
    
    /**
     * The Class PopulateData.
     */
    private static class PopulateData
    {
        
        /** The session. */
        private static TCSession session;
        
        /**
         * Gets the session.
         * 
         * @return the session
         */
        protected TCSession getSession()
        {
            return (TCSession) AIFUtility.getCurrentApplication().getSession();
            
        }
        
        /**
         * Instantiates a new populate data.
         */
        public PopulateData()
        {
            session = getSession();
        }
        
        /**
         * Gets the uOMLOV values.
         * 
         * @return the uOMLOV values
         */
        private Vector<Object> getUOMLOVValues()
        {
            Vector<Object> uomvector = new Vector<Object>();
            uomvector.add("");
            TCComponentListOfValues lovvalues = TCComponentListOfValuesType
                    .findLOVByName(session, NationalOilwell.UOMLOV);
            if (lovvalues != null)
            {
                ListOfValuesInfo lovInfo = null;
                try
                {
                    lovInfo = lovvalues.getListOfValues();
                    Object[] lovS = lovInfo.getListOfValues();
                    String[] lovDesc = lovInfo.getDescriptions();
                    if (lovS.length > 0)
                    {
                        for (int i = 0; i < lovS.length; i++)
                        {
                            String[] splitDesc = null;
                            if (lovDesc != null && lovDesc[i] != null)
                            {
                                splitDesc = lovDesc[i].split(",");
                            }
                            getGrpUOM(splitDesc, uomvector, lovS[i]);
                        }
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                    
                }
                
            }
            return uomvector;
        }
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    public static void getGrpUOM(String[] splitDesc, Vector<Object> uomvector,
            Object lovS)
    {
        for (int inx = 0; inx < splitDesc.length; inx++)
        {
            if (splitDesc != null
                    && splitDesc[inx].trim().equalsIgnoreCase(
                            NationalOilwell.GRPUOMLEG)
                    || splitDesc[inx].trim().equalsIgnoreCase(""))
            {
                uomvector.add(lovS);
                break;
            }
        }
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // item properties
        // m_itemTypeLovPopupButton.getPopupButton().setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.TYPE)));
        IPropertyMap masterPropertyMap = propMap
                .getCompound("IMAN_master_form");
        
        m_comboUOM.setText(PropertyMapHelper.handleNull(propMap
                .getTag(NationalOilwell.NOV_UOM)));
        m_comboName.setText(PropertyMapHelper.handleNull(propMap
                .getString(NationalOilwell.OBJECT_NAME)));
        m_textDesc.setText(PropertyMapHelper.handleNull(propMap
                .getString(NationalOilwell.OBJECT_DESC)));
       /* m_textBase.setText(PropertyMapHelper.handleNull(propMap
                .getString(NationalOilwell.ITEM_ID)));*/
        m_textRev.setText("01");
        // item master properties
        if (masterPropertyMap != null)
        {
            m_textBase.setText(PropertyMapHelper.handleNull(masterPropertyMap
                    .getString(NationalOilwell.BASE_NUMBER)));
            
            String objectType =  propMap.getString(NationalOilwell.OBJECT_TYPE);
            
            String displayName = m_legacyregistry.getString(objectType+".displayname");
                 
            if(displayName != null && objectType.equalsIgnoreCase(NationalOilwell.PART_BO_TYPE))
            {
                String rsoneItemType = masterPropertyMap.getString(NationalOilwell.RSONE_ITEMTYPE);
                
                if(rsoneItemType != null && rsoneItemType.equalsIgnoreCase(m_legacyregistry.getString("RSOneItemType.name")))
                {
                    m_btnType.setText(NationalOilwell.P_PURCHASED_PART);
                }
                else
                {
                    m_btnType.setText(NationalOilwell.M_PART);
                }
            }
            else
            {
                m_btnType.setText(displayName);
            }
            
            m_typeDialog.propertyChange(new PropertyChangeEvent(m_typeDialog, "NewLOVValueSelected", null, m_btnType.getText()));
            
           Integer legacy = masterPropertyMap.getInteger(NationalOilwell.LEGACY);
           if(legacy != null && legacy == 1)
           {
               m_btnCheckLegacy.setSelection(true);
           }
           else
           {
               m_btnCheckLegacy.setSelection(false);
           }
            
        }
        
        // item revision properties
        IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            m_textRev.setText(PropertyMapHelper.handleNull(revisionPropertyMap
                    .getString(NationalOilwell.ITEMREVISIONID)));
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // item properties
        
        propMap.setTag(NationalOilwell.NOV_UOM,
                UOMHelper.getUOMObject(m_comboUOM.getText()));
        propMap.setString(NationalOilwell.OBJECT_NAME, m_comboName.getText());
        propMap.setString(NationalOilwell.OBJECT_DESC, m_textDesc.getText());
        propMap.setString(NationalOilwell.ITEM_ID, m_textBase.getText());
        
        // item master properties
        IPropertyMap masterPropertyMap = propMap
                .getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            masterPropertyMap.setString(NationalOilwell.BASE_NUMBER,
                    m_textBase.getText());
            if(m_btnType.getText().equalsIgnoreCase(NationalOilwell.P_PURCHASED_PART))
            {
                masterPropertyMap.setString(NationalOilwell.RSONE_ITEMTYPE, m_legacyregistry.getString("RSOneItemType.name"));
            }
            if(m_btnCheckLegacy.getSelection())
            {
                masterPropertyMap.setInteger(NationalOilwell.LEGACY, 1);
            }
            else
            {
                masterPropertyMap.setInteger(NationalOilwell.LEGACY, 0);
            }
            
            propMap.setCompound("IMAN_master_form", masterPropertyMap);
            
        }
        
        // item revision properties
        IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
        if (revisionPropertyMap != null)
        {
            revisionPropertyMap.setString(NationalOilwell.ITEMREVISIONID,
                    m_textRev.getText());
            propMap.setCompound("revision", revisionPropertyMap);
        }
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    public boolean reset()
    {
        m_itemTypeLovPopupButton.getPopupButton().setText("");
        m_textBase.setText("");
        m_textBase.setEnabled(false);
        m_btnAssignBase.setEnabled(false);
        m_textRev.setText("");
        m_textRev.setEnabled(false);
        m_btnAssignRev.setEnabled(false);
        m_comboUOM.select(0);
        m_comboName.setText("");
        m_textDesc.setText("");
        moveItemTypeButton();
        return true;
    }
    
    private boolean addSelectionListenerOnItemTypeLovPopupBtn()
    {
        m_itemTypeLovPopupButton.getPopupButton().addSelectionListener(
                new SelectionAdapter()
                {
                    public void widgetSelected(SelectionEvent se)
                    {
                        
                        IController controller = ControllerFactory
                                .getInstance().getDefaultController();
                        
                        PublishEvent event = new PublishEvent(
                                getPublisher(),
                                NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT,
                                m_itemTypeLovPopupButton.getPopupButton().getText(), null);
                        
                        controller.publish(event);
                        
                        resetOnItemTypeLovPopupBtn();   
                        
                    }
                });
        
        return true;
    }
       
    private void resetOnItemTypeLovPopupBtn()
    {
        m_itemTypeLovPopupButton.getPopupButton().setText("");
        m_textBase.setText("");
        m_textBase.setEnabled(true);
        m_btnAssignBase.setEnabled(true);
        m_textRev.setText("");
        m_textRev.setEnabled(true);
        m_btnAssignRev.setEnabled(true);
        m_comboUOM.select(0);
        m_comboName.setText("");
        m_textDesc.setText("");
        moveItemTypeButton();
        UIHelper.makeOptional(m_comboUOM);
        
        //this.getComposite().redraw();
    }
    
    private boolean addSelectionListenerOnLegacyCheckBtn()
    {
        m_btnCheckLegacy.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent se)
            {
            	if(m_btnCheckLegacy.getSelection() && m_itemTypeLovPopupButton.getPopupButton().getText().equals(NationalOilwell.D_DOCUMENTS))
            	{
            		m_btnAssignBase.setEnabled(false);
            	}
                final IController controller = ControllerFactory.getInstance()
                        .getDefaultController();
                
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_LEGACY_CHECK_BTN_SELECTION_EVENT,
                        m_btnCheckLegacy.getSelection(), null);
                
                controller.publish(event);
                
            }
        });
        return true;
    }
    
    private void processLovButtonSelection()
    {
        String itemTypeVal = m_itemTypeLovPopupButton.getPopupButton()
                .getText();
        
        final IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        final PublishEvent event = new PublishEvent(
                getPublisher(),
                NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT,
                itemTypeVal, null);
        
        controller.publish(event);
        
        int documentResult = itemTypeVal.compareTo(NationalOilwell.D_DOCUMENTS);
        if (documentResult == 0)
        {
            m_comboUOM.setEnabled(false);
        }
        else
        {
            m_comboUOM.setEnabled(true);
        }
        
        int productResult = itemTypeVal.compareTo(NationalOilwell.S_PRODUCT);
        if (productResult == 0)
        {
            m_btnCheckLegacy.setEnabled(false);
        }
        else
        {
            m_btnCheckLegacy.setEnabled(true);
        }
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    
    @Override
    public boolean createUIPost() throws TCException
    {
        boolean returnValue = true;
        
        UIHelper.makeMandatory(m_itemTypeLovPopupButton.getPopupButton());
        UIHelper.makeMandatory(m_textBase);
        UIHelper.makeMandatory(m_textRev);
        UIHelper.makeMandatory(m_comboName);
       // UIHelper.makeOptional(m_comboName);
        
        //UIHelper.makeComboOptional(m_comboName);
        //m_typeDialog.propertyChange(new PropertyChangeEvent(m_itemTypeLovPopupButton, "NewLOVValueSelected", null, "M - Part"));
        
        return returnValue;
    }
    
    /**
     * Fire a m_ButDocType move event.
     */
    protected void moveItemTypeButton()
    {
        Rectangle rect = m_itemTypeLovPopupButton.getPopupButton().getBounds();
        m_itemTypeLovPopupButton.getPopupButton().setLocation(rect.x + 1,
                rect.y);
        m_itemTypeLovPopupButton.getPopupButton().setLocation(rect.x, rect.y);
    }
    
    private void doTextValidation(VerifyEvent event, Text m_tempText)
    {
        for (char c : event.text.toCharArray())
        {
            if (!Character.isDigit(c))
            {
                event.doit = false;
            }
        }
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if(event.getPropertyName().equals(NationalOilwell.MSG_APPLY_BTN_SELECTION_EVENT))
        {
            m_textBase.setText("");
        }
    }
    
    @Override
    public void dispose()
    {
        super.dispose();
        unregisterSubscriber();
    }
    
    private void unregisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        controller.unregisterSubscriber(
                NationalOilwell.MSG_APPLY_BTN_SELECTION_EVENT, this);
        
    }

    /** The m_registry. */
    private Registry m_registry = null;
    
    private Registry m_legacyregistry = null;
    
    private Button m_btnCheckLegacy;
    
    /** The m_btn item type. */
    private Button m_btnType;
    
    /** The item type dialog. */
    private LegacyItemCreateLovPopupDialog m_typeDialog;
    
    /** The m_item type lov popup button. */
    SWTLovPopupButton m_itemTypeLovPopupButton;
    
    /** The m_ lov popup button image. */
    private Image m_lovPopupButtonImage;
    
    private Text m_textBase;
    
    private Button m_btnAssignBase;
    
    private Button m_btnAssignRev;
    
    private Text m_textRev;
    
    private Combo m_comboUOM;
    
    private Combo m_comboName;
    
    private Text m_textDesc;
    
    private boolean m_isUOMMandatory;
    
    private static final int DEFAULT_COUNTER_START_VALUE = 1000000;
    private static final int NO_OF_COLUMNS_FOR_MAIN_COMPOSITE = 6;
    private static final int HORIZONTAL_SPAN_FOR_GORUP_ID_LABEL = 5;
    private static final int ASSIGN_BTN_HORIZONTAL_INDENT = 5;
    private static final int HORIZONTAL_SPAN_FOR_BASE_TEXT = 4;
	private static final int HORIZONTAL_SPAN_FOR_NAME_COMBO = 4;
	private static final int HORIZONTAL_SPAN_FOR_DESC_TEXT = 4;
    
}
