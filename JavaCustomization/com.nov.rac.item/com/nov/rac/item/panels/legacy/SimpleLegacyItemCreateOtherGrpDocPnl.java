package com.nov.rac.item.panels.legacy;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.teamcenter.rac.kernel.TCException;

public class SimpleLegacyItemCreateOtherGrpDocPnl extends SimpleLegacyItemCreateDocPnl
{
    
    public SimpleLegacyItemCreateOtherGrpDocPnl(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean isCreateUI = false;
        isCreateUI = super.createUI();
        
        Composite legacyOtherGrpDocPnlComposite = getComposite();
        
        legacyOtherGrpDocPnlComposite.setLayout(new GridLayout(6, true));
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        legacyOtherGrpDocPnlComposite.setLayoutData(gd_panel);
        
        createDocSelectionRow(legacyOtherGrpDocPnlComposite);
        
        return isCreateUI;
    }
    
    /**
     * Creates the Seq No row.
     * 
     * @param legacyDbaGrpDocPnlComposite
     *            the composite for legacy document panel for DBA group
     * @throws TCException
     *             the tC exception
     */
    private void createDocSelectionRow(Composite legacyOtherGrpDocPnlComposite) throws TCException
    {
        new Label(legacyOtherGrpDocPnlComposite, SWT.NONE);
        
        Composite legacyDocSelComposite = new Composite(legacyOtherGrpDocPnlComposite, SWT.NONE);
        legacyDocSelComposite.setLayout(new GridLayout(4, true));
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 4, 1);
        legacyDocSelComposite.setLayoutData(gd_panel);
        
        Button m_radBtnDefiningDoc = new Button(legacyDocSelComposite, SWT.RADIO);
        GridData m_gd_radBtnDefiningDoc = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
        m_radBtnDefiningDoc.setText(getRegistry().getString("radBtnDefiningDoc.NAME", "Key Not Found"));
        m_radBtnDefiningDoc.setLayoutData(m_gd_radBtnDefiningDoc);
        
        Button m_radBtnRelatedDoc = new Button(legacyDocSelComposite, SWT.RADIO);
        GridData m_gd_radBtnRelatedDoc = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
        m_radBtnRelatedDoc.setText(getRegistry().getString("radBtnRelatedDoc.NAME", "Key Not Found"));
        m_radBtnRelatedDoc.setLayoutData(m_gd_radBtnRelatedDoc);
        
        new Label(legacyOtherGrpDocPnlComposite, SWT.NONE);
    }
    
}
