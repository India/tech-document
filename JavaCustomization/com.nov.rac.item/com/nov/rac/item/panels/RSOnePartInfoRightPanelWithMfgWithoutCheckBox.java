package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

public class RSOnePartInfoRightPanelWithMfgWithoutCheckBox extends RSOnePartInfoRightPanelWithMfg
{

    public RSOnePartInfoRightPanelWithMfgWithoutCheckBox(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue =  super.createUI();
        
        m_copyAllBtn.setVisible(false);
        m_orgCheckBox.dispose();
        m_nameCheckBox.dispose();
        m_classCheckBox.dispose();
        m_descCheckBox.dispose();
        m_uomCheckBox.dispose();
        m_traceabilityCheckBox.dispose();
        m_mfgCheckBox.dispose();
        m_mfgNumberCheckBox.dispose();
        m_sendToClassificationBtn.setEnabled(true);
        
        return returnValue;
    }
    
}
	