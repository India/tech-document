package com.nov.rac.item.panels;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class ComboCellRenderer implements TableCellRenderer
{
    private JComboBox<String> m_combo ;
    public ComboCellRenderer(JComboBox<String> m_combo2)
    {
        this.m_combo = new JComboBox<String>();
        for (int i=0; i<m_combo2.getItemCount(); i++){
            m_combo.addItem(m_combo2.getItemAt(i));
        }
        /*m_combo = new JComboBox<String>(new String []{"001","002","003"});
        m_combo.setSelectedIndex(2);*/
    }
    
    public void addActionListener(ActionListener actionListener)
    {
        m_combo.addActionListener(actionListener);
    }
    

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        m_combo.setSelectedItem(value);
        return m_combo;
    }
    
}
