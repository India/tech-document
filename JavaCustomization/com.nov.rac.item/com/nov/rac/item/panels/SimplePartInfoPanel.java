package com.nov.rac.item.panels;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.GenerateItemId;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.nov.rac.utilities.utils.UOMHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

/**
 * The Class PartInfoPanel.
 */
public class SimplePartInfoPanel extends AbstractUIPanel implements ILoadSave, IPublisher, ISubscriber
{
    
    /** The Constant POPULATE_DATA. */
    private final static PopulateData POPULATE_DATA;
    
    static
    {
        POPULATE_DATA = new PopulateData();
    }
    
    /**
     * The Class PopulateData.
     */
    private static class PopulateData
    {
        
        /** The session. */
        private static TCSession session;
        
        /**
         * Gets the session.
         * 
         * @return the session
         */
        protected TCSession getSession()
        {
            return (TCSession) AIFUtility.getCurrentApplication().getSession();
            
        }
        
        /**
         * Instantiates a new populate data.
         */
        public PopulateData()
        {
            session = getSession();
        }

		/**
		 * Gets the uOMLOV values.
		 * 
		 * @return the uOMLOV values
		 */
		private Vector<Object> getUOMLOVValues() 
		{
			Vector<Object> uomvector = new Vector<Object>();
			TCComponentListOfValues lovvalues = TCComponentListOfValuesType.findLOVByName(session, NationalOilwell.UOMLOV);
					
			if (lovvalues != null) 
			{
				ListOfValuesInfo lovInfo = null;
				try 
				{
					lovInfo = lovvalues.getListOfValues();
					Object[] lovS = lovInfo.getListOfValues();
					String[] lovDesc = lovInfo.getDescriptions();
					if (lovS.length > 0)
					{
						//KLOC227
						addValuesToUOMVector(uomvector, lovS, lovDesc);
					}
				} 
				catch (TCException e) 
				{
					e.printStackTrace();

				}

			}
			return uomvector;
		}

		private void addValuesToUOMVector(Vector<Object> uomvector, Object[] lovS,String[] lovDesc) 
		{
			for (int i = 0; i < lovS.length; i++)
			{
				String[] splitDesc = null;
				if (lovDesc != null && lovDesc[i] != null)
				{
					splitDesc = lovDesc[i].split(",");
				}
				for (int inx = 0; inx < splitDesc.length; inx++)
				{
					if (splitDesc != null && splitDesc[inx].trim().equalsIgnoreCase(NationalOilwell.GRPUOM)
							|| splitDesc[inx].trim().equalsIgnoreCase(""))
					{	
						uomvector.add(lovS[i]);
						break;
					}
				}
			}
		}
		
	}
    
    /**
     * Instantiates a new part info panel.
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public SimplePartInfoPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        m_registry = getRegistry();
        session = getSession();
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    /**
     * Gets the session.
     * 
     * @return the session
     */
    protected TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        Composite partInfoPanel = getComposite();
        partInfoPanel.setLayout(new GridLayout(5, true));
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        partInfoPanel.setLayoutData(gd_panel);
       // createSelectedItemPanelRow(partInfoPanel);
        createRadioButtons1Row(partInfoPanel);
        createTypeRow(partInfoPanel);
        createItemIdRow(partInfoPanel);
        createRevisionRow(partInfoPanel);
        createNameRow(partInfoPanel);
        createName2Row(partInfoPanel);
        createDescription(partInfoPanel);
        createProductLineRow(partInfoPanel);
        createProductCodeRow(partInfoPanel);
        createRadioButtons2Row(partInfoPanel);
        registerProperties();
        disableRightClick();
        return true;
    }
    
    /**
     * Creates the Selected Item Panel row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createSelectedItemPanelRow(Composite infoPanel)
    {
        /**
         * This method is added by nitin Objective - Reference for other
         * developers to test search functionality in their code. This method
         * should not be called in final code otherwise...
         */
        Label selItemLabel = new Label(infoPanel, SWT.NONE);
        selItemLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        selItemLabel.setText("Selected Item");
        
        Text selItemText = new Text(infoPanel, SWT.BORDER);
        selItemText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
        /*SearchButtonComponent searchButton = new SearchButtonComponent(infoPanel, SWT.TOGGLE);
        searchButton.setSearchItemType(new String[] { "Nov4Part", "Non-Engineering" });*/
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1);
        String [] searchItemTypes = {"Nov4Part", "Non-Engineering"};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(infoPanel, searchBtnGridData, searchItemTypes);
    }
    
    /**
     * Creates the radio buttons2 row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createRadioButtons2Row(Composite infoPanel)
    {
    	new Label(infoPanel, SWT.NONE);
        Composite radio2Composite = new Composite(infoPanel, SWT.NONE);
        radio2Composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true, 4, 1));
        radio2Composite.setLayout(new GridLayout(3, false));
        m_noneRadio = new Button(radio2Composite, SWT.RADIO);
        m_noneRadio.setText(m_registry.getString("PartInfoPanel.None"));
        m_noneRadio.setSelection(true);
        
        m_serializedRadio = new Button(radio2Composite, SWT.RADIO);
        m_serializedRadio.setText(m_registry.getString("PartInfoPanel.Serialized"));
        
        m_lotControlRadio = new Button(radio2Composite, SWT.RADIO);
        m_lotControlRadio.setText(m_registry.getString("PartInfoPanel.LotControl"));
        
    }
    
    /**
     * Creates the product code row.
     * 
     * @param infoPanel
     *            the info panel
     * @throws TCException
     */
    private void createProductCodeRow(Composite infoPanel) throws TCException
    {
        m_productCodeLabel = new Label(infoPanel, SWT.NONE);
        m_productCodeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_productCodeLabel.setText(m_registry.getString("PartInfoPanel.ProductCode"));
        
        m_productCodeCombo = new Combo(infoPanel, SWT.DROP_DOWN);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, true, 3, 1);
        gridData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_productCodeCombo, NationalOilwell.COMBO_FIELD_SIZE);
        m_productCodeCombo.setLayoutData(gridData);
        TCComponentListOfValues productCodeLov = TCComponentListOfValuesType.findLOVByName(session,
                NationalOilwell.PRODUCTCODELOV);
        UIHelper.setAutoComboArray(m_productCodeCombo, LOVUtils.getLovStringValues(productCodeLov));
        new Label(infoPanel, SWT.NONE);
      //  new Label(infoPanel, SWT.NONE);
        
    }
    
    /**
     * Creates the product line row.
     * 
     * @param infoPanel
     *            the info panel
     * @throws TCException
     */
    private void createProductLineRow(Composite infoPanel) throws TCException
    {
        m_productLineLabel = new Label(infoPanel, SWT.NONE);
        m_productLineLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_productLineLabel.setText(m_registry.getString("PartInfoPanel.ProductLine"));
        
        m_productLineCombo = new Combo(infoPanel, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, true, 3, 1);
        gridData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_productLineCombo, NationalOilwell.COMBO_FIELD_SIZE);
        m_productLineCombo.setLayoutData(gridData);
        TCComponentListOfValues productLineLov = TCComponentListOfValuesType.findLOVByName(session,
                NationalOilwell.PRODUCTLINELOV);
        UIHelper.setAutoComboArray(m_productLineCombo, LOVUtils.getLovStringValues(productLineLov));
        new Label(infoPanel, SWT.NONE);
        
    }
    
    /**
     * Creates the description.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createDescription(Composite infoPanel)
    {
        m_descriptionLabel = new Label(infoPanel, SWT.NONE);
        m_descriptionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 3));
        m_descriptionLabel.setText(m_registry.getString("PartInfoPanel.Description"));
        
        m_descriptionText = new Text(infoPanel, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
        
        m_descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 3, 3));
        m_descriptionText.setTextLimit(240);
        UIHelper.convertToUpperCase(m_descriptionText);
        new Label(infoPanel, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 3));
        
    }
    
    /**
     * Creates the name2 row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createName2Row(Composite infoPanel)
    {
        m_name2Label = new Label(infoPanel, SWT.NONE);
        m_name2Label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_name2Label.setText(m_registry.getString("PartInfoPanel.Name2"));
        
        m_name2Text = new Text(infoPanel, SWT.BORDER);
        UIHelper.convertToUpperCase(m_name2Text);
        m_name2Text.setTextLimit(30);
        m_name2Text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
        new Label(infoPanel, SWT.NONE);
        
    }
    
    /**
     * Creates the name row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createNameRow(Composite infoPanel)
    {
        m_nameLabel = new Label(infoPanel, SWT.NONE);
        m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_nameLabel.setText(m_registry.getString("PartInfoPanel.Name"));
        
        m_nameText = new Text(infoPanel, SWT.BORDER);
        m_nameText.setTextLimit(30);
        UIHelper.convertToUpperCase(m_nameText);
        m_nameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
        new Label(infoPanel, SWT.NONE);
        
    }
    
    /**
     * Creates the revision row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createRevisionRow(Composite infoPanel)
    {
        m_revisionLabel = new Label(infoPanel, SWT.NONE);
        m_revisionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_revisionLabel.setText(m_registry.getString("PartInfoPanel.Revision"));
        
        m_revisionText = new Text(infoPanel, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        m_revisionText.setLayoutData(gridData);
        m_revisionText.setEditable(false);
       // m_revisionText.setEnabled(false);//TCDECREL-6754
        m_revisionText.setText("01");
        m_revisionText.setTextLimit(NationalOilwell.REV_TEXT_LIMIT);
        
        m_uomLabel = new Label(infoPanel, SWT.NONE);
        m_uomLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        m_uomLabel.setText(m_registry.getString("PartInfoPanel.UOM"));
        
        m_uomCombo = new Combo(infoPanel, SWT.NONE);
        m_uomCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        UIHelper.setAutoComboCollection(m_uomCombo, POPULATE_DATA.getUOMLOVValues());
        new Label(infoPanel, SWT.NONE);
        
    }
    
    /**
     * Creates the item id row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createItemIdRow(Composite infoPanel)
    {
        m_itemIdLabel = new Label(infoPanel, SWT.NONE);
        m_itemIdLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_itemIdLabel.setText(m_registry.getString("PartInfoPanel.ItemID"));
        
        m_itemIdText = new Text(infoPanel, SWT.BORDER);
        m_itemIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1));
        m_itemIdText.setTextLimit(25);
        UIHelper.convertToUpperCase(m_itemIdText);
        m_assignButton = new Button(infoPanel, SWT.NONE);
        m_assignButton.setText(m_registry.getString("PartInfoPanel.Assign"));
        GridData assignBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        assignBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(m_assignButton,NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON );
        m_assignButton.setLayoutData(assignBtnGridData);
        m_assignButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent theEvent)
            {
                m_itemIdText.setText(GenerateItemId.assignItemId());
            }
        });
        
    }
    
    /**
     * Creates the type row.
     * 
     * @param infoPanel
     *            the info panel
     * @throws TCException
     */
    private void createTypeRow(Composite infoPanel) throws TCException
    {
        m_typeLabel = new Label(infoPanel, SWT.NONE);
        m_typeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_typeLabel.setText(m_registry.getString("PartInfoPanel.Type"));
        
        m_typeCombo = new Combo(infoPanel, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1);
        gridData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_typeCombo, NationalOilwell.COMBO_FIELD_SIZE);
        m_typeCombo.setLayoutData(gridData);
        m_typeCombo.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent e)
            {
                widgetSelected_TypesCombo(e);
                
            }
        });
        
        InitializeTypeCombo();
        // MandatoryField.markMandatoryField(m_typeCombo);
        new Label(infoPanel, SWT.NONE);
        
    }
    
    private void InitializeTypeCombo() throws TCException
    {
        TCComponentListOfValues typeLov = TCComponentListOfValuesType.findLOVByName(session,
                NationalOilwell.MISSIONTYPELOV);
        String[] types = LOVUtils.getLovStringValues(typeLov);
        if (types != null)
        {
            String[] typesActualNames = new String[types.length];
            for (int i = 0; i < types.length; i++)
            {
                typesActualNames[i] = m_registry.getString(types[i] + NationalOilwell.DOTSEPARATOR
                        + NationalOilwell.NAME);
                m_typeCombo.setData(types[i], typesActualNames[i]);
                
            }
            UIHelper.setAutoComboArray(m_typeCombo, types);
        }
    }
    
    protected void widgetSelected_TypesCombo(ModifyEvent e)
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        //IController theController = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        String actualName = (String) m_typeCombo.getData(m_typeCombo.getText());
        theController.publish(new PublishEvent(this, NationalOilwell.ITEM_TYPE, actualName, ""));
    }
    
    /**
     * Creates the radio buttons1 row.
     * 
     * @param infoPanel
     *            the info panel
     */
    private void createRadioButtons1Row(Composite infoPanel)
    {
        new Label(infoPanel, SWT.NONE);
        Composite radio1Composite = new Composite(infoPanel, SWT.NONE);
        radio1Composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 4, 1));
        radio1Composite.setLayout(new GridLayout(3, false));
        m_makeItemRadio = new Button(radio1Composite, SWT.RADIO);
        m_makeItemRadio.setText(m_registry.getString("PartInfoPanel.MakeItem"));
        m_makeItemRadio.setSelection(true);
        
        m_purchasedRadio = new Button(radio1Composite, SWT.RADIO);
        m_purchasedRadio.setText(m_registry.getString("PartInfoPanel.Purchased"));
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#reload()
     */
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see com.nov.rac.ui.ILoadSave#load(com.nov.rac.propertymap.IPropertyMap)
	 */
	 @Override
	    public boolean load(IPropertyMap propMap) throws TCException
	    {
	        String rsone_itemType = PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.TYPE));
	        if (m_makeItemRadio.getText().equals(rsone_itemType))
	        {
	            m_makeItemRadio.setSelection(true);
	        }
	        else if (m_purchasedRadio.getText().equals(rsone_itemType))
	        {
	            m_purchasedRadio.setSelection(true);
	            m_makeItemRadio.setSelection(false);
	        }
	        m_itemIdText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID).split(":")[0]));
	        m_uomCombo.setText(PropertyMapHelper.handleNull(propMap.getTag(NationalOilwell.NOV_UOM)));
	        m_nameText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_NAME)));
	        
	        m_descriptionText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_DESC)));
	        
	        // master form properties
	        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
	        if (masterPropertyMap != null)
	        {
	        	//KLOC230
	            loadMasterFormProperties(masterPropertyMap);
	        }
	        
	        return true;
	    }

	private void loadMasterFormProperties(IPropertyMap masterPropertyMap)
	{
		m_typeCombo.setText(PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.MIS_TYPE)));
		m_productLineCombo.setText(PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.NOV4PRODUCTLINE)));
		
		m_productCodeCombo.setText(PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.NOV4PRODUCTCODE)));
		
		m_name2Text.setText(PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.NAME2)));
		
		String mis_serialize = PropertyMapHelper.handleNull(masterPropertyMap.getString(NationalOilwell.MIS_SERIALIZE));
		
		if (m_noneRadio.getText().equals(mis_serialize))
		{
		    m_noneRadio.setSelection(true);
		    m_lotControlRadio.setSelection(false);
		    m_serializedRadio.setSelection(false);
		}
		else if (m_lotControlRadio.getText().equals(mis_serialize))
		{
		    m_lotControlRadio.setSelection(true);
		    m_noneRadio.setSelection(false);
		    m_serializedRadio.setSelection(false);
		}
		else if (m_serializedRadio.getText().equals(mis_serialize))
		{
		    m_serializedRadio.setSelection(true);
		    m_noneRadio.setSelection(false);
		    m_lotControlRadio.setSelection(false);
		}
	}

	    /*
	     * (non-Javadoc)
	     * @see com.nov.rac.ui.ILoadSave#save(com.nov.rac.propertymap.IPropertyMap)
	     */
	    @Override
	    public boolean save(IPropertyMap propMap) throws TCException
	    {
	        String itemTypeProp;
	        if (m_makeItemRadio.getSelection())
	            itemTypeProp = m_makeItemRadio.getText();
	        else
	            itemTypeProp = m_purchasedRadio.getText();
	        
	        //KLOC228
	        String serialiseOption;
	        serialiseOption = getSerialiseOption();
	        
	        // item properties
	        setItemProperties(propMap);
	        
	        // master form properties (Added propMap.geType() as parameter as per latest TC83 code)
	        setMasterFormProperties(propMap, itemTypeProp, propMap.getType() );
	        
	        // item revision properties
	        setRevisionProperties(propMap);
	        
	        return true;
	    }

		private String getSerialiseOption() 
		{
			String serialiseOption;
			if (m_lotControlRadio.getSelection())
			{
				serialiseOption = m_lotControlRadio.getText();
				
			}
	        else if (m_serializedRadio.getSelection())
	        {
	        	serialiseOption = m_serializedRadio.getText();
	        }
	        else
	        {
	        	 serialiseOption = m_noneRadio.getText();
	        }
	           
			return serialiseOption;
		}

		private void setItemProperties(IPropertyMap propMap) throws TCException
		{
			propMap.setString("item_id", m_itemIdText.getText().trim());
	        propMap.setTag(NationalOilwell.NOV_UOM, UOMHelper.getUOMObject(m_uomCombo.getText()));
	        propMap.setString("object_name", m_nameText.getText().trim());
	        propMap.setString("object_desc", m_descriptionText.getText().trim());
		}

		private void setMasterFormProperties(IPropertyMap propMap,String itemTypeProp, String objectType)
		{
			IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
	        if (masterPropertyMap != null)
	        {
	        	String serialiseOption = getSerialiseOption();
	        	
	        	if(objectType.compareTo( NationalOilwell.PART_BO_TYPE) == 0)
	        	{
	        		masterPropertyMap.setString("rsone_itemtype", itemTypeProp);	
	        	}	
	            masterPropertyMap.setString(NationalOilwell.MIS_TYPE, m_typeCombo.getText());
	            masterPropertyMap.setString("nov4_productline", m_productLineCombo.getText());
	            masterPropertyMap.setString("nov4_productcode", m_productCodeCombo.getText());
	            masterPropertyMap.setString("nov4_mis_serialize", serialiseOption);
	            masterPropertyMap.setString("Name2", m_name2Text.getText().trim());
	            propMap.setCompound("IMAN_master_form", masterPropertyMap);
	        }
		}

		private void setRevisionProperties(IPropertyMap propMap)
		{
			IPropertyMap revisionPropertyMap = propMap.getCompound("revision");
	        if (revisionPropertyMap != null)
	        {
	            revisionPropertyMap.setString("item_revision_id", m_revisionText.getText());
	            propMap.setCompound("revision", revisionPropertyMap);
	        }
		}

    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.ui.ILoadSave#validate(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        //IController l_controller = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        l_controller.registerSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED, getSubscriber());
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.ONAPPLY_OBJECT_CREATED))
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    if (new Boolean(event.getNewValue().toString()))
                    {
                        m_itemIdText.setText("");
                    }
                }
            });
        }
    }
    
    ISubscriber getSubscriber()
    {
        return (ISubscriber) this;
    }
    
    @Override
    public void dispose()
    {
    	IController l_controller = ControllerFactory.getInstance().getDefaultController();
        //IController l_controller = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        l_controller.unregisterSubscriber(NationalOilwell.ONAPPLY_OBJECT_CREATED, getSubscriber());
        super.dispose();
    }
    
    private void disableRightClick()
    {
    	NewItemCreationHelper.disableRightClick( m_itemIdText );
    	NewItemCreationHelper.disableRightClick( m_typeCombo );
    	NewItemCreationHelper.disableRightClick( m_nameText );
    	NewItemCreationHelper.disableRightClick( m_name2Text );
    	NewItemCreationHelper.disableRightClick( m_descriptionText );
    	NewItemCreationHelper.disableRightClick( m_productCodeCombo );
    	NewItemCreationHelper.disableRightClick( m_productLineCombo );
    	NewItemCreationHelper.disableRightClick(m_uomCombo);
    	NewItemCreationHelper.disableRightClick(m_revisionText);
    }
    
    protected Button m_makeItemRadio;
    
    protected Button m_purchasedRadio;
    
    private Button m_assignButton;
    
    /** The m_none radio. */
    private Button m_noneRadio;
    
    /** The m_serialized radio. */
    private Button m_serializedRadio;
    
    /** The m_lot control radio. */
    private Button m_lotControlRadio;
    
    /** The m_revision label. */
    private Label m_revisionLabel;
    
    /** The m_uom label. */
    private Label m_uomLabel;
    
    /** The m_item id label. */
    private Label m_itemIdLabel;
    
    /** The m_type label. */
    private Label m_typeLabel;
    
    /** The m_name label. */
    private Label m_nameLabel;
    
    /** The m_name2 label. */
    private Label m_name2Label;
    
    /** The m_description label. */
    private Label m_descriptionLabel;
    
    /** The m_product line label. */
    private Label m_productLineLabel;
    
    /** The m_product code label. */
    private Label m_productCodeLabel;
    
    /** The m_type combo. */
    protected Combo m_typeCombo;
    
    /** The m_revision text. */
    protected Text m_revisionText;
    
    /** The m_item id text. */
    protected Text m_itemIdText;
    
    /** The m_name text. */
    protected Text m_nameText;
    
    /** The m_name2 text. */
    protected Text m_name2Text;
    
    /** The m_description text. */
    protected Text m_descriptionText;
    
    /** The m_uom combo. */
    protected Combo m_uomCombo;
    
    /** The m_product line combo. */
    protected Combo m_productLineCombo;
    
    /** The m_product code combo. */
    protected Combo m_productCodeCombo;
    
    /** The m_registry. */
    protected Registry m_registry;
    
    /** The session. */
    private TCSession session;
    
    
}
