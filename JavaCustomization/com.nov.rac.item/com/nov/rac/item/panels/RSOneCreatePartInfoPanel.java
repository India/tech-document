package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreatePartInfoPanel extends RSOneSimplePartInfoPanel implements ILoadSave
{

    /**
     * @wbp.parser.entryPoint
     */
    public RSOneCreatePartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
       
    }
    @Override
    public boolean createUI() throws TCException
    {
        Composite partInfoPanel = getComposite();
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        partInfoPanel.setLayoutData(gd_panel);
        partInfoPanel.setLayout(new GridLayout(5, true));
        
        createLeftPanel(partInfoPanel);
        
        createRightPanel(partInfoPanel);
        IPropertyMap propertyMap = new SOAPropertyMap(NationalOilwell.PART_BO_TYPE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(propertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        m_RSOnePartInfoRightPanel.save(propertyMap);
        
        dashPropertyMap.put("001", propertyMap);
        
        currentSelectedDash = "001";
        
        registerSubscriber();
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
      //  m_RSOnePartInfoRightPanel.save(propMap);
        IPropertyMap propertyMap = dashPropertyMap.get(currentSelectedDash);
        m_RSOnePartInfoRightPanel.save(propertyMap);
        dashPropertyMap.put(currentSelectedDash, propertyMap);
        IPropertyMap[] propMapArray = new SOAPropertyMap[dashPropertyMap.size()];
        propMap.setCompoundArray("dashPropertyMap", dashPropertyMap.values().toArray(propMapArray));
        
        //dashMapSaved = true;
        
        return true;
    }
}
