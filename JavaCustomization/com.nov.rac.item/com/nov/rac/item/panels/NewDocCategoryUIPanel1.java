package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class NewDocCategoryUIPanel1 extends SimpleNewDocCategoryUIPanel1
{
    
    public NewDocCategoryUIPanel1(Composite parent, int style)
    {
        super(parent, style);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
       
        return returnValue;
    }
    
    // TCDECREL-6756 Start Red asterisk issue
    @Override
    public boolean createUIPost() throws TCException
    {
        UIHelper.makeMandatory(m_docTypeLovPopupButton.getPopupButton());
        UIHelper.makeMandatory(m_textDocCategory);
		return false;  
    }
    // TCDECREL-6756 End
    
}
