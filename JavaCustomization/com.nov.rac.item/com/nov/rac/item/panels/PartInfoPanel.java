package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class PartInfoPanel extends SimplePartInfoPanel
{

    public PartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException 
    {
        boolean returnValue = super.createUI();
        
        return returnValue;
    }
    
    // TCDECREL-6756 Start Red asterisk issue
    @Override
    public boolean createUIPost() throws TCException
    {
        UIHelper.makeMandatory(m_typeCombo);
        UIHelper.makeMandatory(m_itemIdText);
        UIHelper.makeMandatory(m_nameText);
        UIHelper.makeMandatory(m_uomCombo);
        m_typeCombo.setText(m_registry.getString("PartInfoPanel.TypeDefaultValue"));
        //m_uomCombo.setText("EA");
        int index = m_uomCombo.indexOf(UOM_EA);
        m_uomCombo.select(index);
        
        return false;
    }
    // TCDECREL-6756 End
    
    private static final String UOM_EA = "EA";
    
}