/* ============================================================
   
   	File description: 

  	 Filename: NewDocCategoryUIPanel1.java
  	 Module  : <name of the module/folder where this file resides> 

   	<Short Description of the file> 

   	Date         Developers    Description
   	16-08-2012   snehac    Initial creation

   	$HISTORY
 ============================================================ */
package com.nov.rac.item.panels;

import static com.nov.rac.item.helpers.DocumentHelper.getDocCategory;
import static com.nov.rac.item.helpers.DocumentHelper.getDocumentTypes;
import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.awt.AWTException;
import java.awt.Robot;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DocCategoryUIModel;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.contentadapter.TextboxContentAdapter;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.NIDLovPopupDialog;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.common.controls.SWTLovPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.InterfaceExceptionStack;
import com.teamcenter.rac.util.Registry;

/**
 * The Class NewDocCategoryUIPanel1.
 */
public class SimpleNewDocCategoryUIPanel1 extends AbstractUIPanel implements ILoadSave, IPublisher
{
    
    protected Composite subComposite;
    
    /**
     * Instantiates a NewDocCategoryUIPanel1.
     * 
     * @param parent
     *            the parent
     * @param style
     *            the style
     */
    public SimpleNewDocCategoryUIPanel1(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#createUI()
     */
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite documentComposite = getComposite();
        documentComposite.setLayout(new GridLayout(1, true));
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        documentComposite.setLayoutData(gd_panel);
        createDocumentHeader(documentComposite);
        subComposite = SWTUIHelper.createRowComposite(documentComposite, 3, false);
        
        createDocCategoryRow(subComposite);
        createDocTypeRow(subComposite);
        createPullDocRow(subComposite);
        // TCDECREL-6370 disable right mouse click
        disableRightClick();
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#load(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_textDocCategory.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)));
        m_BtnDocType.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.DOCUMENTS_TYPE)));
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            if (masterPropertyMap.getLogical(NationalOilwell.PULL_DRAWING))
            {
                int l_index = m_ComboPullDoc.indexOf("yes");
                if (l_index != -1)
                {
                    m_ComboPullDoc.select(l_index);
                }
            }
            else
            {
                int l_index = m_ComboPullDoc.indexOf("no");
                m_ComboPullDoc.select(l_index);
            }
            
        }
        
        moveDocTypeButton();
        
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.ILoadSave#save(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        PropertyMapHelper.setString(NationalOilwell.DOCUMENTS_CATEGORY, m_textDocCategory.getText(), propMap);
        PropertyMapHelper.setString(NationalOilwell.DOCUMENTS_TYPE, m_BtnDocType.getText(), propMap);
        
        // master form properties
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            masterPropertyMap.setLogical(NationalOilwell.PULL_DRAWING, UIHelper.getBoolean(m_ComboPullDoc.getText()));
            
            if( m_textDocCategory.getText().equalsIgnoreCase( NationalOilwell.DOCCAT_CAL ) )
            {
            	masterPropertyMap.setLogical(NationalOilwell.SECURE, true);
            }
            
            propMap.setCompound("IMAN_master_form", masterPropertyMap);
        }
        return true;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.ui.ILoadSave#validate(com.nov.rac.propertymap.IPropertyMap)
     */
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        if (!hasContent(propMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)))
        {
            TCException e = new TCException("Document Category is mandatory field");
            stack.addErrors(e);
        }
        if (!hasContent(propMap.getString(NationalOilwell.DOCUMENTS_TYPE)))
        {
            TCException e = new TCException("Document Type is mandatory field");
            stack.addErrors(e);
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            InterfaceExceptionStack intStack = stack;
            TCException newExp = new TCException(intStack.getErrorSeverities(), intStack.getErrorCodes(),
                    intStack.getErrorStack());
            
            throw newExp;
        }
        
        return true;
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.AbstractUIPanel#reload()
     */
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    /**
     * Toggle doc type button.
     * 
     * @param flag
     *            the flag
     */
    protected void toggleDocTypeButton(boolean flag)
    {
        if (null != m_BtnDocType)
        {
            m_BtnDocType.setEnabled(flag);
            
        }
        
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    /**
     * function will enable or disable textfield and combo depending on which
     * radio button is selected.
     * 
     * @param flag
     *            the flag
     */
    protected void toggleState(boolean flag)
    {
        this.m_textDocCategory.setEnabled(flag);
        this.m_ComboPullDoc.setEnabled(flag);
        if (null != m_BtnDocCategory)
            this.m_BtnDocCategory.setEnabled(flag);
        
    }
    
    /**
     * will reset document category composite if copy doc or copy RDD radio
     * button is selected.
     */
    protected void clear()
    {
        if (null != this.m_textDocCategory)
            this.m_textDocCategory.setText("");
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        publishDocCategory_Type();
    }
    
    protected void resetDocType()
    {
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        publishDocCategory_Type();
    }
    
    private void publishDocCategory_Type()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        // IController controller =
        // ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        DocCategoryUIModel docCategoryUIModel = new DocCategoryUIModel();
        docCategoryUIModel.setDocumentCategory(m_textDocCategory.getText());
        docCategoryUIModel.setDocumentType(m_BtnDocType.getText());
        docCategoryUIModel.setDocCategoryTextEnable(m_textDocCategory.isEnabled());
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.DOCCATEGORY_TYPE, docCategoryUIModel,
                null);
        
        controller.publish(event);
        
    }
    
    protected TCComponentListOfValues getDocCategoryLOV()
    {
        return getDocCategory(NationalOilwell.DOCUMENT_TYPES_LOV);
    }
    
    /**
     * Creates the doc category row.
     * 
     * @param documentComposite
     *            the document composite
     * @throws TCException
     *             the tC exception
     */
    
    protected void createDocumentHeader(final Composite documentComposite) throws TCException
    {
        
        Label l_lblRDDHeader = new Label(documentComposite, SWT.NONE);
        l_lblRDDHeader.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        l_lblRDDHeader.setText(m_registry.getString("docType.Header", "Key Not Found"));// "Doc Category"
        
    }
    
    protected void createDocCategoryRow(final Composite parent) throws TCException
    {
        
        Label m_lblDocCategory = new Label(parent, SWT.NONE);
        // m_lblDocCategory.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
        // false, false, 1, 1));
        m_lblDocCategory.setText(m_registry.getString("docCategory.NAME", "Key Not Found"));// "Doc Category"
        
        Composite createRowComposite = SWTUIHelper.createRowComposite(parent, 2, false);
        createRowComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
        
        m_textDocCategory = new Text(createRowComposite, SWT.BORDER);
        GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_textDocCategory.setLayoutData(gd_text);
        m_textDocCategory.setToolTipText(m_registry.getString("docCategoryEditor.TOOLTIP"));
        
        // GridData m_gd_txtDocCat = new GridData(SWT.FILL, SWT.CENTER, true,
        // true, 3, 1);
        //
        // m_textDocCategory.setLayoutData(m_gd_txtDocCat);
        m_lovValuesForDocCategory = LOVUtils.getLovStringValues(getDocCategoryLOV());
        new AutoCompleteField(m_textDocCategory, new TextboxContentAdapter(Arrays.asList(m_lovValuesForDocCategory)),
                m_lovValuesForDocCategory);
        addingFocusListenerOnDocCategoryText(m_textDocCategory);
        addingListenerOnDocCategoryText(m_textDocCategory);
        createDocCategoryLovPopupButton(createRowComposite);
        
    }
    
    /**
     * Creates the doc type row.
     * 
     * @param documentComposite
     *            the document composite
     * @throws TCException
     *             the tC exception
     */
    protected void createDocTypeRow(final Composite documentComposite) throws TCException
    {
        
        Label m_lblDocType = new Label(documentComposite, SWT.NONE);
        m_lblDocType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        m_lblDocType.setText(m_registry.getString("docType.NAME", "Key Not Found"));
        
        createDocTypeLovPopupButton(documentComposite);
        
    }
    
    /**
     * Creates the pull doc row.
     * 
     * @param parent
     *            the document composite
     */
    protected void createPullDocRow(Composite parent)
    {
        
        Label m_lblPullDoc = new Label(parent, SWT.NONE);
        // m_lblPullDoc.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
        // false, 1, 1));
        m_lblPullDoc.setText(m_registry.getString("pullLabel.NAME", "Key Not Found"));
        m_ComboPullDoc = new Combo(parent, SWT.NONE);
        
      	//Tushar_Start (TCDECREL-6808)
        m_ComboPullDoc.addFocusListener(new FocusAdapter()
        {
        	 @Override
             public void focusLost(FocusEvent arg0)
             {
                 if(m_ComboPullDoc.getText().equalsIgnoreCase(""))
                 {
                     m_ComboPullDoc.setText("no");
                 }
             }
		});
        //Tushar_End
        
        GridData gd_PullDoc = new GridData(SWT.FILL, SWT.CENTER, true, true, 2, 1);
        gd_PullDoc.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_ComboPullDoc, NationalOilwell.COMBO_FIELD_SIZE);
        
        m_ComboPullDoc.setLayoutData(gd_PullDoc);
        
        // String[] values = m_registry.getStringArray("pullComboBox.VALUE");
        String[] values = new String[] { "yes", "no" };
        UIHelper.setAutoComboArray(m_ComboPullDoc, values);
        setSelectedValueInCombo(m_ComboPullDoc, m_registry.getString("pullComboBox.DEFAULTVALUE"));
        new Label(parent, SWT.NONE);
    }
    
    private void setSelectedValueInCombo(Combo combo, String value)
    {
        int l_index = combo.indexOf(value);
        if (l_index != -1)
        {
            combo.select(l_index);
        }
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    IPublisher getPublisher()
    {
        return this;
        
    }
    
    /**
     * Adding listener on doc category text.
     * 
     * @param m_textDocCategory
     *            the m_text doc category
     */
    private void addingListenerOnDocCategoryText(final Text m_textDocCategory)
    {
        m_textDocCategory.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                if (isModifiedByLOV)
                {
                    isModifiedByLOV = false;
                    return;
                }
                
                String value = m_textDocCategory.getText();
                if (null != value && value.length() > 0)
                {
                    if (value.indexOf('-') == HIPHEN_POSITION && value.length() > HIPHEN_POSITION)
                    {
                        isModifiedByLOV = true;
                        setTextForDocCategoryAndDocType(m_textDocCategory.getText());
                    }
                    else if (value.length() > 3)
                    {
                        
                        toggleDocTypeButton(false);
                        clear();
                    }
                    else
                    {
                        if (value.length() <= 3)
                        {
                            if (!value.equals(m_textDocCategory.getText().toUpperCase()))
                            {
                                m_textDocCategory.setText(m_textDocCategory.getText().toUpperCase());
                            }
                            m_textDocCategory.setSelection(m_textDocCategory.getText().length());
                            toggleDocTypeButton(false);
                            resetDocType();
                        }
                    }
                }
                else
                {
                    toggleDocTypeButton(false);
                    resetDocType();
                }
                moveDocTypeButton();
            }
        });
        
        addKeyListenerOnDocCategoryTextField(m_textDocCategory);
    }
    
    private void addKeyListenerOnDocCategoryTextField(final Text m_textDocCategory)
    {
        m_textDocCategory.addKeyListener(new KeyListener()
        {
            
            @Override
            public void keyReleased(KeyEvent keyevent)
            {
                
            }
            
            @Override
            public void keyPressed(KeyEvent keyevent)
            {
                int keyCode = keyevent.keyCode;
                if (keyCode == SWT.TAB)
                {
                    isTabPressed = true;
                }
                else
                {
                    isTabPressed = false;
                }
            }
        });
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.framework.communication.IPublisher#getData()
     */
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    /**
     * Sets the text for doc category and doc type.
     * 
     * @param value
     *            the new text for doc category and doc type
     */
    private void setTextForDocCategoryAndDocType(String value)
    {
        if (hasContent(value) && value.length() > (HIPHEN_POSITION + 1))
        {
            final String textValue = value.substring(0, HIPHEN_POSITION).trim();
            final String buttonCaption = value.substring(HIPHEN_POSITION + 1).trim();
            
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    if (m_textDocCategory.isEnabled()) // TCDECREL-6566
                    {
                        m_textDocCategory.setText(textValue);
                        m_BtnDocType.setText(buttonCaption);
                        m_BtnDocType.setEnabled(true);
                        setDocTypeLovComponent(m_BtnDocType, textValue);
                        publishDocCategory_Type();
                        m_textDocCategory.setSelection(m_textDocCategory.getText().length());
                        moveDocTypeButton();
                        
                        Display.getDefault().asyncExec(new Runnable()
                        {
                            public void run()
                            {
                                try
                                {
                                    if (m_textDocCategory.setFocus() && m_textDocCategory.isFocusControl())
                                    {
                                        Robot robot = new Robot();
                                        robot.keyPress(java.awt.event.KeyEvent.VK_INSERT);
                                        robot.keyRelease(java.awt.event.KeyEvent.VK_INSERT);
                                    }
                                    
                                }
                                catch (AWTException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    
                }
            });
        }
    }
    
    /**
     * Creates the doc category lov popup button.
     * 
     * @param current
     *            the current
     * @throws TCException
     *             the tC exception
     */
    private void createDocCategoryLovPopupButton(Composite current) throws TCException
    {
        
        TCComponent l_tcComponent = null;
        m_BtnDocCategory = new Button(current, SWT.NONE);
        GridData lovBtngridData = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
        lovBtngridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(m_BtnDocCategory,
                NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
        m_BtnDocCategory.setLayoutData(lovBtngridData);
        m_BtnDocCategory.setToolTipText(m_registry.getString("docCategoryPopup.TOOLTIP", "Key Not Found "));
        
        m_LovPopupButtonImage = new Image(current.getDisplay(),
                m_registry.getImage("NewDocCategoryUIPanel1.LOVButtonImage"), SWT.NONE);
        
        final NIDLovPopupDialog docCategoryDialog = new NIDLovPopupDialog(current.getShell(), SWT.NONE,
                getDocCategoryLOV(), "Default");
        
        docCategoryDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    isModifiedByLOV = true;
                    setTextForDocCategoryAndDocType(propertychangeevent.getNewValue().toString());
                    
                }
            }
        });
        
        m_docCategoryPopupButton = new SWTLovPopupButton(m_BtnDocCategory, docCategoryDialog, getDocCategoryLOV(), "",
                l_tcComponent);
        
        m_docCategoryPopupButton.setIcon(m_LovPopupButtonImage);
        
    }
    
    /**
     * Creates the DocTypeLOVPopupButton.
     * 
     * @param documentComposite
     *            the document composite
     * @throws TCException
     *             the tC exception
     */
    private void createDocTypeLovPopupButton(Composite documentComposite) throws TCException
    {
        
        TCComponent l_tcComponent = null;
        m_BtnDocType = new Button(documentComposite, SWT.NONE);
        m_BtnDocType.setToolTipText(m_registry.getString("docTypePopup.TOOLTIP", "Key Not Found "));
        m_BtnDocType.setEnabled(false);
        
        docTypeDialog = new NIDLovPopupDialog(documentComposite.getShell(), SWT.NONE, getDocCategoryLOV(), "Default");
        m_docTypeLovPopupButton = new SWTLovPopupButton(m_BtnDocType, docTypeDialog, getDocCategoryLOV(), "Default",
                l_tcComponent);
        GridData gd_DocType = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
        
        gd_DocType.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_textDocCategory, 140);
        m_docTypeLovPopupButton.getPopupButton().setLayoutData(gd_DocType);
        m_docTypeLovPopupButton.setIcon(m_LovPopupButtonImage);
        
        docTypeDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(final PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        public void run()
                        {
                            m_docTypeLovPopupButton.setText(propertychangeevent.getNewValue().toString());
                            docTypeDialog.setLovPopupButton((m_docTypeLovPopupButton));
                            publishDocCategory_Type();
                        }
                    });
                }
            }
        });
        
    }
    
    /**
     * Sets the doc type lov component.
     * 
     * @param m_btnDocType
     *            the m_btn doc type
     * @param docCategoryLovValueSelected
     *            the doc category lov value selected
     */
    private void setDocTypeLovComponent(Button m_btnDocType, final String docCategoryLovValueSelected)
    {
        
        try
        {
            docTypeDialog.setLovComponent(getDocumentTypes(docCategoryLovValueSelected));
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    protected void disableRightClick()
    {
        
        NewItemCreationHelper.disableRightClick(m_textDocCategory);
        NewItemCreationHelper.disableRightClick(m_ComboPullDoc);
    }
    
    /**
     * Fire a m_ButDocType move event.
     */
    protected void moveDocTypeButton()
    {
        Rectangle rect = m_BtnDocType.getBounds();
        m_BtnDocType.setLocation(rect.x + 1, rect.y);
        m_BtnDocType.setLocation(rect.x, rect.y);
    }
    
    private void addingFocusListenerOnDocCategoryText(final Text m_textDocCategory)
    {
        m_textDocCategory.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent focusEvent)
            {
                m_textDocCategory.getDisplay().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if (!isTabPressed)
                        {
                            setDocCategoryOnFocusLost(m_textDocCategory);
                        }
                        else
                        {
                            String comboValue = m_textDocCategory.getText();
                            if (!Arrays.asList(m_lovValuesForDocCategory).contains(comboValue))
                            {
                                if (comboValue.length() > 0)
                                {
                                    String[] filteredProposals = filterProposals(m_lovValuesForDocCategory, comboValue);
                                    m_textDocCategory.setText(filteredProposals[0]);
                                }
                            }
                        }
                    }
                    
                });
                
            }
            
            @Override
            public void focusGained(FocusEvent focusEvent)
            {
                
            }
        });
    }
    
    private void setDocCategoryOnFocusLost(final Text m_textDocCategory)
    {
        if (null != m_textDocCategory.getText() && !m_textDocCategory.getText().isEmpty())
        {
            String docCategoryValue = m_textDocCategory.getText();
            int docCategoryValueLength = docCategoryValue.length();
            if (docCategoryValueLength > 0 && docCategoryValueLength < 3)
            {
                m_textDocCategory.setText("");
                
            }
            else
            {
                // User types correct 3 letters on doc category then set
                // DocumentType Button value explicitly
                setDocumentTypeButtonValue(m_textDocCategory);
                
            }
        }
    }
    
    /**
     * Sets Doc Type Button value
     * 
     * @param m_textDocCategory
     */
    private void setDocumentTypeButtonValue(final Text m_textDocCategory)
    {
        String docCategoryTextValue = m_textDocCategory.getText();
        for (final String docCategoryValueFromLov : m_lovValuesForDocCategory)
        {
            if (docCategoryValueFromLov.contains(docCategoryTextValue))
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        m_BtnDocType.setText(docCategoryValueFromLov.substring(HIPHEN_POSITION + 1).trim());
                        m_BtnDocType.setEnabled(true);
                    }
                });
                return;
            }
        }
    }
    
    private String[] filterProposals(String proposals[], String filterString)
    {
        if (filterString.length() == 0)
            return proposals;
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < proposals.length; i++)
        {
            String string = proposals[i];
            if (string.length() >= filterString.length()
                    && string.substring(0, filterString.length()).equalsIgnoreCase(filterString))
                list.add(proposals[i]);
        }
        
        return (String[]) list.toArray(new String[list.size()]);
    }
    
    /** The m_registry. */
    protected Registry m_registry = null;
    
    /** The m_ lov popup button image. */
    private Image m_LovPopupButtonImage;
    
    /** The m_ btn doc type. */
    private Button m_BtnDocType;
    
    /** The m_ btn doc category. */
    private Button m_BtnDocCategory;
    
    /** The m_doc type lov popup button. */
    protected SWTLovPopupButton m_docTypeLovPopupButton;
    
    /** The m_doc category popup button. */
    SWTLovPopupButton m_docCategoryPopupButton;
    
    /** The doc type dialog. */
    NIDLovPopupDialog docTypeDialog;
    
    protected Text m_textDocCategory;
    
    /** The m_ combo pull doc. */
    private Combo m_ComboPullDoc;
    
    /** The text content adapter. */
    TextContentAdapter textContentAdapter = new TextContentAdapter();
    
    /** The is modified by lov. */
    private boolean isModifiedByLOV = false;
    
    /** The Constant HIPHEN_POSITION. */
    private static final int HIPHEN_POSITION = 4;
    
    private String[] m_lovValuesForDocCategory;
    
    private boolean isTabPressed;
    
}