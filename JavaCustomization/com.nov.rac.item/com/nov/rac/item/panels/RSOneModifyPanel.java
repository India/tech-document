package com.nov.rac.item.panels;

import java.util.Collections;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.RSOneErrorMessageDialog;
import com.nov.rac.item.panels.rsone.RSOneCreateRightPartInfoPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneModifyPanel extends RSOneCreateRightPartInfoPanel implements IPublisher
{
    
    public RSOneModifyPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        GridData l_gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1);
        
        m_composite = getComposite();
        m_composite.setLayoutData(l_gridData);
        GridLayout l_glModify = new GridLayout(4, true);
        m_composite.setLayout(l_glModify);
        
        //createEmptyLabels(m_composite, 4, 2);
        
        createDashNosRow(m_composite);
        
        createAddDashNoRow(m_composite);
        
        createAddRangeRow(m_composite);
        
        CreateDoneBtnRow(m_composite);
        
        return true;
    }
    
    private void createDashNosRow(Composite l_composite)
    {
        m_dashLabel = new Label(l_composite, SWT.NONE);
        m_dashLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 5));
        m_dashLabel.setText(m_registry.getString("DashButton.Name"));
        
        Composite l_dashListComposite = new Composite(l_composite, SWT.NONE);
        l_dashListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 5));
        GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        l_dashListComposite.setLayout(gridLayout);
        
        m_dashNoList = new List(l_dashListComposite, SWT.BORDER | SWT.V_SCROLL);
        GridData l_gdList = new GridData(SWT.FILL, SWT.FILL, true, true, 6, 5);
        l_gdList.heightHint = m_dashNoList.getShell().getSize().y / 7;
        m_dashNoList.setLayoutData(l_gdList);
        m_dashNoList.showSelection();
        
        m_removeDashNoBtn = new Button(l_dashListComposite, SWT.NONE);
        m_removeDashNoBtn.setLayoutData(new GridData(SWT.CENTER, SWT.UP, true, true, 1, 1));
        m_removeImage = new Image(l_composite.getDisplay(), m_registry.getImage("remove.Icon"), SWT.NONE);
        m_removeDashNoBtn.setImage(m_removeImage);
        m_removeDashNoBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                removeDashNo();
            }
        });
        
        createEmptyLabels(l_dashListComposite, 1, 1);
        createEmptyLabels(l_dashListComposite, 1, 1);
        createEmptyLabels(l_dashListComposite, 1, 1);
        createEmptyLabels(l_dashListComposite, 1, 1);
    }
    
    private void createAddDashNoRow(Composite l_composite)
    {
        createEmptyLabels(l_composite, 1, 1);
        
        Composite l_dashComposite = new Composite(l_composite, SWT.NONE);
        l_dashComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        l_dashComposite.setLayout(gridLayout);
        
        m_dashNoText = new Text(l_dashComposite, SWT.BORDER);
        m_dashNoText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 6, 1));
        
        m_dashNoText.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_dashNoText);
            }
        });
        
        m_addDashNoBtn = new Button(l_dashComposite, SWT.NONE);
        m_addDashNoBtn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        m_addImage = new Image(l_composite.getDisplay(), m_registry.getImage("add.Icon"), SWT.NONE);
        m_addDashNoBtn.setImage(m_addImage);
        m_addDashNoBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                addDashNo();
            }
        });
    }
    
    private void createAddRangeRow(Composite l_composite)
    {
        m_rangeLabel = new Label(l_composite, SWT.NONE);
        GridData gd_lblRange = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
        m_rangeLabel.setLayoutData(gd_lblRange);
        m_rangeLabel.setText(m_registry.getString("RangeButton.Name"));
        
        Composite l_rangeComposite = new Composite(l_composite, SWT.NONE);
        l_rangeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        l_rangeComposite.setLayout(gridLayout);
        
        Composite l_textrangeComposite = new Composite(l_rangeComposite, SWT.NONE);
        l_textrangeComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
        GridLayout l_gridLayout = new GridLayout(3, false);
        l_gridLayout.marginWidth = 0;
        l_gridLayout.marginHeight = 0;
        l_textrangeComposite.setLayout(l_gridLayout);
        
        m_rangeTextFisrt = new Text(l_textrangeComposite, SWT.BORDER);
        m_rangeTextFisrt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        m_rangeTextFisrt.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_rangeTextFisrt);
            }
        });
        
        m_dashTextLabel = new Label(l_textrangeComposite, SWT.NONE);
        m_dashTextLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 1));
        m_dashTextLabel.setImage(m_removeImage);
        
        m_rangeTextLast = new Text(l_textrangeComposite, SWT.BORDER);
        m_rangeTextLast.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        m_rangeTextLast.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_rangeTextLast);
            }
        });
        
        m_addDashRangeBtn = new Button(l_rangeComposite, SWT.NONE);
        m_addDashRangeBtn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        m_addImage = new Image(l_composite.getDisplay(), m_registry.getImage("add.Icon"), SWT.NONE);
        m_addDashRangeBtn.setImage(m_addImage);
        m_addDashRangeBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                if (ValidateDashNos())
                {
                    addDashRangeToList();
                }
            }
        });
    }
    
    private void CreateDoneBtnRow(final Composite l_composite)
    {
        createEmptyLabels(l_composite, 1, 1);
        
        Composite l_doneComposite = new Composite(l_composite, SWT.NONE);
        l_doneComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        GridLayout gridLayout = new GridLayout(7, true);
        gridLayout.marginWidth = 0;
        l_doneComposite.setLayout(gridLayout);
        
        new Label(l_doneComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true, 2, 1));
        
        m_doneBtn = new Button(l_doneComposite, SWT.NONE);
        GridData l_gdDoneBtn = new GridData(SWT.FILL, SWT.CENTER, true, true, 2, 1);
        m_doneBtn.setLayoutData(l_gdDoneBtn);
        m_doneBtn.setText(m_registry.getString("DoneButton.Name"));
        m_doneBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                IController controller = ControllerFactory.getInstance().getDefaultController();
                controller.publish(new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_MODIFY_PANEL_DONE_BUTTON_SELECTION, m_dashNoList.getItems(), null));
            }
        });
        
        new Label(l_doneComposite, SWT.NONE).setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true, 2, 1));
        
        createEmptyLabels(l_doneComposite, 1, 1);
    }
    
    private void createEmptyLabels(Composite l_composite, int l_hSpan, int l_vSpan)
    {
        new Label(l_composite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, l_hSpan, l_vSpan));
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    private void doTextValidation(VerifyEvent event, Text m_tempText)
    {
        for (char c : event.text.toCharArray())
            
            if (!Character.isDigit(c) && event.keyCode != SWT.KEYPAD_SUBTRACT)
            {
                event.doit = false;
            }
            else if ((m_tempText.getCharCount() == 3 && m_tempText.getSelectionText().isEmpty())
                    || event.keyCode == SWT.KEYPAD_SUBTRACT)
            {
                event.doit = false;
                
                getMessage(m_registry.getString("Warning.text"), m_registry.getString("ValidDashRangeWithin.msg"),
                        SWT.ICON_WARNING | SWT.OK);
            }
            else
            {
                event.doit = true;
            }
    }
    
    private void removeDashNo()
    {
        if (m_dashNoList.getSelectionCount() != 0)
        {
            removeValidDashNo();
        }
        else
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("NoDashNoSelected.msg"),
                    SWT.ICON_WARNING | SWT.OK);
        }
    }
    
    private void removeValidDashNo()
    {
        if (m_dashNoList.getItem(m_dashNoList.getSelectionIndex()).equalsIgnoreCase("001"))
        {
            m_errorMsgDialog = new RSOneErrorMessageDialog(new Shell(),
                    m_registry.getString("DashNumberMandatory.error"));
            m_errorMsgDialog.open();
            
            m_dashNoList.setSelection(-1);
        }
        else
        {
            m_vDashNos.remove(m_dashNoList.getSelectionIndex());
            
            m_dashNoList.remove(m_dashNoList.getSelectionIndex());
            
        }
    }
    
    private void addDashNo()
    {
        String dashNo = m_dashNoText.getText();
        
        if (Integer.parseInt(dashNo) == 0)
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("DashNumbercannotbe0.msg"),
                    SWT.ICON_WARNING | SWT.OK);
        }
        else
        {
            String l_tempDashNO = formatDashNo(dashNo);
            
            setDashVector(l_tempDashNO);
        }
    }
    
    private void addDashRangeToList()
    {
        int l_iFisrtDashNo = Integer.parseInt(m_rangeTextFisrt.getText());
        int l_LastDashNo = Integer.parseInt(m_rangeTextLast.getText());
        
        for (int index = l_iFisrtDashNo; index <= l_LastDashNo; index++)
        {
            Integer.toString(index);
            
            String l_tempDashNo = formatDashNo(Integer.toString(index));
            
            setDashVector(l_tempDashNo);
        }
        
        m_rangeTextFisrt.setText("");
        m_rangeTextLast.setText("");
    }
    
    private String formatDashNo(String l_dashNo)
    {
        String l_tempDashNo = "";
        
        if (l_dashNo.length() == 1)
        {
            l_tempDashNo = "0" + "0" + l_dashNo;
        }
        else if (l_dashNo.length() == 2)
        {
            l_tempDashNo = "0" + l_dashNo;
        }
        else
        {
            l_tempDashNo = l_dashNo;
        }
        
        return getNewDashNumber(l_tempDashNo);
    }
    
    private String getNewDashNumber(String l_tempDashNo)
    {
        if (m_dashNoList.indexOf(l_tempDashNo) == -1)
        {
            m_vDashNos.add(l_tempDashNo);
            
            return l_tempDashNo;
        }
        else
        {
            return "";
        }
    }
    
    public boolean ValidateDashNos()
    {
        
        String l_firstDashNo = m_rangeTextFisrt.getText();
        String l_lastDashNo = m_rangeTextLast.getText();
        
        if (l_firstDashNo.isEmpty() || l_lastDashNo.isEmpty())
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("EmptyDashRange.msg"),
                    SWT.ICON_WARNING | SWT.OK);
            
            return false;
        }
        
        int l_iFirstDashNo = Integer.parseInt(l_firstDashNo);
        int l_iLastDashNo = Integer.parseInt(l_lastDashNo);
        
        if (l_iFirstDashNo == 0)
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("ValidDashRangeWithin.msg"),
                    SWT.ICON_WARNING | SWT.OK);
            
            return false;
        }
        else if (l_iFirstDashNo > l_iLastDashNo || l_iFirstDashNo == l_iLastDashNo)
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("ValidDashRangeHigher.msg"),
                    SWT.ICON_WARNING | SWT.OK);
            
            return false;
        }
        else
        {
            return true;
        }
    }
    
    private void setDashVector(String l_tempDashNO)
    {
        if (!l_tempDashNO.isEmpty() && l_tempDashNO != null)
        {
            setSortedList();
            
            m_dashNoText.setText("");
        }
        
        m_dashNoText.setText("");
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
        MessageBox dialog = new MessageBox(this.getComposite().getShell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
    public void loadList(Object object)
    {
        String[] items = (String[]) object;
        
        if (m_vDashNos.isEmpty())
        {
            m_vDashNos.clear();
        }
        
        Collections.addAll(m_vDashNos, items);
        
        setSortedList();
    }
    
    private void setSortedList()
    {
        if (!m_vDashNos.isEmpty())
        {
            Collections.sort(m_vDashNos);
        }
        
        String[] m_aDashNos = m_vDashNos.toArray(new String[m_vDashNos.size()]);
        
        m_dashNoList.setItems(m_aDashNos);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private Registry m_registry;
    
    private Composite m_composite;
    
    private RSOneErrorMessageDialog m_errorMsgDialog;
    
    private Vector<String> m_vDashNos = new Vector<String>();
    
    protected Button m_removeDashNoBtn;
    protected Button m_addDashNoBtn;
    protected Button m_addDashRangeBtn;
    protected Button m_doneBtn;
    
    protected Text m_rangeTextLast;
    protected Text m_rangeTextFisrt;
    protected Text m_dashNoText;
    
    protected List m_dashNoList;
    
    private Label m_dashLabel;
    private Label m_rangeLabel;
    private Label m_dashTextLabel;
    
    private Image m_addImage;
    private Image m_removeImage;
    
}
