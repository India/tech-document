package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneCreateOperationTypePanel extends RSOneSimpleEnggNonEnggTypePanel implements ILoadSave
{
    
    public RSOneCreateOperationTypePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    public boolean createUI() throws TCException
    {
//        Composite l_typeComposite = getComposite();
//        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
//        l_typeComposite.setLayoutData(l_gdDocComposite);
//        l_typeComposite.setLayout(new GridLayout(5, true));
//        
//        createEmptyLabel(l_typeComposite);
//        
//        createRadioButtons1Row(l_typeComposite);
//        
//        createEmptyLabel(l_typeComposite);
//        
//        createTypeRow(l_typeComposite);
        
        //Tushar added
        boolean returnValue = super.createUI();
        
        UIHelper.makeMandatory(m_typeCombo);
        
        m_engineeringRadio.setSelection(true);
        m_engineeringRadio.notifyListeners(SWT.Selection, new Event());
      //  m_typeCombo.notifyListeners(SWT.Selection, new Event());
        
        return true;
    }
    
    protected void createEmptyLabel(Composite l_composite)
    {
    	Label emptyLabel = new Label(l_composite, SWT.None);
        emptyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
    }
    
    protected Registry getRegistry() 
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }

    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    

  
}
