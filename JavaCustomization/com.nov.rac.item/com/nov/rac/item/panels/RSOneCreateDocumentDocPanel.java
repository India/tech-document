package com.nov.rac.item.panels;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreateDocumentDocPanel extends SimpleDocumentCreatePanel
{
    
    public RSOneCreateDocumentDocPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnCode = super.createUI();
        UIHelper.makeMandatory(m_textDocCategory);
        
        return returnCode;
    }
}
