package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

// TODO: Auto-generated Javadoc
/**
 * The Class CopyPartInfoPanel.
 */
public class CopyPartInfoPanel extends PartInfoPanel{

	/**
	 * Instantiates a new copy part info panel.
	 *
	 * @param parent the parent
	 * @param style the style
	 */
	public CopyPartInfoPanel(Composite parent, int style) {
		super(parent, SWT.BORDER);
		m_registry = getRegistry();
	}

	/* (non-Javadoc)
	 * @see com.nov.rac.item.panels.PartInfoPanel#createUI()
	 */
	@Override
	public boolean createUI() throws TCException {
		Composite copyPartInfoPanel = getComposite();
		GridData l_gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, false,1, 1);
		copyPartInfoPanel.setLayoutData(l_gd_panel);
		copyPartInfoPanel.setLayout(new GridLayout(1, true));
		createSelectedItemRow(copyPartInfoPanel);
		super.createUI();
		m_makeItemRadio.setEnabled(false);
		m_purchasedRadio.setEnabled(false);
		m_revisionText.setEditable(true);
		disableRightClick();
		registerSubscriber();
		return true;
	}

	/**
	 * Creates the selected item row.
	 *
	 * @param copyPartInfoPanel the copy part info panel
	 */
	private void createSelectedItemRow(Composite copyPartInfoPanel) {
		GridData l_gd_panel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		copyPartInfoPanel.setLayoutData(l_gd_panel);
		copyPartInfoPanel.setLayout(new GridLayout(5, true));

		m_selectedItemLabel = new Label(copyPartInfoPanel, SWT.NONE);
		m_selectedItemLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1));
		m_selectedItemLabel.setText(m_registry
				.getString("CopyPartInfoPanel.SelectedItem"));
		m_selectedItemText = new Text(copyPartInfoPanel, SWT.BORDER);
		m_selectedItemText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				false, false, 3, 1));
		m_selectedItemText.setEnabled(false);
		TCComponent targetItem = NewItemCreationHelper.getTargetComponent();
		GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		/*SearchButtonComponent searchButton = new SearchButtonComponent(copyPartInfoPanel, SWT.TOGGLE);
		GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		searchBtnGridData.horizontalIndent=SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON );
		searchButton.setLayoutData(searchBtnGridData);
		searchButton.setSearchItemType(new String[]{"Nov4Part", "Non-Engineering"});*/
		
		String [] searchItemTypes = {"Nov4Part", "Non-Engineering"};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(copyPartInfoPanel, searchBtnGridData, searchItemTypes);
        
		if(targetItem==null)
		{
			searchButton.setVisible(true);
		}
		else
		{
			searchButton.setVisible(false);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.nov.rac.item.panels.SimplePartInfoPanel#load(com.nov.rac.propertymap.IPropertyMap)
	 */
	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		m_selectedItemText.setText(PropertyMapHelper.handleNull(propMap.getString("item_id")));
		super.load(propMap);
		m_itemIdText.setText("");
		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException
	{
		super.save(propMap);
		// if Copy RDD radio button in document panel is selected, then attached RDD from selected part to newly created part
		if(m_isCopyDocSelected)
		{				
			addDocumentsByRelation(NationalOilwell.RELATED_DEFINING_DOCUMENT, propMap);
		}	
		
		// by default all RD should be attached
		addDocumentsByRelation(NationalOilwell.RELATED_DOCUMENT, propMap);
		return true;
	}
	
	@Override	
	public void update(PublishEvent event)	
	{		
		super.update(event);
	
		if(event.getPropertyName().equals(NationalOilwell.CREATE_NEW_RDD))
		{
			m_isCopyDocSelected = false;
		}
		if(event.getPropertyName().equals(NationalOilwell.COPY_RDD))
		{
			m_isCopyDocSelected = true;
		}
	}
	
	private void addDocumentsByRelation(String relationName, IPropertyMap propMap) throws TCException 
	{
		// 1. getSelected item's latest revision
		// 2. get all RDD attached to item rev
		// 3. set tag property to property map
		
		TCComponentItemRevision selectedItemRev = getItemRevOfSelectedItem();
		if(null != selectedItemRev)
		{
			TCComponent[] relatedDocumentList = selectedItemRev.getRelatedComponents(relationName);
			propMap.getCompound(NationalOilwell.REVISION).setTagArray(relationName, relatedDocumentList);
		}
		
	}
	
	private TCComponentItemRevision getItemRevOfSelectedItem() throws TCException
	{	
		TCComponent selectedObject = NewItemCreationHelper.getTargetComponent();	
		TCComponentItemRevision selectedItemRev = null;
		if (selectedObject == null)	
		{
			selectedObject = NewItemCreationHelper.getSelectedComponent();
		}	
		if(selectedObject instanceof TCComponentItem)
		{			
			selectedItemRev = ((TCComponentItem) selectedObject).getLatestItemRevision();
		}
		else if(selectedObject instanceof TCComponentItemRevision)
		{
			TCComponentItem item = ((TCComponentItemRevision) selectedObject).getItem();
			selectedItemRev = item.getLatestItemRevision();
		}				
		
		return selectedItemRev;
	}
	
	private void registerSubscriber()
	{
			IController l_controller = ControllerFactory.getInstance().getDefaultController();
			l_controller.registerSubscriber(NationalOilwell.CREATE_NEW_RDD, this);
			l_controller.registerSubscriber(NationalOilwell.COPY_RDD, this);
	}

	@Override
	public void dispose()
	{		 
		IController l_controller = ControllerFactory.getInstance().getDefaultController();
		l_controller.unregisterSubscriber(NationalOilwell.CREATE_NEW_RDD, this);
		l_controller.unregisterSubscriber(NationalOilwell.COPY_RDD, this);	
		super.dispose();	 
	}

	private void disableRightClick()
	{
		NewItemCreationHelper.disableRightClick( m_selectedItemText );
		NewItemCreationHelper.disableRightClick( m_itemIdText );
	}
	
	
	/** The m_selected item text. */
	private Text m_selectedItemText;
	
	/** The m_selected item label. */
	private Label m_selectedItemLabel;
	
	/** The m_registry. */
	private Registry m_registry;
	
	private boolean m_isCopyDocSelected = true;
	
}
