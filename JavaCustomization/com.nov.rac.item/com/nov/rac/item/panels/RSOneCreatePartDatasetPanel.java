package com.nov.rac.item.panels;


import org.eclipse.swt.widgets.Composite;


import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;

import com.teamcenter.rac.kernel.TCException;


public class RSOneCreatePartDatasetPanel extends NewDatasetPanel

{
    
    public RSOneCreatePartDatasetPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        m_textDatasetType.setText(PropertyMapHelper.handleNull(propMap.getString("object_type")));
        m_comboFileTemplate.setText("");
        
        return true;
    }
    
}
