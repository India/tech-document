package com.nov.rac.item.panels;

import static com.nov.rac.item.helpers.DocumentHelper.getDocCategory;
import static com.nov.rac.item.helpers.DocumentHelper.getDocumentTypes;
import static com.nov.rac.item.helpers.DocumentHelper.hasContent;

import java.awt.AWTException;
import java.awt.Robot;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DocCategoryUIModel;
import com.nov.rac.item.helpers.NOVDatasetTypeTemplateFindHelper;
import com.nov.rac.item.helpers.NOVDatasetTypeUIPanelHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.contentadapter.TextboxContentAdapter;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.NIDLovPopupDialog;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.controls.SWTLovPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

/**
 * @author lavanyan
 * 
 */
public class RSOneSimpleDocCategoryPanel extends AbstractUIPanel implements ILoadSave, IPublisher
{
    
    public RSOneSimpleDocCategoryPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        m_session = getSession();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    protected TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
        
    }
    
    protected GridLayout getLayout()
    {
        GridLayout gridLayout = new GridLayout(NO_OF_COLUMNS_IN_SUB_COMPOSITE, true);
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        
        return gridLayout;
    }
    
    protected GridData getLayoutData()
    {
        return new GridData(SWT.FILL, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_TEXTAREA, 1);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, NO_OF_COLUMNS_IN_MAIN_COMPOSITE, 1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(NO_OF_COLUMNS_IN_MAIN_COMPOSITE, true));
        
        createDocCategoryRow(l_composite);
        
        createDocTypeRow(l_composite);
        
        createDatasetTypeRow(l_composite);
        
        createFileTemplateRow(l_composite);
        
        createImportFileRow(l_composite);
        
        disableRightClick(); // TCDECREl-6370
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    protected TCComponentListOfValues getDocCategoryLOV()
    {
        return getDocCategory(NationalOilwell.DOCUMENT_TYPES_LOV);
    }
    
    protected void createDocCategoryRow(final Composite l_composite) throws TCException
    {
        // new Label(l_composite, SWT.NONE);
        Label m_lblDocCategory = new Label(l_composite, SWT.NONE);
        m_lblDocCategory.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_lblDocCategory.setText(m_registry.getString("docCategory.NAME", "Key Not Found"));// "Doc Category"
        
        Composite l_docCatagoryComposite = new Composite(l_composite, SWT.NONE);
        l_docCatagoryComposite.setLayoutData(getLayoutData());
        
        l_docCatagoryComposite.setLayout(getLayout());
        
        m_textDocCategory = new Text(l_docCatagoryComposite, SWT.BORDER);
        m_textDocCategory.setToolTipText(m_registry.getString("docCategoryEditor.TOOLTIP"));
        
        GridData m_gd_txtDocCat = new GridData(SWT.FILL, SWT.TOP, true, false, HORIZONTAL_SPAN_FOR_FIELDS, 1);
        
        m_textDocCategory.setLayoutData(m_gd_txtDocCat);
        m_lovValuesForDocCategory = LOVUtils.getLovStringValues(getDocCategoryLOV());
        
        new AutoCompleteField(m_textDocCategory, new TextboxContentAdapter(Arrays.asList(m_lovValuesForDocCategory)),
                m_lovValuesForDocCategory);
        
        addingFocusListenerOnDocCategoryText(m_textDocCategory);
        
        addingListenerOnDocCategoryText(m_textDocCategory);
        
        createDocCategoryLovPopupButton(l_docCatagoryComposite);
        
    }
    
    protected void createDocTypeRow(final Composite l_composite) throws TCException
    {
        // new Label(l_composite, SWT.NONE);
        Label m_lblDocType = new Label(l_composite, SWT.NONE);
        m_lblDocType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_lblDocType.setText(m_registry.getString("docType.NAME", "Key Not Found"));
        
        Composite l_docTypeComposite = new Composite(l_composite, SWT.NONE);
        l_docTypeComposite.setLayoutData(getLayoutData());
        
        l_docTypeComposite.setLayout(getLayout());
        
        createDocTypeLovPopupButton(l_docTypeComposite);
        
        new Label(l_docTypeComposite, SWT.NONE);
        
    }
    
    protected void createDatasetTypeRow(final Composite l_composite) throws TCException
    {
        // new Label(l_composite, SWT.NONE);
        Label l_lblDatasetType = new Label(l_composite, SWT.NONE);
        l_lblDatasetType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_lblDatasetType.setText(m_registry.getString("DatasetTypeUIPanel.DatasetType"));
        
        Composite l_datasTypeComposite = new Composite(l_composite, SWT.NONE);
        l_datasTypeComposite.setLayoutData(getLayoutData());
        
        l_datasTypeComposite.setLayout(getLayout());
        
        m_textDatasetType = new Text(l_datasTypeComposite, SWT.BORDER);
        m_textDatasetType.setToolTipText(m_registry.getString("DatasetType.ToolTip"));
        m_textDatasetType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_FIELDS, 1));
        
        m_lovValuesForDatasetType = LOVUtils.getLovStringValues(NOVDatasetTypeUIPanelHelper.getDatasetTypes());
        
        new AutoCompleteField(m_textDatasetType, new TextboxContentAdapter(Arrays.asList(m_lovValuesForDatasetType)),
                m_lovValuesForDatasetType);
        
        m_textDatasetType.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent e)
            {
                try
                {
                    onDatasetTypeTextModified(e);
                }
                catch (TCException e1)
                {
                    e1.printStackTrace();
                }
            }
        });
        
        CreateLovPopupButton(l_datasTypeComposite);
        addingFocusListenerOnDatasetTypeText(m_textDatasetType);
    }
    
    // End Dataset Type
    
    // Start File Template
    protected void createFileTemplateRow(final Composite l_composite)
    {
        // new Label(l_composite, SWT.NONE);
        Label l_lblFileTemplate = new Label(l_composite, SWT.NONE);
        l_lblFileTemplate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_lblFileTemplate.setText(m_registry.getString("DatasetTypeUIPanel.FileTemplate"));
        
        Composite l_fileTemplateComposite = new Composite(l_composite, SWT.NONE);
        l_fileTemplateComposite.setLayoutData(getLayoutData());
        
        l_fileTemplateComposite.setLayout(getLayout());
        
        m_comboFileTemplate = new Combo(l_fileTemplateComposite, SWT.NONE);
        m_comboFileTemplate
                .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_FIELDS, 1));
        new Label(l_fileTemplateComposite, SWT.NONE);
        m_comboFileTemplate.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent e)
            {
                m_textImportFile.setText("");
            }
        });
    }
    
    // End File Template
    
    // Start Import File
    protected void createImportFileRow(final Composite l_composite)
    {
        // new Label(l_composite, SWT.NONE);
        Label l_lblImportFile = new Label(l_composite, SWT.NONE);
        l_lblImportFile.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_lblImportFile.setText(m_registry.getString("DatasetTypeUIPanel.ImportFile"));
        
        Composite l_importFileComposite = new Composite(l_composite, SWT.NONE);
        l_importFileComposite.setLayoutData(getLayoutData());
        
        l_importFileComposite.setLayout(getLayout());
        
        m_textImportFile = new Text(l_importFileComposite, SWT.BORDER);
        m_textImportFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_FIELDS, 1));
        
        m_browseButton = new Button(l_importFileComposite, SWT.PUSH);
        GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
        gd.horizontalIndent = POPUP_BUTTON_HORIZONTAL_INDENT;
        m_browseButton.setLayoutData(gd);
        m_browseButton.getBackground();
        m_browseButton.setText("....");
        m_browseButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent e)
            {
                onBrowseButtonWidgetSelected(e);
            }
        });
    }
    
    // End Import File
    
    protected void disableRightClick()
    {
        
        NewItemCreationHelper.disableRightClick(m_textDocCategory);
        NewItemCreationHelper.disableRightClick(m_textDatasetType);
        NewItemCreationHelper.disableRightClick(m_textImportFile);
        NewItemCreationHelper.disableRightClick(m_comboFileTemplate);
    }
    
    private void onDatasetTypeTextModified(ModifyEvent e) throws TCException
    {
        Object source = (Object) (e.getSource());
        if (m_textDatasetType.equals(source))
        {
            if (!m_textDatasetType.getText().equals(""))
            {
                
                ControllerFactory
                        .getInstance()
                        .getDefaultController()
                        .publish(
                                getPublishEvent(NationalOilwell.RSONE_DATASET_TYPE, "Prev Val",
                                        m_textDatasetType.getText()));
                List<String> lovList = Arrays.asList(LOVUtils.getLovStringValues(NOVDatasetTypeUIPanelHelper
                        .getDatasetTypes()));
                if (lovList.contains(m_textDatasetType.getText()))
                {
                    m_comboFileTemplate.removeAll();
                    buildFileTemplateCombo();
                    setEnable(m_textDatasetType.isEnabled());
                }
            }
            else
            {
                m_comboFileTemplate.setText("");
                m_textImportFile.setText("");
                m_comboFileTemplate.setEnabled(false);
                m_textImportFile.setEnabled(false);
                m_browseButton.setEnabled(false);
            }
        }
    }
    
    private PublishEvent getPublishEvent(String property, Object prevValue, Object newValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property, newValue, null);
        event.setNewValue(newValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private void CreateLovPopupButton(Composite l_composite) throws TCException
    {
        TCComponent l_tcComponent = null;
        m_datasetTypeLovbutton = new Button(l_composite, SWT.NONE);
        GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
        gd.horizontalIndent = 5;
        m_datasetTypeLovbutton.setLayoutData(gd);
        m_datasetTypeLovbutton.setToolTipText(m_registry.getString("LovButton.ToolTip"));
        Image l_dataSetLovPopupButtonImage = new Image(l_composite.getDisplay(),
                m_registry.getImage("DatasetTypeUIPanel.LOVButtonImage"), SWT.NONE);
        
        final NIDLovPopupDialog l_lovpopupdialog = new NIDLovPopupDialog(l_composite.getShell(), SWT.NONE,
                NOVDatasetTypeUIPanelHelper.getDatasetTypes(), "Default");
        
        m_dataSetLovPopupButton = new SWTLovPopupButton(m_datasetTypeLovbutton, l_lovpopupdialog,
                NOVDatasetTypeUIPanelHelper.getDatasetTypes(), "Default", l_tcComponent);
        m_dataSetLovPopupButton.setIcon(l_dataSetLovPopupButtonImage);
        l_lovpopupdialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(final PropertyChangeEvent propertychangeevent)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        m_textDatasetType.setText(propertychangeevent.getNewValue().toString());
                    }
                });
            }
        });
    }
    
    private void onBrowseButtonWidgetSelected(SelectionEvent e)
    {
        final Composite current = getComposite();
        FileDialog dialog = new FileDialog(current.getShell(), SWT.NULL);
        dialog.open();
        m_comboFileTemplate.setText("");
        String selectedFile = dialog.getFileName();
        if (!selectedFile.isEmpty())
        {
            m_importFileAbsolutePath = dialog.getFilterPath() + File.separator + selectedFile;
            m_textImportFile.setText(m_importFileAbsolutePath);
        }
    }
    
    private void buildFileTemplateCombo()
    {
        if (m_comboFileTemplate.getSelectionIndex() < 0)
        {
            m_textImportFile.setText("");
            TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
            String currentGroup = null;
            try
            {
                currentGroup = theSession.getCurrentGroup().getGroupName();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            NOVDatasetTypeTemplateFindHelper l_panelHelper = NOVDatasetTypeTemplateFindHelper.getInstance();
            datasetTemplateMap = l_panelHelper.getDatasetTemplate(m_textDatasetType.getText(), currentGroup,
                    m_textDocCategory.getText(), m_BtnDocType.getText());
            
            String[] templates = new String[datasetTemplateMap.size()];
            templates = (String[]) datasetTemplateMap.keySet().toArray(templates);
            m_comboFileTemplate.setItems(templates);
            m_comboFileTemplate.select(0);
        }
    }
    
    private void setEnable(boolean enable)
    {
        m_textDatasetType.setEnabled(enable);
        m_textImportFile.setEnabled(enable);
        m_comboFileTemplate.setEnabled(enable);
        m_browseButton.setEnabled(enable);
        m_dataSetLovPopupButton.getPopupButton().setEnabled(enable);
    }
    
    private void addingListenerOnDocCategoryText(final Text m_textDocCategory)
    {
        m_textDocCategory.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                if (isModifiedByLOV)
                {
                    isModifiedByLOV = false;
                    return;
                }
                
                String value = m_textDocCategory.getText();
                if (null != value && value.length() > 0)
                {
                    if (value.indexOf('-') == HIPHEN_POSITION && value.length() > HIPHEN_POSITION)
                    {
                        isModifiedByLOV = true;
                        setTextForDocCategoryAndDocType(m_textDocCategory.getText());
                    }
                    else if (value.length() > 3)
                    {
                        
                        toggleDocTypeButton(false);
                        clear();
                    }
                    else
                    {
                        if (value.length() <= 3)
                        {
                            if (!value.equals(m_textDocCategory.getText().toUpperCase()))
                            {
                                m_textDocCategory.setText(m_textDocCategory.getText().toUpperCase());
                            }
                            m_textDocCategory.setSelection(m_textDocCategory.getText().length());
                            toggleDocTypeButton(false);
                            resetDocType();
                        }
                    }
                }
                else
                {
                    toggleDocTypeButton(false);
                    resetDocType();
                }
                moveDocTypeButton();
            }
        });
        
        addKeyListenerOnDocCategoryTextField(m_textDocCategory);
    }
    
    protected void toggleDocTypeButton(boolean flag)
    {
        if (null != m_BtnDocType)
        {
            m_BtnDocType.setEnabled(flag);
            
        }
        
    }
    
    protected void clear()
    {
        if (null != this.m_textDocCategory)
            this.m_textDocCategory.setText("");
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        publishDocCategory_Type();
    }
    
    protected void resetDocType()
    {
        if (hasContent(this.m_BtnDocType.getText()))
            this.m_BtnDocType.setText("");
        publishDocCategory_Type();
    }
    
    protected void moveDocTypeButton()
    {
        Rectangle rect = m_BtnDocType.getBounds();
        m_BtnDocType.setLocation(rect.x + 1, rect.y);
        m_BtnDocType.setLocation(rect.x, rect.y);
    }
    
    private void publishDocCategory_Type()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        // IController controller =
        // ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        DocCategoryUIModel docCategoryUIModel = new DocCategoryUIModel();
        docCategoryUIModel.setDocumentCategory(m_textDocCategory.getText());
        docCategoryUIModel.setDocumentType(m_BtnDocType.getText());
        docCategoryUIModel.setDocCategoryTextEnable(m_textDocCategory.isEnabled());
        
    }
    
    private void addingFocusListenerOnDocCategoryText(final Text m_textDocCategory)
    {
        m_textDocCategory.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent focusEvent)
            {
                m_textDocCategory.getDisplay().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if (!isTabPressed)
                        {
                            setDocCategoryOnFocusLost(m_textDocCategory);
                        }
                        else
                        {
                            String comboValue = m_textDocCategory.getText();
                            if (!Arrays.asList(m_lovValuesForDocCategory).contains(comboValue))
                            {
                                if (comboValue.length() > 0)
                                {
                                    String[] filteredProposals = filterProposals(m_lovValuesForDocCategory, comboValue);
                                    m_textDocCategory.setText(filteredProposals[0]);
                                }
                            }
                        }
                    }
                    
                });
                
            }
            
            @Override
            public void focusGained(FocusEvent focusEvent)
            {
                
            }
        });
    }
    
    private void addingFocusListenerOnDatasetTypeText(final Text m_textDocCategory)
    {
        m_textDatasetType.addFocusListener(new FocusListener()
        {
            @Override
            public void focusLost(FocusEvent focusEvent)
            {
                m_textDatasetType.getDisplay().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        String comboValue = m_textDatasetType.getText();
                        if (!Arrays.asList(m_lovValuesForDatasetType).contains(comboValue))
                        {
                            if (comboValue.length() > 0)
                            {
                                String[] filteredProposals = filterProposals(m_lovValuesForDatasetType, comboValue);
                                m_textDatasetType.setText(filteredProposals[0]);
                            }
                        }
                    }
                    
                });
                
            }
            
            @Override
            public void focusGained(FocusEvent focusEvent)
            {
                
            }
        });
    }
    
    private void addKeyListenerOnDocCategoryTextField(final Text m_textDocCategory)
    {
        m_textDocCategory.addKeyListener(new KeyListener()
        {
            
            @Override
            public void keyReleased(KeyEvent keyevent)
            {
                
            }
            
            @Override
            public void keyPressed(KeyEvent keyevent)
            {
                int keyCode = keyevent.keyCode;
                if (keyCode == SWT.TAB)
                {
                    isTabPressed = true;
                }
                else
                {
                    isTabPressed = false;
                }
            }
        });
    }
    
    private void setDocCategoryOnFocusLost(final Text m_textDocCategory)
    {
        if (null != m_textDocCategory.getText() && !m_textDocCategory.getText().isEmpty())
        {
            String docCategoryValue = m_textDocCategory.getText();
            int docCategoryValueLength = docCategoryValue.length();
            if (docCategoryValueLength > 0 && docCategoryValueLength < 3)
            {
                m_textDocCategory.setText("");
                
            }
            else
            {
                // User types correct 3 letters on doc category then set
                // DocumentType Button value explicitly
                setDocumentTypeButtonValue(m_textDocCategory);
                
            }
        }
    }
    
    private void setDocumentTypeButtonValue(final Text m_textDocCategory)
    {
        String docCategoryTextValue = m_textDocCategory.getText();
        for (final String docCategoryValueFromLov : m_lovValuesForDocCategory)
        {
            if (docCategoryValueFromLov.contains(docCategoryTextValue))
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        m_BtnDocType.setText(docCategoryValueFromLov.substring(HIPHEN_POSITION + 1).trim());
                        m_BtnDocType.setEnabled(true);
                    }
                });
                return;
            }
        }
    }
    
    private void createDocCategoryLovPopupButton(Composite current) throws TCException
    {
        
        TCComponent l_tcComponent = null;
        m_BtnDocCategory = new Button(current, SWT.NONE);
        GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
        gd.horizontalIndent = 5;
        m_BtnDocCategory.setLayoutData(gd);
        m_BtnDocCategory.setToolTipText(m_registry.getString("docCategoryPopup.TOOLTIP", "Key Not Found "));
        
        m_LovPopupButtonImage = new Image(current.getDisplay(),
                m_registry.getImage("NewDocCategoryUIPanel1.LOVButtonImage"), SWT.NONE);
        
        final NIDLovPopupDialog docCategoryDialog = new NIDLovPopupDialog(current.getShell(), SWT.NONE,
                getDocCategoryLOV(), "Default");
        
        docCategoryDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    isModifiedByLOV = true;
                    setTextForDocCategoryAndDocType(propertychangeevent.getNewValue().toString());
                    
                }
            }
        });
        
        m_docCategoryPopupButton = new SWTLovPopupButton(m_BtnDocCategory, docCategoryDialog, getDocCategoryLOV(), "",
                l_tcComponent);
        
        m_docCategoryPopupButton.setIcon(m_LovPopupButtonImage);
        
    }
    
    private void createDocTypeLovPopupButton(Composite documentComposite) throws TCException
    {
        TCComponent l_tcComponent = null;
        m_BtnDocType = new Button(documentComposite, SWT.NONE);
        m_BtnDocType.setToolTipText(m_registry.getString("docTypePopup.TOOLTIP", "Key Not Found "));
        m_BtnDocType.setEnabled(false);
        
        docTypeDialog = new NIDLovPopupDialog(documentComposite.getShell(), SWT.NONE, getDocCategoryLOV(), "Default");
        m_docTypeLovPopupButton = new SWTLovPopupButton(m_BtnDocType, docTypeDialog, getDocCategoryLOV(), "Default",
                l_tcComponent);
        GridData gd_DocType = new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1);
        
        gd_DocType.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_textDocCategory, 120);
        m_docTypeLovPopupButton.getPopupButton().setLayoutData(gd_DocType);
        m_docTypeLovPopupButton.setIcon(m_LovPopupButtonImage);
        
        docTypeDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(final PropertyChangeEvent propertychangeevent)
            {
                String l_propertyName = propertychangeevent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        public void run()
                        {
                            m_docTypeLovPopupButton.setText(propertychangeevent.getNewValue().toString());
                            docTypeDialog.setLovPopupButton((m_docTypeLovPopupButton));
                            publishDocCategory_Type();
                        }
                    });
                }
            }
        });
        
    }
    
    private void setTextForDocCategoryAndDocType(String value)
    {
        if (hasContent(value) && value.length() > (HIPHEN_POSITION + 1))
        {
            final String textValue = value.substring(0, HIPHEN_POSITION).trim();
            final String buttonCaption = value.substring(HIPHEN_POSITION + 1).trim();
            
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    if (m_textDocCategory.isEnabled()) // TCDECREL-6566
                    {
                        m_textDocCategory.setText(textValue);
                        m_BtnDocType.setText(buttonCaption);
                        m_BtnDocType.setEnabled(true);
                        setDocTypeLovComponent(m_BtnDocType, textValue);
                        publishDocCategory_Type();
                        m_textDocCategory.setSelection(m_textDocCategory.getText().length());
                        moveDocTypeButton();
                        
                        Display.getDefault().asyncExec(new Runnable()
                        {
                            public void run()
                            {
                                try
                                {
                                    if (m_textDocCategory.setFocus() && m_textDocCategory.isFocusControl())
                                    {
                                        Robot robot = new Robot();
                                        robot.keyPress(java.awt.event.KeyEvent.VK_INSERT);
                                        robot.keyRelease(java.awt.event.KeyEvent.VK_INSERT);
                                    }
                                    
                                }
                                catch (AWTException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    
                }
            });
        }
    }
    
    private void setDocTypeLovComponent(Button m_btnDocType, final String docCategoryLovValueSelected)
    {
        
        try
        {
            docTypeDialog.setLovComponent(getDocumentTypes(docCategoryLovValueSelected));
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    private String[] filterProposals(String proposals[], String filterString)
    {
        if (filterString.length() == 0)
            return proposals;
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < proposals.length; i++)
        {
            String string = proposals[i];
            if (string.length() >= filterString.length()
                    && string.substring(0, filterString.length()).equalsIgnoreCase(filterString))
                list.add(proposals[i]);
        }
        
        return (String[]) list.toArray(new String[list.size()]);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_textDocCategory.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.DOCUMENTS_CATEGORY)));
        m_BtnDocType.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.DOCUMENTS_TYPE)));
        m_textDatasetType.setText(PropertyMapHelper.handleNull(propMap.getString("object_type")));
        m_comboFileTemplate.setText("");
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        PropertyMapHelper.setString(NationalOilwell.DOCUMENTS_CATEGORY, m_textDocCategory.getText(), propMap);
        PropertyMapHelper.setString(NationalOilwell.DOCUMENTS_TYPE, m_BtnDocType.getText(), propMap);
        String datasetTypeText = m_textDatasetType.getText();
        if (null != datasetTypeText && (!"".equals(datasetTypeText.trim())))
        {
            
            TCComponent datasetDefinition = NOVDatasetTypeUIPanelHelper
                    .getDatasetDefinition(datasetTypeText, m_session);
            propMap.setString("dataset_type", datasetDefinition.toString());
            propMap.setString("datasetFilePath", m_textImportFile.getText());
            
            String template = m_comboFileTemplate.getText();
            if (template != null && !template.equals(""))
            {
                String uid = (String) datasetTemplateMap.get(template);
                
                TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
                TCComponent selectedTemplate = theSession.stringToComponent(uid);
                propMap.setTag("selectedTemplate", selectedTemplate);
            }
            
        }
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean reset()
    {
        m_textDocCategory.setText("");
        m_docCategoryPopupButton.setText("");
        m_textDatasetType.setText("");
        m_textImportFile.setText("");
        return true;
    }
    
    protected Text m_textDatasetType, m_textImportFile;
    protected Combo m_comboFileTemplate;
    protected Button m_browseButton;
    protected SWTLovPopupButton m_dataSetLovPopupButton;
    private String m_importFileAbsolutePath = "";
    private TCComponent[] m_templateList = null;
    protected Registry m_registry = null;
    private Image m_LovPopupButtonImage;
    protected Button m_BtnDocType;
    protected Button m_BtnDocCategory;
    private SWTLovPopupButton m_docTypeLovPopupButton;
    private SWTLovPopupButton m_docCategoryPopupButton;
    private NIDLovPopupDialog docTypeDialog;
    protected Text m_textDocCategory;
    private TextContentAdapter textContentAdapter = new TextContentAdapter();
    private boolean isModifiedByLOV = false;
    private static final int HIPHEN_POSITION = 4;
    private String[] m_lovValuesForDocCategory;
    private boolean isTabPressed;
    private TCSession m_session;
    protected Button m_datasetTypeLovbutton;
    private Map datasetTemplateMap = new HashMap<String, String>();
    
    private static final int NO_OF_COLUMNS_IN_MAIN_COMPOSITE = 4;
    private static final int NO_OF_COLUMNS_IN_SUB_COMPOSITE = 7;
    private static final int HORIZONTAL_SPAN_FOR_FIELDS = 6;
    private static final int HORIZONTAL_SPAN_FOR_TEXTAREA = 3;
    private static final int TEXTAREA_LIMIT = 3;
    private static final int POPUP_BUTTON_HORIZONTAL_INDENT = 5;
    private static final int HORIZONTAL_DLUs = 120;
    private String[] m_lovValuesForDatasetType;
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
}
