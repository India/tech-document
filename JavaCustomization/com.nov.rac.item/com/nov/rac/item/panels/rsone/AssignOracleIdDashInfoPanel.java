package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.RSOneDashNumberHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdDashInfoPanel extends SimpleDashInfoPanel2 implements ILoadSave
{

    public AssignOracleIdDashInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponent targetComponent = propMap.getComponent();
        TCComponentItem item = GeneralItemUtils.getItemFromTCComponent(targetComponent);
        
        String[] dashNumberList = null;
        String altId = null;
        
        if (item != null)
        {
            altId = item.getProperty("altid_list");
            
            if (altId != null && !altId.equals(""))
            {
                // example : get '1218123646' from 1218123646-001@RSOne
                altId = altId.split(DASH_STR)[0];
                dashNumberList = RSOneDashNumberHelper.getAllDashNumbers(altId);
                setExistingDashVector(dashNumberList, RSOneDashNumberHelper.getMissingDashNumber());
            }
            else
            {
                dashNumberList = new String[1];
                dashNumberList[0] = "001";
                m_missingNumStr = "001";
            }
            
            removeAllRows();
            addRows(dashNumberList);
            setMissingDashRowSelection(dashNumberList, m_missingNumStr);
        }
        
        publishEvents();
        
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL, m_missingNumStr, null);
        
        controller.publish(event);
    }
    
    IPublisher getPublisher()
    {
        return this;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    private static final String DASH_STR = "-";
    private String m_missingNumStr = null;
}
