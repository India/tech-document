package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyDocInfoPanel extends RSOneSimpleCopyDocInfoPanel
{

    public RSOneCopyDocInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
        UIHelper.makeMandatory(m_docIdText);
        m_docIdText.setEditable(false);
        return returnValue;
    }
    
}
