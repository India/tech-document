package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class CloneDefiningDocumentPanel extends AbstractUIPanel implements
		IPublisher, ILoadSave {

	public CloneDefiningDocumentPanel(Composite parent, int style) {
		super(parent, SWT.NONE);
		m_registry = getRegistry();
	}

	private Registry getRegistry() {
		return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
	}

	@Override
	public boolean createUI() throws TCException {
		Composite l_composite = getComposite();

		GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		l_composite.setLayoutData(gd_panel);
		l_composite.setLayout(new GridLayout(1, true));

		m_clonedoc = new Button(l_composite, SWT.CHECK);

		m_clonedoc.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true,
				false, 1, 1));

		m_clonedoc.setText(m_registry.getString("cloneDefDoc.text"));

		m_clonedoc.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				IController controller = ControllerFactory.getInstance()
						.getDefaultController();
				PublishEvent event = new PublishEvent(getPublisher(),
						NationalOilwell.CLONE_DOC_SELECTION_EVENT, m_clonedoc
								.getSelection(), null);
				controller.publish(event);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}
		});

		return true;
	}

	private IPublisher getPublisher() {
		return this;
	}

	@Override
	public boolean reload() throws TCException {
		return false;
	}

	@Override
	public IPropertyMap getData() {
		// TODO Auto-generated method stub
		return null;
	}

	private Button m_clonedoc;
	private Registry m_registry;

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {

		TCComponent selectedItem = propMap.getComponent();

		TCComponentItemRevision partRevisionList = ((TCComponentItem)selectedItem).getLatestItemRevision();
				
		TCComponent[] documentList = partRevisionList
				.getRelatedComponents("RelatedDefiningDocument");

		if (null != documentList && 0 < documentList.length) {
			m_clonedoc.setEnabled(true);
		} else {
			m_clonedoc.setEnabled(false);
		}

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		return false;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException {
		// TODO Auto-generated method stub
		return false;
	}

}
