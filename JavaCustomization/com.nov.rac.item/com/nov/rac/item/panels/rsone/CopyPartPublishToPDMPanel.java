package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class CopyPartPublishToPDMPanel extends RSOneSimplePublishToPDMDocumentPanel implements ISubscriber
{
  
    public CopyPartPublishToPDMPanel(Composite parent, int style)
    {
        super(parent, style);
        registerSubscribers();
    }
    
    private void registerSubscribers()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.CLONE_DOC_SELECTION_EVENT, this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_propMap = new SimplePropertyMap();
        m_propMap.addAll(propMap);
        return true;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.CLONE_DOC_SELECTION_EVENT))
        {
            m_iscloneDocSelected = (Boolean) event.getNewValue();
            if (m_propMap != null)
            {
                if (m_iscloneDocSelected)
                {
                    try
                    {
                        super.load(m_propMap);
                        
                        disableFields();
                    }
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    super.reset();
                    enableFields();
                }
            }
            
        }
    }
    
    private void enableFields()
    {
        m_textDocCategory.setEnabled(true);
        m_BtnDocCategory.setEnabled(true);
      
    }
    
    private void disableFields()
    {
        m_textDocCategory.setEnabled(false);
        m_BtnDocCategory.setEnabled(false);
        m_BtnDocType.setEnabled(false);
        
      
    }
    
    @Override
    public void dispose()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.unregisterSubscriber(NationalOilwell.CLONE_DOC_SELECTION_EVENT, this);
        super.dispose();
    }
    
    private boolean m_iscloneDocSelected = false;
    private IPropertyMap m_propMap = null;
    
}
