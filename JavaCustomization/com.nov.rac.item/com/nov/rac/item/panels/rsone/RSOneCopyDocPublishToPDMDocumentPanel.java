package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyDocPublishToPDMDocumentPanel extends
		RSOneSimplePublishToPDMDocumentPanel 
{

	public RSOneCopyDocPublishToPDMDocumentPanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	@Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        
 //       m_textDatasetType.setEnabled(false);
//        m_dataSetLovPopupButton.getPopupButton().setEnabled(false);
//        m_comboFileTemplate.setEnabled(false);
//        m_textImportFile.setEnabled(false);
//        m_browseButton.setEnabled(false);
        
        UIHelper.makeMandatory(m_textDocCategory);
        
        return true;
    }

}
