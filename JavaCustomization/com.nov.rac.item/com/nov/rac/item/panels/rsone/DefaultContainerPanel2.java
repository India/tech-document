package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;
import com.nov.rac.item.custom.layout.ILayoutProvider;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class DefaultContainerPanel2 extends AbstractUIPanel
{
    Composite m_parent = null;
    Registry m_reg = Registry.getRegistry("com.nov.rac.item.panels.panels");
    
    public DefaultContainerPanel2(Composite parent, int style)
    {
        super(parent, style);
        m_parent = parent;
    }
    
    @Override
    public boolean createUI()
    {
        Composite baseComposite = getComposite();
        ILayoutProvider m_custLayoutProvider = null;
        
        String[] layoutClass = NewItemCreationHelper.getConfiguration(NewItemCreationHelper.getSelectedOperation()
                + "DefaultContainerPanel2.LayoutProvider");
        
        try
        {
            m_custLayoutProvider = (ILayoutProvider) Instancer.newInstanceEx(layoutClass[0]);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        baseComposite.setLayoutData(m_custLayoutProvider.getLayoutData());
        baseComposite.setLayout(m_custLayoutProvider.getLayout());
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
}
