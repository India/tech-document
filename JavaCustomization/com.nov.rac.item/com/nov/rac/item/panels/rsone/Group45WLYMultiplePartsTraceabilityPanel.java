package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.CopyPropertyHelper;
import com.nov.rac.propertymap.IPropertyMap;

public class Group45WLYMultiplePartsTraceabilityPanel extends Group45WLYTraceabilityPanel implements IPublisher
{
    public Group45WLYMultiplePartsTraceabilityPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    protected void createTraceablityLabelFields(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(NUM_OF_COLUMNS_FOR_TRACEABILITY_LABEL_FIELD, false));
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        final Button l_traceCheckbox = new Button(l_composite, SWT.CHECK);
        l_traceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        l_traceCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                handleTraceablityCheckboxSelection();
                
                l_traceCheckbox.setSelection(false);
            }
        });
        
        createTracebilityLabel(l_composite);
        createTraceabilityHelpIcon(l_composite);
    }
    
    private void handleTraceablityCheckboxSelection()
    {
        boolean isLotControlledNASelected = m_lotControlledNA.getSelection();
        boolean isSerializedNASelected = m_serializedNA.getSelection();
        boolean isQARequiredNASelected = m_qaRequiredNA.getSelection();
        
        if (isLotControlledNASelected || isSerializedNASelected || isQARequiredNASelected)
        {
            m_lotControlledTrue.setSelection(false);
            m_lotControlledFalse.setSelection(true);
            m_lotControlledNA.setSelection(false);
            m_serializedTrue.setSelection(false);
            m_serializedFalse.setSelection(true);
            m_serializedNA.setSelection(false);
            m_qaRequiredTrue.setSelection(false);
            m_qaRequiredFalse.setSelection(true);
            m_qaRequiredNA.setSelection(false);
        }
        
        callCopyTracePropertyEvent();
    }
    
    private void callCopyTracePropertyEvent()
    {
        publishCopyPropertyEvent(new String[] { "IMAN_master_form", NationalOilwell.RSONE_LOTCONTROL },
                m_lotControlledTrue.getSelection(), m_registry.getString("Traceability.NAME"));
        publishCopyPropertyEvent(new String[] { "IMAN_master_form", NationalOilwell.RSONE_SERIALIZE },
                m_serializedTrue.getSelection(), m_registry.getString("Traceability.NAME"));
        publishCopyPropertyEvent(new String[] { "IMAN_master_form", NationalOilwell.RSONE_QAREQUIRED },
                m_qaRequiredTrue.getSelection(), m_registry.getString("Traceability.NAME"));
    }
    
    private void publishCopyPropertyEvent(String[] propertyName, Object propValue, String propDisplayName)
    {
        CopyPropertyHelper l_copyProperty = new CopyPropertyHelper();
        l_copyProperty.setProperties(propertyName, propValue, propDisplayName);
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, l_copyProperty, null);
        controller.publish(event);
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private final static int NUM_OF_COLUMNS_FOR_TRACEABILITY_LABEL_FIELD = 3;
}
