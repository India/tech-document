package com.nov.rac.item.panels.rsone;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RelatedDocumentInfoPanel extends AbstractUIPanel implements
		ILoadSave, IPublisher {


	public RelatedDocumentInfoPanel(Composite parent, int style) {
		super(parent, style);
		m_registry = getRegistry();
		m_session = getSession();
	}

	protected TCSession getSession() {
		return (TCSession) AIFUtility.getCurrentApplication().getSession();

	}

	private Registry getRegistry() {
		return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
	}
	
	protected GridData getUILayoutData()
    {
        
        return new GridData(SWT.FILL, SWT.FILL, true, true,
				HORIZONTAL_SPAN_FOR_GRID, 1);
    }
    
    protected GridLayout getUILayout()
    {
        GridLayout l_glDocPanel = new GridLayout(NO_OF_COLUMNS_IN_SUB_COMPOSITE, true);
        l_glDocPanel.marginWidth = 0;
        
        return l_glDocPanel;
    }

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public boolean createUI() throws TCException {

		Composite l_composite = getComposite();

		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1);
		l_composite.setLayoutData(gridData);
		l_composite.setLayout(new GridLayout(NO_OF_COLUMNS_IN_MAIN_COMPOSITE,
				true));

		createNameField(l_composite);

		createSelectedBaseRow(l_composite);

		createDescriptionField(l_composite);

		UIHelper.makeMandatory(m_comboName, false);

		return true;
	}

	private void createNameField(Composite infoPanel) {

		m_nameLabel = new Label(infoPanel, SWT.NONE);
		m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 1, 1));
		m_nameLabel.setText(m_registry
				.getString("RSOneDocumentSimpleInfoPanel.Name"));

		Composite l_nameComposite = new Composite(infoPanel, SWT.NONE);
		l_nameComposite.setLayoutData(getUILayoutData());

		l_nameComposite.setLayout(getUILayout());

		m_comboName = new Combo(l_nameComposite, SWT.BORDER);
		m_comboName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false,
				HORIZONTAL_SPAN_FOR_FIELDS, 1));

		String groupName = m_session.getGroup().toString();
		String[] suggestedValues = RSOneSuggestedByNameLOVHelper
				.getSuggestedNameValues(m_session,
						"_SuggestedNamesByGroupQuery_",
						new String[] { m_registry.getString("Group.NAME") },
						new String[] { groupName },
						m_registry.getString("Name.NAME"));
		m_comboName.setItems(suggestedValues);
		new AutoCompleteField(m_comboName, new ComboContentAdapter(),
				suggestedValues);

		int length = PreferenceHelper.getParsedPrefValueForGroup(
				"NOV_ObjectName_Length_For_Groups",
				TCPreferenceService.TC_preference_site, groupName, "|");
		m_comboName.setTextLimit(length);

	}

	private void createSelectedBaseRow(Composite l_composite) {
		m_selectedBaseLbl = new Label(l_composite, SWT.NONE);
		m_selectedBaseLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				true, true, 1, 1));
		m_selectedBaseLbl.setText(m_registry
				.getString("operationTypePanel.selectedBase"));

		Composite selectedBaseComposite = new Composite(l_composite, SWT.NONE);
		selectedBaseComposite.setLayoutData(getUILayoutData());

		selectedBaseComposite.setLayout(getUILayout());

		m_selectedBaseText = new Text(selectedBaseComposite, SWT.READ_ONLY
				| SWT.BORDER);
		m_selectedBaseText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, true, HORIZONTAL_SPAN_FOR_SELECTED_BASE, 1));
		UIHelper.makeMandatory(m_selectedBaseText);
		
		m_selectedBaseText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent arg0) {
				 IController controller = ControllerFactory.getInstance().getDefaultController();
	                PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.SELECTED_ITEM_MODIFY_EVENT, null, null);
	                controller.publish(event);
				
			}
		});

		GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1);
		
		/*SearchButtonComponent searchButton = new SearchButtonComponent(
				selectedBaseComposite, SWT.TOGGLE);
		GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true,
				false, 2, 1);
		searchBtnGridData.horizontalIndent = SWTUIHelper
				.convertHorizontalDLUsToPixels(searchButton,
						NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
		searchButton.setLayoutData(searchBtnGridData);
		searchButton
				.setSearchItemType(new String[] { NationalOilwell.PART_BO_TYPE,
						NationalOilwell.NON_ENGINEERING });*/
		
		String [] searchItemTypes = {NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(selectedBaseComposite, searchBtnGridData, searchItemTypes);

		new Label(selectedBaseComposite, SWT.NONE).setVisible(false);

	}
	
	private IPublisher getPublisher() {
		return this;
	}

	private void createDescriptionField(Composite infoPanel) {

		m_descriptionLabel = new Label(infoPanel, SWT.NONE);
		m_descriptionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				true, false, 1, 1));
		m_descriptionLabel.setText(m_registry
				.getString("RSOneDocumentSimpleInfoPanel.Description"));

		Composite l_descriptionComposite = new Composite(infoPanel, SWT.NONE);
		l_descriptionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				true, true, HORIZONTAL_SPAN_FOR_TEXTAREA,
				VERTICAL_SPAN_FOR_TEXTAREA));

		l_descriptionComposite.setLayout(getUILayout());

		m_descriptionText = new Text(l_descriptionComposite, SWT.BORDER
				| SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);

		UIHelper.limitCharCountInTextWithPopUpMsg(m_descriptionText,
				NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
				m_registry.getString("ExceedingDescTextLimit.msg"));

		m_descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, HORIZONTAL_SPAN_FOR_FIELDS, VERTICAL_SPAN_FOR_TEXTAREA));
		m_descriptionText
				.setTextLimit(NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT);
		
		UIHelper.enableSelectAll(m_descriptionText);

		new Label(infoPanel, SWT.NONE);
		new Label(infoPanel, SWT.NONE);
	}

	@Override
	public IPropertyMap getData() {
		return null;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		m_comboName.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_NAME)));
		m_selectedBaseText.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.ITEM_ID)));
		m_descriptionText.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_DESC)));

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		PropertyMapHelper.setString(NationalOilwell.OBJECT_NAME, m_comboName
				.getText().trim(), propMap);
		PropertyMapHelper.setString(NationalOilwell.OBJECT_DESC,
				m_descriptionText.getText().trim(), propMap);
		PropertyMapHelper.setString(NationalOilwell.ITEM_ID, m_selectedBaseText
				.getText().trim(), propMap);
		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException {
		return false;
	}

	@Override
	public boolean reload() throws TCException {
		return false;
	}

	private Registry m_registry = null;

	/** The m_name label. */
	private Label m_nameLabel;

	/** The m_description label. */
	private Label m_descriptionLabel;

	/** The m_comboName ComboBox. */
	public Combo m_comboName;

	/** The m_description Text. */
	public Text m_descriptionText;

	private Label m_selectedBaseLbl;
	private Text m_selectedBaseText;
	private TCSession m_session;

	private static final int NO_OF_COLUMNS_IN_MAIN_COMPOSITE = 4;
	private static final int NO_OF_COLUMNS_IN_SUB_COMPOSITE = 7;
	private static final int HORIZONTAL_SPAN_FOR_FIELDS = 6;
	private static final int HORIZONTAL_SPAN_FOR_SELECTED_BASE = 4;
	private static final int HORIZONTAL_SPAN_FOR_TEXTAREA = 3;
	private static final int VERTICAL_SPAN_FOR_TEXTAREA = 3;
	private static final int HORIZONTAL_SPAN_FOR_GRID = 3;
}
