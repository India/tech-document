package com.nov.rac.item.panels.rsone;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.PartFamilySearchProvider;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class CloneADefiningPartPanel extends AbstractUIPanel implements IPublisher, ILoadSave
{
    public CloneADefiningPartPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_oracleIdcomposite = getComposite();
        l_oracleIdcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        l_oracleIdcomposite.setLayout(new GridLayout(1, false));
        
        m_cloneADefiningPartBtn = new Button(l_oracleIdcomposite, SWT.CHECK);
        m_cloneADefiningPartBtn.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true, 1, 1));
        m_cloneADefiningPartBtn.setText(" " + m_registry.getString("CloneADefiningPart.Name"));
        m_cloneADefiningPartBtn.setEnabled(false);
        
        m_cloneADefiningPartBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
            	try 
            	{
            		publishCopyDocumnetEvent();
            		
					setTargetItem();
					
					publishOpearionChangeEvent();
				} 
            	catch (TCException e) 
            	{
					e.printStackTrace();
				}
            }
        });
        
        return true;
    }
    
    private void publishCopyDocumnetEvent() 
    {
    	TCComponent document = NewItemCreationHelper.getTargetComponent();
    	
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.COPY_DOCUMENT_EVENT, document, null);
        
        controller.publish(event);
	}
    
    private void publishOpearionChangeEvent()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.OPERATION_CHANGE_EVENT, "CopyAPartWithDocument", null);
        
        controller.publish(event);
    }
    
    protected void setTargetItem() throws TCException
    {
        INOVResultSet result = null;
      
        PartFamilySearchProvider searchService = new PartFamilySearchProvider();
        result = searchService.executeQuery(searchService);
            
        if(result != null && result.getRows() > 0)
        {
        	Vector<String> rowData = result.getRowData();
        	
        	String puid = rowData.get(0);
            
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
                
            TCComponent tcComponent = tcSession.stringToComponent(puid);
               
            NewItemCreationHelper.setTargetComponent(tcComponent);
        }
        else
        {
        	NewItemCreationHelper.setTargetComponent(null);
        }
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		TCComponent tcComponent = propMap.getTag("RelatedDefiningDocument");
		
		if(tcComponent != null)
		{
			m_cloneADefiningPartBtn.setEnabled(true);
		}
		
		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException 
	{
		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException 
	{
		return true;
	}
    
    protected Registry m_registry;
    protected Button m_cloneADefiningPartBtn;
}
