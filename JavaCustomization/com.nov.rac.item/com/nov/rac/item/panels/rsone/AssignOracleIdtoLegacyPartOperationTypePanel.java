package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.item.panels.RSOneCreateOperationTypePanel;
import com.nov.rac.item.panels.RSOneSimpleOperationTypePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdtoLegacyPartOperationTypePanel extends RSOneSimpleOperationTypePanel/*RSOneCreateOperationTypePanel*/ implements ILoadSave,
        IPublisher
{
    
    private Label m_selectedBaseLbl;
    private Label m_selectedLegacyPartLbl;
    
    private Text m_selectedBaseText;
    private Text m_selectedLegacyPartText;
    
    private GridData m_baseGridData;
    private GridLayout m_baseGridLayout;
    
    private final static int HORIZONTAL_SPAN_FOR_BUTTON = 2;
    private final static int HORIZONTAL_SPAN_FOR_LEGACY_PART = 3;
    
    private final static int HORIZONTAL_SPAN_FOR_TEXT = 4;
    private final static int NUM_OF_COLUMNS_FOR_BASE_ROW = 7;
    
    public AssignOracleIdtoLegacyPartOperationTypePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = true;/*super.createUI()*/;
        
        Composite l_composite = getComposite();
        
        //Tushar added
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(l_gdDocComposite);
        l_composite.setLayout(new GridLayout(5, false));
        
        
        //Tushar commented
//        createEmptyLabel(l_composite);
        
        createSelectedLegacyPartRow(l_composite);
        
      //Tushar commented
//        createEmptyLabel(l_composite);
        
        createSelectedBaseRow(l_composite);
        
        return returnValue;
    }
    
    private void createSelectedLegacyPartRow(Composite l_composite)
    {
        // TODO Auto-generated method stub
        m_selectedLegacyPartLbl = new Label(l_composite, SWT.NONE);
        m_selectedLegacyPartLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        
        m_selectedLegacyPartLbl.setText(m_registry.getString("CreateLegacyPartOperationTypePanel.selectedLegacyPart"));
        
        m_baseGridData = new GridData(SWT.FILL, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_LEGACY_PART, 1);
        m_baseGridLayout = new GridLayout(NUM_OF_COLUMNS_FOR_BASE_ROW, true);
        
        m_baseGridLayout.marginWidth = 0;
        m_baseGridLayout.marginTop = 0;
        
        //Tushar commented
//        Composite selectedLegacyPartComposite = new Composite(l_composite, SWT.NONE);
//        selectedLegacyPartComposite.setLayoutData(m_baseGridData);
//        
//        selectedLegacyPartComposite.setLayout(m_baseGridLayout);
        
        m_selectedLegacyPartText = new Text(/*selectedLegacyPartComposite*/l_composite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedLegacyPartText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, /*HORIZONTAL_SPAN_FOR_TEXT*/4, 1));
        UIHelper.makeMandatory(m_selectedLegacyPartText);
    }
    
    private void createSelectedBaseRow(Composite l_composite)
    {
        m_selectedBaseLbl = new Label(l_composite, SWT.NONE);
        m_selectedBaseLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        m_selectedBaseLbl.setText(m_registry.getString("operationTypePanel.selectedBase"));
        
        //Tushar commented
//        Composite selectedBaseComposite = new Composite(l_composite, SWT.NONE);
//        selectedBaseComposite.setLayoutData(m_baseGridData);
//        
//        selectedBaseComposite.setLayout(m_baseGridLayout);
        
        m_selectedBaseText = new Text(/*selectedBaseComposite*/l_composite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedBaseText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, /*HORIZONTAL_SPAN_FOR_TEXT*/3, 1));
        
        GridData searchBtnGridData = new GridData(SWT.CENTER, SWT.CENTER, false, false, /*HORIZONTAL_SPAN_FOR_BUTTON*/1, 1);
        /*SearchButtonComponent searchButton = new SearchButtonComponent(selectedBaseComposite, SWT.TOGGLE);
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_BUTTON, 1);
        
        searchBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
                NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
        
        searchButton.setLayoutData(searchBtnGridData);
        searchButton.setSearchItemType(new String[] { NationalOilwell.PART_BO_TYPE,  NationalOilwell.NON_ENGINEERING});*/
        
        String [] searchItemTypes = {NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(/*selectedBaseComposite*/l_composite, searchBtnGridData, searchItemTypes);
        
        //Tushar commented
//        new Label(selectedBaseComposite, SWT.NONE).setVisible(false);
        
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        //Tushar commented
//        m_engineeringRadio.setEnabled(false);
//        m_nonengineeringRadio.setEnabled(false);
//        super.load(propMap);
        
        if (m_selectedLegacyPartText.getText() == null || m_selectedLegacyPartText.getText().equals(""))
        {
            m_selectedLegacyPartText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        }
        else
        {
            m_typeCombo.setEnabled(false);
            String itemId = PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID));
            
            itemId = GeneralItemUtils.removeDashnumberFromItemId(itemId);
            m_selectedBaseText.setText(itemId);
        }
        
        publishEvents();
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        
        propMap.setString(NationalOilwell.ITEM_ID, m_selectedLegacyPartText.getText().trim());
        
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, m_selectedBaseText.getText(),
                m_selectedLegacyPartText.getText());
        
        controller.publish(event);
    }
    
    IPublisher getPublisher()
    {
        return this;
    }
}
