package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.RSOneDashTableDialog2;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneOperationTypePanelHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class AddDashNumberPartInfoPanel extends MainPartInfoPanel
{

    public AddDashNumberPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    protected void registerSubscriber()
    {
        super.registerSubscriber();
        
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.EVENT_LOAD_ADD_DASH_NUMBER_BASE_PANEL, this);
        
    }
    
    protected void unRegisterSubscriber()
    {
        super.unRegisterSubscriber();
        
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ADD_DASH_NUMBER_BASE_PANEL, this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        super.update(event);
        
        String propertyName = event.getPropertyName();
        
        if (propertyName.equals(NationalOilwell.EVENT_LOAD_ADD_DASH_NUMBER_BASE_PANEL))
        {
            // this is to set Item Id as target object
            m_selectedBase = event.getNewValue().toString();
        }
    }
    
    @Override
    protected void openDashTableDialog()
    {
        m_dashDialog = new RSOneDashTableDialog2(this.getComposite().getShell(), SWT.BORDER | SWT.NO_TRIM);
        m_dashDialog.open();
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String l_partType = getPartType(propMap);
        
        if (l_partType.equalsIgnoreCase("purchased part") || l_partType.equalsIgnoreCase("Non-Engg Purchased Part"))
        {
            NewItemCreationHelper.setSelectedType(l_partType);
            m_partInfoPanel.dispose();
            
            createPartInfoSection();
            this.getComposite().getParent().layout();
        }
        
        super.load(propMap);
        
        return true;
    }

    private String getPartType(IPropertyMap propMap)
    {
        IPropertyMap l_masterPropertyMap = propMap.getCompound("IMAN_master_form");
        
        boolean engControlled = l_masterPropertyMap.getLogical(NationalOilwell.RSONE_ENGCONTROLLED);
        String l_rsoneItemType = PropertyMapHelper.handleNull(l_masterPropertyMap
                .getString(NationalOilwell.RSONE_ITEMTYPE));
        
        setLovName(engControlled);
        RSOneOperationTypePanelHelper.setSelectedItemType(l_rsoneItemType);
        
        String l_partType = RSOneOperationTypePanelHelper.getPartType();
        return l_partType;
    }
    
    private void setLovName(boolean engControlled)
    {
        if(engControlled)
        {
            RSOneOperationTypePanelHelper.setLoveName("Engineering");
        }
        else
        {
            RSOneOperationTypePanelHelper.setLoveName("Non-Engineering");
        }
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        
        for(String dashNumber : m_dashPropertyMap.keySet())
        {
            m_dashPropertyMap.get(dashNumber).setString("item_id", m_selectedBase + "-" + dashNumber);
        }
        
        IPropertyMap[] propMapArray = new SOAPropertyMap[m_dashPropertyMap.size()];
        propMap.setCompoundArray("dashPropertyMap", m_dashPropertyMap.values().toArray(propMapArray));
        
        return true;
    }
    
    private String m_selectedBase;
}
