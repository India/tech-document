package com.nov.rac.item.panels.rsone;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.item.dialog.NOVRSOneMfgDialog;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.manufacturerhelper.NOVManufacturerHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class SimplePurchasePartInfoPanel extends SimplePartInfoPanel
{
    
    public SimplePurchasePartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_partInfoComposite = getComposite();
        l_partInfoComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_PART_INFO_COMP, true));
        l_partInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, /*1*/6, 1));

        createOrgRow(l_partInfoComposite);
        
        createNameRow(l_partInfoComposite);
        
        createClassRow(l_partInfoComposite);
        
        createSendToClassificationRow(l_partInfoComposite);
        
        createUOMRow(l_partInfoComposite);
        
        createDescRow(l_partInfoComposite);
        
        createMfgRow(l_partInfoComposite);
        
        createTraceabilityRow(l_partInfoComposite);
        
        return true;
    }
    
    @Override
    public boolean createUIPost() throws TCException
    {
        loadMfrNameCombo();
        return true;
    }
    
    protected void createMfgRow(Composite parent)
    {
        createManifactureLabel(parent);
        
        createManifacturerCombo(parent);
        
        createMfgIdFields(parent);
    }
    
    protected void createManifactureLabel(Composite parent)
    {
        Label l_manufacturerLabel = new Label(parent, SWT.NONE);
        l_manufacturerLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_manufacturerLabel.setText(m_registry.getString("PART_INFO_PANEL_MANUFACTURER_LABEL"));
    }
    
    protected void createManifacturerCombo(Composite parent)
    {
        m_manufacturerCombo = new Combo(parent, SWT.NONE);
        m_manufacturerCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
    }
    
    private void loadMfrNameCombo()
    {
        NOVManufacturerHelper novManufacturerHelper = new NOVManufacturerHelper();
        try
        {
            novManufacturerHelper.setMfgStatus("Y");
            novManufacturerHelper.setMfgID("*");
            INOVResultSet theResSet = novManufacturerHelper.execute(INOVSearchProvider.INIT_LOAD_ALL);
            String[] l_activeManfacturers = getActiveMfgs(theResSet);
            
            UIHelper.setAutoComboArray(m_manufacturerCombo, l_activeManfacturers);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        m_manufacturerCombo.select(0);
    }
    
    private String[] getActiveMfgs(INOVResultSet theResSet)
    {
        Vector<String> rowData = theResSet.getRowData();
        Vector<String> objectNames = new Vector<String>();
        
        final int colCount = theResSet.getCols();
        
        for (int i = 0; i < theResSet.getRows(); i++)
        {
            objectNames.add(rowData.elementAt((colCount * i) + 1));
        }
        
        return objectNames.toArray(new String[objectNames.size()]);
    }
    
    protected void createMfgIdFields(Composite parent)
    {
        Composite l_mfgIdFieldsComposite = new Composite(parent, SWT.NONE);
        GridLayout l_mfgIdFieldsLayout = new GridLayout(NO_OF_COLUMNS_FOR_MFG_ID_COMP, true);
        l_mfgIdFieldsLayout.marginHeight = 0;
        l_mfgIdFieldsLayout.marginWidth = 0;
        l_mfgIdFieldsComposite.setLayout(l_mfgIdFieldsLayout);
        l_mfgIdFieldsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_MFG_ID_FIELDS_COMP, 1));
        
        createMfgIdLabel(l_mfgIdFieldsComposite);
        
        createMfgIdText(l_mfgIdFieldsComposite);
        
        createReqMfgButton(l_mfgIdFieldsComposite);
    }
    
    protected void createMfgIdLabel(Composite parent)
    {
        Label l_mfgIdLabel = new Label(parent, SWT.NONE);
        l_mfgIdLabel
                .setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_MFG_ID_LABEL, 1));
        l_mfgIdLabel.setText(m_registry.getString("PART_INFO_PANEL_MANUFACTURER_ID_LABEL"));
    }
    
    protected void createMfgIdText(Composite parent)
    {
        m_mfgIdText = new Text(parent, SWT.BORDER);
        m_mfgIdText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, HORIZONTAL_SPAN_FOR_MFG_ID_TEXT, 1));
    }
    
    protected void createReqMfgButton(Composite parent)
    {
        m_reqMfgIdButton = new Button(parent, SWT.NONE);
        m_reqMfgIdButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_reqMfgIdButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                NOVRSOneMfgDialog mfgDialog = new NOVRSOneMfgDialog(m_reqMfgIdButton.getShell());
                mfgDialog.open();
            }
        });
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        
        IPropertyMap l_revisionPropMap = propMap.getCompound("revision");
        
        if (NewItemCreationHelper.getSelectedType().equalsIgnoreCase(NationalOilwell.PART_BO_TYPE))
        {
            m_manufacturerCombo.setText(PropertyMapHelper.handleNullForArray(l_revisionPropMap
                    .getStringArray(NationalOilwell.MANUFACTURER_NAME))[0]);
            m_mfgIdText.setText(PropertyMapHelper.handleNullForArray(l_revisionPropMap
                    .getStringArray(NationalOilwell.MANUFACTURER_PARTNUMBER))[0]);
        }
        else
        {
            m_manufacturerCombo.setText(PropertyMapHelper.handleNullForArray(l_revisionPropMap
                    .getStringArray(NationalOilwell.NOV4_MFG_NAME))[0]);
            m_mfgIdText.setText(PropertyMapHelper.handleNullForArray(l_revisionPropMap
                    .getStringArray(NationalOilwell.NOV4_MFG_NUMBER))[0]);
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        
        IPropertyMap l_revisionPropMap = propMap.getCompound("revision");
        
        if (NewItemCreationHelper.getSelectedType().equalsIgnoreCase(NationalOilwell.PART_BO_TYPE))
        {
            l_revisionPropMap.setStringArray(NationalOilwell.MANUFACTURER_NAME, new String[] { m_manufacturerCombo
                    .getText().trim() });
            l_revisionPropMap.setStringArray(NationalOilwell.MANUFACTURER_PARTNUMBER, new String[] { m_mfgIdText
                    .getText().trim() });
        }
        else
        {
            l_revisionPropMap.setStringArray(NationalOilwell.NOV4_MFG_NAME, new String[] { m_manufacturerCombo
                    .getText().trim() });
            l_revisionPropMap.setStringArray(NationalOilwell.NOV4_MFG_NUMBER, new String[] { m_mfgIdText.getText()
                    .trim() });
        }
        
        return true;
    }
    
    private Combo m_manufacturerCombo;
    
    private Text m_mfgIdText;
    
    private Button m_reqMfgIdButton;
    
    protected final static int NO_OF_COLUMNS_FOR_MFG_ID_COMP = 8;
    
    protected final static int HORIZONTAL_SPAN_FOR_MFG_ID_FIELDS_COMP = 3;
    protected final static int HORIZONTAL_SPAN_FOR_MFG_ID_LABEL = 3;
    protected final static int HORIZONTAL_SPAN_FOR_MFG_ID_TEXT = 4;
    
}
