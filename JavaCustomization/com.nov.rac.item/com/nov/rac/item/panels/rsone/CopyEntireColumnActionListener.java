package com.nov.rac.item.panels.rsone;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import org.eclipse.swt.widgets.MessageBox;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class CopyEntireColumnActionListener implements ActionListener, ISubscriber
{
    private TCTable m_table;
    private Registry m_registry;
    
    private static int m_columnNo = -1;
    
    public CopyEntireColumnActionListener(TCTable table)
    {
        this.m_table = table;
        m_registry = Registry.getRegistry(this);
        
        registerSubscriber();
    }
    
    public void setColumn(int col)
    {
        m_columnNo = col;
    }
    
    public int getColumn()
    {
        return m_columnNo;
    }
    @Override
    public void actionPerformed(ActionEvent e)
    {
        JCheckBox checkbox = (JCheckBox) e.getSource();
        
        copyEntireColumn(checkbox);
    }
    
    public void copyEntireColumn(final JCheckBox checkbox)
    {
        final Object[] rowData = m_table.getRowData(m_table.getSelectedRow());
        
        Display.getDefault().asyncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (m_table.getSelectedRow() != -1)
                {
                    Object emptyObj = m_table.getValueAt(m_table.getSelectedRow(), getColumn());
                    
                    if (emptyObj != null & !emptyObj.toString().trim().isEmpty())
                    {
                        int returnCode = getMessage(m_registry.getString("Confirmation.text"),
                                m_registry.getString("AllAttribute.msg"), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
                        copyData(checkbox, rowData, returnCode);
                    }
                    else
                    {
                        int returnCode = getMessage(m_registry.getString("Confirmation.text"),
                                m_registry.getString("EmptyRow.msg"), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
                        copyData(checkbox, rowData, returnCode);
                    }
                }
                else
                {
                    getMessage(m_registry.getString("Information.text"), m_registry.getString("SelectRow.msg"),
                            SWT.ICON_INFORMATION | SWT.OK);
                    updateCheckBox(checkbox);
                }
            }
        });
    }
    
    private void copyData(final JCheckBox checkbox, final Object[] rowData, int returnCode)
    {
        if (returnCode == SWT.YES)
        {
            stopCellEditingForCopying();
            
            setColumnValues(rowData);
            updateCheckBox(checkbox);
        }
        else
        {
            updateCheckBox(checkbox);
        }
    }
    
    private void stopCellEditingForCopying()
    {
        int iEditingRow = m_table.getEditingRow();
        int iEditingColumn = m_table.getEditingColumn();
        
        if ((iEditingRow != -1) && (iEditingColumn != -1))
        {
            m_table.getCellEditor(iEditingRow, iEditingColumn).stopCellEditing();
        }
    }
    
    private void setColumnValues(final Object[] rowData)
    {
        for (int iCnt = 0; iCnt < m_table.getModel().getRowCount(); iCnt++)
        {
            if (m_table.isCellEditable(iCnt, getColumn()))
            {
                m_table.getModel().setValueAt(rowData[getColumn()], iCnt, getColumn());
            }
        }
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
        MessageBox dialog = new MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
    private void updateCheckBox(final JCheckBox checkbox)
    {
        checkbox.setSelected(false);
        m_table.updateUI();
        m_table.repaint();
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber(NationalOilwell.DASH_TABLE_SET_COLUMN_EVENT, this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String propertyName = event.getPropertyName();
        
        if(propertyName.compareTo(NationalOilwell.DASH_TABLE_SET_COLUMN_EVENT) == 0)
        {
            setColumn((Integer) event.getNewValue());
        }
    }
}
