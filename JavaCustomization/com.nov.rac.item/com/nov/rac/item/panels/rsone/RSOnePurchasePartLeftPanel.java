package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.teamcenter.rac.kernel.TCException;

public class RSOnePurchasePartLeftPanel extends RSOneDashNumberPanel
{

    public RSOnePurchasePartLeftPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
       super.createUI();
       
       new Label(l_leftPanel, SWT.NONE);
       new Label(l_leftPanel, SWT.NONE);
       new Label(l_leftPanel, SWT.NONE);
       
       return true;
    }
    
}
