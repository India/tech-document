package com.nov.rac.item.panels.rsone;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class SimpleDashInfoPanel extends AbstractUIPanel implements ListSelectionListener, IPublisher
{
    
    public SimpleDashInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        
        m_registry = getRegistry();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_dashComposite = getComposite();
        l_dashComposite.setLayout(new GridLayout(1, false));
        l_dashComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        createDashLabelRow(l_dashComposite);
        createTableRow(l_dashComposite);
        configureTable();
        
        return true;
    }
    
    protected void createDashLabelRow(Composite parent)
    {
        Label dashLabel = new Label(parent, SWT.NONE);
        dashLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        dashLabel.setText(m_registry.getString("DASH_LABEL"));
    }
    
    private void createTableRow(Composite parent)
    {
        Composite l_tableCmposite = new Composite(parent, SWT.EMBEDDED);
        
        l_tableCmposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        m_dashNumberTable = createTable();
        
        ScrollPagePane ScrollPagePane = new ScrollPagePane(m_dashNumberTable);
        ScrollPagePane.setHorizontalScrollBarPolicy(ScrollPagePane.HORIZONTAL_SCROLLBAR_NEVER);
        SWTUIUtilities.embed(l_tableCmposite, ScrollPagePane, false);
    }
    
    private JTable createTable()
    {
        m_dashNumberTableModel = new DefaultTableModel();
        
        return new JTable(m_dashNumberTableModel)
        {
            private static final long serialVersionUID = 1L;
            
            @Override
            public boolean isCellEditable(int i, int j)
            {
                boolean isEditable = true;
                if (j == 0)
                {
                    isEditable = false;
                }
                return isEditable;
            }
        };
    }
    
    protected void configureTable()
    {
        m_dashNumberTableModel.addColumn(new String());
        m_dashNumberTable.getTableHeader().setVisible(false);
        
        //Tushar commented
        m_dashNumberTable.getSelectionModel().addListSelectionListener(this);
        
        TableColumn dashNumberCol = m_dashNumberTable.getColumnModel().getColumn(0);
        dashNumberCol.setCellRenderer(new DashNumberTableCellRenderer());
    }
    
    protected void removeAllRows()
    {
        int l_rowCount = m_dashNumberTable.getRowCount();
        
        for (int rowIndex = 0; l_rowCount > 0 && rowIndex < l_rowCount; rowIndex++)
        {
            m_dashNumberTableModel.removeRow(0);
        }
    }
    
    protected void addRows(String[] dashNumbers)
    {
        int l_rowCount = dashNumbers.length;
        
        for (int rowIndex = 0; rowIndex < l_rowCount; rowIndex++)
        {
            m_dashNumberTableModel.addRow(new String[] { dashNumbers[rowIndex] });
        }
        
        setRowSelection(dashNumbers);
    }
    
    protected void setRowSelection(String[] dashNumbers)
    {
        if (!isRowSelected(dashNumbers))
        {
            m_dashNumberTable.changeSelection(0, 0, false, false);
        }
    }
    
    protected boolean isRowSelected(String[] dashNumbers)
    {
        boolean isSelected = false;
        
        for (int dashIndex = 0; dashNumbers != null && dashIndex < dashNumbers.length; dashIndex++)
        {
            if (dashNumbers[dashIndex].compareTo(getSelectedDash()) == 0)
            {
                m_dashNumberTable.changeSelection(dashIndex, 0, false, false);
                isSelected = true;
            }
        }
        return isSelected;
    }
    
    protected String[] getTableItems()
    {
        int l_rowCount = m_dashNumberTableModel.getRowCount();
        
        String[] l_dashNumbers = new String[l_rowCount];
        
        for (int rowIndex = 0; rowIndex < l_rowCount; rowIndex++)
        {
            l_dashNumbers[rowIndex] = (String) m_dashNumberTableModel.getValueAt(rowIndex, 0);
        }
        return l_dashNumbers;
    }
    
    @Override
    public void valueChanged(ListSelectionEvent listselectionevent)
    {
        if (!listselectionevent.getValueIsAdjusting() && m_dashNumberTable.getSelectedRow() != -1)
        {
            String l_previousSelectedDash = getSelectedDash();
            String l_currentSelectedDash = (String) m_dashNumberTableModel.getValueAt(
                    m_dashNumberTable.getSelectedRow(), 0);
            
            setSelectedDash(l_currentSelectedDash);
            
            publishDashSelectionChangeEvent(l_previousSelectedDash, l_currentSelectedDash);
        }
    }
    
    protected void publishDashSelectionChangeEvent(String previousSelectedDash, String currentSelectedDash)
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.DASH_SELECTION_CHANGE_EVENT,
                currentSelectedDash, previousSelectedDash);
        
        controller.publish(event);
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    public String getSelectedDash()
    {
        return m_selectedDashNumber;
    }
    
    public void setSelectedDash(String selectedDashNumber)
    {
        this.m_selectedDashNumber = selectedDashNumber;
    }
    
    protected void resetTable(String[] dashNumbers)
    {
        removeAllRows();
        addRows(dashNumbers);
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    protected Registry m_registry;
    
    protected JTable m_dashNumberTable;
    
    protected DefaultTableModel m_dashNumberTableModel;
    
    private String m_selectedDashNumber;

}
