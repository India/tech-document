package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class SelectedLegacyPartAndSelectedBasePanel extends AbstractUIPanel implements ILoadSave, IPublisher
{
    private Registry m_registry = null;
    
    private Label m_selectedBaseLbl;
    private Label m_selectedLegacyPartLbl;
    
    private Text m_selectedBaseText;
    private Text m_selectedLegacyPartText;
    
    //Tushar start
    private final static int HORIZONTAL_SPAN_FOR_BUTTON = 2;
    private final static int HORIZONTAL_SPAN_FOR_LEGACY_PART = 2;
    private final static int HORIZONTAL_SPAN_FOR_TEXT = 2;
    private final static int NUM_OF_COLUMNS_FOR_LEGACY_PART_ROW = 3;
    private final static int NUM_OF_COLUMNS_FOR_BASE_ROW = 4;
    private final static int HORIZONTAL_INDENT = 10;
    private final static int NUM_OF_COLUMNS_FOR_COMPOSITE = 2;
    //Tushar End
    
    public SelectedLegacyPartAndSelectedBasePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_composite = getComposite();
        
        // Set composite's layout
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(l_gdDocComposite);
        l_composite.setLayout(new GridLayout( NUM_OF_COLUMNS_FOR_COMPOSITE, true));
        
        createSelectedLegacyPartRow(l_composite);
        
        createSelectedBaseRow(l_composite);
        
        return true;
    }
    
    private void createSelectedLegacyPartRow(Composite l_composite)
    {
        Composite legacyPartComposite = new Composite(l_composite, SWT.NONE);
        GridData legacyGrid = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        legacyPartComposite.setLayoutData(legacyGrid);
        legacyPartComposite.setLayout(new GridLayout(NUM_OF_COLUMNS_FOR_LEGACY_PART_ROW, false));
        
        m_selectedLegacyPartLbl = new Label( legacyPartComposite, SWT.NONE);
        m_selectedLegacyPartLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        m_selectedLegacyPartLbl.setText(m_registry.getString("CreateLegacyPartOperationTypePanel.selectedLegacyPart"));
        
        m_selectedLegacyPartText = new Text( legacyPartComposite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedLegacyPartText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, HORIZONTAL_SPAN_FOR_TEXT, 1));
        UIHelper.makeMandatory(m_selectedLegacyPartText);
    }
    
    private void createSelectedBaseRow(Composite l_composite)
    {
        Composite basePartComposite = new Composite(l_composite, SWT.NONE);
        GridData baseGrid = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        baseGrid.horizontalIndent = HORIZONTAL_INDENT;
        basePartComposite.setLayoutData(baseGrid);
        basePartComposite.setLayout(new GridLayout( NUM_OF_COLUMNS_FOR_BASE_ROW, false));
        
        m_selectedBaseLbl = new Label( basePartComposite, SWT.NONE);
        m_selectedBaseLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        m_selectedBaseLbl.setText(m_registry.getString("operationTypePanel.selectedBase"));
        
        m_selectedBaseText = new Text( basePartComposite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedBaseText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
                HORIZONTAL_SPAN_FOR_LEGACY_PART, 1));
        
        GridData searchBtnGridData = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        /*
         * SearchButtonComponent searchButton = new
         * SearchButtonComponent(selectedBaseComposite, SWT.TOGGLE); GridData
         * searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false,
         * HORIZONTAL_SPAN_FOR_BUTTON, 1); searchBtnGridData.horizontalIndent =
         * SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
         * NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
         * searchButton.setLayoutData(searchBtnGridData);
         * searchButton.setSearchItemType(new String[] {
         * NationalOilwell.PART_BO_TYPE, NationalOilwell.NON_ENGINEERING});
         */
        
        String[] searchItemTypes = { NationalOilwell.PART_BO_TYPE, NationalOilwell.NON_ENGINEERING };
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton( basePartComposite, searchBtnGridData,
                searchItemTypes);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        if (m_selectedLegacyPartText.getText() == null || m_selectedLegacyPartText.getText().equals(""))
        {
            m_selectedLegacyPartText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        }
        else
        {
            String itemId = PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID));
            itemId = GeneralItemUtils.removeDashnumberFromItemId(itemId);
            m_selectedBaseText.setText(itemId);
        }
        
        publishEvents();
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(NationalOilwell.ITEM_ID, m_selectedLegacyPartText.getText().trim());
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, m_selectedBaseText.getText(),
                m_selectedLegacyPartText.getText());
        
        controller.publish(event);
    }
    
    IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
}
