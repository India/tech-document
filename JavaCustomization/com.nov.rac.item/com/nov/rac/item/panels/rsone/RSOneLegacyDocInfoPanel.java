package com.nov.rac.item.panels.rsone;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RSOneLegacyDocInfoPanel extends AbstractUIPanel implements ILoadSave
{
    
    public RSOneLegacyDocInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        m_session = getSession();
    }
    
    protected Registry getRegistry()
    {
        
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    protected static TCSession getSession()
    {
        
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite l_legacyDocComposite = getComposite();
        
        l_legacyDocComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 4, 1));
        l_legacyDocComposite.setLayout(new GridLayout(4, true));
        
        createDocNameRow(l_legacyDocComposite);
        
        createBaseRevRow(l_legacyDocComposite);
        
        createDocDescriptionRow(l_legacyDocComposite);
        
        UIHelper.makeMandatory(m_docNameCombo, false);
        UIHelper.makeMandatory(m_baseText);
        UIHelper.makeMandatory(m_revText);
        
        return false;
    }
    
    protected GridData getUILayoutData()
    {
        
        return new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
    }
    
    protected GridLayout getUILayout()
    {
        
        GridLayout l_glDocPanel = new GridLayout(7, true);
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginHeight = 0;
        
        return l_glDocPanel;
    }
    
    private void createDocNameRow(Composite l_legacyComposite)
    {
        
        m_docNameLabel = new Label(l_legacyComposite, SWT.NONE);
        m_docNameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        m_docNameLabel.setText(m_registry.getString("Name.NAME"));
        
        Composite l_nameComposite = new Composite(l_legacyComposite, SWT.NONE);
        
        l_nameComposite.setLayoutData(getUILayoutData());
        l_nameComposite.setLayout(getUILayout());
        
        m_docNameCombo = new Combo(l_nameComposite, SWT.NONE);
        m_docNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));
        
        String groupName = m_session.getGroup().toString();
        String[] suggestedValues = RSOneSuggestedByNameLOVHelper.getSuggestedNameValues(m_session,
                "_SuggestedNamesByGroupQuery_", new String[] { m_registry.getString("Group.NAME") },
                new String[] { groupName }, m_registry.getString("Name.NAME"));
        
        m_docNameCombo.setItems(suggestedValues);
        
        new AutoCompleteField(m_docNameCombo, new ComboContentAdapter(), suggestedValues);
        
        int length = PreferenceHelper.getParsedPrefValueForGroup(
				"NOV_ObjectName_Length_For_Groups",
				TCPreferenceService.TC_preference_site, groupName, "|");
        m_docNameCombo.setTextLimit(length);
		
        new Label(l_nameComposite, SWT.NONE);
    }
    
    private void createBaseRevRow(Composite l_composite)
    {
        
        m_baseNameLabel = new Label(l_composite, SWT.NONE);
        m_baseNameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        m_baseNameLabel.setText(m_registry.getString("Base.NAME"));
        
        Composite l_baseRevTextComposite = new Composite(l_composite, SWT.NONE);
        l_baseRevTextComposite.setLayoutData(getUILayoutData());
        
        l_baseRevTextComposite.setLayout(getUILayout());
        
        m_baseText = new Text(l_baseRevTextComposite, SWT.BORDER);
        GridData l_gdBaseText = new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1);
        l_gdBaseText.widthHint = 100;
        m_baseText.setLayoutData(l_gdBaseText);
        m_baseText.setTextLimit(32);
        
        m_revLabel = new Label(l_baseRevTextComposite, SWT.NONE);
        GridData l_gdRevLabel = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
        l_gdRevLabel.horizontalIndent = 10;
        m_revLabel.setLayoutData(l_gdRevLabel);
        m_revLabel.setText(m_registry.getString("Rev.NAME"));
        
        m_revText = new Text(l_baseRevTextComposite, SWT.BORDER);
        GridData l_gdRevText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_gdRevText.widthHint = 20;
        m_revText.setLayoutData(l_gdRevText);
        m_revText.setTextLimit(2);
        
        new Label(l_baseRevTextComposite, SWT.NONE);
    }
    
    private void createDocDescriptionRow(Composite l_legacyComposite)
    {
        
        m_descLabel = new Label(l_legacyComposite, SWT.NONE);
        m_descLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 3));
        m_descLabel.setText(m_registry.getString("Description.NAME"));
        
        Composite l_descComposite = new Composite(l_legacyComposite, SWT.NONE);
        l_descComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3));
        
        l_descComposite.setLayout(getUILayout());
        
        m_descText = new Text(l_descComposite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.WRAP);
        GridData l_gdDescText = new GridData(SWT.FILL, SWT.FILL, true, true, 6, 3);
        
        m_descText.setLayoutData(l_gdDescText);
        
        UIHelper.enableSelectAll(m_descText);
        
		UIHelper.limitCharCountInTextWithPopUpMsg(m_descText,
				NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
				m_registry.getString("ExceedingDescTextLimit.msg"));
        
		UIHelper.limitCharCountInTextWithPopUpMsg(m_descText,
				NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
				m_registry.getString("ExceedingDescTextLimit.msg"));
        
        new Label(l_descComposite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        new Label(l_descComposite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        new Label(l_descComposite, SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_docNameCombo.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_NAME)));
        
        m_baseText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        
        m_revText.setText(PropertyMapHelper.handleNull(propMap.getCompound("revision").getString(
                NationalOilwell.ITEM_REVISION_ID)));
        
        m_descText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_DESC)));
        
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(NationalOilwell.ITEM_ID, m_baseText.getText().trim());
        propMap.setString(NationalOilwell.OBJECT_NAME, m_docNameCombo.getText().trim());
        propMap.setString(NationalOilwell.OBJECT_DESC, m_descText.getText().trim());
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        if (masterPropertyMap != null)
        {
            masterPropertyMap.setString(NationalOilwell.RSONE_BASE_NUMBER, m_baseText.getText().trim());
        }
        
        IPropertyMap revisionPropertyMap = propMap.getCompound(NationalOilwell.REVISION);
        if (revisionPropertyMap != null)
        {
            revisionPropertyMap.setString(NationalOilwell.ITEM_REVISION_ID, m_revText.getText());
            propMap.setCompound("revision", revisionPropertyMap);
        }
        
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return false;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        
        return false;
    }
    
    private Label m_docNameLabel;
    private Label m_baseNameLabel;
    private Label m_descLabel;
    private Label m_revLabel;
    
    private Text m_baseText;
    private Text m_descText;
    private Text m_revText;
    
    private Combo m_docNameCombo;
    
    protected Registry m_registry;
    
    protected TCSession m_session;
}
