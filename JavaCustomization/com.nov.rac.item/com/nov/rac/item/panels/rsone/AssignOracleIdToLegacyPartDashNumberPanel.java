package com.nov.rac.item.panels.rsone;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.itemsearch.IdentifierSearchProvider;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdToLegacyPartDashNumberPanel extends RSOneDashNumberPanel implements IPublisher
{
    String m_missingNumStr = null;
    private static final String DASH_STR = "-";
    
    private static final int MAX_THREE_DIGIT_NUMBER = 999;
    private static final int LEAST_FOUR_DIGIT_NUMBER = 1000;
    private static final int LEAST_FIVE_DIGIT_NUMBER = 10000;
    
    public AssignOracleIdToLegacyPartDashNumberPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
        
        m_modifyButton.setVisible(false);
        m_tableButton.setVisible(false);
        
        return returnValue;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponent targetComponent = propMap.getComponent();
        TCComponentItem item = GeneralItemUtils.getItemFromTCComponent(targetComponent);
        
        String[] dashNumberList = null;
        String altId = null;
        
        if (item != null)
        {
            altId = item.getProperty("altid_list");
            if (altId != null && !altId.equals(""))
            {
                // example : get '1218123646' from 1218123646-001@RSOne
                altId = altId.split(DASH_STR)[0];
                
                try
                {
                    IdentifierSearchProvider altIdSearchProvider = new IdentifierSearchProvider(altId + "*");
                    INOVResultSet results = altIdSearchProvider.execute(INOVSearchProvider.INIT_LOAD_ALL);
                    
                    Vector<String> dashListVector = results.getRowData();
                    dashNumberList = getDashNumberList(dashListVector);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                dashNumberList = new String[1];
                dashNumberList[0] = "001";
                m_missingNumStr = "001";
            }
        }
        
        m_dashList.setItems(dashNumberList);
        int missingNumIndex = m_dashList.indexOf(m_missingNumStr);
        
        m_dashList.setSelection(missingNumIndex);
        m_dashList.setEnabled(false);
        
        publishEvents();
        
        return true;
    }
    
    private String[] getDashNumberList(Vector<String> dashListVector)
    {
        // TODO Auto-generated method stub
        String[] dashNumberArray;
        // first remove puid entries from vector if any
        for (int i = 0; i < dashListVector.size(); i++)
        {
            if (!dashListVector.get(i).contains("-"))
            {
                dashListVector.remove(i);
            }
        }
        // list needs to be listed in sorted order with new dash number to be
        // added in it.
        SortedSet<String> dashNumberSet = new TreeSet<String>();
        // separate itemid string from dash number
        for (int i = 0; i < dashListVector.size(); i++)
        {
            if (dashListVector.get(i).contains(DASH_STR))
            {
                String dashNumber = dashListVector.get(i).split(DASH_STR)[1];
                dashNumberSet.add(dashNumber);
            }
        }
        
        dashNumberSet = addNewDashNumber(dashNumberSet);
        dashNumberArray = dashNumberSet.toArray(new String[dashNumberSet.size()]);
        return dashNumberArray;
    }
    
    private SortedSet<String> addNewDashNumber(SortedSet<String> dashNumberSet)
    {
        int missingNum = 0;
        // TODO Auto-generated method stub
        if (Integer.valueOf(dashNumberSet.first()) != 1)
        {
            missingNum = 1;
        }
        else if (Integer.valueOf(dashNumberSet.last()) == dashNumberSet.size())
        {
            missingNum = Integer.valueOf(dashNumberSet.last()) + 1;
        }
        else
        {
            //findFirstMissingDashNumber expects array to start with 0
            dashNumberSet.add("000");
            String[] dashNumberArray = dashNumberSet.toArray(new String[dashNumberSet.size()]);
            missingNum = findFirstMissingDashNumber(dashNumberArray, 0, dashNumberArray.length - 1);
        }
        
        if (missingNum < LEAST_FOUR_DIGIT_NUMBER)
        {
            m_missingNumStr = String.format("%03d", missingNum);
        }
        else if (missingNum > MAX_THREE_DIGIT_NUMBER && missingNum < LEAST_FIVE_DIGIT_NUMBER)
        {
            m_missingNumStr = String.format("%04d", missingNum);
        }
        
        dashNumberSet.add(m_missingNumStr);
        
        return dashNumberSet;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        // super.save is not avoided.
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL, m_missingNumStr, null);
        
        controller.publish(event);
    }
    
    IPublisher getPublisher()
    {
        return this;
    }
    
    private int findFirstMissingDashNumber(String[] array, int startIndex, int endIndex)
    {
        if (startIndex > endIndex)
        {
            return endIndex + 1;
        }
        
        if (startIndex != Integer.valueOf(array[startIndex]))
        {
            return startIndex;
        }
        
        int midIndex = (startIndex + endIndex) / 2;
        
        if (Integer.valueOf(array[midIndex]) > midIndex)
        {
            return findFirstMissingDashNumber(array, startIndex, midIndex);
        }
        else
        {
            return findFirstMissingDashNumber(array, midIndex + 1, endIndex);
        }
    }
    
}
