package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.custom.layout.ILayoutProvider;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;

public class ContainerPanel1 extends AbstractUIPanel
{
    Composite m_parent = null;
    
    public ContainerPanel1(Composite parent, int style)
    {
        super(parent, style);
        m_parent = parent;
    }
    
    @Override
    public boolean createUI()
    {
        Composite Row3comp = getComposite();
       
        String[] layoutClass = NewItemCreationHelper.getConfiguration(NewItemCreationHelper.getSelectedOperation()
                + ".ContainerPanel1.LayoutProvider");
        ILayoutProvider custLayoutProvider = null;
        Composite containerComposite = getComposite();
        
        try
        {
            custLayoutProvider = (ILayoutProvider) Instancer.newInstanceEx(layoutClass[0]);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        containerComposite.setLayoutData(custLayoutProvider.getLayoutData());
        containerComposite.setLayout(custLayoutProvider.getLayout());
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
}
