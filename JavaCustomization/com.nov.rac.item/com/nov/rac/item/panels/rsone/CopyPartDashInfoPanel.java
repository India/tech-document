package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

public class CopyPartDashInfoPanel extends SimpleDashInfoPanel
{

    public CopyPartDashInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    protected void configureTable()
    {
        super.configureTable();
        
        m_dashNumberTableModel.addRow(new String[] { "001" });
        m_dashNumberTable.changeSelection(0, 0, false, false);
    }
}
