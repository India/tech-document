package com.nov.rac.item.panels.rsone;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RSOneDocumentSimpleInfoPanel extends AbstractUIPanel implements
		ILoadSave, IPublisher {


	public RSOneDocumentSimpleInfoPanel(Composite parent, int style) {
		super(parent, style);
		m_registry = getRegistry();
		m_session = getSession();

	}
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
    }
 
	protected TCSession getSession() 
	{
		return (TCSession) AIFUtility.getCurrentApplication().getSession();
	}
	
    protected GridData getUILayoutData()
    {
        
        return new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
    }
    
    protected GridLayout getUILayout()
    {
        
        GridLayout l_glDocPanel = new GridLayout(7, true);
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginHeight = 0;
        
        return l_glDocPanel;
    }
    

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public boolean createUI() throws TCException {

		Composite l_composite = getComposite();

		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1);
		l_composite.setLayoutData(gridData);
		l_composite.setLayout(new GridLayout(4, true));

		createNameField(l_composite);

		createDescriptionField(l_composite);

		UIHelper.makeMandatory(m_comboName, false);

		return true;
	}

	private void createNameField(Composite infoPanel) {
		m_nameLabel = new Label(infoPanel, SWT.NONE);
		m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
				false, 1, 1));
		m_nameLabel.setText(m_registry
				.getString("RSOneDocumentSimpleInfoPanel.Name"));
		
		Composite l_nameComposite = new Composite(infoPanel, SWT.NONE);
	    l_nameComposite.setLayoutData(getUILayoutData());
	           
	    l_nameComposite.setLayout(getUILayout());

		m_comboName = new Combo(l_nameComposite, SWT.BORDER);
		m_comboName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false,
				6, 1));

		String groupName = m_session.getGroup().toString();
		String[] suggestedValues = RSOneSuggestedByNameLOVHelper
				.getSuggestedNameValues(m_session,
						"_SuggestedNamesByGroupQuery_",
						new String[] { m_registry.getString("Group.NAME") },
						new String[] { groupName },
						m_registry.getString("Name.NAME"));
		m_comboName.setItems(suggestedValues);
		new AutoCompleteField(m_comboName, new ComboContentAdapter(),
				suggestedValues);

		int length = PreferenceHelper.getParsedPrefValueForGroup(
				"NOV_ObjectName_Length_For_Groups",
				TCPreferenceService.TC_preference_site, groupName, "|");
		m_comboName.setTextLimit(length);

	}

	private void createDescriptionField(Composite infoPanel) 
	{
		m_descriptionLabel = new Label(infoPanel, SWT.NONE);
		m_descriptionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				true, false, 1, 1));
		m_descriptionLabel.setText(m_registry
				.getString("RSOneDocumentSimpleInfoPanel.Description"));

		Composite l_descriptionComposite = new Composite(infoPanel, SWT.NONE);
		l_descriptionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				true, true, 3, 3));

		l_descriptionComposite.setLayout(getUILayout());

		m_descriptionText = new Text(l_descriptionComposite, SWT.BORDER
				| SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);

		UIHelper.limitCharCountInTextWithPopUpMsg(m_descriptionText,
				NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
				m_registry.getString("ExceedingDescTextLimit.msg"));

		m_descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 6, 3));
		UIHelper.enableSelectAll(m_descriptionText);

		new Label(infoPanel, SWT.NONE);
		new Label(infoPanel, SWT.NONE);
		// new Label(infoPanel, SWT.NONE);
	}

	@Override
	public boolean reload() throws TCException {

		return false;
	}

	@Override
	public IPropertyMap getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {

		m_comboName.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_NAME)));

		m_descriptionText.setText(PropertyMapHelper.handleNull(propMap
				.getString(NationalOilwell.OBJECT_DESC)));

		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		setItemProperties(propMap);
		return true;
	}

	private void setItemProperties(IPropertyMap propMap) {

		propMap.setString("object_name", m_comboName.getText().trim());
		propMap.setString("object_desc", m_descriptionText.getText().trim());
		// propMap.setString("item_id", NOVRSOneItemIDHelper.getOracleId());
	}

	@Override
	public boolean reset() {
		m_comboName.setText("");
		m_descriptionText.setText("");
		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException {
		// TODO Auto-generated method stub
		return false;
	}

	private Registry m_registry = null;

	private TCSession m_session;

	/** The m_name label. */
	private Label m_nameLabel;

	/** The m_description label. */
	private Label m_descriptionLabel;

	/** The m_comboName ComboBox. */
	public Combo m_comboName;

	/** The m_description Text. */
	public Text m_descriptionText;

}
