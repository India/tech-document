package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.item.panels.RSOneCreateOperationTypePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class AddDashNumberSelectedBasePanel extends AbstractUIPanel implements ILoadSave,ISubscriber, IPublisher
{ 
    private Label m_selectedBaseLbl;
    private Text m_selectedBaseText;
    
    private final static int HORIZONTAL_SPAN_FOR_BUTTON = 2;
    private final static int HORIZONTAL_SPAN_FOR_TEXT = 4;
    
    private final static int NUM_OF_COLUMNS_FOR_BASE_ROW = 7;
    
    protected Registry m_registry;
    
    protected GridData m_gdDocPanel;
    protected GridLayout m_glDocPanel;
    
    public AddDashNumberSelectedBasePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry() 
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite l_composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
        
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(5, true));
        
        createEmptyLabel(l_composite);
        
        //createEmptyLabel(l_composite);
        
        createSelectedBaseRow(l_composite);
        
      //  createEmptyLabel(l_composite);
        
        return true;
    }
    
    protected void createEmptyLabel(Composite l_composite)
    {
        Label emptyLabel = new Label(l_composite, SWT.None);
        emptyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
    }
    
    private void createSelectedBaseRow(Composite l_composite)
    {
        Composite composite = new Composite(l_composite, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 4, 1);
        
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(4, true));
        
        m_selectedBaseLbl = new Label(composite, SWT.NONE);
        m_selectedBaseLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        m_selectedBaseLbl.setText(m_registry.getString("operationTypePanel.selectedBase"));
        
        m_gdDocPanel = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
        m_glDocPanel = new GridLayout(NUM_OF_COLUMNS_FOR_BASE_ROW, true);
        m_glDocPanel.marginWidth = 0;
        m_glDocPanel.marginTop = 0;
        
        Composite selectedBaseComposite = new Composite(composite, SWT.NONE);
        selectedBaseComposite.setLayoutData(m_gdDocPanel);
        
        selectedBaseComposite.setLayout(m_glDocPanel);
        
        m_selectedBaseText = new Text(selectedBaseComposite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedBaseText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_TEXT, 1));
        UIHelper.makeMandatory(m_selectedBaseText);
        
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_BUTTON, 1);
        /*SearchButtonComponent searchButton = new SearchButtonComponent(selectedBaseComposite, SWT.TOGGLE);
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_BUTTON, 1);
        searchBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
                NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
        searchButton.setLayoutData(searchBtnGridData);*/
        //searchButton.setSearchItemType(new String[] { NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING });
       
        String [] searchItemTypes = {NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(selectedBaseComposite, searchBtnGridData, searchItemTypes);
        new Label(selectedBaseComposite, SWT.NONE).setVisible(false);
        
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
/*        TCComponent alternateIdTag = propMap.getTagArray(NationalOilwell.ALTERNATE_ID_LIST)[0];
        if(alternateIdTag != null)
        {
            String alternateId = alternateIdTag.getProperty("idfr_id");
            alternateId = alternateId.split("-")[0];
            m_selectedBaseText.setText(alternateId);
        }*/
        

        String itemId = PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID));
        itemId = itemId.split("-")[0];
        m_selectedBaseText.setText(itemId);
        
        publishEvents();

        
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(this,
                NationalOilwell.EVENT_LOAD_ADD_DASH_NUMBER_BASE_PANEL, m_selectedBaseText.getText(),
                null);
        
        controller.publish(event);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
/*        super.save(propMap);
        
        propMap.setString(NationalOilwell.ITEM_ID, m_selectedLegacyPartText.getText().trim());
     */   
        return true;
    }
    
    private void registerSubscriber()
    {
/*        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber("ItemSearch", this);*/
    }
    
    private void unRegisterSubscriber()
    {
/*        IController theController = ControllerFactory.getInstance()
                .getDefaultController();
        theController.unregisterSubscriber("ItemSearch", this);*/
    }

    @Override
    public void update(PublishEvent event)
    {
        // TODO Auto-generated method stub
/*        String propertyName = event.getPropertyName();
        if (propertyName.equals("ItemSearch"))
        {
          //this is to control load when searched object is loaded in panel
            m_SearchResultsAvailable = true;
        }*/
    }
    
    @Override
    public void dispose()
    {
        // TODO Auto-generated method stub
        unRegisterSubscriber();
        super.dispose();
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
}
