package com.nov.rac.item.panels.rsone;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.CopyPropertyHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class MultiplePartsInfoPanel extends SimplePartInfoPanel implements IPublisher
{
    
    public MultiplePartsInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    private Composite getComposite(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(2, false));
        l_composite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        return l_composite;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_partInfoComposite = getComposite();
        l_partInfoComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_PART_INFO_COMP, true));
        l_partInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 7, 1));
        
        createCopyAllBtnRow(l_partInfoComposite);
        
        createOrgRow(l_partInfoComposite);
        
        createNameRow(l_partInfoComposite);
        
        createClassRow(l_partInfoComposite);
        
        createSendToClassificationRow(l_partInfoComposite);
        
        createUOMRow(l_partInfoComposite);
        
        createDescRow(l_partInfoComposite);
        
        createTraceabilityRow(l_partInfoComposite);
        
        return true;
    }
    
    protected void createCopyAllBtnRow(Composite parent) throws TCException
    {
        new Label(parent, SWT.NONE);
        
        final Button l_copyAllButton = new Button(parent, SWT.NONE);
        
        l_copyAllButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_COPY_ALL_BUTTON, 1));
        l_copyAllButton.setText(m_registry.getString("PART_INFO_PANEL_COPY_ALL_BUTTON_TEXT"));
        
        l_copyAllButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionevent)
            {
                int msgCode = getMessage(m_registry.getString("confirmation.msg"), m_registry.getString("copyAll.msg"),
                        SWT.YES | SWT.NO | SWT.ICON_QUESTION);
                
                if (msgCode == SWT.YES)
                {
                    IController controller = ControllerFactory.getInstance().getDefaultController();
                    PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.RSONE_COPY_ALL_PROPERTY,
                            null, null);
                    controller.publish(event);
                }
                
                l_copyAllButton.setSelection(false);
            }
        });
    }
    
    @Override
    protected void createOrgLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        final Button l_orgCheckbox = new Button(l_composite, SWT.CHECK);
        l_orgCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        l_orgCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                handleCheckboxSelection(m_orgCombo, new String[] { "IMAN_master_form", NationalOilwell.RSONE_ORG },
                        new String[] { m_orgCombo.getText() }, "Org.NAME");
                
                l_orgCheckbox.setSelection(false);
            }
        });
        
        super.createOrgLabel(l_composite);
    }
    
    @Override
    protected void createNameLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        final Button l_nameCheckbox = new Button(l_composite, SWT.CHECK);
        l_nameCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        l_nameCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                handleCheckboxSelection(m_nameCombo, new String[] { NationalOilwell.OBJECT_NAME },
                        m_nameCombo.getText(), "Name.NAME");
                
                l_nameCheckbox.setSelection(false);
            }
        });
        
        super.createNameLabel(l_composite);
    }
    
    @Override
    protected void createClassLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        final Button l_ClassCheckbox = new Button(l_composite, SWT.CHECK);
        l_ClassCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        l_ClassCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                handleCheckboxSelection(m_classText, new String[] { NationalOilwell.ICS_subclass_name },
                        m_classText.getText(), "Class.NAME");
                
                l_ClassCheckbox.setSelection(false);
            }
        });
        
        super.createClassLabel(l_composite);
    }
    
    @Override
    protected void createUOMLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        final Button l_uomCheckbox = new Button(l_composite, SWT.CHECK);
        l_uomCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        l_uomCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                handleCheckboxSelection(m_uomCombo, new String[] { "IMAN_master_form", NationalOilwell.RSONE_UOM },
                        m_uomCombo.getText(), "UOM.NAME");
                
                l_uomCheckbox.setSelection(false);
            }
        });
        
        super.createUOMLabel(l_composite);
    }
    
    @Override
    protected void createDescLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        final Button l_descCheckbox = new Button(l_composite, SWT.CHECK);
        l_descCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        
        l_descCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                
                handleCheckboxSelection(m_descText, new String[] { NationalOilwell.OBJECT_DESC }, m_descText.getText(),
                        "Description.NAME");
                
                l_descCheckbox.setSelection(false);
            }
        });
        
        super.createDescLabel(l_composite);
    }
    
    private void handleCheckboxSelection(Control control, String[] propertyName, Object value, String propDisplayName)
    {
        String textInControl = "";
        
        if (control instanceof Combo)
        {
            textInControl = ((Combo) control).getText();
        }
        else if (control instanceof Text)
        {
            textInControl = ((Text) control).getText();
        }
        if (textInControl.isEmpty())
        {
            int msgCode = getMessage(m_registry.getString("confirmation.msg"), m_registry.getString(propDisplayName)
                    + " " + m_registry.getString("Copy_Empty_Value.msg"), SWT.YES | SWT.NO | SWT.ICON_QUESTION);
            
            if (msgCode == SWT.YES)
            {
                publishCopyPropertyEvent(propertyName, value, m_registry.getString(propDisplayName));
            }
        }
        else
        {
            publishCopyPropertyEvent(propertyName, value, m_registry.getString(propDisplayName));
        }
    }
    
    @Override
    protected void createTraceabilityRow(Composite parent)
    {
        ArrayList<IUIPanel> l_panels = createPanel("MULTIPLE_TRACEABILITY_PANEL", parent);
        
        // ArrayList<IUIPanel> will contain just one panel
        m_traceabilityPanel = l_panels.get(0);
    }
    
    private void publishCopyPropertyEvent(String[] propertyName, Object propValue, String propDisplayName)
    {
        CopyPropertyHelper l_copyProperty = new CopyPropertyHelper();
        l_copyProperty.setProperties(propertyName, propValue, propDisplayName);
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, l_copyProperty, null);
        controller.publish(event);
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
        org.eclipse.swt.widgets.MessageBox dialog = new org.eclipse.swt.widgets.MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private final static int HORIZONTAL_SPAN_FOR_COPY_ALL_BUTTON = 5;
}
