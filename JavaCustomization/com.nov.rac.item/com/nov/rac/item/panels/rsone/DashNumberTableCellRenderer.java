package com.nov.rac.item.panels.rsone;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.util.iTextField;


public class DashNumberTableCellRenderer implements TableCellRenderer
{
    private iTextField dashNumberText;
    
    public DashNumberTableCellRenderer()
    {
        dashNumberText  = new iTextField();
    }
    
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        
        dashNumberText.setBorder(BorderFactory.createEmptyBorder());
        dashNumberText.setRequired(true);
        
        if(value != null)
        {
            dashNumberText.setText(value.toString());
        }
        
        if(table.getSelectedRow() == row)
        {
            dashNumberText.setBackground(table.getSelectionBackground());
            dashNumberText.setForeground(table.getSelectionForeground());
        }
        else
        {
            dashNumberText.setBackground(table.getBackground());
            dashNumberText.setForeground(table.getForeground());
        }

        return dashNumberText;
    }
}
