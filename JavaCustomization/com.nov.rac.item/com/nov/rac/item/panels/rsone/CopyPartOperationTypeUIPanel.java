package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.item.panels.RSOneCreateOperationTypePanel;
import com.nov.rac.item.panels.RSOneSimpleOperationTypePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class CopyPartOperationTypeUIPanel extends /*RSOneCreateOperationTypePanel*/ RSOneSimpleOperationTypePanel
{

    public CopyPartOperationTypeUIPanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = true/*super.createUI()*/;
        
        Composite l_composite = getComposite();
        
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(l_gdDocComposite);
        l_composite.setLayout(new GridLayout(5, false));
        
//        createEmptyLabel(l_composite);
        
        createSelectedBaseRow(l_composite);
        
        return returnValue;
    }

    private void createSelectedBaseRow(Composite l_composite)
    {
        m_selectedBaseLbl = new Label(l_composite, SWT.NONE);
        m_selectedBaseLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, /*true*/ false, true, 1, 1));
        m_selectedBaseLbl.setText(m_registry.getString("operationTypePanel.selectedBase"));
        
        //Tushar commented
//        Composite selectedBaseComposite = new Composite(l_composite, SWT.NONE);
//        selectedBaseComposite.setLayoutData(getSubCompositeLayoutData());
//        
//        selectedBaseComposite.setLayout(getSubCompositeLayout());
        
        m_selectedBaseText = new Text(/*selectedBaseComposite*/l_composite, SWT.READ_ONLY | SWT.BORDER);
        m_selectedBaseText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, /*4*/ 3, 1));
        UIHelper.makeMandatory(m_selectedBaseText);
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, /*true*/false, false, /*2*/ 1, 1);
        /*SearchButtonComponent searchButton = new SearchButtonComponent(selectedBaseComposite, SWT.TOGGLE);
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
        searchBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
                NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
        searchButton.setLayoutData(searchBtnGridData);
        searchButton.setSearchItemType(new String[] { NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING });*/
        String [] searchItemTypes = {NationalOilwell.PART_BO_TYPE , NationalOilwell.NON_ENGINEERING};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(/*selectedBaseComposite*/l_composite, searchBtnGridData, searchItemTypes);
        
//        new Label(selectedBaseComposite, SWT.NONE).setVisible(false);
        
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
//        super.load(propMap);
        
        String itemId = PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID));
        itemId = itemId.split("-")[0];
        m_selectedBaseText.setText(itemId);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException 
    {
    	
        //Tushar Commented
//        super.save(propMap);
    	
    	propMap.setString(NationalOilwell.ITEM_ID , m_selectedBaseText.getText().trim());
    	
    	return true;
    }
    
    private Label m_selectedBaseLbl;
    private Text m_selectedBaseText;

}
