package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.item.panels.RSOneSimpleDocCategoryPanel;
import com.nov.rac.item.panels.SimpleDocumentCreatePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCException;

public class RSOneSimplePublishToPDMDocumentPanel extends SimpleDocumentCreatePanel implements ILoadSave
{
	
	public RSOneSimplePublishToPDMDocumentPanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	@Override
	public boolean createUI() throws TCException
	{
		boolean retCode = false;
		
        Composite l_composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_MAIN_COMPOSITE, 1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_MAIN_COMPOSITE, true));
        
		
		
	    retCode = super.createUI();
		
	    createPublishToPDMCheckBoxRow(subComposite);
	    
		return retCode;
	}

	protected void createPublishToPDMCheckBoxRow(Composite parent) throws TCException
    {
            
	    Label lbl = new Label(parent, SWT.NONE);
        Composite l_publishToPDMComposite = new Composite(parent, SWT.NONE);
        GridLayout gl_PDM = new GridLayout(1, true);
      
        gl_PDM.marginWidth = 0;
      
        l_publishToPDMComposite.setLayout(gl_PDM);
        
       
        Button l_publishToPDMCheckbox = new Button(l_publishToPDMComposite, SWT.CHECK);
        l_publishToPDMCheckbox.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_PUBLISH_TO_PDM_CHECKBOX, 1));
        
        l_publishToPDMCheckbox.setText(" " + m_registry.getString("PublishToPDM.Name"));
        
        addListener(l_publishToPDMCheckbox);
        
      
    }
	
	private void addListener(final Button checkBox) 
	{
		checkBox.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent event)
			{
				if(checkBox.getSelection())
				{
					m_isPublishToPDM = true;
				}
			}
		});
	}
	
	@Override
	public boolean load(IPropertyMap propMap) throws TCException
	{
	    super.load(propMap);   
	    
		return true;
	}
	    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
    	super.save(propMap);
    	
        return true;
    }
    
	
private static final int HORIZONTAL_SPAN_FOR_PUBLISH_TO_PDM_CHECKBOX = 2;
private static final int HORIZONTAL_SPAN_FOR_MAIN_COMPOSITE = 4;
private static final int NO_OF_COLUMNS_FOR_MAIN_COMPOSITE = 4;

private boolean m_isPublishToPDM = false;

}
