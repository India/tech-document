package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class CreatePartInfoPanel extends MainPartInfoPanel
{

    public CreatePartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        String oracleId = NOVRSOneItemIDHelper.getOracleId();
        
        for(String dashNumber : m_dashPropertyMap.keySet())
        {
            m_dashPropertyMap.get(dashNumber).setString("item_id", oracleId + "-" + dashNumber);
        }
        
        IPropertyMap[] propMapArray = new SOAPropertyMap[m_dashPropertyMap.size()];
        propMap.setCompoundArray("dashPropertyMap", m_dashPropertyMap.values().toArray(propMapArray));
        
        return true;
    }
    
}
