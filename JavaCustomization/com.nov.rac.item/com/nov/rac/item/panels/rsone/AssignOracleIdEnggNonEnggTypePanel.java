package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.RSOneSimpleEnggNonEnggTypePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdEnggNonEnggTypePanel extends RSOneSimpleEnggNonEnggTypePanel
{

    public AssignOracleIdEnggNonEnggTypePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
        
        m_engineeringRadio.setEnabled(false);
        m_nonengineeringRadio.setEnabled(false);
        
        return returnValue;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        return super.load(propMap);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return super.save(propMap);
    }
}
