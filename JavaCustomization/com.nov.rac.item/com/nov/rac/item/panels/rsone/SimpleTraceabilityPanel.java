package com.nov.rac.item.panels.rsone;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.MandatoryLabelFieldValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SimpleTraceabilityPanel extends AbstractUIPanel implements ILoadSave
{
    public SimpleTraceabilityPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite traceabilityComposite = getComposite();
        GridLayout gd_rowComp = new GridLayout(NUM_OF_COLUMNS_FOR_TRACEABILITY_COMPOSITE, true);
        gd_rowComp.marginHeight = 0;
        gd_rowComp.marginWidth = 0;
        traceabilityComposite.setLayout(gd_rowComp);
        traceabilityComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, HORIZONTAL_SPAN_FOR_TRACEABILITY_COMPOSITE, 1));
        
        createTraceablityLabelFields(traceabilityComposite);
        createTraceabilityFields(traceabilityComposite);
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_lotControlledButton.setSelection(propMap.getLogical(NationalOilwell.RSONE_LOTCONTROL));
        m_serializedButton.setSelection(propMap.getLogical(NationalOilwell.RSONE_SERIALIZE));
        m_qaRequiredButton.setSelection(propMap.getLogical(NationalOilwell.RSONE_QAREQUIRED));
        
        enableDisableButtons();
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setLogical(NationalOilwell.RSONE_LOTCONTROL, m_lotControlledButton.getSelection());
        propMap.setLogical(NationalOilwell.RSONE_SERIALIZE, m_serializedButton.getSelection());
        propMap.setLogical(NationalOilwell.RSONE_QAREQUIRED, m_qaRequiredButton.getSelection());
        
        enableDisableButtons();
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    protected void createTraceablityLabelFields(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(2, false));
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        createTracebilityLabel(l_composite);
        createTraceabilityHelpIcon(l_composite);
    }
    
    protected void createTraceabilityHelpIcon(Composite composite)
    {
        Label l_helpIconLabel = new Label(composite, SWT.NONE);
        GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
        gd.horizontalIndent = HELP_ICON_HORIZONTAL_INDENT;
        l_helpIconLabel.setLayoutData(gd);
        
        Image l_helpIcon = new Image(composite.getDisplay(), m_registry.getImage("help.Icon"), SWT.NONE);
        l_helpIconLabel.setImage(l_helpIcon);
        l_helpIconLabel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseDown(MouseEvent arg0)
            {
                String helpURL = "cmd /c start " + m_registry.getString("traceabilityHelp.LINK");
                try
                {
                    Runtime.getRuntime().exec(helpURL);
                }
                catch (IOException e1)
                {
                    e1.printStackTrace();
                    MessageBox.post(NationalOilwell.NOHELP, NationalOilwell.SEEADMIN, MessageBox.ERROR);
                }
            }
        });
    }
    
    protected void createTracebilityLabel(Composite composite)
    {
        m_traceabilityLabel = new Label(composite, SWT.NONE);
        m_traceabilityLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_traceabilityLabel.setText(m_registry.getString("PART_INFO_PANEL_TRACEABILITY_LABEL"));
    }
    
    protected void createTraceabilityFields(Composite parent)
    {
        Composite l_traceFieldsComposite = new Composite(parent, SWT.NONE);
        GridLayout l_traceFieldsLayout = new GridLayout(NO_OF_COLUMNS_FOR_TRACE_FIELDS_COMP, false);
        l_traceFieldsLayout.marginHeight = 0;
        l_traceFieldsLayout.marginWidth = 0;
        l_traceFieldsComposite.setLayout(l_traceFieldsLayout);
        l_traceFieldsComposite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_TRACE_FIELDS_COMP, 1));
        
        SelectionAdapter l_listener = new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                enableDisableButtons();
            }
        };
        
        m_lotControlledButton = new Button(l_traceFieldsComposite, SWT.CHECK);
        m_lotControlledButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        m_lotControlledButton.setText(m_registry.getString("PART_INFO_PANEL_LOT_CONTROLLED_LABEL"));
        m_lotControlledButton.addSelectionListener(l_listener);
        
        m_serializedButton = new Button(l_traceFieldsComposite, SWT.CHECK);
        m_serializedButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        m_serializedButton.setText(m_registry.getString("PART_INFO_PANEL_SERIALIZED_LABEL"));
        m_serializedButton.addSelectionListener(l_listener);
        
        m_qaRequiredButton = new Button(l_traceFieldsComposite, SWT.CHECK);
        m_qaRequiredButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        m_qaRequiredButton.setText(m_registry.getString("PART_INFO_PANEL_QA_REQUIRED_LABEL"));
        m_qaRequiredButton.addSelectionListener(l_listener);
    }
    
    protected GridLayout getLayout(int numofColumns, boolean equalWidth)
    {
        GridLayout l_layout = new GridLayout(numofColumns, equalWidth);
        l_layout.marginHeight = 0;
        l_layout.marginWidth = 0;
        
        return l_layout;
    }
    
    protected void enableDisableButtons()
    {
        boolean isLotControlled = m_lotControlledButton.getSelection();
        boolean isSerialized = m_serializedButton.getSelection();
        boolean isNA = m_qaRequiredButton.getSelection();
        
        if (isLotControlled || isSerialized || isNA)
        {
            m_labelFieldValidator.setValid(true);
            moveTraceabilityLabel();
        }
        else
        {
            m_labelFieldValidator.setValid(false);
            moveTraceabilityLabel();
        }
        
        if (isLotControlled || isSerialized)
        {
            m_qaRequiredButton.setEnabled(false);
            m_qaRequiredButton.setSelection(false);
        }
        else
        {
            m_qaRequiredButton.setEnabled(true);
        }
        
        if (isNA)
        {
            m_lotControlledButton.setEnabled(false);
            m_lotControlledButton.setSelection(false);
            
            m_serializedButton.setEnabled(false);
            m_serializedButton.setSelection(false);
        }
        else
        {
            m_lotControlledButton.setEnabled(true);
            m_serializedButton.setEnabled(true);
        }
    }
    
    protected void moveTraceabilityLabel()
    {
        Rectangle rect = m_traceabilityLabel.getBounds();
        m_traceabilityLabel.setLocation(rect.x + 1, rect.y);
        m_traceabilityLabel.setLocation(rect.x, rect.y);
    }
    
    protected final static int HORIZONTAL_SPAN_FOR_TRACEABILITY_COMPOSITE = 6;
    protected final static int NUM_OF_COLUMNS_FOR_TRACEABILITY_COMPOSITE = 6;
    protected final static int NO_OF_COLUMNS_FOR_TRACE_FIELDS_COMP = 3;
    protected final static int HORIZONTAL_SPAN_FOR_TRACE_FIELDS_COMP = 5;
    private static final int HELP_ICON_HORIZONTAL_INDENT = 6;
    
    protected MandatoryLabelFieldValidator m_labelFieldValidator = new MandatoryLabelFieldValidator();
    protected Label m_traceabilityLabel;
    protected Button m_lotControlledButton;
    protected Button m_serializedButton;
    protected Button m_qaRequiredButton;
    
    protected Registry m_registry;
}
