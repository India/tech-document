package com.nov.rac.item.panels.rsone;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.RSOneDashTableDialog;
import com.nov.rac.item.helpers.CopyPropertyHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneDashPropertyMap;
import com.nov.rac.item.panels.factory.PanelFactory;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class MainPartInfoPanel extends AbstractUIPanel implements IPublisher, ISubscriber, ILoadSave
{
    
    public MainPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        
        m_registry = getRegistry();
        
        registerSubscriber();
    }
    
    protected IPropertyMap getPartPropMap()
    {
        IPropertyMap l_initialPropertyMap = new SOAPropertyMap(NationalOilwell.PART_BO_TYPE);
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(l_initialPropertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        return l_initialPropertyMap;
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
    }
    
    protected void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.registerSubscriber(NationalOilwell.PART_TYPE_CHANGE_EVENT, this);
        theController.registerSubscriber(NationalOilwell.MSG_DASH_INFO_MODIFY_EVENT, this);
        theController.registerSubscriber(NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT, this);
        theController.registerSubscriber(NationalOilwell.DASH_SELECTION_CHANGE_EVENT, this);
        theController.registerSubscriber(NationalOilwell.RSONE_COPY_ALL_PROPERTY, this);
        theController.registerSubscriber(NationalOilwell.RSONE_COPY_PROPERTY, this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        m_mainComposite = getComposite();
        m_mainComposite.setLayout(new GridLayout(8, true));
        m_mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        createDashSection();
        
        createPartInfoSection();
        
        return true;
    }
    
    private void createDashSection()
    {
        String l_context = NewItemCreationHelper.getSelectedOperation() + NationalOilwell.DOTSEPARATOR
                + NewItemCreationHelper.getSelectedType() + NationalOilwell.DOTSEPARATOR + "LEFT_PANEL";
        
        ArrayList<IUIPanel> l_panels = createPanel(l_context);
        
        m_dashInfoPanel = l_panels.get(0);
    }
    
    protected void createPartInfoSection()
    {
        String l_context = NewItemCreationHelper.getSelectedOperation() + NationalOilwell.DOTSEPARATOR
                + NewItemCreationHelper.getSelectedType() + NationalOilwell.DOTSEPARATOR + "PART_INFO_PANEL";
        
        ArrayList<IUIPanel> l_panels = createPanel(l_context);
        
        m_partInfoPanel = l_panels.get(0);
        
        this.getComposite().layout();
    }
    
    private ArrayList<IUIPanel> createPanel(String context)
    {
        String[] l_panelNames = NewItemCreationHelper.getConfiguration(context, m_registry);
        
        PanelFactory l_panelFactory = PanelFactory.getInstance();
        
        ArrayList<IUIPanel> panels = l_panelFactory.createPanels(m_mainComposite, l_panelNames, false);
        
        return panels;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String proertyName = event.getPropertyName();
        
        if (proertyName.compareTo(NationalOilwell.PART_TYPE_CHANGE_EVENT) == 0)
        {
            handlePartTypeChangeEvent(event);
        }
        else if (proertyName.compareTo(NationalOilwell.MSG_DASH_INFO_MODIFY_EVENT) == 0)
        {
            handleDashInfoModifyEvent(event);
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT))
        {
            if (m_dashDialog != null && m_dashDialog.close())
            {
                processTableDialogCloseEvent();
            }
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.DASH_SELECTION_CHANGE_EVENT))
        {
            m_currentSelectedDash = (String) event.getNewValue();
            
            final String l_previousSelectedDash = (String) event.getOldValue();
            
            Display.getDefault().asyncExec(new Runnable()
            {
                @Override
                public void run()
                {
                    if(l_previousSelectedDash != null && !l_previousSelectedDash.isEmpty())
                    {
                        saveDashMap(l_previousSelectedDash); 
                        loadCurrentDash();
                    }
                }
            });
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.RSONE_COPY_ALL_PROPERTY))
        {
            copyAllProperties();
        }
        else if (event.getPropertyName().equalsIgnoreCase(NationalOilwell.RSONE_COPY_PROPERTY))
        {
            CopyPropertyHelper copyProperty = (CopyPropertyHelper) event.getNewValue();
            
            String[] chainPropertyName = copyProperty.getPropertyName();
            Object propertyValue = copyProperty.getPropValue();
            String displayName = copyProperty.getDisplayName();
            
            copyProperty(chainPropertyName, propertyValue, displayName);
        }
    }

    private void handleDashInfoModifyEvent(PublishEvent event)
    {
        if(m_currentSelectedDash == null)
        {
            m_currentSelectedDash = ((String[]) event.getNewValue())[0];
        }
        saveDashMap(m_currentSelectedDash);
        
        RSOneDashPropertyMap.setDashPropertyMap(m_dashPropertyMap);
        
        openDashTableDialog();
    }

    private void handlePartTypeChangeEvent(PublishEvent event)
    {
        String l_partType = (String) event.getNewValue();
        String[] l_arrDashNumbers = getCurrentDashNumbers();
        
        checkForBoType(l_partType, l_arrDashNumbers);
        
        if (l_partType.equalsIgnoreCase("purchased part") || l_partType.equalsIgnoreCase("Non-Engg Purchased Part"))
        {
            processRetainValuesPopUp(l_arrDashNumbers);
        }
        else
        {
            checkForMultipleDashPanel(l_arrDashNumbers);
        }
    }
    
    private void processTableDialogCloseEvent()
    {
        m_dashPropertyMap = RSOneDashPropertyMap.getDashPropertyMap();
        
        String l_partType = NewItemCreationHelper.getSelectedType();
        String[] l_arrDashNumbers = getCurrentDashNumbers();
        
        checkForBoType(l_partType, l_arrDashNumbers);
        
        RSOneDashPropertyMap.setDashPropertyMap(m_dashPropertyMap);
        
        if (l_arrDashNumbers.length > 1)
        {
            resetToMultiplePartsPanel();
        }
        
        publishPanelReloadEvent(l_arrDashNumbers);
        
        loadCurrentDash();
    }
    
    private void processRetainValuesPopUp(String[] dashNumbers)
    {
        int l_returnCode = getMessage(m_registry.getString("confirmation.msg"),
                m_registry.getString("retainValues.msg"), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
        
        if (l_returnCode == SWT.YES)
        {
            saveDashMap(m_currentSelectedDash);
            checkForMultipleDashPanel(dashNumbers);
            loadCurrentDash();
        }
        else if (l_returnCode == SWT.NO)
        {
            checkForMultipleDashPanel(dashNumbers);
            
            if (m_partInfoPanel instanceof ILoadSave)
            {
                IPropertyMap propMap = getPartPropMap();
                try
                {
                    ((ILoadSave) m_partInfoPanel).load(propMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void loadCurrentDash()
    {
        try
        {
            IPropertyMap propMap = m_dashPropertyMap.get(m_currentSelectedDash);
            
            if(propMap == null)
            {
                propMap = getPartPropMap();
                propMap.setType(NationalOilwell.PART_BO_TYPE);
                m_dashPropertyMap.put(m_currentSelectedDash, propMap);
            }
            
            if (m_partInfoPanel instanceof ILoadSave)
            {
                ((ILoadSave) m_partInfoPanel).load(propMap);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void checkForMultipleDashPanel(String[] l_arrDashNumbers)
    {
        if (l_arrDashNumbers.length > 1)
        {
            saveDashMap(m_currentSelectedDash);
            resetToMultiplePartsPanel();
            loadCurrentDash();
        }
    }
    
    private void checkForBoType(String partType, String[] dashNumbers)
    {
        if (partType.compareTo("Part") == 0 || partType.compareTo("Purchased Part") == 0)
        {
            setBOType(dashNumbers, NationalOilwell.PART_BO_TYPE);
        }
        else if (partType.compareTo("Non-Engineering") == 0 || partType.compareTo("Non-Engg Purchased Part") == 0)
        {
            setBOType(dashNumbers, NationalOilwell.NON_ENGINEERING);
        }
        
    }
    
    private void setBOType(String[] dashNumbers, String boType)
    {
        for (String theDashNumber : dashNumbers)
        {
            IPropertyMap propertyMap = m_dashPropertyMap.get(theDashNumber);
            propertyMap.setType(boType);
            m_dashPropertyMap.put(theDashNumber, propertyMap);
        }
    }
    
    private String[] getCurrentDashNumbers()
    {
        Set<String> l_sDashNumbers = m_dashPropertyMap.keySet();
        
        String[] l_arrDashNumbers = sortDashNumbers(l_sDashNumbers);
        
        return l_arrDashNumbers;
    }
    
    private void resetToMultiplePartsPanel()
    {
        m_partInfoPanel.dispose();
        
        String l_context = NewItemCreationHelper.getSelectedOperation() + NationalOilwell.DOTSEPARATOR + "Multiple"
                + NationalOilwell.DOTSEPARATOR + NewItemCreationHelper.getSelectedType() + NationalOilwell.DOTSEPARATOR
                + "PART_INFO_PANEL";
        
        ArrayList<IUIPanel> l_panels = createPanel(l_context);
        
        this.getComposite().layout();
        this.getComposite().getParent().layout();
        this.getComposite().getParent().getParent().layout();
        
        m_partInfoPanel = l_panels.get(0);
    }
    
    private void copyProperty(String[] chainPropertyName, Object propertyValue, String displayName)
    {
        Collection<IPropertyMap> mapList = m_dashPropertyMap.values();
        
        for (IPropertyMap propertyMap : mapList)
        {
            IPropertyMap propMap = null;
            try
            {
                propMap = TableUtils.getChainPropertyMap(propertyMap, chainPropertyName);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            PropertyMapHelper.addPropertyValueToMap(chainPropertyName[chainPropertyName.length - 1], propertyValue,
                    propMap);
        }
        
        if (!chainPropertyName[chainPropertyName.length - 1].equalsIgnoreCase(NationalOilwell.RSONE_SERIALIZE)
                && !chainPropertyName[chainPropertyName.length - 1].equalsIgnoreCase(NationalOilwell.RSONE_QAREQUIRED))
        {
            getMessage(m_registry.getString("information.msg"), displayName + " " + m_registry.getString("copy.msg")
                    + " " + displayName + " attribute.", SWT.ICON_INFORMATION | SWT.OK);
        }
    }
    
    private void copyAllProperties()
    {
        Collection<IPropertyMap> mapList = m_dashPropertyMap.values();
        
        for (IPropertyMap propertyMap : mapList)
        {
            try
            {
                if (m_partInfoPanel instanceof ILoadSave)
                {
                    ((ILoadSave) m_partInfoPanel).save(propertyMap);
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        getMessage(m_registry.getString("success.msg"), m_registry.getString("copyAllSuccess.msg"), SWT.ICON_WARNING
                | SWT.OK);
    }
    
    private String[] sortDashNumbers(Set<String> dashNumbers)
    {
        List<String> l_dashNoList = new ArrayList<String>();
        l_dashNoList.addAll(dashNumbers);
        
        Collections.sort(l_dashNoList);
        
        String[] l_arrDashNumbers = l_dashNoList.toArray(new String[l_dashNoList.size()]);
        
        return l_arrDashNumbers;
    }
    
    private void publishPanelReloadEvent(String[] dashNumbers)
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.MSG_RSONE_RELOAD_PANELS, dashNumbers,
                null);
        
        controller.publish(event);
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    private void saveDashMap(String dashNumber)
    {
        IPropertyMap propertyMap = m_dashPropertyMap.get(dashNumber);
        
        if(propertyMap == null)
        {
            propertyMap = getPartPropMap();
            propertyMap.setType(NationalOilwell.PART_BO_TYPE);
        }
        try
        {
            if (m_partInfoPanel instanceof ILoadSave)
            {
                ((ILoadSave) m_partInfoPanel).save(propertyMap);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        m_dashPropertyMap.put(dashNumber, propertyMap);
    }
    
    protected void openDashTableDialog()
    {
        m_dashDialog = new RSOneDashTableDialog(this.getComposite().getShell(), SWT.BORDER | SWT.NO_TRIM);
        m_dashDialog.open();
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        if (m_dashInfoPanel instanceof ILoadSave)
        {
            ((ILoadSave) m_dashInfoPanel).load(propMap);
        }
        if (m_partInfoPanel instanceof ILoadSave)
        {
            ((ILoadSave) m_partInfoPanel).load(propMap);
        }
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        IPropertyMap propertyMap = m_dashPropertyMap.get(m_currentSelectedDash);
        
        if(propertyMap == null)
        {
            propertyMap = getPartPropMap();
            m_dashPropertyMap.put(m_currentSelectedDash, propertyMap);
        }
        
        if (m_partInfoPanel instanceof ILoadSave)
        {
            ((ILoadSave) m_partInfoPanel).save(propertyMap);
        }
        
        m_dashPropertyMap.put(m_currentSelectedDash, propertyMap);
        
        return true;
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
        org.eclipse.swt.widgets.MessageBox dialog = new org.eclipse.swt.widgets.MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        
        m_dashInfoPanel.dispose();
        m_partInfoPanel.dispose();
        
        super.dispose();
    }
    
    protected void unRegisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.unregisterSubscriber(NationalOilwell.PART_TYPE_CHANGE_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.MSG_DASH_INFO_MODIFY_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.DASH_SELECTION_CHANGE_EVENT, this);
        theController.unregisterSubscriber(NationalOilwell.RSONE_COPY_ALL_PROPERTY, this);
        theController.unregisterSubscriber(NationalOilwell.RSONE_COPY_PROPERTY, this);
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    private Registry m_registry;
    
    private Composite m_mainComposite;
    
    protected Map<String, IPropertyMap> m_dashPropertyMap = new HashMap<String, IPropertyMap>();
    protected String m_currentSelectedDash = null;
    
    protected IUIPanel m_partInfoPanel;
    private IUIPanel m_dashInfoPanel;
    
    protected AbstractSWTDialog m_dashDialog;
}
