package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.RSOneCreatePartInfoPanel;
import com.nov.rac.item.panels.RSOneSimplePartInfoPanel;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCopyPartInfoPanel extends RSOneSimplePartInfoPanel
{

    public RSOneCopyPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    protected void createLeftPanel(Composite partInfoPanel)
    {
        l_leftPanel = new RSOneDashNumberListPanel(partInfoPanel, SWT.NONE);
        
        try
        {
            l_leftPanel.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
}
