package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.RSOneSimpleDocCategoryPanel;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreateDocumentDocCategoryPanel extends RSOneSimpleDocCategoryPanel
{
    
    public RSOneCreateDocumentDocCategoryPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnCode = super.createUI();
        UIHelper.makeMandatory(m_textDocCategory);
        
        return returnCode;
    }
}
