package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class Group45WLYTraceabilityPanel extends SimpleTraceabilityPanel
{
    public Group45WLYTraceabilityPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite traceabilityComposite = getComposite();
        
        GridLayout gd_rowComp = new GridLayout(NUM_OF_COLUMNS_FOR_TRACEABILITY_COMPOSITE, true);
        gd_rowComp.marginHeight = 0;
        gd_rowComp.marginWidth = 0;
        traceabilityComposite.setLayout(gd_rowComp);
        traceabilityComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_TRACEABILITY_COMPOSITE, 1));
        
        createTraceablityLabelFields(traceabilityComposite);
        
        Composite buttonsComposite = new Composite(traceabilityComposite, SWT.NONE);
        buttonsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
                HORIZONTAL_SPAN_FOR_RADIO_COMPOSITE, 1));
        buttonsComposite.setLayout(new GridLayout(2, false));
        
        createRadioButtonRows(buttonsComposite);
        
        return true;
    }
    
    @Override
    protected void createTraceablityLabelFields(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(1, false));
        
        GridData labelComp = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        labelComp.verticalIndent = TRACEABILITY_VERTICAL_INDENT;
        l_composite.setLayoutData(labelComp);
        
        super.createTraceablityLabelFields(l_composite);
    }
    
    private void createRadioButtonRows(Composite buttonsComposite)
    {
        String trueLabel = "True";
        String falseLabel = "False";
        String naLabel = "N/A";
        
        createLabels(buttonsComposite);
        
        Composite radioButtonsComp = new Composite(buttonsComposite, SWT.NONE);
        radioButtonsComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        radioButtonsComp.setLayout(new GridLayout(1, true));
        
        GridLayout radioGridLayout = new GridLayout(NUM_OF_COLUMNS_FOR_RADIO_COMPOSITE, false);
        radioGridLayout.marginHeight = 0;
        radioGridLayout.marginWidth = 0;
        
        Composite lotControlledComp = new Composite(radioButtonsComp, SWT.NONE);
        lotControlledComp.setLayout(radioGridLayout);
        m_lotControlledTrue = createRadioButton(lotControlledComp, trueLabel);
        m_lotControlledFalse = createRadioButton(lotControlledComp, falseLabel);
        m_lotControlledNA = createRadioButton(lotControlledComp, naLabel);
        m_lotControlledNA.setSelection(true);
        
        Composite serializedComp = new Composite(radioButtonsComp, SWT.NONE);
        serializedComp.setLayout(radioGridLayout);
        m_serializedTrue = createRadioButton(serializedComp, trueLabel);
        m_serializedFalse = createRadioButton(serializedComp, falseLabel);
        m_serializedNA = createRadioButton(serializedComp, naLabel);
        m_serializedNA.setSelection(true);
        
        Composite qaRequiredComp = new Composite(radioButtonsComp, SWT.NONE);
        qaRequiredComp.setLayout(radioGridLayout);
        m_qaRequiredTrue = createRadioButton(qaRequiredComp, trueLabel);
        m_qaRequiredFalse = createRadioButton(qaRequiredComp, falseLabel);
        m_qaRequiredNA = createRadioButton(qaRequiredComp, naLabel);
        m_qaRequiredNA.setSelection(true);
        
        addListeners();
    }
    
    protected void addListeners()
    {
        m_lotControlledTrue.addSelectionListener(m_selectionListener);
        m_lotControlledFalse.addSelectionListener(m_selectionListener);
        m_lotControlledNA.addSelectionListener(m_selectionListener);
        m_serializedTrue.addSelectionListener(m_selectionListener);
        m_serializedFalse.addSelectionListener(m_selectionListener);
        m_serializedNA.addSelectionListener(m_selectionListener);
        m_qaRequiredTrue.addSelectionListener(m_selectionListener);
        m_qaRequiredFalse.addSelectionListener(m_selectionListener);
        m_qaRequiredNA.addSelectionListener(m_selectionListener);
    }
    
    protected Button createRadioButton(Composite baseComp, String buttonLabel)
    {
        Button radioButton = new Button(baseComp, SWT.RADIO);
        radioButton.setText(buttonLabel);
        return radioButton;
    }
    
    protected void createLabels(Composite buttonsComposite)
    {
        Composite labelsComposite = new Composite(buttonsComposite, SWT.NONE);
        labelsComposite.setLayout(new GridLayout(1, true));
        labelsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        
        Label lotControlledLabel = new Label(labelsComposite, SWT.NONE);
        lotControlledLabel.setText("Lot Controlled");
        
        Label serializedLabel = new Label(labelsComposite, SWT.NONE);
        serializedLabel.setText("Serialized");
        
        Label qaRequiredLabel = new Label(labelsComposite, SWT.NONE);
        qaRequiredLabel.setText("QA Required");
    }
    
    @Override
    protected void enableDisableButtons()
    {
        boolean isLotControlledTrueSelected = m_lotControlledTrue.getSelection();
        boolean isSerializedTrueSelected = m_serializedTrue.getSelection();
        boolean isLotControlledFalseSelected = m_lotControlledFalse.getSelection();
        boolean isSerializedFalseSelected = m_serializedFalse.getSelection();
        boolean isLotControlledNASelected = m_lotControlledNA.getSelection();
        boolean isSerializedNASelected = m_serializedNA.getSelection();
        boolean isQARequiredNASelected = m_qaRequiredNA.getSelection();
        
        if (isLotControlledNASelected || isSerializedNASelected || isQARequiredNASelected)
        {
            m_labelFieldValidator.setValid(false);
            moveTraceabilityLabel();
        }
        else
        {
            m_labelFieldValidator.setValid(true);
            moveTraceabilityLabel();
        }
        if (isLotControlledTrueSelected)
        {
            m_serializedTrue.setSelection(false);
            m_serializedFalse.setSelection(true);
            m_serializedNA.setSelection(false);
            m_qaRequiredTrue.setSelection(true);
            m_qaRequiredFalse.setSelection(false);
            m_qaRequiredNA.setSelection(false);
            
            m_lotControlledTrue.setEnabled(true);
            m_lotControlledFalse.setEnabled(true);
            m_lotControlledNA.setEnabled(true);
            m_qaRequiredTrue.setEnabled(false);
            m_qaRequiredFalse.setEnabled(false);
            m_qaRequiredNA.setEnabled(false);
            m_serializedTrue.setEnabled(false);
            m_serializedFalse.setEnabled(false);
            m_serializedNA.setEnabled(false);
        }
        else if (isSerializedTrueSelected)
        {
            m_lotControlledTrue.setSelection(false);
            m_lotControlledFalse.setSelection(true);
            m_lotControlledNA.setSelection(false);
            m_qaRequiredTrue.setSelection(true);
            m_qaRequiredFalse.setSelection(false);
            m_qaRequiredNA.setSelection(false);
            
            m_serializedTrue.setEnabled(true);
            m_serializedFalse.setEnabled(true);
            m_serializedNA.setEnabled(true);
            m_lotControlledTrue.setEnabled(false);
            m_lotControlledFalse.setEnabled(false);
            m_lotControlledNA.setEnabled(false);
            m_qaRequiredTrue.setEnabled(false);
            m_qaRequiredFalse.setEnabled(false);
            m_qaRequiredNA.setEnabled(false);
        }
        else if (isLotControlledFalseSelected || isSerializedFalseSelected || isLotControlledNASelected
                || isSerializedNASelected)
        {
            m_lotControlledTrue.setEnabled(true);
            m_lotControlledFalse.setEnabled(true);
            m_lotControlledNA.setEnabled(true);
            m_serializedTrue.setEnabled(true);
            m_serializedFalse.setEnabled(true);
            m_serializedNA.setEnabled(true);
            m_qaRequiredTrue.setEnabled(true);
            m_qaRequiredFalse.setEnabled(true);
            m_qaRequiredNA.setEnabled(true);
        }
    }
    
    protected SelectionAdapter m_selectionListener = new SelectionAdapter()
    {
        @Override
        public void widgetSelected(SelectionEvent selectionevent)
        {
            enableDisableButtons();
        }
    };
    
    public boolean load(IPropertyMap masterPropertyMap) throws TCException
    {
        // if (masterPropertyMap.getLogicalPropNames().size() > 0)
        // {
        
        System.out.println("\n Load called \n");
        
        m_lotControlledNA.setSelection(false);
        m_serializedNA.setSelection(false);
        m_qaRequiredNA.setSelection(false);
        m_lotControlledTrue.setSelection(masterPropertyMap.getLogical(NationalOilwell.RSONE_LOTCONTROL));
        m_serializedTrue.setSelection(masterPropertyMap.getLogical(NationalOilwell.RSONE_SERIALIZE));
        m_qaRequiredTrue.setSelection(masterPropertyMap.getLogical(NationalOilwell.RSONE_QAREQUIRED));
        m_lotControlledFalse.setSelection(!masterPropertyMap.getLogical(NationalOilwell.RSONE_LOTCONTROL));
        m_serializedFalse.setSelection(!masterPropertyMap.getLogical(NationalOilwell.RSONE_SERIALIZE));
        m_qaRequiredFalse.setSelection(!masterPropertyMap.getLogical(NationalOilwell.RSONE_QAREQUIRED));
        
        enableDisableButtons();
        // }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setLogical("Traceability_45WLY_Check_NA", false);
        // Trial added if else
        if (m_lotControlledNA.getSelection() == true || m_serializedNA.getSelection() == true
                || m_qaRequiredNA.getSelection() == true)
        {
            //Commented for time being
//            propMap.setLogical(NationalOilwell.RSONE_LOTCONTROL, null);
//            propMap.setLogical(NationalOilwell.RSONE_SERIALIZE, null);
//            propMap.setLogical(NationalOilwell.RSONE_QAREQUIRED, null);
            
            //Trial
            propMap.setLogical("Traceability_45WLY_Check_NA", true);
        }
        else
        {
            propMap.setLogical(NationalOilwell.RSONE_LOTCONTROL, m_lotControlledTrue.getSelection());
            propMap.setLogical(NationalOilwell.RSONE_SERIALIZE, m_serializedTrue.getSelection());
            propMap.setLogical(NationalOilwell.RSONE_QAREQUIRED, m_qaRequiredTrue.getSelection());
        }
        enableDisableButtons();
        return true;
    }
    
    private final static int TRACEABILITY_VERTICAL_INDENT = 10;
    private final static int HORIZONTAL_SPAN_FOR_RADIO_COMPOSITE = 5;
    private final static int NUM_OF_COLUMNS_FOR_RADIO_COMPOSITE = 3;
    
    protected Button m_lotControlledTrue;
    protected Button m_lotControlledFalse;
    protected Button m_lotControlledNA;
    protected Button m_serializedTrue;
    protected Button m_serializedFalse;
    protected Button m_serializedNA;
    protected Button m_qaRequiredTrue;
    protected Button m_qaRequiredFalse;
    protected Button m_qaRequiredNA;
}
