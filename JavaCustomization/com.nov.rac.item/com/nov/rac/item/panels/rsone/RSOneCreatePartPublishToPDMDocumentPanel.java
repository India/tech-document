package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.item.panels.RSOneCreatePartDocCategoryPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreatePartPublishToPDMDocumentPanel extends
		RSOneCreatePartDocCategoryPanel 
{

	public RSOneCreatePartPublishToPDMDocumentPanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	@Override
    public boolean createUI() throws TCException
    {
        Composite l_docComposite = getComposite();
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_docComposite.setLayoutData(l_gdDocComposite);
        l_docComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_MAIN_COMPOSITE, true));
        
       
        createPublishToPDMCheckBoxRow(l_docComposite);
        
        createDocCategoryRow(l_docComposite);
        
        createDocTypeRow(l_docComposite);
        
        createDatasetTypeRow(l_docComposite);
        
        createFileTemplateRow(l_docComposite);
        
        createImportFileRow(l_docComposite);
        
        return true;
    }
	
	protected void createPublishToPDMCheckBoxRow(Composite l_composite) throws TCException
    {
        new Label(l_composite, SWT.NONE);
        
        Composite l_publishToPDMComposite = new Composite(l_composite, SWT.NONE);
        
        l_publishToPDMComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        l_publishToPDMComposite.setLayout(new GridLayout(7, true));
        
        Button l_publishToPDMCheckbox = new Button(l_publishToPDMComposite, SWT.CHECK);
        l_publishToPDMCheckbox.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_PUBLISH_TO_PDM_CHECKBOX, 1));
        
        l_publishToPDMCheckbox.setText(" " + m_registry.getString("PublishToPDM.Name"));
        
        addListener(l_publishToPDMCheckbox);
        
        new Label(l_publishToPDMComposite, SWT.NONE);
    }
	
	private void addListener(final Button checkBox) 
	{
		checkBox.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent event)
			{
				if(checkBox.getSelection())
				{
					m_isPublishToPDM = true;
				}
			}
		});
	}
	
   
    
	
private static final int HORIZONTAL_SPAN_FOR_PUBLISH_TO_PDM_CHECKBOX = 6;
private static final int NO_OF_COLUMNS_FOR_MAIN_COMPOSITE = 5;

private boolean m_isPublishToPDM = false;
	
}
