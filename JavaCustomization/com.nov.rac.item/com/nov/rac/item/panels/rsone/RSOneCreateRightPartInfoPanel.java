package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.panels.RSOnePartInfoRightPanel;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;

public class RSOneCreateRightPartInfoPanel extends RSOnePartInfoRightPanel {
    
    public RSOneCreateRightPartInfoPanel(Composite parent, int style) {
        
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException {
        
        boolean returnCode = super.createUI();
        
        makeFieldsMandatory();
        
        return returnCode;
    }
    
    private void makeFieldsMandatory() {
        
        UIHelper.makeMandatory(m_orgCombo);
        UIHelper.makeMandatory(m_nameCombo, false);
        UIHelper.makeMandatory(m_classText);
        UIHelper.makeMandatory(m_uomCombo);
        UIHelper.makeMandatory(m_traceabilityLabel, m_labelFieldValidator);
    }
    
}
