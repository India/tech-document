package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;

public class CreatePartDashInfoPanel extends SimpleDashInfoPanel implements ISubscriber
{
    
    public CreatePartDashInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        
        registerSubscriber();
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.registerSubscriber(NationalOilwell.MSG_RSONE_RELOAD_PANELS, this);
    }
    
    @Override
    protected void configureTable()
    {
        super.configureTable();
        
        m_dashNumberTableModel.addRow(new String[] { "001" });
        m_dashNumberTable.changeSelection(0, 0, false, false);
        
        //Tushar added
     //   m_dashNumberTable.getSelectionModel().addListSelectionListener(this);
    }
    
    @Override
    protected void createDashLabelRow(Composite parent)
    {
        Composite l_dashComposite = new Composite(parent, SWT.NONE);
        GridLayout l_gridLayout = new GridLayout(2, false);
        l_gridLayout.marginWidth = 0;
        l_gridLayout.marginHeight = 0;
        l_dashComposite.setLayout(l_gridLayout);
        l_dashComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        super.createDashLabelRow(l_dashComposite);
        
        m_modifyDashInfoBtn = new Button(l_dashComposite, SWT.NONE);
        m_modifyDashInfoBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
        m_addImage = new Image(l_dashComposite.getDisplay(), m_registry.getImage("add.Icon"), SWT.NONE);
        m_modifyDashInfoBtn.setImage(m_addImage);
        
        addListners();
    }
    
    private void addListners()
    {
        m_modifyDashInfoBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                publishDashInfoModifyEvent();
            }
        });
    }
    
    private void publishDashInfoModifyEvent()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.MSG_DASH_INFO_MODIFY_EVENT,
                getTableItems(), null);
        
        controller.publish(event);
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String proertyName = event.getPropertyName();
        
        if (proertyName.compareTo(NationalOilwell.MSG_RSONE_RELOAD_PANELS) == 0)
        {
            String[] l_dashNumbers = (String[]) event.getNewValue();
            
            resetTable(l_dashNumbers);
        }
    }
    
    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        
        super.dispose();
    }
    
    private void unRegisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.unregisterSubscriber(NationalOilwell.MSG_RSONE_RELOAD_PANELS, this);
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private Button m_modifyDashInfoBtn;
    
    private Image m_addImage;
}
