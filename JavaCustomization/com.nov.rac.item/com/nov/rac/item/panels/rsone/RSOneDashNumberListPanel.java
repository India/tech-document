package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class RSOneDashNumberListPanel extends RSOneDashNumberPanel
{

    public RSOneDashNumberListPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        boolean returnValue = super.createUI();
        
        m_modifyButton.setVisible(false);
        m_tableButton.setVisible(false);
        
        return returnValue;
    }
    
}
