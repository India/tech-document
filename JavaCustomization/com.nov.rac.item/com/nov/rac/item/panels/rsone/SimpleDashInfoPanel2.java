package com.nov.rac.item.panels.rsone;


import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumn;

import org.eclipse.swt.widgets.Composite;

public class SimpleDashInfoPanel2 extends SimpleDashInfoPanel
{
    
    public SimpleDashInfoPanel2(Composite parent, int style)
    {
        super(parent, style);
    }
    
    protected void configureTable()
    {
        m_dashNumberTableModel.addColumn(new String());
        m_dashNumberTableModel.addColumn(new String());
        hideTableColumn(m_dashNumberTable, 1);
        m_dashNumberTable.getTableHeader().setVisible(false);
        m_dashNumberTable.getSelectionModel().addListSelectionListener(this);
        
        TableColumn dashNumberCol = m_dashNumberTable.getColumnModel().getColumn(0);
        dashNumberCol.setCellRenderer(new DashNumberTableCellRenderer2());
    }
    
    private void hideTableColumn(JTable table, int columnNo)
    {
        table.getColumnModel().getColumn(columnNo).setMaxWidth(0);
        table.getColumnModel().getColumn(columnNo).setPreferredWidth(0);
        table.getColumnModel().getColumn(columnNo).setMinWidth(0);
        table.getColumnModel().getColumn(columnNo).setPreferredWidth(0);
        table.getColumnModel().getColumn(columnNo).setResizable(false);
    }
    
    
    @Override
    public void valueChanged(ListSelectionEvent listselectionevent)
    {
        if(listselectionevent.getValueIsAdjusting())
        {
            boolean isDashExist = (Boolean) m_dashNumberTable.getValueAt(m_dashNumberTable.getSelectedRow(), 1);
            if(!isDashExist)
            {
                m_dashNumberTable.changeSelection(getSelectionIndex(), 0, false, false);
            }
        }
        if (!listselectionevent.getValueIsAdjusting() && m_dashNumberTable.getSelectedRow() != -1)
        {
            String l_previousSelectedDash = getSelectedDash();
            String l_currentSelectedDash = (String) m_dashNumberTableModel.getValueAt(
                    m_dashNumberTable.getSelectedRow(), 0);
            
            setSelectedDash(l_currentSelectedDash);
            setSelectionIndex(m_dashNumberTable.getSelectedRow());
            
            publishDashSelectionChangeEvent(l_previousSelectedDash, l_currentSelectedDash);
        }
    }
    
    public int getSelectionIndex()
    {
        return m_selectedDashIndex;
    }
    
    public void setSelectionIndex(int selectedDashIndex)
    {
        this.m_selectedDashIndex = selectedDashIndex;
    }

    @Override
    protected void addRows(String[] dashNumbers)
    {
        int l_rowCount = dashNumbers.length;
        
        for (int rowIndex = 0; rowIndex < l_rowCount; rowIndex++)
        {
            if(m_existingDashNumbers != null && m_existingDashNumbers.contains(dashNumbers[rowIndex]))
            {
                m_dashNumberTableModel.addRow(new Object[]{dashNumbers[rowIndex], false});
            }
            else
            {
                m_dashNumberTableModel.addRow(new Object[]{dashNumbers[rowIndex], true});
            }
        }
    }
    
    @Override
    protected void setRowSelection(String[] dashNumbers)
    {
        if (!isRowSelected(dashNumbers))
        {
            for(int dashIndex = 0; dashNumbers != null && dashIndex < dashNumbers.length; dashIndex++)
            {
                if (!(m_existingDashNumbers.contains(dashNumbers[dashIndex])))
                {
                    m_dashNumberTable.changeSelection(dashIndex, 0, false, false);
                }
            }
            m_dashNumberTable.changeSelection(0, 0, false, false);
        }
    }
    
    @Override
    protected boolean isRowSelected(String[] dashNumbers)
    {
        boolean isSelected = false;
        
        for (int dashIndex = 0; dashNumbers != null && dashIndex < dashNumbers.length; dashIndex++)
        {
            if (dashNumbers[dashIndex].compareTo(getSelectedDash()) == 0)
            {
                m_dashNumberTable.changeSelection(dashIndex, 0, false, false);
                isSelected = true;
            }
        }
        return isSelected;
    }
    
    protected void setMissingDashRowSelection(String[] dashNumbers, String missingDash)
    {
        for (int dashIndex = 0; dashNumbers != null && dashIndex < dashNumbers.length; dashIndex++)
        {
            if (dashNumbers[dashIndex].compareTo(missingDash) == 0)
            {
                m_dashNumberTable.changeSelection(dashIndex, 0, false, false);
            }
        }
    }
    
    protected void setExistingDashVector(String[] dashNumberList, String missingDash)
    {
        for(String theDashNumber : dashNumberList)
        {
            if(!(theDashNumber.compareTo(missingDash) == 0))
            {
                m_existingDashNumbers.add(theDashNumber);
            }
        }
    }
    
    protected Vector<String> m_existingDashNumbers = new Vector<String>();
    private int m_selectedDashIndex = 0;;
    protected String[] m_allDashNumbers;
}
