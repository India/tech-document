package com.nov.rac.item.panels.rsone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.RSOneClassificationDialog;
import com.nov.rac.item.helpers.ClassHierarchyCreator;
import com.nov.rac.item.helpers.ClassificationAutoComplete;
import com.nov.rac.item.helpers.ClassificationHelper;
import com.nov.rac.item.helpers.MandatoryLabelFieldValidator;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.item.helpers.RSOneUOMLOVHelper;
import com.nov.rac.item.panels.factory.PanelFactory;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class SimplePartInfoPanel extends AbstractUIPanel implements ILoadSave, IPublisher, ISubscriber
{
    
    public SimplePartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        
        m_registry = getRegistry();
        m_session = getSession();
        
        registerSubscriber();
    }
    
    protected GridLayout getLayout(int numofColumns, boolean equalWidth)
    {
        GridLayout l_layout = new GridLayout(numofColumns, equalWidth);
        l_layout.marginHeight = 0;
        l_layout.marginWidth = 0;
        
        return l_layout;
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
    }
    
    private TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.registerSubscriber(NationalOilwell.MSG_RSONE_RELOAD_PANELS, this);
        theController.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL, this);
        theController.registerSubscriber(NationalOilwell.SELECTEDCLASS, this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_partInfoComposite = getComposite();
        l_partInfoComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_PART_INFO_COMP, true));
        l_partInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_PART_INFO_COMP, 1));
        
        createOrgRow(l_partInfoComposite);
        
        createNameRow(l_partInfoComposite);
        
        createClassRow(l_partInfoComposite);
        
        createSendToClassificationRow(l_partInfoComposite);
        
        createUOMRow(l_partInfoComposite);
        
        createDescRow(l_partInfoComposite);
        
        createTraceabilityRow(l_partInfoComposite);
        
        return true;
    }
    
    protected void createOrgRow(Composite parent) throws TCException
    {
        createOrgLabel(parent);
        
        createOrgFields(parent);
    }
    
    protected void createOrgLabel(Composite parent)
    {
        Label l_orgLabel = new Label(parent, SWT.NONE);
        l_orgLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_orgLabel.setText(m_registry.getString("PART_INFO_PANEL_ORG_LABEL"));
    }
    
    protected void createOrgFields(Composite parent)
    {
        m_orgCombo = new Combo(parent, SWT.NONE);
        m_orgCombo.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, HORIZONTAL_SPAN_FOR_ORG_COMBO, 1));
        m_orgCombo.addModifyListener(m_modifyListener);
    }
    
    protected void createNameRow(Composite parent)
    {
        createNameLabel(parent);
        
        createNameFields(parent);
    }
    
    protected void createNameLabel(Composite parent)
    {
        Label l_nameLabel = new Label(parent, SWT.NONE);
        l_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_nameLabel.setText(m_registry.getString("PART_INFO_PANEL_NAME_LABEL"));
    }
    
    protected void createNameFields(Composite parent)
    {
        m_nameCombo = new Combo(parent, SWT.NONE);
        m_nameCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, HORIZONTAL_SPAN_FOR_NAME_COMBO, 1));
        m_nameCombo.addModifyListener(m_modifyListener);
    }
    
    private void setNameComboTextLimit()
    {
        String groupName = m_session.getGroup().toString();
        
        int length = PreferenceHelper.getParsedPrefValueForGroup("NOV_ObjectName_Length_For_Groups",
                TCPreferenceService.TC_preference_site, groupName, "|");
        // Dont use it PreferenceHelper..try to get value here only
        
        m_nameCombo.setTextLimit(length);
    }
    
    private void loadNameCombo()
    {
        String groupName = m_session.getGroup().toString();
        
        String[] suggestedValues = RSOneSuggestedByNameLOVHelper.getSuggestedNameValues(m_session,
                "_SuggestedNamesByGroupQuery_", new String[] { m_registry.getString("Group.NAME") },
                new String[] { groupName }, m_registry.getString("Name.NAME"));
        
        m_nameCombo.setItems(suggestedValues);
        
        new AutoCompleteField(m_nameCombo, new ComboContentAdapter(), suggestedValues);
    }
    
    protected void createClassRow(Composite parent)
    {
        createClassLabel(parent);
        
        createClassFields(parent);
    }
    
    protected void createClassLabel(Composite parent)
    {
        Label l_classLabel = new Label(parent, SWT.NONE);
        l_classLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_classLabel.setText(m_registry.getString("PART_INFO_PANEL_CLASSIFICATION_LABEL"));
    }
    
    protected void createClassFields(Composite parent)
    {
        Composite l_classFieldsComposite = new Composite(parent, SWT.NONE);
        GridLayout l_classFieldsLayout = new GridLayout(NO_OF_COLUMNS_FOR_CLASS_FIELDS_COMP, true);
        l_classFieldsLayout.marginHeight = 0;
        l_classFieldsLayout.marginWidth = 0;
        l_classFieldsComposite.setLayout(l_classFieldsLayout);
        l_classFieldsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_CLASS_FIELDS_COMP, 1));
        
        m_classText = new Text(l_classFieldsComposite, SWT.BORDER);
        m_classText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, HORIZONTAL_SPAN_FOR_CLASS_TEXT, 1));
        addListenerToClassText();
        
        m_classHierarchyButton = new Button(l_classFieldsComposite, SWT.NONE);
        GridData l_gdClassButton = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_gdClassButton.horizontalIndent = CLASS_BUTTON_HORIZONTAL_INDENT;
        m_classHierarchyButton.setLayoutData(l_gdClassButton);
        
        addListenerToClassButton();
    }
    
    private void addListenerToClassText()
    {
        m_classText.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                String value = m_classText.getText();
                if (value.contains("->"))
                {
                    value = value.substring(value.lastIndexOf(">") + 1);
                    m_classText.setText(value);
                }
            }
        });
    }
    
    private void addListenerToClassButton()
    {
        m_classHierarchyButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                RSOneClassificationDialog classificationDialog = new RSOneClassificationDialog(m_classHierarchyButton
                        .getShell(), m_classText);
                Point btnLocation = m_classHierarchyButton.toDisplay(m_classHierarchyButton.getLocation());
                Point btnSize = m_classHierarchyButton.getSize();
                classificationDialog.setLocation(btnLocation, btnSize);
                classificationDialog.open();
            }
        });
    }
    
    protected void createSendToClassificationRow(Composite parent)
    {
        // since there is no label left to send to classification button, empty
        // label created
        new Label(parent, SWT.NONE);
        
        m_sendToClassCheckkbox = new Button(parent, SWT.CHECK);
        m_sendToClassCheckkbox.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_SEND_TO_CLASS_BUTTON, 1));
        m_sendToClassCheckkbox.setText(m_registry.getString("PART_INFO_PANEL_SEND_TO_CLASS_LABEL"));
        
        m_sendToClassCheckkbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                IController controller = ControllerFactory.getInstance().getDefaultController();
                PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.SEND_TO_CLASSIFICATION,
                        m_sendToClassCheckkbox.getSelection(), null);
                controller.publish(event);
            }
        });
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    protected void createUOMRow(Composite parent)
    {
        createUOMLabel(parent);
        
        createUOMFields(parent);
    }
    
    protected void createUOMLabel(Composite parent)
    {
        Label l_uomLabel = new Label(parent, SWT.NONE);
        l_uomLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_uomLabel.setText(m_registry.getString("PART_INFO_PANEL_UOM_LABEL"));
    }
    
    protected void createUOMFields(Composite parent)
    {
        m_uomCombo = new Combo(parent, SWT.NONE);
        m_uomCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
        
        m_uomCombo.setText("");
        m_uomCombo.addModifyListener(m_modifyListener);
        Label emptyLabel = new Label(parent, SWT.NONE);
        GridData l_dgLabel = new GridData(SWT.FILL, SWT.FILL, true, false, HORIZONTAL_SPAN_UOM_EMPTY_LABEL, 1);
        l_dgLabel.horizontalIndent = UOM_EMPTY_LABEL_HORIZONTAL_INDENT;
        emptyLabel.setLayoutData(l_dgLabel);
    }
    
    protected void createDescRow(Composite parent)
    {
        createDescLabel(parent);
        
        createDescFields(parent);
    }
    
    protected void createDescLabel(Composite parent)
    {
        Label l_descLabel = new Label(parent, SWT.NONE);
        l_descLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true, 1, 1));
        l_descLabel.setText(m_registry.getString("PART_INFO_PANEL_DESCRIPTION_LABEL"));
    }
    
    protected void createDescFields(Composite parent)
    {
        m_descText = new Text(parent, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        m_descText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_DESC_TEXT,
                VERTICAL_SPAN_DESC_TEXT));
        
        // To make effect of description text vertical span three empty labels
        // created
        new Label(parent, SWT.NONE);
        new Label(parent, SWT.NONE);
        new Label(parent, SWT.NONE);
    }
    
    protected void createTraceabilityRow(Composite parent)
    {
        ArrayList<IUIPanel> l_panels = createPanel("TRACEABILITY_PANEL", parent);
        
        // ArrayList<IUIPanel> will contain just one panel
        m_traceabilityPanel = l_panels.get(0);
    }
    
    protected ArrayList<IUIPanel> createPanel(String context, Composite parent)
    {
        String[] l_panelNames = NewItemCreationHelper.getConfiguration(context, m_registry);
        
        PanelFactory l_panelFactory = PanelFactory.getInstance();
        
        ArrayList<IUIPanel> panels = l_panelFactory.createPanels(parent, l_panelNames, false);
        
        return panels;
    }
    
    @Override
    public boolean createUIPost() throws TCException
    {
        ClassHierarchyCreator classhierarchycreator = new ClassHierarchyCreator();
        String rootClassID = "MDI";
        
        try
        {
            classhierarchycreator.createStructure(rootClassID);
        }
        catch (Exception e)
        {
            throw new TCException(e);
        }
        
        String[] cp_array = (String[]) classhierarchycreator.getLinearStructure().toArray(new String[0]);
        String[] classID_array = (String[]) classhierarchycreator.getClassID().toArray(new String[0]);
        String[] classpuid_array = (String[]) classhierarchycreator.getClassPuid().toArray(new String[0]);
        Map<String, String> classiMap = new HashMap<String, String>();
        Map<String, String> classUidMap = new HashMap<String, String>();
        new ClassificationAutoComplete(m_classText, new TextContentAdapter(), cp_array);
        String[] classificationArray = new String[cp_array.length];
        for (int inx = 0; inx < cp_array.length; inx++)
        {
            if (cp_array[inx].contains("->"))
            {
                classificationArray[inx] = cp_array[inx].substring(cp_array[inx].lastIndexOf(">") + 1);
            }
            
            classiMap.put(classificationArray[inx], classID_array[inx]);
            classUidMap.put(classificationArray[inx], classpuid_array[inx]);
            
        }
        ClassificationHelper.setClassificationMap(classiMap);
        ClassificationHelper.setClassificationUIDMap(classUidMap);
        
        makeFieldsMandatory();
        
        loadCombos();
        
        return true;
    }
    
    private void loadCombos()
    {
        UIHelper.setAutoComboArray(m_orgCombo, LOVUtils.getStringArrayforLOV(NationalOilwell.RSOne_ORG_LOV));
        UIHelper.setAutoComboArray(m_uomCombo, (Vector<String>) RSOneUOMLOVHelper.getUOMLOVValues());
        
        setNameComboTextLimit();
        loadNameCombo();
        
        UIHelper.limitCharCountInTextWithPopUpMsg(m_descText, NationalOilwell.RSONE_DESCRIPTION_TEXT_LIMIT,
                m_registry.getString("ExceedingDescTextLimit.msg"));
        
        UIHelper.enableSelectAll(m_descText);
    }
    
    private void makeFieldsMandatory()
    {
        UIHelper.makeMandatory(m_orgCombo);
        UIHelper.makeMandatory(m_nameCombo, false);
        UIHelper.makeMandatory(m_classText);
        UIHelper.makeMandatory(m_uomCombo);
        UIHelper.makeMandatory(m_traceabilityLabel, m_labelFieldValidator);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap l_masterPropertyMap = propMap.getCompound("IMAN_master_form");
        
        m_classText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ICS_subclass_name)));
        m_descText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_DESC)));
        
        m_nameCombo.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_NAME)));
        m_orgCombo.setText(PropertyMapHelper.handleNullForArray(l_masterPropertyMap
                .getStringArray(NationalOilwell.RSONE_ORG))[0]);
        m_uomCombo.setText(PropertyMapHelper.handleNull(l_masterPropertyMap.getString(NationalOilwell.RSONE_UOM)));
        
        if (m_traceabilityPanel instanceof ILoadSave)
        {
            ((ILoadSave) m_traceabilityPanel).load(l_masterPropertyMap);
        }

        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        setItemProperties(propMap);
        setMasterFormProperties(propMap);
        
        return true;
    }
    
    private void setItemProperties(IPropertyMap propMap) throws TCException
    {
        propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.OBJECT_NAME), m_nameCombo.getText().trim());
        propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.OBJECT_DESC), m_descText.getText().trim());
        propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.ICS_subclass_name), m_classText.getText().trim());
        
        if (!"".equals(m_classText.getText().trim()))
        {
            propMap.setString(PropertyMapHelper.handleNull(NationalOilwell.ICS_classified), "Y");
        }
    }
    
    private void setMasterFormProperties(IPropertyMap propMap)
    {
        IPropertyMap l_masterPropertyMap = propMap.getCompound("IMAN_master_form");
        
        if (l_masterPropertyMap != null)
        {
            try
            {
                if (m_traceabilityPanel instanceof ILoadSave)
                {
                    ((ILoadSave) m_traceabilityPanel).save(l_masterPropertyMap);
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
            l_masterPropertyMap.setStringArray(NationalOilwell.RSONE_ORG, new String[] { m_orgCombo.getText().trim() });
            l_masterPropertyMap.setString(NationalOilwell.RSONE_UOM, m_uomCombo.getText());
            
            propMap.setCompound("IMAN_master_form", l_masterPropertyMap);
        }
    }

    @Override
    public void update(PublishEvent event)
    {
        String proertyName = event.getPropertyName();
        
        if (proertyName.compareTo(NationalOilwell.MSG_RSONE_RELOAD_PANELS) == 0)
        {
            String[] l_dashNumbers = (String[]) event.getNewValue();
            
            if (l_dashNumbers.length > 1)
            {
                m_sendToClassCheckkbox.setEnabled(false);
            }
            else
            {
                m_sendToClassCheckkbox.setEnabled(true);
            }
        }
            
        final PublishEvent event1 = event;
        if(event.getPropertyName().equals(NationalOilwell.SELECTEDCLASS))
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                
                @Override
                public void run()
                {
                    m_classText.setText(event1.getNewValue().toString());
                }
            });
          
        }
            
        if(event.getPropertyName().equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL))
        {
            m_nameCombo.setEnabled(false);
        // Tushar will handle this in his traceability panel
            
            /*m_qaRequiredButton.setSelection(true);
            m_serializedButton.setSelection(false);
            m_lotControlledButton.setSelection(false);
            enableDisableButtons();*/
        }
    }
    
    private void checkMandatoryFields()
    {   
        boolean isOrgEmpty = m_orgCombo.getText().isEmpty();
        boolean isNameEmpty = m_nameCombo.getText().isEmpty();
        boolean isClassEmpty = m_classText.getText().isEmpty();
        boolean isUOMEmpty = m_uomCombo.getText().isEmpty();
        
        if(!isOrgEmpty && !isNameEmpty && !isUOMEmpty)
        {
            isMandatory = false;
            publishMakeDashOptionalEvent(isMandatory);
        }
        else if(!isMandatory)
        {
            isMandatory = true;
            publishMakeDashOptionalEvent(isMandatory);
        }
    }
    
    private void publishMakeDashOptionalEvent(boolean isMandatory)
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.MSG_DASH_NUMBER_MANDATORY,
                isMandatory, null);
        
        controller.publish(event);
    }

    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        
        super.dispose();
    }
    
    private void unRegisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.unregisterSubscriber(NationalOilwell.MSG_RSONE_RELOAD_PANELS, this);
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL, this);
        theController.unregisterSubscriber(NationalOilwell.SELECTEDCLASS, this);
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private ModifyListener m_modifyListener = new ModifyListener()
    {
        @Override
        public void modifyText(ModifyEvent arg0)
        {
           // checkMandatoryFields();
        }
    };
    
    private static boolean isMandatory = true;
    protected Registry m_registry;
    protected TCSession m_session;
    protected Label m_traceabilityLabel;
    protected Combo m_orgCombo;
    protected Combo m_nameCombo;
    protected Combo m_uomCombo;
    
    protected Text m_classText;
    protected Text m_descText;
    
    private Button m_sendToClassCheckkbox;
    protected Button m_lotControlledButton;
    protected Button m_serializedButton;
    protected Button m_qaRequiredButton;
    protected Button m_classHierarchyButton;
    
    protected MandatoryLabelFieldValidator m_labelFieldValidator = new MandatoryLabelFieldValidator();
    
    protected final static int NO_OF_COLUMNS_FOR_PART_INFO_COMP = 6;
    protected final static int NO_OF_COLUMNS_FOR_TRACE_FIELDS_COMP = 3;
    protected final static int NO_OF_COLUMNS_FOR_CLASS_FIELDS_COMP = 15;
    
    protected final static int HORIZONTAL_SPAN_FOR_PART_INFO_COMP = 7;
    protected final static int HORIZONTAL_SPAN_FOR_TRACE_FIELDS_COMP = 5;
    protected final static int HORIZONTAL_SPAN_FOR_CLASS_FIELDS_COMP = 5;
    protected final static int HORIZONTAL_SPAN_FOR_ORG_COMBO = 5;
    protected final static int HORIZONTAL_SPAN_FOR_DESC_TEXT = 5;
    protected final static int HORIZONTAL_SPAN_FOR_CLASS_TEXT = 14;
    protected final static int HORIZONTAL_SPAN_FOR_NAME_COMBO = 5;
    protected final static int HORIZONTAL_SPAN_FOR_SEND_TO_CLASS_BUTTON = 5;
    protected final static int HORIZONTAL_SPAN_UOM_EMPTY_LABEL = 3;
    
    protected final static int VERTICAL_SPAN_DESC_TEXT = 4;
    
    private static final int CLASS_BUTTON_HORIZONTAL_INDENT = 6;
    private static final int UOM_EMPTY_LABEL_HORIZONTAL_INDENT = 6;
    
    protected IUIPanel m_traceabilityPanel;
}
