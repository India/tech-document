package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdPartInfoPanel extends MainPartInfoPanel
{

    public AssignOracleIdPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    protected void registerSubscriber()
    {
        super.registerSubscriber();
        
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, this);
        theController.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL, this);
        
    }
    
    protected void unRegisterSubscriber()
    {
        super.unRegisterSubscriber();
        
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, this);
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL, this);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        
        publishEvents();
        
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL, null, null);
        
        controller.publish(event);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        super.update(event);
        
        String propertyName = event.getPropertyName();
        if (propertyName.equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL))
        {
            // this is to control load when searched object is loaded in panel
            m_newDashNumber = event.getNewValue().toString();
        }
        
        if (propertyName.equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL))
        {
            // this is to control load when searched object is loaded in panel
            m_selectedBase = event.getNewValue().toString();
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        
        String itemId = null;
        
        if (m_selectedBase == null || m_selectedBase.equals(""))
        {
            itemId = NOVRSOneItemIDHelper.getOracleId() + "-" + m_newDashNumber;
        }
        else
        {
            itemId = m_selectedBase + "-" + m_newDashNumber;
        }
        
        for(String dashNumber : m_dashPropertyMap.keySet())
        {
            m_dashPropertyMap.get(dashNumber).setString("item_id", itemId);
        }
        
        propMap.addAll(m_dashPropertyMap.get(m_currentSelectedDash));
        
        return true;
    }
    
    private String m_newDashNumber = null;
    private String m_selectedBase = null;
    
}
