package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
//import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NOVRSOneItemIDHelper;
import com.nov.rac.item.panels.RSOneCreatePartInfoPanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public class AssignOracleIdToLegacyPartInfoPanel extends RSOneCreatePartInfoPanel implements IPublisher, ISubscriber
{
    // private boolean m_SearchResultsAvailable = false;
    // private IPropertyMap m_archivedPropertyMap = null;
    
    private String m_newDashNumber = null;
    private String m_selectedBase = null;
    
    public AssignOracleIdToLegacyPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
        registerSubscriber();
    }
    
    @Override
    protected void createLeftPanel(Composite partInfoPanel)
    {
        l_leftPanel = new AssignOracleIdToLegacyPartDashNumberPanel(partInfoPanel, SWT.NONE);
        
        try
        {
            l_leftPanel.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        super.load(propMap);
        l_leftPanel.load(propMap);
        
        publishEvents();
        return true;
    }
    
    private void publishEvents()
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        
        final PublishEvent event = new PublishEvent(getPublisher(),
                NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL, null, null);
        
        controller.publish(event);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        super.save(propMap);
        String itemId = null;
        
        if (m_selectedBase == null || m_selectedBase.equals(""))
        {
            itemId = NOVRSOneItemIDHelper.getOracleId() + "-" + m_newDashNumber;
        }
        else
        {
            itemId = m_selectedBase + "-" + m_newDashNumber;
        }
        
        IPropertyMap[] dashPropertyMap = propMap.getCompoundArray("dashPropertyMap");
        propMap.addAll(dashPropertyMap[0]);
        
        propMap.setString(NationalOilwell.ITEM_ID, itemId);
        
        return true;
    }
    
    IPublisher getPublisher()
    {
        return this;
    }
    
    protected void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, this);
        theController.registerSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL, this);
        
    }
    
    private void unRegisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL, this);
        theController.unregisterSubscriber(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL, this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        // TODO Auto-generated method stub
        String propertyName = event.getPropertyName();
        if (propertyName.equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL))
        {
            // this is to control load when searched object is loaded in panel
            m_newDashNumber = event.getNewValue().toString();
        }
        
        if (propertyName.equals(NationalOilwell.EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL))
        {
            // this is to control load when searched object is loaded in panel
            m_selectedBase = event.getNewValue().toString();
        }
    }
    
    @Override
    public void dispose()
    {
        // TODO Auto-generated method stub
        unRegisterSubscriber();
        super.dispose();
    }
}
