package com.nov.rac.item.panels.rsone;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class MultiplePurchasePartInfoPanel extends SimplePurchasePartInfoPanel
{
    
    public MultiplePurchasePartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    private Composite getComposite(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(2, false));
        l_composite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        return l_composite;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_partInfoComposite = getComposite();
        l_partInfoComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_PART_INFO_COMP, true));
        l_partInfoComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 7, 1));
        
        createCopyAllBtnRow(l_partInfoComposite);
        
        createOrgRow(l_partInfoComposite);
        
        createNameRow(l_partInfoComposite);
        
        createClassRow(l_partInfoComposite);
        
        createSendToClassificationRow(l_partInfoComposite);
        
        createUOMRow(l_partInfoComposite);
        
        createDescRow(l_partInfoComposite);
        
        createMfgRow(l_partInfoComposite);
        
        createTraceabilityRow(l_partInfoComposite);
        
        return true;
    }
    
    protected void createCopyAllBtnRow(Composite parent) throws TCException
    {
        new Label(parent, SWT.NONE);
        
        Button l_copyAllButton = new Button(parent, SWT.NONE);
        l_copyAllButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false,
                HORIZONTAL_SPAN_FOR_COPY_ALL_BUTTON, 1));
        l_copyAllButton.setText(m_registry.getString("PART_INFO_PANEL_COPY_ALL_BUTTON_TEXT"));
    }
    
    @Override
    protected void createOrgLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        Button l_copyAllOrgCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllOrgCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        super.createOrgLabel(l_composite);
    }
    
    @Override
    protected void createNameLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        Button l_copyAllNameCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllNameCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        super.createNameLabel(l_composite);
    }
    
    @Override
    protected void createClassLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        Button l_copyAllClassCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllClassCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        super.createClassLabel(l_composite);
    }
    
    @Override
    protected void createUOMLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        Button l_copyAllUOMCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllUOMCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        super.createUOMLabel(l_composite);
    }
    
    @Override
    protected void createDescLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        Button l_copyAllDescCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllDescCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
        
        super.createDescLabel(l_composite);
    }
    
    @Override
    protected void createManifactureLabel(Composite parent)
    {
        Composite l_composite = getComposite(parent);
        
        Button l_copyAllMfgNameCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllMfgNameCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        super.createManifactureLabel(l_composite);
    }
    
    @Override
    protected void createMfgIdLabel(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(NO_OF_COLUMNS_FOR_MFG_ID_LABEL_COMP, false));
        l_composite.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, HORIZONTAL_SPAN_FOR_MFG_ID_LABEL_COMP,
                1));
        
        Button l_copyAllMfgIdCheckbox = new Button(l_composite, SWT.CHECK);
        l_copyAllMfgIdCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        super.createMfgIdLabel(l_composite);
    }
    
    //Tushar commented
//    @Override
//    protected void createTraceablityLabelFields(Composite parent)
//    {
//        Composite l_composite = getComposite(parent);
//        
//        Button l_copyAllTraceCheckbox = new Button(l_composite, SWT.CHECK);
//        l_copyAllTraceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//        
//        super.createTraceablityLabelFields(l_composite);
//    }
    
    //Tushar added
    @Override
    protected void createTraceabilityRow(Composite parent)
    {
        String l_context = "MULTIPLE_TRACEABILITY_PANEL";
        
        ArrayList<IUIPanel> l_panels = createPanel(l_context, /*Added a parameter*/ parent);
        
        m_traceabilityPanel = l_panels.get(0);
    }
    //Tushar added
    
    private final static int NO_OF_COLUMNS_FOR_MFG_ID_LABEL_COMP = 4;
    
    private final static int HORIZONTAL_SPAN_FOR_MFG_ID_LABEL_COMP = 3;
    private final static int HORIZONTAL_SPAN_FOR_COPY_ALL_BUTTON = 5;
}
