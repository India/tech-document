package com.nov.rac.item.panels.rsone;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneSuggestedByNameLOVHelper;
import com.nov.rac.item.helpers.SearchButtonHelper;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class RSOneSimpleCopyDocInfoPanel extends AbstractUIPanel implements ILoadSave, IPublisher
{
    
    public RSOneSimpleCopyDocInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        m_session = getSession();
    }
    
    protected TCSession getSession() 
    {
		return (TCSession) AIFUtility.getCurrentApplication().getSession();
	}
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.rsone.rsone");
    }
    
    protected GridLayout getLayout()
    {
    	GridLayout gridLayout = new GridLayout(NO_OF_COLUMNS_FOR_SUB_COMPOSITE, true);
    	gridLayout.marginWidth = 0;
        
        return gridLayout;
    }
    
    protected GridData getLayoutData()
    {
    	return new GridData(SWT.FILL, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_SUB_COMPOSITE, 1);
    }
    
    /**
     * @wbp.parser.entryPoint
     */
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite l_composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_MAIN_COMPOSITE, true));
        
        createNameField(l_composite);
        
        createSelectedDocId(l_composite);
        
        UIHelper.makeMandatory(m_comboName, false);
        
        return true;
    }
    
    private void createNameField(Composite infoPanel)
    {
        
        m_nameLabel = new Label(infoPanel, SWT.NONE);
        m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_nameLabel.setText(m_registry.getString("RSOneDocumentSimpleInfoPanel.Name"));
        
        Composite l_nameComposite = new Composite(infoPanel, SWT.NONE);
        l_nameComposite.setLayoutData(getLayoutData());
        
        l_nameComposite.setLayout(getLayout());
        
        m_comboName = new Combo(l_nameComposite, SWT.BORDER);
        m_comboName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, HORIZONTAL_SPAN_FOR_NAME_FIELD, 1));
        
        //m_comboName.setTextLimit(NationalOilwell.NAME_TEXT_LIMIT);
        
        String groupName = m_session.getGroup().toString();
		String[] suggestedValues = RSOneSuggestedByNameLOVHelper
				.getSuggestedNameValues(m_session,
						"_SuggestedNamesByGroupQuery_",
						new String[] { m_registry.getString("Group.NAME") },
						new String[] { groupName },
						m_registry.getString("Name.NAME"));
		m_comboName.setItems(suggestedValues);
		new AutoCompleteField(m_comboName, new ComboContentAdapter(),
				suggestedValues);

		int length = PreferenceHelper.getParsedPrefValueForGroup(
				"NOV_ObjectName_Length_For_Groups",
				TCPreferenceService.TC_preference_site, groupName, "|");
		m_comboName.setTextLimit(length);
        
    }
    
    private void createSelectedDocId(Composite infoPanel)
    {
        m_docIdLabel = new Label(infoPanel, SWT.NONE);
        m_docIdLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_docIdLabel.setText(m_registry.getString("RSOneDocumentSimpleInfoPanel.DocumentId"));
        
        Composite l_selectedDocIdComposite = new Composite(infoPanel, SWT.NONE);
        l_selectedDocIdComposite.setLayoutData(getLayoutData());
        
        l_selectedDocIdComposite.setLayout(getLayout());
        
        m_docIdText = new Text(l_selectedDocIdComposite, SWT.BORDER);
        m_docIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_FOR_DOCUMENT_ID_TEXT_FIELD, 1));
        
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
        /*SearchButtonComponent searchButton = new SearchButtonComponent(l_selectedDocIdComposite, SWT.TOGGLE);
        GridData searchBtnGridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
        searchBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,
                NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON);
        searchButton.setLayoutData(searchBtnGridData);
        searchButton.setSearchItemType(new String[] { "Documents" });*/
        
        String [] searchItemTypes = {"Documents"};
        SearchButtonComponent searchButton = SearchButtonHelper.getSerachButton(l_selectedDocIdComposite, searchBtnGridData, searchItemTypes);
        
        new Label(l_selectedDocIdComposite, SWT.NONE).setVisible(false);
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        m_comboName.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.OBJECT_NAME)));
        m_docIdText.setText(PropertyMapHelper.handleNull(propMap.getString(NationalOilwell.ITEM_ID)));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        setItemProperties(propMap);
        return true;
    }
    
    private void setItemProperties(IPropertyMap propMap)
    {
        propMap.setString("object_name", m_comboName.getText().trim());
        propMap.setString("item_id", m_docIdText.getText().trim());
    }
    
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private Registry m_registry = null;
    
    private TCSession m_session;
    
    /** The m_name label. */
    private Label m_nameLabel;
    
    /** The m_description label. */
    private Label m_docIdLabel;
    
    /** The m_comboName ComboBox. */
    protected Combo m_comboName;
    
    /** The m_description Text. */
    protected Text m_docIdText;
    
    private static final int HORIZONTAL_SPAN_FOR_NAME_FIELD = 6;
    private static final int HORIZONTAL_SPAN_FOR_SUB_COMPOSITE = 3;
    private static final int HORIZONTAL_SPAN_FOR_DOCUMENT_ID_TEXT_FIELD = 4;
    private static final int NO_OF_COLUMNS_FOR_MAIN_COMPOSITE = 4;
    private static final int NO_OF_COLUMNS_FOR_SUB_COMPOSITE = 7;
    
}
