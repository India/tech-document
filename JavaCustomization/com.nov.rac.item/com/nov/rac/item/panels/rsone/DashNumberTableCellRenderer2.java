package com.nov.rac.item.panels.rsone;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.util.iTextField;


public class DashNumberTableCellRenderer2 implements TableCellRenderer
{
    private iTextField dashNumberText;
    
    public DashNumberTableCellRenderer2()
    {
        dashNumberText  = new iTextField();
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        boolean isDashExist = (Boolean) table.getValueAt(row, 1);
        
        dashNumberText.setBorder(BorderFactory.createEmptyBorder());
        dashNumberText.setRequired(true);
        
        if(value != null)
        {
            dashNumberText.setText(value.toString());
        }
        
        if(!isDashExist)
        {
            dashNumberText.setRequired(false);
            dashNumberText.setEnabled(false);
            dashNumberText.setEditable(false);
        }
        else
        {
            dashNumberText.setEnabled(true);
        }
        
        if(table.getSelectedRow() == row)
        {
            dashNumberText.setBackground(table.getSelectionBackground());
            dashNumberText.setForeground(table.getSelectionForeground());
        }
        else
        {
            dashNumberText.setBackground(table.getBackground());
            dashNumberText.setForeground(table.getForeground());
        }

        return dashNumberText;
    }
}
