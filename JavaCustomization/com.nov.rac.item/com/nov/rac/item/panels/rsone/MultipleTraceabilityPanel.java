package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.CopyPropertyHelper;
import com.nov.rac.propertymap.IPropertyMap;

public class MultipleTraceabilityPanel extends SimpleTraceabilityPanel implements IPublisher
{
    
    public MultipleTraceabilityPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    protected void createTraceablityLabelFields(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(getLayout(NUM_OF_COLUMNS_FOR_TRACEABILITY_LABEL_FIELD, false));
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        final Button l_traceCheckbox = new Button(l_composite, SWT.CHECK);
        l_traceCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        l_traceCheckbox.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                handleTraceablityCheckboxSelection();
                
                l_traceCheckbox.setSelection(false);
            }
        });
        
        createTracebilityLabel(l_composite);
        createTraceabilityHelpIcon(l_composite);
    }
    
    private void handleTraceablityCheckboxSelection()
    {
        boolean isLotControlled = m_lotControlledButton.getSelection();
        boolean isSerialized = m_serializedButton.getSelection();
        boolean isNA = m_qaRequiredButton.getSelection();
        
        if (!isLotControlled && !isSerialized && !isNA)
        {
            int msgCode = getMessage(m_registry.getString("confirmation.msg"),
                    m_registry.getString("Traceability.NAME") + " " + m_registry.getString("Copy_Empty_Value.msg"),
                    SWT.YES | SWT.NO | SWT.ICON_QUESTION);
            
            if (msgCode == SWT.YES)
            {
                callCopyTracePropertyEvent();
            }
        }
        else
        {
            callCopyTracePropertyEvent();
        }
    }
    
    private void callCopyTracePropertyEvent()
    {
        publishCopyPropertyEvent(new String[] { "IMAN_master_form", NationalOilwell.RSONE_LOTCONTROL },
                m_lotControlledButton.getSelection(), m_registry.getString("Traceability.NAME"));
        publishCopyPropertyEvent(new String[] { "IMAN_master_form", NationalOilwell.RSONE_SERIALIZE },
                m_serializedButton.getSelection(), m_registry.getString("Traceability.NAME"));
        publishCopyPropertyEvent(new String[] { "IMAN_master_form", NationalOilwell.RSONE_QAREQUIRED },
                m_qaRequiredButton.getSelection(), m_registry.getString("Traceability.NAME"));
    }
    
    private void publishCopyPropertyEvent(String[] propertyName, Object propValue, String propDisplayName)
    {
        CopyPropertyHelper l_copyProperty = new CopyPropertyHelper();
        l_copyProperty.setProperties(propertyName, propValue, propDisplayName);
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.RSONE_COPY_PROPERTY, l_copyProperty, null);
        controller.publish(event);
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
        org.eclipse.swt.widgets.MessageBox dialog = new org.eclipse.swt.widgets.MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private final static int NUM_OF_COLUMNS_FOR_TRACEABILITY_LABEL_FIELD = 3;
}
