package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneDashNumberPanel extends AbstractUIPanel implements ILoadSave, IPublisher, ISubscriber
{
    
    public RSOneDashNumberPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        m_registry = getRegistry();
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
    	m_dashList.setItems(PropertyMapHelper.handleNullForArray(propMap.getStringArray("dashNumbers")));
    	m_dashList.setSelection(0);
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
    	propMap.setStringArray("dashNumbers", m_dashList.getItems());
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        l_leftPanel = getComposite();
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        l_leftPanel.setLayoutData(gd_panel);
        l_leftPanel.setLayout(new GridLayout(1, true));
        
        // Dash# title
        m_dashLable = new Label(l_leftPanel, SWT.NONE);
        m_dashLable.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        m_dashLable.setText(m_registry.getString("RSOnePartInfoPanel.DashTitle"));
        
        // List of Dash Numbers
        m_dashList = new List(l_leftPanel, SWT.V_SCROLL | SWT.BORDER);
        GridData l_gdList = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 3);
        
        m_dashList.setLayoutData(l_gdList);
        
        /*m_dashList.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                // Modify button clicked...
                String[] l_selectedDashNumbers = m_dashList.getSelection();
                
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_DASH_NUMBER_SELECTION_EVENT, l_selectedDashNumbers[0], null);
                
                controller.publish(event);
                
            }
        });*/
        
        m_dashList.add("001");
        m_dashList.setSelection(0);
        setPreviousSelectedIndex(0);
        m_dashListSelectionListener = new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                int prevSelection = getPreviousSelectedIndex();
                int currentSelectedIndex = m_dashList.getSelectionIndex();
                setPreviousSelectedIndex(currentSelectedIndex);
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.DASH_PROPERTYMAP_MODIFY_EVENT, m_dashList.getItem(currentSelectedIndex), m_dashList.getItem(prevSelection));
                controller.publish(event);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        };
        m_dashList.addSelectionListener(m_dashListSelectionListener);
        
        // Modify button
        m_modifyButton = new Button(l_leftPanel, SWT.NONE);
        m_modifyButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_modifyButton.setText(m_registry.getString("modifyButton.Name"));
        m_modifyButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                // Modify button clicked...
            	m_dashList.removeSelectionListener(m_dashListSelectionListener);
            	setPreviousSelectedIndex(m_dashList.getSelectionIndex());
                m_tableButton.setEnabled(false);
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_DASH_NUMBER_MODIFY_EVENT, m_dashList.getItems(), null);
                
                controller.publish(event);
            }
        });
        
        // Table Button
        m_tableButton = new Button(l_leftPanel, SWT.NONE);
        m_tableButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_tableButton.setText(m_registry.getString("tableButton.Name"));
        m_tableButton.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                final IController controller = ControllerFactory.getInstance().getDefaultController();
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_DASH_NUMBER_TABLE_EVENT, m_dashList.getItems(), null);
                
                controller.publish(event);
            }
        });
        
        new Label(l_leftPanel, SWT.NONE);
        new Label(l_leftPanel, SWT.NONE);
        
        if(m_isAssignOracleIdChecked)
        {
        	m_modifyButton.setVisible(false);
        	m_tableButton.setVisible(false);
        }
        
        registerSubscriber();
        
        UIHelper.makeMandatory(m_dashList);
        
        return true;
    }
    
    private void setPreviousSelectedIndex(int i)
    {
        previousSelectedIndex = i;
    }
    
    public int getPreviousSelectedIndex()
    {
        return previousSelectedIndex;
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.MSG_MODIFY_PANEL_DONE_BUTTON_SELECTION, this);
        controller.registerSubscriber(NationalOilwell.MSG_ASSIGN_ORACLE_ID_SELECTED, this);
        controller.registerSubscriber(NationalOilwell.SEND_TO_CLASSIFICATION, this);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().compareTo(NationalOilwell.MSG_MODIFY_PANEL_DONE_BUTTON_SELECTION) == 0)
        {
        	String[] dashNumbers = (String[]) event.getNewValue();
            m_dashList.setItems(dashNumbers);
            m_tableButton.setEnabled(true);
            m_dashList.setSelection(getPreviousSelectedIndex());
            m_dashList.addSelectionListener(m_dashListSelectionListener);
            IController controller = ControllerFactory.getInstance().getDefaultController();
            PublishEvent event2 = new PublishEvent(getPublisher(),
                    NationalOilwell.MODIFY_PANEL_DONE_BUTTON_SELECTION, dashNumbers, null);
            
            controller.publish(event2);
        }
        else if (event.getPropertyName().compareTo(NationalOilwell.MSG_ASSIGN_ORACLE_ID_SELECTED) == 0)
        {
            m_tableButton.setVisible(false);
            m_modifyButton.setVisible(false);
        	//m_isAssignOracleIdChecked = (Boolean) event.getNewValue();
        }
        else if (event.getPropertyName().compareTo(NationalOilwell.SEND_TO_CLASSIFICATION) == 0)
        {
        	boolean isChekced = (Boolean) event.getNewValue();
        	
        	if(isChekced)
        	{
        		 m_tableButton.setEnabled(false);
                 m_modifyButton.setEnabled(false);
        	}
        	else
        	{
        		m_tableButton.setEnabled(true);
                m_modifyButton.setEnabled(true);
        	}
           
        }
    }
    
    @Override
    public void dispose()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.unregisterSubscriber(NationalOilwell.MSG_MODIFY_PANEL_DONE_BUTTON_SELECTION, this);
        theController.unregisterSubscriber(NationalOilwell.MSG_ASSIGN_ORACLE_ID_SELECTED, this);
        theController.unregisterSubscriber(NationalOilwell.SEND_TO_CLASSIFICATION, this);
        
        super.dispose();
    }
/*    private void setPreviousSelectedIndex(int i)

    {

    previousSelectedIndex = i;

    }

    public int getPreviousSelectedIndex()

    {

    return previousSelectedIndex;

    }*/


    
    protected Label m_dashLable;
    private boolean m_isAssignOracleIdChecked = false;
    protected Registry m_registry;
    protected Button m_modifyButton;
    private int previousSelectedIndex = 0;
    protected Button m_tableButton;
    private SelectionListener m_dashListSelectionListener;
    protected List m_dashList;
    protected SimplePropertyMap subPropertyMap = new SimplePropertyMap();
    protected Composite l_leftPanel;
    
}
