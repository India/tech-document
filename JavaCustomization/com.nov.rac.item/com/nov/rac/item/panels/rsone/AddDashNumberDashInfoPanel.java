package com.nov.rac.item.panels.rsone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.RSOneDashNumberHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.GeneralItemUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class AddDashNumberDashInfoPanel extends SimpleDashInfoPanel2 implements ILoadSave, ISubscriber
{
    public AddDashNumberDashInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        registerSubscriber();
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(NationalOilwell.MSG_RSONE_RELOAD_PANELS, this);
    }
    
    @Override
    protected void createDashLabelRow(Composite parent)
    {
        Composite l_dashComposite = new Composite(parent, SWT.NONE);
        GridLayout l_gridLayout = new GridLayout(2, false);
        l_gridLayout.marginWidth = 0;
        l_gridLayout.marginHeight = 0;
        l_dashComposite.setLayout(l_gridLayout);
        l_dashComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        super.createDashLabelRow(l_dashComposite);
        
        m_modifyDashInfoBtn = new Button(l_dashComposite, SWT.NONE);
        m_modifyDashInfoBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
        m_addImage = new Image(l_dashComposite.getDisplay(), m_registry.getImage("add.Icon"), SWT.NONE);
        m_modifyDashInfoBtn.setImage(m_addImage);
        
        addListners();
    }
    
    private void addListners()
    {
        m_modifyDashInfoBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                publishDashInfoModifyEvent();
            }
        });
    }
    
    private void publishDashInfoModifyEvent()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.MSG_DASH_INFO_MODIFY_EVENT,
                m_newDashNumbers, null);
        
        controller.publish(event);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String proertyName = event.getPropertyName();
        
        if (proertyName.compareTo(NationalOilwell.MSG_RSONE_RELOAD_PANELS) == 0)
        {
            m_newDashNumbers = (String[]) event.getNewValue();
            
            m_allDashNumbers = getAlllDashNumbers(m_newDashNumbers);
            
            resetTable(m_allDashNumbers);
        }
    }
    
    private String[] getAlllDashNumbers(String[] dashNumbers)
    {
        List<String> dashNoList = new ArrayList<String>();
        dashNoList.addAll(m_existingDashNumbers);
        
        for(String dashNumber : dashNumbers)
        {
            dashNoList.add(dashNumber);
        }
        Collections.sort(dashNoList);
        
        String[] allDashNumbers = dashNoList.toArray(new String[dashNoList.size()]);
        
        return allDashNumbers;
    }

    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        super.dispose();
    }
    
    private void unRegisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(NationalOilwell.MSG_RSONE_RELOAD_PANELS, this);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponent targetComponent = propMap.getComponent();
        TCComponentItem item = GeneralItemUtils.getItemFromTCComponent(targetComponent);
        
        String[] dashNumberList = null;
        String altId = null;
        
        if (item != null)
        {
            altId = item.getProperty("altid_list");
            
            if (altId != null && !altId.equals(""))
            {
                // example : get '1218123646' from 1218123646-001@RSOne
                altId = altId.split(DASH_STR)[0];
                dashNumberList = RSOneDashNumberHelper.getAllDashNumbers(altId);
                setExistingDashVector(dashNumberList, RSOneDashNumberHelper.getMissingDashNumber());
                
                removeAllRows();
                addRows(dashNumberList);
                setMissingDashRowSelection(dashNumberList, RSOneDashNumberHelper.getMissingDashNumber());
                m_newDashNumbers = new String[]{RSOneDashNumberHelper.getMissingDashNumber()};
            }
        }
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    private static final String DASH_STR = "-";
    private Button m_modifyDashInfoBtn;
    private Image m_addImage;
    
    private String[] m_newDashNumbers;
    
}
