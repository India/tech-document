package com.nov.rac.item.panels.rsone;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Event;

import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.item.panels.RSOneSimpleEnggNonEnggTypePanel;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneLegacyOperationTypePanel extends AbstractUIPanel implements IPublisher, ILoadSave
{
    private Registry m_registry;
    
    public RSOneLegacyOperationTypePanel(Composite parent, int style) throws Exception
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    public boolean createUI() throws TCException
    {
        Composite l_typeComposite = getComposite();
        GridData l_gdDocComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_typeComposite.setLayoutData(l_gdDocComposite);
        l_typeComposite.setLayout(new GridLayout(5, true));
        
        createEmptyPanel(l_typeComposite);
        
        try
        {
            createTypePanel(l_typeComposite);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }
    
    private void createEmptyPanel(Composite l_composite)
    {
        Composite emptyComposite = new Composite(l_composite, SWT.None);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        emptyComposite.setLayoutData(gridData);
        emptyComposite.setLayout(new GridLayout(1, true));
    }
    
    private void createTypePanel(Composite l_typeComposite) throws Exception
    {
        final RSOneSimpleEnggNonEnggTypePanel l_typePanel = new RSOneSimpleEnggNonEnggTypePanel(l_typeComposite, SWT.NONE);
        
        l_typePanel.createUI();
        UIHelper.makeMandatory(l_typePanel.m_typeCombo);
        l_typePanel.m_engineeringRadio.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                if (l_typePanel.m_engineeringRadio.getSelection())
                {
                    l_typePanel.loadPartsTypes(m_registry.getString("operationTypePanel.Engineering"));
                }
            }
            
        });
        l_typePanel.m_engineeringRadio.setSelection(true);
        l_typePanel.m_engineeringRadio.notifyListeners(SWT.Selection, new Event());
        l_typePanel.m_typeCombo.notifyListeners(SWT.Selection, new Event());
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
}
