package com.nov.rac.item.panels;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.DocCategoryUIModel;
import com.nov.rac.item.helpers.NOVDatasetTypeUIPanelHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.teamcenter.rac.kernel.TCException;

public class RSOneDatasetPanel extends NewDatasetPanel
{
    
    public RSOneDatasetPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        
        super.update(event);
        
        final DocCategoryUIModel docCategoryUIModel = (DocCategoryUIModel) event.getNewValue();
        Display.getDefault().asyncExec(new Runnable()
        {
            public void run()
            {
                if (!docCategoryUIModel.getDocumentCategory().equals(""))
                {
                    m_propDocCategory = docCategoryUIModel.getDocumentCategory();
                    m_textDatasetType.setEnabled(docCategoryUIModel.isDocCategoryTextEnabled());
                    m_dataSetLovPopupButton.getPopupButton().setEnabled(docCategoryUIModel.isDocCategoryTextEnabled());
                }
                else
                {
                    m_textDatasetType.setText("");
                    m_comboFileTemplate.setText("");
                    setEnable(true);
                }
                if (!docCategoryUIModel.getDocumentType().equals(""))
                {
                    m_propDocType = docCategoryUIModel.getDocumentType();
                }
            }
        });
        
    }
    
    @Override
    protected PublishEvent getPublishEventForDatasetType()
    {
        // TODO Auto-generated method stub
        return getPublishEvent(NationalOilwell.RSONE_DATASET_TYPE, "Prev Val", m_textDatasetType.getText());
    }
}
