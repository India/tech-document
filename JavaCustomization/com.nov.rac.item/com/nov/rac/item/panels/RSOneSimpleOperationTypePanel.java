package com.nov.rac.item.panels;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;

import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneOperationTypePanelHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.utils.UIHelper;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RSOneSimpleOperationTypePanel extends AbstractUIPanel implements IPublisher, ILoadSave
{
    public RSOneSimpleOperationTypePanel(Composite parent, int style) throws Exception
    {
        super(parent, SWT.NONE);
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    public boolean createUI() throws TCException
    {
        Composite l_operationTypePanel = getComposite();
        GridData gd_panel = getUILayoutData();
        
        l_operationTypePanel.setLayoutData(gd_panel);
        l_operationTypePanel.setLayout(getUILayout());
        
        createRadioButtons1Row(l_operationTypePanel);
        createTypeRow(l_operationTypePanel);
        
        //Tushar added to remove dependancy of RSOneCreateOperationTypePanel
        UIHelper.makeMandatory(m_typeCombo);
        
        //Tushar commented for time being
        m_engineeringRadio.setSelection(true);
        m_engineeringRadio.notifyListeners(SWT.Selection, new Event());
        
        return true;
    }
    
    protected GridData getUILayoutData()
    {
        return new GridData(SWT.FILL, SWT.FILL, true, /*true, 4*/ false, 1, 1);
    }
    
    protected GridLayout getUILayout()
    {
        return new GridLayout(/*4, true*/ 7, false);
    }
    
	protected GridData getSubCompositeLayoutData()
    {
        
        return  new GridData(SWT.FILL, SWT.CENTER, true, true, 3, 1);
    }
    
    protected GridLayout getSubCompositeLayout()
    {
        GridLayout l_glDocPanel = new GridLayout(/*7*/ 8, true);
        l_glDocPanel.marginWidth = 0;
        l_glDocPanel.marginTop = 0;
        
        return l_glDocPanel;
    }
    
    protected void createTypeRow(Composite typePanel)
    {
        //Tushar commented
//        m_typeLabel = new Label(typePanel, SWT.NONE);
//        m_typeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
//        m_typeLabel.setText(m_registry.getString("operationTypePanel.Type"));
        
        Composite l_typeComposite = new Composite(typePanel, SWT.NONE);
        l_typeComposite.setLayoutData(getSubCompositeLayoutData());
        
        //Tushar added
        Label blank = new Label(l_typeComposite, SWT.NONE);
        blank.setText(" ");
        
        m_typeLabel = new Label(/*typePanel*/l_typeComposite, SWT.NONE);
        m_typeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 2));
        m_typeLabel.setText(m_registry.getString("operationTypePanel.Type"));
        
        
        l_typeComposite.setLayout(getSubCompositeLayout());
        
        m_typeCombo = new Combo(l_typeComposite, SWT.READ_ONLY | SWT.BORDER);
        m_typeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, /*true*/false, 6, 2));
        m_typeCombo.addListener(SWT.Modify, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                if (m_typeCombo.getSelectionIndex() != -1)
                {
                    RSOneOperationTypePanelHelper.setSelectedItemType(m_typeCombo.getItem(m_typeCombo
                            .getSelectionIndex()));
                    
                    String partType = RSOneOperationTypePanelHelper.getPartType();    
                    
                    
                    publishPartType(partType);
                }
                else
                {
                	publishPartType("Part");
                }
            }
        });
        new Label(l_typeComposite, SWT.NONE).setVisible(false);
    }
    
    private void publishPartType(final String partType)
    {
        if (!partType.equalsIgnoreCase(NewItemCreationHelper.getSelectedType()))
        {
            NewItemCreationHelper.setSelectedType(partType);
            Display.getDefault().asyncExec(new Runnable()
            {
                
                @Override
                public void run()
                {
                  
                    IController theController = ControllerFactory.getInstance().getDefaultController();
                    theController.publish(new PublishEvent(getpublisher(), NationalOilwell.PART_TYPE_CHANGE_EVENT ,partType, ""));
                    theController.publish(new PublishEvent(getpublisher(), NationalOilwell.UPDATE_DIALOG_AREA ,partType, ""));
                }
            });
           
        }
        
    }
    
    private IPublisher getpublisher()
    {
        return this;
        
    }
    
    protected void createRadioButtons1Row(Composite typePanel)
    {
//        new Label(typePanel, SWT.NONE).setVisible(false);
        
        Composite radioComposite = new Composite(typePanel, SWT.NONE);
        radioComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, /*true*/false, false, /*3*/ 2, 1));
        GridLayout gl_typeCombo = new GridLayout(2, false);
        gl_typeCombo.marginHeight = 0;
        gl_typeCombo.marginWidth = 0;
        radioComposite.setLayout(gl_typeCombo);
        
        m_engineeringRadio = new Button(radioComposite, SWT.RADIO);
        m_engineeringRadio.setText(m_registry.getString("operationTypePanel.Engineering"));
        m_engineeringRadio.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                if (m_engineeringRadio.getSelection())
                {
                    loadPartsTypes(m_registry.getString("operationTypePanel.Engineering"));
                    
                    //Tushar added if condition
                    if(!NewItemCreationHelper.getSelectedOperation().equalsIgnoreCase("CreateLegacyPart") && 
                            !NewItemCreationHelper.getSelectedOperation().equalsIgnoreCase("CopyAPart"))
                    {
                        selectType(m_registry.getString("MakeItemProp.NAME"));
                    }
                    
                    IController theController = ControllerFactory.getInstance().getDefaultController();
                    theController.publish(new PublishEvent(getpublisher(), NationalOilwell.RSONE_ITEM_TYPE_EVENT,
                            NationalOilwell.PART_BO_TYPE, ""));
                    RSOneOperationTypePanelHelper.setSelectedPartType(NationalOilwell.PART_BO_TYPE);
                    
                }
                
            }
 });
        
        m_nonengineeringRadio = new Button(radioComposite, SWT.RADIO);
        m_nonengineeringRadio.setText(m_registry.getString("operationTypePanel.NonEngineering"));
        m_nonengineeringRadio.setSelection(false);
        m_nonengineeringRadio.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                if (m_nonengineeringRadio.getSelection())
                {
                    loadPartsTypes(m_registry.getString("operationTypePanel.NonEngineering"));
                    IController theController = ControllerFactory.getInstance().getDefaultController();
                    theController.publish(new PublishEvent(getpublisher(), NationalOilwell.RSONE_ITEM_TYPE_EVENT,
                            NationalOilwell.NON_ENGINEERING, ""));
                    
                    //To work purchase part reloading for non-engineerin part also : 8090
                   /* RSOneOperationTypePanelHelper.setSelectedItemType("");
                    theController.publish(new PublishEvent(getpublisher(), NationalOilwell.PART_TYPE_CHANGE_EVENT ,"Non-Engineering", ""));
                    theController.publish(new PublishEvent(getpublisher(), NationalOilwell.UPDATE_DIALOG_AREA ,"", ""));*/
                    //To work purchase part reloading for non-engineerin part also : 8090
                }
            }
        });
    }
    
    public void populateTypeCombo(Map<String, String> l_OracleTCEEngTypeMap)
    {
        Set<String> EngKeySet = l_OracleTCEEngTypeMap.keySet();
        Collection<String> EngValueSet = l_OracleTCEEngTypeMap.values();
        
        Object[] obj_Key = EngKeySet.toArray();
        Object[] obj_Value = EngValueSet.toArray();
        
        String[] m_EngOracleType = new String[obj_Key.length];
        
        for (int i = 0; i < obj_Key.length; i++)
        {
            m_EngOracleType[i] = String.valueOf(obj_Key[i]);
            m_typeCombo.setData(m_EngOracleType[i], obj_Value[i]);
        }
        
        m_typeCombo.setItems(m_EngOracleType);
    }
    
    protected void selectType(String typeName)
    {
        m_typeCombo.select(m_typeCombo.indexOf(typeName));
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    public void loadPartsTypes(String argType)
    {
        try
        {
            Map<String, String> l_OracleTCEEngTypeMap = new HashMap<String, String>();
            
            l_OracleTCEEngTypeMap = RSOneOperationTypePanelHelper.getPartsTypeLov(argType);
            
            populateTypeCombo(l_OracleTCEEngTypeMap);
        }
        
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
        boolean engSelected = masterPropertyMap.getLogical(NationalOilwell.RSONE_ENGCONTROLLED);
        m_engineeringRadio.setSelection(engSelected);
        m_nonengineeringRadio.setSelection(!engSelected);
        m_typeCombo.notifyListeners(SWT.Selection, new Event());
        
        notifyListeners();
        
        m_typeCombo.setText(PropertyMapHelper.handleNull(masterPropertyMap
                .getString(NationalOilwell.RSONE_ITEMTYPE)));
        
        return true;
    }
    
    private void notifyListeners()
    {
        m_engineeringRadio.notifyListeners(SWT.Selection, new Event());
        m_nonengineeringRadio.notifyListeners(SWT.Selection, new Event());
        m_typeCombo.notifyListeners(SWT.Selection, new Event());
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        IPropertyMap masterPropertyMap = propMap.getCompound("IMAN_master_form");
       // propMap.setStringArray(NationalOilwell.RSONE_ITEMTYPE, new String[]{m_typeCombo.getText()});
        
        masterPropertyMap.setLogical(NationalOilwell.RSONE_ENGCONTROLLED, m_engineeringRadio.getSelection());
        
        masterPropertyMap.setString(NationalOilwell.RSONE_ITEMTYPE, m_typeCombo.getText());
        
        propMap.setCompound("IMAN_master_form", masterPropertyMap);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    public Button m_engineeringRadio;
    public static Button m_nonengineeringRadio;
    protected Button m_tablePush;
    private Label m_typeLabel;
    public Combo m_typeCombo;
    protected Registry m_registry;
}