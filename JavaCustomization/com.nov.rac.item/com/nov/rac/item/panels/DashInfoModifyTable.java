package com.nov.rac.item.panels;

import java.util.Map;
import java.util.Set;

import javax.swing.table.TableColumn;

import com.nov.NationalOilwell;

import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneDashPropertyMap;

import com.nov.rac.item.renderer.CheckboxHeaderRenderer;
import com.nov.rac.item.renderer.ComboBoxCellRenderer;
import com.nov.rac.item.renderer.ComboCellEditor;
import com.nov.rac.item.renderer.ArrayValueComboEditor;
import com.nov.rac.item.renderer.ArrayValueComboRenderer;
import com.nov.rac.item.renderer.ManufacturerNameCellEditor;
import com.nov.rac.item.renderer.ManufacturerNameCellRenderer;
import com.nov.rac.item.renderer.TextAreaCellEditor;
import com.nov.rac.item.renderer.TextAreaRenderer;
import com.nov.rac.item.renderer.TraceabilityComboEditor;
import com.nov.rac.item.renderer.TraceabilityComboRenderer;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.TableUtils;

import com.teamcenter.rac.common.TCTable;

import com.teamcenter.rac.kernel.TCSession;

import com.teamcenter.rac.util.Registry;

public class DashInfoModifyTable extends TCTable
{
    private static final long serialVersionUID = -8954422881453190650L;
    
    private static final int DASH_NO_COLUMN_WIDTH = 5;
    
    public TCSession m_session;
    protected Registry m_registry;
    
    protected CheckboxHeaderRenderer m_headerrenderer = null;
    
    protected TableColumn m_no;
    protected TableColumn m_rsoneOrg;
    protected TableColumn m_name;
    protected TableColumn m_class;
    protected TableColumn m_uom;
    protected TableColumn m_desc;
    protected TableColumn m_traceability;
    protected TableColumn m_mfgName;
    protected TableColumn m_mfgNo;
    
    protected static int column = -1;
    
    public DashInfoModifyTable(TCSession tcsession, String[] columnNames)
    {
        super(tcsession, columnNames);
    }
    
    public DashInfoModifyTable(String[] columnIds, String[] columnNames)
    {
        super(columnIds, columnNames);
        
        m_registry = Registry.getRegistry(this);
        setSortEnabled(false);
        setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
        
        configureTable();
    }
    
    private void configureTable()
    {
        try
        { 
            m_no = getColumnModel().getColumn(0);
            m_no.setPreferredWidth(DASH_NO_COLUMN_WIDTH);
            setColumnRenderer(m_no);
            
            m_rsoneOrg = getColumnModel().getColumn(TableUtils.getColumnIndex("IMAN_master_form.rsone_org", this));
            m_rsoneOrg.setCellEditor(new ArrayValueComboEditor(NationalOilwell.RSOne_ORG_LOV));
            m_rsoneOrg.setCellRenderer(new ArrayValueComboRenderer(NationalOilwell.RSOne_ORG_LOV));
            
            m_name = getColumnModel().getColumn(TableUtils.getColumnIndex("object_name", this));
            
            m_class = getColumnModel().getColumn(TableUtils.getColumnIndex("ics_subclass_name", this));
            
            m_uom = getColumnModel().getColumn(TableUtils.getColumnIndex("IMAN_master_form.rsone_uom", this));
            m_uom.setCellEditor(new ComboCellEditor(NationalOilwell.UOMLOV));
            m_uom.setCellRenderer(new ComboBoxCellRenderer(NationalOilwell.UOMLOV));
            
            m_desc = getColumnModel().getColumn(TableUtils.getColumnIndex("object_desc", this));
            m_desc.setCellRenderer(new TextAreaRenderer());
            m_desc.setCellEditor(new TextAreaCellEditor());
            
            m_traceability = getColumnModel().getColumn(TableUtils.getColumnIndex("traceability", this));
            m_traceability.setCellRenderer(new TraceabilityComboRenderer());
            m_traceability.setCellEditor(new TraceabilityComboEditor());
            
            configureMfgColumns();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void configureMfgColumns()
    {
        String l_boType =  getBoType();
        String l_partType = NewItemCreationHelper.getSelectedType();
        
        if (null != l_partType && null != l_boType)
        {
            boolean isEngPurchased = l_partType.equalsIgnoreCase(
                    m_registry.getString("EnggPurchasedType.NAME"));
            boolean isNonEngPurchased = l_partType.equalsIgnoreCase(
                    m_registry.getString("NonEnggPurchasedType.Name"));
            
            if ((isEngPurchased || isNonEngPurchased) && l_boType.equalsIgnoreCase(NationalOilwell.PART_BO_TYPE)) 
            {
                m_mfgName = getColumnModel().getColumn(TableUtils.getColumnIndex("revision.nov4_mfg_name", this));
                m_mfgNo = getColumnModel().getColumn(TableUtils.getColumnIndex("revision.nov4_mfg_number", this));
                
                setCellEditorToMfgFields();
                setCellRendererToMfgFields();
            }
            else if((isEngPurchased || isNonEngPurchased) && l_boType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
            {
                m_mfgName = getColumnModel().getColumn(TableUtils.getColumnIndex("revision.manufacturer_name", this));
                m_mfgNo = getColumnModel().getColumn(TableUtils.getColumnIndex("revision.manufacturer_partnumber", this));
                
                setCellEditorToMfgFields();
                setCellRendererToMfgFields();
            }
        }
    }
    
    private String getBoType()
    {
        Map<String, IPropertyMap> dashPropMap = RSOneDashPropertyMap.getDashPropertyMap();
        Set<String> sdashNumbers = dashPropMap.keySet();
        String [] adashNumbers = sdashNumbers.toArray(new String[sdashNumbers.size()]);
        IPropertyMap propMap = dashPropMap.get(adashNumbers[0]);
        
        return propMap.getType();
    }
    
    private void setCellRendererToMfgFields()
    {
        m_mfgName.setCellRenderer(new ArrayValueComboRenderer(NationalOilwell.MFGNAMES));
        m_mfgNo.setCellRenderer(new ManufacturerNameCellRenderer());
    }
    
    private void setCellEditorToMfgFields()
    {
        m_mfgName.setCellEditor(new ArrayValueComboEditor(NationalOilwell.MFGNAMES));
        m_mfgNo.setCellEditor(new ManufacturerNameCellEditor());
    }
    
    @Override
    public boolean isCellEditable(int row, int column)
    {
        boolean isCellEditable = true;
        
        if (column == 0)
        {
            isCellEditable = false;
            return isCellEditable;
        }
        
        return isCellEditable;
    }
}
