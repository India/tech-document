package com.nov.rac.item.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

public class RSOnePurchasedPartInfoPanel extends RSOneSimplePartInfoPanel
{
    public RSOnePurchasedPartInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected void createRightPanel(Composite partInfoPanel) throws TCException
    {
        if(dashNumbers!=null && dashNumbers.length > 1)
        {
            m_RSOnePartInfoRightPanel = new RSOnePartInfoRightPanelWithMfg(partInfoPanel, SWT.NONE);
            m_RSOnePartInfoRightPanel.createUI();
        }
        else
        {
            m_RSOnePartInfoRightPanel = new RSOnePartInfoRightPanelWithMfgWithoutCheckBox(partInfoPanel, SWT.NONE);
            m_RSOnePartInfoRightPanel.createUI();
        }
    }
    
}
