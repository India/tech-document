package com.nov.rac.item.panels;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.IOperationInputProvider;
import com.nov.rac.item.dialog.DialogReloadHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class AssignOracleIdPanel extends AbstractUIPanel implements IPublisher
{
    
    public AssignOracleIdPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.panels.panels");
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_oracleIdcomposite = getComposite();
        l_oracleIdcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        l_oracleIdcomposite.setLayout(new GridLayout(1, false));
        
        m_assignOracleIdBtn = new Button(l_oracleIdcomposite, SWT.CHECK);
        m_assignOracleIdBtn.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true, 1, 1));
        m_assignOracleIdBtn.setText(" " + m_registry.getString("AssignOracleID.Name"));
        
        m_assignOracleIdBtn.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                publishEvent();
            }
        });
        
        return true;
    }
    
    private void publishEvent()
    {
        Display.getDefault().asyncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (m_assignOracleIdBtn.getSelection())
                {
                    publishReloadEvent("CreateLegacyPartWithOracleId");
                }
                else
                {
                    publishReloadEvent("CreateLegacyPart");
                }
            }
        });
    }
    
    private void publishReloadEvent(String operationName)
    {
        NewItemCreationHelper.setSelectedOperation(operationName);
        
        IPropertyMap propertyMap = new SOAPropertyMap(NationalOilwell.PART_BO_TYPE);
        
        CreateInCompoundHelper compoundHelper = new CreateInCompoundHelper(propertyMap);
        compoundHelper.addCompound("MasterForm", "IMAN_master_form");
        compoundHelper.addCompound("ItemRevision", "revision");
        
        ArrayList<IUIPanel> panels = getPanels(operationName);
        savePropertyMap(panels, propertyMap);
        
        DialogReloadHelper dialogObject = new DialogReloadHelper();
        dialogObject.setPropertyMap(propertyMap);
        dialogObject.setpropertyValue(operationName);
        
        final IController theController = ControllerFactory.getInstance().getDefaultController();
        
        theController.publish(new PublishEvent(getPublisher(), NationalOilwell.DIALOG_RELOAD, dialogObject, ""));
    }
    
    private ArrayList<IUIPanel> getPanels(String panelType)
    {
        ArrayList<IUIPanel> panels = null;
        Composite l_composite = this.getComposite();
        
        Composite l_parentComposite = l_composite.getParent();
        
        //Tushar added
        Composite newItemComp = l_parentComposite.getParent();
        
        if (/*l_parentComposite*/newItemComp instanceof IOperationInputProvider)
        {
            panels = ((IOperationInputProvider) (/*l_parentComposite*/newItemComp)).getOperationInputProvider();
        }
        
        return panels;
    }
    
    private void savePropertyMap(ArrayList<IUIPanel> allPanels, IPropertyMap propertyMap)
    {
        if (allPanels != null)
        {
            for (IUIPanel panel : allPanels)
            {
                if (panel instanceof ILoadSave)
                {
                    ILoadSave saveablepanel = (ILoadSave) panel;
                    try
                    {
                        saveablepanel.save(propertyMap);
                    }
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    protected Registry m_registry;
    protected Button m_assignOracleIdBtn;
}