package com.nov.rac.item.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.util.ColorUtilities;
import com.teamcenter.rac.util.Registry;

public class TraceabilityComboRenderer implements TableCellRenderer
{
    private JComboBox<String> m_theUIBox;
    
    protected Registry m_registry = Registry.getRegistry("com.nov.rac.item.panels");;
    
    protected Color alternateBackground;
    protected static final Border noFocusBorder = BorderFactory.createEmptyBorder(1, 2, 1, 2);
    
    public TraceabilityComboRenderer()
    {
        alternateBackground = ColorUtilities.lighten(SystemColor.activeCaption, 0.45000000000000001D);
        alternateBackground = ColorUtilities.contrast(alternateBackground, -0.5D);
        alternateBackground = Color.YELLOW;
        
        m_theUIBox = new JComboBox<String>();
        
        // Load the LOV
        m_theUIBox.setOpaque(true);
        
        Vector<String> lovValues = getTraceabilityVector();
        
        // Populate the combobox with values.
        populateComponent(lovValues);
        
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        
        String strValue = "";
        
        if (value != null & !value.toString().trim().isEmpty())
        {
            strValue = value.toString();
        }
        
        // setTableColumns(table, strValue, row, column);
        if(!isSelected)
        {
            strValue = getRequiredValue(table, strValue, row, column);
        }
        
        table.setValueAt(strValue, row, column);
        
        m_theUIBox.setSelectedItem(strValue);
        
        return m_theUIBox;
    }
    
/*    private String getRequiredValue(JTable table, String value, int row, int column)
    {
        String returnValue = "";
        
        boolean lotControlled = (Boolean) table.getValueAt(row, column + 1);
        boolean isSerialized =  (Boolean)table.getValueAt(row, column + 2);
        boolean qaReqired = (Boolean)table.getValueAt(row, column + 3);
        
        if (qaReqired)
        {
            returnValue = "N/A";
        }
        else if (isSerialized && lotControlled)
        {
            returnValue = "Lot Controlled & Serialized";
            
        }
        else if (isSerialized)
        {
            returnValue = "Serialized";
            
        }
        else if (lotControlled)
        {
            returnValue = "Lot Controlled";
        }
        else
        {
            returnValue = value;
        }
        
        return returnValue;
    }*/
    
    private String getRequiredValue(JTable table, String value, int row, int column)
    {
        String returnValue = "";
        
       String lotControlled = (String) table.getValueAt(row, column + 1).toString();
       String isSerialized = (String) table.getValueAt(row, column + 2).toString();
       String qaReqired = (String) table.getValueAt(row, column + 3).toString();
       
       if (qaReqired.compareTo("true") == 0)
       {
           returnValue = "N/A";
       }
       else if (isSerialized.compareTo("true") == 0 && lotControlled.compareTo("true") == 0)
       {
           returnValue = "Lot Controlled & Serialized";
           
       }
       else if (isSerialized.compareTo("true") == 0)
       {
           returnValue = "Serialized";
           
       }
       else if (lotControlled.compareTo("true") == 0)
       {
           returnValue = "Lot Controlled";
       }
       else
       {
           returnValue = value;
       }
       
       return returnValue;
    }
    
    private Vector<String> getTraceabilityVector()
    {
        Vector<String> traceVector = new Vector<String>();
        traceVector.add(0, "");
        traceVector.add(1,
                Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_LotControlled"));
        traceVector.add(2,
                Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_Serialized"));
        traceVector.add(3, Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_N_A"));
        traceVector.add(
                4,
                Registry.getRegistry("com.nov.rac.item.panels.panels").getString(
                        "DashTable.Trace_Lot_Controlled_Serialized"));
        return traceVector;
    }
    
    protected void populateComponent(Vector<String> lovValues)
    {
        m_theUIBox.setModel(new DefaultComboBoxModel<String>(lovValues));
        m_theUIBox.setVisible(true);
    }
}
