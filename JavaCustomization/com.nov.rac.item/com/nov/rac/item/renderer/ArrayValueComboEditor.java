package com.nov.rac.item.renderer;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.JTable;

public class ArrayValueComboEditor extends ComboCellEditor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArrayValueComboEditor(String theLOVName) 
	{
		super(theLOVName);
	}
	
    public boolean shouldSelectCell(EventObject eventobject)
    {
        return false;
    }
	
	@Override
	public Component getTableCellEditorComponent(JTable jtable, Object value, boolean flag, int i, int j)
	{
		String strValue = "";
        
        if(value instanceof String[])
        {
            String[] strArray = (String[]) value;
            value = (Object)strArray[0];
        }
        
        if (value != null & !value.toString().trim().isEmpty())
        {
            strValue = value.toString();
        }
        
        m_theComboBox.setSelectedItem(strValue);
        
		return m_theComboBox;
	}
	
    public Object getCellEditorValue()
    {
    	String value = (String) m_theComboBox.getSelectedItem();
    	
        return new String[]{value};
    }

}
