package com.nov.rac.item.renderer;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import java.util.EventObject;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import com.teamcenter.rac.util.Registry;

public class TraceabilityComboEditor extends DefaultCellEditor
{
    
    private static final long serialVersionUID = 1L;
    
    private JComboBox<String> m_theComboBox;
    
    private JTable m_dashTable;
    private int m_selectedRow;
    private int m_selectedColumn;
    
    
    public TraceabilityComboEditor()
    {
        super(new JComboBox<Object>());
        
        m_theComboBox = (JComboBox<String>) getComponent();
        // Load the LOV
        Vector<String> lovValues = getTraceabilityVector();
        // Populate the combobox with values.
        populateComponent(lovValues);
        
        m_theComboBox.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent itemevent)
            {
                if (itemevent.getStateChange() == 1)
                {
                    setHiddenColumn();    
                }
            }
        });
    }
    
    private void setHiddenColumn()
    {
        String l_traceValue = m_theComboBox.getSelectedItem().toString();
        setRequiredValue(l_traceValue);
    }
    
    private Vector<String> getTraceabilityVector()
    {
        Vector<String> traceVector = new Vector<String>();
        traceVector.add(0, "");
        traceVector.add(1, Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_LotControlled"));
        traceVector.add(2, Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_Serialized"));
        traceVector.add(3, Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_N_A"));
        traceVector.add(4, Registry.getRegistry("com.nov.rac.item.panels.panels").getString("DashTable.Trace_Lot_Controlled_Serialized"));
        return traceVector;
    }
    
    protected void populateComponent(Vector<String> lovValues)
    {
        m_theComboBox.setModel(new DefaultComboBoxModel<String>(lovValues));
        m_theComboBox.setVisible(true);
    }
    
    public boolean shouldSelectCell(EventObject eventobject)
    {
        return true;
    }
    
    public Component getTableCellEditorComponent(JTable table, Object obj, boolean isSelected, int row, int column)
    {
        m_dashTable = table;
        m_selectedRow = row;
        m_selectedColumn = column;
        
        Object selectedObject = "";
        
        if (obj != null & !obj.toString().trim().isEmpty())
        {
            selectedObject = obj;
        }
        
        setRequiredValue((String)selectedObject);
        
        m_theComboBox.setSelectedItem(selectedObject);    
        
        return m_theComboBox;
    }
    
    private void setRequiredValue(String value)
    {
        if(value.compareTo("") == 0)
        {
            setValue(false, false, false);            
        }
        else if(value.compareTo("N/A") == 0)
        {
            setValue(false, false, true);            
        }
        else if(value.compareTo("Lot Controlled & Serialized") == 0 )
        {
            setValue(true, true, false);
        }
        else if(value.compareTo("Serialized") == 0 )
        {
            setValue(false, true, false);
        }
        else if(value.compareTo("Lot Controlled") == 0)
        {
            setValue(true, false, false);
        }
    }
        
    private void setValue(boolean isLotControlled, boolean isSerialized, boolean isQARequired)
    {
        m_dashTable.setValueAt(isLotControlled, m_selectedRow, m_selectedColumn+1);
        m_dashTable.setValueAt(isSerialized, m_selectedRow, m_selectedColumn+2);
        m_dashTable.setValueAt(isQARequired, m_selectedRow, m_selectedColumn+3);
    }
}