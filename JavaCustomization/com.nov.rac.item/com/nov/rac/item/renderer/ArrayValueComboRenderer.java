package com.nov.rac.item.renderer;

import java.awt.Component;

import javax.swing.JTable;

public class ArrayValueComboRenderer extends ComboBoxCellRenderer {

	public ArrayValueComboRenderer(String theLOVName) 
	{
		super(theLOVName);
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        
		String strValue = "";
        
		if(value instanceof String[])
        {
            String[] strArray = (String[]) value;
            value = (Object)strArray[0];
        }
        
        if (value != null & !value.toString().trim().isEmpty())
        {
            strValue = value.toString();
        }
        
        m_theUIBox.setSelectedItem(strValue);
        
		return m_theUIBox;
    }

	
}
