package com.nov.rac.item.renderer;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

public class ManufacturerNameCellRenderer implements TableCellRenderer
{
    
    private JTextField m_theTextField;
    
    public ManufacturerNameCellRenderer()
    {
        m_theTextField = new JTextField();
        m_theTextField.setBorder(BorderFactory.createEmptyBorder());
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
            int column)
    {

        
        String strValue = "";
        
        if(value instanceof String[])
        {
            String[] strArray = (String[]) value;
            value = (Object)strArray[0];
        }
        
        if (value != null & !value.toString().trim().isEmpty())
        {
            strValue = value.toString();
        }
        
        m_theTextField.setText(strValue);
        
        return m_theTextField;
    }
    
}
