package com.nov.rac.item.renderer;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellEditor;

public class TextAreaCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private static final long serialVersionUID = 1L;
    private JTextArea m_textArea = null;
    protected JScrollPane scrollpane = null;

    public TextAreaCellEditor()
    {
        
        m_textArea = new JTextArea();
/*        m_textArea.setColumns(70);
        m_textArea.setRows(3);*/
        m_textArea.setWrapStyleWord(true);
        m_textArea.setLineWrap(true);
        
        scrollpane = new JScrollPane(m_textArea);
        scrollpane.setBorder(BorderFactory.createEmptyBorder());
        scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        m_textArea.setText(value.toString().trim());
        System.out.println(scrollpane.getHeight());
        int rowHeight = table.getRowHeight(row);
        int thisHeight = m_textArea.getPreferredSize().height * 3;
        if (thisHeight > rowHeight)
            table.setRowHeight(row, thisHeight);
  
        return scrollpane;
    }

    public Object getCellEditorValue()
    {
        return m_textArea.getText();
    }

}
