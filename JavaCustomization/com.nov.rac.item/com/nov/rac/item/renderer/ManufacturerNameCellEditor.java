package com.nov.rac.item.renderer;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;

public class ManufacturerNameCellEditor extends DefaultCellEditor
{

    private static final long serialVersionUID = 1L;

    protected JTextField m_theTextField;
    
    public ManufacturerNameCellEditor()
    {
        super(new JTextField());
        
        m_theTextField = (JTextField) getComponent();
    }
    
    public Component getTableCellEditorComponent(JTable jtable, Object value, boolean flag, int i, int j)
    {
        String strValue = "";
        
        if(value instanceof String[])
        {
            String[] strArray = (String[]) value;
            value = (Object)strArray[0];
        }
        
        if (value != null & !value.toString().trim().isEmpty())
        {
            strValue = value.toString();
        }
        
        m_theTextField.setText(strValue);
        
        return m_theTextField;
    }
    
    public Object getCellEditorValue()
    {
        String value = (String) m_theTextField.getText();
        
        return new String[]{value};
    }
}
