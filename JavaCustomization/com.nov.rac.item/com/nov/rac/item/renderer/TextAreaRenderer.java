package com.nov.rac.item.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellRenderer;

public class TextAreaRenderer implements TableCellRenderer
{

    private static final long serialVersionUID = 1L;
    
    private JTextArea m_textArea;
    
    public TextAreaRenderer()
    {
        m_textArea = new JTextArea();
        m_textArea.setLineWrap(true);
        m_textArea.setWrapStyleWord(true);       
    }
        
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column)
    {
        m_textArea.setText((String) value);
        
        if (table.getRowHeight(row) != m_textArea.getPreferredSize().height)
        {         
            table.setRowHeight(row, 20);
        }
        if (isSelected)
            
        {
            m_textArea.setBackground(table.getSelectionBackground());
            m_textArea.setForeground(table.getSelectionForeground());
        }
        else
        {    
            m_textArea.setBackground(row % 2 != 1 ? table.getBackground() : (new Color(191, 214, 248)));
            m_textArea.setForeground(new Color(0, 0, 0));
        }
        return m_textArea;
    }
}
