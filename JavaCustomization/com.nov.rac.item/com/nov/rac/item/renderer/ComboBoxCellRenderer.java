package com.nov.rac.item.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.ManufacturerHelper;
import com.nov.rac.item.helpers.RSOneUOMLOVHelper;
import com.teamcenter.rac.util.ColorUtilities;
import com.teamcenter.rac.util.Registry;

public class ComboBoxCellRenderer implements TableCellRenderer
{
    protected JComboBox<String> m_theUIBox;
    
    private String m_lovName;
    
    protected Color alternateBackground;
    protected static final Border noFocusBorder = BorderFactory.createEmptyBorder(1, 2, 1, 2);
    
    public ComboBoxCellRenderer(String theLOVName)
    {
        alternateBackground = ColorUtilities.lighten(SystemColor.activeCaption, 0.45000000000000001D);
        alternateBackground = ColorUtilities.contrast(alternateBackground, -0.5D);
        alternateBackground = Color.YELLOW;
        
        m_lovName = theLOVName;
        
        m_theUIBox = new JComboBox<String>();
        
        // Load the LOV
        m_theUIBox.setOpaque(true);
        
        Vector<String> lovValues = loadLOV();
        
        // Populate the combobox with values.
        populateComponent(lovValues);
        
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        
        String strValue = "";
        
        if (value != null & !value.toString().trim().isEmpty())
        {
        	strValue = (String) value;
        }

        
        m_theUIBox.setSelectedItem(strValue);
        
        return m_theUIBox;
    }
    
    protected Vector<String> loadLOV()
    {
        Vector<String> vrsOneOrg = null;
        
        if (m_lovName.equalsIgnoreCase(NationalOilwell.RSOne_ORG_LOV))
        {
            vrsOneOrg = RSOneUOMLOVHelper.getRSOneOrgLOVValues();
        }
        else if (m_lovName.equalsIgnoreCase(NationalOilwell.UOMLOV))
        {
            vrsOneOrg = RSOneUOMLOVHelper.getUOMLOVValues();
            vrsOneOrg.add(0, "");
        }
        else if (m_lovName.equalsIgnoreCase(NationalOilwell.MFGNAMES))
        {
            try
            {
                vrsOneOrg = ManufacturerHelper.getActiveManifacturersVector();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            vrsOneOrg.add(0, "");
        }
        
        return vrsOneOrg;
    }
    
    protected void populateComponent(Vector<String> lovValues)
    {
        m_theUIBox.setModel(new DefaultComboBoxModel<String>(lovValues));
        m_theUIBox.setVisible(true);
    }
    
}
