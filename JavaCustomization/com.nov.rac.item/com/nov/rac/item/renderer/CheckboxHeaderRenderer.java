package com.nov.rac.item.renderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.panels.DashInfoModifyTable;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.UIHelper;

public class CheckboxHeaderRenderer implements TableCellRenderer, MouseListener,IPublisher
{
    private static final long serialVersionUID = 1L;
    private JCheckBox m_checkBox;
    private boolean m_mousePressed = false;
    private int m_column = -1;
    
    public CheckboxHeaderRenderer(String name)
    {
        m_checkBox = new JCheckBox();
        m_checkBox.setText(name);       
        m_checkBox.setBorderPainted(true);        
    }
    
    public void addActionListener(ActionListener actionListener)
    {
        m_checkBox.addActionListener(actionListener);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column)
    {
        JTableHeader header = table.getTableHeader();
        header.addMouseListener(this);
        m_column = column;
        
        return m_checkBox;
    }
    
    protected void handleClickEvent(MouseEvent mouseevent)
    {
        if (m_mousePressed)
        {
            m_mousePressed = false;
            JTableHeader header = (JTableHeader) (mouseevent.getSource());
            JTable tableView = header.getTable();
            TableColumnModel columnModel = tableView.getColumnModel();
            
            int viewColumn = columnModel.getColumnIndexAtX(mouseevent.getX());
            
            int column = tableView.convertColumnIndexToModel(viewColumn);                      
            
            if (viewColumn == this.m_column & mouseevent.getClickCount() == 1 & column != -1 & column != 0)
            {
                //DashTable_TCTable.setColumn(column);
                setColumn(column);
                m_checkBox.doClick();
            }
        }
    }
    
    private void setColumn(int l_columnIndex)
    {
        final IController controller = ControllerFactory.getInstance().getDefaultController();
        final PublishEvent event = new PublishEvent( getPublisher(), NationalOilwell.DASH_TABLE_SET_COLUMN_EVENT, l_columnIndex, null);
        controller.publish(event);        
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }

    @Override
    public void mouseClicked(MouseEvent mouseevent)
    {
        handleClickEvent(mouseevent);
        ((JTableHeader) mouseevent.getSource()).repaint();
    }
    
    @Override
    public void mouseEntered(MouseEvent mouseevent)
    {
    }
    
    @Override
    public void mouseExited(MouseEvent mouseevent)
    {
    }
    
    @Override
    public void mousePressed(MouseEvent mouseevent)
    {
        m_mousePressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent arg0)
    {
       
    }

    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    
}
