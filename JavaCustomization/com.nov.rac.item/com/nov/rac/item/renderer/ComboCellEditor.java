package com.nov.rac.item.renderer;

import java.awt.Component;

import java.util.EventObject;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import com.nov.NationalOilwell;
import com.nov.rac.item.helpers.ManufacturerHelper;
import com.nov.rac.item.helpers.RSOneUOMLOVHelper;

public class ComboCellEditor extends DefaultCellEditor
{
    
    private static final long serialVersionUID = 1L;
    
    protected JComboBox<String> m_theComboBox;
    private String m_lovName;
    
    public ComboCellEditor(String theLOVName)
    {
        super(new JComboBox<Object>());
        
        m_lovName = theLOVName;
        
        m_theComboBox = (JComboBox<String>) getComponent();
        
        // Load the LOV
        Vector<String> lovValues = loadLOV();
        
        // Populate the combobox with values.
        populateComponent(lovValues);
    }
    
    protected Vector<String> loadLOV()
    {
        Vector<String> vrsOneOrg = null;
        
        if (m_lovName != null & m_lovName.equalsIgnoreCase(NationalOilwell.RSOne_ORG_LOV))
        {
            vrsOneOrg = RSOneUOMLOVHelper.getRSOneOrgLOVValues();
        }
        else if (m_lovName != null & m_lovName.equalsIgnoreCase(NationalOilwell.UOMLOV))
        {
            vrsOneOrg = RSOneUOMLOVHelper.getUOMLOVValues();
            vrsOneOrg.add(0, "");
        }
        else if (m_lovName.equalsIgnoreCase(NationalOilwell.MFGNAMES))
        {
            try
            {
                vrsOneOrg = ManufacturerHelper.getActiveManifacturersVector();
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            vrsOneOrg.add(0, "");
        }
        
        return vrsOneOrg;
    }
    
    protected void populateComponent(Vector<String> lovValues)
    {
        m_theComboBox.setModel(new DefaultComboBoxModel<String>(lovValues));
        m_theComboBox.setVisible(true);
    }
    
    public boolean shouldSelectCell(EventObject eventobject)
    {
        return false;
    }
    
    public Component getTableCellEditorComponent(JTable jtable, Object obj, boolean flag, int i, int j)
    {
        
        Object selectedObject = null;
        
        if (obj != null & !obj.toString().trim().isEmpty())
        {
            selectedObject = obj;
        }
        
        m_theComboBox.setSelectedItem(selectedObject);
        
        return m_theComboBox;
    }
}