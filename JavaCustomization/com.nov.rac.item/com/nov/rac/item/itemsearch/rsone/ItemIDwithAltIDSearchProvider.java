package com.nov.rac.item.itemsearch.rsone;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.item.itemsearch.IBindVariableProvider;
import com.nov.rac.propertymap.IPropertyMap;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ItemIDwithAltIDSearchProvider implements INOVSearchProvider2
{
    private String m_itemId = null;
    private String m_objectName = null;
    
    private Registry m_registry;
    
    private IBindVariableProvider m_bindVarProvider;
    
    public ItemIDwithAltIDSearchProvider(IBindVariableProvider bindVarProvider)
    {
        // TODO Auto-generated constructor stub
        m_bindVarProvider = bindVarProvider;
        m_registry = getRegistry();
    }
    
    Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.itemsearch.itemsearch");
    }
    
    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return "Item";
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        // TODO Auto-generated method stub
        if (m_itemId == null || m_itemId.isEmpty())
        {
            m_itemId = "*";
        }
        
        if (m_objectName == null || m_objectName.isEmpty())
        {
            m_objectName = "*";
        }
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[3];
        
        // Item.item_id
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_string;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { m_itemId };
        
        // Item.object_name
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { m_objectName };

        // Item.object_name
        allBindVars[2] = new QuickSearchService.QuickBindVariable();
        allBindVars[2].nVarType = POM_string;
        allBindVars[2].nVarSize = 1;
        allBindVars[2].strList = new String[] { "RSOne" };
        
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        // TODO Auto-generated method stub
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-item-with-alternate-ids";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2,3 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5, 6 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 3, 4, 5, 6 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-nov4part-master-props";
        allHandlerInfo[1].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 7 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 7 };
        
        return allHandlerInfo;
    }
    
    @Override
    public boolean validateInput()
    {
        // TODO Auto-generated method stub
        boolean retValue = true;
        
        IPropertyMap bindVariablesMap = m_bindVarProvider.readBindVariables();
        
        m_itemId = bindVariablesMap.getString(NationalOilwell.ITEM_ID);
        m_objectName = bindVariablesMap.getString(NationalOilwell.OBJECT_NAME);
        
        m_itemId = m_itemId.trim();
        m_objectName = m_objectName.trim();
        
        if (m_itemId.isEmpty() && m_objectName.isEmpty())
        {
            MessageBox.post(m_registry.getString("ItemSearch.NO_INPUT_CRITERIA_FOUND"),
                    m_registry.getString("ItemSearch.TITLE"), 1);
            
            retValue = false;
        }
        
        return retValue;
    }
}
