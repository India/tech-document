package com.nov.rac.item.custom.layout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Layout;

public class HorizontalContainerTwoPanelLayout implements ILayoutProvider
{
    public HorizontalContainerTwoPanelLayout()
    {
        
    }
    
    @Override
    public Object getLayoutData()
    {
        return new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
    }
    
    @Override
    public Layout getLayout()
    {
        return new GridLayout(2, false);
    };
}
