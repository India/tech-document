package com.nov.rac.item.custom.layout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Layout;
//Write description
public class HorizontalContainerOnePanelLayout implements ILayoutProvider
{
    public HorizontalContainerOnePanelLayout()
    {
        
    }
    
    @Override
    public Object getLayoutData()
    {
        return new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
    }
    
    @Override
    public Layout getLayout()
    {
        return new GridLayout(1, true);
    };
}
