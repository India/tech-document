package com.nov.rac.item.custom.layout;

import org.eclipse.swt.widgets.Layout;

public interface ILayoutProvider
{
    public Object getLayoutData();
    
    public Layout getLayout();
}
