package com.nov.rac.item.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.LegacyItemCreateHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.propertymap.IPropertyMap;

public class DualItemOperationButtonBar extends OperationButtonBar implements ISubscriber, IPublisher
{
    
    public DualItemOperationButtonBar(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite dialogButtonBar)
    {
        if (LegacyItemCreateHelper.isDualItemCreationGroup())
        {
            m_OKButton = createButton(dialogButtonBar, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
            m_OKButton.setVisible(false);
            
            registerSubscriber();
            
            addListenerOnOKButton();
        }
        
        super.createButtonsForButtonBar(dialogButtonBar);
    }
    
    private void addListenerOnOKButton()
    {
        m_OKButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                m_parent.getChildren()[0].dispose();
                
                String operationName = "CreateNonRSOnePart";
                
                NewItemCreationHelper.setSelectedOperation(operationName);
                
                IController controller = ControllerFactory.getInstance().getDefaultController();
                
                PublishEvent event = new PublishEvent(getPublisher(), NationalOilwell.OPERATION, operationName, "");
                
                controller.publish(event);
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionEvent)
            {
                
            }
        });
    }
    
    public IPropertyMap getData()
    {
        return null;
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(NationalOilwell.MSG_CREATE_NON_RSONE_ITEM_EVENT))
        {
            m_OKButton.setVisible(true);
        }
        
        if (event.getPropertyName().equals(NationalOilwell.MSG_CREATE_RSONE_ITEM_EVENT))
        {
            m_OKButton.setVisible(false);
        }
    }
    
    protected void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.registerSubscriber(NationalOilwell.MSG_CREATE_NON_RSONE_ITEM_EVENT, this);
        controller.registerSubscriber(NationalOilwell.MSG_CREATE_RSONE_ITEM_EVENT, this);
    }
    
    protected void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        
        controller.unregisterSubscriber(NationalOilwell.MSG_CREATE_NON_RSONE_ITEM_EVENT, this);
        controller.unregisterSubscriber(NationalOilwell.MSG_CREATE_RSONE_ITEM_EVENT, this);
    }
    
    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        super.dispose();
    }
    
    private Button m_OKButton;
    
}
