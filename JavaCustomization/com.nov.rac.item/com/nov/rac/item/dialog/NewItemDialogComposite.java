package com.nov.rac.item.dialog;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.operations.ILoadDelegate;
import com.nov.rac.item.operations.OperationHelper;
import com.nov.rac.item.operations.legacy.LegacyOperationHelper;
import com.nov.rac.item.panels.factory.PanelFactory;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
 
public class NewItemDialogComposite extends Composite implements ISubscriber, IOperationInputProvider
{
    public NewItemDialogComposite(Composite parent, int style)
    {
        super(parent, style);
        registerSubscriber();
        createUI();
        loadUIData();
        getRegistry();
    }
    
    private void loadUIData()
    {
        TCComponent targetItem = NewItemCreationHelper.getTargetComponent();
        if (targetItem != null)
        {
            loadComponent(targetItem);
        }
    }
    
    private void createUI()
    {
        Composite newItemDialogComposite = this;
        
        // set layout
        GridLayout gridLayout = new GridLayout(1, true);
        
        newItemDialogComposite.setLayout(gridLayout);
        newItemDialogComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        
        /*
         * RowLayout rowLayout = new RowLayout(); rowLayout.center = true;
         * rowLayout.fill = true; rowLayout.justify = true; rowLayout.type =
         * SWT.VERTICAL; newItemDialogComposite.setLayout(rowLayout);
         */
        
        // create default panels using factory
        theFactory = PanelFactory.getInstance();
        theFactory.setParentComposite(newItemDialogComposite);
      
        // Layout Framework
        
        m_defaultContainerPanelNames = NewItemCreationHelper.getConfiguration("DEFAULT_LAYOUT_CONTAINERS");
        if(m_defaultContainerPanelNames!=null)
        {
        	m_defaultContainerPanels = theFactory.createPanels(this, m_defaultContainerPanelNames, true);
        	
        	for (int inx = 0 ; inx<m_defaultContainerPanels.size();inx++)
        	{
        		
        		m_defaultPanels.addAll(theFactory.createPanels((Composite) m_defaultContainerPanels.get(inx).getUIPanel()/* this */,
        				m_defaultContainerPanelNames[inx]+ ".DEFAULT_PANELS", false));
        	}
        }
        // Layout Framework
        else
        {
        	m_defaultPanels = theFactory.createPanels( this ,"DEFAULT_PANELS", false);
        }
        
        
        m_buttonBar = theFactory.createButtonBar(newItemDialogComposite, "DEFAULT.BUTTONBAR");
        m_buttonBar.registerOperationInputProvider(getOperationInputProvider());
        
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        // using hashcode() as toString() is overridden
        // IController theController =
        // ControllerFactory.getInstance().getController(""+getShell().hashCode());
        theController.registerSubscriber(NationalOilwell.OPERATION, this);
        theController.registerSubscriber(NationalOilwell.DIALOG_RELOAD, this);
        theController.registerSubscriber(NationalOilwell.SELECTED_ITEM, m_LoadComponent);
        theController.registerSubscriber(NationalOilwell.UPDATE_DIALOG_AREA, this);
        
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String propertyName = event.getPropertyName();
        if (propertyName.equals(NationalOilwell.OPERATION))
        {
            String panelType = (String) event.getNewValue();
            System.out.println("::::::::::" + panelType + ":::::::::");
            if (m_buttonBar != null)
            {
                m_buttonBar.dispose();
            }
            removeAllPanels();
            updateArea();
            
            if (m_defaultContainerPanelNames!=null)
            {
            	addContainerPanels(panelType);
            }
            else
            {	
            	addPanels(panelType);
            }
            // Layout Framework Start
            
            // Layout Framework End
            
            updateButtonBar(panelType);
            updateArea();
            if (!isLargerSize)
                this.getShell().pack();
            this.getShell().setLocation(getInitialLocation());
            TCComponent l_targetItem = NewItemCreationHelper.getTargetComponent();
            if (l_targetItem != null)
            {
                loadComponent(l_targetItem);
            }
        }
        else if (propertyName.equals(NationalOilwell.DIALOG_RELOAD))
        {
            
            String panelType = "";
            IPropertyMap propertyMap = null;
            
            DialogReloadHelper dialogHelper = (DialogReloadHelper) event.getNewValue();
            if (dialogHelper.getPropertyMap() != null)
            {
                propertyMap = dialogHelper.getPropertyMap();
            }
            
            if (dialogHelper.getpropertyValue() != null)
            {
                panelType = dialogHelper.getpropertyValue();
            }
            
            if (m_buttonBar != null)
            {
                m_buttonBar.dispose();
            }
            removeAllPanels();
            updateArea();
            
            //Tushar commented and added
            //addPanels(panelType);
            if (m_defaultContainerPanelNames!=null)
            {
                addContainerPanels(panelType);
            }
            else
            {   
                addPanels(panelType);
            }
            
            updateButtonBar(panelType);
            updateArea();
            if (!isLargerSize)
                this.getShell().pack();
            this.getShell().setLocation(getInitialLocation());
            
            if (propertyMap != null)
            {
                loadComponent(propertyMap);
            }
            
        }
        
        else if (propertyName.equals(NationalOilwell.UPDATE_DIALOG_AREA))
        {
            
            updateArea();
            if (!isLargerSize)
                this.getShell().pack();
            this.getShell().setLocation(getInitialLocation());
        }
    }
    
    // Layout Framework Start
    private void addContainerPanels(String panelType)
    {
        m_allContainerPanels = new ArrayList<IUIPanel>();
        String[] layoutContainerPanelNames = NewItemCreationHelper.getConfiguration(panelType + ".LAYOUT_CONTAINERS");    
       
        String[] allContainerPanelNames = new String[layoutContainerPanelNames.length + m_defaultContainerPanelNames.length]; 
        allContainerPanelNames[0] = m_defaultContainerPanelNames[0];
        
        for (int i = 0; i < layoutContainerPanelNames.length; i++)
        {
            allContainerPanelNames[i + 1] = layoutContainerPanelNames[i];
        }
        
        PanelFactory theFactory = PanelFactory.getInstance();
        m_allContainerPanels.addAll(m_defaultContainerPanels);
        m_layoutContainerPanels = theFactory.createPanels(this, layoutContainerPanelNames, true);
        m_allContainerPanels.addAll(m_layoutContainerPanels);
        
        addActualPanels(allContainerPanelNames, m_allContainerPanels);
    }
    
    private void addActualPanels(String[] containerPanelNames, ArrayList<IUIPanel> containerPanels)
    {
        m_actualPanels = new ArrayList<IUIPanel>();
        for (int i = 0; i < containerPanelNames.length; i++)
        {
        	
            String[] actualPanelNames = NewItemCreationHelper.getConfiguration( NewItemCreationHelper.getSelectedOperation() + "."
            							+ containerPanelNames[i] + ".PANELS_LIST");
            PanelFactory theFactory = PanelFactory.getInstance();
            
            if(actualPanelNames!=null)
            {
            	
            	m_actualInternalPanels = theFactory.createPanels((Composite) containerPanels.get(i).getUIPanel(),
            							actualPanelNames, false);
            	m_actualPanels.addAll(m_actualInternalPanels);
            }
        }
        
    }
    
    // Layout Framework End
    
    private void updateArea()
    {
        ScrolledComposite com = (ScrolledComposite) this.getParent().getParent();
        com.setMinSize(this.getParent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
        this.getShell().setMinimumSize(computeBounds());
    }
    
    private void removeAllPanels()
    {
        if (m_actualPanels != null)
        {
            for (int i = 2; i < this.getChildren().length; i++)
            {
                if (this.getChildren()[i] instanceof Label)
                {
                    this.getChildren()[i].dispose();
                }
            }
            for (IUIPanel panels : m_actualPanels)
            {
                panels.dispose();
                // Object panelObject = panels.getUIPanel();
                // if (panelObject instanceof Composite)
                // {
                // Composite panelComposite = (Composite) panelObject;
                // panelComposite.dispose();
                //
                // }
            }
            
            // Layout Framework Start
            for (IUIPanel panels : m_layoutContainerPanels)
            {
                panels.dispose();
            }
            // Layout Framework End
        }
    }
    
    private Point getInitialLocation()
    {
        // Rectangle shellBounds = getShell().getParent().getBounds();
        Monitor theMonitor = getMonitor();
        Rectangle screenSize = theMonitor.getClientArea();
        Point dialogSize = this.getShell().getSize();
        
        int x = screenSize.x + (screenSize.width - dialogSize.x) / 2;
        int y = screenSize.y + (screenSize.height - dialogSize.y) / 2;
        Point location = new Point(x, y);
        
        return location;
    }
    
    private Point computeBounds()
    {
        // Point scrollbarSize = new Point(16,16);
        Point dialogSize = this.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        Point requiredDialogSize = dialogSize;
        Monitor theMonitor = getMonitor();
        Rectangle screenSize = theMonitor.getClientArea();
        if (dialogSize.x < screenSize.width && dialogSize.y < screenSize.height)
            this.getShell().pack();
        if (dialogSize.x > screenSize.width)
        {
            requiredDialogSize.x = (int) ((screenSize.width) * (0.9));
            isLargerSize = true;
        }
        if (dialogSize.y > screenSize.height)
        {
            requiredDialogSize.y = (int) ((screenSize.height) * (0.9));
            isLargerSize = true;
        }
        
        return requiredDialogSize;
    }
    
    private void addPanels(String panelType)
    {
        m_actualPanels = null;
        
        String[] panelNames = NewItemCreationHelper.getConfiguration(panelType + "." + "PANELS_LIST");
        
        PanelFactory theFactory = PanelFactory.getInstance();
        
        m_actualPanels = theFactory.createPanels(this, panelNames, true);
    }
    
    private void updateButtonBar(String panelType)
    {
        // panelType = "CreateNewPart";
        m_buttonBar = null;
        PanelFactory theFactory = PanelFactory.getInstance();
        
        m_buttonBar = theFactory.createButtonBar(this, panelType + ".BUTTONBAR");
        m_buttonBar.registerOperationInputProvider(getOperationInputProvider());
    }
    
    @Override
    public ArrayList<IUIPanel> getOperationInputProvider()
    {
        ArrayList<IUIPanel> l_panelList = m_actualPanels;
        
        if (l_panelList == null)
        {
            l_panelList = m_defaultPanels;
        }
        
        return l_panelList;
    }
    
    private void loadComponent(final Object targetItem)
    {
        
        final ILoadDelegate l_loadDelegate = OperationHelper
                .getLoadDelegate(getRegistry(), getOperationInputProvider());
        
        if (l_loadDelegate != null)
        {
            Display.getDefault().asyncExec(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        l_loadDelegate.loadComponent(targetItem);
                    }
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
        
    }
    
    private Registry getRegistry()
    {
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        
        return m_registry;
    }
    
    private class IC_LoadComponent implements ISubscriber
    {
        public IC_LoadComponent()
        {
        }
        
        @Override
        public void update(PublishEvent event)
        {
            TCComponent[] tcComponents = (TCComponent[]) event.getNewValue();
            /*
             * NewItemCreationHelper.setSelectedComponent((TCComponent) event
             * .getNewValue()); loadComponent((TCComponent)
             * event.getNewValue());
             */
            
            NewItemCreationHelper.setSelectedComponent(tcComponents[0]);
            loadComponent(tcComponents[0]);
        }
    }
    
    @Override
    public void dispose()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(NationalOilwell.OPERATION, this);
        theController.unregisterSubscriber(NationalOilwell.DIALOG_RELOAD, this);
        theController.unregisterSubscriber(NationalOilwell.SELECTED_ITEM, m_LoadComponent);
        theController.unregisterSubscriber(NationalOilwell.UPDATE_DIALOG_AREA, this);
        disposeChildPanels();
        disposeParentPanels();
        disposeButtonBar();
        super.dispose();
    }
    
    private void disposeChildPanels()
    {
        if (m_actualPanels != null)
        {
            for (IUIPanel actualPanel : m_actualPanels)
            {
                actualPanel.dispose();
            }
        }
    }
    
    private void disposeParentPanels()
    {
        if (m_defaultPanels != null)
        {
            for (IUIPanel defaultPanel : m_defaultPanels)
            {
                defaultPanel.dispose();
            }
        }
    }
    
    private void disposeButtonBar()
    {
        if (m_buttonBar != null)
        {
            m_buttonBar.dispose();
        }
    }
    
    // Layout Framework Start
    private String[] m_defaultContainerPanelNames = null;
    private ArrayList<IUIPanel> m_defaultContainerPanels;
    ArrayList<IUIPanel> m_allContainerPanels = null;
    ArrayList<IUIPanel> m_actualInternalPanels = null;
    ArrayList<IUIPanel> m_layoutContainerPanels = null;
    // Layout Framework End
    
    private Registry m_registry = null;
    private ArrayList<IUIPanel> m_defaultPanels = new ArrayList<IUIPanel>();
    private ArrayList<IUIPanel> m_actualPanels = null;
    protected AbstractDialogButtonBar m_buttonBar = null;
    private boolean isLargerSize = false;
    private IC_LoadComponent m_LoadComponent = new IC_LoadComponent();
    private PanelFactory theFactory;
}
