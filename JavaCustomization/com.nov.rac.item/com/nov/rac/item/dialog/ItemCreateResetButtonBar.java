package com.nov.rac.item.dialog;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.operations.IOperationDelegate;
import com.nov.rac.item.operations.OperationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.NOVBrowser;
import com.teamcenter.rac.util.Registry;

public class ItemCreateResetButtonBar extends AbstractDialogButtonBar implements IPublisher
{

	public ItemCreateResetButtonBar(Composite parent, int style) {
		super(parent, style);
	    m_parent = parent;
        addListeners();
	}


	@Override
	protected void createButtonsForButtonBar(Composite dialogButtonBar) 
		{
			m_okButton = createButton(dialogButtonBar, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	        m_applyButton = createButton(dialogButtonBar, 22, m_registry.getString("APPLY_LABEL"), false);
	        m_cancelButton = createButton(dialogButtonBar, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	        m_resetbButton = createButton(dialogButtonBar, 24, m_registry.getString("RESET_LABEL"), false);
	        m_helpButton = createButton(dialogButtonBar, 23, m_registry.getString("HELP_LABEL"), false);
		}

	private void addListeners()
		{
			Object[] oprParams = new Object[1];
	        oprParams[0] = m_parentComposite.getShell();
	        m_operationDelegate = OperationHelper.getSaveDelegate(m_registry, oprParams);
	        
	        addOkButtonListener();
	        
	        addApplyButtonListener();
	        
	        addCancelButtonListener();
	        
	        addHelpButtonListener();
	        
	        addResetButtonListener();
			
		}

	
	private void addResetButtonListener()
			{
			
				m_resetbButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) {
				
					//m_resetoperationDelegate.executeOperation();
					
					for (IUIPanel panel: m_inputProviders)
					{
						panel.reset();
					}
					
			 }
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{	
			}
		});
		
	}


	private void addHelpButtonListener() 
	
	{
		 m_helpButton.addSelectionListener(new SelectionListener()
	        {
	            
	            @Override
	            public void widgetDefaultSelected(SelectionEvent arg0)
	            {
	                m_helpOperationDelegate.executeOperation();
	            }
	            
	            @Override
	            public void widgetSelected(SelectionEvent event)
	            {
	                String url = readURL();
	                if (null != url)
	                {
	                    try
	                    {
	                        NOVBrowser.open(url);
	                    }
	                    catch (Exception e)
	                    {
	                       com.teamcenter.rac.util.MessageBox.post(e);
	                    }
	                }
	            }
	            
	        });
	}


	


	private void addCancelButtonListener() {
		 m_cancelButton.addSelectionListener(new SelectionListener()
	        {
	            
	            @Override
	            public void widgetSelected(SelectionEvent arg0)
	            {
	                //boolean cancel = MessageDialog.openQuestion( AIFUtility.getActiveDesktop().getShell(),
	                 //       IDialogConstants.CANCEL_LABEL, m_registry.getString("cancel.MSG") ); 
	                //if(cancel)
	               // {
	                    m_parent.getShell().dispose();
	               // }
	            }
	            
	            @Override
	            public void widgetDefaultSelected(SelectionEvent event)
	            {
	                
	            }
	        });
		
	}


	private void addApplyButtonListener() 
	{
		  m_applyButton.addSelectionListener(new SelectionListener()
	        {
	            
	            @Override
	            public void widgetSelected(SelectionEvent arg0)
	            {
	                setEnable(false);
	                boolean success = m_applyOperationDelegate.executeOperation();
	                if(success)
	                {
	                    ControllerFactory
	                    .getInstance()
	                    .getDefaultController()
	                    .publish(getPublishEvent(NationalOilwell.ONAPPLY_OBJECT_CREATED, true,null));
	                }
	                setEnable(true);
	            }
	            
	            @Override
	            public void widgetDefaultSelected(SelectionEvent event)
	            {
	                
	            }
	        });
	        m_applyOperationDelegate = m_operationDelegate;
	}


	


	private void addOkButtonListener()
	{
		 m_okButton.addSelectionListener(new SelectionListener()
	        {
	            @Override
	            public void widgetDefaultSelected(SelectionEvent arg0)
	            {
	                
	            }
	            
	            @Override
	            public void widgetSelected(SelectionEvent arg0)
	            {
	               boolean success = m_okOperationDelegate.executeOperation();
	               if(success)
	               {
	                   m_parent.getShell().dispose();
	               }
	            }
	        });
	        m_okOperationDelegate = m_operationDelegate;
	}


	@Override
	public void registerOperationInputProvider(List<IUIPanel> inputProviders)
	{
		 m_inputProviders=inputProviders;
		 m_okOperationDelegate.registerOperationInputProvider(inputProviders);
		// m_resetoperationDelegate.registerOperationInputProvider(inputProviders);
	}
	
	protected void setEnable(boolean enable) 
	{
		m_okButton.setEnabled(enable);
	    m_cancelButton.setEnabled(enable);
	    m_applyButton.setEnabled(enable);
	}

	protected PublishEvent getPublishEvent(String property,Object newValue,Object oldValue) {

        PublishEvent event = new PublishEvent(getPublisher(), property, newValue, oldValue);
        return event;
	}

	IPublisher getPublisher()
	    {
	        return this;
	    }

	protected String readURL()
	{
			Registry registry = getRegistry();
	        String url = registry.getString("HelpIcon.HelpURL");
	        return url;
	}
	
    protected Registry getRegistry()
    {
        return Registry.getRegistry(this);
    }
    
    public IPropertyMap getData()
    {
    	return null;
    }
    
		private Composite m_parent = null;
		private List<IUIPanel> m_inputProviders;
	    protected Button m_okButton;
	    protected Button m_applyButton;
	    protected Button m_cancelButton;
	    protected Button m_helpButton;
	    protected Button m_resetbButton;
	    
	    protected IOperationDelegate m_resetoperationDelegate=null;
	    protected IOperationDelegate m_okOperationDelegate = null;
	    protected IOperationDelegate m_applyOperationDelegate = null;
	    protected IOperationDelegate m_helpOperationDelegate = null;
	    protected IOperationDelegate m_operationDelegate = null;
}
