package com.nov.rac.item.dialog.legacy;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.AbstractDialogButtonBar;
import com.nov.rac.item.operations.IOperationDelegate;
import com.nov.rac.item.operations.legacy.LegacyOperationHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.HelpButtonComposite;
import com.nov.rac.utilities.utils.NOVBrowser;
import com.teamcenter.rac.util.Registry;

public class ItemCreateButtonBar extends AbstractDialogButtonBar implements
        IPublisher, ISubscriber
{
    
    public ItemCreateButtonBar(Composite parent, int style)
    {
        super(parent, style);
        m_parent = parent;
        addListeners();
        registerSubscriber();
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite dialogButtonBar)
    {
        
        GridLayout parentLayout = (GridLayout) dialogButtonBar.getLayout();
        GridData parentLayoutData = ((GridData) dialogButtonBar.getLayoutData());
        parentLayout.numColumns++;
        parentLayout.makeColumnsEqualWidth = false;
        parentLayoutData.horizontalAlignment = SWT.FILL;
        parentLayoutData.grabExcessHorizontalSpace = true;
        
        createHelpAndFeedBack(dialogButtonBar);
        createButtons(dialogButtonBar);
        
        /*
         * m_okButton = createButton(dialogButtonBar, IDialogConstants.OK_ID,
         * IDialogConstants.OK_LABEL, true); m_okButton.setEnabled(false);
         * m_applyButton = createButton(dialogButtonBar, 22,
         * m_registry.getString("APPLY_LABEL"), false);
         * m_applyButton.setEnabled(false); m_cancelButton =
         * createButton(dialogButtonBar, IDialogConstants.CANCEL_ID,
         * IDialogConstants.CANCEL_LABEL, false); m_resetbButton =
         * createButton(dialogButtonBar, 24,
         * m_registry.getString("RESET_LABEL"), false); m_helpButton =
         * createButton(dialogButtonBar, 23, m_registry.getString("HELP_LABEL"),
         * false);
         */
    }
    
    private void createHelpAndFeedBack(Composite dialogButtonBar)
    {
        Composite l_composite = new Composite(dialogButtonBar, SWT.NONE);
        l_composite.setLayout(new GridLayout(2, true));
        GridData gd_l_Composite = new GridData(SWT.LEFT, SWT.CENTER, true,
                false, 1, 1);
        l_composite.setLayoutData(gd_l_Composite);
        
        feedBackComp = new FeedBackButtonComposite(l_composite, SWT.NONE);
        feedBackComp.setSummaryDesc(getSummaryDescValue());
        helpButtonCmp = new HelpButtonComposite(l_composite, SWT.NONE);
        
    }
    
    private void createButtons(Composite dialogButtonBar)
    {
        Composite r_composite = new Composite(dialogButtonBar, SWT.NONE);
        GridData gd_r_Composite = new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1);
        r_composite.setLayoutData(gd_r_Composite);
        r_composite.setLayout(new GridLayout());
        
        m_okButton = createButton(dialogButtonBar, IDialogConstants.OK_ID,
                IDialogConstants.OK_LABEL, true);
        m_okButton.setEnabled(false);
        m_applyButton = createButton(dialogButtonBar, 22,
                m_registry.getString("APPLY_LABEL"), false);
        m_applyButton.setEnabled(false);
        m_cancelButton = createButton(dialogButtonBar,
                IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL,
                false);
        m_resetbButton = createButton(dialogButtonBar, 24,
                m_registry.getString("RESET_LABEL"), false);
    }
    
    private void addListeners()
    {
        Object[] oprParams = new Object[1];
        oprParams[0] = m_parentComposite.getShell();
        m_operationDelegate = LegacyOperationHelper.getSaveDelegate(m_registry,
                oprParams);
        
        addOkButtonListener();
        
        addApplyButtonListener();
        
        addCancelButtonListener();
        
        addHelpButtonListener();
        
        addResetButtonListener();
        
    }
    
    private void addResetButtonListener()
    {
        
        m_resetbButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                
                // m_resetoperationDelegate.executeOperation();
                
                for (IUIPanel panel : m_inputProviders)
                {
                    panel.reset();
                }
                
                m_okButton.setEnabled(false);
                m_applyButton.setEnabled(false);
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
        
    }
    
    private SelectionListener createHelpListener()
    {
        SelectionListener listener = new SelectionListener()
        {
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                m_helpOperationDelegate.executeOperation();
            }
            
            @Override
            public void widgetSelected(SelectionEvent event)
            {
                String url = readURL();
                if (null != url)
                {
                    try
                    {
                        NOVBrowser.open(url);
                    }
                    catch (Exception e)
                    {
                        com.teamcenter.rac.util.MessageBox.post(e);
                    }
                }
            }
            
        };
        
        return listener;
        
    }
    
    private void addHelpButtonListener()
    {
        helpButtonCmp.setSlectionListener(createHelpListener());
    }
    
    /*
     * private void addHelpButtonListener() {
     * m_helpButton.addSelectionListener(new SelectionListener() {
     * @Override public void widgetDefaultSelected(SelectionEvent arg0) {
     * m_helpOperationDelegate.executeOperation(); }
     * @Override public void widgetSelected(SelectionEvent event) { String url =
     * readURL(); if (null != url) { try { NOVBrowser.open(url); } catch
     * (Exception e) { com.teamcenter.rac.util.MessageBox.post(e); } } } }); }
     */
    
    private void addCancelButtonListener()
    {
        m_cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                m_parent.getShell().dispose();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent event)
            {
                
            }
        });
        
    }
    
    private void addApplyButtonListener()
    {
        m_applyButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                setEnable(false);
                boolean success = m_applyOperationDelegate.executeOperation();
                if (success)
                {
                    ControllerFactory
                            .getInstance()
                            .getDefaultController()
                            .publish(
                                    getPublishEvent(
                                            NationalOilwell.ONAPPLY_OBJECT_CREATED,
                                            true, null));
                    
                    ControllerFactory
                            .getInstance()
                            .getDefaultController()
                            .publish(
                                    getPublishEvent(
                                            NationalOilwell.MSG_APPLY_BTN_SELECTION_EVENT,
                                            true, null));
                }
                setEnable(true);
                m_applyButton.setEnabled(true);
                m_okButton.setEnabled(true);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent event)
            {
                
            }
        });
        m_applyOperationDelegate = m_operationDelegate;
    }
    
    private void addOkButtonListener()
    {
        m_okButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                boolean success = m_okOperationDelegate.executeOperation();
                if (success)
                {
                    m_parent.getShell().dispose();
                }
            }
        });
        m_okOperationDelegate = m_operationDelegate;
    }
    
    @Override
    public void registerOperationInputProvider(List<IUIPanel> inputProviders)
    {
        m_inputProviders = inputProviders;
        m_okOperationDelegate.registerOperationInputProvider(inputProviders);
        // m_resetoperationDelegate.registerOperationInputProvider(inputProviders);
    }
    
    protected void setEnable(boolean enable)
    {
        m_okButton.setEnabled(false);
        m_cancelButton.setEnabled(enable);
        m_applyButton.setEnabled(false);
    }
    
    protected PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        // controller.registerSubscriber(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT,
        // getSubscriber());
        controller
                .registerSubscriber(
                        NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT,
                        getSubscriber());
    }
    
    @Override
    public void update(PublishEvent event)
    {
        // if
        // (event.getPropertyName().equals(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT))
        // // MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT
        if (event
                .getPropertyName()
                .equals(NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT)) // MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT
        {
            String itemType = (String) event.getNewValue();
            
            if (itemType != "" || itemType != null)
            {
                m_okButton.setEnabled(true);
                m_applyButton.setEnabled(true);
            }
        }
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    protected String readURL()
    {
        Registry registry = getRegistry();
        String url = registry.getString("HelpIcon.HelpURL");
        return url;
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry(this);
    }
    
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        super.dispose();
    }
    
    private void unRegisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        controller
                .unregisterSubscriber(
                        NationalOilwell.MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT,
                        getSubscriber());
    }
    
    private String getSummaryDescValue()
    {
        String summaryDesc = null;
        
        String currrentGroupName = GroupUtils.getCurrentGroupName();
        
        String businessGroupName = GroupUtils
                .getBusinessGroupName(currrentGroupName);
        
        if (businessGroupName != null)
        {
            summaryDesc = "Feeback for " + businessGroupName + " "
                    + "Item Creation";
        }
        else if (businessGroupName == null && currrentGroupName != null)
        {
            summaryDesc = "Feeback for " + currrentGroupName + " "
                    + "Item Creation";
        }
        
        return summaryDesc;
    }
    
    private Composite m_parent = null;
    private List<IUIPanel> m_inputProviders;
    protected Button m_okButton;
    protected Button m_applyButton;
    protected Button m_cancelButton;
    // protected Button m_helpButton;
    protected Button m_resetbButton;
    private HelpButtonComposite helpButtonCmp;
    private FeedBackButtonComposite feedBackComp;
    
    protected IOperationDelegate m_resetoperationDelegate = null;
    protected IOperationDelegate m_okOperationDelegate = null;
    protected IOperationDelegate m_applyOperationDelegate = null;
    protected IOperationDelegate m_helpOperationDelegate = null;
    protected IOperationDelegate m_operationDelegate = null;
}
