package com.nov.rac.item.dialog;

import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;

public class TextCellEditor extends DefaultCellEditor
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private JTextField textField ;
    
    public TextCellEditor(JTextField textField)
    {
        super(textField);
        this.textField = textField;
        textField.addHierarchyBoundsListener(new HierarchyBoundsAdapter()
        {
            @Override
            public void ancestorResized(HierarchyEvent hierarchyevent)
            {
                stopCellEditing();
                
            }
        });
        
    }
    
    public JTextField getTextField()
    {
        return textField;
    }
    
    
}
