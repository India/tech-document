package com.nov.rac.item.dialog;

import org.eclipse.swt.widgets.Composite;

public class ItemCreationButtonBarWithoutApply extends ItemCreationButtonBar
{

    public ItemCreationButtonBarWithoutApply(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite dialogButtonBar)
    {
        super.createButtonsForButtonBar(dialogButtonBar);
        m_applyButton.setEnabled(false);
    }
    
}
