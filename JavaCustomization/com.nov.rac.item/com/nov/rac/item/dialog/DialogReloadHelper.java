package com.nov.rac.item.dialog;

import com.nov.rac.propertymap.IPropertyMap;

public class DialogReloadHelper
{
    public IPropertyMap l_propertyMap = null;
    public String l_propertyValue = null;
    
    public IPropertyMap getPropertyMap()
    {
        return l_propertyMap;
    }
    public void setPropertyMap(IPropertyMap propertyMap)
    {
        this.l_propertyMap = propertyMap;
    }
    
    public String getpropertyValue()
    {
        return l_propertyValue;
    }
    public void setpropertyValue(String propertyValue)
    {
        this.l_propertyValue = propertyValue;
    }
}
