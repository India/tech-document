package com.nov.rac.item.dialog;

import java.util.Collections;
import java.util.Vector;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.item.helpers.RSOneDashNumberHelper;

public class RSOneDashTableComposite2 extends RSOneDashTableComposite
{

    public RSOneDashTableComposite2(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public void loadTable()
    {
        super.loadTable();
        
        Vector<String> dashNumbers = RSOneDashNumberHelper.getExistingDashNumbers();
        
        if(dashNumbers != null && !dashNumbers.isEmpty())
        {
            for(String theDashNumber : dashNumbers)
            {
                m_dashNoList.add(theDashNumber);
            }
            // SortedDashList
            Collections.sort(m_dashNoList);
        }
    }
}
