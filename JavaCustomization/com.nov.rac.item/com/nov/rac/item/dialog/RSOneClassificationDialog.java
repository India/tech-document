package com.nov.rac.item.dialog;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.TreePath;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneOperationTypePanelHelper;
import com.teamcenter.rac.classification.common.tree.G4MTreeNode;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class RSOneClassificationDialog extends NOVClassificationDialog
{

    public RSOneClassificationDialog(Shell shell, Text m_classText)
    {
        super(shell, m_classText);
        updateTypClassUsingICOPrefAndRSOneLOV();
    }
    
    @Override
    protected Control createDialogArea(Composite composite)
    {
        Control returnValue =  super.createDialogArea(composite);
        
        
        addListenerOnClassificationRoot();
        
        return returnValue;
    }
    
    @Override
	protected void okPressed() {
		TreePath path = m_classificationtree.getSelectionPath();
		G4MTreeNode node = (G4MTreeNode) m_classificationtree
				.getLastSelectedPathComponent();
		setSelectedNode(path, node);
	}
    
    private void addListenerOnClassificationRoot()
    {
        m_classificationtree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me ) {
                if ( me.getClickCount() == 2 )  {
                    TreePath path =  m_classificationtree.getSelectionPath();
                 //   Object[] objs = path.getPath();
                    G4MTreeNode node = (G4MTreeNode) m_classificationtree.getLastSelectedPathComponent();
                    filterClassificationTreeByTypeWithEvent(path,node);
                }
        }
        });
        
        m_classificationtree.addTreeExpansionListener(new TreeExpansionListener(){
            public void treeCollapsed(TreeExpansionEvent treeExpansionEvent) {
            }

            public void treeExpanded(TreeExpansionEvent treeExpansionEvent) {

              TreePath path = treeExpansionEvent.getPath();
              
              G4MTreeNode curNode = (G4MTreeNode)path.getLastPathComponent();
                  filterClassificationTreeByTypeWithEvent(path,curNode);
            }
      });
        
    }
    
    protected void setSelectedNode(TreePath path,
			G4MTreeNode node) {

		Object[] objs = path.getPath();
		String selPath = "";
		String selStr = "";
		for (int i = 1; i < objs.length; i++) {
			if (objs[i].toString().contains("[")) {
				selStr = objs[i].toString().substring(0,
						objs[i].toString().indexOf("["));
			} else {
				selStr = objs[i].toString();
			}
			if (selPath.trim().length() > 0) {
				selPath = selPath + "->" + selStr.trim();
				// selPath = selStr.trim();
			} else {
				selPath = selStr.trim();
			}

		}
		if (node != null) {
			if (node.getICSDescriptor().isStorageClass()/*
														 * descript.
														 * isStorageClass()
														 */) {
				try {

					IController theController = ControllerFactory.getInstance()
							.getDefaultController();
					theController.publish(new PublishEvent(this,
							NationalOilwell.SELECTEDCLASS, selPath, ""));
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							getShell().close();
						}
					});

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
    
    protected void filterClassificationTreeByTypeWithEvent(TreePath path, G4MTreeNode node)
    {
        
        if (node != null)
        {
            boolean treeDel = false;
            int childCnt = m_classificationtree.getModel().getChildCount(m_classificationtree.getModel().getRoot());
            for (int index = childCnt - 1; index >= 0; index--)
            {/*
                G4MTreeNode node1 = (G4MTreeNode) m_classificationtree.getModel().getChild(
                        m_classificationtree.getModel().getRoot(), index);
                if (node1.getNodeName().equalsIgnoreCase(m_registry.getString("ClassficationGroup.NAME")))
                {
                    m_compContext.getTree().setRootNode(node1, true);
                    m_compContext.getTree().repaint();
                    m_compContext.getTree().refresh(node1.getNodeName(), true, true);
                  //  m_classificationtree = m_compContext.getTree().getTree();
                    treeDel = true;
                    break;
                }*/
            }
            if (treeDel)
            {
                m_classificationtree.repaint();
                m_classificationtree.validate();
                classTreePane.removeAll();
                /*
                 * classpopup = new JDialog((java.awt.Dialog)
                 * newItemPanel.newItemDlg, true);
                 * classpopup.getRootPane().setWindowDecorationStyle
                 * (JRootPane.PLAIN_DIALOG);
                 */
                
                classTreePane = new ScrollPagePane(m_classificationtree);
                classTreePane.setSize(300, 325);
                /*
                 * classpopup.getContentPane().add(scrollPane);
                 * classpopup.setSize(300, 325); int x =
                 * classPopupButton.getLocationOnScreen().x -
                 * classpopup.getWidth(); int y =
                 * classPopupButton.getLocationOnScreen().y -
                 * classpopup.getHeight(); classpopup.setLocation(x, y);
                 * classpopup.repaint(); classpopup.validate();
                 * classpopup.setModal(true); classpopup.setVisible(true);
                 */
            }
            String strSelType = RSOneOperationTypePanelHelper.getSelectedItemType();
            Object[] objs = path.getPath();
            if (objs.length == 2)
            {
                
                if (strSelType != null && strSelType.length() == 0)
                {
                    TCComponent item = NewItemCreationHelper.getTargetComponent();
                    try
                    {
                        if (item != null)
                        {
                            TCComponentForm partForm = (TCComponentForm) item.getRelatedComponent("IMAN_master_form");
                            if (partForm != null && partForm.getFormTCProperty("rsone_itemtype") != null)
                            {
                                strSelType = partForm.getFormTCProperty("rsone_itemtype").toString();
                            }
                        }
                    }
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }
                }
                
                
                if (typClassMap != null && strSelType != null)
                {
                    if (typClassMap.containsKey(strSelType))
                    {
                        String chStr = typClassMap.get(strSelType);
                        String childStr[] = chStr.split(",");
                        int chCnt = childStr.length;
                        
                        Map<String, String> chMap = new HashMap<String, String>();
                        for (int ik = 0; ik < chCnt; ik++)
                        {
                            chMap.put(childStr[ik], childStr[ik]);
                        }
                        
                        Enumeration enum1 = node.children();
                        
                        Map chdMap = new HashMap();
                        int iObj = 0;
                        while (enum1.hasMoreElements())
                        {
                            G4MTreeNode chNodeG4M1 = (G4MTreeNode) enum1.nextElement();
                            String chNodeNameStr = chNodeG4M1.getUserObject().toString();
                            
                            if (!chMap.containsKey(chNodeNameStr))
                            {
                                chdMap.put(iObj, chNodeG4M1);
                                iObj++;
                            }
                        }
                        
                        for (int kj = 0; kj < chdMap.size(); kj++)
                        {
                            G4MTreeNode chNodeG4M1 = (G4MTreeNode) chdMap.get(kj);
                            chNodeG4M1.removeFromParent();
                            chNodeG4M1.setParent(null);
                            chNodeG4M1 = null;
                        }
                        
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run()
                            {
                                m_classificationtree.repaint();
                                m_classificationtree.validate();
                                m_classificationtree.updateUI();
                            }
                        });
                        
                    }
                }
            }
            
            setSelectedNode(path, node);
        }
        
    }
    
    private void updateTypClassUsingICOPrefAndRSOneLOV()
    {
        Map<String, String> prefKeyValueMap = new HashMap<String, String>();
        TCPreferenceService typClassServ = m_session.getPreferenceService();
        String[] prefKeyValueStrArray = typClassServ.getStringArray(TCPreferenceService.TC_preference_site,
                m_registry.getString("ClassificationTypeFilter.NAME"));
        
        if (prefKeyValueStrArray.length == 0)
        {
            MessageBox.post(m_registry.getString("ClassTypFilterPrefNotFound.MSG"), "", MessageBox.ERROR);
        }
        else
        {
            for (int ik = 0; ik < prefKeyValueStrArray.length; ik++)
            {
                String[] tempStr = prefKeyValueStrArray[ik].split("->");
                if (tempStr.length >= 2)
                {
                    prefKeyValueMap.put(tempStr[0], tempStr[1]);
                }
            }
            
            Map<String, String> rsoneLOVValueDescMap = RSOneOperationTypePanelHelper.getOracleItemTypeCodeMap();

            for (Map.Entry<String, String> entryPref : prefKeyValueMap.entrySet())
            {
                String typeClassPref = entryPref.getValue();
                String descValPref = entryPref.getKey();
                // System.out.println("pref = " + typeClassPref
                // +"---"+descValPref);
                for (Map.Entry<String, String> entryLOV : rsoneLOVValueDescMap.entrySet())
                {
                    String descValLOV = entryLOV.getValue();
                    String typeClassLOV = entryLOV.getKey();
                    // System.out.println("LOV = " + typeClassLOV
                    // +"---"+descValLOV);
                    if (descValPref.equalsIgnoreCase(descValLOV))
                    {
                        typClassMap.put(typeClassLOV, typeClassPref);
                    }
                }
            }
        }
    }
    
    
}
