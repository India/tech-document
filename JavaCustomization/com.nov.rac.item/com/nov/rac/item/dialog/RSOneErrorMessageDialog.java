package com.nov.rac.item.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class RSOneErrorMessageDialog extends AbstractSWTDialog
{
    
    public RSOneErrorMessageDialog(Shell shell, String message)
    {
        super(shell);
        
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        
        setShellStyle(SWT.CENTER | SWT.CLOSE | SWT.APPLICATION_MODAL | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
        
        m_errorMessage = message;
        
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        setTitle();
        m_composite = parent;
        
        Composite l_labelComposite = new Composite(parent, SWT.NONE);
        l_labelComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        l_labelComposite.setLayout(new GridLayout(2, false));
        
        errorImage = new Label(l_labelComposite, SWT.NONE);
        errorImage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        errorImage.setImage(getShell().getDisplay().getSystemImage(SWT.ERROR));
        
        l_messageText = new StyledText(l_labelComposite, SWT.READ_ONLY);
        l_messageText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        l_messageText.setEditable(false);
        l_messageText.setCaret(null);
        l_messageText.setText(getErrorMessage());
        
        return parent;
    }
    
    @Override
    protected Control createButtonBar(Composite parent)
    {
        Composite i_buttonComposite = new Composite(parent, SWT.NONE);
        i_buttonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
        i_buttonComposite.setLayout(new GridLayout(2, true));
        
        m_ok = new Button(i_buttonComposite, SWT.PUSH);
        m_ok.setText(m_registry.getString("OK_LABEL"));
        m_ok.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_ok.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                getShell().dispose();
            }
        });
        
        m_copyText = new Button(i_buttonComposite, SWT.PUSH);
        m_copyText.setText(m_registry.getString("COPYTEXT_LABEL"));
        m_copyText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        m_copyText.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                if (!l_messageText.getSelectionText().isEmpty())
                {
                    String textData = l_messageText.getSelectionText();
                    final Clipboard cb = new Clipboard(getShell().getDisplay());
                    TextTransfer textTransfer = TextTransfer.getInstance();
                    cb.setContents(new Object[] { textData }, new Transfer[] { textTransfer });
                }
                getShell().dispose();
            }
        });
        
        return null;
        
    }
    
    @Override
    protected Point getInitialLocation(Point arg0)
    {
        Monitor theMonitor = m_composite.getMonitor();
        Rectangle screenSize = theMonitor.getClientArea();
        
        int x = screenSize.width / 3;
        int y = screenSize.height / 3;
        Point location = new Point(x, y);
        
        return location;
    }
    
    private void setTitle()
    {
        this.getShell().setText(m_registry.getString("ErrorMessageDialog.Title"));
    }
    
    private String getErrorMessage()
    {
        return m_errorMessage;
    }
    
    private String m_errorMessage = "";
    private Registry m_registry = null;
    private Label errorImage;
    private Button m_ok;
    private Button m_copyText;
    private StyledText l_messageText;
    
    private Composite m_composite = null;
}
