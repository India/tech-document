package com.nov.rac.item.dialog;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NewItemDialog extends AbstractSWTDialog /* implements ISubscriber */
{
    
    public NewItemDialog(Shell shell)
    {
        super(shell);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE );
        // registerSubscriber();
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        setTitle();
        
        ScrolledComposite scrolledComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        
        scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        Composite composite = new Composite(scrolledComposite, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        dialogComposite = new NewItemDialogComposite(composite, SWT.FILL);
        scrolledComposite.setContent(composite);
        scrolledComposite.setExpandHorizontal(true);
        scrolledComposite.setExpandVertical(true);
        scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        
        parent.getShell().addDisposeListener(new DisposeListener()
        {
            
            @Override
            public void widgetDisposed(DisposeEvent disposeevent)
            {
                setReturnCode(1); // when dialog is disposed or closed
                System.out.println(" ************************  widget is disposed *************** ");
                ArrayList<IUIPanel> iupanel = dialogComposite.getOperationInputProvider();
                if (null != iupanel && iupanel.size() > 0)
                {
                    for (IUIPanel panels : iupanel)
                    {
                        panels.dispose();
                    }
                    
                }
                dialogComposite.dispose();
                
            }
        });
        return parent;
    }
    
    @Override
    protected Control createButtonBar(Composite parent)
    {
        /*
         * PanelFactory theFactory = PanelFactory.getInstance(); m_buttonBar=
         * theFactory.createButtonBar(parent,"DEFAULT.BUTTONBAR"); return
         * m_buttonBar;
         */
        return null;
    }
    
    @Override
    protected Point getInitialLocation(Point point)
    {
        
        // Move the dialog to the center of the top level shell.
        Rectangle shellBounds = getShell().getParent().getBounds();
        Point dialogSize = getInitialSize();
        
        int x = shellBounds.x + (shellBounds.width - dialogSize.x) / 2;
        int y = shellBounds.y + (shellBounds.height - dialogSize.y) / 2;
        Point location = new Point(x, y);
        
        return location;
    }
    
    private void setTitle()
    {
        String title = m_registry.getString("newItemDialog.Title");
        this.getShell().setText(title);
        
    }
    
    /*
     * @Override public void update(PublishEvent event) { String propertyName
     * =event.getPropertyName(); if(propertyName.equals("operation")) { String
     * panelType = (String) event.getNewValue(); updateButtonBar(panelType); } }
     */
    
    /*
     * private void updateButtonBar( String panelType) { panelType =
     * "CreateNewPart"; PanelFactory theFactory = PanelFactory.getInstance();
     * Composite parent = (Composite)m_buttonBar; // m_buttonBar.dispose();
     * m_buttonBar = theFactory.createButtonBar(parent,panelType+".BUTTONBAR");
     * }
     */
    
    /*
     * private void registerSubscriber() { IController theController =
     * ControllerFactory.getFactory().getDefaultController();
     * theController.registerSubscriber("operation", this); }
     */
    // protected AbstractDialogButtonBar m_buttonBar = null;
    private NewItemDialogComposite dialogComposite;
    private Registry m_registry = null;
    private Button okButton;
    private Button cancelButton;
    private Button applyButton;
    private Button helpButton;
    
}
