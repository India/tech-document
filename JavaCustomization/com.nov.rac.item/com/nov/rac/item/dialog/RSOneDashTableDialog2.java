package com.nov.rac.item.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCException;

public class RSOneDashTableDialog2 extends RSOneDashTableDialog
{

    public RSOneDashTableDialog2(Shell shell, int style)
    {
        super(shell, style);
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        setTitle();
        
        m_composite = parent;
        
        m_dashComposite = new RSOneDashTableComposite2(parent, SWT.NONE);
        try
        {
            m_dashComposite.createUI();
            m_dashComposite.loadTable();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return parent;
    }
    
}
