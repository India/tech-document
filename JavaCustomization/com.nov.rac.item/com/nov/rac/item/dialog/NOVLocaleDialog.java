package com.nov.rac.item.dialog;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVLocaleDialog extends AbstractSWTDialog
{

    public NOVLocaleDialog(Shell arg0)
    {
        super(arg0);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
    }
    
    public NOVLocaleDialog(Shell shell, String locale, String masterValue)
    {
        super(shell);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
        this.m_masterLocale = locale;
        this.m_masterValue = masterValue;
    }

    @Override
    protected Control createDialogArea(Composite l_composite)
    {
        setTitle();
        Composite composite = new Composite(l_composite, SWT.NONE);
        composite.setLayout(new GridLayout(3, true));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        createMasterLocaleRow(composite);
        
        createMasterValueRow(composite);
        
        createLocaleRow(composite);
        
        createValueRow(composite);
        
        createButtonRow(composite);
        
        getShell().setMinimumSize(350, 250);
        
        return composite;
    }


    private void createMasterLocaleRow(Composite composite)
    {
        m_masterLocaleLabel = new Label(composite, SWT.NONE);
        m_masterLocaleLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        m_masterLocaleLabel.setText(m_registry.getString("masterLocale.Label"));
        
        m_masterLocaleText = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
        m_masterLocaleText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_masterLocaleText.setText(m_masterLocale);
        
        new Label(composite, SWT.NONE);
    }
    
    private void createMasterValueRow(Composite composite)
    {
        m_masterValueLabel = new Label(composite, SWT.NONE);
        m_masterValueLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        m_masterValueLabel.setText(m_registry.getString("masterValue.Label"));
        
        m_masterValueText = new Text(composite, SWT.BORDER | SWT.READ_ONLY);
        m_masterValueText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
        m_masterValueText.setText(m_masterValue);
        
    }
    
    private void createLocaleRow(Composite composite)
    {
        m_localeLabel = new Label(composite, SWT.NONE);
        m_localeLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        m_localeLabel.setText(m_registry.getString("locale.Label"));
        
        m_localeCombo = new Combo(composite, SWT.NONE);
        m_localeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
    }


    private void createValueRow(Composite composite)
    {
        m_ValueLabel = new Label(composite, SWT.NONE);
        m_ValueLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, true, 1, 3));
        m_ValueLabel.setText(m_registry.getString("Value.Label"));
        
        m_ValueText = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
        m_ValueText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 3));
        
       // new Label(composite, SWT.NONE);
    }
    
    private void setTitle()
    {
        String title = m_registry.getString("localizationDialog.Title");
        this.getShell().setText(title);
    }

    public void setMasterLocaleText(String text)
    {
        m_masterLocaleText.setText(text);
    }
    
    private void createButtonRow(Composite composite)
    {
        Composite l_buttonComp = new Composite(composite, SWT.NONE);
        l_buttonComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        l_buttonComp.setLayout(new GridLayout(3,true));
        
        m_okBtn = new Button(l_buttonComp, SWT.NONE);
        GridData okbtngd = new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1);
        okbtngd.widthHint = 50;
        m_okBtn.setLayoutData(okbtngd);
        m_okBtn.setText(m_registry.getString("ok.Button"));
        
        m_applyBtn = new Button(l_buttonComp, SWT.NONE);
        m_applyBtn.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        m_applyBtn.setText(m_registry.getString("Apply.Button"));
        
        m_cancelBtn = new Button(l_buttonComp, SWT.NONE);
        m_cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        m_cancelBtn.setText(m_registry.getString("Cancel.Button"));
      
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
        // TODO Auto-generated method stub
        //super.createButtonsForButtonBar(parent);
    }

    private Registry m_registry;
    private Label m_masterLocaleLabel;
    private Text m_masterLocaleText;
    private Label m_masterValueLabel;
    private Text m_masterValueText;
    private Label m_localeLabel;
    private Combo m_localeCombo;
    private Label m_ValueLabel;
    private Text m_ValueText;
    private String m_masterLocale, m_masterValue;
    private Button m_okBtn, m_applyBtn, m_cancelBtn;
}
