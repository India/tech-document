package com.nov.rac.item.dialog;

import org.eclipse.swt.widgets.Composite;

public class ItemCreationButtonBarForRDD extends ItemCreationButtonBar
{

	public ItemCreationButtonBarForRDD(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite dialogButtonBar)
	{
		super.createButtonsForButtonBar(dialogButtonBar);
		m_applyButton.setEnabled(false);
	}

}
