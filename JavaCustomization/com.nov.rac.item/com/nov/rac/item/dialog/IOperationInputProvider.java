package com.nov.rac.item.dialog;

import java.util.ArrayList;

import com.nov.rac.ui.IUIPanel;

/**
 * The Interface IOperationInputProvider.
 */
public interface IOperationInputProvider {

    /**
     * Gets the operation input provider.
     *
     * @return the operation input provider
     */
    public  ArrayList<IUIPanel> getOperationInputProvider();
}
