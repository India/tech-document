package com.nov.rac.item.dialog;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.ui.IUIPanel;

public class OperationButtonBar extends AbstractDialogButtonBar
{
    public OperationButtonBar(Composite parent, int style)
	{
		super(parent, style);
		m_parent = parent;
	}

	@Override
	protected void createButtonsForButtonBar(Composite dialogButtonBar)
	{
	    ((GridLayout) dialogButtonBar.getLayout()).numColumns = 0;
	    m_cancelButton = createButton(dialogButtonBar, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	    addCancelButtonListener();
	}

	private void addCancelButtonListener()
    {
        m_cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
//                boolean cancel = MessageDialog.openQuestion( AIFUtility.getActiveDesktop().getShell(),
//                        IDialogConstants.CANCEL_LABEL, m_registry.getString("cancel.MSG") ); 
//                if(cancel)
//                {
                    m_parent.getShell().dispose();
              //  }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent event)
            {
                
            }
        });
    }

    @Override
    public void registerOperationInputProvider(List<IUIPanel> inputProviders)
    {
        // does not do anything (did this change while refactoring)
    }
    
    private Button m_cancelButton;
	protected Composite m_parent;
}
