package com.nov.rac.item.dialog;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractDialogButtonBar extends Composite
{
    public AbstractDialogButtonBar(Composite parent, int style)
    {
        super(parent, style);
        this.setFont(parent.getFont());
        m_parentComposite = parent;
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        createUI();
    }
    
    private void createUI()
    {
        
        GC gc = new GC(this);
        
        FontMetrics fontMetrics = gc.getFontMetrics();
        gc.dispose();
        
        GridLayout layout = new GridLayout();
       // layout.numColumns = 0;
        layout.makeColumnsEqualWidth = true;
        layout.marginWidth = Dialog.convertHorizontalDLUsToPixels(fontMetrics, 7);
        layout.marginHeight = Dialog.convertVerticalDLUsToPixels(fontMetrics, 7);
        layout.horizontalSpacing = Dialog.convertHorizontalDLUsToPixels(fontMetrics, 4);
        layout.verticalSpacing = Dialog.convertVerticalDLUsToPixels(fontMetrics, 4);
        this.setLayout(layout);
        
        GridData data = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        this.setLayoutData(data);
        
        createButtonsForButtonBar(this);
        
    }
    
    protected Button createButton(Composite parent, int id, String label, boolean defaultButton)
    {
        ((GridLayout) parent.getLayout()).numColumns++;
        Button button = new Button(parent, 8);
        button.setText(label);
        button.setFont(JFaceResources.getDialogFont());
        button.setData(new Integer(id));
        /*
         * button.addSelectionListener(new SelectionAdapter() { public void
         * widgetSelected(SelectionEvent event) { // buttonPressed(((Integer)
         * event.widget.getData()).intValue()); } });
         */
        if (defaultButton)
        {
            Shell shell = parent.getShell();
            if (shell != null)
                shell.setDefaultButton(button);
        }
        // buttons.put(new Integer(id), button);
        setButtonLayoutData(button);
        
        return button;
    }
    
    protected void setButtonLayoutData(Button button)
    {
        GridData data = new GridData(256);
        int widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(button, 61);
        Point minSize = button.computeSize(-1, -1, true);
        data.widthHint = Math.max(widthHint, minSize.x);
        button.setLayoutData(data);
    }
    
    abstract protected void createButtonsForButtonBar(Composite dialogButtonBar);
    
    public abstract void registerOperationInputProvider(List<IUIPanel> inputProviders);
    
    protected Registry m_registry = null;
    protected Composite m_parentComposite = null;
    // Commented following for unused
    // private List<Button> buttons = new ArrayList<Button>();
}