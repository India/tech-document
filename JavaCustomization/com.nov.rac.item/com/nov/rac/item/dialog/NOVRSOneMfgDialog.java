package com.nov.rac.item.dialog;


import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.item.helpers.ManufacturerHelper;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.services.emailHelper.EmailHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVRSOneMfgDialog extends AbstractSWTDialog
{
    public NOVRSOneMfgDialog(Shell shell)
    {
        super(shell);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
        m_session = getSession();
    }
    
    protected TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
    }
    
    @Override
    protected Control createDialogArea(Composite l_composite)
    {
        setTitle();
        Composite composite = new Composite(l_composite, SWT.NONE);
        composite.setLayout(new GridLayout(6, true));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        createManufacturerNameRow(composite);
        createAdditionalInfoRow(composite);
        createOrgRow(composite);
        createSubmitButtonBar(composite);
        getShell().setMinimumSize(300, 250);
        return composite;
    }
    
    private void createManufacturerNameRow(Composite composite)
    {
        m_mfgNameLabel = new Label(composite, SWT.NONE);
        m_mfgNameLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
        m_mfgNameLabel.setText(m_registry.getString("mfgName.Label"));
        
        m_mfgNameText = new Text(composite, SWT.BORDER);
        GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
        m_mfgNameText.setLayoutData(gd);
        UIHelper.makeMandatory(m_mfgNameText);
        new Label(composite, SWT.NONE);
    }

    private void createAdditionalInfoRow(Composite composite)
    {
        m_infoLabel = new Label(composite, SWT.NONE);
        m_infoLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 2, 3));
        m_infoLabel.setText(m_registry.getString("additionalInfo.Label"));
        
        m_infoText = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
        m_infoText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 3));
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
    }

    private void createOrgRow(Composite composite)
    {
        m_OrgLabel = new Label(composite, SWT.NONE);
        m_OrgLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 2, 1));
        m_OrgLabel.setText(m_registry.getString("org.Label"));
        
        m_orgCombo = new Combo(composite, SWT.BORDER);
        m_orgCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        TCComponentListOfValues rsOneOrgLov = TCComponentListOfValuesType.findLOVByName(m_session,
                NationalOilwell.RSOne_ORG_LOV);
        try
        {
            UIHelper.setAutoComboArray(m_orgCombo, LOVUtils.getLovStringValues(rsOneOrgLov));
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Label(composite, SWT.NONE);
    }

    private void setTitle()
    {
        String title = m_registry.getString("requestMfgDialog.Title");
        this.getShell().setText(title);
    }
    
    private void createSubmitButtonBar(Composite composite)
    {
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        Composite l_buttonComposite = new Composite(composite, SWT.NONE);
        l_buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
        l_buttonComposite.setLayout(new GridLayout(2, true));
        
        m_submitBtn = new Button(l_buttonComposite, SWT.NONE);
        m_submitBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
        m_submitBtn.setText(m_registry.getString("submit.name"));
        addListenerOnSubmit(m_submitBtn);
        
        m_cancelBtn = new Button(l_buttonComposite, SWT.NONE);
        m_cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        m_cancelBtn.setText(m_registry.getString("cancel.name"));
        m_cancelBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                getShell().dispose();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }

    
    private void addListenerOnSubmit(Button m_submitBtn2)
    {
        m_submitBtn2.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                Vector<String> mfgNamesVect = new Vector<String>();
                 ManufacturerHelper mfghelper=new ManufacturerHelper(); 
                try 
                {
					mfgNamesVect = ManufacturerHelper.getActiveManifacturersVector();
					
				} 
                catch (Exception e) 
                {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                
                String strNewMfg = m_mfgNameText.getText();
                if(strNewMfg.length()>0 &&m_orgCombo.getText()!= null && m_orgCombo.getText().length() > 0)
                {
                    if(!mfgNamesVect.contains(strNewMfg))
                    {
                    	
                        try 
                        {        
                        	
                        	EmailHelper emailhelper = new EmailHelper();
                        	String status= prepareEmail(strNewMfg,emailhelper);
                        	
                        	if(status.equals("Success"))
                        	{
                        		emailhelper.sendEmail();
                        	}
                        	
                        	getShell().close();
                            
                        } 
                        catch (TCException e1) 
                        {
                            // TODO Auto-generated catch block
                            String[] errMsgs = e1.errorStrings;
                            String msg = null;
                            if(errMsgs[0].contains("NOV_RequestMfg_MailIds") || errMsgs[0].contains("Mail_server_name") || errMsgs[0].contains("Mail_server_port"))
                            {
                                String[] error = errMsgs[0].split(":");
                                msg = m_registry.getString("PrefMiss.MSG")+" "+error[1]+" "+m_registry.getString("PrefMiss1.MSG");
                            }
                            else
                            {
                                msg = errMsgs[0];
                            }
                            MessageBox.post(getShell(),msg,"Error...",MessageBox.INFORMATION);
                            e1.printStackTrace();
                        }       
                    }
                    else
                    {
                        MessageBox.post(getShell(),(m_registry.getString("DuplicateMfg.ERROR")),"Duplicate Mfg...",MessageBox.INFORMATION);
                    }
                }
                else
                {
                    if(strNewMfg.length() == 0)
                    {
                        MessageBox.post(getShell(),(m_registry.getString("MfgNameMandatory.ERROR")),"Error...",MessageBox.INFORMATION);
                    }
                    else if(m_orgCombo.getText() == null || m_orgCombo.getText().toString().length() == 0)
                    {
                        MessageBox.post(getShell(),(m_registry.getString("MfgNameOrgMandatory.ERROR")),"Error...",MessageBox.INFORMATION);
                    }
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
       // super.createButtonsForButtonBar(parent);
    }
    public String prepareEmail(String strNewMfg ,EmailHelper emailhelper) throws TCException
    {
    	String status="Success";
    	//add Recipients
    	String[] recipients = PreferenceHelper.getStringValues("NOV_RequestMfg_MailIds_Test", TCPreferenceService.TC_preference_site);
    	if (recipients==null ||recipients.length==0)
    	{
    		MessageBox.post(getShell(),"Check the NOV_RequestMfg_MailIds Preference for valid email IDs","Error...",MessageBox.INFORMATION);
    		return "Fail";
    	}
    	emailhelper.addRecipient(recipients);
    	
    	//Add Subject
    	String strSubject = m_registry.getString("MailSubject.MSG");  
    	emailhelper.setSubject(strSubject);
    	
    	//Set body
    	String strBody = null;
        if(m_infoText.getText().length()>0)
        {
            strBody =m_registry.getString("MailBody1.MSG")+" " +strNewMfg +" "+m_registry.getString("MailBody2.MSG")+
            "\n"+m_registry.getString("RsOneOrg.NAME")+" " +"-"+" "+m_orgCombo.getText()+
            "\n"+m_registry.getString("MailBodyAddnlInfo.MSG")+"\n"+m_infoText.getText();
        }
        else
        {
            strBody =m_registry.getString("MailBody1.MSG")+" " +strNewMfg +" "+m_registry.getString("MailBody2.MSG")+
            "\n"+m_registry.getString("RsOneOrg.NAME")+" " +"-"+" "+m_orgCombo.getText();
        }
    	emailhelper.setBody(strBody);
    	
 
    	// set Sender
    	String  sender = m_session.getUser().getTCProperty("person").getReferenceValue().getProperty("PA9");
    	emailhelper.setSenderMailID(sender);
    	
    	String  senderDisplayName = m_session.getUser().getTCProperty("user_name").toString();
    	emailhelper.setsenderDisplayName(senderDisplayName);
    	return status;
    }
    
    private Registry m_registry;
    private Label m_mfgNameLabel;
    private Text m_mfgNameText;
    private Label m_infoLabel;
    private Text m_infoText;
    private Label m_OrgLabel;
    private Combo m_orgCombo;
    private TCSession m_session;
    private Button m_submitBtn;
    private Button m_cancelBtn;
}
