package com.nov.rac.item.dialog;

import java.util.HashMap;
import java.util.Map;

import javax.swing.text.Position;
import javax.swing.tree.TreePath;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.item.helpers.ClassificationDialogHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.classification.common.G4MInClassDialog;
import com.teamcenter.rac.classification.common.tree.G4MAutoScrollingJTree;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class NOVClassificationDialog extends AbstractSWTDialog implements
		IPublisher {

	public NOVClassificationDialog(Shell shell) {
		super(shell);
		m_session = getSession();
		m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
		setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE
				| SWT.RESIZE);
	}

	public NOVClassificationDialog(Shell shell, Text m_classText) {

		this(shell);
	}

	protected TCSession getSession() {
		return (TCSession) AIFUtility.getCurrentApplication().getSession();

	}

	@Override
	protected Control createDialogArea(Composite composite) 
	{
		setTitle();

		createSearchClassRow(composite);

		m_composite = new Composite(composite, SWT.EMBEDDED);

		m_composite.setLayout(new GridLayout(1, true));

		m_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
				1, 1));

		m_app = ClassificationDialogHelper.getClassificationApp();

		m_compContext = new G4MInClassDialog(m_app);

		m_classificationtree = m_compContext.getTree().getTree();

		classTreePane = new ScrollPagePane(m_classificationtree);

		SWTUIUtilities.embed(m_composite, classTreePane, false);

		addSelectionListenerOnSearchBtn();

		return composite;
	}

	protected void findNodes(String srchString) 
	{	
		Object[] pathElements = null;
		TreePath nextMatch = null;
		for (int i = 0; i < m_classificationtree.getRowCount(); i++) 
		{
			
			m_classificationtree.expandRow(i);
		
			nextMatch = m_classificationtree.getNextMatch(srchString, 0,
					Position.Bias.Forward);
			
			if(nextMatch != null)
			{
				pathElements= nextMatch.getPath();
			
				m_classificationtree.setVisibleRowCount(i);
				m_classificationtree.expandPath(nextMatch);
				m_classificationtree.setSelectionPath(nextMatch);
				
				break;
				
			}
			
		}
		
		if(pathElements != null)
		{
			for (int i = 0; i < m_classificationtree.getRowCount(); i++) 
			{
				for(int j=0; j< pathElements.length; j++)
				{
					if( m_classificationtree.getPathForRow(i).getPathCount() >= j+1 )
					
						if(!m_classificationtree.getPathForRow(i).getPathComponent(j).equals(pathElements[j]))
						{
							m_classificationtree.collapsePath(m_classificationtree.getPathForRow(i));
						}
				}
			}
		}
		
		else
		{
			MessageBox.post("No class found with given search criteria",
                    "Information", MessageBox.INFORMATION);
			
		}
			
	}

	public void setLocation(Point location, Point size) {
		m_dialogLocation = location;

		m_classBtnSize = size;
	}

	protected Point getInitialLocation(Point point) {
		Point size = getInitialSize();

		Point location = new Point(m_dialogLocation.x - size.x,
				m_dialogLocation.y - size.y);

		if (location.x < 0) {
			location = new Point(m_dialogLocation.x + m_classBtnSize.x,
					m_dialogLocation.y - size.y);
		}
		return location;
	}

	private void setTitle() {
		String title = m_registry.getString("classificationDialog.Title");
		this.getShell().setText(title);

	}

	@Override
	public IPropertyMap getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Point getInitialSize() {
		Monitor theMonitor = m_composite.getMonitor();

		Rectangle screenSize = theMonitor.getClientArea();

		int x = screenSize.width / 4;

		int y = screenSize.height / 3;

		Point location = new Point(x, y);

		return location;

	}

	protected void createSearchClassRow(Composite composite) 
	{
		m_composite = new Composite(composite, SWT.NONE);

		m_composite.setLayout(new GridLayout(2, false));

		m_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
				false, 2, 1));

		m_textSearch = new Text(m_composite, SWT.BORDER);
		GridData gd_txtSearch = new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1);
		m_textSearch.setLayoutData(gd_txtSearch);

		m_btnSearchClass = new Button(m_composite, SWT.NONE);
		GridData gd_btnSearch = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		m_btnSearchClass.setText(m_registry.getString("btnSearchClass.NAME",
				"Key Not Found"));
		m_btnSearchClass.setLayoutData(gd_btnSearch);

	}

	protected void addSelectionListenerOnSearchBtn() {
		m_btnSearchClass.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent se) {
				String srchString = m_textSearch.getText();
				findNodes(srchString);
			}
		});
	}

	private AbstractAIFUIApplication m_app;
	Point m_dialogLocation;
	Point m_classBtnSize;
	protected Registry m_registry;
	protected static G4MAutoScrollingJTree m_classificationtree;
	protected G4MInClassDialog m_compContext;
	private Composite m_composite;
	protected TCSession m_session;
	protected ScrollPagePane classTreePane;
	protected Map<String, String> typClassMap = new HashMap<String, String>();
	protected Text m_textSearch;
	protected Button m_btnSearchClass;
}
