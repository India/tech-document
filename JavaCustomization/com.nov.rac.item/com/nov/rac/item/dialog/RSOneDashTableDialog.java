package com.nov.rac.item.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class RSOneDashTableDialog extends AbstractSWTDialog implements IPublisher
{
    public RSOneDashTableDialog(Shell shell, int style)
    {
        super(shell, style);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
        setShellStyle(SWT.CENTER | SWT.RESIZE | SWT.CLOSE | SWT.APPLICATION_MODAL | SWT.BORDER | SWT.TITLE);
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        setTitle();
        
        m_composite = parent;
        
        m_dashComposite = new RSOneDashTableComposite(parent, SWT.NONE);
        try
        {
            m_dashComposite.createUI();
            m_dashComposite.loadTable();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return parent;
    }
    
    @Override
    protected Control createButtonBar(Composite parent)
    {
        Composite L_buttonComposite = new Composite(parent, SWT.NONE);
        L_buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        L_buttonComposite.setLayout(new GridLayout(NO_OF_COLUMNS_FOR_BUTTON_COMP, true));
        
        new Label(L_buttonComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        new Label(L_buttonComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        new Label(L_buttonComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        
        m_ok = new Button(L_buttonComposite, SWT.PUSH);
        m_ok.setText(m_registry.getString("OK_LABEL"));
        m_ok.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_ok.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                publishEvent();
                close();
            }
        });
        
        m_cancel = new Button(L_buttonComposite, SWT.PUSH);
        m_cancel.setFocus();
        m_cancel.setText(m_registry.getString("CANCEL_LABEL"));
        m_cancel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_cancel.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                close();
            }
        });
        
        new Label(L_buttonComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        new Label(L_buttonComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        new Label(L_buttonComposite, SWT.NONE).setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        
        getShell().setSize(getShell().getSize().x, getShell().getSize().y / 2);
        getShell().setMinimumSize(DIALOG_WIDTH, DIALOG_HIEGHT);
        
        return null;
    }
    
    private void publishEvent()
    {
        m_dashComposite.saveTable();
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(this, NationalOilwell.RSONE_TABLE_OK_BUTTON_EVENT, null, null);
        controller.publish(event);
    }
    
    @Override
    protected Point getInitialLocation(Point arg0)
    {
        Monitor theMonitor = m_composite.getMonitor();
        Rectangle screenSize = theMonitor.getClientArea();
        
        int x = screenSize.width / DIALOG_INITIAL_LOCATION_WIDTH_DIVISOR;
        int y = screenSize.height / DIALOG_INITIAL_LOCATION_HIEGHT_DIVISOR;
        Point location = new Point(x, y);
        
        return location;
    }
    
    protected void setTitle()
    {
        this.getShell().setText(m_registry.getString("RSOneItemCreate.Title"));
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean close()
    {
        m_dashComposite.dispose();
        
        return super.close();
    }
    
    private Registry m_registry;
    protected Composite m_composite;
    protected RSOneDashTableComposite m_dashComposite;
    protected Button m_ok;
    protected Button m_cancel;
    
    private static final int NO_OF_COLUMNS_FOR_BUTTON_COMP = 8;
    
    private static final int DIALOG_WIDTH = 1000;
    private static final int DIALOG_HIEGHT = 300;
    
    private static final int DIALOG_INITIAL_LOCATION_WIDTH_DIVISOR = 4;
    private static final int DIALOG_INITIAL_LOCATION_HIEGHT_DIVISOR = 4;
}
