package com.nov.rac.item.dialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Shell;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.item.helpers.RSOneDashPropertyMap;
import com.nov.rac.item.panels.DashInfoModifyTable;
import com.nov.rac.item.panels.rsone.CopyEntireColumnActionListener;
import com.nov.rac.item.renderer.CheckboxHeaderRenderer;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class RSOneDashTableComposite extends AbstractUIPanel implements IPublisher
{
    
    public RSOneDashTableComposite(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.dialog");
    }
    
    protected GridLayout getLayout(int numofColumns, boolean equalWidth)
    {
        GridLayout l_layout = new GridLayout(numofColumns, equalWidth);
        l_layout.marginHeight = 0;
        l_layout.marginWidth = 0;
        
        return l_layout;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(1, false));
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        createFields(l_composite);
        createTableUI(l_composite);
        
        setFields();
        
        return true;
    }
    
    private void createFields(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        GridLayout l_layout = new GridLayout(2, true);
        l_layout.marginWidth = 0;
        l_composite.setLayout(l_layout);
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        Composite l_leftComposite = new Composite(l_composite, SWT.NONE);
        l_leftComposite.setLayout(getLayout(HORIZONTAL_SPAN_FOR_LEFT_COMP, true));
        l_leftComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        Composite l_rightComposite = new Composite(l_composite, SWT.NONE);
        l_rightComposite.setLayout(getLayout(HORIZONTAL_SPAN_FOR_RIGHT_COMP, false));
        l_rightComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        
        createCopyAllButton(l_leftComposite);
        
        createRemoveDashButton(l_leftComposite);
        
        Label emptylabel = new Label(l_leftComposite, SWT.NONE);
        emptylabel.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true, HORIZONTAL_SPAN_FOR_EMPTY_LABEL, 1));
        
        creatAddDashFields(l_rightComposite);
    }
    
    private void createCopyAllButton(Composite parent)
    {
        m_copyAllBtn = new Button(parent, SWT.NONE);
        m_copyAllBtn.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        m_copyAllBtn.setVisible(true);
        m_copyAllBtn.setText(m_registry.getString("COPYALL_LABEL"));
        m_copyAllBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                copyData();
            }
        });
    }
    
    private void createRemoveDashButton(Composite parent)
    {
        m_removeRowBtn = new Button(parent, SWT.NONE);
        m_removeRowBtn.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
        m_removeRowBtn.setVisible(false);
        m_removeRowBtn.setText(m_registry.getString("Remove Row"));
        m_removeRowBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                removeRow();
                setButtons();
                resetHeaderRenderer();
            }
        });
    }
    
    private void removeRow()
    {
        if (m_dashTable.getSelectedRowCount() > 0)
        {
            removeValidRow();
        }
        else
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("NoDashNoSelected.msg"),
                    SWT.ICON_WARNING | SWT.OK);
        }
    }
    
    private void removeValidRow()
    {
        int L_selectedRowsCount = m_dashTable.getSelectedRowCount();
        int[] L_selectedRowsIndex = m_dashTable.getSelectedRows();
        
        for (int rowindex = 1; rowindex <= L_selectedRowsCount; rowindex++)
        {
            Object[] rowData = m_dashTable.getRowData(L_selectedRowsIndex[L_selectedRowsCount - rowindex]);
            
            String l_dashNo = (String) rowData[0];
            
            if (l_dashNo.compareTo("001") == 0)
            {
                MessageBox.post(m_registry.getString("DashNumberMandatory.error"), m_registry.getString("Error!"),
                        MessageBox.ERROR);
            }
            else
            {
                m_dashTable.removeRow(L_selectedRowsIndex[L_selectedRowsCount - rowindex]);
                m_dashNoList.remove(l_dashNo);
            }
        }
    }
    
    private void creatAddDashFields(Composite parent)
    {
        m_addSingleDashNoBtn = new Button(parent, SWT.RADIO);
        m_addSingleDashNoBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1));
        m_addSingleDashNoBtn.setText("Add One Dash");
        
        m_addMultiDashNoBtn = new Button(parent, SWT.RADIO);
        m_addMultiDashNoBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1));
        m_addMultiDashNoBtn.setText("Add a Range");
        
        m_rangeTextStart = new Text(parent, SWT.BORDER);
        m_rangeTextStart.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1));
        
        m_dashLabel = new Label(parent, SWT.NONE);
        m_dashLabel.setText("__");
        m_dashLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, true, false, 1, 1));
        
        m_rangeTextEnd = new Text(parent, SWT.BORDER);
        m_rangeTextEnd.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1));
        
        m_addBtn = new Button(parent, SWT.NONE);
        m_addBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1));
        Image l_addImage = new Image(parent.getDisplay(), m_registry.getImage("add.Icon"), SWT.NONE);
        m_addBtn.setImage(l_addImage);
        
        addListeners();
    }
    
    private void addListeners()
    {
        m_addSingleDashNoBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                if (m_addSingleDashNoBtn.getSelection())
                {
                    m_dashLabel.setEnabled(false);
                    m_rangeTextEnd.setEnabled(false);
                    m_rangeTextEnd.setText("");
                }
            }
        });
        
        m_addMultiDashNoBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                if (m_addMultiDashNoBtn.getSelection())
                {
                    m_dashLabel.setEnabled(true);
                    m_rangeTextEnd.setEnabled(true);
                }
            }
        });
        
        m_rangeTextStart.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_rangeTextStart);
            }
        });
        
        m_rangeTextEnd.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                doTextValidation(event, m_rangeTextEnd);
            }
        });
        
        m_addBtn.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                processAddBtnClickedEvent();
            }
        });
    }
    
    private void processAddBtnClickedEvent()
    {
        if (m_rangeTextEnd.isEnabled())
        {
            addMultipleDashNo();
        }
        else
        {
            addOneDashNumber();
        }
        setButtons();
        setHeaderRenderer();
    } 
    
    private void setHeaderRenderer()
    {
        Display.getDefault().asyncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (m_dashTable.getRowCount() > 1)
                {
                    assignHeaderRenderer("IMAN_master_form.rsone_org", m_registry.getString("RSOneOrg.NAME"));
                    assignHeaderRenderer("object_name", m_registry.getString("Name.NAME"));
                    assignHeaderRenderer("ics_subclass_name", m_registry.getString("Class.NAME"));
                    assignHeaderRenderer("IMAN_master_form.rsone_uom", m_registry.getString("UOM.NAME"));
                    assignHeaderRenderer("object_desc", m_registry.getString("Desc.NAME"));
                    assignHeaderRenderer("traceability", m_registry.getString("Traceability.NAME"));
                    
                    assignHeaderRendererToMfgColumns();
                    m_dashTable.updateUI();
                }
            }
        });
    }
    
    private void assignHeaderRendererToMfgColumns()
    {
        IPropertyMap propMap = RSOneDashPropertyMap.getDashPropertyMap().get("001");
        
        String l_boType = propMap.getType();
        String l_partType = NewItemCreationHelper.getSelectedType();
        
        if (null != l_partType && null != l_boType)
        {
            boolean isEngPurchased = l_partType.equalsIgnoreCase(
                    m_registry.getString("EnggPurchasedType.NAME"));
            boolean isNonEngPurchased = l_partType.equalsIgnoreCase(
                    m_registry.getString("NonEnggPurchasedType.Name"));
            
            if ((isEngPurchased || isNonEngPurchased) && l_boType.equalsIgnoreCase(NationalOilwell.PART_BO_TYPE)) 
            {
                assignHeaderRenderer("revision.nov4_mfg_name", m_registry.getString("MfgName.NAME"));
                assignHeaderRenderer("revision.nov4_mfg_number", m_registry.getString("MfgNo.NAME"));
            }
            else if((isEngPurchased || isNonEngPurchased) && l_boType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
            {
                assignHeaderRenderer("revision.manufacturer_name", m_registry.getString("MfgName.NAME"));
                assignHeaderRenderer("revision.manufacturer_partnumber", m_registry.getString("MfgNo.NAME"));
            }
        }
    }
    
    private void resetHeaderRenderer()
    {
        Display.getDefault().asyncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (m_dashTable.getRowCount() <= 1)
                {
                    removeHeaderRenderer("IMAN_master_form.rsone_org");
                    removeHeaderRenderer("object_name");
                    removeHeaderRenderer("ics_subclass_name");
                    removeHeaderRenderer("IMAN_master_form.rsone_uom");
                    removeHeaderRenderer("object_desc");
                    removeHeaderRenderer("traceability");
                    
                    removeHeaderRendererToMfgColumns();
                    
                    m_dashTable.updateUI();
                }
            }
        });
    }
    
    private void removeHeaderRendererToMfgColumns()
    {
        IPropertyMap propMap = RSOneDashPropertyMap.getDashPropertyMap().get("001");
        
        String l_boType = propMap.getType();
        String l_partType = NewItemCreationHelper.getSelectedType();
        
        if (null != l_partType && null != l_boType)
        {
            boolean isEngPurchased = l_partType.equalsIgnoreCase(
                    m_registry.getString("EnggPurchasedType.NAME"));
            boolean isNonEngPurchased = l_partType.equalsIgnoreCase(
                    m_registry.getString("NonEnggPurchasedType.Name"));
            
            if ((isEngPurchased || isNonEngPurchased) && l_boType.equalsIgnoreCase(NationalOilwell.PART_BO_TYPE)) 
            {
                removeHeaderRenderer("revision.nov4_mfg_name");
                removeHeaderRenderer("revision.nov4_mfg_number");
            }
            else if((isEngPurchased || isNonEngPurchased) && l_boType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
            {
                removeHeaderRenderer("revision.manufacturer_name");
                removeHeaderRenderer("revision.manufacturer_partnumber");
            }
        }
    }
    
    private void assignHeaderRenderer(String columnId, String columnName)
    {
        TableColumn l_column = m_dashTable.getColumnModel().getColumn(TableUtils.getColumnIndex(columnId, m_dashTable));
        
        if(!(l_column.getHeaderRenderer() instanceof CheckboxHeaderRenderer))
        {
            CheckboxHeaderRenderer l_headerrenderer = new CheckboxHeaderRenderer(columnName);
            l_column.setHeaderRenderer(l_headerrenderer);
            l_headerrenderer.addActionListener(new CopyEntireColumnActionListener(m_dashTable));
        }
    }
    
    private void removeHeaderRenderer(String columnId)
    {
        TableColumn l_column = m_dashTable.getColumnModel().getColumn(TableUtils.getColumnIndex(columnId, m_dashTable));
        l_column.setHeaderRenderer(null);
        l_column.setHeaderRenderer(m_defaultCellRenderer);
    }
    
    private void addOneDashNumber()
    {
        if (!m_rangeTextStart.getText().isEmpty())
        {
            String dashNo = m_rangeTextStart.getText();
            
            if (dashNo.isEmpty() || Integer.parseInt(dashNo) == 0)
            {
                getMessage(m_registry.getString("Warning.text"), m_registry.getString("DashNumbercannotbe0.msg"),
                        SWT.ICON_WARNING | SWT.OK);
            }
            else
            {
                String l_newDashNo = formatDashNo(dashNo);
                
                if (!m_dashNoList.contains(l_newDashNo))
                {
                    m_dashNoList.add(l_newDashNo);
                    addRow(l_newDashNo);
                }
            }
            
            m_rangeTextStart.setText("");
        }
    }
    
    private void addMultipleDashNo()
    {
        boolean isValidDash = validateDashNos();
        int l_iFisrtDashNo = Integer.parseInt(m_rangeTextStart.getText());
        int l_LastDashNo = Integer.parseInt(m_rangeTextEnd.getText());
        
        for (int index = l_iFisrtDashNo; isValidDash && index <= l_LastDashNo; index++)
        {
            String l_newDashNo = formatDashNo(Integer.toString(index));
            
            if (!m_dashNoList.contains(l_newDashNo))
            {
                addRow(l_newDashNo);
                m_dashNoList.add(l_newDashNo);
            }
        }
        m_rangeTextStart.setText("");
        m_rangeTextEnd.setText("");
    }
    
    private boolean validateDashNos()
    {
        String l_firstDashNo = m_rangeTextStart.getText();
        String l_lastDashNo = m_rangeTextEnd.getText();
        
        if (l_firstDashNo.isEmpty() || l_lastDashNo.isEmpty())
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("EmptyDashRange.msg"),
                    SWT.ICON_WARNING | SWT.OK);
            
            return false;
        }
        
        int l_iFirstDashNo = Integer.parseInt(l_firstDashNo);
        int l_iLastDashNo = Integer.parseInt(l_lastDashNo);
        
        if (l_iFirstDashNo == 0)
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("ValidDashRangeWithin.msg"),
                    SWT.ICON_WARNING | SWT.OK);
            
            return false;
        }
        else if (l_iFirstDashNo > l_iLastDashNo || l_iFirstDashNo == l_iLastDashNo)
        {
            getMessage(m_registry.getString("Warning.text"), m_registry.getString("ValidDashRangeHigher.msg"),
                    SWT.ICON_WARNING | SWT.OK);
            
            return false;
        }
        else
        {
            return true;
        }
    }
    
    private String formatDashNo(String l_dashNo)
    {
        String l_tempDashNo = "";
        
        if (l_dashNo.length() == 1)
        {
            l_tempDashNo = "0" + "0" + l_dashNo;
        }
        else if (l_dashNo.length() == 2)
        {
            l_tempDashNo = "0" + l_dashNo;
        }
        else
        {
            l_tempDashNo = l_dashNo;
        }
        
        return l_tempDashNo;
    }
    
    private void addRow(String dashNo)
    {
        Object[] rowData = new Object[] { dashNo, new String[]{""}, "", "", "", "", "", false, false, false };
        
        m_dashTable.addRows(rowData);
    }
    
    private void doTextValidation(VerifyEvent event, Text m_tempText)
    {
        for (char c : event.text.toCharArray())
        {
            if (!Character.isDigit(c) && event.keyCode != SWT.KEYPAD_SUBTRACT)
            {
                event.doit = false;
            }
            else if ((m_tempText.getCharCount() == ADD_DASH_TEXT_CHAR_LIMIT && m_tempText.getSelectionText().isEmpty())
                    || event.keyCode == SWT.KEYPAD_SUBTRACT)
            {
                event.doit = false;
                
                getMessage(m_registry.getString("Warning.text"), m_registry.getString("ValidDashRangeWithin.msg"),
                        SWT.ICON_WARNING | SWT.OK);
            }
            else
            {
                event.doit = true;
            }
        }
    }
    
    private void createTableUI(Composite parent)
    {
        Composite l_tablePanel = new Composite(parent, SWT.EMBEDDED);
        l_tablePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, HORIZONTAL_SPAN_FOR_TABLE_COMP));
        
        m_dashTable = createTable();
     
        configureTable();
        
        ScrollPagePane tablePane = new ScrollPagePane(m_dashTable);
        
        SWTUIUtilities.embed(l_tablePanel, tablePane, false);
    }

    protected void configureTable()
    {
        TableColumn l_column = m_dashTable.getColumnModel().getColumn(0);
        m_defaultCellRenderer = l_column.getHeaderRenderer();
        hideColumns(m_dashTable);
        m_dashTable.sortColumn(0);
    }
    
    private TCTable createTable()
    {
        String l_boType = getBoType();
        
        if (null != NewItemCreationHelper.getSelectedType())
        {
            String[] l_columnNames = null;
            String[] l_columnIds = null;
            
            if (!NewItemCreationHelper.getSelectedType().equalsIgnoreCase(
                    m_registry.getString("EnggPurchasedType.NAME"))
                    && !NewItemCreationHelper.getSelectedType().equalsIgnoreCase(
                            m_registry.getString("NonEnggPurchasedType.Name")))
            {
                l_columnNames = m_registry.getString("DashTableColPart.NAME").split(",");
                l_columnIds = m_registry.getString("DashTableColPart.IDs").split(",");
            }
            else if(l_boType.equalsIgnoreCase(NationalOilwell.PART_BO_TYPE))
            {
                l_columnNames = m_registry.getString("DashTableColPurchasePart.NAME").split(",");
                l_columnIds = m_registry.getString("DashTableColNov4PurchasePart.IDs").split(",");
            }
            else if(l_boType.equalsIgnoreCase(NationalOilwell.NON_ENGINEERING))
            {
                l_columnNames = m_registry.getString("DashTableColPurchasePart.NAME").split(",");
                l_columnIds = m_registry.getString("DashTableColNon-EngPurchasePart.IDs").split(",");
            }
            
            TCTable l_dashTable = new DashInfoModifyTable(l_columnIds, l_columnNames);
            
            return l_dashTable;
        }
        
        return null;
    }
    
    private String getBoType()
    {
        String boType = null;
        Map<String, IPropertyMap> dashPropMap = RSOneDashPropertyMap.getDashPropertyMap();
        Set<String> sdashNumbers = dashPropMap.keySet();
        String [] adashNumbers = sdashNumbers.toArray(new String[sdashNumbers.size()]);
        IPropertyMap propMap = null;
        for(String theDashNumber : adashNumbers)
        {
            propMap = dashPropMap.get(theDashNumber);
            
            if(propMap != null)
            {
                boType = propMap.getType();
            }
        }
        
        return boType;
    }

    private void hideColumns(TCTable dashTable)
    {
        TableUtils.hideTableColumn(dashTable,
                TableUtils.getColumnIndex("IMAN_master_form.nov4rsone_lotcontrol", dashTable));
        TableUtils.hideTableColumn(dashTable, TableUtils.getColumnIndex("IMAN_master_form.rsone_serialize", dashTable));
        TableUtils.hideTableColumn(dashTable,
                TableUtils.getColumnIndex("IMAN_master_form.nov4rsone_qarequired", dashTable));
    }
    
    private void setFields()
    {
        m_addSingleDashNoBtn.setSelection(true);
        m_addSingleDashNoBtn.notifyListeners(SWT.Selection, new Event());
    }
    
    private void copyData()
    {
        if (m_dashTable.getSelectedRow() != -1)
        {
            int returnCode = getMessage(m_registry.getString("Confirm.MSG"),
                    m_registry.getString("AllAttrCopied.WARNING"), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
            
            if (returnCode == SWT.YES)
            {
                stopCellEditing();
                copyEntireRowData();
                MessageBox.post(m_registry.getString("AllAttributeCopy.MSG"), m_registry.getString("Information.MSG"),
                        MessageBox.INFORMATION);
            }
            m_copyAllBtn.setSelection(false);
        }
        else
        {
            MessageBox.post(m_registry.getString("SelectRow.MSG"), m_registry.getString("Information.MSG"),
                    MessageBox.INFORMATION);
            m_copyAllBtn.setSelection(false);
        }
    }
    
    private void copyEntireRowData()
    {
        Object[] rowData = m_dashTable.getRowData(m_dashTable.getSelectedRow());
        
        for (int l_columnNo = 0; l_columnNo < m_dashTable.getModel().getColumnCount(); l_columnNo++)
        {
            for (int l_rowNo = 0; l_rowNo < m_dashTable.getModel().getRowCount(); l_rowNo++)
            {
                if (m_dashTable.isCellEditable(l_rowNo, l_columnNo))
                {
                    m_dashTable.getModel().setValueAt(rowData[l_columnNo], l_rowNo, l_columnNo);
                }
            }
        }
    }
    
    protected void stopCellEditing()
    {
        int iEditingRow = m_dashTable.getEditingRow();
        int iEditingColumn = m_dashTable.getEditingColumn();
        
        if ((iEditingRow != -1) && (iEditingColumn != -1))
        {
            m_dashTable.getCellEditor(iEditingRow, iEditingColumn).stopCellEditing();
        }
    }
    
    private int getMessage(String theText, String theMessage, int style)
    {
        org.eclipse.swt.widgets.MessageBox dialog = new org.eclipse.swt.widgets.MessageBox(new Shell(), style);
        dialog.setText(theText);
        dialog.setMessage(theMessage);
        int returnCode = dialog.open();
        
        return returnCode;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    public void loadTable()
    {
        Map<String, IPropertyMap> dashPropertyMap = RSOneDashPropertyMap.getDashPropertyMap();
        
        Set<String> dashNumbers = dashPropertyMap.keySet();
        
        m_dashNoList.addAll(dashNumbers);
        // SortedDashList
        Collections.sort(m_dashNoList);
        
        // Collection<IPropertyMap> propMaps = dashPropertyMap.values();
        List<IPropertyMap> propMaps = new ArrayList<IPropertyMap>();
        for (String dashNumber : m_dashNoList)
        {
            propMaps.add(dashPropertyMap.get(dashNumber));
        }
        
        IPropertyMap[] l_propMapArray = new IPropertyMap[propMaps.size()];
        l_propMapArray = propMaps.toArray(l_propMapArray);
        
        /*
         * for (int inx = 0; inx < dashNumbers.size(); inx++) {
         * l_propMapArray[inx].setString("part_dash_no", (String)
         * sortedList.toArray()[inx]); }
         */
        
        try
        {
            TableUtils.populateTable(l_propMapArray, m_dashTable);
            
            loadDashNumbers(m_dashNoList);
            
            setButtons();
            
            setHeaderRenderer();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void setButtons()
    {
        Display.getDefault().asyncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (m_dashTable.getRowCount() > 1)
                {
                    m_copyAllBtn.setVisible(true);
                    m_removeRowBtn.setVisible(true);
                }
                else
                {
                    m_copyAllBtn.setVisible(false);
                    m_removeRowBtn.setVisible(false);
                }
            }
        });
    }
    
    private void loadDashNumbers(List<String> sortedList)
    {
        for (int inx = 0; inx < sortedList.size(); inx++)
        {
            m_dashTable.setValueAt(sortedList.get(inx), inx, 0);
        }
    }
    
    public Map<String, IPropertyMap> saveTable()
    {
        stopCellEditing();
        
        Map<String, IPropertyMap> l_dashPropertyMap = new HashMap<String, IPropertyMap>();
        
        IPropertyMap[] l_propMapArray = TableUtils.populateMap(m_dashTable, new String[] { "part_dash_no",
                "traceability" });
        
        String[] dashNumbers = getDashNumbers();
        List<String> sortedList = Arrays.asList(dashNumbers);
        Collections.sort(sortedList);
        
        // Collection<IPropertyMap> propMaps = dashPropertyMap.values();
        int inx = 0;
        for (String dashNumber : sortedList)
        {
            l_dashPropertyMap.put(dashNumber, l_propMapArray[inx]);
            inx++;
        }
        
        RSOneDashPropertyMap.setDashPropertyMap(l_dashPropertyMap);
        
        return l_dashPropertyMap;
    }
    
    protected String[] getDashNumbers()
    {
        int l_rowCount = m_dashTable.getRowCount();
        
        String[] l_dashNumbers = new String[l_rowCount];
        
        for (int rowIndex = 0; rowIndex < l_rowCount; rowIndex++)
        {
            l_dashNumbers[rowIndex] = (String) m_dashTable.getValueAt(rowIndex, 0);
        }
        return l_dashNumbers;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private TableCellRenderer m_defaultCellRenderer;
    protected Button m_copyAllBtn;
    
    private Label m_dashLabel;
    
    private Button m_removeRowBtn;
    private Button m_addSingleDashNoBtn;
    private Button m_addMultiDashNoBtn;
    private Button m_addBtn;
    
    private Text m_rangeTextStart;
    private Text m_rangeTextEnd;
    
    protected List<String> m_dashNoList = new ArrayList<String>();
    
    protected Registry m_registry;
    
    protected TCTable m_dashTable;
    
    private static final int HORIZONTAL_SPAN_FOR_TABLE_COMP = 20;
    private static final int HORIZONTAL_SPAN_FOR_RIGHT_COMP = 6;
    private static final int HORIZONTAL_SPAN_FOR_LEFT_COMP = 5;
    private static final int HORIZONTAL_SPAN_FOR_EMPTY_LABEL = 3;
    
    private static final int ADD_DASH_TEXT_CHAR_LIMIT = 3;
    
}
