package com.nov.rac.item.dialog.downhole;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NOVSiteComposite extends AbstractUIPanel implements IPublisher
{
    
    public NOVSiteComposite(Composite parent, int style)
    {
        super(parent, style);
        m_registry = getRegistry();
        m_session = getSession();
    }
    
    /**
     * Gets the registry.
     * 
     * @return the registry
     */
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.dialog.dialog");
    }
    
    protected TCSession getSession()
    {
        return (TCSession) AIFUtility.getCurrentApplication().getSession();
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite siteComposite = getComposite();
        siteComposite.setLayout(new GridLayout(1, false));
        
        m_listSite = new List(siteComposite, SWT.BORDER | SWT.MULTI);
        GridData gd_listSite = new GridData(SWT.CENTER, SWT.CENTER, false,
                true, 1, 4);
        m_listSite.setLayoutData(gd_listSite);
        
        TCComponentListOfValues lovValues = TCComponentListOfValuesType
                .findLOVByName(getSession(), NationalOilwell.DH_SITE_LOV);
        ListOfValuesInfo lovInfo;
        String[] lovS = null;
        try
        {
            lovInfo = lovValues.getListOfValues();
            lovS = lovInfo.getStringListOfValues();
            
            for (int i = 0; i < lovS.length; i++)
            {
                m_listSite.add(lovS[i], i);
            }
        }
        
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        addSelectionListenerOnSiteList(lovS);
        
        return true;
    }
    
    private void addSelectionListenerOnSiteList(final String[] lovS)
    {
        m_listSite.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                
                int[] selection = m_listSite.getSelectionIndices();
                
                String[] siteVal = new String[selection.length];
                
                for (int i = 0; i < selection.length; i++)
                {
                    siteVal[i] = lovS[selection[i]];
                }
                
                System.out.println(siteVal);
                
                final IController controller = ControllerFactory.getInstance()
                        .getDefaultController();
                
                final PublishEvent event = new PublishEvent(getPublisher(),
                        NationalOilwell.MSG_SITE_LIST_SELECTION_EVENT, siteVal,
                        null);
                
                controller.publish(event);
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionEvent)
            {
                // TODO Auto-generated method stub
            }
        });
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    private Registry m_registry = null;
    private TCSession m_session;
    private List m_listSite;
}
