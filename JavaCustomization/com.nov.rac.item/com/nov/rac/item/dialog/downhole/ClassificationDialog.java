package com.nov.rac.item.dialog.downhole;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.TreePath;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.NationalOilwell;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.item.dialog.NOVClassificationDialog;
import com.nov.rac.item.helpers.ClassificationDialogHelper;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.classification.common.G4MInClassDialog;
import com.teamcenter.rac.classification.common.tree.G4MTreeNode;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class ClassificationDialog extends NOVClassificationDialog {

	public ClassificationDialog(Shell shell, Text m_classText) 
	{
		super(shell, m_classText);
		
		setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE
				| SWT.RESIZE);
	}

	@Override
	protected Control createDialogArea(Composite composite)
	{
		super.createSearchClassRow(composite);
		
		m_composite = new Composite(composite, SWT.EMBEDDED);

		m_composite.setLayout(new GridLayout(1, true));

		m_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
				1, 1));

		m_app = ClassificationDialogHelper.getClassificationApp();

		m_compContext = new G4MInClassDialog(m_app);

		m_compContext.dispose();
		m_classificationtree = m_compContext.getTree().getTree();

		G4MTreeNode currentNode = m_compContext.getTree().getRootNode();

		if (currentNode != null) {
			TreePath tracePath = new TreePath(currentNode.getPath());
			if (tracePath != null) {
				m_classificationtree.expandPath(tracePath);
			}

			currentNode = searchNode("DHT", currentNode);

			m_compContext.getTree().setRootNode(currentNode, true);
			m_compContext.getTree().repaint();
			m_classificationtree = m_compContext.getTree().getTree();
			filterClassNodes(currentNode);
		}

		classTreePane = new ScrollPagePane(m_classificationtree);

		SWTUIUtilities.embed(m_composite, classTreePane, false);

		addListenerOnClassificationRoot();
		
		super.addSelectionListenerOnSearchBtn();
		
		super.createButtonBar(composite);

		return composite;

	}
	
	@Override
	protected void okPressed() {
		TreePath path = m_classificationtree.getSelectionPath();
		G4MTreeNode node = (G4MTreeNode) m_classificationtree
				.getLastSelectedPathComponent();
		setSelectedNode(path, node);
	}

	public void filterClassNodes(G4MTreeNode node) {
		Vector<Object> partSubTypeClasses = new Vector<Object>();
		Vector<Object> nodeNamesToRemove = new Vector<Object>();

		partSubTypeClasses = getPartSubTypeValues();

		Enumeration enum1 = node.children();
		while (enum1.hasMoreElements()) {
			G4MTreeNode chNodeG4M1 = (G4MTreeNode) enum1.nextElement();
			String chNodeNameStr = chNodeG4M1.getUserObject().toString();

			if (partSubTypeClasses.indexOf(chNodeNameStr) == -1) {
				nodeNamesToRemove.add(chNodeG4M1);
			}
		}

		for (int kj = 0; kj < nodeNamesToRemove.size(); kj++) {
			G4MTreeNode chNodeG4M1 = (G4MTreeNode) nodeNamesToRemove.get(kj);
			chNodeG4M1.removeFromParent();
			chNodeG4M1.setParent(null);
			chNodeG4M1 = null;
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				m_classificationtree.repaint();
				m_classificationtree.validate();
				m_classificationtree.updateUI();
			}
		});

	}

	private Vector<Object> getPartSubTypeValues() {

		Vector<Object> dhlItemTypes = LOVUtils
				.getListOfValuesforLOV(NationalOilwell.DH_ITEM_TYPES
						+ NewItemCreationHelper.getCurrentGroup().substring(0,
								2));

		return dhlItemTypes;
	}

	private void addListenerOnClassificationRoot() {
		m_classificationtree.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				if (me.getClickCount() == 2) {
					TreePath path = m_classificationtree.getSelectionPath();
					G4MTreeNode node = (G4MTreeNode) m_classificationtree
							.getLastSelectedPathComponent();
					setSelectedNode(path, node);
				}
			}
		});

		m_classificationtree
				.addTreeExpansionListener(new TreeExpansionListener() {
					public void treeCollapsed(
							TreeExpansionEvent treeExpansionEvent) {
					}

					public void treeExpanded(
							TreeExpansionEvent treeExpansionEvent) {

						TreePath path = treeExpansionEvent.getPath();

						G4MTreeNode curNode = (G4MTreeNode) path
								.getLastPathComponent();
						setSelectedNode(path, curNode);
					}
				});
	}

	protected void setSelectedNode(TreePath path,
			G4MTreeNode node) {

		Object[] objs = path.getPath();
		String selPath = "";
		String selStr = "";
		for (int i = 1; i < objs.length; i++) {
			if (objs[i].toString().contains("[")) {
				selStr = objs[i].toString().substring(0,
						objs[i].toString().indexOf("["));
			} else {
				selStr = objs[i].toString();
			}
			if (selPath.trim().length() > 0) {
				selPath = selPath + "->" + selStr.trim();
				// selPath = selStr.trim();
			} else {
				selPath = selStr.trim();
			}

		}
		if (node != null) {
			if (node.getICSDescriptor().isStorageClass()/*
														 * descript.
														 * isStorageClass()
														 */) {
				try {

					IController theController = ControllerFactory.getInstance()
							.getDefaultController();
					theController.publish(new PublishEvent(this,
							NationalOilwell.SELECTEDCLASS, selPath, ""));
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							getShell().close();
						}
					});

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public G4MTreeNode searchNode(String nodeStr, G4MTreeNode currentNode) {
		G4MTreeNode node = null;
		// Get the enumeration
		Enumeration<?> enu = currentNode.children();
		// iterate through the enumeration
		while (enu.hasMoreElements()) {
			String noArryNodeStrng = null;
			// get the node
			node = (G4MTreeNode) enu.nextElement();
			noArryNodeStrng = noArrayString(node.getUserObject().toString());
			if (nodeStr != null && noArryNodeStrng != null
					&& nodeStr.equals(noArryNodeStrng)) {
				// tree node with string found
				return node;
			}
		}
		// tree node with string node found return null
		return null;
	}

	public String noArrayString(String nodeString) {
		String noArryStrng = null;
		if (nodeString != null && nodeString.endsWith("]")) {
			int arrayCheck = nodeString.indexOf("[");
			String noArryNode = nodeString.substring(0, arrayCheck);
			noArryStrng = noArryNode.trim();
		} else {
			noArryStrng = nodeString;
		}
		return noArryStrng;
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		return null;
	}

	public void setLocation(Point location, Point size) {
		m_dialogLocation = location;

		m_classBtnSize = size;
	}

	protected Point getInitialLocation(Point point) {
		Point size = getInitialSize();

		Point location = new Point(m_dialogLocation.x - size.x,
				m_dialogLocation.y - size.y);

		if (location.x < 0) {
			location = new Point(m_dialogLocation.x + m_classBtnSize.x,
					m_dialogLocation.y - size.y);
		}
		return location;
	}

	@Override
	protected Point getInitialSize() {
		Monitor theMonitor = m_composite.getMonitor();

		Rectangle screenSize = theMonitor.getClientArea();

		int x = screenSize.width / 4;

		int y = screenSize.height / 3;

		Point location = new Point(x, y);

		return location;

	}

	private Composite m_composite;
	private AbstractAIFUIApplication m_app;
	Point m_dialogLocation;
	Point m_classBtnSize;
}
