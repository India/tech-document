package com.nov.rac.item.dialog.downhole;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVSiteDialog extends AbstractSWTDialog implements IPublisher
{

    private Point m_initialLocation;
    
    public NOVSiteDialog(Shell shell, int style)
    {
        super(shell, style);
        m_registry = Registry.getRegistry("com.nov.rac.item.dialog.downhole.downhole");
        setShellStyle(SWT.CENTER);
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        m_composite = parent;
        
        m_siteComposite = new NOVSiteComposite(parent, SWT.NONE);
        try
        {
            m_siteComposite.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return parent;
    }
    
    @Override
    protected Control createButtonBar(Composite parent)
    {
        Composite buttonComposite = new Composite(parent, SWT.NONE);
        buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        buttonComposite.setLayout(new GridLayout(1, true));
        
        m_btnOk = new Button(buttonComposite, SWT.PUSH);
        m_btnOk.setText(m_registry.getString("btnSiteOk.NAME"));
        m_btnOk.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_btnOk.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent selectionEvent)
            {
                //publishEvent();
                close();
            }
        });
        
        return null;
    }
    
    @Override
    protected Point getInitialLocation(Point point)
    {
        Point location = m_initialLocation;
        
        if(location == null)
        {
            Monitor theMonitor = m_composite.getMonitor();
            Rectangle screenSize = theMonitor.getClientArea();
            
            int x = screenSize.width / HORZONTAL_FACTOR;
            int y = screenSize.height / VERTICAL_FACTOR;
            location = new Point(x, y);
            
        }        
        
        return location;
    }
    
    public void setInitialLocation(Point thePoint)
    {
        m_initialLocation = thePoint;
    }
    
    public Point getInitialDialogSize()
    {
		return getInitialSize();
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    private Registry m_registry = null;
    private Composite m_composite = null;
    private NOVSiteComposite m_siteComposite;
    private Button m_btnOk;
    private static final Integer HORZONTAL_FACTOR = 4;
    private static final Integer VERTICAL_FACTOR = 4;
    
}
