package com.nov.rac.item.action;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.item.dialog.NewItemDialog;
import com.nov.rac.item.helpers.NewItemCreationHelper;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;

public class NewItemCreationHandler extends AbstractHandler
{
    /*
     * private NewItemDialog newItemDialog = null; public
     * NewItemCreationHandler() { super(); } public Object
     * executeNID(ExecutionEvent event) throws ExecutionException { if(null ==
     * newItemDialog || newItemDialog.getReturnCode() == 1 ) { IWorkbenchWindow
     * window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
     * newItemDialog = new NewItemDialog(window.getShell());
     * newItemDialog.open(); } else { if(null != newItemDialog.getShell()) {
     * newItemDialog.getShell().setFocus(); } } if(null == newItemDialog ) {
     * IWorkbenchWindow window =
     * HandlerUtil.getActiveWorkbenchWindowChecked(event); newItemDialog = new
     * NewItemDialog(window.getShell()); newItemDialog.open(); } else {
     * if(newItemDialog.getReturnCode() == 1 ) { newItemDialog.open(); } }
     * return null; } public Object execute(ExecutionEvent arg0) throws
     * ExecutionException { // Check if logged in group is Mission. // if yes,
     * then call executeNID. //String businessGroup = (String
     * )NewItemCreationHelper.getBusinessGroup(); TCComponent targetComponent =
     * null; TCComponent targetContainer = null; String selectedItem = null;
     * InterfaceAIFComponent[] selectedComponents = (InterfaceAIFComponent[])
     * AifrcpPlugin.getSelectionMediatorService().getTargetComponents();
     * if(selectedComponents != null && selectedComponents.length > 0) {
     * targetComponent = (TCComponent)selectedComponents[0]; //
     * NewItemCreationHelper.setSelectedComponent(targetComponent);
     * NewItemCreationHelper.setSelectedContainer(targetComponent); } String
     * businessGroup = (String )GroupUtils.getBusinessGroupName(); if(null !=
     * businessGroup && businessGroup.equals("Mission")) { try {
     * NewItemCreationHelper.validateTargetComponent(); executeNID(arg0); }
     * catch (TCException e) { // e.printStackTrace(); MessageBox.post(e); } }
     * else { super.execute(arg0); } return null; }
     */
    
    /*
     * public Object execute(ExecutionEvent event) throws ExecutionException {
     * IWorkbenchWindow window =
     * HandlerUtil.getActiveWorkbenchWindowChecked(event); NewItemDialog
     * newItemDialog = new NewItemDialog(window.getShell());
     * newItemDialog.open(); return null; }
     */
    
    public NewItemCreationHandler()
    {
        
    }
    
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        TCComponent targetComponent = null;
        
        NewItemCreationHelper.Initializer();
        
        InterfaceAIFComponent[] selectedComponents = (InterfaceAIFComponent[]) AIFUtility.getTargetComponents();
        
        if (selectedComponents != null && selectedComponents.length > 0)
        {
            targetComponent = (TCComponent) selectedComponents[0];
            // NewItemCreationHelper.setSelectedComponent(targetComponent);
            NewItemCreationHelper.setSelectedContainer(targetComponent);
        }
        
        IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        
        NewItemDialog newItemDialog = new NewItemDialog(window.getShell());
        
        newItemDialog.open();
        
        return null;
    }
    
}