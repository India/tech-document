package com.nov;

/**
 * <p>
 * Description: Static variables for Site
 * </p>
 */

//

// /////////////////////////////////////////////////////
// $Date: 20 Aug 2012
// $Author: Sneha C $
// /////////////////////////////////////////////////////

//


public class NationalOilwell
{
    public final static String ITEMTYPELOV = "ValidItemTypes_LOV";
    public final static String DOCTYPELOV = "ValidDocumentTypes_LOV";
	public final static String DOCUMENT_TYPES_LOV = "ValidDocumentTypes_LOV";
    public final static String SUBTYPE_LOV = "_SubType_LOV";
    public final static String DATASETTYPELOV = "ValidDatasetTypes_LOV";
    
    public final static String DSTEMPLATE_USERSERVICE = "NATOIL_getDatasetTemplatesUS";
    public final static String DOCCATEGORY_TYPE = "DocCategory_Type";
    public final static String NR_USERSERVICE = "NATOIL_isNamedRefOkUS";
    public final static String DATASET_TYPE = "dataset_type";
    public final static String DOCUMENT_TYPE = "Documents";
    public final static String DATASET_BO_TYPE = "Dataset";
    
    public final static String UOMLOV = "_RSOne_UnitsOfMeasure_";
    public final static String GRPUOM = "MIS";
    public final static String PRODUCTLINELOV = "Nov4_Mission_Product_Line";
    public final static String PRODUCTCODELOV = "Nov4_Mission_Product_Code";
    public final static String MISSIONTYPELOV = "Nov4_MISSION_Item_Types";
    
    public final static String NEXT_ID_USERSERVICE = "NATOIL_getNextIdUS";
    public final static String SEEADMIN = "See your TCEng Administrator";
    
    public final static String OPERATION = "operation";
    public final static String ITEM_TYPE = "itemType";
    public static final String OPERATIONS = "OPERATIONS";
    public static final String DOTSEPARATOR = ".";
    public static final String NAME = "Name";
    public static final String COMMASEPARATOR = ",";
    public static final String NOOPERATION = "No Operation Found";
    
    public static final String RSONE_ITEMTYPE = "rsone_itemtype";
    public static final String MIS_TYPE = "nov4_mis_type";
    public static final String ITEM_ID = "item_id";
    public static final String NOV_UOM = "uom_tag";
    public static final String OBJECT_NAME = "object_name";
    public static final String OBJECT_DESC = "object_desc";
    public static final String NOV4PRODUCTLINE = "nov4_productline";
    public static final String NOV4PRODUCTCODE = "nov4_productcode";
    public static final String NAME2 = "Name2";
    public static final String MIS_SERIALIZE = "nov4_mis_serialize";
    public static final String REVISION_LIST = "revision_list";
    public static final String RELATED_DEFINING_DOCUMENT = "RelatedDefiningDocument";
    public static final String RELATED_DOCUMENT = "RelatedDocuments";
    public static final String REVISION = "revision";
    public static final String ITEM_REVISION_ID = "item_revision_id";
    public static final String IMAN_SPECIFICATION = "IMAN_specification";
    public static final String OBJECT_TYPE = "object_type";
    public static final String ITEMREVISIONID = "item_revision_id";
    public static final String DIALOG_RELOAD = "Reload";
    public static final String DOCUMENTS_CATEGORY = "DocumentsCAT";
    public static final String DOCUMENTS_TYPE = "DocumentsType";
    public static final String PULL_DRAWING = "PullDrawing";
    public static final String SELECTED_ITEM = "SelectedItem";
    public static final int REV_TEXT_LIMIT = 2;
    public static final String ONAPPLY_OBJECT_CREATED = "object_created";
    public static final String CreateRDDOperation = "CreateRelatedDefiningDocument";
    public static final String STATUS_STANDARD = "Standard";
    public static final String COPY_DOC = "CopyDoc";
    
    // Legacy Item Create
    public static final String GROUP_ID = "owning_group";
    public static final String TYPE = "Type";
    public static final String BASE_NUMBER = "Base_Number";
    public final static String GRPUOMLEG = "TOT";
    public static final String SEQUENCE = "Sequence";
    public static final String MSG_LEGACY_CHECK_BTN_SELECTION_EVENT = "LegacyCheckButtonSelectionEvent";
    public static final String MSG_ITEM_TYPE_LOVPOPUP_BTN_SELECTION_EVENT = "ItemTypeLovpoupButtonSelectionEvent";
    public static final String MSG_ITEM_TYPE_LOVPOPUP_BTN_PROPERTY_CHANGE_EVENT = "ItemTypeLovpoupButtonPropertyChangeEvent";
    // public static final String MSG_ITEMTYPE_POPUP_BTN_SELECTION_EVENT =
    // "ItemTypeLovpoupButtonSelectionEvent";
    public static final String LEGACY_DATASET_TYPE = "legacy_dataset_type";
    public static final String LEGACY_ITEM_TYPE_EVENT = "legacy_item_type_event";
    public static final String NUMBERED_PRODUCT_BO_TYPE = "Numbered Product";
    public static final String PRODUCTCATEGORY_BO_TYPE = "ProductCategory";
    public static final String PRODUCTC_BO_TYPE = "Product";
    public static final String D_DOCUMENTS = "D - Documents";
    public static final String S_PRODUCT = "S - Product";
    public static final String M_PART = "M - Part";
    public static final String N_NUMBERED_PRODUCT = "N - Numbered Product";
    public static final String P_PURCHASED_PART = "P - Purchased Part";
    public static final String MSG_BASE_NUMBER_TEXT_MODIFY_EVENT = "BaseNumberTextModifyEvent";
    public static final String MSG_REV_TEXT_MODIFY_EVENT = "RevTextModifyEvent";
    public static final int BASE_TEXT_LIMIT = 15;
    public static final int DESC_TEXT_LIMIT = 130;
    public static final int SEQ_TEXT_LIMIT = 3;
	public static final Integer DEFAULT_COUNTER_START_VALUE = 1000000;
	public static final String DEFAULT_REV_ID = "01";

	// DH Item Create
    public final static String UOM_FILTER_DHT = "DHT";
    public final static String DOC_TYPE_LOV_FOR_DHL = "_DHL_ValidDocumentTypes_LOV";
    public final static String DH_SITE_LOV = "DH_Sites";
    public final static String MSG_SITE_LIST_SELECTION_EVENT = "SiteListSelectionEvent";
    public static final String ICS_SUBCLASS_NAME = "ics_subclass_name";
    public static final String SITES = "Sites";
    public static final String DH_DATASET_TYPE = "downhole_dataset_type";
    public static final String SECURE = "secure";
    public static final String DOCCAT_CAL = "CAL";
    public static final String PREF_DHL_ITEMID_EDITABLE_ITEM_TYPES = "DHL_ItemId_Editable_Item_Types";
    
    public static final String LOV_NOVEngineeringParts = "_RSOne_EngControlled_";
    public static final String NOVNon_EngineeringPartsLOV = "_RSOne_NonEngControlled_";
    public static final String ListOfValues = "ListOfValues";
    public static final String RSONE_PARTTYPE = "rsone_partType";
    public static final int COMBO_FIELD_SIZE = 50;
    public static final int COMBO_DEFAULT_VISIBLE_ITEMS = 8;
    
    public static final int HORIZONTAL_INDENT_FOR_BUTTON = 6;
    
    public static final String CREATE_NEW_RDD = "CreateNewRDD";
    public static final String COPY_RDD = "CopyDocRDD";
    
    public static final String DATASET_FILE_PATH = "datasetFilePath";
    
    public static final String PART_BO_TYPE = "Nov4Part";
    
    public static final String NON_ENGINEERING = "Non-Engineering";
    
    public static final String CREATE_DOCUMENT = "CreateDocument";
    
    // RSOne
    public static final String RSOne_ORG_LOV = "_RSOne_org";
    public static final String NOHELP = "Help file cannot be located.";
    public static final String SELECTEDCLASS = "selectedClass";
    public static final String RSONE_ORG = "rsone_org";
    public final static String SEND_REQUESTMFG_MAIL_USERSERVICE = "NATOIL_Send_RequestMfg_eMail";
    
    // RSOne
    public static final String RSONE_TRACEABILITY_STRING = "traceability";
    
    public static final String RSONE_LOTCONTROL = "nov4rsone_lotcontrol";
    public static final String RSONE_SERIALIZE = "rsone_serialize";
    public static final String RSONE_QAREQUIRED = "nov4rsone_qarequired";
    public static final String ICS_subclass_name = "ics_subclass_name";
    public static final String RSONE_UOM = "rsone_uom";
    public static final String RSONE_SAVE_DASH_PROPERTMAP = "RSOne_save_dash_propertyMap";
    public static final String RSONE_LOAD_DASH_PROPERTMAP = "RSOne_load_dash_propertyMap";
    public static final String DASH_PROPERTYMAP_MODIFY_EVENT = "dash_propertymap_modify_event";
    public static final String RSONE_DATASET_TYPE = "RSOne_dataset_type";
    public static final String RSONE_ITEM_TYPE_EVENT = "rsone_item_type_event";
    public static final String ICS_classified = "ics_classified";
    public static final String PART_TYPE_CHANGE_EVENT = "part_type_change_event";
    public static final String RSONE_COPY_PROPERTY = "RSOne_copy_property";
    public static final String RSONE_COPY_ALL_PROPERTY = "RSOne_copy_all_property";
    public static final String MSG_MODIFY_AND_TABLE_BUTTON_DISABLED = "Modify_and_table_button_disabled";
    public static final String RSONE_BASE_NUMBER = "Base_Number";
    
    public static final String MSG_DASH_NUMBER_MODIFY_EVENT = "RSOne_dash_number_modify";
    public static final String MSG_DASH_NUMBER_SELECTION_EVENT = "RSOne_dash_number_selection";
    public static final String MSG_MODIFY_PANEL_DONE_BUTTON_SELECTION = "RSOne_done_button_selection";
    public static final String MSG_DASH_NUMBER_TABLE_EVENT = "RSOne_dash_number_table";
    
    public static final String DASH_TABLE_SET_COLUMN_EVENT = "Dash_Table_column_index_set";
    public static final String MSG_ASSIGN_ORACLE_ID_SELECTED = "Assign_oracle_id_checkbox_selected";
    
    public static final String SEND_TO_CLASSIFICATION = "send_to_classification";
    public static final String CLASSIFICATION_EVENT = "CLASSIFICATION_EVENT";
    public static final String ADD_CID_EVENT = "add_cid_event";
    public static final String RSONE_ENGCONTROLLED = "rsone_engcontrolled";
    public static final String RSONE_TABLE_OK_BUTTON_EVENT = "RSOne_table_ok_button";
    public static final String UPDATE_DIALOG_AREA = "update_dialog_area";
    
    public static final int NAME_TEXT_LIMIT = 32;
    public static final int RSONE_DESCRIPTION_TEXT_LIMIT = 240;
    public static final String MFGNAMES = "MfgNames";
    
    public static final String MANUFACTURER_NAME = "manufacturer_name";
    public static final String MANUFACTURER_PARTNUMBER = "manufacturer_partnumber";
    public static final String NOV4_MFG_NAME = "nov4_mfg_name";
    public static final String NOV4_MFG_NUMBER = "nov4_mfg_number";
    public static final String CLONE_DOC_SELECTION_EVENT = "clone_doc_selection_event";
    public static final String OPERATION_CHANGE_EVENT = "Opearation_Change_Event";
    public static final String OPERATION_ASSIGN_ORACLEID_TO_LEGACY_PART = "AssignOracleIDtoLegacyPart";
    public static final String OPERATION_ADD_DASH_NUMBERS = "AddDashNumber";
    public static final String SELECTED_ITEM_MODIFY_EVENT = "selected_item_modify_event";
	public static final String MODIFY_PANEL_DONE_BUTTON_SELECTION = "modify_panel_done_button_selection";
    public static final String EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_TYPE_PANEL = "AssignOracleIdToLegacyPartTypePanelLoadEvent";
    public static final String EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_INFO_PANEL = "AssignOracleIdToLegacyPartInfoPanelLoadEvent";
    public static final String EVENT_LOAD_ASSIGN_ORACLE_ID_TO_LEGACY_PART_DASH_PANEL = "AssignOracleIdToLegacyPartDashPanelLoadEvent";
    public static final String ALTERNATE_ID_LIST = "altid_list";
	public static final String COPY_DOCUMENT_EVENT = "Copy_Document_To_Copied_Part";
	public static final String CLASS_ROOT_RIGSOLUTIONS ="Rig Solutions";
	
	
	// Dual Item Create
	public static final String MSG_CREATE_NON_RSONE_ITEM_EVENT = "Create_Non_RSOne_Item_Event";
	public static final String MSG_CREATE_RSONE_ITEM_EVENT = "Create_RSOne_Item_Event";
	public static final String PREF_NOV_RS_DUAL_ITEM_CREATION_GROUPS = "_NOV_RS_dualitem_creation_groups_";
	
    public static final String LEGACY = "Legacy";
	public static final String LEGACY_RD_OR_RDD_SELECTED_MSG = "RD_Or_RDD_Selected_For_Legacy_Item_create";
    public static String PART = "Part";
    public static String PRODUCT_FOLDERS_LOV = "ProductFolders_LOV";
    public static String MSG_APPLY_BTN_SELECTION_EVENT = "apply_btn_selection_event";
	public static String DH_ITEM_TYPES = "_DHL_Item_Types_";
    public static String EVENT_LOAD_ADD_DASH_NUMBER_BASE_PANEL = "Add_Dash_Number_Panel_Base_Laoded";
    public static final String MSG_RSONE_DASH_TABLE_CLOSE_EVENT = "Table_Closed";
    public static final String MSG_RSONE_RELOAD_PANELS = "Reload_Dash_Numebrs";
    public static final String MSG_DASH_INFO_MODIFY_EVENT = "Modify_Dash_Info_Table";
    public static final String DASH_SELECTION_CHANGE_EVENT = "Dash_Selection_change_";
    public static final String MSG_DASH_NUMBER_MANDATORY = "Make_Dash_Number_Mandatory";
    
    public NationalOilwell()
    {
    }
}