package rs7dmUtils;

import com.rs7.services.rac.custom.RS7DataManagementService;
import com.rs7.services.rac.custom._2015_07.RS7DataManagement.*;
import com.rs7.services.rac.custom._2015_07.RS7DataManagement;
import com.rs7.services.rac.custom._2015_07.RS7DataManagement.CreateOrUpdateIn;
import com.rs7.services.rac.custom._2015_07.RS7DataManagement.CreateOrUpdateResponse;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;

public class RS7DatamgmtServiceUtils {
	
	
public RS7DataManagement getDataMgmtService() {
		
		// 1. create service stub for NOV4DataManagementService
		TCSession session = (TCSession) AIFUtility.getCurrentApplication()
				.getSession();

		RS7DataManagementService dmService = RS7DataManagementService
				.getService(session);

		return dmService;

	}


public void performOp() {
		
	CreateOrUpdateIn createIn  = new CreateOrUpdateIn();
	CreateDatasetIn  createDSIn = new CreateDatasetIn();
	
	//createIn.clientId = "TcEnt-Direct";
	createIn.clientId = "TcEnt";
	createIn.boTypeName = "R6_Material";
	createIn.partNumber= "8888.4444.10";
	createIn.rsRevision = "01.04";
	
	createIn.rsPaMaClass = "Ent Class" ;	
	createIn.rsPaMaAktAbt= "A";	
	createIn.projectName= "EEE";
	//createIn.rsPaMaAktAbt= "C";
	createIn.rsPaMaBennEn= "D";
	createIn.rsPaMaDesignType	= "Y_Des";
	createIn.rsPaMaErNam= "F";
	createIn.rsPaMaGefKla= "G";
	createIn.rsPaMaEstimYearEoL= "H";	
	createIn.rsPaMaGewichtsEinheit= "I";
	createIn.rsPaMaLabor= "ZZ";
	createIn.rsPaMaLackierbar= "K";
	createIn.rsPaMaLoetStd= "L";
	createIn.rsPaMaLoetStdTemp= "M";	
	createIn.rsPaMaMirManufacturer= "N";
	createIn.rsPaMaMirManufPartNumber= "O";	
	createIn.rsPaMaMstae= "P";
	createIn.rsPaMaMtArt= "Q";	
	createIn.rsPaMaRoHS= "R";
	createIn.rsPaMaSchilderArt= "S";
	createIn.rsPaMaTeilfam= "T";	
	createIn.rsPaMaVerfRicht= "U";
	createIn.rsPaMaVerwHin= "V";
	createIn.rsPaMaWaschbar= "W";
	createIn.rsPartWerkBenn= "X";
	createIn.rsPartWerkSnr= "Y";	
	createIn.rsSapStatus="Z";
	createIn.ownerName="AA";
	createIn.makeBuyIndicator = "Make";
	
	createIn.defaultUnitOfMeasure = "G";
	
	createIn.rsPaMaTempOben	= -1000;
	createIn.rsPaMaTempUnten = -1000;
	
	createIn.rsPaMaPreis= 1000;
	createIn.rsPaMaGewicht= 10000;
	
	createIn.frozenIndicator=true;
	createIn.rsPaMaMirCableLibrary= true;
	createIn.rsPaMaMirEcadLibrary= false;
	createIn.rsPaMaMirFlugzulassung= false;
	createIn.rsPaMaMirMcadLibrary	= true;		
	createIn.rsPaMaEssTauglich= true;
	createIn.r0IsVsnfD= true;
	
	//CreateOrUpdateResponse response = this.getDataMgmtService().createObject(createIn);
	CreateOrUpdateResponse response = this.getDataMgmtService().updateObject(createIn);
	//CreateDatsetResponse response = this.getDataMgmtService().createDataset(createDSIn);
	
	//ErrorStack msg = response.serviceData.getPartialError(0);
	//String outMsg = ErrorStack.class.getName();
	System.out.println("After Service call");
	
	response.serviceData.getPartialError(0);
	
	
}

}
