package com.nov.rac.common;

import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISaveablePart;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.form.viewer.NOVFormViewer;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.ui.factory.UIPanelFactory;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.event.ClientEventDispatcher;
import com.teamcenter.rac.util.event.IClientEvent;

public class Pane extends AbstractUIPanel implements INOVFormLoadSave, ISaveablePart
{
    
    private Registry m_registry = null;
    private UIPanelFactory m_panelFactory = null;
    private ArrayList<IUIPanel> m_panels = null;
    private final static String PANE = "Pane";
    private final static String REGISTRY = "REGISTRY";
    private static final String FORM_POPUP_DIALOG_DATA_MODEL = "FORM_POPUP_DIALOG_DATA_MODEL";
    public Pane(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected void setRegistry(Registry registry)
    {
        //String formType = FormHelper.getTargetComponentType();
       // String formRegistryString = registry.getString(formType+"."+REGISTRY);
       // m_registry = Registry.getRegistry(formRegistryString);
        m_registry = registry;
    }
    
    @Override 
    public boolean createUI() throws TCException
    {
        try
        {
            final Composite composite = getComposite();
            composite.setLayout(new GridLayout(1, true));
            composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                    true, 1, 1));
            
            m_panelFactory = UIPanelFactory.getInstance();
            m_panelFactory.setRegistry(getRegistry());
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            
            StringBuilder dialogType = new StringBuilder("");
            String formType = FormHelper.getTargetComponentType();
            
            dialogType.append(formType).append(".").append(PANE);
            
            registerDataModel();
            
            try
            {
                m_panels = m_panelFactory.createPanels(composite, dialogType.toString(),
                        theGroup);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }
    
    public boolean load(IPropertyMap propertyMap)
    {
        ArrayList<IUIPanel> filteredPanels = m_panels;
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof INOVFormLoadSave)
            {
                INOVFormLoadSave loadablepanel = (INOVFormLoadSave) panel;
                try
                {
                    loadablepanel.load(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        clearDataModel();
        
        return true;
    }
    
    protected void registerDataModel()
    {
        FormHelper.setDialogDataModelName(FORM_POPUP_DIALOG_DATA_MODEL);
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        dataBindingRegistry.registerDataModel(FormHelper.getDialogDataModelName(),
                new DataBindingModel());
        
    }
    
    private void clearDataModel()
    {
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        DataBindingModel dataModel = (DataBindingModel) dataBindingRegistry
                .getDataModel(FormHelper.getDialogDataModelName());
        dataModel.clear();
        
    }
    
    public void dispose()
    {
        clearDataModel();
        
        ArrayList<IUIPanel> iupanel = m_panels;
        if (null != iupanel && iupanel.size() > 0)
        {
            for (IUIPanel panels : iupanel)
            {
                panels.dispose();
            }
        }
        
        this.getComposite().dispose();
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        
    }

    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public void doSave(IProgressMonitor arg0)
    {
        IPropertyMap propertyMap = new SimplePropertyMap();
        ArrayList<IUIPanel> filteredPanels = m_panels;
        
        try
        {
            for (IUIPanel panel : filteredPanels)
            {
                if (panel instanceof INOVFormLoadSave)
                {
                    INOVFormLoadSave saveablepanel = (INOVFormLoadSave) panel;
                    
                    saveablepanel.save(propertyMap);
                    
                }
            }
            clearDataModel();
        }
        catch (Exception e)
        {
            MessageBox.post(null, e, "ERROR", MessageBox.ERROR);
        }
        
        
    }

    @Override
    public void doSaveAs()
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isDirty()
    {
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        DataBindingModel dataModel = (DataBindingModel) dataBindingRegistry
                .getDataModel(FormHelper.getDialogDataModelName());
        
        return dataModel.isDirty();
    }

    @Override
    public boolean isSaveAsAllowed()
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSaveOnCloseNeeded()
    {
        return true;
    }
    
    
}
