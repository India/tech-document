package com.nov.rac.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

/**
 * 
 * @author vasantha kumar 
 * This class is Creating LegendPanel and appended to
 *         ENForm PopUpDialog
 */
public class LegendPanel extends AbstractUIPanel
{
    
    private Registry m_registry = null;
    private static final int NUM_COLUMNS = 8;
    
    /*
     * LegendPanel Constructor
     */
    public LegendPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    /**
     * @wbp.parser.entryPoint
     * 
     *  
     */
    @Override
    public boolean createUI() throws TCException
    {
        Composite legendComposite = getComposite();
        
        if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            
            GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false,
                    1, 1);
            legendComposite.setLayoutData(gridData);
            legendComposite.setLayout(new GridLayout(NUM_COLUMNS, false));
            
            Image addedLegendImg = new Image(getComposite().getDisplay(),
                    m_registry.getImage("AddedLegend.IMAGE"), SWT.NONE);
            Label addedLegendImgLbl = new Label(legendComposite, SWT.NONE);
            addedLegendImgLbl.setImage(addedLegendImg);
            
            Label addedLegendLbl = new Label(legendComposite, SWT.NONE);
            addedLegendLbl.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
                    false, false, 1, 1));
            addedLegendLbl
                    .setText(m_registry.getString("AddedLegend.Label"));
            
            new Label(legendComposite, SWT.NONE);
            
            Image modifiedLegendImg = new Image(getComposite().getDisplay(),
                    m_registry.getImage("ModifiedLegend.IMAGE"), SWT.NONE);
            Label modifiedLegendImgLbl = new Label(legendComposite, SWT.NONE);
            modifiedLegendImgLbl.setImage(modifiedLegendImg);
            
            Label modifyLegendLbl = new Label(legendComposite, SWT.NONE);
            modifyLegendLbl.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
                    false, false, 1, 1));
            modifyLegendLbl.setText(m_registry.getString(
                    "ModifiedLegend.Label"));
            
            new Label(legendComposite, SWT.NONE);
            
            Image deletedLegendImg = new Image(getComposite().getDisplay(),
                    m_registry.getImage("DeletedLegend.IMAGE"), SWT.NONE);
            Label deletedLegendImgLbl = new Label(legendComposite, SWT.NONE);
            deletedLegendImgLbl.setImage(deletedLegendImg);
            
            Label deleteLegendLbl = new Label(legendComposite, SWT.NONE);
            deleteLegendLbl.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
                    false, false, 1, 1));
            deleteLegendLbl.setText(m_registry.getString(
                    "DeletedLegend.Label"));
        }
        else
        {
            legendComposite.setLayout(new GridLayout(1, false));
            
        }
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
}
