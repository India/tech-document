package com.nov.rac.common;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.dialog.savedelegate.TabSaveDelegate;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.loaddelegate.ILoadDelegate;
import com.nov.rac.operations.IOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.ui.factory.UIPanelFactory;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class TabsPanel extends AbstractUIPanel implements INOVFormLoadSave,
        ISubscriber, IPublisher
{
    private Registry m_registry = null;
    private CTabFolder m_folder = null;
    private List<IUIPanel> m_panels = null;
    private UIPanelFactory m_panelFactory = null;
    private IPropertyMap m_propertyMap = null;
    private Map<String, CTabItem> m_tabMap = new HashMap<String, CTabItem>();
    private Map<String, List<IUIPanel>> m_tabPanelsMap = new HashMap<String, List<IUIPanel>>();
    private List<CTabItem> m_tabArray = new ArrayList<CTabItem>();
    private List<String> m_tabNames = new ArrayList<String>();
    private Composite m_parent = null;
    private Composite m_tabComposite = null;
    private static final String LOAD_DELEGATE = "LOAD_DELEGATE";
    private static final String TABS_NAME = "TabNames";
    private static final String DISPLAY = "Display";
    private static final String DEFAULT_TAB = "DefaultTab";
    private String m_currentTabName = "";
    private static final String SAVE_DELEGATE = "SAVE_DELEGATE";
    private static final String SAVEABLE_TABS = "SAVEABLE_TABS";
    private final static String REGISTRY = "REGISTRY";
    private Registry m_thisRegistry = null;
    
    public TabsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_thisRegistry = Registry.getRegistry(this);
        setRegistry(m_thisRegistry);
    }
    
    protected void setRegistry(Registry registry)
    {
        String formType = FormHelper.getTargetComponentType();
        String formRegistryString = registry.getString(formType+"."+REGISTRY);
        m_registry = Registry.getRegistry(formRegistryString);
    }

    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_propertyMap = new SimplePropertyMap();
        m_propertyMap.setComponent(propMap.getComponent());
        
        String formType = FormHelper.getTargetComponentType();
        
        String defaultTab = m_thisRegistry.getString(
                formType + "." + DEFAULT_TAB);
        
        CTabItem detaultTab = m_tabMap.get(defaultTab);
        initializeTab(detaultTab);
        
        m_folder.setSelection(detaultTab);
        m_folder.setSimple(false);
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        try
        {
            return save();
        }
        catch (Exception e)
        {
            throw new TCException(e);
        }
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        m_parent = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        m_parent.setLayoutData(gridData);
        m_parent.setLayout(new GridLayout(1, false));
        
        m_folder = new CTabFolder(m_parent, SWT.TOP | SWT.BORDER);
        m_folder.setLayout(new GridLayout(1, false));
        m_folder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1));
        
        createTabs(m_folder);
        registerProperties();
        return false;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    private void createTabs(final CTabFolder folder)
    {
        String formType = FormHelper.getTargetComponentType();
        String[] strTabs = m_thisRegistry.getStringArray(
                formType + "." + TABS_NAME);
        
        for (final String strTab : strTabs)
        {
            final CTabItem tab = new CTabItem(folder, SWT.NONE);
            StringBuilder tabNameKey = new StringBuilder(strTab);
            tabNameKey.append(".").append(DISPLAY);
            
            String tabName = m_thisRegistry.getString(tabNameKey.toString());
            
            tab.setText("   " + tabName + "   ");
            m_tabArray.add(tab);
            m_tabMap.put(tabName, tab);
        }
        
        folder.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                CTabItem selectedTab = folder.getSelection();
                String tabName = (selectedTab.getText()).trim();
                
                if (!m_tabNames.contains(tabName))
                {
                    resetCompareContext();
                    
                    initializeTab(selectedTab);
                }
                
                PublishEvent publishEvent = getPublishEvent(
                        GlobalConstant.EVENT_TAB_CHANGED, tabName, null);
                getController().publish(publishEvent);
            }
        });
        
    }
    
    private void resetCompareContext()
    {
        if(GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        { // Resetting the compare context when tabs are loaded in compare mode.
            //Here while loading the tab it creates new compare context and perform comparison.
            ITableComparator comp = DefaultTableComparator.getController();
            comp.unregisterToComparator("");
        }
    }
    
    private void initializeTab(CTabItem tab)
    {
        String tabName = (tab.getText()).trim();
        m_tabComposite = createTabUI(tabName);
        
        IPropertyMap propertyMap = new SimplePropertyMap();
        propertyMap.setComponent(m_propertyMap.getComponent());
        
        loadTabPanelsData(propertyMap, tabName);
       
        tab.setControl(m_tabComposite);
        m_tabNames.add(tabName);
        
        System.out.println(tabName + " selected");
    }
    
    private Composite createTabUI(String tabName)
    {
        Composite tabComposite = createTabComposite();
        m_currentTabName = tabName;
        Composite mainComposite = new Composite(tabComposite, SWT.NONE);
        mainComposite.setLayout(new GridLayout(1, false));
        mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
        
        StringBuilder panelType = new StringBuilder("");
        String formType = FormHelper.getTargetComponentType();
        
        panelType.append(formType).append(".").append(tabName);
        createTabPanels(mainComposite, panelType.toString());
        return tabComposite;
    }
    
    private Composite createTabComposite()
    {
        Composite tabComposite = new Composite(m_folder, SWT.NONE);
        tabComposite.setLayout(new GridLayout(1, false));
        tabComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1));
        
        return tabComposite;
    }
    
    public void createTabPanels(final Composite parent, String panelType)
    {
        try
        {
            m_panelFactory = UIPanelFactory.getInstance();
            m_panelFactory.setRegistry(getRegistry());
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            try
            {
                m_panels = m_panelFactory.createPanels(parent, panelType,
                        theGroup);
                m_tabPanelsMap.put(m_currentTabName, m_panels);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void loadTabPanelsData(IPropertyMap propertyMap, String tabName)
    {
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        String formType = FormHelper.getTargetComponentType();
        
        StringBuilder context = new StringBuilder(formType);
        context.append(".").append(tabName);
        
        Object[] objects = new Object[1];
        //objects[0] = m_panels;
        objects[0] = m_tabPanelsMap.get(tabName);
        String[] strOperationDelegate = RegistryUtils.getConfiguration(
                theGroup, context + "." + LOAD_DELEGATE, getRegistry());
        
        ILoadDelegate iTabLoadDelegate = null;
        try
        {
            iTabLoadDelegate = (ILoadDelegate) Instancer.newInstanceEx(
                    strOperationDelegate[0], objects);
            iTabLoadDelegate.setRegistry(getRegistry());
            iTabLoadDelegate.loadComponent(propertyMap, tabName);
          
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
           
    @Override
    public void update(PublishEvent event)
    {
        if (!getComposite().isDisposed())
        {
            Object thisParent = getComposite().getParent();
            Object sourceParent = null;
            Object sourceTabComposite = null;
            
            IPublisher eventPublisher = event.getSource();
            
            if (eventPublisher instanceof AbstractUIPanel)
            {
                Composite sourceComposite = ((AbstractUIPanel) eventPublisher)
                        .getComposite();
                sourceParent = sourceComposite.getParent();
                sourceTabComposite = ((Composite)sourceParent).getParent();
            }
            
            if (event.getPropertyName().equals(
                    GlobalConstant.EVENT_REVISION_SELECTION_CHANGED)
                    && sourceParent != null && thisParent.equals(sourceParent))
            {
                IPropertyMap revisionPropertyMap = (IPropertyMap) event
                        .getNewValue();
                
                resetOnRevisionChange(revisionPropertyMap);
                String tabName = m_folder.getSelection().getText().trim();

                if (!m_tabNames.contains(tabName))
                {
                     m_tabNames.add(tabName);
                }
                
                reLoadPanelData(revisionPropertyMap, tabName, m_thisRegistry.getString("Revision_Change_Progress.MSG"));
                
                //loadTabPanelsData(revisionPropertyMap, tabName);
                clearDataModel();
            }
            
            if (event.getPropertyName().equals(
                    GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED)
                   && ((Composite)sourceTabComposite).getParent() != null 
                   && getParentCTab(m_tabComposite) == (getParentCTab((Composite)sourceTabComposite)))
            {
               
                final IPropertyMap propertyMap = new SimplePropertyMap();
                propertyMap.setComponent(m_propertyMap.getComponent());
                
                final String tabName = m_folder.getSelection().getText().trim();
                
                if(propertyMap!=null && tabName !=null)
                {
                    //Displaying Progress Bar to indicate cancel operation running in background
                    
                    reLoadPanelData(propertyMap, tabName,
                            m_thisRegistry.getString("OverView_Cancel_Progress.MSG"));
                }
            }
            
            if (event.getPropertyName()
                    .equals(GlobalConstant.EVENT_TAB_CHANGED)
                    && sourceParent != null && !thisParent.equals(sourceParent))
            {
                String tabName = event.getNewValue().toString().trim();
                CTabItem selectedTab = m_tabMap.get(tabName);
                selectedTab.getParent().setSelection(selectedTab);
                if (!m_tabNames.contains(tabName))
                {
                    initializeTab(selectedTab);
                }
            }
            
            if (event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_SAVE_BUTTON_SELECTED)
                    && sourceParent != null && thisParent.equals(sourceParent))
            {
                onSaveSelection();
                
            }
            
        }
        
    }
    
    private CTabFolder getParentCTab(Composite panel)
    {
        Object parent = null;
        if (panel.getParent() == null)
        {
            return null;
        }
        parent = panel.getParent();
        if (parent instanceof CTabFolder)
        {
            return (CTabFolder) parent;
        }
        else
        {
            return getParentCTab((Composite) parent);
        }
    
    }
    
    
    
    private void onSaveSelection()
    {
        boolean success = false;
        if (FormHelper.isLatestItemRevision(m_propertyMap.getComponent()))
        {
            try
            {
                success = save();                
                clearDataModel();
            }
            catch (Exception e)
            {
                MessageBox.post(e);
            }
        }
        
        PublishEvent publishEvent = getPublishEvent(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, success, null);
        getController().publish(publishEvent);
        
    }

    public boolean save() throws Exception
    {
        // String tabName = m_folder.getSelection().getText().trim();
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        String formType = FormHelper.getTargetComponentType();
        
        Object[] objects = new Object[1];
        objects[0] = getRegistry();
        
        Set<String> saveableTabs = m_tabPanelsMap.keySet();
        
        for (String eachTab : saveableTabs)
        {
            StringBuilder context = new StringBuilder(formType);
            context.append(".").append(eachTab);
            
            String[] strOperationDelegate = RegistryUtils.getConfiguration(theGroup, 
                    context + "." + SAVE_DELEGATE,
                    getRegistry());
            
            if (strOperationDelegate != null && strOperationDelegate.length != 0)
            {
                IOperationDelegate iOperationDelegate = (IOperationDelegate) Instancer.newInstanceEx(
                        strOperationDelegate[0], objects);
                
                iOperationDelegate.registerOperationInputProvider(m_tabPanelsMap.get(eachTab));
                
                iOperationDelegate.executeOperation();
            }
        }
        
        return true;
        
    }
    

    private void clearDataModel()
    {
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        DataBindingModel dataModel = (DataBindingModel) dataBindingRegistry
                .getDataModel(FormHelper.getDialogDataModelName());
        dataModel.clear();
        
    }

    private void resetOnRevisionChange(IPropertyMap revisionPropertyMap)
    {
        m_tabNames.clear();
        m_propertyMap.setComponent(revisionPropertyMap.getComponent());
        setEnabledOnRevisionChange(revisionPropertyMap);
        
    }
    
    private void setEnabledOnRevisionChange(IPropertyMap propMap)
    {
        String tabName = m_folder.getSelection().getText().trim();
        
        List<IUIPanel> tabPanelsList = m_tabPanelsMap.get(tabName);
        for (IUIPanel eachPanel : tabPanelsList)
        {
            if (eachPanel instanceof INOVFormLoadSave)
            {
                ((INOVFormLoadSave) eachPanel).setEnabled(false);
            }
        }
    }

    
    private void registerProperties()
    {
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_REVISION_SELECTION_CHANGED,
                getSubscriber());
        
        getController().registerSubscriber(GlobalConstant.EVENT_TAB_CHANGED,
                getSubscriber());
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_BUTTON_SELECTED,
                getSubscriber());
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED,
                getSubscriber());
        
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_REVISION_SELECTION_CHANGED,
                getSubscriber());
        
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_BUTTON_SELECTED,
                getSubscriber());
        
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED,
                getSubscriber());
    }
    
    private IController getController()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        return controller;
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        Iterator itr = m_tabPanelsMap.values().iterator();
        while(itr.hasNext())
        {
            List<IUIPanel> panels = (List<IUIPanel>)itr.next();
            for(IUIPanel panel : panels)
            {
                panel.dispose();
            }
        }
        m_folder.dispose();
        m_parent.dispose();
        super.dispose();
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private void reLoadPanelData(final IPropertyMap propMap,final String tabName, final String progressBarMsg)
    {
        ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(getComposite().getShell());
        try
        {
            progressMonitorDialog.run(true, false, new IRunnableWithProgress()
            {
                @Override
                public void run(final IProgressMonitor monitor) throws InvocationTargetException,
                        InterruptedException
                {
                    monitor.beginTask(progressBarMsg, 10000);
                    
                    Display.getDefault().syncExec(new Runnable()
                    {

                        @Override
                        public void run()
                        {

                            loadTabPanelsData(propMap , tabName);
                        }
                    
                    });
                    
                    monitor.done();
                }
            });
        }
        catch (InvocationTargetException | InterruptedException e)
        {
            // TODO Auto-generated catch block
            
            MessageBox.post(e);
            e.printStackTrace();
        }
        
    }
   
}
