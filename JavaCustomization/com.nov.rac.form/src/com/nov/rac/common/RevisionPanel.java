package com.nov.rac.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class RevisionPanel extends AbstractUIPanel implements INOVFormLoadSave,
        IPublisher, ISubscriber
{
    private Registry m_registry = null;
    private Map<String, TCComponent> m_revisionMap = new HashMap<String, TCComponent>();
    private Combo m_combo = null;
    private Button m_btnCompareRevision = null;
    private Button m_btnViewRddPdf = null;
    private List<String> m_strRevisions = new ArrayList<String>();
    private Button m_saveButton = null;
    private static final int NUM_COLUMNS = 6;
    private TCComponent m_tCComponentItem=null;
    //To Display error Message if any mandatory fields not filled
    private CLabel errMsgLabel = null;
    private Set<Object> m_errorSorces = new HashSet<Object>();
    
    public RevisionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponent tcComponent = propMap.getComponent();
        m_tCComponentItem = tcComponent;
        TCComponent[] revisionArray = FormHelper.getItemRevisions(tcComponent);
        Map<TCComponent, String> revision_revIdMap = new HashMap<TCComponent, String>();
        
        List<TCComponent> revisionList = Arrays.asList(revisionArray);
        
        TCProperty[][] returedTCProperties = TCComponentType
                .getTCPropertiesSet(revisionList,
                        new String[] { "item_revision_id" });
        
        for (int index = 0; index < revisionList.size(); index++)
        {
            String strRev = (returedTCProperties[index][0]).getStringValue();
            m_strRevisions.add(strRev);
            m_revisionMap.put(strRev, revisionList.get(index));
            revision_revIdMap.put(revisionList.get(index), strRev);
        }
        
        if(!(m_strRevisions.size() > 1))
        {
            setEnabled(false);
        }
        
        String[] revisionIdArray = new String[m_strRevisions.size()];
        m_combo.setItems(m_strRevisions.toArray(revisionIdArray));
        m_combo.setText(revision_revIdMap.get(tcComponent));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_btnCompareRevision.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(NUM_COLUMNS, false));
        
      //To create Error Message part in Revision Panel
        createErrorMsgArea(composite);
        
        Label lblRev = new Label(composite, SWT.NONE);
        lblRev.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
                1, 1));
        lblRev.setText(getRegistry().getString("Revision.Label"));
        
        m_combo = new Combo(composite, SWT.READ_ONLY);
        m_combo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false,
                1, 1));
        
        new Label(composite, SWT.NONE);
        //new Label(composite, SWT.NONE);
        
        m_saveButton  = new Button( composite, SWT.NONE );
        m_saveButton.setLayoutData( new GridData( SWT.CENTER, SWT.CENTER, false, false, 1, 1 ) );
        m_saveButton.setText ( m_registry.getString("SaveButton.Label") );
        m_saveButton.setEnabled(false);
        
        m_btnCompareRevision = new Button(composite, SWT.NONE);
        m_btnCompareRevision.setText(getRegistry().getString(
                "CompareRevisionButton.Label"));
        
        m_btnViewRddPdf = new Button(composite, SWT.NONE);
        m_btnViewRddPdf.setText(getRegistry().getString("SeeRDDPdfButton.Lable"));
        
        addListeners();
        
        registerSubscibers();
        
        if(GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            setEnabled(false);
        }
        return true;
    }
	
	private void addListeners()
    {
        addComboListener(m_combo);
		addViewRddPdfListener(m_btnViewRddPdf);
        addCompareButtonListener(m_btnCompareRevision);
        addSaveButtonListener(m_saveButton);
    }
	
	private void addSaveButtonListener(Button saveButton)
    {
        saveButton.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                getController().publish( getPublishEvent(
                                GlobalConstant.EVENT_SAVE_BUTTON_SELECTED,
                                null, null));
            }
        });
    }
    
    private void addViewRddPdfListener(Button m_btnViewRddPdf2)
    {
        m_btnViewRddPdf2.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                
                try
                {
                    TCComponentItem rdd = (TCComponentItem) m_tCComponentItem.getRelatedComponent("RelatedDefiningDocument");
                    checkIsRDDAttached(rdd);
                    TCComponentItemRevision rddObject = rdd.getLatestItemRevision();
                    if(rddObject != null)
                    {
                        openPDFDataset(rddObject);
                    }
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    MessageBox.post(e);
                }
                catch (IOException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
               
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent paramSelectionEvent)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
    }
    private void openPDFDataset(TCComponent rddRev) throws TCException, IOException
    {
        TCComponent[] datasetArray = rddRev.getRelatedComponents("IMAN_specification");
        boolean isPDFDatasetFound = false;
        for(TCComponent dataset : datasetArray)
        {
            if(dataset instanceof TCComponentDataset &&
                    ((TCComponentDataset)dataset).getType().equalsIgnoreCase("PDF"))
            {
                TCComponent[] namedRef = ((TCComponentDataset)dataset).getNamedReferences();
                if(namedRef != null && namedRef.length > 0)
                {
                    ((TCComponentDataset)dataset).open();
                }
                else
                {
                    String[] errorMsg = new String[2];
                    errorMsg[0] = m_registry.getString("ErrorDocView.MSG");
                    errorMsg[1] = m_registry.getString("ReferenceToViewMissing.MSG");
                    throw new TCException(errorMsg);
                }
                isPDFDatasetFound = true;
                break;
            }
        }
        
        if( isPDFDatasetFound == false)
        {
            String[] errorMsg = new String[2];
            errorMsg[0] = m_registry.getString("ErrorDocView.MSG");
            errorMsg[1] = m_registry.getString("NoPDFDatasetFound.MSG");
            throw new TCException(errorMsg);
        }
    }
    
    private void checkIsRDDAttached(TCComponentItem rdd) throws TCException
    {
        if(rdd == null)
        {
            String[] errorMsg = new String[1];
            errorMsg[0] = m_registry.getString("NoRDDAttached.MSG");
            throw new TCException(errorMsg);
        }
    }

    private void addCompareButtonListener(Button btnCompareRevision)
    {
        btnCompareRevision.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                
                getController().publish(
                        getPublishEvent(
                                GlobalConstant.EVENT_COMPARE_MODE_SELECTED,
                                true, true));
                
            }
        });
    }
    
    private void addComboListener(final Combo combo)
    {
        combo.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                String selectedRevision = combo.getText();
                TCComponent component = m_revisionMap.get(selectedRevision);
                if(combo.getItem(0).equals(selectedRevision))
                {
                    if(!GlobalConstant.COMPARE_NONCOMPARE_FLAG)
                    {
                        setEnabled(false);
                    }
                    
                }
                else
                {
                    if(!GlobalConstant.COMPARE_NONCOMPARE_FLAG)
                    {
                        setEnabled(true);
                    }
                }
                errMsgLabel.setVisible(false);
                m_saveButton.setEnabled(false);
                IPropertyMap iMap = new SimplePropertyMap();
                iMap.setComponent(component);
                PublishEvent publishEvent = getPublishEvent(
                        GlobalConstant.EVENT_REVISION_SELECTION_CHANGED, iMap,
                        null);
                getController().publish(publishEvent);
            }
        });
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }

    @Override
    public void update(final PublishEvent event)
    {
        SWTUIUtilities.asyncExec(new Runnable()
        {
            
            @Override
            public void run()
            {
                if (event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_EDIT_BUTTON_SELECTED))
                {
                    onEdit();
                    
                    /*
                     * We are disabling Save button when Error Message is still displaying,even though user
                     * moved to other tab in dialog and click on edit button. 
                     * 
                     * Note : Save Button is enabled only if Error Message is absence in Dialog
                     * 
                     */
                    if(errMsgLabel.getVisible())
                    {
                        m_saveButton.setEnabled(false); 
                    }
                    
                }
                
                if (event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))
                {
                    boolean success = (boolean) event.getNewValue();
                    if(success)
                    {
                        onSave();
                    }
                }
                
                String str_revision = m_combo.getText();
                TCComponent tcRevision = m_revisionMap.get(str_revision);
                
                if (FormHelper.isLatestItemRevision(tcRevision))
                {
                    if(event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_VALIDATE_PROPERTIES_SUCCESS))
                    {
                        m_saveButton.setEnabled(true); 
                        
                        Object souceObj = event.getSource();
                        if(m_errorSorces.contains(souceObj))
                        {
                            m_errorSorces.remove(souceObj);
                        }
                        
                       // Hiding Error Message when all required fields are filled in properly
                        
                        if(errMsgLabel != null && m_errorSorces.isEmpty())
                            errMsgLabel.setVisible(false);
                    }
                    
                    if(event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_VALIDATE_PROPERTIES_ERRORS))
                    {
                        m_saveButton.setEnabled(false); 
                        
                        //Displaying error message in pop up dialog.
                        if(errMsgLabel != null)
                            errMsgLabel.setVisible(true);  
                        
                        m_errorSorces.add(event.getSource());
                    }  
                    if(event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED))
                    {
                        m_saveButton.setEnabled(false);
                    }
                }
            }
        });
    }
    
    private void onEdit()
    {
        String selectedRevision = m_combo.getText();
        TCComponent component = m_revisionMap.get(selectedRevision);
        if (FormHelper.isLatestItemRevision(component))
        {
            m_saveButton.setEnabled(true);
        }
    }

    private void onSave()
    {
        String selectedRevision = m_combo.getText();
        TCComponent component = m_revisionMap.get(selectedRevision);
        if (FormHelper.isLatestItemRevision(component))
        {
            m_saveButton.setEnabled(false);
        }
    }

    private void registerSubscibers()
    {
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED,
                getSubscriber());
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG,
                getSubscriber());
        
     // Subscriber is registered to receive error indication or event 
        // from other Panel        
         getController().registerSubscriber(
                 GlobalConstant.EVENT_VALIDATE_PROPERTIES_SUCCESS,
                 getSubscriber());
         getController().registerSubscriber(
                 GlobalConstant.EVENT_VALIDATE_PROPERTIES_ERRORS,
                 getSubscriber());
         getController().registerSubscriber(
                 GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED,
                 getSubscriber());
        
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED,
                getSubscriber());
        
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG,
                getSubscriber());
        
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_VALIDATE_PROPERTIES_SUCCESS,
                getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_VALIDATE_PROPERTIES_ERRORS,
                getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED,
                getSubscriber());
        
    }
    
   
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    /*
     *To Display Error Message with Image top of Revsion Label field 
     */
    private void createErrorMsgArea(Composite composite)
    {
       
        GridData errMsgGD = new GridData(SWT.FILL, SWT.CENTER, false,
                false, 1, 1);
        errMsgGD.horizontalSpan = NUM_COLUMNS;
        errMsgLabel = new CLabel (composite ,SWT.NONE);
        errMsgLabel.setLayoutData(errMsgGD);
        Image errorImg = new Image(null,m_registry.getImage("Error.IMAGE"), SWT.NONE);
        errMsgLabel.setImage(errorImg);
        errMsgLabel.setText(getRegistry().getString("PopUp_Dialog_Error_Message"));
        errMsgLabel.setForeground(getComposite().getDisplay().getSystemColor(SWT.COLOR_RED));
        
        // At First we are hiding label in Dialog
        errMsgLabel.setVisible(false);
    }
}