package com.nov.rac.summarypage;

import java.util.Vector;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.ExpansionRule;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.log.Debug;
import com.teamcenter.services.rac.core._2010_04.DataManagement.BusinessObjectHierarchy;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.RelatedObjectOutput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.RelatedObjectResponse;

public class NOVProductCategoryTree extends TCTree{
	
	Vector<AIFComponentContext> childComponents;
	
	private RelatedObjectOutput relatedObjectsOutput = null;
	
	@Override
	public Object[] getChildren(Object obj, Object obj1) {
		if(obj == null)
            return null;
		
		Object relatedChildComponents = relatedObjectsOutput.childObjects.get(obj);
		return (Object[]) relatedChildComponents;
			
	}

	public void setRelatedObjectsOutput(RelatedObjectOutput theResponse) {
		// TODO Auto-generated method stub
		relatedObjectsOutput =  theResponse;
	}

}
