package com.nov.rac.summarypage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import com.nov.rac.common.Pane;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.services.ISelectionMediatorService;
import com.teamcenter.rac.util.AbstractCustomPanel;
import com.teamcenter.rac.util.IPageComplete;
import com.teamcenter.rac.util.OSGIUtil;

public class SummaryPageViewer extends AbstractCustomPanel implements
        IPageComplete, ISubscriber
{
    
    private IUIPanel m_pane;

    public SummaryPageViewer()
    {
        
    }
    
    public SummaryPageViewer(Composite parent)
    {
        super(parent);
        // initialize();
    }
    
    private void initialize()
    {
        try
        {
            TCComponent selectedComponent = getSelectedComponent();
            String selectedComponentType = selectedComponent
                    .getProperty("object_type");
            FormHelper.setTargetComponent(selectedComponent);
            FormHelper.setTargetComponentType(selectedComponentType);
            FormHelper.setTargetComponentOwningGroup(selectedComponent
                    .getProperty("owning_group"));
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void createPanel()
    {
        try
        {
            initialize();
            m_pane = new Pane(getComposite(), SWT.NONE);
            m_pane.createUI();
            if (m_pane instanceof INOVFormLoadSave)
            {
                INOVFormLoadSave loadablepanel = (INOVFormLoadSave) m_pane;
                IPropertyMap loadMap = new SimplePropertyMap();
                
                loadMap.setComponent(getSelectedComponent());
                loadablepanel.load(loadMap);
                registerProperties();
            }
            
            addDisposeListener(parent);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public Composite getComposite()
    {
        FormToolkit toolkit = new FormToolkit(parent.getDisplay());
        Composite composite = toolkit.createComposite(parent);
        GridLayout gl = new GridLayout(1, false);
        composite.setLayout(gl);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.grabExcessHorizontalSpace = true;
        composite.setLayoutData(gd);
        
        return composite;
    }
    
    @Override
    public boolean isPageComplete()
    {
        return true;
    }
    
    private TCComponent getSelectedComponent()
    {
        TCComponent tcComponent = null;
        ISelectionMediatorService iselectionmediatorservice = (ISelectionMediatorService) OSGIUtil
                .getService(AifrcpPlugin.getDefault(),
                        ISelectionMediatorService.class);
        InterfaceAIFComponent[] interfaceAIFComponents = (InterfaceAIFComponent[]) iselectionmediatorservice
                .getTargetComponents();
        if (interfaceAIFComponents != null && interfaceAIFComponents.length > 0)
        {
            tcComponent = (TCComponent) interfaceAIFComponents[0];
        }
        
        return tcComponent;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        // TODO Auto-generated method stub
        
        if (event.getPropertyName().equals(
                GlobalConstant.EVENT_COMPARE_MODE_SELECTED))
        {
            IPropertyMap iMap = new SimplePropertyMap();
            iMap.setComponent(getSelectedComponent());
            PopupDialog dialog = new PopupDialog(parent.getShell());
            dialog.setInputPropertyMap(iMap);
            dialog.openCompareDialog(parent.getShell());
            
        }
        
    }
    
    private void addDisposeListener(Composite parent)
    {
        try
        {
            parent.addDisposeListener(new DisposeListener()
            {
                
                @Override
                public void widgetDisposed(DisposeEvent disposeevent)
                {
                    unregisterSubscriber();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void registerProperties()
    {
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_COMPARE_MODE_SELECTED, this);
    }
    
    private IController getController()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        return controller;
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_COMPARE_MODE_SELECTED, this);
    }
    

    @Override
    public void dispose()
    {
        m_pane.dispose();
        super.dispose();
    }
    
}
