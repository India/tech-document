package com.nov.rac.form.dialog.delegate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.impl.enform.DocumentDetailsPanel;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.services.publishdataset.NovPublishDatasetHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class DocRevOveriewTabLoadDelegate extends TabLoadDelegate
{

    public DocRevOveriewTabLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }

    
    @Override
    public void loadComponent(Object inputObject, String tabName)
            throws TCException
    {
        try
        {
            
            IPropertyMap propertyMap = (IPropertyMap) inputObject;
            TCComponent relatedComponent = FormHelper.getTargetComponent();
            propertyMap.setComponent(relatedComponent);
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String formType = FormHelper.getTargetComponentType();
            StringBuilder contextType = new StringBuilder(formType);
            contextType.append(".").append(tabName);
            
            String[] propNames = getPropertyNames(theGroup,
                    contextType.toString());
            
            loadPropertyMap(propertyMap, contextType.toString(), theGroup,
                    propNames);
            TCComponentItem docItem = ((TCComponentItemRevision)relatedComponent).getItem();
            loadPublishDatasetInformation(docItem,propertyMap);
            
            loadPanelsData(theGroup, contextType.toString(), getInputProviders(),
                    propertyMap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        
    }
    private void loadPublishDatasetInformation(TCComponent tCComponent, IPropertyMap propMap) throws TCException
    {
        Map<String, String> stringMap = new HashMap<String, String>();
        stringMap.put("application", "KM");
        NovPublishDatasetHelper.getPublishDatasetRelatedInfo(tCComponent, stringMap , propMap);
    }
}
