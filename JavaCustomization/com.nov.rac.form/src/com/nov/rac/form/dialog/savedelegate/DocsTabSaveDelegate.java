package com.nov.rac.form.dialog.savedelegate;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.publishdataset.NovPublishDatasetHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocsTabSaveDelegate extends TabSaveDelegate
{
    private IPropertyMap m_datasetPropMap;
    private static final String DOCUMENTS_REVISION_TYPE = "Documents Revision";
    
    private static final String DOCS_TAB_NAME = "DOCS";

    private static final String DOCUMENTS_TYPE = "Documents";
    
    public DocsTabSaveDelegate(Registry registry)
    {
        super(registry);
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        boolean success = true;
        String formType = FormHelper.getTargetComponentType();
        
        StringBuilder context = new StringBuilder(DOCS_TAB_NAME);
        context.append(".").append(DOCUMENTS_REVISION_TYPE);
        
        IPropertyMap docPropertyMap = new SimplePropertyMap();
        docPropertyMap.setCompound("revision", new SimplePropertyMap());
        docPropertyMap.setCompound("IMAN_master_form", new SimplePropertyMap());
        
        fillPanelsData("", formType, context.toString(), 
                getOperationInputProviders(), docPropertyMap);
        
        addToCreateInObjectHelper(docPropertyMap);
        
        
        if(docPropertyMap.getComponent() != null)
        {
            TCComponent masterForm = getMasterForm(docPropertyMap.getComponent());
            IPropertyMap masterFormPropMap = docPropertyMap.getCompound("IMAN_master_form");
            masterFormPropMap.setComponent(masterForm);
            
            addToCreateInObjectHelper(masterFormPropMap);
            addToCreateInObjectHelper(docPropertyMap.getCompound("revision"));
        }
        
        StringBuilder validationContext = new StringBuilder(DOCS_TAB_NAME);
        validationContext.append(".").append(DOCUMENTS_REVISION_TYPE);
        
        IPropertyMap[] datasetPropMaps = docPropertyMap
                .getCompoundArray(NovPublishDatasetHelper.PROP_PUBLISH_DATASET);
        
        if (datasetPropMaps != null && datasetPropMaps.length > 0)
        {
            success = validateProperties(validationContext.toString(), datasetPropMaps);
        }
        
        return success;
        
    }

    private TCComponent getMasterForm(TCComponent tcComponent) throws TCException
    {
        TCComponent masterForm = null;
        String relation = "IMAN_master_form";
        
        AIFComponentContext[] secondaryObjects = tcComponent.getRelated(relation);
        masterForm = tcComponent.getRelatedComponents(relation)[0];
        
        for (int inx = 0; inx < secondaryObjects.length; inx++)
        {
            if(secondaryObjects[inx].getComponent() instanceof TCComponentForm)
            {
                masterForm = (TCComponent) secondaryObjects[inx].getComponent();
                break;
            }
        }
        
        return masterForm;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        super.baseAction();
        
        if (m_datasetPropMap != null)
        {
            NovPublishDatasetHelper.updateExcludeTable(m_datasetPropMap);
        }
        
        return true;
    }
    
}
