package com.nov.rac.form.dialog.delegate;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.manufacturerhelper.ManufacturingHelper;
import com.nov.rac.utilities.manufacturerhelper.NOVManufacturerHelper;
import com.nov.rac.utilities.services.ClassificationHelper.AttributePropertiesHelper;
import com.nov.rac.utilities.services.ClassificationHelper.ClassificationHelper;
import com.nov.rac.utilities.services.ClassificationHelper.ICOObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentICO;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;

public class OverviewTabDelegate extends TabLoadDelegate
{
    
    public OverviewTabDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void loadComponent(Object inputObject, String tabName)
            throws TCException
    {
        try
        {
            
            IPropertyMap propertyMap = (IPropertyMap) inputObject;
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String formType = FormHelper.getTargetComponentType();
            StringBuilder contextType = new StringBuilder(formType);
            contextType.append(".").append(tabName);
            
            String[] propNames = getPropertyNames(theGroup,
                    contextType.toString());
            
            loadPropertyMap(propertyMap, contextType.toString(), theGroup,
                    propNames);
            
            loadItemMasterRevPropMap(propertyMap);
            loadItemPropMap(propertyMap);
            loadItemMasterPropMap(propertyMap);
            
            loadManufacturingNamesAndPartNos(propertyMap);
            loadClassificationInfo(propertyMap);
            
            loadPanelsData(theGroup, contextType.toString(),
                    getInputProviders(), propertyMap);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void loadItemMasterPropMap(IPropertyMap propertyMap)
            throws TCException
    {
        
        IPropertyMap itemPropMap = propertyMap.getCompound("items_tag");
        String relationName = "IMAN_master_form";
        
        IPropertyMap[] itemMasterPropMaps = itemPropMap
                .getCompoundArray(relationName);
        if (itemMasterPropMaps == null)
        {
            PropertyMapHelper.createCompoundPropMap(itemPropMap, relationName);
            itemMasterPropMaps = itemPropMap.getCompoundArray(relationName);
        }
        String[] propNames = getRegistry().getStringArray(
                "Nov4Part Master.PROPERTIES_AS_ATTRIBUTE");
        
        TCComponent itemMasterRev = itemMasterPropMaps[0].getComponent();
        
        PropertyMapHelper.componentToMap(itemMasterRev, propNames,
                itemMasterPropMaps[0]);
        
    }
    
    private void loadItemPropMap(IPropertyMap propertyMap) throws TCException
    {
        String relationName = "items_tag";
        
        IPropertyMap itemPropMap = propertyMap.getCompound(relationName);
        if (itemPropMap == null)
        {
            PropertyMapHelper.createCompoundPropMap(propertyMap, relationName);
            itemPropMap = propertyMap.getCompound(relationName);
        }
        
        TCComponent item = itemPropMap.getComponent();
        // Map<String,String> test =itemMasterRev.getCachedProperties();
        
       /* String[] propNames = getRegistry().getStringArray(
                "Nov4Part.PROPERTYNAMES");*/
        String[] propNames = getRegistry().getStringArray(
                "Nov4Part.PROPERTIES_AS_ATTRIBUTE");
        
        PropertyMapHelper.componentToMap(item, propNames, itemPropMap);
    }
    
    private void loadItemMasterRevPropMap(IPropertyMap propertyMap)
            throws TCException
    {
        String relationName = "IMAN_master_form_rev";
        
        IPropertyMap[] itemMasterRevPropMaps = propertyMap
                .getCompoundArray(relationName);
        if (itemMasterRevPropMaps == null)
        {
            PropertyMapHelper.createCompoundPropMap(propertyMap, relationName);
            itemMasterRevPropMaps = propertyMap.getCompoundArray(relationName);
        }
        String[] propNames = getRegistry().getStringArray(
                "Nov4Part Revision Master.PROPERTIES_AS_ATTRIBUTE");
        
        TCComponent itemMasterRev = itemMasterRevPropMaps[0].getComponent();
        
        PropertyMapHelper.componentToMap(itemMasterRev, propNames,
                itemMasterRevPropMaps[0]);
    }
    
    private void loadClassificationInfo(IPropertyMap propertyMap)
            throws TCException
    {
        
        TCComponent itemRev = propertyMap.getComponent();
        // ServerCall , ask kishor
        TCComponentICO[] icoObjects = itemRev.getClassificationObjects();
        
        if (icoObjects != null)
        {
            loadICOObjectPropMaps(propertyMap, icoObjects);
        }
    }
    
    private void loadICOObjectPropMaps(IPropertyMap propertyMap,
            TCComponentICO[] icoObjects) throws TCException
    {
        ICOObjectHelper[] retObj = ClassificationHelper
                .getICOProperties(icoObjects);
        
        if (retObj != null && retObj.length > 0)
        {
            IPropertyMap[] icoPropMaps = new SimplePropertyMap[retObj.length];
            for (int i = 0; i < retObj.length; i++)
            {
                
                TCComponent currIco = retObj[i].getICOObject();
                IPropertyMap icoPropMap = new SimplePropertyMap();
                icoPropMap.setComponent(currIco);
                icoPropMaps[i] = icoPropMap;
                
                AttributePropertiesHelper[] allAttributes = retObj[i]
                        .getAllAttributes();
                
                String classID = retObj[i].getClassID();
               
                if (allAttributes != null && allAttributes.length > 0)
                {
                    loadAttributesPropMaps(icoPropMaps[i], allAttributes);
                    String hierarchy =getClassificationHierarchy(classID);
                    icoPropMaps[i].setString("hierarchy", hierarchy);
                }
                propertyMap.setCompoundArray("icoObjects", icoPropMaps);
            }
        }
    }
    
    private void loadclassificationHierarchy(IPropertyMap iPropertyMap, String classID)
    {
        // TODO Auto-generated method stub
        
    }

    private String getClassificationHierarchy(String classID)
    {
        String hierarchy = null;
        try
        {
            Map parentIDsMap = ClassificationHelper
                    .getClassHierarchy(new String[] { classID });
            String[] parentIds = (String[]) parentIDsMap.get(classID);
            String[] allIds =Arrays.copyOf(parentIds, parentIds.length+1);
            allIds[allIds.length-1] = classID; 
            Map descMap =ClassificationHelper.getClassNames(allIds);
            
            hierarchy = (String) descMap.get(classID);
            for (String id : parentIds)
            {
                hierarchy =(String) descMap.get(id)+">"+hierarchy;
            }
            
           
        }
        catch (ServiceException e)
        {
            e.printStackTrace();
        }
        return hierarchy;
    }
    
    private void loadAttributesPropMaps(IPropertyMap icoPropMap,
            AttributePropertiesHelper[] allAttributes)
    {
        IPropertyMap[] attrPropMaps = new SimplePropertyMap[allAttributes.length];
        
        for (int j = 0; j < allAttributes.length; j++)
        {
            IPropertyMap attrPropMap = new SimplePropertyMap();
            attrPropMaps[j] = attrPropMap;
            
            attrPropMap.setString(AttributePropertiesHelper.PROP_NAME,
                    allAttributes[j].getAttributeName());
            attrPropMap.setString(AttributePropertiesHelper.PROP_METRIC_VALUES,
                    allAttributes[j].getMetricValues());
            attrPropMap.setString(AttributePropertiesHelper.PROP_METRIC_UNIT,
                    allAttributes[j].getMetricUnit());
            attrPropMap.setString(
                    AttributePropertiesHelper.PROP_NON_METRIC_VALUES,
                    allAttributes[j].getNonMetricValues());
            attrPropMap.setString(
                    AttributePropertiesHelper.PROP_NON_METRIC_UNIT,
                    allAttributes[j].getNonMetricUnit());
            
        }
        icoPropMap.setCompoundArray("attributes", attrPropMaps);
    }
    
    // ask about this relation Name
    // String relationName = "IMAN_classification";
    //
    // IPropertyMap[] icoObjectsPropMaps = propertyMap
    // .getCompoundArray(relationName);
    // if (icoObjectsPropMaps == null)
    // {
    // createCompoundPropMap(propertyMap, relationName);
    // icoObjectsPropMaps = propertyMap.getCompoundArray(relationName);
    // }
    //
    // for (int i = 0; i < icoObjectsPropMaps.length; i++)
    // {
    // String relationName2 = "ico_object";
    // IPropertyMap[] attributtesPropMaps = propertyMap
    // .getCompoundArray(relationName);
    // if (attributtesPropMaps == null)
    // {
    // createCompoundPropMap(icoObjectsPropMaps[i], relationName);
    // attributtesPropMaps = icoObjectsPropMaps[i]
    // .getCompoundArray(relationName);
    // }
    // }
    //
    // TCComponent component = propertyMap.getComponent();
    // TCComponentICO[] icoObjects = null;
    //
    // icoObjects = component.getClassificationObjects();
    // if (icoObjects == null)
    // {
    // return;
    // }
    // ICOObjectHelper[] icoProperties = ClassificationHelper
    // .getICOProperties(icoObjects);
    // if (icoProperties == null)
    // {
    // return;
    // }
    //
    // for (int i = 0; i < icoProperties.length; i++)
    // {
    // Vector metricVector = new Vector<String>();
    // Vector nonMetricVector = new Vector<String>();
    //
    // AttributePropertiesHelper[] allAttributes = icoProperties[i]
    // .getAllAttributes();
    // if (allAttributes == null)
    // {
    // return;
    // }
    // for (int j = 0; j < allAttributes.length; j++)
    // {
    //
    // metricVector.add(allAttributes[j].getAttributeName());
    // metricVector.add(allAttributes[j].getMetricValues());
    // metricVector.add(allAttributes[j].getMetricUnit());
    //
    // nonMetricVector.add(allAttributes[j].getAttributeName());
    // nonMetricVector.add(allAttributes[j].getNonMetricValues());
    // nonMetricVector.add(allAttributes[j].getNonMetricUnit());
    // }
    // // propertyMap.setTag("classification_data_metric",
    // // (TCComponent) metricVector);
    // // propertyMap.setTag("classification_data_metric",
    // // nonMetricVector);
    // }
    // }
    
    private void loadManufacturingNamesAndPartNos(IPropertyMap propertyMap)
            throws TCException
    {
        IPropertyMap[] itemMasterRevPropMap = propertyMap
                .getCompoundArray("IMAN_master_form_rev");
        
        String[] mfgIds = PropertyMapHelper.handleNull(itemMasterRevPropMap[0]
                .getStringArray("nov4_manufacturer_id"));
        
        try
        {
            ManufacturingHelper mfgHelperInstance = ManufacturingHelper
                    .getInstance();
            String[] mfgNames = mfgHelperInstance.getMfgNames(mfgIds);
            itemMasterRevPropMap[0].setStringArray("nov4_manufacturer_name",
                    mfgNames);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
