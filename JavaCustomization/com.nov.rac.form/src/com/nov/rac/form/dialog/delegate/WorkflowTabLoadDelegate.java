package com.nov.rac.form.dialog.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;

public class WorkflowTabLoadDelegate extends TabLoadDelegate
{
    
    public WorkflowTabLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void loadComponent(Object inputObject, String tabName)
            throws TCException
    {
        try
        {
            IPropertyMap inputMap = (IPropertyMap)inputObject;
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String formType = FormHelper.getTargetComponentType();
            StringBuilder contextType = new StringBuilder(formType);
            contextType.append(".").append(tabName);
            
            IPropertyMap propertyMap = new SimplePropertyMap();
            TCComponentProcess currentProcess =  inputMap.getComponent().getCurrentJob();
            
            if(currentProcess!=null)
            {
                propertyMap.setComponent(currentProcess);
            String[] propNames = getPropertyNames(theGroup,
                    contextType.toString());
            loadPropertyMap(propertyMap, contextType.toString(), theGroup,
                    propNames);
            IPropertyMap rootTaskPropMap = loadRootTaskPropMap(propertyMap);
            IPropertyMap[] childTasksPropMaps = loadChildTasks(rootTaskPropMap);
            
            for (IPropertyMap childPropMap : childTasksPropMaps)
            {
               String type = childPropMap.getType();
                if(type.equalsIgnoreCase("EPMReviewTask"))
                {
                    loadChildren_ReviewTaskPropMap(childPropMap);
                }
               
            }
            }
            loadPanelsData(theGroup, contextType.toString(),
                    getInputProviders(), propertyMap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }

    private void loadChildren_ReviewTaskPropMap(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] childTasksPropMaps = loadChildTasks(propMap);
    
        for (IPropertyMap childtaskPropMap : childTasksPropMaps)
        {
            String type = childtaskPropMap.getType();
            if (type.equalsIgnoreCase("EPMPerformSignoffTask"))
            {
                loadSignoffPropMap(childtaskPropMap);
            }
        }
    }

    private void loadChildAndSignoffPropMaps(IPropertyMap[] propMaps)
            throws TCException
    {
        for (IPropertyMap propMap : propMaps)
           {
               TCComponent[] child_Tasks = propMap.getComponent().getReferenceListProperty("child_tasks");
               if(child_Tasks!=null && child_Tasks.length>0)
               {
                   IPropertyMap[] childTasks = loadChildTasks(propMap);
                   loadChildAndSignoffPropMaps(childTasks);
               }
               else
               {
                   loadSignoffPropMap(propMap);
               }
           }
    }
    
   
    
    private IPropertyMap[] loadSignoffPropMap(IPropertyMap childTaskPropMap) throws TCException
    {
        String relationName = "signoff_attachments";
        
        IPropertyMap[] signoffPropMaps = childTaskPropMap
                .getCompoundArray(relationName);
        if (signoffPropMaps == null)
        {
            PropertyMapHelper.createCompoundPropMap(childTaskPropMap, relationName);
            signoffPropMaps = childTaskPropMap.getCompoundArray(relationName);
        }
        
        String[] propsAsAtt = getRegistry().getStringArray(
                "Signoff.PROPERTIES_AS_ATTRIBUTE");
        String[] propAsProps = getRegistry().getStringArray(
                "Signoff.PROPERTIES_WITH_PROPERTIES");
        
        ArrayList<String> temp = new ArrayList<String>();
        if(propsAsAtt!=null)
        temp.addAll(Arrays.asList(propsAsAtt));
        if(propAsProps!=null)
        temp.addAll(Arrays.asList(propAsProps));
        String[] propNames = temp.toArray(new String[temp.size()]);
        // String[] propNames = getRegistry().getStringArray(
        // "EPMTask.PROPERTYNAMES");
        
        for (IPropertyMap signoffPropMap : signoffPropMaps)
        {
            TCComponent signoff = signoffPropMap.getComponent();
            PropertyMapHelper.componentToMap(signoff, propNames,
                    signoffPropMap);
        }
        
        return signoffPropMaps;
        
    }

   
    private IPropertyMap loadRootTaskPropMap(IPropertyMap propertyMap)
            throws TCException
    {
        String relationName = "root_task";
        
        IPropertyMap rootTaskPropMap = propertyMap.getCompound(relationName);
        if (rootTaskPropMap == null)
        {
            PropertyMapHelper.createCompoundPropMap(propertyMap, relationName);
            rootTaskPropMap = propertyMap.getCompound(relationName);
        }
        
        TCComponent rootTask = rootTaskPropMap.getComponent();
        String[] propsAsAtt = getRegistry().getStringArray(
                "EPMTask.PROPERTIES_AS_ATTRIBUTE");
        
        String[] propAsProps = getRegistry().getStringArray(
                "EPMTask.PROPERTIES_WITH_PROPERTIES");
        
        ArrayList<String> temp = new ArrayList<String>();
        if(propsAsAtt!=null)
        temp.addAll(Arrays.asList(propsAsAtt));
        if(propAsProps!=null)
        temp.addAll(Arrays.asList(propAsProps));
        String[] propNames = temp.toArray(new String[temp.size()]);
        // String[] propNames = getRegistry().getStringArray(
        // "EPMTask.PROPERTYNAMES");
        PropertyMapHelper.componentToMap(rootTask, propNames, rootTaskPropMap);
        
        return rootTaskPropMap;
        
    }
    
    private IPropertyMap[] loadChildTasks(IPropertyMap rootTaskMap) throws TCException
    {
        String relationName = "child_tasks";
        
        IPropertyMap[] childTasksPropMaps = rootTaskMap
                .getCompoundArray(relationName);
        if (childTasksPropMaps == null)
        {
            PropertyMapHelper.createCompoundPropMap(rootTaskMap, relationName);
            childTasksPropMaps = rootTaskMap.getCompoundArray(relationName);
        }
        
        String[] propsAsAtt = getRegistry().getStringArray(
                "EPMTask.PROPERTIES_AS_ATTRIBUTE");
        String[] propAsProps = getRegistry().getStringArray(
                "EPMTask.PROPERTIES_WITH_PROPERTIES");
        
        ArrayList<String> temp = new ArrayList<String>();
        if(propsAsAtt!=null)
        temp.addAll(Arrays.asList(propsAsAtt));
        if(propAsProps!=null)
        temp.addAll(Arrays.asList(propAsProps));
        String[] propNames = temp.toArray(new String[temp.size()]);
        // String[] propNames = getRegistry().getStringArray(
        // "EPMTask.PROPERTYNAMES");
        
        for (IPropertyMap eachChildTasksPropMap : childTasksPropMaps)
        {
            TCComponent childTask = eachChildTasksPropMap.getComponent();
            PropertyMapHelper.componentToMap(childTask, propNames,
                    eachChildTasksPropMap);
            
        }
        
        return childTasksPropMaps;
    }
}
