package com.nov.rac.form.dialog.savedelegate;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class OverviewTabSaveDelegate extends TabSaveDelegate
{
    private static final String PART_REVISION_TYPE = "Nov4Part Revision";
    private static final String OVERVIEW_TAB_NAME = "OVERVIEW";
    
    public OverviewTabSaveDelegate(Registry registry)
    {
        super(registry);
    }

    @Override
    public boolean preCondition() throws TCException
    {
        boolean success = true;
        String formType = FormHelper.getTargetComponentType();
        
        StringBuilder context = new StringBuilder(OVERVIEW_TAB_NAME);
        context.append(".").append(PART_REVISION_TYPE);
        
        IPropertyMap propertyMap = new SimplePropertyMap();
        
        CreateInCompoundHelper partRevCompoundHelper = new CreateInCompoundHelper(propertyMap);
        partRevCompoundHelper.addCompound("MasterForm", "IMAN_master_form_rev");
        
        fillPanelsData("", formType, context.toString(), getOperationInputProviders(),
                propertyMap);
        
        TCComponent saveableComponent = propertyMap.getComponent();
        String componentType = saveableComponent.getType();
        
        IPropertyMap itemRevPropertyMap = new CreateInObjectHelper(componentType,
                CreateInObjectHelper.OPERATION_UPDATE, propertyMap);
        itemRevPropertyMap.setComponent(saveableComponent);
        
        m_createInObjectHelper.add(itemRevPropertyMap);
        
        TCComponent masterForm = getMasterForm(saveableComponent);
        
        if(masterForm != null)
        {
            StringBuilder validationContext = new StringBuilder(OVERVIEW_TAB_NAME);
            validationContext.append(".").append(componentType);
            
            IPropertyMap itemRevMasterPropertyMap = new CreateInObjectHelper(
                    masterForm.getType(), CreateInObjectHelper.OPERATION_UPDATE, itemRevPropertyMap.getCompound("IMAN_master_form_rev"));
            //itemRevMasterPropertyMap = (CreateInObjectHelper) itemRevPropertyMap.getCompound("IMAN_master_form_rev");
            itemRevMasterPropertyMap.setComponent(masterForm);
            m_createInObjectHelper.add(itemRevMasterPropertyMap);
            
            success = validateProperties(validationContext.toString(), new IPropertyMap[]{itemRevMasterPropertyMap});
        }
        
        return success;

    }
    
    private TCComponent getMasterForm(TCComponent tcComponent) throws TCException
    {
        TCComponent masterForm = null;
        String relation = "IMAN_master_form_rev";
        
        AIFComponentContext[] secondaryObjects = tcComponent.getRelated(relation);
        masterForm = tcComponent.getRelatedComponents(relation)[0];
        
        for (int inx = 0; inx < secondaryObjects.length; inx++)
        {
            if(secondaryObjects[inx].getComponent() instanceof TCComponentForm)
            {
                masterForm = (TCComponent) secondaryObjects[inx].getComponent();
                break;
            }
        }
        
        return masterForm;
    }
    
    
}
