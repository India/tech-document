package com.nov.rac.form.dialog.savedelegate;

import java.util.List;
import java.util.Vector;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.operations.AbstractFormOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.utils.NOVGRMRelationsHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.services.rac.core._2006_03.DataManagement.ItemProperties;

public class TabSaveDelegate extends AbstractFormOperationDelegate
{
    private static final String PART_REVISION_TYPE = "Nov4Part Revision";
    protected Vector<IPropertyMap> m_createInObjectHelper = new Vector<IPropertyMap>();
    private Registry m_registry = null;
    
    public TabSaveDelegate(Registry registry)
    {
        super();
        m_registry = registry;
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        String boType = PART_REVISION_TYPE;
        String formType = FormHelper.getTargetComponentType();
        
        IPropertyMap propertyMap = new SimplePropertyMap();
        
        CreateInCompoundHelper partRevCompoundHelper = new CreateInCompoundHelper(
                propertyMap);
        partRevCompoundHelper.addCompound("MasterForm", "IMAN_master_form_rev");
        
        fillPanelsData("", formType, boType, getOperationInputProviders(),
                propertyMap);
        
        TCComponent saveableComponent = propertyMap.getComponent();
        
        IPropertyMap itemRevPropertyMap = new CreateInObjectHelper(
                saveableComponent.getType(),
                CreateInObjectHelper.OPERATION_UPDATE, propertyMap);
        itemRevPropertyMap.setComponent(saveableComponent);
        
        m_createInObjectHelper.add(itemRevPropertyMap);
        
        return true;
        
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) m_createInObjectHelper
                .toArray(new CreateInObjectHelper[m_createInObjectHelper.size()]);
        
        if (createInObjectHelper != null && createInObjectHelper.length > 0)
        {
            CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
            
            CreateObjectsSOAHelper.throwErrors(createInObjectHelper);
        }
        return true;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected boolean validateProperties(String itemType,
            IPropertyMap[] propMapArray) throws TCException
    {
        IPropertyValidator partPropertiesValidator = getValidator(itemType);
        
        return partPropertiesValidator.validateProperties(propMapArray);
    }
    
    protected IPropertyValidator getValidator(String itemType)
    {
        IPropertyValidator validator = null;
        String formType = FormHelper.getTargetComponentType();
        StringBuffer context = new StringBuffer(formType);
        context.append(".").append(itemType).append(".")
                .append("PROPERTY_VALIDATOR");
        
        String[] validatorClass = RegistryUtils.getConfiguration(
                context.toString(), getRegistry());
        
        if (validatorClass.length > 0)
        {
            try
            {
                validator = (IPropertyValidator) Instancer
                        .newInstanceEx(validatorClass[0]);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return validator;
    }
    
    protected void addToCreateInObjectHelper(IPropertyMap propertyMap)
    {
        TCComponent saveableComponent = propertyMap.getComponent();
        if (saveableComponent != null)
        {
            String componentType = saveableComponent.getType();
            
            IPropertyMap itemRevPropertyMap = new CreateInObjectHelper(
                    componentType, CreateInObjectHelper.OPERATION_UPDATE,
                    propertyMap);
            itemRevPropertyMap.setComponent(saveableComponent);
            
            m_createInObjectHelper.add(itemRevPropertyMap);
        }
    }
    
}
