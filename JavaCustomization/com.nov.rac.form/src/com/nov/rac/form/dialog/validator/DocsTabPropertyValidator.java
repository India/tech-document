package com.nov.rac.form.dialog.validator;

import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.services.publishdataset.NovPublishDatasetHelper;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.Registry;

public class DocsTabPropertyValidator implements IPropertyValidator
{
    String[] m_excludedTypesArray = PreferenceUtils.getStringValues(TCPreferenceService.TC_preference_group, "NOV_Exclude_Publish_Dataset");
    
    @Override
    public boolean validateProperties(IPropertyMap[] propertyMaps) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap eachPropertyMap : propertyMaps)
        {
            if(null != eachPropertyMap)
            {
                validateProperties(eachPropertyMap, stack);
            }
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            String errorStr = getRegistry().getString("ExcludedDatasetTypes.ERROR");
            throw ExceptionHelper.formattedException(stack, errorStr);
        }
        return true;
    }

    private void validateProperties(IPropertyMap propertyMap, ExceptionStack stack)
    {
        if(propertyMap.getLogical(NovPublishDatasetHelper.PROP_IS_INCLUDE))
        {
            String datasetType = PropertyMapHelper.handleNull(propertyMap.getString(NovPublishDatasetHelper.PROP_DATASET_TYPE));
            if(isDatasetTypeExcluded(m_excludedTypesArray, datasetType))
            {
                TCException e = new TCException(datasetType);
                stack.addErrors(e);
            }
        }
    }

    private boolean isDatasetTypeExcluded(String[] excludedTypesArray, String datasetType) 
    {
        for (int i = 0; excludedTypesArray != null && i < excludedTypesArray.length; i++)
        {
            if (datasetType.equals(excludedTypesArray[i]))
            {
                return true;
            }
        }
        return false;
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.form.impl.enform.enform");
    }
    
}
