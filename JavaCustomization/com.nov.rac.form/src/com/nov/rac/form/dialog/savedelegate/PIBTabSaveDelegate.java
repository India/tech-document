package com.nov.rac.form.dialog.savedelegate;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class PIBTabSaveDelegate extends TabSaveDelegate
{
    private static final String DOCUMENTS_REVISION_TYPE = "Documents Revision";
    private static final String PIB_TAB_NAME = "PIB";
    
    public PIBTabSaveDelegate(Registry registry)
    {
        super(registry);
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        String formType = FormHelper.getTargetComponentType();
        
        StringBuilder context = new StringBuilder(PIB_TAB_NAME);
        context.append(".").append(DOCUMENTS_REVISION_TYPE);
        
        IPropertyMap propertyMap = new SimplePropertyMap();
        
        fillPanelsData("", formType, context.toString(), 
                getOperationInputProviders(), propertyMap);
        
        TCComponent saveableComponent = propertyMap.getComponent();
        if (saveableComponent != null)
        {
            String componentType = saveableComponent.getType();
            
            IPropertyMap itemRevPropertyMap = new CreateInObjectHelper(componentType,
                    CreateInObjectHelper.OPERATION_UPDATE, propertyMap);
            itemRevPropertyMap.setComponent(saveableComponent);
            
            m_createInObjectHelper.add(itemRevPropertyMap);
        }
        
        return true;
        
    }
    
}
