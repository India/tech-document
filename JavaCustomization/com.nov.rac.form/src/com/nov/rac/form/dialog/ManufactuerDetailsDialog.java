package com.nov.rac.form.dialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.manufacturerhelper.ManufacturingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class ManufactuerDetailsDialog extends AbstractSWTDialog implements IPublisher
{
    private TCTable m_manufacturingDetailsDialogTable = null;
    private Registry m_registry = null;
    private Button m_addButton = null;
    
    public ManufactuerDetailsDialog(Shell shell, int i)
    {
        super(shell, i);
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    public Registry getRegistry()
    {
        if(m_registry == null)
        {
            m_registry = Registry.getRegistry(this);
        }
        return m_registry;
    }
    
    public void setLocationReferenceControl(Button buttonControl)
    {
        m_addButton = buttonControl;
    }
    @Override
    protected Control createDialogArea(Composite parent)
    {
        setTitle();
        ScrolledComposite scrolledComposite = new ScrolledComposite(parent,
                SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        
        scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
                true, true, 1, 1));
        Composite composite = new Composite(scrolledComposite, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
        createSearchComposite(composite);
        createManufacturerDetailsDialogTable(composite);
        loadManufacturerDetailsDialogTable();
        addDisposeListener(parent);
        
        scrolledComposite.setContent(composite);
        scrolledComposite.setExpandHorizontal(true);
        scrolledComposite.setExpandVertical(true);
        // Point sizeCompsite =composite.computeSize(SWT.DEFAULT,
        // SWT.DEFAULT);
        
        int Width = SWTUIHelper.convertHorizontalDLUsToPixels(parent, 75);
        int Height = SWTUIHelper.convertVerticalDLUsToPixels(parent, 125);
        Point sizeCompsite = new Point(Height, Width);
        scrolledComposite.setMinSize(sizeCompsite);
        parent.setSize(sizeCompsite);
        return parent;
    }
    
    private void createSearchComposite(Composite composite)
    {
        Composite searchComposite = new Composite(composite, SWT.NONE);
        GridData gd_searchComposite = new GridData(SWT.FILL, SWT.FILL,
                true, false, 1, 1);
        searchComposite.setLayoutData(gd_searchComposite);
        searchComposite.setLayout(new GridLayout(3, false));
        
        Label lblMfg = new Label(searchComposite, SWT.NONE);
        lblMfg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1));
        lblMfg.setText(getRegistry().getString("MFG.Label"));
        
        final Text searchText = new Text(searchComposite, SWT.BORDER);
        searchText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        
        Button btnSearch = new Button(searchComposite, SWT.NONE);
        btnSearch.setText(getRegistry().getString("Search.Label"));
        
        btnSearch.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                List<IPropertyMap> propMapList = new ArrayList<IPropertyMap>();
                String search_str = searchText.getText();
                ManufacturingHelper mfgHelper = ManufacturingHelper
                        .getInstance();
                List<String> mfgNames = new ArrayList<String>();
                mfgNames.addAll(mfgHelper.getMFG_IdNameMapping().values());
                
                for (String mfg : mfgNames)
                {
                    if (mfg.contains(search_str))
                    {
                        IPropertyMap propertyMap = new SimplePropertyMap();
                        propertyMap.setString("nov4_manufacturer_id", mfg);
                        propMapList.add(propertyMap);
                    }
                }
                
                final IPropertyMap[] maps = new IPropertyMap[propMapList
                        .size()];
                final IPropertyMap[] mapsArray = propMapList.toArray(maps);
                
                if (maps.length > 0)
                {
                    SwingUtilities.invokeLater(new Runnable()
                    {
                        public void run()
                        {
                            try
                            {
                                m_manufacturingDetailsDialogTable.clear();
                                TableUtils.populateTable(mapsArray,
                                        m_manufacturingDetailsDialogTable);
                            }
                            catch (TCException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
        searchText.addKeyListener(new KeyListener()
        {
            
            @Override
            public void keyReleased(KeyEvent keyevent)
            {
                if (searchText.getText().equals(""))
                {
                    m_manufacturingDetailsDialogTable.clear();
                    loadManufacturerDetailsDialogTable();
                }
                
            }
            
            @Override
            public void keyPressed(KeyEvent keyevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private void loadManufacturerDetailsDialogTable()
    {
        try
        {
            ManufacturingHelper mfgHelper = ManufacturingHelper
                    .getInstance();
            Map<String, String> mfgDetails = mfgHelper
                    .getMFG_IdNameMapping();
            List<IPropertyMap> propMapList = new ArrayList<IPropertyMap>();
            Iterator<Map.Entry<String, String>> itr = mfgDetails.entrySet().iterator();
            
            int index = 0;
            while (itr.hasNext())
            {
                IPropertyMap propertyMap = new SimplePropertyMap();
                Map.Entry<String, String> entry = (Entry<String, String>) itr
                        .next();
                String mfgId = entry.getKey();
                String mfgNumber = entry.getValue();
                // propertyMap.setString("item_id", mfgId);
                propertyMap.setString("nov4_manufacturer_id", mfgNumber);
                propMapList.add(propertyMap);
                index++;
            }
            
            IPropertyMap[] maps = new IPropertyMap[propMapList.size()];
            maps = propMapList.toArray(maps);
            
            TableUtils.populateTable(maps,
                    m_manufacturingDetailsDialogTable);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void addDisposeListener(Composite parent)
    {
        
    }
    
    private void createManufacturerDetailsDialogTable(Composite parent)
    {
        Composite tableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        tableComposite.setLayoutData(gd_tableComposite);
        String[] propertiesToDisplay = getRegistry().getStringArray(
                "ManufacturingDetailsDialogTable.PropertyNames");
        String[] columnNames = getRegistry().getStringArray(
                "ManufacturingDetailsDialogTable.ColumnNames");
        
        m_manufacturingDetailsDialogTable = new TCTable(
                propertiesToDisplay, columnNames);
        m_manufacturingDetailsDialogTable.setShowVerticalLines(true);
        m_manufacturingDetailsDialogTable
                .setAutoResizeMode(m_manufacturingDetailsDialogTable.AUTO_RESIZE_ALL_COLUMNS);
        m_manufacturingDetailsDialogTable.setEditable(true);
        JScrollPane scrollPane = new JScrollPane(
                m_manufacturingDetailsDialogTable);
        SWTUIUtilities.embed(tableComposite, scrollPane, false);
        
    }
    
    private void setTitle()
    {
        String title = getRegistry().getString(
                "ManufactuerDetailsDialog.Title");
        this.getShell().setText(title.toString());
    }
    
    @Override
    protected void okPressed()
    {
        AIFTableLine[] rows = m_manufacturingDetailsDialogTable
                .getSelected();
        if (rows != null && rows.length > 0)
        {
            IController controller = ControllerFactory.getInstance()
                    .getDefaultController();
            
            PublishEvent event = new PublishEvent(this,
                    GlobalConstant.MFG_DETAILS_EVENT, rows, null);
            controller.publish(event);
        }
        
        super.okPressed();
    }
    
    @Override
    protected Point getInitialLocation(Point point)
    {
        Point location = super.getInitialLocation(point);
        
        if(m_addButton != null)
        {
            Point btnLocation = m_addButton
                    .toDisplay(m_addButton.getLocation());
            Point btnSize = m_addButton.getSize();
            
            Point size = getInitialSize();
            
            location = new Point(btnLocation.x, btnLocation.y - size.y);
            
            if (location.y < 0)
            {
                location = new Point(btnLocation.x, btnLocation.y + btnSize.y);
            }
        }
        
        return location;
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
}
