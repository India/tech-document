package com.nov.rac.form.dialog.validator;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class OverviewTabPropertyValidator implements IPropertyValidator
{
    
    @Override
    public boolean validateProperties(IPropertyMap[] propertyMaps) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        
        for (IPropertyMap eachPropertyMap : propertyMaps)
        {
            if(null != eachPropertyMap)
            {
                validateProperties(eachPropertyMap, stack);
            }
        }
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }

    private void validateProperties(IPropertyMap propertyMap, ExceptionStack stack)
    {
        Registry registry = getRegistry();
        
        if ((!FormHelper.hasContent(propertyMap.getDouble("nov4length")) && FormHelper.hasContent(propertyMap
                .getString("nov4lwh_units")))
                || (FormHelper.hasContent(propertyMap.getDouble("nov4length")) && !FormHelper.hasContent(propertyMap
                        .getString("nov4lwh_units"))))
        {
            String errorStr = registry.getString("Dimension_Error_Message") + " " +  registry.getString("Length.Text");
            TCException e = new TCException(errorStr);
            stack.addErrors(e);
        }
        
        if ((!FormHelper.hasContent(propertyMap.getDouble("nov4width")) && FormHelper.hasContent(propertyMap
                .getString("nov4lwh_units")))
                || (FormHelper.hasContent(propertyMap.getDouble("nov4width")) && !FormHelper.hasContent(propertyMap
                        .getString("nov4lwh_units"))))
        {
            String errorStr = registry.getString("Dimension_Error_Message") + " " +  registry.getString("Width.Text");
            TCException e = new TCException(errorStr);
            stack.addErrors(e);
        }
        
        if ((!FormHelper.hasContent(propertyMap.getDouble("nov4height")) && FormHelper.hasContent(propertyMap
                .getString("nov4lwh_units")))
                || (FormHelper.hasContent(propertyMap.getDouble("nov4height")) && !FormHelper.hasContent(propertyMap
                        .getString("nov4lwh_units"))))
        {
            String errorStr = registry.getString("Dimension_Error_Message") + " " + registry.getString("Height.Text");
            TCException e = new TCException(errorStr);
            stack.addErrors(e);
        }
        
        if ((!FormHelper.hasContent(propertyMap.getDouble("nov4weight")) && FormHelper.hasContent(propertyMap
                .getString("nov4weight_units")))
                || (FormHelper.hasContent(propertyMap.getDouble("nov4weight")) && !FormHelper.hasContent(propertyMap
                        .getString("nov4weight_units"))))
        {
            String errorStr = registry.getString("Dimension_Error_Message") + " " + registry.getString("Weight.Text");
            TCException e = new TCException(errorStr);
            stack.addErrors(e);
        }
        
        if ((!FormHelper.hasContent(propertyMap.getDouble("nov4volume")) && FormHelper.hasContent(propertyMap
                .getString("nov4volume_units")))
                || (FormHelper.hasContent(propertyMap.getDouble("nov4volume")) && !FormHelper.hasContent(propertyMap
                        .getString("nov4volume_units"))))
        {
            String errorStr = registry.getString("Dimension_Error_Message") + " " + registry.getString("Volume.Text");
            TCException e = new TCException(errorStr);
            stack.addErrors(e);
        }
    }
    
    protected Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.form.impl.enform.enform");
    }
    
}
