package com.nov.rac.form.dialog.delegate;

import java.util.List;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class PIBTabLoadDelegate extends TabLoadDelegate
{

    public PIBTabLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void loadComponent(Object inputObject, String tabName)
            throws TCException
    {
        try
        {
            
            IPropertyMap propertyMap = (IPropertyMap) inputObject;
            TCComponent documentsRev = propertyMap.getComponent();
            TCComponent[] relatedComponents = documentsRev.getRelatedComponents("Nov4_secondary_form");
            
            TCComponent pibForm = null;
            
            for (TCComponent tcComponent : relatedComponents)
            {
                if(tcComponent.getType().equalsIgnoreCase("_pibform_"))
                {
                    pibForm = tcComponent;
                }
            }
            
            if(pibForm==null)
            {
                return;
            }
            
            
            IPropertyMap propMapPibForm = new SimplePropertyMap();
            propMapPibForm.setComponent(pibForm);
            
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String formType = FormHelper.getTargetComponentType();
            StringBuilder contextType = new StringBuilder(formType);
            contextType.append(".").append(tabName);
            
            String[] propNames = getPropertyNames(theGroup,
                    contextType.toString());
            
           
            loadPropertyMap(propMapPibForm, contextType.toString(), theGroup,
                    propNames);
            
            propertyMap.setCompound("Nov4_secondary_form", propMapPibForm);
            
            loadPanelsData(theGroup, contextType.toString(), getInputProviders(),
                    propertyMap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
}
