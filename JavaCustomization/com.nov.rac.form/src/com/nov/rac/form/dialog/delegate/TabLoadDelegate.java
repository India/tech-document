package com.nov.rac.form.dialog.delegate;

import java.util.List;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.loaddelegate.AbstractLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class TabLoadDelegate extends AbstractLoadDelegate
{
    protected static final String PROPERTY_NAMES = "PROPERTY_NAMES";
    
    public TabLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    @Override
    public void loadComponent(Object inputObject, String tabName)
            throws TCException
    {
        try
        {
            
            IPropertyMap propertyMap = (IPropertyMap) inputObject;
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String formType = FormHelper.getTargetComponentType();
            StringBuilder contextType = new StringBuilder(formType);
            contextType.append(".").append(tabName);
            
            String[] propNames = getPropertyNames(theGroup,
                    contextType.toString());
            
            loadPropertyMap(propertyMap, contextType.toString(), theGroup,
                    propNames);
            
            loadPanelsData(theGroup, contextType.toString(), getInputProviders(),
                    propertyMap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    protected String[] getPropertyNames(String theGroup, String context)
    {
        context = context +"."+ PROPERTY_NAMES;
        String[] propNames = RegistryUtils.getConfiguration(theGroup, context,
                getRegistry());
        return propNames;
    }
    
    protected void loadPropertyMap(IPropertyMap propertyMap,
            String context, String theGroup, String[] propNames)
            throws  Exception
    {
        
        TCComponent targetItem = propertyMap.getComponent();
        
        NOVObjectPolicy formPolicy = null;
        NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
                context);
        
        //
        formPolicy = novObjectPolicyFactory.createObjectPolicy(theGroup,
                getRegistry());
        
        NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(
                targetItem); // Constructor just to get session.
        dmSOAHelper.getObjectProperties(targetItem, propNames, formPolicy);// can pass targetItems[]
        //
        PropertyMapHelper.componentToMap(targetItem, propNames, propertyMap);
     
    }
    
    @Override
    public void setComponent(TCComponent tcComponent)
    {
        // TODO Auto-generated method stub
        
    }
}
