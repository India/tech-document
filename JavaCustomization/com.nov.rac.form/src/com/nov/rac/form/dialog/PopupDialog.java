package com.nov.rac.form.dialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISaveablePart;

import com.nov.rac.common.Pane;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class PopupDialog extends AbstractSWTDialog implements ISubscriber
{

    private Registry m_registry = null;
    private List<IUIPanel> m_paneContainer = new ArrayList<IUIPanel>();
    private IPropertyMap m_propertyMap = null;
    private int m_splitCount = 1;
    
    public PopupDialog(Shell shell)
    {
        super(shell);
        m_registry = Registry.getRegistry(this);
    }
    
    public void setInputPropertyMap(IPropertyMap iPropertyMap)
    {
        m_propertyMap = iPropertyMap;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    /*
     * method: setDialogSplitCount(...) manipulates m_splitCount count Variable:
     * m_splitCount This variable is used to manipulate number of composites
     * needs to be created on pop-up dialog based on mode selected(compare or
     * non compare) for placing left pane,separator and right pane.
     */
    public void setDialogSplitCount(int split)
    {
        int temp = 0;
        if (split > 1)
        {
            temp = split - 1;
        }
        m_splitCount = split + temp;
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        setTitle();
        ScrolledComposite scrolledComposite = new ScrolledComposite(parent,
                SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        
        scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
        Composite composite = new Composite(scrolledComposite, SWT.NONE);
        composite.setLayout(new GridLayout(m_splitCount, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1));
        
        List<Composite> splitCompositeList = new ArrayList<Composite>();
        splitCompositeList = createSplitComposite(composite, m_splitCount);
        
        TCComponent[] revisionList = null;
        
        try
        {
            revisionList = FormHelper.getItemRevisions(m_propertyMap
                    .getComponent());
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
        
        int index = 2; // This index is used to calculate previous revision
                       // index.
        GlobalConstant.COMPARE_NONCOMPARE_FLAG = false;
        for (int inx = 0; inx < splitCompositeList.size(); inx += 2)
        {
            IUIPanel pane = new Pane(splitCompositeList.get(inx), SWT.NONE);
            try
            {
                TCComponent tcComponent = m_propertyMap.getComponent();
                
                if (m_splitCount > 1)
                {
                    GlobalConstant.COMPARE_NONCOMPARE_FLAG = true;
                    tcComponent = revisionList[revisionList.length - index];
                }
                
                pane.createUI();
                if (pane instanceof INOVFormLoadSave)
                {
                    INOVFormLoadSave loadablepanel = (INOVFormLoadSave) pane;
                    IPropertyMap loadMap = new SimplePropertyMap();
                    
                    loadMap.setComponent(tcComponent);
                    loadablepanel.load(loadMap);
                }
                
                index--;
                m_paneContainer.add(pane);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
        }
        
        for (int jnx = 1; jnx < splitCompositeList.size(); jnx += 2)
        {
            Composite separatorComposite = splitCompositeList.get(jnx);
            separatorComposite.setLayoutData(new GridData(SWT.CENTER, SWT.FILL,
                    false, true, 1, 1));
            Label separator = new Label(separatorComposite, SWT.SEPARATOR);
            separator.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false,
                    true, 1, 1));
        }
        
        scrolledComposite.setContent(composite);
        scrolledComposite.setExpandHorizontal(true);
        scrolledComposite.setExpandVertical(true);
        Point sizeCompsite =composite.computeSize(SWT.DEFAULT,
                SWT.DEFAULT);
        
        int screenWidth =Display.getDefault().getBounds().width;
//        if(screenWidth<sizeCompsite.x)
//        {
//            sizeCompsite.x = screenWidth;
//        }
        scrolledComposite.setMinSize(sizeCompsite);
        
        registerProperties();
        addDisposeListener(parent);
        // setDialogLocationAndSize();
        return parent;
    }
    
    private void addDisposeListener(Composite parent)
    {
        try
        {
            parent.getShell().addDisposeListener(new DisposeListener()
            {
                
                @Override
                public void widgetDisposed(DisposeEvent disposeevent)
                {
                    unregisterSubscriber();
                    setReturnCode(1); // when dialog is disposed or closed
                    
                    for (IUIPanel pane : m_paneContainer)
                    {
                        pane.dispose();
                    }
                    
                    setGlobalConstant();
                    
                                       
                    // composite.dispose();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private List<Composite> createSplitComposite(Composite composite,
            int splitCount)
    {
        List<Composite> splitCompositeList = new ArrayList<Composite>();
        
        for (int inx = 0; inx < splitCount; inx++)
        {
            Composite newComposite = new Composite(composite, SWT.NONE);
            newComposite.setLayout(new GridLayout(1, true));
            newComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                    true, 1, 1));
            splitCompositeList.add(newComposite);
        }
        return splitCompositeList;
    }
    
    private void setTitle()
    {
        String objectId = m_propertyMap.getString("item_id");
        String objectName = m_propertyMap.getString("object_name");
        
        StringBuilder title = new StringBuilder("");
        title.append(objectId).append(";").append(objectName);
        this.getShell().setText(title.toString());
    }
    
    @Override
    protected Control createButtonBar(Composite composite)
    {
        return null;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(
                GlobalConstant.EVENT_COMPARE_MODE_SELECTED))
        {
            Shell shell = getShell();
            getShell().dispose();
            openCompareDialog(shell);
        }
    }
    
    private void registerProperties()
    {
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_COMPARE_MODE_SELECTED, getSubscriber());
    }
    
    private IController getController()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        return controller;
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_COMPARE_MODE_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    public void openCompareDialog(Shell shell)
    {
        try
        {
            TCComponent selectedPart = m_propertyMap.getComponent();
            IPropertyMap iMap = new SimplePropertyMap();
            iMap.setComponent(selectedPart);
            
            String objectId = selectedPart.getProperty("item_id");
            String objectName = selectedPart.getProperty("object_name");
            
            iMap.setString("item_id", objectId);
            iMap.setString("object_name", objectName);
            
            PopupDialog dialog = new PopupDialog(shell);
            dialog.setInputPropertyMap(iMap);
            dialog.setDialogSplitCount(2);
            dialog.open();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    protected Point getInitialLocation(Point point)
    {
        
        // Move the dialog to the center of the top level shell.
        Rectangle shellBounds = getShell().getParent().getBounds();
        Point dialogSize = getInitialSize();
        
        int x = shellBounds.x + (shellBounds.width - dialogSize.x) / 2;
        int y = shellBounds.y + (shellBounds.height - dialogSize.y) / 2;
        Point location = new Point(x, y);
        
        return location;
    }
    

    @Override
    public boolean close()
    {
        saveOnClose();
        
        return super.close();
    }

    protected void saveOnClose()
    {
        
        for (IUIPanel eachPane : m_paneContainer)
        {
            if (eachPane instanceof ISaveablePart)
            {
                ISaveablePart saveablePart = (ISaveablePart) eachPane;
                boolean isDirty = saveablePart.isDirty();
                
                if (isDirty)
                {
                    String title = m_registry.getString("ConfirmationDialog.TITLE");
                    String message = m_registry.getString("ConfirmationDialog.MESSAGE");
                    boolean response = MessageDialog.openQuestion(getShell(), title, message);
                    
                    if (response)
                    {
                        IProgressMonitor monitor = new NullProgressMonitor();
                        saveablePart.doSave(monitor);
                    }
                }
            }
        }
        
    }
    
    private void setGlobalConstant()
    {
        // Setting this constant to make enable the compare Rev Button in summary page after opened and closed compare
        // Revision Dialog.
        GlobalConstant.COMPARE_NONCOMPARE_FLAG = false;
    }
    
    /*
     * private void setDialogLocationAndSize() { Double widthMultipler = 0.6;
     * Double heightMultipler = 0.6; if(m_splitCount > 1) { widthMultipler =
     * 0.9; } Rectangle monitorArea = getShell().getDisplay().getClientArea();
     * Rectangle shellArea = getShell().getBounds();
     * this.getShell().setSize((int) (widthMultipler * (monitorArea.width)),
     * (int) (heightMultipler * (monitorArea.height))); // set according to
     * screen // size int x = monitorArea.x + (monitorArea.width -
     * shellArea.width) / 2; int y = monitorArea.y + (monitorArea.height -
     * shellArea.height) / 2; getShell().setLocation(x, y); }
     */
    
}