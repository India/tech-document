package com.nov.rac.form.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVFormDispositionViewerDelegate extends NOVFormViewerDelegate
{
    private Registry m_registry = null;
    public NOVFormDispositionViewerDelegate(TCComponent formComponent, Registry registry)
    {
        super(formComponent, registry);
        m_registry = registry;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
    	IPropertyMap formPropertyMap = loadFormProperties();
    	CreateInObjectHelper dispositionPropertyMap = null;
        
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        String[] dispositionProperties = getRegistry().getStringArray("Disposition.PROP");
        WorkflowProcess currentProcess = WorkflowProcess.getWorkflow();
        
        TCComponent[] dispositionObjects = currentProcess.getTargetDispositions();
        List<TCComponent> dispositionObjectsList = new ArrayList<TCComponent>();
        dispositionObjectsList = Arrays.asList(dispositionObjects);
        
        IPropertyMap propertyMap = new SimplePropertyMap();
        List<IPropertyMap> propertyMapList = new ArrayList<IPropertyMap>();
        
        String targetDispositionObjectType = m_registry.getString("TargetDisposition.TYPE");
        fillPanelsData(theGroup, getComponentType(), targetDispositionObjectType, getOperationInputProviders(),
                propertyMap);
        
        if (!propertyMap.isEmpty())
        {
            String targetDispoPropName = getRegistry().getString("targetDisposition.PROP");
            propertyMapList = Arrays.asList(propertyMap.getCompoundArray(targetDispoPropName));
        }
        
        List<IPropertyMap> dispositionDataList = new ArrayList<IPropertyMap>();
        for (IPropertyMap propMap : propertyMapList)
        {
            String dispositionId = propMap.getString("targetitemid");
            
            for (TCComponent tcComponent : dispositionObjectsList)
            {
                String componentId = tcComponent.getProperty("targetitemid");
                if (dispositionId.equals(componentId))
                {
                	dispositionPropertyMap = new CreateInObjectHelper(getComponentType(),
                            CreateInObjectHelper.OPERATION_UPDATE);
                    for (String strPropertyName : dispositionProperties)
                    {
                        if (propMap.getString(strPropertyName) != null
                                && !propMap.getString(strPropertyName).equals(""))
                        {
                            dispositionPropertyMap.setString(strPropertyName, propMap.getString(strPropertyName));
                        }
                    }
                    
                    dispositionPropertyMap.setTargetObject(tcComponent);
                    dispositionDataList.add(dispositionPropertyMap);
                   
                    m_createInObjectHelper.add(dispositionPropertyMap);
                    
                    break;
                }
            }
           
        }
        
       dispositionDataList.add(formPropertyMap);
       validateFormProperties(dispositionDataList.toArray(new IPropertyMap[dispositionDataList.size()]));
      
        return true;
    }
   
}
