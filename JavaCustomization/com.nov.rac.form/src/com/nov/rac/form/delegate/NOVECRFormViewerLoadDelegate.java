package com.nov.rac.form.delegate;

import java.util.List;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class NOVECRFormViewerLoadDelegate extends NOVFormViewerLoadDelegate {

	private static final String FORM = "Form";
	private static final String PROPERTY_NAMES = "PROPERTY_NAMES";

	public NOVECRFormViewerLoadDelegate(List<IUIPanel> inputProviders) {
		super(inputProviders);
	}

	@Override
	public void loadComponent(Object inputObject, String group)
			throws TCException {
		try {
			IPropertyMap iPropertyMap = (IPropertyMap) inputObject;

			// To set targetItem
			iPropertyMap.setComponent(FormHelper.getTargetComponent());

			// 1.0 Load the panels which shows only Form related data
			StringBuilder objectType = new StringBuilder(getComponentType());
			String context = objectType.append(".").append(FORM).toString();

			String[] propertyNames = getPropertyNames(group, context);

			loadPropertyMap(iPropertyMap, context, group, propertyNames);

			loadPanelsData(group, objectType.toString(), getInputProviders(),
					iPropertyMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected String[] getPropertyNames(String theGroup, String context) {
		context = context + "." + PROPERTY_NAMES;
		String[] propNames = RegistryUtils.getConfiguration(theGroup, context,
				getRegistry());
		return propNames;
	}

	protected void loadPropertyMap(IPropertyMap propertyMap, String context,
			String theGroup, String[] propNames) throws Exception {

		TCComponent targetItem = propertyMap.getComponent();

		NOVObjectPolicy formPolicy = null;
		NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
				context);

		formPolicy = novObjectPolicyFactory.createObjectPolicy(theGroup,
				getRegistry());

		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(
				targetItem);
		
		dmSOAHelper.getObjectProperties(targetItem, propNames, formPolicy);
		
		PropertyMapHelper.componentToMap(targetItem, propNames, propertyMap);

	}
}
