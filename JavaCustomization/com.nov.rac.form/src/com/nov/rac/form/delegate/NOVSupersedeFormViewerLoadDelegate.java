package com.nov.rac.form.delegate;

import java.util.ArrayList;
import java.util.List;

import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class NOVSupersedeFormViewerLoadDelegate extends NOVFormViewerLoadDelegate
{
    private static final String FORM = "Form";
    private static final String TARGET_DISPOSITION = "targetdisposition";
    
    public NOVSupersedeFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void loadComponent(Object inputObject, String context)
            throws TCException
    {
        String theGroup = context;
        IPropertyMap iPropertyMap = (IPropertyMap) inputObject;
        // 1.0 Load the panels which shows only Form related data
        StringBuilder objectType = new StringBuilder(getComponentType());
        objectType.append(".").append(FORM);
        
        loadPanelsData(theGroup, objectType.toString(), getInputProviders(),
                iPropertyMap);
        
        // 2.0 Add target disposition related data
        addTargetsDispositionData(iPropertyMap);
        
        // 3.0 Load the panels which shows disposition data
        StringBuilder dispositionObjectType = new StringBuilder(
                getComponentType());
        dispositionObjectType.append(".").append(TARGET_DISPOSITION);
        
        loadPanelsData(theGroup, dispositionObjectType.toString(),
                getInputProviders(), iPropertyMap);
        
    }
    
    private void addTargetsDispositionData(IPropertyMap iPropertyMap)
    {
        List<IPropertyMap> targetsDispositions = new ArrayList<IPropertyMap>();
        
        WorkflowProcess workflow = WorkflowProcess.getWorkflow();
        TCComponent[] dispositionArray = workflow.getTargetDispositions();
        
        String[] dispositionPropNames = getRegistry().getStringArray(
                "targetdisposition.PROPERTIES_AS_ATTRIBUTE");
        
        for (TCComponent disposition : dispositionArray)
        {
            IPropertyMap propMap = new SimplePropertyMap();
            propMap.setComponent(disposition);
            
            try
            {
                PropertyMapHelper.componentToMap(disposition,
                        dispositionPropNames, propMap);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            targetsDispositions.add(propMap);
        }
        
        String dispPropName = getRegistry().getString("targetDisposition.PROP");
        iPropertyMap.setCompoundArray(dispPropName, targetsDispositions
                .toArray(new IPropertyMap[targetsDispositions.size()]));
        
    }
    
}
