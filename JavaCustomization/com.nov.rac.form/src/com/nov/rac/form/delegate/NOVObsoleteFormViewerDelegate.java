package com.nov.rac.form.delegate;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class NOVObsoleteFormViewerDelegate extends NOVFormDispositionViewerDelegate
{

	public NOVObsoleteFormViewerDelegate(TCComponent formComponent,
			Registry registry) {
		super(formComponent, registry);
		
	}
	
}
    
	
