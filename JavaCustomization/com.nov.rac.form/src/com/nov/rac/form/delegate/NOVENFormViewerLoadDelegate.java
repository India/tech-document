package com.nov.rac.form.delegate;

import java.util.List;

import com.nov.rac.ui.IUIPanel;

public class NOVENFormViewerLoadDelegate extends
        NOVFormCommonViewerLoadDelegate
{
    
    public NOVENFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
}
