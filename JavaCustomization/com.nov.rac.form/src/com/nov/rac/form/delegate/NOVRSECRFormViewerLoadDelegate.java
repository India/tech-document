package com.nov.rac.form.delegate;

import java.util.List;
import java.util.Vector;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.loaddelegate.AbstractFormLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NOVRSECRFormViewerLoadDelegate extends NOVFormViewerLoadDelegate
{
    private static final String NOV4_ECR_AFFECTED_PARTS = "nov4_ecr_affected_parts";
    private static final String NOV4_AFFECTED_DOC_PARTS = "Nov4_AffectedDocParts";
    private static final String FORM = "Form";
    private static final String PROPERTY_NAMES = "PROPERTY_NAMES";
    
    public NOVRSECRFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void loadComponent(Object inputObject, String group)
            throws TCException {
        try {
            IPropertyMap iPropertyMap = (IPropertyMap) inputObject;

            // To set targetItem
            iPropertyMap.setComponent(FormHelper.getTargetComponent());

            // 1.0 Load the panels which shows only Form related data
            StringBuilder objectType = new StringBuilder(getComponentType());
            String context = objectType.append(".").append(FORM).toString();

            String[] propertyNames = getPropertyNames(group, context);

            loadPropertyMap(iPropertyMap, context, group, propertyNames);

            loadPanelsData(group, objectType.toString(), getInputProviders(),
                    iPropertyMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected String[] getPropertyNames(String theGroup, String context) {
        context = context + "." + PROPERTY_NAMES;
        String[] propNames = RegistryUtils.getConfiguration(theGroup, context,
                getRegistry());
        return propNames;
    }

    protected void loadPropertyMap(IPropertyMap propertyMap, String context,
            String theGroup, String[] propNames) throws Exception {

        TCComponent targetItem = propertyMap.getComponent();

        NOVObjectPolicy formPolicy = null;
        NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
                context);

        formPolicy = novObjectPolicyFactory.createObjectPolicy(theGroup,
                getRegistry());

        NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(
                targetItem);
        
        dmSOAHelper.getObjectProperties(targetItem, propNames, formPolicy);
        
        PropertyMapHelper.componentToMap(targetItem, propNames, propertyMap);
        
        loadAffectedPartsPropertyMap(propertyMap);

    }

    private void loadAffectedPartsPropertyMap(IPropertyMap propertyMap) throws TCException
    {
        TCComponent[] affectedParts = propertyMap.getTagArray(NOV4_ECR_AFFECTED_PARTS);
        String context = NOV4_AFFECTED_DOC_PARTS + "." + PROPERTY_NAMES;
        String[] propNames = RegistryUtils.getConfiguration("", context,
                getRegistry());
        Vector<IPropertyMap> affectedPartPropMaps = new Vector<IPropertyMap>();
        
        for (int inx = 0; inx < affectedParts.length; inx++)
        {
            IPropertyMap localPropMap = new SimplePropertyMap();
            PropertyMapHelper.componentToMap(affectedParts[inx], propNames, localPropMap);
            
            String objectType = getObjectType(affectedParts[inx]);
            localPropMap.setString("object_type", objectType);
            
            affectedPartPropMaps.add(localPropMap);
            
        }
        
        propertyMap.setCompoundArray(NOV4_ECR_AFFECTED_PARTS, 
                affectedPartPropMaps.toArray(new IPropertyMap[affectedPartPropMaps.size()]));
    }

    private String getObjectType(TCComponent tcComponent)
    {
        String objectType = "";
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        
        try
        {
            String itemId = tcComponent.getProperty("nov4_item_id");
            String itemRevId = tcComponent.getProperty("nov4_item_rev_id");
            TCComponentItemRevisionType itemRevComponent = (TCComponentItemRevisionType) session
                    .getTypeComponent("ItemRevision");
            TCComponentItemRevision[] itemRevisions = itemRevComponent
                    .findRevisions(itemId, itemRevId);
            
            if(itemRevisions != null && itemRevisions.length > 0)
            {
                objectType = itemRevisions[0].getProperty("object_type");
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return objectType;
    }
    
}
