package com.nov.rac.form.delegate;

import java.util.ArrayList;
import java.util.List;

import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;


public class NOVEPRFormViewerLoadDelegate extends NOVFormCommonViewerLoadDelegate
{
    public NOVEPRFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }

    @Override
    protected void addTargetsDispositionData(IPropertyMap iPropertyMap)
    {
        // TODO Auto-generated method stub
        //super.addTargetsDispositionData(iPropertyMap);
        List<IPropertyMap> targetsDispositions = new ArrayList<IPropertyMap>();
        
        WorkflowProcess workflow = WorkflowProcess.getWorkflow();
        TCComponent[] dispositionArray = workflow.getTargetDispositions();
        
        String[] dispositionPropNames = getRegistry().getStringArray(
                "targetdisposition.PROPERTIES_AS_ATTRIBUTE");
        
        for (TCComponent disposition : dispositionArray)
        {
            IPropertyMap propMap = new SimplePropertyMap();
            propMap.setComponent(disposition);
            
            try 
            {
                PropertyMapHelper.componentToMap(disposition,
                        dispositionPropNames, propMap);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            targetsDispositions.add(propMap);
        }
        
        String dispPropName = getRegistry().getString("targetDisposition.PROP");
        iPropertyMap.setCompoundArray(dispPropName, targetsDispositions
                .toArray(new IPropertyMap[targetsDispositions.size()]));
    }

    @Override
    protected boolean isValidDisposition(TCComponent disposition)
    {
        // TODO Auto-generated method stub
        return true;
    }
    
    
}

