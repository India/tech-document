package com.nov.rac.form.delegate;

import java.util.ArrayList;
import java.util.List;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class NOVMBCFormViewerLoadDelegate extends NOVFormViewerLoadDelegate
{
    private static final String FORM = "Form";
    private static final String AFFECTED_ASSEMBLIES = "affectedAssemblies";
    private static final String COMP_REPL_PARTS = "compReplParts";
    
    public NOVMBCFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void loadComponent(Object inputObject, String context) throws TCException
    {
        String theGroup = context;
        IPropertyMap iPropertyMap = (IPropertyMap) inputObject;
        
        // 1.0 Load the panels which shows only Form related data
        StringBuilder objectType = new StringBuilder(getComponentType());
        objectType.append(".").append(FORM);
        
        loadPanelsData(theGroup, objectType.toString(), getInputProviders(), iPropertyMap);
        
        // 2.0 Add Affected Assemblies data, Component part to replace & Replacement part related data
        addAffectedAssembliesData(iPropertyMap);
        addAssembliesItemRevisionData(iPropertyMap);
        addComponentPartToReplaceData(iPropertyMap);
        addReplacementPartData(iPropertyMap);
        
        // 3.0 Load the panels which shows affected assemblies
        StringBuilder affectedAsmObjectType = new StringBuilder(getComponentType());
        affectedAsmObjectType.append(".").append(AFFECTED_ASSEMBLIES);
        
        loadPanelsData(theGroup, affectedAsmObjectType.toString(), getInputProviders(), iPropertyMap);
        
        // 4.0 Load the panels which shows component parts and replacement parts
        StringBuilder CompReplPartsObjectType = new StringBuilder(getComponentType());
        CompReplPartsObjectType.append(".").append(COMP_REPL_PARTS);
        
        loadPanelsData(theGroup, CompReplPartsObjectType.toString(), getInputProviders(), iPropertyMap);
        
    }
    
    private void addAffectedAssembliesData(IPropertyMap iPropertyMap) throws TCException
    {
        List<IPropertyMap> targetsAffectedAssemblies = new ArrayList<IPropertyMap>();
        
        WorkflowProcess workflow = WorkflowProcess.getWorkflow();
        TCComponent[] targetArray = workflow.getTargetDispositions();
        
        String[] targetPropNames = getRegistry().getStringArray("affectedAssemblies.PROPERTIES_AS_ATTRIBUTE");
        
        for (TCComponent disposition : targetArray)
        {
            IPropertyMap propMap = new SimplePropertyMap();
            propMap.setComponent(disposition);
            
            PropertyMapHelper.componentToMap(disposition, targetPropNames, propMap);
            targetsAffectedAssemblies.add(propMap);
        }
        
        String dispPropName = getRegistry().getString("affectedAssemblies.PROP");
        iPropertyMap.setCompoundArray(dispPropName,
                targetsAffectedAssemblies.toArray(new IPropertyMap[targetsAffectedAssemblies.size()]));
        
    }
    
    private void addAssembliesItemRevisionData(IPropertyMap iPropertyMap)
    {
        List<IPropertyMap> targetsItemRevs = new ArrayList<IPropertyMap>();
        
        WorkflowProcess workflow = WorkflowProcess.getWorkflow();
        List<TCComponent> itemRevs = workflow.getTargetItemRevisions();
        
        String[] itemRevPropNames = getRegistry().getStringArray("ItemRevision.PROPERTIES_AS_ATTRIBUTE");
        
        for (TCComponent itemRevision : itemRevs)
        {
            IPropertyMap propMap = new SimplePropertyMap();
            propMap.setComponent(itemRevision);
            
            try
            {
                PropertyMapHelper.componentToMap(itemRevision, itemRevPropNames, propMap);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            targetsItemRevs.add(propMap);
        }
        
        iPropertyMap.setCompoundArray("AffectedAssembliesItemRevision",
                targetsItemRevs.toArray(new IPropertyMap[targetsItemRevs.size()]));
    }
    
    private void addComponentPartToReplaceData(IPropertyMap iPropertyMap) throws TCException
    {
        TCComponent formComp = FormHelper.getTargetComponent();
        
        TCProperty compToReplProperty = formComp.getTCProperty(getRegistry().getString("CompPartToRepl.PROP"));
        TCComponent componentToRepl = compToReplProperty.getReferenceValue();
        
        String[] dispositionPropNames = getRegistry().getStringArray("componentToReplace.PROPERTIES_AS_ATTRIBUTE");
        IPropertyMap propMap = new SimplePropertyMap();
        if (componentToRepl != null)
        {
            propMap.setComponent(componentToRepl);
            PropertyMapHelper.componentToMap(componentToRepl, dispositionPropNames, propMap);
        }
        
        String dispPropName = getRegistry().getString("CompPartToRepl.PROP");
        iPropertyMap.setCompound(dispPropName, propMap);
        
    }
    
    private void addReplacementPartData(IPropertyMap iPropertyMap) throws TCException
    {
        TCComponent formComp = FormHelper.getTargetComponent();
        
        TCProperty replacementPartProperty = formComp.getTCProperty(getRegistry().getString("ReplacementPart.PROP"));
        TCComponent replacementComponent = replacementPartProperty.getReferenceValue();
        
        String[] dispositionPropNames = getRegistry().getStringArray("replacementPart.PROPERTIES_AS_ATTRIBUTE");
        IPropertyMap propMap = new SimplePropertyMap();
        if (replacementComponent != null)
        {
            propMap.setComponent(replacementComponent);
            
            PropertyMapHelper.componentToMap(replacementComponent, dispositionPropNames, propMap);
        }
        String dispPropName = getRegistry().getString("ReplacementPart.PROP");
        iPropertyMap.setCompound(dispPropName, propMap);
    }
    
}
