package com.nov.rac.form.delegate;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class NOVECRFormViewerDelegate extends NOVFormViewerDelegate {

	public NOVECRFormViewerDelegate(TCComponent formComponent, Registry registry) 
	{
		super(formComponent, registry);
	}

}
