package com.nov.rac.form.delegate;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class NOVSupersedeFormViewerDelegate extends NOVFormDispositionViewerDelegate 
{
	public NOVSupersedeFormViewerDelegate(TCComponent formComponent,
			Registry registry) {
		super(formComponent, registry);
		
	}

}
