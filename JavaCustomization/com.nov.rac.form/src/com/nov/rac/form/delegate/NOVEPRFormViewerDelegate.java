/**
 * 
 */
package com.nov.rac.form.delegate;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab ;
 */
public class NOVEPRFormViewerDelegate extends NOVFormViewerDelegate
{
    
    public NOVEPRFormViewerDelegate(TCComponent formComponent, Registry registry)
    {
        super(formComponent, registry);
    }
    
}
