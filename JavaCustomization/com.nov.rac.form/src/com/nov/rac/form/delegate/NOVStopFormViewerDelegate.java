package com.nov.rac.form.delegate;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class NOVStopFormViewerDelegate extends NOVFormViewerDelegate{

	private static final Object PROPERTY_VALIDATOR = "PROPERTY_VALIDATOR";

    public NOVStopFormViewerDelegate(TCComponent formComponent,
			Registry registry) {
		super(formComponent, registry);
		
	}
	
	 @Override
	protected IPropertyValidator getValidator()
    {
        
        IPropertyValidator formValidator = null;
        try
        {
            TCComponentForm selectedForm = (TCComponentForm) FormHelper.getTargetComponent();
            String change = selectedForm
                    .getProperty(GlobalConstant.STOPFORM_CHANGE);
            
            StringBuilder context = new StringBuilder();
            context.append(getComponentType()).append(".").append(change).append(".")
                    .append(PROPERTY_VALIDATOR);
            
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String[] propertyValidator = RegistryUtils.getConfiguration(
                    theGroup, context.toString(), getRegistry());
            
            Object[] objects = new Object[2];
            objects[0] = getComponent();
            objects[1] = getRegistry();
            formValidator = (IPropertyValidator) Instancer.newInstanceEx(
                    propertyValidator[0], objects);
            
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }
        
        return formValidator;
    }
}
