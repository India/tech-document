package com.nov.rac.form.delegate;

import java.util.Vector;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.operations.AbstractFormOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class NOVFormViewerDelegate extends AbstractFormOperationDelegate
{
    
    private TCComponent m_formObject = null;
    protected Vector<CreateInObjectHelper> m_createInObjectHelper = new Vector<CreateInObjectHelper>();
    protected final String TYPE = "Form";
    private Registry m_registry = null;
    private String m_formType = null;
    private final String PROPERTY_VALIDATOR = "PROPERTY_VALIDATOR";
    
    public NOVFormViewerDelegate(TCComponent formComponent, Registry registry)
    {
        m_formObject = formComponent;
        m_registry = registry;
    }
    
    @Override
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected TCComponent getComponent()
    {
        return m_formObject;
    }
    
    protected String getComponentType()
    {
        return m_formType;
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        
    	IPropertyMap formPropertyMap = loadFormProperties();
        
        validateFormProperties(new IPropertyMap[] {formPropertyMap});
        
        return true;
    }

	protected IPropertyMap loadFormProperties() {
		String theGroup = FormHelper.getTargetComponentOwningGroup();
        
        m_formType = getComponent().getType();
        
        CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
                getComponentType(), CreateInObjectHelper.OPERATION_UPDATE);
        fillPanelsData(theGroup,getComponentType(), TYPE, getOperationInputProviders(),
                formPropertyMap);
        formPropertyMap.setTargetObject(getComponent());
        m_createInObjectHelper.add(formPropertyMap);
		return formPropertyMap;
	}
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) m_createInObjectHelper
                .toArray(new CreateInObjectHelper[0]);
        CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
        
        CreateObjectsSOAHelper.throwErrors(createInObjectHelper);
        
        return true;
    }
    
    protected IPropertyValidator getValidator()
    {
        IPropertyValidator formValidator = null;
        StringBuilder context = new StringBuilder();
        context.append(getComponentType()).append(".")
                .append(PROPERTY_VALIDATOR);
        
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        String[] propertyValidator = RegistryUtils.getConfiguration(theGroup,context.toString(), getRegistry());
        
        try
        {
            Object[] objects = new Object[2];
            objects[0] = getComponent();
            objects[1] = getRegistry();
            formValidator = (IPropertyValidator) Instancer.newInstanceEx(propertyValidator[0],objects);
            /*formValidator = (IPropertyValidator) getRegistry().newInstanceFor(
                    context.toString(), objects);*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return formValidator;
    }
    
    protected void validateFormProperties(IPropertyMap[] formPropertyMap) throws TCException
    {
        //IPropertyMap[] arrayOfIPropertyMap = {formPropertyMap};
        IPropertyValidator partPropertiesValidator = getValidator();
        partPropertiesValidator.validateProperties(formPropertyMap);
    }
   
    
}
