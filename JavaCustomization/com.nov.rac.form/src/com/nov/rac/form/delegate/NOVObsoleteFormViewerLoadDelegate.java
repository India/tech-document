package com.nov.rac.form.delegate;

import java.util.ArrayList;
import java.util.List;

import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class NOVObsoleteFormViewerLoadDelegate extends NOVFormViewerLoadDelegate
{
    private static final String FORM = "Form";
    private static final String TARGET_DISPOSITION = "targetdisposition";
    
    public NOVObsoleteFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    @Override
    public void loadComponent(Object inputObject, String context)
            throws TCException
    {
        String theGroup = context;
        IPropertyMap iPropertyMap = (IPropertyMap) inputObject;
        // 1.0 Load the panels which shows only Form related data
        StringBuilder objectType = new StringBuilder(getComponentType());
        objectType.append(".").append(FORM);
        
        loadPanelsData(theGroup, objectType.toString(), getInputProviders(),
                iPropertyMap);
        
        // 2.0 Add target item revision & disposition related data
        addTargetsItemRevisionData(iPropertyMap);
        addTargetsDispositionData(iPropertyMap);
        
        // 3.0 Load the panels which shows disposition data
        StringBuilder dispositionObjectType = new StringBuilder(
                getComponentType());
        dispositionObjectType.append(".").append(TARGET_DISPOSITION);
        
        loadPanelsData(theGroup, dispositionObjectType.toString(),
                getInputProviders(), iPropertyMap);
        
    }
    
    private void addTargetsDispositionData(IPropertyMap iPropertyMap)
    {
        List<IPropertyMap> targetsDispositions = new ArrayList<IPropertyMap>();
        
        WorkflowProcess workflow = WorkflowProcess.getWorkflow();
        TCComponent[] dispositionArray = workflow.getTargetDispositions();
        
        String[] dispositionPropNames = getRegistry().getStringArray(
                "targetdisposition.PROPERTIES_AS_ATTRIBUTE");
        
        for (TCComponent disposition : dispositionArray)
        {
           // if( isValidDisposition(disposition) )
            //{
                IPropertyMap propMap = new SimplePropertyMap();
                propMap.setComponent(disposition);
                
                try
                {
                    PropertyMapHelper.componentToMap(disposition,
                            dispositionPropNames, propMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
                targetsDispositions.add(propMap);
            //}
        }
        
        String dispPropName = getRegistry().getString("targetDisposition.PROP");
        iPropertyMap.setCompoundArray(dispPropName, targetsDispositions
                .toArray(new IPropertyMap[targetsDispositions.size()]));
        
    }
    
    private boolean isValidDisposition(TCComponent disposition)
    {
        boolean isValidDispo = false;
        try
        {
            String itemID = disposition.getProperty("targetitemid");
            TCComponentItemRevision itemRev = (TCComponentItemRevision) WorkflowProcess.getWorkflow().
                    getStringToComponentMap().get(itemID);
            
            String itemRevType = itemRev.getProperty("object_type");
            if( !itemRevType.equalsIgnoreCase( getRegistry().getString("DocumentRevision.TYPE")) )
            {
                if( !isFirstRev(itemRev) );
                {
                    isValidDispo = true;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isValidDispo;
    }

    private void addTargetsItemRevisionData(IPropertyMap iPropertyMap)
    {
        List<IPropertyMap> targetsItemRevs = new ArrayList<IPropertyMap>();
        
        WorkflowProcess workflow = WorkflowProcess.getWorkflow();
        List<TCComponent> itemRevs = workflow.getTargetItemRevisions();
        
        String[] itemRevPropNames = getRegistry().getStringArray(
                "ItemRevision.PROPERTIES_AS_ATTRIBUTE");
        
        for (TCComponent itemRevision : itemRevs)
        {
            IPropertyMap propMap = new SimplePropertyMap();
            propMap.setComponent(itemRevision);
            
            try
            {
                PropertyMapHelper.componentToMap(itemRevision,
                        itemRevPropNames, propMap);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            targetsItemRevs.add(propMap);
        }
        
        iPropertyMap.setCompoundArray("targetsItemRevision", targetsItemRevs
                .toArray(new IPropertyMap[targetsItemRevs.size()]));
    }
    
    private boolean isFirstRev(TCComponent inItemRev)
    {
        boolean isFirstRev = true;
        
        try
        {
            TCComponentItem item = (TCComponentItem) inItemRev.getRelatedComponent("items_tag");
            TCComponent[] itemRev = (TCComponent[]) item .getRelatedComponents("revision_list");
            
            int iNewRev = 0;
            for (int iRev=0; iRev<itemRev.length; iRev++)
            {
                String sRevId = ((TCComponentItemRevision)itemRev[iRev]).getProperty("item_revision_id");
                
                int ind = sRevId.indexOf("PRE-RELEASE");
                if(ind < 0)
                {
                    iNewRev += 1;
                }
            }
            if (iNewRev > 1) 
            {
                isFirstRev = false;
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isFirstRev;
    }

}
