package com.nov.rac.form.delegate;

import java.util.List;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.loaddelegate.AbstractFormLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class NOVFormViewerLoadDelegate extends AbstractFormLoadDelegate
{
    private TCComponent m_formObject = null;
    private static final String FORM = "Form";
    
    public NOVFormViewerLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
        setComponent(FormHelper.getTargetComponent());
    }
    
    protected TCComponent getComponent()
    {
        return m_formObject;
    }
    
    protected String getComponentType()
    {
        return getComponent().getType();
    }
    
    @Override
    public void loadComponent(Object inputObject, String context)
            throws TCException
    {
        String theGroup = context;
        IPropertyMap iPropertyMap = (IPropertyMap) inputObject;
        StringBuilder objectType = new StringBuilder(getComponentType());
        objectType.append(".").append(FORM);
        
        loadPanelsData(theGroup, objectType.toString(), getInputProviders(),
                iPropertyMap);
    }
    
    @Override
    public void setComponent(TCComponent tcComponent)
    {
        m_formObject = tcComponent;
    }
    
}
