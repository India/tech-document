package com.nov.rac.form.viewer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.ISaveablePart;

import com.nov.rac.form.common.FormComposite;
import com.nov.rac.form.contentprovider.ContentProvider;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponentEventListener;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.tcviewer.TCComponentViewerInput;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.AdapterUtil;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.event.ClientEventDispatcher;
import com.teamcenter.rac.util.event.IClientEvent;
import com.teamcenter.rac.util.viewer.IViewerEvent;
import com.teamcenter.rac.util.viewer.ViewerEvent;
import com.teamcenter.rac.viewer.utils.CheckInOutComposite;
import com.teamcenter.rac.viewer.view.AbstractSwtSubViewer;

public class NOVFormViewer extends AbstractSwtSubViewer implements
        ISaveablePart, InterfaceAIFComponentEventListener
{
    
    private TCComponent m_tcComponent = null;
    private Registry m_registry = null;
    private Composite m_parent = null;
    private FormComposite m_formComposite = null;
    private IPropertyMap m_propertyMap = null;
    private final String INFORMATION = "Information";
    
    public NOVFormViewer()
    {
        
    }
    
    public NOVFormViewer(Composite parent)
    {
        m_parent = parent;
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    public void createUI()
    {
        try
        {
            m_parent.setLayout(new GridLayout(1, false));
            
            m_formComposite = new FormComposite(m_parent, SWT.NONE);
            m_formComposite.setRegistry(getRegistry());
            m_formComposite.createUI();
            
            CheckInOutComposite m_cicoComposite = new CheckInOutComposite(
                    m_parent);
            m_cicoComposite.panelLoaded();
            AIFUtility.getDefaultSession().addAIFComponentEventListener(this);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
    }
    
    @Override
    public void setInput(Object viewerInput)
    {
        TCComponentViewerInput input = (TCComponentViewerInput) AdapterUtil
                .getAdapter(viewerInput, TCComponentViewerInput.class);
        m_tcComponent = (TCComponent) input.getViewableObj();
        super.setInput(m_tcComponent);
    }
    
    @Override
    public void inputChanged(Object input, Object oldInput)
    {
        ContentProvider cp = (ContentProvider) getContentProvider();
        
        m_propertyMap = cp.getProperties();
        
        m_formComposite.load(m_propertyMap);
        
        clearDataModel();
        
        ViewerEvent viewerEvent = new ViewerEvent(this, IViewerEvent.RELOADVIEW);
        viewerEvent.queueEvent();
    }
    
    @Override
    public Control getControl()
    {
        return m_formComposite;
    }
    
    @Override
    public void refresh()
    {
        if (m_formComposite == null || m_formComposite.isDisposed())
        {
            return;
        }
        
        m_formComposite.layout();
        setFormState();
    }
    
    private void setFormState()
    {
        if (m_tcComponent.isCheckedOut())
        {
            m_formComposite.setEnabled(true);
        }
        else
        {
            m_formComposite.setEnabled(false);
        }
    }
    
    @Override
    public void doSave(final IProgressMonitor monitor)
    {
        Exception exception = null;
        
        try
        {
            m_formComposite.save();
            clearDataModel();
        }
        catch (Exception e)
        {
            exception = e;
            MessageBox.post(null, e, INFORMATION, MessageBox.INFORMATION);
            // e.printStackTrace();
        }
        finally
        {
            ClientEventDispatcher.fireEventLater(NOVFormViewer.this,
                    IClientEvent.SS_VIEWER_SAVE_COMPLETE, TCComponent.class,
                    m_tcComponent, Exception.class, exception);
        }
        
        System.out.println("doSave");
    }
    
    @Override
    public void doSaveAs()
    {
        
    }
    
    @Override
    public boolean isDirty()
    {
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        DataBindingModel dataModel = (DataBindingModel) dataBindingRegistry
                .getDataModel(FormHelper.getDataModelName());
        setFormState();
        return dataModel.isDirty();
    }
    
    @Override
    public boolean isSaveAsAllowed()
    {
        return false;
    }
    
    @Override
    public boolean isSaveOnCloseNeeded()
    {
        return true;
    }
    
    private void clearDataModel()
    {
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        DataBindingModel dataModel = (DataBindingModel) dataBindingRegistry
                .getDataModel(FormHelper.getDataModelName());
        dataModel.clear();
    }
}
