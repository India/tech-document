package com.nov.rac.form.viewer;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.ISaveablePart;

import com.nov.rac.form.common.FormComposite;
import com.nov.rac.form.common.FormTaskPanel;
import com.nov.rac.form.contentprovider.ContentProvider;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponentEventListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.viewer.IViewerEvent;
import com.teamcenter.rac.util.viewer.ViewerEvent;
import com.teamcenter.rac.viewer.view.AbstractSwtSubViewer;
import com.teamcenter.rac.workflow.processviewer.ProcessViewerApplicationPanel;

public class NOVProcessViewer extends AbstractSwtSubViewer implements
        ISaveablePart, InterfaceAIFComponentEventListener
{
    private Composite m_parent = null;
    private Registry m_registry = null;
    private TCComponent m_selectedComponent = null;
    private ScrolledComposite m_scrollComposite = null;
    private Composite m_topComposite = null;
    private Composite m_bottomComposite = null;
    private Composite m_mainComposite = null;
    private FormComposite m_formComposite = null;
    private IPropertyMap m_propertyMap = null;
    
    public NOVProcessViewer(Composite parent)
    {
        m_parent = parent;
        m_parent.setLayout(new GridLayout(1, false));
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    private Composite getParent()
    {
        return m_parent;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    public void setSelectedComponent(TCComponent tcComponent)
    {
        m_selectedComponent = tcComponent;
    }
    
    private TCComponent getSelectedComponent()
    {
        return m_selectedComponent;
    }
    
    public void createUI()
    {
        createTopComposite(getParent());
        createMainComposite(getParent());
        
        createScrollComposite(getParent());
        createBottomComposite(getParent());
        
        createRadioButtonComposite(m_topComposite);
        createForm(m_scrollComposite);
        createTaskCompleteComposite(m_bottomComposite);
    }
    
    private void createMainComposite(Composite parent)
    {
        m_mainComposite = new Composite(parent, SWT.NONE);
        m_mainComposite.setLayout(new GridLayout(1, false));
        m_mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
    }
    
    private void createBottomComposite(Composite parent)
    {
        m_bottomComposite = new Composite(parent, SWT.NONE);
        m_bottomComposite.setLayout(new GridLayout(1, false));
        m_bottomComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER,
                true, false, 1, 1));
    }
    
    private void createTopComposite(Composite parent)
    {
        m_topComposite = new Composite(parent, SWT.NONE);
        m_topComposite.setLayout(new GridLayout(1, false));
        m_topComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
    }
    
    private void createScrollComposite(Composite parent)
    {
        m_scrollComposite = new ScrolledComposite(parent, SWT.H_SCROLL
                | SWT.V_SCROLL);
        m_scrollComposite.setLayout(new GridLayout(1, false));
        m_scrollComposite.setLayoutData(new GridData(SWT.CENTER, SWT.FILL,
                true, true, 1, 1));
    }
    
    private void createForm(Composite parent)
    {
        m_formComposite = new FormComposite(parent, SWT.NONE);
        m_formComposite.setRegistry(getRegistry());
        m_formComposite.createUI();
        
        m_scrollComposite.setContent(m_formComposite);
        m_scrollComposite.setExpandHorizontal(true);
        m_scrollComposite.setExpandVertical(true);
        m_scrollComposite.setMinSize(m_formComposite.computeSize(SWT.DEFAULT,
                SWT.DEFAULT));
    }
    
    private void createRadioButtonComposite(Composite parent)
    {
        final Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(3, false));
        composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
                false, 1, 1));
        Button btnTaskView = new Button(composite, SWT.RADIO);
        btnTaskView.setText("Task View");
        btnTaskView.setSelection(true);
        
        new Label(composite, SWT.NONE);
        
        Button btnProcessView = new Button(composite, SWT.RADIO);
        btnProcessView.setText("Process View");
        addTaskViewButtonListener(btnTaskView);
        addProcessViewButtonListener(btnProcessView);
        
        createSeparator(parent);
    }
    
    private void createSeparator(Composite parent)
    {
        Composite composite = new Composite(parent,SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 3, 1));
        
        Label separator = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL |SWT.BOLD);
        GridData separatorGridData = new GridData(SWT.FILL, SWT.FILL, true,
                false, 1, 1);
        separator.setLayoutData(separatorGridData);
    }
    
    private void createTaskCompleteComposite(Composite parent)
    {
        try
        {
            final Composite composite = new Composite(parent, SWT.NONE);
            GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                    50);
            composite.setLayoutData(gridData);
            composite.setLayout(new GridLayout(1, true));
            final FormTaskPanel taskPanel = new FormTaskPanel(composite,
                    SWT.NONE);
            taskPanel.createUI();
            taskPanel.setSelectedComponent(getSelectedComponent());
            taskPanel.addButtonsListener(new SelectionListener()
            {
                
                @Override
                public void widgetSelected(SelectionEvent selectionevent)
                {
                    try
                    {
                        m_formComposite.save();
                        taskPanel.save();
                    }
                    catch (Exception e)
                    {
                        /*
                         * MessageBox.post(null, e, "INFORMATION",
                         * MessageBox.INFORMATION);
                         */
                    }
                }
                
                @Override
                public void widgetDefaultSelected(SelectionEvent selectionevent)
                {
                    
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void addTaskViewButtonListener(final Button btnTaskView)
    {
        btnTaskView.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                if (btnTaskView.getSelection())
                {
                    dispose();
                    createTaskView();
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }
    
    private void addProcessViewButtonListener(final Button btnProcessView)
    {
        btnProcessView.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                if (btnProcessView.getSelection())
                {
                    dispose();
                    createProcessView();
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }
    
    private void createProcessView()
    {
        if (m_mainComposite.isDisposed())
        {
            createMainComposite(getParent());
        }
        
        final Composite composite = new Composite(m_mainComposite, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, false));
        
        TCSession tcsession1 = ((TCComponent) getSelectedComponent())
                .getSession();
        final ProcessViewerApplicationPanel processViewPanel = new ProcessViewerApplicationPanel(
                tcsession1);
        processViewPanel.initializeDisplay();
        
        if (processViewPanel instanceof JPanel)
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                
                @Override
                public void run()
                {
                    ((JPanel) processViewPanel).setVisible(true);
                    ((JPanel) processViewPanel).setEnabled(true);
                    ((JPanel) processViewPanel).setMinimumSize(new Dimension(0,
                            0));
                }
            });
            
            getParent().getDisplay().asyncExec(new Runnable()
            {
                
                @Override
                public void run()
                {
                    SWTUIUtilities.embed(composite, processViewPanel, false);
                    
                    int xx = getParent().getSize().x - 1;
                    int yy = getParent().getSize().y;
                    getParent().setSize(xx, yy);
                }
            });
            
            processViewPanel.open((TCComponent) getSelectedComponent());
            
        }
    }
    
    private void createTaskView()
    {
        if (m_scrollComposite.isDisposed())
        {
            createScrollComposite(getParent());
        }
        
        if (m_bottomComposite.isDisposed())
        {
            createBottomComposite(getParent());
        }
        
        createForm(m_scrollComposite);
        createTaskCompleteComposite(m_bottomComposite);
        inputChanged(null, null);
        int xx = getParent().getSize().x + 1;
        int yy = getParent().getSize().y;
        getParent().setSize(xx, yy);
        
    }
    
    private void dispose()
    {
        Control[] controls = getParent().getChildren();
        
        for (Control control : controls)
        {
            if (!control.isDisposed() && control != m_topComposite)
            {
                control.dispose();
            }
        }
    }
    
    @Override
    public void doSave(IProgressMonitor arg0)
    {
        
    }
    
    @Override
    public void doSaveAs()
    {
        
    }
    
    @Override
    public boolean isDirty()
    {
        return false;
    }
    
    @Override
    public boolean isSaveAsAllowed()
    {
        return false;
    }
    
    @Override
    public boolean isSaveOnCloseNeeded()
    {
        return false;
    }
    
    @Override
    public Control getControl()
    {
        return m_scrollComposite;
    }
    
    @Override
    public void refresh()
    {
        
    }
    
    @Override
    public void inputChanged(Object input, Object oldInput)
    {
        ContentProvider cp = (ContentProvider) getContentProvider();
        
        m_propertyMap = cp.getProperties();
        
        m_formComposite.load(m_propertyMap);
        
        clearDataModel();
        
        ViewerEvent viewerEvent = new ViewerEvent(this, IViewerEvent.RELOADVIEW);
        viewerEvent.queueEvent();
    }
    
    private void clearDataModel()
    {
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        DataBindingModel dataModel = (DataBindingModel) dataBindingRegistry
                .getDataModel(FormHelper.getDataModelName());
        dataModel.clear();
    }
}
