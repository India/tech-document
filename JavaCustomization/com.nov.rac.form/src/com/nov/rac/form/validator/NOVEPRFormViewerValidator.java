/**
 * 
 */
package com.nov.rac.form.validator;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */

public class NOVEPRFormViewerValidator extends NOVFormViewerValidator
{

    public NOVEPRFormViewerValidator(TCComponent formComponent,
            Registry registry)
    {
        super(formComponent, registry);

    }
}
