package com.nov.rac.form.validator;

import java.util.ArrayList;
import java.util.List;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVSupersedeFormValidator extends NOVFormViewerValidator
{
    
    private Registry m_registry = null;
    private TCComponent m_formObject = null;
    
    public NOVSupersedeFormValidator(TCComponent formComponent, Registry registry)
    {
        super(formComponent, registry);
        m_registry = registry;
        m_formObject = formComponent;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected TCComponent getComponent()
    {
        return m_formObject;
    }
    
    protected String getComponentType()
    {
        return getComponent().getType();
    }
    
    @Override
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap) throws TCException
    {
        ExceptionStack stack = new ExceptionStack();
        List<IPropertyMap> formPropertyList = new ArrayList<IPropertyMap>();
        List<IPropertyMap> dispositionPropertyList = new ArrayList<IPropertyMap>();
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            if (propertyMap.getComponent().getType().equalsIgnoreCase("Nov4_SupersedeForm"))
            {
                formPropertyList.add(propertyMap);
            }
            else
            {
                dispositionPropertyList.add(propertyMap);
            }
        }
        
        validateFormData(formPropertyList.toArray(new IPropertyMap[formPropertyList.size()]), stack);
        validateDispositionData(dispositionPropertyList.toArray(new IPropertyMap[dispositionPropertyList.size()]),
                stack);
        
        // if we have errors, throw an exception.
        if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }
    
    private void validateDispositionData(IPropertyMap[] arrayOfPropertMap, ExceptionStack stack) throws TCException
    {
        String futureStatus = "dummy_value";
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String value = propertyMap.getString(getRegistry().getString("futureStatus.PROP"));
            if (value.equals(futureStatus))
            {
                TCException e = new TCException(getRegistry().getString("SelectOneAsSuperseded.MSG"));
                stack.addErrors(e);
            }
            futureStatus = value;
        }
    }
    
}
