package com.nov.rac.form.validator;

import  com.nov.rac.form.helpers.FormHelper;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVCancelStopFormViewerValidator extends NOVFormViewerValidator
{

    public NOVCancelStopFormViewerValidator(TCComponent formComponent,
            Registry registry)
    {
        super(formComponent, registry);
        // TODO Auto-generated constructor stub
    }
    
    
    @Override
    protected void validateFormData(IPropertyMap[] arrayPropertyMap,
            ExceptionStack stack) throws TCException
    {
        IPropertyMap propertyMap = arrayPropertyMap[0];
        String stopCommentVals = propertyMap.getString(GlobalConstant.NOV4_STOP_COMMENT_VALS);

        if (!FormHelper.hasContent(stopCommentVals))
        {
            addErrorMsgToStack(stack, GlobalConstant.NOV4_STOP_COMMENT_VALS);
        }
        
        if(FormHelper.hasContent(stopCommentVals) && !stopCommentVals.equals(NO_ISSUE_FOUND))
        {
            if (!FormHelper.hasContent(propertyMap.getString(GlobalConstant.STOPCOMMENT)))
            {
                addErrorMsgToStack(stack, GlobalConstant.STOPCOMMENT);
            }
        }
        
        
        String[] distribution = propertyMap.getStringArray(GlobalConstant.DISTRIBUTION);
        
        if (distribution == null || distribution.length < 1 )
        {
            addErrorMsgToStack(stack, GlobalConstant.DISTRIBUTION);
        }
    }

    
    private static final String NO_ISSUE_FOUND = "No Issue Found";
}
