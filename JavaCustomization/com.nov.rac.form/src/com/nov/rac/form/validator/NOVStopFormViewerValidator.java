package com.nov.rac.form.validator;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVStopFormViewerValidator extends NOVFormViewerValidator
{

    public NOVStopFormViewerValidator(TCComponent formComponent,
            Registry registry)
    {
        super(formComponent, registry);
    }
    
    
    @Override
    protected void validateFormData(IPropertyMap[] arrayPropertyMap,
            ExceptionStack stack) throws TCException
    {
        IPropertyMap propertyMap = arrayPropertyMap[0];
        if (!FormHelper.hasContent(propertyMap.getString(GlobalConstant.STOPREASON)))
        {
            addErrorMsgToStack(stack, GlobalConstant.STOPREASON);
        }
        
        if (propertyMap.getDate(GlobalConstant.NOV4_EXPECTED_RESOLUTION) == null)
        {
            addErrorMsgToStack(stack, GlobalConstant.NOV4_EXPECTED_RESOLUTION);
        }
        
        String[] distribution = propertyMap.getStringArray(GlobalConstant.DISTRIBUTION);
        
        if (distribution == null || distribution.length < 1 )
        {
            addErrorMsgToStack(stack, GlobalConstant.DISTRIBUTION);
        }
    }

}
