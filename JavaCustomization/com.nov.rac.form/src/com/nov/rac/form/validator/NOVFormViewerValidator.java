package com.nov.rac.form.validator;

import static com.nov.rac.form.helpers.FormHelper.hasContent;
import java.util.ArrayList;
import java.util.List;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVFormViewerValidator implements IPropertyValidator
{
    
    protected Registry m_registry = null;
    private TCComponent m_formObject = null;
    protected final String PROPERTIES_TO_VALIDATE = "PROPERTIES_TO_VALIDATE";
    protected final String MANDATORY = "MANDATORY";
    
    public NOVFormViewerValidator(TCComponent formComponent, Registry registry)
    {
        super();
        m_registry = registry;
        m_formObject = formComponent;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected TCComponent getComponent()
    {
        return m_formObject;
    }
    
    protected String getComponentType()
    {
        return getComponent().getType();
    }
    
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap)
            throws TCException
    {
    	 
    	ExceptionStack stack = new ExceptionStack();
    	List<IPropertyMap> formPropertyList = new ArrayList<IPropertyMap>();
    	List<IPropertyMap> dispositionPropertyList = new ArrayList<IPropertyMap>();
    	for(IPropertyMap propertyMap: arrayOfPropertMap)
    	{
    	    
    		if(propertyMap.getComponent().getType().equalsIgnoreCase(getComponent().getType()))
    		{
    			formPropertyList.add(propertyMap);
    		}
    		else
    		{
    			dispositionPropertyList.add(propertyMap);
    		}
    	}
    	
    	validateFormData(formPropertyList.toArray(new IPropertyMap[formPropertyList.size()]),stack);
    	validateDispositionData(dispositionPropertyList.toArray(new IPropertyMap[dispositionPropertyList.size()]),stack);
    	
    	if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }
    private void validateDispositionData(IPropertyMap[] arrayOfPropertMap,ExceptionStack stack) throws TCException
    {
    	//can add validations for disposition if required here .
      
    }
    protected void validateFormData(IPropertyMap[] arrayPropertyMap,ExceptionStack stack) throws TCException
    {
    	 IPropertyMap formPropertyMap=arrayPropertyMap[0];
    	 String theGroup = FormHelper.getTargetComponentOwningGroup();
    	 String[] m_formPropertyNames = RegistryUtils.getConfiguration(theGroup,getComponentType()
                 + "." + PROPERTIES_TO_VALIDATE, getRegistry());
         for (String property : m_formPropertyNames)
         {
             if (!hasContent(formPropertyMap.getString(property)))
             {
                 addErrorMsgToStack(stack, property);
             }
         }
       
    }
   
    protected void addErrorMsgToStack(ExceptionStack stack, String property)
    {
        StringBuilder context = new StringBuilder();
        context.append(getComponentType()).append(".").append(property)
                .append(".").append(MANDATORY);
        
        TCException e = new TCException(m_registry.getString(context
                .toString()));
        stack.addErrors(e);
    }
    
}
