package com.nov.rac.form.validator;

import static com.nov.rac.form.helpers.FormHelper.hasContent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.rac.form.delegate.NOVFormDispositionViewerDelegate;
import com.nov.rac.form.delegate.NOVFormViewerDelegate;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.IPropertyValidator;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.utils.AlternateIDHelper;
import com.nov.rac.utilities.utils.ExceptionHelper;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.nov.rac.utilities.utils.MessageBox;
import com.nov.rac.utilities.utils.MessageBoxHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVObsoleteFormValidator implements IPropertyValidator
{
    
    private Registry m_registry = null;
    private TCComponent m_formObject = null;
    private final String PROPERTIES_TO_VALIDATE = "PROPERTIES_TO_VALIDATE";
    private final String MANDATORY = "MANDATORY";
    
    public NOVObsoleteFormValidator(TCComponent formComponent, Registry registry)
    {
        super();
        m_registry = registry;
        m_formObject = formComponent;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected TCComponent getComponent()
    {
        return m_formObject;
    }
    
    protected String getComponentType()
    {
        return getComponent().getType();
    }
    
    public boolean validateProperties(IPropertyMap[] arrayOfPropertMap)
            throws TCException
    {
    	 
    	ExceptionStack stack = new ExceptionStack();
    	List<IPropertyMap> formPropertyList = new ArrayList<IPropertyMap>();
    	List<IPropertyMap> dispositionPropertyList = new ArrayList<IPropertyMap>();
    	for(IPropertyMap propertyMap: arrayOfPropertMap)
    	{
    		if(propertyMap.getComponent().getType().equalsIgnoreCase("Nov4_ObsoleteForm"))
    		{
    			formPropertyList.add(propertyMap);
    		}
    		else
    		{
    			dispositionPropertyList.add(propertyMap);
    		}
    	}
    	
    	validateReasonForChangeData(formPropertyList.toArray(new IPropertyMap[formPropertyList.size()]),stack);
    	validateDispositionData(dispositionPropertyList.toArray(new IPropertyMap[dispositionPropertyList.size()]),stack);
    	
    	if (stack.getErrorCount() > 0)
        {
            throw ExceptionHelper.formattedException(stack);
        }
        return true;
    }
    private void validateDispositionData(IPropertyMap[] arrayOfPropertMap,ExceptionStack stack) throws TCException
    {
    	//need to put check null check of map
    	boolean checkStatus = true;
        List<String> statuses = new ArrayList<String>();
        for (IPropertyMap propertyMap : arrayOfPropertMap)
        {
            String statusValue = propertyMap.getString(getRegistry().getString("futureStatus.PROP"));
           
            if(statusValue == null || statusValue.equals(" "))
			{
            	TCException e = new TCException(getRegistry().getString("StatusCanotbeEmpty.MSG"));
                stack.addErrors(e);
                checkStatus = false;
			}
            else
			{
				if(statusValue.equals("Obsolete") && statuses.contains(statusValue))
				{
					TCException e = new TCException(getRegistry().getString("OnlyOneTargetAsObsolete.MSG"));
	                stack.addErrors(e);
	                checkStatus = false;
				}
				else
				{
					statuses.add(statusValue);
				}
			}
        }
        if(!statuses.contains("Obsolete") && checkStatus == true)
		 {
        	TCException e = new TCException(getRegistry().getString("AtleastOneTargetAsObsolete.MSG"));
            stack.addErrors(e);
			checkStatus = false;
		 }
       
        validateOracleId(arrayOfPropertMap,stack);
    }
    private void validateReasonForChangeData(IPropertyMap[] arrayPropertyMap,ExceptionStack stack) throws TCException
    {
    	 IPropertyMap formPropertyMap=arrayPropertyMap[0];
    	 String theGroup = FormHelper.getTargetComponentOwningGroup();
    	 String[] m_formPropertyNames = RegistryUtils.getConfiguration(theGroup,getComponentType()
                 + "." + PROPERTIES_TO_VALIDATE, getRegistry());
         for (String property : m_formPropertyNames)
         {
             if (!hasContent(formPropertyMap.getString(property)))
             {
                 StringBuilder context = new StringBuilder();
                 context.append(getComponentType()).append(".").append(property)
                         .append(".").append(MANDATORY);
                 
                 TCException e = new TCException(m_registry.getString(context
                         .toString()));
                 stack.addErrors(e);
             }
         }
       
    }
    private void validateOracleId(IPropertyMap[] arrayOfPropertMap,ExceptionStack stack) throws TCException 
    {
    	WorkflowProcess currentProcess = WorkflowProcess.getWorkflow();
    	List<TCComponent> targetItems=currentProcess.getTargetItems();
    	
    	List<String> oracleIdsList = new ArrayList<String>();
    	List<String> replacementObjsIds=getReplaceObjectIds(arrayOfPropertMap);
    	
    	for (int inx = 0; inx < targetItems.size(); ++inx) 
		{
			String objectId = targetItems.get(inx).getProperty("item_id");

			if (replacementObjsIds.contains(objectId)) 
			{
				String[] alIds = AlternateIDHelper.getAlternateIds(targetItems.get(inx));
				if(null != alIds && alIds.length > 0)
				{
					oracleIdsList.addAll(Arrays.asList(alIds));
				}
				else
				{
					oracleIdsList.add("");
				}
			}
		}
    	if (oracleIdsList.contains("") ) 
		{
			String messageString = null;
			List<String> emptyList = new ArrayList<String>();
			emptyList.add("");

			List<String> noEmptyList = new ArrayList<String>();
			noEmptyList.addAll(oracleIdsList);
			noEmptyList.removeAll(emptyList);

			if (noEmptyList.isEmpty()) 
			{				
				messageString =getRegistry().getString("DispositionObjectsOracleID.MSG");
			} 
			else 
			{
				messageString =getRegistry().getString("DispositionAlterId.MSG");
			}
			
			checkObsoleteAltIdValidation(arrayOfPropertMap, messageString,stack);

		}
    	
    }
    private List<String> getReplaceObjectIds(IPropertyMap[] arrayOfPropertMap)
    {
    	List<String> replacementObjIds = new ArrayList<String>();
    	for(IPropertyMap propertyMap: arrayOfPropertMap)
    	{
    		String statusValue = propertyMap.getString(getRegistry().getString("futureStatus.PROP"));
    		if(statusValue.equalsIgnoreCase("Replacement"))
    		{
    			try {
					replacementObjIds.add(propertyMap.getComponent().getProperty("targetitemid"));
				} catch (TCException e) {
					
					e.printStackTrace();
				}
    		}
    	}
    	return replacementObjIds;
    }
    private List<String> getObsoleteObjectIds(IPropertyMap[] arrayOfPropertMap)
    {
    	List<String> obsoleteObjIds = new ArrayList<String>();
    	for(IPropertyMap propertyMap: arrayOfPropertMap)
    	{
    		String statusValue = propertyMap.getString(getRegistry().getString("futureStatus.PROP"));
    		if(statusValue.equalsIgnoreCase("Obsolete"))
    		{
    			try {
    				obsoleteObjIds.add(propertyMap.getComponent().getProperty("targetitemid"));
				} catch (TCException e) {
					
					e.printStackTrace();
				}
    		}
    	}
    	return obsoleteObjIds;
    }
    private void checkObsoleteAltIdValidation(IPropertyMap[] arrayOfPropertMap,String messageString,ExceptionStack stack) throws TCException 
    {
    	List<String> obsoleteObjIds=getObsoleteObjectIds(arrayOfPropertMap);
    	
    	WorkflowProcess currentProcess = WorkflowProcess.getWorkflow();
    	List<TCComponent> targetItems=currentProcess.getTargetItems();
    	
    	for (int inx = 0; inx < targetItems.size(); ++inx) 
		{
    		String objectId = targetItems.get(inx).getProperty("item_id");
    		if(obsoleteObjIds.contains(objectId))
    		{
    			String[] altIds = AlternateIDHelper.getAlternateIds(targetItems.get(inx));
    			if(null != altIds && altIds.length > 0)
				{
    				TCException e = new TCException(messageString);
    	            stack.addErrors(e);
				}
    		}
		}
    	
    }
    
}
