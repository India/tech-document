package com.nov.rac.form.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.Registry;

public class DistributionDualList extends DualList
{

    public DistributionDualList(Composite parent, Registry registry)
    {
        super(parent, registry);
    }
    
    protected void createUI(Composite composite)
    {
        Composite dualListComposite = new Composite(composite, SWT.NONE);
        GridData dualList_gridData = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1);
        dualListComposite.setLayoutData(dualList_gridData);
        dualListComposite.setLayout(new GridLayout(9, true));
        
        createDualListHeader(dualListComposite);
        
        createSourceList(dualListComposite);
        
        createDualListButtons(dualListComposite);
        
        createTargetList(dualListComposite);
    }
    
   /* protected void createDualListButtons(Composite dualListComposite)
    {
        //Composite buttonComposite = new Composite(dualListComposite, SWT.BORDER);
        //GridData btn_gridData = new GridData(SWT.FILL, SWT.FILL, true,
         //       true, 1, 1);
        //buttonComposite.setLayoutData(btn_gridData);
        //buttonComposite.setLayout(new GridLayout(1, true));
        
        m_plusMinusButtonComposite = new DualListButton(dualListComposite);
        
        Image plusButtonImage = new Image(dualListComposite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        
        m_plusMinusButtonComposite.getPlusButton().setImage(plusButtonImage);
        
        Image minusButtonImage = new Image(dualListComposite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        
        m_plusMinusButtonComposite.getMinusButton().setImage(minusButtonImage);
        
        m_plusMinusButtonComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
                true, 1, 1));
    }*/
    
    protected void createDualListHeader(Composite dualListComposite)
    {
        Label lblAvailable = new Label(dualListComposite, SWT.NONE);
        lblAvailable.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
                false, 4, 1));
        lblAvailable.setText(getRegistry().getString("Available.Label"));
        SWTUIHelper.setFont(lblAvailable, SWT.BOLD);
        
        new Label(dualListComposite, SWT.NONE);
        
        Label lblSelected = new Label(dualListComposite, SWT.NONE);
        lblSelected.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
                false, 4, 1));
        lblSelected.setText(getRegistry().getString("Selected.Label"));
        SWTUIHelper.setFont(lblSelected, SWT.BOLD);
    }
    
    protected void createTargetList(Composite dualListComposite)
    {
        m_targetList = new List(dualListComposite, SWT.BORDER | SWT.V_SCROLL);
        GridData l_gd_targetList = new GridData(SWT.FILL, SWT.FILL, true, true,
                4, 10);
        m_targetList.setLayoutData(l_gd_targetList);
        m_targetList
                .setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
    }
    
    protected void createSourceList(Composite dualListComposite)
    {
        m_sourceList = new List(dualListComposite, SWT.BORDER | SWT.V_SCROLL);
        GridData l_gd_sourceList = new GridData(SWT.FILL, SWT.FILL, true, true,
                4, 10);
        m_sourceList.setLayoutData(l_gd_sourceList);
        m_sourceList
                .setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
    }
}
