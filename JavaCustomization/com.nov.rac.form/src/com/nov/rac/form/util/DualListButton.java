package com.nov.rac.form.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class DualListButton extends Composite
{
    private int verticalSpan = 10;
    
    private Button m_plusButton = null;
    private Button m_minusButton = null;
    
    public DualListButton(Composite parent)
    {
        super(parent, SWT.NONE);
        // createUI(this);
    }
    
    public void createUI()
    {
        
        setLayout(new GridLayout(1, true));
        setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1,
                verticalSpan));
        
        createEmptyLabel(this);
        
        createAddButton(this);
        
        createRemoveButton(this);
        
        createEmptyLabel(this);
    }
    
    private void createEmptyLabel(Composite parent)
    {
        int labelVerticalSpan = (verticalSpan - 2) / 2;
        
        if (labelVerticalSpan > 0)
        {
            new Label(parent, SWT.NONE).setLayoutData(new GridData(SWT.CENTER,
                    SWT.TOP, true, true, 1, labelVerticalSpan));
        }
        
        /*
         * Image minusButtonImage = new Image(parent.getDisplay(), getRegistry()
         * .getImage("removeButton.IMAGE"), SWT.NONE);
         * m_minusButton.setImage(minusButtonImage);
         */
    }
    
    private void createRemoveButton(Composite parent)
    {
        m_minusButton = new Button(parent, SWT.NONE);
        m_minusButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
                false, 1, 1));
        
        /*
         * Image minusButtonImage = new Image(parent.getDisplay(), getRegistry()
         * .getImage("removeButton.IMAGE"), SWT.NONE);
         * m_minusButton.setImage(minusButtonImage);
         */
    }
    
    private void createAddButton(Composite parent)
    {
        m_plusButton = new Button(parent, SWT.NONE);
        m_plusButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
                false, 1, 1));
        
        /*
         * Image plusButtonImage = new Image(parent.getDisplay(), getRegistry()
         * .getImage("addButton.IMAGE"), SWT.NONE);
         * m_plusButton.setImage(plusButtonImage);
         */
    }
    
    /*
     * public Button getPlusButton() { return m_plusButton; } public Button
     * getMinusButton() { return m_minusButton; }
     */
    
    public void setEnabled(boolean flag)
    {
        m_plusButton.setEnabled(flag);
        m_minusButton.setEnabled(flag);
    }
    
    public void addSelectionListenerToPlusButton(SelectionListener listener)
    {
        m_plusButton.addSelectionListener(listener);
    }
    
    public void addSelectionListenerToMinusButton(SelectionListener listener)
    {
        m_minusButton.addSelectionListener(listener);
    }
    
    public void setPlusButtonImage(Image image)
    {
        m_plusButton.setImage(image);
    }
    
    public void setMinusButtonImage(Image image)
    {
        m_minusButton.setImage(image);
    }
    
    public void changeSpan(int horizontalSpan, int verticalSpan)
    {
        this.verticalSpan = verticalSpan;
    }
}
