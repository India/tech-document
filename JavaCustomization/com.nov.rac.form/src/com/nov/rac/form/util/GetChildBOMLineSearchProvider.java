package com.nov.rac.form.util;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.kernel.TCComponentItemRevision;

public class GetChildBOMLineSearchProvider implements INOVSearchProvider2
{
    private TCComponentItemRevision m_topBOMLineItemRev;

    public GetChildBOMLineSearchProvider(TCComponentItemRevision topBOMLineItemRev)
    {
        m_topBOMLineItemRev = topBOMLineItemRev;
    }
    
    @Override
    public String getBusinessObject()
    {
        return "";
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        QuickBindVariable[] bindVariables = new QuickBindVariable[4];
        
        // TOP BOMLine item revision
        bindVariables[0] = new QuickBindVariable();
        bindVariables[0].nVarType = POM_typed_reference;
        bindVariables[0].nVarSize = 1;
        bindVariables[0].strList = new String[] { getUid() };
        
        // PSBOMView type
        bindVariables[1] = new QuickBindVariable();
        bindVariables[1].nVarType = POM_string;
        bindVariables[1].nVarSize = 1;
        bindVariables[1].strList = new String[] { "view" };

        // NOTE Name 
        bindVariables[2] = new QuickBindVariable();
        bindVariables[2].nVarType = POM_string;
        bindVariables[2].nVarSize = 1;
        bindVariables[2].strList = new String[] {"Nov4RefDefComments"};
        
        // Alt ID
        bindVariables[3] = new QuickSearchService.QuickBindVariable();
        bindVariables[3].nVarType = POM_string;
        bindVariables[3].nVarSize = 2;
        bindVariables[3].strList = new String[] { "RSOne","JDE"};

        return bindVariables;
    }
    
    private String getUid()
    {
        String puid = null;
        if (m_topBOMLineItemRev != null)
        {
            puid = m_topBOMLineItemRev.getUid();
        }
        return puid;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        // req cols ::- Seq.,Part Number, Alternate ID,Name,Status,Qty.,UOM,Ref. Des. Comment
        
        // actual cols ::- puid of child Items, object_type, Seq., Part Number, Alternate ID, Name, Status, Qty., UOM, Ref. Des. Comment
        QuickHandlerInfo[] allHandlerInfo = new QuickHandlerInfo[6];
             
        allHandlerInfo[0] = new QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-single-level-child-BOMLine"; //item_id, object_name, object_type, seq_no, qty_value, symbol
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2};
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5};
        allHandlerInfo[0].listInsertAtIndex = new int[] { 3, 5, 1, 2, 7};
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo(); 
        allHandlerInfo[1].handlerName = "NOVSRCH-get-alternate-ids"; //altid_of, idfr_id, idcxt_name
        allHandlerInfo[1].listBindIndex = new int[] { 4 };
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 4 };

        allHandlerInfo[2] = new QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-release-status"; //puid, pseq(release_status_list), name
        allHandlerInfo[2].listBindIndex = new int[0];
        allHandlerInfo[2].listReqdColumnIndex = new int[] {2};
        allHandlerInfo[2].listInsertAtIndex = new int[] { 6 };
        
        allHandlerInfo[3] = new QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-unit-of-measure"; //puid, symobl, unit
        allHandlerInfo[3].listBindIndex = new int[0];
        allHandlerInfo[3].listReqdColumnIndex = new int[] {1};
        allHandlerInfo[3].listInsertAtIndex = new int[] { 8 };
        
        allHandlerInfo[4] = new QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-nov4refdefcomment"; //puid, pval
        allHandlerInfo[4].listBindIndex = new int[] { 1, 3};
        allHandlerInfo[4].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[4].listInsertAtIndex = new int[] { 9 };
        
        allHandlerInfo[5] = new QuickHandlerInfo();
        allHandlerInfo[5].handlerName = "NOVSRCH-get-nov4part-master-props"; //rsone_itemtype
        allHandlerInfo[5].listBindIndex = new int[0];
        allHandlerInfo[5].listReqdColumnIndex = new int[]{1};
        allHandlerInfo[5].listInsertAtIndex = new int[] { 10 };
        
        
        return allHandlerInfo;
    }
    
    @Override
    public boolean validateInput()
    {
        return true;
    }
    
}
