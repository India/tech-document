package com.nov.rac.form.util;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public interface IPropertyValidator
{
    public boolean validateProperties(IPropertyMap[] propertyMap) throws TCException;
}
