package com.nov.rac.form.util;

import java.util.Map;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

public interface INOVDisposition extends INOVFormLoadSave
{
    public void load(String partId,Map<String,IPropertyMap> map) throws TCException;
}
