package com.nov.rac.form.util;

import java.util.Arrays;
import java.util.List;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.teamcenter.rac.kernel.TCPreferenceService;

public class ENUtility 
{
	public static boolean isValidENGroupForMinorRevisionTable()
	{
		int theScope = TCPreferenceService.TC_preference_site;
        String prefrenceName = "NOV_en_add_minor_revision_table_groups";
        String[] strings = PreferenceUtils.getStringValues(theScope, prefrenceName);
        
        String currentGroupName = GroupUtils.getCurrentGroupName();
        //System.out.println("Current Group = " + currentGroupName);
        
        List<String> preferences = Arrays.asList(strings);
        //for (String preference : preferences)
        //{
        //    System.out.println(preference);
        //}
        boolean isValidENGroup = preferences.contains(currentGroupName);
        System.out.println("EN Group validation:  " + isValidENGroup);
        return isValidENGroup;
	}
}