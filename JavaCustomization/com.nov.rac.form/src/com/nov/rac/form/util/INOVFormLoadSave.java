package com.nov.rac.form.util;

import com.nov.rac.ui.ILoadSave;

public interface INOVFormLoadSave extends ILoadSave
{
    public void setEnabled(boolean flag);
}
