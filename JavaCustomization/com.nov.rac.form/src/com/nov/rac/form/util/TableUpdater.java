package com.nov.rac.form.util;

import javax.swing.JTable;

public interface TableUpdater
{
    public void update(JTable table, Object value, int row, int col);
    
    public boolean isCellEditable(int row, int col);

    public boolean isRowEditable(int row);
    

    
}
