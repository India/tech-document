package com.nov.rac.form.util;

import java.io.IOException;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.utils.NOVBrowser;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentSite;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCSiteInfo;

public class ENFormPrinter 
{
	private IPropertyMap m_formPropMap = null;
	private TCComponentTask m_rootTask = null;
	private boolean m_isMisGrp = false;
	
	public ENFormPrinter(IPropertyMap formPropMap)
	{
		this.m_formPropMap = formPropMap;
	}

	public void printForm() throws Exception
	{
		String baseCommandLine = "";

		String processUID = getProcessID();

		// 1.0 initialize command line
		baseCommandLine += initializeALCPrintCommand( processUID );

		// 2.0 append request name for specific form
		baseCommandLine += getRequestName();
		
		// 3.0 append group name and version
		baseCommandLine += getGroupName();
		
		// 4.0 append version
		baseCommandLine += getVersion();
		
		// 5.0 execute command
		//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + baseCommandLine );
		NOVBrowser.open(baseCommandLine);
	}

	private String getProcessID()
	{
		String processUID = "";
		
		WorkflowProcess workFlowObject = WorkflowProcess.getWorkflow();
		processUID = workFlowObject.getRootTask().getUid();
		
//		try 
//		{
//			TCComponentForm form = FormHelper.getTargetComponent();
//			AIFComponentContext[] whereReferenced = form.whereReferenced();
//
//			for(AIFComponentContext context : whereReferenced)
//			{
//				if((TCComponent)context.getComponent() instanceof TCComponentProcess)
//				{
//					TCComponentProcess currentProcess = (TCComponentProcess)context.getComponent();
//
//					m_rootTask =((TCComponentProcess)currentProcess).getRootTask();
//
//					processUID = m_rootTask.getUid();
//					break;
//				}
//			}
//		}
//		catch (TCException e) 
//		{
//			e.printStackTrace();
//		}

		return processUID;
	}

	private String initializeALCPrintCommand( String processUID) throws TCException 
	{
		String formName = m_formPropMap.getString("object_name");
		
		TCPreferenceService prefServ = getPreferenceService();
		String server = prefServ.getStringValue( "CETGALCServices_server" );
		String port = prefServ.getStringValue( "CETGALCServices_port" );
		String context = prefServ.getStringValue( "CETGALCServices_context" );
		
		TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
		TCComponentSite site = session.getCurrentSite();
		TCSiteInfo info = site.getSiteInfo();
		String database = info.getSiteName();
		
		String commandLine = "http://" + server + ":" + port + context + "enPrint?ecn="
		                     + formName + "&db=" + database + "&processpuid=" + processUID;
	
		return commandLine;
	}

	
	private String getRequestName() 
	{
		String reqName = "&requestname=EN";
		return reqName;
	}

	private String getGroupName() 
	{
		String owningGroup = (m_formPropMap.getTag( "owning_group" )).toString();
		
		TCPreferenceService prefService = getPreferenceService();
		String[] missionGroups = prefService.getStringValues("_NOV_Mission_item_creation_groups_");

		String groupName = "&group=RS";

		for( String misGrp : missionGroups)
		{
			if(owningGroup != null && owningGroup.equalsIgnoreCase( misGrp ))
			{
				groupName = "&group=Mission";
				m_isMisGrp = true;
				break;
			}
		}

		return groupName;
	}
	
	private String getVersion()
	{
		Integer enVersion = m_formPropMap.getInteger( "nov4_en_version" );
		
		String versionString = "&version=";
		if( enVersion != null && enVersion == 1 && m_isMisGrp == true )
		{
			versionString += "3.2_2";
		}
		else if ( enVersion != null && enVersion == 1 && m_isMisGrp == false )
		{
			versionString += "3.2";
		}
		else
		{
			versionString += "3.1";
		}
		
		return versionString;
	}
	
	protected TCPreferenceService getPreferenceService()
	{
		TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
		TCPreferenceService prefService = session.getPreferenceService();
		
		return prefService;
	}
	
}
