package com.nov.rac.form.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class WorkflowProcess
{
    private TCComponent m_form = null;
    private TCComponentProcess m_currentProcess = null;
    private TCComponentTask m_rootTask = null;
    private Registry m_registry = null;
    private List<TCComponent> m_targetItemRevisionsList = new ArrayList<TCComponent>();
    private List<TCComponent> m_targetItemsList = new ArrayList<TCComponent>();
    private HashMap<TCComponent, TCComponent> m_targetRevision_DispositionMap = new HashMap<TCComponent, TCComponent>();
    private HashMap<TCComponent, TCComponent> m_disposition_TargetRevisionMap = new HashMap<TCComponent, TCComponent>();
    private HashMap<String, TCComponent> m_partIdTCComponentMap = new HashMap<String, TCComponent>();
    private HashMap<String, TCComponent> m_partIdDispositiontMap = new HashMap<String, TCComponent>();
    private static WorkflowProcess m_workflow = null;
    
    public WorkflowProcess(TCComponent form, Registry registry)
    {
        m_form = form;
        m_registry = registry;
        initialize();
    }
    
    private void initialize()
    {
        setCurrentProcess();
        setRootTask();
        setTargetItemRevisions();
        setRevisionDispositionMapping();
        setTargetItems();
        //createDispositionData(m_targetItemRevisionsList);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    private void setCurrentProcess()
    {
        try
        {
            AIFComponentContext[] contexts = m_form.whereReferenced();
            for (int j = 0; j < contexts.length; j++)
            {
                if ((TCComponent) contexts[j].getComponent() instanceof TCComponentProcess)
                {
                    m_currentProcess = (TCComponentProcess) contexts[j]
                            .getComponent();
                    break;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void setRootTask()
    {
        try
        {
            if (m_currentProcess != null)
            {
                m_rootTask = m_currentProcess.getRootTask();
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public TCComponent[] getTargetDispositions()
    {
        TCComponent[] targetDispositions = null;
        try
        {
            TCProperty targetdispProperty = m_form.getTCProperty(getRegistry()
                    .getString("targetDisposition.PROP"));
            targetDispositions = targetdispProperty.getReferenceValueArray();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return targetDispositions;
    }
    
    public List<TCComponent> findTargetItemRevisions(
            TCComponent[] targetDispositions)
    {
        try
        {
            String[] requiredTCProperties = {
                    getRegistry().getString("targetDispositionItemId.PROP"),
                    getRegistry().getString("targetDispositionRevisionId.PROP") };
            
            List<TCComponent> targetDispositionsList = Arrays
                    .asList(targetDispositions);
            
            TCProperty[][] returedTCProperties = TCComponentType
                    .getTCPropertiesSet(targetDispositionsList,
                            requiredTCProperties);
            
            TCComponentItemRevisionType type = (TCComponentItemRevisionType) (FormHelper
                    .getTargetComponent().getSession().getTypeService()
                    .getTypeComponent(getRegistry().getString(
                            "itemRevision.PROP")));
            
            for (int index = 0; index < targetDispositions.length; ++index)
            {
                String targetId = (returedTCProperties[index][0]
                        .getStringValue());
                String revid = (returedTCProperties[index][1].getStringValue());
                TCComponent comp = type.findRevision(targetId, revid);
                if (comp != null)
                {
                    m_targetItemRevisionsList.add(comp);
                }
                    
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return m_targetItemRevisionsList;
    }
    
    private void setTargetItemRevisions()
    {
        try
        {
            TCComponent[] targetItemRevisions = null;
            TCComponent[] targetDispositions = getTargetDispositions();
            TCComponentProcess currentProcess = (TCComponentProcess) getCurrentProcess();
            
            if (currentProcess != null)
            {
                TCComponentTask rootTask = (TCComponentTask) getRootTask();
                targetItemRevisions = rootTask.getAttachments(
                        TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
                
                if (targetItemRevisions != null)
                {
                    for (int i = 0; i < targetItemRevisions.length; i++)
                    {
                        /*
                         * if (targetItemRevisions[i] instanceof
                         * TCComponentItemRevision) m_targetItemRevisionsList
                         * .add(targetItemRevisions[i]);
                         */
                        addTarget(targetItemRevisions[i]);
                        
                        /*
                         * if (targetItemRevisions[i] instanceof
                         * TCComponentItem) m_targetItemRevisionsList
                         * .add(targetItemRevisions[i]);
                         */
                        
                    }
                }
            }
            else
            {
                findTargetItemRevisions(targetDispositions);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void addTarget(TCComponent targetItem)
    {
        if (targetItem instanceof TCComponentItemRevision
                || targetItem instanceof TCComponentItem)
        {
            m_targetItemRevisionsList.add(targetItem);
        }
        
    }
    
    private TCComponent[] createDispositionData(
            List<TCComponent> m_targetItemRevisionsList)
    {
        TCComponent[] dispositionComps = null;
        NOVObjectPolicy itemPolicy = null;
        try
        {
            List<TCComponent> dispToCreate = dispositionToCreate();
            if (dispToCreate != null && dispToCreate.size() > 0)
            {
                NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
                        "Item");
                
                itemPolicy = novObjectPolicyFactory.createObjectPolicy("",
                        getRegistry());
                
                Vector<TCComponent> targetVector = new Vector<TCComponent>();
                for (TCComponent target : m_targetItemRevisionsList)
                {
                    if (target instanceof TCComponentItem)
                    {
                        targetVector.add(target);
                    }
                }
                
                TCComponent[] targetItems = dispToCreate
                        .toArray(new TCComponent[dispToCreate.size()]);
                List<TCComponent> createdDispositions = new ArrayList<TCComponent>();
                
                createdDispositions = createOrUpdateDispoistion(itemPolicy,
                        targetItems);
                
                TCProperty dispositionProperty = m_form
                        .getTCProperty(getRegistry().getString(
                                "targetDisposition.PROP"));
                TCComponent[] dispAttatched = dispositionProperty
                        .getReferenceValueArray();
                List<TCComponent> disAttatchedVector = new ArrayList<TCComponent>();
                disAttatchedVector.addAll(createdDispositions);
                if (dispAttatched != null && dispAttatched.length > 0)
                {
                    for (TCComponent obj : dispAttatched)
                    {
                        disAttatchedVector.add(obj);
                    }
                }
                dispositionComps = (TCComponent[]) disAttatchedVector
                        .toArray(new TCComponent[disAttatchedVector.size()]);
                
                dispositionProperty.setReferenceValueArray(dispositionComps);
                
            }
            
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }
        
        return dispositionComps;
        
    }

    private List<TCComponent> createOrUpdateDispoistion(NOVObjectPolicy itemPolicy,
            TCComponent[] targetItems)
            throws TCException
    {
        List<TCComponent> createdorUpdatedObjs = new ArrayList<TCComponent>();
        
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        
        NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(
                tcSession);
        
        String[] attrList = { GlobalConstant.RELEASE_STATUS_LIST };
        
        Vector<CreateInObjectHelper> vecCreateInObjectHelper = new Vector<CreateInObjectHelper>();
        
        nOVDataManagementServiceHelper.getObjectsProperties(targetItems,
                attrList, itemPolicy);
        
        for (int i = 0; i < targetItems.length; i++)
        {
            CreateInObjectHelper formPropertyMap = new CreateInObjectHelper(
                    GlobalConstant.TARGETDISPOSITION,
                    CreateInObjectHelper.OPERATION_CREATE);
            String item_id = targetItems[i].getProperty(GlobalConstant.ITEMID);
            String revision_id = ((TCComponentItem) targetItems[i])
                    .getLatestItemRevision().getProperty(
                            GlobalConstant.CURRENT_REV_ID);
            String statusName = targetItems[i]
                    .getProperty(GlobalConstant.RELEASE_STATUS_LIST);
            
            formPropertyMap.setString("targetitemid", item_id);
            formPropertyMap.setString("rev_id", revision_id);
            
            formPropertyMap.setString("currentstatus", statusName);
            
            vecCreateInObjectHelper.add(formPropertyMap);
            
        }
        
        if(vecCreateInObjectHelper !=null && vecCreateInObjectHelper.size()>0)
        {
            CreateInObjectHelper createInObjectHelper[] = (CreateInObjectHelper[]) vecCreateInObjectHelper
                    .toArray(new CreateInObjectHelper[vecCreateInObjectHelper
                            .size()]); 
            CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
            
            for (int j = 0; j < createInObjectHelper.length; j++)
            {
                TCComponent created = CreateObjectsSOAHelper
                        .getCreatedOrUpdatedRootObject(createInObjectHelper[j]);
                createdorUpdatedObjs.add(created);
            }
        }
        
        return createdorUpdatedObjs;
    }
    
    private List<TCComponent> dispositionToCreate()
    {
        List<TCComponent> createDispForTargets = new ArrayList<TCComponent>();
        TCProperty dispositionProperty;
        try
        {
            dispositionProperty = m_form.getTCProperty("stoptargetdisposition");
            TCComponent[] createdDisposition = dispositionProperty
                    .getReferenceValueArray();
            
            if (createdDisposition != null && createdDisposition.length > 0)
            {
                if (m_targetItemRevisionsList.size() != createdDisposition.length)
                {
                    for (TCComponent component : m_targetItemRevisionsList)
                    {
                        if (!checkDispForTarget(createdDisposition, component))
                        {
                            createDispForTargets.add(component);
                        }
                    }
                }
            }
            else
            {
                createDispForTargets.addAll(m_targetItemRevisionsList);
            }
          
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return createDispForTargets;
    }

    private boolean checkDispForTarget(TCComponent[] createdDisposition, TCComponent tcComponent)
    {
        boolean dispExistsForTarget = false;
        try
        {
            String itemId = tcComponent.getStringProperty(GlobalConstant.ITEMID);
            
            for(TCComponent disposition : createdDisposition )
            {
                String targetItemId = disposition.getStringProperty("targetitemid");
                if(targetItemId == itemId )
                {
                    dispExistsForTarget = true;
                    break;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return dispExistsForTarget;
    }

    public void setRevisionDispositionMapping()
    {
        String[] requiredTargetRevisionProperties = { getRegistry().getString(
                "itemId.PROP") };
        String[] requiredDispositionProperties = { getRegistry().getString(
                "targetDispositionItemId.PROP") };
        try
        {
            List<TCComponent> allTableItemsList = new ArrayList<TCComponent>();
            allTableItemsList.addAll(m_targetItemRevisionsList);
            // addRDDComponents(allTableItemsList);
            
            String[][] targetRevisionItemIdArray = TCComponentType
                    .getPropertiesSet(allTableItemsList,
                            requiredTargetRevisionProperties);
            
            List<TCComponent> dispositionsList = Arrays
                    .asList(getTargetDispositions());
            
            String[][] dispositionsItemIdArray = TCComponentType
                    .getPropertiesSet(dispositionsList,
                            requiredDispositionProperties);
            
            for (int targetIndex = 0; targetIndex < allTableItemsList.size(); ++targetIndex)
            {
                m_partIdTCComponentMap.put(
                        targetRevisionItemIdArray[targetIndex][0],
                        allTableItemsList.get(targetIndex));
                
                for (int refIndex = 0; refIndex < dispositionsList.size(); ++refIndex)
                {
                    if (dispositionsItemIdArray[refIndex][0]
                            .equalsIgnoreCase(targetRevisionItemIdArray[targetIndex][0]))
                    {
                        m_targetRevision_DispositionMap.put(
                                allTableItemsList.get(targetIndex),
                                dispositionsList.get(refIndex));
                        m_disposition_TargetRevisionMap.put(
                                dispositionsList.get(refIndex),
                                allTableItemsList.get(targetIndex));
                        
                        m_partIdDispositiontMap.put(
                                dispositionsItemIdArray[refIndex][0],
                                dispositionsList.get(refIndex));
                        
                        break;
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    private void setTargetItems()
    {
        try
        {
            NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
                    "ItemRevision");
            
            NOVObjectPolicy itemRevPolicy = novObjectPolicyFactory
                    .createObjectPolicy("", getRegistry());
            
            String[] propertyNames = RegistryUtils.getConfiguration(
                    "ItemRevision" + "." + "PROPERTY_NAMES", getRegistry());
            
            TCSession session = (TCSession) AIFUtility.getCurrentApplication()
                    .getSession();
            NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(
                    session);
            
            TCComponent[] itemRevArray = m_targetItemRevisionsList
                    .toArray(new TCComponent[m_targetItemRevisionsList.size()]);
            dmSOAHelper.getObjectsProperties(itemRevArray, propertyNames,
                    itemRevPolicy);
            
            for (TCComponent itemRev : m_targetItemRevisionsList)
            {
                addRequiredItemToList(itemRev, m_targetItemsList);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void addRequiredItemToList(TCComponent itemRev,
            List<TCComponent> targetItemsList)
    {
        TCComponent requiredObject = itemRev;
        
        if (requiredObject instanceof TCComponentItemRevision)
        {
            try
            {
                requiredObject = ((TCComponentItemRevision) (itemRev))
                        .getItem();
            }
            catch (TCException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        targetItemsList.add(requiredObject);
    }
    
    private void addRDDComponents(List<TCComponent> allTableItemsList)
    {
        String rddRelation = getRegistry().getString("rdd.TYPE");
        for (TCComponent targetItemRevision : m_targetItemRevisionsList)
        {
            try
            {
                TCComponent[] rdd = targetItemRevision
                        .getRelatedComponents(rddRelation);
                if (rdd != null && rdd.length > 0)
                {
                    allTableItemsList.add(rdd[0]);
                }
                
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    /*
     * public List<List<String[]>> getTargetItemRevisionProperties(String[]
     * targetItemRevisionProp,String[] dispositionProp,int noOfColumns) {
     * List<List<String[]>> rowObjects = new ArrayList<List<String[]>>(); try {
     * //List<String[]> row = new ArrayList<String[]>(); for(int i=0;i<2;i++) {
     * String[] parentData = new String[]{"100001","Test","01","Active",""};
     * String[] childData = new String[]{"100001","Doc","02","",""};
     * row.add(parentData); row.add(childData); rowObjects.add(row); } //
     * TCComponent[] rdd =
     * tcComponents.getRelatedComponents(getRegistry().getString("rdd.TYPE"));
     * List<TCComponent> targetItemRevisions = new ArrayList<TCComponent>();
     * targetItemRevisions.addAll(targetRevision_DispositionMap.keySet());
     * List<TCComponent> dispoComponents = new ArrayList<TCComponent>();
     * dispoComponents.addAll(targetRevision_DispositionMap.values());
     * TCProperty[][] returedTargetItemRevisionProperties = TCComponentType
     * .getTCPropertiesSet(targetItemRevisions, targetItemRevisionProp);
     * TCProperty[][] returedDispositionProperties = TCComponentType
     * .getTCPropertiesSet(dispoComponents, dispositionProp); for(int rowindex =
     * 0; rowindex < targetItemRevisions.size(); rowindex++ ) { List<String[]>
     * row = new ArrayList<String[]>(); List<String>parentDataList = new
     * ArrayList<String>(); List<String>childDataList = new ArrayList<String>();
     * for(int columnindex = 0; columnindex < noOfColumns; columnindex++) {
     * if(columnindex < targetItemRevisionProp.length) {
     * parentDataList.add(returedTargetItemRevisionProperties
     * [rowindex][columnindex].getStringValue()); } else {
     * parentDataList.add(returedDispositionProperties
     * [rowindex][columnindex].getStringValue()); } } String[] parentData = new
     * String[parentDataList.size()];
     * row.add(parentDataList.toArray(parentData)); row.add(new String[]{});
     * rowObjects.add(row); } } catch (TCException e) { e.printStackTrace(); }
     * return rowObjects; }
     */
    
    public List<List<String[]>> getTargetItemsData(
            String[] targetItemRevisionProp, String[] dispositionProp,
            int noOfColumns)
    {
        List<List<String[]>> rowObjects = new ArrayList<List<String[]>>();
        
        // List<TCComponent> revisionsList = new ArrayList<TCComponent>();
        List<TCComponent> revisionsList = new CopyOnWriteArrayList<TCComponent>();
        revisionsList.addAll(m_targetItemRevisionsList);
        
        String rddRelation = getRegistry().getString("rdd.TYPE");
        
        List<TCComponent> allTableItems = new ArrayList<TCComponent>();
        allTableItems.addAll(m_targetRevision_DispositionMap.keySet());
        
        try
        {
            
            for (TCComponent revision : revisionsList)
            {
                TCComponent[] rdd = revision.getRelatedComponents(rddRelation);
                TCComponentItemRevision rddRev = null;
                
                List<String> parentDataList = new ArrayList<String>();
                List<String> childDataList = new ArrayList<String>();
                List<String[]> row = new ArrayList<String[]>();
                
                if (rdd != null && rdd.length > 0)
                {
                    TCComponentItem rddItem = (TCComponentItem) rdd[0];
                    rddRev = rddItem.getLatestItemRevision();
                }
                
                int inx = 0;
                for (int index = 0; index < noOfColumns; index++)
                {
                    if (index < targetItemRevisionProp.length)
                    {
                        parentDataList.add(revision
                                .getProperty(targetItemRevisionProp[index]));
                        if (rdd != null && rdd.length > 0
                                && revisionsList.contains(rddRev))
                        {
                            childDataList
                                    .add(rddRev
                                            .getProperty(targetItemRevisionProp[index]));
                        }
                    }
                    else
                    {
                        TCComponent disposition = m_targetRevision_DispositionMap
                                .get(revision);
                        parentDataList.add(disposition
                                .getProperty(dispositionProp[inx]));
                        inx++;
                    }
                }
                
                if (revisionsList.contains(rddRev))
                {
                    revisionsList.remove(rddRev);
                }
                
                String[] parentData = new String[parentDataList.size()];
                String[] childData = new String[childDataList.size()];
                
                row.add(parentDataList.toArray(parentData));
                row.add(childDataList.toArray(childData));
                rowObjects.add(row);
            }
            String[] processData = new String[] { m_rootTask.getName(), "", "",
                    "", "" };
            String[] processSubData = new String[] {};
            
            List<String[]> processRow = new ArrayList<String[]>();
            processRow.add(processData);
            processRow.add(processSubData);
            rowObjects.add(0, processRow);
        }
        catch (TCException e)
        {
            System.out
                    .println("TCException:####################getTargetItemsData()");
            e.printStackTrace();
        }
        catch (Exception ex)
        {
            System.out
                    .println("Exception1111:####################getTargetItemsData()");
            ex.printStackTrace();
        }
        return rowObjects;
    }
    
    public HashMap<TCComponent, TCComponent> getTargetRevisionDispositionMap()
    {
        return m_targetRevision_DispositionMap;
    }
    
    public HashMap<TCComponent, TCComponent> getDispositionTargetRevisionMap()
    {
        return m_disposition_TargetRevisionMap;
    }
    
    public HashMap<String, TCComponent> getStringToComponentMap()
    {
        return m_partIdTCComponentMap;
    }
    
    public HashMap<String, TCComponent> getStringToDispositionMap()
    {
        return m_partIdDispositiontMap;
    }
    
    public List<TCComponent> getTargetItemRevisions()
    {
        return m_targetItemRevisionsList;
    }
    
    public List<TCComponent> getTargetItems()
    {
        return m_targetItemsList;
    }
    
    public TCComponentTask getRootTask()
    {
        return m_rootTask;
    }
    
    public TCComponentProcess getCurrentProcess()
    {
        return m_currentProcess;
    }
  
    public static WorkflowProcess getWorkflow()
    {
        return m_workflow;
    }
    
    public static void setWorkflow(WorkflowProcess workflow)
    {
        m_workflow = workflow;
    }
    
}
