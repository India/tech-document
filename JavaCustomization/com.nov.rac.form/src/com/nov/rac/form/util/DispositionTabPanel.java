package com.nov.rac.form.util;

import java.util.ArrayList;
import java.util.Map;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.ui.factory.UIPanelFactory;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DispositionTabPanel
{
    protected ArrayList<IUIPanel> m_panels = null;
    private UIPanelFactory m_panelFactory = null;
    private Registry m_registry = null;
    
    
    public DispositionTabPanel (Registry registry)
    {
        m_registry = registry;
    }
    
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    public boolean createUI(final Composite parent,String panelType)
    {
        try
        {
            m_panelFactory = UIPanelFactory.getInstance();
            m_panelFactory.setRegistry(getRegistry());
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            try
            {
                m_panels = m_panelFactory.createPanels(parent,
                        panelType,theGroup);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
            Display.getDefault().asyncExec(new Runnable()
            {
                @Override
                public void run()
                {
                    parent.pack();
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }
    
    public boolean load(IPropertyMap propertyMap)
    {
        ArrayList<IUIPanel> filteredPanels = m_panels;
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof INOVFormLoadSave)
            {
                INOVFormLoadSave loadablepanel = (INOVFormLoadSave) panel;
                try
                {
                    loadablepanel.load(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        return true;
    }
    
    public boolean loadMap(String partId,Map<String,IPropertyMap> map)
    {
        ArrayList<IUIPanel> filteredPanels = m_panels;
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof INOVDisposition)
            {
                INOVDisposition loadablepanel = (INOVDisposition) panel;
                try
                {
                    loadablepanel.load(partId,map);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        return true;
    }
}
