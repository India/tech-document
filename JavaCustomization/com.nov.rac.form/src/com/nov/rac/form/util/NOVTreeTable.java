package com.nov.rac.form.util;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.nov.rac.form.impl.enform.NOVENTargetsPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.Registry;

public class NOVTreeTable extends JTreeTable
{
    private static final long serialVersionUID = 1L;
    private NOVTreeTableModel m_treeTableModel = null;
    public final int AUTO_RESIZE_ALL_COLUMNS = 4;
    private Registry m_regisrty = null;
    private NOVENTargetsPanel novTargetsPanel = null;
    private Map<String,String> m_ColumnIdentifierMap = new HashMap<String,String>();
    private List<Map> rowObjects = new ArrayList<Map>();
    
    public NOVTreeTable(TCSession tcSession, TreeTableModel treeTableModel)
    {
        super(tcSession, treeTableModel);
    }
    
    public NOVTreeTable(List<Map> rowObjects,    
            TCSession tcSession, String[] columnNames)
    {
        super(tcSession);
       // this.novTargetsPanel = novTargetsPanel;
        this.rowObjects = rowObjects;
        m_treeTableModel = new NOVTreeTableModel(columnNames);
        setModel(m_treeTableModel);
        addColumnsToTable(columnNames);
        //setColumnRenderers();
        loadTableData();
    }
    
    private void loadTableData()
    {
       /* if(rowObjects.size() > 0)
        {
            NovTreeTableNode node1 = new NovTreeTableNode( itemRevArr[index] );
            m_treeTableModel.addRoot( node1 );        
            updateUI();
        }*/
    }

    public Registry getRegistry()
    {
        return m_regisrty;
    }
    
    public void setRegistry(Registry regisrty)
    {
        this.m_regisrty = regisrty;
    }
    
    public NOVTreeTableModel getModel()
    {
        return m_treeTableModel;
    }
        
    public void setColumnIdentifierMapping(Map<String,String> column_identifier)
    {
        m_ColumnIdentifierMap = column_identifier;
    }
    
    public Map<String,String> getColumnIdentifierMapping()
    {
        return m_ColumnIdentifierMap;
    }
    
    private void addColumnsToTable(String[] columnNames)
    {
        for (int i = 0; i < columnNames.length; i++)
        {
            TableColumn column = new TableColumn(i);
            addColumn(column);
        }
    }
    
    @Override
    public void setAutoResizeMode(int i)
    {
        super.setAutoResizeMode(i);
    }
        
       
    public class NOVTreeTableModel extends TreeTableModel
    {
        private Vector<Object> columnNamesVec;
        
        public NOVTreeTableModel(String columnNames[])
        {
            super();
            columnNamesVec = new Vector<Object>();
            TreeTableNode top = new TreeTableNode();
            setRoot(top);
            top.setModel(this);
            for (int i = 0; i < columnNames.length; i++)
            {
                columnNamesVec.addElement(columnNames[i]);
               // modelIndexToProperty.add(columnNames[i]);
            }
            
        }
        
        public void addRoot(TreeTableNode root)
        {
            TreeTableNode top = (TreeTableNode) getRoot();
            top.add(root);
        }
        
        public TreeTableNode getRoot(int index)
        {
            if (((TreeTableNode) getRoot()).getChildCount() > 0)
                return (TreeTableNode) ((TreeTableNode) getRoot())
                        .getChildAt(index);
            else
                return null;
        }
        
        public int getColumnCount()
        {
            return columnNamesVec.size();
        }
        
        public String getColumnName(int column)
        {
            return (String) columnNamesVec.elementAt(column);
        }
        
        public void removeNode(int row)
        {
            // getNodeForRow(row).remove(arg0);
            ((TreeTableNode) getRoot()).remove(getNodeForRow(row));
        }
    }
    
    class RootCellRenderer extends JLabel implements TableCellRenderer
    {
        
        public RootCellRenderer()
        {
            super();
        }
        
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column)
        {
            if (row == 0)
            {
                setText("");
                setBackground(table.getBackground());
                setForeground(table.getForeground());
            }
            else
            {
                setText(value.toString());
                if (isSelected)
                {
                    setOpaque(true);
                    setBackground(table.getSelectionBackground());
                    setForeground(table.getForeground());
                }
                else
                {
                    setBackground(table.getBackground());
                    setForeground(table.getForeground());
                }
            }
            return this;
        }
    }
    
    class TargetIdRenderer extends TreeTableTreeCellRenderer
    {
        
        public TargetIdRenderer(JTreeTable jTreeTable)
        {
            super(jTreeTable);
        }
        
        public Component getTreeCellRendererComponent(JTree tree, Object value,
                boolean sel, boolean expanded, boolean leaf, int row,
                boolean hasFocus)
        {
            // Select the current value
           /* try
            {
                super.getTreeCellRendererComponent(tree, value, sel, expanded,
                        leaf, row, hasFocus);
                
                if (row == 0)
                {
                    if (novTargetsPanel.getCurrentProcess() != null)
                    {
                        String name = novTargetsPanel.getCurrentProcess()
                                .getRootTask().getName();// getProperty("object_name");
                        setText(name);
                    }
                    // setIcon(TCTypeRenderer.getTypeIcon(session.getTypeComponent("Job"),
                    // "Job"));
                }
                else
                {
                    setText(((NovTreeTableNode) getNodeForRow(row)).context
                            .getProperty("item_id"));
                    // setIcon(TCTypeRenderer.getIcon(((NovTreeTableNode)getNodeForRow(row)).context));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                
            }*/
            return this;
        }
    }
    
    public class NovTreeTableNode extends TreeTableNode
    {
        
        private static final long serialVersionUID = 1L;
        //public TCComponent context;
        
       /* public NovTreeTableNode(TCComponent com)
        {
            context = com;
        }*/
        
        @Override
        public String getProperty(String name)
        {
            String status = m_ColumnIdentifierMap.get(name);
            return status != null? status : "" ;
        }
       /* 
        public String getRelation()
        {
            return " ";
        }
        
        public String getType()
        {
            return context.getType();
        }*/
    }
    
}
