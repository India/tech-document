package com.nov.rac.form.util;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.propertymap.IPropertyMap;

public interface INOVForm
{
    public boolean createUI(Composite m_composite);
    public boolean load(IPropertyMap m_propertyMap);
    public boolean save() throws Exception;
    public void setEnabled(boolean flag);
}
