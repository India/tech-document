package com.nov.rac.form.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;

public class TreeTable
{
    private String[] m_tableColumns = null;
    private Tree m_tree = null;
    private int treeRows = 0;
    private TreeEditor m_editor = null;
    private List<TreeEditor> m_editorList = new ArrayList<TreeEditor>();
    private Map<String,TreeEditor> m_editorMap = new HashMap<String,TreeEditor>();
    private int m_width = 800;
    private int m_hight = 120;
    
    public TreeTable(Composite composite)
    {
        createUI(composite);
    }
    
    private void createUI(Composite composite)
    {
        m_tree = new Tree(composite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
                | SWT.FULL_SELECTION);
        m_tree.setHeaderVisible(true);
        m_tree.setLinesVisible(false);
        m_tree.setSortDirection(SWT.ARROW_UP);
        //m_tree.setLayout(new GridLayout(5, true));
        m_tree.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        GridData tree_gridData = new GridData(SWT.FILL, SWT.CENTER, true, true,
                1, 1);
        m_tree.setLayoutData(tree_gridData);
       // m_tree.setBounds(new org.eclipse.swt.graphics.Rectangle(0, 0, 800, 120));
        //addColumns();
    }
    
    public void setBounds(int width,int hight)
    {
        m_width = width;
        m_hight = hight;
        m_tree.setBounds(new org.eclipse.swt.graphics.Rectangle(0, 0, m_width, m_hight));
    }
    
    public void addColumns(String[] tableColumns)
    {
        m_tableColumns = tableColumns;
        int totalColumns = m_tableColumns.length;
        for (int i = 0; i < totalColumns; i++)
        {
            TreeColumn column = new TreeColumn(m_tree, SWT.CENTER);
            column.setMoveable(true);
            column.setText(m_tableColumns[i]);
            column.setWidth(m_width / totalColumns);
            //column.addSelectionListener(new SortTreeListener());
        }
    }
    
    public Tree getTree()
    {
        return m_tree;
    }
    
    public TreeEditor getNewTreeEditor()
    {
        m_editor = new TreeEditor(m_tree);
        return m_editor;
    }
    
    public List<TreeEditor> getAllEditor()
    {
        return m_editorList;
    }
    
    public Map<String,TreeEditor> getEditorMap()
    {
        return m_editorMap;
    }
    
    public TreeItem getTreeItem()
    {
        TreeItem item = new TreeItem(m_tree, SWT.NONE);
        return item;
    }
    
    public void loadTree(List<List<String[]>> rowObjects)
    {
        int totalColumns = m_tableColumns.length;
        treeRows = rowObjects.size();
        int inx = 0;
        for (List<String[]> row : rowObjects)
        {
            String[] parentData = row.get(0);
            String[] childData = row.get(1);
            TreeItem item = getTreeItem();
            TreeItem subItem = null;
            if (childData.length > 0)
            {
                subItem = new TreeItem(item, SWT.NONE);
            }
           for (int i = 0; i < totalColumns; i++)
            {
                item.setText(parentData);
                
                if (childData.length > 0)
                {
                    subItem.setText(childData);
                }
                
            }
            inx++;
        }
    }
    
    public void addImages(Image[] parentImage, Image[] childImage, int Index)
    {
        for (int inx = 0; inx < treeRows; inx++)
        {
            TreeItem item = getTree().getItem(inx);
            
            if (parentImage != null && parentImage.length > 0)
            {
                item.setImage(0, parentImage[inx]);
            }
            if (childImage != null && childImage.length > 0
                    && item.getItemCount() > 0)
            {
                item.getItem(0).setImage(0, childImage[0]);
            }
        }
    }
    
    public void addComboControl(int index, String[] comboValues,
            String defaultValue)
    {
        List<TreeEditor> editorList = new ArrayList<TreeEditor>();
        Map<String,TreeEditor> editorMap = new HashMap<String,TreeEditor>();
        
        for (int inx = 0; inx < treeRows; inx++)
        {
            if (inx == 0)
            {
                continue;
            }
            
            TreeItem item = getTree().getItem(inx);
            defaultValue = item.getText(index);
            Rectangle size = item.getBounds();
            TreeEditor editor = getNewTreeEditor();
            editorList.add(editor);
            editorMap.put(item.getText(0), editor);
            Combo combo = new Combo(getTree(), SWT.DROP_DOWN);
            combo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
            combo.setItems(comboValues);
            combo.setText(defaultValue);
            // combo.setSize(size.width, size.height-5);
            combo.pack();
            
            editor.minimumHeight = combo.getSize().y;
            editor.minimumWidth = combo.getSize().x;
            editor.grabHorizontal = true;
            editor.verticalAlignment = SWT.CLOSE;
            editor.setEditor(combo, item, index);
        }
        
        m_editorList = editorList;
        m_editorMap = editorMap;
    }
    
    public void setComboControlStatus(String defaultStatus, String partId)
    {
        Combo combo = (Combo) m_editorMap.get(partId).getEditor();
        combo.setText(defaultStatus);
    }
    
    public List<String> getTreeColumnData(int columnIndex,
            boolean isComboControl)
    {
        List<String> columnData = new ArrayList<String>();
        String columnText = "";
        int comboIdx = 0;
        for (int inx = 1; inx < treeRows; inx++)
        {
            if (!isComboControl)
            {
                columnText = getTree().getItem(inx).getText(columnIndex);
                columnData.add(columnText);
            }
            else
            {
                Combo combo = (Combo) m_editorList.get(comboIdx).getEditor();
                columnText = combo.getText();
                columnData.add(columnText);
                comboIdx++;
            }
            
        }
        return columnData;
    }
    
    public void clear()
    {
        getTree().removeAll();
        for (final TreeEditor editor : getAllEditor())
        {
            final Combo combo = (Combo) editor.getEditor();
            combo.dispose();
        }
    }
    /*
     * public List<String> getControlTreeCoulmnData() { List<String> columnData
     * = new ArrayList<String>(); for (TreeEditor editor : getAllEditor()) {
     * String comboText = ((Combo) editor.getEditor()).getText();
     * columnData.add(comboText); } return columnData; }
     */
}
