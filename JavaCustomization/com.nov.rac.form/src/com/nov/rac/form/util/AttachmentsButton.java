package com.nov.rac.form.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class AttachmentsButton extends Composite
{

    private Button m_plusButton = null;
    private Button m_minusButton = null;
    
    public AttachmentsButton(Composite parent, int style, Image plus, Image minus )
    {
    	super(parent, SWT.NONE);
        setLayout(new GridLayout(2, true));
        
        m_plusButton = new Button( this, SWT.NONE );
        m_plusButton.setLayoutData( new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1) );
        m_plusButton.setImage( plus );
        
        m_minusButton = new Button( this, SWT.NONE );
        m_minusButton.setLayoutData( new GridData( SWT.CENTER, SWT.BOTTOM, false, false, 1, 1 ) );
        m_minusButton.setImage ( minus );
    }
    
    public void addSelectionListenerToPlusButton(SelectionListener listener )
    {
		m_plusButton.addSelectionListener(listener);
    }
    
    public void addSelectionListenerToMinusButton(SelectionListener listener )
    {
		m_minusButton.addSelectionListener(listener);
    }
    
    @Override
    public void setEnabled(boolean enabled) 
    {
    	m_plusButton.setVisible(enabled);
    	m_minusButton.setVisible(enabled);
    	
    	super.setEnabled(enabled);
    }
    

}
