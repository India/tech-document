package com.nov.rac.form.util;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class AddTargetToEnSearchProvider implements INOVSearchProvider2
{
    
  String m_sItemId = null;
  String m_sAltID = null;
  boolean m_isMissionGrp = false;
    
  public  AddTargetToEnSearchProvider(String itemID,boolean isMissionGrp)
  {
      m_sItemId = itemID;
      m_isMissionGrp = isMissionGrp;
  }
    
    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public QuickBindVariable[] getBindVariables()
    {
        // Prepare input fields - substitute * for empty values
//        m_sItemId = m_itemIDText;
        m_sAltID = m_sItemId;
        if (m_sAltID == null || m_sAltID.isEmpty())
            m_sAltID = "*";
        
        // Construct Bind Variables
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[12];
        
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[] { m_sItemId };
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[] { "*" }; // item name
        
        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[] { "*" };// item type
        
        bindVars[3] = new QuickSearchService.QuickBindVariable();
        bindVars[3].nVarType = POM_date;
        bindVars[3].nVarSize = 2;
        bindVars[3].strList = new String[] { "*", "*" };// m_sCreatedAfter,m_sCreatedBefore
        
        bindVars[4] = new QuickSearchService.QuickBindVariable();
        bindVars[4].nVarType = POM_string;
        bindVars[4].nVarSize = 1;
        bindVars[4].strList = new String[] { "*" };// desc
        
        bindVars[5] = new QuickSearchService.QuickBindVariable();
        bindVars[5].nVarType = POM_string;
        bindVars[5].nVarSize = 1;
        bindVars[5].strList = new String[] { "*" };// release status
        
        bindVars[6] = new QuickSearchService.QuickBindVariable();
        bindVars[6].nVarType = POM_string;
        bindVars[6].nVarSize = 1;
        bindVars[6].strList = new String[] { "*" };// m_sOwningUser
        
        bindVars[7] = new QuickSearchService.QuickBindVariable();
        bindVars[7].nVarType = POM_typed_reference;
        bindVars[7].nVarSize = 1;
        bindVars[7].strList = new String[] { "" };
        // bindVars[7].strList = new String[] {m_sGrpID};
        
        bindVars[8] = new QuickSearchService.QuickBindVariable();
        bindVars[8].nVarType = POM_string;
        bindVars[8].nVarSize = 1;
        
        // Tushar Time being set to true
        // if(!m_isMissionGrp)
        if (m_isMissionGrp)
            bindVars[8].strList = new String[] { "JDE" };
        else
            bindVars[8].strList = new String[] { "RSOne" };
        
        bindVars[9] = new QuickSearchService.QuickBindVariable();
        bindVars[9].nVarType = POM_string;
        bindVars[9].nVarSize = 1;
        bindVars[9].strList = new String[] { "*" };// m_sLastModifiedUser
        
        // Rakesh K - Start - Send Alternate ID selection
        bindVars[10] = new QuickSearchService.QuickBindVariable();
        bindVars[10].nVarType = POM_string;
        bindVars[10].nVarSize = 1;
        bindVars[10].strList = new String[] { m_sAltID };
        
        bindVars[11] = new QuickSearchService.QuickBindVariable();
        bindVars[11].nVarType = POM_string;
        bindVars[11].nVarSize = 1;
        bindVars[11].strList = new String[] { "*" };
        
        return bindVars;
    }

    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        // 0, 1 2 3 4 5 6 7 8
        // puid, item_id, alt_id, Latest
        // Revision,object_name,object_desc,Revision Status,
        // object_type,rsone_itemtype
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[4];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8,
                10, 11, 12 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 5 }; // "object_name",
                                                                          // "item_id",
                                                                          // "object_type",,"object_desc"
        allHandlerInfo[0].listInsertAtIndex = new int[] { 4, 1, 7, 5 }; // Item
                                                                        // Id,
                                                                        // Item
                                                                        // Name
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        // allHandlerInfo[1].handlerName = "NOVSRCH-get-item-latest-revs";
        allHandlerInfo[1].handlerName = "NOVSRCH-get-nov4part-latest-revs";
        allHandlerInfo[1].listBindIndex = new int[0];
        // allHandlerInfo[1].listBindIndex = new int[1];
        // allHandlerInfo[1].listBindIndex[0] = 0;
        // allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1, 2 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 3, 6 }; // latest rev
                                                                  // id, release
                                                                  // status
        
        allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-alternate-ids";
        allHandlerInfo[2].listBindIndex = new int[] { 9 };
        allHandlerInfo[2].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[2].listInsertAtIndex = new int[] { 2 };
        
        allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-nov4part-master-props";
        allHandlerInfo[3].listBindIndex = new int[0];
        allHandlerInfo[3].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[3].listInsertAtIndex = new int[] { 8 };
        
        return allHandlerInfo;
    }

    @Override
    public boolean validateInput()
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
