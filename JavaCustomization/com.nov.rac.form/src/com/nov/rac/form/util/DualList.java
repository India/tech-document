package com.nov.rac.form.util;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class DualList
{
    private int horizontalSpan = 4;
    private int verticalSpan = 10;
    
    protected List m_sourceList = null;
    protected DualListButton m_plusMinusButtonComposite = null;
    protected List m_targetList = null;
    private Registry m_registry = null;
    private Composite m_dualListComposite = null;
    private Composite m_parent = null;
    private TCTable m_sourceTable;
    private TCTable m_targetTable;
    
    public DualList(Composite parent, Registry registry)
    {
        m_registry = registry;
        m_parent = parent;
        // createUI(parent);
        // addButtonListener();
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void createUI()
    {
        m_dualListComposite = new Composite(m_parent, SWT.NONE);
        
        Composite dualListComposite = m_dualListComposite;
        
        GridData dualList_gridData = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1);
        
        dualListComposite.setLayoutData(dualList_gridData);
        
        GridLayout gl_l_composite = new GridLayout(2 * horizontalSpan + 1,
                false);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        
        dualListComposite.setLayout(gl_l_composite);
        
        // createDualListHeader(dualListComposite);
        
        createSourceTCTable(dualListComposite);
        // createSourceList(dualListComposite);
        
        createDualListButtons(dualListComposite);
        
        createTargetTCTable(dualListComposite);
        // createTargetList(dualListComposite);
        
        addButtonListener();
    }
    
    private void createTargetTCTable(Composite parent)
    {
        Object[] retObject = createTCTable();
        m_targetTable = (TCTable) retObject[0];
        JScrollPane scrollPane = (JScrollPane) retObject[1];
        SWTUIUtilities.embed(createTableComposite(parent), scrollPane, false);
        
    }
    
    private void createSourceTCTable(Composite parent)
    {
        
        Object[] retObject = createTCTable();
        m_sourceTable = (TCTable) retObject[0];
        JScrollPane scrollPane = (JScrollPane) retObject[1];
        Composite composite = createTableComposite(parent);
        SWTUIUtilities.embed(composite, scrollPane, false);
        
    }
    
    private Composite createTableComposite(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, horizontalSpan, verticalSpan);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        return tCTableComposite;
    }
    
    private Object[] createTCTable()
    {
        Object[] retObjects = new Object[2];
        TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication()
                .getSession();
        
        TCTable tCTable = new TCTable(tcSession, new String[] { "Column" });
        tCTable.setShowVerticalLines(true);
        tCTable.setAutoCreateColumnsFromModel(false);
        tCTable.setAutoResizeMode(tCTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane scrollPane = new JScrollPane(tCTable);
        TitledBorder titleBorder = new TitledBorder("");
        scrollPane.setBorder(titleBorder);
        
        tCTable.setVisible(true);
        tCTable.getTableHeader().setBorder(
                UIManager.getBorder("TableHeader.cellBorder"));
        tCTable.getTableHeader().setVisible(false);
        tCTable.getTableHeader().setPreferredSize(new Dimension(-1, 0));
    
        scrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
//        tCTable.setBackground(Color.WHITE);
        scrollPane.getViewport().setBackground(Color.WHITE);
        retObjects[0] = tCTable;
        retObjects[1] = scrollPane;
        return retObjects;
        
    }
    
//    protected void createTargetList(Composite dualListComposite)
//    {
//        m_targetList = new List(dualListComposite, SWT.BORDER | SWT.V_SCROLL);
//        GridData l_gd_targetList = new GridData(SWT.FILL, SWT.FILL, true, true,
//                horizontalSpan, verticalSpan);
//        m_targetList.setLayoutData(l_gd_targetList);
//        m_targetList
//                .setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//    }
    
    protected void createDualListButtons(Composite dualListComposite)
    {
        m_plusMinusButtonComposite = new DualListButton(dualListComposite);
        
        m_plusMinusButtonComposite.changeSpan(horizontalSpan, verticalSpan);
        
        m_plusMinusButtonComposite.createUI();
        
        Image plusButtonImage = new Image(dualListComposite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        
        m_plusMinusButtonComposite.setPlusButtonImage(plusButtonImage);
        
        Image minusButtonImage = new Image(dualListComposite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        
        m_plusMinusButtonComposite.setMinusButtonImage(minusButtonImage);
    }
    
//    protected void createSourceList(Composite dualListComposite)
//    {
//        m_sourceList = new List(dualListComposite, SWT.BORDER | SWT.V_SCROLL);
//        GridData l_gd_sourceList = new GridData(SWT.FILL, SWT.FILL, true, true,
//                horizontalSpan, verticalSpan);
//        m_sourceList.setLayoutData(l_gd_sourceList);
//        m_sourceList
//                .setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//    }
//    
//    protected void createDualListHeader(Composite dualListComposite)
//    {
//        Label lblAvailable = new Label(dualListComposite, SWT.NONE);
//        lblAvailable.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
//                false, horizontalSpan, 1));
//        lblAvailable.setText(getRegistry().getString("Available.Label"));
//        SWTUIHelper.setFont(lblAvailable, SWT.BOLD);
//        
//        new Label(dualListComposite, SWT.NONE);
//        
//        Label lblSelected = new Label(dualListComposite, SWT.NONE);
//        lblSelected.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
//                false, horizontalSpan, 1));
//        lblSelected.setText(getRegistry().getString("Selected.Label"));
//        SWTUIHelper.setFont(lblSelected, SWT.BOLD);
//    }
    
    protected void addButtonListener()
    {
        m_plusMinusButtonComposite
                .addSelectionListenerToPlusButton(new SelectionListener()
                {
                    
                    @Override
                    public void widgetSelected(SelectionEvent selectionevent)
                    {
                        addToTargetTable();
                    }
                    
                    @Override
                    public void widgetDefaultSelected(
                            SelectionEvent selectionevent)
                    {
                        
                    }
                });
        
        m_plusMinusButtonComposite
                .addSelectionListenerToMinusButton(new SelectionListener()
                {
                    
                    @Override
                    public void widgetSelected(SelectionEvent selectionevent)
                    {
                        removeFromTargetTable();
                    }
                    
                    @Override
                    public void widgetDefaultSelected(
                            SelectionEvent selectionevent)
                    {
                        
                    }
                });
        
    }
    
    // private void removeFromTargetList()
    // {
    // String[] selectedComponents = getTargetList().getSelection();
    // for (int i = 0; i < selectedComponents.length; i++)
    // {
    // getTargetList().remove(selectedComponents[i]);
    // java.util.List<String> availableSourceComponents = Arrays
    // .asList(getSourceList().getItems());
    // if ((!availableSourceComponents.contains(selectedComponents[i])))
    // {
    // getSourceList().add(selectedComponents[i]);
    // }
    // }
    // }
    
    private void removeFromTargetTable()
    {
        
        int noRowSelected = -1;
        int selectedRowIndex = getTargetTable().getSelectedRow();
        if (selectedRowIndex != noRowSelected)
        {
            
            String selectedComponent = (String) getTargetTable().getValueAt(
                    selectedRowIndex, 0);
            TCTableRemoveString(getTargetTable(), selectedComponent);
            
            Vector<String> tableRow = new Vector<String>();
            tableRow.add(selectedComponent);
            getSourceTable().addRow(tableRow);
        }
        
    }
    
    // private void addToTargetList()
    // {
    // /*
    // * int index = getSourceList().getSelectionIndex(); if (index != -1) {
    // * getTargetList().add(getSourceList().getItem(index));
    // * getSourceList().remove(index); }
    // */
    //
    // String[] selectedComponents = getSourceList().getSelection();
    // java.util.List<String> existComps = new ArrayList<String>();
    //
    // for (int i = 0; i < selectedComponents.length; i++)
    // {
    // int objInd = getTargetList().indexOf(selectedComponents[i]);
    // if (objInd >= 0)
    // {
    // existComps.add(selectedComponents[i]);
    // }
    // else
    // {
    // java.util.List<String> availableTargetComponents = Arrays
    // .asList(getTargetList().getItems());
    // if (!availableTargetComponents.contains(selectedComponents[i]))
    // {
    // getSourceList().remove(selectedComponents[i]);
    // }
    // getTargetList().add(selectedComponents[i]);
    // }
    // }
    // if (existComps.size() > 0)
    // {
    // showErrorIfListisNotEmpty(existComps);
    // }
    // }
    //
    
    private void addToTargetTable()
    {
        int noRowSelected = -1;
        int selectedRowIndex = getSourceTable().getSelectedRow();
        if (selectedRowIndex != noRowSelected)
        {
            String selectedComponent = (String) getSourceTable().getValueAt(
                    selectedRowIndex, 0);
            TCTableRemoveString(getSourceTable(), selectedComponent);
            
            java.util.List<String> tableRow = new java.util.ArrayList<String>();
            tableRow.add(selectedComponent);
            getTargetTable().addRow(tableRow);
       
        }
    }
    
//    private void showErrorIfListisNotEmpty(java.util.List<String> existComps)
//    {
//        String errorStr = getRegistry().getString("AlreadyObjects.MSG");
//        
//        for (int i = 0; i < existComps.size(); i++)
//        {
//            errorStr = errorStr + existComps.get(i).toString() + ",";
//        }
//        if (errorStr.charAt(errorStr.length() - 1) == ',')
//        {
//            errorStr = errorStr.substring(0, errorStr.length() - 1);
//        }
//        
//        MessageBox.post(errorStr, getRegistry().getString("Info.MSG"),
//                MessageBox.INFORMATION);
//        // MessageBox.post(getRegistry().getString("Info.MSG"), errorStr,
//        // MessageBox.INFORMATION); // Vivek
//        
//    }
    
//    public List getSourceList()
//    {
//        return m_sourceList;
//    }
//    
    public TCTable getSourceTable()
    {
        return m_sourceTable;
    }
    
//    public void setSourceList(List m_sourceList)
//    {
//        this.m_sourceList = m_sourceList;
//    }
    
    public DualListButton getPlusMinusButtonComposite()
    {
        return m_plusMinusButtonComposite;
    }
    
//    public List getTargetList()
//    {
//        return m_targetList;
//    }
    
    public TCTable getTargetTable()
    {
        return m_targetTable;
    }
    
//    public void setTargetList(List m_targetList)
//    {
//        this.m_targetList = m_targetList;
//    }
    
    public void setEnabled(boolean flag)
    {
         m_sourceTable.setEnabled(flag);
         m_targetTable.setEnabled(flag);
        m_plusMinusButtonComposite.setEnabled(flag);
    }
    
//    public void addToSourceList(Object[] sourceObjs)
//    {
//        java.util.List<String> availableSourceComponents = Arrays
//                .asList(getSourceList().getItems());
//        
//        if (sourceObjs != null)
//        {
//            if (getSourceList().getItemCount() > 0)
//            {
//                getSourceList().removeAll();
//            }
//            for (int i = 0; i < sourceObjs.length; i++)
//            {
//                if (!availableSourceComponents.contains(sourceObjs[i]
//                        .toString()))
//                {
//                    getSourceList().add(sourceObjs[i].toString());
//                }
//            }
//        }
//    }
    
    private boolean TCTableContains(TCTable tCTable, String stringToCheck)
    {
        
        for (int inx = 0; inx < tCTable.getRowCount(); inx++)
        {
            String currValue = (String) tCTable.getValueAt(inx, 0);
            if (currValue.equalsIgnoreCase(stringToCheck))
            {
                return true;
            }
        }
        
        return false;
        
    }
    private boolean TCTableContains(TCTable tCTable, TCComponent tCComponentToCheck)
    {
        
        for (int inx = 0; inx < tCTable.getRowCount(); inx++)
        {
            TCComponent currValue =  (TCComponent) tCTable.getValueAt(inx, 0);
            if (currValue.equals(tCComponentToCheck))
            {
                return true;
            }
        }
        
        return false;
        
    }
    
    private boolean TCTableRemoveString(TCTable tCTable, String stringToRemove)
    {
        
        for (int inx = 0; inx < tCTable.getRowCount(); inx++)
        {
            String currValue = (String) tCTable.getValueAt(inx, 0);
            if (currValue.equalsIgnoreCase(stringToRemove))
            {
                tCTable.removeRow(inx);
                return true;
            }
        }
        
        return false;
        
    } // ask inputs , can we improve this code?
    
    private boolean TCTableRemoveString(TCTable tCTable, TCComponent tComponentToRemove)
    {
        
        for (int inx = 0; inx < tCTable.getRowCount(); inx++)
        {
            TCComponent currValue = (TCComponent) tCTable.getValueAt(inx, 0);
            if (currValue.equals(tComponentToRemove))
            {
                tCTable.removeRow(inx);
                return true;
            }
        }
        
        return false;
        
    }
    
    public void addToTargetTable(String strValue)
    {
       
        if (TCTableContains(getTargetTable(), strValue))
        {
            String errorStr = strValue+" "
                    + getRegistry().getString("AlreadyObjects.MSG");
            MessageBox.post(errorStr, getRegistry().getString("Info.MSG"),
                    MessageBox.INFORMATION);
            return;
        }
        
        if (TCTableContains(getSourceTable(), strValue))
        {
            TCTableRemoveString(getSourceTable(), strValue);
        }
        Vector<String> tableRow = new Vector<String>();
        tableRow.add(strValue);
        getTargetTable().addRow(tableRow);

    }
    public void addToTargetTable(TCComponent tCComponent)
    {
       
        if (TCTableContains(getTargetTable(), tCComponent))
        {
            String errorStr = tCComponent.getObjectString()+" "
                    + getRegistry().getString("AlreadyObjects.MSG");
            MessageBox.post(errorStr, getRegistry().getString("Info.MSG"),
                    MessageBox.INFORMATION);
            return;
        }
        
        if (TCTableContains(getSourceTable(), tCComponent))
        {
            TCTableRemoveString(getSourceTable(), tCComponent);
        }
        Vector<TCComponent> tableRow = new Vector<TCComponent>();
        tableRow.add(tCComponent);
        getTargetTable().addRow(tableRow);

    }
    
    public void addToSourceTable(String strValue)
    {

        
        if (TCTableContains(getSourceTable(), strValue) ||TCTableContains(getTargetTable(), strValue))
        {
            String errorStr = strValue+" "
                    + getRegistry().getString("AlreadyObjects.MSG");
            MessageBox.post(errorStr, getRegistry().getString("Info.MSG"),
                    MessageBox.INFORMATION);
            return;
        }
        
        
        Vector<String> tableRow = new Vector<String>();
        tableRow.add(strValue);
        getSourceTable().addRow(tableRow);
      
        
    }
    
    public void addToSourceTable(TCComponent tCComponent)
    {

        
        if (TCTableContains(getSourceTable(), tCComponent) ||TCTableContains(getTargetTable(), tCComponent))
        {
            String errorStr = tCComponent.getObjectString()+" "
                    + getRegistry().getString("AlreadyObjects.MSG");
            MessageBox.post(errorStr, getRegistry().getString("Info.MSG"),
                    MessageBox.INFORMATION);
            return;
        }
        
        
        Vector<TCComponent> tableRow = new Vector<TCComponent>();
        tableRow.add(tCComponent);
        getSourceTable().addRow(tableRow);
      
        
    }
    
    public void changeSpan(int horizontalSpan, int verticalSpan)
    {
        this.horizontalSpan = horizontalSpan;
        this.verticalSpan = verticalSpan;
    }
}
