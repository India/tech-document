package com.nov.rac.form.impl.ecrform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TargetsTableBottomPanel;
import com.teamcenter.rac.util.Registry;

public class ECRTargetsTableBottomPanel extends TargetsTableBottomPanel 
{

	public ECRTargetsTableBottomPanel(Composite parent, int style) 
	{
		super(parent, style);
		Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
	}

}
