package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class EditButtonPanel extends AbstractUIPanel implements
        INOVFormLoadSave, IPublisher, ISubscriber
{
    
    private Registry m_registry;
    private Button m_editButton;
    private Button m_cancelButton;
    protected TCComponent m_docRev;
    private String m_docRevSelectionChangeEventName = "";
    private FeedBackButtonComposite m_feedBackButton;
    
    public EditButtonPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        if (propMap != null && propMap.getComponent() != null)
        {
            m_docRev = propMap.getComponent();
        }
        setEditButtonEnabled();
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // m_editButton.setEnabled(flag);
    }
    
    public void setDocRevSelectionChangeEventName(String name)
    {
        m_docRevSelectionChangeEventName = name;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        createButtons(composite);
        
        registerSubscriber();
        return true;
    }
    
    protected void createButtons(Composite parent)
    {
        Composite buttonsComposite = createButtonsComposite(parent, SWT.NONE);
        
        m_feedBackButton = createFeedBackButtonUI(buttonsComposite);
        m_editButton = createEditButtonUI(buttonsComposite, SWT.NONE);
        m_cancelButton = createCancelButtonUI(buttonsComposite, SWT.NONE);
        
    }
    
    protected FeedBackButtonComposite createFeedBackButtonUI(Composite parent)
    {
        FeedBackButtonComposite feedBackButtonComposite = new FeedBackButtonComposite(
                parent, SWT.NONE);
        feedBackButtonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL,
                true, true, 1, 1));
        return feedBackButtonComposite;
    }
    
    private Button createCancelButtonUI(Composite parent, int style)
    {
        Button button = new Button(parent, style);
        button.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, true, 1,
                1));
        button.setText(getRegistry().getString("CancelButton.Label"));
        
        button.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e)
            {
                cancelButtonListener();
            }
        });
        button.setEnabled(false);
        return button;
    }
    
    private void cancelButtonListener()
    {
        m_cancelButton.setEnabled(false);
        PublishEvent publishEvent = new PublishEvent(EditButtonPanel.this,
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, null, null);
        getController().publish(publishEvent);
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    protected Button createEditButtonUI(Composite buttonComposite, int style)
    {
        Button button = new Button(buttonComposite, style);
        button.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, true, 1,
                1));
        button.setText(getRegistry().getString("EditButton.Lable"));
        button.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent e)
            {
                PublishEvent event = new PublishEvent(EditButtonPanel.this,
                        GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, null, null);
                getController().publish(event);
                m_cancelButton.setEnabled(true);
            }
        });
        
        return button;
    }
    
    protected Composite createButtonsComposite(Composite parent, int style)
    {
        Composite composite = new Composite(parent, style);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(3, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        Display.getDefault().syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (!getComposite().isDisposed())
                {
                    
                    if (event.getPropertyName().equalsIgnoreCase(
                            GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))
                    {
                        m_cancelButton.setEnabled(false);
                    }
                    if (event.getPropertyName().equalsIgnoreCase(
                            m_docRevSelectionChangeEventName))
                    {
                        
                        IPropertyMap docRevPropMap = (IPropertyMap) event
                                .getNewValue();
                        try
                        {
                            load(docRevPropMap);
                        }
                        catch (TCException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    
                }
            }
        });
        
    }
    
    protected void setEditButtonEnabled()
    {
        
        if (!FormHelper.isLatestItemRevision(m_docRev)
                || !FormHelper.hasWriteAccess(m_docRev))
        {
            m_editButton.setEnabled(false);
        }
        else
        {
            m_editButton.setEnabled(true);
        }
    }
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(m_docRevSelectionChangeEventName,
                getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(m_docRevSelectionChangeEventName,
                getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
}
