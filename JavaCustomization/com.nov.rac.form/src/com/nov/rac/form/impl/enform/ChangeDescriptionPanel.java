package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

/**
 * 
 * 
 * @author surajrk
 * 
 */

public class ChangeDescriptionPanel extends AbstractExpandableUIPanel
        implements INOVFormLoadSave
{
    private Text m_changeDescriptionTextField = null;
    
    private Composite m_changeDescriptionGroupContainer = null;
    private Registry m_registry = null;
    
    public ChangeDescriptionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_changeDescriptionTextField.setText(PropertyMapHelper
                .handleNull(propMap.getString("nov4_change_desc")));
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString("nov4_change_desc",
                m_changeDescriptionTextField.getText());
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return true;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, true));
        
        createChangeDescriptionGroupContainer(l_composite);
        createChangeDescriptionTextField();
        
        createDataModelMapping();
        
        return true;
    }
    
    private void createChangeDescriptionGroupContainer(Composite l_composite)
    {
        m_changeDescriptionGroupContainer = l_composite;
        String label = getRegistry().getString(
                "ChangeDescriptionPanel.GroupContainerText");
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 25);
        // m_ChangeDescriptionGroupContainer = new Group(l_composite, SWT.NONE);
        m_changeDescriptionGroupContainer.setLayoutData(gridData);
        m_changeDescriptionGroupContainer.setLayout(new GridLayout(1, true));
        // m_ChangeDescriptionGroupContainer.setText(label);
        SWTUIHelper.setFont(m_changeDescriptionGroupContainer, SWT.BOLD);
        
        getExpandableComposite().setText(label);
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
    }
    
    private void createChangeDescriptionTextField()
    {
        m_changeDescriptionTextField = new Text(
                m_changeDescriptionGroupContainer, SWT.BORDER | SWT.MULTI
                        | SWT.WRAP | SWT.V_SCROLL);
        UIHelper.makeMandatory(m_changeDescriptionTextField);
        m_changeDescriptionTextField.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        m_changeDescriptionTextField.setLayoutData(gridData);
        
        
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_changeDescriptionTextField.setEditable(flag);
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_changeDescriptionTextField,
                "nov4_change_desc", FormHelper.getDataModelName());
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
}
