package com.nov.rac.form.impl.enform;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCClassificationService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentICO;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.ics.ICSKeyLov;
import com.teamcenter.rac.kernel.ics.ICSProperty;
import com.teamcenter.rac.kernel.ics.ICSPropertyDescription;
import com.teamcenter.rac.util.Registry;

public class ClassificationDataPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    private Composite m_composite = null;
    private ExpandBar m_expandBar = null;
    private Button m_editClassifiction = null;
    private Combo m_unitSystemCombo = null;
    private ExpandItem m_expandItem = null;
    private Table m_table = null;
    
    private Registry m_registry;
    private Vector<String> m_metricAttributes = new Vector<>();
    private Vector<String> m_nonMetricAttributes = new Vector<>();
    private TCComponentItemRevision m_targetComponet;
    
    public ClassificationDataPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        m_composite = getComposite();
        m_composite.setLayout(new GridLayout(1, true));
        
        GridData gridData = new GridData(SWT.FILL, SWT.NONE, false, false, 1, 1);
        m_composite.setLayoutData(gridData);
        
        createExpandBar(m_composite);
        createClassificationDataPanel();
        return true;
    }
    
    private void createExpandBar(final Composite composite)
    {
        Composite expandBarComposite = new Composite(composite, SWT.NONE);
        expandBarComposite.setLayout(new GridLayout(6, true));
        
        expandBarComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
        
        // create expandbar
        m_expandBar = new ExpandBar(expandBarComposite, SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1);
        m_expandBar.setLayoutData(gridData);
        SWTUIHelper.setFont(m_expandBar, SWT.BOLD);
        
        m_editClassifiction = new Button(expandBarComposite, SWT.NONE);
        m_editClassifiction.setText(m_registry
                .getString("EditClassificationButton.Label"));
        GridData btnGridData = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        btnGridData.verticalIndent = 4;
        m_editClassifiction.setLayoutData(btnGridData);
        
        addListenerToEditClassifiationButton();
        addListenerToExpandBar();
    }
    
    private void addListenerToEditClassifiationButton()
    {
        m_editClassifiction.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                IPerspectiveDef persDef = PerspectiveDefHelper
                        .getPerspectiveDef("com.teamcenter.rac.classification.icm.ClassificationPerspective");
                InterfaceAIFComponent[] component = new InterfaceAIFComponent[] { m_targetComponet };
                persDef.openPerspective(component);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent paramSelectionEvent)
            {
            }
        });
    }
    
    private void addListenerToExpandBar()
    {
        m_expandBar.addExpandListener(new ExpandListener()
        {
            @Override
            public void itemExpanded(ExpandEvent arg0)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        m_composite.getParent().pack();
                    }
                });
            }
            
            @Override
            public void itemCollapsed(ExpandEvent arg0)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        m_composite.getParent().pack();
                    }
                });
            }
        });
    }
    
    private void createClassificationDataPanel()
    {
        Composite classificationDataComposite = new Composite(m_expandBar,
                SWT.NONE);
        classificationDataComposite.setLayout(new GridLayout(7, true));
        classificationDataComposite.setLayoutData(new GridData(SWT.FILL,
                SWT.FILL, true, true, 1, 1));
        
        createAtributePanel(classificationDataComposite);
        
        createUnitPanel(classificationDataComposite);
        
        m_expandItem = new ExpandItem(m_expandBar, SWT.NONE);
        
        m_expandItem.setText(m_registry.getString("Classification.Label"));
        m_expandItem.setHeight(120);
        m_expandItem.setControl(classificationDataComposite);
    }
    
    private void createAtributePanel(Composite content)
    {
        ScrolledComposite sc = new ScrolledComposite(content, SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.BORDER);
        sc.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 5, 5));
        
        sc.setLayout(new GridLayout());
        
        m_table = new Table(sc, SWT.BORDER);
        m_table.setHeaderVisible(false);
        new TableColumn(m_table, SWT.LEFT);

        SWTUIHelper.setFont(m_table, SWT.BOLD);
        
        sc.setContent(m_table);
        sc.setExpandHorizontal(true);
        sc.setExpandVertical(true);
    }
    
    private void createUnitPanel(Composite inComposite)
    {
        Label unitLabel = new Label(inComposite, SWT.NONE);
        unitLabel.setText("Units");
        SWTUIHelper.setFont(unitLabel, SWT.BOLD);
        GridData grdData = new GridData(SWT.RIGHT, SWT.TOP, true, true, 1, 1);
        grdData.verticalIndent = 3;
        unitLabel.setLayoutData( grdData );
        
        m_unitSystemCombo = new Combo(inComposite, SWT.READ_ONLY);
        m_unitSystemCombo.add("Metric");
        m_unitSystemCombo.add("Imperial");
        
        m_unitSystemCombo.select(0);
        m_unitSystemCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
                true, 1, 1));
        
        addListenerToUnitSystemList();
    }
    
    private void addListenerToUnitSystemList()
    {
        m_unitSystemCombo.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                populateClassificationData();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_targetComponet = (TCComponentItemRevision) propMap.getComponent();

//        String classified = propMap.getString("ics_classified");
//        String objectString = propMap.getString("object_string");
//        String className = propMap.getString("ics_subclass_name");
        
        String classified = m_targetComponet.getProperty("ics_classified");
        String objectString = m_targetComponet.getProperty("object_string");
        String className = m_targetComponet.getProperty("ics_subclass_name");
        
        if (classified != null && classified.contains("Yes")) // need to check
        {
            String text = m_registry.getString("Classification.Label") + " : "
                    + objectString + " "
                    + m_registry.getString("IsClassifiedIn.Label") + " "
                    + className;
            m_expandItem.setText(text);
            
            initAttributeValues(propMap);
            populateClassificationData();
        }
        else
        {
            String text = m_registry.getString("Classification.Label") + " : "
                    + objectString + " "
                    + m_registry.getString("NotClassified.Label");
            
            m_expandItem.setText(text);
            m_expandBar.setEnabled(false);
        }
        
//////============= Testing : start ===============
//        m_expandBar.setEnabled(true);
//        for (int i = 0; i < 20; i++)
//        {
//            m_metricAttributes.add("metric data - " + i);
//            m_nonMetricAttributes.add("non metric data - " + i);
//        }
//        populateClassificationData();
//////============= Testing : end ===============
        
        return true;
    }
    
    private void populateClassificationData()
    {
        int selection = m_unitSystemCombo.getSelectionIndex();
        Vector<String> attributes;
        if (selection == 0)
        {
            attributes = m_metricAttributes;
        }
        else
        {
            attributes = m_nonMetricAttributes;
        }
       
        m_table.removeAll();
        for(int inx = 0; inx < attributes.size(); inx ++)
        {
            TableItem item = new TableItem(m_table, SWT.NONE);
            item.setText( attributes.get(inx) );
        }
        
        TableColumn column = m_table.getColumn(0);
        
        column.pack();
        // hack : column.pack does not show some last characters of cell
        column.setWidth(column.getWidth() + 15);
    }
    
    private void initAttributeValues(IPropertyMap propMap) throws TCException
    {
        //TCComponent[] icoObjects = propMap.getTagArray("IMAN_classification");
        TCComponent[] icoObjects = m_targetComponet.getRelatedComponents("IMAN_classification");
        
        TCComponentICO icoObject = (TCComponentICO) icoObjects[0];
        int unitSys = icoObject.getUnitSystem();
        
        // get all properties of ico object. this give attribute ID and value
        ICSProperty[] icsProperties = icoObject.getICSProperties(true);
        Map<Integer, String> currentAttributeIDtoValueMap = new HashMap<Integer, String>();
        for (ICSProperty icsProp : icsProperties)
        {
            currentAttributeIDtoValueMap.put(icsProp.getId(),
                    icsProp.getValue());
        }
        
        // get attribute name and corresponding value
        ICSPropertyDescription[] icsPropDesc = icoObject
                .getICSPropertyDescriptors();
        
        // create arrays required for converting attribute values
        int arraySize = icsPropDesc.length;
        String[] attributeIDs = new String[arraySize];
        String[] convertToUnits = new String[arraySize];
        String[] values = new String[arraySize];
        String[] currentUnits = new String[arraySize];
        int[] formates = new int[arraySize];
        
        ICSPropertyDescription propDescriptor;
        for (int inx = 0; inx < icsPropDesc.length; inx++)
        {
            propDescriptor = icsPropDesc[inx];
            propDescriptor.setActiveUnitsystem(unitSys);
            attributeIDs[inx] = String.valueOf(propDescriptor.getId());
            convertToUnits[inx] = propDescriptor.getAltUnit();
            
            values[inx] = currentAttributeIDtoValueMap.get(propDescriptor
                    .getId());
            currentUnits[inx] = propDescriptor.getUnit();
            formates[inx] = propDescriptor.getFormat().getFormat();
        }
        
        // call convertAttibuteValues service to convert to alt unit
        TCSession session = (TCSession) AIFUtility.getCurrentApplication()
                .getSession();
        TCClassificationService classificationService = session
                .getClassificationService();
        
        ICSProperty[] convertedProperties = classificationService
                .convertAttributeValues(attributeIDs, convertToUnits, values,
                        currentUnits, formates);
        Map<Integer, String> convertedAttributeIDtoValueMap = new HashMap<Integer, String>();
        // fill the converted attribute ID-Value map
        for (ICSProperty icsProp : convertedProperties)
        {
            convertedAttributeIDtoValueMap.put(icsProp.getId(),
                    icsProp.getValue());
        }
        
        // initialize attributeNameValue map
        prepareAttributeNameValue(icsPropDesc, unitSys,
                currentAttributeIDtoValueMap, convertedAttributeIDtoValueMap);
    }
    
    private void prepareAttributeNameValue(
            ICSPropertyDescription[] icsPropDesc, int unitSys,
            Map<Integer, String> currentAttributeIDtoValueMap,
            Map<Integer, String> convertedAttributeIDtoValueMap)
    {
        for (ICSPropertyDescription propDescriptor : icsPropDesc)
        {
            String attributeName = propDescriptor.getName();
            int attributeID = propDescriptor.getId();
            
            String currentValue = getRealValue(
                    currentAttributeIDtoValueMap.get(attributeID),
                    propDescriptor);
            String convertedValue = getRealValue(
                    convertedAttributeIDtoValueMap.get(attributeID),
                    propDescriptor);
            
            String metricValue = "";
            String nonMetricValue = "";
            if (unitSys == 0)
            {
                metricValue = currentValue;
                nonMetricValue = convertedValue;
            }
            else
            {
                metricValue = convertedValue;
                nonMetricValue = currentValue;
            }
            
            String metricRowData = attributeName + " = " + metricValue;
            String nonMetricRowData = attributeName + " = " + nonMetricValue;
            
            // all attributes which has value should appear at top position,
            // empty values at bottom
            if (metricValue.isEmpty() && nonMetricValue.isEmpty())
            {
                // if empty, add last
                m_metricAttributes.add(metricRowData);
                m_nonMetricAttributes.add(nonMetricRowData);
            }
            else
            {
                // if has value, add front
                m_metricAttributes.add(0, metricRowData);
                m_nonMetricAttributes.add(0, nonMetricRowData);
            }
            
        }
    }
    
    private String getRealValue(String attrValue,
            ICSPropertyDescription propDesc)
    {
        // 1.0 Get the actual value if the attribue is LOV
        ICSKeyLov keyLOV = propDesc.getFormat().getKeyLov();
        if (keyLOV != null)
        {
            attrValue = keyLOV.getValueOfKey(attrValue);
        }
        
        // 2.0 Removes zeros after decimal place
        String attrDatatype = propDesc.getFormat().getDisplayString();
        if (attrDatatype.contains("INTEGER") || attrDatatype.contains("REAL")
                || attrDatatype.contains("DOUBLE"))
        {
            attrValue = new BigDecimal(attrValue).stripTrailingZeros()
                    .toPlainString();
        }
        
        // 3.0 Append annotation
        attrValue = propDesc.getAnnotation() == null ? attrValue : propDesc
                .getAnnotation() + " " + attrValue;
        
        return attrValue.trim();
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
}
