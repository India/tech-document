package com.nov.rac.form.impl.supersedeform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.AttachmentsPanel;
import com.teamcenter.rac.util.Registry;

/**
 * @author mishalt
 * 
 */
public class SupersedeAttachmentsPanel extends AttachmentsPanel
{
    public SupersedeAttachmentsPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
