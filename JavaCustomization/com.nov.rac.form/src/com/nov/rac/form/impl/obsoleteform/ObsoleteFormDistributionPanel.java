package com.nov.rac.form.impl.obsoleteform;

import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.impl.common.DistributionPanel;
import com.teamcenter.rac.util.Registry;

public class ObsoleteFormDistributionPanel extends DistributionPanel 
{

	public ObsoleteFormDistributionPanel(Composite parent, int style) {
		super(parent, style);
		setRegistry(Registry.getRegistry("com.nov.rac.form.impl.obsoleteform.obsoleteform"));
	}
	
}
