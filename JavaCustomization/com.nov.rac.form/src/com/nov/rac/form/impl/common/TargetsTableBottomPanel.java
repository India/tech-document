package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class TargetsTableBottomPanel extends AbstractUIPanel 
{
	private Registry m_registry;
	
	protected Button m_compareRevButton;
	
	protected Button m_viewRddPdfButton;

	public TargetsTableBottomPanel(Composite parent, int style) 
	{
		super(parent, style);
    }
    
	public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(2, false));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_composite.setLayoutData(gd_panel);
        
        createTargetsTableBottomRow(l_composite);
    	
    	return true;
    }

	private void createTargetsTableBottomRow(Composite l_composite) 
	{
		createCompareRevButton(l_composite);
		createViewRddPdfButton(l_composite);
	}
	
	private void createCompareRevButton(Composite composite)
    {   
        m_compareRevButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1);
        m_compareRevButton.setText(m_registry.getString("compareRevButton.NAME",
                "Key Not Found"));
        
        m_compareRevButton.setLayoutData(gridData);
    }
	
	private void createViewRddPdfButton(Composite composite)
    {   
        m_viewRddPdfButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        m_viewRddPdfButton.setText(m_registry.getString("viewRddPdfButton.NAME",
                "Key Not Found"));
        
        m_viewRddPdfButton.setLayoutData(gridData);
    }

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}

}
