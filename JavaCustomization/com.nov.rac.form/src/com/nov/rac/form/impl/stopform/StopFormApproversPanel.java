package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.Signatures;
import com.teamcenter.rac.util.Registry;

public class StopFormApproversPanel extends Signatures
{

    public StopFormApproversPanel(Composite parent, int style)
    {
        super(parent, style);

        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}