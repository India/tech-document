package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocumentDetailsPanel extends AbstractExpandableUIPanel
        implements INOVFormLoadSave, ISubscriber

{
    
    private Registry m_registry;
    private ContainerTopBottomDocsPanel m_propsPanel;
    private EditButtonPanel m_editButtonPanel;
    private DocumentsDetailsRevisionComboPanel m_comboPanel;
    
    public DocumentDetailsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
       
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        forceLayout(getExpandableComposite());   
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_propsPanel.save(propMap);
        return false;
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        m_comboPanel = new DocumentsDetailsRevisionComboPanel(composite,
                SWT.NONE);
        m_comboPanel.createUI();
        m_editButtonPanel = new EditButtonPanel(composite, SWT.NONE);
        m_editButtonPanel.setDocRevSelectionChangeEventName(GlobalConstant.DOCS_REVISION_SELECTION_CHANGED);
        m_editButtonPanel.createUI();
        m_propsPanel = new ContainerTopBottomDocsPanel(composite, SWT.NONE);
        m_propsPanel.setDocRevSelectionChangeEventName(GlobalConstant.DOCS_REVISION_SELECTION_CHANGED);
        m_propsPanel.createUI();
        registerSubscriber();
        return false;
    }
    
    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        Display.getDefault().syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (!getComposite().isDisposed())
                {
                    Object parentPublisher = getParentCTab(((AbstractUIPanel) event
                            .getSource()).getComposite());
                    Object parentSubscriber = getParentCTab(getComposite());
                    if (parentPublisher == parentSubscriber)
                    {
                        if (event.getPropertyName().equalsIgnoreCase(
                                GlobalConstant.DOCUMENT_PUBLISH_EVENT))
                        {
                            try
                            {
                                TCComponent document = (TCComponent) event
                                        .getNewValue();
                                getExpandableComposite().setText(
                                        document.getProperty("object_string"));
                                forceLayout(getComposite());
                            }
                            catch (TCException e)
                            {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        
                        else if (event.getPropertyName().equalsIgnoreCase(
                                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED))
                        
                        {
                            setEnabled(true);
                        }
                      
                    }
                    if (event.getPropertyName().equalsIgnoreCase(
                            GlobalConstant.EVENT_SAVE_SUCCESS_FLAG)||event.getPropertyName().equalsIgnoreCase(
                                    GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED))
                    
                    {
                        setEnabled(false);
                    }
                }
                
            }
        });
        
    }
    
    private CTabFolder getParentCTab(Composite panel)
    {
        Object parent = null;
        if (panel.getParent() == null)
        {
            return null;
        }
        parent = panel.getParent();
        if (parent instanceof CTabFolder)
        {
            return (CTabFolder) parent;
        }
        else
        {
            return getParentCTab((Composite) parent);
        }
        
    }
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.DOCUMENT_PUBLISH_EVENT, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.DOCUMENT_PUBLISH_EVENT, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_propsPanel.setEnabled(flag);
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    private void forceLayout(Composite parent)
    {
        Control[] controls = parent.getChildren();
        for (Control control : controls)
        {
            
            if (control instanceof Composite)
            {
                forceLayout((Composite) control);
            }
            
        }
        parent.layout();
    }
    
}
