package com.nov.rac.form.impl.enform;

import java.awt.Color;
import java.awt.Component;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.dialog.ManufactuerDetailsDialog;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.manufacturerhelper.ManufacturingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class ManufacturingDetailsPanel extends AbstractUIPanel implements
        INOVFormLoadSave, ISubscriber,IPublisher
{
    
    private Registry m_registry;
    private TCTable m_manufacturerTCTable;
    private Composite m_addRemoveButtonComposite;
    private Button m_addButton;
    private Button m_removeButton;
    private TCComponent m_revision = null;
    private Set<Object> m_errorLabels = new HashSet<Object>();
    
    public ManufacturingDetailsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        // TODO Auto-generated constructor stub
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_revision = propMap.getComponent();
        m_manufacturerTCTable.removeAllRows();
        IPropertyMap[] itemMasterRevPropMaps = propMap
                .getCompoundArray("IMAN_master_form_rev");
        IPropertyMap itemMasterRevPropMap = itemMasterRevPropMaps[0];
        
        String[] mfgIds = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getStringArray("nov4_manufacturer_id"));
        
        String[] mfg_PartNumbers = PropertyMapHelper
                .handleNull(itemMasterRevPropMap
                        .getStringArray("manufacturer_partnumber"));
        
        ManufacturingHelper mfgHelper = ManufacturingHelper.getInstance();
        String[] mfgNames = mfgHelper.getMfgNames(mfgIds);
        
        // ask a better way to populate tables
        for (int inx = 0; inx < mfgIds.length; inx++)
        {
            Vector<String> addToTable = new Vector<String>();
            addToTable.add(mfgNames[inx]);
            addToTable.add(mfg_PartNumbers[inx]);
            m_manufacturerTCTable.addRow(addToTable);
        }
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        
        int rowCount = m_manufacturerTCTable.getRowCount();
        int mfgPartNumberIndex = m_manufacturerTCTable
                .getColumnIndex("manufacturer_partnumber");
        int mfgNameIndex = m_manufacturerTCTable
                .getColumnIndex("nov4_manufacturer_id");
        String[] mfgId = new String[rowCount];
        String[] mfgPartNumber = new String[rowCount];
        ManufacturingHelper mfgHelper = ManufacturingHelper.getInstance();
        for (int inx = 0; inx < rowCount; inx++)
        {
            String mfgName = (String) m_manufacturerTCTable.getValueAt(inx,
                    mfgNameIndex);
            mfgId[inx] = mfgHelper.getMFG_NameIdMapping().get(mfgName);
            mfgPartNumber[inx] = (String) m_manufacturerTCTable.getValueAt(inx,
                    mfgPartNumberIndex);
        }
        IPropertyMap itemMasterRevPropMap = propMap
                .getCompound("IMAN_master_form_rev");
        itemMasterRevPropMap.setStringArray("nov4_manufacturer_id", mfgId);
        itemMasterRevPropMap.setStringArray("manufacturer_partnumber",
                mfgPartNumber);
        
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_addButton.setEnabled(flag);
        m_removeButton.setEnabled(flag);
        m_manufacturerTCTable.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        
        CLabel manufacturerLabel = new CLabel(composite, SWT.NONE);
        manufacturerLabel
                .setText(getRegistry().getString("Manufacturer.Label"));
        SWTUIHelper.setFont(manufacturerLabel, SWT.BOLD);
        manufacturerLabel.setImage(new Image(getComposite().getDisplay(),
                    m_registry.getImage("Manufacturer.IMAGE"), SWT.NONE));
        
        manufacturerLabel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
                false, 1, 1));
        createAddRemoveButton(composite);
        createManufacturerTCTable(composite);
        addRenderer();
        hideColumns();
        setEnabled(false);
        addButtonListeners();
        registerSubscriber();
        createDataModelMapping();
        return false;
    }
    
    private void addRenderer()
    {
        int colIndex = m_manufacturerTCTable
                .getColumnIndex("manufacturer_partnumber");
        TableColumn column = m_manufacturerTCTable.getColumnModel().getColumn(
                colIndex);
        
        column.setCellRenderer(new DefaultTableCellRenderer()
        {
            
            @Override
            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int col)
            {
                JLabel label = (JLabel) super.getTableCellRendererComponent(
                        table, value, isSelected, hasFocus, row, col);
                
                int partNoColumnIndex = TableUtils.getColumnIndex(
                        "manufacturer_partnumber", (AIFTable) table);
                if (partNoColumnIndex != -1)
                {
                    String partNumber = (String) table.getModel().getValueAt(
                            row, partNoColumnIndex);
                    if (partNumber == null || partNumber.equals(" ") || partNumber.trim().equals(""))
                    {
                        // label.setBackground(new Color(252,190,200));
                        Border border = BorderFactory.createLineBorder(
                                Color.RED, 3);
                        label.setBorder(border);
                        m_errorLabels.add(label);
                        
                        SWTUIUtilities.asyncExec(new Runnable()
                        {
                            
                            @Override
                            public void run()
                            {
                                PublishEvent publishEvent = getPublishEvent(
                                        GlobalConstant.EVENT_VALIDATE_PROPERTIES_ERRORS, null, null);
                                
                                getController().publish(publishEvent);
                                
                            }
                        });
                    }
                    else
                    {
                        if(!m_errorLabels.isEmpty() && m_errorLabels.contains(label))
                        {
                            m_errorLabels.remove(label);
                            SWTUIUtilities.asyncExec(new Runnable()
                            {
                                
                                @Override
                                public void run()
                                {
                                    PublishEvent publishEvent = getPublishEvent(
                                            GlobalConstant.EVENT_VALIDATE_PROPERTIES_SUCCESS, null, null);
                                    
                                    getController().publish(publishEvent);
                                    
                                }
                            });
                        }
                    }
                }
                
                return label;
            }
        });
        
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    private void addButtonListeners()
    {
        m_removeButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        try
                        {
                            validateTableRowSelection();
                            if (confirmRemoval())
                            {
                                deleteManufacturer();
                            }
                        }
                        catch (TCException e)
                        {
                            MessageBox.post(e.getMessage(), "ERROR",
                                    MessageBox.ERROR);
                        }
                    }
                });
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
        
        m_addButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        addManufactuerer();
                        
                    }
                });
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
    }
    
    private void deleteManufacturer()
    {
        int[] rows = m_manufacturerTCTable.getSelectedRows();
        if (rows.length > 0)
        {
            for (int row : rows)
            {
                m_manufacturerTCTable.removeRow(row);
            }
        }
    }
    
    private void addManufactuerer()
    {
        Shell shell = m_addButton.getShell();
        ManufactuerDetailsDialog popupDialog = new ManufactuerDetailsDialog(
                shell, SWT.RESIZE);
        popupDialog.setLocationReferenceControl(m_addButton);
        popupDialog.open();
    }
    
    private void createAddRemoveButton(Composite composite)
    {
        m_addRemoveButtonComposite = new Composite(composite, SWT.NONE);
        GridLayout layout = new GridLayout(2, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        m_addRemoveButtonComposite.setLayout(layout);
        m_addRemoveButtonComposite.setLayoutData(new GridData(SWT.RIGHT,
                SWT.CENTER, true, false, 1, 1));
        createRemoveButton(m_addRemoveButtonComposite);
        createAddButton(m_addRemoveButtonComposite);
        setAddRemoveButtonImages();
    }
    
    private void setAddRemoveButtonImages()
    {
        Image minusButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        Image pluseButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_addButton.setImage(pluseButtonImage);
        m_removeButton.setImage(minusButtonImage);
    }
    
    private void createAddButton(Composite composite)
    {
        m_addButton = new Button(composite, SWT.NONE);
        GridData gdAddBtn = new GridData(SWT.FILL, SWT.CENTER, false, false, 1,
                1);
        m_addButton.setLayoutData(gdAddBtn);
    }
    
    private void createRemoveButton(Composite composite)
    {
        m_removeButton = new Button(composite, SWT.NONE);
        GridData gdRemoveBtn = new GridData(SWT.FILL, SWT.CENTER, false, false,
                1, 1);
        m_removeButton.setLayoutData(gdRemoveBtn);
    }
    
    private Composite getMainComposite()
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(2, true);
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void createManufacturerTCTable(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, 2, 1);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        String[] propertiesToDisplay = getRegistry().getStringArray(
                "ManufacturingTable.PropertyNames");
        String[] columnNames = getRegistry().getStringArray(
                "ManufacturingTable.ColumnNames");
        
        m_manufacturerTCTable = new TCTable(propertiesToDisplay, columnNames)
        {
            @Override
            public boolean isCellEditable(int i, int j)
            {
                int colIndex = m_manufacturerTCTable
                        .getColumnIndex("manufacturer_partnumber");
                return j == colIndex;
            }
        };
        
        m_manufacturerTCTable.setShowVerticalLines(true);
        m_manufacturerTCTable
                .setAutoResizeMode(m_manufacturerTCTable.AUTO_RESIZE_ALL_COLUMNS);
        m_manufacturerTCTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrollPane = new JScrollPane(m_manufacturerTCTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    private void hideColumns()
    {
        // hide columns
        String[] hideColumnNames = getRegistry().getStringArray(
                "ManufacturingTable.hideColumns");
        int colIndex = -1;
        for (String colName : hideColumnNames)
        {
            colIndex = m_manufacturerTCTable.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_manufacturerTCTable, colIndex);
        }
    }
    
    private void validateTableRowSelection() throws TCException
    {
        if (m_manufacturerTCTable != null
                && m_manufacturerTCTable.getSelectedRowCount() < 1)
        {
            String[] errors = new String[] { getRegistry().getString(
                    "NoItemSelected.MSG") };
            TCException exception = new TCException(errors);
            
            throw exception;
        }
    }
    
    private boolean confirmRemoval()
    {
        boolean response = MessageDialog.openQuestion(
                m_removeButton.getShell(),
                getRegistry().getString("Confirm.Label"), getRegistry()
                        .getString("RemovalConfirm.Msg"));
        return response;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (!getComposite().isDisposed())
        {
            Object thisParent = getComposite().getParent().getParent();
            Object sourceParent = null;
            
            IPublisher eventPublisher = event.getSource();
            
            if (eventPublisher instanceof AbstractUIPanel)
            {
                Composite sourceComposite = ((AbstractUIPanel) eventPublisher)
                        .getComposite();
                sourceParent = sourceComposite.getParent();
            }
            
            if (event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_EDIT_BUTTON_SELECTED)
                    && sourceParent != null && thisParent.equals(sourceParent))
            {
                onEdit();
            }
            
            if (event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))
            {
                boolean success = (boolean) event.getNewValue();
                if (success)
                {
                    onSave();
                }
                
            }
            
            if (event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.MFG_DETAILS_EVENT)
                    && FormHelper.isLatestItemRevision(m_revision))
            {
                AIFTableLine[] rows = (AIFTableLine[]) event.getNewValue();
                if (rows != null && rows.length > 0)
                {
                    m_manufacturerTCTable.addRow(rows);
                }
            }
        }
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, this);
        
        controller.registerSubscriber(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG,
                this);
        controller.registerSubscriber(GlobalConstant.MFG_DETAILS_EVENT, this);
    }
    
    private void unregisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, this);
        
        controller.unregisterSubscriber(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG,
                this);
        controller.unregisterSubscriber(GlobalConstant.MFG_DETAILS_EVENT, this);
    }
    
    private void onEdit()
    {
        setEnabled(true);
    }
    
    private void onSave()
    {
        setEnabled(false);
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        
        dataBindingHelper.bindData(m_manufacturerTCTable, "nov4_manufacturer_id", 
                FormHelper.getDialogDataModelName());
        
    }
}
