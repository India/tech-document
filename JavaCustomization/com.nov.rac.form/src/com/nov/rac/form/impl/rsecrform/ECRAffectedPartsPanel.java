package com.nov.rac.form.impl.rsecrform;

import javax.swing.table.TableColumn;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TargetsTablePanel;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECRAffectedPartsPanel extends TargetsTablePanel
{
    
    private Registry m_registry;

    public ECRAffectedPartsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        setRegistry(m_registry);
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] affectedPartsMap = propMap.getCompoundArray("nov4_ecr_affected_parts");
        getTargetsTable().removeAllRows();
        TableUtils.populateTable(affectedPartsMap, getTargetsTable());
        return true;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        
        int colIndex = getTargetsTable().getColumnIndex("object_type");
        TableUtils.hideTableColumn(getTargetsTable(), colIndex);
        
        // set icon renderer
        colIndex = getTargetsTable().getColumnIndex("object_name");
        TableColumn nameColumn = getTargetsTable().getColumnModel().getColumn(colIndex);
        nameColumn.setCellRenderer(new PartIDIconRenderer());
        
        return true;
    }
    
}
