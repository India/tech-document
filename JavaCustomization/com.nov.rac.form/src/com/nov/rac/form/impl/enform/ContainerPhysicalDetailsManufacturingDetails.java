package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class ContainerPhysicalDetailsManufacturingDetails extends AbstractUIPanel implements
INOVFormLoadSave
{
   
    private static final int NO_OF_COLUMNS_MAIN_COMPOSITE = 3;
    private static final int NO_OF_GRIDS_PHYSICAL_DETAILS = 2;
    private PhysicalDetailsPanel m_physicalDetailsPanel;
    private ManufacturingDetailsPanel m_manufacturingDetailsPanel;
    
    public ContainerPhysicalDetailsManufacturingDetails(Composite parent,
            int style)
    {
        super(parent, style);
        m_physicalDetailsPanel = new PhysicalDetailsPanel(getComposite(), style);
        m_manufacturingDetailsPanel = new ManufacturingDetailsPanel(getComposite(), style);
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_physicalDetailsPanel.load(propMap);
        m_manufacturingDetailsPanel.load(propMap);
        
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
    	m_physicalDetailsPanel.save(propMap);
    	m_manufacturingDetailsPanel.save(propMap);
    	
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
    	m_physicalDetailsPanel.setEnabled(flag);
    	m_manufacturingDetailsPanel.setEnabled(flag);
        
    }

    @Override
    public boolean createUI() throws TCException
    {
        Composite composite =getComposite();
        GridLayout gl_composite =new GridLayout(NO_OF_COLUMNS_MAIN_COMPOSITE,true);
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        m_physicalDetailsPanel.createUI();
        m_physicalDetailsPanel.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, NO_OF_GRIDS_PHYSICAL_DETAILS,
                1));
        
        m_manufacturingDetailsPanel.createUI();
        m_manufacturingDetailsPanel.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1));
        
        return false;
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
   
}
