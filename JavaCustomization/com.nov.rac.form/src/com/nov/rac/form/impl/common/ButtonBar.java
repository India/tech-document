package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ButtonBar extends AbstractUIPanel 
{
	private Registry m_registry;
	
	private Composite m_parent;
	
	protected Button m_saveButton;
	
	protected Button m_cancelButton;

	public ButtonBar(Composite parent, int style) 
	{
		super(parent, style);
		m_registry = Registry.getRegistry(this);
        m_parent = parent;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(2, false));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        l_composite.setLayoutData(gd_panel);
        
        createBottoms(l_composite);
        
        addCancelButtonListener();
    	
    	return true;
    }

	private void createBottoms(Composite l_composite) 
	{
		createSaveButton(l_composite);
		createCancelButton(l_composite);
	}
	
	private void createSaveButton(Composite composite)
    {   
		m_saveButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1);
        m_saveButton.setText(m_registry.getString("saveButton.NAME",
                "Key Not Found"));
        
        m_saveButton.setLayoutData(gridData);
    }
	
	private void createCancelButton(Composite composite)
    {   
		m_cancelButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        m_cancelButton.setText(m_registry.getString("cancelButton.NAME",
                "Key Not Found"));
        
        m_cancelButton.setLayoutData(gridData);
    }
	
	private void addCancelButtonListener()
    {
        m_cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent se)
            {
            	m_parent.dispose();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent se)
            {
                
            }
        });
    }

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}

}