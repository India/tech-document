package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class ContainerAlternateIdsSecondaryForms extends AbstractUIPanel implements
INOVFormLoadSave
{
    private AlternateIDsPanel m_alternateIdsPanel ;
    private SecondaryFormsPanel m_secondaryFormsPanel;
    private static final int NO_OF_COLUMNS_MAIN_COMPOSITE = 3;
    private static final int NO_OF_GRIDS_ALTERNATE_IDS = 2;
    
    public ContainerAlternateIdsSecondaryForms(Composite parent, int style)
    {
        super(parent, style);
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_alternateIdsPanel.load(propMap);
        m_secondaryFormsPanel.load(propMap);
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_alternateIdsPanel.save(propMap);
        m_secondaryFormsPanel.save(propMap);
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        m_alternateIdsPanel.setEnabled(flag);
        m_secondaryFormsPanel.setEnabled(flag);
        
    }

    @Override
    public boolean createUI() throws TCException
    {
        Composite composite =getComposite();
        GridLayout gl_composite =new GridLayout(NO_OF_COLUMNS_MAIN_COMPOSITE,true);
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        m_alternateIdsPanel = new AlternateIDsPanel(getComposite(), SWT.NONE);
        m_secondaryFormsPanel = new SecondaryFormsPanel(getComposite(), SWT.NONE);
        
        m_alternateIdsPanel.createUI();
        m_alternateIdsPanel.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, NO_OF_GRIDS_ALTERNATE_IDS,
                1));
        
        m_secondaryFormsPanel.createUI();
        m_secondaryFormsPanel.getComposite().setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1,
                1));
     
        return false;
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
  
    
   
    
    public void dispose()
    {
        super.dispose();
    }
    

    
}
