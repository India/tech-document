package com.nov.rac.form.impl.common;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.SwingUtilities;

import org.eclipse.core.runtime.Preferences.IPropertyChangeListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.DualList;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.interfaces.IAddressBook;
import com.nov.rac.interfaces.IPropertyChangeNotifier;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.addressbook.dialog.SimpleAddressBookDialog;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

/**
 * @author Vineela
 * 
 */
public class DistributionPanel extends AbstractExpandableUIPanel implements INOVFormLoadSave, PropertyChangeListener
{
    
    private Registry m_registry = null;
    protected Text m_emailText = null;
    protected Button m_addEmailButton = null;
    protected Button m_globalAddressButton = null;
    protected DualList m_dualList = null;
    
    public DistributionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(2, true));
        getExpandableComposite().setText(getRegistry().getString("DistibutionPanel.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        createDualList(l_composite);
        createEmailTextField(l_composite);
        addButtonListener();
        createDataModelMapping();
        
        return true;
    }
    
    private void createDualList(Composite composite)
    {
        m_dualList = new DualList(composite, getRegistry());
        m_dualList.createUI();
    }
    
    private void addButtonListener()
    {
        m_globalAddressButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
            	 try
                 {
                     String viewerClass = getRegistry().getString("Addressbook.CLASS");
                     Object[] params = new Object[1];
                     params[0] = AIFDesktop.getActiveDesktop().getShell();
                     
                     IAddressBook addressBookDialog = (IAddressBook) Instancer.newInstanceEx(viewerClass, params);
                     
                     addressBookDialog.setButtonsForDialog(SimpleAddressBookDialog.OK_BUTTON_ID
                             | SimpleAddressBookDialog.APPLY_BUTTON_ID | SimpleAddressBookDialog.CANCEL_BUTTON_ID);
                     
                     if (addressBookDialog instanceof IPropertyChangeNotifier)
                     {
                         ((IPropertyChangeNotifier) addressBookDialog).addPropertyChangeListener(DistributionPanel.this);
                     }
                     
                     addressBookDialog.open();
                     
                 }
                 catch (Exception e)
                 {
                     e.printStackTrace();
                 }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
        
        m_addEmailButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                addEmailAddress();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }
    
    protected void addEmailAddress()
    {
        String emailAddress = m_emailText.getText();
        if (FormHelper.isValidEmail(emailAddress))
        {
            m_dualList.addToTargetTable(emailAddress);
        }
        else
        {
            MessageBox.post(getRegistry().getString("emailIdFormat.MSG") + " : " + emailAddress, getRegistry()
                    .getString("info.TITLE"), MessageBox.INFORMATION);
        }
    }
    
    protected void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_dualList.getTargetTable().dataModel, getRegistry().getString("Distribution.NAME"),
                FormHelper.getDataModelName());
    }
    
    protected void createEmailTextField(Composite composite)
    {
        Composite subComposite = new Composite(composite, SWT.NONE);
        subComposite.setLayout(new GridLayout(20, true));
        subComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 2, 1));
        Label emptyLabel = new Label(subComposite, SWT.NONE);
        emptyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 11, 1));
        m_emailText = new Text(subComposite, SWT.BORDER);
        m_emailText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 7, 1));
        m_emailText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        createAddEmailButton(subComposite);
        
        createGlobalAddressButton(subComposite);
        
    }
    
    protected void createAddEmailButton(Composite composite)
    {
        m_addEmailButton = new Button(composite, SWT.NONE);
        Image addEmailButtonImage = new Image(composite.getDisplay(), getRegistry().getImage("addButton.IMAGE"),
                SWT.NONE);
        m_addEmailButton.setImage(addEmailButtonImage);
    }
    
    protected void createGlobalAddressButton(Composite composite)
    {
        m_globalAddressButton = new Button(composite, SWT.NONE);
        Image addressButtonImage = new Image(composite.getDisplay(), getRegistry()
                .getImage("globalAddressButton.IMAGE"), SWT.NONE);
        m_globalAddressButton.setImage(addressButtonImage);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String[] targets = PropertyMapHelper.handleNullForArray(propMap.getStringArray(getRegistry().getString(
                "distribution.PROP")));
        TCTable targetTable = m_dualList.getTargetTable();
        for (int inx = 0; inx < targets.length; inx++)
        {
            java.util.List<String> tableRow = new java.util.ArrayList<String>();
            tableRow.add(targets[inx]);
            targetTable.addRow(tableRow);
        }
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        TCTable targetTable = m_dualList.getTargetTable();
        int rowCount = targetTable.getRowCount();
        String[] allValues_TargetTable = new String[rowCount];
        for (int inx = 0; inx < rowCount; inx++)
        {
            allValues_TargetTable[inx] = (String) targetTable.getValueAt(inx, 0);
        }
        propMap.setStringArray(getRegistry().getString("distribution.PROP"), allValues_TargetTable);
        return true;
        
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_dualList.setEnabled(flag);
        m_addEmailButton.setEnabled(flag);
        m_globalAddressButton.setEnabled(flag);
        m_emailText.setEnabled(flag);
    }
    
    private void setSelectedIdToList(String[] selectedNames)
    {
        if (selectedNames != null && selectedNames.length > 0)
        {
            m_dualList.addToTargetTable(selectedNames[0]);
        }
    }
     

    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        if(event.getPropertyName().equalsIgnoreCase(SimpleAddressBookDialog.ADDRESS_BOOK_MAIL_IDS_SELECTED))
        {
            String[] selectedIds = (String[]) event.getNewValue();
            setSelectedIdToList(selectedIds);
        }
        
    }
    
}
