package com.nov.rac.form.impl.enform;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class PartPropertiesHeaderPanel extends AbstractUIPanel implements
        INOVFormLoadSave, IPublisher, ISubscriber
{
    
    private static final int NO_OF_COLUMNS_MAINCOMPOSITE = 2;
    private Registry m_registry;
    private CLabel m_nameLabel;
    private Button m_editButton = null;
    // private Button m_saveButton = null;
    private TCComponent m_tcComponent;
    //private Button m_feedbackButton;
    private Button m_cancelButton = null;
    
    public PartPropertiesHeaderPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(NO_OF_COLUMNS_MAINCOMPOSITE,
                true);
        l_composite.setLayout(gl_l_composite);
        
        createPartPropertiesField(l_composite);
        createButtonBar(l_composite);
        
        addButtonListeners();
        
        registerSubscibers();
        
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_tcComponent = propMap.getComponent();
        makeLatestRevisionEditable(m_tcComponent);
        m_cancelButton.setEnabled(false);  
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    private void addButtonListeners()
    {
        m_editButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                editButtonListener();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
        
        m_cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
                cancelButtonListener();
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
    }
    
    private void editButtonListener()
    {
        m_editButton.setEnabled(false);
        m_cancelButton.setEnabled(true);
        
        PublishEvent publishEvent = getPublishEvent(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, null, null);
        getController().publish(publishEvent);
    }
    
    private void cancelButtonListener()
    {
        m_editButton.setEnabled(true);
        m_cancelButton.setEnabled(false);
        PublishEvent publishEvent = getPublishEvent(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, null, null);
        getController().publish(publishEvent);
    }
    
    private void createButtonBar(Composite l_composite)
    {
        Composite buttonComposite = new Composite(l_composite, SWT.RIGHT);
        GridData buttonCompositgridData = new GridData(SWT.RIGHT, SWT.CENTER,
                true, true, 1, 1);
        buttonComposite.setLayoutData(buttonCompositgridData);
        GridLayout gl_buttonComposite = new GridLayout(3, false);
        buttonComposite.setLayout(gl_buttonComposite);
       
        new FeedBackButtonComposite(buttonComposite, SWT.NONE);       
        
        m_editButton = new Button(buttonComposite, SWT.NONE);
        m_editButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false,
                false, 1, 1));
        m_editButton.setText(m_registry.getString("EditButton.Lable"));
        m_editButton.setEnabled(true);
        
        m_cancelButton = new Button(buttonComposite, SWT.NONE);
        m_cancelButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER,
                false, false, 1, 1));
        m_cancelButton.setText(m_registry.getString("CancelButton.Label"));
        
        
    }
    
    private void createPartPropertiesField(Composite l_composite)
    {
        m_nameLabel = new CLabel(l_composite, SWT.NONE);
        SWTUIHelper.setFont(m_nameLabel, SWT.BOLD);
        m_nameLabel.setText("Part Properties");
        m_nameLabel.setImage(new Image(getComposite().getDisplay(),
                m_registry.getImage("Details.IMAGE"), SWT.NONE));
         m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
                false, 1, 1));
         
    }
    
    protected void makeLatestRevisionEditable(TCComponent tcComponent)
    {
        if (!FormHelper.isLatestItemRevision(tcComponent)
                || !FormHelper.hasWriteAccess(tcComponent))
        {
            m_editButton.setEnabled(false);
        }
        else
        {
            m_editButton.setEnabled(true);
        }
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public void update(PublishEvent event)
    {
        
        if (event.getPropertyName().equalsIgnoreCase(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))
        {
            boolean success = (boolean) event.getNewValue();
            if (success)
            {
                onSave();
            }
        }
        
    }
    
    private void onSave()
    {
        if (FormHelper.isLatestItemRevision(m_tcComponent))
        {
            if(!m_editButton.isDisposed())
            {
                m_editButton.setEnabled(true);
            }
            
            if(!m_cancelButton.isDisposed())
            {
                m_cancelButton.setEnabled(false);
            }
        }
    }
    
    private void registerSubscibers()
    {
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_REVISION_SELECTION_CHANGED,
                getSubscriber());
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_REVISION_SELECTION_CHANGED,
                getSubscriber());
        
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
}
