package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RelatedFormsTablePanel extends FormsTablePanel 
{

    public RelatedFormsTablePanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        GridLayout gl_composite = new GridLayout(1, true);
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        createPlusMinusButtons(composite);
        createRelatedFormsTCTable(composite);
       
        return true;
    }

    private void createPlusMinusButtons(Composite parent)
    {
        Composite composite = new Composite(parent,SWT.NONE);
        GridLayout gl_composite = new GridLayout(1, false);
        gl_composite.marginWidth =0;
        gl_composite.marginHeight = 0;
        gl_composite.verticalSpacing = 0;
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        m_panelNameLabel = new CLabel(composite, SWT.NONE);
        m_panelNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1));
        m_panelNameLabel.setText(getRegistry().getString("RelatedForms.Label"));
        SWTUIHelper.setFont(m_panelNameLabel, SWT.BOLD);
        m_panelNameLabel.setImage(TCTypeRenderer.getTypeImage("Form", null));
        
        
        //Only place holder added till now. waiting on harshal for this.
//        Button plusButton = new Button(composite,SWT.NONE);
//        plusButton.setText("+");
//        Button minusButton = new Button(composite,SWT.NONE);
//        minusButton.setText("-");
       
        
        
    }
}
