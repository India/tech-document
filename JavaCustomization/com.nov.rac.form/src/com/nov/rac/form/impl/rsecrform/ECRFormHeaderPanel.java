package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.FormHeaderPanel;
import com.teamcenter.rac.util.Registry;

public class ECRFormHeaderPanel extends FormHeaderPanel
{
    
private Registry m_registry = null;
    
    public ECRFormHeaderPanel(Composite parent, int style) 
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        setRegistry(m_registry);
    }
    
      
}
