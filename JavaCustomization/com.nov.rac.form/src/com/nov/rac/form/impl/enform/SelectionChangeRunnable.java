package com.nov.rac.form.impl.enform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCException;

public class SelectionChangeRunnable implements Runnable
{
    
    private Composite m_source;
    private AbstractUIPanel m_child;
    private IPropertyMap m_propMap;
    
    public SelectionChangeRunnable(Composite theSource, AbstractUIPanel theChild,
            IPropertyMap propMap)
    {
        m_source = theSource;
        m_child = theChild;
        m_propMap = propMap;
    }
    
    @Override
    public void run()
    {
        if (!m_child.getComposite().isDisposed())
        {
            if (m_source == m_child.getComposite().getParent())
            {
                try
                {
                    if (m_child instanceof ILoadSave)
                    {
                        ((ILoadSave) m_child).load(m_propMap);
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
}
