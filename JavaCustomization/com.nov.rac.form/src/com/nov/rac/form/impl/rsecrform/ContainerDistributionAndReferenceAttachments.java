package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ContainerDistributionAndReferenceAttachments extends
        AbstractExpandableUIPanel implements INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private DistributionPanel m_distributionPanel = null;
    private ReferenceAttachmentsPanel m_refernceAttachmentsPanel = null;
    
    public ContainerDistributionAndReferenceAttachments(Composite parent,
            int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_distributionPanel.load(propMap);
        m_refernceAttachmentsPanel.load(propMap);
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_distributionPanel.setEnabled(flag);
        m_refernceAttachmentsPanel.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();        
        getExpandableComposite().setText(
                getRegistry().getString("DistributionAndRefAttachments.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
                
        m_distributionPanel = new DistributionPanel(composite, SWT.NONE);
        m_distributionPanel.createUI();
        
        m_refernceAttachmentsPanel = new ReferenceAttachmentsPanel(composite,
                SWT.NONE);
        m_refernceAttachmentsPanel.createUI();
        m_refernceAttachmentsPanel.getComposite().setLayoutData(
                new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
        return false;
    }
    
    private Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(3, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
