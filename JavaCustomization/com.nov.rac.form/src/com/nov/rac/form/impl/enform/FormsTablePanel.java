package com.nov.rac.form.impl.enform;

import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class FormsTablePanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private TCTable m_formsTable;
    protected CLabel m_panelNameLabel;
    
    public FormsTablePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_formsTable.removeAllRows();
        TCComponent[] relatedForms = propMap.getTagArray("RelatedECN");
        TableUtils.populateTable(relatedForms, m_formsTable);
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite composite = getComposite();
        GridLayout gl_composite = new GridLayout(1, true);
      
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        m_panelNameLabel = new CLabel(composite, SWT.NONE);
        m_panelNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1));
        createRelatedFormsTCTable(composite);
        return false;
    }
    
    
    
    protected void createRelatedFormsTCTable(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        GridLayout gl_tCTableComposite = new GridLayout (1,false);
       
        
        m_formsTable = createTCTable();
        JScrollPane scrollPane = new JScrollPane(m_formsTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    protected void setLabelText(String setText)
    {
        m_panelNameLabel.setText(setText);
        SWTUIHelper.setFont(m_panelNameLabel, SWT.BOLD);
        m_panelNameLabel.setImage(TCTypeRenderer.getTypeImage("Form", null));
    }
    
    private TCTable createTCTable()
    {
        String[] columnIdentifiers = getRegistry().getStringArray(
                "FormsTablePanel.ColumnIdentifier");
        String[] columnNames = getRegistry().getStringArray(
                "FormsTablePanel.ColumnNames");
        TCTable tCTable = new TCTable(columnIdentifiers, columnNames);
        tCTable.setShowVerticalLines(true);
        tCTable.setAutoResizeMode(tCTable.AUTO_RESIZE_ALL_COLUMNS);
        tCTable.getTableHeader().setVisible(false);
        tCTable.getTableHeader().setPreferredSize(new Dimension(-1, 0));
        tCTable.setEditable(false);
        
        // hide col
        int colIndex = tCTable.getColumnIndex(getRegistry().getString(
                "FormsTablePanel.ColumnsToHide"));
        TableUtils.hideTableColumn(tCTable, colIndex);
        
        // set icon renderer
        colIndex = tCTable.getColumnIndex(getRegistry().getString(
                "FormsTablePanel.ColumnsToRender"));
        TableColumn column = tCTable.getColumnModel().getColumn(colIndex);
        column.setCellRenderer(new PartIDIconRenderer());
        
        return tCTable;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    public void dispose()
    {
        super.dispose();
    }
    
}
