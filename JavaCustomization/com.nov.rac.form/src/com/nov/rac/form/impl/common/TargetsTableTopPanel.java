package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.quicksearch.export.NOVExportSearchResultUtils;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.table.export.ExportTableHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class TargetsTableTopPanel extends AbstractUIPanel
{
	private Registry m_registry;
	
	protected Button m_showTargetsTableButton;
	
	protected Button m_addButton;
	
	protected Button m_removeButton;
	
	protected Button m_exportExcelButton;

	public TargetsTableTopPanel(Composite parent, int style) 
	{
		super(parent, SWT.NONE);
    }
    
	public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(5, false));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_composite.setLayoutData(gd_panel);
        
        createTargetsTableTopRow(l_composite);
    	
    	return true;
    }

	protected void createTargetsTableTopRow(Composite composite) 
	{
		
		m_showTargetsTableButton = new Button(composite, SWT.NONE);
		GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        Image image = getRegistry().getImage(
                "TargetsTable.ShowTargetsTable.IMAGE");
        m_showTargetsTableButton.setImage(image);
        m_showTargetsTableButton.setLayoutData(gridData);
        m_showTargetsTableButton.setEnabled(false);
        
        Label l_lblTargetsTableName = new Label(composite, SWT.NONE);
        l_lblTargetsTableName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_lblTargetsTableName.setText(m_registry.getString("lblTargetsTableName.NAME",
                "Key Not Found"));
        SWTUIHelper.setFont(l_lblTargetsTableName, SWT.BOLD);
        createExportExcelButton(composite);
        
        createAddButton(composite);
        
        createRemoveButton(composite);
		
	}
	
	private void createExportExcelButton(Composite composite)
    {   
        m_exportExcelButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, false,
                true, 1, 1);
        m_exportExcelButton.setText(m_registry.getString("exportExcelButton.NAME",
                "Key Not Found"));
        
        m_exportExcelButton.setLayoutData(gridData);
    }
	
	protected void createAddButton(Composite composite)
    {
        m_addButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                true, 1, 1);
        
        Image addButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_addButton.setImage(addButtonImage);
        
        m_addButton.setLayoutData(gridData);
    }
	
	protected void createRemoveButton(Composite composite)
    {
        m_removeButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                true, 1, 1);
        
        Image addButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        m_removeButton.setImage(addButtonImage);
        
        m_removeButton.setLayoutData(gridData);
    }

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}
	protected void addButtonListener(SelectionListener listener)
	{
		m_addButton.addSelectionListener(listener);
	}
	protected void removeButtonListener(SelectionListener listener)
	{
		m_removeButton.addSelectionListener(listener);
	}

	public void addExportButtonListener(final AIFTable table)
	{
	    m_exportExcelButton.addSelectionListener(new SelectionListener()
        {

            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                ExportTableHelper.exportTableToExcel(composite.getShell(), table);
                
            }
        });

	}
}
