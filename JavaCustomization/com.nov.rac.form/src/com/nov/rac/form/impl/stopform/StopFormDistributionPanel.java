package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.helpers.FieldMandatoryValidator;
import com.nov.rac.form.helpers.LabelTextFieldMandatoryValidator;
import com.nov.rac.form.impl.common.DistributionPanel;
import com.nov.rac.form.util.DualList;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class StopFormDistributionPanel extends DistributionPanel
{

    public StopFormDistributionPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        FieldMandatoryValidator fieldValidator = new FieldMandatoryValidator();
        //UIHelper.makeMandatory(m_expandableComposite.getTextClient());
        getExpandableComposite().getTextClient();
        //UIHelper.makeMandatory(m_dualList.getTargetTable());
        return true;
    }
    
   
    
}
