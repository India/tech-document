package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ContainerTopBottomDocsPanel extends AbstractUIPanel implements
        INOVFormLoadSave, ISubscriber
{
    private DocumentDetailsTopPanel m_documentDetailsTopPanel;
    private DocumentDetailsBottomPanel m_documentDetailBottomPanel;
    private TCComponent m_selectedRev;
    private String m_docRevSelectionChangeEventName = "";
    private IPropertyMap m_selectedRevPropMap;
    private Registry m_registry;
    
    public ContainerTopBottomDocsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setDocRevSelectionChangeEventName(String name)
    {
        m_docRevSelectionChangeEventName = name;
    }
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        createHeaderLabel(composite);
        m_documentDetailsTopPanel = new DocumentDetailsTopPanel(composite,
                SWT.NONE);
        m_documentDetailsTopPanel.createUI();
        m_documentDetailBottomPanel = new DocumentDetailsBottomPanel(composite,
                SWT.NONE);
        m_documentDetailBottomPanel.createUI();
        registerSubscriber();
        return false;
    }
    
    private void createHeaderLabel(Composite parent)
    {
        CLabel label = new CLabel(parent, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1,
                1));
        label.setText(getRegistry().getString("DocumentsProperties.Label"));
        label.setImage(new Image(getComposite().getDisplay(),
                m_registry.getImage("Details.IMAGE"), SWT.NONE));
        
    }

    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        load(m_selectedRevPropMap);
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_selectedRev = propMap.getComponent();
        m_selectedRevPropMap =propMap;
        m_documentDetailsTopPanel.load(propMap);
        m_documentDetailBottomPanel.load(propMap);
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        if (m_selectedRev!=null && FormHelper.isLatestItemRevision(m_selectedRev))
        {
            m_documentDetailsTopPanel.save(propMap);
            m_documentDetailBottomPanel.save(propMap);
        }
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        
        Display.getDefault().syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (!getComposite().isDisposed())
                {
                    Object parentPublisher = getParentCTab(((AbstractUIPanel) event
                            .getSource()).getComposite());
                    Object parentSubscriber = getParentCTab(getComposite());
                    if (event.getPropertyName().equalsIgnoreCase(
                            m_docRevSelectionChangeEventName)
                            && parentPublisher == parentSubscriber)
                    {
                        try
                        {
                            load((IPropertyMap) event.getNewValue());
                        }
                        catch (TCException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    
                    if(event.getPropertyName().equalsIgnoreCase(
                            GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED)
                            && parentPublisher == parentSubscriber)
                    {
                        setEnabled(false);
                    }
                }
            }
        });
    }
    
    private CTabFolder getParentCTab(Composite panel)
    {
        Object parent = null;
        if (panel.getParent() == null)
        {
            return null;
        }
        parent = panel.getParent();
        if (parent instanceof CTabFolder)
        {
            return (CTabFolder) parent;
        }
        else
        {
            return getParentCTab((Composite) parent);
        }
        
    }
    
    private void registerSubscriber()
    {
        getController()
                .registerSubscriber(
                        m_docRevSelectionChangeEventName,
                        getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController()
                .unregisterSubscriber(
                        m_docRevSelectionChangeEventName,
                        getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
        if (m_selectedRev!=null &&FormHelper.isLatestItemRevision(m_selectedRev))
        {
            m_documentDetailsTopPanel.setEnabled(flag);
            m_documentDetailBottomPanel.setEnabled(flag);
        }
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
}
