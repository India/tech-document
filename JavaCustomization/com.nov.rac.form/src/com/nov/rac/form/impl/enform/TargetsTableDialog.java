package com.nov.rac.form.impl.enform;

import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class TargetsTableDialog extends AbstractSWTDialog
{
    private ENTargetsTableDialogPanel m_panel;
    private Registry m_registry;
    private static final float PERCENTAGE_WIDTH = 0.9f;
    private static final float PERCENTAGE_HIGHT = 0.6f;
    private static final int PARAM_2 = 2;
    
    public TargetsTableDialog(Shell parent)
    {
        super(parent);
    }
    
    public void setTableModel(TableModel tableModel)
    {
        m_panel.setTableModel((AIFTableModel) tableModel);
    }
    
    protected void setShellStyle(int arg0)
    {
        super.setShellStyle(SWT.CLOSE | SWT.APPLICATION_MODAL | SWT.RESIZE);
    }

    @Override
    protected Control createContents(Composite paramComposite)
    {
        m_panel = new ENTargetsTableDialogPanel(paramComposite, SWT.NONE);
        paramComposite.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false,1,1));
        
        m_panel.createUI();
        
        setDefaultSize();
        return paramComposite;
    }
    
    private void setDefaultSize()
    {
        Rectangle monitorArea = getShell().getParent().getClientArea();
        this.getShell().setSize((int) (PERCENTAGE_WIDTH * (monitorArea.width)),
                (int) (PERCENTAGE_HIGHT * (monitorArea.height)));        
    }

    @Override
    protected Point getInitialLocation(Point point)
    {
        
        // Move the dialog to the center of the top level shell.
        Rectangle shellBounds = getShell().getParent().getBounds();
        Point dialogSize = getInitialSize();

        int x= shellBounds.x + (shellBounds.width - dialogSize.x) / 2;
        int y= shellBounds.y + (shellBounds.height - dialogSize.y) / 2;
        Point location = new Point(x,y);
        
        return location;
    }

    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }

    @Override
    public boolean close()
    {
        m_panel.dispose();
        return super.close();
    }
}
