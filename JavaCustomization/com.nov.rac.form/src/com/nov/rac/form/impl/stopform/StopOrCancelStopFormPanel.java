package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class StopOrCancelStopFormPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private Registry m_registry;
    private IUIPanel m_panel = null;
    
    
    public StopOrCancelStopFormPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
       
        gl_l_composite.horizontalSpacing = 0;
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginLeft = 0;
        gl_l_composite.marginRight = 0;
        l_composite.setLayout(gl_l_composite);
        
        TCComponentForm selectedForm = (TCComponentForm) FormHelper.getTargetComponent();
        
        String change = selectedForm
                .getProperty(GlobalConstant.STOPFORM_CHANGE);
        
        String className = null;
        
        if(change != null)
        {
            className = m_registry.getString(FormHelper.getTargetComponentType()
                    + "." + change + "." + "PANEL");
            
            Object[] params = new Object[2];
            params[0] = l_composite;
            params[1] = SWT.NONE;
            
            try
            {
                m_panel = (IUIPanel) Instancer.newInstanceEx(className, params);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
            if (m_panel != null)
            {
                try
                {
                    m_panel.createUI();
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
                
            }
        }
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        if(m_panel instanceof INOVFormLoadSave)
        {
            INOVFormLoadSave l_panel = (INOVFormLoadSave) m_panel;
            l_panel.load(propMap);
        }
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        if(m_panel instanceof INOVFormLoadSave)
        {
            INOVFormLoadSave l_panel = (INOVFormLoadSave) m_panel;
            l_panel.save(propMap);
        }
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        if(m_panel instanceof INOVFormLoadSave)
        {
            INOVFormLoadSave l_panel = (INOVFormLoadSave) m_panel;
            l_panel.setEnabled(flag);
        }
        
    }
}
