package com.nov.rac.form.impl.enform;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.DualList;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.QueryUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class RelatedECRPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private Group m_relatedECRGroupContainer = null;
    private Text m_ecrText = null;
    private Button m_addECRButton = null;
    private DualList m_dualList = null;
    private HashMap<String, Object> m_eCRNumberAndTCComponentMap = new HashMap<String, Object>();
    
    public RelatedECRPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // m_dualList.getSourceList().add("A", 0);
        // m_dualList.getSourceList().add("B", 0);
        // m_dualList.getSourceList().add("C", 0);
        String[] relatedECR = PropertyMapHelper.handleNull(propMap
                .getStringArray("relatedecr"));
        if (relatedECR.length > 0)
        {
            for (String ecr : relatedECR)
            {
                m_dualList.addToTargetTable(ecr);
            }
        }
        
        return true;
    }
    
    public DualList getDualList()
    {
        return m_dualList;
    }
    
    @Override
    // public boolean save(IPropertyMap propMap) throws TCException
    // {
    // propMap.setStringArray("relatedecr", PropertyMapHelper
    // .handleNull(m_dualList.getTargetList().getItems()));
    // return true;
    // }
    public boolean save(IPropertyMap propMap) throws TCException
    {
        
        TCTable targetTable = m_dualList.getTargetTable();
        int rowCount = targetTable.getRowCount();
        String[] allValues_TargetTable = new String[rowCount];
        


        
        for (int inx = 0; inx < rowCount; inx++)
        {
            String currValue = (String) targetTable
                    .getValueAt(inx, 0);
            allValues_TargetTable[inx] = currValue;
            
            
        }
        propMap.setStringArray("relatedecr", allValues_TargetTable);
        
        TCComponent enForm = FormHelper.getTargetComponent();//some error is throw ask kishor
        CreateENECRRelationHelper objCreateENECR = new CreateENECRRelationHelper(m_eCRNumberAndTCComponentMap);
        objCreateENECR.CreateENECRRelation(enForm, propMap);
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_ecrText.setEnabled(flag);
        m_addECRButton.setEnabled(flag);
        m_dualList.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        
        l_composite.setLayout(gl_l_composite);
        
        // createRelatedECRGroupContainer(l_composite);
        createECRTextField(l_composite);
        createDualList(l_composite);
        addButtonListener();
        createDataModelMapping();
        return true;
    }
    
   
    
    protected void createDualList(Composite composite)
    {
        m_dualList = new DualList(composite, getRegistry());
        m_dualList.changeSpan(1, 1);
        m_dualList.createUI();
    }
    
    protected void createECRTextField(Composite composite)
    {
        final String defaultString = getRegistry().getString("ECRText.Default");
        Composite subComposite = new Composite(composite, SWT.NONE);
        
        GridLayout gl_l_composite = new GridLayout(3, false);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        
        subComposite.setLayout(gl_l_composite);
        subComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        
        Label lblEcr = new Label(subComposite, SWT.NONE);
        lblEcr.setText(getRegistry().getString("ECR.Label"));
        SWTUIHelper.setFont(lblEcr, SWT.BOLD);
        
        m_ecrText = new Text(subComposite, SWT.BORDER);
        m_ecrText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        m_ecrText.setText(defaultString);
        m_ecrText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        m_ecrText.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent arg0)
            {
                if (m_ecrText.getText().equals(""))
                {
                    m_ecrText.setText(defaultString);
                }
            }
            
            @Override
            public void focusGained(FocusEvent arg0)
            {
                if (m_ecrText.getText().equals(defaultString))
                {
                    m_ecrText.setText("");
                }
            }
        });
        
        m_addECRButton = new Button(subComposite, SWT.NONE);
        // m_addECRButton.setText(getRegistry().getString("AddECRButton.Label"));
        Image plusButtonImage = new Image(composite.getDisplay(),
                m_registry.getImage("addButton.IMAGE"), SWT.NONE);
        m_addECRButton.setImage(plusButtonImage);
    }
    
    protected void addButtonListener()
    {
        m_addECRButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                String searchText = m_ecrText.getText();
                if (searchText.equalsIgnoreCase("*"))
                {
                    return;
                }
                
                executeQuery(searchText);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
        
    }
    
    private void executeQuery(String searchText)
    {
        String[] ipEntries = { "Name", "Type" };
        String[] ipValues1 = { searchText, "RS ECR Form" };
        String query = "General...";
        int resultsType = 0;
        
        try
        {
            TCComponent[] foundECRForms = QueryUtils.executeSOAQuery(ipEntries,
                    ipValues1, query, resultsType);
            
            if (foundECRForms.length == 0)
            {
                searchText = getRegistry().getString("ECRsNumber.Label")
                        + m_ecrText.getText();
                String[] ipValues2 = { searchText, "RS ECR Form" };
                foundECRForms = QueryUtils.executeSOAQuery(ipEntries,
                        ipValues2, query, resultsType);
                if (foundECRForms.length == 0)
                {
                    MessageBox.post(getRegistry().getString("NOOBJFOUND.MSG"),
                            getRegistry().getString("NOOBJFOUND.INFO"),
                            MessageBox.INFORMATION);
                    // m_addECRButton.setEnabled(false);
                    m_ecrText.setText(getRegistry()
                            .getString("ECRText.Default"));
                    return;
                }
            }
            
            if (foundECRForms.length > 1)
            {
                searchText += " " + getRegistry().getString("DUPLICATEECR.MSG");
                MessageBox.post(searchText,
                        getRegistry().getString("DUPLICATEECR.INFO"),
                        MessageBox.INFORMATION);
                
                m_addECRButton.setEnabled(false);
                m_ecrText.setText(getRegistry().getString(
                        "ECRsNumberToolTip.TXT"));
                return;
            }
            
            // Assuming only one ECR will be found
            String releaseStatus = foundECRForms[0].getProperty(getRegistry()
                    .getString("releaseStatusList.PROP"));
            if (!releaseStatus.equalsIgnoreCase(getRegistry().getString(
                    "WITHDRAWN.MSG"))
                    && !releaseStatus.equalsIgnoreCase(getRegistry().getString(
                            "REJECTED.MSG")))
            {
                String ecrNumber = foundECRForms[0].getProperty(getRegistry()
                        .getString("objectName.PROP"));
                m_dualList.addToTargetTable(ecrNumber);
                m_eCRNumberAndTCComponentMap.put(ecrNumber, foundECRForms[0]);
            }
        }
        catch (TCException tce)
        {
            tce.printStackTrace();
        }
    }
    
    protected void createRelatedECRGroupContainer(Composite composite)
    {
        String label = getRegistry().getString(
                "RelatedECRPanel.GroupContainerText");
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 7, 1);
        m_relatedECRGroupContainer = new Group(composite, SWT.NONE);
        m_relatedECRGroupContainer.setLayoutData(gridData);
        m_relatedECRGroupContainer.setLayout(new GridLayout(1, true));
        m_relatedECRGroupContainer.setText(label);
        SWTUIHelper.setFont(m_relatedECRGroupContainer, SWT.BOLD);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    protected void createDataModelMapping()
    {
        
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_dualList.getTargetTable().dataModel,
                "relatedecr", FormHelper.getDataModelName());
    }
}
