package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TargetsTableTopPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class StopFormTargetsTableTopPanel extends TargetsTableTopPanel
{
    
    public StopFormTargetsTableTopPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
       super.createUI();
       
      /* m_addButton.setVisible(false);
       m_removeButton.setVisible(false);
       
       m_addButton.setEnabled(false);
       m_removeButton.setEnabled(false);*/
       
       m_addButton.dispose();
       m_removeButton.dispose();
       
       return true;
    }
}
