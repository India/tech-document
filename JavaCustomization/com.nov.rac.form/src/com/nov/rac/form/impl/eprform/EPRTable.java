/**
 * 
 */
package com.nov.rac.form.impl.eprform;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import com.nov.rac.form.renderer.AlternateIDRenderer;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */
public class EPRTable extends TCTable
{
    private Registry m_registry;
    private String m_editableColumn = null;
    
    public EPRTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
        
        m_editableColumn = m_registry.getString("futureStatus.PROP");
        
        addColumnRenderersEditors();
    }
    
    public void addColumnRenderersEditors()
    {
        // Part ID column
        addIconRenderer("TargetsTableColumnIdentifier.ItemID");
        
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        TableCellEditor editor = getColumnModel().getColumn(columnIndex)
                .getCellEditor();
        return editor.isCellEditable(null);
    }
    
    private void addIconRenderer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new PartIDIconRenderer());
    }
    
}
