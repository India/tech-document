package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TopPanel;
import com.teamcenter.rac.util.Registry;

public class StopFormTopPanel extends TopPanel
{
    
    public StopFormTopPanel(Composite parent, int style)
    {
        super(parent, style);
        
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
