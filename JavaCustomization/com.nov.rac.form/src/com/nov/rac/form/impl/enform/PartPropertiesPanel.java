package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class PartPropertiesPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    private TCComponent m_tcComponent;
    private static final int NO_OF_COLUMNS_PART_PROPS = 4;
    private Registry m_registry;
    private Text m_nameText;
    private Text m_descriptionText;
    private CLabel m_typeCLabel;
    private CLabel m_revisionStatusCLabel;
    private CLabel m_statusCLabel;
	private static final int DESCRIPTION_FIELD_TEXT_LIMIT = 240; 
    
    public PartPropertiesPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    protected TCComponent getTCComponent()
    {
        return m_tcComponent;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
//        ApplyStatusSOAHelper.applyStatus(item, "MFG");
        m_tcComponent = propMap.getComponent();
        m_nameText.setText(PropertyMapHelper.handleNull(propMap
                .getString("object_name")));
        loadTypefield(propMap);
        loadItemStatus(propMap);
        m_descriptionText.setText(PropertyMapHelper.handleNull(propMap
                .getString("object_desc")));
        loadItemRevStatus(propMap);
        return true;
    }
    
    private void loadTypefield(IPropertyMap propMap) throws TCException
    {
        m_typeCLabel.setImage(null);
        IPropertyMap[] itemMasterPropMaps =propMap.getCompound("items_tag").getCompoundArray("IMAN_master_form");
        String rsOneItemType = itemMasterPropMaps[0].getComponent().getStringProperty("rsone_itemtype");
        String object_type = PropertyMapHelper.handleNull(propMap.getCompound("items_tag").getString("object_type"));
        
        m_typeCLabel.setText(PropertyMapHelper.handleNull(rsOneItemType));
        if (!m_typeCLabel.getText().equalsIgnoreCase(""))
        {
            Image imageSWT = TCTypeRenderer.getTypeImage(
                    object_type, null);
            m_typeCLabel.setImage(imageSWT);
        }
    }
    
    private void loadItemRevStatus(IPropertyMap propMap)
    {
        
        try
        {
            TCComponent[] releaseStatuses_ItemRev = propMap
                    .getTagArray("release_statuses");
            // get the last rev
            if (releaseStatuses_ItemRev.length != 0)
            {
                
                TCComponent LastStatus_ItemRev = releaseStatuses_ItemRev[releaseStatuses_ItemRev.length - 1];
                String lastReleaseStatus = LastStatus_ItemRev
                        .getProperty("name");
                m_revisionStatusCLabel.setText(lastReleaseStatus);
            }
            else
            {
                m_revisionStatusCLabel.setText(getRegistry().getString(
                        "InProgress.Status"));
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    private void loadItemStatus(IPropertyMap propMap)
    {
        m_statusCLabel.setImage(null);
        try
        {
            TCComponent item = propMap.getTag("items_tag");
            TCComponent[] releaseStatuses_Item = item.getTCProperty(
                    "release_statuses").getReferenceValueArray();
            if (releaseStatuses_Item.length != 0)
            {
                TCComponent lastStatus_Item = releaseStatuses_Item[releaseStatuses_Item.length - 1];
                String statusName_item = lastStatus_Item.getProperty("name");
                m_statusCLabel.setText(PropertyMapHelper
                        .handleNull(statusName_item));
                if (!m_statusCLabel.getText().equalsIgnoreCase(""))
                {
                    String release_status_full_name = "release_status_list."
                            + m_statusCLabel.getText();
                    Image imageSWT = TCTypeRenderer.getTypeImage(
                            lastStatus_Item.getTypeComponent(), null);
                    m_statusCLabel.setImage(imageSWT);
                }
            }
        }
        catch (Exception e)
        {
            
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setComponent(getTCComponent());
        
        propMap.setString("object_name", m_nameText.getText());
        propMap.setString("object_desc", m_descriptionText.getText());
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_nameText.setEditable(flag);
        m_descriptionText.setEditable(flag);
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1,
                true);
        l_composite.setLayout(gl_l_composite);
        
        createPartPropertiesComposite(l_composite);
        
		createDataModelMapping();
		
        return false;
    }
    
    private void createPartPropertiesComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_composite = new GridLayout(NO_OF_COLUMNS_PART_PROPS,
                false);
        composite.setLayout(gl_composite);
        
        createNameField(composite);
        createTypeField(composite);
        createDescriptionField(composite);
        createStatusField(composite);
        createRevisionStatusField(composite);
        
    }
    
    private void createRevisionStatusField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("RevisionStatus.Label"));
        m_revisionStatusCLabel = createValueCLabel(parent, "");
        
    }
    
    private void createDescriptionField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("Description.Label"));
        m_descriptionText = createValueTextField(parent, "");
        m_descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 1, 1));
        m_descriptionText.setEditable(false);
        m_descriptionText.setTextLimit(DESCRIPTION_FIELD_TEXT_LIMIT);
    }
    
    private void createStatusField(Composite parent)
    {
        
        createKeyLabel(parent, getRegistry().getString("Status.Label"));
        m_statusCLabel = createValueCLabel(parent, "");
        
    }
    
    private void createTypeField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("Type.Label"));
        m_typeCLabel = createValueCLabel(parent, "");
        
    }
    
    private void createNameField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("Name.Label"));
        m_nameText = createValueTextField(parent, "");
        m_nameText.setEditable(false);
		String groupName = ((TCSession) AIFUtility.getDefaultSession()).getGroup().toString();
        int textLimit = PreferenceHelper.getParsedPrefValueForGroup("NOV_ObjectName_Length_For_Groups",
                TCPreferenceService.TC_preference_site, groupName, "|");
        m_nameText.setTextLimit(textLimit);
        
    }
    
    private CLabel createValueCLabel(Composite parent, String setText)
    {
        CLabel clabel = new CLabel(parent, SWT.NONE);
        clabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1));
        clabel.setText(setText);
        return clabel;
    }
    
    private Text createValueTextField(Composite parent, String setText)
    {
        Text text = new Text(parent, SWT.BORDER);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        text.setText(setText);
        return text;
    }
    
    private Label createKeyLabel(Composite parent, String setText)
    {
        Label label = new Label(parent, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1,
                1));
        label.setText(setText);
        return label;
    }
    
    public void setEditable(boolean flag)
    {
        m_nameText.setEditable(flag);
        m_descriptionText.setEditable(flag);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    public void dispose()
    {
        super.dispose();
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        
        dataBindingHelper.bindData(m_nameText, "object_name", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_descriptionText, "object_desc",
                FormHelper.getDialogDataModelName());
        
    }
    
}
