package com.nov.rac.form.impl.enform;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.services.publishdataset.NovPublishDatasetHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.newdataset.NewDatasetCommand;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class DatasetTablePanel extends AbstractUIPanel implements INOVFormLoadSave, PropertyChangeListener
{
    private Registry m_registry = null;
    private TCTable m_datasetTable;
    private Button m_removeButton;
    private Button m_addButton;
    private TCComponent m_selectedDocument = null;
    private Composite m_addRemoveButtonComposite;
    private static final String DATASET_RELATION = "IMAN_specification";
    
    public DatasetTablePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_selectedDocument = propMap.getComponent();
        m_datasetTable.removeAllRows();
        // TCComponent[] datasets = propMap.getTagArray(DATASET_RELATION);
        // TableUtils.populateTable(datasets, m_datasetTable);
        
        IPropertyMap[] compoundArray = propMap.getCompoundArray(NovPublishDatasetHelper.PROP_PUBLISH_DATASET);
        TableUtils.populateTable(compoundArray, m_datasetTable);
        
        
        if(!FormHelper.hasWriteAccess(m_selectedDocument))
        {
            m_addButton.setEnabled(false);
            m_removeButton.setEnabled(false);
        }
        
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] datasetPropMaps = TableUtils.populateMap(m_datasetTable);
        propMap.setCompoundArray(NovPublishDatasetHelper.PROP_PUBLISH_DATASET, datasetPropMaps);
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        m_addButton.setEnabled(flag);
        m_removeButton.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite composite = getComposite();
        GridLayout gl_composite = new GridLayout(2, false);
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        composite.setLayoutData(gridData);
        
        CLabel datasetLabel = new CLabel(composite, SWT.NONE);
        datasetLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
        datasetLabel.setText(getRegistry().getString("Dataset.Label"));
        datasetLabel.setImage(TCTypeRenderer.getTypeImage("Dataset", null));
        SWTUIHelper.setFont(datasetLabel, SWT.BOLD);
        TCComponentType tcomponene = new TCComponentType();
        // discussed with harshalr how to show image=
        // Object image = TCTypeRenderer.getTypeImage(,
        // null);
        // datasetLabel.setImage(image)
        
        createAddRemoveButton(composite);
        createDatasetTCTable(composite);
        addButtonListeners();
        return false;
    }
    
    private void addButtonListeners()
    {
        m_removeButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        try
                        {
                            validateTableRowSelection();
                            if(confirmRemoval())
                            {
                                deleteDataset();
                            }
                        }
                        catch (TCException e)
                        {
                            MessageBox.post(e.getMessage(), "ERROR", MessageBox.ERROR);
                        }
                    }
                });
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
        
        m_addButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                Display.getDefault().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        addDataset();
                        
                    }
                });
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        });
    }
    
    private void deleteDataset()
    {
        try
        {
            int row = m_datasetTable.getSelectedRow();
            if (row > -1)
            {
                int column = m_datasetTable.getColumnIndex("object_name");
                String datasetId = (String) m_datasetTable.getValueAt(row, column);
                
                TCComponent[] relatedComponents = m_selectedDocument.getRelatedComponents(DATASET_RELATION);
                
                for (int inx = 0; inx < relatedComponents.length; inx++)
                {
                    String tempDatasetId = relatedComponents[inx].getProperty("object_name");
                    if (tempDatasetId.equals(datasetId))
                    {
                        FormHelper.deleteRelation(m_selectedDocument, new TCComponent[] { relatedComponents[inx] },
                                DATASET_RELATION);
                        m_datasetTable.removeRow(row);
                        break;
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    protected void createAddRemoveButton(Composite composite)
    {
        m_addRemoveButtonComposite = new Composite(composite, SWT.NONE);
        GridLayout layout = new GridLayout(2, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        m_addRemoveButtonComposite.setLayout(layout);
        m_addRemoveButtonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
        createRemoveButton(m_addRemoveButtonComposite);
        createAddButton(m_addRemoveButtonComposite);
        setAddRemoveButtonImages();
    }
    
    private void setAddRemoveButtonImages()
    {
        Image minusButtonImage = new Image(getComposite().getDisplay(), getRegistry().getImage("removeButton.IMAGE"),
                SWT.NONE);
        Image pluseButtonImage = new Image(getComposite().getDisplay(), getRegistry().getImage("addButton.IMAGE"),
                SWT.NONE);
        m_addButton.setText("");
        m_addButton.setImage(pluseButtonImage);
        m_removeButton.setImage(minusButtonImage);
    }
    
    private void createAddButton(Composite addRemoveButtonComposite)
    {
        m_addButton = new Button(addRemoveButtonComposite, SWT.NONE);
        GridData gdAddBtn = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        m_addButton.setLayoutData(gdAddBtn);
    }
    
    private void createRemoveButton(Composite addRemoveButtonComposite)
    {
        m_removeButton = new Button(addRemoveButtonComposite, SWT.NONE);
        GridData gdRemoveBtn = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        m_removeButton.setLayoutData(gdRemoveBtn);
        
    }
    
    private void createDatasetTCTable(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        
        m_datasetTable = createTCTable();
        JScrollPane scrollPane = new JScrollPane(m_datasetTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    
    private TCTable createTCTable()
    {
        /*
         * String[] colIdentifiers = getRegistry().getStringArray(
         * "DatasetTable.ColumnIdentifiers");
         */
        String[] colIdentifiers = { NovPublishDatasetHelper.PROP_DATASET_TYPE,
                NovPublishDatasetHelper.PROP_APPLICATION, NovPublishDatasetHelper.PROP_DATASET_NAME,
                NovPublishDatasetHelper.PROP_IS_INCLUDE, NovPublishDatasetHelper.PROP_IS_IN_TABLE,
                NovPublishDatasetHelper.PROP_DOCUMENT };
        
        String[] colNames = getRegistry().getStringArray("DatasetTable.ColumnNames");
        
        TCTable tCTable = new TCTable(colIdentifiers, colNames)
        {
            
            private static final long serialVersionUID = 1L;
            
            @Override
            public boolean isCellEditable(int row, int col)
            {
                int includeCol = this.getColumnIndex(NovPublishDatasetHelper.PROP_IS_INCLUDE);
                return col == includeCol;
            }
            
        };
        
        tCTable.setShowVerticalLines(true);
        tCTable.setAutoResizeMode(TCTable.AUTO_RESIZE_ALL_COLUMNS);
        
        // hide col
        int colIndex = tCTable.getColumnIndex(NovPublishDatasetHelper.PROP_DATASET_TYPE);
        TableUtils.hideTableColumn(tCTable, colIndex);
        
        colIndex = tCTable.getColumnIndex(NovPublishDatasetHelper.PROP_APPLICATION);
        TableUtils.hideTableColumn(tCTable, colIndex);
        
        // set icon renderer
        colIndex = tCTable.getColumnIndex(NovPublishDatasetHelper.PROP_DATASET_NAME);
        TableColumn nameColumn = tCTable.getColumnModel().getColumn(colIndex);
        nameColumn.setCellRenderer(new PartIDIconRenderer());
        
        // set combo renderer
        colIndex = tCTable.getColumnIndex(NovPublishDatasetHelper.PROP_IS_INCLUDE);
        TableColumn publishToColumn = tCTable.getColumnModel().getColumn(colIndex);
        publishToColumn.setCellEditor(tCTable.getDefaultEditor(Boolean.class));
        publishToColumn.setCellRenderer(tCTable.getDefaultRenderer(Boolean.class));
        
        colIndex = tCTable.getColumnIndex(NovPublishDatasetHelper.PROP_IS_IN_TABLE);
        TableUtils.hideTableColumn(tCTable, colIndex);
        TableColumn inTableColumn = tCTable.getColumnModel().getColumn(colIndex);
        inTableColumn.setCellEditor(tCTable.getDefaultEditor(Boolean.class));
        
        colIndex = tCTable.getColumnIndex(NovPublishDatasetHelper.PROP_DOCUMENT);
        TableUtils.hideTableColumn(tCTable, colIndex);
        TableColumn documentColumn = tCTable.getColumnModel().getColumn(colIndex);
        documentColumn.setCellEditor(tCTable.getDefaultEditor(TCComponent.class));
        
        return tCTable;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private Object getSourceParent(final PublishEvent event)
    {
        Object sourceParent = null;
        IPublisher eventPublisher = event.getSource();
        Composite sourceComposite = ((AbstractUIPanel) eventPublisher).getComposite();
        sourceParent = sourceComposite.getParent();
        return sourceParent;
    }
    
    public void dispose()
    {
        super.dispose();
    }
    
    private void addDataset()
    {
        try
        {
            Registry l_reg = Registry.getRegistry("com.teamcenter.rac.common.actions.actions");
            
            Object param[] = new Object[2];
            param[0] = AIFUtility.getCurrentApplication().getDesktop();
            param[1] = AIFUtility.getCurrentApplication();
            
            NewDatasetCommand command = (NewDatasetCommand) l_reg.newInstanceForEx("newDatasetCommand", param);
            command.addPropertyChangeListener(DatasetTablePanel.this);
            command.run();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        try
        {
            TCComponentDataset newDataset = (TCComponentDataset) event.getNewValue();
            // m_datasetTable.addRow(newDataset);
            
            FormHelper.createRelation(m_selectedDocument, new TCComponent[] { newDataset }, DATASET_RELATION);
            
            TCComponent selectedDocItem = ((TCComponentItemRevision) m_selectedDocument).getItem();
            loadPublishDatasetInformation(selectedDocItem);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void loadPublishDatasetInformation(TCComponent tCComponent) throws TCException
    {
        IPropertyMap propMap = new SimplePropertyMap();
        Map<String, String> stringMap = new HashMap<String, String>();
        stringMap.put("application",
                NovPublishDatasetHelper.getPublishDatasetApplication("NOV_publish_dataset_portals"));
        
        NovPublishDatasetHelper.getPublishDatasetRelatedInfo(tCComponent, stringMap, propMap);
        IPropertyMap[] compoundArray = propMap.getCompoundArray(NovPublishDatasetHelper.PROP_PUBLISH_DATASET);
        m_datasetTable.removeAllRows();
        TableUtils.populateTable(compoundArray, m_datasetTable);
    }
    
    protected void makeLatestRevisionEditable(TCComponent tcComponent)
    {
        if ((GlobalConstant.COMPARE_NONCOMPARE_FLAG
                && !FormHelper.isLatestItemRevision(tcComponent)) || !FormHelper.hasWriteAccess(tcComponent))
        {
            m_addButton.setEnabled(false);
            m_removeButton.setEnabled(false);
        }
        else
        {
            m_addButton.setEnabled(true);
            m_removeButton.setEnabled(true);
        }
    }
    
    protected boolean confirmRemoval()
    {
        boolean response = MessageDialog.openQuestion(m_removeButton.getShell(), getRegistry().getString("Confirm.Label"), getRegistry().getString("RemovalConfirm.Msg"));
        return response;
    }
    
    protected void validateTableRowSelection() throws TCException
    {
        if( m_datasetTable != null && m_datasetTable.getSelectedRowCount() < 1)
        {
            String[] errors =  new String[]{ getRegistry().getString("NoItemSelected.MSG") };
            TCException exception = new TCException( errors );
            
            throw exception;
        }
    }
}
