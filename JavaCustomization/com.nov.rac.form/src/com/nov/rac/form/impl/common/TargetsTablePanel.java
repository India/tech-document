package com.nov.rac.form.impl.common;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TargetsTable;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class TargetsTablePanel extends AbstractUIPanel implements INOVFormLoadSave, IPublisher, ISubscriber 
{
	private Registry m_registry;
    private TCTable m_table;
	private int m_varticalSpan = 30;
    private boolean m_isGrabExcessVerticalSpace = false;
	
	public int getVarticalSpan()
    {
        return m_varticalSpan;
    }

    public void setVarticalSpan(int varticalSpan)
    {
        m_varticalSpan = varticalSpan;
    }
   

    public void setGrabVarticalSpace(boolean bValue)
    {
        m_isGrabExcessVerticalSpace = bValue;   
    }

	public TargetsTablePanel(Composite parent, int style) 
	{
		super(parent, style);
	}
	
	public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
	public boolean createUI() throws TCException 
	{
		Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, m_isGrabExcessVerticalSpace , 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        
        createTCTable(composite);
		
		return true;
	}
    
    private void createTCTable(Composite composite)
    {
        Composite comp = new Composite(composite, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true,
                1, m_varticalSpan);
        comp.setLayoutData(gridData);
        
        String[] columnNames = getRegistry().getStringArray(
                "TargetsTable.COLUMN_NAMES");
        String[] columnIdentifiers = getRegistry().getStringArray(
                "TargetsTable.COLUMN_IDENTIFIERS");
        
        m_table = new TargetsTable(columnIdentifiers, columnNames,
                getRegistry());
        
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_table.setSortEnabled(false);
        
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        
        ScrollPagePane scrollPane = new ScrollPagePane(m_table);
        tablePanel.add(scrollPane);
        
        SWTUIUtilities.embed(comp, tablePanel, false);
    }
    
    protected TCTable getTargetsTable()
    {
        return m_table;
    }

	@Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException 
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(PublishEvent event) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public IPropertyMap getData() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEnabled(boolean flag) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean reload() throws TCException 
	{
		// TODO Auto-generated method stub
		return false;
	}

}
