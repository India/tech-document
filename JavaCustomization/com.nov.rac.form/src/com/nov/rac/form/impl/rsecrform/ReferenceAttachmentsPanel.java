package com.nov.rac.form.impl.rsecrform;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.common.AttachmentsBoxPanel;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;

public class ReferenceAttachmentsPanel extends AttachmentsBoxPanel
{
    
    public ReferenceAttachmentsPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        setNameLabel(m_registry.getString("ReferenceAttachmentsPanel.Label"));
        return true;
    }
    
    @Override
    protected void createButtonBar(Composite composite)
    {
        createHeaderName(composite);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        List<TCComponentDataset> datasetArray = new ArrayList<TCComponentDataset>();
        TCComponentForm form = (TCComponentForm) FormHelper
                .getTargetComponent();
        
        AIFComponentContext[] datasets = form.getRelated(m_registry
                .getString("CustomSubForm.RELATION"));
        
        for (AIFComponentContext object : datasets)
        {
            datasetArray.add((TCComponentDataset) object.getComponent());
        }
        
        addRowsToTable(datasetArray);
        
        return true;
        
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
}
