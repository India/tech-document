package com.nov.rac.form.impl.common;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.common.AttachmentsBoxPanel;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.newdataset.NewDatasetCommand;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

/**
 * @author mishalt
 * 
 */
public class ChangeDescriptionAttachmentsPanel extends AttachmentsBoxPanel implements PropertyChangeListener
{
    private final static int NUMBER_OF_OBJECTS = 3;
    public ChangeDescriptionAttachmentsPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    protected void setRegistry(Registry registry)
    {
        this.m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        setNameLabel(m_registry.getString("ChangeDescriptionAttachmentsPanel.Label"));
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        List<TCComponentDataset> datasetArray = new ArrayList<TCComponentDataset>();
        TCComponentForm form = (TCComponentForm) FormHelper.getTargetComponent();
        
        AIFComponentContext[] comps = form.getSecondary();
        String relationName = m_registry.getString("ChangeDescriptionDocument.RELATION");
        
        for (AIFComponentContext object : comps)
        {
            if (object.getComponent() instanceof TCComponentDataset
                    && object.getContextDisplayName().equals(relationName))
            {
                datasetArray.add((TCComponentDataset) object.getComponent());
            }
        }
        
        addRowsToTable(datasetArray);
        
        return true;
    }
    
    @Override
    protected void plusButtonSelectionListener()
    {
        try
        {
            Registry l_reg = Registry.getRegistry("com.teamcenter.rac.common.actions.actions");
            
            Object param[] = new Object[NUMBER_OF_OBJECTS];
            param[0] = AIFUtility.getCurrentApplication().getDesktop();
            param[1] = AIFUtility.getCurrentApplication();
            param[2] = new Boolean(true);
            
            NewDatasetCommand command = (NewDatasetCommand) l_reg.newInstanceForEx("newDatasetCommand", param);
            command.addPropertyChangeListener(ChangeDescriptionAttachmentsPanel.this);
            command.run();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void minusButtonSelectionListener()
    {
        int rowIndex = m_table.getSelectedRow();
        
        try
        {
            validateTableRowSelection();
            if (confirmRemoval() && rowIndex >= 0)
            {
                AIFTableLine[] object = m_table.getSelected();
                TCComponentDataset dataset = (TCComponentDataset) object[0].getComponent();
                TCComponent[] secondaryObjects = new TCComponent[] { dataset };
                TCComponentForm form = (TCComponentForm) FormHelper.getTargetComponent();
                String relationName = m_registry.getString("ChangeDescriptionDocument.RELATION");
                FormHelper.deleteRelation(form, secondaryObjects, relationName);
                m_table.removeRow(rowIndex);
            }
        }
        catch (TCException e)
        {
            MessageBox.post(e.getMessage(), "ERROR", MessageBox.ERROR);
        }
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        TCComponentDataset newDataset = (TCComponentDataset) event.getNewValue();
        m_table.dataModel.addRow(newDataset);
    }
    
}