package com.nov.rac.form.impl.stopform;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.common.TargetsTablePanel;
import com.nov.rac.form.impl.common.TargetsTableTopPanelWithoutAddRemove;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class StopFormTargetsTablePanel extends TargetsTablePanel implements INOVFormLoadSave
{
   
    private static final int VERTICAL_SPAN = 30;
    private TargetsTableTopPanelWithoutAddRemove m_topPanel;
    
    public StopFormTargetsTablePanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        setRegistry(Registry.getRegistry(this));
    }
    
    @Override
    public boolean createUI() throws TCException
    {

        Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, false , 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        
        createTopPanel(composite);
        
        createTCTable(composite);
        
        createDataModelMapping();
        
        m_topPanel.addExportButtonListener(m_table);
        
        return true;
    
    }
    

    private void createTopPanel(Composite composite)
    {
        m_topPanel = new TargetsTableTopPanelWithoutAddRemove(composite, SWT.NONE);
        m_topPanel.setRegistry(getRegistry());
        try
        {
            m_topPanel.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void createTableInstance(String[] columnNames,
            String[] columnIdentifiers)
    {
        m_table = new TCTable(columnIdentifiers, columnNames);
    }
    
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        IPropertyMap[] dispositionsAry = propMap.getCompoundArray(getRegistry()
                .getString("targetDisposition.PROP"));
        
        if(dispositionsAry != null)
        {
            for(IPropertyMap dispMap : dispositionsAry)
            {
                String futureStatus = dispMap.getString("futurestatus");
                if(futureStatus == null || futureStatus.equals(""))
                {
                    dispMap.setString("futurestatus" , "STOP");
                }
            }
            
        }
       
        super.load(propMap);
        
        return true;
    
        
    }
    

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        super.setEnabled(flag);
        
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
