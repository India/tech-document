package com.nov.rac.form.impl.enform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocumentDetailsTopPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private static final int NO_OF_COLUMNS_MAIN_COMPOSITE = 3;
    private Registry m_registry;
    private CLabel m_categoryLabel;
    private CLabel m_statusLabel;
    private CLabel m_typeLabel;
    private CLabel m_releasedDateLabel;
    private CLabel m_ownerCLabel;
    private Text m_descriptionText;
    private Text m_nameText;
    private RelatedFormsTablePanel m_relatedFormsTablePanel;
    private TCComponent m_tcComponent;
    
    public DocumentDetailsTopPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        loadDocRevProperties(propMap);
        m_relatedFormsTablePanel.load(propMap);
        return true;
    }
    private void loadDocRevProperties(IPropertyMap propMap)
    {
        m_tcComponent = propMap.getComponent();
        m_nameText.setText(PropertyMapHelper.handleNull(propMap
                .getString("object_name")));
        m_typeLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("DocumentsType")));
        loadDocRevStatus(propMap);
        loadOwnerField(propMap);
        loadReleasedDate(propMap);
        m_descriptionText.setText(PropertyMapHelper.handleNull(propMap
                .getString("object_desc")));
        m_categoryLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("DocumentsCAT")));
        
      
    }
    
    private void loadDocRevStatus(IPropertyMap propMap)
    {
        
        try
        {
            TCComponent[] releaseStatuses_DocRev = propMap
                    .getTagArray("release_statuses");
            // get the last rev
            if (releaseStatuses_DocRev != null)
            {
                if (releaseStatuses_DocRev.length != 0)
                {
                    
                    TCComponent LastStatus_ItemRev = releaseStatuses_DocRev[releaseStatuses_DocRev.length - 1];
                    String lastReleaseStatus = LastStatus_ItemRev
                            .getProperty("name");
                    m_statusLabel.setText(lastReleaseStatus);
                }
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    private void loadOwnerField(IPropertyMap propMap)
    {
        m_ownerCLabel.setImage(null);
        m_ownerCLabel.setText(PropertyMapHelper.handleNull(propMap
                .getTag("owning_user")));
        if (!m_ownerCLabel.getText().equalsIgnoreCase(""))
        {
            Image imageSWT = TCTypeRenderer.getTypeImage(
                    propMap.getTag("owning_user").getTypeComponent(), null);
            m_ownerCLabel.setImage(imageSWT);
        }
    }
    private void loadReleasedDate(IPropertyMap propMap)
    {
        Date theDate = propMap.getDate("date_released");
        if (theDate == null)
        {
//            if (m_documentRevisionCombo.getItemCount() > 0)
//            {
//                m_releasedDateLabel.setText(getRegistry().getString("NA"));
//            }
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            m_releasedDateLabel.setText(dateFormat.format(theDate));
        }
    }
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        IPropertyMap revPropMap = propMap.getCompound("revision");
        
        revPropMap.setComponent(getComponent());
        
        revPropMap.setString("object_name", m_nameText.getText());
        
        revPropMap.setString("object_desc", m_descriptionText.getText());
        
        return true;
    }
    
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_nameText.setEnabled(flag);
        m_descriptionText.setEnabled(flag);
        
    }
    
    private TCComponent getComponent()
    {
        return m_tcComponent;
    }

    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        
        Composite mainComposite = createMainComposite(l_composite);
        createDocPropComposite(mainComposite);
        m_relatedFormsTablePanel = new RelatedFormsTablePanel(mainComposite,
                SWT.NONE);
        m_relatedFormsTablePanel.createUI();
        m_relatedFormsTablePanel.getComposite().setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        
        createDataModelMapping();
        
        return true;
    }
    
    private Composite createMainComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(NO_OF_COLUMNS_MAIN_COMPOSITE, true);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        return composite;
        
    }
    
    private void createDocPropComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(2, true);
        
        composite.setLayout(gl_l_composite);
        
        createCol1(composite);
        createCol2(composite);
    }
    
    private void createCol1(Composite parent)
    {
        Composite composite = createColumnComposite(parent);
        createKeyLabel(composite, getRegistry().getString("Name.Label"));
        m_nameText = createValueText(composite, "");
        m_nameText.setEnabled(false);
        createKeyLabel(composite, getRegistry().getString("Description.Label"));
        m_descriptionText = createValueText(composite, "");
        m_descriptionText.setEnabled(false);
        createKeyLabel(composite, getRegistry().getString("Owner.Label"));
        m_ownerCLabel = createValueCLabel(composite, "");
        createKeyLabel(composite, getRegistry().getString("ReleasedDate.Label"));
        m_releasedDateLabel = createValueCLabel(composite, "");
    }
    
    private void createCol2(Composite parent)
    {
        Composite composite = createColumnComposite(parent);
        createKeyLabel(composite, getRegistry().getString("Type.Label"));
        m_typeLabel = createValueCLabel(composite, "");
        createKeyLabel(composite, getRegistry().getString("Status.Label"));
        m_statusLabel = createValueCLabel(composite, "");
        createKeyLabel(composite, getRegistry().getString("Category.Label"));
        m_categoryLabel = createValueCLabel(composite, "");
        createKeyLabel(composite, "");
    }
    
    private Composite createColumnComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        // gl_composite.horizontalSpacing = 15;
        composite.setLayout(gl_composite);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1));
        return composite;
        
    }
    
    private Label createKeyLabel(Composite parent, String setText)
    {
        Label label = new Label(parent, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1,
                1));
        label.setText(setText);
        return label;
    }
    
    private CLabel createValueCLabel(Composite parent, String setText)
    {
        CLabel clabel = new CLabel(parent, SWT.NONE);
        clabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1));
        clabel.setText(setText);
        return clabel;
    }
    
    private Text createValueText(Composite parent, String setText)
    {
        Text text = new Text(parent, SWT.BORDER);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1));
        text.setText(setText);
        return text;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        
        dataBindingHelper.bindData(m_nameText, "object_name", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_descriptionText, "object_desc",
                FormHelper.getDialogDataModelName());
    }

    
}
