package com.nov.rac.form.impl.enform;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.plaf.TableUI;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.renderer.AlternateIDContextRenderer;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.framework.tablecomparator.impl.ComparatorDataModel;
import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;
import com.nov.rac.framework.tablecomparator.impl.ItemComparator;
import com.nov.rac.framework.tablecomparator.impl.TablePainter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class AlternateIDsPanel extends AbstractUIPanel implements
        INOVFormLoadSave,ITableComparatorSubscriber
{
    
    private Registry m_registry = null;
    private TCTable m_alternateIDsTable = null;
    private TCComponent m_tcComponent = null;
    private ITableComparator m_iTableComparator = null;
    private static final String ALTERNATEID_CONTEXT = "ALTERNATEID";
    
    public AlternateIDsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_tcComponent = propMap.getComponent();
        m_alternateIDsTable.removeAllRows();
        TCComponent[] altIds = propMap.getTagArray("altid_list");
       
        Vector<IPropertyMap> propMaps = new Vector<IPropertyMap>();
        String[] propNames = new String[]{"object_type","object_string"};
        for (TCComponent eachAltid : altIds)
        {
            IPropertyMap propertyMap = new SimplePropertyMap();
            propertyMap.setComponent(eachAltid);
            PropertyMapHelper.componentToMap(eachAltid,propNames ,propertyMap );
            String context = eachAltid.getProperty("idcontext");
            propertyMap.setString("idcontext", context);
            propMaps.add(propertyMap);
        }        
       
//        TableUtils.populateTable(altIds, m_alternateIDsTable);        
        TableUtils.populateTable(propMaps.toArray(new IPropertyMap[propMaps.size()]), m_alternateIDsTable);
       
        if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            initializeComparision();
        }
        return true;
    }
    
    private void initializeComparision()
    {
        String[] tableColumnNames = getRegistry().getStringArray(
                "AlternateIDsPanel.CompareColumns");
        
        IDataModel model = new ComparatorDataModel();
        model.setPrimaryKeyColumn("object_string");
        model.setTableColumns(tableColumnNames);
        model.setTable(m_alternateIDsTable);
        model.setComparator(new ItemComparator());
        model.setCompareObject(m_tcComponent);
        model.setSubscriber(this);
        
        m_iTableComparator = DefaultTableComparator.getController();
        m_iTableComparator.setContext(ALTERNATEID_CONTEXT, model);
        m_iTableComparator.compareTable(ALTERNATEID_CONTEXT);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        GridLayout gl_composite = new GridLayout(1, true);
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        CLabel alternalteIdsLabel = new CLabel(composite, SWT.NONE);
        alternalteIdsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
                true, true, 1, 1));
        alternalteIdsLabel.setText(getRegistry()
                .getString("AlternateIDs.Label"));
        SWTUIHelper.setFont(alternalteIdsLabel, SWT.BOLD);
        alternalteIdsLabel.setImage(TCTypeRenderer.getTypeImage("Identifier", null));
        
        createAlternateIDsTCTable(composite);
        
        return true;
    }
    
    private void createAlternateIDsTCTable(Composite parent)
    {
        
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        
        m_alternateIDsTable = createTCTable();
        JScrollPane scrollPane = new JScrollPane(m_alternateIDsTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
    }
    
    private TCTable createTCTable()
    {
        
        // TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication()
        // .getSession();
        String[] propertyNames = getRegistry().getStringArray(
                "AlternateIDsPanel.PropertyNames");
        String[] colNames = getRegistry().getStringArray(
                "AlternateIDsPanel.ColumnNames");
        
        TCTable tCTable = new TCTable(propertyNames, colNames);
        tCTable.setShowVerticalLines(false);
        tCTable.setAutoCreateColumnsFromModel(false);
        tCTable.setAutoResizeMode(tCTable.AUTO_RESIZE_ALL_COLUMNS);
        tCTable.getTableHeader().setBorder(
                UIManager.getBorder("TableHeader.cellBorder"));
        tCTable.getTableHeader().setVisible(false);
        tCTable.getTableHeader().setPreferredSize(new Dimension(-1, 0));
        tCTable.setShowGrid(false);


        hideColumns(tCTable);
        
        // set icon renderer
        int colIndex = tCTable.getColumnIndex(getRegistry().getString(
                "AlternateIDsPanel.ColumnsToRender"));
        TableColumn column = tCTable.getColumnModel().getColumn(colIndex);
        column.setCellRenderer(new PartIDIconRenderer());        
        
        TableColumn cnxtColumn =tCTable.getColumnModel().getColumn(tCTable.getColumnIndex("idcontext"));
        cnxtColumn.setMaxWidth(50);
        cnxtColumn.setCellRenderer(new AlternateIDContextRenderer());
        return tCTable;
        
    }
    
    
    
    private void hideColumns(TCTable tCTable)
    {
        // hid col
        String[] hideColumns = getRegistry().getStringArray("AlternateIDsPanel.ColumnsToHide");
        for(String column : hideColumns)
        {
            int colIndex = tCTable.getColumnIndex(column);
            TableUtils.hideTableColumn(tCTable, colIndex);
        }
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public void dispose()
    {
        if (m_iTableComparator != null)
        {
            m_iTableComparator.unregisterToComparator(ALTERNATEID_CONTEXT);
        }
        super.dispose();
    }

    @Override
    public void updateTable(PublishEvent event)
    {
        CompareOutputBean bean = null;
        TablePainter painter = null;
        if (event != null)
        {
            bean = (CompareOutputBean) event.getNewValue();
            
            if (event.getPropertyName().equals(ALTERNATEID_CONTEXT)
                    && this == bean.getObject())
            {
                painter = new TablePainter(bean);
            }
        }
        else
        {
            painter = new TablePainter(bean);
        }
        painter.paintTable(m_alternateIDsTable);
    }
}



