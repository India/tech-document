package com.nov.rac.form.impl.mbcform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TopPanel;
import com.teamcenter.rac.util.Registry;

/**
 * @author mishalt
 *
 */
public class MBCTopPanel extends TopPanel
{ 
    public MBCTopPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
