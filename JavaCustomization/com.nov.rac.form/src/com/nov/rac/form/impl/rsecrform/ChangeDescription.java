package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

/**
 * 
 * 
 * @author vasanthak
 * 
 */

public class ChangeDescription extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    private Text m_changeDescriptionTextField = null;
    
    private Composite m_changeDescriptionGroupContainer = null;
    private Registry m_registry = null;
    
    public ChangeDescription(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_changeDescriptionTextField.setText(PropertyMapHelper
                .handleNull(propMap.getString("nov4_chage_desc")));
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return true;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, true));
        
        getExpandableComposite().setText(getRegistry().getString("ChangeDescription.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        createChangeDescriptionGroupContainer(l_composite);
        createChangeDescriptionTextField();
        
        // createDataModelMapping();
        
        return true;
    }
    
    private void createChangeDescriptionGroupContainer(Composite l_composite)
    {
        m_changeDescriptionGroupContainer = new Composite(l_composite, SWT.NONE);
        String label = getRegistry().getString(
                "ChangeDescriptionPanel.GroupContainerText");
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 25);
        // m_ChangeDescriptionGroupContainer = new Group(l_composite, SWT.NONE);
        m_changeDescriptionGroupContainer.setLayoutData(gridData);
        m_changeDescriptionGroupContainer.setLayout(new GridLayout(1, true));
    }
    
    private void createChangeDescriptionTextField()
    {
        m_changeDescriptionTextField = new Text(
                m_changeDescriptionGroupContainer, SWT.BORDER | SWT.MULTI
                        | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        m_changeDescriptionTextField.setLayoutData(gridData);
        m_changeDescriptionTextField.setBackground(getComposite().getDisplay().
                getSystemColor( SWT.COLOR_WHITE));
        m_changeDescriptionTextField.setEnabled(false);
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_changeDescriptionTextField.setEditable(flag);
    }
    
   
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
}
