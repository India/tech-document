package com.nov.rac.form.impl.ecrform;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TopPanel;
import com.nov.rac.utilities.utils.NOVBrowser;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ECRFormTopPanel extends TopPanel 
{

	private Registry m_registry = null;
	
	public ECRFormTopPanel(Composite parent, int style) 
	{
		super(parent, style);
		m_registry = Registry.getRegistry(this);
        setRegistry(m_registry);
	}
	
	@Override
	public boolean createUI() throws TCException 
	{
		super.createUI();
		
		addListeners();
		
		return true;
	}
	
	private void addListeners()
    {
		selectionListenerOnHelpBtn();
	}
	
	private void selectionListenerOnHelpBtn() 
	{
		m_helpButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent se)
			{
				ecrFormShowHelp();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent se) 
			{
			}
		});
	}
	
	private void ecrFormShowHelp()
    {
    	String helpURL = "";

    	TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
    	TCPreferenceService prefService = session.getPreferenceService();

    	helpURL = prefService.getStringValue( m_registry.getString("ECRFormHelpURL.NAME"));

    	try 
    	{
    		if( !helpURL.isEmpty() )
    		{
    			//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpURL);
    			NOVBrowser.open(helpURL);
    		}
    		else
    		{
    			String[] errorStrings = new String[2];
    			errorStrings[0] = m_registry.getString("");
    			errorStrings[1] = m_registry.getString("");
    			throw new TCException( errorStrings  );
    		}
    	} 
    	catch (Exception ex)
    	{
    		//ex.printStackTrace();
    		MessageBox.post(ex);
    	}
    }

}
