package com.nov.rac.form.impl.ecoform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.common.TargetsTablePanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class TestECOTargetsTablePanel extends TargetsTablePanel 
{

	public TestECOTargetsTablePanel(Composite parent, int style) {
		super(parent, style);
		Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
	}

	
	@Override
	public boolean createUI() throws TCException {
		// TODO Auto-generated method stub
		return super.createUI();
	}
	
	@Override
	protected void createTableInstance(String[] columnNames,
			String[] columnIdentifiers) {
		
		m_table = new ECOTable(columnIdentifiers, columnNames,
                getRegistry());
		System.out.println("<<<<<<<<< createTableInstance invoked >>>>>>>>>>>");
	}
	

}
