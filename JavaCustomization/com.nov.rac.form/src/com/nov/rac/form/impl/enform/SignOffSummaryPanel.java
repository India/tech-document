package com.nov.rac.form.impl.enform;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JScrollPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponentSignoff;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class SignOffSummaryPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    private Registry m_registry;
    private TCTable m_signOffTable;
    
    public SignOffSummaryPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_signOffTable.removeAllRows();
        
        IPropertyMap rootTaskPropMap = propMap.getCompound("root_task");
        if(rootTaskPropMap ==null)
        {
            return false;
        }
       
        IPropertyMap[] childTasksPropMaps = rootTaskPropMap
                .getCompoundArray("child_tasks");
        
        if(childTasksPropMaps==null)
        {
            return false;
        }
            
        Vector<Vector<Object>> allRows = new Vector<Vector<Object>>();
        
        for (IPropertyMap childTaskPropMap : childTasksPropMaps)
        {
            if (childTaskPropMap.getType().equalsIgnoreCase("EPMReviewTask"))
            {
                IPropertyMap[] children_reviewTask = childTaskPropMap
                        .getCompoundArray("child_tasks");
                for (IPropertyMap child_reviewTask : children_reviewTask)
                {
                    if (child_reviewTask.getType().equalsIgnoreCase(
                            "EPMPerformSignoffTask"))
                    {
                        IPropertyMap[] signoffPropMaps = child_reviewTask
                                .getCompoundArray("signoff_attachments");
                        if (signoffPropMaps != null
                                && signoffPropMaps.length > 0)
                        {
                            for (IPropertyMap signoffPropMap : signoffPropMaps)
                            {
                                loadSignoffRow(allRows, childTaskPropMap,
                                        signoffPropMap);
                            }
                        }
                        else
                        {
                            loadTaskRow(allRows, childTaskPropMap);
                        }
                    }
                }
            }
            else
            {
                loadTaskRow(allRows, childTaskPropMap);
            }
        }
        for (Vector<Object> vector : allRows)
        {
            m_signOffTable.addRow(vector);
        }
        return false;
    }
    
    private void loadTaskRow(Vector<Vector<Object>> allRows,
            IPropertyMap childTaskPropMap)
    {
        Vector<Object> row = new Vector<Object>();
        row.add(PropertyMapHelper.handleNull(childTaskPropMap
                .getString("task_state")));
        row.add(PropertyMapHelper.handleNull(childTaskPropMap
                .getTag("task_template")));
        
        row.add(PropertyMapHelper.handleNull(childTaskPropMap
                .getString("comments")));
        String startDate = getDateinFormat(childTaskPropMap
                .getDate("fnd0StartDate"));
        row.add(PropertyMapHelper.handleNull(startDate));
        String endDate = getDateinFormat(childTaskPropMap
                .getDate("fnd0EndDate"));
        row.add(PropertyMapHelper.handleNull(endDate));
        row.add(PropertyMapHelper.handleNull(childTaskPropMap
                .getString("resp_party")));
        row.add(PropertyMapHelper.handleNull(childTaskPropMap
                .getString("active_surrogate")));
        allRows.add(row);
    }
    
    private void loadSignoffRow(Vector<Vector<Object>> allRows,
            IPropertyMap childTaskPropMap, IPropertyMap signoffPropMap)
            throws TCException
    {
        Vector<Object> row = new Vector<Object>();
        TCComponentSignoff component = (TCComponentSignoff) signoffPropMap
                .getComponent();
        String decision = component.getDecision().toString();
        
        row.add(PropertyMapHelper.handleNull(decision));
        row.add(PropertyMapHelper.handleNull(childTaskPropMap
                .getTag("task_template")));
        
        row.add(PropertyMapHelper.handleNull(signoffPropMap
                .getString("comments")));
        String creationDate = getDateinFormat(signoffPropMap
                .getDate("creation_date"));
        row.add(PropertyMapHelper.handleNull(creationDate));
        String decisionDate = getDateinFormat(signoffPropMap
                .getDate("decision_date"));
        row.add(PropertyMapHelper.handleNull(decisionDate));
        row.add(PropertyMapHelper.handleNull(signoffPropMap
                .getTag("owning_user")));
        row.add(PropertyMapHelper.handleNull(signoffPropMap
                .getTag("active_surrogate")));
        allRows.add(row);
    }
    
    private String getDateinFormat(Date date)
    {
        SimpleDateFormat ft = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        String formattedDate = "";
        if(date!=null)
        {
            formattedDate= ft.format(date);
        }
        return formattedDate;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getMainComposite();
        createHeaderComposite(mainComposite);
        createSignOffTable(mainComposite);
        return false;
    }
    
    private void createHeaderComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true,
                false, 1, 1);
        composite.setLayoutData(gd_composite);
        composite.setLayout(new GridLayout());
        CLabel label = new CLabel(composite, SWT.NONE) ;
        label.setText(getRegistry().getString("SIGNOFFSUMMARY.Label"));
        SWTUIHelper.setFont(label, SWT.BOLD);     
    }

    private void createSignOffTable(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        GridLayout gl_tCTableComposite = new GridLayout(1, false);
        
        m_signOffTable = createTCTable();
        JScrollPane scrollPane = new JScrollPane(m_signOffTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    
    private TCTable createTCTable()
    {
        String[] columnIdentifiers = getRegistry().getStringArray(
                "SignOffSummaryPanel.ColumnIdentifier");
        String[] columnNames = getRegistry().getStringArray(
                "SignOffSummaryPanel.ColumnNames");
        TCTable tCTable = new TCTable(columnIdentifiers, columnNames);
        tCTable.setShowVerticalLines(true);
        tCTable.setAutoResizeMode(tCTable.AUTO_RESIZE_ALL_COLUMNS);
        tCTable.setEditable(false);
        
        return tCTable;
        
    }
    
    private Composite getMainComposite()
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
}
