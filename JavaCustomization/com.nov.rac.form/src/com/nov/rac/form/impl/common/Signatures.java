package com.nov.rac.form.impl.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class Signatures extends AbstractUIPanel implements INOVFormLoadSave
{
    protected Registry m_registry = null;
    
    private Composite m_column1Composite = null;
    private Label m_draftedByLabel = null;
    private Label m_draftedOnLabel = null;
    private Label m_approvedByLabel = null;
    private Label m_approvedOnLabel = null;
    
    public Signatures(Composite parent, int style)
    {
        super(parent, style);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // Name Labels
        m_draftedByLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString(getRegistry().getString("draftedby.PROP"))));
        
        m_approvedByLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString(getRegistry().getString("approvedby.PROP"))));
        
        // Date Labels
        this.setDate(m_draftedOnLabel, getRegistry()
                .getString("draftedon.PROP"), propMap);
        this.setDate(m_approvedOnLabel,
                getRegistry().getString("approvedon.PROP"), propMap);
        
        m_column1Composite.layout();
        
        return true;
    }
    
    protected void setDate(Label dateLabel, String propertyName,
            IPropertyMap propMap)
    {
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate != null)
        {
            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
            dateLabel.setText(dateFormat.format(theDate));
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, false));
        
        createColumn1Composite(l_composite);
        
        createHorizontalSeparator(m_column1Composite);
        createLabel(m_column1Composite,
                getRegistry().getString("DraftedBy.Label"));
        createDraftedByLabel(m_column1Composite);
        createLabel(m_column1Composite,
                getRegistry().getString("DraftedOn.Label"));
        createDraftedOnLabel(m_column1Composite);
        createLabel(m_column1Composite,
                getRegistry().getString("ApprovedBy.Label"));
        createApprovedByLabel(m_column1Composite);
        createLabel(m_column1Composite,
                getRegistry().getString("ApprovedOn.Label"));
        createApprovedOnLabel(m_column1Composite);
        createHorizontalSeparator(m_column1Composite);
        
        return false;
    }
    
    private void createLabel(Composite composite, String setText)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1,
                1));
        label.setText(setText);
    }
    
    private void createHorizontalSeparator(Composite composite)
    {
        Label horizontalSeparator = new Label(composite, SWT.SEPARATOR
                | SWT.HORIZONTAL);
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, false, 8,
                1);
        horizontalSeparator.setLayoutData(layoutData);
    }
    
    private void createColumn1Composite(Composite composite)
    {
        m_column1Composite = new Composite(composite, SWT.NONE);
        GridLayout gl_composite = new GridLayout(8, false);
        m_column1Composite.setLayout(gl_composite);
        m_column1Composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
                false, 1, 3));
        
    }
    
    private Label createNameLabel(Composite composite)
    {
        Label nameLabel = new Label(composite, SWT.NONE);
        nameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
                1, 1));
        return nameLabel;
    }
   
    private void createDraftedByLabel(Composite composite)
    {
        m_draftedByLabel = createNameLabel(composite);
    }
    
    private void createDraftedOnLabel(Composite composite)
    {
        m_draftedOnLabel = createNameLabel(composite);
    }
    
    private void createApprovedByLabel(Composite composite)
    {
        m_approvedByLabel = createNameLabel(composite);
    }
    
    private void createApprovedOnLabel(Composite composite)
    {
        m_approvedOnLabel = createNameLabel(composite);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    //
    
}
