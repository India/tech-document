package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;

import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ReasonForChangePanel extends AbstractExpandableUIPanel
implements INOVFormLoadSave
{

    private static final int REASON_FOR_CHANGE_SIZE = 100;
    private Text m_reasonForChangeText;
    private Text m_explainationText;
    private Registry m_registry;

    public ReasonForChangePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_explainationText.setText(PropertyMapHelper.handleNull(propMap.getString("nov4_reason_change")));
        //ask rajeev about typetext
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        m_explainationText.setEditable(flag);
        
    }

    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        Composite mainGroup = createMainGroup(composite);
        
        createReasonForChangeText(mainGroup);
        
        createExplanationText(mainGroup);
        getExpandableComposite().setText(getRegistry().getString("ReasonForChange.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);

        return false;
    }

    private void createExplanationText(Composite parent)
    {
        m_explainationText = new Text(parent, SWT.BORDER | SWT.MULTI
                | SWT.WRAP | SWT.V_SCROLL);
        GridData gd_text = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                25);
        m_explainationText.setLayoutData(gd_text);
        m_explainationText.setEnabled(false);
        m_explainationText.setBackground(getComposite().getDisplay().getSystemColor( SWT.COLOR_WHITE));
    }

    protected Composite createMainGroup(Composite composite)
    {
        Composite mainGroup = new Composite(composite,SWT.NONE);
        GridData gd_mainGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        mainGroup.setLayoutData(gd_mainGroup);
        GridLayout gl_mainGroup = new GridLayout(1, true);
        gl_mainGroup.marginHeight = 0;
        gl_mainGroup.marginWidth = 0;
        mainGroup.setLayout(gl_mainGroup);
        return mainGroup;
    }
    
    private void createReasonForChangeText(Composite parent)
    {
        m_reasonForChangeText = new Text(parent,SWT.BORDER);
        GridData gd_reasonForChange = new GridData();
        gd_reasonForChange.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_reasonForChangeText, REASON_FOR_CHANGE_SIZE);
        m_reasonForChangeText.setLayoutData(gd_reasonForChange);
        m_reasonForChangeText.setEnabled(false);
        m_reasonForChangeText.setBackground(getComposite().getDisplay().getSystemColor( SWT.COLOR_WHITE));
    }

    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
