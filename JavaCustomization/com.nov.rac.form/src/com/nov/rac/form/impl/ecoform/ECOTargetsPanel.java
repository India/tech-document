package com.nov.rac.form.impl.ecoform;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.form.util.TreeTable;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECOTargetsPanel extends AbstractUIPanel implements
INOVFormLoadSave
{
    private Registry m_registry=null;
    private Composite m_composite =null;
    private TreeTable m_treeTable = null;
    private String[] m_tableColumnNames = new String[] {};
    private final String m_panelName = getClass().getSimpleName();
    private final String COLUMN_NAMES = "COLUMN_NAMES";
    private final String DELIMITER = ",";
    private String m_emptyString = new String();
    
    public ECOTargetsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    @Override
    public boolean createUI() throws TCException
    {
        m_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        m_composite.setLayoutData(gridData);
        m_composite.setLayout(new GridLayout(6, false));
        
        
        createTargetsLabel(m_composite);
        createValidateButton(m_composite);
        createViewReportButton(m_composite);
        createMassBOMEditButton(m_composite);
        createAddItemButton(m_composite);
        createSetStatus(m_composite);
        createTable(m_composite);
        createTable(m_composite);
        return true;
    }
    

    private void createSetStatus(Composite composite)
    {
        
        Button setStatusButton = new Button(composite, SWT.NONE);
        setStatusButton.setText(getRegistry().getString("ECOTargetsPanel.SetStatusButton"));
        
    }

    private void createAddItemButton(Composite composite)
    {
        Button addItemButton = new Button(composite, SWT.NONE);
        Image editImage = new Image(composite.getDisplay(),
                getRegistry().getImage("editButton.IMAGE"), SWT.NONE);
        addItemButton.setImage(editImage);
        addItemButton.setText(getRegistry().getString("ECOTargetsPanel.AddItemButton"));
        
    }

    private void createMassBOMEditButton(Composite composite)
    {
        Button massBOMEditButton = new Button(composite, SWT.NONE);
        massBOMEditButton.setText(getRegistry().getString("ECOTargetsPanel.MassBOMEdit"));
        
    }

    private void createViewReportButton(Composite composite)
    {
        Button viewReportButton = new Button(composite, SWT.NONE);
        viewReportButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true,
                false, 1, 1));
        viewReportButton.setText(getRegistry().getString("ECOTargetsPanel.ViewReportButton"));
        
    }
    
    

    private void createValidateButton(Composite composite)
    {
        Button btnValidate = new Button(composite, SWT.NONE);
        btnValidate.setText(getRegistry().getString("ECOTargetsPanel.ValidateButton"));
        
    }

    private void createTargetsLabel(Composite composite)
    {
        Label targetsLabel = new Label(composite, SWT.NONE);
        targetsLabel.setText(getRegistry().getString("ECOTargetsPanel.TargetsLabel"));
        SWTUIHelper.setFont(targetsLabel, SWT.BOLD);
        
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    private void createTable(final Composite composite)
    {
        final Composite tabelComposite = new Composite(composite, SWT.BORDER);
        GridData table_gridData = new GridData(SWT.FILL, SWT.CENTER, true,
                true, 6, 1);
        tabelComposite.setLayoutData(table_gridData);
        m_treeTable = new TreeTable(tabelComposite);
        m_treeTable.setBounds(825, 120);
        m_treeTable.addColumns(getTableColumns());
        
        m_treeTable.getTree().addListener(SWT.MouseDoubleClick, new Listener()
        {
            public void handleEvent(Event e)
            {
                e.doit = false;
              //  createDispositionTab();
                System.out.println("double clicked");
            }
        });
    }
    
    protected String[] getTableColumns()
    {
        m_tableColumnNames = getRegistry().getStringArray(
                m_panelName + "." + COLUMN_NAMES, DELIMITER, m_emptyString);
        return m_tableColumnNames;
    }
    
}
