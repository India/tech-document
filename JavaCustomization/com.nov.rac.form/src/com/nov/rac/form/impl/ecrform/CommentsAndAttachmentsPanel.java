package com.nov.rac.form.impl.ecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class CommentsAndAttachmentsPanel  extends AbstractUIPanel implements INOVFormLoadSave
{
	private Registry m_registry = null;
	
	private Text m_commentsTextField = null;
	
	private Text m_attachmentsTextField = null;
	
	//private Button m_addButton = null;
	
	//private Button m_removeButton = null;
	
	private AttachmentsButton m_buttonBar;

	public CommentsAndAttachmentsPanel(Composite parent, int style) 
	{
		super(parent, style);
		m_registry = Registry.getRegistry(this);
	}
	
	protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite composite = getComposite();
    	composite.setLayout(new GridLayout(21, true));
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        composite.setLayoutData(gridData);
        
        createLeftPanel(composite);
        
        createVerticalSeparator(composite);
        
        createRightPanel(composite);
        
        createUIPost();
    	
    	return true;
    }

	private void createLeftPanel(Composite composite) 
	{
		Composite l_composite = new Composite(composite, SWT.NONE);
        l_composite.setLayout(new GridLayout(4, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                10, 1);
        l_composite.setLayoutData(gd_l_composite);
        
        createCommentsRow(l_composite);
        
        createCommentsTextFieldRow(l_composite);
		
	}
	
	private void createCommentsRow(Composite l_composite) 
	{
		
		Label l_lblComments = new Label(l_composite, SWT.NONE);
		l_lblComments.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 4, 1));
		l_lblComments.setText(m_registry.getString("lblComments.NAME",
                "Key Not Found"));
	}

	private void createCommentsTextFieldRow(Composite l_composite) 
	{
		
		m_commentsTextField = new Text(l_composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 3);
        m_commentsTextField.setLayoutData(gridData);
        
	}
	
	private void createVerticalSeparator(Composite composite)
    {
        Label verticalSeparator = new Label(composite, SWT.SEPARATOR
                | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.CENTER, SWT.FILL, true, true, 1,
                1);
        verticalSeparator.setLayoutData(layoutData);
    }

	private void createRightPanel(Composite composite) 
	{
		
		Composite l_composite = new Composite(composite, SWT.NONE);
        l_composite.setLayout(new GridLayout(4, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                10, 1);
        l_composite.setLayoutData(gd_l_composite);
        
        createAttachmentsRow(l_composite);
        
        createAttachmentsTextFieldRow(l_composite);
		
	}

	private void createAttachmentsRow(Composite l_composite) 
	{
		Label l_lblAttachments = new Label(l_composite, SWT.NONE);
		l_lblAttachments.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1));
		l_lblAttachments.setText(m_registry.getString("lblAttachments.NAME",
                "Key Not Found"));
		
		// create plus-minus buttons
        Image pluseButtonImage = new Image(l_composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        Image minusButtonImage = new Image(l_composite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        
        m_buttonBar = new AttachmentsButton(l_composite, SWT.NONE,
                pluseButtonImage, minusButtonImage);
        GridData gridData1 = new GridData(SWT.RIGHT, SWT.FILL, true, true, 1, 1);
        m_buttonBar.setLayoutData(gridData1);
		
		//createAddButton(l_composite);
		
		//createRemoveButton(l_composite);
		
	}
	
	/*private void createAddButton(Composite composite)
    {
        m_addButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        
        Image addButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_addButton.setImage(addButtonImage);
        
        m_addButton.setLayoutData(gridData);
    }
	
	private void createRemoveButton(Composite composite)
    {
        m_removeButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        
        Image addButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        m_removeButton.setImage(addButtonImage);
        
        m_removeButton.setLayoutData(gridData);
    }*/

	private void createAttachmentsTextFieldRow(Composite l_composite) 
	{
		m_attachmentsTextField = new Text(l_composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 3);
        m_attachmentsTextField.setLayoutData(gridData);
		
	}
	
	
	public boolean createUIPost() throws TCException {

		boolean returnValue = true;

		m_commentsTextField.setEditable(false);

		m_attachmentsTextField.setEditable(false);

		m_buttonBar.setEnabled(false);

		return returnValue;
	}
	

	@Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		m_commentsTextField.setText(PropertyMapHelper.handleNull(propMap
				.getString("comments")));
		
		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException 
	{
		return false;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException 
	{
		return false;
	}

	@Override
	public void setEnabled(boolean flag) 
	{
		
	}

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}

}
