package com.nov.rac.form.impl.supersedeform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.DescriptionPanel;
import com.teamcenter.rac.util.Registry;

/**
 * @author mishalt
 * 
 */
public class ReasonForSupersedePanel extends DescriptionPanel
{
    public ReasonForSupersedePanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
