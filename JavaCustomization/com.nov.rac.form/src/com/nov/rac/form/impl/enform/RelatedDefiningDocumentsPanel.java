package com.nov.rac.form.impl.enform;

import javax.swing.ListSelectionModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.framework.tablecomparator.impl.ComparatorDataModel;
import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;
import com.nov.rac.framework.tablecomparator.impl.ItemComparator;
import com.nov.rac.framework.tablecomparator.impl.TablePainter;
import com.nov.rac.item.itemsearch.SearchResultBean;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class RelatedDefiningDocumentsPanel extends DocumentsPanel implements
        ITableComparatorSubscriber, ISubscriber
{
    private static int TABLE_VERTICAL_SPAN = 1;
    private TCComponent m_tcComponent = null;
    private ITableComparator m_iTableComparator = null;
    private static final String RDD_CONTEXT = "RDD";
    private static final String SEARCH_RESULT = "SEARCH_RESULT";
    
    public RelatedDefiningDocumentsPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        setTableVerticalSpan(TABLE_VERTICAL_SPAN);
        setAddButtonSelection(ListSelectionModel.SINGLE_SELECTION);
        
        super.createUI();
        final String label = getRegistry().getString(
                "RelatedDefiningDocumentPanel.GroupContainerText");
        // getGroupContainer().setText(label);
        
        getGroupContainer().addPaintListener(new PaintListener()
        {
            @Override
            public void paintControl(PaintEvent e)
            {
                e.gc.drawImage(TCTypeRenderer.getTypeImage("Documents", null),
                        10, 0);
                e.gc.drawString(label,
                        TCTypeRenderer.getTypeImage("Documents", null)
                                .getBounds().width + 15, 0);
            }
        });
        
        setAddButtonText("");
        setAddButtonText(getRegistry().getString("AddReplace.Button"));
        // setRemoveButtonText(getRegistry().getString("Feedback.Button"));        
        registerSubscriber(SEARCH_RESULT, this);
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponent tcComponent = propMap.getComponent();
        m_tcComponent = tcComponent;
        setRelation("RelatedDefiningDocument");
        super.load(propMap);
        // Select RDD if present
        if (getTable().getRowCount() > 0)
        {
            int rowIndex = 0;
            int colIndex = 0;
            getTable().changeSelection(rowIndex, colIndex, false, false);
        }
        setTableColumnsWidth(getTable());
        makeLatestRevisionEditable(m_tcComponent);
        if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            initializeComparision();
        }
        return true;
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    protected void setTableVerticalSpan(int verticalSpan)
    {
        super.setTableVerticalSpan(verticalSpan);
    }
    
    private void initializeComparision()
    {
        String[] tableColumnNames = getRegistry().getStringArray(
                "DocumentTable.CompareColumns");
        
        IDataModel model = new ComparatorDataModel();
        model.setPrimaryKeyColumn("item_id");
        model.setTableColumns(tableColumnNames);
        model.setTable(getTable());
        model.setComparator(new ItemComparator());
        model.setCompareObject(m_tcComponent);
        model.setSubscriber(this);
        
        m_iTableComparator = DefaultTableComparator.getController();
        m_iTableComparator.setContext(RDD_CONTEXT, model);
        m_iTableComparator.compareTable(RDD_CONTEXT);
    }
    
    @Override
    public void dispose()
    {
        if (m_iTableComparator != null)
        {
            m_iTableComparator.unregisterToComparator(RDD_CONTEXT);
        }
        unregisterSubscriber(SEARCH_RESULT, this);
        super.dispose();
    }
    
    @Override
    public void updateTable(PublishEvent event)
    {
        CompareOutputBean bean = null;
        TablePainter painter = null;
        if (event != null)
        {
            bean = (CompareOutputBean) event.getNewValue();
            
            if (event.getPropertyName().equals(RDD_CONTEXT)
                    && this == bean.getObject())
            {
                painter = new TablePainter(bean);
            }
        }
        else
        {
            painter = new TablePainter(bean);
        }
        painter.paintTable(getTable());
    }
    
    @Override
    public void update(PublishEvent event)
    {
        String eventContext = (String) event.getPropertyName();
        SearchResultBean resultBean = (SearchResultBean) event.getNewValue();
        
        if (SEARCH_RESULT.equals(eventContext)
                && resultBean.getSource() == getButtonComposite())
        {
            TCComponent[] secondaryObjects = (TCComponent[]) resultBean
                    .getSearchResult();
            deleteRDD();
            createRelation(m_tcComponent, secondaryObjects);
        }
    }
    
    private void deleteRDD()
    {
        try
        {
            int rowCnt = getTable().getRowCount();
            if (rowCnt > 0)
            {
                int row = 0;
                deleteObject(m_tcComponent, row);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void makeLatestRevisionEditable(TCComponent tcComponent)
    {
        if ((GlobalConstant.COMPARE_NONCOMPARE_FLAG && !FormHelper
                .isLatestItemRevision(tcComponent))
                || !FormHelper.hasWriteAccess(tcComponent))
        {
            m_addButton.setEnabled(false);
        }
        else
        {
            m_addButton.setEnabled(true);
        }
    }
    
    @Override
    protected void createButtons()
    {
        createButtonsComposite();
        createFeedBackButton(getButtonComposite());
        createAddButton(getButtonComposite());
    }
    
}
