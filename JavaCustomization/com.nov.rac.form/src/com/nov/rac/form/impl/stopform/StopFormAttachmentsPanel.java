package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.AttachmentsPanel;
import com.teamcenter.rac.util.Registry;

public class StopFormAttachmentsPanel extends AttachmentsPanel
{

    public StopFormAttachmentsPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
