package com.nov.rac.form.impl.mbcform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class MBCSignaturesPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private Composite m_column1Composite = null;
    private Label m_initiatedByLabel = null;
    private Label m_initiatedOnLabel = null;
    private Label m_owningGroupLabel = null;
    
    private static final int NUMBER_OF_COLUMNS = 6;
    private static final int HORIZONTAL_SPAN = 8;
    private static final int VERTICAL_SPAN = 3;
    
    public MBCSignaturesPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // Name Labels
        m_initiatedByLabel.setText(PropertyMapHelper.handleNull(propMap.getTag(getRegistry().getString(
                "OwningUser.PROP"))));
        
        m_owningGroupLabel.setText(PropertyMapHelper.handleNull(propMap.getTag(getRegistry().getString(
                "OwningGroup.PROP"))));
        
        // Date Labels
        this.setDate(m_initiatedOnLabel, getRegistry().getString("CreationDate.PROP"), propMap);
        
        m_column1Composite.layout();
        
        return true;
    }
    
    protected void setDate(Label dateLabel, String propertyName, IPropertyMap propMap)
    {
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate != null)
        {
            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
            dateLabel.setText(dateFormat.format(theDate));
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, false));
        
        createColumnComposite(l_composite);
        
        createHorizontalSeparator(m_column1Composite);
        
        createLabel(m_column1Composite, getRegistry().getString("InitiatedBySign.Label"));
        createInitiatedByLabel(m_column1Composite);
        
        createLabel(m_column1Composite, getRegistry().getString("InitiatedOnSign.Label"));
        createInitiatedOnLabel(m_column1Composite);
        
        createLabel(m_column1Composite, getRegistry().getString("OwningGroupSign.Label"));
        createOwningGroupLabel(m_column1Composite);
        
        createHorizontalSeparator(m_column1Composite);
        
        return false;
    }
    
    private void createLabel(Composite composite, String setText)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        label.setText(setText);
    }
    
    private void createHorizontalSeparator(Composite composite)
    {
        Label horizontalSeparator = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, false, HORIZONTAL_SPAN, 1);
        horizontalSeparator.setLayoutData(layoutData);
    }
    
    private void createColumnComposite(Composite composite)
    {
        m_column1Composite = new Composite(composite, SWT.NONE);
        GridLayout gl_composite = new GridLayout(NUMBER_OF_COLUMNS, false);
        m_column1Composite.setLayout(gl_composite);
        m_column1Composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, VERTICAL_SPAN));
        
    }
    
    private Label createNameLabel(Composite composite)
    {
        Label nameLabel = new Label(composite, SWT.NONE);
        nameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        SWTUIHelper.setFont(nameLabel, SWT.BOLD);
        return nameLabel;
    }
    
    private void createInitiatedByLabel(Composite composite)
    {
        m_initiatedByLabel = createNameLabel(composite);
    }
    
    private void createInitiatedOnLabel(Composite composite)
    {
        m_initiatedOnLabel = createNameLabel(composite);
    }
    
    private void createOwningGroupLabel(Composite composite)
    {
        m_owningGroupLabel = createNameLabel(composite);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
