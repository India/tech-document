package com.nov.rac.form.impl.obsoleteform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.common.TargetsTablePanel;
import com.nov.rac.form.impl.common.TargetsTableTopPanelWithoutAddRemove;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ObsoleteFormTargetsTablePanel extends TargetsTablePanel
{
   
    private TargetsTableTopPanelWithoutAddRemove m_topPanel;
    public ObsoleteFormTargetsTablePanel(Composite parent, int style)
    {
        super(parent, style);
        setRegistry(Registry.getRegistry(this));
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        createTopPanel(composite);
        m_topPanel.addExportButtonListener(m_table);
        super.createUI();
    	return true;
    }
    

    private void createTopPanel(Composite composite)
    {
        m_topPanel = new TargetsTableTopPanelWithoutAddRemove(composite, SWT.NONE);
        m_topPanel.setRegistry(getRegistry());
        try
        {
            m_topPanel.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    protected void addDoubleClickListener(final Composite composite)
    {
    	
    }
    @Override
    protected void createTableInstance(String[] columnNames,
            String[] columnIdentifiers)
    {
        m_table = new ObsoleteTargetsTable(columnIdentifiers, columnNames,
                getRegistry());
    }
    
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
    	super.load(propMap);
    	IPropertyMap[] itemRevisionsAry = propMap.getCompoundArray("targetsItemRevision");
    	int lineNo_colNo = TableUtils.getColumnIndex("Line_number", m_table);
        for (int i = 0; i < itemRevisionsAry.length; i++)
        {
            int rowNo = i;
            String lineNo = Integer.toString(++rowNo);
            m_table.setValueAt(lineNo, i, lineNo_colNo);
        }
        return true;
    
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
    	String[] excludeColumnIds = getRegistry().getStringArray(
                "TargetsTable.Disposition.EXCLUDE_COLUMN_IDS");
        IPropertyMap[] dispositionPropMap = TableUtils.populateMap(m_table,
                excludeColumnIds);
        
        propMap.setCompoundArray(getRegistry().getString("targetDisposition.PROP"), dispositionPropMap);
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        super.setEnabled(flag);
        
    }

   
}
