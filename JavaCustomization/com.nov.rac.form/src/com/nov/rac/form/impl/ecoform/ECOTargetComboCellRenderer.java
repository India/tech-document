package com.nov.rac.form.impl.ecoform;

import java.util.Arrays;
import java.util.Vector;

import com.nov.rac.form.renderer.ComboBoxCellRenderer;

public class ECOTargetComboCellRenderer extends ComboBoxCellRenderer
{
    private static String[] listOfValues = { "", "Engineering","Standard","Phase Out","Obsolete"};
    private static String m_emptyString = "";
    
    public ECOTargetComboCellRenderer()
    {
        this(m_emptyString);
    }
    
    public ECOTargetComboCellRenderer(String theLOVName)
    {
        super(theLOVName);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected Vector<String> loadLOV()
    {
        Vector<String> lovValues = new Vector<String>(Arrays.asList(listOfValues));
        return lovValues;
    }
    
}
