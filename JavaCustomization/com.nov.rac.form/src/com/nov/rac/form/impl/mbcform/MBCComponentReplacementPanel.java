package com.nov.rac.form.impl.mbcform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.Registry;

public class MBCComponentReplacementPanel extends Composite
{
    private Registry m_registry = null;
    private Text m_partID = null;
    private Text m_altID = null;
    private Text m_partName = null;
    private Text m_itemStatus = null;
    private Text m_description = null;
    private Text m_revision = null;
    private Text m_revisionStatus = null;
    private Text m_owner = null;
    private Text m_owningGroup = null;
    private String m_labelName;
    private final static int NUM_OF_COLUMNS_REPL_COMP = 4;
    private final static int LABEL_VERTICAL_SPAN = 6;
    private final static int DESC_TXT_HEIGHT_HINT = 40;
    private final static int REPL_COMP_HORIZONTAL_SPAN = 10;
    private final static int HORIZONTAL_SPAN_3 = 3;
    private final static int HORIZONTAL_SPAN_4 = 4;
    
    public MBCComponentReplacementPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        setLayout();
    }
    
    private void setLayout()
    {
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, false, REPL_COMP_HORIZONTAL_SPAN, 1);
        this.setLayout(new GridLayout(NUM_OF_COLUMNS_REPL_COMP, false));
        this.setLayoutData(gd_l_composite);
    }
    
    public void setTopPanelLabel(String labelName)
    {
        m_labelName = labelName;
    }
    
    public void createUI()
    {
        createComponentPartLabel();
        
        createCompPartIDRow();
        
        createCompAltIDRow();
        
        createCompPartNameRow();
        
        createCompItemStatusRow();
        
        createCompDescriptionRow();
        
        createCompRevisionRow();
        
        createCompOwnerRow();
        
        createCompOwningGroupRow();
        
        createUIPost();
    }
    
    private void createComponentPartLabel()
    {
        Label l_lblCompPartToReplace = new Label(this, SWT.NONE);
        GridData gd_lblCompPartToReplace = new GridData(SWT.LEFT, SWT.TOP, false, false, HORIZONTAL_SPAN_4, 1);
        gd_lblCompPartToReplace.verticalSpan = LABEL_VERTICAL_SPAN;
        
        l_lblCompPartToReplace.setLayoutData(gd_lblCompPartToReplace);
        l_lblCompPartToReplace.setText(m_labelName);
        SWTUIHelper.setFont(l_lblCompPartToReplace, SWT.BOLD);
    }
    
    private void createCompPartIDRow()
    {
        Label l_lblPartID = new Label(this, SWT.NONE);
        l_lblPartID.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblPartID.setText(m_registry.getString("PartID.Label", "Key Not Found"));
        
        m_partID = new Text(this, SWT.BORDER);
        GridData gd_txtPartID = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_3, 1);
        m_partID.setLayoutData(gd_txtPartID);
    }
    
    private void createCompAltIDRow()
    {
        Label l_lblAltID = new Label(this, SWT.NONE);
        l_lblAltID.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblAltID.setText(m_registry.getString("AltID.Label", "Key Not Found"));
        
        m_altID = new Text(this, SWT.BORDER);
        GridData gd_txtAltID = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_3, 1);
        m_altID.setLayoutData(gd_txtAltID);
    }
    
    private void createCompPartNameRow()
    {
        Label l_lblPartName = new Label(this, SWT.NONE);
        l_lblPartName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblPartName.setText(m_registry.getString("PartName.Label", "Key Not Found"));
        
        m_partName = new Text(this, SWT.BORDER);
        GridData gd_txtPartName = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_3, 1);
        m_partName.setLayoutData(gd_txtPartName);
    }
    
    private void createCompItemStatusRow()
    {
        Label l_lblItemStatus = new Label(this, SWT.NONE);
        l_lblItemStatus.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblItemStatus.setText(m_registry.getString("ItemStatus.Label", "Key Not Found"));
        
        m_itemStatus = new Text(this, SWT.BORDER);
        GridData gd_txtItemStatus = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_itemStatus.setLayoutData(gd_txtItemStatus);
        
        new Label(this, SWT.NONE);
        new Label(this, SWT.NONE);
    }
    
    private void createCompDescriptionRow()
    {
        Label l_lblDesription = new Label(this, SWT.NONE);
        l_lblDesription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
        l_lblDesription.setText(m_registry.getString("Description.Label", "Key Not Found"));
        
        m_description = new Text(this, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
        GridData gd_txtDesription = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_3, 1);
        gd_txtDesription.heightHint = DESC_TXT_HEIGHT_HINT;
        m_description.setLayoutData(gd_txtDesription);
    }
    
    private void createCompRevisionRow()
    {
        Label l_lblRevision = new Label(this, SWT.NONE);
        l_lblRevision.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblRevision.setText(m_registry.getString("Revision.Label", "Key Not Found"));
        
        m_revision = new Text(this, SWT.BORDER);
        GridData gd_txtRevision = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_revision.setLayoutData(gd_txtRevision);
        
        Label l_lblRevStatus = new Label(this, SWT.NONE);
        l_lblRevStatus.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblRevStatus.setText(m_registry.getString("RevisionStatus.Label", "Key Not Found"));
        
        m_revisionStatus = new Text(this, SWT.BORDER);
        GridData gd_txtRevStatus = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_revisionStatus.setLayoutData(gd_txtRevStatus);
    }
    
    private void createCompOwnerRow()
    {
        Label l_lblOwner = new Label(this, SWT.NONE);
        l_lblOwner.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblOwner.setText(m_registry.getString("Owner.Label", "Key Not Found"));
        
        m_owner = new Text(this, SWT.BORDER);
        GridData gd_txtOwner = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_3, 1);
        m_owner.setLayoutData(gd_txtOwner);
    }
    
    private void createCompOwningGroupRow()
    {
        Label l_lblOwningGroup = new Label(this, SWT.NONE);
        l_lblOwningGroup.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblOwningGroup.setText(m_registry.getString("OwningGroup.Label", "Key Not Found"));
        
        m_owningGroup = new Text(this, SWT.BORDER);
        GridData gd_txtOwningGroup = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTAL_SPAN_3, 1);
        m_owningGroup.setLayoutData(gd_txtOwningGroup);
    }
    
    private boolean createUIPost()
    {
        m_partID.setEditable(false);
        m_altID.setEditable(false);
        m_partName.setEditable(false);
        m_itemStatus.setEditable(false);
        m_description.setEditable(false);
        m_revision.setEditable(false);
        m_revisionStatus.setEditable(false);
        m_owner.setEditable(false);
        m_owningGroup.setEditable(false);
        return true;
    }
    
    public Text getPartIDText()
    {
        return m_partID;
    }
    
    public Text getAltIDText()
    {
        return m_altID;
    }
    
    public Text getPartNameTxt()
    {
        return m_partName;
    }
    
    public Text getItemStatusText()
    {
        return m_itemStatus;
    }
    
    public Text getDescriptionText()
    {
        return m_description;
    }
    
    public Text getRevisionText()
    {
        return m_revision;
    }
    
    public Text getRevisionStatusText()
    {
        return m_revisionStatus;
    }
    
    public Text getOwnerText()
    {
        return m_owner;
    }
    
    public Text getOwningGroupText()
    {
        return m_owningGroup;
    }
    
}
