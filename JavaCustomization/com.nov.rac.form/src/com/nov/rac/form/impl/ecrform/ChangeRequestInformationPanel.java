package com.nov.rac.form.impl.ecrform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ChangeRequestInformationPanel extends AbstractUIPanel implements INOVFormLoadSave
{
	private Registry m_registry = null;
	
	private Text m_textECRNumber = null;
	
	private Text m_textECRStatus = null;
	
	private Text m_textRequestor = null;
	
	private Text m_textRequestDate = null;
	
	private Text m_textDistribution = null;
	
	//private Button m_addEmailButton = null;
    
    //private Button m_globalAddressButton = null;
    
    //private Text m_distributionTextField = null;
	
	private List m_distributionList;
    
    private Text m_changeDescriptionTextField = null;
    
    private Text m_textReasonForChange = null;
    
    private Text m_textTypeOfChange = null;
    
    private Text m_textProductLine = null;
    
    private AttachmentsButton m_buttonBar;

	public ChangeRequestInformationPanel(Composite parent, int style) 
	{
		super(parent, style);
		m_registry = Registry.getRegistry(this);
	}
	
	protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite composite = getComposite();
    	composite.setLayout(new GridLayout(21, true));
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        composite.setLayoutData(gridData);
        
        createLeftPanel(composite);
        
        createVerticalSeparator(composite);
        
        createRightPanel(composite);
        
        createUIPost();
    	
    	return true;
    }
    
    private void createLeftPanel(Composite composite) 
    {
    	Composite l_composite = new Composite(composite, SWT.NONE);
        l_composite.setLayout(new GridLayout(4, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                10, 1);
        l_composite.setLayoutData(gd_l_composite);
        
        createChangeRequestInformation(l_composite);
        
        createECRNumberRow(l_composite);
        
        createRequestorRow(l_composite);
        
        createRequestDateRow(l_composite);
        
        createDistributionRow(l_composite);
        
        createDistributionTextFieldRow(l_composite);
	}

	private void createChangeRequestInformation(Composite l_composite) 
	{
		
		Label l_lblChangeReqInfo = new Label(l_composite, SWT.NONE);
		l_lblChangeReqInfo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		l_lblChangeReqInfo.setText(m_registry.getString("lblChangeReqInfo.NAME",
                "Key Not Found"));
		SWTUIHelper.setFont(l_lblChangeReqInfo, SWT.BOLD);
		
	}

	private void createECRNumberRow(Composite l_composite) 
	{
		Label l_lblECRNumber = new Label(l_composite, SWT.NONE);
		l_lblECRNumber.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblECRNumber.setText(m_registry.getString("lblECRNumber.NAME",
                "Key Not Found"));
		
		m_textECRNumber = new Text(l_composite, SWT.BORDER);
        GridData gd_txtECRNumber = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_textECRNumber.setLayoutData(gd_txtECRNumber);
        
        Label l_lblECRStatus = new Label(l_composite, SWT.NONE);
        l_lblECRStatus.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblECRStatus.setText(m_registry.getString("lblECRStatus.NAME",
                "Key Not Found"));
        
        m_textECRStatus = new Text(l_composite, SWT.BORDER);
        GridData gd_textECRStatus = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_textECRStatus.setLayoutData(gd_textECRStatus);
		
	}
	
	private void createRequestorRow(Composite l_composite) 
	{
		
		Label l_lblRequestor = new Label(l_composite, SWT.NONE);
		l_lblRequestor.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblRequestor.setText(m_registry.getString("lblRequestor.NAME",
                "Key Not Found"));
		
		m_textRequestor = new Text(l_composite, SWT.BORDER);
        GridData gd_txtRequestor = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
        m_textRequestor.setLayoutData(gd_txtRequestor);
		
	}
	
	private void createRequestDateRow(Composite l_composite) 
	{
		
		Label l_lblRequestDate = new Label(l_composite, SWT.NONE);
		l_lblRequestDate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblRequestDate.setText(m_registry.getString("lblRequestDate.NAME",
                "Key Not Found"));
		
		m_textRequestDate = new Text(l_composite, SWT.BORDER);
        GridData gd_txtRequestDate = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        m_textRequestDate.setLayoutData(gd_txtRequestDate);
        
        new Label(l_composite, SWT.NONE);
        new Label(l_composite, SWT.NONE);
		
	}
	
	private void createDistributionRow(Composite l_composite) 
	{
		
		Label l_lblDistribution = new Label(l_composite, SWT.NONE);
		l_lblDistribution.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblDistribution.setText(m_registry.getString("lblDistribution.NAME",
                "Key Not Found"));
		
		m_textDistribution = new Text(l_composite, SWT.BORDER);
        GridData gd_txtRequestDate = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
        m_textDistribution.setLayoutData(gd_txtRequestDate);
 
        // create Add Email and Global Address buttons
        Image pluseButtonImage = new Image(l_composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        Image globalAddrButtonImage = new Image(l_composite.getDisplay(),
                getRegistry().getImage("globalAddressButton.IMAGE"), SWT.NONE);
        
        m_buttonBar = new AttachmentsButton(l_composite, SWT.NONE,
                pluseButtonImage, globalAddrButtonImage);
        GridData gridData1 = new GridData(SWT.RIGHT, SWT.FILL, true, true, 1, 1);
        m_buttonBar.setLayoutData(gridData1);
        
        //createAddEmailButton(l_composite);
        
        //createGlobalAddressButton(l_composite);
        
    }
    
	/*private void createAddEmailButton(Composite composite)
    {
        m_addEmailButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        
        Image addEmailButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_addEmailButton.setImage(addEmailButtonImage);
        
        m_addEmailButton.setLayoutData(gridData);
    }
    
	private void createGlobalAddressButton(Composite composite)
    {
        m_globalAddressButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        
        Image addressButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("globalAddressButton.IMAGE"), SWT.NONE);
        m_globalAddressButton.setImage(addressButtonImage);
        
        m_globalAddressButton.setLayoutData(gridData);
    }*/
    
    private void createDistributionTextFieldRow(Composite l_composite) 
	{
		
		/*m_distributionTextField = new Text(l_composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 4);
        m_distributionTextField.setLayoutData(gridData);*/
        
        
        m_distributionList = new List(l_composite, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL | SWT.WRAP);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 4);
		m_distributionList.setLayoutData(gridData);
		
	}
    
    private void createVerticalSeparator(Composite composite)
    {
        Label verticalSeparator = new Label(composite, SWT.SEPARATOR
                | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.CENTER, SWT.FILL, false, true, 1,
                1);
        verticalSeparator.setLayoutData(layoutData);
    }

	private void createRightPanel(Composite composite) 
	{
		Composite l_composite = new Composite(composite, SWT.NONE);
        l_composite.setLayout(new GridLayout(4, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                10, 1);
        l_composite.setLayoutData(gd_l_composite);
        
        Label l_lblEmpty = new Label(l_composite, SWT.NONE);
        l_lblEmpty.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 4, 1));
        
        createProductLineRow(l_composite);
        
        createTypeOfChangeRow(l_composite);
        
        createReasonForChangeRow(l_composite);
        
        createChangeDescriptionRow(l_composite);
        
        createChangeDescriptionTextFieldRow(l_composite); 
		
	}

	private void createProductLineRow(Composite l_composite) 
	{
		
		Label l_lblProductLine = new Label(l_composite, SWT.NONE);
		l_lblProductLine.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblProductLine.setText(m_registry.getString("lblProductLine.NAME",
                "Key Not Found"));
		
		m_textProductLine = new Text(l_composite, SWT.BORDER);
        GridData gd_txtProductLine = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
        m_textProductLine.setLayoutData(gd_txtProductLine);
		
	}

	private void createTypeOfChangeRow(Composite l_composite) 
	{

		Label l_lblTypeOfChange = new Label(l_composite, SWT.NONE);
		l_lblTypeOfChange.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblTypeOfChange.setText(m_registry.getString("lblTypeOfChange.NAME",
                "Key Not Found"));
		
		m_textTypeOfChange = new Text(l_composite, SWT.BORDER);
        GridData gd_txtTypeOfChange = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
        m_textTypeOfChange.setLayoutData(gd_txtTypeOfChange);
		
	}

	private void createReasonForChangeRow(Composite l_composite) 
	{
		
		Label l_lblReasonForChange = new Label(l_composite, SWT.NONE);
		l_lblReasonForChange.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblReasonForChange.setText(m_registry.getString("lblReasonForChange.NAME",
                "Key Not Found"));
		
		m_textReasonForChange = new Text(l_composite, SWT.BORDER);
        GridData gd_txtReasonForChange = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
        m_textReasonForChange.setLayoutData(gd_txtReasonForChange);
		
	}

	private void createChangeDescriptionRow(Composite l_composite) 
	{
		
		Label l_lblChangeDescription = new Label(l_composite, SWT.NONE);
		l_lblChangeDescription.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 4, 1));
		l_lblChangeDescription.setText(m_registry.getString("lblChangeDescription.NAME",
                "Key Not Found"));
		
	}

	private void createChangeDescriptionTextFieldRow(Composite l_composite) 
	{
		m_changeDescriptionTextField = new Text(l_composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 4, 4);
        m_changeDescriptionTextField.setLayoutData(gridData);
        
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		m_textECRNumber.setText(PropertyMapHelper.handleNull(propMap
                .getString("object_name")));
		m_textECRStatus.setText(PropertyMapHelper.handleNull(propMap
                .getString("ecr_status")));
		m_textRequestor.setText(PropertyMapHelper.handleNull(propMap
                .getString("requested_by")));
		
		// Date Labels
        this.setDate(m_textRequestDate, "requested_date", propMap);
		
		m_distributionList.setItems(PropertyMapHelper.handleNull(propMap
				.getStringArray("distribution")));
		
		m_textProductLine.setText(PropertyMapHelper.handleNull(propMap
				.getString("nov4_product_line")));
		m_textTypeOfChange.setText(PropertyMapHelper.handleNull(propMap
				.getString("change_type")));
		m_textReasonForChange.setText(PropertyMapHelper.handleNull(propMap
				.getString("reason_for_change")));
		m_changeDescriptionTextField.setText(PropertyMapHelper.handleNull(propMap
				.getString("change_desc")));
		
		return true;
	}
	
	private void setDate(Text dateText, String propertyName,
            IPropertyMap propMap)
    {
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate == null)
        {
            // do something?ask input form kishore:can write NA?or -?
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateText.setText(dateFormat.format(theDate));
        }
    }
	
	
	public boolean createUIPost() throws TCException {

		boolean returnValue = true;

		m_textECRNumber.setEditable(false);

		m_textECRStatus.setEditable(false);

		m_textRequestor.setEditable(false);

		m_textRequestDate.setEditable(false);

		m_textDistribution.setEditable(false);

		m_buttonBar.setEnabled(false);

		m_distributionList.setEnabled(false);

		m_textProductLine.setEditable(false);

		m_textTypeOfChange.setEditable(false);

		m_textReasonForChange.setEditable(false);

		m_changeDescriptionTextField.setEditable(false);

		return returnValue;
	}
	

	@Override
	public boolean save(IPropertyMap propMap) throws TCException 
	{
		return false;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException 
	{
		return false;
	}

	@Override
	public void setEnabled(boolean flag) 
	{
		
	}

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}

}
