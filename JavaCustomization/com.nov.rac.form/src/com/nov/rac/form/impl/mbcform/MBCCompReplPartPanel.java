package com.nov.rac.form.impl.mbcform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class MBCCompReplPartPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    private Registry m_registry = null;
    private MBCComponentReplacementPanel compPartToReplacePanel;
    private MBCComponentReplacementPanel replacementPartPanel;
    private final static int NUM_OF_COLUMNS = 21;
    
    public MBCCompReplPartPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite composite = getComposite();
        composite.setLayout(new GridLayout(NUM_OF_COLUMNS, true));
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        composite.setLayoutData(gridData);
        
        createCompPartToReplacePanel(composite);
        
        createVerticalSeparator(composite);
        
        createReplacementPartPanel(composite);
        
        return true;
    }
    
    private void createCompPartToReplacePanel(Composite composite)
    {
        compPartToReplacePanel = new MBCComponentReplacementPanel(composite, SWT.NONE );
        compPartToReplacePanel.setTopPanelLabel(m_registry.getString("ComponentPartToReplace.Label"));
        compPartToReplacePanel.createUI();
    }
    
    private void createVerticalSeparator(Composite composite)
    {
        Label verticalSeparator = new Label(composite, SWT.SEPARATOR | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.CENTER, SWT.FILL, false, true, 1, 1);
        verticalSeparator.setLayoutData(layoutData);
    }
    
    private void createReplacementPartPanel(Composite composite)
    {
        replacementPartPanel = new MBCComponentReplacementPanel(composite, SWT.NONE);
        replacementPartPanel.setTopPanelLabel(m_registry.getString("ReplacementPartSurvivor.Label"));
        replacementPartPanel.createUI();
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap compDataAry = propMap.getCompound(m_registry.getString("CompPartToRepl.PROP"));
        IPropertyMap replDataAry = propMap.getCompound(m_registry.getString("ReplacementPart.PROP"));
        
        compPartToReplacePanel.getPartIDText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_item_id")));
        replacementPartPanel.getPartIDText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_item_id")));
        
        compPartToReplacePanel.getAltIDText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_alt_id")));
        replacementPartPanel.getAltIDText().setText(PropertyMapHelper.handleNull(replDataAry.getString("nov4_alt_id")));
        
        compPartToReplacePanel.getPartNameTxt().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_name")));
        replacementPartPanel.getPartNameTxt().setText(PropertyMapHelper.handleNull(replDataAry.getString("nov4_name")));
        
        compPartToReplacePanel.getItemStatusText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_item_status")));
        replacementPartPanel.getItemStatusText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_item_status")));
        
        compPartToReplacePanel.getDescriptionText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_description")));
        replacementPartPanel.getDescriptionText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_description")));
        
        compPartToReplacePanel.getRevisionText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_revision_id")));
        replacementPartPanel.getRevisionText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_revision_id")));
        
        compPartToReplacePanel.getRevisionStatusText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_revision_status")));
        replacementPartPanel.getRevisionStatusText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_revision_status")));
        
        compPartToReplacePanel.getOwnerText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_owning_user")));
        replacementPartPanel.getOwnerText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_owning_user")));
        
        compPartToReplacePanel.getOwningGroupText().setText(
                PropertyMapHelper.handleNull(compDataAry.getString("nov4_owning_group")));
        replacementPartPanel.getOwningGroupText().setText(
                PropertyMapHelper.handleNull(replDataAry.getString("nov4_owning_group")));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
}
