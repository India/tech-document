package com.nov.rac.form.impl.supersedeform;

import java.util.Arrays;
import java.util.Vector;

import com.nov.rac.form.renderer.ComboBoxCellRenderer;

public class SupersedeTargetComboCellRenderer extends ComboBoxCellRenderer
{
    private static String[] listOfValues = { "", "Superseded" };
    private static String m_emptyString = "";
    
    public SupersedeTargetComboCellRenderer()
    {
        this(m_emptyString);
    }
    
    public SupersedeTargetComboCellRenderer(String theLOVName)
    {
        super(theLOVName);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected Vector<String> loadLOV()
    {
        Vector<String> lovValues = new Vector<String>(Arrays.asList(listOfValues));
        return lovValues;
    }
    
}
