package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class MfgReviewDocControlPanel  extends AbstractUIPanel implements
INOVFormLoadSave , IPublisher
{

    private static final int COLUMNS = 5;
    private Button m_mfgReviewReqCheckbox;
    private Button m_docCntrlReqCheckbox;
    private Registry m_registry;
    public MfgReviewDocControlPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }

    protected Registry getRegistry()
    {
        return m_registry;
    }
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_mfgReviewReqCheckbox.setSelection(propMap
              .getLogical("mfgreviewrequired"));
      m_docCntrlReqCheckbox.setSelection(propMap
              .getLogical("nov4_doccontrol_required"));
        return false;
    }
    

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setLogical("mfgreviewrequired",
                m_mfgReviewReqCheckbox.getSelection());
        propMap.setLogical("nov4_doccontrol_required",
                m_docCntrlReqCheckbox.getSelection());
        return true;
    }
    private void addCheckBoxListeners()
    {
        m_mfgReviewReqCheckbox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                if (m_mfgReviewReqCheckbox.getSelection())
                {
                    m_docCntrlReqCheckbox.setEnabled(false);
                }
                else
                {
                    m_docCntrlReqCheckbox.setEnabled(true);
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
        m_docCntrlReqCheckbox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                if (m_docCntrlReqCheckbox.getSelection())
                {
                    m_mfgReviewReqCheckbox.setEnabled(false);
                }
                else
                {
                    m_mfgReviewReqCheckbox.setEnabled(true);
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
        
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        if (flag)
        {
            if (m_mfgReviewReqCheckbox.getSelection())
            {
                m_mfgReviewReqCheckbox.setEnabled(true);
                m_docCntrlReqCheckbox.setEnabled(false);
            }
            else if (m_docCntrlReqCheckbox.getSelection())
            {
                m_mfgReviewReqCheckbox.setEnabled(false);
                m_docCntrlReqCheckbox.setEnabled(true);
            }
            else
            {
                m_mfgReviewReqCheckbox.setEnabled(true);
                m_docCntrlReqCheckbox.setEnabled(true);
            }
        }
        else
        {
            m_mfgReviewReqCheckbox.setEnabled(false);
            m_docCntrlReqCheckbox.setEnabled(false);
        }
        
    }

    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, true));
        createCheckBoxComposite(l_composite);
        addCheckBoxListeners();
        createDataModelMapping();
        return false;
    }

    private void createCheckBoxComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(COLUMNS, false));
        
        m_mfgReviewReqCheckbox = createButton(composite,SWT.CHECK,"ManufacturerReviewRequired.Lable");
        
        // crate document control required check box
        m_docCntrlReqCheckbox =createButton(composite,SWT.CHECK,"DocumentControlRequired.Lable");
        GridData gridData2 = new GridData(SWT.FILL, SWT.FILL, true, false, 1,
                1);
        m_docCntrlReqCheckbox.setLayoutData(gridData2);
        
        // crate notify ERP button
       Button notifyERPBtn = createButton(composite,SWT.PUSH,"NotifyERP.Label");
       addSelectionEvent(notifyERPBtn,GlobalConstant.EVENT_NOTIFY_ERP);
        // create compare rev button
       Button compareRevBtn = createButton(composite,SWT.PUSH,"CompareRevButton.Lable");
       addSelectionEvent(compareRevBtn,GlobalConstant.EVENT_COMPARE_REVISION);
        // crate view RDD/PDF button
       Button m_seeRddPdfBtn = createButton(composite,SWT.PUSH,"SeeRDDPdfButton.Lable");
       addSelectionEvent(m_seeRddPdfBtn,GlobalConstant.EVENT_VIEW_RDD_PDF);
        
//        PublishEvent publishEvent = getPublishEvent(
//                GlobalConstant.EVENT_REVISION_SELECTION_CHANGED, null,
//                null);
//        getController().publish(publishEvent);
    }
    private void addSelectionEvent(Button button, final String eventName)
    {
        button.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                PublishEvent publishEvent = getPublishEvent(
                        eventName, null,
                      null);
                getController().publish(publishEvent);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }

    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    private IPublisher getPublisher()
    {
        return this;
    }
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    private Button createButton(Composite parent,int Style, String text)
    {
        Button button = new Button(parent, Style);
        button.setText(getRegistry().getString(text));
        return button;
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    private void createDataModelMapping()
    {
      
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_mfgReviewReqCheckbox,
                "mfgreviewrequired", FormHelper.getDataModelName());
        
        dataBindingHelper.bindData(m_docCntrlReqCheckbox,
                "nov4_doccontrol_required", FormHelper.getDataModelName());
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
  
}
