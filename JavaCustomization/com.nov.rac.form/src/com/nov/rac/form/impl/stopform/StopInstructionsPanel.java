package com.nov.rac.form.impl.stopform;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FieldMandatoryValidator;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.common.CalendarPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.DateButton.ButtonsID;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class StopInstructionsPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private Registry m_registry;
    
    public StopInstructionsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 10,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(4, false));
        
        createStopInstructionLblRow(l_composite);
        createTextAreaRow(l_composite);
        createExpectedResolutionDateRow(l_composite);
        
        createDataModelMapping();
        
        return true;
    }
    
    private void createExpectedResolutionDateRow(Composite composite)
    {
        
        Composite l_composite = new Composite(composite, SWT.NONE);
        
        l_composite.setLayout(new RowLayout(SWT.HORIZONTAL));
        
        m_expectedResolutionDateLbl = new Label(l_composite, SWT.NONE);
        
        m_expectedResolutionDateLbl.setText(m_registry
                .getString("expectedResolutionDate.label"));
        
        
        m_dateComposite = new Composite(composite, SWT.EMBEDDED);
        
        m_dateComposite.setLayout(new GridLayout(1, true));
        
        m_dateComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1));
       
        m_expectedResolutionDateValue = new DateButton();
        
        SWTUIUtilities.embed(m_dateComposite, m_expectedResolutionDateValue, false);
        
        UIHelper.makeMandatory(m_dateComposite, dateFieldValidator, true, "");
       
        m_expectedResolutionDateValue.addPropertyChangeListener(new PropertyChangeListener()
        {
            
            @Override
            public void propertyChange(PropertyChangeEvent propertychangeevent)
            {

                if(m_expectedResolutionDateValue.getDate() != null)
                {
                    
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            dateFieldValidator.setValid(true);
                            moveComposite();
                            
                        }
                    });
                }
                else
                {
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        
                        @Override
                        public void run()
                        {
                            dateFieldValidator.setValid(false);
                            moveComposite();
                            
                        }
                    });
                }
            }
        });
    }
    
    private void moveComposite()
    {
       // dateFieldValidator.setValid(false);
        Rectangle rect = m_dateComposite.getBounds();
        m_dateComposite.setLocation(rect.x + 1, rect.y);
        m_dateComposite.setLocation(rect.x, rect.y);
        
    }
    
    private void createTextAreaRow(Composite composite) 
    {
        
        m_stopInstructionTxt = new Text(composite, SWT.MULTI | SWT.WRAP
                | SWT.BORDER | SWT.V_SCROLL);
        m_stopInstructionTxt.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 3, 4);
        m_stopInstructionTxt.setLayoutData(gridData);
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
    }
    
    private void createStopInstructionLblRow(Composite composite)
    {
        
        m_stopInstructionLabel = new Label(composite, SWT.NONE);
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 4,
                1);
        m_stopInstructionLabel.setLayoutData(gridData);
        
        m_stopInstructionLabel.setText(m_registry
                .getString("stopInstruction.label"));
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_stopInstructionTxt.setText(PropertyMapHelper.handleNull(propMap.getString(GlobalConstant.STOPINSTRUCTIONS)));
        
        Date theDate = propMap.getDate(GlobalConstant.NOV4_EXPECTED_RESOLUTION);
        
        if (theDate != null)
        {
            m_expectedResolutionDateValue.setDate(theDate);
           
        }
        
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(GlobalConstant.STOPINSTRUCTIONS, m_stopInstructionTxt.getText());
        
        Date date = m_expectedResolutionDateValue.getDate();
        
        System.out.println(date);
        
        propMap.setDate(GlobalConstant.NOV4_EXPECTED_RESOLUTION, date);
        
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return false;
    }

    @Override
    public void setEnabled(final boolean flag)
    {
        m_stopInstructionTxt.setEnabled(flag);
        Display.getDefault().asyncExec(new Runnable()
        {
            
            @Override
            public void run()
            {
                m_expectedResolutionDateValue.setEnabled(flag);
                
            }
        });
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        
        dataBindingHelper.bindData(m_stopInstructionTxt,
                GlobalConstant.STOPINSTRUCTIONS, FormHelper.getDataModelName());
       dataBindingHelper.bindData(m_expectedResolutionDateValue,
                GlobalConstant.NOV4_EXPECTED_RESOLUTION, FormHelper.getDataModelName());
       
        
    }
    
    
    private Label m_stopInstructionLabel;
    private Text m_stopInstructionTxt;
    private Label m_expectedResolutionDateLbl;
    private DateButton m_expectedResolutionDateValue;
    private FieldMandatoryValidator dateFieldValidator = new FieldMandatoryValidator();
    Composite m_dateComposite;
  //  private DateTime m_dateFrom;
}
