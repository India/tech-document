package com.nov.rac.form.impl.enform;

import javax.swing.JScrollPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class PublishToPanel extends AbstractUIPanel implements
        INOVFormLoadSave, ISubscriber
{
    
    private static final int NO_OF_COLUMNS_MAIN_COMPOSITE = 1;
    private static final int NO_OF_COLUMNS_PUBLISH_TO = 3;
    private static final int VERTICAL_GRIDS_TABLE = 25;
    private Registry m_registry;
    private TCTable m_publishToTCTable;
    
    public PublishToPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(NO_OF_COLUMNS_MAIN_COMPOSITE,
                false);
        l_composite.setLayout(gl_l_composite);
        
        createPublishToComposite(l_composite);
        
        createTCTable(l_composite);
        
        return false;
    }
    
    private void createTCTable(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                false, 1, VERTICAL_GRIDS_TABLE);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        
        //if only want ot show column names
//        TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication()
//                .getSession();
//        TCTable tCTable = new TCTable(tcSession,
//                new String[] { "Type", "Name" });
        
        String[] propertyNames = getRegistry().getStringArray(
                "PublishToPanelTable.PropertyNames");
        String[] columnNames = getRegistry().getStringArray(
                "PublishToPanelTable.ColumnNames");
        
        m_publishToTCTable = new TCTable(propertyNames, columnNames);
        m_publishToTCTable.setShowVerticalLines(true);
        m_publishToTCTable
                .setAutoResizeMode(m_publishToTCTable.AUTO_RESIZE_ALL_COLUMNS);
        
        JScrollPane scrollPane = new JScrollPane(m_publishToTCTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    
    private void createPublishToComposite(Composite parent)
    {
        final Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(NO_OF_COLUMNS_PUBLISH_TO,
                false);
        composite.setLayout(gl_l_composite);
        
        CLabel publishTo = new CLabel(composite, SWT.NONE);
        GridData gd_publishTo = new GridData(SWT.LEFT, SWT.CENTER, true, false,
                1, 1);
        publishTo.setLayoutData(gd_publishTo);
        publishTo.setText(getRegistry().getString("ERP.PublishTo.Label"));
        SWTUIHelper.setFont(publishTo, SWT.BOLD);
        publishTo.setImage(new Image(getComposite().getDisplay(),
                m_registry.getImage("Publish.IMAGE"), SWT.NONE));
        
        
        /*Button feedBack = new Button(composite, SWT.NONE);
        GridData gd_feedBack = new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1);
        feedBack.setLayoutData(gd_feedBack);
        feedBack.setText(getRegistry().getString("FEEDBACK.Button"));*/
        
        new FeedBackButtonComposite(composite, SWT.NONE);
        
        Button exportToExcel = new Button(composite, SWT.NONE);
        GridData gd_exportToExcel = new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1);
        exportToExcel.setLayoutData(gd_exportToExcel);
        exportToExcel.setText(getRegistry().getString("EXPORTTOEXCEL.Button"));
        
       
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
