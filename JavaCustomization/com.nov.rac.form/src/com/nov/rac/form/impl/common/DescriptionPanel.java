package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

/**
 * 
 * 
 * @author sachinkab
 * 
 */

public class DescriptionPanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    private Text m_textField = null;
    
    private Composite m_groupContainer = null;
    private Registry m_registry = null;
    private static final int VERTICAL_SPACE = 25;
    
    public DescriptionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_textField.setText(PropertyMapHelper.handleNull(propMap
                .getString(getRegistry().getString("description.prop"))));
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(getRegistry().getString("description.prop"),
                m_textField.getText());
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return true;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, true));
        
        createGroupContainer(l_composite);
        createTextField();
        
        createDataModelMapping();
        
        return true;
    }
    
    private void createGroupContainer(Composite l_composite)
    {
        m_groupContainer = l_composite;
        
        String label = getRegistry().getString(
                "descriptionPanel.GroupContainerText");
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                VERTICAL_SPACE);
        m_groupContainer.setLayoutData(gridData);
        m_groupContainer.setLayout(new GridLayout(1, true));
        
        SWTUIHelper.setFont(m_groupContainer, SWT.BOLD);
        getExpandableComposite().setText(label);
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
    }
    
    private void createTextField()
    {
        m_textField = new Text(m_groupContainer, SWT.BORDER | SWT.MULTI
                | SWT.WRAP | SWT.V_SCROLL);
        UIHelper.makeMandatory(m_textField);
        m_textField.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        m_textField.setLayoutData(gridData);
        
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_textField.setEditable(flag);
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_textField,
                getRegistry().getString("description.prop"),
                FormHelper.getDataModelName());
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
}
