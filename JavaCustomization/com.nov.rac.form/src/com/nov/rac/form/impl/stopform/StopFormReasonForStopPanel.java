package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class StopFormReasonForStopPanel extends AbstractExpandableUIPanel implements INOVFormLoadSave
{
    

    private Registry m_registry;
    
    public StopFormReasonForStopPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        
        GridLayout gl_l_composite = new GridLayout(NUMBER_OF_COLUMNS, true);
        
        getExpandableComposite().setText(m_registry.getString("StopFormInformation.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        gl_l_composite.horizontalSpacing = 0;
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginLeft = 0;
        gl_l_composite.marginRight = 0;
        l_composite.setLayout(gl_l_composite);
        
        createLeftPanel(l_composite);
        createVerticalLine(l_composite);
        createRightPanel(l_composite);
        
        return true;
    }
    
    private void createRightPanel(Composite l_composite) throws TCException
    {
        m_stopInstructionsPanel = new StopInstructionsPanel(
                l_composite, SWT.NONE);
        m_stopInstructionsPanel.createUI();
    }
    
    private void createVerticalLine(Composite l_composite)
    {
        
        Label verticalSeparator = new Label(composite, SWT.SEPARATOR
                | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.CENTER, SWT.FILL, true, true, 1,
                1);
        verticalSeparator.setLayoutData(layoutData);
        
    }
    
    private void createLeftPanel(Composite l_composite) throws TCException
    {
        
        m_reasonForStopPanel = new ReasonForStopPanel(
                l_composite, SWT.NONE);
        m_reasonForStopPanel.createUI();
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }

	@Override
	public boolean load(IPropertyMap propMap) throws TCException {
		
		m_stopInstructionsPanel.load(propMap);
		m_reasonForStopPanel.load(propMap);
		
		return true;
	}

	@Override
	public boolean save(IPropertyMap propMap) throws TCException {
		
		m_stopInstructionsPanel.save(propMap);
		m_reasonForStopPanel.save(propMap);
		
		return true;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setEnabled(boolean flag) {
		m_stopInstructionsPanel.setEnabled(flag);
		m_reasonForStopPanel.setEnabled(flag);
		
	}
    
	private StopInstructionsPanel m_stopInstructionsPanel;
	
	private ReasonForStopPanel m_reasonForStopPanel;
	
	 private static final int NUMBER_OF_COLUMNS = 21;
}
