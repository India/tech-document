package com.nov.rac.form.impl.enform;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.ui.AbstractUIPanel;

public class SetEnabledRunnable implements Runnable
{
    private PublishEvent m_event;
    private INOVFormLoadSave m_panel;
    
    public SetEnabledRunnable(PublishEvent event, INOVFormLoadSave panel)
    {
        m_event = event;
        m_panel = panel;
    }
    
    public void run()
    {
        
        if (!((AbstractUIPanel) m_panel).getComposite().isDisposed())
        {
            
            if ((m_event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))||(m_event.getPropertyName().equalsIgnoreCase(
                            GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED)))
            {
                m_panel.setEnabled(false);
            }
            
            if (m_event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_EDIT_BUTTON_SELECTED))
            {
                m_panel.setEnabled(true);
            }
            
        }
    }
}
