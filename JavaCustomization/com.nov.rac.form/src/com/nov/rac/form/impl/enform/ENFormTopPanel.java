package com.nov.rac.form.impl.enform;

import java.io.IOException;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.ENFormPrinter;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.NOVBrowser;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ENFormTopPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private Button m_btnNotifyOracle = null;
    private Button m_btnValidate = null;
    private Button m_btnHelp = null;
    private Button m_btnPrint = null;
    private Registry m_registry = null;
	private IPropertyMap m_propMap = null;
    
    public ENFormTopPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
    	m_propMap =  propMap;
    	
    	m_btnNotifyOracle.setEnabled( enableNotifyOracleButton() );
    	 
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(7, true));
        Composite topComposite = new Composite(l_composite, SWT.NONE);
        GridData topCompositeGridData = new GridData(SWT.FILL, SWT.CENTER,
                true, false, 7, 1);
        topComposite.setLayoutData(topCompositeGridData);
        topComposite.setLayout(new GridLayout(5, false));
        createLable(topComposite);
        createNotifyOracleButton(topComposite);
        createValidateButton(topComposite);
        createHelpButton(topComposite);
        createPrintButton(topComposite);
        
        addListeners();
        
        return true;
    }

	private void createPrintButton(Composite l_composite)
    {
        m_btnPrint = new Button(l_composite, SWT.NONE);
        
          m_btnPrint.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
          false, 1, 1));
         
        m_btnPrint.setText(getRegistry().getString("PrintButton.Lable"));
    }
    
    private void createHelpButton(Composite l_composite)
    {
        m_btnHelp = new Button(l_composite, SWT.NONE);
        m_btnHelp.setText(getRegistry().getString("HelpButton.Lable"));
    }
    
    private void createValidateButton(Composite l_composite)
    {
        m_btnValidate = new Button(l_composite, SWT.NONE);
        m_btnValidate.setText(getRegistry().getString("ValidateButton.Lable"));
    }
    
    private void createNotifyOracleButton(Composite composite)
    {
        m_btnNotifyOracle = new Button(composite, SWT.NONE);
        m_btnNotifyOracle
                .setText(getRegistry().getString("NotifyOracle.Lable"));
       
    }
    
	private void createLable(Composite composite)
    {
        Label m_lblEngineeringTargets = new Label(composite, SWT.NONE);
        SWTUIHelper.setFont(m_lblEngineeringTargets, SWT.BOLD);
        m_lblEngineeringTargets.setText(getRegistry().getString(
                "EngNoticeTarget.Label"));
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    private boolean enableNotifyOracleButton() 
    {
    	boolean isNotifyOracleEnable = false;
    	
    	WorkflowProcess workFlowObject = WorkflowProcess.getWorkflow();
    	List<TCComponent> itemsList = workFlowObject.getTargetItems();
    	
    	for(TCComponent item : itemsList)
    	{
    		try
    		{
				String status = item.getProperty("release_status_list");
				//System.out.println("<<<<<<<<<<<============== ITEM = "+ item + " :: STATUS = " + status +  " ==============>>>>>>>>>>>");
				if( status.contains("ACTIVE") || status.contains("MFG") )
				{
					isNotifyOracleEnable = true;
					break;
				}
			} 
    		catch (TCException e)
    		{
				e.printStackTrace();
			}
    	}
		
    	return isNotifyOracleEnable;
	}
    
    private void addListeners()
    {
    	notifyOracleButtonAddListenerTo();
    	helpButtonAddListenerTo();
    	printButtonAddListenerTo();
	}
    
    private void notifyOracleButtonAddListenerTo() 
    {
    	m_btnNotifyOracle.addSelectionListener(new SelectionListener()
    	{
    		@Override
    		public void widgetSelected(SelectionEvent arg0) 
    		{
    			notifyOracle();
    		}

    		@Override
    		public void widgetDefaultSelected(SelectionEvent arg0) 
    		{
    		}
    	});
    }

	private void helpButtonAddListenerTo() 
	{
		m_btnHelp.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				enFormShowHelp();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{
			}
		});
	}

	private void printButtonAddListenerTo() 
	{
		m_btnPrint.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				enFormPrint();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{
			}
		});
	}

	// Notify Oracle
    private void notifyOracle()
    {		
    	try
    	{
    		WorkflowProcess workFlowObject = WorkflowProcess.getWorkflow();
    		List<TCComponent> targets = workFlowObject.getTargetItemRevisions();
    		TCComponent[] targetsArry = targets.toArray(new TCComponent[targets.size()]);

    		String processUID = workFlowObject.getRootTask().getUid();

    		int isOracleNotifyProp = m_propMap.getInteger("isOracleNotify");
    		if(isOracleNotifyProp == 0)
    		{
    			TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
    			TCUserService userService = session.getUserService();

    			Object param[] = new Object[3];
    			param[0] = targetsArry.length;
    			param[1] = targetsArry;
    			param[2] = processUID;

    			userService.call("NATOIL_OracleNotify", param);
    			m_btnNotifyOracle.setEnabled(false);
    		} 
    	}
    	catch (TCException ex) 
    	{
    		// ex.printStackTrace();
    		MessageBox.post(ex);
    	}
    }
    
    // Help Button
    private void enFormShowHelp()
    {
    	String helpURL = "";

    	TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
    	TCPreferenceService prefService = session.getPreferenceService();

    	helpURL = prefService.getStringValue( m_registry.getString("ENFormHelpURL.PREF"));

    	try 
    	{
    		if( !helpURL.isEmpty() )
    		{
    			//Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + helpURL);
    			NOVBrowser.open(helpURL);
    		}
    		else
    		{
    			String[] errorStrings = new String[2];
    			errorStrings[0] = m_registry.getString("");
    			errorStrings[1] = m_registry.getString("");
    			throw new TCException( errorStrings  );
    		}
    	} 
    	catch (Exception ex)
    	{
    		//ex.printStackTrace();
    		MessageBox.post(ex);
    	}
    }
    
    // print form
    private void enFormPrint()
    {
    	try 
		{
			ENFormPrinter formPrinter = new ENFormPrinter(m_propMap);
			formPrinter.printForm();
		} 
		catch (Exception ex)
		{
			//ex.printStackTrace();
			MessageBox.post(ex);
		}
    }
}