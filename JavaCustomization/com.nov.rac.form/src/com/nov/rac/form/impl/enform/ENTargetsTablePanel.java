package com.nov.rac.form.impl.enform;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.common.TargetsTablePanel;
import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.services.TransferInfoToWebServerHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ENTargetsTablePanel extends TargetsTablePanel
{
    
    private Button m_showTargetsTableButton;
    private AttachmentsButton m_buttonBar;
    
    private static final int NUMBER_OF_COLUMN_3 = 3;
    private IPropertyMap m_propMap;
    
    public ENTargetsTablePanel(Composite parent, int style)
    {
        super(parent, style);
        
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        
        createTopPanel(composite);
        super.createUI();
        registerSubscriber();
        
        addShowTargetsButtonListener();
        addPlusMinusButtonListener();
        return true;
    }
    
    private void registerSubscriber()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        l_controller.registerSubscriber(GlobalConstant.EVENT_COMPARE_REVISION,
                getSubscriber());
        
        l_controller.registerSubscriber(
                GlobalConstant.EVENT_NOTIFY_ERP,
                getSubscriber());
        
        l_controller.registerSubscriber(
                GlobalConstant.EVENT_VIEW_RDD_PDF,
                getSubscriber());
        
        
    }
    
    private void notifyOracle()
    {         
       try
       {
              WorkflowProcess workFlowObject = WorkflowProcess.getWorkflow();
              List<TCComponent> targets = workFlowObject.getTargetItemRevisions();
              TCComponent[] targetsArry = targets.toArray(new TCComponent[targets.size()]);

              String processUID = workFlowObject.getRootTask().getUid();

              int isOracleNotifyProp = m_propMap.getInteger("isOracleNotify");
              if(isOracleNotifyProp == 0)
              {
                     /*TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
                     TCUserService userService = session.getUserService();

                     Object param[] = new Object[3];
                     param[0] = targetsArry.length;
                     param[1] = targetsArry;
                     param[2] = processUID;

                     userService.call("NATOIL_OracleNotify", param);
                     */
                           TransferInfoToWebServerHelper transferInfoToWebServerHelper = new TransferInfoToWebServerHelper();
                  
                               
                           String context = PreferenceHelper.getStringValue("CETGWSServices_context", TCPreferenceService.TC_preference_site);                                                
                           String serveletName = PreferenceHelper.getStringValue("CETGWSServices_SubmitEN_servlet", TCPreferenceService.TC_preference_site); 
                           
                           transferInfoToWebServerHelper.setContext(context);
                           transferInfoToWebServerHelper.setServeletName(serveletName);
                           transferInfoToWebServerHelper.setModuleName("RSOne_EN_V2"); 
                           
                           transferInfoToWebServerHelper.setTargetRevision(targetsArry);
                           transferInfoToWebServerHelper.setProcessUID(processUID);
                           String params = "processpuid=" + processUID + "&requestname=" + "SUS";
                
                           transferInfoToWebServerHelper.oracleNotify();
              } 
       }
       catch (TCException ex) 
       {
              // ex.printStackTrace();
              MessageBox.post(ex);
       }
    }

    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    private void fireCompareEvent() throws Exception
    {
        IPropertyMap iMap = new SimplePropertyMap();
        if (isSelectedItemComparable())
        {
            TCComponent[] selectedRevisions = getSelectedItemRevisions();
            iMap.setComponent(selectedRevisions[0]);
            Shell shell = getComposite().getShell();
            PopupDialog dialog = new PopupDialog(shell);
            dialog.setInputPropertyMap(iMap);
            dialog.openCompareDialog(shell);
        }
        else
        {
            String[] errors = new String[] { getRegistry().getString(
                    "notComparableRevision.MSG") };
            TCException exception = new TCException(errors);
            throw exception;
        }
    }
    
    
    private void createTopPanel(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(new GridLayout(NUMBER_OF_COLUMN_3, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1);
        l_composite.setLayoutData(gd_l_composite);
        
        m_showTargetsTableButton = new Button(l_composite, SWT.NONE);
        Image image = getRegistry().getImage(
                "TargetsTable.ShowTargetsTable.IMAGE");
        m_showTargetsTableButton.setImage(image);
        
        Label label = new Label(l_composite, 0);
        label.setText(getRegistry().getString("ENTargets.Label"));
        
        // create plus-minus buttons
        Image minusButtonImage = new Image(l_composite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        Image pluseButtonImage = new Image(l_composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        
        m_buttonBar = new AttachmentsButton(l_composite, SWT.NONE,
                pluseButtonImage, minusButtonImage);
        GridData gridData1 = new GridData(SWT.RIGHT, SWT.FILL, true, true, 1, 1);
        m_buttonBar.setLayoutData(gridData1);
    }
    
    private void addShowTargetsButtonListener()
    {
        m_showTargetsTableButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                TargetsTableDialog dialog = new TargetsTableDialog(
                        ENTargetsTablePanel.this.getComposite().getShell());
                
                dialog.setRegistry(getRegistry());
                dialog.create();
                
                dialog.setTableModel(getTable().getModel());
                
                dialog.open();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }
    
    private void addPlusMinusButtonListener()
    {
        m_buttonBar.addSelectionListenerToMinusButton(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                ENTargetsTablePanel.this.removeSelectedTargets();
            }
        });
        
        m_buttonBar.addSelectionListenerToPlusButton(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                ENTargetsTablePanel.this.addTargetsToWorkflow();
            }
        });
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_propMap = propMap;
        super.load(propMap);
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return  super.save(propMap);
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_showTargetsTableButton.setEnabled(flag);
        super.setEnabled(flag);
        
        
    }
    @Override
    public void dispose()
    {
        unRegisterEventSubscriber();
        super.dispose();
    }
    @Override
    public void update(PublishEvent event)
    {
        super.update(event);
        if (event.getPropertyName().equals(GlobalConstant.EVENT_VIEW_RDD_PDF))
        {
            viewSelectedRDDPDF();
        }
        else if(event.getPropertyName().equals(GlobalConstant.EVENT_COMPARE_REVISION))
        {
            try
            {
                // validate single row is selected from table
                validateSingleSelection();
                fireCompareEvent();
            }
            catch (TCException e)
            {
                MessageBox.post(e);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        else if (event.getPropertyName().equals(GlobalConstant.EVENT_NOTIFY_ERP))
        {
            notifyOracle();
        }
        
    }
    public void unRegisterEventSubscriber()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
      
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_COMPARE_REVISION, this);
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_NOTIFY_ERP, this);
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_VIEW_RDD_PDF, this);
    }
    
}
