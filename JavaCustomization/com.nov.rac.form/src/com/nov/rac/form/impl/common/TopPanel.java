package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class TopPanel extends AbstractUIPanel 
{
	private Registry m_registry;
	
	protected Button m_helpButton;
	
	protected Button m_feedbackButton;
	
	protected Button m_printButton;

	public TopPanel(Composite parent, int style) 
	{
		super(parent, style);
    }
    
	public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(1, true));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        l_composite.setLayoutData(gd_panel);
        
        createTopRow(l_composite);
    	
    	return true;
    }

	private void createTopRow(Composite l_composite) 
	{
		Composite composite = new Composite(l_composite,SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		composite.setLayout(new GridLayout(4, false));
		
		Label l_lblFormType = new Label(composite, SWT.NONE);
		l_lblFormType.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		l_lblFormType.setText(m_registry.getString("lblFormType.NAME",
                "Key Not Found"));
		SWTUIHelper.setFont(l_lblFormType, SWT.BOLD);
		
		createHelpButton(composite);
		
		createFeedbackButton(composite);
		
		createPrintButton(composite);
		
	}

	private void createHelpButton(Composite l_composite) 
	{
		m_helpButton = new Button(l_composite, SWT.NONE);
		GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true,
                false, 1, 1);
		
        Image image = getRegistry().getImage(
                "helpButton.IMAGE");
        m_helpButton.setImage(image);
        
        m_helpButton.setLayoutData(gridData);
	}
	
	private void createFeedbackButton(Composite composite)
    {   
        m_feedbackButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1);
        m_feedbackButton.setText(m_registry.getString("feedbackButton.NAME",
                "Key Not Found"));
        
        m_feedbackButton.setLayoutData(gridData);
    }
	
	private void createPrintButton(Composite composite)
    {   
        m_printButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1);
        m_printButton.setText(m_registry.getString("printButton.NAME",
                "Key Not Found"));
        
        m_printButton.setLayoutData(gridData);
    }

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}

}
