package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.quicksearch.searchprovider.NOVProjectFolderSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.DualList;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ProjectPanel extends AbstractExpandableUIPanel implements INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private Group m_selectProjectGroupContainer = null;
    protected Text m_searchText = null;
    private Button m_searchProjectButton = null;
    private DualList m_dualList = null;
    
    public ProjectPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String[] projects = PropertyMapHelper.handleNull(propMap
                .getStringArray(getRegistry().getString("projects.PROP")));
        if (projects.length > 0)
        {
            for (String project : projects)
            {
                m_dualList.addToTargetTable(project);
            }
        }
        
        return true;
    }
    
    public DualList getDualList()
    {
        return m_dualList;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        TCTable targetTable = m_dualList.getTargetTable();
        int rowCount =targetTable.getRowCount();
        String[] allValues_TargetTable = new String[rowCount];
    
        
        for (int inx = 0; inx < rowCount; inx++)
        {
            allValues_TargetTable[inx] = (String) targetTable.getValueAt(inx, 0);
        }
        propMap.setStringArray(getRegistry().getString("projects.PROP"), allValues_TargetTable);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_searchText.setEnabled(flag);
        m_searchProjectButton.setEnabled(flag);
        m_dualList.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        l_composite.setLayout(gl_l_composite);
        
        // createProjectGroupContainer(l_composite);
        createSearchTextField(l_composite);
        createDualList(l_composite);
        addButtonListener();
        createDataModelMapping();
        return true;
    }
    
    protected void createDualList(Composite composite)
    {
        m_dualList = new DualList(composite, getRegistry());
        m_dualList.changeSpan(1, 1);
        m_dualList.createUI();
    }
    
    protected void createSearchTextField(Composite composite)
    {
        final String defaultString = getRegistry().getString(
                "SearchText.Default");
        Composite subComposite = new Composite(composite, SWT.NONE);
        
        GridLayout gl_l_composite = new GridLayout(3, false);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        subComposite.setLayout(gl_l_composite);
        
        // subComposite.setLayout(new GridLayout(3, false));
        subComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        
        Label lblProject = new Label(subComposite, SWT.NONE);
        lblProject.setText(getRegistry().getString("Project.Label"));
        SWTUIHelper.setFont(lblProject, SWT.BOLD);
        
        m_searchText = new Text(subComposite, SWT.BORDER);
        m_searchText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        m_searchText
                .setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        m_searchText.setText(defaultString);
        m_searchText.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent arg0)
            {
                if (m_searchText.getText().equals(""))
                {
                    m_searchText.setText(defaultString);
                }
            }
            
            @Override
            public void focusGained(FocusEvent arg0)
            {
                if (m_searchText.getText().equals(defaultString))
                {
                    m_searchText.setText("");
                }
            }
        });
        m_searchProjectButton = new Button(subComposite, SWT.NONE);
        m_searchProjectButton.setText(getRegistry().getString(
                "SearchProjectButton.Label"));
    }
    
    private void addButtonListener()
    {
        m_searchProjectButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                String searchText = m_searchText.getText();
                executeQuery(searchText);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                
            }
        });
    }
    
    protected void createProjectGroupContainer(Composite composite)
    {
        String label = getRegistry().getString(
                "ProjectPanel.GroupContainerText");
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 7, 1);
        m_selectProjectGroupContainer = new Group(composite, SWT.NONE);
        m_selectProjectGroupContainer.setLayoutData(gridData);
        m_selectProjectGroupContainer.setLayout(new GridLayout(1, true));
        m_selectProjectGroupContainer.setText(label);
        SWTUIHelper.setFont(m_selectProjectGroupContainer, SWT.BOLD);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    protected void createDataModelMapping()
    {
    
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_dualList.getTargetTable().dataModel, getRegistry().getString("projects.PROP"),
                FormHelper.getDataModelName());
        
    }
    
    private void executeQuery(String searchText)
    {
        try
        {
            NOVProjectFolderSearchProvider searchProvider = new NOVProjectFolderSearchProvider(searchText);
            
            TCComponent[] projComps = searchProvider.execute(INOVSearchProvider.LOAD_ALL);
            
            if ((projComps != null) && (projComps.length > 0))
            {
                for (TCComponent tcComponent : projComps)
                {
                    m_dualList.addToSourceTable(tcComponent.getProperty("object_string"));
                }
            }
            else
            {
                MessageBox.post(getRegistry().getString("NoObjectFound.MSG"), getRegistry().getString("Info.MSG"),
                        MessageBox.INFORMATION);
            }
        }
        catch (Exception te)
        {
            System.out.println(te);
        }
    }
    
}
