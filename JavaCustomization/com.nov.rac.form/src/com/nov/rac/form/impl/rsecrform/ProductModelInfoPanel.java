package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ProductModelInfoPanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry;
    private Text m_productModelValue;
    private Text m_projectSerialValue;
    private Text m_rigNameValue;
    private Text m_relatedDocumentValue;
    private Text m_customerValue;
    
    public ProductModelInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_productModelValue.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_product_model")));
        m_projectSerialValue.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_project_number")));
        m_customerValue.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_customer")));
        m_relatedDocumentValue.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_related_doc")));
        m_rigNameValue.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_rig_name")));
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        Composite mainComposite = createComposite(composite);
        GridLayout gl_composite = new GridLayout(2, true);
        gl_composite.horizontalSpacing = SWTUIHelper.convertHorizontalDLUsToPixels(mainComposite, 10);
        mainComposite.setLayout(gl_composite);
        
        getExpandableComposite().setText(getRegistry().getString("RelatedInformation.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        createLeftComposite(mainComposite);
        createRightComposite(mainComposite);
        return true;
    }
    
    private void createLeftComposite(Composite parent)
    {
        Composite leftComposite = createComposite(parent);
        m_productModelValue = createKeyLabelValueText(leftComposite,
                "ProductModel.Label");
        m_projectSerialValue = createKeyLabelValueText(leftComposite,
                "ProjectSerial.Label");
        m_rigNameValue = createKeyLabelValueText(leftComposite, "RigName.Label");
        
    }
    
    private void createRightComposite(Composite parent)
    {
        Composite rightComposite = createComposite(parent);
        m_relatedDocumentValue = createKeyLabelValueText(rightComposite,
                "RelatedDocument.Label");
        m_customerValue = createKeyLabelValueText(rightComposite,
                "Customer.Label");
//        new DateTime(rightComposite, SWT.DROP_DOWN).setEnabled(false);
        new Label(rightComposite,SWT.NONE);
    }
    
    private Composite createComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
        
    }
    
    private Text createKeyLabelValueText(Composite composite, String setText)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1,
                1));
        label.setText(getRegistry().getString(setText));
        SWTUIHelper.setFont(label, SWT.BOLD);
        
        Text text = new Text(composite, SWT.BORDER);
        text.setEditable(false);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        text.setBackground(getComposite().getDisplay().getSystemColor(SWT.COLOR_WHITE));
        
        return text;
    }
    
    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false,
                1, 1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        gl_composite.horizontalSpacing = 100;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
