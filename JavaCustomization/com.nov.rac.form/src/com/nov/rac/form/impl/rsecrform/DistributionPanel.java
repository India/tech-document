package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.wb.swt.SWTResourceManager;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DistributionPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private List m_distributionList = null;
    
    public DistributionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        String[] distributions = PropertyMapHelper.handleNull(propMap
                .getStringArray("nov4_distribution"));
        
        for (String dist : distributions)
        {
            m_distributionList.add(dist);
        }
        
        return true;
        
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite mainComposite = getMainComposite();
        
        createDistributionLabel(mainComposite, "distributionPanel.LABEL");
        createDistribution(mainComposite);
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void createDistributionLabel(Composite composite, String prpKey)
    {
        Label distributionLbl = new Label(composite, SWT.NONE);
        distributionLbl.setLayoutData(new GridLayout(1, false));
        distributionLbl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
        distributionLbl.setText(getRegistry().getString(prpKey));
        SWTUIHelper.setFont(distributionLbl, SWT.BOLD);
    }
    
    private void createDistribution(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gl_distribution = new GridLayout(1, true);
        composite.setLayout(gl_distribution);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                25));
        gl_distribution.marginHeight = 0;
        gl_distribution.marginWidth = 0;
        
        m_distributionList = new List(composite, SWT.SINGLE | SWT.BORDER
                | SWT.V_SCROLL | SWT.WRAP | SWT.READ_ONLY);
        GridData gd_distributionList = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        gd_distributionList.heightHint = composite.getBounds().height;
        m_distributionList.setLayoutData(gd_distributionList);
        m_distributionList.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private Composite getMainComposite()
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
}
