package com.nov.rac.form.impl.ecoform;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class ECOTable extends TCTable
{
	
	private Registry m_registry;
    //String[] m_editableColumns = null;
    
    
    public ECOTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
        
        // making this member variable as we need this many times.
        /*m_editableColumns = m_registry
                .getStringArray("TargetsTable.EDITABLE_COLUMNS");*/
        
        addColumnRenderersEditors();
    }
	public void addColumnRenderersEditors()
    {
        // Part ID column
        addIconRenderer("TargetsTableColumnIdentifier.ItemID");
        addColumnRenderersEditors(m_registry.getString("targetsnewlifecycle.PROP"));
        
    }
    
    private void addIconRenderer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        // int colIndex = TableUtils.getColumnIndex(columnName, m_table);
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new PartIDIconRenderer());
    }

    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        TableCellEditor editor = getColumnModel().getColumn(columnIndex).getCellEditor();
        return editor.isCellEditable(null);
    }
    
    public void addColumnRenderersEditors(String columnName)
    {        
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        ECOTartgetComboCellEditor cellEditorObj = new ECOTartgetComboCellEditor();
        column.setCellEditor(cellEditorObj);
        
        ECOTargetComboCellRenderer cellRendererObj = new ECOTargetComboCellRenderer();
        column.setCellRenderer(cellRendererObj);
    }
}
