package com.nov.rac.form.impl.eprform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class EPRTargetsBottomPanel extends AbstractUIPanel implements
        IPublisher
{
    
    private static final int COLUMNS = 2;
    private Registry m_registry;
    
    public EPRTargetsBottomPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, true));
        createButtonComposite(l_composite);
        return false;
    }
    
    private void createButtonComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1,
                1);
        
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(COLUMNS, false));
        
        // create compare rev button
        Button compareRevBtn = createButton(composite, SWT.PUSH,
                "compareRevButton.NAME");
        addSelectionEvent(compareRevBtn, GlobalConstant.EVENT_COMPARE_REVISION);
        // crate view RDD/PDF button
        Button m_seeRddPdfBtn = createButton(composite, SWT.PUSH,
                "viewRddPdfButton.NAME");
        addSelectionEvent(m_seeRddPdfBtn, GlobalConstant.EVENT_VIEW_RDD_PDF);
        
        PublishEvent publishEvent = getPublishEvent(
                GlobalConstant.EVENT_REVISION_SELECTION_CHANGED, null, null);
        getController().publish(publishEvent);
    }
    
    private void addSelectionEvent(Button button, final String eventName)
    {
        button.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                PublishEvent publishEvent = getPublishEvent(eventName, null,
                        null);
                getController().publish(publishEvent);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    private Button createButton(Composite parent, int Style, String text)
    {
        Button button = new Button(parent, Style);
        button.setText(getRegistry().getString(text));
        return button;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
}
