package com.nov.rac.form.impl.eprform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TopPanel;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */
public class EPRTopPanel extends TopPanel
{
    
    public EPRTopPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
