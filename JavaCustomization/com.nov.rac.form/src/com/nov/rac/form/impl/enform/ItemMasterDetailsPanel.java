package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.NOVBrowser;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class ItemMasterDetailsPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private static final int NO_OF_COLUMNS_PROP_COMPOSITE = 3;
    private static final int NO_OF_COLUMNS_HEADER_COMPOSITE = 3;
    private Registry m_registry;
    private Label m_partTypeValueLabel;
    private Label m_stockUOMValueLabel;
    private Label m_vendorUOMValueLabel;
    private Label m_leadTimeValueLabel;
    private Label m_usage1to12eValueLabel;
    private Label m_usage13to24ValueLabel;
    private Label m_primaryVendorValueLabel;
    private Label m_plannerCodeValueLabel;
    private Label m_buyerCodeValueLabel;
    private Label m_productLineValueLabel;
    private Label m_productTypeValueLabel;
    private Label m_productFamilyValueLabel;
    private Label m_productGroupValueLabel;
    private Label m_productDescValueLabel;
    private Label m_countryOfOriginValueLabel;
    private Label m_effectiveDateValueLabel;
    private Label m_priceClassValueLabel;
    private Label m_drawingNumberValueLabel;
    private Label m_drawingRevValueLabel;
    private Label m_serialControlValueLabel;
    private Label m_lotControlValueLabel;
    private Label m_hTSCodeValueLabel;
    private Label m_eCCNValueLabel;
    private Label m_uNSPSCValueLabel;
    private Label m_xrefValueLabel;
    private Label m_partInfoLabel;
    private TCComponent m_component;
    
    public ItemMasterDetailsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        // TODO Auto-generated constructor stub
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // m_PartTypeValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        // m_StockUOMValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        // m_VendorUOMValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_LeadTimeValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        // m_Usage1to12eValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_Usage13to24ValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_PrimaryVendorValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_PlannerCodeValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_BuyerCodeValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_ProductLineValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_ProductTypeValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_ProductFamilyValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_ProductGroupValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_ProductDescValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_CountryOfOriginValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_EffectiveDateValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_PriceClassValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_DrawingNumberValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_DrawingRevValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_SerialControlValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_LotControlValueLabel.setText(PropertyMapHelper
        // .handleNull(getRegistry().getString("")));
        // m_HTSCodeValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        // m_ECCNValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        // m_UNSPSCValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        // m_XrefValueLabel.setText(PropertyMapHelper.handleNull(getRegistry()
        // .getString("")));
        
        m_component = propMap.getComponent();
        
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, false));
        
        createItemMasterDetailsHeaderLabelComposite(l_composite);
        createPartInfoLabel(l_composite);
        createPropertiesComposite(l_composite);
        
        return false;
    }
    
    private void createPropertiesComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        composite
                .setLayout(new GridLayout(NO_OF_COLUMNS_PROP_COMPOSITE, false));
        
        // Part Type,Stock UOM ,etc
        createColumn1(composite);
        // Product Line,Product Type, etc
        createColumn2(composite);
        // Drawing Number , Drawing Rev, etc
        createColumn3(composite);
        
    }
    
    private void createPartInfoLabel(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        composite.setLayout(new GridLayout(1, false));
        
        m_partInfoLabel = new Label(composite, SWT.NONE);
        m_partInfoLabel.setText("PartInfo");
        
    }
    
    private void createItemMasterDetailsHeaderLabelComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        GridLayout gl_composite = new GridLayout(
                NO_OF_COLUMNS_HEADER_COMPOSITE, false);
//        gl_composite.marginWidth = 0;
//        gl_composite.marginHeight = 0;
        composite.setLayout(gl_composite);
        
        CLabel itemMasterDetailsHeaderLabel = new CLabel(composite, SWT.NONE);
        itemMasterDetailsHeaderLabel.setText(getRegistry().getString(
                "ItemMasterDetailsHeader.Label"));
        SWTUIHelper.setFont(itemMasterDetailsHeaderLabel, SWT.BOLD);
        itemMasterDetailsHeaderLabel.setImage(new Image(getComposite().getDisplay(),
                m_registry.getImage("Details.IMAGE"), SWT.NONE));       
        createDataWareHouseButton(composite);
        createPrintButton(composite);
      
    }
    
    private void createPrintButton(Composite parent)
    {
        Button printButton = new Button(parent, SWT.NONE);
       printButton.setText(getRegistry().getString("PRINT.Button"));
    }
    
    private void createDataWareHouseButton(Composite parent)
    {
        Button dataWareHouseButton = new Button(parent, SWT.NONE);
        dataWareHouseButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1));
        dataWareHouseButton.setText(getRegistry().getString("DATAWAREHOUSE.Button"));
        
        dataWareHouseButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                addDWSearchListener();
            }

        });
    }
    
    private void addDWSearchListener()
    {
        try
        {
            String dwSearchURL = getURL();
            
            if (null != dwSearchURL)
            {
                NOVBrowser.open(dwSearchURL);
            }
        }
        catch (Exception e)
        {
            com.teamcenter.rac.util.MessageBox.post(e);
        }
    }
    
    private String getURL() throws TCException
    {
        TCPreferenceService preferenceService = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
        String url = preferenceService.getStringValueAtLocation("NOV_dataware_search_URL",
                TCPreferenceLocation.OVERLAY_LOCATION);
        if (url != null)
        {
            url += getItemIdtoSearch();
        }
        
        return url;
    }

    private String getItemIdtoSearch() throws TCException
    {
        String itemId = m_component.getProperty("item_id");
        int gropuCodeIndex = itemId.lastIndexOf(":");
        
        if (gropuCodeIndex > 0)
        {
            itemId = itemId.substring(0, gropuCodeIndex);
        }
        return itemId;
    }

    private String getItemIdSuffix(boolean isPart)
    {
        String suffix = "";
        String groupCode = null;
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        TCPreferenceService prefServ = session.getPreferenceService();
        groupCode = prefServ.getStringValueAtLocation("NOV_group_code_for_legacy_part_document",
                TCPreferenceLocation.OVERLAY_LOCATION);
        
        if (groupCode != null && groupCode.length() > 0)
        {
            suffix = ":" + groupCode;
            if (isPart)
            {
                suffix = suffix + "P";
            }
        }
        return suffix;
        
    }

    private void createColumn3(Composite parent)
    {
        Composite column3Composite = createColumnComposite(parent);
        Registry registry =  getRegistry();
        
        m_drawingNumberValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("DrawingNumber.Label"));
        m_drawingRevValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("DrawingRev.Label"));
        m_serialControlValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("SerialControl.Label"));
        
        m_lotControlValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("LotControl.Label"));
        m_hTSCodeValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("HTSCode.Label"));
        m_eCCNValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("ECCN.Label"));
        
        m_uNSPSCValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("UNSPSC.Label"));
        m_xrefValueLabel = createLabelLabelComposite(column3Composite,
                registry.getString("Xref.Label"));
        
    }
    
    private void createColumn2(Composite parent)
    {
        Composite column2Composite = createColumnComposite(parent);
        Registry registry =  getRegistry();
        
        m_productLineValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("ProductLine.Label"));
        m_productTypeValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("ProductType.Label"));
        m_productFamilyValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("ProductFamily.Label"));
        
        m_productGroupValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("ProductGroup.Label"));
        m_productDescValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("ProductDesc.Label"));
        m_countryOfOriginValueLabel = createLabelLabelComposite(
                column2Composite,
                registry.getString("CountryOfOrigin.Label"));
        
        m_effectiveDateValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("ListEffectiveDate.Label"));
        m_priceClassValueLabel = createLabelLabelComposite(column2Composite,
                registry.getString("PriceClass.Label"));
        
    }
    
    private void createColumn1(Composite parent)
    {
        
        Composite column1Composite = createColumnComposite(parent);
        Registry registry =  getRegistry();
        
        m_partTypeValueLabel = createLabelLabelComposite(column1Composite, registry
                .getString("PartType.Label"));
        m_stockUOMValueLabel = createLabelLabelComposite(column1Composite, registry
                .getString("StockUOM.Label"));
        m_vendorUOMValueLabel = createLabelLabelComposite(column1Composite, registry
                .getString("VendorUOM.Label"));
        
        m_leadTimeValueLabel = createLabelLabelComposite(column1Composite,
                registry.getString("LeadTime.Label"));
        m_usage1to12eValueLabel = createLabelLabelComposite(column1Composite,
                registry.getString("Usage1-12.Label"));
        m_usage13to24ValueLabel = createLabelLabelComposite(column1Composite,
                registry.getString("Usage13-24.Label"));
        
        m_primaryVendorValueLabel = createLabelLabelComposite(column1Composite,
                registry.getString("PrimaryVendor.Label"));
        m_plannerCodeValueLabel = createLabelLabelComposite(column1Composite,
                registry.getString("PlannerCode.Label"));
        m_buyerCodeValueLabel = createLabelLabelComposite(column1Composite,
                registry.getString("BuyerCode.Label"));
        
    }
    
    private Label createLabelLabelComposite(Composite parent,
            String keyLabelText)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        GridLayout gl_Composite = new GridLayout(2, false);
        gl_Composite.marginWidth = 0;
        gl_Composite.marginHeight = 0;
        composite.setLayout(gl_Composite);
        
        Label keyLabel = new Label(composite, SWT.NONE);
        keyLabel.setText(keyLabelText);
        keyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
                1, 1));
        
        Label valueLabel = new Label(composite, SWT.NONE);
        valueLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        return valueLabel;
        
    }
    
    private Composite createColumnComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gl_composite = new GridLayout(1, false);
        // gl_composite.horizontalSpacing = 15;
        composite.setLayout(gl_composite);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1,
                1));
        return composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
