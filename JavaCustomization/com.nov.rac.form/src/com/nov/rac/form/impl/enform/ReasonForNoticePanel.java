package com.nov.rac.form.impl.enform;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ReasonForNoticePanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    private Group m_reasonForNoticeGroupContainer = null;
    private Text m_reasonForNoticeTextField = null;
    private Combo m_reasonForNoticeComboBox = null;
    private Button m_markENasCriticalCheckBox = null;
    private Registry m_registry = null;
    
    public ReasonForNoticePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        Vector<Object> reasonForChangeLOV = LOVUtils
                .getListOfValuesforLOV(m_registry.getString("ReasonForNoticePanel.ReasonForChangeLOV"));
        reasonForChangeLOV.remove(0);// remove blank string
        String[] allItems = reasonForChangeLOV.toArray(new String[0]);
        m_reasonForNoticeComboBox.setItems(allItems);
        
        String reason = PropertyMapHelper.handleNull(propMap
                .getString("reason"));
        
        m_reasonForNoticeComboBox.setText(reason);
        
        m_reasonForNoticeTextField.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4reason_desc")));
        
        m_markENasCriticalCheckBox.setSelection(propMap
                .getLogical("nov4_is_en_critical"));
        
        return true;
        
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString("nov4reason_desc",
                m_reasonForNoticeTextField.getText());
        propMap.setString("reason", m_reasonForNoticeComboBox.getText());
        propMap.setLogical("nov4_is_en_critical",
                m_markENasCriticalCheckBox.getSelection());
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_reasonForNoticeTextField.setEditable(flag);
        m_reasonForNoticeComboBox.setEnabled(flag);
        m_markENasCriticalCheckBox.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {           
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gridLayout = new GridLayout(3, true);
        gridLayout.horizontalSpacing = 20; 
        l_composite.setLayout(gridLayout);
        
       getExpandableComposite().setText(getRegistry().getString(
                "ReasonForNoticePanel.GroupContainerText"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        //CreateReasonForNoticeGroupContainer(l_composite);
        createReasonForNoticeComboBox(l_composite);
        createReasonForNoticeTextField(l_composite);
//        CreateEmptyControlOnGrid(l_composite);
        createMarkENasCriticalCheckBox(l_composite);
        
       
        
        createDataModelMapping();
        return true;
    }
    
    private void createEmptyControlOnGrid(Composite composite)
    {
        Label emptyLabel = new Label(composite, SWT.NONE);
        emptyLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1));
        SWTUIHelper.setFont(emptyLabel, SWT.BOLD);
    }
    
    private void createMarkENasCriticalCheckBox(Composite composite)
    {
        m_markENasCriticalCheckBox = new Button(
                composite, SWT.CHECK);
        m_markENasCriticalCheckBox.setLayoutData(new GridData(SWT.LEFT,
                SWT.BOTTOM, true, true, 1, 1));
        m_markENasCriticalCheckBox.setText(getRegistry().getString(
                "MarkENascriticalCheckbox.CheckBox"));
        SWTUIHelper.setFont(m_markENasCriticalCheckBox, SWT.BOLD);
    }
    
    private void createReasonForNoticeTextField(Composite composite)
    {
        m_reasonForNoticeTextField = new Text(composite,
                SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
        GridData gd_text = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 2);
        
        int firstFont = 0;
        gd_text.heightHint = ((m_reasonForNoticeTextField.getFont()
                .getFontData())[firstFont].getHeight()) * 10;
        
        m_reasonForNoticeTextField.setLayoutData(gd_text);
        m_reasonForNoticeTextField.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
    }
    
    private void createReasonForNoticeComboBox(Composite composite)
    {
        m_reasonForNoticeComboBox = new Combo(composite,
                SWT.READ_ONLY);
        m_reasonForNoticeComboBox.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        GridData gd_combo = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
        m_reasonForNoticeComboBox.setLayoutData(gd_combo);
      
        UIHelper.makeMandatory(m_reasonForNoticeComboBox);
    }
    
    private void createReasonForNoticeGroupContainer(Composite l_composite)
    {
        String label = getRegistry().getString(
                "ReasonForNoticePanel.GroupContainerText");
        m_reasonForNoticeGroupContainer = new Group(l_composite, SWT.NONE);
        m_reasonForNoticeGroupContainer.setText(label);
        m_reasonForNoticeGroupContainer.setLayoutData((new GridData(SWT.FILL,
                SWT.FILL, true, false, 1, 1)));
        m_reasonForNoticeGroupContainer.setLayout(new GridLayout(6, true));
        SWTUIHelper.setFont(m_reasonForNoticeGroupContainer, SWT.BOLD);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_reasonForNoticeTextField,
                "nov4reason_desc", FormHelper.getDataModelName());
        
        dataBindingHelper.bindData(m_reasonForNoticeComboBox, "reason",
                FormHelper.getDataModelName());
        
        dataBindingHelper.bindData(m_markENasCriticalCheckBox,
                "nov4_is_en_critical", FormHelper.getDataModelName());
        
    }
}
