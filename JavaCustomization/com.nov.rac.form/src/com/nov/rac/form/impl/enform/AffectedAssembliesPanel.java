package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.MandatoryFieldValidator;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class AffectedAssembliesPanel extends AbstractUIPanel implements
        INOVFormLoadSave, ISubscriber
{
    
    private static final int HORIZONTAL_SPACING = 10;
    private static final int VERTICAL_GRIDS_TEXT = 10;
    private Registry m_registry;
    private Text m_affectedAssembliesText;
    protected MandatoryFieldValidator m_labelFieldValidator = new MandatoryFieldValidator();
    
    public AffectedAssembliesPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap pibFormPropMap = propMap
                .getCompound("Nov4_secondary_form");
        
        String affectedAssemblies = PropertyMapHelper.handleNull(pibFormPropMap
                .getString("effected"));
        m_affectedAssembliesText.setText(affectedAssemblies);
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
   
        propMap
                .setString("effected", m_affectedAssembliesText.getText());
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_affectedAssembliesText.setEnabled(flag);
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getMainComposite();
        createAffectedAssembliesComposite(mainComposite);
        registerSubscriber();
        setEnabled(false);
        return false;
    }
    
    private void createAffectedAssembliesComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false,
                1, 1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.horizontalSpacing = SWTUIHelper
                .convertHorizontalDLUsToPixels(composite, HORIZONTAL_SPACING);
        composite.setLayout(gl_composite);
        final Label affectedAssemblies = new Label(composite, SWT.NONE);
        affectedAssemblies.setText(getRegistry().getString(
                "AffectedAssemblies.Label"));
        SWTUIHelper.setFont(affectedAssemblies, SWT.BOLD);
        
        m_affectedAssembliesText = new Text(composite, SWT.BORDER);
        GridData gd_Text = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                VERTICAL_GRIDS_TEXT);
        m_affectedAssembliesText.setLayoutData(gd_Text);
        m_affectedAssembliesText.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent paramModifyEvent)
            {
                
                if (m_affectedAssembliesText.getText().equalsIgnoreCase(""))
                {
                    m_labelFieldValidator.setValid(false);
                }
                else
                {
                    m_labelFieldValidator.setValid(true);
                }
                MandatoryFieldValidator.updateControl(affectedAssemblies);
            }
        });
        
        UIHelper.makeMandatory(affectedAssemblies, m_labelFieldValidator);
    }
    
    private Composite getMainComposite()
    {
        Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public void update(PublishEvent event)
    {
        
        SetEnabledRunnable runnable = new SetEnabledRunnable(event, this);
        Display.getDefault().syncExec(runnable);
        
    }
}
