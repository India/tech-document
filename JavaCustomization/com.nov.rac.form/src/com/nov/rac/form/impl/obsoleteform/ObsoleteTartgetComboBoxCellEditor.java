package com.nov.rac.form.impl.obsoleteform;

import java.util.Arrays;
import java.util.Vector;

import com.nov.rac.form.renderer.ComboBoxCellRenderer;

public class ObsoleteTartgetComboBoxCellEditor extends ComboBoxCellRenderer
{
	private static String[] listOfValues = { "", "Obsolete", "Replacement" };
	
    public ObsoleteTartgetComboBoxCellEditor(String theLOVName)
    {
        super(theLOVName);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected Vector<String> loadLOV()
    {
        Vector<String> lovValues = new Vector<String>(Arrays.asList(listOfValues));
        return lovValues;
    }
    
}
