package com.nov.rac.form.impl.enform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.common.TargetsTable;
import com.nov.rac.form.common.TargetsTablePanel;
import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import org.eclipse.jface.dialogs.MessageDialog;

public class ENTargetsTableDialogPanel extends AbstractUIPanel implements
        ISubscriber, IPublisher
{
    private Registry m_registry = null;
    private AIFTableModel m_tableModel = null;
    private Button m_copyAllBtn;
    private TCTable m_table;
    private Button m_compareRevBtn;
    private Button m_seeRddPdfBtn;
	private AttachmentsButton m_buttonBar;
	private TargetsTablePanel m_targetsTablePanel;
    private HashMap<String, TCComponent> m_itemIdToComponentMap;
	
	private static final int NUMBER_OF_COLUMN_3 = 3;
	
    public ENTargetsTableDialogPanel(Composite parent, int style)
    {
        super(parent, style);
        
        m_registry = Registry.getRegistry(this);
    }
    
    public void setTableModel(AIFTableModel tableModel)
    {
        m_tableModel = tableModel;
        onSetTableModel();
    }
    
    public boolean createUI()
    {
        Composite parent = getComposite();
        
        parent.getShell().setText(m_registry.getString("ENTargetsTable.TITLE"));
        parent.setLayout(new GridLayout(1, false));
        
        createTopButtonComposite(parent);
        createTargetsTable(parent);
        createBottomButtonComposite(parent);
        
        addPlusMinusButtonListener();
        
        registerProperties();
        
        return true;
    }
    
    private void createTopButtonComposite(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayout(new GridLayout(NUMBER_OF_COLUMN_3, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, false,
                1, 1);
        l_composite.setLayoutData(gd_l_composite);
        
        // create plus-minus buttons
        Image minusButtonImage = new Image(l_composite.getDisplay(),
                m_registry.getImage("removeButton.IMAGE"), SWT.NONE);
        Image pluseButtonImage = new Image(l_composite.getDisplay(),
                m_registry.getImage("addButton.IMAGE"), SWT.NONE);
        
        m_buttonBar = new AttachmentsButton(l_composite, SWT.NONE,
                pluseButtonImage, minusButtonImage);
        GridData gridData1 = new GridData(SWT.RIGHT, SWT.FILL, true, true, 1, 1);
        m_buttonBar.setLayoutData(gridData1);
    }
    
    private void addPlusMinusButtonListener()
    {
        m_buttonBar.addSelectionListenerToMinusButton(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                m_targetsTablePanel.removeSelectedTargets();
            }
        });
        
        m_buttonBar.addSelectionListenerToPlusButton(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                m_targetsTablePanel.addTargetsToWorkflow();
            }
        });
    }
    
    private void createBottomButtonComposite(Composite parent)
    {
        
        parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        
        Composite buttonBar = new Composite(parent, SWT.NONE);
        buttonBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        buttonBar.setLayout(new GridLayout(NUMBER_OF_COLUMN_3, false));
        
        // create copyall button
        m_copyAllBtn = new Button(buttonBar, SWT.NONE);
        m_copyAllBtn.setText(m_registry.getString("CopyAllButton.Lable"));
        
        GridData gridData = new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1);
        m_copyAllBtn.setLayoutData(gridData);
        addCopyAllButtonListener();
        
        // create compare rev button
        m_compareRevBtn = new Button(buttonBar, SWT.NONE);
        m_compareRevBtn.setText(m_registry.getString("CompareRevButton.Lable"));
        
        GridData gridData1 = new GridData(SWT.RIGHT, SWT.FILL, false, false, 1,
                1);
        m_compareRevBtn.setLayoutData(gridData1);
        
        // create see rdd/pdf button
        m_seeRddPdfBtn = new Button(buttonBar, SWT.NONE);
        m_seeRddPdfBtn.setText(m_registry.getString("SeeRDDPdfButton.Lable"));
        
        GridData gridData2 = new GridData(SWT.RIGHT, SWT.FILL, false, false, 1,
                1);
        m_seeRddPdfBtn.setLayoutData(gridData2);
        addListenerToSeeRDDPDFButton();
        addListernerToCompareRevButton();
        
    }
    
    private void addListernerToCompareRevButton()
    {
        m_compareRevBtn.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                try
                {
                    validateSingleSelection();
                    fireCompareEvent();
                }
                catch (TCException ex)
                {
                    MessageBox.post(ex);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }
        });
        
    }
    protected void validateSingleSelection() throws TCException
    {
        if(m_table.getSelectedRowCount() != 1)
        {
            String[] errors =  new String[]{ m_registry.getString("pleaseSelectOneRow.MSG") };
            TCException exception = new TCException( errors );
            
            throw exception;
        }
    }
    private void fireCompareEvent() throws Exception
    {
        IPropertyMap iMap = new SimplePropertyMap();
        if (isSelectedItemComparable())
        {
            TCComponent[] selectedRevisions = getSelectedItemRevisions();
            iMap.setComponent(selectedRevisions[0]);
            Shell shell = m_compareRevBtn.getShell();
            PopupDialog dialog = new PopupDialog(shell);
            dialog.setInputPropertyMap(iMap);
            dialog.openCompareDialog(shell);
        }
        else
        {
            String[] errors = new String[] {m_targetsTablePanel.getRegistry().getString(
                    "notComparableRevision.MSG") };
            TCException exception = new TCException(errors);
            throw exception;
        }
    }
    protected boolean isSelectedItemComparable()
    {
        boolean isComparable = false;
        try
        {
            TCComponent[] selectedItemRevisions = getSelectedItemRevisions();
            String selectedItemType = selectedItemRevisions[0].getProperty("object_type");
            if(!selectedItemType.equals("Documents Revision"))
            {
                TCComponent[] itemRevisions = FormHelper.getItemRevisions(selectedItemRevisions[0]);
                
                if(itemRevisions.length > 1)
                {
                    isComparable = true;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return isComparable;
    }
    protected TCComponent[] getSelectedItemRevisions()
    {
        List<TCComponent> targetToRemove = new ArrayList<>(); 
        
        // 1.0 Get all selected item revisions from table
        int itemIDColIndex = m_table.getColumnIndex(m_registry
                .getString("TargetsTableColumnIdentifier.ItemID"));
        
        for(int rowIndex : m_table.getSelectedRows())
        {
            String itemID = (String) m_table.getValueAt(rowIndex, itemIDColIndex);
            TCComponent itemRevision = m_itemIdToComponentMap.get(itemID);
            
            if(itemRevision != null)
            {
                targetToRemove.add(itemRevision);
            }
        }
        
        return targetToRemove.toArray( new TCComponent[targetToRemove.size()]);
    }
    
    
    private void addListenerToSeeRDDPDFButton()
    {
        m_seeRddPdfBtn.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                m_targetsTablePanel.viewSelectedRDDPDF();
            }
        });
    }
    
    private void createTargetsTable(Composite paramComposite)
    {
        m_targetsTablePanel = new TargetsTablePanel(paramComposite, SWT.NONE);
        try
        {
            m_targetsTablePanel.setRegistry(m_registry);
            m_targetsTablePanel.setGrabVarticalSpace(true);
            m_targetsTablePanel.createUI();
            m_table = m_targetsTablePanel.getTable();
            
            // initialize the itemID to Component and itemID to Disposition Maps
            m_targetsTablePanel.initializeItemRevDispositionMaps();
            m_itemIdToComponentMap = WorkflowProcess.getWorkflow().getStringToComponentMap();
            //Fix for double inclusion of added target item revisions
            m_targetsTablePanel.unRegisterSubscriber();
            addRowSelectionListener();
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void hideColumns()
    {
        // hide columns
        String[] hideColumnNames = m_registry
                .getStringArray("TargetsTableDialog.HIDE_COLUMNS");
        int colIndex = -1;
        for (String colName : hideColumnNames)
        {
            colIndex = m_table.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_table, colIndex);
        }
    }
    
    private void addCopyAllButtonListener()
    {
        m_copyAllBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                try
                {
                    validateSingleSelection();//Validating the row selected in table
                    
                    //Displaying confirmation message to invoke CopyAll action
                    boolean response = MessageDialog.openConfirm(
                            m_copyAllBtn.getShell(),
                            m_registry.getString("CopyAllConfirmation.TITLE"),
                            m_registry.getString("CopyAllConfirmation.MSG"));
                    
                    if (response)
                    {
                        publishCopyAllDispositon();
                    }
                    
                }
                catch (TCException e)
                {
                    MessageBox.post(e);
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent paramSelectionEvent)
            {
            }
        });
    }
    
    private void publishCopyAllDispositon()
    {
        int rowInx = m_table.getSelectedRow();
        int colInx = -1;
        String value = "";
        
        IPropertyMap propMap = new SimplePropertyMap();
        
        String[] editableColumns = m_registry
                .getStringArray("TargetsTable.EDITABLE_COLUMNS");
        for (String colName : editableColumns)
        {
            colInx = m_table.getColumnIndex(colName);
            value = (String) m_table.getValueAt(rowInx, colInx);
            propMap.setString(colName, value);
        }
        
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        PublishEvent event = new PublishEvent(this,
                GlobalConstant.EVENT_COPY_ALL_DISPOSITION_PROPERTY, propMap,
                null);
        
        controller.publish(event);
    }
    
    private void onSetTableModel()
    {
        m_table.setModel(m_tableModel);
        ((TCTable) m_table).assignColumnRenderer();
        ((TargetsTable) m_table).addColumnRenderersEditors();
        
        hideColumns();
    }
    
    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        l_controller.registerSubscriber(GlobalConstant.EVENT_TARGETS_ADDED,
                this);
        l_controller.registerSubscriber(
                GlobalConstant.EVENT_DISPOSITION_INSTRUCTION_CHANGED, this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(GlobalConstant.EVENT_TARGETS_ADDED))
        {
            m_table.repaint();
        }
        else if (event.getPropertyName().equals(
                GlobalConstant.EVENT_DISPOSITION_INSTRUCTION_CHANGED))
        {
            String newInstuction = (String) event.getNewValue();
            updateDispoInstruValue(newInstuction);
        }
    }
    
    private void updateDispoInstruValue(String newInstuction)
    {
        // get disposition instructions from table
        int rowIndex = m_table.getSelectedRow();
        
        String columnName = m_registry
                .getString("TargetsTableColumnIdentifier.DispositionInstruction");
        int instColIndex = m_table.getColumnIndex(columnName);
        
        m_table.setValueAt(newInstuction, rowIndex, instColIndex);
        ((AIFTableModel) m_table.getModel()).fireTableRowsUpdated(rowIndex,
                rowIndex);
    }
    
    @Override
    public void dispose()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        l_controller.unregisterSubscriber(GlobalConstant.EVENT_TARGETS_ADDED,
                this);
        
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_DISPOSITION_INSTRUCTION_CHANGED, this);
        
        super.dispose();
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void addRowSelectionListener()
    {
        m_table.getSelectionModel().addListSelectionListener(
                new ListSelectionListener()
                {
                    @Override
                    public void valueChanged(
                            ListSelectionEvent listselectionevent)
                    {
                        // toggling CopyAll Button based upon the selection in table
                        toggleCopyAllButtonState();
                    }
                    
                });
    }
    
    private void addCompareRevsListener()
    {
        m_compareRevBtn.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                try
                {
                    // validate single row is selected from table
                    validateSingleSelection();
                    fireCompareEvent();
                }
                catch (TCException e)
                {
                    MessageBox.post(e);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    // Toggling CopyAll button by the Value of hidden column 'hasDisposition' of particular
    // Row Selected in Table
    private void toggleCopyAllButtonState()
    {
        int colIndex = ((TCTable) m_table).getColumnIndex("hasDisposition");
        int rowIndex = m_table.getSelectedRow();
        
        if (colIndex >= 0 && rowIndex >= 0)
        {
            final boolean isEditable = (Boolean) m_table.getValueAt(
                    m_table.getSelectedRow(), colIndex);
            
            Display.getDefault().asyncExec(new Runnable()
            {
                @Override
                public void run()
                {
                    m_copyAllBtn.setEnabled(isEditable);
                }
            });
            
        }
    }
    
}
