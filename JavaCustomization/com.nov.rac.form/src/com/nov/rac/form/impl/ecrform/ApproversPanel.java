package com.nov.rac.form.impl.ecrform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ApproversPanel extends AbstractUIPanel implements INOVFormLoadSave
{
	
	private Registry m_registry = null;
	
	//private Label l_lblValApprovers;
	
	private List m_approversList;
	
	private Label m_lblValApproveOn;
	
	private Label m_lblValApprovalGroup;

	public ApproversPanel(Composite parent, int style)
	{
		super(parent, style);
		m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
    	final Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(1, true));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        l_composite.setLayoutData(gd_panel);
        
        createApproversRow(l_composite);
    	
    	return true;
    }

	private void createApproversRow(final Composite l_composite)
	{
		Composite composite = new Composite(l_composite,SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(6, false));
		
		Label l_lblApprovers = new Label(composite, SWT.NONE);
        l_lblApprovers.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblApprovers.setText(m_registry.getString("lblApprovers.NAME",
                "Key Not Found"));
        
        /*l_lblValApprovers = new Label(composite, SWT.NONE);
        l_lblValApprovers.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        SWTUIHelper.setFont(l_lblValApprovers, SWT.BOLD);*/
        
        m_approversList = new List(composite, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		m_approversList.setLayoutData(gridData);
		SWTUIHelper.setFont(m_approversList, SWT.BOLD);
        
        Label l_lblApproveOn = new Label(composite, SWT.NONE);
        l_lblApproveOn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblApproveOn.setText(m_registry.getString("lblApproveOn.NAME",
                "Key Not Found"));
        
        m_lblValApproveOn = new Label(composite, SWT.NONE);
        m_lblValApproveOn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        SWTUIHelper.setFont(m_lblValApproveOn, SWT.BOLD);
        
        Label l_lblApprovalGroup = new Label(composite, SWT.NONE);
        l_lblApprovalGroup.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblApprovalGroup.setText(m_registry.getString("lblApprovalGroup.NAME",
                "Key Not Found"));
        
        m_lblValApprovalGroup = new Label(composite, SWT.NONE);
        m_lblValApprovalGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        SWTUIHelper.setFont(m_lblValApprovalGroup, SWT.BOLD);
	}

	@Override
	public boolean load(IPropertyMap propMap) throws TCException 
	{
		/*l_lblValApprovers.setText(PropertyMapHelper.handleNull(propMap
                .getString("disposition_by")));*/
		
		m_approversList.setItems(PropertyMapHelper.handleNull(propMap
				.getStringArray("disposition_by")));
		
		// Date Labels
        this.setDate(m_lblValApproveOn, "decision_date", propMap);
		
		m_lblValApprovalGroup.setText(PropertyMapHelper.handleNull(propMap
                .getString("group")));
		
		return true;
	}
	
	private void setDate(Label dateLabel, String propertyName,
            IPropertyMap propMap)
    {
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate == null)
        {
            // do something?ask input form kishore:can write NA?or -?
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateLabel.setText(dateFormat.format(theDate));
        }
    }

	@Override
	public boolean save(IPropertyMap propMap) throws TCException 
	{
		return false;
	}

	@Override
	public boolean validate(IPropertyMap propMap) throws TCException 
	{
		return false;
	}

	@Override
	public void setEnabled(boolean flag) 
	{
		
	}

	@Override
	public boolean reload() throws TCException 
	{
		return false;
	}

}
