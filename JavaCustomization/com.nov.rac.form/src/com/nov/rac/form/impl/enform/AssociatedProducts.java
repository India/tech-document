package com.nov.rac.form.impl.enform;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.summarypage.NOVProductCategoryTree;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.pibhelper.NOVRelatedObjectsSOAHelper;
import com.nov.rac.utilities.utils.MandatoryFieldValidator;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.RelatedObjectResponse;
import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class AssociatedProducts extends AbstractUIPanel implements
        INOVFormLoadSave,ISubscriber
{
    private static final int HINT_HEIGHT = 100;
    private static final int COLUMNS_SIDE_COMPOSITE = 10;
    private static final int COLUMNS_SUB_MAIN_COMPOSITE = 2 * COLUMNS_SIDE_COMPOSITE + 1;
    private Text m_searchText;
    private NOVProductCategoryTree m_availableProductTree;
    private Registry m_registry;
    private TCTable m_selectedTCTable;
    private TCComponent m_pibform;
    protected MandatoryFieldValidator m_labelFieldValidator = new MandatoryFieldValidator();
    private Label m_associatedProducts;
    private Button m_plusButton;
    private Button m_minusButton;
    private Button m_searchButton;
    
    public AssociatedProducts(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        // TODO Auto-generated constructor stub
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        IPropertyMap pibFormPropMap = propMap.getCompound("Nov4_secondary_form");
        m_pibform = pibFormPropMap.getComponent();
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        TCPreferenceService prefServ = tcSession.getPreferenceService();
        String sParentProdCategory = prefServ.getString(
                TCPreferenceService.TC_preference_all,
                "NOV_associated_products_parent_id");
        
        TCComponentItemType itemType = (TCComponentItemType) tcSession
                .getTypeService().getTypeComponent("ProductCategory");
        
        
//        TCComponentItem rootNode = itemType.find(sParentProdCategory);
        TCComponentItem rootNode = itemType.find("M000000102");
        populateSourceList(rootNode);
        populateSelectedList(pibFormPropMap);
        
        return false;
    }
    
    private void populateSelectedList(IPropertyMap propMap)
    {
        String[] associatedProducts = propMap
                .getStringArray("associatedproducts");
        if (associatedProducts == null)
        {
            return;
        }
        for (String eachProduct : associatedProducts)
        {
            findNodes(eachProduct);
            addProductstoSelected();
        }
        expandAll(m_availableProductTree,
                new TreePath(m_availableProductTree.getRootNode()), false);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setComponent(m_pibform);
        
        String[] array = getSelectedProductsList();
        
        propMap.setStringArray("associatedproducts", array);
        return false;
    }
    
    private String[] getSelectedProductsList()
    {
        int column = m_selectedTCTable.getColumnIndex(getRegistry().getString(
                "AssociatedProducts.ProductsColumn "));
        Vector<String> associatedProducts = new Vector<String>();
        
        for (int inx = 0; inx < m_selectedTCTable.getRowCount(); inx++)
        {
            associatedProducts.add((String) m_selectedTCTable.getValueAt(inx,
                    column));
        }
        String[] array = associatedProducts.toArray(new String[] {});
        return array;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_searchText.setEnabled(flag);
        m_availableProductTree.setEnabled(flag);
        m_selectedTCTable.setEnabled(flag);
        m_minusButton.setEnabled(flag);
        m_plusButton.setEnabled(flag);
        m_searchButton.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getMainComposite();
        
        createHeaderComposite(mainComposite);
        createAssociatedProductsComposite(mainComposite);
        createDataModelMapping();
        registerSubscriber();
        setEnabled(false);
        return false;
    }
    
    private void createHeaderComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        m_associatedProducts = new Label(composite, SWT.NONE);
        m_associatedProducts.setText(getRegistry().getString(
                "AssociatedProducts.Label"));
        SWTUIHelper.setFont(m_associatedProducts, SWT.BOLD);
    }
    
    private void createAssociatedProductsComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(COLUMNS_SUB_MAIN_COMPOSITE,
                true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        createleftComposite(composite);
        createButtonBar(composite);
        createRightComposite(composite);
    }
    
    private void createRightComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true,
                COLUMNS_SIDE_COMPOSITE, 1);
        gridData.heightHint = HINT_HEIGHT;
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        createSelectedProductTCTable();
        
        JScrollPane scrollPane = new JScrollPane(m_selectedTCTable);
        SWTUIUtilities.embed(composite, scrollPane, false);
        
    }
    
    protected void createSelectedProductTCTable()
    {
        String[] columnNames = getRegistry().getStringArray(
                "AssociatedProducts.ColumnNames");
        String[] columnIdentifiers = getRegistry().getStringArray(
                "AssociatedProducts.ColumnIdentifiers");
        
        m_selectedTCTable = new TCTable(columnIdentifiers, columnNames);
        m_selectedTCTable.setShowVerticalLines(true);
        m_selectedTCTable
                .setAutoResizeMode(m_selectedTCTable.AUTO_RESIZE_ALL_COLUMNS);
        
        m_selectedTCTable.getTableHeader().setVisible(false);
        m_selectedTCTable.getTableHeader().setPreferredSize(
                new Dimension(-1, 0));
        
        int colIndex = m_selectedTCTable.getColumnIndex(getRegistry()
                .getString("AssociatedProducts.ColumnsToRender"));
        TableColumn column = m_selectedTCTable.getColumnModel().getColumn(
                colIndex);
        
        hideColumns();
        column.setCellRenderer(new PartIDIconRenderer());
        
        m_selectedTCTable.getModel().addTableModelListener(
                new TableModelListener()
                {
                    
                    @Override
                    public void tableChanged(
                            TableModelEvent paramTableModelEvent)
                    {
                        if (m_selectedTCTable.getRowCount() <= 0)
                        {
                            m_labelFieldValidator.setValid(false);
                        }
                        else
                        {
                            m_labelFieldValidator.setValid(true);
                        }
                        MandatoryFieldValidator
                                .updateControl(m_associatedProducts);
                    }
                    
                });
        
        UIHelper.makeMandatory(m_associatedProducts, m_labelFieldValidator);
    }
    
    private void hideColumns()
    {
        String[] hideColumns = getRegistry().getStringArray(
                "AssociatedProducts.ColumnsToHide");
        for (String column : hideColumns)
        {
            int colIndex = m_selectedTCTable.getColumnIndex(column);
            TableUtils.hideTableColumn(m_selectedTCTable, colIndex);
        }
    }
    
    private void createButtonBar(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        createPlusButton(composite);
        
        createMinusButton(composite);
    }
    
    private void createMinusButton(Composite composite)
    {
        m_minusButton = new Button(composite, SWT.NONE);
        GridData gd_button2 = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        m_minusButton.setLayoutData(gd_button2);
        
        Image minusButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        m_minusButton.setImage(minusButtonImage);
        m_minusButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                int[] selectedRows = m_selectedTCTable.getSelectedRows();
                Arrays.sort(selectedRows);
                
                for (int inx = selectedRows.length - 1; inx >= 0;inx--) 
                {
                    m_selectedTCTable.removeRow(inx);
                }
            }
        });
    }
    
    private void createPlusButton(Composite composite)
    {
        m_plusButton = new Button(composite, SWT.NONE);
        GridData gd_button1 = new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1,
                1);
        m_plusButton.setLayoutData(gd_button1);
        
        Image plusButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_plusButton.setImage(plusButtonImage);
        m_plusButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                addProductstoSelected();
            }
        });
    }
    
    private void createleftComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true,
                COLUMNS_SIDE_COMPOSITE, 1);
        
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        createSearchComposite(composite);
        createAvailableProductList(composite);
        
    }
    
    public void populateSourceList(TCComponentItem rootNode)
    {
        TCTreeNode treeRootNode = new TCTreeNode(rootNode);
        RelatedObjectResponse theResponse = null;
        theResponse = NOVRelatedObjectsSOAHelper.getRelatedObjects(rootNode);
        m_availableProductTree
                .setRelatedObjectsOutput(theResponse.relatedObjects[0]);
        m_availableProductTree.setRoot(treeRootNode);
        expandAll(m_availableProductTree, new TreePath(treeRootNode), false);
        m_availableProductTree.loadAllNodes(m_availableProductTree
                .getRootNode());
    }
    
    private void createAvailableProductList(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gridData.heightHint = HINT_HEIGHT;
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        m_availableProductTree = new NOVProductCategoryTree();
        JScrollPane scrollPane = new JScrollPane(m_availableProductTree);
        m_availableProductTree.setRoot(null);
        SWTUIUtilities.embed(composite, scrollPane, false);
    }
    
    private void createSearchComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.BORDER);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_l_composite = new GridLayout(2, false);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        m_searchText = new Text(composite, SWT.NONE);
        GridData gd_searchText = new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1);
        m_searchText.setLayoutData(gd_searchText);
        composite.setBackground(m_searchText.getBackground());
        m_searchButton = new Button(composite, SWT.NONE);
        m_searchButton.setText("S");
        m_searchButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                findNodes(m_searchText.getText());
            }
        });
        
    }
    
    protected void findNodes(String srchString)
    {
       
        
        AIFTreeNode[] findNode = (AIFTreeNode[]) m_availableProductTree
                .findNodes(srchString);
        expandAll(m_availableProductTree,
                new TreePath(m_availableProductTree.getRootNode()), false);
        // for (AIFTreeNode treeNode : findNode)
        // {
        // m_availableProductTree.expandPath(treeNode.getTreePath());
        //
        // }
        m_availableProductTree.setSelectedNode(findNode);
    }
    
    private void expandAll(JTree tree, TreePath parent, boolean expand)
    {
        // Traverse children
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        if (node.getChildCount() >= 0)
        {
            for (Enumeration e = node.children(); e.hasMoreElements();)
            {
                TreeNode n = (TreeNode) e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                expandAll(tree, path, expand);
            }
        }
        // Expansion or collapse must be done bottom-up
        if (expand)
        {
            tree.expandPath(parent);
        }
        else
        {
            tree.collapsePath(parent);
        }
    }
    
    private Composite getMainComposite()
    {
        Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void addProductstoSelected()
    {
        InterfaceAIFComponent[] selectedComponents = m_availableProductTree
                .getSelectedComponents();
        // TCComponent[] components = Arrays.copyOf(selectedComponents,
        // selectedComponents.length, TCComponent[].class);
        
        for (InterfaceAIFComponent eachComponent : selectedComponents)
        {
            TCComponent eachTCComponent = (TCComponent) eachComponent;
            if (eachTCComponent.getType().equalsIgnoreCase("Product"))
            {
                String[] selectedProductsList = getSelectedProductsList();
                
                try
                {
                    if (Arrays.asList(selectedProductsList).contains(
                            eachTCComponent.getProperty("object_string")))
                    {
                        continue;
                    }
                    TableUtils.populateTable(
                            new TCComponent[] { eachTCComponent },
                            m_selectedTCTable);
                }
                catch (TCException e1)
                {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_selectedTCTable, "associatedproducts",
                FormHelper.getDataModelName());
        
    }
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public void update(PublishEvent event)
    {
        
        SetEnabledRunnable runnable = new SetEnabledRunnable(event, this);
        Display.getDefault().syncExec(runnable);
        
    }
}
