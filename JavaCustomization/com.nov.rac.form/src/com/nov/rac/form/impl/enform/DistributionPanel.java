package com.nov.rac.form.impl.enform;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.DualList;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.interfaces.IAddressBook;
import com.nov.rac.interfaces.IPropertyChangeNotifier;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.addressbook.dialog.SimpleAddressBookDialog;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.QueryUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class DistributionPanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave, PropertyChangeListener
{
    
    private Registry m_registry = null;
    private Group m_distributionGroupContainer = null;
    private Text m_emailText = null;
    private Button m_addEmailButton = null;
    private List m_distributionList = null;
    private Button m_globalAddressButton = null;
    private DualList m_dualList = null;
    
    public DistributionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
     
        String[]targets = PropertyMapHelper
                .handleNull(propMap.getStringArray("distribution"));
        TCTable targetTable = m_dualList.getTargetTable();
       
        
        for (int inx = 0; inx < targets.length; inx++)
        {
            java.util.List<String> tableRow = new java.util.ArrayList<String>();
            tableRow.add(targets[inx]);
            targetTable.addRow(tableRow);
        }
        return true;
    }
    
    protected void loadSelectedList(IPropertyMap propMap)
    {
        // String[] targetList = new String[] {};
        // targetList = PropertyMapHelper.handleNull(propMap
        // .getStringArray("distribution"));
        //
        // if (targetList.length > 0)
        // {
        // for (String email : targetList)
        // {
        // m_dualList.addToTargetList(email);
        // }
        // }
    }
    
    protected void loadECRDistributionList(IPropertyMap propMap)
    {
        String[] relatedECR = new String[] {};
        relatedECR = PropertyMapHelper.handleNull(propMap
                .getStringArray("relatedecr"));
        String[] distributionList = getECRDistributionList(relatedECR);
        
        if (distributionList.length > 0)
        {
            for (String email : distributionList)
            {
                m_distributionList.add(email);
            }
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        TCTable targetTable = m_dualList.getTargetTable();
        int rowCount =targetTable.getRowCount();
        String[] allValues_TargetTable = new String[rowCount];
    
        
        for (int inx = 0; inx < rowCount; inx++)
        {
            allValues_TargetTable[inx] = (String) targetTable.getValueAt(inx, 0);
        }
        propMap.setStringArray("distribution", allValues_TargetTable);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_dualList.setEnabled(flag);
        m_addEmailButton.setEnabled(flag);
        m_globalAddressButton.setEnabled(flag);
        m_emailText.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(2, true));
        getExpandableComposite().setText("Distribution");
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        // createProjectGroupContainer(l_composite);
        createDualList(l_composite);
        // createECRDistributionList();
        createEmailTextField(l_composite);
        addButtonListener();
        createDataModelMapping();
        
        return true;
    }
    
    protected void createECRDistributionList()
    {
        Composite distributionListComposite = new Composite(
                m_distributionGroupContainer, SWT.NONE);
        GridData list_gridData = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1);
        distributionListComposite.setLayoutData(list_gridData);
        distributionListComposite.setLayout(new GridLayout(1, true));
        
        createDistributionListHeader(distributionListComposite);
        
        m_distributionList = new List(distributionListComposite, SWT.BORDER);
        GridData gd_distributionList = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 23);
        m_distributionList.setLayoutData(gd_distributionList);
        m_distributionList.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        m_distributionList.setEnabled(false);
    }
    
    protected void createDistributionListHeader(
            Composite distributionListComposite)
    {
        Label lbldistribution = new Label(distributionListComposite, SWT.NONE);
        lbldistribution.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER,
                true, false, 1, 1));
        lbldistribution.setText(getRegistry().getString("Distribution.Label"));
        SWTUIHelper.setFont(lbldistribution, SWT.BOLD);
    }
    
    protected void createDualList(Composite composite)
    {
        /*
         * m_dualList = new DistributionDualList(m_distributionGroupContainer,
         * getRegistry());
         */
        m_dualList = new DualList(composite, getRegistry());
        // m_dualList.changeSpan(1, 6);
        m_dualList.createUI();
    }
    
    protected void createEmailTextField(Composite composite)
    {
        Composite subComposite = new Composite(composite, SWT.NONE);
        subComposite.setLayout(new GridLayout(20, true));
        subComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 2, 1));
        // new Label(subComposite,SWT.NONE);
        Label emptyLabel = new Label(subComposite,SWT.NONE);
        emptyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 11, 1));
        m_emailText = new Text(subComposite, SWT.BORDER);
        m_emailText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 7, 1));
        m_emailText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        createAddEmailButton(subComposite);
        
        createGlobalAddressButton(subComposite);
        // new Label(subComposite,SWT.NONE);
    }
    
    protected void createAddEmailButton(Composite composite)
    {
        m_addEmailButton = new Button(composite, SWT.NONE);
        Image addEmailButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_addEmailButton.setImage(addEmailButtonImage);
    }
    
    protected void createGlobalAddressButton(Composite composite)
    {
        m_globalAddressButton = new Button(composite, SWT.NONE);
        Image addressButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("globalAddressButton.IMAGE"), SWT.NONE);
        m_globalAddressButton.setImage(addressButtonImage);
    }
    
    protected void createProjectGroupContainer(Composite composite)
    {
        String label = getRegistry().getString(
                "DistributionPanel.GroupContainerText");
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 7, 1);
        m_distributionGroupContainer = new Group(composite, SWT.NONE);
        m_distributionGroupContainer.setLayoutData(gridData);
        m_distributionGroupContainer.setLayout(new GridLayout(3, true));
        m_distributionGroupContainer.setText(label);
        SWTUIHelper.setFont(m_distributionGroupContainer, SWT.BOLD);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    protected void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_dualList.getTargetTable().dataModel, "distribution",
                FormHelper.getDataModelName());
    }
    
    private void addButtonListener()
    {
        m_globalAddressButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
//                SwingUtilities.invokeLater(new Runnable()
//                {
//                    public void run()
//                    {
//                        String viewerClass = getRegistry().getString(
//                                "Addressbook.CLASS");
//                        Object[] params = new Object[2];
//                        params[0] = AIFUtility.getCurrentApplication()
//                                .getDesktop();
//                        params[1] = false;
//                        Shell shell = AIFDesktop.getActiveDesktop().getShell();
//                        // set string to argument
//                        final IAddressBook globalAddressBook;
//                        try
//                        {
////                            globalAddressBook = (IAddressBook) Instancer
////                                    .newInstanceEx(viewerClass, params);
//                            globalAddressBook = new GlobalAddressBookDialog(shell);
//                            globalAddressBook.setVisible(true);
//                            
//                            Display.getDefault().asyncExec(new Runnable()
//                            {
//                                
//                                @Override
//                                public void run()
//                                {
//                                    // m_dualList.getTargetList().add(globalAddressBook.getSelectedText());
//                                    m_dualList
//                                            .addToTargetTable(globalAddressBook
//                                                    .getSelectedText());
//                                }
//                            });
//                            
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
//                });
                
                try
                {
                    String viewerClass = getRegistry().getString("Addressbook.CLASS");
                    Object[] params = new Object[1];
                    params[0] = AIFDesktop.getActiveDesktop().getShell();
                    
                    IAddressBook addressBookDialog = (IAddressBook) Instancer.newInstanceEx(viewerClass, params);
                    
                    addressBookDialog.setButtonsForDialog(SimpleAddressBookDialog.OK_BUTTON_ID
                            | SimpleAddressBookDialog.APPLY_BUTTON_ID | SimpleAddressBookDialog.CANCEL_BUTTON_ID);
                    
                    if (addressBookDialog instanceof IPropertyChangeNotifier)
                    {
                        ((IPropertyChangeNotifier) addressBookDialog).addPropertyChangeListener(DistributionPanel.this);
                    }
                    
                    addressBookDialog.open();
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
        
        m_addEmailButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                addEmailAddress();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }
    
    protected void addEmailAddress()
    {
        String emailAddress = m_emailText.getText();
        if (FormHelper.isValidEmail(emailAddress))
        {
            // int objInd = m_dualList.getTargetList().indexOf(emailAddress);
            // if (objInd >= 0)
            // {
            // MessageBox.post(getRegistry().getString("hasMailIds.MSG")
            // + " : " + emailAddress,
            // getRegistry().getString("info.TITLE"),
            // MessageBox.INFORMATION);
            // }
            // else
            {
                m_dualList.addToTargetTable(emailAddress);
            }
        }
        else
        {
            MessageBox.post(getRegistry().getString("emailIdFormat.MSG")
                    + " : " + emailAddress,
                    getRegistry().getString("info.TITLE"),
                    MessageBox.INFORMATION);
        }
    }
    
    protected String[] getECRDistributionList(String[] strEcrNumber)
    {
        TCProperty propObjs = null;
        String[] distributionList = new String[] {};
        try
        {
            for (int index = 0; index < strEcrNumber.length; index++)
            {
                String[] ipEntries = { "Name", "Type" };
                String[] ipValues1 = { strEcrNumber[index], "RS ECR Form" };
                String query = "General...";
                int resultsType = 0;
                
                TCComponent[] foundECRForms = QueryUtils.executeSOAQuery(
                        ipEntries, ipValues1, query, resultsType);
                
                propObjs = foundECRForms[0].getTCProperty(getRegistry()
                        .getString("nov4Distribution.PROP"));
                
                if (propObjs.getStringValueArray() != null)
                {
                    distributionList = propObjs.getStringValueArray();
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return distributionList;
    }


    private void setSelectedIdToList(String[] selectedNames)
    {
        if (selectedNames != null && selectedNames.length > 0)
        {
            m_dualList.addToTargetTable(selectedNames[0]);
        }
    }
    
    
    private IController getController()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        return controller;
    }
    

    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        if(event.getPropertyName().equalsIgnoreCase(SimpleAddressBookDialog.ADDRESS_BOOK_MAIL_IDS_SELECTED))
        {
            String[] selectedIds = (String[]) event.getNewValue();
            setSelectedIdToList(selectedIds);
        }
        
    }
}
