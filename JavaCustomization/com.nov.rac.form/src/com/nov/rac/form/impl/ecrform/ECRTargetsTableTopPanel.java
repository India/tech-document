package com.nov.rac.form.impl.ecrform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TargetsTableTopPanel;
import com.teamcenter.rac.util.Registry;

public class ECRTargetsTableTopPanel extends TargetsTableTopPanel 
{

	public ECRTargetsTableTopPanel(Composite parent, int style) 
	{
		super(parent, style);
		Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
	}

}
