package com.nov.rac.form.impl.enform;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.interfaces.IAddressBook;
import com.nov.rac.interfaces.IPropertyChangeNotifier;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.addressbook.dialog.SimpleAddressBookDialog;
import com.nov.rac.utilities.utils.MandatoryFieldValidator;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ProductBulletinReviewNotificationPanel extends AbstractUIPanel
        implements INOVFormLoadSave,ISubscriber, PropertyChangeListener
{
    private static final int HORIZONTAL_SPACING = 10;
    private static final int COLUMNS_LEFT_COMPOSITE = 3;
    private static final int COMPOSITE_SIZE = 10;
    private static final int COLUMNS_MAIN_COMPOSITE = (2 * COMPOSITE_SIZE) + 1;
    protected MandatoryFieldValidator m_labelFieldValidator = new MandatoryFieldValidator();
    
    private Registry m_registry;
    private Text m_addText;
    private Combo m_reviewTimeCombo;
    private Text m_legalReviewText;
    private List m_mailList;
    private Button m_addButton;
    private Button m_minusButton;
    private Button m_mailButton;
    
    public ProductBulletinReviewNotificationPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap pibFormPropMap = propMap.getCompound("Nov4_secondary_form");
        loadReviewTimeCombo(pibFormPropMap);
        
        m_mailList.setItems(pibFormPropMap.getStringArray("nov4_emailnotification"));
        
        m_legalReviewText.setText(PropertyMapHelper.handleNull(pibFormPropMap
                .getString("legal")));
        m_legalReviewText.notifyListeners(SWT.Selection, new Event());
        return false;
    }
    
    private void loadReviewTimeCombo(IPropertyMap propMap) throws TCException
    {
        if (!(m_reviewTimeCombo.getItemCount() > 0))
        {
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            TCComponentListOfValues lovValues = TCComponentListOfValuesType
                    .findLOVByName(tcSession,
                            (m_registry.getString("PIBEmailLOV.NAME")));
            Object[] lovS = null;
            if (lovValues != null)
            {
                ListOfValuesInfo lovInfo = lovValues.getListOfValues();
                lovS = lovInfo.getListOfValues();
            }
            String[] copyOf = Arrays.copyOf(lovS, lovS.length, String[].class);
            m_reviewTimeCombo.setItems(copyOf);
        }
        int indexOf = m_reviewTimeCombo.indexOf(PropertyMapHelper
                .handleNull(propMap.getString("nov4_email_time")));
        if (indexOf > 0)
        {
            m_reviewTimeCombo.setText(m_reviewTimeCombo.getItem(indexOf));
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString("nov4_email_time", m_reviewTimeCombo.getText());
        propMap.setStringArray("nov4_emailnotification", m_mailList.getItems());
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
       m_reviewTimeCombo.setEnabled(flag);
       m_mailList.setEnabled(flag);
       m_addText.setEnabled(flag);
       m_minusButton.setEnabled(flag);
       m_addButton.setEnabled(flag);
       m_mailButton.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        Label headerLabel = createLabel(
                composite,
                getRegistry().getString(
                        "ProductBulletinReviewNotification.Label"));
        SWTUIHelper.setFont(headerLabel, SWT.BOLD);
        creteSubHeaderComposite(composite);
        
        createSubComposite(composite);
        
        registerSubscriber();
        setEnabled(false);
        return false;
    }
    
    private void creteSubHeaderComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        gl_l_composite.verticalSpacing = 0;
        composite.setLayout(gl_l_composite);
        
        createLabel(composite, "SuggestedNotification.Label");
        createLabel(composite, "Groups.Label");
        
    }
    
    private void createSubComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(COLUMNS_MAIN_COMPOSITE, true);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        createLeftComposite(composite);
        new Label(composite, SWT.NONE);
        createRightComposite(composite);
        
    }
    
    private void createRightComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                COMPOSITE_SIZE, 1);
        composite.setLayoutData(gd_composite);
        
        GridLayout gl_l_composite = new GridLayout(2, false);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        gl_l_composite.horizontalSpacing = SWTUIHelper
                .convertHorizontalDLUsToPixels(composite, HORIZONTAL_SPACING);
        composite.setLayout(gl_l_composite);
        
        final Label reviewEmailTimeLabel = createLabel(composite, getRegistry()
                .getString("ReviewEmailTime.Label"));
        
        m_reviewTimeCombo = new Combo(composite, SWT.NONE);
        UIHelper.makeMandatory(reviewEmailTimeLabel, m_labelFieldValidator);
        
        m_reviewTimeCombo.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent paramModifyEvent)
            {
                if (m_reviewTimeCombo.getText().equalsIgnoreCase(""))
                {
                    m_labelFieldValidator.setValid(false);
                }
                else
                {
                    m_labelFieldValidator.setValid(true);
                }
                MandatoryFieldValidator.updateControl(reviewEmailTimeLabel);
            }
        });
        
        createLabel(composite, getRegistry().getString("LegalReview.Label"));
        m_legalReviewText = new Text(composite, SWT.NONE);
        
    }
    
    private void createLeftComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
                COMPOSITE_SIZE, 1);
        composite.setLayoutData(gd_composite);
        
        GridLayout gl_l_composite = new GridLayout(COLUMNS_LEFT_COMPOSITE,
                false);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
        m_addText = new Text(composite, SWT.BORDER);
        
        GridData gd_searchText = new GridData(SWT.FILL, SWT.FILL, true, true,1, 1);
        m_addText.setLayoutData(gd_searchText);
        
        createAddButton(composite);
        
        m_mailButton = new Button(composite, SWT.NONE);
        Image addressButtonImage = new Image(composite.getDisplay(),
                getRegistry().getImage("globalAddressButton.IMAGE"), SWT.NONE);
        m_mailButton.setImage(addressButtonImage);
        m_mailButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent event)
            {
                try
                {
                    String viewerClass = getRegistry().getString("Addressbook.CLASS");
                    Object[] params = new Object[1];
                    params[0] = AIFDesktop.getActiveDesktop().getShell();
                    
                    IAddressBook addressBookDialog = (IAddressBook) Instancer.newInstanceEx(viewerClass, params);
                    
                    addressBookDialog.setButtonsForDialog(SimpleAddressBookDialog.OK_BUTTON_ID
                            | SimpleAddressBookDialog.APPLY_BUTTON_ID | SimpleAddressBookDialog.CANCEL_BUTTON_ID);
                    
                    if (addressBookDialog instanceof IPropertyChangeNotifier)
                    {
                        ((IPropertyChangeNotifier) addressBookDialog).addPropertyChangeListener(ProductBulletinReviewNotificationPanel.this);
                    }
                    
                    addressBookDialog.open();
                    
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }
        });
        
        m_mailList = new List(composite, SWT.BORDER);
        GridData gd_mailList = new GridData(SWT.FILL, SWT.FILL, true, false, 1,
                15);
        m_mailList.setLayoutData(gd_mailList);
        
        createRemoveButton(composite);
    }
    
    private void createRemoveButton(Composite composite)
    {
        m_minusButton = new Button(composite, SWT.NONE);
        
        Image minusButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        
        m_minusButton.setImage(minusButtonImage);
        
        GridData gd_minusButton = new GridData(SWT.FILL, SWT.TOP, false, false,
                1, 1);
        m_minusButton.setLayoutData(gd_minusButton);
        m_minusButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                String[] selections = m_mailList.getSelection();
                for (String eachSelection : selections)
                {
                    m_mailList.remove(eachSelection);
                }
            }
        });
    }
    
    private void createAddButton(Composite composite)
    {
        m_addButton = new Button(composite, SWT.NONE);
        Image plusButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        m_addButton.setImage(plusButtonImage);
        m_addButton.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                addToSourceTable(m_addText.getText());
            }
        });
    }
    
    private Label createLabel(Composite parent, String text)
    {
        Label label = new Label(parent, SWT.NONE);
        label.setText(getRegistry().getString(text));
        return label;
    }
    
    private Composite getMainComposite()
    {
        Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    public void addToSourceTable(String strValue)
    {
        if(strValue == null || strValue.equalsIgnoreCase(""))
        {
            return;
        }
        
        if ((m_mailList.indexOf(strValue) != -1) )// check exists
                                                          // condition
        {
            String errorStr = strValue + " "
                    + getRegistry().getString("AlreadyObjects.MSG");
            MessageBox.post(errorStr, getRegistry().getString("Info.MSG"),
                    MessageBox.INFORMATION);
            return;
        }
        m_mailList.add(strValue);
    }
    
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public void update(PublishEvent event)
    {
        
        SetEnabledRunnable runnable = new SetEnabledRunnable(event, this);
        Display.getDefault().syncExec(runnable);
        
    }

    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        if(event.getPropertyName().equalsIgnoreCase(SimpleAddressBookDialog.ADDRESS_BOOK_MAIL_IDS_SELECTED))
        {
            String[] selectedIds = (String[]) event.getNewValue();
            setSelectedIdToList(selectedIds);
        }
        
    }
    
    private void setSelectedIdToList(String[] selectedNames)
    {
        if (selectedNames != null && selectedNames.length > 0)
        {
            addToSourceTable(selectedNames[0]);
        }
    }
}
