package com.nov.rac.form.impl.ecoform;



import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECOReasonForChangePanel extends AbstractUIPanel implements
INOVFormLoadSave
{
    protected Text m_reasonForChangeText =null;
    protected Registry m_registry =null;
    private Composite m_mainComposite;
    private Group m_mainGroup;
     
    public ECOReasonForChangePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }

    protected Registry getRegistry()
    {
        return m_registry;
    }

    private void createReasonForChangeGroupContainer(Composite composite)
    
    {
        m_mainGroup = new  Group (composite,SWT.NONE);
        GridData gd_mainGroup = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        m_mainGroup.setLayoutData(gd_mainGroup);
        m_mainGroup.setLayout(new GridLayout(1, true));
        m_mainGroup.setText(getRegistry().getString("ECOReasonForChange.ReasonForChangeGroupTitle"));
        SWTUIHelper.setFont(m_mainGroup, SWT.BOLD);
    }

    private void createReasonForChangeText(Composite composite)
    {
        m_reasonForChangeText = new Text(composite, SWT.BORDER);
        GridData gd_reasonForChangeText =new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_reasonForChangeText.heightHint = ((m_reasonForChangeText
                .getFont().getFontData())[0].getHeight()) * 10;
        m_reasonForChangeText.setLayoutData(gd_reasonForChangeText);
        m_reasonForChangeText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean createUI() throws TCException
    {
        m_mainComposite = getComposite();
        GridData gd_mainComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        m_mainComposite.setLayoutData(gd_mainComposite);
        m_mainComposite.setLayout(new GridLayout(1, true));
        
        createReasonForChangeGroupContainer(m_mainComposite);
        createReasonForChangeText(m_mainGroup);
       
        return true;
    }

    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
}
