package com.nov.rac.form.impl.supersedeform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.Signatures;
import com.teamcenter.rac.util.Registry;

public class SupersedeSignaturesPanel extends Signatures
{

    public SupersedeSignaturesPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
