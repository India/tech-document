package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class AdapterSummaryPageDocDetails extends AbstractUIPanel implements
        INOVFormLoadSave, ISubscriber
{
    
    private ContainerTopBottomDocsPanel m_propsPanel;
    private EditButtonPanel m_editButtonPanel;
    
    
    public AdapterSummaryPageDocDetails(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_propsPanel.load(propMap);
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_propsPanel.save(propMap);
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_propsPanel.setEnabled(flag);
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        // Label label = new Label(composite,SWT.BORDER);
        // label.setText("asdfasdf");
        m_editButtonPanel = new EditButtonPanel(composite, SWT.NONE);
        m_editButtonPanel.setDocRevSelectionChangeEventName(GlobalConstant.EVENT_REVISION_SELECTION_CHANGED);
        m_editButtonPanel.createUI();
        m_propsPanel = new ContainerTopBottomDocsPanel(composite, SWT.NONE);
        m_propsPanel
                .setDocRevSelectionChangeEventName(GlobalConstant.EVENT_REVISION_SELECTION_CHANGED);
        m_propsPanel.createUI();
        registerSubscriber();
        return true;
    }
    
    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false,
                1, 1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        SetEnabledRunnable runnable = new SetEnabledRunnable(event, this);
        Display.getDefault().syncExec(runnable);
    }
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
}
