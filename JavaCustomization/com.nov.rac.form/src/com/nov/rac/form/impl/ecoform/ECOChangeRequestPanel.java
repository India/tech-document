package com.nov.rac.form.impl.ecoform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECOChangeRequestPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Composite m_subComposite;
    private Composite m_changeRequestcomposite;
    private Registry m_registry;
    private Text m_changeRequestText;
    private Group m_changeRequestGroup;
    private Text m_reasonForChangeText;
    
    public ECOChangeRequestPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        // TODO Auto-generated constructor stub
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getComposite();
        GridData gd_mainComposite = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1);
        
        mainComposite.setLayoutData(gd_mainComposite);
        mainComposite.setLayout(new GridLayout(1, true));
        
        createsubComposite(mainComposite);
        
        createChangeRequestComposite(m_subComposite);
        createchangeRequestLabel(m_changeRequestcomposite);
        createEditButton(m_changeRequestcomposite);
        createChangeRequestText(m_changeRequestcomposite);
        createReasonForChangeFromECRGroup(m_subComposite);
        createReasonForChangeFromECRText(m_changeRequestGroup);
        
        return true;
    }
    
    private void createsubComposite(Composite composite)
    {
        m_subComposite = new Composite(composite, SWT.BORDER);
        GridData gd_subComposite = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1);
        m_subComposite.setLayoutData(gd_subComposite);
        GridLayout gl_subComposite = new GridLayout(3, true);
        gl_subComposite.marginTop = 0;
        gl_subComposite.verticalSpacing = 0;
        m_subComposite.setLayout(gl_subComposite);
        
    }
    
    protected void createReasonForChangeFromECRText(Composite composite)
    {
        m_reasonForChangeText = new Text(composite, SWT.BORDER);
        m_reasonForChangeText.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
                true, true, 1, 1));
        m_reasonForChangeText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createReasonForChangeFromECRGroup(Composite composite)
    {
        m_changeRequestGroup = new Group(composite, SWT.NONE);
        m_changeRequestGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
                true, true, 2, 1));
        m_changeRequestGroup.setLayout(new GridLayout(1, true));
        m_changeRequestGroup.setText(getRegistry().getString(
                "ECOChangeRequestPanel.ReasonForChangeFromECRGroupTitle"));
        SWTUIHelper.setFont(m_changeRequestGroup, SWT.BOLD);
    }
    
    private void createchangeRequestLabel(Composite composite)
    {
        Label changeRequestLabel = new Label(composite, SWT.NONE);
        changeRequestLabel.setText(getRegistry().getString(
                "ECOChangeRequestPanel.ChangeRequestLabel"));
        changeRequestLabel.pack();
        SWTUIHelper.setFont(changeRequestLabel, SWT.BOLD);
    }
    
    private void createChangeRequestComposite(Composite composite)
    {
        m_changeRequestcomposite = new Composite(composite, SWT.BORDER);
        m_changeRequestcomposite.setLayoutData(new GridData(SWT.FILL,
                SWT.CENTER, true, false, 1, 1));
        m_changeRequestcomposite.setLayout(new GridLayout(3, false));
        
    }
    
    private void createChangeRequestText(Composite composite)
    {
        m_changeRequestText = new Text(composite, SWT.BORDER);
        GridData gd_ChangeRequestText = new GridData(SWT.FILL, SWT.FILL, true,
                false, 3, 1);
        gd_ChangeRequestText.heightHint = ((m_changeRequestcomposite.getFont()
                .getFontData())[0].getHeight()) * 10; // review this line //
                                                      // argument = text field
                                                      // should not be given
                                                      // vertical
                                                      // grids.instead hint
                                                      // height should be
                                                      // specified. here we have
                                                      // specified the
                                                      // hint height in terms of
                                                      // the 10 lines.
        m_changeRequestText.setLayoutData(gd_ChangeRequestText);
        m_changeRequestText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createEditButton(Composite composite)
    {
        Button editButton = new Button(composite, SWT.NONE); // add image to
                                                             // this button
        Image editImage = new Image(composite.getDisplay(), getRegistry()
                .getImage("editButton.IMAGE"), SWT.NONE);
        editButton.setImage(editImage);
        editButton.setText(getRegistry().getString(
                "ECOChangeRequestPanel.EditButton"));
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
