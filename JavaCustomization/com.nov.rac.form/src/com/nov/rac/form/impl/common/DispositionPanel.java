package com.nov.rac.form.impl.common;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

//import com.nov.NationalOilwell;

public class DispositionPanel extends AbstractExpandableUIPanel implements
INOVFormLoadSave, ISubscriber, IPublisher
{
    private static final int NO_OF_COLUMNS_MAINCOMPOSITE = 3;
    private static final int VERTICAL_GRIDS = 3;
    private static final int NO_OF_LINES_TEXTBOX = 4;
    private Text m_dispositionText = null;
    private Button m_copyDispostionButton = null;
    
    private Registry m_registry = null;
    private Combo m_inProcessCombo;
    private Combo m_inInventoryCombo;
    private Combo m_assembledCombo;
    private Combo m_inFieldCombo;
  
    public DispositionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    } 
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(NO_OF_COLUMNS_MAINCOMPOSITE, false));
        
        getExpandableComposite().setText(getRegistry().getString("ObsoleteFormDispositionPanel.DispositionLabel"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        createInProcessAndInInventoryFields(l_composite);
        
        createAssembledAndInFieldFields(l_composite);
        
        createDispositionInstruction(l_composite);
        
        registerSubscriber();
        createDataModelMapping();
        return true;
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_inProcessCombo,
                "inprocess", FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_inFieldCombo,
                "infield", FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_assembledCombo,
                "assembled", FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_inInventoryCombo,
                "ininventory", FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_dispositionText,
                "dispinstruction", FormHelper.getDataModelName());
       
    }
    private void createDispositionInstruction(Composite parent)
    {
        Composite composite =createComposite(parent, true);
        createDispositionInstructionLabel(composite);
        createCopyDispostionButton(composite);
        createDispostionText(composite);
         
    }

    private void createAssembledAndInFieldFields(Composite parent) throws TCException
    {
        Composite composite =createComposite(parent,false);
       
        createLabel(composite,"ObsoleteFormDispositionPanel.AssembledLabel");
        m_assembledCombo = createCombo(composite);
        createLabel(composite,"ObsoleteFormDispositionPanel.InFieldLabel");
        m_inFieldCombo = createCombo(composite);
        
    }

    private void createInProcessAndInInventoryFields(Composite parent) throws TCException
    {
       
        Composite composite =  createComposite(parent,false);
        
        createLabel(composite,"ObsoleteFormDispositionPanel.InProcessLabel");
        m_inProcessCombo = createCombo(composite);
        createLabel(composite,"ObsoleteFormDispositionPanel.InInventoryLabel"); 
        m_inInventoryCombo = createCombo(composite);
    }

    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.registerSubscriber(
                GlobalConstant.EVENT_TARGETS_TABLE_ROW_SELECTED, this);
    }
    
    private Composite createComposite(Composite parent,boolean horizontalGrab)
    {
       Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gl_composite = new GridLayout(2, false);
        composite.setLayout(gl_composite);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
                horizontalGrab, true, 1, VERTICAL_GRIDS));
        return composite;
        
    }
    
    private void createCopyDispostionButton(Composite Parent)
    {
        m_copyDispostionButton = new Button(Parent, SWT.PUSH);
        m_copyDispostionButton.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL,
                true, false, 1, 1));
        
        m_copyDispostionButton.setText(getRegistry().getString(
                "CopyAllButton.Label"));
        
        m_copyDispostionButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                copyAllWidgetSelected();
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }
    
    protected void copyAllWidgetSelected()
    {
        IPropertyMap propMap = new SimplePropertyMap();
        
        try
        {
            save(propMap);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        PublishEvent event = new PublishEvent(this,
                GlobalConstant.EVENT_COPY_ALL_DISPOSITION_PROPERTY, propMap, null);
        
        controller.publish(event);
        
    }
    
   
    
    
    private void publishDispositionDataChanged()
    {
        IPropertyMap propMap = new SimplePropertyMap();
        
        try
        {
            save(propMap);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        PublishEvent event = new PublishEvent(this,
                GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED, propMap, null);
        
        controller.publish(event);
        
    }
    
//    
   
    private void createDispostionText(Composite parent)
    {
        m_dispositionText = new Text(parent, SWT.BORDER | SWT.V_SCROLL
                | SWT.H_SCROLL | SWT.MULTI | SWT.WRAP);
        GridData gd_text = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 2);
     
        int firstFont = 0;
        gd_text.heightHint = ((m_dispositionText.getFont()
                .getFontData())[firstFont ].getHeight()) * NO_OF_LINES_TEXTBOX;
        
        m_dispositionText.setLayoutData(gd_text);
        m_dispositionText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        m_dispositionText.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent arg0)
            {
                publishDispositionDataChanged();
            }
            
            @Override
            public void focusGained(FocusEvent arg0)
            {
                
            }
        });
        
    }
    
 
    private void createLabel(Composite composite,String getStringFromRegistry)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setText(getRegistry().getString(
                getStringFromRegistry));
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1));
    }
    
    private void createDispositionInstructionLabel(Composite parent)
    {
        Label dispositionInstructionLabel = new Label(parent, SWT.NONE);
        dispositionInstructionLabel.setLayoutData(new GridData(SWT.LEFT,
                SWT.CENTER, true, false, 1, 1));
        dispositionInstructionLabel.setText(getRegistry().getString(
                "ObsoleteFormDispositionPanel.DispositionInstructionLabel"));
        
    }
    
    public String[] getLOV() throws TCException
    {
        
        String[] retObjs = null;
        try
        {
            String enOwningGroup = FormHelper.getTargetComponentOwningGroup();
            String lOVName = null;
            if (enOwningGroup.contains(m_registry.getString("PCG.Group")))
            {
                lOVName = m_registry
                        .getString("ObsoleteFormDispositionPanel.Disposition_PCGLOV");
            }
            else
            {
                lOVName = m_registry
                        .getString("ObsoleteFormDispositionPanel.DispositionLOV");
            }
            TCSession tcSession = (TCSession) AIFUtility
                    .getCurrentApplication().getSession();
            
            //deprecated method + server call.. ask what should be done
            TCComponentListOfValues lOV  = TCComponentListOfValuesType.findLOVByName(tcSession, lOVName);
            
            Object[] lOVValues = lOV.getListOfValues().getListOfValues();
            
             retObjs = Arrays.copyOf(lOVValues, lOVValues.length, String[].class);
            
        }
        //ask about this exception.
        catch (TCException e)
        {
            throw e;
        }
        return retObjs;
    }
    
    public Combo createCombo(Composite parent) throws TCException
    {
        Combo combo = new Combo(parent,SWT.DROP_DOWN|SWT.READ_ONLY);
        combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
                1, 1));
        combo.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                publishDispositionDataChanged();                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        return combo;
        
    }
    
    @Override
    public boolean load(final IPropertyMap propMap) throws TCException
    {
        String[] lOVValues = getLOV();
        m_inProcessCombo.setItems(lOVValues);
        m_inFieldCombo.setItems(lOVValues);
        m_assembledCombo.setItems(lOVValues);
        m_inInventoryCombo.setItems(lOVValues);
        
        String inprocess = PropertyMapHelper.handleNull(propMap
                .getString("inprocess"));
        m_inProcessCombo.setText(inprocess);
        
        String infield = PropertyMapHelper.handleNull(propMap
                .getString("infield"));
        m_inFieldCombo.setText(infield);
        
        String assembled = PropertyMapHelper.handleNull(propMap
                .getString("assembled"));
        m_assembledCombo.setText(assembled);
        
        String ininventory = PropertyMapHelper.handleNull(propMap
                .getString("ininventory"));
        m_inInventoryCombo.setText(ininventory);
        
        String dispinstruction = PropertyMapHelper.handleNull(propMap
                .getString("dispinstruction"));
        m_dispositionText.setText(dispinstruction);
        
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString("inprocess", m_inProcessCombo.getText());
        propMap.setString("infield", m_inFieldCombo.getText());
        propMap.setString("assembled", m_assembledCombo.getText());
        propMap.setString("ininventory", m_inInventoryCombo.getText());
        propMap.setString("dispinstruction", m_dispositionText.getText());
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_dispositionText.setEnabled(flag);
        m_inProcessCombo.setEnabled(flag);
        m_inInventoryCombo.setEnabled(flag);
        m_assembledCombo.setEnabled(flag);
        m_inFieldCombo.setEnabled(flag);
        m_copyDispostionButton.setEnabled(flag);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equalsIgnoreCase(
                GlobalConstant.EVENT_TARGETS_TABLE_ROW_SELECTED))
        {
            IPropertyMap propMap = (IPropertyMap) event.getNewValue();
            
            populateDisopsitionData(propMap);
        }
    }
    
    private void populateDisopsitionData(final IPropertyMap propMap)
    {
        Display.getDefault().asyncExec(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    load(propMap);
                    
                    //disable panel
                    String hasDispositionStr = m_registry.getString("Targets.HasDisposition");
                    boolean bHasDispo = propMap.getLogical(hasDispositionStr);
                    setEnabled( bHasDispo );
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    @Override
    public void dispose()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_TARGETS_TABLE_ROW_SELECTED, this);
        
        super.dispose();
    }
    
}
