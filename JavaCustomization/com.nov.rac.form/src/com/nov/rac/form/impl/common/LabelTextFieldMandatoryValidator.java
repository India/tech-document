package com.nov.rac.form.impl.common;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.validator.IFieldValidator;

public class LabelTextFieldMandatoryValidator implements IFieldValidator
{
    
    private boolean isValidValue = false;
    private final Text textField;
    private Label label;
    
    public LabelTextFieldMandatoryValidator(Text text, Label lbl)
    {
        this.textField = text;
        this.label = lbl;
        addListener();
    }
    
    private void addListener()
    {
        textField.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                if (textField.getText().isEmpty())
                {
                    setValid(false);
                }
                else
                {
                    setValid(true);
                }
            }
        });
    }
    
    public void setValid(boolean decision)
    {
        isValidValue = decision;
        moveLabel();
    }
    
    @Override
    public String getErrorMessage()
    {
        return null;
    }
    
    @Override
    public String getWarningMessage()
    {
        return null;
    }
    
    @Override
    public boolean isValid(Object arg0)
    {
        return isValidValue;
    }
    
    @Override
    public boolean warningExist(Object arg0)
    {
        return false;
    }
    
    protected void moveLabel()
    {
        Rectangle rect = label.getBounds();
        label.setLocation(rect.x + 1, rect.y);
        label.setLocation(rect.x, rect.y);
    }
    
}
