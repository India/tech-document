package com.nov.rac.form.impl.rsecrform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ApprovalGroupPanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    private Registry m_registry = null;
    private Text m_approvalGroupText = null;
    private Text m_engApproverText = null;
    private Text m_engApproveDateText = null;
    private Text m_additionalApproverText = null;
    private Text m_additionalApprDateText = null;
    
    public ApprovalGroupPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite mainComposite = getMainComposite();
        getExpandableComposite().setText(
                m_registry.getString("approvalGroup.LABEL"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        createApprovalGroup(mainComposite);
        createApprover(mainComposite);
        return true;
    }
    
    private void createApprovalGroup(Composite parent)
    {
        Composite groupCmp = new Composite(parent, SWT.NONE);
        GridLayout gl_group = new GridLayout(5, true);
        gl_group.marginHeight = 0;
        gl_group.marginWidth = 0;
        groupCmp.setLayout(gl_group);
        groupCmp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1));        
        m_approvalGroupText = createText(groupCmp);
        m_approvalGroupText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
                2, 1));
        
    }
    
    private void createApprover(Composite parent)
    {
        Composite approverCmp = new Composite(parent, SWT.NONE);
        approverCmp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1));
        GridLayout gl_approver = new GridLayout(2, true);
        gl_approver.marginHeight = 0;
        gl_approver.marginWidth = 0;
        gl_approver.horizontalSpacing = SWTUIHelper.convertHorizontalDLUsToPixels(approverCmp, 10);
        approverCmp.setLayout(gl_approver);
        
        createLeftComposite(approverCmp);
        createRightComposite(approverCmp);        
    }
    
   
    private Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    private void createLeftComposite(Composite parent)
    {
        
        Composite l_composite = createComposite(parent);        
        createLabel(l_composite, "engApprover.LABEL");
        m_engApproverText = createText(l_composite);
        createLabel(l_composite, "engApproveDate.LABEL");
        m_engApproveDateText = createText(l_composite);
        
    }
    
    private void createRightComposite(Composite parent)
    {
        Composite r_composite = createComposite(parent); 
        createLabel(r_composite, "additionalApprover.LABEL");
        m_additionalApproverText = createText(r_composite);
        createLabel(r_composite, "additionalApproveDate.LABEL");
        m_additionalApprDateText = createText(r_composite);         
    }
    
    private Composite createComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
        
    }    
   
    private Label createLabel(Composite composite, String propKey)
    {
        Label tempLabel = new Label(composite, SWT.NONE);
        tempLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true,
                1, 1));
        tempLabel.setText(m_registry.getString(propKey));
        SWTUIHelper.setFont(tempLabel, SWT.BOLD);
        
        return tempLabel;
    }
    
    private Text createText(Composite composite)
    {
        Text tempText = new Text(composite, SWT.BORDER);
        tempText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
                1, 1));
        tempText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        tempText.setEnabled(false);
        return tempText;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        
        m_approvalGroupText.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_approval_group")));
        
        m_engApproverText.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_eng_approver")));
        
        m_additionalApproverText.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_additional_approver")));
        
        setDate(m_engApproveDateText, "nov4_eng_approve_date", propMap);
        setDate(m_additionalApprDateText, "nov4_addtional_approve_date", propMap);
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    private void setDate(Text dateText, String propertyName,
            IPropertyMap propMap)
    {
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate == null)
        {
            dateText.setText(getRegistry().getString("No_Date_Set.Label"));
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateText.setText(dateFormat.format(theDate));
        }
    }
}
