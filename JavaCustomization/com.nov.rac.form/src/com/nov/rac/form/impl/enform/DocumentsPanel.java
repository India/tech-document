package com.nov.rac.form.impl.enform;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.common.DocumentTable;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.impl.TCTableComparator;
import com.nov.rac.item.itemsearch.ISearchPropertiesProvider;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.NOVGRMRelationsHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class DocumentsPanel extends AbstractUIPanel implements
        INOVFormLoadSave, IPublisher
{
    private Registry m_registry = null;
    private Group m_documentsGroupContainer = null;
    private TCTable m_documentsTable = null;
    private String m_relationName = "";
    private TCComponent[] m_docs = null;
    private static int TABLE_VERTICAL_SPAN = 1;
    private TCTableComparator m_comparator = null;
    private Map<String,TCComponent> m_idComponentMap = new HashMap<String,TCComponent>();
    protected SearchButtonComponent m_addButton = null;
    protected Button m_removeButton = null;
    private String m_searchContext = "DOC";
    private int m_selection = ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
    private Composite m_buttonComposite = null;
    
    public DocumentsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        initializeComparator();
    }
    
    private void initializeComparator()
    {
        if(GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            m_comparator = new TCTableComparator();
        }
    }

    protected TCTableComparator getComparator()
    {
        return m_comparator;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    protected Group getGroupContainer()
    {
        return m_documentsGroupContainer;
    }
    
    protected TCTable getTable()
    {
        return m_documentsTable;
    }
    
    protected void setRelation(String relationName)
    {
        m_relationName = relationName;
    }
    
    protected String getRelation()
    {
        return m_relationName;
    }
    
    protected TCComponent[] getDocuments()
    {
        return m_docs;
    }
    
    protected void setDocuments(TCComponent[] docs)
    {
        m_docs = docs;
    }
    
    protected void setTableVerticalSpan(int verticalSpan)
    {
        TABLE_VERTICAL_SPAN = verticalSpan;
    }
    
    protected int getTableVerticalSpan()
    {
        return TABLE_VERTICAL_SPAN;
    }
    
    protected Map<String,TCComponent> getIdComponentMap()
    {
        return m_idComponentMap;
    }
    
    protected void setAddButtonText(String text)
    {
        m_addButton.setText(text);
    }
    
    protected void setRemoveButtonText(String text)
    {
        m_removeButton.setText(text);
    }
    
    protected void setAddButtonEnabled(boolean enabled)
    {
        m_addButton.setEnabled(enabled);
    }
    
    protected void setRemoveButtonEnabled(boolean enabled)
    {
        m_removeButton.setEnabled(enabled);
    }
    
    protected void setAddButtonImage(Image image)
    {
        m_addButton.setImage(image);
    }
    
    protected void setRemoveButtonImage(Image image)
    {
        m_removeButton.setImage(image);
    }
    
    protected void setRemoveButtonListener(SelectionListener selectionListener)
    {
        m_removeButton.addSelectionListener(selectionListener);
    }
    
    protected void setAddButtonSelection(int selection)
    {
        m_selection = selection;
    }
    
    protected Composite getButtonComposite()
    {
        return m_buttonComposite;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        // TODO Auto-generated method stub
        final Composite composite = getComposite();
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        
        createDocumentsGroupContainer(composite);
        createButtons();
        createDocumentsTable();
        return true;
    }
    
    protected void createButtons()
    {
        createButtonsComposite();
        
        createRemoveButton(m_buttonComposite);
        
        createAddButton(m_buttonComposite);
    }

    protected void createButtonsComposite()
    {
        m_buttonComposite = new Composite(m_documentsGroupContainer,
                SWT.NONE);
        GridLayout layout = new GridLayout(2, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        m_buttonComposite.setLayout(layout);
        m_buttonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false,
                1, 1));
    }

    protected void createRemoveButton(Composite composite)
    {
        m_removeButton = new Button(composite,SWT.NONE);
        GridData gdRemoveBtn = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        m_removeButton.setLayoutData(gdRemoveBtn);
    }
    
    protected void createFeedBackButton(Composite composite)
    {
        new FeedBackButtonComposite(composite, SWT.NONE);
    }

    protected void createAddButton(Composite composite)
    {
        m_addButton = new SearchButtonComponent(composite, SWT.TOGGLE);
        GridData gdAddBtn = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        m_addButton.setLayoutData(gdAddBtn);
        m_addButton.setProperty(ISearchPropertiesProvider.ITEM_TYPES, new String[]{"Documents"});
        m_addButton.setProperty(ISearchPropertiesProvider.SEARCH_CONTEXT, m_searchContext);
        m_addButton.setProperty(ISearchPropertiesProvider.BUTTON_SOURCE,composite);
        m_addButton.setProperty(ISearchPropertiesProvider.SELECTION_MODE,m_selection);
    }

    private void createDocumentsTable()
    {
        Composite composite = new Composite(m_documentsGroupContainer,
                SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, TABLE_VERTICAL_SPAN);
        composite.setLayoutData(gridData);
        
        String[] columnNames = getRegistry().getStringArray(
                "DocumentTable.ColumnNames");
        
        String[] columnIds = getRegistry().getStringArray(
                "DocumentTable.ColumnIdentifiers");
        
        m_documentsTable = new DocumentTable(columnIds, columnNames,
                getRegistry());
        
        addRowSelectionListener();
        m_documentsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_documentsTable.setSortEnabled(false);
        m_documentsTable.setEditable(false);
        m_documentsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // setTableColumnsWidth(m_documentsTable);
        // hide columns
        hideColumns();
        
        JPanel docTablePanle = new JPanel();
        docTablePanle.setLayout(new BorderLayout());
        ScrollPagePane scrollPane = new ScrollPagePane(m_documentsTable);
        docTablePanle.add(scrollPane);
        
        SWTUIUtilities.embed(composite, docTablePanle, false);
    }
    
    protected void setTableColumnsWidth(TCTable documentsTable)
    {
        int tabelWidth = documentsTable.getSize().width;
        
        documentsTable.getColumn("Document ID").setPreferredWidth(
                (tabelWidth * 50) / 100);
        documentsTable.getColumn("CAT").setPreferredWidth(
                (tabelWidth * 15) / 100);
        documentsTable.getColumn("Type").setPreferredWidth(
                (tabelWidth * 15) / 100);
        documentsTable.getColumn("WO Included").setPreferredWidth(
                (tabelWidth * 10) / 100);
        documentsTable.getColumn("PO Included").setPreferredWidth(
                (tabelWidth * 10) / 100);
    }
    
    private void addRowSelectionListener()
    {
        m_documentsTable.getSelectionModel().addListSelectionListener(
                new ListSelectionListener()
                {
                    @Override
                    public void valueChanged(
                            ListSelectionEvent paramListSelectionEvent)
                    {
                        if(!paramListSelectionEvent.getValueIsAdjusting())
                        {
                            publishSelectedDocument();
                        }
                    }
                    
                });
    }
    
    protected void publishSelectedDocument()
    {
        int rowInx = getTable().getSelectedRow();
        TCComponent[] docs = getDocuments();
        
        if (docs != null && docs.length > 0 && rowInx > -1)
        {
            IController controller = ControllerFactory.getInstance()
                    .getDefaultController();
            
            PublishEvent event = new PublishEvent(getPublisher(),
                    GlobalConstant.DOCUMENT_PUBLISH_EVENT, docs[rowInx], null);
            controller.publish(event);
        }
    }
    
    private void hideColumns()
    {
        // hide columns
        String[] hideColumnNames = getRegistry().getStringArray(
                "DocumentTable.HIDE_COLUMNS");
        int colIndex = -1;
        for (String colName : hideColumnNames)
        {
            colIndex = m_documentsTable.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_documentsTable, colIndex);
        }
    }
    
    private void createDocumentsGroupContainer(Composite composite)
    {
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        m_documentsGroupContainer = new Group(composite, SWT.NONE);
        m_documentsGroupContainer.setLayoutData(gridData);
        m_documentsGroupContainer.setLayout(new GridLayout(1, true));
        SWTUIHelper.setFont(m_documentsGroupContainer, SWT.BOLD);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        String relationName = getRelation();
        //m_docs 
        TCComponent[] docs = propMap.getTagArray(relationName);
        //setDocuments(docs);
        TCComponent primaryObject = propMap.getComponent();
        
        loadTable(primaryObject,docs,relationName);
        
        return true;
    }

    private void loadTable(TCComponent primaryObject,TCComponent[] docs,
            String relationName) throws TCException
    {
        setDocuments(docs);
        getTable().clear();
        if (docs != null && docs.length > 0)
        {
            TCComponent[] primaryObjects = new TCComponent[] {primaryObject};
            List<TCComponent> relationObjects = getRelationObjects(
                    primaryObjects, relationName);
            
            IPropertyMap[] propertyMapArray = new SimplePropertyMap[docs.length];
            
            int index = 0;
            m_idComponentMap.clear();
            for (TCComponent doc : docs)
            {
                m_idComponentMap.put(doc.getProperty("item_id"), doc);
                IPropertyMap propertyMap = new SimplePropertyMap();
                TCComponent relationObject = relationObjects.get(index);
                
                propertyMap.setString("object_type", PropertyMapHelper
                        .handleNull(doc.getProperty("object_type")));
                propertyMap.setString("item_id", PropertyMapHelper
                        .handleNull(doc.getProperty("item_id")));
                propertyMap.setString("DocumentsCAT", PropertyMapHelper
                        .handleNull(doc.getProperty("DocumentsCAT")));
                propertyMap.setString("DocumentsType", PropertyMapHelper
                        .handleNull(doc.getProperty("DocumentsType")));
                
                List<TCComponent> relationObjectsList = new ArrayList<TCComponent>();
                relationObjectsList.add(relationObject);
                
                String[] propertyNameArray = { "nov4_include_in_WO",
                        "nov4_include_in_PO" };
                
                TCProperty[][] returedTCProperties = TCComponentType
                        .getTCPropertiesSet(relationObjectsList,
                                propertyNameArray);
                
                for (int inx = 0; inx < relationObjectsList.size(); inx++)
                {
                    for (int jnx = 0; jnx < propertyNameArray.length; jnx++)
                    {
                        Boolean propertyValue = (returedTCProperties[inx][jnx])
                                .getBoolValue();
                        propertyMap.setLogical(propertyNameArray[jnx],
                                propertyValue);
                    }
                }
                
                propertyMapArray[index] = propertyMap;
                index++;
            }
            TableUtils.populateTable(propertyMapArray, getTable());
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    protected List<TCComponent> getRelationObjects(
            TCComponent[] primaryObjects, String relationName)
    {
        Map<String, List<TCComponent>> relationObjectMap = null;
        List<TCComponent> tcComponentRelationObjects = new ArrayList<TCComponent>();
        try
        {
            NOVGRMRelationsHelper relationHelper = new NOVGRMRelationsHelper();
            relationHelper.expandGRMRelations(primaryObjects[0],
                    new String[] { relationName },
                    NOVGRMRelationsHelper.EXPAND_FOR_PRIMARY);
            relationObjectMap = relationHelper.getRelationObjectsMap();
            
            if (relationObjectMap != null)
            {
                tcComponentRelationObjects.addAll(relationObjectMap
                        .get(relationName));
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return tcComponentRelationObjects;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    protected IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    protected PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    /*function: createRelation(...)
     * Description: This method is invoked on plus and add/replace button exist in RD and RDD panel respectively.  
     * This method creates the relationship between primary object and secondary object based on relation name
     * before creating relation it checks whether secondary object already exist or not
     */
    protected void createRelation(TCComponent primaryObject,TCComponent[] secondaryObjects)
    {
        boolean isRelationCreated = FormHelper.createRelation(primaryObject, secondaryObjects, getRelation());
        if(isRelationCreated)
        {
            refreshObject(primaryObject);
        }
        /*try
        {
            TCComponent[] existingSecondaryObjects = primaryObject.getRelatedComponents(getRelation());
             List<TCComponent> existingSecondaryObjectList = Arrays.asList(existingSecondaryObjects);
             
            List<CreateRelationObjectHelper> creRelationObjectHelperList = new ArrayList<CreateRelationObjectHelper>();
            CreateRelationObjectHelper creRelationObjectHelper = null;
            
            for (int i = 0; i <secondaryObjects.length; i++)
            {
                if(!isSecondaryObjectExist(existingSecondaryObjectList,secondaryObjects[i]))
                {
                    creRelationObjectHelper = new CreateRelationObjectHelper(CreateRelationObjectHelper.OPERATION_CREATE_RELATION);
                    creRelationObjectHelper.setPrimaryObject(primaryObject);
                    creRelationObjectHelper.setSecondaryObject(secondaryObjects[i]);
                    creRelationObjectHelper.setRelationName(getRelation());
                    creRelationObjectHelperList.add(creRelationObjectHelper);
                }
            }
            
            if(creRelationObjectHelperList.size() > 0)
            {
                CreateRelationObjectHelper[] creRelationObjectHelpeArray = new CreateRelationObjectHelper[creRelationObjectHelperList.size()];
                CreateObjectsSOAHelper.createRelationObjects(creRelationObjectHelperList.toArray(creRelationObjectHelpeArray));
                refreshObject(primaryObject);
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }  */
    }
    
    /*function: deleteRelation(...)
     * Description: This function deletes the relation between primary object and secondary object.
     * It is call on minus and add/replace button exist in RD and RDD panel respectively.
     * After deleting the relation,primary object must be refreshed.
     * 
     
    protected void deleteRelation(TCComponent primaryObject,TCComponent[] secondaryObjects)
    {
        try
        {
            DeleteObjectHelper delRelationObjectHelper[] = new DeleteObjectHelper[secondaryObjects.length];
            for (int i = 0; i <secondaryObjects.length; i++)
            {
                delRelationObjectHelper[i] = new DeleteObjectHelper(
                        DeleteObjectHelper.OPERATION_DELETE_RELATION);
                delRelationObjectHelper[i].setPrimaryObject(primaryObject);
                delRelationObjectHelper[i].setSecondaryObject(secondaryObjects[i]);
                delRelationObjectHelper[i].setRelationName(getRelation());
            }
            
            CreateObjectsSOAHelper.deleteRelationObjects(delRelationObjectHelper);
            primaryObject.refresh();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }*/
    
   /* private boolean isSecondaryObjectExist(List<TCComponent> existingSecondaryObjectList,TCComponent tcComponent)
    {
        boolean isExist = false;
        if(existingSecondaryObjectList.contains(tcComponent))
        {
            isExist = true;
        }
        return isExist;
    }*/
    
    /*function: refreshObject(...)
     * Description: This function loads the data in RD/RDD table in RD/RDD panel after 
     * adding/deleting the record.
     */
    private void refreshObject(TCComponent primaryObject)
    {
        try
        {
            TCComponent[] docs = primaryObject.getRelatedComponents(getRelation());
            loadTable(primaryObject, docs, getRelation());
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    /*function: deleteObject(...)
     * Description:This function delete the row in the table based on item_id match
     */
    protected void deleteObject(TCComponent primaryObject,int row) throws TCException
    {
        if(row > -1)
        {
            int column = getTable().getColumnIndex("item_id");
            String documentId = (String) getTable().getValueAt(row, column);
            
            TCComponent[] docs = getDocuments();
            
            for(int inx=0; inx < docs.length; inx++)
            {
                String tempDocId = docs[inx].getProperty("item_id");
                if(tempDocId.equals(documentId))
                {
                    FormHelper.deleteRelation(primaryObject, new TCComponent[]{docs[inx]},getRelation());
                    getTable().removeRow(row);
                    break;
                }
            }
        }
        refreshObject(primaryObject);
    }
    
    public void registerSubscriber(String propName,ISubscriber subscriber)
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber(propName, subscriber);
    }
    
    public void unregisterSubscriber(String propName,ISubscriber subscriber)
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber(propName, subscriber);
    }
    
    protected boolean confirmRemoval()
    {
        boolean response = MessageDialog.openQuestion(m_removeButton.getShell(), getRegistry().getString("Confirm.Label"), getRegistry().getString("RemovalConfirm.Msg"));
        return response;
    }
    
    protected void validateTableRowSelection() throws TCException
    {
        if( getTable() != null && getTable().getSelectedRowCount() < 1)
        {
            String[] errors =  new String[]{ getRegistry().getString("NoItemSelected.MSG") };
            TCException exception = new TCException( errors );
            
            throw exception;
        }
    }
}
