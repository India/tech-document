package com.nov.rac.form.impl.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.HelpButtonComposite;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class FormHeaderPanel extends AbstractUIPanel
{
    
    private Registry m_registry = null;
    private IPropertyMap localPropMap = null;
    private Button printButton = null;
    
    public FormHeaderPanel(Composite parent, int style)
    {
        super(parent, style);
        
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite headerMainComposite = getComposite();
        GridData gd_FormHeader = new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1);
        headerMainComposite.setLayoutData(gd_FormHeader);
        GridLayout headerLayout = new GridLayout(1, true);
        headerLayout.marginWidth = 0;
        headerLayout.marginHeight = 0;
        
        headerMainComposite.setLayout(headerLayout);
        createHeader(headerMainComposite);
        addSeparator(headerMainComposite);
        
        return true;
    }
    
    private void createHeader(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        GridData gd_composite = new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1);
        composite.setLayoutData(gd_composite);
        createNameAndHelp(composite);
        // createHelpButton(composite);
        createButtons(composite);
    }
    
    private void createNameAndHelp(Composite parent)
    {
        Composite l_Composite = new Composite(parent, SWT.NONE);
        l_Composite.setLayout(new GridLayout(3, false));
        l_Composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
                false, 1, 1));
        Label formNameLabel = new Label(l_Composite, SWT.NONE);
        String formHeaderName = m_registry.getString(FormHelper
                .getTargetComponentType() + ".Form.Header.Name");
        formNameLabel.setText(formHeaderName);
        
        Label enFormObjectNamelbl = new Label(l_Composite, SWT.NONE);
        String formObjectName = "(" + FormHelper.getTargetComponent() + ")";
        enFormObjectNamelbl.setText(formObjectName);
        SWTUIHelper.setFont(enFormObjectNamelbl, SWT.BOLD);
        
        new HelpButtonComposite(l_Composite, SWT.NONE);
    }
    
    /*
     * private void createHelpButton(Composite parent) { new
     * HelpButtonComposite(parent, SWT.NONE); }
     */
    private void createButtons(Composite parent)
    {
        
        Composite buttonComposite = new Composite(parent, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(2, true));
        buttonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true,
                true, 1, 1));
        
        new FeedBackButtonComposite(buttonComposite, SWT.NONE);
        
        printButton = new Button(buttonComposite, SWT.NONE);
        printButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
        printButton.setText(getRegistry().getString("PRINT.Button"));
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private void addSeparator(Composite parent)
    {
        Label separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
        separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    }
    
}
