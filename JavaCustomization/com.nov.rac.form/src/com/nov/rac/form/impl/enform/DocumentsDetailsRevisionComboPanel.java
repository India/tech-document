package com.nov.rac.form.impl.enform;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.services.publishdataset.NovPublishDatasetHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;

public class DocumentsDetailsRevisionComboPanel extends AbstractUIPanel
        implements INOVFormLoadSave, IPublisher, ISubscriber
{
    
    private Combo m_combo;
    private Registry m_registry;
    private TCComponent m_selectedDocument;
    
    public DocumentsDetailsRevisionComboPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_combo.removeAll();
        
        m_selectedDocument = propMap.getComponent();
        // needs propMap of Document
        TCComponent[] allDocumentsRevs = m_selectedDocument.getReferenceListProperty("revision_list");
        
        List<TCComponent> revisionList = Arrays.asList(allDocumentsRevs);
        
        TCProperty[][] returedTCProperties = TCComponentType
                .getTCPropertiesSet(revisionList,
                        new String[] { "item_revision_id" });
        
        for (int index = 0; index < revisionList.size(); index++)
        {
            
            String revID = (returedTCProperties[index][0]).getStringValue();
            m_combo.add(revID);
            m_combo.setData(revID, revisionList.get(index));
        }
        // 1 based and 0 based index, hence -1
        int initialSelection = m_combo.getItemCount() - 1;
        
        m_combo.select(initialSelection);
        m_combo.notifyListeners(SWT.Selection,null);
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        createCombo(composite);
        registerSubscriber();
        return false;
    }
    
    protected void createCombo(Composite composite)
    {
        Composite comboComposite = createComboComposite(composite, SWT.NONE);
        createLabelUI(comboComposite, SWT.NONE);
        createComboUI(comboComposite, SWT.READ_ONLY);
    }
    
    protected CLabel createLabelUI(Composite composite, int style)
    {
        CLabel label = new CLabel(composite, style);
        label.setText(getRegistry().getString("DocumentRevision.Label"));
        return label;
        
    }
    
    protected Combo createComboUI(Composite parent, int style)
    {
        m_combo = new Combo(parent, style);
        m_combo.setLayoutData(new GridData());
        createComboListeners(m_combo);
        return m_combo;
    }
    
    protected void createComboListeners(Combo combo)
    {
        if (combo == null)
        {
            return;
        }
        combo.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent e)
            {
                IPropertyMap selectedRevPropMap =null;
                try
                {
                    TCComponent docrev= (TCComponent) m_combo.getData(m_combo.getText());
                    selectedRevPropMap = loadComponent(docrev,"DOCUMENTDETAILS");
                    loadPublishDatasetInformation(m_selectedDocument, selectedRevPropMap);
                }
                catch (TCException e1)
                {
                    e1.printStackTrace();
                }
                PublishEvent event = new PublishEvent(
                        DocumentsDetailsRevisionComboPanel.this,
                        GlobalConstant.DOCS_REVISION_SELECTION_CHANGED,
                        selectedRevPropMap, null);
                getController().publish(event);
            }
        });
        
    }
    
    private void loadPublishDatasetInformation(TCComponent tCComponent, IPropertyMap propMap) throws TCException
    {
        Map<String, String> stringMap = new HashMap<String, String>();
        stringMap.put("application", "KM");
        NovPublishDatasetHelper.getPublishDatasetRelatedInfo(tCComponent, stringMap , propMap);
    }

    private void registerSubscriber()
    {
        getController().registerSubscriber(GlobalConstant.DOCUMENT_PUBLISH_EVENT,
                getSubscriber());
    }
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(GlobalConstant.DOCUMENT_PUBLISH_EVENT,
                getSubscriber());
    }
    
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }

    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    protected Composite createComboComposite(Composite parent, int style)
    {
        Composite composite = new Composite(parent, style);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    public String getText()
    {
        if (m_combo != null)
        {
            return m_combo.getText();
        }
        return "";
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        Display.getDefault().syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                if (!getComposite().isDisposed())
                {
                    Object parentPublisher = getParentSource(((AbstractUIPanel) event
                            .getSource()).getComposite());
                    Object parentSubscriber = getParentSource(getComposite());
                    if (event.getPropertyName().equalsIgnoreCase(
                            GlobalConstant.DOCUMENT_PUBLISH_EVENT)
                            && parentPublisher == parentSubscriber)
                    {
                        try
                        {
                            TCComponent document = (TCComponent) event.getNewValue();
                            IPropertyMap propMap = new SimplePropertyMap();
                            propMap.setComponent(document);
                            load(propMap);
                        }
                        catch (TCException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        
    }
    
    private CTabFolder getParentSource(Composite panel)
    {
        Object parent = null;
        if (panel.getParent() == null)
        {
            return null;
        }
        parent = panel.getParent();
        if (parent instanceof CTabFolder)
        {
            return (CTabFolder) parent;
        }
        else
        {
            return getParentSource((Composite) parent);
        }
        
    }
    
    public IPropertyMap loadComponent(TCComponent targetObject, String tabName)
            throws TCException
    {
        IPropertyMap propertyMap = new SimplePropertyMap();
        propertyMap.setComponent(targetObject);
        try
        {
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            String formType = FormHelper.getTargetComponentType();
            
            NOVObjectPolicy formPolicy = null;
            NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(
                    targetObject);
            
            StringBuilder contextType = new StringBuilder(formType);
            contextType.append(".").append(tabName);
            
            NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
                    contextType.toString());
            
            formPolicy = novObjectPolicyFactory.createObjectPolicy(theGroup,
                    getRegistry());
            
            StringBuilder context = new StringBuilder(formType);
            String PROPERTY_NAMES = "PROPERTY_NAMES";
            context.append(".").append(tabName).append(".")
                    .append(PROPERTY_NAMES);
            
            String[] m_propertyNames = RegistryUtils.getConfiguration(theGroup,
                    context.toString(), getRegistry());
            
            dmSOAHelper.getObjectProperties(targetObject, m_propertyNames,
                    formPolicy);
            
            PropertyMapHelper.componentToMap(targetObject, m_propertyNames,
                    propertyMap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return propertyMap;
    }
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
}
