package com.nov.rac.form.impl.common;

import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.impl.common.TargetsTableTopPanel;
import com.teamcenter.rac.kernel.TCException;

public class TargetsTableTopPanelWithoutAddRemove extends TargetsTableTopPanel
{

    public TargetsTableTopPanelWithoutAddRemove(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public boolean createUI() throws TCException
    { 
       super.createUI();
       return true;
    }
    protected void createAddButton(Composite composite)
    {
       
    }
	
	protected void createRemoveButton(Composite composite)
    {
        
    }
}
