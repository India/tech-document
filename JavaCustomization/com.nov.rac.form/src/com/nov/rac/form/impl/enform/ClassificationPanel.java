package com.nov.rac.form.impl.enform;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.renderer.TableCellColourRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.framework.tablecomparator.impl.ComparatorDataModel;
import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;
import com.nov.rac.framework.tablecomparator.impl.ItemComparator;
import com.nov.rac.framework.tablecomparator.impl.TablePainter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.services.ClassificationHelper.AttributePropertiesHelper;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class ClassificationPanel extends AbstractUIPanel implements
        INOVFormLoadSave, ITableComparatorSubscriber, ISubscriber, IPublisher
{
    private Combo m_unitsCombo;
    private TCTable m_classificationTCTable;
    private Vector<Vector<String>> m_metricTableRows = new Vector<Vector<String>>();
    private Vector<Vector<String>> m_nonMetricTableRows = new Vector<Vector<String>>();
    // private TCComponent m_componentLoaded = null;
    private Registry m_registry;
    // private int m_nameColumIndex;
    // private int m_valueColumnIndex;
    // private int m_uOMColumnIndex;
    private static final int NO_OF_VERTICALGRIDS_TCTABLECOMPOSITE = 25;
    private ITableComparator m_iTableComparator = null;
    private TCComponent m_tcComponent = null;
    private static final String CLASSIFICATION_CONTEXT = "CLASSIFICATION";
    private String[] m_tableColumnNames = null;
    // private Label m_helpIcon;
    // private Button m_feedbackBtn;
    private Button m_editBtn;
    private Label m_hierarchyLabel;
    private static String CLASSICATION_HIERARCHY = "ClassificationHierarchy";
    
    public ClassificationPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        m_tableColumnNames = getCompareColumns();
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_tcComponent = propMap.getComponent();
        loadTCTable(propMap);
        showSelectedColumns();
        
        IPropertyMap[] icoPropMaps = propMap.getCompoundArray("icoObjects");
        if (icoPropMaps != null)
        {
            String hierarchy = PropertyMapHelper.handleNull(icoPropMaps[0].getString("hierarchy"));
            String businessGroupName = GroupUtils.getBusinessGroupName();
            String hierarchyRootkey = businessGroupName +"."+ CLASSICATION_HIERARCHY; 
            String hierarchyRootValue = getRegistry().getString(hierarchyRootkey);
            int indexOfRoot = hierarchy.indexOf(hierarchyRootValue);
            
            if(indexOfRoot >= 0)
            {
                String substring = hierarchy.substring(indexOfRoot);
                m_hierarchyLabel.setText(substring);
            }
        }
        
        if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            initializeComparision();
        }
        return true;
    }
    
    private void showSelectedColumns()
    {
        int nonMetricValuesIndex = m_classificationTCTable
                .getColumnIndex(AttributePropertiesHelper.PROP_NON_METRIC_VALUES);
        int nonMetricUOMIndex = m_classificationTCTable
                .getColumnIndex(AttributePropertiesHelper.PROP_NON_METRIC_UNIT);
        int metricValuesIndex = m_classificationTCTable
                .getColumnIndex(AttributePropertiesHelper.PROP_METRIC_VALUES);
        int metricUOMIndex = m_classificationTCTable
                .getColumnIndex(AttributePropertiesHelper.PROP_METRIC_UNIT);
        
        int nameIndex = m_classificationTCTable
                .getColumnIndex(AttributePropertiesHelper.PROP_NAME);
        
        if (m_unitsCombo.getText().equalsIgnoreCase("Metric"))
        {
            TableUtils.hideTableColumn(m_classificationTCTable,
                    nonMetricValuesIndex);
            TableUtils.hideTableColumn(m_classificationTCTable,
                    nonMetricUOMIndex);
            showColumn(m_classificationTCTable, metricValuesIndex);
            showColumn(m_classificationTCTable, metricUOMIndex);
            showColumn(m_classificationTCTable, nameIndex);
            
        }
        else
        {
            //
            TableUtils.hideTableColumn(m_classificationTCTable,
                    metricValuesIndex);
            TableUtils.hideTableColumn(m_classificationTCTable, metricUOMIndex);
            showColumn(m_classificationTCTable, nonMetricValuesIndex);
            showColumn(m_classificationTCTable, nonMetricUOMIndex);
            showColumn(m_classificationTCTable, nameIndex);
        }
        
    }
    
    private void showColumn(TCTable table, int colIndextable)
    {
        
        TableColumn column = table.getColumnModel().getColumn(colIndextable);
        
        int width = table.getWidth();
        column.setMinWidth(0);
        column.setMaxWidth(width);
        column.setWidth(width);
        column.setPreferredWidth(width);
        table.doLayout();
        
    }
    
    private void loadTCTable(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] icoPropMaps = propMap.getCompoundArray("icoObjects");
        if (icoPropMaps == null)
        {
            return;
        }
        
        for (IPropertyMap icoPropMap : icoPropMaps)
        {
            IPropertyMap[] attrPropMaps = icoPropMap
                    .getCompoundArray("attributes");
            if (attrPropMaps == null)
            {
                return;
            }
            
            TableUtils.populateTable(attrPropMaps, m_classificationTCTable);
        }
    }
    
    private String[] getCompareColumns()
    {
        String[] tableColumnNames = getRegistry().getStringArray(
                "ClassificationTable.CompareColumns");
        return tableColumnNames;
    }
    
    private void initializeComparision()
    {
        IDataModel model = new ComparatorDataModel();
        model.setPrimaryKeyColumn("name");
        model.setTableColumns(m_tableColumnNames);
        model.setTable(m_classificationTCTable);
        model.setComparator(new ItemComparator());
        model.setCompareObject(m_tcComponent);
        model.setSubscriber(this);
        
        m_iTableComparator = DefaultTableComparator.getController();
        m_iTableComparator.setContext(CLASSIFICATION_CONTEXT, model);
        invokeComparision();
    }
    
    private void invokeComparision()
    {
        m_iTableComparator.compareTable(CLASSIFICATION_CONTEXT);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridLayout gl_composite = new GridLayout(1, false);
        l_composite.setLayout(gl_composite);
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        l_composite.setLayoutData(gridData);
        
        createLabelButtonComposite(l_composite);
        createHierarchyLable(l_composite);
        createClassificationTable(l_composite);
        
        addListenerToEditButton();
        registerSubscriber();
        return true;
    }
    
    private void createLabelButtonComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(5, false));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gridData);
        
        createClassificationLabel(composite);
        // createHelpIcon(composite);
        createUnitsLabel(composite);
        createUnitsCombo(composite);
        createFeedbackButton(composite);
        createEditButton(composite);
    }
    
    private void createEditButton(Composite composite)
    {
        m_editBtn = new Button(composite, SWT.NONE);
        m_editBtn.setText(getRegistry().getString("EditButton.Lable"));
        GridData gridData5 = new GridData(SWT.RIGHT, SWT.FILL, false, false, 1,
                1);
        m_editBtn.setLayoutData(gridData5);
    }
    
    private void addListenerToEditButton()
    {
        m_editBtn.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                IPerspectiveDef persDef = PerspectiveDefHelper
                        .getPerspectiveDef("com.teamcenter.rac.classification.icm.ClassificationPerspective");
                InterfaceAIFComponent[] component = new InterfaceAIFComponent[] { m_tcComponent };
                persDef.openPerspective(component);
                                
                if( !Display.getCurrent().getActiveShell().equals(getComposite().getShell()))
                {
                    getComposite().getShell().dispose();
                }

            }
        });
    }
    
    private void createFeedbackButton(Composite composite)
    {
        /*
         * m_feedbackBtn = new Button(composite, SWT.NONE);
         * m_feedbackBtn.setText
         * (getRegistry().getString("FeedbackButton.Label")); GridData gridData4
         * = new GridData(SWT.RIGHT, SWT.FILL, false, false, 1, 1);
         * m_feedbackBtn.setLayoutData(gridData4);
         */
        new FeedBackButtonComposite(composite, SWT.NONE);
    }
    
    private void createUnitsLabel(Composite composite)
    {
        Label unitsLabel = new Label(composite, SWT.NONE);
        unitsLabel.setText(getRegistry().getString("Units.Label"));
        
        GridData gridData3 = new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1,
                1);
        unitsLabel.setLayoutData(gridData3);
    }
    
    /*
     * private void createHelpIcon(Composite composite) { m_helpIcon = new
     * Label(composite, SWT.NONE); GridData gridData2 = new GridData(SWT.LEFT,
     * SWT.TOP, false, false, 1, 1); m_helpIcon.setLayoutData(gridData2); Image
     * helpImage = new Image(composite.getDisplay(),
     * m_registry.getImage("Help.IMAGE"), SWT.NONE);
     * m_helpIcon.setImage(helpImage); }
     */
    
    private void createClassificationLabel(Composite composite)
    {
        CLabel classification = new CLabel(composite, SWT.NONE);
        classification.setText(getRegistry().getString("CLASSIFICATION.Label"));
        classification.setImage(TCTypeRenderer.getTypeImage("icm0", null));
        GridData gridData1 = new GridData(SWT.LEFT, SWT.FILL, false, false, 1,
                1);
        classification.setLayoutData(gridData1);
        SWTUIHelper.setFont(classification, SWT.BOLD);
    }
    
    private void createHierarchyLable(Composite parent)
    {
        m_hierarchyLabel = new Label(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        m_hierarchyLabel.setLayoutData(gridData);
    }
    
    private void createClassificationTable(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.EMBEDDED);
        
        // ask kishor about 25, cant set height by any other method
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                NO_OF_VERTICALGRIDS_TCTABLECOMPOSITE);
        
        m_classificationTCTable = createTCTable();
        hideColumns();
        addColourRenderer(m_tableColumnNames);
        JScrollPane scrollPane = new JScrollPane(m_classificationTCTable);
        composite.setLayoutData(gd_composite);
        SWTUIUtilities.embed(composite, scrollPane, false);
        
    }
    
    private void addColourRenderer(String[] columnNames)
    {
        for (String columnName : columnNames)
        {
            int colIndex = m_classificationTCTable.getColumnIndex(columnName);
            TableColumn column = m_classificationTCTable.getColumnModel()
                    .getColumn(colIndex);
            
            column.setCellRenderer(new TableCellColourRenderer());
        }
    }
    
    private void hideColumns()
    {
        // hide columns
        String[] hideColumnNames = getRegistry().getStringArray(
                "ClassificationTable.HIDE_COLUMNS");
        int colIndex = -1;
        for (String colName : hideColumnNames)
        {
            colIndex = m_classificationTCTable.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_classificationTCTable, colIndex);
        }
    }
    
    private TCTable createTCTable()
    {
        String[] colNames = getRegistry().getString(
                "ClassificationTable.ColumnNames").split(",");
        String[] colIdentifiers = getRegistry().getStringArray(
                "ClassificationTable.Identifiers");
        
        TCTable tCTable = new TCTable(colIdentifiers, colNames);
        tCTable.setShowVerticalLines(true);
        tCTable.setAutoCreateColumnsFromModel(false);
        tCTable.setAutoResizeMode(tCTable.AUTO_RESIZE_ALL_COLUMNS);
        int totalColumnWidth = tCTable.getColumnModel().getTotalColumnWidth();
        
        tCTable.getColumnModel().getColumn(0)
                .setPreferredWidth((int) (0.4 * totalColumnWidth));
        
        tCTable.setVisible(true);
        tCTable.getTableHeader().setBorder(
                UIManager.getBorder("TableHeader.cellBorder"));
        
        return tCTable;
        
    }
    
    private void createUnitsCombo(Composite parent)
    {
        m_unitsCombo = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
        GridData gridData = new GridData(SWT.RIGHT, SWT.FILL, false, false, 1,
                1);
        m_unitsCombo.setLayoutData(gridData);
        
        String[] comboItemsToSet = getRegistry().getStringArray(
                "ClassificationUnitsCombo");
        m_unitsCombo.setItems(comboItemsToSet);
        
        int defaultUnitIndex = 0;
        if (!isDefaultUOMMatric())
        {
            defaultUnitIndex = 1;
        }
        m_unitsCombo.select(defaultUnitIndex);
        m_unitsCombo.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                try
                {
                    showSelectedColumns();
                    
                    if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
                    {
                        int selectionIndex = m_unitsCombo.getSelectionIndex();
                        publishUOM(m_unitsCombo.getItem(selectionIndex));
                        invokeComparision();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }
    
    private boolean isDefaultUOMMatric()
    {
        int theScope = TCPreferenceService.TC_preference_group;
        String prefrenceName = GlobalConstant.PREF_CLASSIFICATION_DEFAULT_UOM;
        String[] strings = PreferenceUtils.getStringValues(theScope,
                prefrenceName);
        String currentGroupName = GroupUtils.getCurrentGroupName();
        List<String> preferences = Arrays.asList(strings);
        boolean isDefaultUOMMatrix = preferences.contains(currentGroupName);
        
        return isDefaultUOMMatrix;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void dispose()
    {
        if (m_iTableComparator != null)
        {
            m_iTableComparator.unregisterToComparator(CLASSIFICATION_CONTEXT);
        }
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public void updateTable(PublishEvent event)
    {
        CompareOutputBean bean = null;
        TablePainter painter = null;
        if (event != null)
        {
            bean = (CompareOutputBean) event.getNewValue();
            
            if (event.getPropertyName().equals(CLASSIFICATION_CONTEXT)
                    && this == bean.getObject())
            {
                painter = new TablePainter(bean);
            }
        }
        else
        {
            painter = new TablePainter(bean);
        }
        painter.paintTable(m_classificationTCTable);
    }
    
    private IController getController()
    {
        IController theController = ControllerFactory.getInstance()
                .getDefaultController();
        return theController;
    }
    
    public void registerSubscriber()
    {
        getController().registerSubscriber(GlobalConstant.EVENT_UOM_CHANGED,
                this);
    }
    
    public void unregisterSubscriber()
    {
        getController().unregisterSubscriber(GlobalConstant.EVENT_UOM_CHANGED,
                this);
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (!getComposite().isDisposed())
        {
            Object thisParent = getComposite().getParent();
            Object sourceParent = null;
            
            IPublisher eventPublisher = event.getSource();
            
            if (eventPublisher instanceof AbstractUIPanel)
            {
                Composite sourceComposite = ((AbstractUIPanel) eventPublisher)
                        .getComposite();
                sourceParent = sourceComposite.getParent();
            }
            
            if (event.getPropertyName()
                    .equals(GlobalConstant.EVENT_UOM_CHANGED)
                    && sourceParent != null && !thisParent.equals(sourceParent))
            {
                String uom = (String) event.getNewValue();
                int count = m_unitsCombo.getItemCount();
                for (int inx = 0; inx < count; inx++)
                {
                    String selectedUOM = m_unitsCombo.getItem(inx);
                    if (selectedUOM.equals(uom))
                    {
                        m_unitsCombo.select(inx);
                        showSelectedColumns();
                        invokeComparision();
                    }
                }
            }
        }
        
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(this, property, newValue,
                oldValue);
        return event;
    }
    
    private void publishUOM(String uom)
    {
        PublishEvent publishEvent = getPublishEvent(
                GlobalConstant.EVENT_UOM_CHANGED, uom, null);
        getController().publish(publishEvent);
    }
    
    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
}
