package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.impl.common.TargetsTableBottomPanel;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECRAffectedPartsBottomPanel extends TargetsTableBottomPanel implements IPublisher
{
    
    public ECRAffectedPartsBottomPanel(Composite parent, int style) 
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        addSelectionEvent(m_compareRevButton, GlobalConstant.EVENT_COMPARE_REVISION);
        addSelectionEvent(m_viewRddPdfButton, GlobalConstant.EVENT_VIEW_RDD_PDF);
        return false;
    }
    
    private void addSelectionEvent(Button button, final String eventName)
    {
        button.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                PublishEvent publishEvent = getPublishEvent(
                        eventName, null,
                      null);
                getController().publish(publishEvent);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }

    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    private IPublisher getPublisher()
    {
        return this;
    }
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
}
