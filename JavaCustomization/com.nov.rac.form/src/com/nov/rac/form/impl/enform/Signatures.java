package com.nov.rac.form.impl.enform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class Signatures extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    protected Registry m_registry = null;
    
    private Composite m_column1Composite = null;
    private Composite m_column2Composite = null;
    private Composite m_column3Composite = null;
    
    private Label m_creatorNameLabel = null;
    private Label m_drafterNameLabel = null;
    private Label m_checkerNameLabel = null;
    private Label m_approverNameLabel = null;
    private Label m_docControllerNameLabel = null;
    private Label m_mEApproverNameLabel = null;
    private Label m_cEApproverNameLabel = null;
    
    private Label m_createdOnLabel = null;
    private Label m_draftedOnLabel = null;
    private Label m_checkedOnLabel = null;
    private Label m_approvedOnLabel = null;
    private Label m_docControllerOnLabel = null;
    private Label m_mEApprovedONLabel = null;
    private Label m_cEApprovedONLabel = null;
    
    public Signatures(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // Name Labels
        m_creatorNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getTag("owning_user")));
        m_drafterNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("draftedby")));
        m_checkerNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("checkedby")));
        m_approverNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("approvedby")));
        m_docControllerNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4ercapprovedby")));
        m_mEApproverNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_me_approver")));
        m_cEApproverNameLabel.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_ce_approver")));
        
        // Date Labels
        this.setDate(m_createdOnLabel, "creation_date", propMap);
        this.setDate(m_draftedOnLabel, "draftedon", propMap);
        this.setDate(m_checkedOnLabel, "checkedon", propMap);
        this.setDate(m_approvedOnLabel, "approvedon", propMap);
        this.setDate(m_docControllerOnLabel, "nov4ercapproveddate", propMap);
        this.setDate(m_mEApprovedONLabel, "nov4_me_approved_date", propMap);
        this.setDate(m_cEApprovedONLabel, "nov4_ce_approved_date", propMap);
        
        
        m_column1Composite.layout();
        m_column2Composite.layout();
        m_column3Composite.layout();
        
//        m_column1Composite.pack();
//        m_column2Composite.pack();
//        m_column3Composite.pack();
//        getComposite().pack();
//      
        return true;
    }
    
    protected void setDate(Label dateLabel, String propertyName,
            IPropertyMap propMap)
    {
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate == null)
        {
            // do something?ask input form kishore:can write NA?or -?
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateLabel.setText(dateFormat.format(theDate));
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(5, false));
        getExpandableComposite().setText("Signature");
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        createColumn1Composite(l_composite);
        
        createLabel(m_column1Composite,
                getRegistry().getString("CreatedBy.Label"));
        createCreatorNameLabel(m_column1Composite);
        createOnLabel(m_column1Composite);
        createCreatedOnLabel(m_column1Composite);
        
        createLabel(m_column1Composite,
                getRegistry().getString("DraftedBy.Label"));
        createDrafterNameLabel(m_column1Composite);
        createOnLabel(m_column1Composite);
        createDraftedOnLabel(m_column1Composite);
        
        createLabel(m_column1Composite,
                getRegistry().getString("CheckedBy.Label"));
        createCheckerNameLabel(m_column1Composite);
        createOnLabel(m_column1Composite);
        createCheckedOnLabel(m_column1Composite);
        
        createVerticalSeparator(l_composite);
        
        createColumn2Composite(l_composite);
        
        createLabel(m_column2Composite,
                getRegistry().getString("ApprovedBy.Label"));
        createApproverNameLabel(m_column2Composite);// done
        createOnLabel(m_column2Composite);
        createApprovedOnLabel(m_column2Composite);
        
        createLabel(m_column2Composite,
                getRegistry().getString("DocController.Label"));
        createDocControllerNameLabel(m_column2Composite);
        createOnLabel(m_column2Composite);
        createDocControllerOnLabel(m_column2Composite);
        
        createVerticalSeparator(l_composite);
        
        createColumn3Composite(l_composite);
        
        createLabel(m_column3Composite,
                getRegistry().getString("MEApprover.Label"));
        createMEApproverNameLabel(m_column3Composite);
        createOnLabel(m_column3Composite);
        createMEApprovedOnLabel(m_column3Composite);
        
        createLabel(m_column3Composite,
                getRegistry().getString("CEApprover.Label"));
        createCEApproverNameLabel(m_column3Composite);
        createOnLabel(m_column3Composite);
        createCEApprovedOnLabel(m_column3Composite);
        return false;
    }
    
    private void createLabel(Composite composite, String setText)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
        label.setText(setText);
    }
    
    private void createMEApproverNameLabel(Composite composite)
    {
        m_mEApproverNameLabel =createNameLabel(composite);
        
    }
    
    private void createCEApproverNameLabel(Composite composite)
    {
        m_cEApproverNameLabel = createNameLabel(composite);
        
    }
    
    private void createDocControllerNameLabel(Composite composite)
    {
        m_docControllerNameLabel =createNameLabel(composite);
        
        
    }
    
    private void createCheckedOnLabel(Composite composite)
    {
        m_checkedOnLabel =createDateLabel(composite);
        
    }
    
    private void createCEApprovedOnLabel(Composite composite)
    {
        m_cEApprovedONLabel =createDateLabel(composite);
        
    }
    
    private void createApprovedOnLabel(Composite composite)
    {
        m_approvedOnLabel =createDateLabel(composite);
        
    }
    
    private void createMEApprovedOnLabel(Composite composite)
    {
        m_mEApprovedONLabel =createDateLabel(composite);
        
    }
    
    private void createDocControllerOnLabel(Composite composite)
    {
        m_docControllerOnLabel = createDateLabel(composite);
        
    }
    
    private void createDraftedOnLabel(Composite composite)
    {
        m_draftedOnLabel =createDateLabel(composite);
    }
    
    private void createCreatedOnLabel(Composite composite)
    {
        m_createdOnLabel =createDateLabel(composite);
        
        
    }
    
    private void createVerticalSeparator(Composite composite)
    {
        Label verticalSeparator = new Label(composite, SWT.SEPARATOR
                | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, false, false, 1,
                3);
        // layoutData.heightHint =
        // m_reasonForNoticeComboBox.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        verticalSeparator.setLayoutData(layoutData);
    }
    
    private void createCheckerNameLabel(Composite composite)
    {
        m_checkerNameLabel = createNameLabel(composite);
        
    }
    
    private void createColumn3Composite(Composite composite)
    {
        m_column3Composite = new Composite(composite, SWT.NONE);
        GridLayout gl_composite = new GridLayout(4, false);
        // gl_composite.horizontalSpacing = 15;
        m_column3Composite.setLayout(gl_composite);
        m_column3Composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
                false, 1, 2));
        
    }
    
    private void createApproverNameLabel(Composite composite)
    {
        m_approverNameLabel = createNameLabel(composite);
        
    }
    
    private void createColumn2Composite(Composite composite)
    {
        m_column2Composite = new Composite(composite, SWT.NONE);
        GridLayout gl_composite = new GridLayout(4, false);
        // gl_composite.horizontalSpacing = 15;
        m_column2Composite.setLayout(gl_composite);
        m_column2Composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
                false, 1, 2));
        
    }
    
    private void createOnLabel(Composite composite)
    {
        Label oNLabel = new Label(composite, SWT.NONE);
        oNLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
                1, 1));
        oNLabel.setText(getRegistry().getString("ON.Label"));
        
    }
    
    private void createDrafterNameLabel(Composite composite)
    {
        m_drafterNameLabel = createNameLabel(composite);
        
    }
    
    private void createCreatorNameLabel(Composite composite)
    {
        m_creatorNameLabel =createNameLabel(composite);
    }
    
    private void createColumn1Composite(Composite composite)
    {
        m_column1Composite = new Composite(composite, SWT.NONE);
        GridLayout gl_composite = new GridLayout(4, false);
        // gl_composite.horizontalSpacing = 15;
        m_column1Composite.setLayout(gl_composite);
        m_column1Composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
                false, 1, 3));
        
    }
    
    private Label createNameLabel(Composite composite)
    {
        Label nameLabel = new Label(composite, SWT.NONE);
        nameLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, true, 1, 1));
        return nameLabel;
    }
    private Label createDateLabel(Composite composite)
    {
        Label  dateLabel = new Label(composite, SWT.NONE);
        dateLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1));
        return dateLabel;
    }
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    //
    
}
