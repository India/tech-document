package com.nov.rac.form.impl.enform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TreeItem;

import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.form.util.DispositionTabPanel;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.form.util.TreeTable;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVENTargetsPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private String[] m_tableColumnNames = new String[] {};
    private String[] m_tableColumnIdentifiers = new String[] {};
    private final String m_panelName = getClass().getSimpleName();
    private final String COLUMN_NAMES = "COLUMN_NAMES";
    private final String COLUMN_IDENTIFIERS = "COLUMN_IDENTIFIERS";
    private final String DELIMITER = ",";
    private String m_emptyString = new String();
    private TCSession m_tcSession = null;
    private TreeTable m_treeTable = null;
    private Composite m_composite = null;
    private List<TCComponent> m_targetItemRevisionsList = new ArrayList<TCComponent>();
    private HashMap<TCComponent, TCComponent> targetRevision_DispositionMap = new HashMap<TCComponent, TCComponent>();
    private WorkflowProcess m_workflow = null;
    private TCComponent[] m_dispositionObjects = null;
    private AttachmentsButton m_buttonBar = null;
    protected int m_noOfTableColumns = 5;
    protected int m_comboBoxColumnNumber = 4;
    protected int m_componentImageColumnNumber = 0;
    private Map<String, IPropertyMap> tabelRowDataPropertyMapList = null;
    private List<String> itemRevisionsList = new ArrayList<String>();
    private IPropertyMap comboStatusMap = new SimplePropertyMap();
    
    public NOVENTargetsPanel(Composite parent, int style)
    {
        super(parent, SWT.BORDER);
        m_registry = Registry.getRegistry(this);
        m_tcSession = (TCSession) AIFUtility.getCurrentApplication()
                .getSession();
        m_composite = parent;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    protected String[] getTableColumns()
    {
        m_tableColumnNames = getRegistry().getStringArray(
                m_panelName + "." + COLUMN_NAMES, DELIMITER, m_emptyString);
        return m_tableColumnNames;
    }
    
    protected String[] getTableColumnIdentifiers()
    {
        m_tableColumnIdentifiers = getRegistry().getStringArray(
                m_panelName + "." + COLUMN_IDENTIFIERS, DELIMITER,
                m_emptyString);
        return m_tableColumnIdentifiers;
    }
    
    public TCSession getSession()
    {
        return m_tcSession;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        List<List<String[]>> rowObjects = new ArrayList<List<String[]>>();
        
        m_workflow = new WorkflowProcess(FormHelper.getTargetComponent(),
                getRegistry());
        WorkflowProcess.setWorkflow(m_workflow);
        
        // m_dispositionObjects = propMap.getTagArray("entargetsdisposition");
        m_dispositionObjects = m_workflow.getTargetDispositions();
        
        m_targetItemRevisionsList = m_workflow.getTargetItemRevisions();
        
        String[] targetItemRevisionProp = new String[] { "item_id",
                "object_name", "current_revision_id" };
        String[] dispositionProp = new String[] { "currentstatus",
                "futurestatus" };
        
        rowObjects = m_workflow.getTargetItemsData(targetItemRevisionProp,
                dispositionProp, m_noOfTableColumns);
        
        m_treeTable.loadTree(rowObjects);
        
        m_treeTable.addComboControl(m_comboBoxColumnNumber,
                getListOfComboValues(), "");
        
        loadRowItemImage();
        loadDefaultComboStatus();
        createDataModelMapping();
        createTableDataMaps();
        addListenerOnCombo();
        
        return true;
    }
    
    private void createTableDataMaps()
    {
        int rowCount = m_treeTable.getTree().getItemCount();
        String[] properties = getRegistry().getStringArray("Disposition.PROP");
        // tabelRowDataPropertyMapList = new HashMap<String, IPropertyMap>();
        
        WorkflowProcess currentProcess = WorkflowProcess.getWorkflow();
        Map<String,TCComponent> dispositionCompoMap = currentProcess.getStringToDispositionMap();
        Map<String,TCComponent> targetItemRevisions = currentProcess.getStringToComponentMap();
        
        for (int inx = 0; inx < rowCount; inx++)
        {
            String itemid = m_treeTable.getTree().getItem(inx).getText(0);
            TCComponent tcComponent = targetItemRevisions.get(itemid);
            TCComponent dispositionComponent = dispositionCompoMap.get(itemid);
            
            if (tabelRowDataPropertyMapList != null && tcComponent != null)
            {
                if (!tabelRowDataPropertyMapList.containsKey(itemid))
                {
                    IPropertyMap rowDataMap = new SimplePropertyMap();
                    rowDataMap.setString("targetitemid", itemid);
                    rowDataMap.setComponent(tcComponent);
                    
                    for (int jnx = 0; jnx < properties.length; jnx++)
                    {
                        try
                        {
                            rowDataMap.setString(properties[jnx], dispositionComponent.getProperty(properties[jnx]));
                        }
                        catch (TCException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    tabelRowDataPropertyMapList.put(itemid, rowDataMap);
                }
            }
        }
        
    }
    
    private String[] getListOfComboValues()
    {
        String[] futureStatusList = new String[] {};
        String owningGroup = GroupUtils.getBusinessGroupName(FormHelper
                .getTargetComponentOwningGroup());
        if (owningGroup.equals("RSOne"))
        {
            futureStatusList = getRegistry().getStringArray(
                    "RSOne.FutureStatus");
        }
        else
        {
            futureStatusList = getRegistry().getStringArray(
                    "Mission.FutureStatus");
        }
        return futureStatusList;
    }
    
    private void loadDefaultComboStatus()
    {
        String owningGroup = GroupUtils.getBusinessGroupName(FormHelper
                .getTargetComponentOwningGroup());
        List<TreeEditor> editors = m_treeTable.getAllEditor();
        
        String startingStatus = "";
        String futureStatus = "";
        
        int comboInx = 0;
        int noOfRows = m_treeTable.getTree().getItemCount();
        
        for (int inx = 1; inx < noOfRows; inx++)
        {
            
            // futureStatus = ((Combo) editors.get(comboInx).getEditor())
            // .getText();
            
            futureStatus = m_treeTable.getTree().getItem(inx).getText(4);
            String partId = m_treeTable.getTree().getItem(inx).getText(0);
            
            if (owningGroup.equals("RSOne"))
            {
                
                if (futureStatus == null || futureStatus.equalsIgnoreCase(""))
                {
                    futureStatus = getRegistry().getString(
                            "RSOne.DefaultFutureStatus");
                }
                
                /*
                 * startingStatus = m_treeTable.getTree().getItem(3)
                 * .getText(3); futureStatus = ((Combo)
                 * editors.get(inx).getEditor()) .getText(); if
                 * (!startingStatus.equalsIgnoreCase("ENG")) { futureStatus =
                 * "MFG"; } else { if (futureStatus.equalsIgnoreCase("")) {
                 * futureStatus = "MFG"; } }
                 */
            }
            else
            {
                if (futureStatus.equalsIgnoreCase(""))
                {
                    futureStatus = getRegistry().getString(
                            "Mission.DefaultFutureStatus");
                }
            }
            
            m_treeTable.setComboControlStatus(futureStatus, partId);
            comboInx++;
        }
    }
    
    private void loadRowItemImage()
    {
        List<Image> parentImages = new ArrayList<Image>();
        List<Image> childImages = new ArrayList<Image>();
        
        Image cureentProcessImage = TCTypeRenderer.getTypeImage(m_workflow
                .getCurrentProcess().getType(), "Job");
        parentImages.add(cureentProcessImage);
        childImages.add(TCTypeRenderer.getTypeImage("Documents", "Documents"));
        
        for (TCComponent tcComponent : m_targetItemRevisionsList)
        {
            Image image = TCTypeRenderer.getTypeImage(tcComponent.getType(),
                    tcComponent.getType());
            parentImages.add(image);
            System.out.println("");
        }
        Image[] parentImagesArray = new Image[parentImages.size()];
        Image[] childImagesArray = new Image[childImages.size()];
        
        m_treeTable.addImages(parentImages.toArray(parentImagesArray),
                childImages.toArray(childImagesArray),
                m_componentImageColumnNumber);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        List<IPropertyMap> propertyMapList = new ArrayList<IPropertyMap>();
        
        for (IPropertyMap propertyMap : tabelRowDataPropertyMapList.values())
        {
            propertyMapList.add(propertyMap);
        }
        
        IPropertyMap[] propertyMapArray = new IPropertyMap[propertyMapList
                .size()];
        propertyMapArray = propertyMapList.toArray(propertyMapArray);
        
        propMap.setCompoundArray("rowDataPropertyMaps", propertyMapArray);
        System.out.println("");
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_buttonBar.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        tabelRowDataPropertyMapList = new HashMap<String, IPropertyMap>();
        createTable(composite);
        createButtons(composite);
        // createDataModelMapping();
        return true;
    }
    
    private void createTable(final Composite composite)
    {
        final Composite tabelComposite = new Composite(composite, SWT.BORDER);
        GridData table_gridData = new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1);
        tabelComposite.setLayoutData(table_gridData);
        m_treeTable = new TreeTable(tabelComposite);
        m_treeTable.setBounds(800, 120);
        m_treeTable.addColumns(getTableColumns());
        
        m_treeTable.getTree().addListener(SWT.MouseDoubleClick, new Listener()
        {
            public void handleEvent(Event e)
            {
                e.doit = false;
                createDispositionTab();
                System.out.println("double clicked");
            }
        });
    }
    
    protected void createDispositionTab()
    {
        TreeItem[] treeItem = m_treeTable.getTree().getSelection();
        treeItem[0].setExpanded(false);
        
        WorkflowProcess currentProcess = WorkflowProcess.getWorkflow();
        String partId = treeItem[0].getText(0);
        String revision = treeItem[0].getText(2);
        TCComponent component = currentProcess.getStringToComponentMap().get(
                partId);
        String processName = "";
 /*       try
        {
            processName = (currentProcess.getRootTask()).getName();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        if (partId.equals(processName)
                || component.getType().equals("Documents"))
        {
            return;
        }
        String panelType = "DefaultDisposition";
        IPropertyMap iPropertyMap = tabelRowDataPropertyMapList.get(partId);
        
        if (!revision.equals("01"))
        {
            panelType = "Disposition";
        }
        
        Control[] tabList = FormHelper.getFolder().getTabList();
        CTabItem tab = new CTabItem(FormHelper.getFolder(), SWT.NONE);
        if (tabList.length > 1)
        {
            tab = FormHelper.getFolder().getItem(1);
        }
        tab.setText(treeItem[0].getText());
        FormHelper.getFolder().setSelection(tab);
        Composite tabComposite = new Composite(FormHelper.getFolder(),
                SWT.BORDER);
        tabComposite.setLayout(new GridLayout(1, false));
        tabComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1));
        
        Composite mainComposite = new Composite(tabComposite, SWT.BORDER);
        mainComposite.setLayout(new GridLayout(1, false));
        mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1));
        
        DispositionTabPanel dispositionTabPanel = new DispositionTabPanel(
                getRegistry());
        dispositionTabPanel.createUI(mainComposite, panelType);
        dispositionTabPanel.loadMap(partId, tabelRowDataPropertyMapList);
        dispositionTabPanel.load(iPropertyMap);*/
        
        // tabComposite.setBackground(new Color("blue"));
      //  tab.setControl(tabComposite);
        
        try
        {
            Map strigToComponentMap = currentProcess.getStringToComponentMap();
            
            IPropertyMap iMap = new SimplePropertyMap();
            iMap.setComponent(component);
            String objectId = component.getProperty("item_id");
            String objectName = component.getProperty("object_name");
            
            iMap.setString("item_id", objectId);
             iMap.setString("object_name", objectName);
             
            PopupDialog dialog = new PopupDialog(m_treeTable.getTree().getShell());
            dialog.setInputPropertyMap(iMap);
            //dialog.setDialogSplitCount(2);
            dialog.open();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    protected void createButtons(Composite composite)
    {
        
        Image minusButtonImage = new Image(composite.getDisplay(),
                m_registry.getImage("removeButton.IMAGE"), SWT.NONE);
        Image plusButtonImage = new Image(composite.getDisplay(),
                m_registry.getImage("addButton.IMAGE"), SWT.NONE);
        
        m_buttonBar = new AttachmentsButton(composite, SWT.CENTER,
                plusButtonImage, minusButtonImage);
        GridData gridData = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1,
                1);
        m_buttonBar.setLayoutData(gridData);
        m_buttonBar.addSelectionListenerToMinusButton(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                removeSelectedTargets();
                
                TreeItem[] selection = m_treeTable.getTree().getSelection();
                String partId = selection[0].getText(0);
                tabelRowDataPropertyMapList.remove(partId);
                getComposite().getDisplay().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        reloadTreeTable();
                        resetFutureStatus(tabelRowDataPropertyMapList);
                        /*
                         * if(null != FormHelper.getFolder().getItem(1)) {
                         * FormHelper.getFolder().getItem(1).dispose(); }
                         */
                    }
                    
                });
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
            }
        });
        
        m_buttonBar.addSelectionListenerToPlusButton(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                new AddTargetToENForm();
                getComposite().getDisplay().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        reloadTreeTable();
                    }
                });
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
            }
        });
    }
    
    private void resetFutureStatus(
            Map<String, IPropertyMap> tabelRowDataPropertyMapList)
    {
        
        Map<String, TreeEditor> editorMap = m_treeTable.getEditorMap();
        for (Map.Entry entry : editorMap.entrySet())
        {
            String partId = (String) entry.getKey();
            TreeEditor editor = (TreeEditor) entry.getValue();
            Combo combo = (Combo) editor.getEditor();
            String futureStatus = PropertyMapHelper.handleNull(comboStatusMap
                    .getString(partId));
            if (!futureStatus.equals(""))
            {
                combo.setText(futureStatus);
            }
            else
            {
                String owningGroup = GroupUtils.getBusinessGroupName(FormHelper
                        .getTargetComponentOwningGroup());
                if (owningGroup.equals("RSOne"))
                {
                    
                    if (futureStatus == null
                            || futureStatus.equalsIgnoreCase(""))
                    {
                        futureStatus = getRegistry().getString(
                                "RSOne.DefaultFutureStatus");
                    }
                }
                else
                {
                    if (futureStatus.equalsIgnoreCase(""))
                    {
                        futureStatus = getRegistry().getString(
                                "Mission.DefaultFutureStatus");
                    }
                }
            }
        }
    }
    
    private void reloadTreeTable()
    {
        try
        {
            m_treeTable.clear();
            load(new SimplePropertyMap());
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public Object getUIPanel()
    {
        return this;
    }
    
    protected void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        for (TreeEditor editor : m_treeTable.getAllEditor())
        {
            Combo combo = (Combo) editor.getEditor();
            dataBindingHelper.bindData(combo, "entargetsdisposition",
                    FormHelper.getDataModelName());
        }
    }
    
    private void addListenerOnCombo()
    {
        
        for (final TreeEditor editor : m_treeTable.getAllEditor())
        {
            final Combo combo = (Combo) editor.getEditor();
            
            combo.addModifyListener(new ModifyListener()
            {
                @Override
                public void modifyText(ModifyEvent modifyevent)
                {
                    String selectedText = combo.getText();
                    String partId = editor.getItem().getText(0);
                    comboStatusMap.setString(partId, selectedText);
                    IPropertyMap propertyMap = tabelRowDataPropertyMapList
                            .get(partId);
                    propertyMap.setString("futurestatus", selectedText);
                }
            });
        }
    }
    
    /*
     * private void initialize_CurrentProcess_EnRootTask() { try {
     * com.teamcenter.rac.aif.kernel.AIFComponentContext[] contexts =
     * com.nov.rac.form.helpers.FormHelper
     * .getTargetComponent().whereReferenced(); TCComponent[]
     * targetItemRevisions = null; if (contexts != null) { for (int j = 0; j <
     * contexts.length; j++) { if ((TCComponent) contexts[j].getComponent()
     * instanceof TCComponentProcess) { currentProcess = (TCComponentProcess)
     * contexts[j] .getComponent(); enRootTask = ((TCComponentProcess)
     * currentProcess) .getRootTask(); break; } } } } catch (TCException e) {
     * e.printStackTrace(); } }
     */
    
    protected void removeSelectedTargets()
    {
        
        {
            // saving the form before removing target - TCDECREL-2051
            // enFormPanel.saveForm();
            // fetching isLocked value in both listeners- because cache value is
            // carried to next add/remove
            // comp = (JPanel)this.getParent().getParent();
            // try {
            // isLockedPropVal =
            // enFormPanel.Enform.getLogicalProperty(reg.getString("PropertyMapHelper
            
            // } catch (TCException e) {
            // e.printStackTrace();
            // }
            // if(isLockedPropVal == true)
            // {
            // MessageBox.post(getWindow(comp),"ERROR",reg.getString("NOVENTargetsPanel.formIsLocked.MSG.MSG"),
            // MessageBox.ERROR);
            // return;
            // }
            // initialize_CurrentProcess_EnRootTask();
            TCComponentTask enRootTask = WorkflowProcess.getWorkflow()
                    .getRootTask();
            {
                Object addRemInputObjs[] = new Object[3];
                Object addRemRetObj = null;
                int RemoveFlag = 0;
                int singleSelection = 0;
                String processUID = enRootTask.getUid();
                TreeItem[] tableSelections = m_treeTable.getTree()
                        .getSelection();
                if (tableSelections.length < 1)
                {
                    MessageBox.post(
                            m_registry.getString("selectPartToRemove.MSG"),
                            "ERROR", MessageBox.ERROR);
                    return;
                }
                String tableSelection_ItemId = tableSelections[singleSelection]
                        .getText();
                
                TCComponentItemRevision targetToRemove = null;
                //
                for (int inx = 0; (inx < m_targetItemRevisionsList.size()); inx++)
                {
                    TCComponent currentTargetItem = m_targetItemRevisionsList
                            .get(inx);
                    try
                    {
                        if ((currentTargetItem instanceof TCComponentItemRevision)
                                && (currentTargetItem
                                        .getStringProperty("item_id")
                                        .equalsIgnoreCase(tableSelection_ItemId)))
                        {
                            targetToRemove = (TCComponentItemRevision) currentTargetItem;
                            break;
                        }
                    }
                    catch (TCException e)
                    {
                        
                    }
                }
                // //close disp tab
                // if(enFormPanel.enautomatonTabbePane. ()>1)
                // {
                // NOVEnDispositionTabPanel_v2
                // dispPanel=(NOVEnDispositionTabPanel_v2)enFormPanel.enautomatonTabbePane.getComponentAt(
                // 1 );
                // if(dispPanel != null)
                // {
                // dispPanel.dispcomp.refresh();
                // enFormPanel.enautomatonTabbePane.remove( dispPanel );
                // }
                // }
                //
                // call user service
                TCUserService userservice = ((TCSession) AIFUtility
                        .getDefaultSession()).getUserService();
                addRemInputObjs[0] = (TCComponent) enRootTask;
                addRemInputObjs[1] = new TCComponent[] { targetToRemove };
                addRemInputObjs[2] = RemoveFlag;
                try
                {
                    addRemRetObj = (Object[]) userservice.call(
                            "NATOIL_AddRemoveTargets_EN", addRemInputObjs);
                }
                catch (TCException e)
                {
                    // throw error if all targets are being removed
                	MessageBox.post("Atleast one Target should be attached",
                            "ERROR", MessageBox.ERROR);
                }
                //
                try
                {
                    enRootTask.refresh();
                    
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
                // if(addRemRetObj!= null)
                // {
                // //String selObjType = targetToRemove.getType();
                // TCComponent[] removeList = (TCComponent[])addRemRetObj;
                // int removeCnt = removeList.length;
                //
                // //enRootTask.removeAttachments(removeList);
                // if(removeCnt==1)
                // {
                // if(removeList[0].getType().startsWith(reg.getString("DocsItem.TYPE")))
                // {
                // this.enFormPanel.Entargets.remove(removeList[0]);
                // }else
                // {
                // TreeTableNode selNode = (TreeTableNode)selNodes[0];
                // targetsTable.removeTarget(targetToRemove, selNode);
                // }
                // }
                // if(removeCnt>1)
                // {
                // for(int j=0; j<removeCnt; j++)
                // {
                // //if RDD, remove from targets list - no need to remove from
                // UI
                // if(removeList[j].getType().startsWith(reg.getString("DocsItem.TYPE")))
                // {
                // this.enFormPanel.Entargets.remove(removeList[j]);
                // continue;
                // }
                // //if not doc, find and remove the node from UI -- in turn
                // removes RDD also
                // //and remove from targets list.
                // for(int k=1; k< targetsTable.treeTableModel.getRowCount();
                // k++)
                // {
                // TCComponentItemRevision compAtNode= null;
                // TreeTableNode treeNode = targetsTable.getNodeForRow(k);
                // if (treeNode instanceof NovTreeTableNode)
                // compAtNode =
                // (TCComponentItemRevision)((NovTreeTableNode)treeNode).context;
                // if(compAtNode != null && removeList[j] == compAtNode)
                // {
                // targetsTable.removeTarget(compAtNode, treeNode);
                // }
                // }
                // }
                // }//if remove cnt is more than 1
                // enFormPanel.Enform.save();
                // enFormPanel.Enform.refresh();
                // targetsTable.updateUI();
                // }//if return not null
                // } catch (TCException e)
                // {
                // //throw error if all targets are being removed
                // MessageBox.post(getWindow(comp),"ERROR",
                // e.getDetailsMessage(), MessageBox.ERROR);
                // }
            }
            //
        }
    }
    
}
