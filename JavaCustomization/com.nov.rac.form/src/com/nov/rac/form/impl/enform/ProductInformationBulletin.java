package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.MandatoryFieldValidator;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ProductInformationBulletin extends AbstractUIPanel implements
        INOVFormLoadSave,ISubscriber
{
    
    private static final int COLUMNS_REVIEW_STATUS_COMPOSITE = 3;
    private static final int COLUMNS_COMPOSITE_4 = 4;
    private static final int HORIZONTAL_SPACING = 10;
    
    private Registry m_registry;
    
    private Button m_safetyAlertsButton;
    private Button m_productInformationBulletin;
    
    private Button m_internalBulletinButton;
    
    private Button m_reviewBoardCompletedButton;
    
    private Button m_legalReviewCompletedButton;
    
    protected MandatoryFieldValidator m_reviewStatusValidator = new MandatoryFieldValidator();
    protected MandatoryFieldValidator m_productBulletinTypeValidator = new MandatoryFieldValidator();
    
    private Label m_reviewStatusLabel;
    private Label m_productBulletinTypeLabel;
    
    public ProductInformationBulletin(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap pibFormPropMap = propMap.getCompound("Nov4_secondary_form");
        
        m_safetyAlertsButton.setSelection(pibFormPropMap.getLogical("safetyalert"));
        m_internalBulletinButton.setSelection(pibFormPropMap
                .getLogical("internal_bulletin"));
        m_productInformationBulletin.setSelection(pibFormPropMap
                .getLogical("product_info_bulletin"));
        m_reviewBoardCompletedButton.setSelection(pibFormPropMap
                .getLogical("nov4_alc_review_board"));
        m_legalReviewCompletedButton.setSelection(pibFormPropMap
                .getLogical("nov4_alc_legal_review"));
        m_internalBulletinButton.notifyListeners(SWT.Selection,new Event());
        m_reviewBoardCompletedButton.notifyListeners(SWT.Selection,new Event());
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setLogical("safetyalert", m_safetyAlertsButton.getSelection());
        propMap.setLogical("internal_bulletin",
                m_internalBulletinButton.getSelection());
        propMap.setLogical("product_info_bulletin",
                m_productInformationBulletin.getSelection());
        propMap.setLogical("nov4_alc_review_board",
                m_reviewBoardCompletedButton.getSelection());
        propMap.setLogical("nov4_alc_legal_review",
                m_legalReviewCompletedButton.getSelection());
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_safetyAlertsButton.setEnabled(flag);
        m_internalBulletinButton.setEnabled(flag);
        m_productInformationBulletin.setEnabled(flag);
        m_reviewBoardCompletedButton.setEnabled(flag);
        m_legalReviewCompletedButton.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getMainComposite();
        createHeaderComposite(mainComposite);
        createReviewStatusComposite(mainComposite);
        createBulletinTypeComposite(mainComposite);
        registerSubscriber();
        setEnabled(false);
        return false;
    }
    
    private void createBulletinTypeComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(COLUMNS_COMPOSITE_4, false);
        composite.setLayout(gl_composite);
        
       
        m_productBulletinTypeLabel = createLabel(composite,
                "ProductBulletinType.Label");
        
        

        m_safetyAlertsButton = createButton(composite, SWT.RADIO,
                "SafetyAlerts.Button");
        GridData gd_safetyAlertsButton = new GridData();
        gd_safetyAlertsButton.horizontalIndent =  SWTUIHelper.convertHorizontalDLUsToPixels(composite, HORIZONTAL_SPACING);
        m_safetyAlertsButton.setLayoutData(gd_safetyAlertsButton);
        m_productInformationBulletin = createButton(composite, SWT.RADIO,
                "ProductInfomationBulletin.Button");
        m_internalBulletinButton = createButton(composite, SWT.RADIO,
                "InternalBulletin.Button");
        
        UIHelper.makeMandatory(m_productBulletinTypeLabel, m_productBulletinTypeValidator);
        
        SelectionListener listener = new RadioSelectionListener();
        m_safetyAlertsButton.addSelectionListener(listener);
        m_productInformationBulletin.addSelectionListener(listener);
        m_internalBulletinButton.addSelectionListener(listener);
        
    }
    
    private void createReviewStatusComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(COLUMNS_REVIEW_STATUS_COMPOSITE,
                false);
        composite.setLayout(gl_composite);
        
        m_reviewStatusLabel = createLabel(composite, "ReviewStatus.Label");
        m_reviewBoardCompletedButton = createButton(composite, SWT.CHECK,
                "ReviewBoardCompleted.Button");
        GridData gd_reviewBoardButton = new GridData();
        gd_reviewBoardButton.horizontalIndent =  SWTUIHelper.convertHorizontalDLUsToPixels(composite, HORIZONTAL_SPACING);
        m_reviewBoardCompletedButton.setLayoutData(gd_reviewBoardButton);
        
        m_legalReviewCompletedButton = createButton(composite, SWT.CHECK,
                "LegalReviewCompleted.Button");
        
        UIHelper.makeMandatory(m_reviewStatusLabel, m_reviewStatusValidator);
        
        SelectionListener listener = new CheckBoxListener();
        m_reviewBoardCompletedButton.addSelectionListener(listener);
        m_legalReviewCompletedButton.addSelectionListener(listener);
    }
    
    private void createHeaderComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        composite.setLayout(new GridLayout(COLUMNS_COMPOSITE_4, false));
        
        Label headerLabel = createLabel(composite,
                "PRODUCTINFORMATIONBULLETIN.Label");
        SWTUIHelper.setFont(headerLabel, SWT.BOLD);
        GridData gd_headerLabel = new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1);
        headerLabel.setLayoutData(gd_headerLabel);
        
//        createButton(composite, SWT.PUSH, "FEEDBACK.Button");
//        createButton(composite, SWT.PUSH, "SAVE.Button");
//        createButton(composite, SWT.PUSH, "CANCEL.Button");
        
    }
    
    private Label createLabel(Composite parent, String text)
    {
        Label label = new Label(parent, SWT.NONE);
        label.setText(getRegistry().getString(text));
        return label;
    }
    
    private Button createButton(Composite parent, int Style, String text)
    {
        Button button = new Button(parent, Style);
        button.setText(getRegistry().getString(text));
        return button;
    }
    
    private Composite getMainComposite()
    {
        Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private class CheckBoxListener extends SelectionAdapter
    {
        @Override
        public void widgetSelected(SelectionEvent e)
        {
            if (m_reviewBoardCompletedButton.getSelection()
                    || m_legalReviewCompletedButton.getSelection())
            {
                m_reviewStatusValidator.setValid(true);
            }
            else
            {
                m_reviewStatusValidator.setValid(false);
            }
            MandatoryFieldValidator.updateControl(m_reviewStatusLabel);
        }
    }
    
    private class RadioSelectionListener extends SelectionAdapter
    {
        @Override
        public void widgetSelected(SelectionEvent e)
        {
            if (m_safetyAlertsButton.getSelection()
                    || m_productInformationBulletin.getSelection()
                    || m_internalBulletinButton.getSelection())
            {
                m_productBulletinTypeValidator.setValid(true);
            }
            else
            {
                m_productBulletinTypeValidator.setValid(false);
            }
            
            MandatoryFieldValidator
                    .updateControl(m_productBulletinTypeLabel);
        }
    }
    
    private void registerSubscriber()
    {
        getController().registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private void unregisterSubscriber()
    {
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, getSubscriber());
        getController().unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, getSubscriber());
    }
    
    private ISubscriber getSubscriber()
    {
        // TODO Auto-generated method stub
        return this;
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    @Override
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public void update(PublishEvent event)
    {
        
        SetEnabledRunnable runnable = new SetEnabledRunnable(event, this);
        Display.getDefault().syncExec(runnable);
        
    }
}
