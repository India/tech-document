package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.framework.tablecomparator.impl.ComparatorDataModel;
import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;
import com.nov.rac.framework.tablecomparator.impl.ItemComparator;
import com.nov.rac.framework.tablecomparator.impl.TablePainter;
import com.nov.rac.item.itemsearch.SearchResultBean;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class RelatedDocumentsPanel extends DocumentsPanel implements
        ITableComparatorSubscriber,ISubscriber
{
    private static int TABLE_VERTICAL_SPAN = 21;
    private TCComponent m_tcComponent = null;
    private ITableComparator m_iTableComparator = null;
    private static final String RD_CONTEXT = "RD";
    private static final String SEARCH_RESULT = "SEARCH_RESULT";
    
    public RelatedDocumentsPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        setTableVerticalSpan(TABLE_VERTICAL_SPAN);
        super.createUI();
        final String label = getRegistry().getString(
                "RelatedDocumentPanel.GroupContainerText");
        //getGroupContainer().setText(label);
        
        getGroupContainer().addPaintListener(new PaintListener () {
            @Override
            public void paintControl (PaintEvent e) {
                Image docImage = TCTypeRenderer.getTypeImage("Documents", null);
                e.gc.drawImage (docImage, 10, 0);
                e.gc.drawString(label,docImage.getBounds().width+15, 0);
            }
        });
        
        Image minusButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        Image pluseButtonImage = new Image(getComposite().getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        setAddButtonText("");
        setAddButtonImage(pluseButtonImage);
        setRemoveButtonImage(minusButtonImage);
        addListeners();
        registerSubscriber(SEARCH_RESULT,this);
        return true;
    }

    private void addListeners()
    {
        setRemoveButtonListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                try
                {
                    validateTableRowSelection();
                
                    if(confirmRemoval())
                    {
                        deleteRD();
                    }
                }
                catch (TCException e)
                {
                    MessageBox.post(e.getMessage(), "ERROR", MessageBox.ERROR);
                }
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponent tcComponent = propMap.getComponent();
        m_tcComponent = tcComponent;
        
        setRelation("RelatedDocuments");
        super.load(propMap);
        setTableColumnsWidth(getTable());
        makeLatestRevisionEditable(m_tcComponent);
        if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
            initializeComparision();
        }
        
        return true;
    }
    
    private void initializeComparision()
    {
        String[] tableColumnNames = getRegistry().getStringArray(
                "DocumentTable.CompareColumns");
        
        IDataModel model = new ComparatorDataModel();
        model.setPrimaryKeyColumn("item_id");
        model.setTableColumns(tableColumnNames);
        model.setTable(getTable());
        model.setComparator(new ItemComparator());
        model.setCompareObject(m_tcComponent);
        model.setSubscriber(this);
        
        m_iTableComparator = DefaultTableComparator.getController();
        m_iTableComparator.setContext(RD_CONTEXT, model);
        m_iTableComparator.compareTable(RD_CONTEXT);
    }
    
    protected IPublisher getPublisher()
    {
        return this;
    }
    
    @Override
    protected void setTableVerticalSpan(int verticalSpan)
    {
        super.setTableVerticalSpan(verticalSpan);
    }
    
    @Override
    public void dispose()
    {
        if (m_iTableComparator != null)
        {
            m_iTableComparator.unregisterToComparator(RD_CONTEXT);
        }
        unregisterSubscriber(SEARCH_RESULT, this);
        super.dispose();
    }
    
    @Override
    public void updateTable(PublishEvent event)
    {
        CompareOutputBean bean = null;
        TablePainter painter = null;
        if (event != null)
        {
            bean = (CompareOutputBean) event.getNewValue();
            
            if (event.getPropertyName().equals(RD_CONTEXT)
                    && this == bean.getObject())
            {
                painter = new TablePainter(bean);
            }
        }
        else
        {
            painter = new TablePainter(bean);
        }
        painter.paintTable(getTable());
    }

    @Override
    public void update(PublishEvent event)
    {
        String eventContext = (String) event.getPropertyName();
        SearchResultBean resultBean =  (SearchResultBean) event.getNewValue();
        if(SEARCH_RESULT.equals(eventContext) && resultBean.getSource() == getButtonComposite() )
        {
            TCComponent[] secondaryObjects = (TCComponent[]) resultBean.getSearchResult();
            createRelation(m_tcComponent, secondaryObjects);
        }
    }
    
    private void deleteRD()
    {
        try
        {
            int row = getTable().getSelectedRow();
            deleteObject(m_tcComponent,row);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void makeLatestRevisionEditable(TCComponent tcComponent)
    {
        if((GlobalConstant.COMPARE_NONCOMPARE_FLAG && !FormHelper.isLatestItemRevision(tcComponent))
               || !FormHelper.hasWriteAccess(tcComponent))
        {
            m_addButton.setEnabled(false);
            m_removeButton.setEnabled(false);
        }
        else
        {
            m_addButton.setEnabled(true);
            m_removeButton.setEnabled(true);
        }
    }

}
