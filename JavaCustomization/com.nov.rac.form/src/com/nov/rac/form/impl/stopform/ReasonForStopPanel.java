package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.helpers.LabelTextFieldMandatoryValidator;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ReasonForStopPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    public ReasonForStopPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, HORIZONTALSPAN,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(2, false));
        
        createReasonForStopLabelRow(l_composite);
        createTextAreaRow(l_composite);
        createStopDispositionLabelRow(l_composite);
        createStatusRadioButtonsRow(l_composite);
        LabelTextFieldMandatoryValidator m_lableFieldValidator = new LabelTextFieldMandatoryValidator(m_reasonForStopTxt,m_reasonForStopLbl);
        UIHelper.makeMandatory(m_reasonForStopLbl, m_lableFieldValidator);
        
        createDataModelMapping();
        
        return true;
    }
    
    private void createStatusRadioButtonsRow(Composite composite)
    {
        
        Composite l_composite = new Composite(composite, SWT.NONE);
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2,
                1);
        l_composite.setLayoutData(gridData);
        
        l_composite.setLayout(new RowLayout(SWT.HORIZONTAL));
        
        m_stopAllBtn = new Button(l_composite, SWT.RADIO);
        m_stopAllBtn.setText(m_registry.getString("stopAll.button"));
        
        m_stopAllBtn.setSelection(true);
        
        m_stopWOPOBtn = new Button(l_composite, SWT.RADIO);
        m_stopWOPOBtn.setText(m_registry.getString("stopWOPO.button"));
        
        m_stopShipmentBtn = new Button(l_composite, SWT.RADIO);
        m_stopShipmentBtn.setText(m_registry.getString("stopShipment.button"));
        
        m_stopSalesOrderBtn = new Button(l_composite, SWT.RADIO);
        m_stopSalesOrderBtn.setText(m_registry
                .getString("stopSalesOrder.button"));
    }
    
    private void createStopDispositionLabelRow(Composite composite)
    {
        
        m_stopDispositionLbl = new Label(composite, SWT.NONE);
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 2,
                1);
        m_stopDispositionLbl.setLayoutData(gridData);
        
        m_stopDispositionLbl.setText(m_registry
                .getString("stopDisposition.label"));
        
    }
    
    private void createTextAreaRow(Composite composite)
    {
        
        m_reasonForStopTxt = new Text(composite, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        m_reasonForStopTxt.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, TEXTAREA_VERTICAL_SPAN);
        m_reasonForStopTxt.setLayoutData(gridData);
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        
        new Label(composite, SWT.NONE);
        
    }
    
    private void createReasonForStopLabelRow(Composite composite)
    {
        
        m_reasonForStopLbl = new Label(composite, SWT.NONE);
        
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2,
                1);
        m_reasonForStopLbl.setLayoutData(gridData);
        
        m_reasonForStopLbl.setText(m_registry.getString("reasonForStop.label"));
        
    }
    
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
   
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_reasonForStopTxt.setText(PropertyMapHelper.handleNull(propMap.getString(GlobalConstant.STOPREASON)));
        
        m_stopAllBtn.setSelection(PropertyMapHelper.handleNullForLogical(propMap.getLogical(GlobalConstant.NOV4_STOPALL)));
        m_stopWOPOBtn.setSelection(PropertyMapHelper.handleNullForLogical(propMap.getLogical(GlobalConstant.STOPWOANDPO)));
        m_stopShipmentBtn.setSelection(PropertyMapHelper.handleNullForLogical(propMap.getLogical(GlobalConstant.STOPSHIPMENT)));
        m_stopSalesOrderBtn.setSelection(PropertyMapHelper.handleNullForLogical(propMap.getLogical(GlobalConstant.NOV4_SSO)));
        
        if((!m_stopAllBtn.getSelection()) && (!m_stopWOPOBtn.getSelection()) && (!m_stopShipmentBtn.getSelection()) && (!m_stopSalesOrderBtn.getSelection()))
        {
            m_stopAllBtn.setSelection(true);
        }
        
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(GlobalConstant.STOPREASON , m_reasonForStopTxt.getText());
        propMap.setLogical(GlobalConstant.STOPWOANDPO, m_stopWOPOBtn.getSelection());
        propMap.setLogical(GlobalConstant.STOPSHIPMENT, m_stopShipmentBtn.getSelection());
        propMap.setLogical(GlobalConstant.NOV4_SSO, m_stopSalesOrderBtn.getSelection());
        propMap.setLogical(GlobalConstant.NOV4_STOPALL, m_stopAllBtn.getSelection());
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }

   
    @Override
    public void setEnabled(boolean flag) 
    {
        m_reasonForStopTxt.setEnabled(flag);
        m_stopAllBtn.setEnabled(flag);
        m_stopWOPOBtn.setEnabled(flag);
        m_stopShipmentBtn.setEnabled(flag);
        m_stopSalesOrderBtn.setEnabled(flag);
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_reasonForStopTxt,
        		GlobalConstant.STOPREASON, FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_stopAllBtn,
                GlobalConstant.NOV4_STOPALL, FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_stopWOPOBtn,
                GlobalConstant.STOPWOANDPO, FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_stopShipmentBtn,
                GlobalConstant.STOPSHIPMENT, FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_stopSalesOrderBtn,
                GlobalConstant.NOV4_SSO, FormHelper.getDataModelName());
        
    }
    
    private Registry m_registry;
    private Label m_reasonForStopLbl;
    private Text m_reasonForStopTxt;
    private Label m_stopDispositionLbl;
    private Button m_stopAllBtn;
    private Button m_stopWOPOBtn;
    private Button m_stopShipmentBtn;
    private Button m_stopSalesOrderBtn;
    private static final int HORIZONTALSPAN = 10;
    private static final int TEXTAREA_VERTICAL_SPAN = 3;
}
