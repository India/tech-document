package com.nov.rac.form.impl.enform;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.common.AttachmentsBoxPanel;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.newdataset.NewDatasetCommand;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ChangeDescriptionAttachmentsPanel extends AttachmentsBoxPanel
        implements PropertyChangeListener
{
    public ChangeDescriptionAttachmentsPanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        setNameLabel(m_registry.getString("ChangeDescriptionAttachmentsPanel.Label"));
//        setGroupCompositName(m_registry
//                .getString("ChangeDescriptionAttachmentsPanel.GroupContainerText"));
        
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        List<TCComponentDataset> datasetArray = new ArrayList<TCComponentDataset>();
        TCComponentForm form = (TCComponentForm) FormHelper.getTargetComponent();
        
        AIFComponentContext[] comps = form.getSecondary();// ("_ChangeDescriptionDocument_");
        String relationName = m_registry
                .getString("ChangeDescriptionDocument.RELATION");
        
        form.getRelated("_ChangeDescriptionDocument_");
        
        for (AIFComponentContext object : comps)
        {
            if (object.getComponent() instanceof TCComponentDataset
                    && object.getContextDisplayName().equals(relationName))
            {
                datasetArray.add((TCComponentDataset) object.getComponent());
            }
        }
        
        addRowsToTable(datasetArray);
        
        
        // delete this code!!!!!!!!!!!!
        String prop = form.getProperty("_CustomSubForm_");
        String prop1 = form.getProperty("_ChangeDescriptionDocument_");
        String prop2 = form.getProperty("object_name");
        
        // delete this code!!!!!!!!!!!!
        TCComponent relatedComp = form.getRelatedComponent(relationName);
        AIFComponentContext[] relatedComp12 = form.getRelated();
        AIFComponentContext[] datasets = form.whereReferencedByTypeRelation(
                null, new String[] { relationName });
        
        return true;
    }
    
    @Override
    protected void plusButtonSelectionListener()
    {
        try
        {
            Registry l_reg = Registry
                    .getRegistry("com.teamcenter.rac.common.actions.actions");
            
            Object param[] = new Object[3];
            param[0] = AIFUtility.getCurrentApplication().getDesktop();
            param[1] = AIFUtility.getCurrentApplication();
            param[2] = new Boolean(true);
            
            NewDatasetCommand command = (NewDatasetCommand) l_reg
                    .newInstanceForEx("newDatasetCommand", param);
            command.addPropertyChangeListener(ChangeDescriptionAttachmentsPanel.this);
            command.run();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void minusButtonSelectionListener()
    {
        int rowIndex = m_table.getSelectedRow();
        
        try
        {
            validateTableRowSelection();
            if ( confirmRemoval() && rowIndex >= 0)
            {
                AIFTableLine[] object = m_table.getSelected();
                TCComponentDataset dataset = (TCComponentDataset) object[0]
                        .getComponent();
                TCComponent[] secondaryObjects = new TCComponent[]{dataset};
                TCComponentForm form = (TCComponentForm) FormHelper.getTargetComponent();
                String relationName = m_registry
                        .getString("ChangeDescriptionDocument.RELATION");
                /*DeleteRelationHelper delete = new DeleteRelationHelper(
                        form,
                        dataset,
                        m_registry
                                .getString("ChangeDescriptionDocument.RELATION"));
                delete.deleteRelation();*/
                FormHelper.deleteRelation(form, secondaryObjects, relationName);
                m_table.removeRow(rowIndex);
            }
        }
        catch (TCException e)
        {
            MessageBox.post(e.getMessage(), "ERROR", MessageBox.ERROR);
        }
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        TCComponentDataset newDataset = (TCComponentDataset) event
                .getNewValue();
        m_table.dataModel.addRow(newDataset);
    }
    
}