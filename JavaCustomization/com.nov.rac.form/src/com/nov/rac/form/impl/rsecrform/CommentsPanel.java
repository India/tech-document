package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class CommentsPanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private Text m_commentsText = null;
    
    public CommentsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_commentsText.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_comments")));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        final Composite mainComposite = getMainComposite();
        getExpandableComposite()
                .setText(getRegistry().getString("comments.LABEL"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        createComments(mainComposite);
        
        return true;
    }
    
    private Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    private void createComments(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout commentsLayout = new GridLayout(1, false);
        commentsLayout.marginHeight = 0;
        commentsLayout.marginWidth = 0;
        composite.setLayout(commentsLayout);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                25));
        
        m_commentsText = new Text(composite, SWT.BORDER | SWT.V_SCROLL |SWT.WRAP
                | SWT.READ_ONLY | SWT.MULTI);
        GridData gd_commentsText = new GridData(SWT.FILL, SWT.FILL, true, true,
                1, 1);
        gd_commentsText.heightHint = composite.getBounds().height;
        m_commentsText.setLayoutData(gd_commentsText);
        m_commentsText
                .setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
               
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
}
