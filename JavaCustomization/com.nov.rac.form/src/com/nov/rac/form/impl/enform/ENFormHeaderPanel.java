package com.nov.rac.form.impl.enform;


import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.FormHeaderPanel;
import com.teamcenter.rac.util.Registry;

public class ENFormHeaderPanel extends FormHeaderPanel
{
    
    public ENFormHeaderPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
        
    }
    
    
}
