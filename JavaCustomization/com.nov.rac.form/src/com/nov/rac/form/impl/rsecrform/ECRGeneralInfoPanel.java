package com.nov.rac.form.impl.rsecrform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.www.cet.webservices.V1_3.CETGServicesProxy;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.CreateIn;
import com.teamcenter.services.rac.core._2008_06.DataManagement.CreateInput;

public class ECRGeneralInfoPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private Text m_ecrNumberText;
    private Text m_ecrRequesterText;
    private Text m_ecrRequesterDeptText;
    private Text m_ecrStatusText;
    private Text m_closeDispositionText;
    private Text m_changeRespText;
    private Text m_requestDateText;
    private Text m_closeDateText;
    private Registry m_registry;

    public ECRGeneralInfoPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }

   
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_ecrNumberText.setText(PropertyMapHelper.handleNull(propMap.getString("object_name")));
        m_ecrRequesterText.setText(PropertyMapHelper.handleNull(propMap.getString("nov4_ecr_requester")));
        m_ecrRequesterDeptText.setText(PropertyMapHelper.handleNull(propMap.getString("nov4_requester_dept")));
        m_closeDispositionText.setText(PropertyMapHelper.handleNull(propMap.getString("nov4_close_disposition")));
        m_changeRespText.setText(PropertyMapHelper.handleNull(propMap.getString("nov4_change_resp")));
        
        m_ecrStatusText.setText(getReleaseStatusName("release_status_list", propMap));
        m_requestDateText.setText(getDateString("nov4_request_date", propMap));
        m_closeDateText.setText(getDateString("nov4_close_date", propMap));
        
        return false;
    }
    
    protected String getDateString(String propertyName,
            IPropertyMap propMap)
    {
        String dateString = "";
        Date theDate = propMap.getDate(propertyName);
        
        if (theDate != null)
        {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateString = dateFormat.format(theDate);
        }
        return dateString;
    }
    
    protected String getReleaseStatusName(String propertyName,
            IPropertyMap propMap) throws TCException
    {
        String statusName = "";
        TCComponent[] releaseStatuses = propMap.getTagArray(propertyName);
        
        if (releaseStatuses != null && releaseStatuses.length > 0)
        {
            statusName = releaseStatuses[0].getProperty("name");
            
        }
        return statusName;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getMainComposite();
        Composite mainComposite = createComposite(composite);
        GridLayout gl_composite = new GridLayout(3, true);
        gl_composite.horizontalSpacing = SWTUIHelper.convertHorizontalDLUsToPixels(mainComposite, 10);
        mainComposite.setLayout(gl_composite);
        createColumn1Composite(mainComposite);
        createColumn2Composite(mainComposite);
        createColumn3Composite(mainComposite);
        
        return true;
    }
    
    private void createColumn1Composite(Composite parent)
    {
        Composite column1Composite = createComposite(parent);
        m_ecrNumberText = createKeyLabelValueText(column1Composite,
                "ECRNumber.Label");
        m_ecrStatusText = createKeyLabelValueText(column1Composite,
                "ECRStatus.Label");
        m_requestDateText = createKeyLabelValueText(column1Composite, "RequestDate.Label");
        
    }
    
    private void createColumn2Composite(Composite parent)
    {
        Composite column2Composite = createComposite(parent);
        m_ecrRequesterText = createKeyLabelValueText(column2Composite,
                "Requester.Label");
        m_closeDispositionText = createKeyLabelValueText(column2Composite,
                "CloseDisp.Label");
        m_closeDateText = createKeyLabelValueText(column2Composite,
                "CloseDate.Label");
    }
    
    private void createColumn3Composite(Composite parent)
    {
        Composite column3Composite = createComposite(parent);
        m_ecrRequesterDeptText = createKeyLabelValueText(column3Composite,
                "RequesterDept.Label");
        m_changeRespText = createKeyLabelValueText(column3Composite,
                "ChangeResp.Label");
    }
    
    private Composite createComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gd_composite = new GridData(SWT.FILL, SWT.TOP, true, false, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
        
    }
    
    private Text createKeyLabelValueText(Composite composite, String setText)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1,
                1));
        label.setText(getRegistry().getString(setText));
        SWTUIHelper.setFont(label, SWT.BOLD);
        
        Text text = new Text(composite, SWT.BORDER);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        text.setBackground(getComposite().getDisplay().getSystemColor( SWT.COLOR_WHITE));
        text.setEnabled(false);
        return text;
    }
    
    protected Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, false, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, true);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
}
