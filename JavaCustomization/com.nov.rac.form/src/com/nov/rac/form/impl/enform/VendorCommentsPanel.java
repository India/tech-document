package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class VendorCommentsPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private Label m_lblVendorCommentsValue = null;
    
    private static final int VENDOR_COMMENTS_COLUMN_NO = 2;
    
    private Composite m_composite = null;
    
    public VendorCommentsPanel(Composite parent, int style)
    {
        super(parent, style);
        // TODO Auto-generated constructor stub
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        // TODO Auto-generated method stub
        m_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        
        m_composite.setLayoutData(gridData);
        m_composite.setLayout(new GridLayout(VENDOR_COMMENTS_COLUMN_NO, false));
        
        createVendorCommentsLable();
        createVendorCommentsValueLabel();
        
        return true;
    }
    
    private void createVendorCommentsValueLabel()
    {
        // TODO Auto-generated method stub
        m_lblVendorCommentsValue = new Label(m_composite, SWT.WRAP);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        m_lblVendorCommentsValue.setLayoutData(gridData);
        //SWTUIHelper.setFont(m_lblVendorCommentsValue, SWT.BOLD);  
        
    }
    
    private void createVendorCommentsLable()
    {
        // TODO Auto-generated method stub
        Label lblVendorComments = new Label(m_composite, SWT.NONE);
        lblVendorComments.setText(getRegistry().getString("VendorComments.Label"));
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        //TCComponent[] masterform = propMap.getTagArray("IMAN_master_form_rev");
        //String type = masterform[0].getProperty("object_type");
        
        String vendorComments = PropertyMapHelper.handleNull(propMap.getString("nov4_comments"));
        m_lblVendorCommentsValue.setText(vendorComments);                

        m_composite.pack(true);

        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
}
