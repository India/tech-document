/**
 * 
 */
package com.nov.rac.form.impl.eprform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.DistributionPanel;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */
public class EPRDistributionPanel extends DistributionPanel
{
    
    public EPRDistributionPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
