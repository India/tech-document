package com.nov.rac.form.impl.enform;

import java.awt.Component;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellRenderer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.AddTargetToEnSearchProvider;
import com.nov.rac.form.util.RelatedMinorRevisionSearchProvider;
import com.nov.rac.form.util.RelatedMinorRevision_TCTable;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class AddTargetToENForm
{
    private Shell m_shell = null;
    private Composite m_SWTAWT = null;
    private Composite m_minorRevisionTableComposite = null;
    private Text m_itemIDSearchText = null;
//    private NOVENTargetsPanel m_targetsPanel = null;
    private TCSession m_session = null;
    private TCTable m_itemSearchTable = null;
    private Registry m_registry = Registry.getRegistry(this);
    
    private String m_itemIDText = "";
    boolean m_isMissionGrp = false;
    private String m_sItemId = "";
//    private String m_sAltID = "";
    private Composite m_addCancelComposite = null;
    private Label m_searchStatusLabel = null;
    
    private Button m_showRelatedMinorRevButton = null;
    private RelatedMinorRevision_TCTable m_relatedMinorRevTable = null;
//    private Map<Integer, String> m_itemInfo = new HashMap<Integer, String>();
    private TCComponentTask m_enRootTask = null;
    private Vector<String> m_targetMinorRevsPuid = new Vector<String>();
    private ProgressBar m_progressBar = null;
    private TCComponent[] m_targets = null;;
    private TCComponentProcess m_currentProcess = null;
    
    public AddTargetToENForm()
    {
        m_session = (TCSession) AIFUtility.getDefaultSession();
        
        initialize_IsMissionGroup();
        initialize_CurrentProcess_EnRootTask();
        initializeTargets();
        
        createUI(Display.getCurrent());
        
    }
    
    private void initialize_IsMissionGroup()
    {
        TCPreferenceService prefServ = m_session.getPreferenceService();
        String[] missionGrps = prefServ.getStringArray(
                TCPreferenceService.TC_preference_all,
                m_registry.getString("misPref.NAME"));
        String enGroupName = FormHelper.getTargetComponentOwningGroup();
        
        for (int k = 0; k < missionGrps.length; k++)
        {
            if (missionGrps[k].equalsIgnoreCase(enGroupName))
            {
                m_isMissionGrp = true;
                return;
                
            }
        }
        
    }
    
    private void initializeTargets()
    {
        try
        {
            m_targets = m_enRootTask.getAttachments(TCAttachmentScope.LOCAL,
                    TCAttachmentType.TARGET);
        }
        catch (TCException e)
        {
            MessageBox.post(
                    "Failed to Get Targets on EnForm" + "\n"
                            + e.getDetailsMessage(), "ERROR", MessageBox.ERROR);
        }
        
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
        
    }
    
    private void createUI(Display display)
    {
        
        createShell(display);
        
        if (isValidENGroupForMinorRevisionTable())
        {
            
            new Label(m_shell, SWT.NONE);
            createShowRelatedMinorRevisionsButton();
            new Label(m_shell, SWT.NONE);
            createRelatedMinorRevisionTableLabel();
            createSelectCheckboxToAddLabel();
            createMinorRevisionTableComposite();
            JScrollPane relatedMinorRevisionScrollPane = createMinorRevisionTable();
            if (relatedMinorRevisionScrollPane != null)
            {
                SWTUIUtilities.embed(m_minorRevisionTableComposite,
                        relatedMinorRevisionScrollPane, false);
                
            }
        }
        createSearchForPartToAddLabel();
        createItemIDSearchText();
        createSearchButton();
        
        createPartSearchResultTableLabel();
        new Label(m_shell, SWT.NONE);
        new Label(m_shell, SWT.NONE);
        
        createSingleSelectionAllowedLabel();
        new Label(m_shell, SWT.NONE);
        new Label(m_shell, SWT.NONE);
        
        createSearchTableComposite();
        createSearchTable();
        createSearchStatusLabel();
        createProgressBar();
        createAddCancelComposite();
        createAddButton();
        createCancelButton();
        new Label(m_shell, SWT.NONE);
        new Label(m_shell, SWT.NONE);
        new Label(m_shell, SWT.NONE);
        m_shell.open();
        
        while (!m_shell.isDisposed())
        {
            if (!display.readAndDispatch())
            {
                display.sleep();
            }
        }
    }
    
    private void createMinorRevisionTableComposite()
    {
        m_minorRevisionTableComposite = new Composite(m_shell, SWT.EMBEDDED
                | SWT.BORDER);
        m_minorRevisionTableComposite.setLayoutData(new GridData(SWT.FILL,
                SWT.FILL, true, true, 3, 1));
    }
    
    private void createSelectCheckboxToAddLabel()
    {
        Label label = new Label(m_shell, SWT.NONE);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3,
                1));
        label.setText(getRegistry().getString(
                "AddTargetToEnForm.RelatedMinorRevisionsNoteLabel"));
        label.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        
    }
    
    private void createRelatedMinorRevisionTableLabel()
    {
        Label label = new Label(m_shell, SWT.NONE);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3,
                1));
        label.setText(getRegistry().getString(
                "AddTargetToEnForm.RelatedMinorRevisionsTableLabel"));
        FontData fontData = label.getFont().getFontData()[0];
        Font font = new Font(m_shell.getDisplay(), new FontData(
                fontData.getName(), fontData.getHeight(), SWT.BOLD));
        label.setFont(font);
        
    }
    
    //
    
    private void createShowRelatedMinorRevisionsButton()
    {
        m_showRelatedMinorRevButton = new Button(m_shell, SWT.PUSH);
        m_showRelatedMinorRevButton.setLayoutData(new GridData(SWT.FILL,
                SWT.CENTER, true, false, 1, 1));
        m_showRelatedMinorRevButton.setText(getRegistry().getString(
                "AddTargetToEnForm.RelatedMinorRevisionsButton"));
        
        m_showRelatedMinorRevButton
                .addSelectionListener(new SelectionListener()
                {
                    
                    @Override
                    public void widgetSelected(SelectionEvent selectionevent)
                    {
                        try
                        {
                            m_relatedMinorRevTable.clear();
                            m_relatedMinorRevTable.clearCheckboxHeader();
                            populateRelatedMinorRevisionTable();
                        }
                        catch (TCException e2)
                        {
                            e2.printStackTrace();
                            MessageBox.post(e2.getDetailsMessage(), "ERROR",
                                    MessageBox.ERROR);
                        }
                        
                    }
                    
                    @Override
                    public void widgetDefaultSelected(
                            SelectionEvent selectionevent)
                    {
                        // TODO Auto-generated method stub
                        
                    }
                });
        
    }
    
    private void createSearchStatusLabel()
    {
        m_searchStatusLabel = new Label(m_shell, SWT.NONE);
        m_searchStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 3, 1));
        m_searchStatusLabel.setAlignment(SWT.CENTER);
        
    }
    
    private void createCancelButton()
    {
        Button cancelButton = new Button(m_addCancelComposite, SWT.PUSH); // Add
        cancelButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
                false, 1, 1));
        cancelButton.setText(getRegistry().getString(
                "AddTargetToEnForm.CancelButton"));
        cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                m_shell.dispose();
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
            }
        });
        
    }
    
    private void createAddButton()
    {
        Button addButton = new Button(m_addCancelComposite, SWT.PUSH); // cancel
        addButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1));
        addButton.setText(getRegistry()
                .getString("AddTargetToEnForm.AddButton"));
        addButton.setToolTipText(m_registry
                .getString("AddTargetToEnForm.AddButtonToolTip"));
        addButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                performOnAddButtonSelection();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
    }
    
    private void createAddCancelComposite()
    {
        m_addCancelComposite = new Composite(m_shell, SWT.EMBEDDED | SWT.NONE);
        m_addCancelComposite.setLayout(new GridLayout(2, true));
        m_addCancelComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 3, 1));
        
    }
    
    private void createProgressBar()
    {
        m_progressBar = new ProgressBar(m_shell, SWT.NONE);
        m_progressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 3, 1));
        m_progressBar.setVisible(false);
        m_progressBar.setEnabled(false);
        
    }
    
    private void createSearchTable()
    {
        String[] colNames = null;
        colNames = getRegistry().getString(
                "AddTargetToEnForm.SearchTableColumnNames").split(",");
        TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication()
                .getSession();
        
        m_itemSearchTable = new TCTable(tcSession, colNames);
        m_itemSearchTable.setShowVerticalLines(true);
        m_itemSearchTable.setAutoCreateColumnsFromModel(false);
        m_itemSearchTable
                .setAutoResizeMode(m_itemSearchTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane scrollPane = new JScrollPane(m_itemSearchTable);
        TitledBorder titleBorder = new TitledBorder("");
        scrollPane.setBorder(titleBorder);
        
        m_itemSearchTable.setVisible(true);
        m_itemSearchTable.getTableHeader().setBorder(
                UIManager.getBorder("TableHeader.cellBorder"));
        
        String[] columnsToHide = m_registry.getString(
                "AddTargetToEnForm.ColumnsToHideInSearchTable").split(",");
        
        hideUnrequiredColumnsInTable(m_itemSearchTable, columnsToHide);
        SWTUIUtilities.embed(m_SWTAWT, scrollPane, false);
        
    }
    
    private void createSearchTableComposite()
    {
        m_SWTAWT = new Composite(m_shell, SWT.EMBEDDED | SWT.BORDER);
        m_SWTAWT.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
        
    }
    
    private void createSingleSelectionAllowedLabel()
    {
        Label singleSelectionLabel = new Label(m_shell, SWT.NONE);
        singleSelectionLabel.setForeground(SWTResourceManager
                .getColor(SWT.COLOR_RED));
        singleSelectionLabel.setText(getRegistry().getString(
                "AddTargetToEnForm.NoteOnlySingleSelectionAllowedLabel"));
        
    }
    
    private void createPartSearchResultTableLabel()
    {
        Label searchTableLabel = new Label(m_shell, SWT.NONE);
        searchTableLabel.setText(getRegistry().getString(
                "AddTargetToEnForm.PartSearchResultTableLabel"));
        FontData fontData = searchTableLabel.getFont().getFontData()[0];
        Font font = new Font(m_shell.getDisplay(), new FontData(
                fontData.getName(), fontData.getHeight(), SWT.BOLD));
        searchTableLabel.setFont(font);
        
    }
    
    public JScrollPane createMinorRevisionTable()
    {
        String[] columnNames = null;
        columnNames = m_registry.getString("columnNamesRelatedMinorRevs.LIST")
                .split(",");
        TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication()
                .getSession();
        m_relatedMinorRevTable = new RelatedMinorRevision_TCTable(tcSession,
                columnNames, m_registry);
        m_relatedMinorRevTable.setCheckboxHeaderState();
        JScrollPane treePaneRelatedMinors = new JScrollPane(
                m_relatedMinorRevTable);
        treePaneRelatedMinors
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        String[] columnsToHide = m_registry.getString(
                "AddTargetToEnForm.ColumnsToHideInRelatedMinorRevisionsTable")
                .split(",");
        hideUnrequiredColumnsInTable(m_relatedMinorRevTable, columnsToHide);
        return treePaneRelatedMinors;
    }
    
    private void hideUnrequiredColumnsInTable(TCTable table,
            String[] columnsToHide)
    {
        for (int inx = 0; inx < columnsToHide.length; inx++)
        {
            int columnIndex = table.getColumnIndex(columnsToHide[inx].trim());
            
            table.getColumnModel().getColumn(columnIndex).setMinWidth(0);
            table.getColumnModel().getColumn(columnIndex).setMaxWidth(0);
            table.getColumnModel().getColumn(columnIndex).setWidth(0);
        }
    }
    
    private void createSearchButton()
    {
        final Button searchButton = new Button(m_shell, SWT.NONE);
        Image image = new Image(m_shell.getDisplay(), getRegistry().getImage(
                "AddTargetToEnForm.SearchButtonImage"), SWT.NONE);
        searchButton.setImage(image);
        
        //
        searchButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                // Test
                
                // Test
                search_PopulateItems();
                
            };
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private void createItemIDSearchText()
    {
        m_itemIDSearchText = new Text(m_shell, SWT.BORDER);
        m_itemIDSearchText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                false, false, 1, 1));
        //
        m_itemIDSearchText.addKeyListener(new KeyListener()
        {
            
            @Override
            public void keyReleased(KeyEvent keyevent)
            {
                int keycode_Enter = 13;
                if (keyevent.keyCode == SWT.KEYPAD_CR
                        || keyevent.keyCode == keycode_Enter)
                {
                    search_PopulateItems();
                    return;
                }
                m_itemIDText = m_itemIDSearchText.getText();
                
            }
            
            @Override
            public void keyPressed(KeyEvent keyevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private void createSearchForPartToAddLabel()
    {
        Label lblNewLabel = new Label(m_shell, SWT.NONE);
        lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1));
        lblNewLabel.setText(getRegistry().getString(
                "AddTargetToEnForm.SearchForPartToAddLabel"));
        
    }
    
    private void createShell(Display d1)
    {
        float percentageOfDesktopSize = 0.6f;
        Rectangle desktopSize = Display.getDefault().getClientArea();
        
        m_shell = new Shell(d1, SWT.CLOSE | SWT.RESIZE | SWT.TITLE
                | SWT.APPLICATION_MODAL | SWT.EMBEDDED);
        m_shell.setSize((int) (percentageOfDesktopSize * (desktopSize.width)),
                (int) (percentageOfDesktopSize * (desktopSize.height))); // set according to screen
                                                     // size
        
        Rectangle shellSize = m_shell.getBounds();
        int x = desktopSize.x + (desktopSize.width - shellSize.width) / 2;
        int y = desktopSize.y + (desktopSize.height - shellSize.height) / 2;
        m_shell.setLocation(x, y);
        
        GridLayout gl_shell = new GridLayout(3, true);
        gl_shell.horizontalSpacing = 15;
        m_shell.setLayout(gl_shell);
        
    }
    
    private void populateRelatedMinorRevisionTable() throws TCException // NOVEnTargetsPanel_v2
    {
        m_targetMinorRevsPuid.clear();
        TCComponent[] minorTargets = getMinorRevsFromTargets();
        // Populate vector for filtering the search results later
        for (int index = 0; index < minorTargets.length; index++)
        {
            m_targetMinorRevsPuid.add(minorTargets[index].getUid());
        }
        search_related_minor_revisions(minorTargets);
    }
    
    // Initialize the member variables currentProcess and enRootTask
    private void initialize_CurrentProcess_EnRootTask()
    {
        try
        {
            
            com.teamcenter.rac.aif.kernel.AIFComponentContext[] contexts = com.nov.rac.form.helpers.FormHelper
                    .getTargetComponent().whereReferenced();
            TCComponent[] targetItemRevisions = null;
            if (contexts != null)
            {
                for (int j = 0; j < contexts.length; j++)
                {
                    if ((TCComponent) contexts[j].getComponent() instanceof TCComponentProcess)
                    {
                        m_currentProcess = (TCComponentProcess) contexts[j]
                                .getComponent();
                        m_enRootTask = ((TCComponentProcess) m_currentProcess)
                                .getRootTask();
                        break;
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    //
    
    private void search_related_minor_revisions(
            TCComponent[] minorTargetsAttached)
    {
        
        INOVSearchProvider searchService = new RelatedMinorRevisionSearchProvider(
                minorTargetsAttached);
        INOVSearchResult searchResult = null;
        
        try
        {
            if (searchService != null)
            {
                if ((searchService instanceof INOVSearchProvider2)
                        && (((INOVSearchProvider2) searchService)
                                .validateInput() == false))
                {
                    MessageBox.post(
                            m_registry.getString("noMinorRevisions.MSG"),
                            "Info", MessageBox.INFORMATION);
                    return;
                }
                searchResult = NOVSearchExecuteHelperUtils.execute(
                        searchService, 0);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        if (searchResult == null)
        {
            return;
        }
        
        INOVResultSet theResultSet = searchResult.getResultSet();
        
        // Populate the table with search results
        
        Vector<String> itemData = new Vector<String>();
        Vector<String> searchData = theResultSet.getRowData();
        Vector<String> objectTypes = new Vector<String>();
        
        int puid_Index = 0;
        int revID_Index = 3;
        int releaseStatus_Index = 7;
        int objectType_Index = 6;
        for (int inx = 0; inx < theResultSet.getRows(); inx++)
        {
            m_progressBar.setSelection(inx);
            
            int nCurrentRowIndex = (inx * theResultSet.getCols());
            itemData.clear();
            for (int jnx = 0; jnx < theResultSet.getCols(); jnx++)
            {
                
                itemData.add(searchData.get(nCurrentRowIndex + jnx));
            }
            
            if (!isMinorRev(itemData.get(revID_Index))
                    || itemData.get(releaseStatus_Index).equalsIgnoreCase(
                            "Released")
                    || m_targetMinorRevsPuid.contains(itemData.get(puid_Index)))
            {
                continue;
            }
            objectTypes.add(itemData.get(objectType_Index));
            itemData.add(0, "false");
            m_relatedMinorRevTable.addRow(itemData);
        }
        
        m_relatedMinorRevTable
                .getColumnModel()
                .getColumn(2)
                .setCellRenderer(new SearchResultTableCellRenderer(objectTypes)); // 2nd
                                                                                  // column
                                                                                  // is
                                                                                  // item
                                                                                  // name
        int minorTableCount = objectTypes.size();
        
        if (minorTableCount > 0)
        {
            m_relatedMinorRevTable.setCheckboxHeaderState();
            m_relatedMinorRevTable.updateUI();
        }
        else
        {
            MessageBox.post(m_registry.getString("noMinorRevisions.MSG"),
                    "Info", MessageBox.INFORMATION);
        }
    }
    
    private TCComponent[] getMinorRevsFromTargets() throws TCException
    {
        TCComponent[] minorRevTargets = new TCComponent[] {};
        Vector<TCComponent> trgtVec = new Vector<TCComponent>();
        for (int count = 0; count < m_targets.length; count++)
        {
            if (m_targets[count] instanceof TCComponentItemRevision)
            {
                String revisionID = m_targets[count]
                        .getProperty("item_revision_id");
                if (isMinorRev(revisionID))
                {
                    trgtVec.add(m_targets[count]);
                }
            }
        }
        minorRevTargets = trgtVec.toArray(new TCComponent[trgtVec.size()]);
        return minorRevTargets;
    }
    
    private boolean isMinorRev(String revID)
    {
        boolean isRevMinor = false;
        if (revID.contains("."))
        {
            String tempStr = revID.substring(revID.lastIndexOf(".") + 1,
                    revID.length());
            try
            {
                int num = Integer.parseInt(tempStr);
                // is an integer!
                isRevMinor = true;
            }
            catch (NumberFormatException e)
            {
                // not an integer!
                isRevMinor = false;
            }
        }
        return isRevMinor;
    }
    
    public void performOnAddButtonSelection()
    {
        boolean targetAlreadyExists = false;
        boolean isValidType = false;
        
        int selRowsIndex_SearchTable[] = m_itemSearchTable.getSelectedRows();
        
        // TCDECREL-5689: Get selected row indices from Related Minor Revision
        // Table
        int selRowsIndex_RelatedMinorRevTable[] = null;
        if (null != m_relatedMinorRevTable)
        {
            selRowsIndex_RelatedMinorRevTable = m_relatedMinorRevTable
                    .getSelectedRowsIndices();
        }
        // TCDECREL-5689: Warning dialog if nothing is selected from both tables
        if ((selRowsIndex_SearchTable == null || selRowsIndex_SearchTable.length == 0)
                && (selRowsIndex_RelatedMinorRevTable == null || selRowsIndex_RelatedMinorRevTable.length == 0))
        {
            MessageBox.post(m_registry.getString("noPartSelected.MSG"),
                    "Warning", MessageBox.WARNING);
            return;
        }
        if (selRowsIndex_SearchTable.length > 1)
        {
            MessageBox.post(m_registry.getString("selectOneItem.Name"),
                    "Warning", MessageBox.WARNING);
            return;
        }
        
        TCComponent selcomp = null;
        // TCDECREL-5689: Vector of Item Ids
        Vector<String> vectorOfItemIds = new Vector<String>();
        // TCDECREL-5689: Vector of TCComponent to populate selected parts
        Vector<TCComponent> targetVector = new Vector<TCComponent>();
        try
        {
            if (selRowsIndex_SearchTable.length != 0)
            {
                String selPuid = (String) (m_itemSearchTable
                        .getRowData(m_itemSearchTable.getSelectedRow()))[0];
                selcomp = m_session.stringToComponent(selPuid);
                TCComponentItem item = (TCComponentItem) selcomp;
                
                // usha
                String selObjType = selcomp.getType();
                if (selObjType
                        .startsWith(m_registry.getString("DocsItem.TYPE"))
                        || selObjType.startsWith(m_registry
                                .getString("Nov4PartItem.TYPE"))
                        || selObjType.startsWith(m_registry
                                .getString("NonEngItem.TYPE")))
                {
                    isValidType = true;
                    // TCDECREL-5689: Add part from Part/Doc Search Table to
                    // target vector
                    targetVector.add(item);
                    vectorOfItemIds.add(item.getProperty("item_id"));
                    
                }
                if (!isValidType)
                {
                    MessageBox.post(m_registry.getString("INVALIDTYPE.MSG"),
                            "Warning", MessageBox.WARNING);
                    // surajchange
                    return;
                }
                // usha
                // NOVAddENTargetSearchDialog.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                // surajchange//add wait cursor
                // shell.setCursor(shell.getDisplay().getSystemCursor(
                // SWT.CURSOR_WAIT));
            }
            
            // TCDECREL-5689: Adding selected related minor revisions to target
            // vector
            if (selRowsIndex_RelatedMinorRevTable != null)
            {
                for (int index = 0; index < selRowsIndex_RelatedMinorRevTable.length; index++)
                {
                    
                    TCComponent selRowComp = null;
                    int selRowIndex = 0;
                    
                    selRowIndex = selRowsIndex_RelatedMinorRevTable[index];
                    String selPuid = (String) (m_relatedMinorRevTable
                            .getRowData(selRowIndex))[1];
                    
                    // String selPuid = m_minorRevInfo.get(Integer
                    // .valueOf(selRowIndex));
                    selRowComp = m_session.stringToComponent(selPuid);
                    
                    TCComponentItemRevision itemRev = (TCComponentItemRevision) selRowComp;
                    // Check if Related Minor Revision Table and Part/Doc Search
                    // Table contain the same selected part
                    if (!targetVector.isEmpty())
                    {
                        if (targetVector
                                .get(0)
                                .getProperty("item_id")
                                .equalsIgnoreCase(
                                        itemRev.getProperty("item_id")))
                        {
                            continue;
                        }
                        targetVector.add(itemRev);
                        vectorOfItemIds.add(itemRev.getProperty("item_id"));
                    }
                    else
                    {
                        targetVector.add(itemRev);
                        vectorOfItemIds.add(itemRev.getProperty("item_id"));
                    }
                }
            }
            
            // TCComponent[] targets = null;
            // targets = enRootTask.getAttachments(TCAttachmentScope.LOCAL,
            // TCAttachmentType.TARGET);
            
            targetAlreadyExists = false;
            String targetCompId = null;
            for (int i = 0; i < m_targets.length; i++)
            {
                targetCompId = m_targets[i].getProperty("item_id");
                // TCDECREL-5689: Avoid adding revision of same part as target
                if (vectorOfItemIds.contains(m_targets[i].getProperty("item_id")))
                {
                    targetAlreadyExists = true;
                    break;
                }
            }
            if (targetAlreadyExists == true)
            {
                MessageBox.post(
                        targetCompId + " , "
                                + m_registry.getString("targetExists.msg"),
                        "Warning", MessageBox.WARNING);
                // NOVAddENTargetSearchDialog.this.setCursor(Cursor.getDefaultCursor());
                // shell.setCursor(shell.getDisplay().getSystemCursor(
                // SWT.CURSOR_ARROW));
                
                // surajchange//
                return;
            }
            
            if ((targetVector.size() != 0) && (targetAlreadyExists == false))
            {
                TCComponent[] trgtArr = targetVector
                        .toArray(new TCComponent[targetVector.size()]);
                addItemToTarget(trgtArr);
                m_shell.dispose();
            }
            // NOVAddENTargetSearchDialog.this.setCursor(Cursor.getDefaultCursor());
            // shell.setCursor(shell.getDisplay()
            // .getSystemCursor(SWT.CURSOR_ARROW));
            // surajchange//
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
    }
    
    private void addItemToTarget(TCComponent[] selComponents)
    {
        TCUserService userservice = m_session.getUserService();
        // Object addRemRetObj = null;
        Object addRemInputObjs[] = new Object[3];
        int addRemFlag = 1;
        addRemInputObjs[0] = (TCComponent) (m_enRootTask);
        // TCDECREL-5689: Modified to take tccomp[] as input
        addRemInputObjs[1] = selComponents;
        addRemInputObjs[2] = addRemFlag;
        
        Vector<TCComponent> addRemRetObj = null;
        try
        {
            addRemRetObj = new Vector<TCComponent>(
                    Arrays.asList((TCComponent[]) userservice.call(
                            "NATOIL_AddRemoveTargets_EN", addRemInputObjs)));
        }
        catch (TCException e)
        {
            MessageBox.post(e.getDetailsMessage(), "ERROR", MessageBox.ERROR);
        }
        
        try
        {
            AIFComponentContext[] cxtObjs = m_enRootTask.getChildren();
            for (int i = 0; i < cxtObjs.length; i++)
            {
                if (cxtObjs[i].getContext().equals(
                        m_registry.getString("PSEUDOFOLDER.NAME")))
                {
                    ((TCComponent) cxtObjs[i].getComponent()).refresh();
                }
            }
            m_enRootTask.refresh();
        }// why do this?
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        String notAdded = "";
        String added = "";
        for (int inx = 0; inx < selComponents.length; inx++)
        {
            if (!addRemRetObj.contains(selComponents[inx]))
            {
                notAdded = notAdded + "\n" + selComponents[inx].toString();
            }
        }
        
        for (TCComponent currentObj : addRemRetObj)
        {
            added = added + "\n" + currentObj.toString();
        }
        
        MessageBox.post(
                m_registry.getString("AddTargetToEnForm.AddButtonMessage")
                        + "\n\nTargets Not Added: " + notAdded
                        + "\n\nTargets Added:" + added, "Info",
                MessageBox.INFORMATION);
        
    }
    
    public boolean isValidENGroupForMinorRevisionTable()
    {
        int theScope = TCPreferenceService.TC_preference_site;
        
        String prefrenceName = m_registry
                .getString("AddTargetToEnForm.ValidENGroupForMinorRevisionTablePref");
        String[] strings = PreferenceUtils.getStringValues(theScope,
                prefrenceName);
        
        String currentGroupName = GroupUtils.getCurrentGroupName();
        
        List<String> preferences = Arrays.asList(strings);
        
        boolean isValidENGroup = preferences.contains(currentGroupName);
        System.out.println("EN Group validation:  " + isValidENGroup);
        return isValidENGroup;
    }
    
    private void search_PopulateItems()
    {
        m_itemSearchTable.removeAllRows();
        m_sItemId = m_itemIDSearchText.getText();
        try
        {
            m_searchStatusLabel.setText("");
            String itemId = m_itemIDText;
            
            if (itemId.trim().length() >= 3)
            {
                // if wild card exists in search string, remove the wild cards
                // and verify string length.
                // it should be min. 3 characters long.- Else throw error.
                char[] arr = itemId.trim().toCharArray();
                String inputId = "";
                for (int i = 0; i < arr.length; i++)
                {
                    if (arr[i] != '*')
                    {
                        inputId = inputId + arr[i];
                    }
                }
                if (inputId.length() < 3)
                {
                    MessageBox.post(
                            m_registry.getString("searchWithMinInput.MSG"),
                            "ERROR", MessageBox.ERROR);
                    return;
                }
            }
            if (itemId.trim().length() < 3)
            {
                MessageBox.post(m_registry.getString("searchWithMinInput.MSG"),
                        "ERROR", MessageBox.ERROR);
                return;
            }
            
            // INOVSearchProvider searchService = this;
            INOVSearchProvider2 searchService = new AddTargetToEnSearchProvider(
                    m_itemIDText, m_isMissionGrp);
            INOVSearchResult searchResult = null;
            INOVResultSet theResSet = null;
            try
            {
                searchResult = NOVSearchExecuteHelperUtils.execute(
                        searchService, INOVSearchProvider.LOAD_ALL);
            }
            catch (Exception e)
            {
                System.out.println("Exception in search: " + e.getMessage());
                e.printStackTrace();
            }
            
            if (searchResult != null)
            {
                theResSet = (INOVResultSet) searchResult.getResultSet();
            }
            
            if (searchResult != null && theResSet.getRows() > 0)
            {
                int revisionStatus_Index = 6;
                String revisionStatus = null;
                m_progressBar.setVisible(true);
                m_progressBar.setEnabled(true);
                m_progressBar.setMinimum(0);
                m_progressBar.setMaximum(theResSet.getRows());
                
                Vector<String> itemData = new Vector<String>();
                Vector<String> searchData = theResSet.getRowData();
                int objectType_ColumnIndex = 7;
                int rsone__itemType_ColumnIndex = 8;
                Vector<String> objectTypes = new Vector<String>();
                for (int inx = 0; inx < theResSet.getRows(); inx++)
                {
                    m_progressBar.setSelection(inx);
                    
                    int nCurrentRowIndex = (inx * theResSet.getCols());
                    itemData.clear();
                    
                    for (int jnx = 0; jnx < theResSet.getCols(); jnx++)
                    {
                        itemData.add(searchData.get(nCurrentRowIndex + jnx));
                    }
                    
                    revisionStatus = (String) itemData
                            .get(revisionStatus_Index);
                    
                    if (((revisionStatus == null))
                            || ((revisionStatus != null) && (!(revisionStatus
                                    .equalsIgnoreCase("Engineering Pre Release")))))
                    {
                        m_itemSearchTable.addRow(itemData);
                        
                        if (itemData.get(rsone__itemType_ColumnIndex) != null
                                && itemData.get(rsone__itemType_ColumnIndex)
                                        .equalsIgnoreCase(
                                                "Purchased (Inventory Item)"))
                        {
                            objectTypes.add("Purchased Part Revision");
                        }
                        else
                        {
                            objectTypes.add(itemData
                                    .get(objectType_ColumnIndex) + " Revision");
                        }
                        // objType = "Purchased Part";
                        // objTYPE[iTblRowCnt] = "Purchased Part Revision";
                        // iTblRowCnt++;
                        
                    }
                    //
                    //
                    // if( ((relStatus == null)) ||( (relStatus != null) &&
                    // (!(relStatus.equalsIgnoreCase("Engineering Pre Release"))))
                    // )
                    // {
                    //
                    // m_itemSearchTable.addRow(itemData);
                    // //itemInfo.put(objPUID, itemData);
                    // itemInfo.put(Integer.valueOf(iTblRowCnt), objPUID);
                    // if(rsOneType!= null &&
                    // rsOneType.equalsIgnoreCase("Purchased (Inventory Item)"))
                    // objType = "Purchased Part";
                    // objTYPE[iTblRowCnt] = objType + " Revision";
                    // iTblRowCnt++;
                    // }
                }
                m_itemSearchTable
                        .getColumnModel()
                        .getColumn(1)
                        .setCellRenderer(
                                new SearchResultTableCellRenderer(objectTypes));
                int foundCount = objectTypes.size();
                if (m_itemSearchTable.getRowCount() < 1)
                {
                    m_searchStatusLabel.setVisible(true);
                    m_searchStatusLabel
                            .setText(m_registry
                                    .getString("AddTargetToEnForm.SearchStatusNoItemFound"));
                }
                else
                {
                    m_searchStatusLabel.setForeground(SWTResourceManager
                            .getColor(SWT.COLOR_BLUE));
                    m_searchStatusLabel.setVisible(true);
                    m_searchStatusLabel
                            .setText(foundCount
                                    + " "
                                    + m_registry
                                            .getString("AddTargetToEnForm.SearchStatusItemsFound"));
                }
            }
            else
            {
                m_searchStatusLabel.setForeground(SWTResourceManager
                        .getColor(SWT.COLOR_RED));
                m_searchStatusLabel.setVisible(true);
                m_searchStatusLabel
                        .setText(m_registry
                                .getString("AddTargetToEnForm.SearchStatusNoItemFound"));
            }
            m_progressBar.setVisible(false);
            m_progressBar.setEnabled(false);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    class SearchResultTableCellRenderer extends JLabel implements
            TableCellRenderer
    {
        private Vector<String> m_objectTypes;
        private int m_row;
        
        public SearchResultTableCellRenderer(Vector<String> objectTypes)
        {
            super();
            m_objectTypes = objectTypes;
            
        }
        
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column)
        {
            this.setIcon(TCTypeRenderer.getTypeIcon(m_objectTypes.get(row),
                    null));
            this.setText(value.toString());
            if (isSelected)
            {
                setOpaque(true);
                this.setBackground(table.getSelectionBackground());
                this.setForeground(table.getSelectionForeground());
            }
            else
            {
                this.setBackground(null);
                this.setForeground(null);
                
            }
            
            return this;
        }
        
    }
}
