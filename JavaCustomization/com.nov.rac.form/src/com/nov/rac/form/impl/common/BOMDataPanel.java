package com.nov.rac.form.impl.common;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.renderer.TableCellColourRenderer;
import com.nov.rac.form.util.GetChildBOMLineSearchProvider;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.framework.tablecomparator.impl.ComparatorDataModel;
import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;
import com.nov.rac.framework.tablecomparator.impl.ItemComparator;
import com.nov.rac.framework.tablecomparator.impl.TablePainter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class BOMDataPanel extends AbstractUIPanel implements INOVFormLoadSave,ITableComparatorSubscriber
{

    private Registry m_registry;
    private TCTable m_table;
    private TCComponentItemRevision m_targetComponet;
    private ITableComparator m_iTableComparator = null;
    private static final String BOM_CONTEXT = "BOM";
    private static final int NUMBER_OF_COLUMN_4 = 4;
    private String[] m_tableColumnNames = null;
    private Button m_feedbackBtn;
    private Button m_editBtn;
    private Label m_helpIcon;
    //private Composite m_composite;
    
    public BOMDataPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        m_tableColumnNames = getCompareColumns();
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        composite.setLayout(new GridLayout(1, true));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        composite.setLayoutData(gridData);
        
        createTopPanel(composite);
        createTCTable(composite);
        hideColumns();
        
        
        addStatusRenderer();
        addColourRenderer(m_tableColumnNames);
        addItemIDRenderer();
        
        return true;
    }
    
    private void createTopPanel(Composite parent)
    {

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout( new GridLayout(NUMBER_OF_COLUMN_4, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gd_l_composite);
        
        // crate bill of material label
        Label billOfMaterial = new Label(composite, SWT.NONE);
        billOfMaterial.setText(getRegistry().getString("FormDispositionPanel.BillOfMaterialsLabel"));
        SWTUIHelper.setFont(billOfMaterial, SWT.BOLD);        
        GridData gridData1 = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
        billOfMaterial.setLayoutData(gridData1);
        
        // create help icon
        m_helpIcon = new Label(composite, SWT.NONE);
        GridData gridData2 = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
        m_helpIcon.setLayoutData(gridData2);
        Image helpImage = new Image(composite.getDisplay(), m_registry.getImage("Help.IMAGE"), SWT.NONE);
        m_helpIcon.setImage(helpImage);
        
        // crate feedback button
        m_feedbackBtn = new Button(composite, SWT.NONE);
        m_feedbackBtn.setText(getRegistry().getString("FeedbackButton.Label"));
        GridData gridData3 = new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1);
        m_feedbackBtn.setLayoutData(gridData3);
        
        
        // create edit button
        m_editBtn = new Button(composite, SWT.NONE);
        m_editBtn.setText(getRegistry().getString("EditButton.Label"));
        GridData gridData4 = new GridData(SWT.RIGHT, SWT.FILL, false, false, 1, 1);
        m_editBtn.setLayoutData(gridData4);
        addEditButtonListener();
    }

    private void createTCTable(Composite inComposit)
    {
        Composite comp = new Composite(inComposit, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        comp.setLayoutData(gridData);
        
        String[] columnNames = m_registry
                .getStringArray("BOMTableColumns.Names");
        String[] columnProperties = m_registry
                .getStringArray("BOMTableColumns.Properties");
        
        m_table = new TCTable(columnProperties, columnNames);
        
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_table.setEditable(false);
        m_table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        DefaultTableCellRenderer rend = (DefaultTableCellRenderer) m_table
                .getTableHeader().getDefaultRenderer();
        rend.setHorizontalAlignment(JLabel.CENTER);
        
        JPanel tablePanle = new JPanel();
        tablePanle.setLayout(new BorderLayout());
        
        ScrollPagePane scrollPane = new ScrollPagePane(m_table);
        tablePanle.add(scrollPane);
        
        SWTUIUtilities.embed(comp, tablePanle, false);
    }

    private void hideColumns()
    {
        int statusColIndex = m_table.getColumnIndex("status");
        int puidColIndex = m_table.getColumnIndex("puid");
        int objectTypeColIndex = m_table.getColumnIndex("object_type");
        int rsoneItemTypeColIndex = m_table.getColumnIndex("rsone_itemtype");
        
        TableUtils.hideTableColumn(m_table, statusColIndex);
        TableUtils.hideTableColumn(m_table, puidColIndex);
        TableUtils.hideTableColumn(m_table, objectTypeColIndex);
        TableUtils.hideTableColumn(m_table, rsoneItemTypeColIndex);
    }

    private void addItemIDRenderer()
    {
        try
        {
            String partIconRendererClassName = m_registry.getString("PartIcon.RENDERER");
            Object partIconRenderer = Instancer.newInstanceEx(partIconRendererClassName);
            
            int itemIDcolIndex = m_table.getColumnIndex("item_id");
            TableColumn itemIDcolumn = m_table.getColumnModel().getColumn(itemIDcolIndex);
    
            itemIDcolumn.setCellRenderer((TableCellRenderer)partIconRenderer);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void addStatusRenderer()
    {
        try
        {
            String releaseStatusIconRendererClassName = m_registry.getString("ReleaseStatusIcon.RENDERER");
            Object releaseStatusRenderer = Instancer.newInstanceEx(releaseStatusIconRendererClassName);
            
            int statusColIndex = m_table.getColumnIndex("releas_status_list");
            TableColumn statuscolumn = m_table.getColumnModel().getColumn(statusColIndex);
            
            statuscolumn.setCellRenderer((TableCellRenderer)releaseStatusRenderer);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void addEditButtonListener()
    {
        m_editBtn.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent paramSelectionEvent)
            {
                if(m_targetComponet != null)
                {
                    IPerspectiveDef persDef = PerspectiveDefHelper
                            .getPerspectiveDef("com.teamcenter.rac.pse.PSEPerspective");
                    InterfaceAIFComponent[] component = new InterfaceAIFComponent[] { m_targetComponet };
                    persDef.openPerspective(component);
                    
                    getComposite().getShell().dispose();
                }
            }
        });
    }

    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_table.clear();
        m_targetComponet = (TCComponentItemRevision) propMap.getComponent();
        
        INOVSearchProvider2 m_searchService = new GetChildBOMLineSearchProvider(
                m_targetComponet);
        INOVSearchResult searchResult;
        try
        {
            searchResult = NOVSearchExecuteHelperUtils.execute(m_searchService,
                    -2);
            INOVResultSet result = searchResult.getResultSet();
            
            int columnCount = result.getCols();
            int rowCount = result.getRows();
            int jumpBy = 0;
            Vector<String> resutlRowsData = result.getRowData();
            
            Vector<String> tableRowData = new Vector<>();
            for (int inx = 0; inx < rowCount; inx++)
            {
                tableRowData.clear();
                jumpBy = inx * columnCount;
                for (int jnx = 0; jnx < columnCount; jnx++)
                {
                    tableRowData.add(resutlRowsData.get(jnx + jumpBy));
                }
                m_table.addRow(tableRowData);
            }
            
            if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
            {
                initializeComparision();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return true;
    }
    
    private void addColourRenderer(String[] columnNames)
    {
        for (String columnName : columnNames)
        {
            int colIndex = m_table.getColumnIndex(columnName);
            TableColumn column = m_table.getColumnModel().getColumn(colIndex);
            
            column.setCellRenderer(new TableCellColourRenderer());
        }
    }
    
    private void initializeComparision()
    {
        IDataModel model = new ComparatorDataModel();
        model.setPrimaryKeyColumn("item_id");
        model.setTableColumns(m_tableColumnNames);
        model.setTable(m_table);
        model.setComparator(new ItemComparator());
        model.setCompareObject(m_targetComponet);
        model.setSubscriber(this);
        
        m_iTableComparator = DefaultTableComparator.getController();
        m_iTableComparator.setContext(BOM_CONTEXT, model);
        m_iTableComparator.compareTable(BOM_CONTEXT);
    }

    private String[] getCompareColumns()
    {
        String[] tableColumnNames = getRegistry().getStringArray(
                "BOMTableColumns.CompareColumns");
        return tableColumnNames;
    }
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }

    @Override
    public void updateTable(PublishEvent event)
    {
        CompareOutputBean bean = null;
        TablePainter painter = null;
        if (event != null)
        {
            bean = (CompareOutputBean) event.getNewValue();
            
            if (event.getPropertyName().equals(BOM_CONTEXT)
                    && this == bean.getObject())
            {
                painter = new TablePainter(bean);
            }
        }
        else
        {
            painter = new TablePainter(bean);
        }
        painter.paintTable(m_table);
    }

    @Override
    public void dispose()
    {
        if (m_iTableComparator != null)
        {
            m_iTableComparator.unregisterToComparator(BOM_CONTEXT);
        }
        super.dispose();
    }
    // renderer
    // class BOMCompareRenderer extends DefaultTableCellRenderer
    // {
    // protected boolean isStricked = false;
    // protected int changeDescriptionColumn = -1;
    //
    // public Component getTableCellRendererComponent(JTable table,
    // Object value, boolean isSelected, boolean hasFocus, int row,
    // int column)
    // {
    // // default background will be white
    // setBackground(Color.WHITE);
    // isStricked = false;
    //
    // changeDescriptionColumn = table.getColumnCount();
    // String cellValue = (String) table.getModel().getValueAt(row,
    // changeDescriptionColumn);
    //
    // if( cellValue.equalsIgnoreCase("Added"))
    // {
    // setBackground(Color.GREEN);
    // }
    // else if( cellValue.equalsIgnoreCase("Removed") )
    // {
    // isStricked = true;
    // }
    //
    // return super.getTableCellRendererComponent(table,
    // value, isSelected, hasFocus, row, column);
    // }
    //
    // public void paintComponent(Graphics g)
    // {
    // super.paintComponent(g);
    //
    // if(isStricked)
    // {
    // Graphics2D g2 = (Graphics2D) g;
    // g2.setColor(Color.RED);
    // int midpoint = getHeight() / 2;
    // g2.setStroke(new BasicStroke(2));
    // g2.drawLine(0, midpoint, getWidth() - 1, midpoint);
    // }
    // }
    // }
    //
    // class QuantityBOMCompareRenderer extends BOMCompareRenderer
    // {
    // public Component getTableCellRendererComponent(JTable table,
    // Object value, boolean isSelected, boolean hasFocus, int row,
    // int column)
    // {
    // // default background will be white
    // setBackground(Color.WHITE);
    // Component component = super.getTableCellRendererComponent(table,
    // value, isSelected, hasFocus, row, column);
    //
    // int changeDescColumn = table.getColumnCount();
    // String cellValue = (String) table.getModel().getValueAt(row,
    // changeDescColumn);
    //
    // if( cellValue.equalsIgnoreCase("Quantity") )
    // {
    // isStricked = true;
    // }
    //
    // return component;
    // }
    // }

}
