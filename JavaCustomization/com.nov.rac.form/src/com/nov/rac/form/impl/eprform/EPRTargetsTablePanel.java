package com.nov.rac.form.impl.eprform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.common.TargetsTablePanel;
import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.impl.common.TargetsTableTopPanelWithoutAddRemove;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */
public class EPRTargetsTablePanel extends TargetsTablePanel
{
    
    private TargetsTableTopPanelWithoutAddRemove m_topPanel;
    
    public EPRTargetsTablePanel(Composite parent, int style)
    {
        super(parent, style);
        
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        
        createTopPanel(composite);
        super.createUI();
        registerSubscriber();
        m_topPanel.addExportButtonListener(m_table);
        
        return true;
    }
    
    @Override
    protected void createTableInstance(String[] columnNames,
            String[] columnIdentifiers)
    {
        
        m_table = new EPRTable(columnIdentifiers, columnNames, getRegistry());
    }
    
    private void registerSubscriber()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        l_controller.registerSubscriber(GlobalConstant.EVENT_COMPARE_REVISION,
                getSubscriber());
        
        l_controller.registerSubscriber(GlobalConstant.EVENT_VIEW_RDD_PDF,
                getSubscriber());
        
    }
    
    private ISubscriber getSubscriber()
    {
        return this;
    }
    
    private void fireCompareEvent() throws Exception
    {
        IPropertyMap iMap = new SimplePropertyMap();
        if (isSelectedItemComparable())
        {
            TCComponent[] selectedRevisions = getSelectedItemRevisions();
            iMap.setComponent(selectedRevisions[0]);
            Shell shell = getComposite().getShell();
            PopupDialog dialog = new PopupDialog(shell);
            dialog.setInputPropertyMap(iMap);
            dialog.openCompareDialog(shell);
        }
        else
        {
            String[] errors = new String[] { getRegistry().getString(
                    "notComparableRevision.MSG") };
            TCException exception = new TCException(errors);
            throw exception;
        }
    }
    
    private void createTopPanel(Composite parent)
    {
        createTopPanelInstance(parent);
    }
    
    private void createTopPanelInstance(Composite parent)
    {
        m_topPanel = new TargetsTableTopPanelWithoutAddRemove(parent, SWT.NONE);
        try
        {
            m_topPanel.setRegistry(getRegistry());
            m_topPanel.createUI();
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        super.load(propMap);
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return super.save(propMap);
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        super.setEnabled(flag);
    }
    
    @Override
    public void dispose()
    {
        unRegisterEventSubscriber();
        super.dispose();
    }
    
    @Override
    public void update(PublishEvent event)
    {
        super.update(event);
        if (event.getPropertyName().equals(GlobalConstant.EVENT_VIEW_RDD_PDF))
        {
            viewSelectedRDDPDF();
        }
        else if (event.getPropertyName().equals(
                GlobalConstant.EVENT_COMPARE_REVISION))
        {
            try
            {
                // validate single row is selected from table
                validateSingleSelection();
                fireCompareEvent();
            }
            catch (TCException e)
            {
                MessageBox.post(e);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        
    }
    
    public void unRegisterEventSubscriber()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_COMPARE_REVISION, this);
        
        l_controller.unregisterSubscriber(GlobalConstant.EVENT_VIEW_RDD_PDF,
                this);
    }
    
}
