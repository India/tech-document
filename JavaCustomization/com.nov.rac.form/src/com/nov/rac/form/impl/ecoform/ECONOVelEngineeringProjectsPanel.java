package com.nov.rac.form.impl.ecoform;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECONOVelEngineeringProjectsPanel extends AbstractUIPanel implements
INOVFormLoadSave
{
    private Text m_nOVelEngineeringProjectsText;
    private Text m_distributionText;
    private Text m_distributionAddText;
    private Registry m_registry;
    private Composite m_subComposite;
    private Composite m_editButtonComposite;
    
    public ECONOVelEngineeringProjectsPanel(Composite parent, int style)
    {
        super(parent, SWT.NONE);
        m_registry = Registry.getRegistry(this);
       
    }
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    
    public boolean createUI()
    {
        Composite mainComposite = getComposite();
        mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1));
        mainComposite.setLayout(new GridLayout(1, true));
        
        createSubComposite(mainComposite);
        createEditButtonComposite(m_subComposite);
        
        createNOVelEngineeringProjectsLabel(m_editButtonComposite);
        createEditButton(m_editButtonComposite);
       
//        createEmptyLabel(m_subComposite,SWT.FILL, SWT.FILL, true,
//                false, 6, 1);
        
        createDistributionLabel(m_subComposite);
        
//        createEmptyLabel(m_subComposite,SWT.FILL, SWT.FILL, true,
//                false, 6, 1);
        
        createNOVelEngineeringText(m_subComposite);
        createDistributionText(m_subComposite);
        createMinusButton(m_subComposite);
        
//        createEmptyLabel(m_subComposite,SWT.FILL, SWT.FILL, true,
//                false, 1, 1);
//        
        
        createEmptyLabel(m_subComposite,SWT.FILL, SWT.FILL, true,
                false, 14, 1);
        
        createDistributionAddText(m_subComposite);
      createPlusButton(m_subComposite);
        createMailButton(m_subComposite);
        return true;
        
    }
    
    private void createEditButtonComposite(Composite composite)
    {
        m_editButtonComposite = new Composite(composite,SWT.NONE);
        GridData gd_mainComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 14,
                1);
        
        m_editButtonComposite.setLayoutData(gd_mainComposite);
        GridLayout gl_composite =  new GridLayout(2, false);
        m_editButtonComposite.setLayout(gl_composite);
        
    }
    private void createSubComposite(Composite composite)
    {
        m_subComposite = new Composite(composite,SWT.BORDER);
        GridData gd_mainComposite = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        m_subComposite.setLayoutData(gd_mainComposite);
        GridLayout gl_composite =  new GridLayout(24, true);
        gl_composite.marginRight = 0;
        m_subComposite.setLayout(gl_composite);
        
    }
    private void createEmptyLabel(Composite composite,int horizontalAlignment, int verticalAlignment, boolean grabExcessHorizontalSpace, boolean grabExcessVerticalSpace, int horizontalSpan, int verticalSpan)
    {
        Label emptyLabel = new Label(composite, SWT.NONE);
        emptyLabel.setLayoutData(new GridData(horizontalAlignment, verticalAlignment, grabExcessHorizontalSpace,
                grabExcessVerticalSpace, horizontalSpan, verticalSpan));
        
    }

    private void createMailButton(Composite composite)
    {
        
        Button globalAddressButton = new Button(composite, SWT.NONE);
        Image mailButtonImage = new Image(m_subComposite.getDisplay(),
                getRegistry().getImage("globalAddressButton.IMAGE"), SWT.NONE);
        globalAddressButton.setImage(mailButtonImage);
        globalAddressButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
                false, 1, 1));
        
    }

    private void createMinusButton(Composite composite)
    {
        Button minusButton = new Button(composite, SWT.NONE);
        Image minusButtonImage = new Image(m_subComposite.getDisplay(),
                getRegistry().getImage("removeButton.IMAGE"), SWT.NONE);
        minusButton.setImage(minusButtonImage);
        minusButton.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true,
                false, 1, 1));
        
    }

    private void createDistributionAddText(Composite composite)
    {
        m_distributionAddText = new Text(composite, SWT.BORDER);
        m_distributionAddText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
        m_distributionAddText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }

    private void createPlusButton(Composite composite)
    {
        Button plusButton = new Button(composite, SWT.NONE);
        Image plusButtonImage = new Image(m_subComposite.getDisplay(),
                getRegistry().getImage("addButton.IMAGE"), SWT.NONE);
        plusButton.setImage(plusButtonImage);
        plusButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false,
                1, 1));
        
    }

    private void createDistributionText(Composite composite)
    {
        m_distributionText = new Text(composite, SWT.BORDER);
        GridData gd_DistributionText = new GridData(SWT.FILL, SWT.FILL, true,
                false, 9, 1);
        gd_DistributionText.heightHint = ((m_distributionText.getFont()
                .getFontData())[0].getHeight()) * 10;
        m_distributionText.setLayoutData(gd_DistributionText);
        m_distributionText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
    }

    private void createNOVelEngineeringText(Composite composite)
    {
        m_nOVelEngineeringProjectsText = new Text(composite, SWT.BORDER);
        GridData gd_NOVelEngineeringProjectsText = new GridData(SWT.FILL,
                SWT.FILL, true, false, 14, 1);
        gd_NOVelEngineeringProjectsText.heightHint = ((m_nOVelEngineeringProjectsText
                .getFont().getFontData())[0].getHeight()) * 10;
        m_nOVelEngineeringProjectsText
                .setLayoutData(gd_NOVelEngineeringProjectsText);
        m_nOVelEngineeringProjectsText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }

    private void createDistributionLabel(Composite composite)
    {
        Label distributionLabel = new Label(composite, SWT.NONE);
        distributionLabel.setText(getRegistry().getString("ECONOVelEngineeringProjectsPanel.DistributionLabel"));
        distributionLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 10, 1));
        SWTUIHelper.setFont(distributionLabel, SWT.BOLD);
        
    }

    private void createEditButton(Composite composite)
    {
        Button editButton = new Button(composite, SWT.NONE);
        editButton.setText(getRegistry().getString("ECONOVelEngineeringProjectsPanel.EditButton"));
        Image editImage = new Image(composite.getDisplay(),
                getRegistry().getImage("editButton.IMAGE"), SWT.NONE);
        editButton.setImage(editImage);
        editButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1,
                1));
        
    }

    private void createNOVelEngineeringProjectsLabel(Composite composite)
    {
        Label nOVelEngineeringProjectsLabel = new Label(composite, SWT.NONE);
        nOVelEngineeringProjectsLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
                false, 1, 1));
        nOVelEngineeringProjectsLabel.setText("NOVel Engineering Projects: ");
        SWTUIHelper.setFont(nOVelEngineeringProjectsLabel, SWT.BOLD);
        
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
