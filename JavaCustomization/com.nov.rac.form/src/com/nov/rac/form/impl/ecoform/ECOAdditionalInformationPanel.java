package com.nov.rac.form.impl.ecoform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECOAdditionalInformationPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Text m_typeOFChangeText;
    private Text m_WorkFlowText;
    private Registry m_registry;
    private Composite m_mainComposite;
    private Composite m_additionalInfoComposite;
    private Composite m_titleComposite;
    private Composite m_productLineLabelAndComboComposite;
    private Composite m_groupLabelAndComboComposite;
    private Composite m_typeOfChangeLabelAndComboComposite;
    
    public ECOAdditionalInformationPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    private void createECOHelpButton(Composite composite)
    {
        Button eCOHelpButton = new Button(composite, SWT.PUSH);
        eCOHelpButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                false, 2, 1));
        eCOHelpButton.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.ECOHelpButton"));
        eCOHelpButton.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createECOClassificationCombo(Composite composite)
    {
        Combo eCOClassificationCombo = new Combo(composite, SWT.NONE);
        eCOClassificationCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 4, 1));
        eCOClassificationCombo.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createECOClassificationLabel(Composite composite)
    {
        Label eCOClassificationLabel = new Label(composite, SWT.NONE);
        eCOClassificationLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 2, 1));
        eCOClassificationLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.ECOClassificationLabel"));
        SWTUIHelper.setFont(eCOClassificationLabel, SWT.BOLD);
        
    }
    
    private void createReasonCodeCombo(Composite composite)
    {
        Combo reasonCodeCombo = new Combo(composite, SWT.NONE);
        reasonCodeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 3, 1));
        reasonCodeCombo.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createReasonCodeLabel(Composite composite)
    {
        Label reasonCodeLabel = new Label(composite, SWT.NONE);
        reasonCodeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1));
        reasonCodeLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.ReasonCodeLabel"));
        SWTUIHelper.setFont(reasonCodeLabel, SWT.BOLD);
        
    }
    
    private void createChangeCategoryCombo(Composite composite)
    {
        Combo changeCategoryCombo = new Combo(composite, SWT.NONE);
        changeCategoryCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 3, 1));
        changeCategoryCombo.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createChangeCategoryLabel(Composite composite)
    {
        Label changeCategoryLabel = new Label(composite, SWT.NONE);
        changeCategoryLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 2, 1));
        changeCategoryLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.ChangeCategoryLabel"));
        SWTUIHelper.setFont(changeCategoryLabel, SWT.BOLD);
        
    }
    
    private void createWorkFlowText(Composite composite)
    {
        m_WorkFlowText = new Text(composite, SWT.BORDER);
        m_WorkFlowText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 4, 1));
        m_WorkFlowText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createWorkFlowLabel(Composite composite)
    {
        Label workFlowLabel = new Label(composite, SWT.NONE);
        workFlowLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1));
        workFlowLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.WorkflowLabel"));
        SWTUIHelper.setFont(workFlowLabel, SWT.BOLD);
        
    }
    
    private void createTypeOfChangeText(Composite composite)
    {
        m_typeOFChangeText = new Text(composite, SWT.BORDER);
        m_typeOFChangeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 3, 1));
        m_typeOFChangeText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createTypeOfChangeLabel(Composite composite)
    {
        Label typeOfChangeLabel = new Label(composite, SWT.NONE);
        typeOfChangeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, 2, 1));
        typeOfChangeLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.TypeOfChangeLabel"));
        SWTUIHelper.setFont(typeOfChangeLabel, SWT.BOLD);
        
    }
    
    private void createGroupCombo(Composite composite)
    {
        Combo groupCombo = new Combo(composite, SWT.NONE);
        groupCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 3, 1));
        groupCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
    }
    
    private void createGroupLabel(Composite composite)
    {
        Label groupLabel = new Label(composite, SWT.NONE);
        groupLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1));
        groupLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.GroupLabel"));
        SWTUIHelper.setFont(groupLabel, SWT.BOLD);
        
    }
    
    private void createProductLineCombo(Composite composite)
    {
        Combo productLineCombo = new Combo(composite, SWT.NONE);
        productLineCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 4, 1));
        productLineCombo.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
    }
    
    private void createProductLineLabel(Composite composite)
    {
        Label productLineLabel = new Label(composite, SWT.NONE);
        productLineLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 2, 1));
        productLineLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.ProductLineLabel"));
        SWTUIHelper.setFont(productLineLabel, SWT.BOLD);
        
    }
    
    private void createDeleteChangeObjectButton(Composite composite)
    {
        Button deleteChangeObjectButton = new Button(composite, SWT.PUSH);
        deleteChangeObjectButton.setLayoutData(new GridData(SWT.LEFT,
                SWT.CENTER, true, false, 1, 1));
        deleteChangeObjectButton.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.DeleteChangeObjectButton"));
        
    }
    
    private void createTitleLabel(Composite composite)
    {
        Label titleLabel = new Label(composite, SWT.NONE);
        titleLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
                false, 1, 1));
        titleLabel.setFont(new Font(m_mainComposite.getDisplay(), (titleLabel
                .getFont().getFontData())[0].getName(), 14, SWT.BOLD));
        titleLabel.setText(getRegistry().getString(
                "ECOAdditionalInformationPanel.Title"));
        
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        m_mainComposite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        
        m_mainComposite.setLayoutData(gridData);
       // GridLayout gl_mainComposite = new GridLayout();
       // gl_mainComposite.verticalSpacing = 0;
       // gl_mainComposite.numColumns = 15;
       // gl_mainComposite.makeColumnsEqualWidth = true;
        m_mainComposite.setLayout(new GridLayout(1, true));
        
        createTitleComposite(m_mainComposite);
        createTitleLabel(m_titleComposite);
        createDeleteChangeObjectButton(m_titleComposite);
        
        // Label lbl = new Label(m_mainComposite, SWT.NONE);
        // lbl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 10,
        // 1));
        
        createAdditionalInfoComposite(m_mainComposite);
        createProductLineLabelAndComboComposite(m_additionalInfoComposite);
        
        createProductLineLabel(m_productLineLabelAndComboComposite);
        createProductLineCombo(m_productLineLabelAndComboComposite);
        createWorkFlowLabel(m_productLineLabelAndComboComposite);
        createWorkFlowText(m_productLineLabelAndComboComposite);
        createECOClassificationLabel(m_productLineLabelAndComboComposite);
        createECOClassificationCombo(m_productLineLabelAndComboComposite);
        
        createGroupLabelAndComboComposite(m_additionalInfoComposite);
        
        
        createGroupLabel(m_groupLabelAndComboComposite);
        createGroupCombo(m_groupLabelAndComboComposite);
        createChangeCategoryLabel(m_groupLabelAndComboComposite);
        createChangeCategoryCombo(m_groupLabelAndComboComposite);
        createECOHelpButton(m_groupLabelAndComboComposite);
        
        createTypeOfChangeLabelAndCombo(m_additionalInfoComposite);
        
        createTypeOfChangeLabel(m_typeOfChangeLabelAndComboComposite);
        createTypeOfChangeText(m_typeOfChangeLabelAndComboComposite);
      
       
        createReasonCodeLabel(m_typeOfChangeLabelAndComboComposite);
        createReasonCodeCombo(m_typeOfChangeLabelAndComboComposite);
        
       
       
//        m_mainComposite.pack();
        return true;// suraj
    }
    
    private void createTypeOfChangeLabelAndCombo(
            Composite composite)
    {
        m_typeOfChangeLabelAndComboComposite = new Composite(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1,
                1);
        m_typeOfChangeLabelAndComboComposite.setLayoutData(gridData);
        GridLayout gl_Composite = new GridLayout(5, true);
        gl_Composite.horizontalSpacing = 5;
        m_typeOfChangeLabelAndComboComposite.setLayout(gl_Composite);
        
    }

    private void createGroupLabelAndComboComposite(
            Composite composite)
    {
        m_groupLabelAndComboComposite = new Composite(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        m_groupLabelAndComboComposite.setLayoutData(gridData);
        GridLayout gl_Composite = new GridLayout(5, true);
        gl_Composite.horizontalSpacing = 5;
        m_groupLabelAndComboComposite.setLayout(gl_Composite);
        
    }

    private void createProductLineLabelAndComboComposite(
            Composite composite)
    {
        m_productLineLabelAndComboComposite = new Composite(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, false, 1,
                1);
        m_productLineLabelAndComboComposite.setLayoutData(gridData);
        GridLayout gl_Composite = new GridLayout(6, true);
        gl_Composite.horizontalSpacing = 5;
        m_productLineLabelAndComboComposite.setLayout(gl_Composite);
        
    }

    private void createTitleComposite(Composite composite)
    {
        m_titleComposite = new Composite(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        m_titleComposite.setLayoutData(gridData);
        m_titleComposite.setLayout(new GridLayout(2, false));
//        m_titleComposite.pack();
    }
    
    private void createAdditionalInfoComposite(Composite composite)
    {
        m_additionalInfoComposite = new Composite(composite, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        m_additionalInfoComposite.setLayoutData(gridData);
        GridLayout gl_addtionalInfoComposite = new GridLayout(3, false);
        gl_addtionalInfoComposite.horizontalSpacing = 5;
        m_additionalInfoComposite.setLayout(gl_addtionalInfoComposite);
//        m_additionalInfoComposite.pack();
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
