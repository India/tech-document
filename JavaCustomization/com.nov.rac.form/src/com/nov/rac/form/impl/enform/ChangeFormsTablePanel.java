package com.nov.rac.form.impl.enform;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

public class ChangeFormsTablePanel extends FormsTablePanel 
{
    public ChangeFormsTablePanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        super.createUI();
        this.setLabelText(getRegistry().getString("ChangeForms.Label"));
        return true;
    }
}
