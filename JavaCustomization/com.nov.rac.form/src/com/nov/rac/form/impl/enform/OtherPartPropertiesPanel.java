package com.nov.rac.form.impl.enform;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class OtherPartPropertiesPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    private static final int NO_OF_COLUMNS_PART_PROPS = 6;
    private static final int NO_OF_HORIZONTAL_SPAN_ERPDESCRIPTION = 5;
    private Registry m_registry;
    private CLabel m_lastModifiedDateCLabel;
    private CLabel m_dateReleasedCLabel;
    private CLabel m_eRPDescriptionCLabel;
    private CLabel m_dateCreatedCLabel;
    private CLabel m_ownerCLabel;
    private CLabel m_groupIDCLabel;
    
    public OtherPartPropertiesPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_ownerCLabel.setText(PropertyMapHelper.handleNull(propMap
                .getTag("owning_user")));
        m_ownerCLabel.setImage(PropertyMapHelper.getTypeImage("owning_user",
                propMap));
        
         m_groupIDCLabel.setText(PropertyMapHelper.handleNull(propMap
         .getTag("owning_group")));
         m_groupIDCLabel.setImage(PropertyMapHelper.getTypeImage("owning_group", propMap));
        
        setDate(propMap, "creation_date", m_dateCreatedCLabel);
        setDate(propMap, "date_released", m_dateReleasedCLabel);
        setDate(propMap, "last_mod_date", m_lastModifiedDateCLabel);
        return false;
    }
    
    private void setDate(IPropertyMap propMap, String datePropertyName,
            CLabel clabel)
    {
        Date theDate = propMap.getDate(datePropertyName);
        if (theDate != null)
        {
            DateFormat dateFormat = new SimpleDateFormat();
            clabel.setText(dateFormat.format(theDate));
        }
        else
        {
            clabel.setText(getRegistry().getString("NA"));
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_composite = new GridLayout(1, false);
        composite.setLayout(gl_composite);
        
        createMainComposite(composite);
        return false;
    }
    
    private void createMainComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_composite = new GridLayout(NO_OF_COLUMNS_PART_PROPS,
                false);
        composite.setLayout(gl_composite);
        createERPDescriptionField(composite);
        
        createOwnerField(composite);
        createGroupIDField(composite);
        createLastModifiedDateField(composite);
        createDateCreatedField(composite);
        createDateReleasedField(composite);
        
    }
    
    private void createERPDescriptionField(Composite parent)
    {
        Label keylbl = createKeyLabel(parent, getRegistry().getString("ERPDescription.Label"));
        SWTUIHelper.setFont(keylbl,SWT.BOLD);
        m_eRPDescriptionCLabel = createValueCLabel(parent, "");
        
        m_eRPDescriptionCLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, NO_OF_HORIZONTAL_SPAN_ERPDESCRIPTION, 1));
        
    }
    
    private void createDateCreatedField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("DateCreated.Label"));
        m_dateCreatedCLabel = createValueCLabel(parent, "");
        
    }
    
    private void createDateReleasedField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("DateReleased.Label"));
        m_dateReleasedCLabel = createValueCLabel(parent, "");
        
    }
    
    private void createLastModifiedDateField(Composite parent)
    {
        createKeyLabel(parent, getRegistry()
                .getString("LastModifiedDate.Label"));
        m_lastModifiedDateCLabel = createValueCLabel(parent, "");
    }
    
    private void createGroupIDField(Composite parent)
    {
        createKeyLabel(parent, getRegistry().getString("GroupID.Label"));
        m_groupIDCLabel = createValueCLabel(parent, "");
        m_groupIDCLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
    }
    
    private void createOwnerField(Composite parent)
    {
        
        createKeyLabel(parent, getRegistry().getString("Owner.Label"));
        m_ownerCLabel = createValueCLabel(parent, "");
        
    }
    
    private Label createKeyLabel(Composite parent, String setText)
    {
        Label label = new Label(parent, SWT.NONE);
        label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1,
                1));
        label.setText(setText);
        
        return label;
    }
    
    private CLabel createValueCLabel(Composite parent, String setText)
    {
        CLabel clabel = new CLabel(parent, SWT.NONE);
        clabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1,
                1));
        clabel.setText(setText);
        clabel.setSize(5, 5 );
        return clabel;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
