package com.nov.rac.form.impl.obsoleteform;


import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class ObsoleteTargetsTable extends TCTable
{
    private Registry m_registry;
       
    public ObsoleteTargetsTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
        
        addColumnRenderersEditors(m_registry.getString("futureStatus.PROP"));
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        TableCellEditor editor = getColumnModel().getColumn(columnIndex).getCellEditor();
        return editor.isCellEditable(null);
    }
    
    public void addColumnRenderersEditors(String columnName)
    {        
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        ObsoleteTartgetComboCellEditor cellEditorObj=new ObsoleteTartgetComboCellEditor("");
        column.setCellEditor(cellEditorObj);
        
        ObsoleteTartgetComboBoxCellEditor cellRendererObj=new ObsoleteTartgetComboBoxCellEditor("");
        column.setCellRenderer(cellRendererObj);
    }
}