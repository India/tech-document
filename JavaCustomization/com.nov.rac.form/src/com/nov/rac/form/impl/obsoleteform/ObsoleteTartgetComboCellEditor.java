package com.nov.rac.form.impl.obsoleteform;

import java.util.Arrays;
import java.util.Vector;

import com.nov.rac.form.renderer.ComboCellEditor;

public class ObsoleteTartgetComboCellEditor  extends ComboCellEditor
{
    
    private static final long serialVersionUID = 1L;
    private static String[] listOfValues = { "", "Obsolete", "Replacement" };
    //private static String m_emptyString = "";
    
    public ObsoleteTartgetComboCellEditor(String theLOVName)
    {
        super(theLOVName);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected Vector<String> loadLOV()
    {
        Vector<String> lovValues = new Vector<String>(Arrays.asList(listOfValues));
        return lovValues;
    }
    


}
