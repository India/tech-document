/**
 * 
 */
package com.nov.rac.form.impl.eprform;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.ProjectPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */
public class EPRProjectsPanel extends ProjectPanel
{
    
    private static final int WIDTH_HINT_SPACE = 100;
    
    public EPRProjectsPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
    @Override
    protected void createSearchTextField(Composite composite)
    {
        // TODO Auto-generated method stub
        super.createSearchTextField(composite);
        
        GridData gridData = new GridData();
        gridData.horizontalAlignment = GridData.FILL;
        gridData.horizontalSpan = 1;
        gridData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(
                composite, WIDTH_HINT_SPACE);
        
        m_searchText.setLayoutData(gridData);
    }
    
}
