package com.nov.rac.form.impl.obsoleteform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TargetsTableTopPanel;
import com.teamcenter.rac.util.Registry;

public class ObsoleteTargetsTableTopPanel extends TargetsTableTopPanel {

	public ObsoleteTargetsTableTopPanel(Composite parent, int style) {
		super(parent, style);
		super.setRegistry(Registry.getRegistry("com.nov.rac.form.impl.obsoleteform.obsoleteform"));
		
	}
	protected void createTargetsTableTopRow(Composite l_composite) 
	{
		super.createTargetsTableTopRow(getComposite());
		
	}
	public  org.eclipse.swt.widgets.Button getShowTargetsButton()
	{
		return m_showTargetsTableButton;
	}
	
	 
}
