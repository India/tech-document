package com.nov.rac.form.impl.mbcform;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class MBCAffectedAsmTablePanel extends AbstractUIPanel implements INOVFormLoadSave
{
    private Registry m_registry = null;
    private TCTable m_table;
    private Button m_exportExcelButton;
    private final String m_panelName = getClass().getSimpleName();
    private String m_emptyString = new String();
    private static String COLUMN_NAMES = "COLUMN_NAMES";
    private static String DELIMITER = ",";
    private static final int NUMBER_OF_COLUMN_3 = 3;
    private static final int VERTICAL_SPAN = 35;
    
    public MBCAffectedAsmTablePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        
        createTargetTopPanel(composite);
        
        createTCTable(composite);
        
        createDataModelMapping();
        addListeners();
        
        return true;
    }
    
    private void createTargetTopPanel(Composite l_composite)
    {
        Composite composite = new Composite(l_composite, SWT.NONE);
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gd_panel);
        
        composite.setLayout(new GridLayout(NUMBER_OF_COLUMN_3, false));
        
        Button showTargetsTableButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        Image image = getRegistry().getImage("TargetsTable.ShowTargetsTable.IMAGE");
        showTargetsTableButton.setImage(image);
        showTargetsTableButton.setLayoutData(gridData);
        showTargetsTableButton.setEnabled(false);
        
        Label l_lblTargetsTableName = new Label(composite, SWT.NONE);
        l_lblTargetsTableName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_lblTargetsTableName.setText(m_registry.getString("lblTargetsTableName.NAME", "Key Not Found"));
        SWTUIHelper.setFont(l_lblTargetsTableName, SWT.BOLD);
        
        createExportExcelButton(composite);
    }
    
    private void createExportExcelButton(Composite composite)
    {
        m_exportExcelButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1);
        m_exportExcelButton.setText(m_registry.getString("exportExcelButton.NAME", "Key Not Found"));
        
        m_exportExcelButton.setLayoutData(gridData);
    }
    
    private void createTCTable(Composite composite)
    {
        Composite comp = new Composite(composite, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, VERTICAL_SPAN);
        comp.setLayoutData(gridData);
        m_table = new TCTable(getTableColumnIdentifiers(), getTableColumns());
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_table.setSortEnabled(false);
        
        int colIndexItemID = m_table.getColumnIndex(getRegistry().getString("affectedAssebliesItemId.PROP"));
        TableColumn column = m_table.getColumnModel().getColumn(colIndexItemID);
        column.setCellRenderer(new PartIDIconRenderer());
        
        // hide columns
        String[] hideColumnNames = getRegistry().getStringArray(m_panelName + "." + "HIDE_COLUMNS");
        int colIndex = -1;
        for (String colName : hideColumnNames)
        {
            colIndex = m_table.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_table, colIndex);
        }
        
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        
        ScrollPagePane scrollPane = new ScrollPagePane(m_table);
        tablePanel.add(scrollPane);
        
        SWTUIUtilities.embed(comp, tablePanel, false);
    }
    
    protected String[] getTableColumns()
    {
        String[] m_tableColumnNames = getRegistry().getStringArray(m_panelName + "." + COLUMN_NAMES, DELIMITER,
                m_emptyString);
        return m_tableColumnNames;
    }
    
    protected String[] getTableColumnIdentifiers()
    {
        String[] tableColumnIdentifiers = getRegistry().getStringArray(m_panelName + "." + "COLUMN_IDENTIFIERS",
                DELIMITER, m_emptyString);
        return tableColumnIdentifiers;
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        
        dataBindingHelper.bindData(m_table, getRegistry().getString("targetDisposition.PROP"),
                FormHelper.getDataModelName());
    }
    
    protected void addListeners()
    {
        m_exportExcelButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                //NOVExportSearchResultUtils.exportSearchResultToExcel(m_table);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] affectedAsmDataAry = propMap.getCompoundArray(
                m_registry.getString("affectedAssemblies.PROP"));
        IPropertyMap[] affectedAsmItemRevAry = propMap.getCompoundArray(
                "AffectedAssembliesItemRevision");
        
        // 1. Create local IPropertyMap array
        IPropertyMap[] localPropMapAry = new SimplePropertyMap[affectedAsmItemRevAry.length];
        
        // 2. Loop through
        for (int inx = 0; inx < affectedAsmItemRevAry.length; inx++)
        {
            // 2.1 create propmap and fill all properties of itemRev into it
            IPropertyMap localPropMap = new SimplePropertyMap(affectedAsmItemRevAry[inx]);
            
            // 2.2 find corresponding disposition propmap
            String itemID = affectedAsmItemRevAry[inx].getString("item_id");
            IPropertyMap affectedAsmPropMap = findTablePropMap(affectedAsmDataAry, itemID);
            
            // 2.3 add affected assemblies properties to local propMap
            localPropMap.addAll(affectedAsmPropMap);
            
            // 2.4 add to array
            localPropMapAry[inx] = localPropMap;
        }
        TableUtils.populateTable(localPropMapAry, m_table);
        return true;
    }
    
    private IPropertyMap findTablePropMap(IPropertyMap[] dispositionsAry, String itemID)
    {
        IPropertyMap dispPropMap = null;
        
        for (IPropertyMap propMap : dispositionsAry)
        {
            if (propMap.getString("nov4_item_id").equalsIgnoreCase(itemID))
            {
                dispPropMap = propMap;
                break;
            }
        }
        
        return dispPropMap;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
    }
    
}
