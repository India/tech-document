package com.nov.rac.form.impl.cancelstopform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.stopform.StopFormTargetsTablePanel;
import com.teamcenter.rac.util.Registry;

public class CancelStopTargetTablePanel extends StopFormTargetsTablePanel
{
    public CancelStopTargetTablePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        setRegistry(m_registry);
    }
    
    private Registry m_registry ;
}
