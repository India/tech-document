package com.nov.rac.form.impl.supersedeform;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class SupersedeTargetTable extends TCTable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Registry m_registry;
    String[] m_editableColumns = null;
    
    
    public SupersedeTargetTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
        
        addColumnRenderersEditors(m_registry.getString("futureStatus.PROP"));
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        TableCellEditor editor = getColumnModel().getColumn(columnIndex).getCellEditor();
        return editor.isCellEditable(null);
    }
    
    public void addColumnRenderersEditors(String columnName)
    {        
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        SupersedeTartgetComboCellEditor cellEditorObj = new SupersedeTartgetComboCellEditor();
        column.setCellEditor(cellEditorObj);
        
        SupersedeTargetComboCellRenderer cellRendererObj = new SupersedeTargetComboCellRenderer();
        column.setCellRenderer(cellRendererObj);
    }
    
}
