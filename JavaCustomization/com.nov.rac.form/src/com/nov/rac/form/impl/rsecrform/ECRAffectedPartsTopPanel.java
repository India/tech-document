package com.nov.rac.form.impl.rsecrform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.impl.common.TargetsTableTopPanel;
import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ECRAffectedPartsTopPanel extends AbstractUIPanel
{
    
    private Registry m_registry;
    private Button m_exportExcelButton;

    public ECRAffectedPartsTopPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        l_composite.setLayout(new GridLayout(4, false));
        
        GridData gd_panel = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
        l_composite.setLayoutData(gd_panel);
        
        createTargetsTableTopRow(l_composite);
        
        return true;
    }

    private void createTargetsTableTopRow(Composite l_composite) 
    {
        Label l_lblTargetsTableName = new Label(l_composite, SWT.NONE);
        l_lblTargetsTableName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        l_lblTargetsTableName.setText(m_registry.getString("lblTargetsTableName.NAME",
                "Key Not Found"));
        SWTUIHelper.setFont(l_lblTargetsTableName, SWT.BOLD);
        createExportExcelButton(l_composite);
        
    }

    private void createExportExcelButton(Composite composite)
    {   
        m_exportExcelButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1);
        m_exportExcelButton.setText(m_registry.getString("exportExcelButton.NAME",
                "Key Not Found"));
        
        m_exportExcelButton.setLayoutData(gridData);
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
        
}
