package com.nov.rac.form.impl.supersedeform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.TopPanel;
import com.teamcenter.rac.util.Registry;

public class SupersedeTopPanel extends TopPanel
{
    public SupersedeTopPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
}
