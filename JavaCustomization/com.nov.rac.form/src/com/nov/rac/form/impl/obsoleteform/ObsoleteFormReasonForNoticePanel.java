package com.nov.rac.form.impl.obsoleteform;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.impl.common.LabelTextFieldMandatoryValidator;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;

import com.nov.rac.utilities.databinding.DataBindingHelper;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ObsoleteFormReasonForNoticePanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{

    private Text m_reasonForNoticeTextField = null;
    private Text m_changeDescriptionTextField = null;
    private Registry m_registry = null;
    private static final int NUMBER_OF_COLUMN = 21;
    private static final int NUMBER_OF_COLUMN_FOUR= 4;
    private static final int NUMBER_OF_COLUMN_SIX= 6;
    private static final int NUMBER_OF_COLUMN_TEN= 10;
    private static final int NUMBER_OF_COLUMN_THREE= 3;
    Label m_lblReasonForNotice=null;
    
    public ObsoleteFormReasonForNoticePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
   
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_reasonForNoticeTextField.setText(PropertyMapHelper.handleNull(propMap
                .getString("nov4_reason_notice")));
        m_changeDescriptionTextField.setText(PropertyMapHelper
                .handleNull(propMap.getString("nov4_change_desc")));        
        
        return true;
        
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString("nov4_reason_notice",
                m_reasonForNoticeTextField.getText());
        propMap.setString("nov4_change_desc",
                m_changeDescriptionTextField.getText());
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
    	return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_reasonForNoticeTextField.setEditable(flag);
        m_changeDescriptionTextField.setEditable(flag);
       
    }
    
    @Override
    public boolean createUI() throws TCException
    {   
    	
        final Composite composite = getComposite();
    	composite.setLayout(new GridLayout(NUMBER_OF_COLUMN, true));
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        composite.setLayoutData(gridData);
       
        getExpandableComposite().setText(m_registry.getString("reasonForNoticePanel.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        createReasonForNoticePanel(composite);
       
        createVerticalSeparator(composite);
        
        createOrcleDescriptionPanel(composite);
       
    	createDataModelMapping();
    	
        return true;
    }
    
    private void createReasonForNoticePanel(Composite composite) 
	{
		Composite l_composite = new Composite(composite, SWT.NONE);
        l_composite.setLayout(new GridLayout(NUMBER_OF_COLUMN_FOUR, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
        		NUMBER_OF_COLUMN_TEN, NUMBER_OF_COLUMN_SIX);
        l_composite.setLayoutData(gd_l_composite);
        
        createReasonForNoticeRow(l_composite);
        
        createReasonForNoticeTextFieldRow(l_composite);
		
	}
    private void createReasonForNoticeRow(Composite l_composite) 
	{
		
	    m_lblReasonForNotice = new Label(l_composite, SWT.NONE);
		m_lblReasonForNotice.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, NUMBER_OF_COLUMN_FOUR, 1));
		m_lblReasonForNotice.setText(m_registry.getString("reasonForNotice.Label"));
		SWTUIHelper.setFont(m_lblReasonForNotice,  SWT.BOLD);
		

		
	}
    private void createReasonForNoticeTextFieldRow(Composite l_composite) 
	{
		
    	m_reasonForNoticeTextField = new Text(l_composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, NUMBER_OF_COLUMN_FOUR, NUMBER_OF_COLUMN_THREE);
        m_reasonForNoticeTextField.setLayoutData(gridData);
        m_reasonForNoticeTextField.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        
        LabelTextFieldMandatoryValidator m_lableFieldValidator = new LabelTextFieldMandatoryValidator(m_reasonForNoticeTextField,m_lblReasonForNotice);
        UIHelper.makeMandatory(m_lblReasonForNotice, m_lableFieldValidator);

        //UIHelper.makeMandatory(m_reasonForNoticeTextField);
        
        
	}
    private void createOrcleDescriptionPanel(Composite composite) 
	{
		
		Composite l_composite = new Composite(composite, SWT.NONE);
        l_composite.setLayout(new GridLayout(NUMBER_OF_COLUMN_FOUR, false));
        GridData gd_l_composite = new GridData(SWT.FILL, SWT.FILL, true, true,
        		NUMBER_OF_COLUMN_TEN, NUMBER_OF_COLUMN_SIX);
        l_composite.setLayoutData(gd_l_composite);
        
        createOracleDescriptionRow(l_composite);
        
        createOracleDescriptionTextFieldRow(l_composite);
		
	}
    private void createOracleDescriptionRow(Composite composite)
    {
    	Label l_lblOracleDescription = new Label(composite, SWT.NONE);
    	l_lblOracleDescription.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, NUMBER_OF_COLUMN_FOUR, 1));
    	l_lblOracleDescription.setText(m_registry.getString("oracleLongDescription.Label"));
    	SWTUIHelper.setFont(l_lblOracleDescription,  SWT.BOLD);
    }
    
   private void createOracleDescriptionTextFieldRow(Composite composite)
   {
	   m_changeDescriptionTextField = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
       GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, NUMBER_OF_COLUMN_FOUR, NUMBER_OF_COLUMN_THREE);
       m_changeDescriptionTextField.setLayoutData(gridData);
       m_changeDescriptionTextField.setBackground(SWTResourceManager
               .getColor(SWT.COLOR_WHITE));
   }
   
    private void createVerticalSeparator(Composite composite)
    {
        Label verticalSeparator = new Label(composite, SWT.SEPARATOR
                | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, false, false, 1,
        		NUMBER_OF_COLUMN_THREE);
        verticalSeparator.setLayoutData(layoutData);
    }
   
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_reasonForNoticeTextField,
                "nov4_reason_notice", FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_changeDescriptionTextField,
                "nov4_change_desc", FormHelper.getDataModelName());
       
    }
}
