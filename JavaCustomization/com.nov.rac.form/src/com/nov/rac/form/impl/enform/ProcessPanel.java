package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.FeedBackButtonComposite;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ProcessPanel extends AbstractUIPanel implements INOVFormLoadSave
{
    
    private static final int COLUMNS_COMPOSITE = 3;
    private static final int COLUMNS_COMPOSITE_HEADER = 4;
    private Registry m_registry;
    private CLabel m_processTemplateName;
    private CLabel m_owner;
    private CLabel m_dateCreated;
    private CLabel m_process;
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    public ProcessPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        try
        {
            
            m_process.setText(PropertyMapHelper.handleNull(propMap.getString("object_name")));
            m_owner.setText(PropertyMapHelper.handleNull(propMap.getTag("owning_user").toString()));
            m_owner.setImage(PropertyMapHelper.getTypeImage("owning_user",
                    propMap));
            m_processTemplateName.setText(PropertyMapHelper.handleNull(propMap.getTag("process_template")));
            m_dateCreated.setText(PropertyMapHelper.handleNull(propMap.getDate("creation_date")));
            
            
            
            
            
            
            
           
//            TCComponentProcess process = propMap.getComponent().getCurrentJob();
//            TCComponentTask rootTask = (TCComponentTask) process
//                    .getReferenceProperty("root_task");
//            TCComponent[] childTasks = rootTask
//                    .getReferenceListProperty("child_tasks");
//            TCComponent[] drafterChildren = childTasks[1]
//                    .getReferenceListProperty("child_tasks");
//            TCProperty[] allTCProperties = childTasks[1].getAllTCProperties();
//            
//            
//            TCComponentProfile performSignoffProfiles=(TCComponentProfile) drafterChildren[1].getReferenceListProperty("signoff_profiles")[0];
//            performSignoffProfiles.getAllTCProperties();
//            TCComponentSignoff drafterSignoff = (TCComponentSignoff) drafterChildren[1].getReferenceListProperty("signoff_attachments")[0];
//            drafterSignoff.getProperties();
////            performSignoffAttachments.getProperties(new String[]{"comments"});
//            System.out.println("asdf");
//            
//            
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getMainComposite();
        createHeaderComposite(mainComposite);
        createProcessTemplateNameComposite(mainComposite);
        return false;
    }
    
    private void createProcessTemplateNameComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(COLUMNS_COMPOSITE_HEADER, false);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
       m_processTemplateName= createLabelLabelComposite(composite,
                getRegistry().getString("ProcessTemplateName.Label"));
        m_owner =createLabelLabelComposite(composite,
                getRegistry().getString("Owner.Label"));
       m_dateCreated = createLabelLabelComposite(composite,
                getRegistry().getString("DateCreated.Label"));
    }
    
    private void createHeaderComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(4, false);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        
       CLabel processKeyLabel = new CLabel(composite, SWT.NONE);
       processKeyLabel.setText(getRegistry().getString("PROCESS.Label"));
       processKeyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
               1, 1));
       SWTUIHelper.setFont(processKeyLabel, SWT.BOLD);
       processKeyLabel.setImage(new Image(null,TCTypeRenderer.getTypeImage("EPMJob", null),SWT.NONE));       
       m_process= new CLabel(composite, SWT.NONE);
       m_process.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
               false, 1, 1));
      
       createFeedBackButton(composite);
       
       createWorkflowViewerButton(composite);
        
    }
    
    private void createWorkflowViewerButton(Composite parent)
    {
        Button printButton = new Button(parent, SWT.NONE);
        printButton.setText(getRegistry().getString("WORKFLOWVIEWER.Button"));
    }
    
    private void createFeedBackButton(Composite parent)
    {
        /*Button dataWareHouseButton = new Button(parent, SWT.NONE);
        dataWareHouseButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
                true, false, 1, 1));
        dataWareHouseButton.setText(getRegistry().getString("FEEDBACK.Button"));*/
        new FeedBackButtonComposite(parent, SWT.NONE);
    }
    
    private CLabel createLabelLabelComposite(Composite parent,
            String keyLabelText)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        GridLayout gl_Composite = new GridLayout(2, false);
        gl_Composite.marginWidth = 0;
        gl_Composite.marginHeight = 0;
        composite.setLayout(gl_Composite);
        CLabel keyLabel = new CLabel(composite, SWT.NONE);
        keyLabel.setText(keyLabelText);
        keyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                1, 1));
        
        CLabel valueLabel = new CLabel(composite, SWT.NONE);
        valueLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        return valueLabel;
        
    }
    
    private Composite getMainComposite()
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
        
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
