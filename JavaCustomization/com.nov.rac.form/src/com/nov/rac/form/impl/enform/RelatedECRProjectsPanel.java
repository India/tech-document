package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class RelatedECRProjectsPanel extends AbstractExpandableUIPanel
        implements INOVFormLoadSave
{
    
    private Registry m_registry;
    private RelatedECRPanel m_relatedECRPanel;
    private ProjectPanel m_projectPanel;
    
    public RelatedECRProjectsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_relatedECRPanel.load(propMap);
        m_projectPanel.load(propMap);
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_relatedECRPanel.save(propMap);
        m_projectPanel.save(propMap);
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        m_relatedECRPanel.validate(propMap);
        m_projectPanel.validate(propMap);
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_relatedECRPanel.setEnabled(flag);
        m_projectPanel.setEnabled(flag);
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(2, true);
        gl_l_composite.horizontalSpacing = 20;
        l_composite.setLayout(gl_l_composite);
        getExpandableComposite().setText(
                getRegistry().getString("RelatedECRProjects"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        createRelatedECRPanel(l_composite);
        createProjectPanel(l_composite);
        
        createDataModelMapping();
        return true;
    }
    
    
    private void createProjectPanel(Composite composite) throws TCException
    {
        m_projectPanel = new ProjectPanel(composite, SWT.NONE);
        m_projectPanel.createUI();
//        addSelectionListenerToList(m_projectPanel.getDualList().getTargetList());
        
    }
    
    private void createRelatedECRPanel(Composite composite) throws TCException
    {
        m_relatedECRPanel = new RelatedECRPanel(composite, SWT.NONE);
        m_relatedECRPanel.createUI();
//        addSelectionListenerToList(m_relatedECRPanel.getDualList()
//                .getTargetList());
    }
    
    protected void createDataModelMapping()
    {
       
       /* DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_hackText, "test",
                FormHelper.getDataModelName());*/
    }
    
//    private void addSelectionListenerToList(List list)
//    {
//        list.addSelectionListener(new SelectionListener()
//        {
//            
//            @Override
//            public void widgetSelected(SelectionEvent paramSelectionEvent)
//            {
//                m_hackText.setText(m_hackText.getText() + "a");// can try
//                                                               // addition of
//                                                               // small nos
//            }
//            
//            @Override
//            public void widgetDefaultSelected(SelectionEvent paramSelectionEvent)
//            {
//                // TODO Auto-generated method stub
//                
//            }
//        });
//    }
    
    @Override
    public boolean reload() throws TCException
    {
        m_relatedECRPanel.reload();
        m_projectPanel.reload();
        return false;
    }
    
}
