package com.nov.rac.form.impl.rsecrform;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class RelatedENPanel extends AbstractExpandableUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry = null;
    private TCTable m_relatedENTable = null;
    
    public RelatedENPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        List<TCComponent> relatedENList = new ArrayList<TCComponent>();
        TCComponentForm form = (TCComponentForm) FormHelper
                .getTargetComponent();
        
        String[] relation = new String[] { getRegistry().getString(
                "ECNECR.RELATION") };
        AIFComponentContext[] relatedENs = form.whereReferencedByTypeRelation(
                null, relation);
        
        for (AIFComponentContext object : relatedENs)
        {
            relatedENList.add((TCComponent) object.getComponent());
        }
        
        addRowsToTable(relatedENList);
        
        return false;
    }
    
    private void addRowsToTable(List<TCComponent> relatedENsList)
    {
        
        try
        {
            if (!relatedENsList.isEmpty())
            {
                TCComponentType.getPropertiesSet(relatedENsList, new String[] {
                        "object_name", "release_status_list" });
                
                for (TCComponent relatedEN : relatedENsList)
                {
                    Object[] columnValues = new Object[2];
                    
                    columnValues[0] = relatedEN.getProperty("object_name");
                    
                    TCComponent[] releaseStatus = relatedEN
                            .getReferenceListProperty("release_status_list");
                    
                    if (releaseStatus.length != 0)
                    {
                        
                        TCComponent LastStatus = releaseStatus[releaseStatus.length - 1];
                        String lastReleaseStatus = LastStatus
                                .getProperty("object_name");
                        
                        if (lastReleaseStatus.equalsIgnoreCase("Released"))
                        {
                            columnValues[1] = getRegistry().getString(
                                    "Complete.stage");
                            
                        }
                    }
                    else if (relatedEN.getTCProperty("process_stage_list") != null)
                    {
                        columnValues[1] = getRegistry().getString(
                                "InProgress.stage");
                    }
                    
                    m_relatedENTable.dataModel.addRows(columnValues);
                    
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        
        Composite mainComposite = getMainComposite();
        getExpandableComposite().setText(
                getRegistry().getString("RelatedEnPanel.RelatedEns"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        createRelatedENTable(mainComposite);
        return true;
    }
    
    private Composite getMainComposite()
    {
        Composite composite = getComposite();
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gd_composite);
        GridLayout gl_composite = new GridLayout(1, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        return composite;
    }
    
    private void createRelatedENTable(Composite parent)
    {
        
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                false, 1, 25);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        
        m_relatedENTable = createTCTable();
        
        JScrollPane scrollPane = new JScrollPane(m_relatedENTable);
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    
    private TCTable createTCTable()
    {
        String[] propertyNames = getRegistry().getStringArray(
                "RelatedENPanel.ColumnIdentifiers");
        String[] colNames = getRegistry().getStringArray(
                "RelatedENPanel.ColumnNames");
        
        TCTable tCTable = new TCTable(propertyNames, colNames);
        tCTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        tCTable.setEditable(false);
        tCTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        DefaultTableCellRenderer rend = (DefaultTableCellRenderer) tCTable
                .getTableHeader().getDefaultRenderer();
        rend.setHorizontalAlignment(JLabel.CENTER);
        
        JPanel tablePanle = new JPanel();
        tablePanle.setLayout(new BorderLayout());
        
        return tCTable;
    }
    
    protected void setRegistry(Registry registry)
    {
        m_registry = registry;
        
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
}
