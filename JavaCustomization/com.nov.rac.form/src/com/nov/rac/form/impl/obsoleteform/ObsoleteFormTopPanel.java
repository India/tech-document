package com.nov.rac.form.impl.obsoleteform;

import org.eclipse.swt.widgets.Composite;
import com.nov.rac.form.impl.common.TopPanel;
import com.teamcenter.rac.util.Registry;

public class ObsoleteFormTopPanel extends TopPanel {

	public ObsoleteFormTopPanel(Composite parent, int style) {
		super(parent, style);
		super.setRegistry(Registry.getRegistry("com.nov.rac.form.impl.obsoleteform.obsoleteform"));
		
	}

}
