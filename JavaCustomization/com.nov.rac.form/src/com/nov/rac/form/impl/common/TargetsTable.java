package com.nov.rac.form.impl.common;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class TargetsTable extends TCTable 
{
	
	private Registry m_registry;
  
    public TargetsTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
    }

}
