package com.nov.rac.form.impl.obsoleteform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.AttachmentsPanel;
import com.teamcenter.rac.util.Registry;

public class ObsoleteFormAttachmentsPanel extends AttachmentsPanel
{

	public ObsoleteFormAttachmentsPanel(Composite parent, int style)
	{
		super(parent, style);
		setRegistry(Registry.getRegistry("com.nov.rac.form.impl.obsoleteform.obsoleteform"));
	}

}
