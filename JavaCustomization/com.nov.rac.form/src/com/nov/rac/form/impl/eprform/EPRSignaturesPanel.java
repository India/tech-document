package com.nov.rac.form.impl.eprform;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.impl.common.Signatures;
import com.teamcenter.rac.util.Registry;

/**
 * @author sachinkab
 * 
 */
public class EPRSignaturesPanel extends Signatures
{
    
    public EPRSignaturesPanel(Composite parent, int style)
    {
        super(parent, style);
        Registry registry = Registry.getRegistry(this);
        setRegistry(registry);
    }
    
}
