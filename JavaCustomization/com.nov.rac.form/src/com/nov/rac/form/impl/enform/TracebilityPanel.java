package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class TracebilityPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private Registry m_registry;
    private Button m_lotControllerButton;
    private Button m_serializedButton;
    private Button m_nAButton;
    private static final int NO_OF_COLUMNS_MAINCOMPOSITE = 4;
    public TracebilityPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(NO_OF_COLUMNS_MAINCOMPOSITE, false));
        
        createTraceabilityLabel(l_composite);
        
        m_lotControllerButton = createCheckBox(l_composite, getRegistry()
                .getString("LotController.Button"));
        
        m_serializedButton = createCheckBox(l_composite, getRegistry()
                .getString("Serialized.Button"));
        
        m_nAButton = createCheckBox(l_composite,
                getRegistry().getString("NA.Button"));
        
        return true;
    }
    
    
    private Button createCheckBox(final Composite l_composite, String setText)
    {
        Button button = new Button(l_composite, SWT.CHECK);
        button.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                1, 1));
        button.setText(setText);
        button.setEnabled(false);
        return button;
    }
    
    private void createTraceabilityLabel(final Composite l_composite)
    {
        Label traceabilityLabel = new Label(l_composite, SWT.NONE);
        traceabilityLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
                false, false, 1, 1));
        traceabilityLabel
                .setText(getRegistry().getString("Traceability.Label"));
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        TCComponentItem item = (TCComponentItem) propMap.getTag("items_tag");
        TCComponent[] masterForms = item
                .getRelatedComponents("IMAN_master_form");
        TCComponentForm masterForm = (TCComponentForm) masterForms[0];
        
        try
        {
            boolean lotControlBool = masterForm
                    .getLogicalProperty("nov4rsone_lotcontrol");
            boolean serializedBool = masterForm
                    .getLogicalProperty("nov4rsone_serialize");
            boolean nABool = masterForm
                    .getLogicalProperty("nov4rsone_qarequired");
            
            m_lotControllerButton.setSelection(lotControlBool);
            m_serializedButton.setSelection(serializedBool);
            m_nAButton.setSelection(nABool);
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
   
}
