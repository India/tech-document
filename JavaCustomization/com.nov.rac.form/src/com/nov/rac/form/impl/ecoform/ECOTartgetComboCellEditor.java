package com.nov.rac.form.impl.ecoform;

import java.util.Arrays;
import java.util.Vector;

import com.nov.rac.form.renderer.ComboCellEditor;

public class ECOTartgetComboCellEditor extends ComboCellEditor
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static String[] listOfValues = { "", "Engineering","Standard","Phase Out","Obsolete"};
    private static String m_emptyString = "";
    
    public ECOTartgetComboCellEditor()
    {
        this(m_emptyString);
    }
    
    public ECOTartgetComboCellEditor(String theLOVName)
    {
        super(theLOVName);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    protected Vector<String> loadLOV()
    {
        Vector<String> lovValues = new Vector<String>(Arrays.asList(listOfValues));
        return lovValues;
    }
    
}
