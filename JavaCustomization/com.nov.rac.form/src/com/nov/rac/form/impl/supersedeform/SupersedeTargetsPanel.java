package com.nov.rac.form.impl.supersedeform;

import java.awt.BorderLayout;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.quicksearch.export.NOVExportSearchResultUtils;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

/**
 * @author mishalt
 *
 */
public class SupersedeTargetsPanel extends AbstractUIPanel implements INOVFormLoadSave,IPublisher,ISubscriber
{
    private Registry m_registry = null;
    private TCTable m_table;
    private Button m_exportExcelButton;
    private final String m_panelName = getClass().getSimpleName();
    private String m_emptyString = new String();
    private final Set<String> m_rowsModified = new HashSet<String>();
    private static String COLUMN_NAMES = "COLUMN_NAMES";
    private static String DELIMITER = ",";
    private static final int NUMBER_OF_COLUMN_3 = 3;
    private static final int VERTICAL_SPAN = 30;

    public SupersedeTargetsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }

    private Registry getRegistry()
    {
        return m_registry;
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] dispositionsAry = propMap.getCompoundArray(m_registry.getString("targetDisposition.PROP"));
        TableUtils.populateTable(dispositionsAry, m_table);
        int lineNo_colIndex = TableUtils.getColumnIndex("line_no", m_table);
        for (int i = 0; i < dispositionsAry.length; i++)
        {
            int rowNo = i;
            String lineNo = Integer.toString(++rowNo);
            m_table.setValueAt(lineNo, i, lineNo_colIndex);
        }
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] dispositionPropMap = TableUtils.populateMap(m_table);

        // add only modified rows
        //IPropertyMap[] modifiedRows = getModifiedRows(dispositionPropMap); //changed

        propMap.setCompoundArray(getRegistry().getString("targetDisposition.PROP"), dispositionPropMap);
        return true;
    }

    /*private IPropertyMap[] getModifiedRows(IPropertyMap[] dispositionPropMap)
    {
        List<IPropertyMap> modifiedRows = new ArrayList<IPropertyMap>();
        String itemID = "";

        for (IPropertyMap disposition : dispositionPropMap)
        {
            itemID = disposition.getString("targetitemid");
            if (m_rowsModified.contains(itemID))
            {
                modifiedRows.add(disposition);
            }
        }
        return modifiedRows.toArray(new IPropertyMap[modifiedRows.size()]);
    }*/

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        m_table.setEnabled(flag);
    }

    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();

        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));

        createTargetTopPanel(composite);

        createTCTable(composite);

        createDataModelMapping();
        addListeners();
        registerProperties();

        return true;
    }

    private void createTargetTopPanel(Composite l_composite)
    {
        Composite composite = new Composite(l_composite, SWT.NONE);

        GridData gd_panel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gd_panel);

        composite.setLayout(new GridLayout(NUMBER_OF_COLUMN_3, false));

        Button showTargetsTableButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        Image image = getRegistry().getImage("TargetsTable.ShowTargetsTable.IMAGE");
        showTargetsTableButton.setImage(image);
        showTargetsTableButton.setLayoutData(gridData);
        showTargetsTableButton.setEnabled(false);

        Label l_lblTargetsTableName = new Label(composite, SWT.NONE);
        l_lblTargetsTableName.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        l_lblTargetsTableName.setText(m_registry.getString("lblTargetsTableName.NAME", "Key Not Found"));
        SWTUIHelper.setFont(l_lblTargetsTableName, SWT.BOLD);

        createExportExcelButton(composite);
    }

    private void createExportExcelButton(Composite composite)
    {
        m_exportExcelButton = new Button(composite, SWT.NONE);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1);
        m_exportExcelButton.setText(m_registry.getString("exportExcelButton.NAME", "Key Not Found"));

        m_exportExcelButton.setLayoutData(gridData);
    }

    private void createTCTable(Composite composite)
    {
        Composite comp = new Composite(composite, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, VERTICAL_SPAN);
        comp.setLayoutData(gridData);
        m_table = new SupersedeTargetTable(getTableColumnIdentifiers(), getTableColumns(), getRegistry());
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_table.setSortEnabled(false);

        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());

        ScrollPagePane scrollPane = new ScrollPagePane(m_table);
        tablePanel.add(scrollPane);

        SWTUIUtilities.embed(comp, tablePanel, false);
    }

    protected String[] getTableColumns()
    {
        String[] m_tableColumnNames = getRegistry().getStringArray(m_panelName + "." + COLUMN_NAMES, DELIMITER,
                m_emptyString);
        return m_tableColumnNames;
    }

    protected String[] getTableColumnIdentifiers()
    {
        String[] tableColumnIdentifiers = getRegistry().getStringArray(m_panelName + "." + "COLUMN_IDENTIFIERS",
                DELIMITER, m_emptyString);
        return tableColumnIdentifiers;
    }

    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();

        dataBindingHelper.bindData(m_table, getRegistry().getString("targetDisposition.PROP"),
                FormHelper.getDataModelName());
    }

    private void addListeners()
    {
        addTableSelectionListener();
        exportExcelButtonListener();
    }

    protected void exportExcelButtonListener()
    {
        m_exportExcelButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                //NOVExportSearchResultUtils.exportSearchResultToExcel(m_table);
            }
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }

    private void addTableSelectionListener()
    {
        m_table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent paramListSelectionEvent)
            {
                publishRowModified();
            }
        });
    }

    private void publishRowModified()
    {
        int rowInx = m_table.getSelectedRow();
        int colInx = TableUtils.getColumnIndex("targetitemid", m_table);

        String targetItemID = (String) m_table.getModel().getValueAt(rowInx, colInx);

        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent event = new PublishEvent(this, GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED, targetItemID, null);
        controller.publish(event);
    }

    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.registerSubscriber(GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED, this);
    }

    @Override
    public boolean reload() throws TCException
    {
        return false;
    }

    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals(GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED))
        {
            m_rowsModified.add((String) event.getNewValue());
        }
    }

    public void unRegisterSubscriber()
    {
        IController l_controller = ControllerFactory.getInstance().getDefaultController();
        l_controller.unregisterSubscriber(GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED, this);
    }

    @Override
    public void dispose()
    {
        unRegisterSubscriber();
        super.dispose();
    }

    @Override
    public IPropertyMap getData()
    {
        return null;
    }

}
