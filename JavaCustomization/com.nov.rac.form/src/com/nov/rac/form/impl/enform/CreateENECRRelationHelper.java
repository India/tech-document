package com.nov.rac.form.impl.enform;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.util.DualList;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.deleteHelper.CreateRelationObjectHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentPropertySet;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;


public class CreateENECRRelationHelper {

    private Registry m_registry = null;
    private HashMap<String, Object>  m_eCRNumberAndTCComponentMap = null;
    
    public CreateENECRRelationHelper(HashMap<String, Object> eCRNumberAndTCComponentMap)
    {
        m_eCRNumberAndTCComponentMap = eCRNumberAndTCComponentMap;
        m_registry = Registry.getRegistry(this);
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }    
	
	protected void CreateENECRRelation(TCComponent enForm,IPropertyMap propMap)
	    {
	    	AIFComponentContext[] secondaryECRlist;
	     	
	    	ArrayList<TCComponent> ecrObjetsTobeDeleted = new ArrayList<TCComponent>();
	    	ArrayList<TCComponent> ecrObjetsTobeAdded = new ArrayList<TCComponent>();
	    	
	    	 
	    	Map<String,TCComponent> ecrNameTagMap = new HashMap<String,TCComponent>();;
	    	//get list of secondary objects related by _ENECR_ relation
	    	try {
	    		String[] relatedecr = propMap.getStringArray(getRegistry()
				        .getString("ENForm.RelateECR"));

	    		AIFComponentContext[] secCompList = enForm.getSecondary();
	    		
	 
	    		for(AIFComponentContext secondaryECR : secCompList)
	    		{
	    			String ecrNumber;
					try {
						
						if(secondaryECR.getContextDisplayName().equals(getRegistry()
						        .getString("ECNECR.PROP")))
						{	
							ecrNumber = secondaryECR.getComponent().getProperty(getRegistry()
							        .getString("objectName.PROP"));
							
			     			ecrNameTagMap.put(ecrNumber, (TCComponent) secondaryECR.getComponent());
			     			ecrObjetsTobeDeleted.add((TCComponent) secondaryECR.getComponent());
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    			

	    		}

	    		
	    		//get relatedecr array from property map
	    		for(String tmpRelatedEcr : relatedecr)
	    		{
	    			//check ecr value in 
	    			if(ecrNameTagMap.containsKey(tmpRelatedEcr))
					{
	    				//add in create list & remove from delete list if present
	    				//ecrNameTagMap.remove(tmpRelatedEcr);
	    				ecrObjetsTobeDeleted.remove(ecrNameTagMap.get(tmpRelatedEcr));
	 				}
	    			else
	    			{


	    				ecrObjetsTobeAdded.add((TCComponent) m_eCRNumberAndTCComponentMap.get((tmpRelatedEcr)));
	    			}

	    		}
	    		//delete relation
	    		
	    		
	    		if(ecrObjetsTobeDeleted.size() > 0)
	    		{
	    			DeleteObjectHelper delRelationObjectHelper[] = new DeleteObjectHelper[ecrObjetsTobeDeleted.size()];
		            for (int i = 0; i <ecrObjetsTobeDeleted.size(); i++)
		            {
		            	delRelationObjectHelper[i] = new DeleteObjectHelper(
		            			DeleteObjectHelper.OPERATION_DELETE_RELATION);
		            	delRelationObjectHelper[i].setPrimaryObject(enForm);
		            	delRelationObjectHelper[i].setSecondaryObject(ecrObjetsTobeDeleted.get(i));
		            	delRelationObjectHelper[i].setRelationName(getRegistry()
						        .getString("ECNECR.PROP"));
		            }
		            
		            CreateObjectsSOAHelper.deleteRelationObjects(delRelationObjectHelper);
	    		}
	            
	    		//create relation
	    		if(ecrObjetsTobeAdded.size() > 0)
	    		{
		            CreateRelationObjectHelper creRelationObjectHelper[] = new CreateRelationObjectHelper[ecrObjetsTobeAdded.size()];
		            for (int i = 0; i <ecrObjetsTobeAdded.size(); i++)
		            {
		                creRelationObjectHelper[i] = new CreateRelationObjectHelper(
		                        CreateRelationObjectHelper.OPERATION_CREATE_RELATION);
		                creRelationObjectHelper[i].setPrimaryObject(enForm);
		                creRelationObjectHelper[i].setSecondaryObject(ecrObjetsTobeAdded.get(i));
		                creRelationObjectHelper[i].setRelationName(getRegistry()
						        .getString("ECNECR.PROP"));
		            }
		            
		            CreateObjectsSOAHelper.createRelationObjects(creRelationObjectHelper);	
	    		}
	    		
	    		
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
}
