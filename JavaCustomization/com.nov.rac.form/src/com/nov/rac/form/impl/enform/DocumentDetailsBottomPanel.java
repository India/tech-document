package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocumentDetailsBottomPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    
    private static final int NO_OF_COLUMNS_MAIN_COMPOSITE = 3;
    private static final int NO_OF_COLUMNS_PUBLISH_TO = 6;
    private Button m_webCheckBox = null;
    private Button m_pPCheckBox = null;
    private Button m_sVCheckBox = null;
    private Button m_rDCheckBox = null;
    private Button m_externallyCheckBox = null;
    private Button m_secureCheckBox = null;
    private Registry m_registry;
    private DatasetTablePanel m_datasetTablePanel;
    private TCComponent m_documentItem;
    
    public DocumentDetailsBottomPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_datasetTablePanel.load(propMap);
        
        loadPublishTo(propMap);
        return false;
    }
    
    private void loadPublishTo(IPropertyMap propMap) throws TCException
    {
        m_documentItem = propMap.getTag("items_tag");
        if (m_documentItem != null)
        {
            TCComponentForm documentItemMaster = (TCComponentForm) m_documentItem
                    .getRelatedComponent("IMAN_master_form");
            m_sVCheckBox.setSelection(documentItemMaster
                    .getLogicalProperty("nov4_publishtosv"));
            m_rDCheckBox.setSelection(documentItemMaster
                    .getLogicalProperty("nov4_publishtord"));
            m_pPCheckBox.setSelection(documentItemMaster
                    .getLogicalProperty("PublishToPP"));
            m_webCheckBox.setSelection(documentItemMaster
                    .getLogicalProperty("PublishToWeb"));
            m_externallyCheckBox.setSelection(documentItemMaster
                    .getLogicalProperty("External"));
            /*m_secureCheckBox.setSelection(documentItemMaster
                    .getLogicalProperty("secure"));*/
        }
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_datasetTablePanel.save(propMap);

        propMap.setComponent(m_documentItem);
        IPropertyMap masterFormPropMap = propMap.getCompound("IMAN_master_form");
        masterFormPropMap.setLogical("nov4_publishtosv", m_sVCheckBox.getSelection());
        masterFormPropMap.setLogical("nov4_publishtord", m_rDCheckBox.getSelection());
        masterFormPropMap.setLogical("PublishToPP", m_pPCheckBox.getSelection());
        masterFormPropMap.setLogical("PublishToWeb", m_webCheckBox.getSelection());
        masterFormPropMap.setLogical("External", m_externallyCheckBox.getSelection());
//        masterFormPropMap.setLogical("secure", m_secureCheckBox.getSelection());
        
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_sVCheckBox.setEnabled(flag);
        m_rDCheckBox.setEnabled(flag);
        m_pPCheckBox.setEnabled(flag);
        m_webCheckBox.setEnabled(flag);
        m_externallyCheckBox.setEnabled(flag);
//        m_secureCheckBox.setEnabled(flag);
        m_datasetTablePanel.setEnabled(flag);
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1,
                30);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        
        Composite mainComposite = createMainComposite(l_composite);
        
        createPublishToCheckBoxes(mainComposite);
        
        m_datasetTablePanel = new DatasetTablePanel(mainComposite, SWT.NONE);
        m_datasetTablePanel.createUI();
        m_datasetTablePanel.getComposite().setLayoutData(
                new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        
        
//        m_datasetTablePanel.getComposite().setSize(width, height)
        
        setEnabled(false);
        
        return true;
    }
    
    private void createPublishToCheckBoxes(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gl_composite = new GridLayout(NO_OF_COLUMNS_PUBLISH_TO,
                false);
        composite.setLayout(gl_composite);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2,
                1));
        composite.setLayout(gl_composite);
        
        createPublishToLabel(composite);
        
        m_webCheckBox = createCheckBox(composite,
                getRegistry().getString("Web.Button"));
        m_pPCheckBox = createCheckBox(composite,
                getRegistry().getString("PP.Button"));
        m_sVCheckBox = createCheckBox(composite,
                getRegistry().getString("SV.Button"));
        m_rDCheckBox = createCheckBox(composite,
                getRegistry().getString("RD.Button"));
        m_externallyCheckBox = createCheckBox(composite, getRegistry()
                .getString("Externally.Button"));
        /*m_secureCheckBox = createCheckBox(composite,
                getRegistry().getString("Secure.Button"));*/
    }
    
    private void createPublishToLabel(Composite parent)
    {
        Composite labelComposite = new Composite(parent, SWT.NONE);
        GridLayout gl_labelComposite = new GridLayout(1, true);
        labelComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                true, 1, 1));
        labelComposite.setLayout(gl_labelComposite);
        Label publishTo = new Label(labelComposite, SWT.NONE);
        publishTo.setText(getRegistry().getString("PublishTo.Label"));
    }
    
    private Composite createMainComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(
                NO_OF_COLUMNS_MAIN_COMPOSITE, true);
        gl_l_composite.marginHeight = 0;
        gl_l_composite.marginWidth = 0;
        composite.setLayout(gl_l_composite);
        return composite;
        
    }
    
    private Button createCheckBox(Composite parent, String setText)
    {
        Button button = new Button(parent, SWT.CHECK);
        button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        button.setText(setText);
        button.setEnabled(false);
        return button;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
