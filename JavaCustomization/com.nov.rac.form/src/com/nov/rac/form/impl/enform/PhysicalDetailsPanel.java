package com.nov.rac.form.impl.enform;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.dialog.savedelegate.TabSaveDelegate;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.operations.IOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import org.eclipse.swt.events.ModifyListener;

//This panel includes physical details panel and manufacturing panel
public class PhysicalDetailsPanel extends AbstractUIPanel implements
        INOVFormLoadSave,ISubscriber,IPublisher
{
    TCComponent m_item;
    
    private TCComponent m_itemRevision = null;
    private Registry m_registry;
    private Label m_uOMValueLabel;
    private Text m_widthValueText;
    private Combo m_widthUOMValueComboBox;
    private Combo m_lenghtUOMValueComboBox;
    private Combo m_heightUOMValueComboBox;
    private Text m_lenghtValueText;
    private Text m_heightValueText;
    private Text m_weightValueText;
    private Combo m_weightUOMValueComboBox;
    private Text m_volumeValueText;
    private Combo m_volumeUOMValueComboBox;
    private Label m_widthLabel;
    private Label m_lengthLebel;
    private Label m_heightLabel;
    private Label m_weightLabel;
    private Label m_volumeLabel;
    private Label m_widthUOMLabel;
    private Label m_lengthUOMLabel;
    private Label m_heightUOMLabel;
    private Label m_weightUOMLabel;
    private Label m_volumeUOMLabel;
    private boolean is_WidthValid = true;
    private boolean is_LengthValid = true;
    private boolean is_HeightValid = true;
    private boolean is_WeightValid = true;
    private boolean is_VolumeValid = true;
    
    private static final int NO_OF_COLUMNS_PHYSICAL_DETAILS_COMPOSITE = 5;
    private static final int NO_OF_COLUMNS_VERTICAL_SEPARATOR = 3;
    
    final Display display = getComposite().getDisplay();
    
    public PhysicalDetailsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_item = ((TCComponentItemRevision)propMap.getComponent()).getItem();
        m_itemRevision = propMap.getComponent();
                
        fillUOMCombos();
        
        IPropertyMap[] itemMasterRevPropMaps = propMap
                .getCompoundArray("IMAN_master_form_rev");
        
        IPropertyMap itemMasterRevPropMap = itemMasterRevPropMaps[0];
        
        IPropertyMap[] itemMasterPropMaps =propMap.getCompound("items_tag").getCompoundArray("IMAN_master_form");
        
        itemMasterPropMaps[0].getComponent().getStringProperty("rsone_uom");
        String uOMValue =PropertyMapHelper.handleNull(itemMasterPropMaps[0].getString("rsone_uom"));
        m_uOMValueLabel.setText(uOMValue);
        
        String length = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getDouble("nov4length"));
        m_lenghtValueText.setText(length);
        
        String width = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getDouble("nov4width"));
        m_widthValueText.setText(width);
        
        String height = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getDouble("nov4height"));
        m_heightValueText.setText(height);
        
        // lwh = length width height
        String lwhUOM = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getString("nov4lwh_units"));
        m_lenghtUOMValueComboBox.setText(lwhUOM);
        m_widthUOMValueComboBox.setText(lwhUOM);
        m_heightUOMValueComboBox.setText(lwhUOM);
        
        String weight = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getDouble("nov4weight"));
        m_weightValueText.setText(weight);
        
        String volume = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getDouble("nov4volume"));
        m_volumeValueText.setText(volume);
        
        String weightUOM = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getString("nov4weight_units"));
        m_weightUOMValueComboBox.setText(weightUOM);
        
        String volumeUOM = PropertyMapHelper.handleNull(itemMasterRevPropMap
                .getString("nov4volume_units"));
        m_volumeUOMValueComboBox.setText(volumeUOM);

        m_weightUOMValueComboBox.setSize(m_lenghtUOMValueComboBox.getSize());
        m_volumeUOMValueComboBox.setSize(m_lenghtUOMValueComboBox.getSize());
        return true;
        
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        savePhysicalDetails(propMap);
        
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        setFieldsEnabled(flag);
        
    }
    
    private void fillUOMCombos()
    {
        String[] lwhUOMValues = getLOVValues(m_registry
                .getString("PhysicalDetailsPanel.LengthWidthHeightUOMLOV"));
        
        m_lenghtUOMValueComboBox.setItems(lwhUOMValues);
        
        m_widthUOMValueComboBox.setItems(lwhUOMValues);
        
        m_heightUOMValueComboBox.setItems(lwhUOMValues);
        
        m_volumeUOMValueComboBox.setItems(getLOVValues(m_registry
                .getString("PhysicalDetailsPanel.VolumeUOMLOV")));
        
        m_weightUOMValueComboBox.setItems(getLOVValues(m_registry
                .getString("PhysicalDetailsPanel.WeightUOMLOV")));
//        m_weightUOMValueComboBox.setItems(getLOVValues(m_registry
//              .getString("PhysicalDetailsPanel.VolumeUOMLOV")));
        
    }
    
    private String[] getLOVValues(String lovName)
    {
        Vector<Object> lwhUOMLov = LOVUtils.getListOfValuesforLOV(lovName);
        
        String[] allItems = lwhUOMLov.toArray(new String[lwhUOMLov.size()]);
        
        return allItems;
    }
    
    private void savePhysicalDetails(IPropertyMap propMap)
    {
        IPropertyMap itemMasterRevPropMap = propMap.getCompound("IMAN_master_form_rev");
        
        PropertyMapHelper.setDouble("nov4length", m_lenghtValueText.getText(), itemMasterRevPropMap);
        
        PropertyMapHelper.setDouble("nov4width", m_widthValueText.getText(), itemMasterRevPropMap);
        
        PropertyMapHelper.setDouble("nov4height", m_heightValueText.getText(), itemMasterRevPropMap);
        
        PropertyMapHelper.setDouble("nov4weight", m_weightValueText.getText(), itemMasterRevPropMap);
        
        PropertyMapHelper.setDouble("nov4volume", m_volumeValueText.getText(), itemMasterRevPropMap);
        
        itemMasterRevPropMap.setString("nov4lwh_units", m_lenghtUOMValueComboBox.getText());
        
        itemMasterRevPropMap.setString("nov4weight_units", m_weightUOMValueComboBox.getText());
        
        itemMasterRevPropMap.setString("nov4volume_units", m_volumeUOMValueComboBox.getText());
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite mainComposite = getMainComposite();
        
        CLabel physicalDetailsLabel = new CLabel(mainComposite, SWT.NONE);
        physicalDetailsLabel.setText(getRegistry().getString(
                "PhysicalDetails.Label"));
        SWTUIHelper.setFont(physicalDetailsLabel, SWT.BOLD);
        physicalDetailsLabel.setImage(new Image(getComposite().getDisplay(),
                m_registry.getImage("Details.IMAGE"), SWT.NONE));
        
        createPhysicalDetailsInfomationArea(mainComposite);
        
       // addListeners();
        
        setFieldsEnabled(false);
        
        registerSubscriber();
        
        createDataModelMapping();
        
        return true;
    }
    
    private void addListeners()
    {
        addSelectionListenerOnLWHUnitCombo();
        
        addVerifyListenerOnTexts();
        
        //Adding Modify listener to Text
        addModifyListenerOnTexts();
        
        //Adding Modify to listener to comboBox
        addModifyListenerOnCombo();
        
    }


    private void addSelectionListenerOnLWHUnitCombo()
    {
        m_lenghtUOMValueComboBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent event)
            {
                String lwhUnit = ((Combo)event.getSource()).getText();
                setLWHUnit(lwhUnit);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent event)
            {
                
            }
        });
        
        m_widthUOMValueComboBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent event)
            {
                String lwhUnit = ((Combo)event.getSource()).getText();
                setLWHUnit(lwhUnit);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent event)
            {
                
            }
        });
        
        m_heightUOMValueComboBox.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent event)
            {
                String lwhUnit = ((Combo)event.getSource()).getText();
                setLWHUnit(lwhUnit);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent event)
            {
                
            }
        });
        
    }

    private void setLWHUnit(String lwhUnit)
    {
        m_lenghtUOMValueComboBox.setText(lwhUnit);
        m_widthUOMValueComboBox.setText(lwhUnit);
        m_heightUOMValueComboBox.setText(lwhUnit);
    }

    private void addVerifyListenerOnTexts()
    {
        m_lenghtValueText.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent arg0)
            {
                verifyDoubleValue(arg0);
            }
        });
        
        m_widthValueText.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent arg0)
            {
                verifyDoubleValue(arg0);
            }
        });
        
        m_heightValueText.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent arg0)
            {
                verifyDoubleValue(arg0);
            }
        });
        
        m_weightValueText.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent arg0)
            {
                verifyDoubleValue(arg0);
            }
        });
        
        m_volumeValueText.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent arg0)
            {
                verifyDoubleValue(arg0);
            }
        });
    }
    
    private void verifyDoubleValue(VerifyEvent arg0)
    {
        Text text = (Text) arg0.getSource();
        
        final String oldStr = text.getText();
        String newStr = oldStr.substring(0, arg0.start) + arg0.text + oldStr.substring(arg0.end);
        
        if (!isValidDouble(newStr))
        {
            arg0.doit = false;
        }
    }
    
    private boolean isValidDouble(String string)
    {
        boolean isValid = true;
        try
        {
            if(FormHelper.hasContent(string))
            {
                Double.parseDouble(string);
            }
        }
        catch(NumberFormatException ex)
        {
            isValid = false;
        }
        return isValid;
    }

    private void setFieldsEnabled(boolean flag) 
    {
    	m_heightUOMValueComboBox.setEnabled(flag);
    	m_heightValueText.setEnabled(flag);
    	m_lenghtUOMValueComboBox.setEnabled(flag);
    	m_lenghtValueText.setEnabled(flag);
    	m_uOMValueLabel.setEnabled(flag);
    	m_volumeUOMValueComboBox.setEnabled(flag);
    	m_volumeValueText.setEnabled(flag);
    	m_weightUOMValueComboBox.setEnabled(flag);
    	m_weightValueText.setEnabled(flag);
    	m_widthUOMValueComboBox.setEnabled(flag);
    	m_widthValueText.setEnabled(flag);
	}

	private void createPhysicalDetailsInfomationArea(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.BORDER);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
                ((GridLayout) parent.getLayout()).numColumns, 1));
        
        composite.setLayout(new GridLayout(
                NO_OF_COLUMNS_PHYSICAL_DETAILS_COMPOSITE, false));
        
        // UOM field
        createColumn1(composite);
        createVerticalSeparator(composite);
        
        // Length Width Height fields
        createColumn2(composite);
        createVerticalSeparator(composite);
        
        // Weight Volume fields
        createColumn3(composite);
      
        
    }
    
   /* private void createColumn3(Composite parent)
    {
        Composite composite = createColumnComposite(parent, 4);
        
        m_weightValueText = createLabelTextComposite(composite, getRegistry()
                .getString("Weight.Label"));
        m_weightUOMValueComboBox = createLabelComboComposite(composite,
                getRegistry().getString("WeightUOM.Label"));
       
        
        m_volumeValueText = createLabelTextComposite(composite, getRegistry()
                .getString("Volume.Label"));
        m_volumeUOMValueComboBox = createLabelComboComposite(composite,
                getRegistry().getString("VolumeUOM.Label"));
        
    }
    
    private void createColumn2(Composite parent)
    {
        Composite composite = createColumnComposite(parent, 4);
        m_widthValueText = createLabelTextComposite(composite, getRegistry()
                .getString("Width.Label"));
        m_widthUOMValueComboBox = createLabelComboComposite(composite,
                getRegistry().getString("WidthUOM.Label"));
        
        m_lenghtValueText = createLabelTextComposite(composite, getRegistry()
                .getString("Length.Label"));
        m_lenghtUOMValueComboBox = createLabelComboComposite(composite,
                getRegistry().getString("LengthUOM.Label"));
        
        m_heightValueText = createLabelTextComposite(composite, getRegistry()
                .getString("Height.Label"));
        m_heightUOMValueComboBox = createLabelComboComposite(composite,
                getRegistry().getString("HeightUOM.Label"));
        
        
        
    }*/
	
	 private void createColumn3(Composite parent)
	    {
	        Composite composite = createColumnComposite(parent, 4);
	        
	        m_weightLabel = createLabel(composite,
	                getRegistry().getString("Weight.Label"));
	        m_weightValueText = createText(composite);
	        
	        m_weightUOMLabel = createLabel(composite,
	                getRegistry().getString("WeightUOM.Label"));
	        m_weightUOMValueComboBox = createCombo(composite);
	        
	        m_volumeLabel = createLabel(composite,
	                getRegistry().getString("Volume.Label"));
	        m_volumeValueText = createText(composite);
	        
	        m_volumeUOMLabel = createLabel(composite,
	                getRegistry().getString("VolumeUOM.Label"));
	        m_volumeUOMValueComboBox = createCombo(composite);
	        
	    }
	    
	    private void createColumn2(Composite parent)
	    {
	        Composite composite = createColumnComposite(parent, 4);
	        
	        m_widthLabel = createLabel(composite,
	                getRegistry().getString("Width.Label"));
	        m_widthValueText = createText(composite);
	        
	        m_widthUOMLabel = createLabel(composite,
	                getRegistry().getString("WidthUOM.Label"));
	        m_widthUOMValueComboBox = createCombo(composite);
	        
	        m_lengthLebel = createLabel(composite,
	                getRegistry().getString("Length.Label"));
	        m_lenghtValueText = createText(composite);
	        
	        m_lengthUOMLabel = createLabel(composite,
	                getRegistry().getString("LengthUOM.Label"));
	        m_lenghtUOMValueComboBox = createCombo(composite);
	        
	        m_heightLabel = createLabel(composite,
	                getRegistry().getString("Height.Label"));
	        m_heightValueText = createText(composite);
	        
	        m_heightUOMLabel = createLabel(composite,
	                getRegistry().getString("HeightUOM.Label"));
	        m_heightUOMValueComboBox = createCombo(composite);
	        
	    }
    
    private void createColumn1(Composite parent)
    {
        Composite composite = createColumnComposite(parent, 2);
        m_uOMValueLabel = createLabelLabelComposite(composite, getRegistry()
                .getString("UOM.Label"));
        
    }
    
    private Composite getMainComposite()
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, true);
        // gl_l_composite.marginHeight = 0;
        // gl_l_composite.marginWidth = 0;
        l_composite.setLayout(gl_l_composite);
        return l_composite;
        
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    private Composite createColumnComposite(Composite parent, int gl_Columns)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout gl_composite = new GridLayout(gl_Columns, false);
        gl_composite.marginHeight = 0;
        gl_composite.marginWidth = 0;
        composite.setLayout(gl_composite);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1,
                1));
        return composite;
        
    }
    
    private Label createLabelLabelComposite(Composite composite,
            String keyLabelText)
    {
//        Composite composite = new Composite(parent, SWT.NONE);
//        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
//                1, 1));
//        GridLayout gl_Composite = new GridLayout(2, false);
//        gl_Composite.marginWidth = 0;
//        gl_Composite.marginHeight = 0;
//        composite.setLayout(gl_Composite);
        
        Label keyLabel = new Label(composite, SWT.NONE);
        keyLabel.setText(keyLabelText);
        keyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                1, 1));
        
        Label valueLabel = new Label(composite, SWT.NONE);
        valueLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        return valueLabel;
        
    }
    
    /*private Text createLabelTextComposite(Composite composite,
            String keyLabelText)
    {
//        Composite composite = new Composite(parent, SWT.NONE);
//        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
//                1, 1));
//        GridLayout gl_Composite = new GridLayout(2, true);
//        gl_Composite.marginWidth = 0;
//        gl_Composite.marginHeight = 0;
//        composite.setLayout(gl_Composite);
        
        Label keyLabel = new Label(composite, SWT.NONE);
        keyLabel.setText(keyLabelText);
        keyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                1, 1));
        
        Text valueText = new Text(composite, SWT.BORDER);
        valueText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        return valueText;
        
    }
    
    private Combo createLabelComboComposite(Composite composite,
            String keyLabelText)
    {
//        Composite composite = new Composite(parent, SWT.NONE);
//        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
//                1, 1));
//        GridLayout gl_Composite = new GridLayout(2, true);
//        gl_Composite.marginWidth = 0;
//        gl_Composite.marginHeight = 0;
//        composite.setLayout(gl_Composite);
        
        Label keyLabel = new Label(composite, SWT.NONE);
        keyLabel.setText(keyLabelText);
        keyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                1, 1));
        
        Combo valueCombo = new Combo(composite, SWT.READ_ONLY|SWT.DROP_DOWN);
        GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1);
        GC gc = new GC (valueCombo);
        FontMetrics fm = gc.getFontMetrics ();
        int width = 10 * fm.getAverageCharWidth ();
        gc.dispose ();
        gd_combo.widthHint = width;
        valueCombo.setLayoutData(gd_combo);
        return valueCombo;
        
    }*/
    
    private void createVerticalSeparator(Composite parent)
    {
        Label verticalSeparator = new Label(parent, SWT.SEPARATOR
                | SWT.VERTICAL);
        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, false, false, 1,
                NO_OF_COLUMNS_VERTICAL_SEPARATOR);
        
        verticalSeparator.setLayoutData(layoutData);
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, this);
        
        controller.registerSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, this);
        
        getController().registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED,
                this);
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        if (!getComposite().isDisposed())
        {
            Object thisParent = getComposite().getParent().getParent();
            Object sourceParent = null;
            
            IPublisher eventPublisher = event.getSource();
            
            if (eventPublisher instanceof AbstractUIPanel)
            {
                Composite sourceComposite = ((AbstractUIPanel) eventPublisher).getComposite();
                sourceParent = sourceComposite.getParent();
            }
            
            if (event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_EDIT_BUTTON_SELECTED)
                    && sourceParent != null && thisParent.equals(sourceParent))
            {
                onEdit();
                addListeners(); 
            }
           
            if (event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))
            {
                boolean success = (boolean) event.getNewValue();
                if(success)
                {
                    onSave();
                }
                
            }
            
            if(event.getPropertyName().equalsIgnoreCase(GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED)
                    
                    && FormHelper.isLatestItemRevision(m_itemRevision))
            {
                setFieldsEnabled(false);
            }
        }
        
    }
    
    private void onSave()
    {
        setFieldsEnabled(false);
    }
    
    private void onEdit()
    {
        setFieldsEnabled(true);
    }
    
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
        
    }
    
    private void unregisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, this);
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_SAVE_SUCCESS_FLAG, this);
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, this);     
    }
    
    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        
        dataBindingHelper.bindData(m_lenghtValueText, "nov4length", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_widthValueText, "nov4width",
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_heightValueText, "nov4height", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_weightValueText, "nov4weight", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_volumeValueText, "nov4volume", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_lenghtUOMValueComboBox, "nov4lwh_units", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_weightUOMValueComboBox, "nov4weight_units", 
                FormHelper.getDialogDataModelName());
        
        dataBindingHelper.bindData(m_volumeUOMValueComboBox, "nov4volume_units", 
                FormHelper.getDialogDataModelName());
        
    }
    
    // Adding Modify Listener to all textField created in Physical composite Panel
    private void addModifyListenerOnTexts()
    {
        
        final Display display = getComposite().getDisplay();
        
        m_lenghtValueText.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                
                String valueOnTextLength = ((Text) arg0.widget).getText();
                String valueOnComboLength = m_lenghtUOMValueComboBox.getText();
                
                is_LengthValid = validateTextAndCombo(valueOnComboLength,valueOnTextLength);
                
                setLabelColorOnTextChange(is_LengthValid,valueOnTextLength.isEmpty(), m_lengthLebel, m_lengthUOMLabel);
                
                toggleSaveButton();
            }
        });
        
        m_widthValueText.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                
                String valueOnTextWidth = ((Text) arg0.widget).getText();
                String valueOnComboWidth = m_widthUOMValueComboBox.getText();
                
                is_WidthValid = validateTextAndCombo(valueOnComboWidth,valueOnTextWidth);
                
                setLabelColorOnTextChange(is_WidthValid,valueOnTextWidth.isEmpty(), m_widthLabel, m_widthUOMLabel);
                              
                toggleSaveButton();
            }
            
        });
        
        m_heightValueText.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                
                String valueOnTextHeight = ((Text) arg0.widget).getText();
                String vlaueOnComboHeight = m_heightUOMValueComboBox.getText();
                
                
                
                is_HeightValid = validateTextAndCombo(vlaueOnComboHeight, valueOnTextHeight);
                
                setLabelColorOnTextChange(is_HeightValid,valueOnTextHeight.isEmpty(), m_heightLabel, m_heightUOMLabel);
                               
                toggleSaveButton();
            }
        });
        
        m_weightValueText.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {

                String valueOnTextweight = ((Text) arg0.widget).getText();
                String valueOnComboWeight = m_weightUOMValueComboBox.getText();
                
                is_WeightValid = validateTextAndCombo(valueOnComboWeight, valueOnTextweight);
                
                setLabelColorOnTextChange(is_WeightValid,valueOnTextweight.isEmpty(), m_weightLabel, m_weightUOMLabel);
                               
                toggleSaveButton();
            }
        });
        
        m_volumeValueText.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {

                String valueOnTextVolume = ((Text) arg0.widget).getText();
                String valueOnComboVolume = m_volumeUOMValueComboBox.getText();
                
                
                is_VolumeValid = validateTextAndCombo(valueOnComboVolume, valueOnTextVolume);
                
                setLabelColorOnTextChange (is_VolumeValid,valueOnTextVolume.isEmpty(), m_volumeLabel, m_volumeUOMLabel);
                
                toggleSaveButton();
            }
        });
    }

    /**
     * Adding Modify Listener to each ComboBox
     */
    private void addModifyListenerOnCombo()
    {
        final Display display = getComposite().getDisplay();
        
        m_lenghtUOMValueComboBox.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                // TODO Auto-generated method stub
                String valueOnComboLength = ((Combo) arg0.widget).getText();                
                String valueOnTextLength = m_lenghtValueText.getText();
                
                is_LengthValid = validateTextAndCombo(valueOnComboLength,
                        valueOnTextLength);
                
                setLabelColorOnComboChange(is_LengthValid, valueOnComboLength.isEmpty(), 
                        m_lengthLebel, m_lengthUOMLabel);
                               
                toggleSaveButton();
            }
        });
        
        m_widthUOMValueComboBox.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
              
                String valueOnComboWidth = ((Combo) arg0.widget).getText(); 
                String valueOnTextWidth = m_widthValueText.getText();
                
                is_WidthValid = validateTextAndCombo(valueOnComboWidth,valueOnTextWidth); 
                
                setLabelColorOnComboChange(is_WidthValid,valueOnComboWidth.isEmpty(), 
                        m_widthLabel, m_widthUOMLabel);
                                            
                toggleSaveButton();
            }
        });
        
        m_heightUOMValueComboBox.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
               
                String valueOnComboHeight = ((Combo) arg0.widget).getText();
                String valueOnTextHeight = m_heightValueText.getText();
                
                is_HeightValid = validateTextAndCombo(valueOnComboHeight,valueOnTextHeight);
                
                setLabelColorOnComboChange(is_HeightValid,valueOnComboHeight.isEmpty(), 
                        m_heightLabel, m_heightUOMLabel);
               
                toggleSaveButton();
            }
        });
        
        m_weightUOMValueComboBox.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                String valueOnComboWeight = ((Combo) arg0.widget).getText();
                String valueOnTextWeight = m_weightValueText.getText();
                
                is_WeightValid = validateTextAndCombo(valueOnComboWeight,valueOnTextWeight);
                
                setLabelColorOnComboChange(is_WeightValid,valueOnComboWeight.isEmpty(), 
                        m_weightLabel, m_weightUOMLabel);
                             
                toggleSaveButton();
            }
        });
        
        m_volumeUOMValueComboBox.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {

                String valueOnComboVolume = ((Combo) arg0.widget).getText();
                String valueOnTextVolume = m_volumeValueText.getText();
                
                is_VolumeValid = validateTextAndCombo(valueOnComboVolume,valueOnTextVolume);
                
                setLabelColorOnComboChange(is_VolumeValid, valueOnComboVolume.isEmpty(), 
                        m_volumeLabel, m_volumeUOMLabel);
                
                toggleSaveButton();
                
            }
        });
    }
    
    private boolean validateTextAndCombo(String comboBoxValue, String textFieldValue)
    {
        boolean hasComboValue = false;
        boolean hasTextValue = false;
        boolean is_FieldValid = false;        
        double textValueInDouble = 0.0;

        if(comboBoxValue !=null && !comboBoxValue.isEmpty())
        {
            hasComboValue = true;    
        }
        
        if(textFieldValue != null &&  !textFieldValue.isEmpty())
        {
            textValueInDouble = Double.parseDouble(textFieldValue);
            
            if(textValueInDouble != 0.0)
            {
                hasTextValue = true;
            }
        }
        
        if(hasComboValue && hasTextValue)
        {
            is_FieldValid = true;
        }
        else if( !hasComboValue && !hasTextValue)
        {
            is_FieldValid = true;
        }
        else
        {
            is_FieldValid = false;
        }
        
        return is_FieldValid;
    } 
    
  //Enabling or Disabling the Save Button in Revision Panel
    private void toggleSaveButton()
    {

        //If all the Fields are filled in with appropriate values, 
        // this function enabling save button to trigger Save action
        if (is_LengthValid == true && is_WidthValid == true
                && is_HeightValid == true && is_WeightValid == true
                && is_VolumeValid == true)
        {
            PublishEvent publishEvent = getPublishEvent(
                    GlobalConstant.EVENT_VALIDATE_PROPERTIES_SUCCESS, null, null);
            
            getController().publish(publishEvent);
        }
        
        else
        {
            //To disable the Save Button in Revsion Panel
            PublishEvent publishEvent = getPublishEvent(
                    GlobalConstant.EVENT_VALIDATE_PROPERTIES_ERRORS, null, null);
            
            getController().publish(publishEvent);
            
        }
    }
    
    private IController getController()
    {
        return ControllerFactory.getInstance().getDefaultController();
    }
    
    private PublishEvent getPublishEvent(String property, Object newValue,
            Object oldValue)
    {
        PublishEvent event = new PublishEvent(getPublisher(), property,
                newValue, oldValue);
        return event;
    }
    
    private IPublisher getPublisher()
    {
        return this;
    }

    @Override
    public IPropertyMap getData()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    //To create a Label in composite
    private Label createLabel(Composite composite, String labelName)
    {
        Label keyLabel = new Label(composite, SWT.NONE);
        keyLabel.setText(labelName);
        keyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false,
                1, 1));
        return keyLabel;
    }
    
    //Create a Text Field in Composite
    private Text createText(Composite composite)
    {
        Text valueText = new Text(composite, SWT.BORDER);
        valueText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        return valueText;
        
    }
    
    //Create a comboBox in composite
    private Combo createCombo(Composite composite)
    {
        Combo valueCombo = new Combo(composite, SWT.READ_ONLY | SWT.DROP_DOWN);
        GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        GC gc = new GC(valueCombo);
        FontMetrics fm = gc.getFontMetrics();
        int width = 10 * fm.getAverageCharWidth();
        gc.dispose();
        gd_combo.widthHint = width;
        valueCombo.setLayoutData(gd_combo);
        return valueCombo;
    }
    

    private void setLabelColorOnTextChange(boolean is_valid,boolean is_textEmpty,Label textLabel, Label comboLabel)
    {        
        if(is_valid == true)
        {
            textLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
            comboLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
        }        
        else if( is_textEmpty == true)
        {
            textLabel.setForeground(display.getSystemColor(SWT.COLOR_RED));
            comboLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
        }
        else 
        {
            textLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
            comboLabel.setForeground(display.getSystemColor(SWT.COLOR_RED));
        }        
    }
    
    private void setLabelColorOnComboChange(boolean is_valid,boolean is_comboEmpty,Label textLabel, Label comboLabel)
    {
        if(is_valid == true)
        {
            textLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
            comboLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
        }        
        else if( is_comboEmpty == true)
        {           
            textLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
            comboLabel.setForeground(display.getSystemColor(SWT.COLOR_RED));
        }
        else
        {
            textLabel.setForeground(display.getSystemColor(SWT.COLOR_RED));
            comboLabel.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
        }        
    }
    
}