package com.nov.rac.form.impl.stopform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FieldMandatoryValidator;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class CancelStopCommentsPanel extends AbstractExpandableUIPanel implements INOVFormLoadSave
{

    public CancelStopCommentsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(2, false));
        
        getExpandableComposite().setText(m_registry.getString("CancelStopComments.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
        
        m_fieldValidator = new FieldMandatoryValidator();
        
        createCommentsComboRow(l_composite);
        createCommentsTextArea(l_composite);
        
        UIHelper.makeMandatory(m_commentsCombo, m_fieldValidator, true, null);
        
        createDataModelMapping();
        
        return true;
    
    }

    private void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_commentsCombo,
                GlobalConstant.NOV4_STOP_COMMENT_VALS, FormHelper.getDataModelName());
        dataBindingHelper.bindData(m_commentsTxt,
                GlobalConstant.STOPCOMMENT, FormHelper.getDataModelName());
        
    }



    private void createCommentsComboRow(Composite composite) throws TCException
    {
        m_commentsCombo = new Combo(composite, SWT.READ_ONLY);
        
        m_commentsCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        m_commentsCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2,
                1));
        
        TCComponentListOfValuesType lovType = (TCComponentListOfValuesType) getSession().getTypeComponent(
                "ListOfValues");
        TCComponentListOfValues comments[] = lovType.find( GlobalConstant.STOP_ORDER_COMMENTS_LOV);
      
        UIHelper.setAutoComboArray(m_commentsCombo, LOVUtils.getLovStringValues(comments[0]));
        
        m_commentsCombo.select(-1);
        addComboListener();
    }

    private void addComboListener()
    {
        m_commentsCombo.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                validateFields();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }

    private void moveCombo()
    {
        Rectangle rect = m_commentsCombo.getBounds();
        m_commentsCombo.setLocation(rect.x + 1, rect.y);
        m_commentsCombo.setLocation(rect.x, rect.y);
        
    }
    
    private TCSession getSession()
    {
        return (TCSession) AIFUtility.getDefaultSession();
    }

    private void createCommentsTextArea(Composite composite)
    {
        m_commentsTxt =  new Text(composite, SWT.MULTI | SWT.V_SCROLL | SWT.WRAP);
        
        m_commentsTxt.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, m_verticalSpan);
        
        m_commentsTxt.setLayoutData(gridData);
        
        m_commentsTxt.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                m_commentsCombo.notifyListeners(SWT.Selection, new Event());
            } 
         });
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
    }

    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_commentsCombo.setText(PropertyMapHelper.handleNull(propMap.getString(GlobalConstant.NOV4_STOP_COMMENT_VALS)));
        
        m_commentsTxt.setText(PropertyMapHelper.handleNull(propMap.getString(GlobalConstant.STOPCOMMENT)));
        
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString(GlobalConstant.NOV4_STOP_COMMENT_VALS, m_commentsCombo.getText());
        propMap.setString(GlobalConstant.STOPCOMMENT, m_commentsTxt.getText());
        
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        m_commentsTxt.setEnabled(flag);
        m_commentsCombo.setEnabled(flag);
    }
    
    private void validateFields()
    {
        String comment = m_commentsCombo.getText();
        boolean isValid = false;
        if(comment != null)
        {
            
            if(comment.equals(NO_ISSUE_FOUND))
            {
                isValid = true;
            }
            else if(comment.equals(""))
            {
                isValid = false;
            }
            else
            {
                if(m_commentsTxt.getText() != null && !m_commentsTxt.getText().equals(""))
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                }
            }
            
            m_fieldValidator.setValid(isValid);
            moveCombo();
        }
    }

    private Registry m_registry;
    private Combo m_commentsCombo;
    private Text m_commentsTxt;
    
    private FieldMandatoryValidator m_fieldValidator;
    private int m_verticalSpan = 3;
    private static final String NO_ISSUE_FOUND = "No Issue Found";
}
