package com.nov.rac.form.impl.enform;

import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.framework.tablecomparator.impl.ComparatorDataModel;
import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.DefaultTableComparator;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;
import com.nov.rac.framework.tablecomparator.impl.ItemComparator;
import com.nov.rac.framework.tablecomparator.impl.TablePainter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class SecondaryFormsPanel extends AbstractUIPanel implements
        INOVFormLoadSave,ITableComparatorSubscriber
{
    
    private Registry m_registry;
    private TCTable m_secondaryFormTable;
    private TCComponent m_tcComponent = null;
    private ITableComparator m_iTableComparator = null;
    private static final String SECONDARYFORMS_CONTEXT = "SECONDARYFORMS";
    
    public SecondaryFormsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        IPropertyMap itemPropMap = propMap.getCompound("items_tag");
    	m_tcComponent = itemPropMap.getComponent();
        m_secondaryFormTable.removeAllRows();
        TCComponent[] relatedForms = itemPropMap.getTagArray("Nov4_secondary_form");
        TableUtils.populateTable(relatedForms, m_secondaryFormTable);
        if (GlobalConstant.COMPARE_NONCOMPARE_FLAG)
        {
           // initializeComparision();
        }
        return true;
    }
    
    private void initializeComparision()
    {
        String[] tableColumnNames = getRegistry().getStringArray(
                "SecondaryFormsPanel.CompareColumns");
        
        IDataModel model = new ComparatorDataModel();
        model.setPrimaryKeyColumn("object_string");
        model.setTableColumns(tableColumnNames);
        model.setTable(m_secondaryFormTable);
        model.setComparator(new ItemComparator());
        model.setCompareObject(m_tcComponent);
        model.setSubscriber(this);
        
        m_iTableComparator = DefaultTableComparator.getController();
        m_iTableComparator.setContext(SECONDARYFORMS_CONTEXT, model);
        m_iTableComparator.compareTable(SECONDARYFORMS_CONTEXT);
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        GridLayout gl_composite = new GridLayout(1, true);
        composite.setLayout(gl_composite);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        createSecondaryFormsComposite(composite);
        return true;
    }
    
    private void createSecondaryFormsComposite(Composite parent)
    {
        CLabel secondaryFormLabel = new CLabel(parent, SWT.NONE);
        secondaryFormLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
                true, true, 1, 1));
        secondaryFormLabel.setText(getRegistry().getString(
                "SecondaryForm.Label"));
        SWTUIHelper.setFont(secondaryFormLabel, SWT.BOLD);
        secondaryFormLabel.setImage(TCTypeRenderer.getTypeImage("Form", null));
        createSecondaryFormTCTable(parent);
    }
    
    private void createSecondaryFormTCTable(Composite parent)
    {
        Composite tCTableComposite = new Composite(parent, SWT.EMBEDDED);
        GridData gd_tCTableComposite = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        tCTableComposite.setLayoutData(gd_tCTableComposite);
        
        m_secondaryFormTable = createTCTable();
        JScrollPane scrollPane = new JScrollPane(m_secondaryFormTable);
        
        SWTUIUtilities.embed(tCTableComposite, scrollPane, false);
        
    }
    
    private TCTable createTCTable()
    {
        
        // TCSession tcSession = (TCSession) AIFUtility.getCurrentApplication()
        // .getSession();
        String[] propertyNames = getRegistry().getString(
                "SecondaryFormsPanel.PropertyNames").split(",");
        String[] colNames = getRegistry().getString(
                "SecondaryFormsPanel.ColumnNames").split(",");
        TCTable tCTable = new TCTable(propertyNames, colNames);
        tCTable.setShowVerticalLines(true);
        tCTable.setAutoCreateColumnsFromModel(false);
        tCTable.setAutoResizeMode(tCTable.AUTO_RESIZE_ALL_COLUMNS);
        
        tCTable.setVisible(true);
        tCTable.getTableHeader().setBorder(
                UIManager.getBorder("TableHeader.cellBorder"));
        tCTable.getTableHeader().setVisible(false);
        tCTable.getTableHeader().setPreferredSize(new Dimension(-1, 0));
        
        hideColumns(tCTable);
        
        // set icon renderer
        int colIndex = tCTable.getColumnIndex(getRegistry().getString(
                "SecondaryFormsPanel.ColumnsToRender"));
        TableColumn column = tCTable.getColumnModel().getColumn(colIndex);
        column.setCellRenderer(new PartIDIconRenderer());
        return tCTable;
        
    }
    
    private void hideColumns(TCTable tCTable)
    {
        // hid col
        String[] hideColumns = getRegistry().getStringArray("SecondaryFormsPanel.ColumnsToHide");
        for(String column : hideColumns)
        {
            int colIndex = tCTable.getColumnIndex(column);
            TableUtils.hideTableColumn(tCTable, colIndex);
        }
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    public void dispose()
    {
        if (m_iTableComparator != null)
        {
            m_iTableComparator.unregisterToComparator(SECONDARYFORMS_CONTEXT);
        }
        super.dispose();
    }

    @Override
    public void updateTable(PublishEvent event)
    {
        CompareOutputBean bean = null;
        TablePainter painter = null;
        if (event != null)
        {
            bean = (CompareOutputBean) event.getNewValue();
            
            if (event.getPropertyName().equals(SECONDARYFORMS_CONTEXT)
                    && this == bean.getObject())
            {
                painter = new TablePainter(bean);
            }
        }
        else
        {
            painter = new TablePainter(bean);
        }
        painter.paintTable(m_secondaryFormTable);
    }
    
}
