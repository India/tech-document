package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractExpandableUIPanel;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class AttachmentsPanel extends AbstractExpandableUIPanel implements INOVFormLoadSave
{

    private ChangeDescriptionAttachmentsPanel m_changeDescriptionAttachments;
    private ReferenceAttachmentsPanel m_referenceAttachments;
    private Registry m_registry;
    
    public AttachmentsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        // TODO Auto-generated constructor stub
    }

    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_changeDescriptionAttachments.load(propMap);
        m_referenceAttachments.load(propMap);
        return false;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_changeDescriptionAttachments.save(propMap);
        m_referenceAttachments.save(propMap);
        return false;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        m_changeDescriptionAttachments.validate(propMap);
        m_referenceAttachments.validate(propMap);
        return false;
    }

    @Override
    public void setEnabled(boolean flag)
    {
        m_changeDescriptionAttachments.setEnabled(flag);
        m_referenceAttachments.setEnabled(flag);
        
    }
    
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        
        GridLayout gl_l_composite = new GridLayout(2, true);
        gl_l_composite.horizontalSpacing = 20;
        gl_l_composite.marginHeight = 0;
        l_composite.setLayout(gl_l_composite);
        
        
        getExpandableComposite().setText( getRegistry().getString("Attachments.Label"));
        SWTUIHelper.setFont(getExpandableComposite(), SWT.BOLD);
       
        
        m_changeDescriptionAttachments = new ChangeDescriptionAttachmentsPanel(l_composite, SWT.NONE);
        m_referenceAttachments = new ReferenceAttachmentsPanel(l_composite, SWT.NONE);
        
        
        m_changeDescriptionAttachments.createUI();
        m_referenceAttachments.createUI();
        
        return true;
    }

    @Override
    public boolean reload() throws TCException 
    {   
        m_changeDescriptionAttachments.reload();
        m_referenceAttachments.reload();
        return false;
    }
}
