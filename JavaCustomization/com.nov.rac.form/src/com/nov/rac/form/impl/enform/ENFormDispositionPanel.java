package com.nov.rac.form.impl.enform;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVDisposition;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.NIDLovPopupDialog;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.controls.SWTLovPopupButton;
import com.teamcenter.rac.common.controls.SWTLovPopupDialog;
import com.teamcenter.rac.common.propertyicon.binding.PropertyMap;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ENFormDispositionPanel extends AbstractUIPanel implements
INOVDisposition
{
    private Text m_dispositionText = null;
    private Composite m_dispositionPanelComposite = null;
    private Composite m_editClassificationComposite = null;
    private Composite m_edidBOMcomposite = null;
    private Button m_editBOMButton = null;
    private Button m_editClassificationButton = null;
    private Button m_copyDispostionButton = null;
    
    private SWTLovPopupButton m_inFieldLovPopupButton = null;
    private SWTLovPopupButton m_assembledLovPopupButton = null;
    private SWTLovPopupButton m_inInventoryLovPopupButton = null;
    private SWTLovPopupButton m_inProcessLovPopupButton = null;
    
    private Registry m_registry = null;
    private IPropertyMap m_propMap = null;
    private Map<String, IPropertyMap> m_allPropMaps = null;
    private String m_defaultValue = null;
    private Image m_popupButtonimage = null;
    private Text m_inFieldtext;
    private Text m_inProcesstext;
    private Text m_inInventorytext;
    private Text m_assembledtext;
    
    public ENFormDispositionPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        l_composite.setLayout(new GridLayout(1, true));
        
        m_popupButtonimage = new Image(l_composite.getDisplay(), getRegistry()
                .getImage("ENFormDispositionPanel.LOVPopupImage"), SWT.NONE);
        
        createDispositionPanelComposite(l_composite);
        for (int i = 0; i < 8; i++)
        {
            createEmptyControlOnGrid(m_dispositionPanelComposite);
        }
        createDispositionLabel();
        createEmptyControlOnGrid(m_dispositionPanelComposite);
        createDispositionInstructionLabel();
        
        for (int i = 0; i < 4; i++)
        {
            createEmptyControlOnGrid(m_dispositionPanelComposite);
        }
        createInProcessLabel();
        createInProcessPopupButton();
        createDispostionText();
        createEmptyControlOnGrid(m_dispositionPanelComposite);
        createInInventoryLabel();
        createInInventoryPopupButton();
        createEmptyControlOnGrid(m_dispositionPanelComposite);
        createAssembledLabel();
        createAssembledPopupButton();
        createEmptyControlOnGrid(m_dispositionPanelComposite);
        createInFieldLabel();
        createInFieldPopupButton();
        for (int i = 0; i < 9; i++)
        {
            createEmptyControlOnGrid(m_dispositionPanelComposite);
        }
        createCopyDispostionButton();
        for (int i = 0; i < 6; i++)
        {
            createEmptyControlOnGrid(m_dispositionPanelComposite);
        }
        // for (int i = 0; i < 8; i++)//leave a blank space
        // {
        // CreateEmptyControlOnGrid(m_dispositionPanelComposite);
        // }
        // createEditClassificationComposite();
        // createClassificationMessageLabel();
        // createEditClassificationButton();
        //
        // for (int i = 0; i < 8; i++)
        // {
        // CreateEmptyControlOnGrid(m_dispositionPanelComposite);
        // }
        //
        // createEditBOMComposite();
        //
        // createBillOfMaterialsLabel();
        // for (int i = 0; i < 5; i++)
        // {
        // CreateEmptyControlOnGrid(m_edidBOMcomposite);
        // }
        //
        // createEditBOMButton();
        
        return true;
    }
    
    public void load(Map<String, IPropertyMap> allPropMaps)
    {
        m_allPropMaps = allPropMaps;
    }
//    
//    private void createEditBOMButton()
//    {
//        m_editBOMButton = new Button(m_edidBOMcomposite, SWT.NONE);
//        m_editBOMButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
//                false, 1, 1));
//        m_editBOMButton.setText(getRegistry().getString(
//                "ENFormDispositionPanel.EditBOMButton"));
//        
//    }
//    
//    private void createBillOfMaterialsLabel()
//    {
//        Label lblBillOfMaterials = new Label(m_edidBOMcomposite, SWT.NONE);
//        lblBillOfMaterials.setBounds(0, 0, 55, 15);
//        lblBillOfMaterials.setText(getRegistry().getString(
//                "ENFormDispositionPanel.BillOfMaterialsLabel"));
//        
//    }
//    
//    private void createEditBOMComposite()
//    {
//        m_edidBOMcomposite = new Composite(m_dispositionPanelComposite,
//                SWT.NONE);
//        m_edidBOMcomposite.setLayout(new GridLayout(7, true));
//        m_edidBOMcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
//                false, false, 8, 1));
//        
//    }
//    
//    private void createEditClassificationButton()
//    {
//        m_editClassificationButton = new Button(m_editClassificationComposite,
//                SWT.NONE);
//        m_editClassificationButton.setText(getRegistry().getString(
//                "ENFormDispositionPanel.EditClassificationButton"));
//        m_editClassificationButton.setLayoutData(new GridData(SWT.LEFT,
//                SWT.CENTER, false, false, 1, 1));
//        
//    }
//    
//    private void createClassificationMessageLabel()
//    {
//        Label lblNewLabel_5 = new Label(m_editClassificationComposite, SWT.NONE);
//        lblNewLabel_5.setText(getRegistry().getString(
//                "ENFormDispositionPanel.ClassificationMessageLabel"));
//        lblNewLabel_5.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
//                false, 1, 1));
//    }
//    
//    private void createEditClassificationComposite()
//    {
//        m_editClassificationComposite = new Composite(
//                m_dispositionPanelComposite, SWT.NONE);
//        GridLayout gl_editClassificationComposite = new GridLayout(2, false);
//        gl_editClassificationComposite.marginHeight = 0;//
//        gl_editClassificationComposite.marginWidth = 0;//
//        gl_editClassificationComposite.verticalSpacing = 1;// check
//        m_editClassificationComposite.setLayout(gl_editClassificationComposite);
//        m_editClassificationComposite.setLayoutData(new GridData(SWT.FILL,
//                SWT.FILL, false, false, 8, 1));
//        
//    }
    
    private void createCopyDispostionButton()
    {
        m_copyDispostionButton = new Button(m_dispositionPanelComposite,
                SWT.NONE);
        m_copyDispostionButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
                true, false, 2, 1));
        m_copyDispostionButton.setText(getRegistry().getString(
                "ENFormDispositionPanel.CopyDispositionButton"));
        
        m_copyDispostionButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                String[] propertyNames = null;
                propertyNames = m_registry.getString(
                        "ENFormDispositionPanel.PropertyNames").split(",");
                Collection<IPropertyMap> allMaps = m_allPropMaps.values();
                
                for (IPropertyMap currentMap : allMaps)
                {
                    for (int inx = 0; inx < propertyNames.length; inx++)
                    {
                        currentMap.setString(propertyNames[inx],
                                m_propMap.getString(propertyNames[inx]));
                    }
                }
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private void createInFieldPopupButton()
    {
        Object[] retObj = createSWTPopupWithTextInComposite();
        m_inFieldLovPopupButton = (SWTLovPopupButton) retObj[0];
        m_inFieldtext = (Text) retObj[1];
        createModifyListenerToSaveToPropMap(m_inFieldtext, "infield");
    }
    
    private void createModifyListenerToSaveToPropMap(final Text text,
            final String property)
    {
        text.addModifyListener(new ModifyListener()
        {
            
            @Override
            public void modifyText(ModifyEvent arg0)
            {
                m_propMap.setString(property, text.getText());
                
            }
        });
    }
    
    private void createInFieldLabel()
    {
        Label inFieldLabel = new Label(m_dispositionPanelComposite, SWT.NONE);
        inFieldLabel.setText(getRegistry().getString(
                "ENFormDispositionPanel.InFieldLabel"));
        inFieldLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
    }
    
    private void createAssembledPopupButton()
    {
        
        Object[] retObj = createSWTPopupWithTextInComposite();
        m_assembledLovPopupButton = (SWTLovPopupButton) retObj[0];
        m_assembledtext = (Text) retObj[1];
    }
    
    private void createAssembledLabel()
    {
        Label assembledLabel = new Label(m_dispositionPanelComposite, SWT.NONE);
        assembledLabel.setText(getRegistry().getString(
                "ENFormDispositionPanel.AssembledLabel"));
        assembledLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
        
    }
    
    private void createInInventoryPopupButton()
    {
        Object[] retObj = createSWTPopupWithTextInComposite();
        m_inInventoryLovPopupButton = (SWTLovPopupButton) retObj[0];
        m_inInventorytext = (Text) retObj[1];
        createModifyListenerToSaveToPropMap(m_inInventorytext, "ininventory");
        
    }
    
    private void createInInventoryLabel()
    {
        Label inInventoryLabel = new Label(m_dispositionPanelComposite,
                SWT.NONE);
        inInventoryLabel.setText(getRegistry().getString(
                "ENFormDispositionPanel.InInventoryLabel"));
        inInventoryLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
    }
    
    private void createDispostionText()
    {
        m_dispositionText = new Text(m_dispositionPanelComposite, SWT.BORDER
                | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
        m_dispositionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                false, 5, 4));
        m_dispositionText.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        m_dispositionText.addFocusListener(new FocusListener()
        {
            
            @Override
            public void focusLost(FocusEvent arg0)
            {
                m_propMap.setString("dispinstruction",
                        m_dispositionText.getText());
            }
            
            @Override
            public void focusGained(FocusEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
        });
        
    }
    
    private void createInProcessPopupButton()
    {
        
        Object[] retObj = createSWTPopupWithTextInComposite();
        m_inProcessLovPopupButton = (SWTLovPopupButton) retObj[0];
        m_inProcesstext = (Text) retObj[1];
        createModifyListenerToSaveToPropMap(m_inProcesstext, "inprocess");
    }
    
    private void createInProcessLabel()
    {
        Label inProcessLabel = new Label(m_dispositionPanelComposite, SWT.NONE);
        inProcessLabel.setText(getRegistry().getString(
                "ENFormDispositionPanel.InProcessLabel"));
        inProcessLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
    }
    
    private void createDispositionInstructionLabel()
    {
        Label dispositionInstructionLabel = new Label(
                m_dispositionPanelComposite, SWT.NONE);
        dispositionInstructionLabel.setLayoutData(new GridData(SWT.FILL,
                SWT.CENTER, true, false, 2, 1));
        dispositionInstructionLabel.setText(getRegistry().getString(
                "ENFormDispositionPanel.DispositionInstructionLabel"));
        
    }
    
    private void createDispositionLabel()
    {
        Label dispositionLabel = new Label(m_dispositionPanelComposite,
                SWT.NONE);
        dispositionLabel.setText(getRegistry().getString(
                "ENFormDispositionPanel.DispositionLabel"));
        dispositionLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                false, 1, 1));
    }
    
    private void createDispositionPanelComposite(Composite l_composite)
    {
        m_dispositionPanelComposite = new Composite(l_composite, SWT.NONE);
        GridLayout gl_composite = new GridLayout(8, true);
        gl_composite.marginTop = 20;
        gl_composite.marginLeft = 10;
        m_dispositionPanelComposite.setLayout(gl_composite);
        m_dispositionPanelComposite.setLayoutData(new GridData(SWT.FILL,
                SWT.FILL, true, true, 1, 1));
        
    }
    
    private void createEmptyControlOnGrid(Composite parent)
    {
        Label emptyLabel = new Label(parent, SWT.NONE);
        emptyLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, 1, 1));
    }
    
    public TCComponentListOfValues getLOV()
    {
        TCComponentListOfValues lOV = null;
        try
        {
            String enOwningGroup = FormHelper.getTargetComponentOwningGroup();
            String lOVName = null;
            if (enOwningGroup.contains(m_registry.getString("PCG.Group")))
            {
                lOVName = m_registry
                        .getString("ENFormDispositionPanel.ENDisposition_PCGLOV");
                m_defaultValue = m_registry
                        .getString("ENFormDispositionPanel.PCGDefault");
            }
            else
            {
                lOVName = m_registry
                        .getString("ENFormDispositionPanel.ENDispositionLOV");
                m_defaultValue = m_registry
                        .getString("ENFormDispositionPanel.NA");
            }
            TCSession tcSession = (TCSession) AIFUtility
                    .getCurrentApplication().getSession();
            lOV = TCComponentListOfValuesType.findLOVByName(tcSession, lOVName);
            
        }
        catch (Exception e)
        {
            throw e;
        }
        return lOV;
    }
    
    public Object[] createSWTPopupWithTextInComposite()
    {
        // UI
        Composite composite = new Composite(m_dispositionPanelComposite,
                SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1));
        
        GridLayout gl_composite = new GridLayout(2, false);
        gl_composite.horizontalSpacing = 0;
        gl_composite.marginLeft = 0;
        gl_composite.marginRight = 0;
        composite.setLayout(gl_composite);
        
        Text text = new Text(composite, SWT.BORDER);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        text.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        text.setEditable(false);
        TCComponent l_tcComponent = null;
        Button button = new Button(composite, SWT.PUSH);
        button.setEnabled(true);
        
        final NIDLovPopupDialog lOVDialog = new NIDLovPopupDialog(
                m_dispositionPanelComposite.getShell(), SWT.NONE, null, null);
        SWTLovPopupButton sWTLovPopupButton = new SWTLovPopupButton(button,
                lOVDialog, null, null, l_tcComponent);
        
        GridData gd_sWTLovPopupButton = new GridData(SWT.RIGHT, SWT.CENTER,
                false, false, 1, 1);
        
        sWTLovPopupButton.getPopupButton().setLayoutData(gd_sWTLovPopupButton);
        sWTLovPopupButton.setIcon(m_popupButtonimage);
        // Listeners
        
        addListeners(sWTLovPopupButton, text);
        Object[] retObjects = new Object[2];
        retObjects[0] = sWTLovPopupButton;
        retObjects[1] = text;
        return retObjects;
    }
    
    private void addListeners(final SWTLovPopupButton sWTLovPopupButton,
            final Text text)
    {
        final NIDLovPopupDialog lOVDialog = (NIDLovPopupDialog) sWTLovPopupButton
                .getDialog();
        
        lOVDialog.setPropertyChangeListener(new PropertyChangeListener()
        {
            
            @Override
            public void propertyChange(
                    final PropertyChangeEvent propertyChangeEvent)
            {
                String l_propertyName = propertyChangeEvent.getPropertyName();
                if (l_propertyName.equals("NewLOVValueSelected"))
                {
                    Display.getDefault().asyncExec(new Runnable()
                    {
                        public void run()
                        {
                            text.setText(propertyChangeEvent.getNewValue()
                                    .toString());
                            lOVDialog.setLovPopupButton((sWTLovPopupButton));
                            
                        }
                    });
                    
                }
                
            }
            
        });
        
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        m_propMap = propMap;
        NIDLovPopupDialog lOVDialog = null;
        TCComponentListOfValues lOV = getLOV();
        
        String inprocess = PropertyMapHelper.handleNull(m_propMap
                .getString("inprocess"));
        if (inprocess == "")
        {
            inprocess = m_defaultValue;
        }
        lOVDialog = (NIDLovPopupDialog) m_inProcessLovPopupButton.getDialog();
        lOVDialog.setLovComponent(lOV);
        m_inProcesstext.setText(inprocess);
        
        String infield = PropertyMapHelper.handleNull(m_propMap
                .getString("infield"));
        if (infield == "")
        {
            infield = m_defaultValue;
        }
        lOVDialog = (NIDLovPopupDialog) m_inFieldLovPopupButton.getDialog();
        lOVDialog.setLovComponent(lOV);
        m_inFieldtext.setText(infield);
        
        String assembled = PropertyMapHelper.handleNull(m_propMap
                .getString("assembled"));
        if (assembled == "")
        {
            assembled = m_defaultValue;
        }
        lOVDialog = (NIDLovPopupDialog) m_assembledLovPopupButton.getDialog();
        lOVDialog.setLovComponent(lOV);
        m_assembledtext.setText(assembled);
        
        String ininventory = PropertyMapHelper.handleNull(m_propMap
                .getString("ininventory"));
        if (ininventory == "")
        {
            ininventory = m_defaultValue;
        }
        lOVDialog = (NIDLovPopupDialog) m_inInventoryLovPopupButton.getDialog();
        lOVDialog.setLovComponent(lOV);
        m_inInventorytext.setText(ininventory);
        
        m_dispositionText.setText(PropertyMapHelper.handleNull(m_propMap
                .getString("dispinstruction")));
        
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_dispositionText.setEditable(flag);
        m_inProcessLovPopupButton.getPopupButton().setEnabled(flag);
        m_inInventoryLovPopupButton.getPopupButton().setEnabled(flag);
        m_assembledLovPopupButton.getPopupButton().setEnabled(flag);
        m_inFieldLovPopupButton.getPopupButton().setEnabled(flag);
        m_copyDispostionButton.setEnabled(flag);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

	@Override
	public void load(String partId, Map<String, IPropertyMap> map)
			throws TCException {
		  m_allPropMaps = map;
		
	}
    
}
