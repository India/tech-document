package com.nov.rac.form.impl.enform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ContainerPartPropertiesPanel extends AbstractUIPanel implements
        INOVFormLoadSave, ISubscriber
{
    private static final int NO_OF_COLUMNS_MAIN_COMPOSITE = 3;
    private Registry m_registry;
    private PartPropertiesPanel m_partPropsPanel;
    private ChangeFormsTablePanel m_changeFormTable;
    
    public ContainerPartPropertiesPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        // ApplyStatusSOAHelper.applyStatus(item, "MFG");
        
        m_partPropsPanel.load(propMap);
        m_changeFormTable.load(propMap);
        // getComposite().pack();
        forceLayout(getComposite());
        return true;
    }
    
    private void forceLayout(Composite parent)
    {
        Control[] controls = parent.getChildren();
        for (Control control : controls)
        {
            
            if (control instanceof Composite)
            {
                forceLayout((Composite) control);
            }
            
        }
        parent.layout();
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        m_partPropsPanel.save(propMap);
        m_changeFormTable.save(propMap);
        
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        m_partPropsPanel.validate(propMap);
        m_changeFormTable.validate(propMap);
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_partPropsPanel.setEnabled(flag);
        m_changeFormTable.setEnabled(flag);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        GridLayout gl_l_composite = new GridLayout(1, false);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        
        l_composite.setLayout(gl_l_composite);
        
        Composite mainComposite = createMainComposite(l_composite);
        
        m_partPropsPanel = new PartPropertiesPanel(mainComposite, SWT.NONE);
        m_partPropsPanel.createUI();
        m_partPropsPanel.getComposite().setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
        m_changeFormTable = new ChangeFormsTablePanel(mainComposite, SWT.NONE);
        m_changeFormTable.createUI();
        m_changeFormTable.getComposite().setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        registerSubscriber();
        return true;
    }
    
    private Composite createMainComposite(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        GridLayout gl_composite = new GridLayout(NO_OF_COLUMNS_MAIN_COMPOSITE,
                true);
        composite.setLayout(gl_composite);
        return composite;
    }
    
    public void dispose()
    {
        unregisterSubscriber();
        super.dispose();
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public void update(final PublishEvent event)
    {
        if (!getComposite().isDisposed())
        {
            Object thisParent = getComposite().getParent();
            Object sourceParent = null;
            
            IPublisher eventPublisher = event.getSource();
            
            if (eventPublisher instanceof AbstractUIPanel)
            {
                Composite sourceComposite = ((AbstractUIPanel) eventPublisher)
                        .getComposite();
                sourceParent = sourceComposite.getParent();
            }
            
            if (event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_EDIT_BUTTON_SELECTED)
                    && sourceParent != null && thisParent.equals(sourceParent))
            {
                m_partPropsPanel.setEditable(true);
            }
            
            if (event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED))
            {
                setEnabled(false);
            }
            
            if (event.getPropertyName().equalsIgnoreCase(
                    GlobalConstant.EVENT_SAVE_SUCCESS_FLAG))
            {
                boolean success = (boolean) event.getNewValue();
                if (success)
                {
                    m_partPropsPanel.setEditable(false);
                }
                
            }
        }
    }
    
    private void unregisterSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, this);
        
        controller.unregisterSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, this);
        
        controller.unregisterSubscriber(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG,
                this);
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        
        controller.registerSubscriber(
                GlobalConstant.EVENT_EDIT_BUTTON_SELECTED, this);
        
        controller.registerSubscriber(
                GlobalConstant.EVENT_CANCEL_BUTTON_SELECTED, this);
        
        controller.registerSubscriber(GlobalConstant.EVENT_SAVE_SUCCESS_FLAG,
                this);
        
    }
    
}
