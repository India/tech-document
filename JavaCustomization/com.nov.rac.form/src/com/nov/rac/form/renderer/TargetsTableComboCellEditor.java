package com.nov.rac.form.renderer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import com.teamcenter.rac.common.TCTable;

public class TargetsTableComboCellEditor extends ComboCellEditor
{

    private TCTable m_table;
    private int m_row = 0;

    public TargetsTableComboCellEditor(String theLOVName)
    {
        super(theLOVName);
       
        addRowUpdateFireListner();
    }

    private void addRowUpdateFireListner()
    {
        JComboBox<String> comboBox = (JComboBox<String>) getComponent();
        
        comboBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent paramActionEvent)
            {
                if(m_table != null)
                {
                    ((AbstractTableModel)m_table.getModel()).fireTableRowsUpdated(m_row, m_row);
                }
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable jtable, Object obj,
            boolean focus, int row, int column)
    {
        if( m_table == null)
        {
            m_table = (TCTable) jtable;
        }
        m_row = row;
        
        Component comp = super.getTableCellEditorComponent(jtable, obj, focus, row, column);
        
        int colIndex = ((TCTable)m_table).getColumnIndex("hasDisposition");
        if( colIndex >= 0)
        {
            boolean isEditable = (Boolean) m_table.getValueAt(m_row, colIndex);
            comp.setEnabled( isEditable);
            //System.out.println("=============>> TargetsTableComboCellEditor :: "+isEditable);
        }
        
        return comp;
    }
    
}
