package com.nov.rac.form.renderer;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class ButtonCellRenderer extends JButton implements TableCellRenderer 
{
    private ImageIcon m_hasValueIcon;
    private ImageIcon m_noValueIcon;
    private JTable m_table;
    
    public ButtonCellRenderer(ImageIcon hasValueIcon, ImageIcon noValueIcon)
    {
        m_hasValueIcon = hasValueIcon;
        m_noValueIcon = noValueIcon;
        
        setIcon(hasValueIcon);
        
        setBorder(BorderFactory.createEmptyBorder());
        setContentAreaFilled(false);
        
        setOpaque(true);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
    {
        m_table = table;
        
        if (value != null && !((String) value).trim().isEmpty())
        {
            setIcon(m_hasValueIcon);
        }
        else
        {
            setIcon(m_noValueIcon);
        }
        
        return this;
    }
}
