package com.nov.rac.form.renderer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;
import java.util.HashMap;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.common.DispositionInstructionDialog;
import com.nov.rac.form.impl.enform.ENTargetsTableDialogPanel;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;

public class ButtonCellEditor extends AbstractCellEditor implements TableCellEditor
{
    private JButton m_button;
    private ImageIcon m_hasValueIcon;
    private ImageIcon m_noValueIcon;
    private int m_row;
    private TCTable m_table;
    private Object m_value;
    
    public ButtonCellEditor(ImageIcon hasValueIcon, ImageIcon noValueIcon)
    {
        m_hasValueIcon = hasValueIcon;
        m_noValueIcon = noValueIcon;
        m_button = new JButton();
        m_button.setIcon(m_hasValueIcon);
        
        m_button.setBorder(BorderFactory.createEmptyBorder());
        m_button.setContentAreaFilled(false);
        
        m_button.setFocusPainted(false);
        addListener();
    }
    
    private void addListener()
    {
        m_button.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                Display.getDefault().asyncExec(new Runnable()
              {
                  @Override
                  public void run()
                  {
                      int colIndex = ((TCTable)m_table).getColumnIndex("hasDisposition");
                      if( colIndex >= 0)
                      {
                          boolean isEditable = (Boolean) m_table.getValueAt(m_row, colIndex);
                          //m_button.setEnabled( isEditable);
                          
                          if(isEditable)
                          {
                              popupDispositionInstructionDialog();
                          }
                      }
                      
                  }
              });
            }
        });
    }
    
    private void popupDispositionInstructionDialog()
    {
        DispositionInstructionDialog dialog = new DispositionInstructionDialog( Display.getDefault().getActiveShell());//AIFUtility.getCurrentApplication().getDesktop().getShell());
        dialog.create();
        
        int objectTypeColInx = m_table.getColumnIndex("object_type");
        String objectTypeStr = (String) m_table.getValueAt(m_row, objectTypeColInx);
        
        int objectNameColInx = m_table.getColumnIndex("object_name");
        int itemIDColInx = m_table.getColumnIndex("item_id");
        int itemRevIDColInx = m_table.getColumnIndex("item_revision_id");
        
        String objectNameStr = (String) m_table.getValueAt(m_row, objectNameColInx);
        String itemIDStr = (String) m_table.getValueAt(m_row, itemIDColInx);
        String itemRevIDStr = (String) m_table.getValueAt(m_row, itemRevIDColInx);
        
        dialog.setObjectString( itemIDStr +"-"+ itemRevIDStr +";"+ objectNameStr);
        dialog.setObjectType(objectTypeStr);
        dialog.setDispositionInstruction((String) m_value);
        
        dialog.open();
        
        m_value = dialog.getDispositionInstruction();
        
        super.stopCellEditing();
    }
    

    public Component getTableCellEditorComponent(JTable table,
            Object value, boolean isFocus, int row,
            int column)
    {
        m_row = row;
        m_table = (TCTable) table;
        m_value = value;
        
        if (value != null && !value.toString().trim().isEmpty())
        {
            m_button.setIcon(m_hasValueIcon);
        }
        else
        {
            m_button.setIcon(m_noValueIcon);
        }
        
//        int colIndex = ((TCTable)m_table).getColumnIndex("hasDisposition");
//        if( colIndex >= 0)
//        {
//            boolean isEditable = (Boolean) m_table.getValueAt(row, colIndex);
//            m_button.setEnabled( isEditable);
//        }
        
        return m_button;
    }
    
    
    public Component getComponent()
    {
        return m_button;
    }

    @Override
    public Object getCellEditorValue()
    {
        return m_value;
    }
    
    @Override
    public boolean isCellEditable(EventObject arg0)
    {
//        boolean isEditable = false;
//        
//        int colIndex = m_table.getColumnIndex("hasDisposition");
//        isEditable = (boolean) m_table.getValueAt(m_row, colIndex);
//        
        return true;
    }
}
