package com.nov.rac.form.renderer;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellRenderer;
import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.aif.common.AIFTable;


public class AlternateIDContextRenderer extends JLabel implements TableCellRenderer
{
    @Override
    public Component getTableCellRendererComponent(JTable paramJTable,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {

        int contextColumnIndex = TableUtils.getColumnIndex("idcontext",
                (AIFTable) paramJTable);
        
        if(contextColumnIndex != -1)
        {
            String context = (String) paramJTable.getModel().getValueAt(row,
                    contextColumnIndex);
            
            setHorizontalAlignment(JLabel.CENTER);
            setBackground(new Color(160,160,160));
            setBorder(new MatteBorder(1, 4, 1,4, Color.WHITE));
            setOpaque(true);
            if(context.equalsIgnoreCase("RSOne"))
            {
                setText("Oracle");
            }
            else
            {
                setText(context);
            }
        }
        
        return this;
    }
    
}
