package com.nov.rac.form.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.aif.common.AIFTable;

public class TableCellColourRenderer extends DefaultTableCellRenderer
{
    //private static Color m_originalColor = null;
    private static final Color RED = new Color(252,190,200);
    private static final Color GREEN = new Color(181,253,179);
    private static final Color YELLOW = new Color(241,243,148);
    private static final Color BLUE = new Color(23,139,255);
    private static final Color DEFAULT = new Color(255,255,255);
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int col)
    {
        
        // Cells are by default rendered as a JLabel.
        JLabel label = (JLabel) super.getTableCellRendererComponent(table,
                value, isSelected, hasFocus, row, col);
        
       /* if(m_originalColor == null)
        {
            m_originalColor = label.getBackground();
        }*/
        // Get the status for the current row.
        int statusColumnIndex = TableUtils.getColumnIndex("status",
                (AIFTable) table);
        
        if(statusColumnIndex != -1)
        {
            String status = (String) table.getModel().getValueAt(row,
                    statusColumnIndex);
            
            if(!isSelected && !status.equals(" ") && GlobalConstant.COMPARE_NONCOMPARE_FLAG)
            {
                label.setBackground(DEFAULT);
                
                if (status.equals("add"))
                {
                    label.setBackground(GREEN);
                }
                else if (status.equals("remove"))
                {
                    label.setBackground(RED);
                }
                else if (status.equals("modify")) 
                {
                    label.setBackground(YELLOW);
                }
                
                label.setForeground(Color.BLACK);
            }
            else
            {
                if(isSelected)
                {
                    label.setBackground(BLUE);
                }
                else
                {
                    label.setBackground(DEFAULT);
                }
            }
           
        }
        // Return the JLabel which renders the cell.
        return label;
    }
}
