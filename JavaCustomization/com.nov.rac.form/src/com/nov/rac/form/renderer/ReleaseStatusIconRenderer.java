package com.nov.rac.form.renderer;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.table.DefaultRenderer;

public class ReleaseStatusIconRenderer extends DefaultRenderer implements TableCellRenderer
{
    @Override
    public Component getTableCellRendererComponent(JTable paramJTable,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        Component component = super.getTableCellRendererComponent(
                paramJTable, value, isSelected, hasFocus, row, column);
        

        
        String objectType = getStatusName((AIFTable) paramJTable, row); 
                
        ImageIcon imageIcon = TCTypeRenderer.getTypeIcon(objectType, null);
        setIcon(imageIcon);
        
        return component;
    }
    
    private String getStatusName(AIFTable table, int row)
    {
        String statusNameFull = "";
        
        int columnIndex = table.getColumnIndex("releas_status_list");
        
        //int columnIndex = TableUtils.getColumnIndex("release_status_list", table);
        if(columnIndex > 0)
        {
            String statusName = (String) table.getModel().getValueAt(row, columnIndex);
            
            statusNameFull = "release_status_list." + statusName;
            statusNameFull = "ReleaseStatus." + statusName;
        }
        
        System.out.println("=======>>>>>>>>>>>  " +statusNameFull);
        return statusNameFull;
    }
}
