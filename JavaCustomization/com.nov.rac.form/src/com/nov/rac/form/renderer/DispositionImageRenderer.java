package com.nov.rac.form.renderer;

import java.awt.Component;
import java.util.HashSet;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.table.DefaultRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class DispositionImageRenderer extends DefaultRenderer implements TableCellRenderer
{
    private Registry m_registry;
    private TCTable m_table;
    private ImageIcon m_dispFillIcon;
    private ImageIcon m_dispNotFillIcon;
    private Set<Integer> m_notFilledDispoCount = new HashSet<Integer>();

    public DispositionImageRenderer(Registry reg, ImageIcon dispFillIcon, ImageIcon dispNotFillIcon)
    {
        m_registry = reg;
        m_dispFillIcon = dispFillIcon;
        m_dispNotFillIcon = dispNotFillIcon;
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable paramJTable,
            Object value, boolean isSelected,
            boolean hasFocus, int row, int column)
    {
        Component l_component = super.getTableCellRendererComponent(paramJTable, value, isSelected, hasFocus, row, column);
        
        if(m_table == null)
        {
            m_table = (TCTable) paramJTable;
        }
        
        ImageIcon icon =  isDispFilled(row) ? m_dispFillIcon : m_dispNotFillIcon;
        
        setIcon(icon);
        setHorizontalAlignment(JLabel.CENTER);

        // update header text
        updateHeader(column);
     
        return l_component;
    } 
    
    private void updateHeader(int columnInx)
    {
        TableColumn column = m_table.getColumnModel().getColumn( columnInx );
        String dispName = m_registry.getString( "TargetsTableColumnName.DispositionImage" );
        
        dispName += " (";
        dispName += m_notFilledDispoCount.size();
        dispName += ")";
        
        column.setHeaderValue(dispName);
        m_table.getTableHeader().repaint();
    }

    private boolean isDispFilled(int rowIndex)
    {
        boolean hasDisposition = true;
        
        int colIndex = ((TCTable)m_table).getColumnIndex("hasDisposition");
        if( colIndex >= 0)
        {
            hasDisposition = (Boolean) m_table.getValueAt(rowIndex, colIndex);
        }
        
        String[] validateColumns = m_registry.getStringArray("TargetsTable.Disposition.IMAGE_VALIDATE_COLUMNS");
        int columnIndex = -1;
        for(String columnName : validateColumns)
        {
            columnIndex =  m_table.getColumnIndex( columnName );
            String value = (String) m_table.getValueAt( rowIndex, columnIndex);
            if( !value.trim().isEmpty() || !hasDisposition)
            {
                m_notFilledDispoCount.remove(new Integer(rowIndex));
                return true;
            }
        }
        m_notFilledDispoCount.add(new Integer(rowIndex) );
        return false;
    }
}
