package com.nov.rac.form.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.utilities.utils.LOVUtils;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.ColorUtilities;

public class ComboBoxCellRenderer implements TableCellRenderer
{
    private JComboBox<String> m_theUIBox;
    private String m_lovName;
    protected Color m_alternateBackground;
    private TCTable m_table;
    protected static final Border NO_FOCUS_BORDER = BorderFactory.createEmptyBorder(1, 2, 1, 2);
    
    private static final double LIGHTEN_VALUE = 0.45000000000000001D;
    private static final double CONTRAST_VALUE = -0.5D;
            
    public ComboBoxCellRenderer(String theLOVName)
    {
        m_alternateBackground = ColorUtilities.lighten(SystemColor.activeCaption, LIGHTEN_VALUE);
        m_alternateBackground = ColorUtilities.contrast(m_alternateBackground, CONTRAST_VALUE );
        m_alternateBackground = Color.YELLOW;
        
        m_lovName = theLOVName;
        
        m_theUIBox = new JComboBox<String>();
        
        // Load the LOV
        m_theUIBox.setOpaque(true);
        
        Vector<String> lovValues = loadLOV();
        
        // Populate the combobox with values.
        populateComponent(lovValues);
        
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        int colIndex = ((TCTable)table).getColumnIndex("hasDisposition");
        if( colIndex >= 0)
        {
            boolean isEditable = (Boolean) table.getValueAt( row, colIndex);
            m_theUIBox.setEnabled( isEditable);
        }
        
        String strValue = "";
        
        if (value != null && !value.toString().trim().isEmpty())
        {
            strValue = value.toString();
        }
        
        m_theUIBox.setSelectedItem(strValue);
        
        return m_theUIBox;
    }
    
    protected Vector<String> loadLOV()
    {
        Vector<String> vrsOneOrg = new Vector<String>();
        
        Vector<Object> lovValues = LOVUtils.getListOfValuesforLOV(m_lovName);
        
        // Need to improve it.
        for (Object eachObject : lovValues)
        {
            vrsOneOrg.add(eachObject.toString());
        }
        
        // Collections.addAll(vrsOneOrg, (String[])lovValues.toArray());
        
        return vrsOneOrg;
    }
    
    protected void populateComponent(Vector<String> lovValues)
    {
        m_theUIBox.setModel(new DefaultComboBoxModel<String>(lovValues));
        m_theUIBox.setVisible(true);
    }
    
    protected Component getCellComponent()
    {
        return m_theUIBox;
    }
}
