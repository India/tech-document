package com.nov.rac.form.renderer;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.common.table.DefaultRenderer;
import com.teamcenter.rac.kernel.TCComponent;

public class AlternateIDRenderer extends DefaultRenderer implements TableCellRenderer
{
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean arg2, boolean arg3, int row, int column)
    {
        String alt_id = "";
        
        if (value != null)
        {
            for (TCComponent altID : (TCComponent[]) value)
            {
                alt_id += altID.toString() + ", ";
            }
            
            // remove the last comma
            if (!alt_id.isEmpty())
            {
                alt_id = alt_id.substring(0, alt_id.lastIndexOf(","));
            }
        }
        return super.getTableCellRendererComponent(table, alt_id, arg2, arg3,
                row, column);
    }
}
