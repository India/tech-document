package com.nov.rac.form.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.form.GlobalConstant;
import com.nov.rac.utilities.common.TableUtils;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.table.DefaultRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class PartIDIconRenderer extends DefaultRenderer implements TableCellRenderer
{
        //private static Color m_originalColor = null;
        private static final Color RED = new Color(252,190,200);
        private static final Color GREEN = new Color(181,253,179);
        private static final Color YELLOW = new Color(241,243,148);
        private static final Color BLUE = new Color(23,139,255);
        private static final Color DEFAULT = new Color(255,255,255);
        
        @Override
        public Component getTableCellRendererComponent(JTable paramJTable,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column)
        {
            
            JLabel label = (JLabel) super.getTableCellRendererComponent(paramJTable,
                    value, isSelected, hasFocus, row, column);
            
           /* if(m_originalColor == null)
            {
                m_originalColor = label.getBackground();
            }*/
            
            int statusColumnIndex = TableUtils.getColumnIndex("status",
                    (AIFTable) paramJTable);
            
            if(statusColumnIndex != -1)
            {
                String status = (String) paramJTable.getModel().getValueAt(row,
                        statusColumnIndex);
                
                if(!isSelected && !status.equals(" ") && GlobalConstant.COMPARE_NONCOMPARE_FLAG)
                {
                    label.setBackground(DEFAULT);
                    
                    if (status.equals("add"))
                    {
                        label.setBackground(GREEN);
                    }
                    else if (status.equals("remove"))
                    {
                        label.setBackground(RED);
                    }
                    else if (status.equals("modify")) 
                    {
                        label.setBackground(YELLOW);
                    }
                    
                    label.setForeground(Color.BLACK);
                }
                else
                {
                    if(isSelected)
                    {
                        label.setBackground(BLUE);
                    }
                    else
                    {
                        label.setBackground(DEFAULT);
                    }
                }
            }
            
            int typeColumnIndex = TableUtils.getColumnIndex("object_type",
                    (AIFTable) paramJTable);
            
            String objectType = (String) paramJTable.getModel().getValueAt(row,
                    typeColumnIndex);
            
            TCSession session = (TCSession) AIFUtility.getDefaultSession();
            String strParentType =null ;
            try
            {
                TCComponent tcComponent = session.getTypeService().getTypeComponent(objectType);
                if(tcComponent!=null)
                {
                TCComponentType parentTypeComponent = tcComponent.getTypeComponent().getParent();
                 strParentType = parentTypeComponent.getName();
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
            ImageIcon imageIcon = TCTypeRenderer.getTypeIcon(
                    objectType, strParentType);
            setIcon(imageIcon);
            
            return label;
        }
}
