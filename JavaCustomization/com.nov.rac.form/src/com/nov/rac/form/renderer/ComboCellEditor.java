package com.nov.rac.form.renderer;

import java.awt.Component;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import com.nov.rac.utilities.utils.LOVUtils;

public class ComboCellEditor extends DefaultCellEditor
{
    
    private static final long serialVersionUID = 1L;
    
    private JComboBox<String> m_theComboBox;
    private String m_lovName;
    
    public ComboCellEditor(String theLOVName)
    {
        super(new JComboBox<Object>());
        
        m_lovName = theLOVName;
        
        m_theComboBox = (JComboBox<String>) getComponent();
        
        // Load the LOV
        Vector<String> lovValues = loadLOV();
        
        // Populate the combobox with values.
        populateComponent(lovValues);
    }
    
    protected Vector<String> loadLOV()
    {
        Vector<String> vrsOneOrg = new Vector<String>();
        
        Vector<Object> lovValues = LOVUtils.getListOfValuesforLOV(m_lovName);
        
        // Need to improve it.
        for(Object eachObject : lovValues)
        {
            vrsOneOrg.add(eachObject.toString());
        }
        
        //Collections.addAll(vrsOneOrg, (String[])lovValues.toArray());
        
        return vrsOneOrg;
    }
    
    protected void populateComponent(Vector<String> lovValues)
    {
        m_theComboBox.setModel(new DefaultComboBoxModel<String>(lovValues));
        m_theComboBox.setVisible(true);
    }
    
    public boolean shouldSelectCell(EventObject eventobject)
    {
        return true;
    }
    
    public Component getTableCellEditorComponent(JTable jtable, Object obj, boolean flag, int i, int j)
    {
        
        Object selectedObject = null;
        
        if (obj != null & !obj.toString().trim().isEmpty())
        {
            selectedObject = obj;
        }
        
        m_theComboBox.setSelectedItem(selectedObject);
        
        return m_theComboBox;
    }
    
    @Override
    public boolean isCellEditable(EventObject paramEventObject)
    {
        return true;
    }
}