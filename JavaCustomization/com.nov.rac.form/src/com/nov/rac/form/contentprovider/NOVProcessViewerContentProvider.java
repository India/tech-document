package com.nov.rac.form.contentprovider;

import org.eclipse.jface.viewers.Viewer;

import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyFactory;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVProcessViewerContentProvider extends ContentProvider
{
    public NOVProcessViewerContentProvider(Registry registry)
    {
        super(registry);
    }
    
    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {
        TCComponent form = FormHelper.getTargetComponent();
        String formType = form.getType();
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        // String emptyString = new String();
        
        m_propertyMap = new SimplePropertyMap(formType);
        
        NOVObjectPolicy formPolicy = null;
        NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(
                form);
        // 1.0 read property policy from registry input formtype,PROPERTY_POLICY
        
        NOVObjectPolicyFactory novObjectPolicyFactory = new NOVObjectPolicyFactory(
                form);
        
        // 2.0 set the property policy
        try
        {
            formPolicy = novObjectPolicyFactory.createObjectPolicy(theGroup,
                    getRegistry());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        // 3.0 read property names from registry input formtype.PROP_NAMES
        
        /*
         * m_formPropertyNames = getRegistry().getStringArray( formType + "." +
         * PROPERTY_NAMES, DELIMITER, emptyString);
         */
        
        m_formPropertyNames = RegistryUtils.getConfiguration(theGroup,formType
                + "." + PROPERTY_NAMES, getRegistry());
        
        // 4.0 get these property values
        // call datamanagement.getproperties() soa
        dmSOAHelper.getObjectProperties(form, m_formPropertyNames, formPolicy);
        
        try
        {
            PropertyMapHelper.componentToMap(form, m_formPropertyNames,
                    m_propertyMap);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        WorkflowProcess m_workflow = new WorkflowProcess(FormHelper.getTargetComponent(), getRegistry());
        WorkflowProcess.setWorkflow(m_workflow);
        
        System.out.println("inputChanged");
    }
}
