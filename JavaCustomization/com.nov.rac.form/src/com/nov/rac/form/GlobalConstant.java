/*
 * This file is mainly used to define static variables or properties thats needs to be published or subscribed
 * while using communication framework or any other similar type of purpose...
 */
package com.nov.rac.form;

public class GlobalConstant
{
    public final static String EVENT_REVISION_SELECTION_CHANGED = "revisionPropertyMap";
    public final static String EVENT_COMPARE_MODE_SELECTED= "openCompareMode";
    public final static String EVENT_TAB_CHANGED= "tabChanged";
    public final static String EVENT_UOM_CHANGED= "uomChanged";
    //public final static String RDD_PUBLISH_EVENT= "rddPublished";
    //public final static String RD_PUBLISH_EVENT= "rdPublished";
    public final static String DIALOG_PANE = "PANE";
    public final static String COMPARE_MODE = "compareMode";
    public final static String EVENT_TARGETS_TABLE_ROW_SELECTED = "TargetsTableRowSelected";
    public static final String EVENT_DISPOSITION_DATA_CHANGED = "DispositionDataChanged";
    public static final String EVENT_DISPOSITION_PANEL_COPY_ALL = "DispositionPanelCopyAll";
    public static final String EVENT_COPY_ALL_DISPOSITION_PROPERTY = "DispositionPanelCopyAll";
    public static boolean COMPARE_NONCOMPARE_FLAG = false;
	public static String STOPFORM_CHANGE = "nov4_change_type";
    public static String STOPREASON = "stopreason";
    public static String NOV4_STOPALL = "nov4_stopall";
    public static String STOPSHIPMENT = "stopshipment";
    public static String STOPWOANDPO = "stopwoandpo";
    public static String STOPINSTRUCTIONS = "stopinstructions";
    public static String NOV4_EXPECTED_RESOLUTION = "nov4_expected_resolution";
    public static String ITEMID = "item_id";
    public static String NOV4_SSO = "stopwip";
    public static String NOV4_STOP_COMMENT_VALS = "nov4_stopcomment_vals";
    public static String STOPCOMMENT = "stopcomment";
    public static String DISTRIBUTION = "distribution";
    public static String RELEASE_STATUS_LIST = "release_status_list";
    public static String TARGETDISPOSITION = "targetdisposition";
    public static String CURRENT_REV_ID = "current_revision_id";
    public final static String DOCUMENT_PUBLISH_EVENT= "documentPublished";
	public static final String EVENT_TARGETS_ADDED = "targetItemRevsAddedToWorkflow";
	public static final String EVENT_EDIT_BUTTON_SELECTED = "EditButtonSelected";
	public static final String EVENT_SAVE_BUTTON_SELECTED = "SaveButtonSelected";
	public static final String EVENT_SAVE_SUCCESS_FLAG = "SaveSuccessFlag";
    public static final String EVENT_DISPOSITION_INSTRUCTION_CHANGED = "DispositionInstructionChanged";
    public static final String EVENT_VALIDATE_PROPERTIES_SUCCESS = "ValidatePropertiesSuccess";
    public static final String EVENT_VALIDATE_PROPERTIES_ERRORS = "ValidatePropertiesErrors";
    public static final String EVENT_CANCEL_BUTTON_SELECTED = "CancelButtonSelected";
    public static final String EVENT_COMPARE_REVISION = "CompareRevisions";
    public static final String EVENT_VIEW_RDD_PDF = "ViewRDDPDF";
    public static final String EVENT_NOTIFY_ERP = "NotifyERP";
    public static final String MFG_DETAILS_EVENT = "manufacturer_details";
    public final static String DOCS_REVISION_SELECTION_CHANGED = "DocsrevisionPropertyMap";
    
    //Preferences
    public static final String PREF_CLASSIFICATION_DEFAULT_UOM = "NOV4_classification_default_uom_matric";
    
    public static final String STOP_ORDER_COMMENTS_LOV = "nov4_stopcomment_vals";
}