package com.nov.rac.form.viewerfactory;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.contentprovider.NOVFormViewerContentProvider;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.viewer.NOVFormViewer;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.common.tcviewer.factory.AbstractSWTViewerFactory;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.services.ISelectionMediatorService;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.OSGIUtil;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.viewer.IViewerEvent;
import com.teamcenter.rac.util.viewer.SubViewerListener;
import com.teamcenter.rac.util.viewer.ViewerEvent;

public class NOVFormViewerFactory extends AbstractSWTViewerFactory
{
    
    private static final String CONTENT_PROVIDER = "CONTENT_PROVIDER";
    private Registry m_mainRegistry = null;
    private Registry m_registry = null;
    private String REGISTRY = "REGISTRY";
    
    public NOVFormViewerFactory()
    {
        m_mainRegistry = Registry.getRegistry("com.nov.rac.form.forms");
    }
    
    @Override
    public void loadViewer(final SubViewerListener subviewerlistener)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    initializeCache();
                    NOVFormViewer viewer = new NOVFormViewer(getParent());
                    viewer.setRegistry(m_registry);
                    viewer.createUI();
                    IContentProvider contentProvider = getContentProvider(m_registry);
                    viewer.setContentProvider(contentProvider);
                    subviewerlistener.done(viewer);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    ViewerEvent viewerError = new ViewerEvent(this,
                            IViewerEvent.NOVIEWDATAFOUND);
                    subviewerlistener.error(viewerError);
                }
                finally
                {
                }
                
            }

        };
        Display.getDefault().asyncExec(runnable);
    }
    
    private void initializeCache()
    {
        TCComponentForm tcForm = (TCComponentForm) getSelectedComponent();
        if (tcForm != null)
        {
            String formType = tcForm.getType();
            
            FormHelper.setTargetComponent(tcForm);
            FormHelper.setTargetComponentType(formType);
            String registryPath = m_mainRegistry.getString(formType + "."
                    + REGISTRY);
            m_registry = Registry.getRegistry(registryPath);
            try
            {
                FormHelper.setTargetComponentOwningGroup(tcForm.getProperty(m_registry.getString("owningGroup.PROP")));
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    private TCComponent getSelectedComponent()
    {
        TCComponentForm tcForm = null;
        ISelectionMediatorService iselectionmediatorservice = (ISelectionMediatorService) OSGIUtil
                .getService(AifrcpPlugin.getDefault(),
                        ISelectionMediatorService.class);
        InterfaceAIFComponent[] interfaceAIFComponents = (InterfaceAIFComponent[]) iselectionmediatorservice
                .getTargetComponents();
        if (interfaceAIFComponents != null && interfaceAIFComponents.length > 0)
        {
             tcForm = (TCComponentForm) interfaceAIFComponents[0];
        }
        
        return tcForm;
    }
    
    private IContentProvider getContentProvider(Registry registry)
    {
        IContentProvider contentProvider = null;
        StringBuilder context = new StringBuilder();
        context.append(FormHelper.getTargetComponentType());
        context.append(".").append(CONTENT_PROVIDER);
        
        String[] className = RegistryUtils.getConfiguration(context.toString(), registry);
        Object[] params = new Object[1];
        params[0] = registry;
        
        if(className != null && className.length > 0)
        {
            try
            {
                contentProvider = (IContentProvider) Instancer.newInstanceEx(className[0], params);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
        }
        
        if(contentProvider == null)
        {
            contentProvider = new NOVFormViewerContentProvider(registry);
        }
        return contentProvider;
    }
}
