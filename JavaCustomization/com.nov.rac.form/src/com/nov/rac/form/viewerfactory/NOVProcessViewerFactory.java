package com.nov.rac.form.viewerfactory;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.MissingResourceException;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.form.contentprovider.NOVProcessViewerContentProvider;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.viewer.NOVProcessViewer;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.common.tcviewer.factory.AbstractSWTViewerFactory;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.services.ISelectionMediatorService;
import com.teamcenter.rac.util.OSGIUtil;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.viewer.IViewerEvent;
import com.teamcenter.rac.util.viewer.SubViewerListener;
import com.teamcenter.rac.util.viewer.ViewerEvent;
import com.teamcenter.rac.workflow.common.ProcessViewer;

public class NOVProcessViewerFactory extends AbstractSWTViewerFactory
{
    
    private Registry m_mainRegistry = null;
    private Registry m_registry = null;
    private String REGISTRY = "REGISTRY";
    
    public NOVProcessViewerFactory()
    {
        m_mainRegistry = Registry.getRegistry("com.nov.rac.form.forms");
    }
    
    @Override
    public void loadViewer(final SubViewerListener subviewerlistener)
    {
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    TCComponentForm tccomponentform = getTaskForm();
                    
                    // 1.0 Get the task type.
                    String targetType = getSelectedComponent().getType();
                    
                    // 2.0 If Task Type is mentioned in com.teamcenter.rac.common.tcviewer.tcviewer.properties
                    // file. : TaskPerformViewer.TYPES
                    String[] taskPerformViewerTypes = m_mainRegistry
                            .getStringArray("TaskPerformViewer.TYPES");
                    List<String> taskPerformViewerTypesList = new ArrayList<String>();
                    taskPerformViewerTypesList = Arrays
                            .asList(taskPerformViewerTypes);
                    
                    // 2.1 Create an instance of OOTB ProcessViewer and retunr
                    // that object. eg: subviewerlistener.done(new ProcessViewer() );
                    
                    if (tccomponentform == null || taskPerformViewerTypesList.contains(targetType))
                    {
                        final ProcessViewer processViewer = invokeProcessViewer();
                        
                        subviewerlistener.done(processViewer);
                    }
                    else
                    {
                        // 3.0 If TaskType of selected task is not available in
                        // TaskPerformViewer.TYPES, then create instance of
                        // NOVProcessViewer
                        NOVProcessViewer viewer = invokeNOVProcessViewer();
                        subviewerlistener.done(viewer);
                    }
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    ViewerEvent viewerError = new ViewerEvent(this,
                            IViewerEvent.NOVIEWDATAFOUND);
                    subviewerlistener.error(viewerError);
                }
                finally
                {
                }
                
            }
            
        };
        
        Display.getDefault().asyncExec(runnable);
    }
    
    private void initializeCache()
    {
        try
        {
            TCComponentForm tccomponentform = getTaskForm();
            
            if (tccomponentform != null)
            {
                String formType = tccomponentform.getType();
                
                FormHelper.setTargetComponent(tccomponentform);
                FormHelper.setTargetComponentType(formType);
                String registryPath = m_mainRegistry.getString(formType + "."
                        + REGISTRY);
                m_registry = Registry.getRegistry(registryPath);
                
                FormHelper.setTargetComponentOwningGroup(tccomponentform
                        .getProperty(m_registry.getString("owningGroup.PROP")));
            }
        }
        catch (MissingResourceException e)
        {
            e.printStackTrace();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }

    private TCComponentForm getTaskForm() throws TCException
    {
        TCComponent task = getSelectedComponent();
        TCComponentForm tccomponentform = null;
        
        if (task instanceof TCComponentTask)
        {
            tccomponentform = ((TCComponentTask) task).getTaskForm();
        }
        return tccomponentform;
    }
    
    private TCComponent getSelectedComponent()
    {
        TCComponent selectedComponenet = null;
        ISelectionMediatorService iselectionmediatorservice = (ISelectionMediatorService) OSGIUtil
                .getService(AifrcpPlugin.getDefault(),
                        ISelectionMediatorService.class);
        InterfaceAIFComponent[] interfaceAIFComponents = (InterfaceAIFComponent[]) iselectionmediatorservice
                .getTargetComponents();
        
        selectedComponenet = (TCComponent) interfaceAIFComponents[0];
        return selectedComponenet;
    }

    private ProcessViewer invokeProcessViewer()
    {
        getParent().setLayout(new GridLayout(1, false));
        final Composite composite = new Composite(getParent(),
                SWT.EMBEDDED | SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true,
                true, 1, 1);
        composite.setLayoutData(gridData);
        
        final ProcessViewer processViewer = new ProcessViewer();
        
        if (processViewer instanceof JPanel)
        {
            SwingUtilities.invokeLater(new Runnable()
            {
                
                @Override
                public void run()
                {
                    ((JPanel) processViewer).setVisible(true);
                    ((JPanel) processViewer).setEnabled(true);
                    ((JPanel) processViewer)
                            .setMinimumSize(new Dimension(0, 0));
                }
            });
            
            getParent().getDisplay().asyncExec(new Runnable()
            {
                
                @Override
                public void run()
                {
                    SWTUIUtilities.embed(composite,
                            processViewer, false);
                    
                }
            });
            
        }
        return processViewer;
    }

    private NOVProcessViewer invokeNOVProcessViewer()
    {
        initializeCache();
        
        NOVProcessViewer viewer = new NOVProcessViewer(
                getParent());
        viewer.setRegistry(m_registry);
        viewer.setSelectedComponent(getSelectedComponent());
        viewer.createUI();
        viewer.setContentProvider(new NOVProcessViewerContentProvider(
                m_registry));
        return viewer;
    }
    
}
