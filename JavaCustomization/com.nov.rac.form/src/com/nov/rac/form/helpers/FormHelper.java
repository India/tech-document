package com.nov.rac.form.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.custom.CTabFolder;

import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.deleteHelper.CreateRelationObjectHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAccessControlService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;

public class FormHelper
{
    private static TCComponent m_targetComponent = null;
    private static String m_targetComponentType = null;
    private static String m_dataModelName = null;
    private static String m_owningGroup = null;
   // private static Composite mainComposite = null;
    private static CTabFolder m_folder = null;
    private static String m_dialogDataModelName = null;
    
    public static void setTargetComponent(TCComponent form)
    {
        m_targetComponent = form;
    }
    
    public static void setTargetComponentType(String formType)
    {
        m_targetComponentType = formType;
    }
    
    public static TCComponent getTargetComponent()
    {
        return m_targetComponent;
    }
    
    public static String getTargetComponentType()
    {
        return m_targetComponentType;
    }
    
    public static boolean hasContent(String string)
    {
        boolean flag = true;
        if (null == string || string.isEmpty() || "".equals(string.trim()))
            flag = false;
        return flag;
    }
    
    public static boolean hasContent(TCComponent tag)
    {
        boolean flag = true;
        if (null == tag)
            flag = false;
        return flag;
    }
    
    public static boolean hasContent(Double doubleValue)
    {
        boolean flag = true;
        if (null == doubleValue || doubleValue == 0)
            flag = false;
        return flag;
    }
    
    public static boolean isValidEmail(String mailID)
    {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*"
                + "@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        
        Pattern p = Pattern.compile(EMAIL_PATTERN);
        Matcher m = p.matcher(mailID);
        
        if (!m.find())
        {
            return false;
        }
        
        return true;
    }
    
    public static String getDataModelName()
    {
        return m_dataModelName;
    }
    
    public static void setDataModelName(String dataModelName)
    {
        m_dataModelName = dataModelName;
    }
    
    public static void setTargetComponentOwningGroup( String theGroup)
    {
        m_owningGroup = theGroup;
    }
    
    public static String getTargetComponentOwningGroup()
    {
        return m_owningGroup;
    }

    public static void setFolder(CTabFolder folder)
    {
        m_folder = folder;
    }
    
    public static CTabFolder getFolder()
    {
        return m_folder;
    }
    
    public static String getDialogDataModelName()
    {
        return m_dialogDataModelName;
    }
    
    public static void setDialogDataModelName(String dialogDataModelName)
    {
        m_dialogDataModelName = dialogDataModelName;
    }

    public static TCComponent[] getItemRevisions(TCComponent tcComponent) throws TCException
    {
        TCComponentItemRevision tcComponentItemRevision = (TCComponentItemRevision)tcComponent;
        TCComponentItem tcComponentItem = tcComponentItemRevision.getItem();
        TCComponent[] revisionList = tcComponentItem
                .getReferenceListProperty("revision_list");
        return revisionList;
    }
    
    public static boolean isLatestItemRevision(TCComponent tcComponent) 
    {
        boolean isLatestRevision = false;
        try
        {
            TCComponentItemRevision tcComponentItemRevision = (TCComponentItemRevision)tcComponent;
            TCComponentItem tcComponentItem = tcComponentItemRevision.getItem();
            if(tcComponent.equals(tcComponentItem.getLatestItemRevision()))
            {
                isLatestRevision = true;
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isLatestRevision;
    }
    
    /*function: deleteRelation(...)
     * Description: This function deletes the relation between primary object and secondary object.
     * It is call on minus and add/replace button exist in RD and RDD panel respectively.
     * After deleting the relation,primary object must be refreshed.
     * 
     */
    public static boolean deleteRelation(TCComponent primaryObject,TCComponent[] secondaryObjects,String relationName)
    {
        boolean isRelationDeleted = false;
        try
        {
            DeleteObjectHelper delRelationObjectHelper[] = new DeleteObjectHelper[secondaryObjects.length];
            for (int i = 0; i <secondaryObjects.length; i++)
            {
                delRelationObjectHelper[i] = new DeleteObjectHelper(
                        DeleteObjectHelper.OPERATION_DELETE_RELATION);
                delRelationObjectHelper[i].setPrimaryObject(primaryObject);
                delRelationObjectHelper[i].setSecondaryObject(secondaryObjects[i]);
                delRelationObjectHelper[i].setRelationName(relationName);
            }
            
            CreateObjectsSOAHelper.deleteRelationObjects(delRelationObjectHelper);
            isRelationDeleted = true;
            primaryObject.refresh();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isRelationDeleted;
    }
    
    /*function: createRelation(...)
     * Description: This method is invoked on plus and add/replace button exist in RD and RDD panel respectively.  
     * This method creates the relationship between primary object and secondary object based on relation name
     * before creating relation it checks whether secondary object already exist or not
     */
    public static boolean createRelation(TCComponent primaryObject,TCComponent[] secondaryObjects,String relationName)
    {
        boolean isRelationCreated = false;
        try
        {
            TCComponent[] existingSecondaryObjects = primaryObject.getRelatedComponents(relationName);
             List<TCComponent> existingSecondaryObjectList = Arrays.asList(existingSecondaryObjects);
             
            List<CreateRelationObjectHelper> creRelationObjectHelperList = new ArrayList<CreateRelationObjectHelper>();
            CreateRelationObjectHelper creRelationObjectHelper = null;
            
            for (int i = 0; i <secondaryObjects.length; i++)
            {
                if(!isSecondaryObjectExist(existingSecondaryObjectList,secondaryObjects[i]))
                {
                    creRelationObjectHelper = new CreateRelationObjectHelper(CreateRelationObjectHelper.OPERATION_CREATE_RELATION);
                    creRelationObjectHelper.setPrimaryObject(primaryObject);
                    creRelationObjectHelper.setSecondaryObject(secondaryObjects[i]);
                    creRelationObjectHelper.setRelationName(relationName);
                    creRelationObjectHelperList.add(creRelationObjectHelper);
                }
            }
            
            if(creRelationObjectHelperList.size() > 0)
            {
                CreateRelationObjectHelper[] creRelationObjectHelpeArray = new CreateRelationObjectHelper[creRelationObjectHelperList.size()];
                CreateObjectsSOAHelper.createRelationObjects(creRelationObjectHelperList.toArray(creRelationObjectHelpeArray));
                isRelationCreated = true;
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }  
        
        return isRelationCreated;
    }
    
    private static boolean isSecondaryObjectExist(List<TCComponent> existingSecondaryObjectList,TCComponent tcComponent)
    {
        boolean isExist = false;
        if(existingSecondaryObjectList.contains(tcComponent))
        {
            isExist = true;
        }
        return isExist;
    }
    
    public static boolean hasWriteAccess(TCComponent tcComponent)
    {
        boolean hasPrivilege = false;
        if(tcComponent!= null)
        {
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            TCAccessControlService accessCtrlService = tcSession.getTCAccessControlService();
            try
            {
                hasPrivilege = accessCtrlService.checkPrivilege(tcComponent, TCAccessControlService.WRITE);
            }
            catch (TCException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        return hasPrivilege;
    }
    
}
