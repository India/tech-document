package com.nov.rac.form.common;

import java.awt.Window;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCTaskState;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.workflow.commands.complete.CompleteOperation;
import com.teamcenter.rac.workflow.commands.failure.FailureOperation;

public class FormTaskPanel extends AbstractUIPanel
{
    
    private Text m_processDescription = null;
    private Text m_comments = null;
    private Button m_btnSave = null;
    private Button m_btnClear = null;
    private Window m_window;
    private Button m_btnComplete = null;
    private Registry m_registry = null;
    
    private TCComponentTask m_task = null;
    private Button m_btnPrint = null;
    
    public FormTaskPanel(Composite parent, int style)
    {
        super(parent, style);
        m_window = AIFUtility.getActiveDesktop();
        m_registry = Registry.getRegistry(this);
    }
    
    public void setSelectedComponent(TCComponent tcComponent)
    {
        m_task = (TCComponentTask) tcComponent;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        final Composite l_composite = getComposite();
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        l_composite.setLayoutData(gridData);
        
        GridLayout gl_l_composite = new GridLayout(6, true);
        l_composite.setLayout(gl_l_composite);
        
        createProcessDescription(l_composite);
        createComments(l_composite);
        createDoneLabel(l_composite);
        createCompleteRadioButton(l_composite);
        createSeparator(l_composite);
        createButtons(l_composite);
        
        return false;
    }
    
    private void createPrintButton(Composite parent)
    {
        Registry registry = Registry
                .getRegistry("com.teamcenter.rac.commands.open.open");
        m_btnPrint = new Button(parent, SWT.PUSH);
        m_btnPrint.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, true,
                1, 6));
        m_btnPrint.setImage(registry.getImage("viewer.PRINTICON"));
        m_btnPrint.setToolTipText(registry.getString("viewer.PRINTTIP"));
        m_btnPrint.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                InterfaceAIFComponent ainterfaceaifcomponent[] = { FormHelper.getTargetComponent() };
                Registry registry = Registry
                        .getRegistry("com.teamcenter.rac.common.actions.actions");
                
                AbstractAIFCommand abstractaifcommand = null;
                
                abstractaifcommand = (AbstractAIFCommand) registry
                        .newInstanceFor("printCommand", new Object[] {
                                ainterfaceaifcomponent, Boolean.TRUE });
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
                // TODO Auto-generated method stub
                
            }
        });
    }
    
    private void createSeparator(Composite parent)
    {
        Label separator = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
        GridData separatorGridData = new GridData(SWT.FILL, SWT.FILL, true,
                false, 6, 1);
        separator.setLayoutData(separatorGridData);
    }
    
    private void createButtons(Composite parent)
    {
        m_btnSave = new Button(parent, SWT.NONE);
        m_btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 6, 1));
        m_btnSave.setText(m_registry.getString("Save.Label"));
        
        // m_btnClear = new Button(composite, SWT.NONE);
        // m_btnClear.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
        // false, 1, 1));
        // m_btnClear.setText("Clear");
        
        // addButtonsListener();
        
    }
    
    public void addButtonsListener(SelectionListener selectionListener)
    {
        m_btnSave.addSelectionListener(selectionListener);
    }
    
    private void createCompleteRadioButton(Composite parent)
    {
        createEmptyLabel(parent);
        createEmptyLabel(parent);
        //createPrintButton(parent);
        m_btnComplete = new Button(parent, SWT.RADIO);
        GridData btngridData = new GridData(SWT.LEFT, SWT.CENTER, true, false,
                1, 1);
        m_btnComplete.setLayoutData(btngridData);
        m_btnComplete.setText(m_registry.getString("Complete.Label"));
        
        createEmptyLabel(parent);
        createEmptyLabel(parent);
        createEmptyLabel(parent);
        createPrintButton(parent);
    }
    
    private void createDoneLabel(Composite parent)
    {
        Label lblDone = new Label(parent, SWT.NONE);
        lblDone.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
                1, 1));
        lblDone.setText(m_registry.getString("Done.Label"));
        
        createEmptyLabel(parent);
        createEmptyLabel(parent);
        createEmptyLabel(parent);
        createEmptyLabel(parent);
    }
    
    private void createComments(Composite parent)
    {
        Label lblComments = new Label(parent, SWT.NONE);
        lblComments.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false,
                false, 1, 1));
        lblComments.setText(m_registry.getString("Comments.Label"));
        
        m_comments = new Text(parent, SWT.BORDER | SWT.MULTI
                | SWT.BORDER | SWT.V_SCROLL);
        GridData gd_text_2 = new GridData(SWT.FILL, SWT.CENTER, true, false, 4,
                1);
        gd_text_2.heightHint = 100;
        m_comments.setLayoutData(gd_text_2);
        m_comments.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        
        createEmptyLabel(parent);
    }
    
    private void createProcessDescription(Composite parent)
    {
        Label lblProcessDescription = new Label(parent, SWT.NONE);
        GridData gd_lblProcessDescription = new GridData(SWT.RIGHT, SWT.TOP,
                true, false, 1, 1);
       // gd_lblProcessDescription.widthHint = 105;
        lblProcessDescription.setLayoutData(gd_lblProcessDescription);
        lblProcessDescription.setText(m_registry.getString("ProcessDescription.Label"));
        
        m_processDescription = new Text(parent, SWT.BORDER| SWT.MULTI
                | SWT.BORDER | SWT.V_SCROLL);
        GridData gd_text_1 = new GridData(SWT.FILL, SWT.CENTER, true, false, 4,
                1);
        gd_text_1.heightHint = 45;
        m_processDescription.setLayoutData(gd_text_1);
        m_processDescription.setBackground(SWTResourceManager
                .getColor(SWT.COLOR_WHITE));
        createEmptyLabel(parent);
    }
    
    private void createEmptyLabel(Composite parent)
    {
        Label label = new Label(parent, SWT.NONE);
        GridData gd_label_1 = new GridData(SWT.FILL, SWT.CENTER, true, false,
                1, 1);
        label.setLayoutData(gd_label_1);
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    public boolean save()
    {
        if (m_btnComplete.getSelection())
        {
            postSaveFormUpdates();
            setVisible(false);
            setEnabled(false);
        }
        
        return true;
    }
    
    private boolean updateProcessDescription()
    {
        try
        {
            TCComponentProcess process = m_task.getProcess();
            String newDesc = m_processDescription.getText();
            
            String oldDesc = process.getStringProperty("object_desc");
            if (oldDesc == null || oldDesc.equals(""))
            {
                if (newDesc != null)
                    process.setStringProperty("object_desc", newDesc);
            }
        }
        catch (Exception exception)
        {
            MessageBox.post(null, exception, m_registry.getString("Error.Label"), MessageBox.ERROR);
            return false;
        }
        return true;
    }
    
    protected void postSaveFormUpdates()
    {
        if (!updateProcessDescription())
        {
            return;
        }
        
        try
        {
            m_task.setStringProperty("comments", m_comments.getText());
        }
        catch (TCException tcexception)
        {
            tcexception.printStackTrace();
        }
        if (m_btnComplete.getSelection())
        {
            try
            {
                TCSession session = (TCSession) AIFUtility.getDefaultSession();
                
                /*
                 * CompleteOperation completeoperation = new
                 * CompleteOperation(m_window, new TCComponentTask[] { m_task
                 */
                
                CompleteOperation completeoperation = new CompleteOperation(
                        m_window, new TCComponentTask[] { m_task },
                        m_comments.getText());
                
                completeoperation.executeOperation();
                m_task.clearCache("state");
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        
        else
        {
            try
            {
                FailureOperation failureoperation = new FailureOperation(
                        m_window, new TCComponentTask[] { m_task },
                        m_comments.getText());
                failureoperation.executeOperation();
            }
            catch (Exception exception1)
            {
                exception1.printStackTrace();
            }
        }
    }
    
    public boolean isPanelEditable()
    {
        boolean isPanelEditable = true;
        try
        {
            if (m_task.okToModify() && m_task != null)
            {
                TCTaskState tctaskstate = m_task.getState();
                if (tctaskstate == TCTaskState.STARTED)
                {
                    isPanelEditable = m_task.isValidPerformer();
                }
                else
                {
                    isPanelEditable = false;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isPanelEditable;
    }
    
    public void setEnabled(boolean flag)
    {
        m_comments.setEnabled(flag);
        m_processDescription.setEnabled(flag);
        m_btnComplete.setEnabled(flag);
        
    }
    
    public void setVisible(boolean flag)
    {
        m_btnClear.setVisible(flag);
        m_btnSave.setVisible(flag);
    }
    
}
