package com.nov.rac.form.common;

import javax.swing.ImageIcon;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.renderer.AlternateIDRenderer;
import com.nov.rac.form.renderer.ButtonCellEditor;
import com.nov.rac.form.renderer.ButtonCellRenderer;
import com.nov.rac.form.renderer.ComboBoxCellRenderer;
import com.nov.rac.form.renderer.DispositionImageRenderer;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.renderer.TargetsTableComboCellEditor;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class TargetsTable extends TCTable
{
    private Registry m_registry;
    String[] m_editableColumns = null;
    
    
    public TargetsTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
        
        // making this member variable as we need this many times.
        m_editableColumns = m_registry
                .getStringArray("TargetsTable.EDITABLE_COLUMNS");
        
        addColumnRenderersEditors();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        TableCellEditor editor = getColumnModel().getColumn(columnIndex).getCellEditor();
        return editor.isCellEditable(null);
    }
    
    public void addColumnRenderersEditors()
    {
        // Part ID column
        addIconRenderer("TargetsTableColumnIdentifier.ItemID");
        
        // AltID column
        addAltIDRenderer("TargetsTableColumnIdentifier.AtlernateID");
        
        // Disposition Image column
        addDispositionImageColumnCellRenderer("TargetsTableColumnIdentifier.DispositionImage");
        
        // In Process column
        addLOVComboRederer("TargetsTableColumnIdentifier.DispositionInProcess");
        
        // In Inventory column
        addLOVComboRederer("TargetsTableColumnIdentifier.DispositionInInventory");
        
        // Assembled column
        addLOVComboRederer("TargetsTableColumnIdentifier.DispositionAssembled");
        
        // In Field column
        addLOVComboRederer("TargetsTableColumnIdentifier.DispositionInField");
        
        // Disposition Instruction column
        addDispositionInstructionIconRenderer("TargetsTableColumnIdentifier.DispositionInstruction");
    }
    
    private void addIconRenderer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        // int colIndex = TableUtils.getColumnIndex(columnName, m_table);
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new PartIDIconRenderer());
    }
    
    private void addAltIDRenderer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new AlternateIDRenderer());
        
    }
    
    private void addDispositionImageColumnCellRenderer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        int colIndex = getColumnIndex(columnName);
        
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        ImageIcon dispFillIcon = m_registry
                .getImageIcon("TargetsTable.DispositionBlue.IMAGE");
        ImageIcon dispNotFillIcon = m_registry
                .getImageIcon("TargetsTable.DispositionRed.IMAGE");
        
        column.setCellRenderer(new DispositionImageRenderer(m_registry,
                dispFillIcon, dispNotFillIcon));
    }
    
    private void addLOVComboRederer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new ComboBoxCellRenderer(getLOVName()));
        column.setCellEditor(new TargetsTableComboCellEditor(getLOVName()));
    }
    
    private void addDispositionInstructionIconRenderer(String registryColumnName)
    {
        String columnName = m_registry.getString(registryColumnName);
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        ImageIcon blueIcon = m_registry
                .getImageIcon("TargetsTable.InstructionBlue.IMAGE");
        ImageIcon grayIcon = m_registry
                .getImageIcon("TargetsTable.InstructionGray.IMAGE");
        
        column.setCellRenderer(new ButtonCellRenderer(blueIcon, grayIcon));
        column.setCellEditor(new ButtonCellEditor(blueIcon, grayIcon));
    }
    
    private String getLOVName()
    {
        
        String enOwningGroup = FormHelper.getTargetComponentOwningGroup();
        String lOVName = "";
        
        if (enOwningGroup.contains(m_registry.getString("PCG.Group")))
        {
            lOVName = m_registry
                    .getString("ENFormDispositionPanel.ENDisposition_PCGLOV");
        }
        else
        {
            lOVName = m_registry
                    .getString("ENFormDispositionPanel.ENDispositionLOV");
        }
        
        return lOVName;
    }
}
