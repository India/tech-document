package com.nov.rac.form.common;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.teamcenter.rac.common.TCTable;

public class TCTableComparator
{
    private static final String ADD = "add";
    private static final String REMOVE = "remove";
    private static final String MODIFY = "modify";
    private static final String BLANK = "";
    private TCTable m_table1 = null;
    private TCTable m_table2 = null;
    private int m_statusColumnIndex1 = 0;
    private int m_statusColumnIndex2 = 0;
    private int m_rowCount1 = 0;
    private int m_rowCount2 = 0;
    private String m_primaryKeyColumn = "";
    private String[] m_tableColumnNames = new String[]{};
    private boolean m_isTable1RevisionLatest = true;
    private boolean m_isTable2RevisionLatest = true;
    private int m_primaryColumnIndex1 = 0;
    private int m_primaryColumnIndex2 = 0;
    
    public TCTableComparator()
    {
        
    }
    
    private void compare(TCTable table1, TCTable table2, String primaryKeyColumn, String[] tableColumnNames,boolean isTable1RevisionLatest,boolean isTable2RevisionLatest)
    {
        m_table1 = table1;
        m_table2 = table2;
        
        m_primaryKeyColumn = primaryKeyColumn;
        m_tableColumnNames = tableColumnNames;
        
        m_isTable1RevisionLatest = isTable1RevisionLatest;
        m_isTable2RevisionLatest = isTable2RevisionLatest;
        
         m_rowCount1 = m_table1.getRowCount();
         m_rowCount2 = m_table2.getRowCount();
         
         m_statusColumnIndex1 = m_table1.getColumnIndex("status");
         m_statusColumnIndex2 = m_table2.getColumnIndex("status");
        
         m_primaryColumnIndex1 = m_table1.getColumnIndex(m_primaryKeyColumn);
         m_primaryColumnIndex2 = m_table2.getColumnIndex(m_primaryKeyColumn);
         
        if (m_rowCount1 < m_rowCount2)
        {
            setAddStatus();
        }
        
        if (m_rowCount1 > m_rowCount2)
        {
            setRemoveStatus();
        }
        
        if (m_rowCount1 == m_rowCount2)
        {
            setModifyStatus();
        }
    }
    
    private void setAddStatus()
    {
        List<Object> list1 = new CopyOnWriteArrayList<Object>(); // list1 is the list of primaryKeyColumn objects exist in table1
        m_table1.getColumnObjects(m_primaryColumnIndex1, list1);
        
        int rowCount2 = m_table2.getRowCount();
        List<Object> list2 = new CopyOnWriteArrayList<Object>();
        m_table2.getColumnObjects(m_primaryColumnIndex2, list2); // list2 is the list of primaryKeyColumn objects exist in table2
        
        List<Object> list3 = new CopyOnWriteArrayList<Object>();  // list3 is the list of common objects exist in both the tables
        
        for (Object obj : list1)
        {
            if (list2.contains(obj))
            {
                list3.add(obj);
                list2.remove(obj);
                list1.remove(obj);
            }
        }
        
        for (int jnx = 0; jnx < rowCount2; jnx++)
        {
            
            String val2 = (String) m_table2.getModel().getValueAt(jnx,
                    m_primaryColumnIndex2);
            
            for (Object obj : list2)
            {
                if (val2.equals(obj.toString()))
                {
                    m_table2.getModel().setValueAt(ADD, jnx, m_statusColumnIndex2);
                }
            }
        }
        
        for (int inx = 0; inx < m_rowCount1; inx++)
        {
            
            String val1 = (String) m_table1.getModel().getValueAt(inx,
                    m_primaryColumnIndex1);
            
            for (Object obj : list1)
            {
                if (val1.equals(obj.toString()))
                {
                    m_table1.getModel()
                            .setValueAt(REMOVE, inx, m_statusColumnIndex1);
                }
            }
        }
        
        checkTableRecordsModification(list3);
    }
    
    private void checkTableRecordsModification(List<Object> list3)
    {
        if(list3.size() > 0)
        {
            int rowIndex1 = 0;
            int rowIndex2 = 0;
            
            for(int inx = 0; inx < m_rowCount1; inx++)
            {
                Object tempObject1 = m_table1.getValueAt(inx, m_primaryColumnIndex1);
                
                if(list3.contains(tempObject1))
                {
                    rowIndex1 = inx;
                    for(int jnx = 0; jnx < m_rowCount2; jnx++)
                    {
                        Object tempObject2 = m_table2.getValueAt(jnx, m_primaryColumnIndex2);
                        if(tempObject1.equals(tempObject2))
                        {
                            if(list3.contains(tempObject2))
                            {
                                rowIndex2 = jnx;
                                checkTablesColumnContent(rowIndex1, rowIndex2);
                            }
                            break;
                        }
                    }
                    
                }
            }
            
           /* for(int jnx = 0; jnx < m_rowCount2; jnx++)
            {
                Object tempObject = m_table2.getValueAt(jnx, m_primaryColumnIndex2);
                
                if(list3.contains(tempObject))
                {
                    rowIndex2 = jnx;
                }
            }*/
            
        }
    }

    private void checkTablesColumnContent(int rowIndex1, int rowIndex2)
    {
        for(String column : m_tableColumnNames)
        {
            int colIdx1 = m_table1.getColumnIndex(column);
            int colIdx2 = m_table2.getColumnIndex(column);
            
            Object val1 = m_table1.getValueAt(rowIndex1, colIdx1);
            Object val2 = m_table2.getValueAt(rowIndex2, colIdx2);
            
            if(!val1.equals(val2))
            {
                if(m_isTable1RevisionLatest)
                {
                    m_table1.getModel().setValueAt(MODIFY, rowIndex1, m_statusColumnIndex1);
                }
                else
                {
                    m_table2.getModel().setValueAt(MODIFY, rowIndex2, m_statusColumnIndex2);
                }
            }
            else
            {
                m_table1.getModel().setValueAt(BLANK, rowIndex1, m_statusColumnIndex1);
                m_table2.getModel().setValueAt(BLANK, rowIndex2, m_statusColumnIndex2);
            }
        }
    }

    private void setRemoveStatus()
    {
        List<Object> list1 = new CopyOnWriteArrayList<Object>();
        m_table1.getColumnObjects(m_primaryColumnIndex1, list1);
        
        List<Object> list2 = new CopyOnWriteArrayList<Object>();
        m_table2.getColumnObjects(m_primaryColumnIndex2, list2);
        
        List<Object> list3 = new CopyOnWriteArrayList<Object>();  // list3 is the list of common objects exist in both the tables
        
        for (Object obj : list2)
        {
            if (list1.contains(obj))
            {
                list3.add(obj);
                list1.remove(obj);
                list2.remove(obj);
            }
        }
        
        for (int jnx = 0; jnx < m_rowCount1; jnx++)
        {
            
            String val1 = (String) m_table1.getModel().getValueAt(jnx,
                    m_primaryColumnIndex1);
            
            for (Object obj : list1)
            {
                if (val1.equals(obj.toString()))
                {
                    m_table1.getModel()
                            .setValueAt(ADD, jnx, m_statusColumnIndex1);
                }
            }
        }
        
        for (int inx = 0; inx < m_rowCount2; inx++)
        {
            
            String val2 = (String) m_table2.getModel().getValueAt(inx,
                    m_primaryColumnIndex2);
            
            for (Object obj : list2)
            {
                if (val2.equals(obj.toString()))
                {
                    m_table2.getModel().setValueAt(REMOVE, inx, m_statusColumnIndex2);
                }
            }
        }
        
        checkTableRecordsModification(list3);
    }
    
    private void setModifyStatus()
    {
        
        List<Object> list1 = new CopyOnWriteArrayList<Object>(); // list1 is the list of primaryKeyColumn objects exist in table1
        m_table1.getColumnObjects(m_primaryColumnIndex1, list1);

        List<Object> list2 = new CopyOnWriteArrayList<Object>();
        m_table2.getColumnObjects(m_primaryColumnIndex2, list2); // list2 is the list of primaryKeyColumn objects exist in table2
        
        
        List<Object> list3 = new CopyOnWriteArrayList<Object>();  // list3 is the list of common objects exist in both the tables
        
        for (Object obj : list1)
        {
            if (list2.contains(obj))
            {
                list3.add(obj);
                list2.remove(obj);
                list1.remove(obj);
            }
        }
        
        if(m_isTable1RevisionLatest)
        {
            for (int jnx = 0; jnx < m_rowCount1; jnx++)
            {
                
                String val1 = (String) m_table1.getModel().getValueAt(jnx,
                        m_primaryColumnIndex1);
                
                for (Object obj : list1)
                {
                    if (val1.equals(obj.toString()))
                    {
                        m_table1.getModel()
                                .setValueAt(ADD, jnx, m_statusColumnIndex1);
                    }
                }
            }
            
            for (int inx = 0; inx < m_rowCount2; inx++)
            {
                
                String val2 = (String) m_table2.getModel().getValueAt(inx,
                        m_primaryColumnIndex2);
                
                for (Object obj : list2)
                {
                    if (val2.equals(obj.toString()))
                    {
                        m_table2.getModel().setValueAt(REMOVE, inx, m_statusColumnIndex2);
                    }
                }
            }
        }
        else
        {
            
            for (int jnx = 0; jnx < m_rowCount2; jnx++)
            {
                
                String val2 = (String) m_table2.getModel().getValueAt(jnx,
                        m_primaryColumnIndex1);
                
                for (Object obj : list2)
                {
                    if (val2.equals(obj.toString()))
                    {
                        m_table2.getModel()
                                .setValueAt(ADD, jnx, m_statusColumnIndex2);
                    }
                }
            }
            
            for (int inx = 0; inx < m_rowCount1; inx++)
            {
                
                String val1 = (String) m_table1.getModel().getValueAt(inx,
                        m_primaryColumnIndex2);
                
                for (Object obj : list1)
                {
                    if (val1.equals(obj.toString()))
                    {
                        m_table1.getModel()
                                .setValueAt(REMOVE, inx, m_statusColumnIndex1);
                    }
                }
            }
            
        }
        
        checkTableRecordsModification(list3);
    }
    
    public void compareView(TCTable table1, TCTable table2, String primaryKeyColumn, String[] tableColumnNames,boolean isTable1RevisionLatest,boolean isTable2RevisionLatest)
    {
        compare(table1, table2, primaryKeyColumn,tableColumnNames,isTable1RevisionLatest,isTable2RevisionLatest);
        repaint();
    }
    
    private void repaint()
    {
        m_table1.repaint();
        m_table2.repaint();
    }
}
