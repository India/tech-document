package com.nov.rac.form.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.util.AddTargetToEnSearchProvider;
import com.nov.rac.form.util.RelatedMinorRevisionSearchProvider;
import com.nov.rac.form.util.RelatedMinorRevision_TCTable;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.services.addRemoveTargets.ENAddRemoveTargetsHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class AddTargetToWorkflow implements IPublisher
{
    private Shell m_shell = null;
    private Composite m_minorRevisionTableComposite = null;
    private Text m_itemIDSearchText = null;
    private TCSession m_session = null;
    private TCTable m_itemSearchTable = null;
    private Registry m_registry;
    private ProgressBar m_progressBar = null;
    
    private Composite m_addCancelComposite = null;
    private Label m_searchStatusLabel = null;
    
    private RelatedMinorRevision_TCTable m_relatedMinorRevTable = null;
    private Vector<String> m_targetMinorRevsPuid = new Vector<String>();
    
    private Vector<String> m_selectedTargetsPuid = new Vector<String>();
    private Vector<String> m_selectedTargetsTypes = new Vector<String>();
    private Vector<String> m_selectedTargetsItemID = new Vector<String>();
    
    private static final int KEYCODE_ENTER = 13;
    private static final float PERCENTAGESIZE = 0.6f;
    private static final int HORIZONTAL_SPAN_3 = 3;
    private static final int HORIZONTAL_SPAN_15 = 15;
    private static final int NUMBER_OF_COLUMN_3 = 3;
    
    private static final int SRCH_PUID_INDEX = 0;
    private static final int SRCH_REV_ID_INDEX = 3;
    private static final int SRCH_RELEASE_STATUS_INDEX = 7;
    private static final int SRCH_OBJECT_TYPE_INDEX = 6;
    
    private static final int ITEM_SRCH_STATUS_INDEX = 6;
    private static final int ITEM_SRCH_OBJECT_TYPE_INDEX = 7;
    private static final int ITEM_SRCH_RSONE_ITEM_TYPE_INDEX = 8;
    
    private static final int MIN_REQ_SEARCH_STRING = 3;
    
    public AddTargetToWorkflow(Registry registry)
    {
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_registry = registry;
        
        createUI(Display.getCurrent());
    }
    
    
    private void createUI(Display display)
    {
        createShell(display);
        if (isValidENGroupForMinorRevisionTable())
        {
            new Label(m_shell, SWT.NONE);
            createShowRelatedMinorRevisionsButton();
            new Label(m_shell, SWT.NONE);
            createRelatedMinorRevisionTableLabel();
            createSelectCheckboxToAddLabel();
            createMinorRevisionTableComposite();
            JScrollPane relatedMinorRevisionScrollPane = createMinorRevisionTable();
            if (relatedMinorRevisionScrollPane != null)
            {
                SWTUIUtilities.embed(m_minorRevisionTableComposite,
                        relatedMinorRevisionScrollPane, false);
            }
        }
        
        createSearchForPartToAddLabel();
        createItemIDSearchText();
        createSearchButton();
        
        createPartSearchResultTableLabel();
        new Label(m_shell, SWT.NONE);
        new Label(m_shell, SWT.NONE);
        
        createSingleSelectionAllowedLabel();
        new Label(m_shell, SWT.NONE);
        new Label(m_shell, SWT.NONE);
        
        createSearchTable();
        createSearchStatusLabel();
        createProgressBar();
        createAddCancelComposite();
        createAddButton();
        createCancelButton();
        
        m_shell.open();
        
        while (!m_shell.isDisposed())
        {
            if (!display.readAndDispatch())
            {
                display.sleep();
            }
        }
    }
    
    private void createMinorRevisionTableComposite()
    {
        m_minorRevisionTableComposite = new Composite(m_shell, SWT.EMBEDDED
                | SWT.BORDER);
        m_minorRevisionTableComposite.setLayoutData(new GridData(SWT.FILL,
                SWT.FILL, true, true, HORIZONTAL_SPAN_3, 1));
    }
    
    private void createSelectCheckboxToAddLabel()
    {
        Label label = new Label(m_shell, SWT.NONE);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                HORIZONTAL_SPAN_3, 1));
        label.setText(m_registry.getString(
                "AddTargetToEnForm.RelatedMinorRevisionsNoteLabel"));
        label.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        
    }
    
    private void createRelatedMinorRevisionTableLabel()
    {
        Label label = new Label(m_shell, SWT.NONE);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
                HORIZONTAL_SPAN_3, 1));
        label.setText(m_registry.getString(
                "AddTargetToEnForm.RelatedMinorRevisionsTableLabel"));
        FontData fontData = label.getFont().getFontData()[0];
        Font font = new Font(m_shell.getDisplay(), new FontData(
                fontData.getName(), fontData.getHeight(), SWT.BOLD));
        label.setFont(font);
        
    }
    
    private void createShowRelatedMinorRevisionsButton()
    {
        Button showRelatedMinorRevButton = new Button(m_shell, SWT.PUSH);
        showRelatedMinorRevButton.setLayoutData(new GridData(SWT.FILL,
                SWT.CENTER, true, false, 1, 1));
        showRelatedMinorRevButton.setText(m_registry.getString(
                "AddTargetToEnForm.RelatedMinorRevisionsButton"));
        
        showRelatedMinorRevButton
                .addSelectionListener(new SelectionListener()
                {
                    
                    @Override
                    public void widgetSelected(SelectionEvent selectionevent)
                    {
                        showRelatedMinorRevs();
                    }
                    
                    @Override
                    public void widgetDefaultSelected(
                            SelectionEvent selectionevent)
                    {
                    }
                });
        
    }
    
    private void showRelatedMinorRevs()
    {
        try
        {
            m_relatedMinorRevTable.clear();
            m_relatedMinorRevTable.clearCheckboxHeader();

            m_targetMinorRevsPuid.clear();
            TCComponent[] minorTargets = getMinorRevsFromTargets();
            
            // Populate vector for filtering the search results later
            for (int index = 0; index < minorTargets.length; index++)
            {
                m_targetMinorRevsPuid.add(minorTargets[index].getUid());
            }
            searchRelatedMinorRevisions(minorTargets);
        }
        catch (TCException e2)
        {
            e2.printStackTrace();
            MessageBox.post(e2.getDetailsMessage(), "ERROR", MessageBox.ERROR);
        }
    }
    
    private void createSearchStatusLabel()
    {
        m_searchStatusLabel = new Label(m_shell, SWT.NONE);
        m_searchStatusLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, HORIZONTAL_SPAN_3, 1));
        m_searchStatusLabel.setAlignment(SWT.CENTER);
        
    }
    
    private void createCancelButton()
    {
        Button cancelButton = new Button(m_addCancelComposite, SWT.PUSH); // Add
        cancelButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
                false, 1, 1));
        cancelButton.setText(m_registry.getString(
                "AddTargetToEnForm.CancelButton"));
        cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                m_shell.dispose();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        });
        
    }
    
    private void createAddButton()
    {
        Button addButton = new Button(m_addCancelComposite, SWT.PUSH); // cancel
        addButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true,
                false, 1, 1));
        addButton.setText(m_registry
                .getString("AddTargetToEnForm.AddButton"));
        addButton.setToolTipText(m_registry
                .getString("AddTargetToEnForm.AddButtonToolTip"));
        addButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                performOnAddButtonSelection();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        });
    }
    
    private void createAddCancelComposite()
    {
        m_addCancelComposite = new Composite(m_shell, SWT.EMBEDDED | SWT.NONE);
        m_addCancelComposite.setLayout(new GridLayout(2, true));
        m_addCancelComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                true, false, HORIZONTAL_SPAN_3, 1));
        
    }
    
    private void createProgressBar()
    {
        m_progressBar = new ProgressBar(m_shell, SWT.NONE);
        m_progressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
                false, HORIZONTAL_SPAN_3, 1));
        m_progressBar.setVisible(false);
        m_progressBar.setEnabled(false);
        
    }
    
    private void createSearchTable()
    {
        Composite l_tableComposite = new Composite(m_shell, SWT.EMBEDDED
                | SWT.BORDER);
        l_tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true, HORIZONTAL_SPAN_3, 1));
        
        String[] colNames = m_registry
                .getStringArray("AddTargetToEnForm.SearchTable.COLUMN_NAMES");
        String[] colIdentifiers = m_registry
                .getStringArray("AddTargetToEnForm.SearchTable.COLUMN_IDENTIFIERS");
        
        m_itemSearchTable = new TCTable(colIdentifiers, colNames);
        m_itemSearchTable.setShowVerticalLines(true);
        m_itemSearchTable.setAutoCreateColumnsFromModel(false);
        m_itemSearchTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_itemSearchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane scrollPane = new JScrollPane(m_itemSearchTable);
        TitledBorder titleBorder = new TitledBorder("");
        scrollPane.setBorder(titleBorder);
        
        m_itemSearchTable.setVisible(true);
        m_itemSearchTable.getTableHeader().setBorder(
                UIManager.getBorder("TableHeader.cellBorder"));
        
        // Hide columns
        String[] hideColumnNames = m_registry
                .getStringArray("AddTargetToEnForm.SearchTable.HIDE_COLUMNS");
        int colIndex = -1;
        for (String colName : hideColumnNames)
        {
            colIndex = m_itemSearchTable.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_itemSearchTable, colIndex);
        }
        
        SWTUIUtilities.embed(l_tableComposite, scrollPane, false);
    }
    
    private void createSingleSelectionAllowedLabel()
    {
        Label singleSelectionLabel = new Label(m_shell, SWT.NONE);
        singleSelectionLabel.setForeground(SWTResourceManager
                .getColor(SWT.COLOR_RED));
        singleSelectionLabel.setText(m_registry.getString(
                "AddTargetToEnForm.NoteOnlySingleSelectionAllowedLabel"));
    }
    
    private void createPartSearchResultTableLabel()
    {
        Label searchTableLabel = new Label(m_shell, SWT.NONE);
        searchTableLabel.setText(m_registry.getString(
                "AddTargetToEnForm.PartSearchResultTableLabel"));
        FontData fontData = searchTableLabel.getFont().getFontData()[0];
        Font font = new Font(m_shell.getDisplay(), new FontData(
                fontData.getName(), fontData.getHeight(), SWT.BOLD));
        searchTableLabel.setFont(font);
    }
    
    private JScrollPane createMinorRevisionTable()
    {
        String[] columnNames = m_registry.getStringArray("RelatedMinorRevsTable.COLUMN_NAMES");
        String[] columnIdentifier = m_registry.getStringArray("RelatedMinorRevsTable.COLUMN_IDENTIFIERS");
        
        
        m_relatedMinorRevTable = new RelatedMinorRevision_TCTable( columnIdentifier, columnNames);
        m_relatedMinorRevTable.setRegistry(m_registry);
        
        m_relatedMinorRevTable.createUI();
        
        m_relatedMinorRevTable.setCheckboxHeaderState();
        JScrollPane treePaneRelatedMinors = new JScrollPane(m_relatedMinorRevTable);
      
        treePaneRelatedMinors
                .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        // Hide columns
        String[] columnsToHide = m_registry.getStringArray("RelatedMinorRevsTable.HIDE_COLUMNS");
        int colIndex = -1;
        for (String colName : columnsToHide)
        {
            colIndex = m_relatedMinorRevTable.getColumnIndex(colName);
            TableUtils.hideTableColumn(m_relatedMinorRevTable, colIndex);
        }
        
        return treePaneRelatedMinors;
    }
    
    private void createSearchButton()
    {
        final Button searchButton = new Button(m_shell, SWT.NONE);
        Image image = new Image(m_shell.getDisplay(), m_registry.getImage(
                "AddTargetToEnForm.SearchButtonImage"), SWT.NONE);
        searchButton.setImage(image);
        
        searchButton.addSelectionListener(new SelectionListener()
        {
            @Override
            public void widgetSelected(SelectionEvent selectionevent)
            {
                searchPopulateItems();
            };
            
            @Override
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        });
    }
    
    private void createItemIDSearchText()
    {
        m_itemIDSearchText = new Text(m_shell, SWT.BORDER);
        m_itemIDSearchText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
                false, false, 1, 1));
        //
        m_itemIDSearchText.addKeyListener(new KeyListener()
        {
            @Override
            public void keyReleased(KeyEvent keyevent)
            {
                if (keyevent.keyCode == SWT.KEYPAD_CR
                        || keyevent.keyCode == KEYCODE_ENTER)
                {
                    searchPopulateItems();
                    return;
                }
            }
            
            @Override
            public void keyPressed(KeyEvent keyevent)
            {
            }
        });
    }
    
    private void createSearchForPartToAddLabel()
    {
        Label lblNewLabel = new Label(m_shell, SWT.NONE);
        lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
                false, 1, 1));
        lblNewLabel.setText(m_registry.getString(
                "AddTargetToEnForm.SearchForPartToAddLabel"));
        
    }
    
    private void createShell(Display d1)
    {
        Rectangle desktopSize = Display.getDefault().getClientArea();
        
        m_shell = new Shell(d1, SWT.CLOSE | SWT.RESIZE | SWT.TITLE
                | SWT.APPLICATION_MODAL | SWT.EMBEDDED);
        m_shell.setSize((int) (PERCENTAGESIZE * (desktopSize.width)),
                (int) (PERCENTAGESIZE * (desktopSize.height))); // set according
                                                                // to screen
        Rectangle shellSize = m_shell.getBounds();
        int x = desktopSize.x + (desktopSize.width - shellSize.width) / 2;
        int y = desktopSize.y + (desktopSize.height - shellSize.height) / 2;
        m_shell.setLocation(x, y);
        
        GridLayout gl_shell = new GridLayout(NUMBER_OF_COLUMN_3, true);
        gl_shell.horizontalSpacing = HORIZONTAL_SPAN_15;
        m_shell.setLayout(gl_shell);
        
    }
    
    
    private void searchRelatedMinorRevisions(TCComponent[] minorTargetsAttached)
    {
        INOVSearchProvider searchService = new RelatedMinorRevisionSearchProvider(
                minorTargetsAttached);
        INOVSearchResult searchResult = null;
        
        try
        {
            if (searchService != null)
            {
                if ((searchService instanceof INOVSearchProvider2)
                        && (((INOVSearchProvider2) searchService)
                                .validateInput() == false))
                {
                    MessageBox.post(
                            m_registry.getString("noMinorRevisions.MSG"),
                            "Info", MessageBox.INFORMATION);
                    return;
                }
                searchResult = NOVSearchExecuteHelperUtils.execute(
                        searchService, 0);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        if (searchResult == null)
        {
            return;
        }
        
        Vector<String> objectTypes = populateMinorRevisionTable(searchResult);
        
        setIconRenderer("item_id", m_relatedMinorRevTable);
        
        if (!objectTypes.isEmpty())
        {
            m_relatedMinorRevTable.setCheckboxHeaderState();
            m_relatedMinorRevTable.updateUI();
        }
        else
        {
            MessageBox.post(m_registry.getString("noMinorRevisions.MSG"),
                    "Info", MessageBox.INFORMATION);
        }
    }

    private Vector<String> populateMinorRevisionTable(
            INOVSearchResult searchResult)
    {
        INOVResultSet theResultSet = searchResult.getResultSet();
        
        // Populate the table with search results
        Vector<String> itemData = new Vector<String>();
        Vector<String> searchData = theResultSet.getRowData();
        Vector<String> objectTypes = new Vector<String>();
        
        for (int inx = 0; inx < theResultSet.getRows(); inx++)
        {
            m_progressBar.setSelection(inx);
            
            int nCurrentRowIndex = (inx * theResultSet.getCols());
            itemData.clear();
            for (int jnx = 0; jnx < theResultSet.getCols(); jnx++)
            {
                
                itemData.add(searchData.get(nCurrentRowIndex + jnx));
            }
            
            if (!isMinorRev(itemData.get(SRCH_REV_ID_INDEX))
                    || itemData.get(SRCH_RELEASE_STATUS_INDEX).equalsIgnoreCase(
                            "Released")
                    || m_targetMinorRevsPuid.contains(itemData.get(SRCH_PUID_INDEX)))
            {
                continue;
            }
            objectTypes.add(itemData.get(SRCH_OBJECT_TYPE_INDEX));
            itemData.add(0, "false");
            m_relatedMinorRevTable.addRow(itemData);
        }
        return objectTypes;
    }
    
    private TCComponent[] getMinorRevsFromTargets() throws TCException
    {
        
        TCComponentTask enRootTask = WorkflowProcess.getWorkflow().getRootTask();
        
        TCComponent[] l_targets = enRootTask.getAttachments(TCAttachmentScope.LOCAL,
                TCAttachmentType.TARGET);
        
        TCComponent[] minorRevTargets = new TCComponent[] {};
        Vector<TCComponent> trgtVec = new Vector<TCComponent>();
        for (int count = 0; count < l_targets.length; count++)
        {
            if (l_targets[count] instanceof TCComponentItemRevision)
            {
                String revisionID = l_targets[count]
                        .getProperty("item_revision_id");
                if (isMinorRev(revisionID))
                {
                    trgtVec.add(l_targets[count]);
                }
            }
        }
        minorRevTargets = trgtVec.toArray(new TCComponent[trgtVec.size()]);
        return minorRevTargets;
    }
    
    private boolean isMinorRev(String revID)
    {
        boolean isRevMinor = false;
        if (revID.contains("."))
        {
            String tempStr = revID.substring(revID.lastIndexOf(".") + 1,
                    revID.length());
            try
            {
                Integer.parseInt(tempStr);
                // is an integer!
                isRevMinor = true;
            }
            catch (NumberFormatException e)
            {
                // not an integer!
                isRevMinor = false;
            }
        }
        return isRevMinor;
    }
    
    private void performOnAddButtonSelection()
    {
        /*
         * things to do
         * 1. validate row is selected in table
         * 2. get puid and convert to component and check if it is a valid type
         * 3. check if selected component is already attached to workflow, if so pop up error dialog
         * 4. perform addItemToTarget() function
         * 5. dispose the shell
         */
        try
        {
            validateTableSelection();
            
            initializeTableDataVectors();
            
            validateSelectedComponentType();
            
            validateAreadyExists();
            
            String[] arrayPUIDs = m_selectedTargetsPuid.toArray(new String[m_selectedTargetsPuid.size()]);
            TCComponent[] selectedItemRevVector = m_session.stringToComponent(arrayPUIDs);
            
            if (selectedItemRevVector != null && selectedItemRevVector.length > 0)
            {
                addItemToTarget(selectedItemRevVector);
                m_shell.dispose();
            }
        }
        catch( TCException ex)
        {
            MessageBox.post( ex.getMessage(), "Warning",  MessageBox.WARNING );
        }
        
    }
    
    private void validateAreadyExists() throws TCException
    {
        HashMap<String, TCComponent> itemIDToCompMap = WorkflowProcess.getWorkflow().getStringToComponentMap();
        Set<String> keyItemID = itemIDToCompMap.keySet();
        
        for(String selectedComponentItemId : m_selectedTargetsItemID)
        {
            if(keyItemID.contains(selectedComponentItemId))
            {
                String[] errors = new String[]{ selectedComponentItemId + " : " + m_registry.getString("targetExists.msg") };
                throw new TCException( errors );
            }
        }
    }

    private void validateSelectedComponentType() throws TCException
    {
        for(String itemRevObjectType : m_selectedTargetsTypes)
        {
            if(  !(itemRevObjectType.startsWith( m_registry.getString("DocsItem.TYPE"))
                || itemRevObjectType.startsWith( m_registry.getString("Nov4PartItem.TYPE"))
                || itemRevObjectType.startsWith( m_registry.getString("NonEngItem.TYPE")))
              )
            {
                String[] errors = new String[]{ m_registry.getString("INVALIDTYPE.MSG") };
                throw new TCException( errors );
            }
        }
    }
    
    private void initializeTableDataVectors()
    {
        m_selectedTargetsPuid.clear();
        m_selectedTargetsItemID.clear();
        m_selectedTargetsTypes.clear();
        
        // 1. Get data from item search table
        int rowIndex = m_itemSearchTable.getSelectedRow();
        
        int puidColIndex = m_itemSearchTable.getColumnIndex("puid");
        int itemIDColIndex = m_itemSearchTable.getColumnIndex("item_id");
        int objectTypeColIndex = m_itemSearchTable.getColumnIndex("object_type");
        
        String selPuid = (String)m_itemSearchTable.getModel().getValueAt(rowIndex , puidColIndex);
        m_selectedTargetsPuid.add(selPuid);
        
        String selItemID = (String)m_itemSearchTable.getModel().getValueAt(rowIndex , itemIDColIndex);
        m_selectedTargetsItemID.add(selItemID);
        
        String selObjectType = (String)m_itemSearchTable.getModel().getValueAt(rowIndex , objectTypeColIndex);
        m_selectedTargetsTypes.add(selObjectType);
        
        // 2. Get data from minor revision search table
        // Note: Multiple selection is allowed in this table
        if(m_relatedMinorRevTable != null && m_relatedMinorRevTable.getSelectedRowCount() > 0)
        {
            puidColIndex = m_relatedMinorRevTable.getColumnIndex("puid");
            itemIDColIndex = m_relatedMinorRevTable.getColumnIndex("item_id");
            objectTypeColIndex = m_relatedMinorRevTable.getColumnIndex("object_type");
            
            int[] selRowsIndices = m_relatedMinorRevTable.getSelectedRowsIndices();
            
            for(int rowInx : selRowsIndices)
            {
                selPuid = (String)m_relatedMinorRevTable.getModel().getValueAt(rowInx , puidColIndex);
                m_selectedTargetsPuid.add(selPuid);
                
                selItemID = (String)m_relatedMinorRevTable.getModel().getValueAt(rowInx , itemIDColIndex);
                m_selectedTargetsItemID.add(selItemID);
                
                selObjectType = (String)m_relatedMinorRevTable.getModel().getValueAt(rowInx , objectTypeColIndex);
                m_selectedTargetsTypes.add(selObjectType);
            }
        }
    }

    private TCComponent[] getSelectedItemRevsFromTable() throws TCException
    {
        Vector<String> puids= new Vector<>();
        
        // 1.0 get selected component from item search table
        int rowIndex = m_itemSearchTable.getSelectedRow();
        int puidColIndex = m_itemSearchTable.getColumnIndex("puid");
        
        String selPuid = (String)m_itemSearchTable.getModel().getValueAt(rowIndex , puidColIndex);
        puids.add(selPuid);
        
        // 2.0 get selected component from minor revision table
        if(m_relatedMinorRevTable != null && m_relatedMinorRevTable.getSelectedRowCount() > 0)
        {
            int[] selRowsIndices = m_relatedMinorRevTable
                    .getSelectedRowsIndices();
            puidColIndex = m_relatedMinorRevTable.getColumnIndex("puid");
            
            for(int rowInx : selRowsIndices)
            {
                String selectedPuid = (String)m_relatedMinorRevTable.getModel().getValueAt(rowInx , puidColIndex);
                puids.add(selectedPuid);
            }
        }
        
        String[] arrayPUIDs = puids.toArray(new String[puids.size()]);
        TCComponent[] selectedComponents = m_session.stringToComponent(arrayPUIDs);
        
        return selectedComponents;
    }

    private void validateTableSelection() throws TCException
    {
     // 1.0 Validate row selection in tables
        if( (m_itemSearchTable == null || m_itemSearchTable.getSelectedRowCount() <= 0 ) 
            && (m_relatedMinorRevTable == null || m_relatedMinorRevTable.getSelectedRowCount() <= 0))
        {
            String[] errors =  new String[]{ m_registry.getString("noPartSelected.MSG") };
            TCException exception = new TCException( errors );
            
            throw exception;
        }
    }

    private void addItemToTarget(TCComponent[] selComponents)
    {
        TCComponentTask enRootTask = WorkflowProcess.getWorkflow()
                .getRootTask();
        Map addedTargetsReturn = null;
        try
        {
            ENAddRemoveTargetsHelper enAddRemoveHelper = new ENAddRemoveTargetsHelper();
            addedTargetsReturn = enAddRemoveHelper.addTargets(selComponents,
                    enRootTask);
            
            if (addedTargetsReturn != null && !addedTargetsReturn.isEmpty())
            {
                // Running the separate thread so that adding targets to table is not dependent on msg box.
                final Set<TCComponent> addedTargetsSet = addedTargetsReturn.keySet();
                Display.getDefault().asyncExec( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        popupAddReportDialog(addedTargetsSet);
                    }
                });
                
                publishTargetsAdded(addedTargetsReturn);
            }
        }
        catch (TCException e)
        {
            MessageBox.post(e.getDetailsMessage(), "ERROR", MessageBox.ERROR);
        }
    }

    private void popupAddReportDialog(Set<TCComponent> targetsSet)
    {
        StringBuilder addedTargets = new StringBuilder();
        StringBuilder notAddedTargets = new StringBuilder();
        
        try
        {
            String[] propName = m_registry.getStringArray( "ItemRevision.PROPERTIES_AS_ATTRIBUTE");
            TCComponentType.getPropertiesSet( new ArrayList<TCComponent>(targetsSet), propName);
            
            Vector<String> l_addedItemIDVector = new Vector<>();
            
            // Create string for added targets
            String addedTargetsItemID;
            for (TCComponent eachTarget : targetsSet)
            {
                addedTargetsItemID = eachTarget.getProperty("item_id");
                
                addedTargets.append("\n" + eachTarget.toString());
                l_addedItemIDVector.add(addedTargetsItemID);
            }
            
            // Create string for not added targets
            m_selectedTargetsItemID.removeAll(l_addedItemIDVector);
            if( !m_selectedTargetsItemID.isEmpty())
            {
                for(String eachItemID : m_selectedTargetsItemID)
                {
                    notAddedTargets.append("\n" + eachItemID);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        String msgBoxString = getMSGBoxString(addedTargets.toString(), notAddedTargets.toString());
        MessageBox.post( msgBoxString, m_registry.getString("info.TITLE"), MessageBox.INFORMATION);
    }
    
    private String getMSGBoxString(String addedTargets, String notAddedTargets)
    {
        // TODO Auto-generated method stub
        StringBuilder message = new StringBuilder();
        //message.append(  m_registry.getString("AddTargetToEnForm.AddButtonMessage") + "\n\n");
        message.append(  m_registry.getString("AddTargetToEnForm.TargetsAdded") + " :\n");
        message.append(  addedTargets );
        
        if( !notAddedTargets.trim().isEmpty())
        {
            message.append(  "\n\n" );
            message.append(  m_registry.getString("AddTargetToEnForm.TargetsNotAdded")+ " : \n");
            message.append(  notAddedTargets );
        }
        
        return message.toString();
    }


    private boolean isValidENGroupForMinorRevisionTable()
    {
        int theScope = TCPreferenceService.TC_preference_site;
        
        String prefrenceName = m_registry
                .getString("AddTargetToEnForm.ValidENGroupForMinorRevisionTablePref");
        String[] strings = PreferenceUtils.getStringValues(theScope,
                prefrenceName);
        
        String currentGroupName = GroupUtils.getCurrentGroupName();
        
        List<String> preferences = Arrays.asList(strings);
        
        boolean isValidENGroup = preferences.contains(currentGroupName);
        System.out.println("EN Group validation:  " + isValidENGroup);
        return isValidENGroup;
    }
    
    private void searchPopulateItems()
    {
        m_itemSearchTable.removeAllRows();
        String l_itemIDText = m_itemIDSearchText.getText();
        
        try
        {
            m_searchStatusLabel.setText("");
            
            if (l_itemIDText.trim().length() < MIN_REQ_SEARCH_STRING)
            {
                MessageBox.post(m_registry.getString("searchWithMinInput.MSG"),
                        "ERROR", MessageBox.ERROR);
                return;
            }
            
            // INOVSearchProvider searchService = this;
            INOVSearchProvider2 searchService = new AddTargetToEnSearchProvider(
                    l_itemIDText, isMissionGroup() );
           
            INOVSearchResult searchResult = NOVSearchExecuteHelperUtils.execute(searchService,
                    INOVSearchProvider.LOAD_ALL);
            
            if (searchResult != null && searchResult.getResultSet().getRows() > 0)
            {
                populateItemSearchResultTable(searchResult);
            }
            else
            {
                m_searchStatusLabel.setForeground(SWTResourceManager
                        .getColor(SWT.COLOR_RED));
                m_searchStatusLabel.setVisible(true);
                m_searchStatusLabel
                        .setText(m_registry
                                .getString("AddTargetToEnForm.SearchStatusNoItemFound"));
            }
            m_progressBar.setVisible(false);
            m_progressBar.setEnabled(false);
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }

    private boolean isMissionGroup()
    {
        boolean isMisGrp = false;
        
        TCPreferenceService prefServ = m_session.getPreferenceService();
        String[] missionGrps = prefServ.getStringValues(m_registry
                .getString("misPref.NAME"));
        String enGroupName = FormHelper.getTargetComponentOwningGroup();
        
        for (int k = 0; k < missionGrps.length; k++)
        {
            if (missionGrps[k].equalsIgnoreCase(enGroupName))
            {
                isMisGrp = true;
                break;
            }
        }
        
        return isMisGrp;
    }
    
    private void populateItemSearchResultTable(INOVSearchResult searchResult)
    {
        INOVResultSet theResSet = searchResult.getResultSet();
        
        String revisionStatus = null;
        m_progressBar.setVisible(true);
        m_progressBar.setEnabled(true);
        m_progressBar.setMinimum(0);
            
        m_progressBar.setMaximum(theResSet.getRows());
        
        Vector<String> itemData = new Vector<String>();
        Vector<String> searchData = theResSet.getRowData();
        
        Vector<String> objectTypes = new Vector<String>();
            
        for (int inx = 0; inx < theResSet.getRows(); inx++)
        {
            m_progressBar.setSelection(inx);
            
            int nCurrentRowIndex = (inx * theResSet.getCols());
            itemData.clear();
            
            for (int jnx = 0; jnx < theResSet.getCols(); jnx++)
            {
                itemData.add(searchData.get(nCurrentRowIndex + jnx));
            }
            
            revisionStatus = (String) itemData
                    .get(ITEM_SRCH_STATUS_INDEX);
            
            if (((revisionStatus == null))
                    || ((revisionStatus != null) && (!(revisionStatus
                            .equalsIgnoreCase("Engineering Pre Release")))))
            {
                m_itemSearchTable.addRow(itemData);
                
                if (itemData.get(ITEM_SRCH_RSONE_ITEM_TYPE_INDEX) != null
                        && itemData.get(ITEM_SRCH_RSONE_ITEM_TYPE_INDEX)
                                .equalsIgnoreCase(
                                        "Purchased (Inventory Item)"))
                {
                    objectTypes.add("Purchased Part Revision");
                }
                else
                {
                    objectTypes.add(itemData
                            .get(ITEM_SRCH_OBJECT_TYPE_INDEX) + " Revision");
                }
            }
        }
        
        setIconRenderer("item_id", m_itemSearchTable);
        
        int foundCount = objectTypes.size();
        if (m_itemSearchTable.getRowCount() < 1)
        {
            m_searchStatusLabel.setVisible(true);
            m_searchStatusLabel
                    .setText(m_registry
                            .getString("AddTargetToEnForm.SearchStatusNoItemFound"));
        }
        else
        {
            m_searchStatusLabel.setForeground(SWTResourceManager
                    .getColor(SWT.COLOR_BLUE));
            m_searchStatusLabel.setVisible(true);
            m_searchStatusLabel
                    .setText(foundCount
                            + " "
                            + m_registry
                                    .getString("AddTargetToEnForm.SearchStatusItemsFound"));
        }
    }
    
    
    private void setIconRenderer(String columnName, TCTable table)
    {
        int colIndex = table.getColumnIndex(columnName);
        TableColumn column = table.getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new PartIDIconRenderer());
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    private void publishTargetsAdded(Map addedTargetsReturn)
    {
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        PublishEvent event = new PublishEvent(this,
                GlobalConstant.EVENT_TARGETS_ADDED, addedTargetsReturn, null);
        controller.publish(event);
    }
    
//    class SearchResultTableCellRenderer extends DefaultRenderer implements TableCellRenderer
//    {
//        private Vector<String> m_objectTypes;
//        
//        public SearchResultTableCellRenderer(Vector<String> objectTypes)
//        {
//            super();
//            m_objectTypes = objectTypes;
//        }
//        
//        public Component getTableCellRendererComponent(JTable table,
//                Object value, boolean isSelected, boolean hasFocus, int row,
//                int column)
//        {
//            Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//            
//            this.setIcon(TCTypeRenderer.getTypeIcon(m_objectTypes.get(row),
//                    null));
//           
//            return comp;
//        }
//        
//    }
    
}
