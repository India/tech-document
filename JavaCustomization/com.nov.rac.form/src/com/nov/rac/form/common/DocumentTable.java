package com.nov.rac.form.common;

import javax.swing.table.TableColumn;

import com.nov.rac.form.renderer.PartIDIconRenderer;
import com.nov.rac.form.renderer.TableCellColourRenderer;
import com.nov.rac.utilities.table.GenericCheckBoxCellRenderer;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class DocumentTable extends TCTable
{
    private Registry m_registry = null;
    
    public DocumentTable(String[] columnIdentifiers, String[] columnNames,
            Registry registry)
    {
        super(columnIdentifiers, columnNames);
        m_registry = registry;
        addColumnRederers();
    }
    
    private void addColumnRederers()
    {
        addColourRenderer(getRegistry().getStringArray(
                "DocumentTable.CompareColumns"));
        // Document ID column
        addIconRenderer(m_registry
                .getString("DocumentTableColumnIdentifier.ItemID"));
        
        // WO Included/PO Included column
        String[] columnNames = new String[] {
                m_registry
                        .getString("DocumentTableColumnIdentifier.WOIncluded"),
                m_registry
                        .getString("DocumentTableColumnIdentifier.POIncluded") };
        addCheckBoxRenderer(columnNames);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    private void addColourRenderer(String[] columnNames)
    {
        for (String columnName : columnNames)
        {
            int colIndex = getColumnIndex(columnName);
            TableColumn column = getColumnModel().getColumn(colIndex);
            
            column.setCellRenderer(new TableCellColourRenderer());
        }
    }
    
    private void addCheckBoxRenderer(String[] columnNames)
    {
        for (String columnName : columnNames)
        {
            int colIndex = getColumnIndex(columnName);
            TableColumn column = getColumnModel().getColumn(colIndex);
            
            column.setCellRenderer(new GenericCheckBoxCellRenderer());
        }
    }
    
    private void addIconRenderer(String columnName)
    {
        int colIndex = getColumnIndex(columnName);
        TableColumn column = getColumnModel().getColumn(colIndex);
        
        column.setCellRenderer(new PartIDIconRenderer());
    }
}
