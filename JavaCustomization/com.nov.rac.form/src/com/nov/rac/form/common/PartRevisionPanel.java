package com.nov.rac.form.common;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Button;

public class PartRevisionPanel extends Composite {

	private Combo m_partRevisionCombo;
	private Button m_searchButton;

	public PartRevisionPanel(Composite parent, int style) {
		super(parent, style);
		GridLayout gl_this = new GridLayout(3, false);
		gl_this.marginWidth = 0;
		setLayout(gl_this);

		createUI();

		// TODO Auto-generated constructor stub
	}

	public boolean createUI() {
		createpartRevisionLabel(this);
		createpartRevisionCombo(this);
		createSearchButton(this);
		return true;
	}

	private void createSearchButton(Composite composite) {
		m_searchButton = new Button(composite, SWT.NONE);
		m_searchButton.setText("Image");

	}

	private void createpartRevisionCombo(Composite composite) {
		m_partRevisionCombo = new Combo(composite, SWT.NONE);
		m_partRevisionCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
				true, false, 1, 1));

	}

	private void createpartRevisionLabel(Composite composite) {
		Label partRevision = new Label(composite, SWT.NONE);
		partRevision.setText("Part Revision:");
		partRevision.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
	}

	public void displaySearchButton(boolean flag) {
		if (flag && m_searchButton == null) {
			GridLayout gl_this = new GridLayout(3, false);
			gl_this.marginWidth = 0;
			setLayout(gl_this);
			createSearchButton(this);

		}

		else if (!flag && m_searchButton != null) {
			m_searchButton.dispose();
			m_searchButton = null;
			GridLayout gl_this = new GridLayout(2, false);
			gl_this.marginWidth = 0;
			setLayout(gl_this);
		}
	}
}
