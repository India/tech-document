package com.nov.rac.form.common;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.quicksearch.export.NOVExportSearchResultUtils;
import com.nov.rac.form.GlobalConstant;
import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.form.util.WorkflowProcess;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.databinding.DataBindingHelper;
import com.nov.rac.utilities.services.addRemoveTargets.ENAddRemoveTargetsHelper;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class TargetsTablePanel extends AbstractUIPanel implements
        INOVFormLoadSave, IPublisher, ISubscriber
{
    private Registry m_registry;
    protected TCTable m_table;// Need to inform to rest of the team members changed access specifier
    private final Set<String> m_rowsModified = new HashSet<String>();
    private Map<String, TCComponent> m_itemIdToComponentMap;
    private Map<String, TCComponent> m_itemIdToDispositionMap;
    private int m_varticalSpan = 30;
    private boolean m_isGrabExcessVerticalSpace = false;
    
    public int getVarticalSpan()
    {
        return m_varticalSpan;
    }

    public void setVarticalSpan(int varticalSpan)
    {
        m_varticalSpan = varticalSpan;
    }
   

    public void setGrabVarticalSpace(boolean bValue)
    {
        m_isGrabExcessVerticalSpace = bValue;   
    }
    
    public TargetsTablePanel(Composite parent, int style)
    {
        super(parent, style);
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, m_isGrabExcessVerticalSpace , 1, 1);
        composite.setLayoutData(gridData);
        composite.setLayout(new GridLayout(1, true));
        
        createTCTable(composite);
        
        addRowSelectionListener();
        addDoubleClickListener(composite);
        
        createDataModelMapping();
        addTableModelListner();
        registerProperties();
        
        return true;
    }
    
    protected void createTCTable(Composite composite)//// Need to inform to rest of the team members changed access specifier
    {
        Composite comp = new Composite(composite, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true,
                1, m_varticalSpan);
        comp.setLayoutData(gridData);
        
        String[] columnNames = getRegistry().getStringArray(
                "TargetsTable.COLUMN_NAMES");
        String[] columnIdentifiers = getRegistry().getStringArray(
                "TargetsTable.COLUMN_IDENTIFIERS");
        
      /*  m_table = new TargetsTable(columnIdentifiers, columnNames,
                getRegistry());*/
        createTableInstance(columnNames,columnIdentifiers);
        
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_table.setSortEnabled(false);
        
        // hide columns
        String[] hideColumnNames = getRegistry().getStringArray(
                "TargetsTable.HIDE_COLUMNS");
        if(hideColumnNames != null)
        {
            int colIndex = -1;
            for (String colName : hideColumnNames)
            {
                colIndex = m_table.getColumnIndex(colName);
                TableUtils.hideTableColumn(m_table, colIndex);
            }
        }
       
        
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        
        ScrollPagePane scrollPane = new ScrollPagePane(m_table);
        tablePanel.add(scrollPane);
        
        SWTUIUtilities.embed(comp, tablePanel, false);
    }
    
    protected void createTableInstance(String[] columnNames, String[] columnIdentifiers)
    {
    	  m_table = new TargetsTable(columnIdentifiers, columnNames,
                  getRegistry());
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean load(final IPropertyMap propMap) throws TCException
    {
        IPropertyMap[] itemRevisionsAry = propMap
                .getCompoundArray("targetsItemRevision");
        IPropertyMap[] dispositionsAry = propMap.getCompoundArray(m_registry
                .getString("targetDisposition.PROP"));
        
        // 1. Create local IPropertyMap array
        IPropertyMap[] localPropMapAry = new SimplePropertyMap[itemRevisionsAry.length];
        
        // 2. Loop through
        for (int inx = 0; inx < itemRevisionsAry.length; inx++)
        {
            // 2.1 create propmap and fill all properties of itemRev into it
            IPropertyMap localPropMap = new SimplePropertyMap(
                    itemRevisionsAry[inx]);
            
            // 2.2 find corresponding disposition propmap
            String itemID = itemRevisionsAry[inx].getString("item_id");
            IPropertyMap dispositionPropMap = findDispositionPropMap(
                    dispositionsAry, itemID);
            
            // 2.3 add disposition properties to local propMap
            if(dispositionPropMap == null)
            {
                localPropMap.setLogical( m_registry.getString("TargetsTableColumnName.HasDisposition"), false);
            }
            else
            {
                localPropMap.addAll(dispositionPropMap);
                localPropMap.setLogical( m_registry.getString("TargetsTableColumnName.HasDisposition"), true);
            }
            
            // 2.4 add to array
            localPropMapAry[inx] = localPropMap;
        }
        
        // 3. populate table
        TableUtils.populateTable(localPropMapAry, m_table);
        
        // initialize the Maps
        initializeItemRevDispositionMaps();
        
        return true;
    }

    public void initializeItemRevDispositionMaps()
    {
        // initialize the Maps
        m_itemIdToComponentMap = WorkflowProcess.getWorkflow().getStringToComponentMap();
        m_itemIdToDispositionMap = WorkflowProcess.getWorkflow().getStringToDispositionMap();
    }
    
    private IPropertyMap findDispositionPropMap(IPropertyMap[] dispositionsAry,
            String itemID)
    {
        IPropertyMap dispPropMap = null;
        
        for (IPropertyMap propMap : dispositionsAry)
        {
            if (propMap.getString("targetitemid").equalsIgnoreCase(itemID))
            {
                dispPropMap = propMap;
                break;
            }
        }
        
        return dispPropMap;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        String[] excludeColumnIds = getRegistry().getStringArray(
                "TargetsTable.Disposition.EXCLUDE_COLUMN_IDS");
        IPropertyMap[] dispositionPropMap = TableUtils.populateMap(m_table,
                excludeColumnIds);
        
        // add only modified rows
        IPropertyMap[] modifiedRows = getModifiedRows(dispositionPropMap);
        
        propMap.setCompoundArray(
                getRegistry().getString("targetDisposition.PROP"), modifiedRows);
        return true;
    }
    
    private IPropertyMap[] getModifiedRows(IPropertyMap[] dispositionPropMap)
    {
        List<IPropertyMap> modifiedRows = new ArrayList<IPropertyMap>();
        String itemID = "";
        
        for (IPropertyMap disposition : dispositionPropMap)
        {
            itemID = disposition.getString("targetitemid");
            if (m_rowsModified.contains(itemID))
            {
                modifiedRows.add(disposition);
            }
        }
        
        return modifiedRows.toArray(new IPropertyMap[modifiedRows.size()]);
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_table.setEditable(flag);
    }
    
    protected void addDoubleClickListener(final Composite composite)
    {
        m_table.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    popupOverviewDialog(composite);
                }
            }
        });
        
    }
    
    protected void popupOverviewDialog(final Composite composite)
    {
        try
        {
            int rowInx = m_table.getSelectedRow();
            int columnInx = TableUtils.getColumnIndex("item_id", m_table);
            
            String revId = (String) m_table.getModel().getValueAt(rowInx,
                    columnInx);
            
            TCComponent component = m_itemIdToComponentMap.get(revId);
            String objectName = component.getProperty("object_name");
            String objectType = component.getProperty("object_type");
            
            if(!objectType.equals("Documents Revision"))
            {
                final IPropertyMap iMap = new SimplePropertyMap();
                iMap.setComponent(component);
                
                iMap.setString("item_id", revId);
                iMap.setString("object_name", objectName);
                 
                composite.getDisplay().asyncExec(new Runnable()
                {
                    
                    @Override
                    public void run()
                    {
                        PopupDialog dialog = new PopupDialog(composite.getShell());
                        dialog.setInputPropertyMap(iMap);
                        //dialog.setDialogSplitCount(2);
                        dialog.open();
                        System.out.println(" double click");
                    }
                });
            }
            else
            {
                String[] errors = new String[] { getRegistry().getString(
                        "notItemRevision.MSG") };
                TCException exception = new TCException(errors);
                throw exception;
            }
            
        }
        catch (TCException e1)
        {
            MessageBox.post(e1);
        }
    }
    
  
    private void addRowSelectionListener()
    {
        m_table.getSelectionModel().addListSelectionListener(
                new ListSelectionListener()
                {
                    @Override
                    public void valueChanged(
                            ListSelectionEvent paramListSelectionEvent)
                    {
                        publishDispositionData();
                    }
                });
    }
    
    private void publishDispositionData()
    {
        int rowInx = m_table.getSelectedRow();
        if(rowInx >= 0)
        {
            IPropertyMap propMap = getSelectedDisposition(rowInx);
            
            // adding hasDisposition virtual property in property map
            // so that we can enable or disable disposition panel
            String hasDispositionStr = m_registry.getString("TargetsTableColumnName.HasDisposition");
            int colIndex = ((TCTable)m_table).getColumnIndex( hasDispositionStr );
            if( colIndex >= 0)
            {
                Boolean bHasDispo = (Boolean) m_table.getValueAt(rowInx, colIndex);
                propMap.setLogical(hasDispositionStr, bHasDispo);
            }
            
            IController controller = ControllerFactory.getInstance()
                    .getDefaultController();
            PublishEvent event = new PublishEvent(this,
                    GlobalConstant.EVENT_TARGETS_TABLE_ROW_SELECTED, propMap, null);
            
            controller.publish(event);
        }
    }
    
    private IPropertyMap getSelectedDisposition(int rowIndex)
    {
        IPropertyMap propMap = new SimplePropertyMap();
        
        String[] columnIds = ((AIFTableModel) m_table.getModel())
                .getColumnIdentifiers();
        String[] excludeColumnIds = getRegistry().getStringArray(
                "TargetsTable.Disposition.EXCLUDE_COLUMN_IDS");
        
        String[] requiredColumnIds = excluedeColumns(columnIds,
                excludeColumnIds);
        
        for (String columnID : requiredColumnIds)
        {
            int columnInx = TableUtils.getColumnIndex(columnID, m_table);
            String value = (String) m_table.getModel().getValueAt(rowIndex,
                    columnInx);
            propMap.setString(columnID, value);
        }
        
        return propMap;
    }
    
    private String[] excluedeColumns(String[] columnIds,
            String[] excludeColumnIds)
    {
        String[] requiredColumnIds = null;
        
        Vector<String> My_Vector = new Vector<String>();
        for (int inx = 0; inx < columnIds.length; inx++)
        {
            My_Vector.add(columnIds[inx]);
        }
        
        for (int inx = 0; inx < excludeColumnIds.length; inx++)
        {
            My_Vector.remove(excludeColumnIds[inx]);
        }
        requiredColumnIds = My_Vector.toArray(new String[My_Vector.size()]);
        
        return requiredColumnIds;
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
    
    public TCTable getTable()
    {
        return m_table;
    }
    
    private void addTableModelListner()
    {
        m_table.getModel().addTableModelListener(new TableModelListener()
        {
            
            @Override
            public void tableChanged(TableModelEvent tme)
            {
                if (tme.getType() == TableModelEvent.UPDATE)
                {
                    publishRowModified( tme );
                    //System.out.println("===========>>>>>>>> Row modified :: " + tme.getFirstRow());
                }
            }
        });
    }
    
    private void publishRowModified(TableModelEvent tmEvent)
    {
        int rowInx = tmEvent.getFirstRow();
        int colInx = TableUtils.getColumnIndex("targetitemid", m_table);
        
        String targetItemID = (String) m_table.getModel().getValueAt(rowInx,
                colInx);
        
        IController controller = ControllerFactory.getInstance()
                .getDefaultController();
        PublishEvent event = new PublishEvent(this, "TableCellValueChanged",
                targetItemID, null);
        controller.publish(event);
    }
    
    private void registerProperties()
    {
        IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        l_controller.registerSubscriber("TableCellValueChanged", this);
        l_controller.registerSubscriber(
                GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED, this);
        l_controller.registerSubscriber(
                GlobalConstant.EVENT_COPY_ALL_DISPOSITION_PROPERTY, this);
        l_controller.registerSubscriber(
                GlobalConstant.EVENT_TARGETS_ADDED, this);
        
    }
    
    protected void createDataModelMapping()
    {
        DataBindingHelper dataBindingHelper = new DataBindingHelper();
        dataBindingHelper.bindData(m_table, /*"entargetsdisposition"*/ getRegistry().getString("targetDisposition.PROP"),
                FormHelper.getDataModelName());
    }
    
    @Override
    public void update(PublishEvent event)
    {
        if (event.getPropertyName().equals("TableCellValueChanged"))
        {
            m_rowsModified.add((String) event.getNewValue());
        }
        else if (event.getPropertyName().equals(
                GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED))
        {
            IPropertyMap propMap = (IPropertyMap) event.getNewValue();
            updateDispositionData(propMap);
        }
        else if (event.getPropertyName().equals(
                GlobalConstant.EVENT_COPY_ALL_DISPOSITION_PROPERTY))
        {
            IPropertyMap propMap = (IPropertyMap) event.getNewValue();
            copyAllDispositionProps(propMap);
        }
        else if (event.getPropertyName().equals(
                GlobalConstant.EVENT_TARGETS_ADDED))
        {
            Map<TCComponent, TCComponent> targetToDispositionMap = (Map) event.getNewValue();
            addTargtesToTable(targetToDispositionMap);
        }
        
    }


    private void updateDispositionData(IPropertyMap propMap)
    {
        int rowIndx = m_table.getSelectedRow();
        if (rowIndx >= 0)
        {
            try
            {
                Map<Integer, IPropertyMap> map = new HashMap<>();
                map.put(rowIndx, propMap );
                TableUtils.updateTable(m_table, map);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }
 
    private void addTargtesToTable(Map<TCComponent, TCComponent> targetToDispositionMap)
    {
        try
        {
            // Cache the disposition properties. Targets item rev properties 
            //has already been cached in AddTargetToWorkflow
            String[] dispoPropName = getRegistry().getStringArray( "targetdisposition.PROPERTIES_AS_ATTRIBUTE");
            cacheRequiredProperties(targetToDispositionMap, dispoPropName);
            
            String[] itemRevPropName = getRegistry().getStringArray( "ItemRevision.PROPERTIES_AS_ATTRIBUTE");
            
            // Create propertiesMaps lists
            List<IPropertyMap> targetItemRevsList = new ArrayList<>();
            List<IPropertyMap> dispositionList = new ArrayList<>();
            
            String itemID = "";
            Set<TCComponent> targetsSet = targetToDispositionMap.keySet();
            for( TCComponent eachTarget : targetsSet )
            {
                IPropertyMap itemRevPropMap = getPropMap(eachTarget, itemRevPropName);
                targetItemRevsList.add(itemRevPropMap);
                
                // update the itemIdToComponentMap in WorkflowProcess class
                 itemID = eachTarget.getProperty("item_id");
                 m_itemIdToComponentMap.put(itemID, eachTarget);
                
                TCComponent disposition = (TCComponent) targetToDispositionMap.get(eachTarget);
                if(disposition != null)
                {
                    IPropertyMap dispoPropMap = getPropMap(disposition, dispoPropName);
                    dispositionList.add(dispoPropMap);
                    
                    // update the itemIdToDispositionMap in WorkflowProcess class
                     m_itemIdToDispositionMap.put( itemID, disposition);
                }
            }
            
            IPropertyMap dummyPropertyMap = new SimplePropertyMap();
            dummyPropertyMap.setCompoundArray( "targetsItemRevision",
                            targetItemRevsList.toArray(new IPropertyMap[targetItemRevsList.size()]));
            
            dummyPropertyMap.setCompoundArray(m_registry.getString("targetDisposition.PROP"), 
                             dispositionList.toArray(new IPropertyMap[dispositionList.size()]));
            
            load(dummyPropertyMap);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        m_table.repaint();
    }

    private IPropertyMap getPropMap(TCComponent inTCComponent, String[] propNames) throws TCException
    {
        IPropertyMap propertyMap = new SimplePropertyMap();
        PropertyMapHelper.componentToMap( inTCComponent, propNames, propertyMap );
        propertyMap.setComponent(inTCComponent);
        
        return propertyMap;
    }

    private void cacheRequiredProperties(Map<TCComponent, TCComponent> targetToDispositionMap, String[] propName) throws TCException
    {
        Set<TCComponent> keySet = targetToDispositionMap.keySet();
        List<TCComponent> dispositionList = new ArrayList<TCComponent>();
        
        for( TCComponent eachKey : keySet)
        {
            if( targetToDispositionMap.get(eachKey) != null )
            {
                dispositionList.add(targetToDispositionMap.get(eachKey) );
            }
        }
        
        TCComponentType.getPropertiesSet( dispositionList, propName );
    }


    private void copyAllDispositionProps(IPropertyMap propMap)
    {
        try
        {
            int colIndex = ((TCTable)m_table).getColumnIndex("hasDisposition");
            
            Map<Integer, IPropertyMap> map = new HashMap<>();
            for(int rowIndex = 0; rowIndex < m_table.getRowCount(); rowIndex ++)
            {
               // map.put(rowIndex, propMap);
                if( colIndex >= 0)
                {
                    Boolean hasDisposition = (Boolean) m_table.getValueAt(rowIndex, colIndex);
                    if(hasDisposition)
                    {
                        map.put(rowIndex, propMap);
                    }
                }

            }
            
            TableUtils.updateTable(m_table, map);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        m_table.repaint();
    }
    
    /**
	 * Method to add target item revisions
	 */
	public void addTargetsToWorkflow()
	{
		 new AddTargetToWorkflow( m_registry );
	}

	/**
     * Method to remove selected target item revisions
     */
    public void removeSelectedTargets()
    {
        try
        {
            validateTableRowSelection();
            
            if(confirmRemoval())
            {
                TCComponentTask rootTask = WorkflowProcess.getWorkflow().getRootTask();
                
                TCComponent[] selectedTargets = getSelectedItemRevisions();
                if(selectedTargets.length > 0)
                {
                    ENAddRemoveTargetsHelper helper = new ENAddRemoveTargetsHelper();
                    TCComponent[] removedTargets = helper.removeTargets( selectedTargets, rootTask );
                    
                 // Remove returned objects form table
                    removeRowsFormTable(removedTargets);
                }
            }
        }
        catch (TCException ex)
        {
            MessageBox.post(ex.getMessage(), "ERROR", MessageBox.ERROR);
        }
    }

    /**
     * This method will open the pdf dataset attached to the RDD latest revision.
     */
    public void viewSelectedRDDPDF()
    {
        try
        {
            // validate single row is selected from table
            validateSingleSelection();
            
            TCComponent[] itemRevObject = getSelectedItemRevisions();
            TCComponentItemRevision rddObject = null;
            // check the type of item revision selected
            String objectType = getSelectedRowType();
            if(objectType.startsWith( m_registry.getString("DocsItem.TYPE")))
            {                 
                rddObject = (TCComponentItemRevision) itemRevObject[0];
            }
            else
            {
                TCComponentItem rdd = (TCComponentItem) itemRevObject[0].getRelatedComponent("RelatedDefiningDocument");
                checkIsRDDAttached(rdd);
                rddObject = rdd.getLatestItemRevision();
            }
            
            if(rddObject != null)
            {
                openPDFDataset(rddObject);
            }
          
        }
        catch (TCException e)
        {
            MessageBox.post(e);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void checkIsRDDAttached(TCComponentItem rdd) throws TCException
    {
        if(rdd == null)
        {
            String[] errorMsg = new String[1];
            errorMsg[0] = m_registry.getString("NoRDDAttached.MSG");
            throw new TCException(errorMsg);
        }
    }
    
    private void openPDFDataset(TCComponent rddRev) throws TCException, IOException
    {
        TCComponent[] datasetArray = rddRev.getRelatedComponents("IMAN_specification");
        boolean isPDFDatasetFound = false;
        for(TCComponent dataset : datasetArray)
        {
            if(dataset instanceof TCComponentDataset &&
                    ((TCComponentDataset)dataset).getType().equalsIgnoreCase("PDF"))
            {
                TCComponent[] namedRef = ((TCComponentDataset)dataset).getNamedReferences();
                if(namedRef != null && namedRef.length > 0)
                {
                    ((TCComponentDataset)dataset).open();
                }
                else
                {
                    String[] errorMsg = new String[2];
                    errorMsg[0] = m_registry.getString("ErrorDocView.MSG");
                    errorMsg[1] = m_registry.getString("ReferenceToViewMissing.MSG");
                    throw new TCException(errorMsg);
                }
                isPDFDatasetFound = true;
                break;
            }
        }
        
        if( isPDFDatasetFound == false)
        {
            String[] errorMsg = new String[2];
            errorMsg[0] = m_registry.getString("ErrorDocView.MSG");
            errorMsg[1] = m_registry.getString("NoPDFDatasetFound.MSG");
            throw new TCException(errorMsg);
        }
    }
    
    
    private void validateTableRowSelection() throws TCException
    {
        if( m_table != null && m_table.getSelectedRowCount() < 1)
        {
            String[] errors =  new String[]{ m_registry.getString("selectPartToRemove.MSG") };
            TCException exception = new TCException( errors );
            
            throw exception;
        }
    }
    
    protected TCComponent[] getSelectedItemRevisions()
    {
        List<TCComponent> targetToRemove = new ArrayList<>(); 
        
        // 1.0 Get all selected item revisions from table
        int itemIDColIndex = m_table.getColumnIndex(m_registry
                .getString("TargetsTableColumnIdentifier.ItemID"));
        
        for(int rowIndex : m_table.getSelectedRows())
        {
            String itemID = (String) m_table.getValueAt(rowIndex, itemIDColIndex);
            TCComponent itemRevision = m_itemIdToComponentMap.get(itemID);
            
            if(itemRevision != null)
            {
                targetToRemove.add(itemRevision);
            }
        }
        
        return targetToRemove.toArray( new TCComponent[targetToRemove.size()]);
    }
    
    
    private void removeRowsFormTable(Object[] addRemRetObj) throws TCException
        {
            // Remove selected rows from table
            int itemIDColIndex = m_table.getColumnIndex(m_registry
                    .getString("TargetsTableColumnIdentifier.ItemID"));
            
            for(Object returnObject : addRemRetObj)
            {
                if(returnObject instanceof TCComponentItemRevision)
                {
                    String returnObjItemID =  ((TCComponentItemRevision) returnObject).getProperty("item_id");
                    m_itemIdToComponentMap.remove(returnObjItemID);
                    m_itemIdToDispositionMap.remove( returnObjItemID );
                    
                    for(int rowIndex  = 0; rowIndex < m_table.getRowCount(); rowIndex++)
                    {
                        if(returnObjItemID.equals(m_table.getValueAt(rowIndex, itemIDColIndex ) ))
                        {
                            m_table.removeRow(rowIndex);
                        }
                    }
                }
            }
        }

    /*
     * (non-Javadoc)
     * Keeping this method public so that it can be invoke from dialog panel
     */
    public void unRegisterSubscriber()
    {
    	IController l_controller = ControllerFactory.getInstance()
                .getDefaultController();
        l_controller.unregisterSubscriber("TableCellValueChanged", this);
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_DISPOSITION_DATA_CHANGED, this);
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_COPY_ALL_DISPOSITION_PROPERTY, this);
        l_controller.unregisterSubscriber(
                GlobalConstant.EVENT_TARGETS_ADDED, this);
    }
    
    @Override
    public void dispose()
    {
    	unRegisterSubscriber();
        super.dispose();
    }

    private String getSelectedRowType()
    {
        String objectType = "";
        
        int rowInx = m_table.getSelectedRow();
        int colInx = TableUtils.getColumnIndex("object_type", m_table);
        
        if(rowInx >= 0 && colInx >= 0)
        {
            objectType = (String) m_table.getModel().getValueAt(rowInx, colInx);
        }
        return objectType;
    }
    
    protected void validateSingleSelection() throws TCException
    {
        if(m_table.getSelectedRowCount() != 1)
        {
            String[] errors =  new String[]{ m_registry.getString("pleaseSelectOneRow.MSG") };
            TCException exception = new TCException( errors );
            
            throw exception;
        }
    }
    
    protected boolean isSelectedItemComparable()
    {
        boolean isComparable = false;
        try
        {
            TCComponent[] selectedItemRevisions = getSelectedItemRevisions();
            String selectedItemType = selectedItemRevisions[0].getProperty("object_type");
            if(!selectedItemType.equals("Documents Revision"))
            {
                TCComponent[] itemRevisions = FormHelper.getItemRevisions(selectedItemRevisions[0]);
                
                if(itemRevisions.length > 1)
                {
                    isComparable = true;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return isComparable;
    }
    
    protected boolean confirmRemoval()
    {
        boolean response = MessageDialog.openQuestion(getComposite().getShell(), m_registry.getString("Confirm.Label"), m_registry.getString("RemovalConfirm.Msg"));
        return response;
    }
}
