package com.nov.rac.form.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public final class DispositionInstructionDialog extends AbstractSWTDialog
{
    
    //private Button m_saveButton;
    private DispositionInstructionDialogPanel m_panel;
    private DispositionInstructionDialogPanel m_dialogPanel;

    public DispositionInstructionDialog(Shell shell)
    {
        super(shell);
    }
    
    protected void setShellStyle(int arg0)
    {
        super.setShellStyle(SWT.CLOSE | SWT.APPLICATION_MODAL | SWT.RESIZE);
    }
    
    @Override
    protected Control createContents(Composite parent)
    {
        parent.setLayout( new GridLayout(1, true));
        parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        m_dialogPanel = new DispositionInstructionDialogPanel(parent, SWT.NONE);
        m_dialogPanel.createUI();
        
        setDialogLocationAndSize();
        return parent;
    }
    
    public void setDispositionInstruction(String instuction)
    {
        m_dialogPanel.setDisoInstruction(instuction);
    }
    
    public Object getDispositionInstruction()
    {
        return m_dialogPanel.getDisoInstruction();
    }
    
    public void setObjectType(String objectType)
    {
        m_dialogPanel.setObjectType(objectType);
    }
    
    public void setObjectString(String objectString)
    {
        m_dialogPanel.setObjectString(objectString);
    }
    
    private void setDialogLocationAndSize()
    {
        Rectangle monitorArea = getShell().getDisplay().getClientArea();
        Rectangle shellArea = getShell().getBounds();
        
        this.getShell().setSize((int) (0.6 * (monitorArea.width)),
                (int) (0.6 * (monitorArea.height))); // set according to screen
        
        int x = monitorArea.x + (monitorArea.width - shellArea.width) / 2;
        int y = monitorArea.y + (monitorArea.height - shellArea.height) / 2;
        getShell().setLocation(x, y);
    }

}