package com.nov.rac.form.common;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.form.dialog.PopupDialog;
import com.nov.rac.form.util.AttachmentsButton;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class AttachmentsBoxPanel extends AbstractUIPanel implements
        INOVFormLoadSave
{
    protected Registry m_registry = null;
    
    private Group m_group = null;
    protected TCTable m_table = null;
    private AttachmentsButton m_buttonBar = null;
    // private Button m_viewAttchBtn = null;
    
    private Label m_nameLabel;
    
    public AttachmentsBoxPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        Composite composite = getComposite();
        
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        composite.setLayout(gl_l_composite);
        
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
                1);
        composite.setLayoutData(gridData);
        
        // createGroupComposit(composite);
        createButtonBar(composite);
        createTCTable(composite);
        
        addSelectionListener();
        addDoubleClickListener();
        
        return true;
    }
    
    private void addDoubleClickListener()
    {
        m_table.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    
                    AIFComponentContext[] objects = m_table
                            .getSelectedContextObjects();
                    TCComponentDataset dataset = (TCComponentDataset) objects[0]
                            .getComponent();
                    try
                    {
                        TCComponent[] namedRef = dataset.getNamedReferences();
                        if (namedRef != null && namedRef.length > 0)
                        {
                            dataset.open();
                        }
                        else
                        {
                            String[] errorMsg = new String[2];
                            errorMsg[0] = m_registry
                                    .getString("ErrorDocView.MSG");
                            errorMsg[1] = m_registry
                                    .getString("ReferenceToViewMissing.MSG");
                            throw new TCException(errorMsg);
                        }
                    }
                    catch (TCException e1)
                    {
                        // e.printStackTrace();
                        MessageBox.post(e1);
                    }
                    catch (IOException e2)
                    {
                        e2.printStackTrace();
                    }
                    
                }
            }
        });
        
    }
    
    // private void addRowSelectionListener()
    // {
    //
    // m_table.getSelectionModel().addListSelectionListener(
    // new ListSelectionListener()
    // {
    // @Override
    // public void valueChanged(
    // ListSelectionEvent paramListSelectionEvent)
    // {
    //
    // AIFComponentContext[] objects = m_table
    // .getSelectedContextObjects();
    // TCComponentDataset dataset = (TCComponentDataset) objects[0]
    // .getComponent();
    // try
    // {
    // TCComponent[] namedRef = dataset
    // .getNamedReferences();
    // if (namedRef != null && namedRef.length > 0)
    // {
    // dataset.open();
    // }
    // else
    // {
    // String[] errorMsg = new String[2];
    // errorMsg[0] = m_registry
    // .getString("ErrorDocView.MSG");
    // errorMsg[1] = m_registry
    // .getString("ReferenceToViewMissing.MSG");
    // throw new TCException(errorMsg);
    // }
    // }
    // catch (TCException e)
    // {
    // // e.printStackTrace();
    // MessageBox.post(e);
    // }
    // catch (IOException e)
    // {
    // e.printStackTrace();
    // }
    //
    // }
    // });
    //
    // }
    
    // private void createGroupComposit(Composite l_composite)
    // {
    // m_group = new Group(l_composite, SWT.NONE);
    // m_group.setLayout(new GridLayout(1, true));
    //
    // GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
    // m_group.setLayoutData(gridData);
    //
    // m_group.setText("Defalut Attachment Panel");
    // SWTUIHelper.setFont(m_group, SWT.BOLD);
    // }
    
    private void createTCTable(Composite composite)
    {
        Composite comp = new Composite(composite, SWT.EMBEDDED);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 25);
        // gridData.heightHint = 100;
        GridLayout gl_l_composite = new GridLayout(1, true);
        gl_l_composite.marginWidth = 0;
        gl_l_composite.marginHeight = 0;
        comp.setLayout(gl_l_composite);
        comp.setLayoutData(gridData);
        
        /*
         * String[] columnNames = new String[2]; columnNames[0] =
         * m_registry.getString("TypeHeader.Label"); columnNames[1] =
         * m_registry.getString("NameHeader.Label");
         */
        
        String[] columnNames = getColumnNames();
        String[] columnIdentifiers = getColumnIdentifiers();
        
        /*
         * m_table = new TCTable(new String[] { "object_type", "object_name" },
         * columnNames);
         */
        m_table = new TCTable(columnIdentifiers, columnNames);
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        
        m_table.sortColumn(1);
        m_table.setEditable(false);
        m_table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        DefaultTableCellRenderer rend = (DefaultTableCellRenderer) m_table
                .getTableHeader().getDefaultRenderer();
        rend.setHorizontalAlignment(JLabel.CENTER);
        
        JPanel tablePanle = new JPanel();
        BorderLayout borderLayout = new BorderLayout();
        
        tablePanle.setLayout(new BorderLayout());
        
        ScrollPagePane scrollPane = new ScrollPagePane(m_table);
        tablePanle.add(scrollPane);
        
        SWTUIUtilities.embed(comp, tablePanle, false);
    }
    
    protected String[] getColumnNames()
    {
        String[] columnNames = new String[2];
        columnNames[0] = m_registry.getString("TypeHeader.Label");
        columnNames[1] = m_registry.getString("NameHeader.Label");
        
        return columnNames;
    }
    
    protected String[] getColumnIdentifiers()
    {
        String columnIdentifiers[] = new String[] { "object_type",
                "object_name" };
        return columnIdentifiers;
    }
    
    protected void createButtonBar(Composite composite)
    {
        Composite buttonComposite = new Composite(composite, SWT.CENTER);
        GridData buttonCompositgridData = new GridData(SWT.FILL, SWT.CENTER,
                true, true, 1, 1);
        buttonComposite.setLayoutData(buttonCompositgridData);
        GridLayout gl_buttonComposite = new GridLayout(2, false);
        gl_buttonComposite.marginWidth = 0;
        gl_buttonComposite.marginHeight = 0;
        buttonComposite.setLayout(gl_buttonComposite);
        
        createHeaderName(buttonComposite);
        
        // m_viewAttchBtn = new Button(buttonComposite, SWT.NONE);
        // m_viewAttchBtn.setText("View Attachment");
        // m_viewAttchBtn.setVisible(false);
        // addViewAttachmentButtonListener();
        
        Image minusButtonImage = new Image(buttonComposite.getDisplay(),
                m_registry.getImage("removeButton.IMAGE"), SWT.NONE);
        Image pluseButtonImage = new Image(buttonComposite.getDisplay(),
                m_registry.getImage("addButton.IMAGE"), SWT.NONE);
        
        m_buttonBar = new AttachmentsButton(buttonComposite, SWT.NONE,
                pluseButtonImage, minusButtonImage);
        GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1,
                1);
        m_buttonBar.setLayoutData(gridData);
        
    }
    
    protected void createHeaderName(Composite composite)
    {
        m_nameLabel = new Label(composite, SWT.NONE);
        SWTUIHelper.setFont(m_nameLabel, SWT.BOLD);
        m_nameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false,
                false, 1, 1));
    }
    
    protected void setNameLabel(String name)
    {
        m_nameLabel.setText(name);
    }
    
    protected void setGroupCompositName(String name)
    {
        m_group.setText(name);
    }
    
    private void addSelectionListener()
    {
        if (m_buttonBar != null)
        {
            m_buttonBar
                    .addSelectionListenerToPlusButton(new SelectionListener()
                    {
                        @Override
                        public void widgetSelected(SelectionEvent arg0)
                        {
                            plusButtonSelectionListener();
                        }
                        
                        @Override
                        public void widgetDefaultSelected(SelectionEvent arg0)
                        {
                        }
                    });
            
            m_buttonBar
                    .addSelectionListenerToMinusButton(new SelectionListener()
                    {
                        @Override
                        public void widgetSelected(
                                SelectionEvent paramSelectionEvent)
                        {
                            minusButtonSelectionListener();
                        }
                        
                        @Override
                        public void widgetDefaultSelected(
                                SelectionEvent paramSelectionEvent)
                        {
                        }
                    });
        }
    }
    
    protected void plusButtonSelectionListener()
    {
        // plus button functionality
    }
    
    protected void minusButtonSelectionListener()
    {
        // minus button functionality
    }
    
    protected void addRowsToTable(List<TCComponentDataset> datasetArray)
    {
        try
        {
            if (!datasetArray.isEmpty())
            {
                TCComponentType.getPropertiesSet(datasetArray, new String[] {
                        "object_name", "object_type" });
                
                m_table.dataModel.addRows(datasetArray);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    // private void addTableRowSelectionListener()
    // {
    // m_table.getSelectionModel().addListSelectionListener(
    // new ListSelectionListener()
    // {
    // @Override
    // public void valueChanged(
    // ListSelectionEvent paramListSelectionEvent)
    // {
    //
    // Display.getDefault().asyncExec(new Runnable()
    // {
    // public void run()
    // {
    //
    // if ((m_table.getSelectedRowCount() == 0))
    // {
    // // removeViewAttachmentButton();
    // m_viewAttchBtn.setVisible(false);
    // }
    // else
    // {
    // m_viewAttchBtn.setVisible(true);
    // // addViewAttachmentButton();
    // }
    // }
    // });
    //
    // }
    // });
    //
    // }
    
    // private void removeViewAttachmentButton()
    // {
    // Display.getDefault().asyncExec(new Runnable()
    // {
    // public void run()
    // {
    // m_viewAttchBtn.setVisible(false);
    //
    // //
    // }
    //
    // });
    //
    // }
    //
    // private void addViewAttachmentButton()
    // {
    // Display.getDefault().asyncExec(new Runnable()
    // {
    // public void run()
    // {
    // m_viewAttchBtn.setVisible(true);
    //
    // }
    //
    // });
    // }
    //
    // private void addViewAttachmentButtonListener()
    // {
    // m_viewAttchBtn.addSelectionListener(new SelectionListener()
    // {
    // @Override
    // public void widgetSelected(SelectionEvent paramSelectionEvent)
    // {
    // AIFComponentContext[] objects = m_table
    // .getSelectedContextObjects();
    // TCComponentDataset dataset = (TCComponentDataset) objects[0]
    // .getComponent();
    // try
    // {
    // TCComponent[] namedRef = dataset.getNamedReferences();
    // if (namedRef != null && namedRef.length > 0)
    // {
    // dataset.open();
    // }
    // else
    // {
    // String[] errorMsg = new String[2];
    // errorMsg[0] = m_registry.getString("ErrorDocView.MSG");
    // errorMsg[1] = m_registry
    // .getString("ReferenceToViewMissing.MSG");
    // throw new TCException(errorMsg);
    // }
    // }
    // catch (TCException e)
    // {
    // // e.printStackTrace();
    // MessageBox.post(e);
    // }
    // catch (IOException e)
    // {
    // e.printStackTrace();
    // }
    // }
    //
    // @Override
    // public void widgetDefaultSelected(SelectionEvent paramSelectionEvent)
    // {
    // }
    // });
    // }
    
    @Override
    public boolean reload() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return true;
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        m_buttonBar.setEnabled(flag);
        m_table.setEnabled(flag);
    }
    
    protected boolean confirmRemoval()
    {
        boolean response = MessageDialog.openQuestion(m_buttonBar.getShell(),
                m_registry.getString("Confirm.Label"),
                m_registry.getString("RemovalConfirm.Msg"));
        return response;
    }
    
    protected void validateTableRowSelection() throws TCException
    {
        if (m_table != null && m_table.getSelectedRowCount() < 1)
        {
            String[] errors = new String[] { m_registry
                    .getString("NoItemSelected.MSG") };
            TCException exception = new TCException(errors);
            
            throw exception;
        }
    }
}
