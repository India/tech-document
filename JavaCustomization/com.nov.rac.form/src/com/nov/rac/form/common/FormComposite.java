package com.nov.rac.form.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVForm;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IExpandable;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class FormComposite extends Composite
{
    private Registry m_registry = null;
    private Composite m_composite = null;
    private INOVForm m_novForm = null;
    private final String CLASS = "CLASS";
    private final String FORM_DATA_MODEL = "FORM_DATA_MODEL";
    private CTabFolder m_folder = null;
    private final String NAME = "Name";
    
    public FormComposite(Composite parent, int style)
    {
        super(parent,style);
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }
    
    public boolean createUI()
    {
        GridLayout parentLayout = SWTUIUtilities.tightGridLayout(1);
        this.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true,
                1, 1));
        this.setLayout(new GridLayout(1, false));
        
        m_folder = new CTabFolder(this, SWT.TOP | SWT.BORDER);
        // folder.setUnselectedCloseVisible(false);
        m_folder.setLayout(new GridLayout(1, false));
        m_folder.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true,
                1, 1));
        
        TCComponentForm form = (TCComponentForm) FormHelper
                .getTargetComponent();
        String formType = FormHelper.getTargetComponentType();
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        StringBuilder formName = new StringBuilder("");
        formName.append(formType).append(".").append(NAME);
        
        final CTabItem tab = new CTabItem(m_folder, SWT.NONE);
        tab.setText(getRegistry().getString(formName.toString()));
        m_folder.setSelection(tab);
        FormHelper.setFolder(m_folder);
        
        final ScrolledComposite sc = new ScrolledComposite(m_folder,
                SWT.H_SCROLL | SWT.V_SCROLL);
        sc.setLayout(new GridLayout(1, false));
        sc.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true, 1, 1));
        
        final Composite mainComposite = new Composite(sc, SWT.NONE);
        mainComposite.setLayout(parentLayout);
        m_composite = getComposite(mainComposite);
        
        String[] viewerClass = RegistryUtils.getConfiguration(theGroup,
                formType + "." + CLASS, getRegistry());
        
        Object[] params = new Object[2];
        params[0] = form;
        params[1] = getRegistry();
        
        try
        {
            m_novForm = (INOVForm) Instancer.newInstanceEx(viewerClass[0],
                    params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        registerDataModel();
        
        m_novForm.createUI(m_composite);
        setPanelsState();
        
        sc.setContent(mainComposite);
        sc.setExpandHorizontal(true);
        sc.setExpandVertical(true);
        sc.setMinSize(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        tab.setControl(sc);
        //AIFUtility.getDefaultSession().addAIFComponentEventListener(this);
        return false;
    }
    
    private Composite getComposite(Composite mainComposite)
    {
        Composite l_composite = new Composite(mainComposite, SWT.NONE);
        l_composite.setLayout(new GridLayout(1, false));
        l_composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        return l_composite;
    }
    
    private void registerDataModel()
    {
        FormHelper.setDataModelName(FORM_DATA_MODEL);
        DataBindingRegistry dataBindingRegistry = DataBindingRegistry
                .getRegistry();
        dataBindingRegistry.registerDataModel(FormHelper.getDataModelName(),
                new DataBindingModel());
    }
    
    /*
     * function: setPanelsState() Description: This function is called to set
     * the state of the panels within form. Panel state may be expanded or
     * collapsed.
     */
    private void setPanelsState()
    {
        IExpandable novExpandableForm = (IExpandable) m_novForm;
        novExpandableForm.setExpanded(false);
    }
    
    public boolean load(IPropertyMap propMap)
    {
        m_novForm.load(propMap);
        
        return true;
    }
    
    public boolean save() throws Exception
    {
        m_novForm.save();
        return true;
    }
    
    public void setEnabled(boolean flag)
    {
        m_novForm.setEnabled(flag);
    }
    
    /*@Override
    public void dispose()
    {
        this.dispose();
    }*/
    
}
