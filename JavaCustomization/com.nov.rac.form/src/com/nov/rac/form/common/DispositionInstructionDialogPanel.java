package com.nov.rac.form.common;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DispositionInstructionDialogPanel extends AbstractUIPanel
{
    private Text m_textBox;
    private CLabel m_itemRevLabel;
    private Button m_SaveButton;
    private Button m_CancelButton;
    
    private String m_instruction = "";
    private Registry m_registry;

    public DispositionInstructionDialogPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
    }
 
    public boolean createUI()
    {
        Composite parent = getComposite();
        
        parent.getShell().setText(m_registry.getString("DispositionInstructionDialog.TITLE") );
        parent.setLayout(new GridLayout(1, false));
        parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        createItemRevisionPanel(parent);
        createInstructionTextBox(parent);
        createButtonBar(parent);
        
        addSaveButtonListeners();
        addCancelButtonListeners();
        
        return true;
    }
    
    private void addCancelButtonListeners()
    {
        m_CancelButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                getComposite().getShell().dispose();
            }
        });
    }

    private void addSaveButtonListeners()
    {
        m_SaveButton.addSelectionListener( new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                //publishDispositionInstructionChanged();
                m_instruction = m_textBox.getText();
                
                getComposite().getShell().dispose();
            }
        });
        
    }

    private void createButtonBar(Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayoutData( new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        l_composite.setLayout( new GridLayout(2, false));
        
        m_SaveButton = new Button(l_composite, SWT.PUSH);
        m_SaveButton.setText("SAVE");
        m_SaveButton.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, true, 1, 1));
        
        m_CancelButton = new Button(l_composite, SWT.PUSH);
        m_CancelButton.setText("CANCEL");
        m_CancelButton.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, true, 1, 1));
    }

    private void createInstructionTextBox(Composite parent)
    {
        m_textBox = new Text(parent, SWT.BORDER);
        m_textBox .setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1 ));

    }

    private void createItemRevisionPanel( Composite parent)
    {
        Composite l_composite = new Composite(parent, SWT.NONE);
        l_composite.setLayoutData( new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        l_composite.setLayout( new GridLayout(2, false));
        
        Label label = new Label(l_composite, SWT.NONE);
        label.setText("Part Revision");
        label.setLayoutData( new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        
        m_itemRevLabel = new CLabel(l_composite, SWT.BORDER);
        m_itemRevLabel.setLayoutData( new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
    }

    public String getInstructions()
    {
        return m_textBox.getText();
    }

    @Override
    public boolean reload() throws TCException
    {
        // TODO Auto-generated method stub
        return false;
    }

    public void setObjectType(String objectType)
    {
        Image imageIcon = TCTypeRenderer.getTypeImage(objectType, null);
        m_itemRevLabel.setImage(imageIcon);
    }

    public void setObjectString(String objectString)
    {
        m_itemRevLabel.setText(objectString);
    }


    public void setDisoInstruction(String instructions)
    {
        m_textBox.setText(instructions);
        m_instruction = instructions;
    }
    
    public Object getDisoInstruction()
    {
        return m_instruction;
    }
    
}
