package com.nov.rac.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.form.helpers.FormHelper;
import com.nov.rac.form.util.INOVForm;
import com.nov.rac.form.util.INOVFormLoadSave;
import com.nov.rac.loaddelegate.ILoadDelegate;
import com.nov.rac.operations.IOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.IExpandable;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.ui.factory.UIPanelFactory;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class NOVProcessForm implements INOVForm,IExpandable
{
    private final String SAVE_DELEGATE = "SAVE_DELEGATE";
    private final String LOAD_DELEGATE = "LOAD_DELEGATE";
    protected ArrayList<IUIPanel> m_panels = null;
    protected IPropertyMap m_propertyMap = null;
    private UIPanelFactory m_panelFactory = null;
    private Registry m_registry = null;
    private TCComponentForm m_form = null;
    private String m_formType = null;
    private final String COLLAPSED_PANELS = "COLLAPSED_PANELS";
    
    public NOVProcessForm(TCComponentForm tcComponent, Registry registry)
            throws Exception
    {
        m_form = tcComponent;
        m_formType = m_form.getType();
        m_registry = registry;
        
    }
    
    public TCComponent getComponent()
    {
        return m_form;
    }
    
    public String getComponentType()
    {
        return m_formType;
    }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public boolean createUI(Composite parent)
    {
        try
        {
            
            m_panelFactory = UIPanelFactory.getInstance();
            m_panelFactory.setRegistry(getRegistry());
            String theGroup = FormHelper.getTargetComponentOwningGroup();
            try
            {
                m_panels = m_panelFactory.createPanels(parent,
                        getComponentType(), theGroup);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
        addDisposeListener(parent);
        
        return true;
    }
    
    private void addDisposeListener(Composite parent)
    {
    	parent.addDisposeListener( new DisposeListener() 
    	{

    		@Override
    		public void widgetDisposed(DisposeEvent paramDisposeEvent) 
    		{
    			for ( IUIPanel panel : m_panels)
    			{
    				panel.dispose();
    			}
    		}
    	});
    }
    
    /*
     * public boolean load(IPropertyMap propertyMap) { ArrayList<IUIPanel>
     * filteredPanels = m_panels; for (IUIPanel panel : filteredPanels) { if
     * (panel instanceof INOVFormLoadSave) { INOVFormLoadSave loadablepanel =
     * (INOVFormLoadSave) panel; try { loadablepanel.load(propertyMap); } catch
     * (TCException e) { e.printStackTrace(); } } } return true; }
     */
    
    public boolean load(IPropertyMap propertyMap)
    {
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        Object[] objects = new Object[1];
        objects[0] = m_panels;
        
        String[] strOperationDelegate = RegistryUtils.getConfiguration(
                theGroup, getComponentType() + "." + LOAD_DELEGATE,
                getRegistry());
        
        ILoadDelegate iFormLoadDelegate;
        try
        {
            iFormLoadDelegate = (ILoadDelegate) Instancer.newInstanceEx(
                    strOperationDelegate[0], objects);
            iFormLoadDelegate.setRegistry(getRegistry());
            iFormLoadDelegate.loadComponent(propertyMap, theGroup);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return true;
    }
    
    public boolean save() throws Exception
    {
        String theGroup = FormHelper.getTargetComponentOwningGroup();
        Object[] objects = new Object[2];
        objects[0] = getComponent();
        objects[1] = getRegistry();
        
        /*
         * IOperationDelegate iOperationDelegate = (IOperationDelegate)
         * getRegistry() .newInstanceFor(getComponentType() + "." +
         * SAVE_DELEGATE, objects);
         */
        String[] strOperationDelegate = RegistryUtils.getConfiguration(
                theGroup, getComponentType() + "." + SAVE_DELEGATE,
                getRegistry());
        
        IOperationDelegate iOperationDelegate = (IOperationDelegate) Instancer
                .newInstanceEx(strOperationDelegate[0], objects);
        
        iOperationDelegate.registerOperationInputProvider(m_panels);
        
        return iOperationDelegate.executeOperation();
    }
    
    @Override
    public void setEnabled(boolean flag)
    {
        ArrayList<IUIPanel> filteredPanels = m_panels;
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof INOVFormLoadSave)
            {
                INOVFormLoadSave togglepanel = (INOVFormLoadSave) panel;
                togglepanel.setEnabled(flag);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.IExpandable#setExpanded(boolean)
     * Description: This function reads the EXPANDABLE_PANELS confi. from properties file and 
     * sets the panel either in expanded or in collapsed state.
     */
    @Override
    public void setExpanded(boolean expanded)
    {
        ArrayList<IUIPanel> filteredPanels = m_panels;
        
        String formType = FormHelper.getTargetComponentType();
        
       /* String theGroup = FormHelper.getTargetComponentOwningGroup();
        String[] expandablePanels = RegistryUtils.getConfiguration(theGroup,
                formType + "." + COLLAPSED_PANELS, getRegistry());*/
        int theScope = TCPreferenceService.TC_preference_site;
        
        StringBuilder prefrenceName = new StringBuilder(formType);
        prefrenceName.append(".").append("COLLAPSED_PANELS");
        
        String[] expandablePanels = PreferenceUtils.getStringValues(theScope, prefrenceName.toString());
        List<String> expandablePanelsList = new ArrayList<String>();
        
        for(String panel : expandablePanels)
        {
            String panelClassName = getRegistry().getString(panel);
            expandablePanelsList.add(panelClassName);
        }
        
       // List<String> expandablePanelsList = Arrays.asList(expandablePanels);
        
        for (IUIPanel panel : filteredPanels)
        {
            String panelName = panel.getClass().getSimpleName();
            if (panel instanceof IExpandable && expandablePanelsList.contains(panelName))
            {
                IExpandable expandablePanel = (IExpandable) panel;
                expandablePanel.setExpanded(expanded);
            }
        }
    }
}
