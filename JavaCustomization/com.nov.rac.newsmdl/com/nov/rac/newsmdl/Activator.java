package com.nov.rac.newsmdl;

import java.util.Hashtable;

import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.nov.rac.smdl.services.SMDLApplicationService;
import com.nov.rac.smdl.views.SMDLExplorerView;
import com.teamcenter.rac.kernel.AbstractRACPlugin;
import com.teamcenter.rac.services.IAspectService;
import com.teamcenter.rac.services.IAspectUIService;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.util.Registry;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractRACPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "com.nov.rac.newsmdl";

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		Registry registry = Registry.getRegistry("com.nov.rac.smdl.common.common");
		Image reqImage= registry.getImage("requiredFieldImage.ICON");
		FieldDecorationRegistry.getDefault().registerFieldDecoration("NOV_DEC_REQUIRED", "Field is mandatory", reqImage);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	protected void setupServices(BundleContext bundlecontext)
    {
		SMDLApplicationService smdlApplicationService = null;
        try
        {
        	smdlApplicationService = new SMDLApplicationService();
        	smdlApplicationService.setApplicationId("com.nov.rac.smdl.SMDLApplication");
        	
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            return;
        }
     
        Hashtable hashtable = new Hashtable();
        
        hashtable.put("perspectiveId", "com.nov.rac.smdl.perspectives.SMDLPerspective");
        hashtable.put("viewId", SMDLExplorerView.ID);
        
        m_smdlServiceRegistration=bundlecontext.registerService(IOpenService.class.getName(), smdlApplicationService, hashtable);
    
    }

	 private  ServiceRegistration m_smdlServiceRegistration;
	 

	public ServiceRegistration getSmdlServiceRegistration() {
		return m_smdlServiceRegistration;
	}

	@Override
	public IAspectService getLogicService() {
		
		return null;
	}

	@Override
	public IAspectUIService getUIService() {
		
		return null;
	}
	
	
}
