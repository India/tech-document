package com.nov.rac.smdl.createsmdl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;

import com.teamcenter.rac.util.Registry;

public class CreateSMDLWizard extends Wizard
{
	private Registry m_appReg = null;
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	
   	public CreateSMDLWizard()
	{
		super();
		
		//TODO : Kishor Ahuj, Read the title from Registry.
		m_appReg = Registry.getRegistry(this);
		setWindowTitle(m_appReg.getString("CreateSMDLWizard.TITLE"));
		setNeedsProgressMonitor(false);
    }
	
	@Override
	public void addPages() 
	{
		String[] sWizard_Pages = m_appReg.getStringArray("wizard_pages.Group");
		
		for (int iCount = 0 ; iCount < sWizard_Pages.length ; iCount++)
		{
			String pageClassName = sWizard_Pages[iCount] + ".CLASS";
			IWizardPage iSMDLPage= (IWizardPage) m_appReg.newInstanceFor(pageClassName);
				
			if(null != iSMDLPage)
			{
				addPage(iSMDLPage);
				
				if(iSMDLPage.getName().compareTo(CreateSMDLPage.PAGE_NAME) == 0){
					((CreateSMDLPage)iSMDLPage).addPropertyChangeListener(m_listeners);
				}
			}
		}
	}
	public boolean canFinish()
	{
		IWizard wizard = getContainer().getCurrentPage().getWizard();
		IWizardPage page = wizard.getPage(CreateSMDLBasicInfoPage.PAGE_NAME);
		if(getContainer().getCurrentPage() == page)
			return false;
		else 
			return true;
	}

	@Override
	public boolean performFinish() {
		return true;
	}

	@Override
	public boolean needsPreviousAndNextButtons() 
	{
		// TODO Auto-generated method stub
		return true;
	}

	public void addPropertyChangeListener(ArrayList<PropertyChangeListener> listeners) {

		for( PropertyChangeListener listener:listeners )
		{
			if(!m_listeners.contains(listener) ){
				m_listeners.add(listener);
			}
		}		
	}
}