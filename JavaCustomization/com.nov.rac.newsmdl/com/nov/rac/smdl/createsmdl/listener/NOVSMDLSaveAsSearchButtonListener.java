package com.nov.rac.smdl.createsmdl.listener;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;
import com.nov.rac.smdl.createsmdl.search.SMDLSearchResultDialog;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.listener.LoadAllButtonListener;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLSaveAsSearchButtonListener extends LoadAllButtonListener 
{
	private CreateSMDLBasicInfoPanel m_smdlBasicInfoPanel;
	private NovSearchDataModel m_novSearchDataModel;
	public NOVSMDLSaveAsSearchButtonListener(INOVSearchProvider2 searchProvider, NovSearchDataModel novSearchDataModel )
	{
		super(searchProvider,novSearchDataModel);
		m_smdlBasicInfoPanel = (CreateSMDLBasicInfoPanel)searchProvider;
		m_novSearchDataModel = novSearchDataModel;
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		// TODO Auto-generated method stub
			
		Object target=event.getSource();		
		if (target instanceof Button)
		{
			Text smdlSaveAsText = m_smdlBasicInfoPanel.getSaveAsSMDLText();
			
			Button saveAsSMDLRadioButton = m_smdlBasicInfoPanel.getSaveAsSMDLRadio();
			saveAsSMDLRadioButton.setSelection(true);
			Button selectTemplateRadioButton = m_smdlBasicInfoPanel.getSelectedTemplateRadio();
			selectTemplateRadioButton.setSelection(false);
			m_smdlBasicInfoPanel.getSelectedTemplateCombo().deselectAll();
			
			Display.getCurrent().syncExec(new Runnable() 
			{
				@Override
				public void run()
				{
					execute();
					

				}
			});			
			
			Button saveAsSMDLButton=((Button)target);
			Shell shell = saveAsSMDLButton.getDisplay().getActiveShell();
			if(m_novSearchDataModel.getResultSet().getRows()!= 0)
			{
				
				SMDLSearchResultDialog searchResultsDlg = new SMDLSearchResultDialog(shell);
				searchResultsDlg.setNovSearchDataModel(m_novSearchDataModel);
				searchResultsDlg.setSaveAsSMDLField(smdlSaveAsText);
				searchResultsDlg.open();
			
			}
			else
			{
				Registry appReg = Registry.getRegistry("com.nov.rac.smdl.panes.search.search");
				MessageDialog.openInformation(shell,
						appReg.getString( "MESSAGE.TITLE" ), appReg.getString( "NO_OBJECTS_FOUND.MSG" ) );
			}

		}
	}

}
