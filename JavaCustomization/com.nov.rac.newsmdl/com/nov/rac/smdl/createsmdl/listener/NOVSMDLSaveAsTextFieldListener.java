package com.nov.rac.smdl.createsmdl.listener;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;


public class NOVSMDLSaveAsTextFieldListener implements KeyListener 
{
	public NOVSMDLSaveAsTextFieldListener()
	{
	}
	@Override
	public void keyPressed(KeyEvent event) 
	{
		Object target=event.getSource();
		if(target instanceof Text)
		{
			Text saveAsSMDLField =  (Text)target;
			CreateSMDLBasicInfoPanel smdlBasicInfoPanel = getCreateSMDLBasicInfoPanel(saveAsSMDLField);
			Button saveAsSMDLRadioButton = smdlBasicInfoPanel.getSaveAsSMDLRadio();
			saveAsSMDLRadioButton.setSelection(true);
			Button selectTemplateRadioButton = smdlBasicInfoPanel.getSelectedTemplateRadio();
			selectTemplateRadioButton.setSelection(false);
			smdlBasicInfoPanel.getSelectedTemplateCombo().deselectAll();
			
			Button searchButton = smdlBasicInfoPanel.getSearchSMDLButton();
			smdlBasicInfoPanel.getShell().setDefaultButton(searchButton);
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		// TODO Auto-generated method stub
	}
	private CreateSMDLBasicInfoPanel getCreateSMDLBasicInfoPanel(Text saveAsSMDLField)
	{
		Composite parent = saveAsSMDLField.getParent();
		while(parent!=null && !(parent instanceof CreateSMDLBasicInfoPanel ))
		{
			parent=	parent.getParent();
		}		
		return (CreateSMDLBasicInfoPanel)parent;
	}
}