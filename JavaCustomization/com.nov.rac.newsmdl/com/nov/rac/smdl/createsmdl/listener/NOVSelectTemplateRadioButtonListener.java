package com.nov.rac.smdl.createsmdl.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;

public class NOVSelectTemplateRadioButtonListener implements SelectionListener 
{
	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) 
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button selectTemplateRadioButton= (Button)target;
			CreateSMDLBasicInfoPanel smdlBasicInfoPanel = getCreateSMDLBasicInfoPanel(selectTemplateRadioButton);
			if(smdlBasicInfoPanel != null)
			{
				if(selectTemplateRadioButton.getSelection() == true)
				{
					smdlBasicInfoPanel.getSelectedTemplateCombo().select(smdlBasicInfoPanel.getTemplateComboDefaultSelectedIndex());
					smdlBasicInfoPanel.getSaveAsSMDLText().setText("");
				}
			}
		}
	}
	private CreateSMDLBasicInfoPanel getCreateSMDLBasicInfoPanel(Button radioButton)
	{
		Composite parent = radioButton.getParent();
		while(parent!=null && !(parent instanceof CreateSMDLBasicInfoPanel ))
		{
			parent=	parent.getParent();
		}		
		return (CreateSMDLBasicInfoPanel)parent;
	}
}
