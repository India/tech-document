package com.nov.rac.smdl.createsmdl.listener;

import com.nov.rac.smdl.common.NOVAddCodeButtonListener;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;

public class NOVAddNOVCodeButtonListener extends NOVAddCodeButtonListener
{
	public NOVAddNOVCodeButtonListener(NOVCreateSMDLTable smdlTable)
	{
		super(smdlTable);
	}
}
