package com.nov.rac.smdl.createsmdl.listener;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.createsmdl.CreateSMDLWizard;
import com.nov.rac.smdl.createsmdl.CreateSMDLWizardDialog;

public class NOVCreateSMDLListener implements SelectionListener 
{
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
		
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent arg0) 
	{
		// TODO Auto-generated method stub
		Object target=arg0.getSource();
		if (target instanceof Button)
		{
			Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
			shell.setCursor(new Cursor(shell.getDisplay(),SWT.CURSOR_WAIT));
			Button createSMDLButton= (Button)target;
			Object dpObject=createSMDLButton.getData();
			
			CreateSMDLWizard createSMDLWiz = new CreateSMDLWizard();
			createSMDLWiz.addPropertyChangeListener( m_listeners);
			
			CreateSMDLWizardDialog wizdlg = new CreateSMDLWizardDialog(shell, createSMDLWiz);
			wizdlg.create();
			wizdlg.setSMDLParentObj(dpObject);
			shell.setCursor(new Cursor(shell.getDisplay(),SWT.CURSOR_ARROW));
			wizdlg.open();
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {

		if(!m_listeners.contains(listener) ){
			m_listeners.add(listener);
		}
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		m_listeners.remove(listener);
	}
}
