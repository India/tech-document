package com.nov.rac.smdl.createsmdl.listener;

import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.Set;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVListPopUpDialog;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVSMDLNewEditDialog;
import com.nov.rac.smdl.utilities.NOVGroupDataHelper;
import com.nov.rac.smdl.utilities.NOVUserDataHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;


public class NOVCreateSMDLTableMouseListener extends MouseAdapter 
{
	private NOVCreateSMDLTable m_smdlTable = null;
	private NOVSMDLNewEditDialog    m_newEditDlg;
	private  TCComponent m_dpComp;
	private Shell m_modalShell;
	Registry  reg = Registry.getRegistry("com.nov.rac.smdl.common.common");
	public NOVCreateSMDLTableMouseListener()
	{
	}
	public void mousePressed(MouseEvent e) 
	{			
		m_smdlTable = (NOVCreateSMDLTable) e.getComponent();
		launchDocumentEditDialog(e);
	}
	@Override
	public void mouseReleased(MouseEvent e) 
	{
		m_smdlTable = (NOVCreateSMDLTable) e.getComponent();
		if (e.isPopupTrigger())
		{
			
		
			int iSelectedCol = m_smdlTable.getColumnModel().getColumnIndexAtX(e.getX());
			int iGroupCol = m_smdlTable.getColumnIndex(NOV4ComponentSMDL.GROUP_NAME);
			int iUserCol = m_smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);
			
			if(iSelectedCol == iGroupCol)
			{
				Set<String> groupsSet = NOVGroupDataHelper.getGroupData().keySet();
				String[] sGroupData = (String[]) groupsSet.toArray(new String[groupsSet.size()]);
				String sGroupTitle = reg.getString("groupDialog.TITLE");
				showListPopupDialog(e,iGroupCol,sGroupData,sGroupTitle);
			}
			else if(iSelectedCol == iUserCol)
			{
				String sUserDialogTitle=null;
				//String sGroup = m_smdlTable.getValueAt(m_smdlTable.getSelectedRow(), iGroupCol).toString();
				//Vector<String> sUsers = NOVUserDataHelper.getUsers(sGroup);
				Vector<String> sUsers = NOVUserDataHelper.getUpdateUserList();
				String[] sUserData = sUsers.toArray(new String[sUsers.size()]);
//				if (sGroup!= null && sGroup.trim().length()>0)
//				{
//					sUserDialogTitle = reg.getString("groupUsersDialog.TITLE");
//				}
//				else
//				{
					sUserDialogTitle = reg.getString("allUsersDialog.TITLE");
				//}
				showListPopupDialog(e,iUserCol,sUserData,sUserDialogTitle);
			}
		}
	}
	public void launchDocumentEditDialog(MouseEvent e) 
	{
		int clickCnt = e.getClickCount();
		if ( clickCnt == 2)
		{
			String[] documentEditDialogColumns=reg.getStringArray("documentEditColumns.Name");
			final int iDocCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
			int iSelectedCol = m_smdlTable.getSelectedColumn();
			String columnName =m_smdlTable.getColumnPropertyName(iSelectedCol);
			if (Arrays.asList(documentEditDialogColumns).contains(columnName) ) 
			{
				final Display display = Display.getDefault();
				display.syncExec(new Runnable() 
				{
					public void run() 
					{
						Shell activeShell = display.getActiveShell();
						activeShell.setCursor(new Cursor(display,SWT.CURSOR_WAIT));
						
						m_modalShell = new Shell(activeShell,SWT.EMBEDDED | SWT.APPLICATION_MODAL | SWT.NO_BACKGROUND);
						Frame frame = SWT_AWT.new_Frame(m_modalShell);
						
						int rowIndex = m_smdlTable.getSelectedRow();
						if ( rowIndex >= 0 )
						{
							TCComponent docComp = null;
							Object objDoc = m_smdlTable.getValueAtDataModel(rowIndex, iDocCol);
							if(objDoc.toString().trim().length()>0)
							{
								docComp = (TCComponent)objDoc;
							}
							m_newEditDlg = new NOVSMDLNewEditDialog(frame , true , docComp,m_smdlTable,m_dpComp);
							
							activeShell.setCursor(new Cursor(display,SWT.CURSOR_ARROW));
							
							//To fix SWT and SWING Modality issue
							m_modalShell.setBounds(0, 0, 0, 0);
							m_modalShell.setVisible(true);
						}
					}
				});
				m_newEditDlg.setVisible(true);
				
				display.syncExec(new Runnable() 
				{
					public void run() 
					{
						if(m_modalShell != null && !m_modalShell.isDisposed())
						{
							m_modalShell.dispose();
						}
					}
				});
			}
			else
			{
				return;
			}
		}
	}
	public void setDPObject(TCComponent dpComp)
	{
		m_dpComp = dpComp;
	}
	private void showListPopupDialog(final MouseEvent me, final int iSelectedCol,final String[] sValuesList,final String sDialogTitle)
    {
		Display.getDefault().asyncExec(new Runnable() 
		{
			public void run()
			{
				Shell localShell = Display.getDefault().getActiveShell();
				NOVListPopUpDialog popupDialog = new NOVListPopUpDialog(localShell);
				popupDialog.create();
				popupDialog.setTitle(sDialogTitle);
				popupDialog.populateList(sValuesList);
				
				int x = m_smdlTable.getLocationOnScreen().x + me.getX();
                int y = m_smdlTable.getLocationOnScreen().y + me.getY();
                popupDialog.getShell().setLocation(x, y);
                
				int iReturnCode = popupDialog.open();
				if (iReturnCode == 0)
				{
					int iSelectedRow = m_smdlTable.getSelectedRow();
					String sSelectedValue = popupDialog.getSelectedValue();
					m_smdlTable.setValueAt(sSelectedValue,iSelectedRow, iSelectedCol);
					m_smdlTable.dataModel.fireTableCellUpdated(iSelectedRow, iSelectedCol);
					m_smdlTable.clearSelection();
				}
			}
		});
    }
}