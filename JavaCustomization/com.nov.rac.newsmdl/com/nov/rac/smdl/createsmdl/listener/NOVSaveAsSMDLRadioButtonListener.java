package com.nov.rac.smdl.createsmdl.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;

public class NOVSaveAsSMDLRadioButtonListener implements SelectionListener 
{
	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) 
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button saveAsSMDLRadioButton= (Button)target;
			CreateSMDLBasicInfoPanel smdlBasicInfoPanel = getCreateSMDLBasicInfoPanel(saveAsSMDLRadioButton);
			if(smdlBasicInfoPanel != null)
			{
				if(saveAsSMDLRadioButton.getSelection() == true)
				{
					smdlBasicInfoPanel.getSelectedTemplateCombo().deselectAll();
				}
			}
		}
	}
	private CreateSMDLBasicInfoPanel getCreateSMDLBasicInfoPanel(Button saveAsSMDLRadioButton)
	{
		Composite parent = saveAsSMDLRadioButton.getParent();
		while(parent!=null && !(parent instanceof CreateSMDLBasicInfoPanel ))
		{
			parent=	parent.getParent();
		}		
		return (CreateSMDLBasicInfoPanel)parent;
	}
}
