package com.nov.rac.smdl.createsmdl.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;

public class NOVSelectTemplateComboListener implements SelectionListener 
{
	@Override
	public void widgetDefaultSelected(SelectionEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) 
	{
		Object target=event.getSource();
		if (target instanceof Combo)
		{
			Combo selectTemplateCombo= (Combo)target;
			CreateSMDLBasicInfoPanel smdlBasicInfoPanel = getCreateSMDLBasicInfoPanel(selectTemplateCombo);
			if(smdlBasicInfoPanel != null)
			{
				smdlBasicInfoPanel.getSelectedTemplateRadio().setSelection(true);
				smdlBasicInfoPanel.getSaveAsSMDLRadio().setSelection(false);
				smdlBasicInfoPanel.getSaveAsSMDLText().setText("");
			}
		}
	}
	private CreateSMDLBasicInfoPanel getCreateSMDLBasicInfoPanel(Combo templateCombo )
	{
		Composite parent = templateCombo.getParent();
		while(parent!=null && !(parent instanceof CreateSMDLBasicInfoPanel ))
		{
			parent=	parent.getParent();
		}		
		return (CreateSMDLBasicInfoPanel)parent;
	}
}
