package com.nov.rac.smdl.createsmdl.listener;

import javax.swing.table.TableModel;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableModel;
import com.nov.rac.smdl.createsmdl.editor.NOVDocNoCellEditor;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVRemoveAllDocsButtonListener implements SelectionListener 
{
	private NOVCreateSMDLTable m_smdlTable;
	private Registry m_reg;
	public NOVRemoveAllDocsButtonListener()
	{
		m_reg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) 
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button removeAllDocsButton= (Button)target;
			m_smdlTable=(NOVCreateSMDLTable)removeAllDocsButton.getData();
			removeDocumentsFromTable();
		}
	}	
	private void removeDocumentsFromTable()
	{
		if(m_smdlTable != null)
		{
			TableModel tableModel = m_smdlTable.getModel();
			NOVCreateSMDLTableModel smdlTableModel = null;
    		if(tableModel instanceof NOVCreateSMDLTableModel) //Adding a row from Create SMDL Wizard
    		{
    			smdlTableModel = (NOVCreateSMDLTableModel)tableModel;
    		}
			int iRowCount = m_smdlTable.getRowCount();
			if(iRowCount >0 )
			{
				 Shell shell = Display.getCurrent().getActiveShell();
				 boolean bIsDocExist = checkforDocument();
				 if(bIsDocExist == true)
				 {
					 boolean bResponse = MessageDialog.openConfirm(shell, "Confirm", m_reg.getString("RemoveAllDocsConfirm.MSG"));
					 if(bResponse == true)
					 {
						 int iDocCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
						 int iDocRevCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REV_REF);
						 int iDocStatusCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.SMDL_NAME);
						 int iDocTitleCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.NOV_RELEASE_STATUS);
						 int iDocDescCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.SMDL_DESC);
						 for(int iCount=0;iCount<iRowCount;iCount++)
						 {					
							 m_smdlTable.setValueAt("", iCount, iDocCol);	
							 m_smdlTable.setValueAt("", iCount, iDocRevCol);	
							 m_smdlTable.setValueAt("", iCount, iDocStatusCol);	
							 m_smdlTable.setValueAt("", iCount, iDocTitleCol);	
							 m_smdlTable.setValueAt("", iCount, iDocDescCol);
							 NOVDocNoCellEditor editor = (NOVDocNoCellEditor)m_smdlTable.getCellEditor(iCount, iDocCol);
							 editor.setValue(null);
						 }
						 
						 smdlTableModel.fireTableRowsUpdated(0,iRowCount);
						 
					 }
				 }
				 else
				 {
					 MessageBox.post(shell,m_reg.getString("NoDocuments.MSG"),m_reg.getString("NoDocuments.TITLE"),MessageBox.ERROR);
				 }
			}
		}
	}
	private boolean checkforDocument()
	{
		boolean bIsDocExist = false;
		int iRowCount = m_smdlTable.getRowCount();
		int iDocCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
		for(int iCount=0;iCount<iRowCount;iCount++)
		{
			Object docObj = m_smdlTable.getValueAt(iCount, iDocCol);
			if((docObj != null) && (docObj instanceof TCComponentItem ))
			{
				bIsDocExist = true;
				break;
			}
		}
		return bIsDocExist;
	}
}
