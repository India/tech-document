package com.nov.rac.smdl.createsmdl.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.table.NOVSearchTable;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;

public class NOVSMDLSearchTableMouseListener extends MouseAdapter 
{
	private NOVSearchTable m_smdlSearchTable = null;
	private Text m_SaveAsSMDLField = null;
	public NOVSMDLSearchTableMouseListener(Text saveAsSMDLField)
	{
		m_SaveAsSMDLField = saveAsSMDLField;
	}
	public void mousePressed(MouseEvent event) 
	{
		m_smdlSearchTable = (NOVSearchTable) event.getComponent();
		int clickCnt = event.getClickCount();
		if ( clickCnt == 2)
		{	
			int iSelectdRow  = m_smdlSearchTable.getSelectedRow();
			int iSMDLIdCol = m_smdlSearchTable.getColumnIndexFromDataModel(NOVSMDLConstants.SMDLID_COL_SEARCHTABLE);
			if(iSelectdRow != -1)
			{
				final String sSMDLId = m_smdlSearchTable.getValueAtDataModel( iSelectdRow, iSMDLIdCol );

				Display display = Display.getDefault();
				if(display != null)
				{
					display.syncExec(new Runnable() 
					{			
						@Override
						public void run()
						{
							m_SaveAsSMDLField.setText(sSMDLId);
							disposeSearchResultsDialog();
						}
					});
				}
			}
		}
	}
	public void disposeSearchResultsDialog()
	{
		Display display = Display.getCurrent();
		if(display != null)
		{
			Shell shell = display.getActiveShell();
			if(shell != null)
			{
				shell.close();
			}
		}
	}
}