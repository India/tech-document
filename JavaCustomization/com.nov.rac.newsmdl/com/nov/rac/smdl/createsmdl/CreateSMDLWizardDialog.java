package com.nov.rac.smdl.createsmdl;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class CreateSMDLWizardDialog extends WizardDialog 
{	
	private CreateSMDLBasicInfoPage m_basicInfoPage;
	private CreateSMDLPage m_createSMDLPage;
	private Registry m_appReg = null;
	private TCComponent m_dpObject = null;
	private Rectangle m_basicInfopageBounds;
	private boolean m_bIsReloadData = true;
	private String m_selectedValue = null;
	private String m_SMDLType = null;
	private String m_SMDLDesc = null;
	private TCComponent m_inputComponent = null;
	private CreateSMDLBasicInfoPanel m_basicInfoPanel;
	public CreateSMDLWizardDialog(Shell parentShell, IWizard wizard) 
	{   
		super(parentShell, wizard);
		m_appReg = Registry.getRegistry(this);		
	   	setShellStyle(SWT.DIALOG_TRIM|SWT.RESIZE|SWT.CENTER|SWT.APPLICATION_MODAL);
	}
	@Override
	protected Point getInitialSize() {
		// TODO Auto-generated method stub
		return new Point(convertHorizontalDLUsToPixels(400), convertVerticalDLUsToPixels(280));
	}
	@Override
	protected void nextPressed() 
	{   
		m_basicInfoPage = getBasicInfoPage();
		m_basicInfoPanel = m_basicInfoPage.getPanel();
		String invalidInputs=m_basicInfoPage.validateMandatoryFields();
		Shell basicInfoPageShell = getCurrentPage().getWizard().getContainer().getShell();
		m_basicInfopageBounds = basicInfoPageShell.getBounds();
		if(invalidInputs.length()==0)
		{
			try
			{
				m_inputComponent = getValidInputComponent();
				//TCDECREL-1551 Validate invalid entries in smdl doc responsible for smdl creation
				if( !m_basicInfoPage.getValidInputComponentDocResponsible() )
					throw new Exception(m_appReg.getString("InvalidDocResponsible.MSG"));
				super.nextPressed();	
				Shell shell = this.getShell();
				centerToScreen(shell);

				m_createSMDLPage = getCreateSMDLPage();
				m_createSMDLPage.setDPObject(m_dpObject);
				if(m_bIsReloadData == false)
				{
					if(!(m_selectedValue.equalsIgnoreCase(m_basicInfoPage.getSelectedTemp())) && 
							!(m_selectedValue.equalsIgnoreCase(m_basicInfoPage.getSaveAsSMDLId())))
					{
						populateCreateSMDLWizardTablePanel();
					}

					String smdlType = m_basicInfoPage.getSMDLType();
					String smdlDesc = m_basicInfoPage.getSMDLDesc();
					if(!(m_SMDLType.equalsIgnoreCase(smdlType)) || !(m_SMDLDesc.equalsIgnoreCase(smdlDesc)))
					{
						m_createSMDLPage.populateCreateSMDLWizardTopPanel(smdlType,smdlDesc);
					}
				}
				else
				{
					m_createSMDLPage.populateCreateSMDLWizardTopPanel(m_basicInfoPage.getSMDLType(),m_basicInfoPage.getSMDLDesc());
					populateCreateSMDLWizardTablePanel();
				}
			}
			catch(Exception ex)
			{
				MessageBox.post(basicInfoPageShell,ex.getMessage(),m_appReg.getString("InvalidInput.TITLE"),MessageBox.ERROR);
			}
		}
		else
		{
			MessageBox.post(basicInfoPageShell,m_appReg.getString("FillMandatoryFields.MSG")+"\n"+invalidInputs,"Error" ,MessageBox.ERROR);
		}
	}
	protected void backPressed() 
	{
		super.backPressed();		
		Shell shell = getCurrentPage().getWizard().getContainer().getShell();
	    shell.setBounds(m_basicInfopageBounds);
	    CreateSMDLBasicInfoPage basicInfoPage = getBasicInfoPage();
	    if(basicInfoPage.getSelectedTemp() != null)
	    {
	    	m_selectedValue = basicInfoPage.getSelectedTemp();
	    }
	    else
	    {
	    	m_selectedValue = basicInfoPage.getSaveAsSMDLId();
	    }
	    m_SMDLType = basicInfoPage.getSMDLType();
		m_SMDLDesc = basicInfoPage.getSMDLDesc();
	    
	    m_bIsReloadData = false;
	}
	@Override
	protected void finishPressed() 
	{
		// TODO Auto-generated method stub
		if(m_createSMDLPage != null)
		{
			String invalidInputs=m_createSMDLPage.validateMandatoryFields();
			if(invalidInputs.length()==0)
			{
				m_createSMDLPage.createSMDLOperation();	
				m_createSMDLPage.populateDocResponsible(m_basicInfoPanel.getDocumentResponsibleUser());
				super.finishPressed();
			}
			else
			{
				Shell shell = getCurrentPage().getWizard().getContainer().getShell();
				MessageBox.post(shell,m_appReg.getString("FillMandatoryFields.MSG")+"\n"+invalidInputs,"Error" ,MessageBox.ERROR);
			}
		}
	}
	@Override
	protected void cancelPressed() 
	{
		// TODO Auto-generated method stub		 
		super.cancelPressed();
	}
	private void centerToScreen(Shell shell)
	{
		Rectangle bounds = getPrimaryMonitorBounds(shell);
	    
	    Rectangle shellBounds = getShellBounds(shell);   
	    
	    bounds.height = bounds.height-200;
	    int x = 5;
	    int y = bounds.y + Math.max(0, (bounds.height - shellBounds.height) / 2);	    
	    
		shell.setBounds(x,y,bounds.width-x,bounds.height);
	}
	private Rectangle getPrimaryMonitorBounds(Shell shell)
	{
		Monitor primary = shell.getDisplay().getPrimaryMonitor();
	    Rectangle bounds = primary.getBounds();
		
		return bounds;
	}
	private Rectangle getShellBounds(Shell shell)
	{
		Shell shell1 = getCurrentPage().getWizard().getContainer().getShell();
		Rectangle bounds = shell1.getBounds();
	    
		return bounds;
	}
	private void populateCreateSMDLWizardTablePanel()
	{
		if(m_basicInfoPage.bIsSelectTemplateButtonSelected())
		{
			m_createSMDLPage.populateDocResponsible(m_basicInfoPanel.getDocumentResponsibleUser());
			m_createSMDLPage.populateSMDLTableWithTemplateInfo(m_inputComponent);
		}
		else if(m_basicInfoPage.bIsSaveAsSMDLButtonSelected())
		{
			m_createSMDLPage.populateDocResponsible(m_basicInfoPanel.getDocumentResponsibleUser());
			m_createSMDLPage.populateSMDLTableWithSMDLRevInfo((TCComponentItemRevision)m_inputComponent);
		}
	}
	public CreateSMDLBasicInfoPage getBasicInfoPage()
	{
		IWizard wizard = getCurrentPage().getWizard();
		CreateSMDLBasicInfoPage m_basicInfoPage = (CreateSMDLBasicInfoPage) wizard.getPage(CreateSMDLBasicInfoPage.PAGE_NAME);
		return m_basicInfoPage;
	}
	public CreateSMDLPage getCreateSMDLPage()
	{
		IWizard wizard = getCurrentPage().getWizard();
		CreateSMDLPage m_createSMDLPage = (CreateSMDLPage) wizard.getPage(CreateSMDLPage.PAGE_NAME);
		return m_createSMDLPage;
	}
	public void setSMDLParentObj(Object dpObj)
	{
		m_dpObject = (TCComponent)dpObj;
	}
	protected void createButtonsForButtonBar(Composite parent) 
	{
		super.createButtonsForButtonBar(parent);
		Button backButton = getButton(IDialogConstants.BACK_ID);
		backButton.setVisible(false);	
	}
	private TCComponent getValidInputComponent() throws Exception
	{
		TCComponent inputComponent = null;
		if(m_basicInfoPage.bIsSelectTemplateButtonSelected())
		{
			inputComponent = m_basicInfoPage.getTemplateComp();
			if(inputComponent == null)
			{
				throw new Exception(m_appReg.getString("InvalidTemplate.MSG"));
			}
		}
		else if(m_basicInfoPage.bIsSaveAsSMDLButtonSelected())
		{
			inputComponent = m_basicInfoPage.getSaveAsSMDLRev();	
			if(inputComponent == null)
			{
				throw new Exception(m_appReg.getString("InvalidSaveAsSMDL.MSG"));
			}
		}
		return inputComponent;
	}
	
	
}