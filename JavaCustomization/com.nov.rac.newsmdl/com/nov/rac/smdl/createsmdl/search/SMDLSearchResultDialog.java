package com.nov.rac.smdl.createsmdl.search;

import javax.swing.ListSelectionModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.table.NOVSearchTable;
import com.nov.rac.smdl.createsmdl.listener.NOVSMDLSearchTableMouseListener;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.SearchResultComposite;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class SMDLSearchResultDialog extends AbstractSWTDialog 
{    
	private NovSearchDataModel m_novSearchDataModel;
	private SearchResultComposite m_searchResultComposite;
	private Text m_SaveAsSMDLText; 
	private Registry m_appReg = null;
	public SMDLSearchResultDialog ( Shell parent) 
	{ 
		super(parent);
		setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
	} 
	@Override
	protected Control createDialogArea(Composite parent) 
	{
		setTitle();
		Composite composite = ( Composite )super.createDialogArea(parent);
		composite.setLayout( new FillLayout() );
		m_searchResultComposite = new SearchResultComposite( composite, SWT.NONE );		
		m_searchResultComposite.setNovSearchDataModel(m_novSearchDataModel);
		m_searchResultComposite.getSearchTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		m_searchResultComposite.addSearchTableMouselistener(new NOVSMDLSearchTableMouseListener(m_SaveAsSMDLText));
		
		/*if(m_novSearchDataModel.getResultSet().getRows() != 0)
		{
			m_searchResultComposite.addRowsToTable(m_novSearchDataModel.getResultSet(),
					                               m_novSearchDataModel.getResponseIndex(),
					                               m_novSearchDataModel.getPageSize(), 
					                               -2);
		}*/	
		
		m_novSearchDataModel.notifyObserver();	
		
		return composite;
	}
	@Override
	protected Point getInitialSize() {
		
		Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;

        int width = ((size.width )/2) + (rectwidth / 4);
        int height = ((size.height ) / 2);
        
		return new Point(width, height);
	}
	public void setNovSearchDataModel(NovSearchDataModel novSearchDataModel)
	{
		m_novSearchDataModel = novSearchDataModel;
	}
	public void setSaveAsSMDLField(Text saveAsSMDLField)
	{
		m_SaveAsSMDLText = saveAsSMDLField;
	}
	private void setTitle() 
	{
		String title = 	m_appReg.getString("SMDLSearchResultsDialog.TITLE");		
		this.getShell().setText(title);		
	}
	@Override
	protected void okPressed() 
	{	
		NOVSearchTable smdlSearchTable = m_searchResultComposite.getSearchTable();
		int iSelectdRow  = smdlSearchTable.getSelectedRow();
		int iSMDLIdCol = smdlSearchTable.getColumnIndexFromDataModel(NOVSMDLConstants.SMDLID_COL_SEARCHTABLE);
		if(iSelectdRow != -1)
		{
			String sSMDLId = smdlSearchTable.getValueAtDataModel( iSelectdRow, iSMDLIdCol );
			m_SaveAsSMDLText.setText(sSMDLId);
		}
		super.okPressed();
	}
	@Override
	public boolean close() 
	{
		m_novSearchDataModel.removeObserver(m_searchResultComposite);
		return super.close();
	}
}
