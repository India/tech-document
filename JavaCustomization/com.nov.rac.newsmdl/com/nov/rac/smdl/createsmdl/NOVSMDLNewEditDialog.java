package com.nov.rac.smdl.createsmdl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeSelectionModel;

import com.noi.rac.commands.newitem.NewItemDialog;
import com.nov.rac.commands.newitem.NOVNewItemDialog;
import com.nov.rac.commands.newitem.NOVNewItemPanel;
import com.nov.rac.commands.newitem.NOVProgressBar;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVCodeComboAutoComplete;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.common.NOVSMDLTableLine;
import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.nov.rac.smdl.common.NOVTextFieldLimit;
import com.nov.rac.smdl.common.editors.DataProvider;
import com.nov.rac.smdl.common.editors.MyNOVDocument;
import com.nov.rac.smdl.common.editors.NOVSMDLAutoFillTextField;
import com.nov.rac.smdl.common.editors.NOVSMDLAutofilEditorl;
import com.nov.rac.smdl.createsmdl.editor.NOVDocNoCellEditor;
import com.nov.rac.smdl.panes.addnewcode.NOVSearchPanel;
import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVGroupDataHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.utilities.NOVSMDLTableHelper;
import com.nov.rac.smdl.utilities.NOVUserDataHelper;
import com.nov.rac.utilities.utils.MessageBox;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.common.TCTreeOpenEvent;
import com.teamcenter.rac.common.TCTreeOpenListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinition;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinitionType;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentUser;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ButtonLayout;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextField;
import com.teamcenter.rac.util.iToggleButton;


public class NOVSMDLNewEditDialog extends AbstractAIFDialog implements ActionListener
{
    private static final long serialVersionUID = 1L;
    boolean neglectOKListner = false;
    String  sDocCode         = null;
    private NOVSMDLTable m_smdlTable;

    int                              rowIndexToSelect       = -1;
    private TCComponent              m_docComponent         = null;
    public boolean                   updateExistingRow      = false;
    public boolean                   homeClicked            = false;
    public JButton                   applyButton            = new JButton();
    public JButton                   okButton               = new JButton();
    public JButton                   cancelButton           = new JButton();
    private iToggleButton            homeButton             = null;
    private JButton                  searchButton           = new JButton();
    private JButton                  newButton              = new JButton();
    private JButton                  saveAsButton           = new JButton();
    private JLabel                   lDocCode               = new JLabel();
    private NOVCodeComboAutoComplete	 tDocCode;
    private JLabel                   lPlannedDateValAdd     = new JLabel();
    private JLabel                   lNameDoc               = new JLabel();
    public JTextField                tNameDoc               = new JTextField();
    private JLabel                   lGpNameDoc             = new JLabel();
    public NOVSMDLAutofilEditorl     cGroupName;                                 
    public JButton                   btnGroup               = new JButton();
    private JLabel                   lContractDate          = new JLabel();
    public iTextField                tContractDate          = new iTextField();
    private JLabel                   lPlannedDate           = new JLabel();
    public iTextField                tPlannedDate           = new iTextField();
    private JLabel                   lResponsible           = new JLabel();
    //public NOVSMDLAutofilEditorl     cResponsible;  
    public NOVSMDLAutoFillTextField     cResponsible; 
    public JButton                   btnResponsible         = new JButton();
    private JLabel                   lComment               = new JLabel();
    private JLabel                   lCustomerCode          = new JLabel();
    public iTextField                tCustomerCode          = new iTextField();
    private JLabel                   lAdditionalCode        = new JLabel(); 
    public iTextField                tAdditionalCode        = new iTextField();
    public JTextArea                 tComment               = new JTextArea();
    private JLabel                   labelDoc               = new JLabel();
    private JPanel                   leftAttributePanel     = new JPanel();
    private JPanel                   leftTopAttrPanel       = new JPanel();
    public JPanel                    basePanel              = new JPanel();
    public JPanel                    buttonPanel            = new JPanel();
    private JPanel                   logoPanel              = new JPanel();
    private JPanel                   rightPanel             = new JPanel();
    private JPanel                   searchHomePanel        = new JPanel();
    private JPanel                   attrPanel              = new JPanel();
    public JScrollPane              treeScrollPane          = null;
    private TCSession              session;
    public  TCTree                 navigatorTree;
    protected Frame                  parent                 = null;
    public com.teamcenter.rac.aif.AIFDesktop desktop        = null;
    private com.teamcenter.rac.util.Registry             appReg;
    private GridBagLayout            gridBagLayout          = new GridBagLayout();
    private GridBagLayout            buttonGridBagLayout    = new GridBagLayout();
    private GridBagLayout            attributeGridBagLayout = new GridBagLayout();
    private GridBagLayout            leftTopAttrPanelGrid   = new GridBagLayout();
    private GridBagLayout            rightTopButtonGrid     = new GridBagLayout();
    private com.teamcenter.rac.util.DateButton               dateContractButton;
    private com.teamcenter.rac.util.DateButton               datePlannedButton;
    public TCComponent             rowComp                  = null;
    protected NOVSearchPanel         searchPanel            = null;
    private JDialog                  groupPopup             = null;
    private JDialog                  userPopup              = null;
    private JList                    jlist                  = null;
    private Vector<String>           groupList              = new Vector<String>();
    public HashMap<String,String>    novCodeIssueDate       = new HashMap<String,String>();
    public HashMap<String,Object>    novCodeDesc            = new HashMap<String,Object>();
    public File []imanFileObj;
    private TCComponent  m_dpComponent = null;
    private JButton      m_docInfoClearBtn                  = new JButton(); 
    private JLabel                   lLineItem              = new JLabel(); 
    public iTextField                tLineItem              = new iTextField();
    private JLabel                   lTab                   = new JLabel(); 
    public iTextField                tTab                   = new iTextField();
    private Vector<String> m_userList                       = new Vector<String>();
    private boolean bIs_MousePressed = false;
	private Vector<String> data;
	private DataProvider dataProvider;
	
    public NOVSMDLNewEditDialog(Frame parent, boolean updateExisting, TCComponent argRowComp, 
    		                    NOVSMDLTable smdlTable, TCComponent dpComp)
    {
        super(parent , true);
        tTab.setDocument(new NOVTextFieldLimit(1, true));
        session = (TCSession)AIFUtility.getDefaultSession();
        this.parent = parent;
        if ( parent instanceof AIFDesktop )
        {
            desktop = (AIFDesktop) parent;
        }
        updateExistingRow = updateExisting;
        rowComp = argRowComp;
        m_smdlTable = smdlTable;
        m_dpComponent = dpComp;
        
        createNewEditDialog();
        //loadHomeNodeTree();
        //navigatorTree.refresh();
        //treeScrollPane.revalidate();
        //treeScrollPane.repaint();
    }
    private void createNewEditDialog()
    {
        try
        {      
        	appReg = Registry.getRegistry("com.nov.rac.smdl.panes.addnewcode.addnewcode");
            setTitle(appReg.getString("newEditDialog.NAME"));
            
            createLogoPanel();
            setLabels();          
                                
            navigatorTree();
            
            homeButtonUIOperation();
                        
            createButtonPanel();
            
            contractAndPlannedDateButtons();
                         
            setButtonLabels();
                        
            createTopLeftAttrPanel();
            
            createLeftAttributePanel();
            
            createAttributePanel();
            
            createSearchPanel();
            
            createRightPanel();
            
            treeScrollPane.setVisible(true);
            
            createBasePanel();
            
            lookAndFeel();
            
            setEditability();
            registerListeners();
            
            this.getContentPane().setLayout(new BorderLayout());
            this.getContentPane().add(basePanel, BorderLayout.CENTER);
            this.pack();
            this.centerToScreen(1.0, 1.0);
            this.setFocusable(true);
            //this.setVisible(true);
           // setDefaultValues();
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
    private void setLabels()
    {
    	lDocCode.setText(appReg.getString("codeLabel.NAME"));
        lPlannedDateValAdd.setText(appReg.getString("planneddateLabel.NAME"));
        lNameDoc.setText(appReg.getString("namedocumentLabel.NAME"));
        lGpNameDoc.setText(appReg.getString("groupnameLabel.NAME"));
        lCustomerCode.setText(appReg.getString("customerCode.NAME"));
        lContractDate.setText(appReg.getString("contractdateLabel.NAME"));
        lPlannedDate.setText(appReg.getString("planneddateLabel.NAME"));
        lResponsible.setText(appReg.getString("responsibleLabel.NAME"));
        lComment.setText(appReg.getString("commentsLabel.NAME"));
        labelDoc.setText(appReg.getString("documentLabel.NAME"));
        lAdditionalCode.setText(appReg.getString("additionalCode.NAME"));
        lLineItem.setText(appReg.getString("lineItem.NAME"));
        lTab.setText(appReg.getString("tab.NAME"));
        
        lDocCode.setPreferredSize(new Dimension(90 , 21));
        lPlannedDateValAdd.setPreferredSize(new Dimension(90 , 21));
        lNameDoc.setPreferredSize(new Dimension(90 , 21));
        lGpNameDoc.setPreferredSize(new Dimension(90 , 21));
        lPlannedDate.setPreferredSize(new Dimension(90 , 21));
        lContractDate.setPreferredSize(new Dimension(90 , 21));
        lResponsible.setPreferredSize(new Dimension(90 , 21));
        lComment.setPreferredSize(new Dimension(90 , 21));
        lLineItem.setPreferredSize(new Dimension(90 , 21));
        lTab.setPreferredSize(new Dimension(90 , 21));
    }
    private void createButtonPanel()
    {
    	buttonPanel.setMinimumSize(new Dimension(250 , 35));
        buttonPanel.setPreferredSize(new Dimension(250 , 35));
        buttonPanel.setLayout(buttonGridBagLayout);
                
        buttonPanel.add(okButton, new GridBagConstraints(0 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        		  GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        		  new Insets(0 , 5 , 0 , 5) , 0 , 0));
        buttonPanel.add(applyButton,new GridBagConstraints(1 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        			GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        			new Insets(0 , 5 , 0 , 5) , 0 , 0));
        buttonPanel.add(cancelButton,new GridBagConstraints(2 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        			 GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        			 new Insets(0 , 5 , 0 , 5) , 0 , 0));
    }
    private void createLogoPanel()
    {
    	ImageIcon icon = appReg.getImageIcon("newEditDialog.ICON");
        JLabel newItemIcon = new JLabel();
        newItemIcon.setIcon(icon);
        JPanel iconPanel = new JPanel(new ButtonLayout(ButtonLayout.VERTICAL , ButtonLayout.TOP ,ButtonLayout.DEFAULTGAP));
        iconPanel.add(newItemIcon);
        newItemIcon.setMaximumSize(new Dimension(2147483647 , 2147483647));
        newItemIcon.setMinimumSize(new Dimension(450 , 80));
        newItemIcon.setPreferredSize(new Dimension(450 , 80));
    	logoPanel.add(newItemIcon, new GridBagConstraints(0 , 0 , 1 , 2 , 0.0 , 0.0 ,
                        		   GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        		   new Insets(0 , 0 , 0 , 0) , 0 , 0));
         logoPanel.setMinimumSize(new Dimension(650 , 90));
         logoPanel.setPreferredSize(new Dimension(650 , 90));
    }
    private void clearDocInfoButton()
    {
    	ImageIcon clearDocInfoImage = null;
    	String sClearDocBtnToolTip = null;
        try
        {
        	clearDocInfoImage = appReg.getImageIcon("ClearDocInfo.ICON");
        	sClearDocBtnToolTip = appReg.getString("ClearDocInfoButton.TOOLTIP");
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    	m_docInfoClearBtn.setIcon(clearDocInfoImage);
    	m_docInfoClearBtn.setToolTipText(sClearDocBtnToolTip);
    	m_docInfoClearBtn.setMinimumSize(new Dimension(20 , 22));
    	m_docInfoClearBtn.setPreferredSize(new Dimension(20 , 22));
    	m_docInfoClearBtn.addActionListener(this);
    }
    private void createLeftAttributePanel()
    {
    	groupButton();        
        resPersonButton();
        clearDocInfoButton();
        if ( updateExistingRow && m_smdlTable != null)
        {
            loadPropValueForWizard();
        }
        else
        {
        	
        	tDocCode.setSelectedIndex(-1);
        }
        
    	leftAttributePanel.setMinimumSize(new Dimension(400 , 340));
    	leftAttributePanel.setPreferredSize(new Dimension(400 , 340));
    	leftAttributePanel.setLayout(attributeGridBagLayout);
    	leftAttributePanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
         
    	leftAttributePanel.add(lNameDoc, new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        				 GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        				 new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tNameDoc, new GridBagConstraints(1 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        				 GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        				 new Insets(2 , 2 , 2 , 5) , 0 , 0));
        
        leftAttributePanel.add(m_docInfoClearBtn, new GridBagConstraints(2 , 1 , 1 , 1 , 0.0 , 0.0 ,
				 								  GridBagConstraints.CENTER,GridBagConstraints.NONE ,
				 								  new Insets(0 , 0 , 0 , 0) , 0 , 0));
        
        JPanel optButnPanel = new JPanel();                
        optButnPanel.add(newButton);
        optButnPanel.add(saveAsButton);
        
        leftAttributePanel.add(optButnPanel, new GridBagConstraints(1 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        					 GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        					 new Insets(0 , 0 , 0 , 0) , 0 , 0));
                        
        leftAttributePanel.add(lCustomerCode, new GridBagConstraints(0 , 2 , 1 , 1 , 0.0 , 0.0 ,
                        					  GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        					  new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tCustomerCode, new GridBagConstraints(1 , 2 , 1 , 1 , 0.0 , 0.0 ,
                        					  GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        					  new Insets(2 , 2 , 2 , 5) , 0 , 0));
    
        leftAttributePanel.add(lAdditionalCode,new GridBagConstraints(0 , 3 , 1 , 1 , 0.0 , 0.0 ,
                        					   GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        					   new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tAdditionalCode,new GridBagConstraints(1 , 3 , 1 , 1 , 0.0 , 0.0 ,
                        					   GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        					   new Insets(2 , 2 , 2 , 5) , 0 , 0));
        leftAttributePanel.add(lGpNameDoc,new GridBagConstraints(0 , 4 , 1 , 1 , 0.0 , 0.0 ,
                        				  GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        				  new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(cGroupName,new GridBagConstraints(1 , 4 , 1 , 1 , 0.0 , 0.0 ,
                        				  GridBagConstraints.EAST , GridBagConstraints.HORIZONTAL ,
                        				  new Insets(2 , 2 , 2 , 5) , 0 , 0));
        leftAttributePanel.add(btnGroup, new GridBagConstraints(2 , 4 , 1 , 1 , 0.0 , 0.0 ,
                        				 GridBagConstraints.CENTER,GridBagConstraints.NONE ,
                        				 new Insets(0 , 0 , 0 , 0) , 0 , 0));
        leftAttributePanel.add(lContractDate, new GridBagConstraints(0 , 5, 1 , 1 , 0.0 , 0.0 ,
                        					  GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        					  new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tContractDate, new GridBagConstraints(1 , 5 , 1 , 1 , 0.0 , 0.0 ,
                        					  GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        					  new Insets(2 , 2 , 2 , 5) , 0 , 0));
        leftAttributePanel.add(dateContractButton, new GridBagConstraints(2 , 5 , 1 , 1 , 0.0 , 0.0 ,
                        						   GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        						   new Insets(0 , 0 , 0 , 0) , 0 , 0));
        leftAttributePanel.add(lPlannedDate, new GridBagConstraints(0 , 6 , 1 , 1 , 0.0 , 0.0 ,
                        					 GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        					 new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tPlannedDate, new GridBagConstraints(1 , 6 , 1 , 1 , 0.0 , 0.0 ,
                        					 GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        					 new Insets(2 , 2 , 2 , 5) , 0 , 0));
        leftAttributePanel.add(datePlannedButton, new GridBagConstraints(2 , 6 , 1 , 1 , 0.0 , 0.0 ,
                        						  GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        						  new Insets(0 , 0 , 0 , 0) , 0 , 0));
        leftAttributePanel.add(lResponsible, new GridBagConstraints(0 , 7 , 1 , 1 , 0.0 , 0.0 ,
                        					 GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        					 new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(cResponsible,new GridBagConstraints(1 , 7 , 1 , 1 , 0.0 , 0.0 ,
                        					GridBagConstraints.EAST , GridBagConstraints.HORIZONTAL ,
                        					new Insets(2 , 2 , 2 , 5) , 0 , 0));
        leftAttributePanel.add(btnResponsible, new GridBagConstraints(2 , 7 , 1 , 1 , 0.0 , 0.0 ,
                        					   GridBagConstraints.CENTER , GridBagConstraints.NONE ,
                        					   new Insets(0 , 0 , 0 , 0) , 0 , 0));
        leftAttributePanel.add(lComment, new GridBagConstraints(0 , 8 , 1 , 1 , 0.0 , 0.0 ,
                        				 GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        				 new Insets(0 , 5 , 0 , 5) , 0 , 0));
        
        JScrollPane areaScrollPane = null;
        areaScrollPane = new JScrollPane(tComment);
        areaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        areaScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        areaScrollPane.setMinimumSize(new Dimension(210 , 70));
        areaScrollPane.setPreferredSize(new Dimension(210 , 70));
                        
        leftAttributePanel.add(areaScrollPane,new GridBagConstraints(1 , 8 , 1 , 1 , 0.0 , 0.0 ,
                        					  GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        					  new Insets(2 , 2 , 2 , 5) , 0 , 0));
        
        leftAttributePanel.add(lLineItem,new GridBagConstraints(0 , 9 , 1 , 1 , 0.0 , 0.0 ,
        								GridBagConstraints.WEST , GridBagConstraints.NONE ,
        								new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tLineItem,new GridBagConstraints(1 , 9 , 1 , 1 , 0.0 , 0.0 ,
        								GridBagConstraints.EAST , GridBagConstraints.NONE ,
        								new Insets(2 , 2 , 2 , 5) , 0 , 0));
        leftAttributePanel.add(lTab,new GridBagConstraints(0 , 10 , 1 , 1 , 0.0 , 0.0 ,
        							GridBagConstraints.WEST , GridBagConstraints.NONE ,
        							new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftAttributePanel.add(tTab,new GridBagConstraints(1 , 10 , 1 , 1 , 0.0 , 0.0 ,
        							GridBagConstraints.EAST , GridBagConstraints.NONE ,
        							new Insets(2 , 2 , 2 , 5) , 0 , 0));
    }
    private void navigatorTree()
    {
    	navigatorTree = new TCTree(true , true);
        navigatorTree.addTreeOpenListener(new TCTreeOpenListener() {
            public void openTreeNode(TCTreeOpenEvent imantreeopenevent )
            {
                TCComponent imancomponent = imantreeopenevent.getComponentToOpen();
                try
                {
                	Object[] args = new Object[2];
        			args[0] = AIFUtility.getActiveDesktop();
        			args[1] = (InterfaceAIFComponent)imancomponent;
        			AbstractAIFCommand openCmd = session.getOpenCommand(args);
        			openCmd.executeModeless();
                }
                catch ( Exception exception )
                {
                    JOptionPane.showMessageDialog(NOVSMDLNewEditDialog.this, exception, "",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        TreeSelectionModel tsm = navigatorTree.getSelectionModel();
        tsm.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        String as[] = appReg.getStringArray("navigator.OPEN", ",");
        if ( as != null )
        {
            for ( int i = 0; i < as.length; i++ )
                navigatorTree.addOpenRule(as[i]);
        }
        
        //navigatorTree.addTreeSelectionListener(this);
        treeScrollPane = new JScrollPane(navigatorTree);
        treeScrollPane.setPreferredSize(new Dimension(290 , 370));
        treeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        treeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
    }
    private void homeButtonUIOperation()
    {
    	 homeButton = new iToggleButton(appReg.getString("homeToggleButton.NAME") , 
    			 						appReg.getImageIcon("homeButton.ICON"));
         homeButton.setMinimumSize(new Dimension(190 , 20));
         homeButton.setPreferredSize(new Dimension(190 , 20));
         // Home Button is Default selection
         homeButton.setSelected(true);
         if ( homeButton.isSelected() )
         {
             treeScrollPane.setVisible(true);
             if ( treeScrollPane.isVisible() )
             {
                 homeClicked = false;
                                  
                 loadHomeNodeTree();
                 
                /* treeScrollPane.revalidate();
                 treeScrollPane.repaint();*/
                 treeScrollPane.setVisible(true);
                 //navigatorTree.addTreeSelectionListener(NOVSMDLNewEditDialog.this);
                 //navigatorTree.revalidate();
                 //basePanel.validate();
                 basePanel.repaint();
                 /*treeScrollPane.revalidate();
                 treeScrollPane.repaint();*/
                 //rightPanel.revalidate();
                 rightPanel.repaint();
             }
         }
         homeButton.addActionListener(this);
    }
    private void homeButtonAction()
    {
    	if ( !treeScrollPane.isVisible() )
        {
            homeClicked = false;
            treeScrollPane.setVisible(true);
            loadHomeNodeTree();
            treeScrollPane.revalidate();
            treeScrollPane.setVisible(true);
            //navigatorTree.addTreeSelectionListener(NOVSMDLNewEditDialog.this);
            if ( searchPanel != null )
            {
                searchPanel.setVisible(false);
                basePanel.setMinimumSize(new Dimension(780 , 590));
                basePanel.setPreferredSize(new Dimension(780 , 590));
                basePanel.validate();
                basePanel.repaint();
                attrPanel.setMinimumSize(new Dimension(450 , 440));
                attrPanel.setPreferredSize(new Dimension(450 , 440));
                leftAttributePanel.setMinimumSize(new Dimension(400 , 340));
                leftAttributePanel.setPreferredSize(new Dimension(400 , 340));
                leftAttributePanel.validate();
                leftAttributePanel.repaint();
                leftTopAttrPanel.setMinimumSize(new Dimension(400 ,80));
                leftTopAttrPanel.setPreferredSize(new Dimension( 400 , 80));
                rightPanel.setMinimumSize(new Dimension(350 , 440));
                rightPanel.setPreferredSize(new Dimension(350 , 440));
                rightPanel.validate();
                rightPanel.repaint();
                rightPanel.add(treeScrollPane, new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 ,0.0 ,
                                			   GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                                			   new Insets(0 , 5 , 0 , 5) , 0 , 0));
                rightPanel.validate();
                rightPanel.repaint();
                NOVSMDLNewEditDialog.this.setSize(new Dimension(700,540));
                NOVSMDLNewEditDialog.this.centerToScreen(1.0, 1.0);
            }
            treeScrollPane.revalidate();
            treeScrollPane.repaint();
            rightPanel.repaint();
            rightPanel.updateUI();
        }
        else
        {
            homeClicked = true;
            //navigatorTree.removeTreeSelectionListener(NOVSMDLNewEditDialog.this);
            treeScrollPane.setVisible(false);
            rightPanel.repaint();
            rightPanel.updateUI();
        }
        NOVSMDLNewEditDialog.this.setVisible(true);
    }
    private void searchButtonAction()
    {
    	if ( null == searchPanel )
    	{
    		boolean tabbedPaneRequired = false;
    		searchPanel = new NOVSearchPanel(session , tabbedPaneRequired);
    		if ( treeScrollPane.isVisible() ) homeButton.doClick();
    	}
    	else
    	{
    		searchPanel = searchPanel.getSearchPanel();
    		if ( !searchPanel.isVisible() )
    			searchPanel.setVisible(true);
    		if ( treeScrollPane.isVisible() ) 
    			homeButton.doClick();
    	}
    	// --------------------------------------------------------
    	attrPanel.setMinimumSize(new Dimension(450 , 550));
    	attrPanel.setPreferredSize(new Dimension(450 , 550));
    	leftTopAttrPanel.setMinimumSize(new Dimension(400 , 130));
    	leftTopAttrPanel.setPreferredSize(new Dimension(400 , 130));
    	leftAttributePanel.setMinimumSize(new Dimension(400 , 400));
    	leftAttributePanel.setPreferredSize(new Dimension(400 , 400));
    	leftAttributePanel.validate();
    	leftAttributePanel.repaint();
    	// --------------------------------------------------------
    	rightPanel.setMinimumSize(new Dimension(500 , 550));
    	rightPanel.setPreferredSize(new Dimension(500 , 550));
    	searchPanel.setPreferredSize(new Dimension(480 , 545));
    	rightPanel.add(searchPanel, new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 , 0.0 ,
    			GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
    			new Insets(0 ,0 , 0 , 0) , 0 , 0));
    	logoPanel.setMinimumSize(new Dimension(650 , 80));
    	logoPanel.setPreferredSize(new Dimension(650 , 80));
    	basePanel.setMinimumSize(new Dimension(1000 , 700));
    	basePanel.setPreferredSize(new Dimension(1000 , 700));
    	NOVSMDLNewEditDialog.this.setSize(new Dimension(1000 , 860));
    	NOVSMDLNewEditDialog.this.centerToScreen(1.0, 1.0);
    	searchPanel.revalidate();
    	searchPanel.repaint();
    	rightPanel.validate();
    	rightPanel.repaint();
    	basePanel.validate();
    	basePanel.repaint();
    	NOVSMDLNewEditDialog.this.setVisible(true);
    }
    private void populateNovCodeCombo()
    {
    	Vector<String> vCodeInfo = new Vector<String>();
    	Collections.addAll(vCodeInfo,NOVCodeHelper.getCodeList());
    	novCodeIssueDate.putAll(NOVCodeHelper.getCodeContractIssueDate());
    	novCodeDesc.putAll(NOVCodeHelper.getCodesDescription());
    	
    	tDocCode = new NOVCodeComboAutoComplete(vCodeInfo);
    	
    	tDocCode.addActionListener(new ActionListener() 
    	{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				String sCode = (String) tDocCode.getSelectedItem();
				if(sCode != null)
				{
					populateDateInfoForCode(sCode);
				}
			}
		});
    }
    private void setButtonLabels()
    {
    	cancelButton.setHorizontalTextPosition(SwingConstants.CENTER);
        cancelButton.setText(appReg.getString("cancelButton.NAME"));
        okButton.setHorizontalTextPosition(SwingConstants.CENTER);
        okButton.setText(appReg.getString("okButton.NAME"));
        applyButton.setHorizontalTextPosition(SwingConstants.CENTER);
        applyButton.setText(appReg.getString("applyButton.NAME")); 
        searchButton.setIcon(appReg.getImageIcon("searchButton.ICON"));
        searchButton.setText(appReg.getString("searchButton.NAME"));
        
        newButton.setText(appReg.getString("newButton.NAME"));        
        newButton.setToolTipText(appReg.getString("newDocButton.TOOLTIP"));
        saveAsButton = new JButton(appReg.getString("saveAsButton.NAME"));
        saveAsButton.setToolTipText(appReg.getString("saveAsButton.TOOLTIP"));
                
        searchButton.setMinimumSize(new Dimension(190 , 20));
        searchButton.setPreferredSize(new Dimension(190 , 20));
        
        newButton.setMinimumSize(new Dimension(55 , 22));
        newButton.setPreferredSize(new Dimension(55 , 22));
        saveAsButton.setMinimumSize(new Dimension(95 , 22));
        saveAsButton.setPreferredSize(new Dimension(95 , 22));
    }
    private void registerListeners()
    {
    	cancelButton.addActionListener(this);     
    	okButton.addActionListener(this);        
        applyButton.addActionListener(this);
        newButton.addActionListener(this);
        saveAsButton.addActionListener(this);
        searchButton.addActionListener(this);
        
        //On Mouse Click Tree Selection Listener is taking long time to execute valueChanged method so Mouse Listener is registered
        navigatorTree.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent me )
		    {    
				bIs_MousePressed = true;
				TCComponent docComponent = null;
				if ( navigatorTree != null )
				{
					docComponent = navigatorTree.getSelectedTCComponent();
					validateSelectedComponent(docComponent);
				}
		    }
			@Override
			public void mouseReleased(MouseEvent mouseevent) {
				// TODO Auto-generated method stub
				super.mouseReleased(mouseevent);
				bIs_MousePressed = false;
			}
		});
        navigatorTree.addTreeSelectionListener(new TreeSelectionListener() 
		{
			public void valueChanged(TreeSelectionEvent e) 
			{		
				if(bIs_MousePressed == false)
				{
					TCComponent docComponent = null;
					if ( navigatorTree != null )
					{
						docComponent = navigatorTree.getSelectedTCComponent();
						validateSelectedComponent(docComponent);
					}
				}
				bIs_MousePressed = false;
		    }
		});
    }
    private void setEditability()
    {
    	tNameDoc.setEditable(false);         
        tContractDate.setEditable(false);
        tContractDate.setRequired(true);
        tPlannedDate.setEditable(false);
        tPlannedDate.setRequired(true);           
        tDocCode.setEnabled(true);
    }
    private void createSearchPanel()
    {
    	searchHomePanel.setMinimumSize(new Dimension(250 , 50));
        searchHomePanel.setPreferredSize(new Dimension(250 , 50));
        searchHomePanel.setLayout(rightTopButtonGrid);  
        
    	searchHomePanel.add(searchButton, new GridBagConstraints(0 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        				  GridBagConstraints.NORTH , GridBagConstraints.NONE ,
                        				  new Insets(0 , 0 , 0 , 0) , 0 , 0));
        searchHomePanel.add(homeButton,   new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        				  GridBagConstraints.SOUTH , GridBagConstraints.NONE ,
                        				  new Insets(0 , 5 , 0 , 5) , 0 , 0));
    }
    private void createBasePanel()
    {
    	basePanel.setMinimumSize(new Dimension(780 , 590));
        basePanel.setPreferredSize(new Dimension(780 , 590));
        basePanel.setLayout(gridBagLayout);
        
    	basePanel.add(logoPanel,new GridBagConstraints(0 , 0 , 2 , 1 , 0.0 , 0.0 ,
                        		GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        		new Insets(0 , 0 , 0 , 0) , 0 , 0));
        basePanel.add(attrPanel, new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        		 GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        		 new Insets(0 , 0 , 0 , 0) , 0 , 0));
        basePanel.add(rightPanel,new GridBagConstraints(1 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        		 GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        		 new Insets(0 , 0 , 0 , 0) , 0 , 0));
        basePanel.add(buttonPanel, new GridBagConstraints(0 , 2 , 2 , 1 , 0.0 , 0.0 ,
                        		   GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        		   new Insets(0 , 0 , 0 , 0) , 0 , 0));
    }
    private void lookAndFeel()
    {
    	 String nameForLAndF = UIManager.getLookAndFeel().getClass().getName();
         System.out.println("Font Name : " + nameForLAndF);
         MetalLookAndFeel lookAndFeel = null;
         if ( nameForLAndF.indexOf("MetalLookAndFeel") != -1 )
             lookAndFeel = (MetalLookAndFeel) UIManager.getLookAndFeel();
         // if look and feel is MetalLookAndFeel
         if ( null != lookAndFeel )
         {
             String fontName = MetalLookAndFeel.getWindowTitleFont().getFontName();
             Font setFont = new Font(fontName , 1 , 11);
             lNameDoc.setFont(setFont);
             lDocCode.setFont(setFont);
             lPlannedDateValAdd.setFont(setFont);
             lGpNameDoc.setFont(setFont);
             lPlannedDate.setFont(setFont);
             lContractDate.setFont(setFont);
             lResponsible.setFont(setFont);
             lComment.setFont(setFont);
             lCustomerCode.setFont(setFont);
             lAdditionalCode.setFont(setFont);
             lLineItem.setFont(setFont);
             lTab.setFont(setFont);
             int colSize = 0;
             if ( nameForLAndF.indexOf("SandMetalLookAndFeel") != -1
                  || nameForLAndF.indexOf("BasicMetalLookAndFeel") != -1
                  || nameForLAndF.indexOf("GreenMetalLookAndFeel") != -1
                  || nameForLAndF.indexOf("CyanMetalLookAndFeel") != -1 )
                 colSize = 19;
             else if ( nameForLAndF.indexOf("CharcoalMetalLookAndFeel") != -1
                     || nameForLAndF.indexOf("HighVisionMetalLookAndFeel") != -1 )
                 colSize = 26;
             else if ( nameForLAndF.indexOf("LowVisionMetalLookAndFeel") != -1 )
                 colSize = 16;
             tDocCode.setPreferredSize(new Dimension(210 , 21));
             tNameDoc.setColumns(colSize);
             tCustomerCode.setColumns(colSize);
             tAdditionalCode.setColumns(colSize);
             tContractDate.setColumns(colSize);
             tPlannedDate.setColumns(colSize);
             tLineItem.setColumns(colSize);
             tTab.setColumns(colSize);
         }
         // if look and feel is WindowsLookAndFeel
         else
         {
             int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
             int colSize = 0;
             if ( dpi == 96 )
                 colSize = 25;
             else if ( dpi == 120 ) 
             {
            	 UIManager.getLookAndFeel().getDefaults().getFont("Font");
            	 String fontName = UIManager.getFont("TextField.font").getFontName();
            	 Font setFont = new Font(fontName , 0 , 13);
            	 lNameDoc.setFont(setFont);
            	 lDocCode.setFont(setFont);
            	 lPlannedDateValAdd.setFont(setFont);
            	 lGpNameDoc.setFont(setFont);
            	 lPlannedDate.setFont(setFont);
            	 lContractDate.setFont(setFont);
            	 lResponsible.setFont(setFont);
            	 lComment.setFont(setFont);
            	 lCustomerCode.setFont(setFont);
            	 lAdditionalCode.setFont(setFont);
            	 newButton.setFont(setFont);
            	 saveAsButton.setFont(setFont);
            	 lLineItem.setFont(setFont);
                 lTab.setFont(setFont);
             }
             
             tDocCode.setMinimumSize(new Dimension(210 , 21));
             tDocCode.setPreferredSize(new Dimension(210 , 21));
             tNameDoc.setMinimumSize(new Dimension(210 , 21));
             tNameDoc.setPreferredSize(new Dimension(210 , 21));
             tCustomerCode.setMinimumSize(new Dimension(210 , 21));
             tCustomerCode.setPreferredSize(new Dimension(210 , 21));
             tAdditionalCode.setMinimumSize(new Dimension(210 , 21));
             tAdditionalCode.setPreferredSize(new Dimension(210 , 21));
             tContractDate.setMinimumSize(new Dimension(210 , 21));
             tContractDate.setPreferredSize(new Dimension(210 , 21));
             tPlannedDate.setMinimumSize(new Dimension(210 , 21));
             tPlannedDate.setPreferredSize(new Dimension(210 , 21));
             
             newButton.setMinimumSize(new Dimension(85 , 22));
             newButton.setPreferredSize(new Dimension(85 , 22));
             
             tComment.setRows(10);
             tComment.setColumns(30);
             
             tLineItem.setMinimumSize(new Dimension(210 , 21));
             tLineItem.setPreferredSize(new Dimension(210 , 21));
             
             tTab.setMinimumSize(new Dimension(210 , 21));
             tTab.setPreferredSize(new Dimension(210 , 21));
         }
    }
    private void createRightPanel()
    {
    	SoftBevelBorder softBorder = new SoftBevelBorder( SoftBevelBorder.RAISED);
        Color highlightInnerColor = softBorder.getHighlightInnerColor();
        Color shadowOuterColor = softBorder.getShadowOuterColor();
        Color shadowInnerColor = softBorder.getShadowInnerColor();
        SoftBevelBorder borderToSetToComp = new SoftBevelBorder(
                SoftBevelBorder.RAISED , new Color(233 , 233 , 218) ,
                highlightInnerColor , shadowOuterColor , shadowInnerColor);
        
        attrPanel.setMinimumSize(new Dimension(450 , 440));
        attrPanel.setPreferredSize(new Dimension(450 , 440));
                    
        Dimension minimumSize = new Dimension(350 , 440);
        rightPanel.setMinimumSize(minimumSize);
        rightPanel.setPreferredSize(minimumSize);
        rightPanel.setBorder(borderToSetToComp);
        
        rightPanel.add(searchHomePanel, new GridBagConstraints(0 , 0 , 1 , 1 , 0.0 , 0.0 , 
        								GridBagConstraints.PAGE_START ,GridBagConstraints.HORIZONTAL , 
        								new Insets(0 , 0 , 0 , 0) , 0 , 0));
        rightPanel.add(treeScrollPane,  new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        				GridBagConstraints.CENTER , GridBagConstraints.NONE ,
                        				new Insets(0 , 5 , 0 , 5) , 0 , 0));
    }
    private void createAttributePanel()
    {
    	SoftBevelBorder softBorder = new SoftBevelBorder( SoftBevelBorder.RAISED);
        Color highlightInnerColor = softBorder.getHighlightInnerColor();
        Color shadowOuterColor = softBorder.getShadowOuterColor();
        Color shadowInnerColor = softBorder.getShadowInnerColor();
        SoftBevelBorder borderToSetToComp = new SoftBevelBorder(
                SoftBevelBorder.RAISED , new Color(233 , 233 , 218) ,
                highlightInnerColor , shadowOuterColor , shadowInnerColor);
        
    	attrPanel.add(leftTopAttrPanel, new GridBagConstraints(0 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        				GridBagConstraints.PAGE_START ,GridBagConstraints.NONE ,
                        				new Insets(0 , 0 , 0 , 0) , 0 , 0));
        attrPanel.add(leftAttributePanel, new GridBagConstraints(0 , 1 , 1 , 1 , 0.0 , 0.0 ,
                        				  GridBagConstraints.CENTER ,GridBagConstraints.NONE ,
                        				  new Insets(0 , 0 , 0 , 0) , 0 , 0));
        attrPanel.setBorder(borderToSetToComp);
    }
    private void createTopLeftAttrPanel()
    {
    	populateNovCodeCombo();
    	leftTopAttrPanel.setMinimumSize(new Dimension(400 , 80));
        leftTopAttrPanel.setPreferredSize(new Dimension(400 , 80));
        leftTopAttrPanel.setLayout(leftTopAttrPanelGrid);
        leftTopAttrPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        leftTopAttrPanel.add(lDocCode, new GridBagConstraints(0 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        			   GridBagConstraints.WEST , GridBagConstraints.NONE ,
                        			   new Insets(0 , 5 , 0 , 5) , 0 , 0));
        leftTopAttrPanel.add(tDocCode, new GridBagConstraints(1 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        			   GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        			   new Insets(2 , 2 , 2 , 5) , 0 , 0));
        JLabel dummyLabelForAlignment = new JLabel();
        dummyLabelForAlignment.setPreferredSize(new Dimension(20 , 22));
        leftTopAttrPanel.add(dummyLabelForAlignment, new GridBagConstraints(2 , 0 , 1 , 1 , 0.0 , 0.0 ,
                        							 GridBagConstraints.EAST , GridBagConstraints.NONE ,
                        							 new Insets(0 , 0 , 0 , 0) , 0 , 0));
    }
    private void resPersonButton()
    {
        ImageIcon usericon = null;
        try
        {
            usericon = appReg.getImageIcon("user.ICON");
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
       
        dataProvider = new DataProvider();
        cResponsible = new NOVSMDLAutoFillTextField(new MyNOVDocument(dataProvider), "", 20, dataProvider);
        data = getupdatedResponsibleList();
        dataProvider.setData(data);
        cResponsible.setMinimumSize(new Dimension(210 , 21));
        cResponsible.setPreferredSize(new Dimension(210 , 21));
        btnResponsible.setIcon(usericon);
        btnResponsible.setMinimumSize(new Dimension(20 , 22));
        btnResponsible.setPreferredSize(new Dimension(20 , 22));
        btnResponsible.addActionListener(this);
    }
    private void userButtonAction()
    {
    	try
        {
            userPopup = new JDialog(NOVSMDLNewEditDialog.this ,
                    true);
            userPopup.getRootPane().setWindowDecorationStyle(
                    JRootPane.PLAIN_DIALOG);
            List list = getupdatedResponsibleList();
            if ( list.size() > 0 )
            {
                jlist = new JList(list.toArray());
                jlist.addListSelectionListener(
                		new ListSelectionListener() 
                		{
                			public void valueChanged(ListSelectionEvent arg0 )
                			{
                				cResponsible.setText(jlist.getSelectedValue().toString());
                				userPopup.dispose();
                			}
                		});
                JScrollPane scrollPane = new JScrollPane(jlist);
                scrollPane.setSize(300, 325);
                userPopup.setTitle("Users List");
                userPopup.getContentPane().add(scrollPane);
                int x = btnResponsible.getLocationOnScreen().x;
                int y = btnResponsible.getLocationOnScreen().y;
                userPopup.setSize(300, 325);
                userPopup.setLocation(x, y);
                userPopup.repaint();
                userPopup.validate();
                userPopup.setModal(true);
                userPopup.setVisible(true);
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    private void groupButton()
    {
    	ImageIcon groupimage = null;
        try
        {
            groupimage = appReg.getImageIcon("group.ICON");
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        //if(groupList == null || groupList.size() == 0)
        {
        	//groupList = getGroupList();
        	Set<String> groupsSet = NOVGroupDataHelper.getGroupData().keySet();
        	String[] sGroupData = (String[]) groupsSet.toArray(new String[groupsSet.size()]);
        	Collections.addAll(groupList, sGroupData);
        }
        cGroupName = new NOVSMDLAutofilEditorl(groupList,false);
        cGroupName.addFocusListener(new FocusAdapter() 
        {
            public void focusLost(FocusEvent fe )
            {
            	dataProvider.setData(data);
            }
        });
        
        cGroupName.setMinimumSize(new Dimension(210 , 21));
        cGroupName.setPreferredSize(new Dimension(210 , 21));
        
        btnGroup.setIcon(groupimage);
    	btnGroup.setMinimumSize(new Dimension(20 , 22));
        btnGroup.setPreferredSize(new Dimension(20 , 22));
        btnGroup.addActionListener(this);
    }
    private void groupButtonAction()
    {
    	try
        {
            groupPopup = new JDialog(NOVSMDLNewEditDialog.this ,
                    true);
            groupPopup.getRootPane().setWindowDecorationStyle(
                    JRootPane.PLAIN_DIALOG);
            groupPopup.setTitle("Group List");
            jlist = new JList(groupList.toArray());
            jlist.addListSelectionListener(
            		new ListSelectionListener() 
            		{
            			public void valueChanged(ListSelectionEvent arg0 )
            			{
            				// TODO Auto-generated method stub
            				cGroupName.setText(jlist.getSelectedValue().toString());
            				dataProvider.setData(data);
            				groupPopup.dispose();
            			}
            		});
            JScrollPane scrollPane = new JScrollPane(jlist);
            scrollPane.setSize(300, 325);
            groupPopup.getContentPane().add(scrollPane);
            int x = btnGroup.getLocationOnScreen().x;
            int y = btnGroup.getLocationOnScreen().y;
            groupPopup.setSize(300, 325);
            groupPopup.setLocation(x, y);
            groupPopup.repaint();
            groupPopup.validate();
            groupPopup.setModal(true);
            groupPopup.setVisible(true);
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    private void contractAndPlannedDateButtons()
    {
    	SimpleDateFormat formatter= new SimpleDateFormat();
        String pattern="dd-MMM-yyyy";
        formatter.applyPattern(pattern);
        
    	java.util.Date d = new java.util.Date();
        dateContractButton = new DateButton();
        
        dateContractButton.setDisplayFormatter(formatter);
        
        dateContractButton.setDate(d);
        dateContractButton.setText("");
        dateContractButton.setMinimumSize(new Dimension(20 , 22));
        dateContractButton.setPreferredSize(new Dimension(20 , 22));
        dateContractButton.addPropertyChangeListener(new PropertyChangeListener()
        {
        	public void propertyChange(PropertyChangeEvent evt )
        	{
        		if ( evt.getPropertyName().equals("text") )
        		{
        			dateContractButton.setText("");
        			tContractDate.setText(dateContractButton.getDateString());
        			dateContractButton.postDown();
        		}
        	}
        });
        
        java.util.Date d1 = new java.util.Date();
        datePlannedButton = new DateButton();
        
        datePlannedButton.setDisplayFormatter(formatter);
        
        datePlannedButton.setDate(d1);
        datePlannedButton.setText("");        
        datePlannedButton.setMinimumSize(new Dimension(20 , 22));
        datePlannedButton.setPreferredSize(new Dimension(20 , 22));
        datePlannedButton.addPropertyChangeListener(new PropertyChangeListener() 
        {
        	public void propertyChange(PropertyChangeEvent evt )
        	{
        		if ( evt.getPropertyName().equals("text") )
        		{
        			datePlannedButton.setText("");
        			tPlannedDate.setText(datePlannedButton.getDateString());
        			datePlannedButton.postDown();
        		}
        	}
        });
    }
    public void okButtonListnerAction(boolean disposeDlg )
    {
        String sNovCode = "";
        Object selItem = tDocCode.getSelectedItem();
        String sContractDate = tContractDate.getText();
        String sPlannedDate = tPlannedDate.getText();
        String sResPerson = cResponsible.getText();// != null ? cResponsible.getText().toString() : "";
        String sCustCode = tCustomerCode.getText();
        String sAddnlCode = tAdditionalCode.getText();
        String sGroup = cGroupName.getText() != null ? cGroupName.getText().toString() : "";
        String sComments = tComment.getText();
        String sLineItem = tLineItem.getText();
        String sTab = tTab.getText();
        		
        if (selItem != null) 
        {
        	sNovCode = tDocCode.getSelectedItem().toString().trim();
		}
        sNovCode = sNovCode.indexOf("=")==-1? sNovCode:sNovCode.substring(0, sNovCode.indexOf('='));
        
        boolean bIs_Mandatory = true;
        bIs_Mandatory = isMandatoryFieldsFilled(sNovCode,sContractDate,sPlannedDate,sResPerson);        
        if(bIs_Mandatory == true)
        {
        	TCComponentItem        docItem = null;
        	TCComponentItemRevision docRev = null;

        	String sDocRevId = "";
        	Object sStatus   = "";
        	String sDocTitle = "";
        	String sDocRevDesc = "";

        	try
        	{
        		docItem = getDocItem();
        		if(docItem != null)
        		{
        			//docRev = docItem.getLatestItemRevision();
        			docRev = docItem.getLatestItemRevision();
        			sDocTitle = docItem.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();
        			sDocRevId = docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();
        			TCComponent[] statusList = docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
        			if(statusList.length > 0)
        			{
        				sStatus = statusList[statusList.length-1].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS_NAME).getPropertyValue();
        			}
        			sDocRevDesc = docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC).getStringValue();
        		}
        	}
        	catch ( TCException e )
        	{
        		e.printStackTrace();
        	}
        	boolean bIs_DuplicateDoc = false;
        	bIs_DuplicateDoc = isDuplicateDocument(docItem); 
        	if(bIs_DuplicateDoc != true)
        	{
        		Date dContractDate = null;
        		Date dPlanneddate = null;
        		if(sContractDate != null && !sContractDate.trim().isEmpty())
        		{
        			dContractDate = NOVCreateSMDLHelper.getDate(sContractDate);
        		}
        		if(sPlannedDate != null && !sPlannedDate.trim().isEmpty())
        		{
        			dPlanneddate = NOVCreateSMDLHelper.getDate(sPlannedDate);
        		}
        		Hashtable<String, Object> tableRowVec = new Hashtable<String,Object>(); 
        		
	        		tableRowVec.put(NOV4ComponentSMDL.SMDL_CODE_NUMBER,sNovCode);
	        		tableRowVec.put(NOV4ComponentSMDL.ADDITIONAL_CODE,sAddnlCode);
	        		tableRowVec.put(NOV4ComponentSMDL.CUSTOMER_CODE,sCustCode);
	        		if(docItem!=null)
	        		{
		        		tableRowVec.put(NOV4ComponentSMDL.DOCUMENT_REF,docItem);	
		        		//tableRowVec.put(NOV4ComponentSMDL.DOCUMENT_REV_REF,docRev);
	        		}
        			tableRowVec.put(NOV4ComponentSMDL.NOV_RELEASE_STATUS,sStatus);
	        		tableRowVec.put(NOV4ComponentSMDL.SMDL_NAME,sDocTitle);
	        		tableRowVec.put(NOV4ComponentSMDL.SMDL_DESC,sDocRevDesc);
	        	
	        		tableRowVec.put(NOV4ComponentSMDL.DOC_CONTRACT_DATE,dContractDate);
	        		tableRowVec.put(NOV4ComponentSMDL.DOC_PLANNED_DATE,dPlanneddate);
	        		tableRowVec.put(NOV4ComponentSMDL.GROUP_NAME,sGroup);
	        		tableRowVec.put(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE,sResPerson);
	        		tableRowVec.put(NOV4ComponentSMDL.COMMENTS,sComments);
	        		tableRowVec.put(NOV4ComponentSMDL.LINE_ITEM_NUMBER,sLineItem);
	        		tableRowVec.put(NOV4ComponentSMDL.CODEFORM_TAB,sTab);
        		
        		
        		TableModel tableModel = m_smdlTable.getModel();
        		if(tableModel instanceof NOVCreateSMDLTableModel) //Adding a row from Create SMDL Wizard
        		{
        			NOVCreateSMDLTableLine createSMDLTableLine = null;
        			NOVCreateSMDLTableModel smdlTableModel = (NOVCreateSMDLTableModel)tableModel;
        			//createSMDLTableLine = smdlTableModel.createLine(tableRowVec.toArray());
        			createSMDLTableLine = new NOVCreateSMDLTableLine(tableRowVec);
        			if(!updateExistingRow)
        			{        			
        				m_smdlTable.addRow(createSMDLTableLine);
        				createSMDLTableLine.setCodeAddedStatus(true);
        			}
        			else
        			{
        				int iRowIndexToUpdate = -1;
        				iRowIndexToUpdate = m_smdlTable.getSelectedRow();
        				
        				m_smdlTable.removeRow(iRowIndexToUpdate);
        				smdlTableModel.fireTableRowsDeleted(iRowIndexToUpdate, iRowIndexToUpdate);
        				m_smdlTable.insertRow(iRowIndexToUpdate, createSMDLTableLine);
        				smdlTableModel.fireTableRowsInserted(iRowIndexToUpdate,iRowIndexToUpdate);
        				createSMDLTableLine.setRowUpdatedStatus(true);
        				
        				int iDocCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
        				NOVDocNoCellEditor editor = (NOVDocNoCellEditor)m_smdlTable.getCellEditor(iRowIndexToUpdate, iDocCol);
                   	 	editor.setValue(docItem);
        			}
        		}
        		else //Adding a row from SMDL Pane
        		{
        			NOVSMDLTableLine smdlTableLine = null;
        			NOVSMDLTableModel smdltableModel = (NOVSMDLTableModel)tableModel;
        			    			
        			if(!updateExistingRow)
        			{ 
        				//smdlTableLine = smdltableModel.createLine(tableRowVec.toArray());
        				smdlTableLine= new NOVSMDLTableLine(tableRowVec);
        				m_smdlTable.addRow(smdlTableLine);
        				smdlTableLine.setTableLineAddedStatus(true);
        			}
        			else
        			{        				
        				int iRowIndexToUpdate = -1;
        				iRowIndexToUpdate = m_smdlTable.getSelectedRow();
            			AIFTableLine tableLine = m_smdlTable.getRowLine(iRowIndexToUpdate);
            			smdlTableLine = (NOVSMDLTableLine)tableLine;
            			TCComponent codeObject = smdlTableLine.getTableLineCodeObject();
            			
            			//smdlTableLine = smdltableModel.createLine(tableRowVec.toArray());
            			smdlTableLine = new NOVSMDLTableLine(tableRowVec);
        				smdltableModel.removeRow(iRowIndexToUpdate);
        				smdltableModel.fireTableRowsDeleted(iRowIndexToUpdate, iRowIndexToUpdate);
        				smdltableModel.insertRow(iRowIndexToUpdate, smdlTableLine);
        				smdlTableLine.setTableLineUpdatedStatus(true);
        				smdlTableLine.setTableLineCodeObject(codeObject);
        			}
        		}
        		NOVSMDLTableHelper.sortTableRowsInAscendingOrder(m_smdlTable,NOV4ComponentSMDL.SMDL_CODE_NUMBER);
        		if ( disposeDlg )
        		{
        			NOVSMDLNewEditDialog.this.dispose();
        		}
        		else
        		{
        			clearAllFields();
        			navigatorTree.clearSelection();
        		}
        	}
        	else
        	{
        		MessageBox.post(this, appReg.getString("duplicateDoc.TITLE"),appReg.getString("DuplicateDoc.MSG"),MessageBox.INFORMATION);
        	}
        }
    }
    

    private void clearAllFields()
    {
    	tDocCode.setSelectedIndex(-1);
    	tNameDoc.setText("");
    	tCustomerCode.setText("");
    	tAdditionalCode.setText("");
    	cGroupName.setText("");
        tContractDate.setText("");
        tPlannedDate.setText("");
        cResponsible.setText("");
        tComment.setText("");
        rowComp = null;
        m_docComponent = null;
    	tLineItem.setText("");
    	tTab.setText("");
    }
    private void clearDocumentFields()
    {
    	tNameDoc.setText("");
    	//TCDECREL-2366: click eraser icon should only remove document but should not remove planned date & group
    	//cGroupName.setText("");
    	//tPlannedDate.setText("");
    	rowComp = null;
    	m_docComponent = null;
    }
    private boolean isDuplicateDocument(TCComponentItem docItem)
    {
    	boolean bIs_DuplicateDoc = false;
    	int iTotalRows = m_smdlTable.getRowCount();
		for(int iRow = 0;iRow<iTotalRows;iRow++)
		{
			AIFTableLine tableLine = m_smdlTable.getRowLine(iRow);
			if(tableLine instanceof NOVCreateSMDLTableLine)
			{
				NOVCreateSMDLTableLine smdltableLine = (NOVCreateSMDLTableLine)tableLine;
				if(smdltableLine.getDocRemovedStatus() != true)
				{
					Object docObj = smdltableLine.getProperty(NOV4ComponentSMDL.DOCUMENT_REF);
					if(docObj!=null && docObj.toString().trim().length()>0)
					{
						if((TCComponentItem)docObj == docItem)
						{
							bIs_DuplicateDoc = true;
							break;
						}
					}
				}
    		}
			else if(tableLine instanceof NOVSMDLTableLine)
			{
				NOVSMDLTableLine smdltableLine = (NOVSMDLTableLine)tableLine;
				Object docObj = smdltableLine.getProperty(NOV4ComponentSMDL.DOCUMENT_REF);
				if(docObj!=null && docObj.toString().trim().length()>0)
				{
					if((TCComponentItem)docObj == docItem)
					{
						bIs_DuplicateDoc = true;
						break;
					}
				}
			}
    	}
    	return bIs_DuplicateDoc;
    }
    private TCComponentItem getDocItem()
    {
    	TCComponentItem docItem = null;
    	try
    	{
    		if ( m_docComponent != null )
    		{
    			if ( m_docComponent instanceof TCComponentItemRevision )
    			{
    				docItem = ((TCComponentItemRevision) m_docComponent).getItem();
    			}
    			else
    			{
    				docItem = (TCComponentItem)m_docComponent;
    			}
    		}
    		else
    		{
    			docItem = (TCComponentItem) rowComp;
    		}
    	}
    	catch(TCException ex)
    	{
    		ex.printStackTrace();
    	}
    	return docItem;
    }
    private boolean isMandatoryFieldsFilled(String sNovCode,String sContractDate,String sPlannedDate,String sResPerson)
    {
    	String missingStr = "";
    	boolean bIs_Mandatory = true;
    	if ( (sNovCode.length() == 0)||(sContractDate.equals(""))
    			||(sPlannedDate.equals("")) || (sResPerson.equals("")))
    	{
    		if ( sNovCode.length() == 0 )
    		{
    			missingStr = lDocCode.getText();
    			bIs_Mandatory = false;
    		}
    		if ( sContractDate.equals("") )
    		{
    			missingStr = missingStr == "" ? missingStr : missingStr + ", ";
    			missingStr = missingStr + lContractDate.getText();
    			bIs_Mandatory = false;
    		}
    		if ( sPlannedDate.equals("") )
    		{
    			missingStr = missingStr == "" ? missingStr : missingStr+ ",  ";
    			missingStr = missingStr + lPlannedDate.getText();
    			bIs_Mandatory = false;
    		}
    		if ( sResPerson.equals("") )
    		{
    			missingStr = missingStr == "" ? missingStr : missingStr + " and ";
    			missingStr = missingStr + lResponsible.getText();
    			bIs_Mandatory = false;
    		}
    		MessageBox.post(this, appReg.getString("Error.MSG"),appReg.getString("mandatoryFields.MSG")
    				+ '\n' + missingStr, MessageBox.ERROR);
    		
    	}
    	return bIs_Mandatory;
    }
    public void run()
    {
        // TODO Auto-generated method stub
        super.run();
        this.setVisible(true);
    }
    public void loadHomeNodeTree()
    {
        try
        {
            TCComponentUser tcUser = session.getUser();
            TCComponentFolder tcHomeFolder = tcUser.getHomeFolder();
            //String s = appReg.getString("expansionString") + " " + tcHomeFolder.toString();
            //session.setStatus(s);
            //navigatorTree.clearSelection();
            TCTreeNode tcTreeNode = new TCTreeNode(tcHomeFolder);
            navigatorTree.setRoot(tcTreeNode);    
                        
            setTreeDisplayOrder();
            
            //navigatorTree.clearSelection();
            //treeScrollPane.revalidate();
            treeScrollPane.repaint();
        }
        catch ( TCException imanexception )
        {
            imanexception.printStackTrace();
        }
        catch ( Exception exception )
        {
            exception.printStackTrace();
        }
    }
    private void setTreeDisplayOrder()
    {
    	String strTreeDispOrderValue = null;

		TCPreferenceService prefServ = session.getPreferenceService();
		String treeDispOrderPref = NOVSMDLConstants.HOME_TREE_DISP_ORDER_PREF;
		strTreeDispOrderValue = prefServ.getString(TCPreferenceService.TC_preference_user, treeDispOrderPref);
		
		navigatorTree.setDisplayingOrder(strTreeDispOrderValue);
    }
    public static byte[] getIcon(String str )
    {
        byte[] retval = null;
        try
        {
            InputStream is = ClassLoader.getSystemResourceAsStream(str);
            retval = new byte[is.available() + 1];
            int lastRead = 0;
            int bRead = 0;
            int total = is.available();
            int toRead = is.available();
            while ( lastRead < total )
            {
                bRead = is.read(retval, lastRead, toRead);
                lastRead += bRead;
                toRead = total - lastRead;
            }
        }
        catch ( Exception e )
        {
            System.out.println("Miscellaneous.getIcon Could not get Icon ("
                    + str + ") : " + e);
        }
        return retval;
    }
    public void validateSelectedComponent(TCComponent docComponent )
    {
        if ( docComponent != null )
        {
            String type = docComponent.getType();
            clearAttributePanel();
            if ( type.startsWith("Documents") )
            {
                if ( !(docComponent instanceof TCComponentItem) && !(docComponent instanceof TCComponentItemRevision) )
                {
            		MessageBox.post(this, appReg.getString("selectDocuments.MSG"),appReg.getString("invalidDocumentType.MSG"), MessageBox.ERROR);
                        cGroupName.setEnabled(true);
                }
                else
                {
                    try
                    {
                        String status = null;
                        if ( docComponent instanceof TCComponentItemRevision )
                        {
                            status = docComponent.getProperty(NOVSMDLConstants.DOCUMENT_STATUS);
                            TCComponent navTreeComp = ((TCComponentItemRevision) docComponent).getItem();
                            docComponent = null;
                            docComponent = navTreeComp;
                        }
                        else if(docComponent instanceof TCComponentItem)
                        {
                        	TCComponent[] doc_Rev_list = ((TCComponentItem)docComponent).getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();                			
                            status =doc_Rev_list[doc_Rev_list.length -1].getProperty(NOVSMDLConstants.DOCUMENT_STATUS);
                        }
                        tNameDoc.setText(docComponent.toString());
                        m_docComponent = docComponent;
                        try
                        {
                        	String strContextGroupName = "";
                            strContextGroupName = docComponent.getProperty("owning_group");
                            cGroupName.setText(strContextGroupName);
                            cGroupName.setEnabled(false);
                            //set the Doc Planned Date ToDay's date if the documents released
                            if(status.indexOf("Released")!=-1)
                            {
                                 DateFormat dateFormat = new SimpleDateFormat ("dd-MMM-yyyy");
                                 Date date = new Date ();
                                 String dateStr = dateFormat.format (date); 
                                 tPlannedDate.setText(dateStr);
                            }
                            else
                            {
                            	tPlannedDate.setText("");
                            }
                        }
                        catch ( Exception e )
                        {
                            e.printStackTrace();
                        }
                    }
                    catch ( TCException ex )
                    {
                        ex.printStackTrace();
                    }
                    catch ( Exception e )
                    {
                        e.printStackTrace();
                    }
                }
            }
            else
            {
                cGroupName.setEnabled(true);
                cGroupName.setText("");
                m_docComponent = null;
                MessageBox.post(this, appReg.getString("selectDocuments.MSG"),
                		appReg.getString("invalidDocumentType.MSG"), 
                		MessageBox.ERROR);
            }
        }
    }

    private void clearAttributePanel()
    {
        tNameDoc.setText("");
    }

    private Vector<String> getupdatedResponsibleList()
    {
    	//m_userList = NOVUserDataHelper.getUsers(cGroupName.getText());
    	m_userList = NOVUserDataHelper.getUpdateUserList();
        /*if ( !m_userList.contains(cResponsible.getText()) )
        {
        	cResponsible.setText("");
        }*/
        return m_userList;
    }
    public void loadPropValueForWizard()
    {
        try
        {
        	int iNOVCodeCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.SMDL_CODE_NUMBER);
        	int iCustCodeCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.CUSTOMER_CODE);
        	int iAddnlCodeCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.ADDITIONAL_CODE);
        	int iDocCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
			int iContractDateCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
			int iPlannedDateCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOC_PLANNED_DATE);
			int iGroupCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.GROUP_NAME);
			int iResPersonCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);
			int iCommentsCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.COMMENTS);
			int iLineItemCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.LINE_ITEM_NUMBER);
			int iTabCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.CODEFORM_TAB);
			
			 int iSelectedRow = m_smdlTable.getSelectedRow();
			 if(iSelectedRow != -1)
			 {
				 Object sNovCode = m_smdlTable.getValueAtDataModel(iSelectedRow, iNOVCodeCol);
				 if(sNovCode != null)
				 {
					 tDocCode.setSelectedItem(sNovCode);
				 }
				 else
				 {
					 tDocCode.setSelectedItem(new String(""));
				 }
				 
				// String sGroup = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iGroupCol);
				 String sGroup =( m_smdlTable.getValueAtDataModel(iSelectedRow, iGroupCol)).toString();
				 if(sGroup.trim().length()>0)
				 {
					 cGroupName.setText(sGroup);
				 }
				 else
				 {
					 cGroupName.setText("");
				 }
				 
				 String sResPerson = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iResPersonCol);
				 if(sResPerson.trim().length()>0)
				 {
					 if(m_userList != null && m_userList.size()>0)
					 {
						 dataProvider.setData(m_userList);
					 }
					 else
					 {
						 dataProvider.setData(data);
					 }
	                 cResponsible.setText(sResPerson);
				 }
				 else
				 {
					 cResponsible.setText("");
				 }
				 
				 setContractDate(iSelectedRow,iContractDateCol);
				 setPlannedDate(iSelectedRow,iPlannedDateCol);
				 
				 String sComments = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iCommentsCol);
				 if(sComments.trim().length()>0)
				 {
					 tComment.setText(sComments);
				 }
				 else
				 {
					 tComment.setText("");
				 }
				 
				 String sCustCode = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iCustCodeCol);
				 if(sCustCode.trim().length()>0)
				 {
					 tCustomerCode.setText(sCustCode);
				 }
				 else
				 {
					 tCustomerCode.setText("");
				 }				 
				 String sAddnlCode = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iAddnlCodeCol);
				 if(sAddnlCode.trim().length()>0)
				 {
					 tAdditionalCode.setText(sAddnlCode);
				 }
				 else
				 {
					 tAdditionalCode.setText("");
				 }
				 
				 Object sDocId = m_smdlTable.getValueAtDataModel(iSelectedRow, iDocCol);
				 if(sDocId != null)
				 {
					 tNameDoc.setText(sDocId.toString());
				 }
				 else
				 {
					 tNameDoc.setText("");
				 }
				 String sLineItem = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iLineItemCol);
				 if(sLineItem.trim().length()>0)
				 {
					 tLineItem.setText(sLineItem);
				 }
				 else
				 {
					 tLineItem.setText("");
				 }
				 String sTab = (String)m_smdlTable.getValueAtDataModel(iSelectedRow, iTabCol);
				 if(sTab.trim().length()>0)
				 {
					 tTab.setText(sTab);
				 }
				 else
				 {
					 tTab.setText("");
				 }
			 }
        }
        catch ( Exception ex )
        {
        	ex.printStackTrace();
           
        }
    }
    private void setContractDate(int iSelectedRow,int iContractDateCol)
    {
    	Object objContractValue = m_smdlTable.getValueAtDataModel(iSelectedRow, iContractDateCol);
    	if(objContractValue != null)
    	{
    		String dateButtonStr = objContractValue.toString();
    		if ( dateButtonStr.equalsIgnoreCase("No date set.") )
    		{
    			tContractDate.setText("");
    		}
    		else
    		{
    			Date dContractDate = null;
    			if(!dateButtonStr.trim().isEmpty())
    			{
    				dContractDate = (Date)objContractValue;
    			}
    			tContractDate.setText(dateButtonStr);
    			dateContractButton.setDate(dContractDate);
        		dateContractButton.setText("");
    		}
    	}
    	else
    	{
    		tContractDate.setText("");
    	}
    }
    private void setPlannedDate(int iSelectedRow,int iPlannedDateCol)
    {
    	Object objPlannedDate = m_smdlTable.getValueAtDataModel(iSelectedRow, iPlannedDateCol);
    	if(objPlannedDate != null)
    	{
    		String dateButtonStr = objPlannedDate.toString();
    		if ( dateButtonStr.equalsIgnoreCase("No date set.") )
    		{
    			tPlannedDate.setText("");
    		}
    		else
    		{
    			Date dPlannedDate = null;
    			if(!dateButtonStr.trim().isEmpty())
    			{
    				dPlannedDate = (Date)objPlannedDate;
    			}
    			tPlannedDate.setText(dateButtonStr);
    			datePlannedButton.setDate(dPlannedDate);
        		datePlannedButton.setText("");
    		}
    	}
    	else
    	{
    		tPlannedDate.setText("");
    	}
    }
    public void newButtonAction()
    {
        System.out.println("New Button Selected");
        boolean bIsRSItemCreationGrp = false;
        boolean bIsRSDualItemCreationGrp = false;
        
        TCPreferenceService prefServ = session.getPreferenceService();
        String[] RSItemCreationGrps = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
                										(appReg.getString("RSItemCreationPref.NAME")));  
        String superGroup = session.getCurrentGroup().toString().split("\\.")[0];
        bIsRSItemCreationGrp = IsLoggedInGrpRSGrp(superGroup,RSItemCreationGrps);
        
        try
        {
        	String fullGroup = session.getCurrentGroup().getFullName();
        	String[] RSDualItemCreationGrps = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
        			(appReg.getString("RSDualItemCreationPref.NAME")));
        	
        	bIsRSDualItemCreationGrp = IsLoggedInGrpRSGrp(fullGroup,RSDualItemCreationGrps);
        }
        catch(TCException ex)
        {
        	ex.printStackTrace();
        }
        if(bIsRSItemCreationGrp == true || bIsRSDualItemCreationGrp == true)
        {
        	loadRSOneNewItemDialog();
        }
        else
        {
        	// call legacy new item dialog
        	loadLegacyNewItemDialog();
        }
        
        navigatorTree.refresh();
    }
    private boolean IsLoggedInGrpRSGrp(String sLoginGrp,String[] sGroupArray)
    {
    	int iArrayLength = sGroupArray.length;
    	for (int i=0;i<iArrayLength;i++) 
        {
			if (sLoginGrp.equalsIgnoreCase(sGroupArray[i]))
			{
				return true;
			}
		}
    	return false;
    }
    public void loadLegacyNewItemDialog()
    {
        TCComponentItem item = null;
        TCComponentItemRevision rev = null;
        TCComponentFolder folder = null;
        NewItemDialog newItemDialog = null;
        newItemDialog = new NewItemDialog(parent , AIFUtility
                .getCurrentApplication() , item , rev , folder , true);
        
        newItemDialog.panel.itemTypePopupButton.setText("D - Documents");
        newItemDialog.panel.itemTypePopupButton.setEnabled(false);
        newItemDialog.panel.assignItemIdButton.setEnabled(true);
        newItemDialog.setBounds(100, 100, 500, 600);
        newItemDialog.toFront();
        newItemDialog.setModal(true);
        newItemDialog.setDlgAtCenterToScreen();
        newItemDialog.setVisible(true);
               
        item = newItemDialog.panel.getDocItem();
        if(item != null)
        {
        	populateDocAttrInNewEditDlg(item);
        }
        //if(homeClicked)
        navigatorTree.refresh();
    }
    private void loadRSOneNewItemDialog()
    {
        TCComponentItem item = null;
        TCComponentItemRevision rev = null;
        TCComponentFolder folder = null;
        String[] opName = new String[]{ appReg.getString("CreateDocment.NAME") };   
        
        launchNewItemDialog(item,rev,folder,opName);
    }
    private void launchNewItemDialog(TCComponentItem item,TCComponentItemRevision rev,TCComponentFolder folder,String[] opName)
    {
    	setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    	NOVNewItemDialog newItemDialog = null;
    	newItemDialog = new NOVNewItemDialog(parent , AIFUtility.getCurrentApplication() , item , rev , folder , opName,true);		
        NOVNewItemPanel panel = newItemDialog.panel;
        panel.operationComboBox.setSelectedItem(opName[0]);
        panel.operationComboBox.setEnabled(false);
        panel.updateUI();
        newItemDialog.setDlgAtCenterToScreen(panel);
        newItemDialog.dispose();
        newItemDialog.toFront();
        newItemDialog.setModal(true);
        newItemDialog.pack();
        setCursor(Cursor.getDefaultCursor());
        newItemDialog.setVisible(true);
        NOVProgressBar pb = panel.getProgressBar();
		if(pb != null)
		{
			pb.dispose();
			pb.toFront();
			pb.setModal(true);
			pb.pack();
			pb.setVisible(true);
		}
		TCComponentItem docitem = panel.getDocumentComponent();
		if(docitem != null)
		{
			populateDocAttrInNewEditDlg(docitem);
		}
    }
    public void populateDocAttrInNewEditDlg(TCComponentItem item)
	{
    	tNameDoc.setText(item.toString());
    	try 
    	{
    		TCComponent[] doc_Rev_list = item.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
			m_docComponent = doc_Rev_list[doc_Rev_list.length -1];
			
			TCProperty itemProp = item.getTCProperty("owning_group");
    		if(itemProp != null)
    		{
    			cGroupName.setText(itemProp.toString());
    		}
    		/*if(item.getTCProperty("owning_user")!=null)
    		{
    			cResponsible.setText(item.getTCProperty("owning_user").toString());
    		}*/
    		if(imanFileObj!=null)
    		{
    			TCComponentItemRevision docRevision = item.getLatestItemRevision();
    			AIFComponentContext[] docContext = null;
    			if ( docRevision != null )
    			{
    				docContext = docRevision.getRelated("IMAN_specification");
    			}
    			if ( docContext != null && docContext.length>=0)
    			{
    				InterfaceAIFComponent datasetComp = docContext[0].getComponent();
    				ImportFileToDataset((TCComponentDataset) datasetComp,imanFileObj);
    			}   
    		}
    		navigatorTree.refresh();
    		treeScrollPane.revalidate();
    		treeScrollPane.repaint();
    	} 
    	catch (TCException e) 
    	{
    		e.printStackTrace();
    	}
	}
    public void ImportFileToDataset(TCComponentDataset dataset, File []namedRefFiles)
    {
    	try 
    	{
    		System.out.println();
    		if (null != dataset) 
    		{
    			String[] filePathNames = new String[1];
    			String[] fileTypes = new String[1];
    			String[] fileSubTypes = new String[1];
    			String[] namedReferences = new String[1];

    			String tempfiletype = dataset.getType();
    			if (tempfiletype.equals("XML")) 
    			{
    				fileTypes[0] = "Text";
    			} 
    			else 
    			{
    				fileTypes[0] = tempfiletype;
    			}

    			fileSubTypes[0] = dataset.getSubType();
    			namedReferences = getNamedRefOfDataSet(session, tempfiletype);

    			for(int i=0;namedReferences!=null && i<namedRefFiles.length;i++)
    			{
    				filePathNames[0] = "";
    				if(namedRefFiles!=null)
    				{
    					filePathNames[0] =namedRefFiles[i].getAbsolutePath();;
    				}
    				if (filePathNames[0] != null && fileTypes[0] != null
    						&& fileSubTypes[0] != null
    						&& namedReferences[0] != null) 
    				{
    					try 
    					{
    						dataset.setFiles(filePathNames, fileTypes,
    								fileSubTypes, namedReferences, 20);
    					}
    					catch (TCException ex)
    					{
    						String msg = appReg.getString("datasetImportFail.MSG")+" ("
    							+ ex.getDetailsMessage() + ")";
    						MessageBox.post(this, appReg.getString("sysAdmin.MSG"),msg,MessageBox.ERROR);
    					}
    				}
    			}
    		}

    	} catch (TCException e) {
    		e.printStackTrace();
    	}
    }
    private String[] getNamedRefOfDataSet(TCSession tcsession, String datasettype)
	{
		String namedRef[] = null;
		try{
			TCComponentDatasetDefinitionType tccompdatasetdefitype = 
			(TCComponentDatasetDefinitionType)tcsession.getTypeComponent("DatasetType");
				 
			TCComponentDatasetDefinition datasetdef =  tccompdatasetdefitype.find(datasettype);
		
			namedRef= datasetdef.getNamedReferences();		 
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return namedRef;			
	}
    private void saveAsButtonAction()
    {
    	boolean bIsRSItemCreationGrp = false;
        boolean bIsRSDualItemCreationGrp = false;
        
        TCPreferenceService prefServ = session.getPreferenceService();
        String[] RSItemCreationGrps = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
                										(appReg.getString("RSItemCreationPref.NAME")));  
        String superGroup = session.getCurrentGroup().toString().split("\\.")[0];
        bIsRSItemCreationGrp = IsLoggedInGrpRSGrp(superGroup,RSItemCreationGrps);
        
        try
        {
        	String fullGroup = session.getCurrentGroup().getFullName();
        	String[] RSDualItemCreationGrps = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
        			(appReg.getString("RSDualItemCreationPref.NAME")));
        	
        	bIsRSDualItemCreationGrp = IsLoggedInGrpRSGrp(fullGroup,RSDualItemCreationGrps);
        }
        catch(TCException ex)
        {
        	ex.printStackTrace();
        }
        if(bIsRSItemCreationGrp == true || bIsRSDualItemCreationGrp == true)
        {
        	RSDocumentSaveAsOpetation();
        }
        else
        {
        	legacySaveAsOperation();
        }        
        navigatorTree.refresh();    	
    }
    private void RSDocumentSaveAsOpetation()
    {
    	TCComponentItem item = null;
        TCComponentItemRevision rev = null;
        TCComponentFolder folder = null;
                
        if(navigatorTree.getSelectedContexts()!=null)
        {
            item = (TCComponentItem) navigatorTree.getSelectedContexts()[0].getComponent();
        }
        else
            item = (TCComponentItem) rowComp;
        
        String[] opName = new String[]{ appReg.getString("SaveAsDocument.NAME") };
        
        launchNewItemDialog(item,rev,folder,opName);
    }
    private void legacySaveAsOperation()
    {
    	try
        {
            TCComponentItem item = null;
            TCComponentItemRevision rev = null;
            TCComponentFolder folder = null;
            NewItemDialog newItemDialog = null;
            
            if(navigatorTree.getSelectedContexts()!=null)
            {
                item = (TCComponentItem) navigatorTree.getSelectedContexts()[0].getComponent();
            }
            else
                item = (TCComponentItem) rowComp;
            
            if(item==null)
            {
            	MessageBox.post(this, appReg.getString("Error.MSG"),appReg.getString("docForSaveAs.MSG"), 
            			MessageBox.ERROR);
            }
            else
            {
                TCComponentForm docMasterForm = (TCComponentForm) item
                        .getRelatedComponent("IMAN_master_form");
                newItemDialog = new NewItemDialog(parent , AIFUtility
                        .getCurrentApplication() , item , rev , folder ,true);
                newItemDialog.setBounds(100, 100, 500, 600);
                newItemDialog.panel.itemTypePopupButton.setText("D - Documents");
                newItemDialog.panel.docTypeEditor.setText(docMasterForm
                        .getFormTCProperty("DocumentsCAT").toString());
                newItemDialog.panel.setDocSubTypeName(docMasterForm
                        .getFormTCProperty("DocumentsCAT").toString());
                newItemDialog.panel.docTypePopupButton
                        .setText(docMasterForm.getFormTCProperty("DocumentsCAT").toString());
                newItemDialog.panel.docTypeEditor.setEnabled(false);
                newItemDialog.panel.docSubTypePopupButton
                        .setText(docMasterForm.getFormTCProperty("DocumentsType").toString());
                newItemDialog.panel.docSubTypePopupButton.setEnabled(false);
                newItemDialog.panel.seqno.setText("001");
                TCComponentItemRevision docRevision = ((TCComponentItem) item).getLatestItemRevision();
                AIFComponentContext[] docContext = null;
                System.out.println();
                if ( docRevision != null )
                {
                    docContext = docRevision.getRelated("IMAN_specification");
                }
                if ( docContext != null && docContext.length >= 0 )
                {
                    for ( int j = 0; j < docContext.length; j++ )
                    {
                        InterfaceAIFComponent datasetComp = docContext[j]
                                .getComponent();
                        if ( datasetComp instanceof TCComponentDataset )
                        {
                        	 String fetchFileType = "";
        		             fetchFileType = getFileRefType((TCComponentDataset) datasetComp);
        		             imanFileObj = ((TCComponentDataset) datasetComp).getFiles(fetchFileType);
                            if ( datasetComp != null )
                            {
                                newItemDialog.panel.datasetPanel
                                        .setDatasetType(datasetComp.getProperty("object_type"));
                                newItemDialog.panel.datasetPanel
                                        .setDatasetName(datasetComp.getProperty("object_type"));
                                newItemDialog.panel.datasetPanel.datasetTypeautoTextfield
                                        .setText(datasetComp.getProperty("object_type"));
                                newItemDialog.panel.datasetPanel.datasetTypeautoTextfield
                                        .setEditable(false);
                                newItemDialog.panel.datasetPanel.datasetTypeautoTextfield
                                        .setFocusable(false);
                                newItemDialog.panel.datasetPanel
                                        .setEnabled(false);
                                newItemDialog.panel.datasetPanel.datasetTypePopupButton
                                        .setEnabled(false);
                            }
                        }
                    }
                }
                newItemDialog.panel.itemTypePopupButton.setEnabled(false);
                newItemDialog.toFront();
                newItemDialog.setModal(true);
                newItemDialog.setDlgAtCenterToScreen();
                newItemDialog.setVisible(true);
                
                item = newItemDialog.panel.getDocItem();
                if(item != null)
                {
                	populateDocAttrInNewEditDlg(item);
                }
            }
        }
        catch ( TCException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void actionPerformed(ActionEvent ae )
    {
    	if(ae.getSource()==okButton )
    	{
    		okButtonListnerAction(true);
    	}
    	else if(ae.getSource()==applyButton)
    	{
    		okButtonListnerAction(false);
            if ( neglectOKListner ) return;
    	}
    	else if(ae.getSource()==cancelButton)
    	{
            NOVSMDLNewEditDialog.this.dispose();
            m_smdlTable.clearSelection();
    	}
    	else if(ae.getSource()==btnResponsible)
    	{
    		userButtonAction();
    	}
    	else if(ae.getSource()==btnGroup)
    	{
    		groupButtonAction();
    	}
    	else if(ae.getSource()==newButton)
    	{
    		newButtonAction();
    	}
    	else if(ae.getSource()==saveAsButton )
        {
    		saveAsButtonAction();
        }
    	else if(ae.getSource()==homeButton)
    	{
    		homeButtonAction();
    	}
    	else if(ae.getSource()==searchButton)
    	{
    		searchButtonAction();
    	}
    	else if(ae.getSource()==m_docInfoClearBtn)
    	{
    		clearDocumentFields();
    	}
    }
    
    public String getFileRefType(TCComponentDataset dataset)
    {
    	String strRefType=null;
    	try 
    	{
    		strRefType = dataset.getProperty("ref_names");

    		if( strRefType!=null && strRefType.indexOf(",")!=-1 )
    			strRefType = strRefType.split(",")[0];

    		return strRefType;
    	} 
    	catch (TCException e) 
    	{
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return strRefType;
    }
    
    public void populateDateInfoForCode(String novCode)
    {
    	if ( novCode.indexOf("=") != -1 )
    		novCode = tDocCode.getSelectedItem().toString().substring(0,tDocCode.getSelectedItem().toString().indexOf("="));
    	if ( novCode != null && novCode.length() > 0 )
    	{
    		String sUser = getDocResponsibleDefaultvalue();
    		dataProvider.setData(data);
    		if(sUser!="")
    		{
    		 cResponsible.setText(sUser);
    		}
    		else
    		{
    			cResponsible.setText(session.getUser().toString());
    		}
            //Mohan : Comments field should be auto populated with code description 
    		/*if ( novCodeDesc.get(novCode) != null )
    			tComment.setText(novCodeDesc.get(novCode).toString());*/
            
    		if ( novCodeIssueDate.get(novCode) != null )
    		{
    			Date dContractDate = null;
    			String sContractDate = null;
    			int noOfWeeks = 0;
    			
    			String issueDate = novCodeIssueDate.get(novCode).toString();
    			
    			if(issueDate != null)
    			{
    				noOfWeeks = NOVCreateSMDLHelper.getNoOfWeeks(issueDate);

    				dContractDate = NOVCreateSMDLHelper.getContractAwardDate(m_dpComponent,issueDate,noOfWeeks);
    			}
    			if(dContractDate!=null)
    			{
    				SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MMM-yyyy");
    				sContractDate = sdfDestination.format(dContractDate);    				

    				dateContractButton.setDate(dContractDate);
    				dateContractButton.setText("");
    				tContractDate.setText(sContractDate);
    			}
    			else
    			{
    				tContractDate.setText("");
    			}
    		}
    		else
			{
				tContractDate.setText("");
    		}
    		}
    	else
		{
			tContractDate.setText("");
    	}
    }
    //TCDECREL-1551 set default user selected during smdl creation
    private void setDefaultValues()
	{
		String defaultDocResponsibleValue = getDocResponsibleDefaultvalue();
		cResponsible.setText(defaultDocResponsibleValue);
	
	}
  //TCDECREL-1551 set default user selected during smdl creation
	private String getDocResponsibleDefaultvalue() {
		return NOVSMDLExplorerViewHelper.getDocResposible();
		//return m_smdlTable.getDocResponsible();
		
	}
}
