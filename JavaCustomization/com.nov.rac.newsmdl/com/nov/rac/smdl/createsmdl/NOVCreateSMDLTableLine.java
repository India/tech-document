package com.nov.rac.smdl.createsmdl;

import java.util.Hashtable;

import com.nov.rac.smdl.common.NOVSMDLTableLine;

//public class NOVCreateSMDLTableLine extends TCTableLine
public class NOVCreateSMDLTableLine extends NOVSMDLTableLine 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean m_bCodeRemoved = false;
	private boolean m_bDocRemoved = false;
	private boolean m_bCodeAdded = false;
	private boolean m_bRowUpdated = false;
	public NOVCreateSMDLTableLine(Hashtable<String, Object> hashtable)
	{
		super(hashtable);
	}
	public boolean getCodeRemovedStatus()
	{
		return m_bCodeRemoved;
	}
	public void setCodeRemovedStatus(boolean bIsCodeRemoved)
	{
		m_bCodeRemoved = bIsCodeRemoved;
	}
	public boolean getDocRemovedStatus()
	{
		return m_bDocRemoved;
	}
	public void setDocRemovedStatus(boolean bIsDocRemoved)
	{
		m_bDocRemoved = bIsDocRemoved;
	}
	public boolean getCodeAddedStatus()
	{
		return m_bCodeAdded;
	}
	public void setCodeAddedStatus(boolean bIsCodeAdded)
	{
		m_bCodeAdded = bIsCodeAdded;
	}
	public boolean getRowUpdatedStatus()
	{
		return m_bRowUpdated;
	}
	public void setRowUpdatedStatus(boolean bRowUpdated)
	{
		m_bRowUpdated = bRowUpdated;
	}
	public Object getProperty(String s)
	{
        Object obj = null;        
        if(s != null)
        {
           obj = propertyValues.get(s);
        }       
        return obj;
    }
}
