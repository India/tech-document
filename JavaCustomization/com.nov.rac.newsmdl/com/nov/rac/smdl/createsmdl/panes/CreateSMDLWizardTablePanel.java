package com.nov.rac.smdl.createsmdl.panes;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.JTable;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.kernel.NOV4ComponentTemplateRevision;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.common.NOVSMDLTableLine;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableModel;
import com.nov.rac.smdl.createsmdl.listener.NOVAddNOVCodeButtonListener;
import com.nov.rac.smdl.createsmdl.listener.NOVCreateSMDLTableMouseListener;
import com.nov.rac.smdl.createsmdl.listener.NOVRemoveAllDocsButtonListener;
import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVGroupDataHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.utilities.NOVSMDLObjectSOAHelper;
import com.nov.rac.smdl.utilities.NOVSMDLTableHelper;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class CreateSMDLWizardTablePanel extends Composite 
{
	private Registry m_appReg = null;
	private TCSession m_session;
	
	private Composite m_buttonPanel;
	private Composite m_tablePanel;	
	private String m_sUser=null;
	private NOVCreateSMDLTable  m_smdlTable  = null;
	
	//private CLabel m_addNewLabel;
	//private CLabel m_updateLabel;
	private CLabel m_removeLabel;
	
	private Button m_btnAddNovCode;
	private Button m_btnRemoveAllDocs;
	
	private TCComponent m_dpComp = null;
	private HashMap<String,String> m_novCodeIssueDate    = new HashMap<String,String>();
	private TCComponent m_smdlSOAObject = null;
	private HashSet<TCComponent> m_uniqueDocsAttachedToSMDLRev = new HashSet<TCComponent>(); 
	private NOVCreateSMDLTableMouseListener m_smdlTableMouseListener;
	
	public CreateSMDLWizardTablePanel(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
		m_session=(TCSession)AIFUtility.getDefaultSession();
		
		createUI(this);
	}
	private void createUI(Composite parent) 
	{
		createButtonPanel(parent);
		createTablePanel(parent);
		addListeners();
	}
	private void createButtonPanel(Composite parent)
	{
		m_buttonPanel = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(2, false);
		m_buttonPanel.setLayout(gridLayout);
		m_buttonPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		Composite m_buttonLeftPanel = new Composite(m_buttonPanel, SWT.NONE);		
		GridLayout leftPanelGL = new GridLayout(2, false);
		m_buttonLeftPanel.setLayout(leftPanelGL);
		m_buttonLeftPanel.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1));
		
		m_btnAddNovCode = new Button(m_buttonLeftPanel, SWT.LEFT);
		m_btnAddNovCode.setText(m_appReg.getString("addNOVCode.NAME"));
		m_btnAddNovCode.setToolTipText(m_appReg.getString("addNOVCode.TOOLTIP"));
	
		m_btnRemoveAllDocs = new Button(m_buttonLeftPanel, SWT.LEFT);
		m_btnRemoveAllDocs.setText(m_appReg.getString("removeAllDocs.NAME"));
		m_btnRemoveAllDocs.setToolTipText(m_appReg.getString("removeAllDocs.TOOLTIP"));
		
		Composite m_buttonRightPanel = new Composite(m_buttonPanel, SWT.NONE);		
		GridLayout rightPanelGL = new GridLayout(3, false);
		m_buttonRightPanel.setLayout(rightPanelGL);
		m_buttonRightPanel.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1));
		
/*        m_addNewLabel = new CLabel(m_buttonRightPanel, SWT.NONE);
        m_addNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));        
        m_addNewLabel.setText(m_appReg.getString("addNewLabelIcon.NAME"));
        m_addNewLabel.setImage(m_appReg.getImage("addNew.ICON"));
        
        m_updateLabel = new CLabel(m_buttonRightPanel, SWT.NONE);
        m_updateLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));        
        m_updateLabel.setText(m_appReg.getString("updateLabelIcon.NAME"));
        m_updateLabel.setImage(m_appReg.getImage("update.ICON"));*/
        
        m_removeLabel = new CLabel(m_buttonRightPanel, SWT.NONE);
        m_removeLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));        
        m_removeLabel.setText(m_appReg.getString("removeLabelIcon.NAME"));
        m_removeLabel.setImage(m_appReg.getImage("remove.ICON"));
	}
	private void createTablePanel(Composite parent) 
	{
		Label shadow_sep_h = new Label(parent, SWT.SEPARATOR | SWT.SHADOW_OUT | SWT.HORIZONTAL);
	    shadow_sep_h.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	    
		m_tablePanel = new Composite(parent, SWT.SEPARATOR|SWT.EMBEDDED);
		m_tablePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createSMDLTable();
		
		ScrollPagePane tablePane= new ScrollPagePane(m_smdlTable);
				
		SoftBevelBorder softBorder = new SoftBevelBorder(SoftBevelBorder.RAISED);
        java.awt.Color highlightInnerColor = softBorder.getHighlightInnerColor();
        java.awt.Color shadowOuterColor = softBorder.getShadowOuterColor();
        java.awt.Color shadowInnerColor = softBorder.getShadowInnerColor();
        SoftBevelBorder borderToSetToComp = new SoftBevelBorder(
                SoftBevelBorder.RAISED , new java.awt.Color(233 , 233 , 218) ,
                highlightInnerColor , shadowOuterColor , shadowInnerColor);
        tablePane.setBorder(borderToSetToComp);
        tablePane.getViewport().setBackground(java.awt.Color.WHITE);
        
       	SWTUIUtilities.embed( m_tablePanel, tablePane, false);
	}
	private void createSMDLTable()
	{
		String[] strMasterColNames=null;
		
		strMasterColNames = getMasterSMDLTableColumns();
						
		m_smdlTable = new NOVCreateSMDLTable(m_session , strMasterColNames);
        //m_smdlTable.setColumnSelectionAllowed(true);
		//m_smdlTable.setRowSelectionAllowed(true);
		//m_smdlTable.getColumnModel().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		m_smdlTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		m_smdlTable.getTableHeader().setReorderingAllowed(false);
		m_smdlTable.putClientProperty("terminateEditOnFocusLost" , Boolean.TRUE);
					
		m_smdlTableMouseListener = new NOVCreateSMDLTableMouseListener();
		m_smdlTable.addMouseListener(m_smdlTableMouseListener);
	}
	private String[] getMasterSMDLTableColumns()
	{
		String[] strColNames = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = NOVSMDLConstants.NOVSMDL_TABLECOLUMNS_PREF;
		strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_site, smdlTableColumnPref);
		if (null != strColNames && strColNames.length == 0) 
		{
			MessageBox.post(m_appReg.getString("prefError.MSG"), "ERROR",MessageBox.ERROR);
		}
		return strColNames;
	}
	public NOVCreateSMDLTable getSMDLTable()
	{
		return m_smdlTable;
	}
	private void addListeners()
	{
		m_btnAddNovCode.addSelectionListener(new NOVAddNOVCodeButtonListener(m_smdlTable));
		m_btnRemoveAllDocs.setData(m_smdlTable);
		m_btnRemoveAllDocs.addSelectionListener(new NOVRemoveAllDocsButtonListener());
	}
	public void setDPObject(TCComponent dpObj)
	{
		m_dpComp = dpObj;
		m_btnAddNovCode.setData(m_dpComp);
		m_smdlTableMouseListener.setDPObject(m_dpComp);
	}
	public void populateSMDLTableWithTemplateInfo(TCComponent templateComp)
	{
		m_smdlTable.removeAllRows();
		//TCDECREL-1551 set the selected user in table
		NOVSMDLExplorerViewHelper.setDocResponsible(m_sUser);
		//m_smdlTable.setDocResponsible(m_sUser);
		
		try 
		{
			TCComponent comp[] = templateComp.getTCProperty(NOV4ComponentTemplateRevision.TEMPLATE_STRUCTURE_DATA).getReferenceValueArray();
			int iCompLength = comp.length;
		
			for(int i =0;i<iCompLength;i++)
			{
				Vector<Object> templateVec = new Vector<Object>();
				TCComponentItem docItem = null;
				TCComponentItemRevision docRev = null;
				Date dPlannedDate = null;
				String sDocGroup = null;
				String sDocTitle = null;
				String sDocRevDesc = null;
				
				templateVec.add(comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_CODE).getStringValue());
				templateVec.add(comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_ADDITIONAL_CODE).getStringValue());
				templateVec.add("");
				Object docObj = comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_DOCUMENT_REF).getReferenceValue();
				if(docObj != null)
				{
					docItem = (TCComponentItem) docObj;
					TCComponent[] doc_Rev_list = docItem.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					docRev = (TCComponentItemRevision) doc_Rev_list[doc_Rev_list.length - 1];
					sDocTitle = docItem.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();	        		
					TCComponent[] statusList = docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
					if(statusList.length>0)
					{
						Calendar currentDate = Calendar.getInstance();
						dPlannedDate = currentDate.getTime();
					}
					sDocRevDesc = docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC).getStringValue();
					sDocGroup = docItem.getTCProperty(NOVSMDLConstants.DOCUMENT_OWNING_GROUP).toString();
				}
				templateVec.add(docItem);
				templateVec.add("");
				templateVec.add("");
				templateVec.add(sDocTitle);
				templateVec.addElement(sDocRevDesc);
				String sContractDate = comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_DOC_CONTRACT_DATE).getStringValue();
				int iNoOfWeeks = comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_WEEKS).getIntValue();
				Date dContractDate = null;
				if(sContractDate != null)
				{
					dContractDate = NOVCreateSMDLHelper.getContractAwardDate(m_dpComp,sContractDate,iNoOfWeeks);
				}
				templateVec.add(dContractDate);
				templateVec.add(dPlannedDate);
				templateVec.add(sDocGroup);
				templateVec.add(m_sUser);
				templateVec.add(comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_COMMENTS).getStringValue());
				templateVec.addElement(comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_LINE_ITEM_NUMBER).getPropertyValue());
				templateVec.addElement(comp[i].getTCProperty(NOV4ComponentTemplate.TEMPLATE_TAB).getPropertyValue());
				
				TableModel tableModel = m_smdlTable.getModel();
				if(tableModel instanceof NOVCreateSMDLTableModel)
				{
					NOVCreateSMDLTableLine smdlTableLine = ((NOVCreateSMDLTableModel)tableModel).createLine(templateVec.toArray());
					m_smdlTable.addRow(smdlTableLine);
				}
			}
			NOVSMDLTableHelper.sortTableRowsInAscendingOrder(m_smdlTable, NOV4ComponentSMDL.SMDL_CODE_NUMBER);
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public CreateInObjectHelper[] createCodeOperationInput()
	{
		NOVSMDLTableHelper.stopTableCellEditing(m_smdlTable);
		
		int iTotalRows = m_smdlTable.getRowCount();
		Object linePropertyObject = null;
		Vector<CreateInObjectHelper> createCodeTransferObject = new Vector<CreateInObjectHelper>();
		HashMap<String,TCComponent> groupDataMap = new HashMap<String,TCComponent>();
		groupDataMap = NOVGroupDataHelper.getGroupData();
		String sLoggedInUser = m_session.getUser().toString();
		for(int iRow = 0;iRow<iTotalRows;iRow++)
		{
			AIFTableLine tableLine = m_smdlTable.getRowLine(iRow);
			if(tableLine instanceof NOVCreateSMDLTableLine)
			{
				NOVCreateSMDLTableLine createSMDLLine = (NOVCreateSMDLTableLine)tableLine;
				if(createSMDLLine.getCodeRemovedStatus() == false)
				{
					String sCode = createSMDLLine.getProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER).toString();
					String sAddnlCode = "";
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.ADDITIONAL_CODE);
					if(linePropertyObject != null)
					{
						sAddnlCode = linePropertyObject.toString();
					}
					String sCustCode = "";
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.CUSTOMER_CODE);
					if( linePropertyObject != null)
					{
						sCustCode = linePropertyObject.toString();
					}
					String sResPerson = "";
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);
					if(linePropertyObject != null && linePropertyObject.toString().trim().length()>0)
					{
						sResPerson = linePropertyObject.toString();
					}
					else
					{
						//Mohan: TCDECREL-2193: if user clears/ nulls out Doc Responsible without adding another name, system will automatically default to user creating/editing SMDL 
						sResPerson = sLoggedInUser;
					}
					String sComments = "";
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.COMMENTS);
					if(linePropertyObject != null)
					{
						sComments = linePropertyObject.toString();
					}					
					String sLineItemNo = "";
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.LINE_ITEM_NUMBER);
					if(linePropertyObject != null)
					{
						sLineItemNo = linePropertyObject.toString();
					}
					String sCodeTab = "";
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.CODEFORM_TAB);
					if(linePropertyObject != null)
					{
						sCodeTab = linePropertyObject.toString();
					}
										
					CreateInObjectHelper createCodeTransferObj= new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_CREATE);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.SMDL_CODE_NUMBER, sCode, CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.ADDITIONAL_CODE, sAddnlCode, CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.CUSTOMER_CODE, sCustCode, CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE, sResPerson, CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.COMMENTS, sComments, CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.LINE_ITEM_NUMBER, sLineItemNo, CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.CODEFORM_TAB, sCodeTab, CreateInObjectHelper.SET_PROPERTY);
					
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
					Date dContractDate = null;
					if(linePropertyObject != null)
					{
						dContractDate = (Date)linePropertyObject;
						createCodeTransferObj.setDateProps(NOV4ComponentSMDL.DOC_CONTRACT_DATE, dContractDate, CreateInObjectHelper.SET_PROPERTY);
					}
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.DOC_PLANNED_DATE);
					Date dPlannedDate;
					if(linePropertyObject != null)
					{
						dPlannedDate = (Date)linePropertyObject;
						createCodeTransferObj.setDateProps(NOV4ComponentSMDL.DOC_PLANNED_DATE, dPlannedDate, CreateInObjectHelper.SET_PROPERTY);
					}					
					
					TCComponent docItem = null;
					if(createSMDLLine.getDocRemovedStatus() == false)
					{
						Object docObj = createSMDLLine.getProperty(NOV4ComponentSMDL.DOCUMENT_REF);
						if(docObj!= null && docObj.toString().trim().length()>0)
						{
							docItem = (TCComponent)docObj;
							createCodeTransferObj.setTagProps(NOV4ComponentSMDL.DOCUMENT_REF, docItem, CreateInObjectHelper.SET_PROPERTY);
							m_uniqueDocsAttachedToSMDLRev.add(docItem);
						}
					}
					
					linePropertyObject = createSMDLLine.getProperty(NOV4ComponentSMDL.GROUP_NAME);
					TCComponent groupComponent = null;
					if(linePropertyObject != null && linePropertyObject.toString().trim().length()>0)
					{
						groupComponent = groupDataMap.get(linePropertyObject.toString());
					}
					else
					{
						//TCDECREL-2723 : If document is selected and Group field contains invalid group name or blank value 
						//                then Group field should default to document's owning group
						if(docItem!= null)
						{
							try 
							{
								groupComponent = docItem.getTCProperty(NOVSMDLConstants.DOCUMENT_OWNING_GROUP).getReferenceValue();
							} 
							catch (TCException e) 
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					createCodeTransferObj.setTagProps(NOV4ComponentSMDL.GROUP_NAME, groupComponent, CreateInObjectHelper.SET_PROPERTY);
					
					
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.NOV_CODEFORM_NUM, "", CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObject.add(createCodeTransferObj);
				}
			}
		}	
		return createCodeTransferObject.toArray(new CreateInObjectHelper[createCodeTransferObject.size()]);
	}	
	public String validateDateFields()
	{
		boolean bIsValidDate = true;
		int iTotalRows = m_smdlTable.getRowCount();
		StringBuffer  invalidInputs= new StringBuffer();
		for(int iRow = 0;iRow<iTotalRows;iRow++)
		{
			AIFTableLine tableLine = m_smdlTable.getRowLine(iRow);
			if(tableLine instanceof NOVCreateSMDLTableLine)
			{
				NOVCreateSMDLTableLine createSMDLLine = (NOVCreateSMDLTableLine)tableLine;
				if(createSMDLLine.getCodeRemovedStatus() == false)
				{
					Object contractDate = createSMDLLine.getProperty(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
					Object plannedDate = createSMDLLine.getProperty(NOV4ComponentSMDL.DOC_PLANNED_DATE);
					if( contractDate== null || plannedDate == null)
					{
						bIsValidDate = false;
						break;
					}
				}
			}
		}
		if(bIsValidDate == false)
		{
			int iContractDateCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
			String sContractDate = m_smdlTable.getColumnName(iContractDateCol);
			
			int iPlannedDateCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOC_PLANNED_DATE);
			String sPlannedDate = m_smdlTable.getColumnName(iPlannedDateCol);
			
			invalidInputs.append(sContractDate);
			invalidInputs.append(" OR ");
			invalidInputs.append(sPlannedDate);
		}
		return invalidInputs.toString();
	}
	public void populateSMDLTableWithSMDLRevInfo(TCComponentItemRevision smdlRev)
	{
		
		m_smdlTable.removeAllRows();
		//TCDECREL-1551 set the selected user in table
		NOVSMDLExplorerViewHelper.setDocResponsible(m_sUser);
		//m_smdlTable.setDocResponsible(m_sUser);
		m_smdlSOAObject = NOVSMDLObjectSOAHelper.getSMDLRevisionProps(smdlRev);
		
		try
		{
			TCComponent codeFormObject =null;
			TCComponent[] codeFormObjs = m_smdlSOAObject.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();			
			TableUtils.populateTable(codeFormObjs, m_smdlTable/*propertiesList*/);		
			
			TableModel  smdlTableModal= m_smdlTable.getModel();
			int row_count = m_smdlTable.getRowCount();
			int smdlNOVCodeColumn = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.SMDL_CODE_NUMBER);
			int contractDateColumn = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
			int plannedDateColumn = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOC_PLANNED_DATE);
			//TCDECREL-1551
			int docResponsibleColumn= m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);
			
			for(int inx=0; inx < row_count; inx++)
			{
				codeFormObject = codeFormObjs[inx];
				String nov_code = ( m_smdlTable.getValueAtDataModel(inx, smdlNOVCodeColumn)).toString();
				Date contractDate = getSaveAsSMDLDocContractDate(nov_code);
				Date plannedDate = getSaveAsSMDLDocPlannedDate(codeFormObject);
				smdlTableModal.setValueAt(contractDate, inx, contractDateColumn);
				smdlTableModal.setValueAt(plannedDate, inx, plannedDateColumn);
				//TCDECREL-1551 set the selected user in table modal
				
				if(!m_sUser.equals(""))
				{
					if(smdlTableModal instanceof NOVCreateSMDLTableModel)
					{
						AIFTableLine tableLine = m_smdlTable.getRowLine(inx);
						tableLine.setProperty(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE, m_sUser);
					}
					smdlTableModal.setValueAt(m_sUser, inx, docResponsibleColumn);
				}
				
			}
			
			NOVSMDLTableHelper.sortTableRowsInAscendingOrder(m_smdlTable, NOV4ComponentSMDL.SMDL_CODE_NUMBER);
		} 
		catch (TCException tcexception) 
		{
			tcexception.printStackTrace();
		}
	}
	
	private Date getSaveAsSMDLDocPlannedDate(TCComponent codeFormObject)
	{
		Date plannedDate=null;
		try 
		{
			TCComponent smdlDoc = codeFormObject.getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
			TCComponent smdlDocRev = null;
			if(smdlDoc != null)
			{
				smdlDocRev = codeFormObject.getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if(smdlDocRev == null)
				{
					TCComponent[] doc_Rev_list = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					smdlDocRev = doc_Rev_list[doc_Rev_list.length -1];
				}
				TCComponent[] statusList = smdlDocRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
				if(statusList.length > 0)
				{
					Calendar currentDate = Calendar.getInstance();
					plannedDate = currentDate.getTime();
				}
				
			}
		} 
		catch (TCException tcException) 
		{
			tcException.printStackTrace();
		}
	
		return plannedDate;
	}
	private Date getSaveAsSMDLDocContractDate(String sNovCode)
    {
    	Date dContractDate = null;
		int iNoOfWeeks = 0;
		if ( sNovCode != null && sNovCode.length() > 0 )
    	{
    		m_novCodeIssueDate = NOVCodeHelper.getCodeIssueDate();
    		String sCodeIssueDate = m_novCodeIssueDate.get(sNovCode);
    		if (sCodeIssueDate  != null )
    		{
    			iNoOfWeeks = NOVCreateSMDLHelper.getNoOfWeeks(sCodeIssueDate);
    			
    			dContractDate = NOVCreateSMDLHelper.getContractAwardDate(m_dpComp,sCodeIssueDate,iNoOfWeeks);
    		}
    	}
    	return dContractDate;
    }
	
	public TCComponent[] getDocumentRevisions()
	{
		return m_uniqueDocsAttachedToSMDLRev.toArray(new TCComponent[m_uniqueDocsAttachedToSMDLRev.size()]);
	}
	//TCDECREL-1551
	public void populateDocResponsibleInfo(String docResp) {
		
		m_sUser=docResp;
	}



}