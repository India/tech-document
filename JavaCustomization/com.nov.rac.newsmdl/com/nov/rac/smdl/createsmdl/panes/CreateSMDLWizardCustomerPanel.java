package com.nov.rac.smdl.createsmdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.teamcenter.rac.util.Registry;



public class CreateSMDLWizardCustomerPanel extends Composite 
{
	private Registry m_appReg = null;
		
	private Composite m_CustPanel;
			
	//private Label m_customerOrderLabel;
	//private Label m_purchaseOrderLabel;
	//private Label m_customerLabel;
	private Label m_productLabel;
	private Label m_salesOrderNoLabel;
	private Label m_docControlJobNoLabel;
	//private Text m_customerOrderText;
	//private Text m_purchaseOrderText;
	//private Text m_customerText;
	private Text m_productText;
	private Text m_salesOrderNoText;
	private Text m_docControlJobNoText;
	
				
	public CreateSMDLWizardCustomerPanel(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
				
		createUI(this);
		setLabels();
		setEditability();
	}
	private void createUI(Composite parent)
	{
		m_CustPanel = new Composite(parent,SWT.NONE);
		
		GridLayout gridLayout = new GridLayout(2, false);
		m_CustPanel.setLayout(gridLayout);
		m_CustPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		//m_customerOrderLabel = new Label(m_CustPanel, SWT.NONE);
		//m_customerOrderLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		m_productLabel = new Label(m_CustPanel, SWT.NONE);
		m_productLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
/*		m_customerOrderText = new Text(m_CustPanel, SWT.BORDER);
		GridData gd_m_customerOrderText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_customerOrderText.setLayoutData(gd_m_customerOrderText);*/
		m_productText = new Text(m_CustPanel, SWT.BORDER);
		GridData gd_m_productText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_productText.setLayoutData(gd_m_productText);
		
/*		m_purchaseOrderLabel = new Label(m_CustPanel, SWT.NONE);
		GridData gd_m_purchaseOrderLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_purchaseOrderLabel.setLayoutData(gd_m_purchaseOrderLabel);*/
		m_salesOrderNoLabel = new Label(m_CustPanel, SWT.NONE);
		GridData gd_m_salesOrderNoLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_salesOrderNoLabel.setLayoutData(gd_m_salesOrderNoLabel);
		
/*		m_purchaseOrderText = new Text(m_CustPanel, SWT.BORDER);
		GridData gd_m_purchaseOrderText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_purchaseOrderText.setLayoutData(gd_m_purchaseOrderText);*/
		m_salesOrderNoText = new Text(m_CustPanel, SWT.BORDER);
		GridData gd_m_salesOrderNoText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_salesOrderNoText.setLayoutData(gd_m_salesOrderNoText);
		
/*		m_customerLabel = new Label(m_CustPanel, SWT.NONE);
		GridData gd_m_customerLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_customerLabel.setLayoutData(gd_m_customerLabel);*/
		m_docControlJobNoLabel = new Label(m_CustPanel, SWT.NONE);
		GridData gd_m_docControlJobNoLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_docControlJobNoLabel.setLayoutData(gd_m_docControlJobNoLabel);
		
/*		m_customerText = new Text(m_CustPanel, SWT.BORDER);
		GridData gd_m_customerText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_customerText.setLayoutData(gd_m_customerText);*/
		m_docControlJobNoText = new Text(m_CustPanel, SWT.BORDER);
		GridData gd_m_docControlJobNoText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_docControlJobNoText.setLayoutData(gd_m_docControlJobNoText);
	}
	private void setLabels()
	{
/*		m_customerOrderLabel.setText(m_appReg.getString("CustOrder.LABEL"));
		m_purchaseOrderLabel.setText(m_appReg.getString("PurchOrder.LABEL"));
		m_customerLabel.setText(m_appReg.getString("Customer.LABEL"));*/
		m_productLabel.setText(m_appReg.getString("ProductModel.LABEL"));
		m_salesOrderNoLabel.setText(m_appReg.getString("SalesOrderNo.Label"));
		m_docControlJobNoLabel.setText(m_appReg.getString("DocControlNo.Label"));
	}
	private void setEditability()
	{
/*		m_customerOrderText.setEditable(false);
		m_purchaseOrderText.setEditable(false);
		m_customerText.setEditable(false);*/
		m_productText.setEditable(false);
		m_salesOrderNoText.setEditable(false);
		m_docControlJobNoText.setEditable(false);
	}
	public void populatePanel(String sProduct,String sSalesOrderNo, String sDocControlJobNo)
	{		
/*		if(sCustOrder != null)
		{
			m_customerOrderText.setText(sCustOrder);
		}
		if(sPurchaseOrder != null)
		{
			m_purchaseOrderText.setText(sPurchaseOrder);
		}
		if(sCustomer != null)
		{
			m_customerText.setText(sCustomer);
		}*/
		if(sProduct != null)
		{
			m_productText.setText(sProduct);
		}
		if(sSalesOrderNo != null)
		{
			m_salesOrderNoText.setText(sSalesOrderNo);
		}
		if(sDocControlJobNo != null)
		{
			m_docControlJobNoText.setText(sDocControlJobNo);
		}
	}
}