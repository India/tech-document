package com.nov.rac.smdl.createsmdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.noi.util.components.JIBPlantLOVList;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class CreateSMDLWizardTopPanel extends Composite 
{
	private Registry m_appReg = null;
	private TCSession m_session;
			
	private CreateSMDLWizardNamePanel m_namePanel;
	private CreateSMDLType_OrderIdPanel m_smdlTypeOrderIdPanel;
	private CreateSMDLWizardCustomerPanel m_custPanel;
	private CreateSMDLWizardProjectManagerPanel m_prjctManagerPanel;
	private String m_sOrderId = null;
	private String m_sOrderType = null;
	private String m_sCustOrder = null;
	private String m_sPurchaseOrder = null;
	private String m_sCustomer = null;
	private String m_sProjectManager = null;
	private String m_sDocController = null;
	private String m_sProductModel = null;
	private String m_sSMDLName = null;
	private String m_plantIdStr = null;
	private String m_sEnggJobNo = null;
	private String m_sSalesOrderNo = null;
	private String m_sDocControlJobNo = null;
	private TCComponent m_dpComp;
	private static JIBPlantLOVList m_soaPlantList = null;
	
	public CreateSMDLWizardTopPanel(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(4, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
		m_session=(TCSession)AIFUtility.getDefaultSession();
		createUI(this);
	}
	private void createUI(Composite parent)
	{
		m_namePanel = new CreateSMDLWizardNamePanel(parent,SWT.NONE);
		m_smdlTypeOrderIdPanel = new CreateSMDLType_OrderIdPanel(parent,SWT.NONE);
		m_custPanel = new CreateSMDLWizardCustomerPanel(parent,SWT.NONE);
		m_prjctManagerPanel = new CreateSMDLWizardProjectManagerPanel(parent,SWT.NONE);
	}
	public void populateTopPanel(String sSMDLType, String sSMDLDesc)
	{		
		populateLOVs();
		if(m_dpComp!=null)
		{
			try
			{
				TCComponent orderComp = null;
				orderComp = m_dpComp.getTCProperty(NOV4ComponentDP.DP_ORDER_RELATION).getReferenceValue();
				if(orderComp != null)
				{
					m_sOrderId = orderComp.getTCProperty(NOV4ComponentOrder.ORDER_NUMBER).getStringValue();
					m_sOrderType = orderComp.getTCProperty(NOV4ComponentOrder.ORDER_TYPE).getStringValue();
					m_sCustOrder = orderComp.getTCProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER).getStringValue();
					m_sPurchaseOrder = orderComp.getTCProperty(NOV4ComponentOrder.PURCHASE_ORDER_NUMBER).getStringValue();
					m_sCustomer = orderComp.getTCProperty(NOV4ComponentOrder.CUSTOMER).getStringValue();
					m_sProjectManager = orderComp.getTCProperty(NOV4ComponentOrder.PROJECT_MANAGER).getStringValue();
					m_sDocController = orderComp.getTCProperty(NOV4ComponentOrder.DOCUMENT_CONTROLLER).getStringValue();
					m_sSalesOrderNo = orderComp.getTCProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER).getStringValue();
					m_sDocControlJobNo = orderComp.getTCProperty(NOV4ComponentOrder.DOCUMENT_CONTROL).getStringValue();
					int plantIdInt = orderComp.getTCProperty(NOV4ComponentOrder.PLANT_ID_OTHER).getIntValue();
					m_plantIdStr = m_soaPlantList.getStringFromReference(new Integer(plantIdInt));
				}
				String sBasedOnProduct = m_dpComp.getTCProperty(NOV4ComponentDP.DP_BASED_ON_PROD).getStringValue();
				if(sBasedOnProduct != null)
				{
					//StandardProduct is combination of Product Model:Name
					m_sProductModel = sBasedOnProduct.split(":")[0];
				}
				m_sSMDLName="";
				if(m_sCustomer != null && !m_sCustomer.isEmpty())
				{
					m_sSMDLName = m_sCustomer;
				}
				if(m_plantIdStr != null)
				{
					m_sSMDLName+=" - "+m_plantIdStr;
				}
				if(m_sProductModel != null)
				{
					m_sSMDLName+=" - "+m_sProductModel;
				}
				else
				{
					m_sProductModel = "";
				}
				String[] strEngJobNumbes = m_dpComp.getTCProperty(NOV4ComponentDP.DP_ENG_JOB_NUMBERS).getStringValueArray();
				m_sEnggJobNo = "";
				for(int iCnt = 0; iCnt < strEngJobNumbes.length; iCnt++){
					if(iCnt != strEngJobNumbes.length -1){
						m_sEnggJobNo = m_sEnggJobNo + strEngJobNumbes[iCnt] + ", ";
					}
					else{
						m_sEnggJobNo = m_sEnggJobNo + strEngJobNumbes[iCnt];
					}
				}
			}
			catch(TCException ex)
			{
				ex.printStackTrace();
			}
		}
		m_namePanel.populatePanel(m_sSMDLName, sSMDLDesc);
		m_smdlTypeOrderIdPanel.populatePanel(sSMDLType,m_sCustomer, m_plantIdStr);
		m_custPanel.populatePanel(m_sProductModel, m_sSalesOrderNo, m_sDocControlJobNo);
		m_prjctManagerPanel.populatePanel(/*m_sProjectManager, m_sDocController, */m_sEnggJobNo);
	}
	public void populateCreateInObject(CreateInObjectHelper transferObject)
	{
		m_namePanel.populateCreateInObject(transferObject);
		m_smdlTypeOrderIdPanel.populateCreateInObject(transferObject);
	}
	public void setDPObject(TCComponent dpObj)
	{
		m_dpComp = dpObj;
	}
	private void populateLOVs() 
	{
		if (m_soaPlantList == null) 
		{
			m_soaPlantList = new JIBPlantLOVList();
			m_soaPlantList.init();
		}
	}
	public String validateMandatoryFields()
	{
		return m_smdlTypeOrderIdPanel.validateMandatoryFields();
	}
}