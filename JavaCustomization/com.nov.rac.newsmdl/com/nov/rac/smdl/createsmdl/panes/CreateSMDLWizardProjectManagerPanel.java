package com.nov.rac.smdl.createsmdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;



public class CreateSMDLWizardProjectManagerPanel extends Composite 
{
	private Registry m_appReg = null;
	private TCSession m_session;
	
	private Composite m_PrjctMgrPanel;
			
	//private Label m_prjctMgrLabel;
	//private Label m_docControllerLabel;
	private Label m_engJobNoLabel;
		
	//private Text m_prjctMgrText;
	//private Text m_docControllerText;
	private Text m_engJobNoText;
				
	public CreateSMDLWizardProjectManagerPanel(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
		m_session=(TCSession)AIFUtility.getDefaultSession();
		
		createUI(this);
		setLabels();
		setEditability();
	}
	private void createUI(Composite parent)
	{
		m_PrjctMgrPanel = new Composite(parent,SWT.NONE);
		
		GridLayout gridLayout = new GridLayout(2, false);
		m_PrjctMgrPanel.setLayout(gridLayout);
		m_PrjctMgrPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
/*		m_prjctMgrLabel = new Label(m_PrjctMgrPanel, SWT.NONE);
		m_prjctMgrLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_prjctMgrText = new Text(m_PrjctMgrPanel, SWT.BORDER);
		GridData gd_m_prjctMgrText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_prjctMgrText.setLayoutData(gd_m_prjctMgrText);
		
		m_docControllerLabel = new Label(m_PrjctMgrPanel, SWT.NONE);
		GridData gd_m_docControllerLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_docControllerLabel.setLayoutData(gd_m_docControllerLabel);
		
		m_docControllerText = new Text(m_PrjctMgrPanel, SWT.BORDER);
		GridData gd_m_docControllerText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_docControllerText.setLayoutData(gd_m_docControllerText);
*/		
		m_engJobNoLabel = new Label(m_PrjctMgrPanel, SWT.NONE);
		GridData gd_m_engJobNoLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_engJobNoLabel.setLayoutData(gd_m_engJobNoLabel);
		
		m_engJobNoText = new Text(m_PrjctMgrPanel, SWT.BORDER | SWT.MULTI | SWT.WRAP);
		GridData gd_m_engJobNoText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_m_engJobNoText.heightHint = m_engJobNoText.getLineHeight() * 3;
		m_engJobNoText.setLayoutData(gd_m_engJobNoText);
	}
	private void setLabels()
	{
/*		m_prjctMgrLabel.setText(m_appReg.getString("PrjctMgr.LABEL"));
		m_docControllerLabel.setText(m_appReg.getString("DocController.LABEL"));
*/		m_engJobNoLabel.setText(m_appReg.getString("EnggJobNo.Label"));
	}
	private void setEditability()
	{
/*		m_prjctMgrText.setEditable(false);
		m_docControllerText.setEditable(false);
*/		m_engJobNoText.setEditable(false);
	}
	public void populatePanel(/*String sPrjctMgr,String sDocController,*/ String sEngJobNo)
	{		
/*		if(sPrjctMgr != null)
		{
			m_prjctMgrText.setText(sPrjctMgr);
		}
		if(sDocController != null)
		{
			m_docControllerText.setText(sDocController);
		}*/
		if(sEngJobNo != null)
		{
			m_engJobNoText.setText(sEngJobNo);
		}
	}
}