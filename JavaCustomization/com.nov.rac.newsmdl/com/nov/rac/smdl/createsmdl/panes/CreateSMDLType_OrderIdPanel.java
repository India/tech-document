package com.nov.rac.smdl.createsmdl.panes;

import java.util.Vector;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;



public class CreateSMDLType_OrderIdPanel extends Composite 
{
	private Registry m_appReg = null;
	private TCSession m_session;
	
	private Composite m_smdlTypeOrderIdPanel;
			
	private Label m_smdlTypeLabel;
	//private Label m_orderIdLabel;
	//private Label m_orderTypeLabel;
	private Label m_customerLabel;
	private Label m_plantLabel;
		
	//private Text m_orderIdText;
	//private Text m_orderTypeText;
	private Text m_customerText;
	private Text m_plantText;
	private Combo m_smdlTypeCombo;
				
	public CreateSMDLType_OrderIdPanel(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
		m_session=(TCSession)AIFUtility.getDefaultSession();
		
		createUI(this);
		setLabels();
		setEditability();
		markMandatoryFields();
	}
	private void createUI(Composite parent)
	{
		m_smdlTypeOrderIdPanel = new Composite(parent,SWT.NONE);
		
		GridLayout gridLayout = new GridLayout(2, false);
		m_smdlTypeOrderIdPanel.setLayout(gridLayout);
		m_smdlTypeOrderIdPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
	    		
		m_smdlTypeLabel = new Label(m_smdlTypeOrderIdPanel, SWT.NONE);
		GridData gd_m_smdlTypeLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_smdlTypeLabel.setLayoutData(gd_m_smdlTypeLabel);
		
		///TCDECREL-2377
		//m_smdlTypeCombo = new Combo(m_smdlTypeOrderIdPanel, SWT.DROP_DOWN | SWT.BORDER);
		m_smdlTypeCombo = new Combo(m_smdlTypeOrderIdPanel, SWT.DROP_DOWN | SWT.READ_ONLY);///TCDECREL-2377
		GridData gd_m_smdlTypeCombo = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_smdlTypeCombo.setLayoutData(gd_m_smdlTypeCombo);
				
		//populateLOVs();
		
		//m_orderIdLabel = new Label(m_smdlTypeOrderIdPanel, SWT.NONE);
		//GridData gd_m_orderIdLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		//m_orderIdLabel.setLayoutData(gd_m_orderIdLabel);
		m_customerLabel = new Label(m_smdlTypeOrderIdPanel, SWT.NONE);
		GridData gd_m_customerLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_customerLabel.setLayoutData(gd_m_customerLabel);
		
/*		m_orderIdText = new Text(m_smdlTypeOrderIdPanel, SWT.BORDER);
		GridData gd_m_orderIdText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_orderIdText.setLayoutData(gd_m_orderIdText);*/
		m_customerText = new Text(m_smdlTypeOrderIdPanel, SWT.BORDER);
		GridData gd_m_customerText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_customerText.setLayoutData(gd_m_customerText);
				
/*		m_orderTypeLabel = new Label(m_smdlTypeOrderIdPanel, SWT.NONE);
		GridData gd_m_orderTypeLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_orderTypeLabel.setLayoutData(gd_m_orderTypeLabel);*/		
		m_plantLabel = new Label(m_smdlTypeOrderIdPanel, SWT.NONE);
		GridData gd_m_plantLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_plantLabel.setLayoutData(gd_m_plantLabel);
		
/*		m_orderTypeText = new Text(m_smdlTypeOrderIdPanel, SWT.BORDER);
		GridData gd_m_orderTypeText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_orderTypeText.setLayoutData(gd_m_orderTypeText);*/
		m_plantText = new Text(m_smdlTypeOrderIdPanel, SWT.BORDER);
		GridData gd_m_plantText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_plantText.setLayoutData(gd_m_plantText);
		
	}
	private void setLabels()
	{
		m_smdlTypeLabel.setText(m_appReg.getString("SMDLType.LABEL"));
		//m_orderIdLabel.setText(m_appReg.getString("OrderId.LABEL"));
		//m_orderTypeLabel.setText(m_appReg.getString("OrderType.LABEL"));
		m_customerLabel.setText(m_appReg.getString("Customer.LABEL"));
		m_plantLabel.setText(m_appReg.getString("Plant.Label"));		
	}
	private void setEditability()
	{
		//m_orderIdText.setEditable(false);
		//m_orderTypeText.setEditable(false);
		m_customerText.setEditable(false);
		m_plantText.setEditable(false);
	}
	public void markMandatoryFields()
	{
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry.getDefault(). getFieldDecoration("NOV_DEC_REQUIRED");
		
		ControlDecoration decIDText = new ControlDecoration(m_smdlTypeCombo, SWT.TOP | SWT.LEFT);		
		decIDText.setImage(reqFieldIndicator.getImage());
		decIDText.setDescriptionText(reqFieldIndicator.getDescription());		
	}
	private void populateLOVs() 
	{
		String[] smdlTypeValues = NOVCreateSMDLHelper.getSMDLTypes();
		NOVSMDLComboUtils.setAutoComboArray(m_smdlTypeCombo, smdlTypeValues);
	}
	public void populatePanel(String sSMDLType,String sCustomer, String sPlant)
	{		
		populateLOVs();
		
		if(sSMDLType != null)
		{
			int nIndex = m_smdlTypeCombo.indexOf(sSMDLType);
			if(nIndex > -1) 
			{
				m_smdlTypeCombo.select(nIndex);
			}
		}
/*		if(sOrderId != null)
		{
			m_orderIdText.setText(sOrderId);
		}
		if(sOrderType != null)
		{
			m_orderTypeText.setText(sOrderType);
		}*/
		if(sCustomer != null)
		{
			m_customerText.setText(sCustomer);
		}
		if(sPlant != null)
		{
			m_plantText.setText(sPlant);
		}
	}
	public void populateCreateInObject(CreateInObjectHelper transferObject)
	{
		int iSelectedIndex = m_smdlTypeCombo.getSelectionIndex();
		String smdType="";
		if(iSelectedIndex != -1)
		{
			smdType = m_smdlTypeCombo.getItem(iSelectedIndex);
		}
		//TCDECREL-2377
//		else
//		{
//			smdType = m_smdlTypeCombo.getText();
//		}//TCDECREL-2377
		transferObject.setStringProps(NOV4ComponentSMDL.SMDL_TYPE,smdType,CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.SMDL_NAME,smdType,CreateInObjectHelper.SET_PROPERTY);
	}
	public String validateMandatoryFields()
	{
		StringBuffer  invalidInputs= new StringBuffer();
		int iTypeIdex =m_smdlTypeCombo.getSelectionIndex();
		if(iTypeIdex==-1 && m_smdlTypeCombo.getText().isEmpty())
		{
			invalidInputs.append(m_appReg.getString("SMDL_Type.NAME"));
		}
		return invalidInputs.toString();
	}
}