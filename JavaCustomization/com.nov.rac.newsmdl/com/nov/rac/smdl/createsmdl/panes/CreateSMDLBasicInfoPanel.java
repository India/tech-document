package com.nov.rac.smdl.createsmdl.panes;

import java.util.Collections;
import java.util.Vector;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.smdl.common.NOVAddTextFieldVerifyListener;
import com.nov.rac.smdl.common.NOVListPopUpDialog;
import com.nov.rac.smdl.createsmdl.listener.NOVSMDLSaveAsSearchButtonListener;
import com.nov.rac.smdl.createsmdl.listener.NOVSMDLSaveAsTextFieldListener;
import com.nov.rac.smdl.createsmdl.listener.NOVSaveAsSMDLRadioButtonListener;
import com.nov.rac.smdl.createsmdl.listener.NOVSelectTemplateComboListener;
import com.nov.rac.smdl.createsmdl.listener.NOVSelectTemplateRadioButtonListener;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.nov.rac.smdl.utilities.NOV4TemplateHelper;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.smdl.utilities.NOVUserDataHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.NameIconIdStruct;
import com.teamcenter.rac.kernel.TCComponentUserType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ArraySorter;
import com.teamcenter.rac.util.Registry;

public class CreateSMDLBasicInfoPanel extends Composite implements INOVSearchProvider2
{
	private Vector<String> m_templateVec = new Vector<String>();
	
	private TCSession m_session;
	
	private Composite m_basicInfoPanel;
		
	private Label m_smdlTypeLabel;
	private Label m_smdlDescLabel;
		
	private Button m_selectTempRadio;
	private Button m_saveAsSMDLRadio;
	
	private Combo m_smdlTypeCombo;
	private Combo m_selectTempCombo;
	
	private Text m_smdlDescTextArea;
	private Text m_saveAsSMDLText;
	
	private Button m_SearchButton;
	
	private Registry m_appReg = null;
	private int m_iDefaultSelectedTempIndex = -1;
	private NovSearchDataModel m_novSearchDataModel = null;
	private String m_strSMDLID;
	//TCDECREL-1551
	private Label m_docResponsibleLabel;
	private Label m_docResponsibleText;
	public Text     cResponsible; 
    public Button   btnResponsible     ;
    
	public CreateSMDLBasicInfoPanel(Composite parent, int style) 
	{
		super(parent, style);
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
		m_session=(TCSession)AIFUtility.getDefaultSession();
		createUI(this);
	
	}

	private void createUI(Composite parent) {
		
		m_basicInfoPanel = parent;
		
		GC gc = new GC(m_basicInfoPanel);
		
        FontMetrics fontMetrics = gc.getFontMetrics();
        gc.dispose();
		
		GridLayout gridLayout = new GridLayout(3, false);
		//gridLayout.marginTop = Dialog.convertHorizontalDLUsToPixels(fontMetrics, 30);
		m_basicInfoPanel.setLayout(gridLayout);
		m_basicInfoPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
					
		m_smdlTypeLabel = new Label(m_basicInfoPanel, SWT.NONE);
		///TCDECREL-2377		
		//m_smdlTypeCombo = new Combo(m_basicInfoPanel, SWT.DROP_DOWN | SWT.BORDER);
		m_smdlTypeCombo = new Combo(m_basicInfoPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
		m_smdlTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
				
		populateLOVs();
		
		Label dummyLabel = new Label(m_basicInfoPanel, SWT.NONE);
		dummyLabel.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		
		m_smdlDescLabel = new Label(m_basicInfoPanel, SWT.NONE);
		m_smdlDescLabel.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
			
		m_smdlDescTextArea = new Text(m_basicInfoPanel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		//m_smdlDescTextArea.setTextLimit(256);
		//TCDECREL-2509
		m_smdlDescTextArea.setTextLimit(240);//TCDECREL-2509
		GridData gd_dpDescTextArea = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_dpDescTextArea.heightHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_smdlDescTextArea,40);
		m_smdlDescTextArea.setLayoutData( gd_dpDescTextArea);
		
		Label dummyLabel1 = new Label(m_basicInfoPanel, SWT.NONE);
		dummyLabel1.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		
		m_selectTempRadio = new Button(m_basicInfoPanel, SWT.RADIO);
		GridData gd_selectTemp = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_selectTemp.verticalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(m_smdlDescTextArea,25);
		m_selectTempRadio.setLayoutData(gd_selectTemp);
		
		m_selectTempCombo = new Combo(m_basicInfoPanel, SWT.DROP_DOWN | SWT.BORDER);
		m_selectTempCombo.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 1, 1));
		
		loadCodeTemplate();
		
		Label dummyLabel2 = new Label(m_basicInfoPanel, SWT.NONE);
		dummyLabel2.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		
		m_saveAsSMDLRadio = new Button(m_basicInfoPanel, SWT.RADIO);
		
		m_saveAsSMDLText = new Text(m_basicInfoPanel, SWT.BORDER);
		m_saveAsSMDLText.setTextLimit(32);
		m_saveAsSMDLText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));	
		
		m_SearchButton = new Button(m_basicInfoPanel, SWT.NONE);
		m_SearchButton.setImage(m_appReg.getImage("search.ICON"));
					
		createSearchDataInput();
		
		//TCDECREL1551-NEW UI

		Label separator = new Label(m_basicInfoPanel, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false,3,1));
		
		Composite m_optionPanel = new Composite(m_basicInfoPanel, SWT.NONE);
		GridData panelGrid = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		m_optionPanel.setLayoutData(panelGrid);
		GridLayout grid = new GridLayout(3,false);
		m_optionPanel.setLayout(grid);
		GridData gd_text = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_docResponsibleLabel = new Label(m_optionPanel,SWT.NONE);	
		cResponsible = new Text(m_optionPanel, SWT.BORDER);
		cResponsible.setLayoutData(gd_text);
		
		String[] docValues = populateDocumentResponsibleList();
        new AutoCompleteField(cResponsible, new TextContentAdapter(),docValues);
	
		btnResponsible = new Button(m_optionPanel, SWT.NONE);
		btnResponsible.setImage(m_appReg.getImage("DocResponsible.ICON"));
		

		GridData defaultTextGridData = new GridData(SWT.FILL, SWT.BOTTOM, true, false, 2, 1);
		m_docResponsibleText = new Label(m_optionPanel,SWT.NONE);
		m_docResponsibleText.setLayoutData(defaultTextGridData);

		Color textColor=m_basicInfoPanel.getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY);
		m_docResponsibleText.setForeground(textColor);
		
		FontData fontData = m_docResponsibleText.getFont().getFontData()[0];
		Font font = new Font(m_basicInfoPanel.getShell().getDisplay(), new FontData(fontData.getName(),fontData.getHeight(), SWT.ITALIC));
		
		m_docResponsibleText.setFont(font);
			
		setLabels();
		addListeners();
		markMandatoryFields();
		
		setDefaultValues();

		
	}
	

	private void createSearchDataInput()
	{
		m_novSearchDataModel = new NovSearchDataModel();
	}
	private void addListeners()
	{
		// TODO : Kishor Ahuja - Add Verify Listener directly.
		m_smdlDescTextArea.addVerifyListener(new NOVAddTextFieldVerifyListener());
		m_SearchButton.addSelectionListener(new NOVSMDLSaveAsSearchButtonListener(this,m_novSearchDataModel));		
		m_selectTempRadio.addSelectionListener(new NOVSelectTemplateRadioButtonListener());
		m_saveAsSMDLRadio.addSelectionListener(new NOVSaveAsSMDLRadioButtonListener());
		m_selectTempCombo.addSelectionListener(new NOVSelectTemplateComboListener());
		m_saveAsSMDLText.addKeyListener(new NOVSMDLSaveAsTextFieldListener());
		btnResponsible.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				
				Shell shell = getParent().getShell();
		        openDialogs(shell,selectionevent);
				}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent selectionevent) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	private void setLabels()
	{
		//TCDECREL-1551
		m_docResponsibleLabel.setText(m_appReg.getString("docResponsible.Label"));
		m_docResponsibleText.setText("\tAutomatically defaults selected user to Doc Responsible");
		m_smdlTypeLabel.setText(m_appReg.getString("SMDL_Type.LABEL"));
		m_smdlDescLabel.setText(m_appReg.getString("Description.LABEL"));
		m_selectTempRadio.setText(m_appReg.getString("SelectTemplate.LABEL"));
		m_saveAsSMDLRadio.setText(m_appReg.getString("SaveAsSMDL.LABEL"));
	}
	public void markMandatoryFields()
	{
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry.getDefault(). getFieldDecoration("NOV_DEC_REQUIRED");
		
		ControlDecoration decIDText = new ControlDecoration(m_smdlTypeCombo, SWT.TOP | SWT.LEFT);	
		
		decIDText.setImage(reqFieldIndicator.getImage());
		decIDText.setDescriptionText(reqFieldIndicator.getDescription());		
	}
	private void populateLOVs() 
	{
		String[] smdlTypeValues = NOVCreateSMDLHelper.getSMDLTypes();
		NOVSMDLComboUtils.setAutoComboArray(m_smdlTypeCombo, smdlTypeValues);
	}
	private void loadCodeTemplate()
    {
        m_templateVec.addAll(NOV4TemplateHelper.getTemplateNames());
        Collections.sort(m_templateVec);
        int iTotalTemplates = m_templateVec.size();
        for(int index=0;index<iTotalTemplates;index++)
		{
			if(m_templateVec.get(index).equalsIgnoreCase("New"))
			{
				m_iDefaultSelectedTempIndex = index;
				break;
			}
		}
        if ( m_iDefaultSelectedTempIndex == -1 ) 
        {
        	m_iDefaultSelectedTempIndex = 0;
		}
        
        NOVSMDLComboUtils.setAutoComboVector(m_selectTempCombo, m_templateVec);
		m_selectTempCombo.select(m_iDefaultSelectedTempIndex);
    }
	public String getSelectTempComboValue()
	{
		String sSelectTempVal = null;
		sSelectTempVal = m_selectTempCombo.getText();
		return sSelectTempVal;
	}
	public String getSMDLType()
	{
		String sSMDLType = null;
		int iSelectedIndex = m_smdlTypeCombo.getSelectionIndex();
		if(iSelectedIndex != -1)
		{
			sSMDLType = m_smdlTypeCombo.getItem(iSelectedIndex);
		}
		//TCDECREL-2377
//		else
//		{
//			sSMDLType = m_smdlTypeCombo.getText();
//		}//TCDECREL-2377
		return sSMDLType;
	}
	public String getSMDLDesc()
	{
		String sSMDLDesc = null;
		sSMDLDesc = m_smdlDescTextArea.getText();
		return sSMDLDesc;
	}
	public String validateMandatoryFields()
	{
		StringBuffer  invalidInputs= new StringBuffer();
		int iTypeIdex =m_smdlTypeCombo.getSelectionIndex();
		if(iTypeIdex==-1 && m_smdlTypeCombo.getText().isEmpty())
		
		{
			invalidInputs.append(m_appReg.getString("SMDL_Type.NAME"));
		}		
		if(m_selectTempRadio.getSelection())
		{
			int index =m_selectTempCombo.getSelectionIndex();
			if (index==-1 && m_selectTempCombo.getText().isEmpty())
			{
				if(invalidInputs.length()!=0)
				{
					invalidInputs.append(",");
				}
				invalidInputs.append(m_appReg.getString("SelectTemplate.NAME"));
			}
		}
		else
		{
			if(m_saveAsSMDLText.getText().isEmpty())
			{
				if(invalidInputs.length()!=0)
				{
					invalidInputs.append(",");
				}
				invalidInputs.append(m_appReg.getString("SaveAsSMDL.NAME"));
			}
		}
		return invalidInputs.toString();
	}

	public boolean validateInput()
	{
		boolean retValue = true;
		m_strSMDLID = m_saveAsSMDLText.getText().trim();
		if(m_strSMDLID.isEmpty())
		{
			m_strSMDLID = "*";
		}
		
		return retValue;		
	}

	public QuickBindVariable[] getBindVariables()
	{				
		String itemType = getBusinessObject();
			
		QuickSearchService.QuickBindVariable [] allBindVars = new QuickSearchService.QuickBindVariable[10];		
		
		// Item.object_type
		allBindVars[0] = new QuickSearchService.QuickBindVariable();
		allBindVars[0].nVarType = POM_string;
		allBindVars[0].nVarSize = 1;
		allBindVars[0].strList = new String[] { itemType };
		
		// Order.item_id
		allBindVars[1] = new QuickSearchService.QuickBindVariable();
		allBindVars[1].nVarType = POM_string;
		allBindVars[1].nVarSize = 1;
		allBindVars[1].strList = new String[]{ "*" };
		
		// DP.item_id
		allBindVars[2] = new QuickSearchService.QuickBindVariable();
		allBindVars[2].nVarType = POM_string;
		allBindVars[2].nVarSize = 1;
		allBindVars[2].strList = new String[]{ "*" };
		
		// SMDL.item_id
		allBindVars[3] = new QuickSearchService.QuickBindVariable();
		allBindVars[3].nVarType = POM_string;
		allBindVars[3].nVarSize = 1;
		allBindVars[3].strList = new String[]{ m_strSMDLID };
		
		// Order.nov4_sales_order_number
		allBindVars[4] = new QuickSearchService.QuickBindVariable();
		allBindVars[4].nVarType = POM_string;
		allBindVars[4].nVarSize = 1;
		allBindVars[4].strList = new String[]{ "*" };

		// DP.nov4_eng_job_numbers
		allBindVars[5] = new QuickSearchService.QuickBindVariable();
		allBindVars[5].nVarType = POM_string;
		allBindVars[5].nVarSize = 1;
		allBindVars[5].strList = new String[]{ "*" };
		
		// Order.nov4_customer
		allBindVars[6] = new QuickSearchService.QuickBindVariable();
		allBindVars[6].nVarType = POM_string;
		allBindVars[6].nVarSize = 1;
		allBindVars[6].strList = new String[]{ "*" };
		
		// Order.nov4_plant_id_other
		allBindVars[7] = new QuickSearchService.QuickBindVariable();
		allBindVars[7].nVarType = POM_string;
		allBindVars[7].nVarSize = 1;
		allBindVars[7].strList = new String[]{ "*" };
		
		// DPMaster.nov4_basedon_std_prod
		allBindVars[8] = new QuickSearchService.QuickBindVariable();
		allBindVars[8].nVarType = POM_string;
		allBindVars[8].nVarSize = 1;
		allBindVars[8].strList = new String[]{ "*" };

		// SMDL.smdl_type
		allBindVars[9] = new QuickSearchService.QuickBindVariable();
		allBindVars[9].nVarType = POM_string;
		allBindVars[9].nVarSize = 1;
		allBindVars[9].strList = new String[]{ "*" };
		
        return allBindVars;		
	}

	public String getBusinessObject()
	{
		return "Order";
	}

	public QuickHandlerInfo[] getHandlerInfo()
	{
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[3];
		
		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-orderdpsmdl";
		allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5 };
//		allHandlerInfo[0].listInsertAtIndex = new int[] {   1, 8, 2, 6, 3 };
		allHandlerInfo[0].listInsertAtIndex = new int[] {   1, 9, 2, 7, 3 };

		allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[1].handlerName = "NOVSRCH-get-dp";
		allHandlerInfo[1].listBindIndex = new int[] { 3, 4, 6, 9, 10 };
		allHandlerInfo[1].listReqdColumnIndex = new int[] { 1,  2,  3,  4,  5,  6,  7 };
		allHandlerInfo[1].listInsertAtIndex = new int[] {   10, 11, 6,  8,  12, 14, 13 };		
	
//		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
//		allHandlerInfo[2].handlerName = "NOVSRCH-get-smdl";
//		allHandlerInfo[2].listBindIndex = new int[] { 4, 10 };
//		allHandlerInfo[2].listReqdColumnIndex = new int[] { 1, 2,  3 };
//		allHandlerInfo[2].listInsertAtIndex = new int[] {   12, 14, 13 };
		
		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[2].handlerName = "NOVSRCH-get-dummy";
		allHandlerInfo[2].listBindIndex = new int[0];
		allHandlerInfo[2].listReqdColumnIndex = new int[] { 1, 2 };
		allHandlerInfo[2].listInsertAtIndex = new int[] { 4, 5 };
		
		return allHandlerInfo;
	}
	public Combo getSelectedTemplateCombo()
	{
		return m_selectTempCombo;
	}
	public Button getSelectedTemplateRadio()
	{
		return m_selectTempRadio;
	}
	public Button getSaveAsSMDLRadio()
	{
		return m_saveAsSMDLRadio;
	}
	public Text getSaveAsSMDLText()
	{
		return m_saveAsSMDLText;
	}
	public int getTemplateComboDefaultSelectedIndex()
	{
		return m_iDefaultSelectedTempIndex;
	}
	public String getSaveAsSMDLValue()
	{
		String sSaveAsSMDLVal = null;
		sSaveAsSMDLVal = m_saveAsSMDLText.getText().trim();
		return sSaveAsSMDLVal;
	}
	public boolean bIsSelectTemplateButtonSelected()
	{
		return m_selectTempRadio.getSelection();
	}
	public boolean bIsSaveAsSMDLButtonSelected()
	{
		return m_saveAsSMDLRadio.getSelection();
	}
	public Button getSearchSMDLButton()
	{
		return m_SearchButton;
	}
	//TCDECREL-1551 setting the default values
	private void setDefaultValues()
	{
		String defaultDocResponsibleValue = getDocResponsibleDefaultvalue();
		cResponsible.setText(defaultDocResponsibleValue);
	
	}
	//TCDECREL-1551
	private String getDocResponsibleDefaultvalue() {
		TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
		String loggedInUserName = null;
		
		loggedInUserName = theSession.getUser().toString();
		
		return  loggedInUserName;
	}
	//TCDECREL-1551 populating the doc responsible list
	private String[] populateDocumentResponsibleList() {
		String[] users = null;
		
		Vector<String> userVector = new Vector<String>();
		
		userVector=NOVUserDataHelper.getUpdateUserList();
		users=userVector.toArray(new String[userVector.size()]);
		
		ArraySorter.sort(users);
		//NOVSMDLComboUtils.setAutoComboArray(m_docResponsiblCombo, users);
		return users;
	}
	//TCDECREL-1551 get the doc responsible user
	public  String getDocumentResponsibleUser()
	{		
		String documentResposibleUser =null;
		String docResponsible=cResponsible.getText();
		
		if(docResponsible.equals("") && bIsSelectTemplateButtonSelected())
		{
			documentResposibleUser = getDocResponsibleDefaultvalue(); 
		}
		else
		{
			documentResposibleUser=docResponsible;
		}
	return documentResposibleUser;
	}

	//TCDECREL-1551 Added validation for invalid text entered in doc responsible while SMDL creation
	public String getSelectDocRespComboValue()
	{
		String sSelectDocResponsible = null;
		sSelectDocResponsible = cResponsible.getText();
		
		return sSelectDocResponsible;
	}
	private void openDialogs(final Shell shell, SelectionEvent selectionevent) {
			
		NOVListPopUpDialog popupDialog = new NOVListPopUpDialog(shell);
		
		popupDialog.create();
		popupDialog.setTitle("Users List");
		
		popupDialog.populateList(populateDocumentResponsibleList());
		 Point point = shell.toDisplay(new Point(selectionevent.x, selectionevent.y));
	
		int x = btnResponsible.getLocation().x +point.x;
		int y = btnResponsible.getLocation().y+point.y;
		popupDialog.getShell().setLocation(x, y);;
		popupDialog.open();
		
		String sSelectedValue = popupDialog.getSelectedValue();
		if(sSelectedValue !=null)
		{
			cResponsible.setText(sSelectedValue);
		}
			

	  }
	
}