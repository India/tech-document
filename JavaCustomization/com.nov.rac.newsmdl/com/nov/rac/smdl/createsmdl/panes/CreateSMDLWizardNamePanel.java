package com.nov.rac.smdl.createsmdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVAddTextFieldVerifyListener;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;



public class CreateSMDLWizardNamePanel extends Composite 
{
	private Registry m_appReg = null;
		
	private Composite m_smdlNamePanel;
		
	private Label m_smdlNameLabel;
	private Label m_smdlDescLabel;
		
	private Text m_smdlNameText;
	private Text m_smdlDescTextArea;
			
	public CreateSMDLWizardNamePanel(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
				
		createUI(this);
		setLabels();
		setEditability();
		addListeners();
	}
	private void createUI(Composite parent)
	{
		m_smdlNamePanel = new Composite(parent,SWT.NONE);
		
		GridLayout gridLayout = new GridLayout(2, false);
		m_smdlNamePanel.setLayout(gridLayout);
		m_smdlNamePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		m_smdlNameLabel = new Label(m_smdlNamePanel, SWT.NONE);
		m_smdlNameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_smdlNameText = new Text(m_smdlNamePanel, SWT.BORDER);
		GridData gd_m_smdlNameText = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_smdlNameText.setLayoutData(gd_m_smdlNameText);
		
		m_smdlDescLabel = new Label(m_smdlNamePanel, SWT.NONE);
		GridData gd_m_smdlDescLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		m_smdlDescLabel.setLayoutData(gd_m_smdlDescLabel);
			
		m_smdlDescTextArea = new Text(m_smdlNamePanel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		m_smdlDescTextArea.setTextLimit(240);
		GridData gd_dpDescTextArea = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		//gd_dpDescTextArea.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_smdlDescTextArea,56);
		gd_dpDescTextArea.heightHint=56;
		m_smdlDescTextArea.setLayoutData( gd_dpDescTextArea);
	}
	private void setLabels()
	{
		m_smdlNameLabel.setText(m_appReg.getString("SMDLName.LABEL"));
		m_smdlDescLabel.setText(m_appReg.getString("SMDLDesc.LABEL"));
	}
	private void setEditability()
	{
		m_smdlNameText.setEditable(false);
	}
	private void addListeners()
	{
		m_smdlDescTextArea.addVerifyListener(new NOVAddTextFieldVerifyListener());
	}
	public void populatePanel(String sSMDLName, String sSMDLDesc)
	{
		m_smdlNameText.setText(sSMDLName);
		if(sSMDLDesc != null)
		{
			m_smdlDescTextArea.setText(sSMDLDesc);
		}		
	}
	public CreateInObjectHelper populateCreateInObject(CreateInObjectHelper object)
	{
		//object.setStringProps(NOV4ComponentSMDL.SMDL_ID,m_dpIDText.getText(),CreateInObjectHelper.SET_CREATE_IN);
		object.setStringProps(NOV4ComponentSMDL.SMDL_DESC,m_smdlDescTextArea.getText(),CreateInObjectHelper.SET_PROPERTY);				
		return object;
	}
}