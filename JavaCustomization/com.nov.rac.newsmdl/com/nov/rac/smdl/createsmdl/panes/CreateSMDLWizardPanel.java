package com.nov.rac.smdl.createsmdl.panes;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Vector;
import java.beans.PropertyChangeEvent;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.common.PolicyProperty;

public class CreateSMDLWizardPanel extends Composite 
{
	private CreateSMDLWizardTopPanel 			m_topPanel;
	private CreateSMDLWizardTablePanel 			m_tablePanel;
	private TCSession 							m_session;
	private TCComponent 						m_dpObject;
	private TCComponentItemRevision				m_smdlRev = null;
	private TCComponentItem 					m_smdlItemObject = null;
	private ArrayList<PropertyChangeListener> 	m_listeners = null;
	private CreateInObjectHelper 				m_smdlRevUpdateObject = null;
	private NOVSMDLPolicy 						m_DPPolicy = null;
	private String  							user=null;
		
	public CreateSMDLWizardPanel(Composite parent, int style) 
	{
		super(parent, style);
		m_session=(TCSession)AIFUtility.getDefaultSession();
		createUI(this);
	}
	private void createUI(Composite parent) 
	{
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_topPanel = new CreateSMDLWizardTopPanel(parent,SWT.NONE);		
		m_tablePanel = new CreateSMDLWizardTablePanel(parent, SWT.BORDER);
	}
	public void populateCreateSMDLWizardTopPanel(String sSMDLType, String sSMDLDesc)
	{
		m_topPanel.setDPObject(m_dpObject);
		m_topPanel.populateTopPanel(sSMDLType,sSMDLDesc);
	}
	public void populateSMDLTableWithTemplateInfo(TCComponent templateComp)
	{
		m_tablePanel.setDPObject(m_dpObject);
		m_tablePanel.populateSMDLTableWithTemplateInfo(templateComp);
		
	}
	public void createSMDLOp()
	{
		ProgressMonitorDialog createSMDLDialog=new ProgressMonitorDialog(this.getShell());
		
		CreateInObjectHelper[] theSMDLObjectList = createSMDLObjectHelper();
		
		//CreateOrUpdateOperation createSMDLOp = new CreateOrUpdateOperation(createSMDLOpInput());
		CreateOrUpdateOperation createSMDLOp = new CreateOrUpdateOperation(theSMDLObjectList);
		try 
		{
			createSMDLDialog.run(true, false,createSMDLOp);		        	
		} 
		catch ( Exception e1 ) 
		{
			e1.printStackTrace();
			MessageBox.post("SMDL is NOT created: " + e1.getMessage(), null, MessageBox.ERROR);
		}
        
		TCComponent[] createdComp = CreateObjectsSOAHelper.getCreatedOrUpdatedObjects(theSMDLObjectList[0]);


		int iCompLength = createdComp.length;
		for(int indx=0;indx<iCompLength;indx++)
		{
			//TCDECREL-1551 setting the doc responsible on SMDL
			try 
			{
				if(createdComp[indx] instanceof TCComponentItemRevision)
				{
					m_smdlRev = (TCComponentItemRevision)createdComp[indx];
					
				}
				if(createdComp[indx] instanceof TCComponentItem)
				{
					m_smdlItemObject = (TCComponentItem)createdComp[indx];
					m_smdlItemObject.setProperty(NOV4ComponentSMDL.DOC_RESPONSIBLE, user);
					
				}
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// Get all code form objects from theSMDLObjectList 
		TCComponent[] createdCodeForms = new TCComponent[ theSMDLObjectList.length - 1];  
		
		for(int inx=0; inx < theSMDLObjectList.length - 1; inx++)
		{
			createdCodeForms[inx] = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(theSMDLObjectList[ inx + 1]);
		}
		
		// Get the Rev and add Code Forms to nov4_codes property.
		
		m_smdlRevUpdateObject = new CreateInObjectHelper(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE,CreateInObjectHelper.OPERATION_UPDATE);
		
		m_smdlRevUpdateObject.setTargetObject(m_smdlRev);
		
		m_smdlRevUpdateObject.setTagArrayProps(NOV4ComponentSMDL.SMDL_CODES, createdCodeForms, CreateInObjectHelper.SET_PROPERTY);
		
		m_smdlRevUpdateObject.setTagArrayProps(NOV4ComponentSMDLRevision.SMDLREV_DOCUMENT_RELATION,m_tablePanel.getDocumentRevisions() , CreateInObjectHelper.SET_PROPERTY);
		
		CreateOrUpdateOperation updateSMDLOp = new CreateOrUpdateOperation(m_smdlRevUpdateObject);
		try 
		{
			createSMDLDialog.run(true, false,updateSMDLOp);		        	
		} 
		catch ( Exception e1 ) 
		{
			e1.printStackTrace();
			MessageBox.post("SMDL is NOT created: " + e1.getMessage(), null, MessageBox.ERROR);
		} 
		
		firePropertyChange(this, "CreateSMDLOperation","" , m_smdlItemObject);
		
		//start TCDECREL-3128: add created smdl to user's home folder
		addToUserHomeFolder(m_smdlItemObject);
		
    }
	
	private void addToUserHomeFolder(TCComponentItem smdlItemObject)
	{
		TCSession session = (TCSession) AIFUtility.getDefaultSession();
		
		if(session != null)
		{
		    try 
		    {
		 	    TCComponentFolder userHomeFolder = session.getUser().getHomeFolder();
		 	    if(userHomeFolder != null && smdlItemObject != null)
		 	    {
		 		    userHomeFolder.add("contents", smdlItemObject,0);
		 	    }
		    } 
		    catch (TCException e) 
		    {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
		    }
		}
		
	}
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}
		
	public void setDPObject(TCComponent dpObj)
	{
		m_dpObject = getDeliveredProductProps(dpObj);
	}
		
	//--------------------------------------------------------------------------------
	// Abhijit: Added new logic for object creation
	//--------------------------------------------------------------------------------
	
	private CreateInObjectHelper[] createSMDLObjectHelper(){

		CreateInObjectHelper createInHelperSMDL = new CreateInObjectHelper(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME, CreateInObjectHelper.OPERATION_CREATE);
		
		String smdlId = NOVCreateSMDLHelper.getSMDLID();
		createInHelperSMDL.setStringProps(NOV4ComponentSMDL.SMDL_ID,smdlId,CreateInObjectHelper.SET_CREATE_IN);
		m_topPanel.populateCreateInObject(createInHelperSMDL);	
		
		createInHelperSMDL.setTagProps(NOV4ComponentSMDL.DP_REF, m_dpObject, CreateInObjectHelper.SET_PROPERTY);
		createInHelperSMDL.setRelationName(NOV4ComponentDP.DP_SMDL_RELATION);
		createInHelperSMDL.setParentObject( m_dpObject);

		TCComponent orderComp = null;
		try 
		{
			orderComp =  m_dpObject.getTCProperty(NOV4ComponentDP.DP_ORDER_RELATION).getReferenceValue();
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(orderComp != null)
		{
			createInHelperSMDL.setTagProps(NOV4ComponentSMDL.ORDER_REF, orderComp, CreateInObjectHelper.SET_PROPERTY);
		}
				
		// Creating a SMDL Revision Helper object as compound object on SMDL Item Helper
		CreateInObjectHelper smdlRevHelper = new CreateInObjectHelper(NOV4ComponentSMDL.SMDLREV_OBJECT_TYPE_NAME, CreateInObjectHelper.OPERATION_CREATE);
		
		createInHelperSMDL.setCompoundCreateInput("revision",smdlRevHelper );		
		
		//CreateInObjectHelper[] createInCodeForms = createCodeOpInput();
		//Mohan - As part of code review comments moved above method to table panel class
		CreateInObjectHelper[] createInCodeForms = m_tablePanel.createCodeOperationInput();
		
		CreateInObjectHelper[] theSMDLObjectList = new CreateInObjectHelper[createInCodeForms.length +1];				
		
		theSMDLObjectList[0] = createInHelperSMDL;
		
		for (int iCnt = 1; iCnt < theSMDLObjectList.length; iCnt++){
			theSMDLObjectList[iCnt] = createInCodeForms[iCnt-1];
		}
		
		return theSMDLObjectList;		
		
	}
	
	// --------------------------------------------------------------------------------
	// Abhijit: Added new logic for object creation
	// --------------------------------------------------------------------------------
	
	public String validateMandatoryFields()
	{
		StringBuffer invalidInputs=new StringBuffer();
		
		invalidInputs.append(m_topPanel.validateMandatoryFields());
		if(invalidInputs.length()!=0)
		{
			invalidInputs.append(",");
		}
		invalidInputs.append(m_tablePanel.validateDateFields());
		
		return invalidInputs.toString();
	}
	public void addPropertyChangeListeners(	ArrayList<PropertyChangeListener> listeners) {

		m_listeners = listeners;
		
	}
	public void populateSMDLTableWithSMDLRevInfo(TCComponentItemRevision smdlRev)
	{
		m_tablePanel.setDPObject(m_dpObject);
		m_tablePanel.populateSMDLTableWithSMDLRevInfo(smdlRev);
		
	}
	private TCComponent getDeliveredProductProps(TCComponent dpObject) 
	{
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(dpObject);

		String[] dpAttributeList = getDPAttributeList();

		NOVSMDLPolicy dpPolicy = createSMDLPolicy();

		m_dpObject = dmSOAHelper.getSMDLProperties(dpObject,dpAttributeList,dpPolicy);		

		return m_dpObject;
	}

	private String[] getDPAttributeList() 
	{
		String[] dpAttributeList =	{	
										NOV4ComponentDP.DP_BASED_ON_PROD,
										NOV4ComponentDP.CONTRACT_DELIVERY_DATE,
										NOV4ComponentDP.DP_ORDER_RELATION
								     };

		return dpAttributeList;
	}

	public NOVSMDLPolicy createSMDLPolicy() 
	{
		if(m_DPPolicy == null)
		{
			
			m_DPPolicy = new NOVSMDLPolicy();
						
			// Add Property policy for Order object
			Vector<NOVSMDLPolicyProperty> orderPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_TYPE,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CUSTOMER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PLANT_ID_OTHER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PURCHASE_ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CONTRACT_AWARD_DATE,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CONTRACT_DELIVERY_DATE,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PROJECT_MANAGER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.DOCUMENT_CONTROLLER,PolicyProperty.AS_ATTRIBUTE));
						
			NOVSMDLPolicyProperty[] orderPolicyPropsArr = orderPolicyProps.toArray(new NOVSMDLPolicyProperty[orderPolicyProps.size()]);
			
			m_DPPolicy.addPropertiesPolicy(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME, orderPolicyPropsArr);
		}		
		return m_DPPolicy;
	}
	//TCDECREL-1551
	public void populateDocResponsibleInfo(String docResp) {
		// TODO Auto-generated method stub
		m_tablePanel.populateDocResponsibleInfo(docResp);
		user=docResp;
	}
	

}