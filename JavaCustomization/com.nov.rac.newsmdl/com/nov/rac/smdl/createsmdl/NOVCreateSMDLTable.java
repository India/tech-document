package com.nov.rac.smdl.createsmdl;

import java.util.Arrays;

import javax.swing.ToolTipManager;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.nov.rac.smdl.createsmdl.renderer.NOVCodeTabCellRenderer;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class NOVCreateSMDLTable extends NOVSMDLTable
{    
    private static final long serialVersionUID = 1L;
    private Registry m_smdlAppReg      = null;
    
    public NOVCreateSMDLTable(TCSession session, String columns[])
    {
        super(session , columns);
        m_smdlAppReg =Registry.getRegistry(this);
        
//        dataModel = new NOVCreateSMDLTableModel(columns);        
//        setModel(dataModel);
        assignColumnRenderer();
        assignCellEditor();
        //assignCellRendererEditor();
    }
    
	protected void setModel(TCSession session, String[] columns) {
		if(session != null)
        {
			String strDisplayToken = getDisplayNamesToken();
			
            String as1[][] = getDisplayableHeaderValue(session, columns, strDisplayToken);
            dataModel = new NOVCreateSMDLTableModel(as1);
        }
        else
        {
            dataModel = new NOVCreateSMDLTableModel(columns);
        }
        
        setModel(dataModel);
	}
	
    public void assignCellRendererEditor()
	{
    	ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        String s = r.getString("default.RENDERER");      
        
        for(int j = 0; j < i; j++)
        {
           	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".RENDEREREDITOR").toString(), s);
           	System.out.println(".."+dataModel.getColumnIdentifier(j));
            try
            {
            	Object tableCellRendererEditor = Instancer.newInstance(s1);
            	if(tableCellRendererEditor != null)
            	{
            		if(tableCellRendererEditor instanceof TableCellRenderer)
            		{
            			getColumnModel().getColumn(j).setCellRenderer((TableCellRenderer)tableCellRendererEditor);
            		}
            		if(tableCellRendererEditor instanceof TableCellEditor)
            		{
            			getColumnModel().getColumn(j).setCellEditor((TableCellEditor)tableCellRendererEditor);
            		}
            	}
            }
            catch(Exception exception)
            {
                
            }
        }        
        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }
    public boolean isCellEditable(int row, int col) 
    {		
		String columnName =getColumnPropertyName(col);
      
        String[] editableColumns=m_smdlAppReg.getStringArray("editableColumns.Name");
        if (Arrays.asList(editableColumns).contains(columnName) ) 
        {
            return true;
        }
        else
            return false;
	}
    public void assignColumnRenderer()
    {
        ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        String s = r.getString("default.RENDERER");
       
        
        for(int j = 0; j < i; j++)
        {
           	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".RENDERER").toString(), s);
           	System.out.println(".."+dataModel.getColumnIdentifier(j));
            try
            {
            	TableCellRenderer abstracttctablecellrenderer = (TableCellRenderer)Instancer.newInstance(s1);
                if(abstracttctablecellrenderer != null)
                    getColumnModel().getColumn(j).setCellRenderer(abstracttctablecellrenderer);
            }
            catch(Exception exception)
            {
                
            }
        }

        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }    
    public void assignCellEditor()
    {
        ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        
        for(int j = 0; j < i; j++)
        {
        	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".EDITOR").toString(),null);
            if(s1 != null && s1.length() > 0)
            {
	            try
	            {
	            	TableCellEditor cellEditor = (TableCellEditor)Instancer.newInstance(s1);
	               
	                if(getColumnModel().getColumn(j).getCellEditor() == null)
	                {
	                    getColumnModel().getColumn(j).setCellEditor(cellEditor);
	                }
	            }
	           catch(Exception exception)
	            {
	                
	            }
            }
            
        }

        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }
}