package com.nov.rac.smdl.createsmdl;

import java.util.Vector;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLBasicInfoPanel;
import com.nov.rac.smdl.utilities.NOV4TemplateHelper;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVUserDataHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class CreateSMDLBasicInfoPage extends WizardPage
{    
	public static String PAGE_NAME="CreateSMDLBasicInfoPage";
	private CreateSMDLBasicInfoPanel m_basicInfoPanel;
	private Registry m_appReg = null;
	public CreateSMDLBasicInfoPage() 
	{
		super(PAGE_NAME);
		m_appReg = Registry.getRegistry(this);
		setMessage(m_appReg.getString("BasicInfoPage.MSG"), WizardPage.WARNING);
	}
	@Override
	public void createControl(Composite parent) 
	{		
		m_basicInfoPanel = new CreateSMDLBasicInfoPanel(parent, SWT.NONE);
				
		setControl(m_basicInfoPanel);
		setPageComplete( true );
	}
	public TCComponent getTemplateComp()
	{
		TCComponent templateComp = null;
		String strTempName = null;
		
		strTempName = getSelectedTemp();
		templateComp = NOV4TemplateHelper.getTemplateComponent(strTempName);
		
		if(templateComp != null)
		{
			NOV4TemplateHelper.getTemplateRevisionProps( templateComp );
		}
		
		return templateComp;
	}
	public String getSMDLType()
	{
		return m_basicInfoPanel.getSMDLType();
	}
	public String getSMDLDesc()
	{
		return m_basicInfoPanel.getSMDLDesc();
	}
	public String getSelectedTemp()
	{
		return m_basicInfoPanel.getSelectTempComboValue();
	}
	public String validateMandatoryFields()
	{
		StringBuffer invalidInputs=new StringBuffer();
		
		invalidInputs.append(m_basicInfoPanel.validateMandatoryFields());
		
		return invalidInputs.toString();
	}
	public TCComponentItemRevision getSaveAsSMDLRev()
	{
		TCComponentItemRevision smdlRev = null;
		String smdlId = null;
		
		smdlId = m_basicInfoPanel.getSaveAsSMDLValue();
		smdlRev = NOVCreateSMDLHelper.getSMDLLatestRevision(smdlId);
		return smdlRev;
	}
	public boolean bIsSelectTemplateButtonSelected()
	{
		return m_basicInfoPanel.bIsSelectTemplateButtonSelected();
	}
	public boolean bIsSaveAsSMDLButtonSelected()
	{
		return m_basicInfoPanel.bIsSaveAsSMDLButtonSelected();
	}
	public String getSaveAsSMDLId()
	{
		return m_basicInfoPanel.getSaveAsSMDLValue();
	}
	//TCDECREL-1551
	public CreateSMDLBasicInfoPanel getPanel()
	{
		return m_basicInfoPanel;
	}
	///TCDECREL-1551 Added validation in doc responsible in SMDL creation
	public boolean getValidInputComponentDocResponsible() throws Exception
	{
		String docResponsibleUser=null;
		
		Vector<String> userVector = new Vector<String>();
		
		userVector=NOVUserDataHelper.getUpdateUserList();
		
		docResponsibleUser=m_basicInfoPanel.getSelectDocRespComboValue();

		if( userVector.contains(docResponsibleUser) )
		{
			return true;
		}
		if( docResponsibleUser.equals("") )
		{
			return true;
		}
		else
		{
			return false;			
		}
	}
}