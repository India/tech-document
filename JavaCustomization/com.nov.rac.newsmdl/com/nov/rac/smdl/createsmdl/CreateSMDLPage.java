package com.nov.rac.smdl.createsmdl;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.createsmdl.panes.CreateSMDLWizardPanel;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.Registry;

public class CreateSMDLPage extends WizardPage
{    
	public static String PAGE_NAME="CreateSMDLPage";
	private CreateSMDLWizardPanel m_createSMDLInfoPanel;
	private Registry m_appReg = null;
	
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	private CreateInObjectHelper m_smdlRevUpdateObject = null;
	
	public CreateSMDLPage() 
	{
		super(PAGE_NAME);
		m_appReg = Registry.getRegistry(this);
		setMessage(m_appReg.getString("CreateSMDLPage.MSG"));
	}
	@Override
	public void createControl(Composite parent) 
	{
		m_createSMDLInfoPanel = new CreateSMDLWizardPanel(parent, SWT.NONE);
		
		m_createSMDLInfoPanel.addPropertyChangeListeners(m_listeners);
		
		setControl(m_createSMDLInfoPanel);
		setPageComplete( true );
	}
	public void populateSMDLTableWithTemplateInfo(TCComponent templateComp)
	{
		m_createSMDLInfoPanel.populateSMDLTableWithTemplateInfo(templateComp);
	}
	public void populateCreateSMDLWizardTopPanel(String sSMDLType, String sSMDLDesc)
	{
		m_createSMDLInfoPanel.populateCreateSMDLWizardTopPanel(sSMDLType,sSMDLDesc);
	}
	public void createSMDLOperation()
	{
		m_createSMDLInfoPanel.createSMDLOp();
	}
	public void setDPObject(TCComponent dpObject)
	{
		m_createSMDLInfoPanel.setDPObject(dpObject);
	}
	public void populateSMDLTableWithSMDLRevInfo(TCComponentItemRevision smdlRev)
	{
		m_createSMDLInfoPanel.populateSMDLTableWithSMDLRevInfo(smdlRev);
	}
	public String validateMandatoryFields()
	{
		StringBuffer invalidInputs=new StringBuffer();
		
		invalidInputs.append(m_createSMDLInfoPanel.validateMandatoryFields());
		
		return invalidInputs.toString();
	}
	
	public void addPropertyChangeListener(ArrayList<PropertyChangeListener> listeners) {

//		m_createSMDLInfoPanel.addPropertyChangeListeners(listeners);

		for( PropertyChangeListener listener:listeners )
		{
			if(!m_listeners.contains(listener) ){
				m_listeners.add(listener);
			}
		}	
	}
	//TCDECREL-1551
	public void populateDocResponsible(String docResp) {
		// TODO Auto-generated method stub
		m_createSMDLInfoPanel.populateDocResponsibleInfo(docResp);
	}
		
}