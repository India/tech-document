package com.nov.rac.smdl.createsmdl.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.common.renderereditor.IStrikeThrough;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVDocNoAttrCellRenderer implements TableCellRenderer,IStrikeThrough
{
    private static final long 	serialVersionUID = 1L;
    private boolean 			m_bCodeStrikeThrough 	= false;
    private boolean 			m_bDocStrikeThrough 	= false;
    private Color       		m_alternateBackground 	= Color.white;
    private CommonCellPanel 	m_Panel;
    private JLabel 				m_Text;
    private boolean             m_bCodeAdded       = false;
    private NOVCreateSMDLTable 	m_smdlTable;
    private boolean             m_bRowUpdated      = false;
    public NOVDocNoAttrCellRenderer()
    {
    	super();
    	m_alternateBackground = new Color(191,214,248);
    	m_Panel = new CommonCellPanel();
    	m_Panel.setLayout(new BoxLayout(m_Panel , BoxLayout.LINE_AXIS));
    	m_Text = new JLabel();
    	m_Text.setMinimumSize(new Dimension(80 , 20));
    	m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
    	m_Panel.add(m_Text);
    }
    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
    	if ( comp != null )
    	{
    		ImageIcon compIcon;
    		String strCompType = comp.getType();
    		compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
    		return compIcon;
    	}
    	else
    		return null;
    }
    protected void setValueIcon(Object arg0 , Icon arg1 )
	{
    	if ( arg0 != null )
	    {
    		m_Text.setText(arg0.toString());
    		m_Text.setIcon(arg1);
	    }
	}
    protected void initiateIcons() 
    {
		// TODO Auto-generated method stub		
	}
    public Component getTableCellRendererComponent(JTable table , Object value ,
            boolean isSelected , boolean hasFocus , int row , int column )
    { 
    	String s = value.toString();
    	m_Text.setText(s != null ? s : "");
    	
    	if(table instanceof NOVCreateSMDLTable)
    	{
    		m_smdlTable = (NOVCreateSMDLTable)table;    		
    		AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			m_bCodeStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			m_bDocStrikeThrough = smdlTableLine.getDocRemovedStatus();
    			m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
    			m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		}
    	}
    	if ( isSelected )
        {
        	m_Panel.setForeground(table.getSelectionForeground());
        	m_Panel.setBackground(table.getSelectionBackground());
        }
        else
        {
        	m_Panel.setForeground(table.getForeground());
        	m_Panel.setBackground(row % 2 != 1 ? table.getBackground() : m_alternateBackground);
        }
        if ( hasFocus )
        {
        	m_Panel.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        	if ( table.isCellEditable(row, column) )
        	{
        		m_Panel.setForeground(UIManager.getColor("Table.focusCellForeground"));
        		m_Panel.setBackground(UIManager.getColor("Table.focusCellBackground"));
        	}
        }
        else
        {
        	m_Panel.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
        }
/*        if (!isSelected && m_bCodeAdded)
		{
			m_Panel.setBackground(new Color(170, 255, 170));
		}
        else if(!isSelected && m_bRowUpdated)
        {
        	m_Panel.setBackground(Color.yellow);
        }*/
        return m_Panel;
    }
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bCodeStrikeThrough = bIsStrikeThrough;
	}
	private class CommonCellPanel extends JPanel
	{		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bCodeStrikeThrough || m_bDocStrikeThrough)
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
	};
}
