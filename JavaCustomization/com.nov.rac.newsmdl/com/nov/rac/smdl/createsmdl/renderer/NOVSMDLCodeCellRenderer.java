package com.nov.rac.smdl.createsmdl.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.common.renderereditor.IStrikeThrough;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLCodeCellRenderer implements TableCellRenderer,IStrikeThrough
{
    private static final long serialVersionUID = 1L;
    private JLabel                  m_Text;
    private NovCodeColPanel 		m_Panel;
    private JButton                 m_removeCodeBtn;
    private ImageIcon               m_minusIcon        = null;
    private Color                   m_alternateBackground = Color.white;   
    private Registry 				m_registry = null;
    private NOVCreateSMDLTable 		m_smdlTable;
    protected ChangeEvent     		changeEvent = new ChangeEvent(this);
    private boolean 				m_bCodeStrikeThrough = false;
    private boolean                 m_bCodeAdded       = false;
    private boolean                 m_bRowUpdated      = false;
    
    public NOVSMDLCodeCellRenderer()
    {
    	super();
    	m_registry = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
    	m_alternateBackground = new Color(191,214,248);
    	m_Panel = new NovCodeColPanel();
    	m_Panel.setLayout(new BoxLayout(m_Panel , BoxLayout.LINE_AXIS));
    	m_Text = new JLabel();
    	m_Text.setMinimumSize(new Dimension(80 , 20));
        try
        {
            m_minusIcon =m_registry.getImageIcon("minus.ICON");        	
        }
        catch ( Exception ie )
        {
            System.out.println(ie);
        }
        m_removeCodeBtn = new JButton(m_minusIcon);
                
        m_removeCodeBtn.setMinimumSize(new Dimension(18 , 20));
        m_removeCodeBtn.setPreferredSize(new Dimension(18 , 20));
        m_removeCodeBtn.setMaximumSize(new Dimension(18 , 20));  
        m_removeCodeBtn.setBackground(Color.WHITE);
                
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_removeCodeBtn);
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_Text);        
    }
    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
    	if ( comp != null )
    	{
    		ImageIcon compIcon;
    		String strCompType = comp.getType();
    		compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
    		return compIcon;
    	}
    	else
    		return null;
    }
    protected void setValueIcon(Object arg0 , Icon arg1 )
	{
    	if ( arg0 != null )
	    {
	    	m_Text.setText(arg0.toString());
	    	m_Text.setIcon(null);
	    }
	}
    protected void initiateIcons() 
    {
		// TODO Auto-generated method stub		
	}
    public Component getTableCellRendererComponent(JTable table , Object value ,
            boolean isSelected , boolean hasFocus , int row , int column )
    {    	
    	if(table instanceof NOVCreateSMDLTable)
    	{
    		m_smdlTable = (NOVCreateSMDLTable)table;    		
    		AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			m_bCodeStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
    			m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		}
    	}
    	if ( isSelected )
        {
        	m_Panel.setForeground(table.getSelectionForeground());
        	m_Panel.setBackground(table.getSelectionBackground());
        }
        else
        {
        	m_Panel.setForeground(table.getForeground());
        	m_Panel.setBackground(row % 2 != 1 ? table.getBackground() : m_alternateBackground);
        }
        if ( hasFocus )
        {
        	m_Panel.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        	if ( table.isCellEditable(row, column) )
        	{
        		m_Panel.setForeground(UIManager.getColor("Table.focusCellForeground"));
        		m_Panel.setBackground(UIManager.getColor("Table.focusCellBackground"));
        	}
        }
        else
        {
        	m_Panel.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
        }   
/*        if (!isSelected && m_bCodeAdded)
		{
			m_Panel.setBackground(new Color(170, 255, 170));
		}
        else if(!isSelected && m_bRowUpdated)
        {
        	m_Panel.setBackground(Color.yellow);
        }*/
        
        String s = (String)value;
        //TCDECREL3386 changes for displaying nov code + description
/*        String[] codeList = NOVCodeHelper.getCodeList();
        for (int inx = 0; inx < codeList.length; inx++)
        {
        	String sNovCode = codeList[inx].substring(0, codeList[inx].indexOf('='));
        	if(sNovCode.compareToIgnoreCase(s) == 0)
        	{
        		s = codeList[inx];
        		break;
        	}
        	
		}*/
        m_Text.setText(s != null ? s : "");
        return m_Panel;
    }
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bCodeStrikeThrough = bIsStrikeThrough;
	}
	private class NovCodeColPanel extends JPanel
	{		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bCodeStrikeThrough )
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
	};
}
