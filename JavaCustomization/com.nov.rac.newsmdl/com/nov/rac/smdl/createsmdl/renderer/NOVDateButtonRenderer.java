package com.nov.rac.smdl.createsmdl.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.common.renderereditor.IStrikeThrough;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.util.DateButton;

public class NOVDateButtonRenderer implements TableCellRenderer, IStrikeThrough
{
	
	private PrivateDateButton m_dateButton ;	
	private SimpleDateFormat m_formatter;
	int                       irowIndex        = -1;
	int                       icolIndex        = -1;
	private static final long serialVersionUID = -697898812006419074L;
	private boolean 			m_bIsStrikeThrough = false;
	private boolean             m_bCodeAdded       = false;
	private NOVCreateSMDLTable m_smdlTable;
	private boolean             m_bRowUpdated      = false;
    public NOVDateButtonRenderer()
    {
    	m_dateButton= new PrivateDateButton();
    	m_dateButton.setBackground(Color.white);
    	m_dateButton.setBorderPainted(false);
        setFormatter();
    }  
        
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
    	if ( value != null )
    	{
    		if ( value instanceof DateButton )
    		{
    			if ( value instanceof Component )
    			{
    				m_dateButton.setDate(((DateButton) value).getDate());
    				m_dateButton.setText(((DateButton) value).getText());
    			}
    			else
    			{
    				m_dateButton.setDate(((DateButton) value).getDate());
    				return m_dateButton;
    			}
    			m_dateButton.setBorderPainted(false);
    		}
    		else if( value instanceof Date)
    		{
    			m_dateButton.setDate((Date)value);
    			m_dateButton.setBorderPainted(false);
    		}
    		else if( value instanceof String)
    		{
    			m_dateButton.setDate(value.toString());
    			m_dateButton.setBorderPainted(false);
    		}
    	}
    	if(table instanceof NOVCreateSMDLTable)    
    	{
    		m_smdlTable = (NOVCreateSMDLTable)table;
    		AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			m_bIsStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
    			m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		}
    	}
/*    	if (m_bCodeAdded)
    	{
    		m_dateButton.setBackground(new Color(170, 255, 170));
    	}
    	else if(m_bRowUpdated)
    	{
    		m_dateButton.setBackground(Color.yellow);
    	}*/
		return m_dateButton;  
    }
    private void setFormatter()
    {
    	 m_formatter= new SimpleDateFormat();
         String pattern="dd-MMM-yyyy";
         m_formatter.applyPattern(pattern);
         m_dateButton.setDisplayFormatter(m_formatter);
    }       
    private class PrivateDateButton extends DateButton
    {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public PrivateDateButton()
		{
			super();
			this.setMandatory(true);
		}
    	public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bIsStrikeThrough)
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
    }
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bIsStrikeThrough = bIsStrikeThrough;
	};    
}

