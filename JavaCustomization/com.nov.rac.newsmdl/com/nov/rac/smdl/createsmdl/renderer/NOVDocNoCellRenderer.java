package com.nov.rac.smdl.createsmdl.renderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.common.renderereditor.IStrikeThrough;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class NOVDocNoCellRenderer implements TableCellRenderer, IStrikeThrough
{
    private static final long serialVersionUID = 1L;
    private JLabel                  m_Text;
    private DocNoColPanel           m_Panel;
    private JButton                 m_removeCodeBtn;
    private ImageIcon               m_minusIcon        = null;
    private Color                   m_alternateBackground = Color.white;   
    private Registry 				m_registry = null;
    private NOVCreateSMDLTable 		m_smdlTable;
    private boolean 				m_bCodeStrikeThrough = false;
    private boolean 				m_bDocStrikeThrough  = false;
    private boolean                 m_bCodeAdded         = false;
    private boolean                 m_bRowUpdated        = false;
    public NOVDocNoCellRenderer()
    {
    	super();
    	m_registry = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
    	m_alternateBackground = new Color(191,214,248);
    	m_Panel = new DocNoColPanel();
    	m_Panel.setLayout(new BoxLayout(m_Panel , BoxLayout.LINE_AXIS));
    	m_Text = new JLabel();
    	m_Text.setMinimumSize(new Dimension(80 , 20));
        try
        {
            m_minusIcon =m_registry.getImageIcon("minus.ICON");        	
        }
        catch ( Exception ie )
        {
            System.out.println(ie);
        }
        m_removeCodeBtn = new JButton(m_minusIcon);
                
        m_removeCodeBtn.setMinimumSize(new Dimension(18 , 20));
        m_removeCodeBtn.setPreferredSize(new Dimension(18 , 20));
        m_removeCodeBtn.setMaximumSize(new Dimension(18 , 20));  
        m_removeCodeBtn.setBackground(Color.WHITE);
              
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_removeCodeBtn);
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_Text);
    }

    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
        if ( comp != null )
        {
            ImageIcon compIcon;
            String strCompType = comp.getType();
            compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
            return compIcon;
        }
        else
            return null;
    }

    protected void setValueIcon(Object arg0 , Icon arg1 )
    {
        System.out.println("setValueIcon....");
        ImageIcon docIcon = m_registry.getImageIcon("document.ICON");
        if ( arg0 != null )
        {
        	m_Text.setText(arg0.toString());
        	m_Text.setIcon(docIcon);
        }
    }
    @Override
	public Component getTableCellRendererComponent(JTable jtable , Object obj ,
            boolean isSelected , boolean hasFocus , int row , int col ) 
	{
		TCComponent tccomponent = null;
		if(jtable instanceof NOVCreateSMDLTable)    
		{
			m_smdlTable = (NOVCreateSMDLTable)jtable;
			
			if (obj instanceof TCComponentItem) 
			{
				tccomponent = (TCComponent)obj;
			}
			AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			m_bCodeStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			m_bDocStrikeThrough = smdlTableLine.getDocRemovedStatus();
    			m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
    			m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		}
		}
		m_Panel.setBackground(row % 2 != 1 ? Color.white : (new Color(191 , 214 , 248)));
    	m_removeCodeBtn.setForeground(new JButton().getForeground());     
    	if ( isSelected )
        {
        	m_Panel.setForeground(jtable.getSelectionForeground());
    		m_Panel.setBackground(jtable.getSelectionBackground());
        }
        else
        {
        	m_Panel.setForeground(jtable.getForeground());
        	m_Panel.setBackground(row % 2 != 1 ? jtable.getBackground() : m_alternateBackground);
        }
        if ( hasFocus )
    	{
    		m_Panel.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
    		if ( jtable.isCellEditable(row, col) )
    		{
    			m_Panel.setForeground(UIManager.getColor("Table.focusCellForeground"));
    			m_Panel.setBackground(UIManager.getColor("Table.focusCellBackground"));
    		}
    	}
    	else
    	{
    		m_Panel.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
    	}
        
    	Icon icon = getDisplayIcon(tccomponent, obj);
    	String s = null;
    	/*if(tccomponent != null)
    	{
    		s = tccomponent.toString();
    	}
        m_Text.setText(s != null ? s : "");*/
    	
    	if(tccomponent != null)
    	{
    		try
    		{
				s = ((TCComponentItem) obj).getProperty("item_id");
			} 
    		catch (TCException e)
    		{
				e.printStackTrace();
			}
    	}
    	m_Text.setText(s!= null ? s : "");
        m_Text.setIcon(icon);    
        int k = m_Panel.getFontMetrics(m_Panel.getFont()).stringWidth(m_Text.getText());
        int l = jtable.getCellRect(row, col, false).width;
        if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
        {
        	m_Panel.setToolTipText(s);
        }
        else
        {
        	m_Panel.setToolTipText(null);
        }
        m_Panel.setSize(150, m_Panel.getHeight());  
/*        if (!isSelected && m_bCodeAdded)
		{
			m_Panel.setBackground(new Color(170, 255, 170));
		}
        else if(!isSelected && m_bRowUpdated)
        {
        	m_Panel.setBackground(Color.yellow);
        }*/
    	return m_Panel;
	}
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bCodeStrikeThrough = bIsStrikeThrough;
	}
	private class DocNoColPanel extends JPanel
	{		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bCodeStrikeThrough || m_bDocStrikeThrough)
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
	};
}
