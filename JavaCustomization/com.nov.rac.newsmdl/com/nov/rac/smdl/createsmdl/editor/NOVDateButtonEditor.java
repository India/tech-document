package com.nov.rac.smdl.createsmdl.editor;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.teamcenter.rac.util.DateButton;

public class NOVDateButtonEditor implements TableCellEditor
{
	
	private PrivateDateButton m_dateButton ;	
	private SimpleDateFormat m_formatter;
	int                       m_irowIndex        = -1;
	int                       m_icolIndex        = -1;
	private static final long serialVersionUID = -697898812006419074L;
    public NOVDateButtonEditor()
    {
    	m_dateButton= new PrivateDateButton();
    	m_dateButton.setBackground(Color.white);
    	m_dateButton.setBorderPainted(false);
        setFormatter();
        try
        {
        	m_dateButton.addPropertyChangeListener(new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent evt )
                {
                    // Special check for OK Button
                    if ( evt.getPropertyName().equals("text") )
                    {
                        fireEditingStopped();
                    }
                }
            });
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }  
    public Component getTableCellEditorComponent(JTable table , Object value ,
            boolean isSelected , int row , int column )
    {
        m_irowIndex = row;
        m_icolIndex = column;
        NOVSMDLTable smdlTable = null;
        if(table != null && table instanceof NOVSMDLTable)
    	{
    		smdlTable = (NOVSMDLTable) table;
    	}
        if(value!=null)
        {
        	Date dateToset=null;
	        if ( value instanceof DateButton )
	        {
	        	m_dateButton.setDate(((DateButton) value).getDate());
	        	m_dateButton.setText(((DateButton) value).getText());
	        }
	        else if( value instanceof Date)
	        {
	        		dateToset=(Date)value;
	        	        	      		
	        		m_dateButton.setDate(dateToset);
	                m_dateButton.setBorderPainted(false);
	        }
	        else if(value instanceof String)
	        {
	        	//if(((String) value).trim().length()>0)
	        	{
		           //	dateToset=new Date(value.toString());
	        		m_dateButton.setDate(value.toString());
	        	}
	        }
        }  
        if(smdlTable != null) //TCDECREL-2512
        {
        	int iContractDateCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
        	int iPalnnedDateCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOC_PLANNED_DATE);
        	if(column == iContractDateCol)
        	{
        		TableCellEditor cellEditor = smdlTable.getCellEditor(row, iPalnnedDateCol);
        		if(cellEditor != null)
        		{
        			cellEditor.cancelCellEditing();
        		}
        	}
        	else if (column == iPalnnedDateCol)
        	{
        		TableCellEditor cellEditor = smdlTable.getCellEditor(row, iContractDateCol);
        		if(cellEditor != null)
        		{
        			cellEditor.cancelCellEditing();
        		}
        	}
        }
        return m_dateButton;
    }
    private void setFormatter()
    {
    	 m_formatter= new SimpleDateFormat();
         String pattern="dd-MMM-yyyy";
         m_formatter.applyPattern(pattern);
         m_dateButton.setDisplayFormatter(m_formatter);
    }

    public Object getCellEditorValue()
    {
    	m_dateButton.postDown();
        return m_dateButton.getDate();
    }

    public boolean isCellEditable(EventObject anEvent )
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject anEvent )
    {
    	m_dateButton.repaint();
        return true;
    }

    public boolean stopCellEditing()
    {
        return true;
    }

    public void cancelCellEditing()
    {
        fireEditingCanceled();
    }

    private void fireEditingCanceled()
    {
        CellEditorListener listener;
       
        Object[] listeners = m_dateButton.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
            	if (m_dateButton.m_changeEvent == null)
            	{
            		m_dateButton.m_changeEvent = new ChangeEvent(m_dateButton);
            	}
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(m_dateButton.m_changeEvent);
            }
        }
    }

    public void addCellEditorListener(CellEditorListener listener )
    {
    	m_dateButton.getEventListenerList().add(CellEditorListener.class, listener);
    }

    public void removeCellEditorListener(CellEditorListener listener )
    {
    	m_dateButton.getEventListenerList().remove(CellEditorListener.class, listener);
    }
    
    protected void fireEditingStopped()
    {
    	m_dateButton.repaint();
    	m_dateButton.postDown();
        CellEditorListener listener;
        Object[] listeners = m_dateButton.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
            	if (m_dateButton.m_changeEvent == null)
            	{
            		m_dateButton.m_changeEvent = new ChangeEvent(m_dateButton);
            	}
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(m_dateButton.m_changeEvent);
            }
        }
    }       
    private class PrivateDateButton extends DateButton
    {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		protected ChangeEvent m_changeEvent = null;
		public PrivateDateButton()
		{
			super();
			this.setMandatory(true);
		}
		public EventListenerList getEventListenerList()
    	{
    		return listenerList;
    	}
    }   
}

