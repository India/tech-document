package com.nov.rac.smdl.createsmdl;

import java.util.Hashtable;

import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.teamcenter.rac.aif.common.AIFTableLine;

public class NOVCreateSMDLTableModel extends NOVSMDLTableModel 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NOVCreateSMDLTableModel()
	{
	    super();
	}
	public NOVCreateSMDLTableModel(String as[][])
	{
	   super(as);	    
	}
	public NOVCreateSMDLTableModel(String as[])
	{
	   super(as);	       
	}
	public NOVCreateSMDLTableLine createLine(Object obj)
	{
		NOVCreateSMDLTableLine aiftableline = null;
		Object [] strObj = null;

		if(obj instanceof Object[])
		{
			strObj = ((Object [])obj);	
			if(strObj != null)
			{
				int i = strObj.length;
				int j = getColumnCount();
				Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
				for(int k = 0; k < j && k < i; k++)
				{
					if(strObj[k] != null)
					{
						hashtable.put(getColumnIdentifier(k), strObj[k]);
					}
				}       
				aiftableline = new NOVCreateSMDLTableLine(hashtable);
			}
		}
		else 
		{
			aiftableline = (NOVCreateSMDLTableLine) super.createLine(obj);
		}
		return aiftableline;
	} 
    protected NOVCreateSMDLTableLine[] createLines(Object obj)
    {
    	NOVCreateSMDLTableLine aaiftableline[] = null;

    	if( obj instanceof Object [][] )
        {
    		Object [][] objArrArr = (Object [][])obj;    		
            int i = objArrArr.length;
            aaiftableline = new NOVCreateSMDLTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	NOVCreateSMDLTableLine newLine = createLine( objArrArr[j] );
                aaiftableline[j] = newLine;
            }
        } 
    	else
    	{
    		aaiftableline = (NOVCreateSMDLTableLine[]) super.createLines(obj);
    	}    	
    	return aaiftableline;
    }
    @Override
    public void setValueAt(Object obj, int row, int col)
    {
    	// TODO Auto-generated method stub
    	Object oldValue = getValueAt(row, col);
    	super.setValueAt(obj, row, col);
    	if(!oldValue.equals(obj))
    	{
    		AIFTableLine tableLine = getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			smdlTableLine.setRowUpdatedStatus(true);
    			fireTableRowsUpdated(row, row);
    		}
    	}
    }
}
