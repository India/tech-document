package com.nov.rac.smdl.databinding;

import org.eclipse.core.databinding.property.value.IValueProperty;

import com.nov.rac.utilities.databinding.beanproperties.NOVWidgetProperties;

public class NOVSMDLWidgetProperties extends NOVWidgetProperties 
{
	public static IValueProperty valueChanged()
    {
        return new NOVSMDLSwingValueProperty();
    }

}
