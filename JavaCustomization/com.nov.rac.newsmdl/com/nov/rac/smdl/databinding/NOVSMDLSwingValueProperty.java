package com.nov.rac.smdl.databinding;

import org.eclipse.core.databinding.property.value.IValueProperty;

import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.nov.rac.utilities.databinding.beanproperties.NOVSwingValueProperty;

public class NOVSMDLSwingValueProperty extends NOVSwingValueProperty 
{
	@Override
	protected IValueProperty doGetDelegate(Object obj) {
		
		if(obj instanceof NOVSMDLTableModel)
		{
			if(m_swingCompValue == null)
			{
				m_swingCompValue = new NOVSMDLTableModelChangeProperty();
			}
		}
		else 
		{
			m_swingCompValue = super.doGetDelegate(obj);
		}
		
		return m_swingCompValue;
	}

	private IValueProperty m_swingCompValue;
}
