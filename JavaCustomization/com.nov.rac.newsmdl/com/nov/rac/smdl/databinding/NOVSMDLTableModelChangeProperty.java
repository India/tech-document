package com.nov.rac.smdl.databinding;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.IProperty;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;

import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.nov.rac.utilities.databinding.beanproperties.NOVTableModelChangeProperty;

public class NOVSMDLTableModelChangeProperty extends NOVTableModelChangeProperty 
{
	public NOVSMDLTableModelChangeProperty() 
	{
		super();
	}
	@Override
	public Object getValueType() {
		return null;
	}

	public INativePropertyListener adaptListener(ISimplePropertyListener listener)
	{
		return new TableListener(this, listener);
	}
	private class TableListener extends NativePropertyListener
	implements TableModelListener //, PropertyChangeListener //
	{

		protected void doAddTo(Object source)
		{
			NOVSMDLTableModel tableComponent = (NOVSMDLTableModel)source;
			tableComponent.addTableModelListener(this);
			
			/* ********************************************** 
			 * When a table is being sorted,the Table Model *
			 * Listener might receive events from the Model * 
			 * Need to ignore those events while sorting.   * 
			 * ******************************************** */
			tableComponent.ignoreWhileSorting(this);
		}

		protected void doRemoveFrom(Object source)
		{
		}

		protected TableListener(IProperty property, ISimplePropertyListener listener)
		{
			super(property, listener);
		}
		
		@Override
		public void tableChanged(TableModelEvent tablemodelevent) {
			
			fireChange(tablemodelevent.getSource(), null);			
		}	
	}
	@Override
	protected Object doGetValue(Object source) 
	{		
		if(source != null)
		{
			return ((NOVSMDLTableModel)source).getAllData();
		}
		else
		{
			return null;
		}

	}

	@Override
	protected void doSetValue(Object source, Object val) 
	{

	}
}
