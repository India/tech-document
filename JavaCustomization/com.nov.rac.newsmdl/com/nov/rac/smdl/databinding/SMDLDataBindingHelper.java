package com.nov.rac.smdl.databinding;

import javax.swing.JTable;
import javax.swing.JTextField;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.IBeanListProperty;
import org.eclipse.core.databinding.beans.IBeanValueProperty;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.databinding.beanproperties.NOVBeanPropertyHelper;
import com.nov.rac.utilities.databinding.beanproperties.NOVWidgetProperties;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.util.DateButton;

public class SMDLDataBindingHelper
{

	protected DataBindingContext 		m_dataBindingContext = null;	

	public SMDLDataBindingHelper()
	{
		m_dataBindingContext = new DataBindingContext();
	}
	
	public DataBindingModel getDataModel(String dataModelName){

		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		
		return (DataBindingModel) dataBindingRegistry.getDataModel(dataModelName);
	}

	public void bindData(Text textWidget, String attrName,String dataModelName ) {
		
		IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(textWidget);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);
		
	}
	
	public void bindData(List listWidget, String attrName,String dataModelName ) {
		
		IObservableList widgetValue = WidgetProperties.items().observe(listWidget);
		IBeanListProperty val = NOVBeanPropertyHelper.list(DataBindingModel.class,attrName,null);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableList modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindList(widgetValue, modelValue);
		
		/*IObservableList listWidgetObs = SWTObservables.observeItems(listWidget);
		IBeanListProperty val = NOVBeanPropertyHelper.list(DataBindingModel.class,attrName,null);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableList modelValue = val.observe(bindingModel);
		System.out.println("modelvalue type: "+modelValue.getElementType());
		
		IConverter converter = new Converter(Object.class, String.class) 
		{
			
			@Override
			public Object convert(Object obj) {
				
				System.out.println("...."+obj.toString());
				return obj.toString();
			}

			@Override
			public Object getFromType() {
				// TODO Auto-generated method stub
				return Object.class;
			}

			@Override
			public Object getToType() {
				// TODO Auto-generated method stub
				return String.class;
			}
		};

		UpdateListStrategy strategy = new UpdateListStrategy(UpdateListStrategy.POLICY_UPDATE);
		strategy.setConverter(converter);
		m_dataBindingContext.bindList(listWidgetObs, modelValue, strategy , null);*/
		
//		IValueProperty wigetValue = NOVWidgetProperties.valueChanged();
//		
//		IObservableValue widgetValue = wigetValue.observe(listWidget);
//
//		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
//		
//		DataBindingModel bindingModel = getDataModel(dataModelName);
//
//		IObservableValue modelValue = val.observe(bindingModel);
//		m_dataBindingContext.bindValue(widgetValue, modelValue);
		
	}
	
	public void bindData(Combo comboWidget, String attrName,String dataModelName) {
		
		IObservableValue widgetValue = WidgetProperties.selection().observe(comboWidget);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);
		
	}
	
	public void bindData(DateButton dateButton, String attrName,String dataModelName) {
		
		IValueProperty wigetValue = NOVWidgetProperties.valueChanged();
		
		IObservableValue widgetValue = wigetValue.observe(dateButton);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);
		
	}
	
	public void bindData(JTextField txtField, String attrName,String dataModelName) {
		
		IValueProperty wigetValue = NOVWidgetProperties.valueChanged();
		
		IObservableValue widgetValue = wigetValue.observe(txtField);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);
		
	}

	public void bindData(JTable tableComponent, String attrName,String dataModelName) {
		
		IValueProperty wigetValue = NOVWidgetProperties.valueChanged();
		
		IObservableValue widgetValue = wigetValue.observe(tableComponent);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);	
		
	}
	
	public void bindData(AIFTableModel tableComponent, String attrName,String dataModelName) {
		
		IValueProperty wigetValue = NOVWidgetProperties.valueChanged();
		
		IObservableValue widgetValue = wigetValue.observe(tableComponent);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);	
		
	}

	public void bindData(Button buttonWidget, String attrName,String dataModelName) {

		IObservableValue widgetValue = WidgetProperties.selection().observe(buttonWidget);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		m_dataBindingContext.bindValue(widgetValue, modelValue);
		
	}
	
	public void bindValue(IObservableValue targetObservableValue, IObservableValue modelObservableValue)
	{
		m_dataBindingContext.bindValue(targetObservableValue, modelObservableValue);
	}
}
