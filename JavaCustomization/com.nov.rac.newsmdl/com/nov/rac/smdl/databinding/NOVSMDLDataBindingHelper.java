package com.nov.rac.smdl.databinding;

import org.eclipse.core.databinding.beans.IBeanValueProperty;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.property.value.IValueProperty;

import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.beanproperties.NOVBeanPropertyHelper;


public class NOVSMDLDataBindingHelper extends SMDLDataBindingHelper 
{
	public NOVSMDLDataBindingHelper()
	{
		super();
	}
	
	public void bindData(NOVSMDLTableModel tableComponent, String attrName,String dataModelName) 
	{
		IValueProperty wigetValue = NOVSMDLWidgetProperties.valueChanged();
		
		IObservableValue widgetValue = wigetValue.observe(tableComponent);

		IBeanValueProperty val = NOVBeanPropertyHelper.value(DataBindingModel.class,attrName);
		
		DataBindingModel bindingModel = getDataModel(dataModelName);

		IObservableValue modelValue = val.observe(bindingModel);
		
		m_dataBindingContext.bindValue(widgetValue, modelValue);		
	}
}
