package com.nov.rac.smdl.providers.node;

import com.nov.rac.kernel.ISMDLObject;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.providers.node.IFirstLevelNodeDescriptor;

public class SMDLRootNode implements IFirstLevelNodeDescriptor{
	
	public SMDLRootNode(){
		super();
	}

	public Object getData()
    {
        TCComponent rootNodeObject = null;
        
       	TCComponent targetObject = (TCComponent) AIFUtility.getTargetComponent();
			
       	if(targetObject instanceof ISMDLObject){
       		rootNodeObject =((ISMDLObject) targetObject).getOrder();
       	}
       	
        return rootNodeObject;
    }
}

