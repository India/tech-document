package com.nov.rac.smdl.providers.node;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ui.IViewPart;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.providers.node.IFirstLevelNodeDescriptor;
import com.teamcenter.rac.providers.node.NavigatorRoot;

public class SMDLNavigatorRoot extends NavigatorRoot implements IFirstLevelNodeDescriptor{
	
	private TCComponent	m_SMDLRootNode = null;

	public SMDLNavigatorRoot(IViewPart iviewpart, TCComponent targetOrder) {
		super(iviewpart);
		m_SMDLRootNode = targetOrder;
	}
	
	public void setSMDLRootNode(TCComponent rootNode){
		m_SMDLRootNode = rootNode;
	}

	
	@Override
	public List<Object> getFirstLevelDataObjects() {
		
		super.getFirstLevelDataObjects();
		
		return firstLevelDataObjects;
	}

	@Override
	public Object[] getElements() {

		ArrayList arraylist = new ArrayList();
		 arraylist.add(m_SMDLRootNode);
		 
		return arraylist.size() <= 0 ? new Object[0] : arraylist.toArray();
	}

	@Override
	public Object[] getFirstLevelDescriptorObjects() {
		
		firstLevelDataObjects.add(m_SMDLRootNode);
		firstLevelDescriptorObjects.add(this);
		
		return super.getFirstLevelDescriptorObjects();
	}

	@Override
	public Object getData() {
		// TODO Auto-generated method stub
		return m_SMDLRootNode;
	}
	
	

}
