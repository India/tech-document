package com.nov.rac.smdl.providers.node;

import com.nov.rac.kernel.NOV4ComponentSMDLHome;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.providers.node.IFirstLevelNodeDescriptor;

public class SMDLHomeFolderNode implements IFirstLevelNodeDescriptor{
	
	public SMDLHomeFolderNode(){
		super();
	}

	public Object getData()
    {
        TCComponent rootNodeObject = null;
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        
       	try {
       		TCComponent userHomeFolder = tcSession.getUser().getHomeFolder();
			rootNodeObject = (TCComponentFolder) userHomeFolder.getRelatedComponent(NOV4ComponentSMDLHome.SMDL_HOME_RELATION);
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	
        return rootNodeObject;
    }
}