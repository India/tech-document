package com.nov.rac.smdl.providers.providers;

import java.util.HashMap;
import java.util.Map;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLHome;
import com.nov.rac.smdl.utilities.ComponentHelper;
import com.nov.rac.smdl.utilities.NOVSMDLTreeSOAHelper;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.providers.TCComponentContentProvider;
import com.teamcenter.rac.providers.node.IRootNode;

public class SMDLHomeContentProvider extends SMDLContentProvider //extends TCComponentContentProvider implements ISMDLContentProvider
{
	
	
//	public Object[] getChildren(Object obj) {
//
//		if(obj == null)
//			return null;
//
//		obj = ComponentHelper.getComponent(obj);
//
//		Object[] relatedObjects = null;
////		TCComponent order=null;
////		Map relatedObjectMap =null;
////
////		if(obj instanceof ISMDLObject)
////		{
////			order= ((ISMDLObject)obj).getOrder();
////		}
////
////		if(!m_orderCache.containsKey(order) &&  null!=order)
////		{
////			//call soa & add it to cache;
////			loadSMDLHierarchy(order);
////		}
////		relatedObjectMap =	m_orderCache.get(order);
////
////		if(relatedObjectMap!=null)
////		{
////			relatedObjects = (Object[]) relatedObjectMap.get(obj);
////		}
////		
////		if(obj instanceof ISMDLObject)
////		{
////			order= ((ISMDLObject)obj).getOrder();
////		}
//
//		if(!m_theSMDLTreeOutputMap.containsKey(obj) &&  obj instanceof ISMDLObject)
//		{
//			//call soa & add it to cache;
//			loadSMDLHierarchy((TCComponent) obj);
//		}
//		
//		relatedObjects = m_theSMDLTreeOutputMap.get(obj);
//
//		if(relatedObjects == null && obj instanceof NOV4ComponentSMDL){
//			try {
//				relatedObjects =  ((TCComponentItem) obj).getTCProperty("revision_list").getReferenceValueArray();
//			} catch (TCException e) {
//				e.printStackTrace();
//			}
//		}
//		if (relatedObjects == null && obj instanceof TCComponentFolder)
//		{
//			relatedObjects = super.getChildren(obj);
//		}
//
//		return relatedObjects;
//
//	}

	@Override
	public Object[] getChildren(Object obj) {

		obj = ComponentHelper.getComponent(obj);
		
		Object[] relatedObjects = super.getChildren(obj);
				
		if (relatedObjects == null && obj instanceof TCComponentFolder)
		{
			try {
				relatedObjects = ((TCComponentFolder)obj).getChildren();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return relatedObjects;
	}

	public Object[] getElements(Object element) {
		if(element instanceof IRootNode){
			
			Object[] rootNodeObjects = ((IRootNode)element).getElements();
			Object	 smdlRootNode = null;
			
			if(rootNodeObjects != null && rootNodeObjects.length > 1){
				for(int iCnt = 0; iCnt < rootNodeObjects.length; iCnt++){
					if(rootNodeObjects[iCnt] instanceof NOV4ComponentSMDLHome){
						smdlRootNode = rootNodeObjects[iCnt];
						break;
					}
				}
			}
			
            return new Object[] {smdlRootNode};
		}
		else
			return getChildren(element);
	}
	
//	public void loadSMDLHierarchy(TCComponent order) {
//		
//		SMDLTreeOutput output = NOVSMDLTreeSOAHelper.getRelatedObjects(order);
//		
//		m_theSMDLTreeOutputMap.putAll(output.smdlRelatedObjects);
//		
////		m_orderCache.put((NOV4ComponentOrder)output.smdlOrderObject, output.smdlRelatedObjects);
//	}
	
//	public void addChild(TCComponent parentObject, TCComponent theCreatedObject) {
//		
//		TCComponent[] childrenList = m_theSMDLTreeOutputMap.get(parentObject);
//		
//		TCComponent[] newChildrenList = new TCComponent[childrenList.length + 1];
//		
//		System.arraycopy(childrenList, 0, newChildrenList, 0, childrenList.length);
//		
//		newChildrenList[childrenList.length] = theCreatedObject;
//			
//		m_theSMDLTreeOutputMap.put(parentObject,newChildrenList);
//	}
//	
////	Map<NOV4ComponentOrder,Map> m_orderCache=new HashMap<NOV4ComponentOrder, Map>();
//	
//	private Map<TCComponent, TCComponent[]> m_theSMDLTreeOutputMap = new HashMap<TCComponent, TCComponent[]>();
//	
//	private SMDLTreeOutput theSMDLTreeOutput = null;

}