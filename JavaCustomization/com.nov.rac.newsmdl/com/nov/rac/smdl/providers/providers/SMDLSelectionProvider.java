package com.nov.rac.smdl.providers.providers;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;

public class SMDLSelectionProvider {
	
	public static TCComponent getSelectedComponent(ISelection selection, String objectTypeName){
		
		TCComponent smdlComponent = null;
		
		if (selection instanceof TreeSelection) {
			
			Object firstElement = ((TreeSelection)selection).getFirstElement();
			
			if(firstElement instanceof AIFComponentContext){
			
				AIFComponentContext selectedObject = (AIFComponentContext) ((TreeSelection)selection).getFirstElement();
				smdlComponent = (TCComponent) selectedObject.getComponent();
			}
			else if(firstElement instanceof ISMDLObject){
				smdlComponent = (TCComponent) firstElement;
			}
        	
        	
        	
        	if(smdlComponent != null && smdlComponent.getType().compareTo(objectTypeName) != 0)
			{
        		smdlComponent = null;
			}                
        }
		
		return smdlComponent;
		
	}

}
