package com.nov.rac.smdl.providers.providers;

import com.teamcenter.rac.kernel.TCComponent;

public interface ISMDLContentProvider {
	
	public void loadSMDLHierarchy(TCComponent targetObject);

	public void addChild(TCComponent parentObject, TCComponent theCreatedObject);

	public void removeChild(TCComponent parentObject, TCComponent theRemovedObject);

}
