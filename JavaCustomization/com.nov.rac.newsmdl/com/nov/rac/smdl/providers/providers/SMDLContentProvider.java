package com.nov.rac.smdl.providers.providers;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.utilities.ComponentHelper;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.smdl.utilities.NOVSMDLTreeSOAHelper;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.providers.TCComponentContentProvider;
import com.teamcenter.soa.common.PolicyProperty;

public class SMDLContentProvider extends TCComponentContentProvider implements ISMDLContentProvider{

	public Object[] getChildren(Object obj) {

		if(obj == null)
			return null;

		obj = ComponentHelper.getComponent(obj);

		Object[] relatedObjects = null;

		if(!m_theSMDLTreeOutputMap.containsKey(obj) &&  obj instanceof ISMDLObject)
		{
			//get order obj & check if order is present in m_theSMDLTreeOutputMap
			//if order found in m_theSMDLTreeOutputMap, obj has to be smdlRevObj
			//skip soa call for smdlRevObj
			
			TCComponent orderObject = ((ISMDLObject)obj).getOrder();
			if(	!m_theSMDLTreeOutputMap.containsKey(orderObject)		)
			{
				//call soa & add it to cache;
				loadSMDLHierarchy((TCComponent) obj);
			}
		}
		
		relatedObjects = m_theSMDLTreeOutputMap.get(obj);

		if(relatedObjects == null && obj instanceof NOV4ComponentSMDL)
		{
			obj = getSMDLRevObjProperties((TCComponent)obj);
			try 
			{
				relatedObjects =  ((NOV4ComponentSMDL) obj).getTCProperty("revision_list").getReferenceValueArray();
			} 
			catch (TCException e) {
				e.printStackTrace();
			}
		}
		if (relatedObjects == null && obj instanceof NOV4ComponentSMDLRevision)
		{
			relatedObjects = getRelatedDatasets((TCComponent)obj);
		}

		return relatedObjects;

	}
	
	private Object[] getRelatedDatasets(TCComponent obj) 
	{
		TCComponent[] relatedcomp =null;	
		Vector<TCComponent> pdfDatasets = null;
		
		try 
		{
			relatedcomp = obj.getRelatedComponents("IMAN_manifestation");
			pdfDatasets =new Vector<TCComponent>();
			for (int inx = 0; inx < relatedcomp.length; inx++) 
			{			
				String docType = relatedcomp[inx].getType().toString();
				
				if(docType.equalsIgnoreCase("PDF"))
				{
					pdfDatasets.add(relatedcomp[inx]);
				}		
			}
			
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pdfDatasets.toArray();
	}
	public void loadSMDLHierarchy(TCComponent targetObject) {
		SMDLTreeOutput theTreeOutput = NOVSMDLTreeSOAHelper.getRelatedObjects(targetObject);

		//m_theSMDLTreeOutputMap.putAll(theTreeOutput.smdlRelatedObjects);
		fillOutputMap(theTreeOutput.smdlOrderObject,theTreeOutput.smdlRelatedObjects, m_theSMDLTreeOutputMap );
	}
	
	private void fillOutputMap( TCComponent rootObject,Map inputMap, Map targetMap) 
	{
		//find root obj in map,  make its <k,v> entry in target map 
		TCComponent[] relatedObjectsOfRoot = (TCComponent[]) inputMap.get(rootObject);
		if(relatedObjectsOfRoot!=null)
		{
			targetMap.put(rootObject, relatedObjectsOfRoot);
			
			//make entries for each of its relatedObject
			iterateChildren(relatedObjectsOfRoot,inputMap, targetMap);
		}
		else
		{
			targetMap.put(rootObject, null);
		}
		
	}

	private void iterateChildren(TCComponent[] relatedObjects,Map inputMap, Map targetMap)
	{
		for(TCComponent component : relatedObjects)
		{
			TCComponent[] relatedChildrenObjects = (TCComponent[]) inputMap.get(component);
			if(relatedChildrenObjects!=null)
			{
				targetMap.put(component, relatedChildrenObjects);
				iterateChildren(relatedChildrenObjects,inputMap, targetMap);
			}
			else
			{
				targetMap.put(component, null);
			}
		}
	}

	public void addChild(TCComponent parentObject, TCComponent theCreatedObject) {
		
        if(parentObject != null)
        {

			TCComponent[] childrenList = m_theSMDLTreeOutputMap.get(parentObject);
			
			TCComponent[] newChildrenList = null;
			
			if(childrenList != null && childrenList.length > 0){
			
				newChildrenList = new TCComponent[childrenList.length + 1];
				
				System.arraycopy(childrenList, 0, newChildrenList, 0, childrenList.length);
				
				newChildrenList[childrenList.length] = theCreatedObject;
			}
			else{
				newChildrenList = new TCComponent[1];
				newChildrenList[0] = theCreatedObject;
			}
				
				
			m_theSMDLTreeOutputMap.put(parentObject,newChildrenList);
        }
	}
	
	public void removeChild(TCComponent parentObject, TCComponent theRemovedObject) {
        if(parentObject != null) {
        	
			TCComponent[] childrenList = m_theSMDLTreeOutputMap.get(parentObject);
			TCComponent[] newChildrenList = null;
			
			if(childrenList != null)
			{
				for (int i = 0; i < childrenList.length; i++)  {         
					if (childrenList[i] == theRemovedObject) {             
						newChildrenList = new TCComponent[childrenList.length - 1];            
						System.arraycopy(childrenList, 0, newChildrenList, 0, i);             
						System.arraycopy(childrenList, i+1, newChildrenList, i, childrenList.length-i-1);             
					}     
				}
				m_theSMDLTreeOutputMap.put(parentObject,newChildrenList);
			}
        }
	}
	
	private TCComponent getSMDLRevObjProperties(TCComponent smdlComponent)
	{
		TCComponent smdlSOAObject = null;
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(smdlComponent);
		
		String[] smdlAttributeList = getAttributeList();
		
		NOVSMDLPolicy smdlPolicy = createSMDLPolicy();
		
		smdlSOAObject = dmSOAHelper.getSMDLProperties(smdlComponent,smdlAttributeList,smdlPolicy);
		
		return smdlSOAObject;
	}
	
	private String[] getAttributeList() 
	{

		String[] smdlAttributeList = {
										NOV4ComponentSMDL.SMDL_REVISION_LIST
									  };
		return smdlAttributeList;
	}
	
	private  NOVSMDLPolicy createSMDLPolicy() 
	{
		NOVSMDLPolicy smdlPolicy = new NOVSMDLPolicy();

		// Add Property policy for SMDL Revision object
		Vector<NOVSMDLPolicyProperty> smdlRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_NAME,PolicyProperty.AS_ATTRIBUTE));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_TYPE,PolicyProperty.AS_ATTRIBUTE));
		
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_RELEASE_STATUS,PolicyProperty.WITH_PROPERTIES));
		
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_PROCESS_STAGELIST,PolicyProperty.WITH_PROPERTIES));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_STRING,PolicyProperty.AS_ATTRIBUTE));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_OWNING_SITE,PolicyProperty.AS_ATTRIBUTE));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_IS_VI,PolicyProperty.AS_ATTRIBUTE));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_HAS_MODULE_VARIANT,PolicyProperty.AS_ATTRIBUTE));
		
		NOVSMDLPolicyProperty[] smdlRevPolicyPropArr = smdlRevPolicyProps.toArray(new NOVSMDLPolicyProperty[smdlRevPolicyProps.size()]);
		smdlPolicy.addPropertiesPolicy(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE, smdlRevPolicyPropArr);
		
		// Add Property policy for Release Status object
		Vector<NOVSMDLPolicyProperty> relStatusPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		relStatusPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REL_STATUS_NAME,PolicyProperty.AS_ATTRIBUTE));
		
		NOVSMDLPolicyProperty[] relStatusPolicyPropArr = relStatusPolicyProps.toArray(new NOVSMDLPolicyProperty[relStatusPolicyProps.size()]);
		
		smdlPolicy.addPropertiesPolicy(NOVSMDLConstants.REL_STATUS_OBJECT_TYPE_NAME, relStatusPolicyPropArr);

		return smdlPolicy;
	}
	
	// This Map will be shared by all the SMDL Home View and SMLD Explorer Views.
	private static Map<TCComponent, TCComponent[]> m_theSMDLTreeOutputMap = new HashMap<TCComponent, TCComponent[]>();

}
