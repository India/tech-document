package com.nov.rac.smdl.providers.providers;

import org.eclipse.jface.viewers.LabelProvider;

import com.teamcenter.rac.kernel.TCComponent;

public class SMDLLabelProvider extends LabelProvider {
	public String getText(Object element) {
		return ((TCComponent)element).toString();
	}
}
