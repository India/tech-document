package com.nov.rac.smdl.providers.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.smdl.utilities.NOVSMDLTreeSOAHelper;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.providers.TCComponentContentProvider;
import com.teamcenter.rac.providers.node.IRootNode;

public class SMDLExplorerContentProvider extends SMDLContentProvider //TCComponentContentProvider implements ISMDLContentProvider
{
	
//	public Object[] getChildren(Object obj) {
//		
//		Object[] relatedObjects = null;
//		
//		if(obj == null)
//            return null;
//		
//		if(!m_theSMDLTreeOutputMap.containsKey(obj) && obj instanceof ISMDLObject){
//			loadSMDLHierarchy((TCComponent) obj);
//		}
//		
//		relatedObjects = (Object[]) m_theSMDLTreeOutputMap.get(obj);
//		
//		if(relatedObjects == null && obj instanceof NOV4ComponentSMDL){
//			try {
//				relatedObjects =  ((TCComponentItem) obj).getTCProperty("revision_list").getReferenceValueArray();
//			} catch (TCException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return relatedObjects;
//		
//	}
//
//	public void loadSMDLHierarchy(TCComponent targetObject) {
//		SMDLTreeOutput theTreeOutput = NOVSMDLTreeSOAHelper.getRelatedObjects(targetObject);
//		
//		m_theSMDLTreeOutputMap.putAll(theTreeOutput.smdlRelatedObjects);
////		m_orderObject = theTreeOutput.smdlOrderObject;
//	}

	public Object[] getElements(Object element) {
		if(element instanceof IRootNode){
			
			Object[] rootNodeObjects = ((IRootNode)element).getElements();
			Object	 smdlRootNode = null;
			
			if(rootNodeObjects != null){
				for(int iCnt = 0; iCnt < rootNodeObjects.length; iCnt++){
					
					if(rootNodeObjects[iCnt] instanceof AIFComponentContext){
						
						TCComponent rootNode = (TCComponent) ((AIFComponentContext)rootNodeObjects[iCnt]).getComponent();
					
						if(rootNode instanceof ISMDLObject){
							smdlRootNode = rootNode;
							break;
						}
					}
				}
			}
			
            return new Object[] {smdlRootNode};
		}
		else
			return getChildren(element);
	}

//	public void addChild(TCComponent parentObject, TCComponent theCreatedObject) {
//		
//		TCComponent[] childrenList = m_theSMDLTreeOutputMap.get(parentObject);
//		
//		TCComponent[] newChildrenList = new TCComponent[childrenList.length + 1];
//		
//		System.arraycopy(childrenList, 0, newChildrenList, 0, childrenList.length);
//		
//		newChildrenList[childrenList.length] = theCreatedObject;
//			
//		m_theSMDLTreeOutputMap.put(parentObject,newChildrenList);
//	}
//	
//	private Map<TCComponent, TCComponent[]> m_theSMDLTreeOutputMap = new HashMap<TCComponent, TCComponent[]>();
//	private TCComponent m_orderObject = null;
	
}