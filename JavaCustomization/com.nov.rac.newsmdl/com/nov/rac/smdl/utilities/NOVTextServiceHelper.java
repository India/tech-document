package com.nov.rac.smdl.utilities;

import java.util.HashMap;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.SessionService;
import com.teamcenter.services.rac.core._2008_06.Session.GetDisplayStringsResponse;

public class NOVTextServiceHelper {
	
	private String[] m_inputKeys = null;
	private HashMap<String, String> m_NameValueMap = null;
	
	public NOVTextServiceHelper(String[] inputKeys){
		m_inputKeys = inputKeys;
		
		invokeGetDisplayStrService();
	}
	
	private void invokeGetDisplayStrService(){
		TCSession tcSession = (TCSession)AIFUtility.getDefaultSession();
		
		SessionService sessionService = SessionService.getService(tcSession);
		
		 GetDisplayStringsResponse theResponse = sessionService.getDisplayStrings(m_inputKeys);
		 
		 for(int iCnt = 0; iCnt < theResponse.output.length; iCnt++){
			 m_NameValueMap.put(theResponse.output[iCnt].key,theResponse.output[iCnt].value);
		 }
	}
	
	public HashMap getDisplayStrings(){
		return m_NameValueMap;
		
	}

}
