/*
 * Created on Dec 29, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.nov.rac.smdl.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.nov.tracker.webservices.ArrayOfPlant;
import com.nov.tracker.webservices.GetPlantsProxy;

/**
 * @author hughests
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class JIBPlantLOVList {

    Hashtable stringToRef = new Hashtable();
    Hashtable refToString = new Hashtable();
    Vector    orderedVector = new Vector();
    int[]     customerHash = null;
    int[]     plantHash = null;
    
    ArrayList excludeList = new ArrayList();
    
    final int FORM=0;
    final int POM_QUERY=1;
    /**
     * 
     */
    public JIBPlantLOVList() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() {
        
        try {
        	/*
        	V16Proxy proxy = new V16Proxy();
        	
        	proxy.setEndpoint("http://"+cetgServer+":"+cetgPort+cetgContext+"V1.6/CETGServices");
        	CredentialsInfo cred = proxy.getCredentials("xxxxxxxxx","xxxxxxx");
        	PlantForm[] plants = proxy.getPlants(cred);
        	*/
            GetPlantsProxy pproxy = new GetPlantsProxy();
            ArrayOfPlant plants = pproxy.getPlants();
        	
			//Vector v = new Vector();
			//v.add("");
            if (plants.getPlant() == null) {
            	orderedVector.add("Could not get Plant List from Tracker!");
            	return;
            }
            customerHash = new int[plants.getPlant().length];
            plantHash = new int[plants.getPlant().length];
			for (int i=0;i<plants.getPlant().length;i++) {
				refToString.put(new Integer(plants.getPlant(i).getId()),plants.getPlant(i).getPlantName());
				stringToRef.put(plants.getPlant(i).getPlantName(),new Integer(plants.getPlant(i).getId()));
				customerHash[i] = plants.getPlant(i).getCustomerId();
				plantHash[i] = plants.getPlant(i).getId();
				orderedVector.add(plants.getPlant(i).getPlantName());
			}
			
			Object[] theArray = orderedVector.toArray();
			Arrays.sort(theArray);
			orderedVector = new Vector();
			for (int i=0;i<theArray.length;i++)
				orderedVector.add(theArray[i]);
			
        }
        catch (Exception e) {
            System.out.println("Could not retrieve values for AbstractReferenceLOVList: "+e);
        }
        
    }
    
    public String getStringFromReference(Integer i) {
    	if (i == null) return "";
        return (String)refToString.get(i);
    }
    public Integer getReferenceFromString(String s) {
    	if (s == null) return null;
        return (Integer)stringToRef.get(s);
    }
    
    public void addExclusion(String s) {
        excludeList.add(s);
    }
    
    public void removeExclusion(String s) {
        excludeList.remove(s);
    }
    public void addItem(Integer c,String s) {
        refToString.put(c,s);
        stringToRef.put(s,c);
    }
    public void removeItem(Integer c,String s) {
        refToString.remove(c);
        stringToRef.remove(s);
    }
    public String[] getList() {
        
        Enumeration e = stringToRef.keys();
        ArrayList firstPass = new ArrayList();
        
        while(e.hasMoreElements()) {
            String val = (String)e.nextElement();
            boolean exclude = false;
            if (excludeList.size() > 0) {
                for (int j=0;j<excludeList.size();j++) {
                    if (val.equalsIgnoreCase((String)excludeList.get(j)) )
                        exclude = true;
                }
            }
            if (exclude)
                ;
            else {
                firstPass.add(val);
            }
        }
        String[] retArr = new String[firstPass.size()];
        for (int i=0;i<firstPass.size();i++) 
            retArr[i] = (String)firstPass.get(i);
        
        Arrays.sort(retArr);
        
        return retArr;
    }
    public String[] getList(int customerId) {
        
        Enumeration e = stringToRef.keys();
        ArrayList firstPass = new ArrayList();
        ArrayList secondPass = new ArrayList();
        
        for (int i=0;i<customerHash.length;i++) {
        	if (customerHash[i] == customerId) {
        		firstPass.add(new Integer(plantHash[i]));
        	}
        }
        for (int i=0;i<firstPass.size();i++) {
        	if (refToString.get(firstPass.get(i)) != null) {
        		secondPass.add((String)refToString.get(firstPass.get(i)));
        	}
        }
        String[] retArr = new String[secondPass.size()];
        for (int i=0;i<secondPass.size();i++) 
            retArr[i] = (String)secondPass.get(i);
        
        Arrays.sort(retArr);
        
        return retArr;
    }

    public Integer[] getReferences() {
    	
        Enumeration e = stringToRef.keys();
        ArrayList firstPass = new ArrayList();
        
        while(e.hasMoreElements()) {
            String val = (String)e.nextElement();
            boolean exclude = false;
            if (excludeList.size() > 0) {
                for (int j=0;j<excludeList.size();j++) {
                    if (val.equalsIgnoreCase((String)excludeList.get(j)) )
                        exclude = true;
                }
            }
            if (exclude)
                ;
            else {
                firstPass.add(val);
            }
        }
        Integer[] retArr = new Integer[firstPass.size()];
        for (int i=0;i<firstPass.size();i++) 
        	retArr[i] = (Integer)stringToRef.get((String)firstPass.get(i));
        
        return retArr;
    }
    
    public Hashtable getStringToRefTable() { return stringToRef; }
    public Hashtable getRefToStringTable() { return refToString; }
    public Vector    getOrderedVector()    { return orderedVector; }
}
