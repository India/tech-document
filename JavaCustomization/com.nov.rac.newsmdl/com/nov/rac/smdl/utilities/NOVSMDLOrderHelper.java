package com.nov.rac.smdl.utilities;

import com.noi.util.components.JIBCustomerLOVList;
import com.noi.util.components.JIBPlantLOVList;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLOrderHelper {
	
	private static JIBCustomerLOVList m_customers = null;
	private static JIBPlantLOVList m_soaPlantList = null;

	public static Boolean isOrderEditable() {
    Boolean editOrder = false;		

    Registry appReg = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
    
	TCSession session = (TCSession)AIFUtility.getDefaultSession();
    String grpName = session.getCurrentGroup().toString();
    TCPreferenceService prefServ = session.getPreferenceService();
    if ( prefServ != null ) {
        String prefGrpNames[] = prefServ.getStringArray(TCPreferenceService.TC_preference_site, appReg.getString("OrderFormEdit.PREF"));
        if(prefGrpNames!= null) {
        	int grpLen = prefGrpNames.length;
            for(int inx=0;inx<grpLen;inx++) {
            	if(prefGrpNames[inx].equalsIgnoreCase(grpName)) {
            		editOrder = true;
            		break;
            	}
            }
        }
    }
    return editOrder;
   }
	
	public static JIBCustomerLOVList getCustomerList() {
		if (m_customers == null) {
			m_customers = new JIBCustomerLOVList();
	        m_customers.init();
        }		
	    return m_customers;
	}
	
	public static JIBPlantLOVList getPlantList() {
        if (m_soaPlantList == null) {
        	m_soaPlantList = new JIBPlantLOVList();
        	m_soaPlantList.init();
        } 		
	    return m_soaPlantList;
	}	
	
	public static String[] getOrderDPTableColumnNames() {
	    Registry appReg = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
	    TCPreferenceService prefServ = ((TCSession)AIFUtility.getDefaultSession()).getPreferenceService();
        String ordDpTabColNames[] = prefServ.getStringArray(TCPreferenceService.TC_preference_site, appReg.getString("OrderDPTableColNames.PREF"));
		if (null != ordDpTabColNames && ordDpTabColNames.length == 0) {
			MessageBox.post(appReg.getString("OrderDPTableColNames.PREF") + " " + appReg.getString("prefError.MSG"), "ERROR", MessageBox.ERROR);
		}        
	    return ordDpTabColNames;
	}	

	public static String[] getOrderDPTableColumnDisplayNames() {
	    Registry appReg = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
	    TCPreferenceService prefServ = ((TCSession)AIFUtility.getDefaultSession()).getPreferenceService();
        String ordDpTabColDispNames[] = prefServ.getStringArray(TCPreferenceService.TC_preference_site, appReg.getString("OrderDPTableColDispNames.PREF"));
		if (null != ordDpTabColDispNames && ordDpTabColDispNames.length == 0) {
			MessageBox.post(appReg.getString("OrderDPTableColDispNames.PREF") + " " + appReg.getString("prefError.MSG"), "ERROR", MessageBox.ERROR);
		}         
	    return ordDpTabColDispNames;
	}
}
