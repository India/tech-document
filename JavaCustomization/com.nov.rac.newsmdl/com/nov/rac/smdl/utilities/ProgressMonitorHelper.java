package com.nov.rac.smdl.utilities;

import org.eclipse.core.runtime.IProgressMonitor;

public class ProgressMonitorHelper 
{

	private IProgressMonitor m_progressMonitor;

	public ProgressMonitorHelper(IProgressMonitor progressMonitor)
	{
		m_progressMonitor= progressMonitor;
	}
	
	public void beginTask(String taskName, int interval) 
	{
		if(m_progressMonitor!=null)
		{
			(m_progressMonitor).beginTask(taskName,interval);
		}
	 
	}

	public void done() 
	{
		if(m_progressMonitor!=null)
		{
			m_progressMonitor.done();
		}
	}

	
	
}
