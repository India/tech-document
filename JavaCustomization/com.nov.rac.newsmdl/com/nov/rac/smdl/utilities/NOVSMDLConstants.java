package com.nov.rac.smdl.utilities;

public class NOVSMDLConstants
{
	//Preferences
	public final static String NOVSMDL_TABLECOLUMNS_PREF = "NOV4_SMDL_column_names";
	public final static String NOVSMDL_STRIKETHROUGH_ALLTABLECOLUMNS_PREF = "NOV4_SMDL_StrikeThrough_AllColumns";
	public final static String NOVSMDL_STRIKETHROUGH_DOCUMNETCOLUMNS_PREF = "NOV4_SMDL_StrikeThrough_DocumentColumns";
	public final static String HOME_TREE_DISP_ORDER_PREF = "treeDisplayOrder";
		
	//Documents Attributes
	public final static String DOCUMENT_NUMBER = "item_id";
	public final static String DOCUMENT_TITLE = "object_name";
	public final static String DOCUMENT_REV_ID = "item_revision_id";
	public final static String DOCUMENT_STATUS = "release_status_list";
	public final static String DOCUMENT_STATUS_NAME = "name";
	public final static String DOCUMENT_ITEM_REF = "items_tag";
	
	public final static String DOCUMENT_OBJECT_STRING = "object_string";
	public final static String DOCUMENT_OBJECT_DESC = "object_desc";
	
	public final static String DOC_OBJECT_TYPE_NAME = "Documents";
	public final static String DOCREV_OBJECT_TYPE_NAME = "Documents Revision";
	public final static String REL_STATUS_OBJECT_TYPE_NAME = "ReleaseStatus";
	public final static String REL_STATUS_NAME = "name";
	
	public final static String LOCATION_OBJECT_TYPE_NAME = "_novlocations_";
	public final static String LOCATION_NAME = "hcmname";

	public final static String PREF_SMDL_SEARCH = "SMDL20_order_dp_smdl";
	public final static String REVISION_LIST = "revision_list";
	
	public final static String SMDL_PROCESS_STATUS = "process_stage_list";
	public final static String SMDL_RELEASE_STATUS = "release_status_list";
	
	public final static String SMDLID_COL_SEARCHTABLE = "smdl_id";
	public final static String DOCUMENT_OWNING_GROUP = "owning_group";
	
	public NOVSMDLConstants()
	{
	} 
}