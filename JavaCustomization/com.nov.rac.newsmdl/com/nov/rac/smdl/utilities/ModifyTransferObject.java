package com.nov.rac.smdl.utilities;

import java.util.Arrays;
import java.util.Set;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

//TCDECREL-4526
public class ModifyTransferObject 
{
	public void retainModifiedValues(IPropertyMap [] inputPropMaps, IPropertyMap [] returnPropMaps) 
	            throws TCException
	{	
		for(int inx=0; inx< inputPropMaps.length; inx++)
		{
			retainModifiedValues(inputPropMaps[inx],returnPropMaps[inx]);
		}
	}

	public IPropertyMap retainModifiedValues(IPropertyMap inputPropMap,
			IPropertyMap returnPropMap) throws TCException
	{		
		// Check if it has a component associated.
		TCComponent theComponent = inputPropMap.getComponent();
		
		if(theComponent != null)
		{    	
			// 1.1 Get the string Property Names.

			Set<String> propNames = inputPropMap.getStringPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_string , false, returnPropMap);
			// 1.1 Get the string array Property Names.
			propNames = inputPropMap.getStringArrayPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_string , true, returnPropMap);
	
			// 1.1 Get the integer Property Names.
			propNames = inputPropMap.getIntegerPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_int , false, returnPropMap);
			// 1.1 Get the integer array Property Names.
			propNames = inputPropMap.getIntegerArrayPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_int , true, returnPropMap);
	
			// 1.1 Get the date Property Names.
			propNames = inputPropMap.getDatePropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_date, false, returnPropMap);
			// 1.1 Get the date array Property Names.
			propNames = inputPropMap.getDateArrayPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_date, true, returnPropMap);
	
			// 1.1 Get the double Property Names.
			propNames = inputPropMap.getDoublePropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_double , false, returnPropMap);
			// 1.1 Get the double array Property Names.
			propNames = inputPropMap.getDoubleArrayPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_double , true, returnPropMap);
	
			// 1.1 Get the logical Property Names.
			propNames = inputPropMap.getLogicalPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_logical , false, returnPropMap);
			// 1.1 Get the logical array Property Names.
			propNames = inputPropMap.getLogicalArrayPropNames();	
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_logical , true, returnPropMap);
	
			// 1.1 Get the compound Property Names.
			propNames = inputPropMap.getCompoundPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_compound , false, returnPropMap);
			// 1.1 Get the compound array Property Names.
			propNames = inputPropMap.getCompoundArrayPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_compound , true, returnPropMap);
			
			
			// 1.1 Get the tag Property Names.
			propNames = inputPropMap.getTagPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_typed_reference , false, returnPropMap);
			// 1.1 Get the tag array Property Names.
			propNames = inputPropMap.getTagArrayPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_typed_reference , true, returnPropMap);
			
			// 1.1 Get the tag Property Names.
			propNames = inputPropMap.getTagPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_untyped_reference , false, returnPropMap);
			// 1.1 Get the tag array Property Names.
			propNames = inputPropMap.getTagArrayPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_untyped_reference , true, returnPropMap);
			
			// 1.1 Get the tag Property Names.
			propNames = inputPropMap.getTagPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_external_reference , false, returnPropMap);
			// 1.1 Get the tag array Property Names.
			propNames = inputPropMap.getTagArrayPropNames();
			// 1.2 For each property Name, get theComponent.hasPropertyCached(prop_name)
			copyProperties(inputPropMap, propNames, TCProperty.PROP_external_reference , true, returnPropMap);
			
			// Copy Component from input property map to returnPropMap
			returnPropMap.setComponent(inputPropMap.getComponent());
			
		} // if(theComponent != null)	
		return returnPropMap;
	}

	
	private void copyProperties(IPropertyMap inputPropMap, Set<String> propNames, int propertyType, boolean isArray,
			IPropertyMap returnPropMap) throws TCException 
	{
		boolean canCopyProperty;
		
		TCComponent theComponent = inputPropMap.getComponent();
		
		if( !propNames.isEmpty() )
		{			
		    for( String propertyName : propNames)
		    {
		    	canCopyProperty = true;   	
		    	boolean hasProperty = theComponent.hasPropertyCached(propertyName);
		    	
		    	// 1.3 If property name is found in the cache, then
				// theComponent.getTCProperty( prop_name )
		    	if( hasProperty )
		    	{
		    		TCProperty theProperty = theComponent.getTCProperty(propertyName);
		    		
		    		// 1.4 From TCProperty object, get the value and compare it with value
		    		// from IPropertyMap.		    		
		    		if( theProperty != null)
		    		{
		    			//Object	propValue = theProperty.getPropertyValue();		
		    			Object	propValue = getPropertyValue(theProperty);
		    			// 1.5 If the value is SAME, do NOT copy the value into returnMap.		    			
		    			Object valueFromPropMap = PropertyMapHelper.getPropertyValue(propertyName, propertyType, isArray, inputPropMap);
		    			
		    			if(compareObjects(propValue,valueFromPropMap))
		    			{
		    				canCopyProperty = false;
		    			}
		    		} // theProperty
		    	} // hasProperty
		    	
		    	if(canCopyProperty)
		    	{
		    		Object propertyValue = PropertyMapHelper.getPropertyValue(propertyName, propertyType, isArray, inputPropMap);
		    		PropertyMapHelper.addPropertyValueToMap(propertyName,propertyValue,propertyType,isArray, returnPropMap);		    		
		    	}
		    	
		    } // for
		    
		} // if( !propNames.isEmpty() )
	}
	
	private boolean compareObjects(Object lhsObject, Object rhsObject)
	{
        boolean isEqual = false;
        
        if(lhsObject == null && (rhsObject == null || rhsObject.equals("")))
        {
        	isEqual = true;
        	return isEqual; 
        }    
        
		if(lhsObject != null)
		{
			if(lhsObject.equals(rhsObject))
			{
				isEqual = true;	
			}			
			else if(lhsObject.getClass().isArray() && rhsObject.getClass().isArray()) // Check if lhs is an array
			{
			    Object[] lhsObjArray = (Object[]) lhsObject;
			    Object[] rhsObjArray = (Object[]) rhsObject;		    
			  
			    if(Arrays.equals(lhsObjArray,rhsObjArray))		    
			    {
			    	isEqual = true;
			    }			    
			}	
		}
		
		return isEqual;
	}
	
	 //Check if its referenced property/ referenced array property
	private Object getPropertyValue(TCProperty theProperty)
	{
		Object propValue;
		if( theProperty.isReferenceType() )
		{
			if(theProperty.isNotArray())
			{
				propValue = theProperty.getReferenceValue();
			}
			else
			{
		 	    propValue = theProperty.getReferenceValueArray();
			}
		}
		else
		{
			propValue = theProperty.getPropertyValue();
		}
		
		return propValue;
	} // private Object getPropertyValue(TCProperty theProperty)
	
	
	//Creating clones according to the required operation mode
	public  CreateInObjectHelper[] cloneCreateInObjectProperties( CreateInObjectHelper[] inputObjects , int requiredOperationModes )
	{	
		// 1.0 Create an array of CreateInObjectHelper[] object. The size of the array should be save as inputObjects.size
		int iSize = inputObjects.length;
		
		CreateInObjectHelper[] outputObjects = new CreateInObjectHelper[iSize];
		
		for (int inx = 0; inx < iSize; inx++)
		{
			outputObjects[inx] = cloneCreateInObjectProperties(inputObjects[inx], requiredOperationModes);
		}
		
		return outputObjects;
	}
		
	public  CreateInObjectHelper cloneCreateInObjectProperties( CreateInObjectHelper inputObject , int requiredOperationModes )
	{
	
		String inputObjectType;
		int inputOperationMode;
		CreateInObjectHelper outputObject;
		
		// 2.0 For each inputObject, get its Type (as String) and its OperationMode (as int).
		inputObjectType = inputObject.getType();
		inputOperationMode = inputObject.getOperationMode();		
			
		// 2.1 If the inputObject[inx].mode is not in required Operation mode, then
		// Create a new CreateInObjectHelper object for the given index.
		if ( (inputOperationMode & requiredOperationModes) != inputOperationMode )
		{
			outputObject = new CreateInObjectHelper( inputObjectType, inputObject.getOprerationMode(inputOperationMode) );
		}
		
		// 2.2 If the inputObject[inx].mode is same as required Operation mode,
		// then copy the inputObject[inx] directly into outputObject[inx].
		// outputObject[inx] = inputObject[inx].
		else
		{
			outputObject = inputObject;
		}
		
		return outputObject;
	}	//cloneCreateInObjectProperties End
}
//TCDECREL-4526 End