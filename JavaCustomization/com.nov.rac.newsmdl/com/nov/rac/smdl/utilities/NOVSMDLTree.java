package com.nov.rac.smdl.utilities;


import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;


public class NOVSMDLTree extends TCTree{
	
		
	private SMDLTreeOutput theSMDLTreeOutput = null;
	
	@Override
	public Object[] getChildren(Object obj, Object obj1) {
		
		Object[] relatedObjects = null;
		
		
		if(obj == null)
            return null;
		
		relatedObjects = (Object[]) theSMDLTreeOutput.smdlRelatedObjects.get(obj);
		
		if(relatedObjects == null && obj instanceof NOV4ComponentSMDL){
			try {
				relatedObjects =  ((TCComponentItem) obj).getTCProperty("revision_list").getReferenceValueArray();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return relatedObjects;
			
	}

	public void setSMDLTreeOutput(SMDLTreeOutput theSMDLOutput) {
		
		theSMDLTreeOutput =  theSMDLOutput;
	}

}
