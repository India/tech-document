package com.nov.rac.smdl.utilities;

import java.util.Map;

import com.teamcenter.soa.common.ObjectPropertyPolicy;
import com.teamcenter.soa.common.PolicyProperty;
import com.teamcenter.soa.common.PolicyType;

public class NOVSMDLPolicy{
	
	private ObjectPropertyPolicy m_smdlPolicy = null;
	
	public NOVSMDLPolicy() {
		m_smdlPolicy = new ObjectPropertyPolicy();
	}

	public void addPropertiesPolicy(String policyType, NOVSMDLPolicyProperty[] smdlPolicyProps) {
		
		PolicyType smdlPolicyType = new PolicyType(policyType);
		
		for(int iCnt = 0; iCnt < smdlPolicyProps.length ; iCnt++ ){
			PolicyProperty policyProp = new PolicyProperty(smdlPolicyProps[iCnt].getPropertyName());
			
			if(smdlPolicyProps[iCnt].getPropertyModifier() != null){
				
				String[] propModifiers = smdlPolicyProps[iCnt].getPropertyModifier();
				
				for(int iCnt1 = 0; iCnt1 < propModifiers.length; iCnt1++){
					policyProp.setModifier(propModifiers[iCnt1], true);
				}
			}

			smdlPolicyType.addProperty(policyProp);
		}		
		
		m_smdlPolicy.addType(smdlPolicyType);
		
	}

//	public void addPropertiesPolicy(String policyType, Object[] smdlPolicyProps) {
//		
//		PolicyType smdlPolicyType = new PolicyType(policyType);
//		
//		for(int iCnt = 0; iCnt < smdlPolicyProps.length ; iCnt++ ){
//			NOVSMDLPolicyProperty smdlPropertyPolicy = (NOVSMDLPolicyProperty) smdlPolicyProps[iCnt];
//			PolicyProperty policyProp = new PolicyProperty(smdlPropertyPolicy.getPropertyName());
//			
//			if(smdlPropertyPolicy.getPropertyModifier() != null){
//				String[] propModifiers = ((NOVSMDLPolicyProperty)smdlPolicyProps[iCnt]).getPropertyModifier();
//				
//				for(int iCnt1 = 0; iCnt1 < propModifiers.length; iCnt1++){
//					policyProp.setModifier(propModifiers[iCnt1], true);
//				}
//			}
//
//			smdlPolicyType.addProperty(policyProp);
//		}		
//		
//		m_smdlPolicy.addType(smdlPolicyType);
//		
//	}
	
	public ObjectPropertyPolicy get_smdlPolicy() {
		return m_smdlPolicy;
	}
	
	
	
}
