package com.nov.rac.smdl.utilities;

import java.util.ArrayList;
import java.util.Collections;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseResponse2;
import com.teamcenter.soa.client.model.ErrorStack;

public class ReviseHelper
{

	public static int POM_ERROR_INSTANCE_LOCKED = 515112;
	public static boolean revise(TCComponentItem smdlItem) throws TCException 
	{
		boolean success = false;
		TCComponentItemRevision currentRev = null;
		
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		DataManagementService dmService = DataManagementService.getService(tcSession);
		ReviseInfo reviseInfo = new ReviseInfo();
		
		if( smdlItem != null )
		{
			try
			{
				currentRev = smdlItem.getLatestItemRevision();

			}
			catch( TCException e )
			{
				e.printStackTrace();
			}
		}
		
		reviseInfo.baseItemRevision = currentRev;
		reviseInfo.newRevId = ((TCComponentItem)currentRev.getItem()).getNewRev();
		
		ReviseResponse2 theReviseResponse = dmService.revise2( new ReviseInfo[]{ reviseInfo } );
		
		success= handleErrors(theReviseResponse.serviceData);
		
		return success;
	}
	
	
	private static boolean handleErrors(ServiceData serviceData) throws TCException
	{
		boolean success= true;
		
		ArrayList<String> errorMessages = new ArrayList<String>();
		
		int totalErrors = serviceData.sizeOfPartialErrors();
		if(totalErrors>0)
		{
			for(int i =0 ;i < totalErrors; i++)
			{
				ErrorStack m_error= serviceData.getPartialError(i);
				
				/* Swallow the exception if error code is 515112 
				 * PR in GTAC which shows similar error message with
				 * ItemRevision class and the PR says that this issue will be resolved in
				 * Teamcenter 8.3.3.2.	PR (PR-01892157)*/
				
				int iErrorCodes[] = m_error.getCodes();
				String[] messages = m_error.getMessages();
				for(int indx=0; indx<iErrorCodes.length;indx++)
				{	
					if(iErrorCodes[indx] != POM_ERROR_INSTANCE_LOCKED)
					{
						Collections.addAll(errorMessages, messages[indx]);
					}
				}
			}
		}
		
		if(errorMessages.size()>0)
		{
			success=false;
			String[] errorStrings= new String[1];
			errorStrings= errorMessages.toArray(errorStrings);
		
			throw new TCException(errorStrings);
		}
		
		return success;
	}
	
	
}
