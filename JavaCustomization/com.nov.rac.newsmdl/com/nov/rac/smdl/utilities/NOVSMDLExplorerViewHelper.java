package com.nov.rac.smdl.utilities;

import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPart3;
import org.eclipse.ui.PartInitException;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.views.CustomViewProperties;
import com.nov.rac.smdl.views.DeliveredProductView;
import com.nov.rac.smdl.views.OrderView;
import com.nov.rac.smdl.views.SMDLView;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.AdapterUtil;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLExplorerViewHelper 
{
	protected static ISMDLObject m_lastSelectedObject = null;


    protected static PropertyChangeListener m_lastListner = null;
	
	public static ISMDLObject getLastSelectedObject()
	{
	     return m_lastSelectedObject;		
	}
	
	public static void setLastSelectedObject(ISMDLObject theComponent)
	{
	     m_lastSelectedObject = theComponent;		
	}
	
	public static PropertyChangeListener getLastListener()
	{
	     return m_lastListner;		
	}
	
	public static void setLastListener(PropertyChangeListener changeListener)
	{
		m_lastListner = changeListener;		
	}
	public static void openView(IWorkbenchPage page,PropertyChangeListener theListener, TCComponent component)
	{
		try 
		{
			synchronized (page) 
			{
				setLastSelectedObject( (ISMDLObject) component );
				setLastListener(theListener);
				
				if(component instanceof NOV4ComponentSMDL || component instanceof NOV4ComponentSMDLRevision)
				{
					SMDLView smdlView=(SMDLView)page.showView(SMDLView.ID,null, page.VIEW_VISIBLE);
					smdlView.getContainer().clearPanelValues();
					smdlView.getContainer().setEnabled(true);
					
					if(component instanceof TCComponentItem)
					{
						TCComponentItem itm  = (TCComponentItem) component;
						TCComponentItemRevision itmRev = null;
						try 
						{
							TCComponent[] smdl_Rev_list = itm.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
							itmRev = (TCComponentItemRevision) smdl_Rev_list[smdl_Rev_list.length -1];
							if(itmRev!=null)
							{
								m_SMDLItem = itmRev.getTCProperty(NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
							}
						} 
						catch (TCException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(itmRev != null)
						{
							smdlView.getContainer().populateSMDL(itmRev);
						}
					}
					else
					{
						smdlView.getContainer().populateSMDL((TCComponentItemRevision)component);
					}
					smdlView.getContainer().setDPObject();
					smdlView.getContainer().setButtonsEnableStatus();
				}
				else if(component instanceof NOV4ComponentDP)
				{
					DeliveredProductView dpView=(DeliveredProductView)page.showView(DeliveredProductView.ID,null, page.VIEW_VISIBLE);
					dpView.getContainer().clearPanelValues();
					dpView.getContainer().setEnabled(true);
					dpView.getContainer().populateDP(component);
				}
				else if(component instanceof NOV4ComponentOrder)
				{
					OrderView orderView=(OrderView)page.showView(OrderView.ID,null, page.VIEW_VISIBLE);
					orderView.getContainer().clearPanelValues();
					orderView.getContainer().setEnabled(true);
					orderView.getContainer().populateOrder(component);
				}
			}
		}
		catch (PartInitException e) 
		{

			e.printStackTrace();
		}
	}

	public static boolean saveView(IWorkbenchPage page, ISMDLObject currentSelected) {
		
		boolean bViewDirty = false;
	
		Registry m_Registry = Registry.getRegistry("com.nov.rac.smdl.views.views");
		
		IViewPart smdlView = getSMDLView(page, currentSelected);
		
		ISaveablePart savablePart = null;
		
		if(smdlView != null){

			savablePart = (ISaveablePart) AdapterUtil.getAdapter(smdlView, ISaveablePart.class);

			if(savablePart != null && savablePart.isDirty()){
				
				//Activate the view to be saved...
				
				if(!page.isPartVisible(smdlView)){
					page.bringToTop(smdlView);
				}
				
				Shell parentShell = ((IWorkbenchPart) savablePart).getSite().getShell();
				
				// Ask the part if "isSavable". If you get it "false"
				IWorkbenchPart3 workbenchPart = (IWorkbenchPart3) AdapterUtil.getAdapter(smdlView, IWorkbenchPart3.class);
				if(workbenchPart != null)					
				{
					String isSaveableProp = CustomViewProperties.PROP_IS_SAVEABLE.getProperty();
		        	String isNotSaveableProp = CustomViewProperties.PROP_NOT_SAVEABLE_REASON.getProperty();
		        	
					String isSavable = workbenchPart.getPartProperty(isSaveableProp);
					
					if(isSavable != null && isSavable.equalsIgnoreCase("false"))
					{
						// Then ask the "reason."
						// Display the reason.
						//
						String isNotSaveableReason = workbenchPart.getPartProperty(isNotSaveableProp);
						if(isNotSaveableReason != null && isNotSaveableReason.length() > 0)
						{
							MessageBox messageBox = new MessageBox(parentShell, SWT.APPLICATION_MODAL|SWT.ERROR);
							messageBox.setText("Error");
							messageBox.setMessage(isNotSaveableReason);
							messageBox.open();
						}
					} // if(isSavable != null && isSavable.equalsIgnoreCase("false"))
					else
					{
						
						MessageBox confirmMessageBox = new MessageBox(parentShell, SWT.APPLICATION_MODAL|SWT.YES| SWT.NO |SWT.CANCEL);
						confirmMessageBox.setText(m_Registry.getString("SaveDialog.TITLE"));
						confirmMessageBox.setMessage(m_Registry.getString("SaveDialog.MSG"));
						
						int buttonID = confirmMessageBox.open();
						if(buttonID == SWT.YES)
						{
							savablePart.doSave(null);

							bViewDirty = savablePart.isDirty();
							try 
							{
								if(m_SMDLItem!=null)
								{
									m_SMDLItem.setProperty(NOV4ComponentSMDL.DOC_RESPONSIBLE, docResponsible);
								}					
								
							} catch (TCException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else if(buttonID == SWT.NO)
						{
							setDirty(page, currentSelected,false);
						}
						else if(buttonID ==SWT.CANCEL ||buttonID==SWT.Close)
						{
							bViewDirty=true;
						}
						
					} 
					
				}
			}
		}
		
		return bViewDirty;
	}
	
	private static void setDirty(IWorkbenchPage page,
			ISMDLObject component, boolean b)
	{
		IViewPart view = null;
		
		if(component instanceof NOV4ComponentOrder)
		{
			view = page.findView(OrderView.ID);
			((OrderView)view).setDirty(b);
		}
		else if(component instanceof NOV4ComponentDP)
		{
			view = page.findView(DeliveredProductView.ID);
			((DeliveredProductView)view).setDirty(b);
		}
		else if(component instanceof NOV4ComponentSMDL || component instanceof NOV4ComponentSMDLRevision)
		{
			view = page.findView(SMDLView.ID);
			((SMDLView)view).setDirty(b);
		}
		
		
	}

	public static IViewPart getSMDLView(IWorkbenchPage page, ISMDLObject component){
		
		IViewPart smdlView = null;
		
		if(component instanceof NOV4ComponentOrder)
		{
			smdlView = page.findView(OrderView.ID);
		}
		else if(component instanceof NOV4ComponentDP)
		{
			smdlView = page.findView(DeliveredProductView.ID);
		}
		else if(component instanceof NOV4ComponentSMDL || component instanceof NOV4ComponentSMDLRevision)
		{
			smdlView = page.findView(SMDLView.ID);
		}
		
		return smdlView;
	}

	public static void setDocResponsible(String stringValue) {
		
		docResponsible=stringValue;
	}

	public static String getDocResposible() {
		
		return docResponsible;
	}
	private static String docResponsible =null;
	private static TCComponent m_SMDLItem = null;
	
}