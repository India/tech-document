package com.nov.rac.smdl.utilities;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.kernel.NOV4ComponentTemplateRevision;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.query.FinderService;
import com.teamcenter.services.rac.query._2007_06.Finder.FindWorkspaceObjectsResponse;
import com.teamcenter.services.rac.query._2007_06.Finder.WSOFindCriteria;
import com.teamcenter.services.rac.query._2007_06.Finder.WSOFindSet;
import com.teamcenter.soa.common.PolicyProperty;

public class NOV4TemplateHelper 
{	
	public static TCComponent getTemplateComponent(String strTempName)
	{
		TCSession session=(TCSession)AIFUtility.getDefaultSession();
		TCComponentItem templateItem = null;
		TCComponentItemRevision templateItemRevision = null;
		
		FinderService finderService = FinderService.getService(session);	
		WSOFindSet findTemplate= new WSOFindSet();
		WSOFindCriteria criteria = new WSOFindCriteria();
		criteria.objectType = NOV4ComponentTemplate.TEMPLATE_OBJECT_TYPE_NAME;
		criteria.objectName = strTempName;
		criteria.scope="WSO_scope_All";
		
		findTemplate.criterias = new WSOFindCriteria[]{criteria};
		try 
		{
			//Mohan : Policy is not working properly and is making too many server calls
			          //used below getTemplateRevisionProps method to get the properties
			
			/*NOVSMDLPolicy smdlPolicy = createTemplatePolicy();
			
			String m_strCurrentSOAPolicy = session.getSoaConnection().getCurrentObjectPropertyPolicy();
			
			SessionService service = SessionService.getService(session);

			service.setObjectPropertyPolicy(smdlPolicy.get_smdlPolicy());*/
		
			FindWorkspaceObjectsResponse response = finderService.findWorkspaceObjects(new WSOFindSet[]{findTemplate});
					
			/*TCComponent object= response.serviceData.getPlainObject(0);
			service.setObjectPropertyPolicy(m_strCurrentSOAPolicy);*/
			
			int iNoOfComponents = response.serviceData.sizeOfPlainObjects();
			if(iNoOfComponents >0 )
			{
				templateItem= (TCComponentItem)response.serviceData.getPlainObject(0);

				TCComponent[] revisions= templateItem.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				if(revisions!=null)
				{
					templateItemRevision=(TCComponentItemRevision) revisions[revisions.length-1];
				}
			}
			
		} 
		catch (ServiceException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			
		return templateItemRevision;
	}
	
	public static Vector<String> getTemplateNames()
	{
		TCSession session=(TCSession)AIFUtility.getDefaultSession();
		TCComponent[] templateItems = null;
		
		Vector<String> templateNames = new Vector<String>();
		
		try {
			TCComponentItemType novTemplateType =  (TCComponentItemType) session.getTypeService().getTypeComponent("Nov4Template");
			
			templateItems = novTemplateType.extent();
			
			List<TCComponent> templateItemsList = Arrays.asList(templateItems);//TC10.1 Upgrade
			
			//String[][] arrTemplateNames = novTemplateType.getPropertiesSet(templateItems, new String[]{"object_name"});
			String[][] arrTemplateNames = novTemplateType.getPropertiesSet(templateItemsList, new String[]{"object_name"});//TC10.1 Upgrade
			
			for(int iCnt = 0; iCnt < arrTemplateNames.length; iCnt++){
				templateNames.addElement(arrTemplateNames[iCnt][0]);
			}
			
		} catch (TCException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return templateNames;
	}
	
	
	public static TCComponent[] getTemplatesForLoggedInGroup()
	{
		TCSession session=(TCSession)AIFUtility.getDefaultSession();
			
		TCComponentGroup currentgroup = session.getCurrentGroup();
		
		FinderService finderService = FinderService.getService(session);	
		WSOFindSet findTemplate= new WSOFindSet();
		WSOFindCriteria criteria = new WSOFindCriteria();
		criteria.objectType = NOV4ComponentTemplate.TEMPLATE_OBJECT_TYPE_NAME;
		criteria.scope="WSO_scope_All";
		criteria.group= currentgroup;
		
		findTemplate.criterias = new WSOFindCriteria[]{criteria};
		TCComponent[] templateItems= null;
		Vector<String> templateNames = new Vector<String>();
		try 
		{
		
			FindWorkspaceObjectsResponse response = finderService.findWorkspaceObjects(new WSOFindSet[]{findTemplate});
			templateItems = new TCComponent[response.serviceData.sizeOfPlainObjects()];
			
			for (int i =0 ;i <response.serviceData.sizeOfPlainObjects();i++)
			{
				templateItems[i]= (TCComponentItem) response.serviceData.getPlainObject(i);
				
			}		
		
		} 
		catch (ServiceException e) 
		{
			e.printStackTrace();
		} 
		
			
		return templateItems;
	}
	
	
	public static TCComponent[] getAdminTemplates()
	{
		TCSession session=(TCSession)AIFUtility.getDefaultSession();
		TCComponent[] templateItems = null;
		
		try {
			TCComponentItemType novTemplateType =  (TCComponentItemType) session.getTypeService().getTypeComponent("Nov4Template");
			
			templateItems = novTemplateType.extent();
			
			List<TCComponent> templateItemsList = Arrays.asList(templateItems);//TC10.1 Upgarde
			
			//String[][] arrTemplateNames = novTemplateType.getPropertiesSet(templateItems, new String[]{"object_name"});
			String[][] arrTemplateNames = novTemplateType.getPropertiesSet(templateItemsList, new String[]{"object_name"});//TC10.1 Upgrade
			
		} catch (TCException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return templateItems;
	}
	
	public static TCComponent getTemplateRevisionProps( TCComponent templateRevObject )
	{
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(templateRevObject);
		
		String[] templateAttributeList = getAttributeList();
		
		NOVSMDLPolicy templatePolicy = createTemplatePolicy();
		
		TCComponent m_templateSOAPlainObject = dmSOAHelper.getSMDLProperties(templateRevObject,templateAttributeList,templatePolicy);
		
		return m_templateSOAPlainObject;
	}
	
	private static String[] getAttributeList() {

		String[] smdlAttributeList = {
										NOV4ComponentTemplateRevision.TEMPLATE_STRUCTURE_DATA
									  };
		return smdlAttributeList;
	}
	
	
	private static NOVSMDLPolicy createTemplatePolicy() 
	{
		NOVSMDLPolicy m_TemplatePolicy =null;
			
			m_TemplatePolicy = new NOVSMDLPolicy();
			
			// Add Property policy for Nov4SMDLStructureData object
			
			Vector<NOVSMDLPolicyProperty> structureDataPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_CODE,PolicyProperty.AS_ATTRIBUTE));
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_ADDITIONAL_CODE,PolicyProperty.AS_ATTRIBUTE));
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_DOCUMENT_REF,PolicyProperty.WITH_PROPERTIES));
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_COMMENTS,PolicyProperty.AS_ATTRIBUTE));		
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_DOC_CONTRACT_DATE,PolicyProperty.AS_ATTRIBUTE));
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_WEEKS,PolicyProperty.AS_ATTRIBUTE));
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_LINE_ITEM_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			structureDataPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplate.TEMPLATE_TAB,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] structureDataPolicyPropsArr = structureDataPolicyProps.toArray(new NOVSMDLPolicyProperty[structureDataPolicyProps.size()]);
			
			m_TemplatePolicy.addPropertiesPolicy(NOV4ComponentTemplate.TEMPLATE_STRUCTURE_DATA_TYPE, structureDataPolicyPropsArr);
			
			// Add Property policy for Documents object
			Vector<NOVSMDLPolicyProperty> docsPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_TITLE,PolicyProperty.AS_ATTRIBUTE));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC,PolicyProperty.AS_ATTRIBUTE));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_OBJECT_STRING));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REVISION_LIST));

			NOVSMDLPolicyProperty[] docsPolicyPropsArr = docsPolicyProps.toArray(new NOVSMDLPolicyProperty[docsPolicyProps.size()]);
			m_TemplatePolicy.addPropertiesPolicy(NOVSMDLConstants.DOC_OBJECT_TYPE_NAME, docsPolicyPropsArr);
			
			// Add Property for Revision List
			Vector<NOVSMDLPolicyProperty> templateProps = new Vector<NOVSMDLPolicyProperty>();
			templateProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REVISION_LIST,PolicyProperty.WITH_PROPERTIES));
			
			NOVSMDLPolicyProperty[] templatePropsArr = templateProps.toArray(new NOVSMDLPolicyProperty[templateProps.size()]);
			m_TemplatePolicy.addPropertiesPolicy(NOV4ComponentTemplate.TEMPLATE_OBJECT_TYPE_NAME, templatePropsArr);
			
			
			// Add Property policy for revision
			Vector<NOVSMDLPolicyProperty> templateRevProps1 = new Vector<NOVSMDLPolicyProperty>();
			templateRevProps1.addElement(new NOVSMDLPolicyProperty(NOV4ComponentTemplateRevision.TEMPLATE_STRUCTURE_DATA,PolicyProperty.WITH_PROPERTIES));
			
			
			NOVSMDLPolicyProperty[] templateRevPropsArr = docsPolicyProps.toArray(new NOVSMDLPolicyProperty[templateRevProps1.size()]);
			m_TemplatePolicy.addPropertiesPolicy(NOV4ComponentTemplateRevision.TEMPLATE_REVISION_TYPE_NAME, templateRevPropsArr);
			
		
		return m_TemplatePolicy;
	}

}


