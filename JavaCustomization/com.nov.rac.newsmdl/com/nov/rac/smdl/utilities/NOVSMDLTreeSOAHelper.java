package com.nov.rac.smdl.utilities;

import java.util.Collection;
import java.util.Set;
import java.util.Vector;


import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov4.services.rac.smdl.NOV4SMDLDataManagementService;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeResponse;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.SessionService;
import com.teamcenter.soa.common.PolicyProperty;

public class NOVSMDLTreeSOAHelper {
	
	public static SMDLTreeOutput getRelatedObjects(TCComponent rootObject){

		TCSession session = rootObject.getSession();
		
		// Creating and setting Property policy for SOA Call.
		NOVSMDLPolicy smdlPolicy = createSMDLPolicy();
		
		String existingSOAPolicy = session.getSoaConnection().getCurrentObjectPropertyPolicy();
		
		SessionService service = SessionService.getService(session);

		service.setObjectPropertyPolicy(smdlPolicy.get_smdlPolicy());
		
		//Create Input object for SOA Call 
		
		NOV4SMDLDataManagement.SMDLInputObjectInfo rootObjectInput[] = new NOV4SMDLDataManagement.SMDLInputObjectInfo[1];
		
		rootObjectInput[0] =  new NOV4SMDLDataManagement.SMDLInputObjectInfo(); 
		
		// Get the child objects from all levels 
		rootObjectInput[0].smdlInputObject = rootObject;		
								
		NOV4SMDLDataManagementService soaService = NOV4SMDLDataManagementService.getService(session);
		
		SMDLTreeResponse response =  soaService.getSMDLTreeHierarchy(rootObjectInput);
		
		// Set the Property policy to original policy
		boolean bStatus = false;
		try {
			bStatus = service.setObjectPropertyPolicy(existingSOAPolicy);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response.smdlTreeOutputList[0] ;
		
	}
	
	public static NOVSMDLPolicy createSMDLPolicy() {
		
		NOVSMDLPolicy smdlPolicy = new NOVSMDLPolicy();
			
		// Add Property policy for SMDL Item object
		Vector<NOVSMDLPolicyProperty> itemPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.SMDL_RELEASE_STATUS,PolicyProperty.WITH_PROPERTIES));
		itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.SMDL_PROCESS_STATUS,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] itemPolicyPropsArr = itemPolicyProps.toArray(new NOVSMDLPolicyProperty[itemPolicyProps.size()]);
		
		smdlPolicy.addPropertiesPolicy(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME, itemPolicyPropsArr);
		
		// Add Property policy for Order object
		Vector<NOVSMDLPolicyProperty> orderPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_RELEASE_STATUS,PolicyProperty.WITH_PROPERTIES));
		orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_PROCESS_STATUS,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] orderPolicyPropsArr = orderPolicyProps.toArray(new NOVSMDLPolicyProperty[orderPolicyProps.size()]);
		
		smdlPolicy.addPropertiesPolicy(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME, orderPolicyPropsArr);

		// Add Property policy for DP object
		Vector<NOVSMDLPolicyProperty> dpPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		dpPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_RELEASE_STATUS,PolicyProperty.WITH_PROPERTIES));
		dpPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_PROCESS_STATUS,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] dpPolicyPropsArr = orderPolicyProps.toArray(new NOVSMDLPolicyProperty[dpPolicyProps.size()]);
		
		smdlPolicy.addPropertiesPolicy(NOV4ComponentDP.DP_OBJECT_TYPE_NAME, dpPolicyPropsArr);
		
		return smdlPolicy;
	}

}
