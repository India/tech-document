package com.nov.rac.smdl.utilities;

import java.util.HashMap;
import java.util.Map;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class SMDLHomeTree extends TCTree
{
		
		Map<NOV4ComponentOrder,Map> m_orderCache=new HashMap<NOV4ComponentOrder, Map>();
		
		private SMDLTreeOutput theSMDLTreeOutput = null;
		
		@Override
		public Object[] getChildren(Object obj, Object obj1) {
			
			if(obj == null)
	            return null;
		
			obj=getNodeComponent(obj);
			
			Object[] relatedObjects = null;
			TCComponent order=null;
			Map relatedObjectMap =null;
			
				if(obj instanceof ISMDLObject)
				{
					order= ((ISMDLObject)obj).getOrder();
				}
				
				relatedObjectMap=	m_orderCache.get(order);
				
				if(null==relatedObjectMap &&  null!=order)
				{
					//call soa & add it to cache;
					
					SMDLTreeOutput output = NOVSMDLTreeSOAHelper.getRelatedObjects(order);
					m_orderCache.put((NOV4ComponentOrder)output.smdlOrderObject, output.smdlRelatedObjects);
					relatedObjectMap=output.smdlRelatedObjects;
				}
			
				if(relatedObjectMap!=null)
				{
					relatedObjects = (Object[]) relatedObjectMap.get(obj);
				}
			
			//if(relatedObjects == null || relatedObjects.length == 0)
				//relatedObjects = super.getChildren(obj, obj1);
				
				if(relatedObjects == null && obj instanceof NOV4ComponentSMDL){
					try {
						relatedObjects =  ((TCComponentItem) obj).getTCProperty("revision_list").getReferenceValueArray();
					} catch (TCException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (relatedObjects == null && obj instanceof TCComponentFolder)
				{
					relatedObjects = super.getChildren(obj, obj1);
				}
			
			return relatedObjects;
				
		}

		public void setSMDLTreeOutput(SMDLTreeOutput theSMDLOutput) {
			
			theSMDLTreeOutput =  theSMDLOutput;
		}
		
		private TCComponent getNodeComponent(Object obj)
		{
			TCComponent component=null;
			if(obj instanceof AIFComponentContext)
			{
				component=(TCComponent) ((AIFComponentContext) obj).getComponent();
			}
			else if(obj instanceof TCComponent)
			{
				component=(TCComponent) obj;
			}
			
			return component;
		}

	
	
}
