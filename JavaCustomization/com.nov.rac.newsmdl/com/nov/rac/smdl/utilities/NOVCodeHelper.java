package com.nov.rac.smdl.utilities;

import java.util.HashMap;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NOVCodeHelper
{

	private static TCSession session =(TCSession)AIFUtility.getDefaultSession();
	private static HashMap novCodeIssueDate= new HashMap();
	private static String[] novCodeList = null;
	private static HashMap<String,Object>    novCodeDescValues    = new HashMap<String,Object>();

	public static String[] loadIssueDateLOV() {
		String[] lovValues = null;
		
		Registry reg =Registry.getRegistry("com.nov.rac.smdl.templates.templates");
		try {
			
			TCComponentListOfValuesType lov = (TCComponentListOfValuesType) session
					.getTypeComponent("ListOfValues");
			TCComponentListOfValues[] templateTypeLov = lov.find(reg
					.getString("issueDateLov.NAME"));
			
			
			if (templateTypeLov != null && templateTypeLov.length > 0) {
				lovValues = templateTypeLov[0].getListOfValues()
						.getLOVDisplayValues();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			
		}
		return lovValues;
	}
	
	
	public static String[] getCodeList()
	{
		if(novCodeList == null || novCodeList.length == 0)
		{
			Registry reg =Registry.getRegistry("com.nov.rac.smdl.templates.templates");
			String lovName = reg.getString("SDRL_LOV");

			Object[] novCodekeys =null;
			String novCodeDesc[] =null;
			//String[] novCodeList = null;

			try
			{
				TCComponentListOfValues defaultTypeByDocCatLOV = TCComponentListOfValuesType
				.findLOVByName(session, lovName);

				novCodekeys = defaultTypeByDocCatLOV.getListOfValues()
				.getListOfValues();

				novCodeDesc= defaultTypeByDocCatLOV.getListOfValues().getDescriptions();


				novCodeList= new String[novCodekeys.length];
				for ( int i = 0; i < novCodekeys.length; i++ )
				{
					// Tokanise values to seperate out novCode=Issue Date
					String[] tokens = novCodekeys[i].toString().split("=");
					if ( tokens.length == 2 )
					{
						String snovCode = tokens[0];
						String sNovCodeIssueDate = tokens[1];
						novCodeIssueDate.put(snovCode, sNovCodeIssueDate);

						novCodeList[i] = snovCode+"="+novCodeDesc[i];
						
						novCodeDescValues.put(snovCode, novCodeDesc[i]);
					}
				}

			} catch (TCException e) {

				e.printStackTrace();
			}
		}
		return novCodeList;
	}
	
	
	public static HashMap getCodeIssueDate()
	{
		
		Registry reg =Registry.getRegistry("com.nov.rac.smdl.templates.templates");
		String lovName = reg.getString("SDRL_LOV");
        
        Object[] novCodekeys = null;
        String[] novCodeList = null;
        
        try
        {
       	 TCComponentListOfValues defaultTypeByDocCatLOV = TCComponentListOfValuesType
                .findLOVByName(session, lovName);
       
       	 novCodekeys = defaultTypeByDocCatLOV.getListOfValues()
			         .getListOfValues();
       	 
       	 
       	novCodeList= new String[novCodekeys.length];
            for ( int i = 0; i < novCodekeys.length; i++ )
            {
                // Tokanise values to seperate out novCode=Issue Date
                String[] tokens = novCodekeys[i].toString().split("=");
                if ( tokens.length == 2 )
                {
                    String snovCode = tokens[0];
                    String sNovCodeIssueDate = tokens[1];
                    novCodeIssueDate.put(snovCode, sNovCodeIssueDate);
                }
            }
       	 
		 } catch (TCException e) {
			
			e.printStackTrace();
		 }
		
		return novCodeIssueDate;
	}
	
	public static String[] getDateWeeks(String strNovCode, String[] issueDateLOVList)
	{
        String issueDate = "";
        String weeks = "";
        int index = -1;
        String [] values= new String[2];
   
        if(issueDateLOVList==null)
        {
        	issueDateLOVList= loadIssueDateLOV();
        }
        
        if(novCodeIssueDate==null)
        {
        	novCodeIssueDate=getCodeIssueDate();
        }
        
        if(novCodeIssueDate.get(strNovCode)!=null)
        {
	        String issueDateValue = novCodeIssueDate.get(strNovCode).toString();
	        if((index=issueDateValue.indexOf("WACA"))!=-1)
	        {
	            issueDate=getIssueDateToSet("WACA",issueDateLOVList);
	            weeks=issueDateValue.substring(0,index);
	        }
	        else if((index=issueDateValue.indexOf("WAD"))!=-1)
	        {
	            issueDate=getIssueDateToSet("WAD",issueDateLOVList);
	            weeks=issueDateValue.substring(0,index);
	        }
	        else if((index=issueDateValue.indexOf("WBT"))!=-1)
	        {
	            issueDate=getIssueDateToSet("WBT",issueDateLOVList);
	            weeks=issueDateValue.substring(0,index);
	        }
	        else if((index=issueDateValue.indexOf("WAT"))!=-1)
	        {
	            issueDate=getIssueDateToSet("WAT",issueDateLOVList);
	            weeks=issueDateValue.substring(0,index);
	        }
	        else if((index=issueDateValue.indexOf("WBD"))!=-1)
	        {
	            issueDate=getIssueDateToSet("WBD",issueDateLOVList);
	            weeks=issueDateValue.substring(0,index);
	        }
	        
	        values[0]=issueDate;
	        values[1]=weeks;
        }
       
        
		return values;
	}

	private static String getIssueDateToSet(String toCompare, String[] issueDateLOVList)
    {
        for(int index=0;index<issueDateLOVList.length;index++)
        {
            if(issueDateLOVList[index].indexOf(toCompare)!=-1)
                return issueDateLOVList[index];
        }
        return null;
    }
	public static HashMap getCodeContractIssueDate()
	{
		return novCodeIssueDate;
	}
	public static HashMap getCodesDescription()
	{
		return novCodeDescValues;
	}
}
