package com.nov.rac.smdl.utilities;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;

public class ComponentHelper {

	public static TCComponent getComponent(Object obj)
	{
		TCComponent component=null;
		if(obj instanceof AIFComponentContext)
		{
			component=(TCComponent) ((AIFComponentContext) obj).getComponent();
		}
		else if(obj instanceof TCComponent)
		{
			component=(TCComponent) obj;
		}
		
		return component;
	}
}
