package com.nov.rac.smdl.utilities;

import java.util.Vector;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.soa.common.PolicyProperty;

public class NOVSMDLObjectSOAHelper 
{
	private static NOVSMDLPolicy m_SMDLPolicy = null;
	public static TCComponent getSMDLRevisionProps(TCComponent smdlRevObject) 
	{		
		TCComponent m_smdlSOAPlainObject = null;
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(smdlRevObject);
		
		String[] smdlAttributeList = getAttributeList();
		
		NOVSMDLPolicy smdlPolicy = createSMDLPolicy();
		
		m_smdlSOAPlainObject = dmSOAHelper.getSMDLProperties(smdlRevObject,smdlAttributeList,smdlPolicy);
		
		return m_smdlSOAPlainObject;
	}

	public static String[] getAttributeList() 
	{

		String[] smdlAttributeList = {
										NOV4ComponentSMDL.SMDL_ID,
										NOV4ComponentSMDL.SMDL_REVISION_ID,
										NOV4ComponentSMDL.SMDL_NAME,
										NOV4ComponentSMDL.SMDL_ITEM_REF,
										NOV4ComponentSMDL.SMDL_CODES
									  };
		return smdlAttributeList;
	}
	public static NOVSMDLPolicy createSMDLPolicy() 
	{
		if(m_SMDLPolicy == null)
		{
			
			m_SMDLPolicy = new NOVSMDLPolicy();
			
			// Add Property policy for SMDL Code Form object
			
			Vector<NOVSMDLPolicyProperty> codeFormPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.ADDITIONAL_CODE,PolicyProperty.AS_ATTRIBUTE));		
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.CUSTOMER_CODE,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOC_CONTRACT_DATE,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOC_PLANNED_DATE,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.GROUP_NAME,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.COMMENTS,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REF,PolicyProperty.WITH_PROPERTIES));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF,PolicyProperty.WITH_PROPERTIES));
			
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.CODEFORM_TAB,PolicyProperty.AS_ATTRIBUTE));
			codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.LINE_ITEM_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] codeFormPolicyPropsArr = codeFormPolicyProps.toArray(new NOVSMDLPolicyProperty[codeFormPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME, codeFormPolicyPropsArr);
			
			// Add Property policy for SMDL Item object
			Vector<NOVSMDLPolicyProperty> itemPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_TYPE,PolicyProperty.AS_ATTRIBUTE));		
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DP_REF,PolicyProperty.WITH_PROPERTIES));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.ORDER_REF,PolicyProperty.WITH_PROPERTIES));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_OBJECT_STRING));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_DESC,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] itemPolicyPropsArr = itemPolicyProps.toArray(new NOVSMDLPolicyProperty[itemPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME, itemPolicyPropsArr);
			
			
			// Add Property policy for Release Status object
			Vector<NOVSMDLPolicyProperty> smdlRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_DESC,PolicyProperty.AS_ATTRIBUTE));
			
			smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_RELEASE_STATUS,PolicyProperty.WITH_PROPERTIES));
			
			NOVSMDLPolicyProperty[] smdlRevPolicyPropArr = smdlRevPolicyProps.toArray(new NOVSMDLPolicyProperty[smdlRevPolicyProps.size()]);

			m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE, smdlRevPolicyPropArr);
			
			
			
			// Add Property policy for Order object
			Vector<NOVSMDLPolicyProperty> orderPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CUSTOMER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PLANT_ID_OTHER,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] orderPolicyPropsArr = orderPolicyProps.toArray(new NOVSMDLPolicyProperty[orderPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME, orderPolicyPropsArr);

			// Add Property policy for DP object
			Vector<NOVSMDLPolicyProperty> dpPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			dpPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_BASED_ON_PROD,PolicyProperty.AS_ATTRIBUTE));
			dpPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_ENG_JOB_NUMBERS,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] dpPolicyPropsArr = dpPolicyProps.toArray(new NOVSMDLPolicyProperty[dpPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentDP.DP_OBJECT_TYPE_NAME, dpPolicyPropsArr);
			
			
			// Add Property policy for Documents object
			Vector<NOVSMDLPolicyProperty> docsPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_TITLE,PolicyProperty.AS_ATTRIBUTE));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC,PolicyProperty.AS_ATTRIBUTE));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_OBJECT_STRING));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REVISION_LIST,PolicyProperty.WITH_PROPERTIES));
			docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_OWNING_GROUP,PolicyProperty.WITH_PROPERTIES));
			
			NOVSMDLPolicyProperty[] docsPolicyPropsArr = docsPolicyProps.toArray(new NOVSMDLPolicyProperty[docsPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOC_OBJECT_TYPE_NAME, docsPolicyPropsArr);

			
			// Add Property policy for Documents Revision object
			Vector<NOVSMDLPolicyProperty> docsRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_REV_ID,PolicyProperty.AS_ATTRIBUTE));
			docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC,PolicyProperty.AS_ATTRIBUTE));
			docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_STATUS,PolicyProperty.WITH_PROPERTIES));
			
			NOVSMDLPolicyProperty[] docsRevPolicyPropsArr = docsRevPolicyProps.toArray(new NOVSMDLPolicyProperty[docsRevPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOCREV_OBJECT_TYPE_NAME, docsRevPolicyPropsArr);
			
			// Add Property policy for Release Status object
			Vector<NOVSMDLPolicyProperty> relStatusPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			relStatusPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REL_STATUS_NAME,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] relStatusPolicyPropArr = relStatusPolicyProps.toArray(new NOVSMDLPolicyProperty[relStatusPolicyProps.size()]);
			
			m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.REL_STATUS_OBJECT_TYPE_NAME, relStatusPolicyPropArr);

		}		
		return m_SMDLPolicy;
	}
}