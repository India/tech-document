package com.nov.rac.smdl.utilities;

public class NOVSMDLPolicyProperty {
	
	private String m_propertyName = null;
	
	private String[] m_propertyModifier = null;
	
	public NOVSMDLPolicyProperty (String name,String modifier){
		
		m_propertyName = name;
		
		m_propertyModifier = new String[] {modifier};
	}

	public NOVSMDLPolicyProperty(String name, String[] modifiers) {
		
		m_propertyName = name;
		
		m_propertyModifier = modifiers;

	}

	public NOVSMDLPolicyProperty(String name) {
		m_propertyName = name;
	}

	public String getPropertyName() {
		return m_propertyName;
	}

	public String[] getPropertyModifier() {
		return m_propertyModifier;
	}

}
