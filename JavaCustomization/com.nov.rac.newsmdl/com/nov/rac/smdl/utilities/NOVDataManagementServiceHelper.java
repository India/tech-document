package com.nov.rac.smdl.utilities;

import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core.SessionService;

public class NOVDataManagementServiceHelper {
	
	private TCSession m_tcSession; 
	
	private String m_strCurrentSOAPolicy = null;
	
	public NOVDataManagementServiceHelper(TCComponent smdlObject){		
		m_tcSession = smdlObject.getSession();				
	}

	public TCComponent getSMDLProperties(TCComponent smdlObject, String[] attrProperties,NOVSMDLPolicy smdlPolicy){
		
		TCComponent plainObject = null;
		
		TCComponent[] inputObjs = new TCComponent[1];
		inputObjs[0] = smdlObject;
		
		// Get Session SOA Policy
		getCurrentSOAPolicy();
		
		SessionService service = SessionService.getService(m_tcSession);

		service.setObjectPropertyPolicy(smdlPolicy.get_smdlPolicy());

		DataManagementService DMService = DataManagementService.getService(m_tcSession);

		ServiceData serviceData = DMService.getProperties(inputObjs,attrProperties);

		if (serviceData.sizeOfPlainObjects() == 1) {
			plainObject = serviceData.getPlainObject(0);
		}

		setCurrentSOAPolicy();
		
		return plainObject;	
	}	
	
	private void setCurrentSOAPolicy() {
		try {
			if(m_strCurrentSOAPolicy != null){
				SessionService service = SessionService.getService(m_tcSession);
				
				boolean bStatus = service.setObjectPropertyPolicy(m_strCurrentSOAPolicy);
				
				System.out.println(bStatus);
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getCurrentSOAPolicy() {
		this.m_strCurrentSOAPolicy = m_tcSession.getSoaConnection().getCurrentObjectPropertyPolicy();
	}
}
