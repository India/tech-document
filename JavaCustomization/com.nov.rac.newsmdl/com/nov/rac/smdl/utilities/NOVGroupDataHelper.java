package com.nov.rac.smdl.utilities;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroupType;
import com.teamcenter.rac.kernel.TCSession;

public class NOVGroupDataHelper
{
	private static HashMap<String,TCComponent> groupDataMap = new HashMap<String,TCComponent>();
	public static HashMap<String,TCComponent> getGroupData()
	{
		if(groupDataMap == null || groupDataMap.size() == 0)
		{
			try
			{
				TCComponentGroupType   groupType  = null;
				TCSession session= (TCSession)AIFUtility.getDefaultSession();
				groupType = (TCComponentGroupType) session.getTypeComponent("Group");

				TCComponent[] allGroups = groupType.extent();
				
				List<TCComponent> allGroupsList = Arrays.asList(allGroups);//TC10.1 Upgrade

				//String[][] arrPropertySet = groupType.getPropertiesSet(allGroups, new String[] {"object_full_name"});
				String[][] arrPropertySet = groupType.getPropertiesSet(allGroupsList, new String[] {"object_full_name"});//TC10.1 Upgrade

				if ( allGroups != null )
				{
					int iGroupLength = allGroups.length;
					for ( int indx = 0; indx < iGroupLength; indx++ )
					{
						groupDataMap.put(arrPropertySet[indx][0], allGroups[indx]);
					}
				}

			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
		}
		return groupDataMap;
	}
}