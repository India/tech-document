package com.nov.rac.smdl.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;


public class NOVCreateSMDLHelper 
{
	private static String[] m_smdlTypes = null;
	public static Date getContractAwardDate(TCComponent dpComp, String sdocContractdate, int iNoOfWeeks)
	{
		Date contractDate = null;
		int daysToIncrement = 0;
		if(dpComp !=null)
		{
			try
			{  
				Calendar cal = Calendar.getInstance();
				daysToIncrement = iNoOfWeeks*7;
				if ( sdocContractdate.indexOf("WAD") != -1 )
				{
					contractDate = (Date)dpComp.getTCProperty(NOV4ComponentDP.CONTRACT_DELIVERY_DATE).getPropertyValue();					
				}
				else if ( sdocContractdate.indexOf("WACA") != -1 )
				{
					TCComponent orderComp = null;
					orderComp = dpComp.getTCProperty(NOV4ComponentDP.DP_ORDER_RELATION).getReferenceValue();
					if(orderComp != null)
					{
						contractDate = (Date)orderComp.getTCProperty(NOV4ComponentOrder.CONTRACT_AWARD_DATE).getPropertyValue();
					}
				}
				else if ( sdocContractdate.indexOf("WBD") != -1 )
				{
					contractDate = (Date)dpComp.getTCProperty(NOV4ComponentDP.CONTRACT_DELIVERY_DATE).getPropertyValue();
					daysToIncrement = -daysToIncrement;
				}
				else if ( sdocContractdate.indexOf("WAT") != -1 )
				{
					contractDate = (Date)dpComp.getTCProperty(NOV4ComponentDP.FAT_DATE).getPropertyValue();
				}
				
				if(contractDate != null)
				{
					cal.setTime(contractDate);
					cal.add(Calendar.DATE, daysToIncrement);
					contractDate = cal.getTime();
				}
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
		}
		return contractDate;
	}
	public static Date getDate(String sDate)
    {
		SimpleDateFormat formatter ; 
    	Date date = null ; 
    	try
    	{
    		formatter = new SimpleDateFormat("dd-MMM-yyyy");
    		date = (Date)formatter.parse(sDate);
    	}
    	catch ( Exception ex )
    	{
    		ex.printStackTrace();
    	}
    	return date;
    }
	public static String getSMDLID() 
	{
		TCComponentItemType itemType;
		String smdlId = null;
		try 
		{
			TCSession session=(TCSession)AIFUtility.getDefaultSession();
			itemType = (TCComponentItemType) (session.getTypeComponent(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME));
			smdlId = itemType.getNewID();
		} catch (TCException e)
		{
			e.printStackTrace();
		}
		return smdlId;
	}
	public static TCComponentItemRevision getSMDLLatestRevision(String strSMDLId)
	{
		TCSession session=(TCSession)AIFUtility.getDefaultSession();
		TCComponentItem smdlItem = null;
		TCComponentItemRevision smdlRev = null;
		try 
		{
			TCComponentItemType smdlType =  (TCComponentItemType) session.getTypeService().getTypeComponent(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME);			
			smdlItem = smdlType.find(strSMDLId);
			if((smdlItem != null) && (smdlItem instanceof NOV4ComponentSMDL))
			{
				smdlRev = smdlItem.getLatestItemRevision();
			}
		}
		catch (TCException e1) 
		{
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}
		return smdlRev;
	}
	public static String[] getSMDLTypes()
	{
		if(m_smdlTypes == null)
		{
	        try
	        {
	        	TCSession session =( TCSession ) AIFUtility.getDefaultSession();
	        	Registry appReg = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
	            TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName( session, appReg.getString("SMDL_Type.LOV"));
	            if( lovValues != null )
	            {
					ListOfValuesInfo lovInfo = lovValues.getListOfValues();
					Object[] lovS = lovInfo.getListOfValues();
					int iLovLength = lovS.length;
					if(iLovLength  > 0 )
					{
						m_smdlTypes = new String[iLovLength];
						for( int index = 0; index < iLovLength; ++index )
						{
							m_smdlTypes[index] = ( String )lovS[index];
						}
					}
				}
	        }
	        catch ( TCException e )
	        {
	            e.printStackTrace();
	        }
		}
		return m_smdlTypes;
	}
	
	public static int getNoOfWeeks(String sContractDate)
	{
		int iNoOfWeeks = 0;
		int index = -1;
		if ( ((index = sContractDate.indexOf("WACA")) != -1 ) ||
				((index = sContractDate.indexOf("WAD")) != -1)||
				((index = sContractDate.indexOf("WBD")) != -1)||
				((index = sContractDate.indexOf("WAT")) != -1)
			)
		{
			iNoOfWeeks = Integer.parseInt(sContractDate.substring(0, index));
		}
		return iNoOfWeeks;
	}
	
}