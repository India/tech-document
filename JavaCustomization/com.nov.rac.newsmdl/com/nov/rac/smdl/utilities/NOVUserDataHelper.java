package com.nov.rac.smdl.utilities;
import java.util.Arrays;
import java.util.Vector;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.NameIconIdStruct;
import com.teamcenter.rac.kernel.TCComponentUserType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.UserList;

public class NOVUserDataHelper
{
	
	public static Vector<String> getUsers(String sGroup)
    {
		Vector<String> userData = new Vector<String>();
		try
		{
			TCSession session=(TCSession)AIFUtility.getDefaultSession();
			TCComponentUserType usertype = (TCComponentUserType) session.getTypeComponent("User");
			UserList userList = null;
			if ( sGroup.trim().length() > 0 )
			{
				userList = usertype.getUserList("*", sGroup, "*");
			}
			else
			{
				userList = usertype.getUserList("*", "*", "*");
			}
			if ( userList != null )
			{
				String[] strlist;
				strlist = userList.getFormattedList();
				for ( int k = 0; k < strlist.length; k++ )
				{
					userData.add(strlist[k]);
				}
			}
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		return userData;
    }
	//TCDECREL-1551 get updated user list
	public static Vector<String> getUpdateUserList()
	{
		Vector<String> userVector=new Vector<String>();
		try
		{
			TCSession session=(TCSession)AIFUtility.getDefaultSession();
			TCComponentUserType usertype = (TCComponentUserType) session.getTypeComponent("User");
			UserList userList = null;
			
			userList = usertype.getUserList("*", "*", "*");
			
			if ( userList != null )
			{
				String[] strlist;
				strlist = userList.getFormattedList();
				for ( int k = 0; k < strlist.length; k++ )
				{
					userVector.add(strlist[k]);
				}
			}
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		
		 
		 return userVector;
	}
}