package com.nov.rac.smdl.utilities;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.swt.widgets.Combo;

import java.util.Vector;

import com.teamcenter.rac.common.lov.LOVObject;

public class NOVSMDLComboUtils
{
      
    public NOVSMDLComboUtils()
    {
    }

    public static void setAutoComboLOV(Combo cmb, LOVObject[] arrLOVObjs) {
            if(cmb != null && arrLOVObjs != null && arrLOVObjs.length > 0) {
                  int arrLOVObjLen = arrLOVObjs.length;
                  String [] strArrLOVValues = new String[arrLOVObjLen+1];
                  strArrLOVValues[0] = "";
                  for(int inx=0; inx< arrLOVObjLen; inx++) {
                        strArrLOVValues[inx+1] = arrLOVObjs[inx].getTextValue();
                  }
                  if(strArrLOVValues != null) {
                        cmb.setItems(strArrLOVValues);
                        new AutoCompleteField(cmb, new ComboContentAdapter(), strArrLOVValues);
                  }
            }
    }
    
    public static void setAutoComboVector(Combo cmb, Vector vector)
    {
            if(cmb != null && vector != null && vector.size()>0) {
                  int vecSize = vector.size();
                  String [] strArrLOVValues = new String[vecSize];
                  for(int inx=0; inx< vecSize; inx++) {
                        strArrLOVValues[inx] = (String)vector.get(inx);
                  }
                  if(strArrLOVValues != null) {
                        cmb.setItems(strArrLOVValues);
                        new AutoCompleteField(cmb, new ComboContentAdapter(), strArrLOVValues);
                  }
            }
    }
    
    public static void setAutoComboArray(Combo cmb, String [] values)
    {
        if(cmb != null && values.length>0) 
        {
            cmb.setItems(values);
            new AutoCompleteField(cmb, new ComboContentAdapter(), values);
        }
    }
}

