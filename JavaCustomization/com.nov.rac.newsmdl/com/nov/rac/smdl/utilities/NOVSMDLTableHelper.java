package com.nov.rac.smdl.utilities;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.common.NOVSMDLTableLine;
import com.nov.rac.smdl.common.editors.NOVSMDLTableUserEditor;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.common.AIFIdentifier;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class NOVSMDLTableHelper 
{
	public static void sortTableRowsInAscendingOrder(NOVSMDLTable smdlTable, String sColumnName)
	{
		int iColIndex = smdlTable.getColumnIndexFromDataModel(sColumnName);	
		AIFIdentifier identifier = new AIFIdentifier(sColumnName, smdlTable.dataModel.getColumnOtherId(iColIndex));
		smdlTable.dataModel.sortByColumns((new AIFIdentifier[]{  identifier}));
	}
	
	//TCDECREL-4526 Kishor Ahuja added this method
	public static void removeSortCriteria(NOVSMDLTable smdlTable)
	{	
		smdlTable.dataModel.sortByColumns(null);
	}	

	public static void stopTableCellEditing(JTable table)
    {
          TableCellEditor smdlCellEditor = table.getCellEditor();
          if(smdlCellEditor != null)
          {
                if (smdlCellEditor instanceof NOVSMDLTableUserEditor)
                {
                      NOVSMDLTableUserEditor novsmdlTableUserEditor = (NOVSMDLTableUserEditor) smdlCellEditor;
                      novsmdlTableUserEditor.stopCellEditingOnClosing();
                }
                else
                {
                      smdlCellEditor.stopCellEditing();
                }
          }
    }

	public static void save(NOVSMDLTable smdltable, int rowSelected, int columnSelected) throws TCException
	{
		AIFTableLine tableLine = smdltable.getRowLine(rowSelected);
		if(tableLine instanceof NOVSMDLTableLine)
		{
			NOVSMDLTableLine smdlTableLine = (NOVSMDLTableLine)tableLine ;
			if(smdlTableLine.getTableLineAddedStatus()!= true)
			{
				Object value = smdltable.getValueAt(rowSelected, columnSelected);
				TCComponent codeObject = smdlTableLine.getTableLineCodeObject();
				CreateInObjectHelper smdlLockRevUpdateObject = new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME, CreateInObjectHelper.OPERATION_UPDATE);
				if(value instanceof TCComponentItemRevision)
				{				
					smdlLockRevUpdateObject.setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF, (TCComponent) value, CreateInObjectHelper.SET_PROPERTY);
				}
				else
				{
					smdlLockRevUpdateObject.setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF, null, CreateInObjectHelper.SET_PROPERTY);
				}
				smdlLockRevUpdateObject.setTargetObject(codeObject);
				CreateObjectsSOAHelper.createUpdateObjects(smdlLockRevUpdateObject);
				CreateObjectsSOAHelper.throwErrors(smdlLockRevUpdateObject);
				
			}					
		}	
					
	}
}