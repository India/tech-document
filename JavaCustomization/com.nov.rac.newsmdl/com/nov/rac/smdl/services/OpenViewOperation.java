package com.nov.rac.smdl.services;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseResponse2;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.SetReleaseStatusResponse;

public class OpenViewOperation implements IRunnableWithProgress
{

	TCComponent m_openItem = null;
	Registry	m_Registry = null;
	
	public OpenViewOperation(TCComponent smdlObject)
	{		
		m_openItem = smdlObject;
		m_Registry = Registry.getRegistry(this);
		
	}
	
	@Override
	public void run(IProgressMonitor iprogressmonitor)
			throws InvocationTargetException, InterruptedException {
		
//		iprogressmonitor.beginTask(m_Registry.getString("OpenView.MSG"), 1000/*
//														 * IProgressMonitor.UNKNOWN
//														 */);

		try {
			// Thread.sleep(5);
//			iprogressmonitor.done();

			Display.getDefault().syncExec(new Runnable() 
			{
				@Override
				public void run() 
				{
					IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					IWorkbenchPage page=iw.getActivePage();
//					NOVSMDLExplorerViewHelper.openView(page, m_openItem);
				}
			});

		} catch (/* InterruptedException */Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
