package com.nov.rac.smdl.services;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseResponse2;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.SetReleaseStatusResponse;

public class OpenSMDLOperation implements IRunnableWithProgress
{

	TCComponent m_openItem = null;
	Registry	m_Registry = null;
	
	public OpenSMDLOperation(TCComponent smdlObject)
	{		
		m_openItem = smdlObject;
		m_Registry = Registry.getRegistry(this);
		
	}
	
	@Override
	public void run(IProgressMonitor iprogressmonitor)
			throws InvocationTargetException, InterruptedException {
		
		iprogressmonitor.beginTask(m_Registry.getString("OpenSMDL.MSG"), 1000/*
														 * IProgressMonitor.UNKNOWN
														 */);

		try {
			// Thread.sleep(5);
			iprogressmonitor.done();

			openSMDLRevision(m_openItem);

		} catch (/* InterruptedException */Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void openSMDLRevision(TCComponent smdlObject) {
	
//		TCComponentItem smdlItem = null;
//		try {
//			if (smdlObject instanceof TCComponentItem) {
//
//				smdlItem =  (TCComponentItem) smdlObject;
//
//			} else {
//				smdlItem = ((TCComponentItemRevision)smdlObject).getItem();
//			}
//			
//			TCComponentItemRevision newRev = smdlItem.revise(smdlItem.getNewRev(),smdlItem.getTCProperty(NOV4ComponentSMDL.SMDL_NAME).getStringValue(),null);
//
//			System.out.println();
//		} catch (TCException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		
		DataManagementService dmService = DataManagementService.getService(tcSession);
		
		ReviseInfo reviseInfo = new ReviseInfo();
		
		TCComponentItemRevision smdlItemRevision = null;
		TCComponent smdlItem = null;
		
		if(smdlObject instanceof TCComponentItem){
			try {
				smdlItem = smdlObject;
				smdlItemRevision = ((TCComponentItem)smdlObject).getLatestItemRevision();
				
			} catch (TCException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else{
			try {
				smdlItem = ((TCComponentItemRevision)smdlObject).getItem();
				smdlItemRevision = (TCComponentItemRevision)smdlObject;
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		reviseInfo.baseItemRevision = smdlItemRevision;
		
		ReviseResponse2 theReviseResponse = dmService.revise2(new ReviseInfo[]{reviseInfo} );
		
		WorkflowService workFlowService = WorkflowService.getService(tcSession);
		
		ReleaseStatusInput relStatusInput = new ReleaseStatusInput();
		
		relStatusInput.objects = new TCComponent[] {smdlItem};
		
		ReleaseStatusOption relStatusOption = new ReleaseStatusOption();
		relStatusOption.existingreleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.newReleaseStatusTypeName = "";
		relStatusOption.operation = "Delete";
		
		relStatusInput.operations = new ReleaseStatusOption[]{relStatusOption};
		
		try {
			SetReleaseStatusResponse theRersponse = workFlowService.setReleaseStatus(new ReleaseStatusInput[] {relStatusInput});
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
