package com.nov.rac.smdl.services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.client.model.ErrorStack;

public class CreateOrUpdateOperation  /*extends AbstractAIFOperation*/ implements IRunnableWithProgress
{
	
	private CreateInObjectHelper[] m_transferObject;

	public CreateOrUpdateOperation(CreateInObjectHelper transferObject)
	{
		m_transferObject = new CreateInObjectHelper[] {transferObject};
	}
	

	public CreateOrUpdateOperation(CreateInObjectHelper[] transferMultipleObjects)
	{
		m_transferObject = transferMultipleObjects;
	}
	
	@Override
	public void run(IProgressMonitor iprogressmonitor) throws InvocationTargetException,
			InterruptedException {
		
		iprogressmonitor.beginTask("Create/Update in progress", /*1000*/IProgressMonitor.UNKNOWN);
		
    	try {
		
    		CreateObjectsSOAHelper.createUpdateObjects(m_transferObject);
			iprogressmonitor.done();
			
			handleErrors();
		 
		} catch ( Exception e) {
			
			e.printStackTrace();
		}
		
	}

	private void handleErrors()
	{
		
		ArrayList<String> errorMessages = new ArrayList<String>();
						
		for (int i=0;i<m_transferObject.length;i++) {
			for(ErrorStack errorStack:m_transferObject[i].getErrors()) {
				Collections.addAll(errorMessages, errorStack.getMessages());
			}
		}
		
		if(errorMessages.size()>0)
		{
			String[] errorStrings= new String[1];
			errorStrings= errorMessages.toArray(errorStrings);
			
			MessageBox.post(new TCException(errorStrings));
		}
	}

	
}
