package com.nov.rac.smdl.services;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCSession;

public class CreateDPOperation  extends AbstractAIFOperation
{
	
	private TCSession m_session;

	public CreateDPOperation()
	{
		m_session=(TCSession)AIFUtility.getDefaultSession();
	}

	@Override
	public void executeOperation() throws Exception 
	{
		System.out.println("executing createDp Operation..");
		TCComponentItem dPItem = null;
		
		TCComponentItemType itemType = (TCComponentItemType) m_session
        .getTypeComponent(NOV4ComponentDP.DP_OBJECT_TYPE_NAME);
		
		//dPItem=itemType.create(s, s1, s2, s3, s4, tccomponent);
		
		fillProperties(dPItem);
		
		
	}

	private void fillProperties(TCComponentItem dPItem) {
		
		
	}

}
