package com.nov.rac.smdl.services;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.smdl.utilities.NOVOpenObjectUtils;
import com.nov.rac.smdl.views.SMDLExplorerView;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.ui.common.RACUIUtil;

public class SMDLApplicationService extends AbstractAIFUIApplication implements IOpenService{

	public SMDLApplicationService() throws Exception {
		super();
		
	}

	@Override
	public boolean close() throws Exception {
		
		return false;
	}

	@Override
	public boolean open(InterfaceAIFComponent component) 
	{
			final TCComponent comp = (TCComponent)component;
			if(comp instanceof ISMDLObject)
			{
				Display.getDefault().syncExec(new Runnable() 
				{
				    @Override
				    public void run() 
				    {
				        IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				        
				        //try to get ui service iw.getService(arg0)
				        try 
				        {
				        	IWorkbenchPage page=iw.getActivePage();
				        
				             String secondary_Id = RACUIUtil.getMarshalledUID(comp);
				             SMDLExplorerView explorerView=(SMDLExplorerView)page.showView(SMDLExplorerView.ID,secondary_Id, 1);
	
				             explorerView.open(comp);
				             //NOVSMDLExplorerViewHelper.openView(page, comp);						
						} 
				        catch (PartInitException e) 
				        {
							
							e.printStackTrace();
						}
				    }
				});
			}
			else 
			{
				// JIRA TCDECREL-2568 
				// if object not of type ISMDL, open it in native application
				TCSession session = (TCSession) AIFUtility.getDefaultSession();
				NOVOpenObjectUtils.invokeObjectApplication(session, comp);
			}
		return false;
	}

	
	@Override
	public boolean open(InterfaceAIFComponent[] ainterfaceaifcomponent) {
		
		
		return false;
	}

}
