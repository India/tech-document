package com.nov.rac.smdl.services;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.close.SMDLCloseWizard;
import com.nov.rac.smdl.close.SMDLCloseWizardDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseResponse2;

public class CloseSMDLOperation implements IRunnableWithProgress{

	TCComponent m_closeItem = null;
	SMDLCloseWizard m_Wizard = null;
	
	Registry	m_Registry = null;
	
	
	public CloseSMDLOperation(TCComponent targetComp,SMDLCloseWizard wizard) {
		m_closeItem = targetComp;
		m_Wizard = wizard;
		
		m_Registry = Registry.getRegistry(this);
	}

	@Override
	public void run(IProgressMonitor iprogressmonitor)
			throws InvocationTargetException, InterruptedException {
		
		
		iprogressmonitor.beginTask(m_Registry.getString("CloseWizard.MSG"), 1000/*IProgressMonitor.UNKNOWN*/);
		
		    	try {
					//Thread.sleep(5);
					iprogressmonitor.done();
					
					closeSMDLObject(m_closeItem);
					
				} catch (/*InterruptedException*/ Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
	}
	
	private void closeSMDLObject(final TCComponent smdlObject) {

		Display display = Display.getDefault();

		display.syncExec(new Runnable() {
			public void run() {
				m_Wizard.populateSMDLWizard(smdlObject);
			}
		});
	}

}
