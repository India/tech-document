package com.nov.rac.smdl.panes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.SMDLDataModelConstants;
import com.nov.rac.smdl.panes.dp.DPOrderPanel;
import com.nov.rac.smdl.panes.dp.DPSMDLButtonPanel;
import com.nov.rac.smdl.panes.dp.DPSMDLTablePanel;
import com.nov.rac.smdl.utilities.ModifyTransferObject;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.smdl.utilities.ProgressMonitorHelper;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.soa.common.PolicyProperty;

public class DeliveredProductPane extends Composite implements PropertyChangeListener
{
	
	private TCSession 			m_session;
	protected Registry   		m_appReg 	= null;
	private DPSMDLTablePanel 	smdlTablePanel;
	private TCComponent 		m_dpSOAPlainObject = null;
	private NOVSMDLPolicy 		m_DPPolicy = null;
	private DPOrderPanel 		dpOrderPanel;
	private DPSMDLButtonPanel 	buttonPanel;
	private DataBindingModel 	m_theDataModel = null;
	
	
	public DeliveredProductPane(Composite parent, int style) {
		super(parent, style);
		
		m_session=(TCSession)AIFUtility.getDefaultSession();
		m_appReg= Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		//createUI( this );
				
	}


	public void createUI()
	{
		Composite parent= this;
	
		m_theDataModel = new DataBindingModel();
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.registerDataModel(SMDLDataModelConstants.DP_DATA_MODEL_NAME,m_theDataModel);
		
		setLayout(new GridLayout(1, true));
		
		dpOrderPanel = new DPOrderPanel(parent,SWT.NONE );
		dpOrderPanel.createUI();
		dpOrderPanel.setBindingContext(SMDLDataModelConstants.DP_DATA_MODEL_NAME);
		dpOrderPanel.createDataBinding();
		
		dpOrderPanel.setEditability(DPOrderPanel.DP_FORM_DISPLAY);
	/*	dpOrderPanel.getDpIDPanel().setEditability(DPOrderPanel.DP_FORM_DISPLAY);
		dpOrderPanel.getWTYPanel().setEditability(DPOrderPanel.DP_FORM_DISPLAY);
		dpOrderPanel.getMfgPanel().setEditability(DPOrderPanel.DP_FORM_DISPLAY);*/
		
		smdlTablePanel=new DPSMDLTablePanel(parent,SWT.NONE);
		smdlTablePanel.createUI();
		buttonPanel= new DPSMDLButtonPanel(parent,SWT.NONE);
		buttonPanel.createUI();
		
		buttonPanel.addPropertyChangeListener(this);
	}
	
	

	public void populateCreateInObject(CreateInObjectHelper transferObject) 
	{
		dpOrderPanel.populateCreateInObject(transferObject);
	}

	public void clearPanelValues()
	{
		dpOrderPanel.clearPanelValues();
		smdlTablePanel.clearValues();
		m_theDataModel.clear();
	}
		
	public void populateDP (TCComponent deliveredProduct)
	{
		m_dpSOAPlainObject = getDeliveredProductProps(deliveredProduct);
		
		dpOrderPanel.populate(m_dpSOAPlainObject);
		smdlTablePanel.populate(m_dpSOAPlainObject);
		buttonPanel.populate(m_dpSOAPlainObject);
		
		setDirty(false);
	}
	
	private TCComponent getDeliveredProductProps(TCComponent dpObject) {
		
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(dpObject);
		
		String[] dpAttributeList = getDPAttributeList();
		
		NOVSMDLPolicy dpPolicy = createSMDLPolicy();
		
		m_dpSOAPlainObject = dmSOAHelper.getSMDLProperties(dpObject,dpAttributeList,dpPolicy);		

		return m_dpSOAPlainObject;
	}

	private String[] getDPAttributeList() {
		String[] dpAttributeList =	{	
										NOV4ComponentDP.DP_NUMBER,
										NOV4ComponentDP.DP_DESC,
										NOV4ComponentDP.DP_BASED_ON_PROD,
										NOV4ComponentDP.DP_SERIAL_NUMBER,
										NOV4ComponentDP.ENG_LOCATIONS,
										NOV4ComponentDP.MFG_LOCATIONS,
										NOV4ComponentDP.ASSY_LOCATION,
										NOV4ComponentDP.DP_ENG_JOB_NUMBERS,
										NOV4ComponentDP.MFG_JOB_NUMBERS,
										NOV4ComponentDP.PFAT_DATE,
										NOV4ComponentDP.FAT_DATE,
										NOV4ComponentDP.SHIP_DATE,
										NOV4ComponentDP.COMMISSION_DATE,
										NOV4ComponentDP.REFURB_DATE,
										NOV4ComponentDP.QC_REQUIREMENTS,
										NOV4ComponentDP.SHIP_WTY,
										NOV4ComponentDP.COMMISSION_WTY,
										NOV4ComponentDP.DP_SMDL_RELATION
										};

		return dpAttributeList;
	}

	public NOVSMDLPolicy createSMDLPolicy() {
		if(m_DPPolicy == null){
			
			m_DPPolicy = new NOVSMDLPolicy();
							
			// Add Property policy for SMDL Item object
			Vector<NOVSMDLPolicyProperty> itemPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_ID,PolicyProperty.AS_ATTRIBUTE));		
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_NAME,PolicyProperty.AS_ATTRIBUTE));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_OBJECT_STRING));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_REL_STATUSES));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_TYPE,PolicyProperty.AS_ATTRIBUTE));
			itemPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.SMDL_RELEASE_STATUS,PolicyProperty.WITH_PROPERTIES));
			
			NOVSMDLPolicyProperty[] itemPolicyPropsArr = itemPolicyProps.toArray(new NOVSMDLPolicyProperty[itemPolicyProps.size()]);
			
			m_DPPolicy.addPropertiesPolicy(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME, itemPolicyPropsArr);
			
			// Add Property policy for Order object
			Vector<NOVSMDLPolicyProperty> orderPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_TYPE,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CUSTOMER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PLANT_ID_OTHER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PURCHASE_ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CONTRACT_AWARD_DATE,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.CONTRACT_DELIVERY_DATE,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.SALESMAN,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_MANAGER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PROJECT_MANAGER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.IC_CONTACT_MANAGER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.DOCUMENT_CONTROL,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.DOCUMENT_CONTROLLER,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.ORDER_MAILBOX,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.IC_MAILBOX,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.COUNTRY_DESTINATION,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.EXTERNAL_CONTACT,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.EXTERNAL_PROJECT_NUM,PolicyProperty.AS_ATTRIBUTE));
			orderPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentOrder.PROJECT_SITE,PolicyProperty.AS_ATTRIBUTE));
			
			NOVSMDLPolicyProperty[] orderPolicyPropsArr = orderPolicyProps.toArray(new NOVSMDLPolicyProperty[orderPolicyProps.size()]);
			
			m_DPPolicy.addPropertiesPolicy(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME, orderPolicyPropsArr);
			
			// Add Property policy for Release Status object
			Vector<NOVSMDLPolicyProperty> relStatusPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			relStatusPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REL_STATUS_NAME,PolicyProperty.AS_ATTRIBUTE));			
			
			NOVSMDLPolicyProperty[] relStatusPolicyPropsArr = relStatusPolicyProps.toArray(new NOVSMDLPolicyProperty[relStatusPolicyProps.size()]);
			
			m_DPPolicy.addPropertiesPolicy(NOVSMDLConstants.REL_STATUS_OBJECT_TYPE_NAME, relStatusPolicyPropsArr);
			
			// Add Property policy for _novlocations_ object
			Vector<NOVSMDLPolicyProperty> novLocPolicyProps = new Vector<NOVSMDLPolicyProperty>();
			novLocPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.LOCATION_NAME,PolicyProperty.AS_ATTRIBUTE));			
			
			NOVSMDLPolicyProperty[] novLocPolicyPropsArr = novLocPolicyProps.toArray(new NOVSMDLPolicyProperty[novLocPolicyProps.size()]);
			
			m_DPPolicy.addPropertiesPolicy(NOVSMDLConstants.LOCATION_OBJECT_TYPE_NAME, novLocPolicyPropsArr);

		}
		
		return m_DPPolicy;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals("CreateSMDLOperation") )
		{
			TCComponent theCreatedObject = (TCComponent) event.getNewValue();
			
			smdlTablePanel.addCreatedSMDL(theCreatedObject);	
		}
		
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		smdlTablePanel.addPropertyChangeListener(listener);
	}

    public void save(IProgressMonitor progressMonitor)
	{
        if (!dpOrderPanel.validateSave())
        {
            MessageBox.post(this.getShell(), m_appReg.getString("CantSaveValidation.msg"), "Error", MessageBox.ERROR);
        }
        else
        {
            ProgressMonitorHelper progressMonitorHelper = new ProgressMonitorHelper(progressMonitor);
            progressMonitorHelper.beginTask("Create/Update in progress", IProgressMonitor.UNKNOWN);
            
            try
            {
                //TCDECREL-4526 Start
                CreateInObjectHelper[] transferObjects= new CreateInObjectHelper[]{ createOperationInput()};
				CreateObjectsSOAHelper.createUpdateObjects(transferObjects);				
				progressMonitorHelper.done();
				CreateObjectsSOAHelper.handleErrors(transferObjects);
                //TCDECREL-4526 End
                
        
                setDirty(false);
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
            }
            finally
            {
                progressMonitorHelper.done();
            }
        }
	}
		
	private CreateInObjectHelper createOperationInput() throws TCException
	{
		CreateInObjectHelper transferObject= new CreateInObjectHelper(NOV4ComponentDP.DP_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
			
		populateCreateInObject(transferObject);
		transferObject.setTargetObject(m_dpSOAPlainObject);
			
		//TCDECREL-4526: Populate only the modified properties inside the map and erase others
		ModifyTransferObject modifyTransferObj = new ModifyTransferObject();		
		CreateInObjectHelper slimPropertyMap = modifyTransferObj.cloneCreateInObjectProperties(transferObject, CreateInObjectHelper.OPERATION_CREATE.getMode()/*1*/);
	    modifyTransferObj.retainModifiedValues(transferObject, slimPropertyMap);	
	    
	    return slimPropertyMap;
	    //TCDECREL-4526 commented out
		//return transferObject;
	}
	
	public boolean isDirty(){
		
		return m_theDataModel.isDirty();
	}
	
	public void setDirty(boolean flag){
		
		m_theDataModel.setDirty(flag);
	}
	
}
