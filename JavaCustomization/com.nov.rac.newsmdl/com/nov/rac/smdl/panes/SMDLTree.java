package com.nov.rac.smdl.panes;

import javax.swing.JScrollPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.utilities.NOVSMDLTree;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.SWTUIUtilities;

public class SMDLTree extends Composite
{

	private NOVSMDLTree m_tcTree= null;
	
	
	public NOVSMDLTree getM_tcTree() {
		return m_tcTree;
	}



	public SMDLTree(Composite parent, int style) {
		
		super(parent, SWT.EMBEDDED);
		
		createTree();
 
       
        TCTreeNode rootItemNode=null;
        TCComponentFolder homeFolder=null;
        TCSession session= (TCSession)AIFUtility.getDefaultSession();
        try {
			homeFolder = session.getUser().getHomeFolder();
			rootItemNode=new TCTreeNode(homeFolder);
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		
	//	populateTree(homeFolder);
		
	}


	
	public void populateTree(TCComponent component) {
		m_tcTree.setRoot(new TCTreeNode(component));
		m_tcTree.setRootVisible(true);
	    m_tcTree.setScrollsOnExpand(true);
	}


	private void createTree() {
		m_tcTree = new NOVSMDLTree();
		JScrollPane treePane= new JScrollPane(m_tcTree);
		SWTUIUtilities.embed( this, treePane, false);
		setLocation(10, 10);
		setLayout(new FillLayout());
	}

	

}

//set tree root
/* TCComponentItem myItem=null;
try 
{
	TCSession session= (TCSession)AIFUtility.getDefaultSession();
	TCComponentItemType type=(TCComponentItemType)session.getTypeComponent("Item");
	//myItem = type.find("MYITEM");
	myItem = type.find("51000068:9D");
	
	M000000019
} catch (TCException e) {
	
	e.printStackTrace();
}
Registry registry=Registry.getRegistry(this);

Icon rootImage = registry.getImageIcon("PartIcon.Image");
// AIFTreeNode rootItemNode=new AIFTreeNode(myItem);
TCTreeNode rootItemNode=new TCTreeNode(myItem);
rootItemNode.setNodeIcon(rootImage);
m_tcTree.setRoot(rootItemNode);
m_tcTree.setRootVisible(true);*/