package com.nov.rac.smdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.noi.util.components.JIBPlantLOVList;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVAddTextFieldVerifyListener;
import com.nov.rac.smdl.common.SMDLDataModelConstants;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class SMDLInfoPane extends Composite {
	private Text m_txtCustomer;
	private Text m_txtRigName;
	private Text m_txtProduct;
	private Text m_txtSalesOrder;
	//private Text m_txtEngJob;
	private Text m_txtEngJob;
	//private Text m_txtSMDLType;
	private Text m_txtSMDLTitle;
	private Text m_txtSMDLDesc;
	
	private Label m_lblCustomer;
	private Label m_lblRigName;
	private Label m_lblProduct;
	private Label m_lblSalesOrder;
	private Label m_lblEngJob;
	private Label m_lblSMDLType;
	private Label m_lblSMDLTitle;
	private Label m_lblSMDLDesc;
	
	private Combo m_smdlTypeCombo;
	
	private TCComponent m_SMDLItem = null;
	private TCComponent m_DPObject = null;
	private TCComponent m_OrderObject = null;
	private Registry	m_Registry = null;
	
	private static JIBPlantLOVList m_soaPlantList = null;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public SMDLInfoPane(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		createUI(this);
		
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.panes.panes");
				
		populateSMDLInfoLabels();
	}

	private void createUI(Composite parent) {
		
		Composite titleBlock = new Composite(parent, SWT.NONE);
		GridData gd_titleBlock = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		titleBlock.setLayoutData(gd_titleBlock);
		titleBlock.setLayout(new GridLayout(4, false));
				
		m_lblSMDLTitle = new Label(titleBlock, SWT.NONE);
		
		m_txtSMDLTitle = new Text(titleBlock, SWT.BORDER);
		GridData gd_m_txtSMDLTitle = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		m_txtSMDLTitle.setLayoutData(gd_m_txtSMDLTitle);
		m_txtSMDLTitle.setEditable(false);
		
		m_lblSMDLDesc = new Label(titleBlock, SWT.NONE);
		m_lblSMDLDesc.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		m_txtSMDLDesc = new Text(titleBlock, SWT.MULTI|SWT.BORDER|SWT.WRAP);
		GridData gd_m_txtSMDLDesc = new GridData(SWT.FILL, SWT.CENTER, true, false,1, 1);
		gd_m_txtSMDLDesc.heightHint = 30;
		m_txtSMDLDesc.setLayoutData(gd_m_txtSMDLDesc);
		m_txtSMDLDesc.setEditable(false);
		
		m_txtSMDLDesc.setTextLimit(240); //TCDECREL-2368
		
//		m_lblEngJob = new Label(titleBlock, SWT.NONE);
		
		Composite smdlInfoComposite = new Composite(parent, SWT.NONE);
		GridData gd_smdlInfo = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		smdlInfoComposite.setLayoutData(gd_smdlInfo);
		smdlInfoComposite.setLayout(new GridLayout(6, true));
										
		m_lblCustomer = new Label(smdlInfoComposite, SWT.NONE);
		m_lblRigName = new Label(smdlInfoComposite, SWT.NONE);
		m_lblProduct = new Label(smdlInfoComposite, SWT.NONE);
		m_lblSalesOrder = new Label(smdlInfoComposite, SWT.NONE);
		m_lblEngJob = new Label(smdlInfoComposite, SWT.NONE);
		m_lblSMDLType = new Label(smdlInfoComposite, SWT.NONE);
		
		m_txtCustomer = new Text(smdlInfoComposite, SWT.BORDER);
		m_txtCustomer.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		m_txtCustomer.setEditable(false);
		
		m_txtRigName = new Text(smdlInfoComposite, SWT.BORDER);
		m_txtRigName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		m_txtRigName.setEditable(false);
		
		m_txtProduct = new Text(smdlInfoComposite, SWT.BORDER);
		m_txtProduct.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		m_txtProduct.setEditable(false);
		
		m_txtSalesOrder = new Text(smdlInfoComposite, SWT.BORDER);
		m_txtSalesOrder.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		m_txtSalesOrder.setEditable(false);
		
		m_txtEngJob = new Text(smdlInfoComposite, SWT.BORDER | SWT.MULTI | SWT.WRAP);
		m_txtEngJob.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		GridData gd_EngJob = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_EngJob.heightHint = m_txtEngJob.getLineHeight() * 2;
		
		m_txtEngJob.setLayoutData(gd_EngJob);
		m_txtEngJob.setEditable(false);
		
//		m_txtEngJob.setBackground(smdlInfoComposite.getBackground());		
//		m_txtEngJob = new List(smdlInfoComposite, SWT.BORDER | SWT.V_SCROLL );
//		m_txtEngJob.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

//		m_txtSMDLType = new Text(smdlInfoComposite, SWT.BORDER);
//		m_txtSMDLType.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));	
//		m_txtSMDLType.setEditable(false);
		
		//m_smdlTypeCombo = new Combo(smdlInfoComposite, SWT.DROP_DOWN | SWT.BORDER);
		m_smdlTypeCombo = new Combo(smdlInfoComposite, SWT.DROP_DOWN | SWT.READ_ONLY);
		GridData gd_m_smdlTypeCombo = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		m_smdlTypeCombo.setLayoutData(gd_m_smdlTypeCombo);
		m_smdlTypeCombo.setEnabled(false);
		
		createDataBinding();
		smdl_populateLOVs();
		addListeners();
	}
	
	private void smdl_populateLOVs() 
	{
		String[] smdlTypeValues = NOVCreateSMDLHelper.getSMDLTypes();
		NOVSMDLComboUtils.setAutoComboArray(m_smdlTypeCombo, smdlTypeValues);
	}
	private void addListeners()
	{
		m_txtSMDLDesc.addVerifyListener(new NOVAddTextFieldVerifyListener());
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	private void populateLOVs() {

		if (m_soaPlantList == null) {
			m_soaPlantList = new JIBPlantLOVList();
			m_soaPlantList.init();
		}
	}
	
	public void populateSMDLInfoPane(TCComponent smdlRev){
		
		populateLOVs();
		
		//TODO Nams fill smdl type & Name
		try {
			String smdlID = smdlRev.getTCProperty(NOV4ComponentSMDL.SMDL_ID).getStringValue();
			String smdlRevId = smdlRev.getTCProperty(NOV4ComponentSMDL.SMDL_REVISION_ID).getStringValue();
			
			m_txtSMDLTitle.setText(smdlID + "/" + smdlRevId);
			
			m_SMDLItem = smdlRev.getTCProperty(NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();

			TCComponent[] smdl_statusList = smdlRev.getTCProperty(NOVSMDLConstants.SMDL_RELEASE_STATUS).getReferenceValueArray();
			if (smdl_statusList != null && smdl_statusList.length > 0)
			{
				m_txtSMDLDesc.setEditable(false);
				//m_txtSMDLType.setEditable(false);
				m_smdlTypeCombo.setEnabled(false);;
			}
			else
			{
				m_txtSMDLDesc.setEditable(true);
				//m_txtSMDLType.setEditable(true);
				m_smdlTypeCombo.setEnabled(true);
			}
			
			
			String smdlDesc = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.SMDL_DESC).getStringValue();
			m_txtSMDLDesc.setText(smdlDesc);
			
			// Get SMDL Type Information
//			String smdlType = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.SMDL_TYPE).getStringValue();
//			m_txtSMDLType .setText(smdlType);
			
			String smdlType = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.SMDL_TYPE).getStringValue();
			m_smdlTypeCombo.setText(smdlType);
			
			
			// Get DP Object
			m_DPObject = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.DP_REF).getReferenceValue();
			if(m_DPObject != null){
				m_txtProduct.setText(m_DPObject.getTCProperty(NOV4ComponentDP.DP_BASED_ON_PROD).getStringValue());
				String[] strEngJobNumbes = m_DPObject.getTCProperty(NOV4ComponentDP.DP_ENG_JOB_NUMBERS).getStringValueArray();
				
				for(int iCnt = 0; iCnt < strEngJobNumbes.length; iCnt++){
					if(iCnt != strEngJobNumbes.length -1){
						m_txtEngJob.setText(m_txtEngJob.getText() + strEngJobNumbes[iCnt] + ", ");
					}
					else{
						m_txtEngJob.setText(m_txtEngJob.getText() + strEngJobNumbes[iCnt]);
					}
				}
			}
			
			// Get DP Object
			m_OrderObject = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.ORDER_REF).getReferenceValue();
			if(m_OrderObject != null){
				m_txtCustomer.setText(m_OrderObject.getTCProperty(NOV4ComponentOrder.CUSTOMER).getStringValue());
				m_txtSalesOrder.setText(m_OrderObject.getTCProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER).getStringValue());
				
				int plantIdInt = m_OrderObject.getTCProperty(NOV4ComponentOrder.PLANT_ID_OTHER).getIntValue();
				
				String plantIdStr = m_soaPlantList.getStringFromReference(plantIdInt);
				
				if(plantIdStr != null)
					m_txtRigName.setText(plantIdStr);

			}
			
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void populateSMDLInfoLabels(){
		m_lblSMDLTitle.setText(m_Registry.getString("SMDL_Title.Label"));
		m_lblSMDLDesc.setText(m_Registry.getString("SMDL_Desc.Label"));
		m_lblCustomer.setText(m_Registry.getString("SMDL_Customer.Label"));
		m_lblRigName.setText(m_Registry.getString("SMDL_Rig.Label"));
		m_lblProduct.setText(m_Registry.getString("SMDL_Product.Label"));
		m_lblSalesOrder.setText(m_Registry.getString("SMDL_SalesOrder.Label"));
		m_lblEngJob.setText(m_Registry.getString("SMDL_EngJob.Label"));
		m_lblSMDLType.setText(m_Registry.getString("SMDL_SMDLType.Label"));
		
	}
	public void clearPanelValues()
	{
		m_txtSMDLTitle.setText("");
		m_txtSMDLDesc.setText("");
//		m_txtSMDLType.setText("");
		m_txtProduct.setText("");
		m_txtEngJob.setText("");
		m_txtCustomer.setText("");
		m_txtSalesOrder.setText("");
		m_txtRigName.setText("");
	}
	public void createDataBinding()
	{
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		dataBindingHelper.bindData(m_txtSMDLDesc,NOV4ComponentSMDL.SMDL_DESC,SMDLDataModelConstants.SMDL_DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_smdlTypeCombo,NOV4ComponentSMDL.SMDL_TYPE,SMDLDataModelConstants.SMDL_DATA_MODEL_NAME);
	}

	public void populateCreateInObject(CreateInObjectHelper transferObject)
	{
		transferObject.setStringProps(NOV4ComponentSMDL.SMDL_DESC,m_txtSMDLDesc.getText(),CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.SMDL_TYPE,m_smdlTypeCombo.getText(),CreateInObjectHelper.SET_PROPERTY);
		
	}
	//ankita
	public boolean validateSave()
	{
		
		if(m_txtSMDLTitle.getText().length()==0)
		{
			return false;
		}
		return true;
		
	}

}
