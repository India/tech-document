package com.nov.rac.smdl.panes;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import javax.swing.table.TableModel;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.common.NOVAddCodeButtonListener;
import com.nov.rac.smdl.common.NOVRemoveCodeButtonListener;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.common.NOVSMDLTableLine;
import com.nov.rac.smdl.common.NOVSMDLTableModel;
import com.nov.rac.smdl.common.NOVSMDLTableMouseListener;
import com.nov.rac.smdl.common.SMDLDataModelConstants;
import com.nov.rac.smdl.databinding.NOVSMDLDataBindingHelper;

import com.nov.rac.smdl.utilities.ModifyTransferObject;
import com.nov.rac.smdl.utilities.NOVGroupDataHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.utilities.NOVSMDLObjectSOAHelper;
import com.nov.rac.smdl.utilities.NOVSMDLTableHelper;
import com.nov.rac.smdl.utilities.ProgressMonitorHelper;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class SMDLPane extends Composite {
	
	//public static final String 	DATA_MODEL_NAME = "SMDL";
	
	private SMDLInfoPane topPanel;
	private Composite buttonPanel;
	private Composite tablePanel;
    private NOVSMDLTable  smdlTable  = null;
 	protected Registry   appReg 	= null;
 	private TCComponent m_SMDLItem = null;
	private TCComponent m_smdlSOAPlainObject = null;
	private TCSession m_session = null;
	private Button m_btnAddButton;
	private Button m_btnRemoveButton;
	//private SMDLPaneButtonPanel m_bottomButtonpanel;
	private HashSet<TCComponent> m_docsTobeAttachedToSMDLRev = new HashSet<TCComponent>();
	private HashSet<TCComponent> m_codesTobeAttachedToSMDLRev = new HashSet<TCComponent>();
	private HashMap<String,TCComponent> m_groupDataMap = new HashMap<String,TCComponent>();
	
	private DataBindingModel 	m_theDataModel = null;
	private NOVSMDLTableMouseListener m_smdlTableMouseListener;
	private String m_sLoggedInUser = null;
	
	public SMDLPane(Composite parent, int style)
	{
		super(parent, style);
		m_session= (TCSession)AIFUtility.getDefaultSession();
		appReg = Registry.getRegistry("com.nov.rac.smdl.common.common");
		createUI(this);
	}

	private void createUI(Composite parent) {
		
		m_theDataModel = new DataBindingModel();
		
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.registerDataModel(SMDLDataModelConstants.SMDL_DATA_MODEL_NAME,m_theDataModel);
		
		setLayout(new GridLayout(1, true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		topPanel = new SMDLInfoPane(parent, SWT.NONE);
		
		createButtonPanel(parent);
		createTablePanel(parent);
		
		
		//m_bottomButtonpanel= new SMDLPaneButtonPanel(parent,SWT.NONE);
		
		addListener();
		
		createDataBinding();
		
	}

	private void createTablePanel(Composite parent) {
		tablePanel = new Composite(parent, SWT.EMBEDDED);
		tablePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		createSMDLTable();
		
		ScrollPagePane tablePane= new ScrollPagePane(smdlTable);
				
		/*SoftBevelBorder softBorder = new SoftBevelBorder(
                SoftBevelBorder.RAISED);
        java.awt.Color highlightInnerColor = softBorder.getHighlightInnerColor();
        java.awt.Color shadowOuterColor = softBorder.getShadowOuterColor();
        java.awt.Color shadowInnerColor = softBorder.getShadowInnerColor();
        SoftBevelBorder borderToSetToComp = new SoftBevelBorder(
                SoftBevelBorder.RAISED , new java.awt.Color(233 , 233 , 218) ,
                highlightInnerColor , shadowOuterColor , shadowInnerColor);
        tablePane.setBorder(borderToSetToComp);
        tablePane.getViewport().setBackground(java.awt.Color.WHITE);*/
        
       	SWTUIUtilities.embed( tablePanel, tablePane, false);
	}

	private void createButtonPanel(Composite parent) 
	{
		buttonPanel = new Composite(parent, SWT.NONE);
		RowLayout buttonPanelLayout=new RowLayout();
		buttonPanelLayout.pack=false;
		buttonPanel.setLayout(buttonPanelLayout);
		buttonPanel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false, 1, 1));
		
		m_btnAddButton = new Button(buttonPanel, SWT.CENTER);
		m_btnAddButton.setText(appReg.getString("createDocumentsActionForSMDL.NAME"));
		m_btnAddButton.setImage(appReg.getImage("createDocumentsActionForSMDL.ICON"));
	
		m_btnRemoveButton = new Button(buttonPanel, SWT.CENTER);
		m_btnRemoveButton.setText(appReg.getString("removeDocumentsActionForSMDL.NAME"));
		m_btnRemoveButton.setImage(appReg.getImage("removeDocumentsActionForSMDL.ICON"));
	}


	private void createSMDLTable()
	{
		String[] strColNames=null;
		String[] strMasterColNames=null;
		
		strMasterColNames = getMasterSMDLTableColumns();
		
        smdlTable = new NOVSMDLTable(m_session , strMasterColNames);
     
        smdlTable.setTableColumnNamePreference( NOVSMDLConstants.NOVSMDL_TABLECOLUMNS_PREF, TCPreferenceService.TC_preference_user);
		strColNames= getSMDLTableColumns();
//        if(strColNames != null)
//        {
//        	smdlTable.reOrderColumns(strColNames);
//        }
        m_smdlTableMouseListener = new NOVSMDLTableMouseListener(m_session);
        smdlTable.addMouseListener(m_smdlTableMouseListener);
	}
	
	//this will take SMDLObject
	public void populateSMDL(TCComponent smdlRevObject){
		
		//Mohan - Moved getSMDLRevisionProps to Helper class 
		//m_smdlSOAPlainObject = getSMDLRevisionProps(smdlRevObject);
		
		m_smdlSOAPlainObject = NOVSMDLObjectSOAHelper.getSMDLRevisionProps(smdlRevObject);
		
		topPanel.populateSMDLInfoPane(m_smdlSOAPlainObject);
		
		populateTable(smdlTable);
		
		setDirty(false);
		
		try 
		{//TCDECREL-1551 set the doc responsible in table
			m_SMDLItem = smdlRevObject.getTCProperty(NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
			NOVSMDLExplorerViewHelper.setDocResponsible(m_SMDLItem.getTCProperty(NOV4ComponentSMDL.DOC_RESPONSIBLE).getStringValue());
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//m_bottomButtonpanel.populateSMDL(smdlRevObject);
	}


	private void populateTable(NOVSMDLTable smdlTable) 
	{
        //Get the typed reference for all code forms attached to SMDL Revision
		
		//Removed the search criteria which was causing codeforms comparison issues 
		NOVSMDLTableHelper.removeSortCriteria(smdlTable);
		smdlTable.removeAllRows();
		
        if (m_smdlSOAPlainObject != null)
        {
			try
			{
				// Get SMDL Code Forms, and pass them to helper.
				TCComponent[] codeFormObjs = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();				
				TableUtils.populateTable(codeFormObjs, smdlTable);
				
				TableModel smdlTableModal = smdlTable.getModel();				
				int groupColumn = smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.GROUP_NAME);
				
				AIFTableLine [] tableLines = smdlTable.getRowLines(0, codeFormObjs.length-1);
				
				for(int inx=0; inx<codeFormObjs.length; inx++)
				{	
					
					String groupName=getGroupName(codeFormObjs[inx]);
					smdlTableModal.setValueAt(groupName, inx, groupColumn);
					if(tableLines[inx] != null && tableLines[inx] instanceof NOVSMDLTableLine)
					{
						NOVSMDLTableLine smdlTableLine = ((NOVSMDLTableLine)(tableLines[inx]));
						smdlTableLine.setTableLineCodeObject(codeFormObjs[inx]);
					}
					
				}
				
				NOVSMDLTableHelper.sortTableRowsInAscendingOrder(smdlTable, NOV4ComponentSMDL.SMDL_CODE_NUMBER);
				
				//NOVSMDLTableHelper.populateSMDLTableData(m_smdlSOAPlainObject, smdlTable);
			}
			catch (TCException e1)
			{
				e1.printStackTrace();
			}	
        
        }
	}

	private String getGroupName(TCComponent codeFormObjs)
	{
		String groupname=null;
		try 
		{
			String m_group = codeFormObjs.getTCProperty(NOV4ComponentSMDL.GROUP_NAME).getDisplayableValue();
			if(m_group != "")
			{
				groupname=m_group;
			}
			else
			{
				TCComponent smdlDoc = codeFormObjs.getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
				if(smdlDoc!=null)
				{
					groupname=( smdlDoc.getTCProperty(NOVSMDLConstants.DOCUMENT_OWNING_GROUP).getReferenceValue()).toString();
				}
				else
				{
					groupname="";
				}
			}
		} 
		catch (TCException exp)
		{			
			exp.printStackTrace();
		}

		return groupname;
		
	}
	private String[] getSMDLTableColumns() 
	{
		String[] strColNames = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = NOVSMDLConstants.NOVSMDL_TABLECOLUMNS_PREF;
		strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_user, smdlTableColumnPref);
		return strColNames;

	}
	private String[] getMasterSMDLTableColumns()
	{
		String[] strColNames = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = NOVSMDLConstants.NOVSMDL_TABLECOLUMNS_PREF;
			strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_site, smdlTableColumnPref);
			if (null != strColNames && strColNames.length == 0) 
			{
				MessageBox.post(appReg.getString("prefError.MSG"), "ERROR",
					MessageBox.ERROR);
			}
		return strColNames;

	}

	public NOVSMDLTable getSmdlTable() 
	{
		return smdlTable;
	}

	public TCComponentItem findDocItem(TCSession session, String strDocId)
	{
		TCComponentItem docItem = null;
		TCComponentItemType itemType = null;
		try 
		{			
			itemType = (TCComponentItemType)session.getTypeService().getTypeComponent(NOVSMDLConstants.DOC_OBJECT_TYPE_NAME);
			if(itemType != null)
			{
				docItem = itemType.find(strDocId);
			}
		}
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return docItem;
	}	
	public TCComponent getDPObject()
	{
		TCComponent dpComp = null;
		TCComponentItem smdlItem = null; 
		try 
		{
			smdlItem = (TCComponentItem) m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
			if(smdlItem instanceof  NOV4ComponentSMDL)
			{
				dpComp = smdlItem.getTCProperty(NOV4ComponentSMDL.DP_REF).getReferenceValue();
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		return dpComp;
	}
	private void addListener()
	{
		m_btnAddButton.addSelectionListener(new NOVAddCodeButtonListener(smdlTable));
		m_btnRemoveButton.addSelectionListener(new NOVRemoveCodeButtonListener());
		m_btnRemoveButton.setData(smdlTable);
	}
	public void setDPObject()
	{
		TCComponent dpComponent = getDPObject();
		m_btnAddButton.setData(dpComponent);
		m_smdlTableMouseListener.setDPComponent(dpComponent);
	}
	public void clearValues()
	{
		smdlTable.removeAllRows();
	}
	public void clearPanelValues()
	{
		topPanel.clearPanelValues();
		clearValues();
		m_theDataModel.clear();
	}
	public CreateInObjectHelper[] populateCreateInObject()
	{		
		NOVSMDLTableHelper.stopTableCellEditing(smdlTable);
		
		int iTotalRows = smdlTable.getRowCount();
		Vector<CreateInObjectHelper> createCodeTransferObject = new Vector<CreateInObjectHelper>();
		m_docsTobeAttachedToSMDLRev.clear();
		m_codesTobeAttachedToSMDLRev.clear();
		m_groupDataMap = NOVGroupDataHelper.getGroupData();
		m_sLoggedInUser = m_session.getUser().toString();
		for(int iRow = 0;iRow<iTotalRows;iRow++)
		{
			AIFTableLine tableLine = smdlTable.getRowLine(iRow);
			if(tableLine instanceof NOVSMDLTableLine)
			{
				NOVSMDLTableLine smdlTableLine = (NOVSMDLTableLine)tableLine;
				TCComponent docItem = null;
				
				TCComponent codeObject = smdlTableLine.getTableLineCodeObject();
				Object docObj = smdlTableLine.getProperty(NOV4ComponentSMDL.DOCUMENT_REF);
				if(docObj!= null && docObj.toString().trim().length()>0)
				{
					docItem = (TCComponent)docObj;
					m_docsTobeAttachedToSMDLRev.add(docItem);
				}
				
				if(smdlTableLine.getTableLineAddedStatus() == true)
				{
					CreateInObjectHelper createCodeTransferObj= new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_CREATE);
					createCodeTransferObj = createTransferObject(createCodeTransferObj,smdlTableLine);					
					createCodeTransferObj.setStringProps(NOV4ComponentSMDL.NOV_CODEFORM_NUM, "", CreateInObjectHelper.SET_PROPERTY);
					createCodeTransferObject.add(createCodeTransferObj);
				}
				else if(smdlTableLine.getTableLineUpdatedStatus() == true)
				{
					CreateInObjectHelper createCodeTransferObj= new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);					
					createCodeTransferObj = createTransferObject(createCodeTransferObj,smdlTableLine);										
					createCodeTransferObj.setTargetObject(codeObject);
					
					createCodeTransferObject.add(createCodeTransferObj);
				}
				else if(codeObject != null)
				{
					m_codesTobeAttachedToSMDLRev.add(codeObject);
				}
			}
		}	
		return createCodeTransferObject.toArray(new CreateInObjectHelper[createCodeTransferObject.size()]);
	}
	public HashSet<TCComponent> getDocuments()
	{
		return m_docsTobeAttachedToSMDLRev;
	}
	
	
	//TCDECREL-4526 Get all the cached documents
	public List<TCComponent> getCachedDocs(TCProperty docsCached)
	{
		List<TCComponent> cachedDocuments = new ArrayList<TCComponent>();
		TCComponent[] cachedDocs = docsCached.getReferenceValueArray();
		Collections.addAll(cachedDocuments, cachedDocs );
		
		return cachedDocuments;
	}
	//TCDECREL-4526 getCachedDocs End

	//TCDECREL-4526: Check if documents are modified
	public boolean hasDocumentAttributeChanged( List<TCComponent> cachedDocs, HashSet<TCComponent> newDocs )
	{
		boolean areDocumentsModified = false;
		if( cachedDocs.size() != newDocs.size())
		{
			areDocumentsModified = true;
		}
		else
		{
			cachedDocs.removeAll(newDocs);
			if(!(cachedDocs.size() == 0))
			{
				areDocumentsModified = true;
			}	
		}
		
		return areDocumentsModified;
	}
	//TCDECREL-4526 hasDocumentAttributeChanged End
	
	
	public void setButtonsEnableStatus()
	{
		boolean bIsButtonEnabled = true;
		try 
		{
			TCComponent[] statusList = m_smdlSOAPlainObject.getTCProperty(NOVSMDLConstants.SMDL_RELEASE_STATUS).getReferenceValueArray();
			if (statusList != null && statusList.length > 0)
			{
				bIsButtonEnabled = false;
			}
			else
			{
				bIsButtonEnabled = true;
			}
		}
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		m_btnAddButton.setEnabled(bIsButtonEnabled);
		m_btnRemoveButton.setEnabled(bIsButtonEnabled);
		smdlTable.setEnabled(bIsButtonEnabled);
	}
	public HashSet<TCComponent> getCodeObjects()
	{
		return m_codesTobeAttachedToSMDLRev;
	}
	public CreateInObjectHelper createTransferObject(CreateInObjectHelper transferObject, NOVSMDLTableLine smdlTableLine)
	{
		Object linePropertyObject = null;
		String sCode = smdlTableLine.getProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER).toString();
		String sAddnlCode = "";
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.ADDITIONAL_CODE);
		if(linePropertyObject != null)
		{
			sAddnlCode = linePropertyObject.toString();
		}
		String sCustCode = "";
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.CUSTOMER_CODE);
		if( linePropertyObject != null)
		{
			sCustCode = linePropertyObject.toString();
		}
		String sResPerson = "";
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);
		if(linePropertyObject != null && linePropertyObject.toString().trim().length()>0)
		{
			sResPerson = linePropertyObject.toString();
		}
		else
		{
			//Mohan: TCDECREL-2193: if user clears/ nulls out Doc Responsible without adding another name, system will automatically default to user creating/editing SMDL 
			sResPerson = m_sLoggedInUser;
		}
		String sComments = "";
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.COMMENTS);
		if(linePropertyObject != null)
		{
			sComments = linePropertyObject.toString();
		}					
		String sLineItemNo = "";
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.LINE_ITEM_NUMBER);
		if(linePropertyObject != null)
		{
			sLineItemNo = linePropertyObject.toString();
		}
		String sCodeTab = "";
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.CODEFORM_TAB);
		if(linePropertyObject != null)
		{
			sCodeTab = linePropertyObject.toString();
		}
		
		TCComponent docItem = null;
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.DOCUMENT_REF);
		if(linePropertyObject != null)
		{
			docItem = (TCComponent) linePropertyObject;
		}
		
		TCComponent docItemRev = null;
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF);
		if(linePropertyObject != null)
		{
			docItemRev = (TCComponent) linePropertyObject;
		}
		
		transferObject.setStringProps(NOV4ComponentSMDL.SMDL_CODE_NUMBER, sCode, CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.ADDITIONAL_CODE, sAddnlCode, CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.CUSTOMER_CODE, sCustCode, CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE, sResPerson, CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.COMMENTS, sComments, CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.LINE_ITEM_NUMBER, sLineItemNo, CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringProps(NOV4ComponentSMDL.CODEFORM_TAB, sCodeTab, CreateInObjectHelper.SET_PROPERTY);
		
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
		Date dContractDate = null;
		if(linePropertyObject != null)
		{
			dContractDate = (Date)linePropertyObject;
			transferObject.setDateProps(NOV4ComponentSMDL.DOC_CONTRACT_DATE, dContractDate, CreateInObjectHelper.SET_PROPERTY);
		}
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.DOC_PLANNED_DATE);
		Date dPlannedDate;
		if(linePropertyObject != null)
		{
			dPlannedDate = (Date)linePropertyObject;
			transferObject.setDateProps(NOV4ComponentSMDL.DOC_PLANNED_DATE, dPlannedDate, CreateInObjectHelper.SET_PROPERTY);
		}	
		
		//if(docItem!= null)
		{
			transferObject.setTagProps(NOV4ComponentSMDL.DOCUMENT_REF, docItem, CreateInObjectHelper.SET_PROPERTY);
			transferObject.setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF, docItemRev, CreateInObjectHelper.SET_PROPERTY);
		}
		
		linePropertyObject = smdlTableLine.getProperty(NOV4ComponentSMDL.GROUP_NAME);
		TCComponent groupComponent = null;
		if(linePropertyObject != null && linePropertyObject.toString().trim().length()>0)
		{
			groupComponent = m_groupDataMap.get(linePropertyObject.toString());
		}
		else
		{
			//TCDECREL-2723 : If document is selected and Group field contains invalid group name or blank value 
			//                then Group field should default to document's owning group
			if(docItem!= null)
			{
				try 
				{
					groupComponent = docItem.getTCProperty(NOVSMDLConstants.DOCUMENT_OWNING_GROUP).getReferenceValue();
				} 
				catch (TCException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		transferObject.setTagProps(NOV4ComponentSMDL.GROUP_NAME, groupComponent, CreateInObjectHelper.SET_PROPERTY);
				
		return transferObject;
	}

	public void save(IProgressMonitor progressMonitor) 
	{
		
		ProgressMonitorHelper progressMonitorHelper = new ProgressMonitorHelper(progressMonitor);
		progressMonitorHelper.beginTask("Create/Update in progress", IProgressMonitor.UNKNOWN);
	
		try
		{
			if(!topPanel.validateSave())
			{
				MessageBox.post(this.getShell(),appReg.getString("CantSaveValidation.msg"),"Error" ,MessageBox.ERROR);
			}
			else
			{
				TCComponent m_smdlRev = m_smdlSOAPlainObject;
				int iNoOfCodesAttachedToSMDLRev = 0;
				try 
				{
					TCComponent[] codesAttachedToSMDLRev = m_smdlRev.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
					iNoOfCodesAttachedToSMDLRev = codesAttachedToSMDLRev.length;
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}
				HashSet<TCComponent> codeObjects = new HashSet<TCComponent>();
				codeObjects = 	getCodeObjects();
	
				CreateInObjectHelper[] createSMDLandSMDLRevInput = null;
				Vector<CreateInObjectHelper> vecCreateInSMDLandSMDLRev = new Vector<CreateInObjectHelper> ();
	
				CreateInObjectHelper[] createCodeInput =populateCreateInObject();
				int iCodeInputLength = createCodeInput.length;
				TCComponent[] createdCodeForms = new TCComponent[iCodeInputLength];  
				
				// Kishor Ahuja : Remove the property from cache, as the TCComponent might not know that it got
				// released by workflow.
				m_smdlRev.clearCache(NOV4ComponentSMDL.SMDL_RELEASE_STATUS);
				
				TCComponent[] statusList = m_smdlRev.getTCProperty(NOV4ComponentSMDL.SMDL_RELEASE_STATUS).getReferenceValueArray();
				if (statusList == null || statusList.length == 0)
				{
					if((iCodeInputLength > 0) || (codeObjects.size() != iNoOfCodesAttachedToSMDLRev))
					{
						if(iCodeInputLength > 0)
						{
							//TCDECREL-4526 Start		
							//Populate only the modified properties inside the map and erase others
							ModifyTransferObject modifyTransferObj = new ModifyTransferObject();
							CreateInObjectHelper[] codesPropertyMap = modifyTransferObj.cloneCreateInObjectProperties(createCodeInput, CreateInObjectHelper.OPERATION_CREATE.getMode()/*1*/);								
						    modifyTransferObj.retainModifiedValues(createCodeInput, codesPropertyMap);	
							//TCDECREL-4526 End
							
						    //TCDECREL-4526: commented out
						    //CreateObjectsSOAHelper.createUpdateObjects(createCodeInput);
							CreateObjectsSOAHelper.createUpdateObjects(codesPropertyMap);		
							
							// Get all code form objects 
							for(int inx=0; inx < iCodeInputLength; inx++)
							{
								//TCDECREL-4526 Changed from createCodeInput to codesPropertyMap
								createdCodeForms[inx] = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(codesPropertyMap[inx]);
							}
							codeObjects.addAll(Arrays.asList(createdCodeForms));
						}
	
						HashSet<TCComponent> documentObjects = new HashSet<TCComponent>();
						documentObjects = getDocuments();
						
						
						//TCDECREL-4526 Getting the cached documents
						TCProperty cachedDocuments = m_smdlRev.getTCProperty(NOV4ComponentSMDLRevision.SMDLREV_DOCUMENT_RELATION);
						List<TCComponent> cachedDocObj = getCachedDocs(cachedDocuments);
						//TCDECREL-4526 End
	
						CreateInObjectHelper smdlRevUpdateObject = new CreateInObjectHelper(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE,CreateInObjectHelper.OPERATION_UPDATE);
	
						smdlRevUpdateObject.setTagArrayProps(NOV4ComponentSMDL.SMDL_CODES, codeObjects.toArray(new TCComponent[codeObjects.size()]), CreateInObjectHelper.SET_PROPERTY);
	
						
						//TCDECREL-4526 Check if documents have changed and then add
						if(hasDocumentAttributeChanged(cachedDocObj, documentObjects))
						{
							smdlRevUpdateObject.setTagArrayProps(NOV4ComponentSMDLRevision.SMDLREV_DOCUMENT_RELATION,documentObjects.toArray(new TCComponent[documentObjects.size()]) , CreateInObjectHelper.SET_PROPERTY);
						}
					
						smdlRevUpdateObject.setTargetObject(m_smdlRev);
	
						
						//TCDECREL-4526: Capture smdl & smdl rev in one common vector, commented out the below line
						//CreateObjectsSOAHelper.createUpdateObjects(smdlRevUpdateObject);
						vecCreateInSMDLandSMDLRev.add(smdlRevUpdateObject);
					}
					TCComponent smdlItem = null;
					CreateInObjectHelper createSMDLTransferObj = null;
					try 
					{
						smdlItem = ((TCComponentItemRevision)m_smdlSOAPlainObject).getItem();
					} 
					catch (TCException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(smdlItem != null)
					{
						createSMDLTransferObj= new CreateInObjectHelper(NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
						topPanel.populateCreateInObject(createSMDLTransferObj);
						createSMDLTransferObj.setTargetObject(smdlItem);
	
						vecCreateInSMDLandSMDLRev.add(createSMDLTransferObj);
					}		
					
					
					//TCDECREL-4526 commented out and added
					//createSMDLandSMDLRevInput = vecCreateInSMDLandSMDLRev.toArray( new CreateInObjectHelper[1]);
					createSMDLandSMDLRevInput = vecCreateInSMDLandSMDLRev.toArray( new CreateInObjectHelper[vecCreateInSMDLandSMDLRev.size()] );
	
					//TCDECREL-4526 Populate only the modified properties inside the map and erase others
					ModifyTransferObject modifyTransferObj = new ModifyTransferObject();
					CreateInObjectHelper[] slimPropertyMap = modifyTransferObj.cloneCreateInObjectProperties(createSMDLandSMDLRevInput, CreateInObjectHelper.OPERATION_CREATE.getMode()/*1*/);		                   
				    modifyTransferObj.retainModifiedValues(createSMDLandSMDLRevInput, slimPropertyMap);	
					//TCDECREL-4526 End
					
					CreateObjectsSOAHelper.createUpdateObjects( slimPropertyMap );
					progressMonitorHelper.done();
					CreateObjectsSOAHelper.handleErrors(slimPropertyMap);
				
				}
				else
				{
					MessageBox.post(this.getShell(),appReg.getString("SMDLRELEASEDREV.MSG"),"Error" ,MessageBox.ERROR);
				}
				setDirty(false);
	        }
		}
        catch ( Exception e1 )
        {
        	e1.printStackTrace();
			MessageBox.post("SMDL Rev is NOT updated: " + e1.getMessage(), null, MessageBox.ERROR);
        }
        finally
		{
			progressMonitorHelper.done();
			setDirty(false);
		}
		
	}	
	public boolean isDirty()
	{
	   //Mohan - To fix the JIRA ID TCDECREL-2439
	   NOVSMDLTableHelper.stopTableCellEditing(smdlTable);
	   
	   return m_theDataModel.isDirty();
	}

	public void setDirty(boolean flag){

		m_theDataModel.setDirty(flag);
	}
	
	public void createDataBinding(){
		
		NOVSMDLDataBindingHelper dataBindingHelper = new NOVSMDLDataBindingHelper();
		dataBindingHelper.bindData((NOVSMDLTableModel)smdlTable.dataModel,"SMDL_TABLE_MODEL",SMDLDataModelConstants.SMDL_DATA_MODEL_NAME);
		dataBindingHelper.bindData(smdlTable,"SMDL_TABLE",SMDLDataModelConstants.SMDL_DATA_MODEL_NAME);
	}
	
	public TCComponent getComponent()
	{
		return m_smdlSOAPlainObject;
	}
	

}
