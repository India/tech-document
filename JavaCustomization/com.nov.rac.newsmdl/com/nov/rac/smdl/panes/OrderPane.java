package com.nov.rac.smdl.panes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.common.SMDLDataModelConstants;
import com.nov.rac.smdl.panes.order.OrderIDPanel;
import com.nov.rac.smdl.panes.order.OrderPanel;
import com.nov.rac.smdl.utilities.ModifyTransferObject;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.smdl.utilities.ProgressMonitorHelper;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.soa.common.PolicyProperty;

public class OrderPane extends Composite implements PropertyChangeListener
{
 
	
	private OrderIDPanel 			m_ordIdPanelUI;
	private DataBindingModel 		m_theDataModel = null;
	private TCComponent 			m_orderSOAPlainObject = null;
	private NOVSMDLPolicy 			m_OrderPolicy = null;
	private OrderPanel 				m_OrderPanel;

	private Registry 				m_appReg;
	
	public OrderPane (Composite parent, int style)
	{
		super(parent, style);
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
		//createUI( this );
	}
	
	public void createUI() {
		
		Composite parent =this;
		m_theDataModel = new DataBindingModel();
		
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.registerDataModel(SMDLDataModelConstants.ORDER_DATA_MODEL_NAME,m_theDataModel);
		
		setLayout( new FillLayout() );		
		m_OrderPanel = new OrderPanel(parent,SWT.NONE);
		m_OrderPanel.createUI();
		m_OrderPanel.setBindingContext(SMDLDataModelConstants.ORDER_DATA_MODEL_NAME);
		m_OrderPanel.createDataBinding();
		
		m_OrderPanel.addPropertyChangeListener(this);
	}
	
	public void setEditability(boolean m_editOrder) {
		m_OrderPanel.setEditability(m_editOrder);
	}
	
	public void clearPanelValues()
	{
		m_OrderPanel.clearPanelValues();
		m_theDataModel.clear();
	}
	
	public void populateOrder(TCComponent order){
		if(order != null) {
			m_orderSOAPlainObject = getOrderProps(order);
			m_OrderPanel.populate(m_orderSOAPlainObject);
		}
		
		setDirty(false);
	}
	
	private TCComponent getOrderProps(TCComponent orderObject) {
		
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(orderObject);
		String[] dpAttributeList = getOrderAttributeList();
		NOVSMDLPolicy orderPolicy = createSMDLPolicy();
		m_orderSOAPlainObject = dmSOAHelper.getSMDLProperties(orderObject,dpAttributeList,orderPolicy);		

		return m_orderSOAPlainObject;
	}

	private String[] getOrderAttributeList() {
		String[] orderAttributeList =	{
										NOV4ComponentOrder.ORDER_NUMBER,
										NOV4ComponentOrder.ORDER_TYPE,
										NOV4ComponentOrder.CUSTOMER,
										NOV4ComponentOrder.PLANT_ID_OTHER,
										NOV4ComponentOrder.PURCHASE_ORDER_NUMBER,
										NOV4ComponentOrder.SALES_ORDER_NUMBER,
										NOV4ComponentOrder.CONTRACT_AWARD_DATE,
										NOV4ComponentOrder.CONTRACT_DELIVERY_DATE,
										NOV4ComponentOrder.SALESMAN,
										NOV4ComponentOrder.ORDER_MANAGER,
										NOV4ComponentOrder.PROJECT_MANAGER,
										NOV4ComponentOrder.IC_CONTACT_MANAGER,
										NOV4ComponentOrder.DOCUMENT_CONTROL,
										NOV4ComponentOrder.DOCUMENT_CONTROLLER,
										NOV4ComponentOrder.ORDER_MAILBOX,
										NOV4ComponentOrder.IC_MAILBOX,
										NOV4ComponentOrder.COUNTRY_DESTINATION,
										NOV4ComponentOrder.EXTERNAL_CONTACT,
										NOV4ComponentOrder.EXTERNAL_PROJECT_NUM,
										NOV4ComponentOrder.PROJECT_SITE,
										NOV4ComponentOrder.ORDER_DP_RELATION
										};

		return orderAttributeList;
	}

	public NOVSMDLPolicy createSMDLPolicy() {
		if(m_OrderPolicy == null){
			
			m_OrderPolicy = new NOVSMDLPolicy();
							
			// Add Property policy for DP object
			NOVSMDLPolicyProperty dpPolicyProps[] = new NOVSMDLPolicyProperty[4];
			dpPolicyProps[0] = new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_NUMBER,PolicyProperty.AS_ATTRIBUTE);
			dpPolicyProps[1] = new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_BASED_ON_PROD,PolicyProperty.AS_ATTRIBUTE);
			dpPolicyProps[2] = new NOVSMDLPolicyProperty(NOV4ComponentDP.DP_NAME,PolicyProperty.AS_ATTRIBUTE);
			dpPolicyProps[3] = new NOVSMDLPolicyProperty(NOV4ComponentDP.CONTRACT_DELIVERY_DATE,PolicyProperty.AS_ATTRIBUTE);
			
			m_OrderPolicy.addPropertiesPolicy(NOV4ComponentDP.DP_OBJECT_TYPE_NAME, dpPolicyProps);
			
		}
		
		return m_OrderPolicy;
	}
	
	public OrderIDPanel getOrderIDPanel() {
		return m_ordIdPanelUI;
	}
	
	public void populateCreateInObject(CreateInObjectHelper transferObject) 
	{
		m_OrderPanel.populateCreateInObject(transferObject);
	}

    public void save(IProgressMonitor progressMonitor) 
	{
        ProgressMonitorHelper progressMonitorHelper = new ProgressMonitorHelper(progressMonitor);
        progressMonitorHelper.beginTask("Create/Update in progress", IProgressMonitor.UNKNOWN);
        
        try
        {
            if (!m_OrderPanel.validateSave())
            {
                MessageBox.post(this.getShell(), m_appReg.getString("CantSaveValidation.msg"), "Error",
                        MessageBox.ERROR);
            }
            else
            {
                String invalidMandatoryInputs = "";
                invalidMandatoryInputs = m_OrderPanel.validateMandatoryFields();
                if (invalidMandatoryInputs.length() == 0)
                {
                    String invalidFieldValues = "";
                    invalidFieldValues = m_OrderPanel.validateFieldValues();
                    if (invalidFieldValues.length() == 0)
                    {
                    	//TCDECREL-4526 Start
                    	CreateInObjectHelper[] transferObjects= new CreateInObjectHelper[]{ createOperationInput()};
						CreateObjectsSOAHelper.createUpdateObjects(transferObjects);
						progressMonitorHelper.done();
						CreateObjectsSOAHelper.handleErrors(transferObjects);
						//TCDECREL-4526 End
                        
                        
                        
                        setDirty(false);
                        
                    }
                    else
                    {
                        MessageBox.post(invalidFieldValues, "Error", MessageBox.ERROR);
                        setDirty(true);
                    }
                }
                else
                {
                    MessageBox.post(m_appReg.getString("FillMandatoryFields.MSG") + "\n" + invalidMandatoryInputs,
                            "Error", MessageBox.ERROR);
                    setDirty(true);
                }
                
            }
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }
        finally
        {
            progressMonitorHelper.done();
        }

	}
	
	private CreateInObjectHelper createOperationInput() throws TCException
	{
		CreateInObjectHelper transferObject= new CreateInObjectHelper(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
			
		m_OrderPanel.populateCreateInObject(transferObject);
		transferObject.setTargetObject(m_orderSOAPlainObject);
			
		//TCDECREL-4526 Populate only the modified properties inside the map and erase others
		ModifyTransferObject modifyTransferObj = new ModifyTransferObject();
		CreateInObjectHelper slimPropertyMap = modifyTransferObj.cloneCreateInObjectProperties(transferObject, CreateInObjectHelper.OPERATION_CREATE.getMode()/*1*/);	
		modifyTransferObj.retainModifiedValues(transferObject, slimPropertyMap);	
		
		return slimPropertyMap;
		 //TCDECREL-4526 commented out
		//return transferObject;
	}
	
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals("CreateDPOperation") )
		{
			TCComponent theCreatedObject = (TCComponent) event.getNewValue();
			
			m_OrderPanel.updateDPTable(theCreatedObject);
			
		}
		
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		m_OrderPanel.addPropertyChangeListener(listener);		
	}
	
	public boolean isDirty(){
		
		return m_theDataModel.isDirty();
	}
	
	public void setDirty(boolean flag){
		
		m_theDataModel.setDirty(flag);
	}

}