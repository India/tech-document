package com.nov.rac.smdl.panes.search;

import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.noi.util.components.JIBCustomerLOVList;
import com.noi.util.components.JIBPlantLOVList;
import com.noi.util.components.StandardProductList;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class SMDLSearchInputDataProvider implements IRunnableWithProgress
{
	private String[] m_customerNames = null;
	private String[] m_rigNames = null;
	private String[] m_smdlTypes = null;
	private String[] m_stdProdModelData = null;
	private JIBCustomerLOVList m_jibCustomerLOVList = null;
	private JIBPlantLOVList m_jibPlantLOVList = null;

	private static SMDLSearchInputDataProvider m_smdlSearchInputDataProvider;
	
 	protected Registry appReg = null;
	
 	public static synchronized SMDLSearchInputDataProvider getInstance()
 	{
 		if( m_smdlSearchInputDataProvider == null )
 		{
 			m_smdlSearchInputDataProvider = new SMDLSearchInputDataProvider();
 		}
 		
 		return m_smdlSearchInputDataProvider;
 	}
 	
	private SMDLSearchInputDataProvider()
	{
		super();
		appReg = Registry.getRegistry( this );
	}
	
	@Override
	public void run( IProgressMonitor iprogressmonitor ) throws InvocationTargetException, InterruptedException
	{
		iprogressmonitor.beginTask( appReg.getString( "AccessingWebData.MSG" ),IProgressMonitor.UNKNOWN );
		
    	try
    	{
    		createCustomerNames();
    		createRigNames();
    		createStandardProductData();
    		createSMDLTypes();
    		
			iprogressmonitor.done();
		}
    	catch( Exception e ) 
    	{			
			e.printStackTrace();
		}
	}

	
	public String[] getCustomerNames()
	{
		return m_customerNames;
	}
	
	public void createCustomerNames()
	{
		if( m_customerNames == null )
		{
			// Thread thread = new Thread()
			// {
			// public void run()
			// {
			m_jibCustomerLOVList = new JIBCustomerLOVList();			
			m_jibCustomerLOVList.init();
			Vector<Object> customerVec = m_jibCustomerLOVList.getOrderedVector();
			m_customerNames = new String[customerVec.size()];
			for (int inx = 0; inx < customerVec.size(); inx++) {
				m_customerNames[inx] = customerVec.elementAt(inx).toString();
			}
		}
		// };
		// thread.start();
		// }
	}
	
	public String[] getRigNames()
	{
		return m_rigNames;
	}
	
	public String[] getRigNames( String strCustomerName) 
	{
		// Initialize the return list with all available RigNames, 
		// so that if customer name is empty/null then we can return all RigNames.
		String[] cplants = m_rigNames;
		
		// Get rigNames only when we have a customer name.
		if(strCustomerName != null && !strCustomerName.isEmpty())
		{
			cplants = m_jibPlantLOVList.getList(m_jibCustomerLOVList.getReferenceFromString(strCustomerName).intValue());	
		}
		
		return cplants;		
	}

	public void createRigNames() 
	{
		if (m_rigNames == null) 
		{
			// Thread thread = new Thread()
			// {
			// public void run()
			// {
			m_jibPlantLOVList = new JIBPlantLOVList();
			m_jibPlantLOVList.init();
			Vector<Object> rigsVec = m_jibPlantLOVList.getOrderedVector();			
			m_rigNames = new String[rigsVec.size()];
			for (int inx = 0; inx < rigsVec.size(); inx++) {
				m_rigNames[inx] = rigsVec.elementAt(inx).toString();
			}
		}
		// };
		// thread.start();
		// }
	}
	
	public String[] getStandardProductData()
	{
		return m_stdProdModelData;
	}
	
	public void createStandardProductData()
	{
		if( m_stdProdModelData == null )
		{
//			Thread thread = new Thread()
//			{
//				public void run()
//				{
					StandardProductList stdProductList = new StandardProductList();
					Vector<Object> stdProdVec = stdProductList.getVector();
					m_stdProdModelData = new String[ stdProdVec.size() ];
					for (int inx = 0; inx< stdProdVec.size();inx++)
					{
						m_stdProdModelData[inx] = stdProdVec.elementAt(inx).toString(); 
					}
				}
//			};
//			thread.start();
//		}
	}

	public void createSMDLTypes()
	{
		if( m_smdlTypes == null )
		{
	        try
	        {
	        	TCSession session =( TCSession ) AIFUtility.getDefaultSession();
	            TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName( session, "SMDL_Type" );
	            if( lovValues != null )
	            {
					ListOfValuesInfo lovInfo = lovValues.getListOfValues();
					Object[] lovS = lovInfo.getListOfValues();
		            m_smdlTypes = new String[lovS.length];
					if( lovS.length > 0 )
					{
						for( int index = 0; index < lovS.length; ++index )
						{
							m_smdlTypes[index] = ( String )lovS[index];
						}
					}
				}
	        }
	        catch ( TCException e )
	        {
	            e.printStackTrace();
	        }
		}
	}
	
	public String[] getSMDLTypes()
	{
		return m_smdlTypes;	
	}
}