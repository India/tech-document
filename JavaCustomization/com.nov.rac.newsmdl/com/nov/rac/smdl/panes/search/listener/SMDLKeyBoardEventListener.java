package com.nov.rac.smdl.panes.search.listener;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SMDLKeyBoardEventListener implements KeyListener
{
	/// Provides the Input for the execution of the search.
	private INOVSearchProvider2 m_searchProvider;
	
	private NovSearchDataModel m_novSearchDataModel;
	protected int m_loadIndex = 0;
	
	public SMDLKeyBoardEventListener( INOVSearchProvider2 searchProvider, NovSearchDataModel novSearchDataModel )
	{
		super();

		m_searchProvider = searchProvider;
		m_novSearchDataModel = novSearchDataModel; 
		
	}
	
	public void keyPressed( KeyEvent keyevent )
	{
		if( keyevent.character == '\r' )
		{
			execute();
		}
	}

	public void keyReleased( KeyEvent keyevent )
	{
	}
	
	///TODO : NITIN This is duplication of the function. Need to refactor the code.
	private void execute()
	{
		m_novSearchDataModel.setResponseIndex( 0 );
		
		if( m_novSearchDataModel.getResultSet() != null )
			m_novSearchDataModel.getResultSet().clear();
		
		INOVSearchResult searchResult = null;
		try
		{
			if( m_searchProvider != null )
			{
				if( m_searchProvider.validateInput() == false )
				{
                    return;
				}
				
				searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, m_loadIndex);
			}
		}
		catch( Exception exception ) 
		{
			exception.printStackTrace();
		}
		if( searchResult == null )
			return;
		
		INOVResultSet theResultSet = searchResult.getResultSet();
		if( theResultSet.getRows() <= 0 )
		{
			m_novSearchDataModel.setTotalResultCount( 0 );	
		}
		else
		{
			m_novSearchDataModel.setTotalResultCount( searchResult.getResponse().nActualRowCount );
		}
	
		m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_LOAD );
		m_novSearchDataModel.setResultSet( theResultSet );
	}
}

