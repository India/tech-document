package com.nov.rac.smdl.panes.search.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;

public class NextPageButtonListener implements SelectionListener 
{
	/// Provides the Input for the execution of the search.
	private INOVSearchProvider m_searchProvider;
	
	private NovSearchDataModel m_novSearchDataModel;
	
	public NextPageButtonListener( INOVSearchProvider searchProvider, NovSearchDataModel novSearchDataModel )
	{
		super();

		m_searchProvider = searchProvider;
		m_novSearchDataModel = novSearchDataModel; 
	}
	
	public void widgetDefaultSelected( SelectionEvent selectionevent )
	{
	}

	public void widgetSelected( SelectionEvent selectionevent )
	{
		execute();
	}
	
	private void execute()
	{
		int iOldCurrentIndex = m_novSearchDataModel.getResponseIndex();
		
		if( ( iOldCurrentIndex + 1 ) >= m_novSearchDataModel.getResultSet().getRows() )
		{
			INOVSearchResult searchResult = null;
			try
			{
				if( m_searchProvider != null )
				{
					searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, iOldCurrentIndex );
				}
			}
			catch( Exception exception ) 
			{
				exception.printStackTrace();
			}
			INOVResultSet theResultSet = searchResult.getResultSet();

			m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_NEXT );
			m_novSearchDataModel.addResultSet( theResultSet );
		}
		else
		{
			m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_NEXT );
			INOVResultSet searchResult = m_novSearchDataModel.getResultSet();
			m_novSearchDataModel.setResultSet( searchResult );
		}
	}
}
