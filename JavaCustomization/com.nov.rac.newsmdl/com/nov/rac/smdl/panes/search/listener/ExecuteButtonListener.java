package com.nov.rac.smdl.panes.search.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ExecuteButtonListener implements SelectionListener
{
	/// Provides the Input for the execution of the search.
	private INOVSearchProvider2 m_searchProvider;
	
	private NovSearchDataModel m_novSearchDataModel;

	
	public ExecuteButtonListener( INOVSearchProvider2 searchProvider, NovSearchDataModel novSearchDataModel)
	{
		super();

		m_searchProvider = searchProvider;
		m_novSearchDataModel = novSearchDataModel;
	}

	public void widgetDefaultSelected( SelectionEvent selectionEvent )
	{
	}

	public void widgetSelected( SelectionEvent selectionEvent )
	{
		execute();
	}

	protected void execute()
	{
		m_novSearchDataModel.setResponseIndex( 0 );
		
		if( m_novSearchDataModel.getResultSet() != null )
			m_novSearchDataModel.getResultSet().clear();
		
		INOVSearchResult searchResult = null;
		try
		{
			if( m_searchProvider != null )
			{
				if( m_searchProvider.validateInput() == false )
				{
                    return;
				}
				searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, 0);
			}
		}
		catch( Exception exception ) 
		{
			exception.printStackTrace();
		}
		if( searchResult == null )
			return;
		
		INOVResultSet theResultSet = searchResult.getResultSet();

		if( theResultSet.getRows() <= 0 )
		{
			m_novSearchDataModel.setTotalResultCount( 0 );	
		}
		else
		{
			m_novSearchDataModel.setTotalResultCount( searchResult.getResponse().nActualRowCount );
		}
		
		m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_LOAD );
		//m_novSearchDataModel.setTotalResultCount( searchResult.getResponse().nActualRowCount );		
		m_novSearchDataModel.setResultSet( theResultSet );
	}
	
	protected NovSearchDataModel getNovSearchDataModel() 
	{	
		return m_novSearchDataModel;
	}
}