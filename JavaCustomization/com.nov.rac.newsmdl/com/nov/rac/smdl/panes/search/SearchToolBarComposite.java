package com.nov.rac.smdl.panes.search;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.smdl.panes.search.listener.ClearButtonListener;
import com.nov.rac.smdl.panes.search.listener.ExecuteButtonListener;
import com.nov.rac.smdl.panes.search.listener.ExportButtonListener;
import com.nov.rac.smdl.panes.search.listener.LoadAllButtonListener;
import com.nov.rac.smdl.panes.search.listener.NextPageButtonListener;
import com.nov.rac.smdl.panes.search.listener.PreviousPageButtonListener;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.util.Registry;

public class SearchToolBarComposite extends Composite
{
	private Composite m_mainComposite;		
	
	///Search ToolBar Components
	private ToolBar m_toolBar; 
	private ToolItem m_execute;
	private ToolItem m_clear;
	private ToolItem m_prevPage;
	private ToolItem m_nextPage;
	private ToolItem m_loadAll;
	private ToolItem m_export;

	/// Provides Input for executing the search.
	private INOVSearchProvider2 m_smdlSearchProvider;

	private NovSearchDataModel m_novSearchDataModel;
	
	protected Registry   appReg = null;	
	
	public SearchToolBarComposite( Composite parent, int style )
	{
		super( parent, style );
		appReg = Registry.getRegistry( this );
		
		createUI( parent, style );
	}

	///Creates the UI for the Search Input Data.
	private void createUI( Composite parent, int style  )
	{
        setLayout( new FillLayout() );
        m_mainComposite = new Composite( this, style );
		m_mainComposite.setLayout( new FillLayout( SWT.NONE ) );
		
        createSearchToolBar( m_mainComposite, style );
	}
	
	///Creates the Search ToolBar Composite and its components.
	private void createSearchToolBar( Composite parent, int style )
	{
		m_toolBar = new ToolBar( parent, SWT.NONE );
		
		m_execute = new ToolItem( m_toolBar, SWT.PUSH );
		m_execute.setImage( appReg.getImage( "ExecuteSearch.Image" ) );
		m_execute.setToolTipText( appReg.getString(	"ExecuteSearch.ToolTip ") );

		m_clear = new ToolItem( m_toolBar, SWT.PUSH );
		m_clear.setImage( appReg.getImage( "ClearSearchInput.Image" ) );
		m_clear.setToolTipText( appReg.getString( "ClearFields.ToolTip" ) );

		m_prevPage = new ToolItem( m_toolBar, SWT.PUSH );
		m_prevPage.setImage( appReg.getImage( "PrevSearchPage.Image" ) );
		m_prevPage.setToolTipText( appReg.getString( "PrevPage.ToolTip" ) );
		m_prevPage.setEnabled( false );

		m_nextPage = new ToolItem(m_toolBar, SWT.FLAT);
		m_nextPage.setImage( appReg.getImage( "NextSearchPage.Image" ) );
		m_nextPage.setToolTipText( appReg.getString( "NextPage.ToolTip") );
		m_nextPage.setEnabled( false );

		m_loadAll = new ToolItem( m_toolBar, SWT.PUSH );
		m_loadAll.setImage( appReg.getImage( "LoadAllResults.Image" ) );
		m_loadAll.setToolTipText( appReg.getString(	"LoadAll.ToolTip" ) );
		m_loadAll.setEnabled( false );

		m_export = new ToolItem( m_toolBar, SWT.FLAT );
		Image imgExport = TCTypeRenderer.getTypeImage( "MSExcel", null );
		m_export.setImage( imgExport );
		m_export.setToolTipText( appReg.getString( "Export.ToolTip" ) );
		m_export.setEnabled( false );
		
		m_toolBar.pack();
	}


	private void addListeners()
	{
		ExecuteButtonListener exeBtnListener = new ExecuteButtonListener( 
				m_smdlSearchProvider, m_novSearchDataModel );
		m_execute.addSelectionListener( exeBtnListener );

		ClearButtonListener clrBtnListener = new ClearButtonListener( m_novSearchDataModel );
		m_clear.addSelectionListener( clrBtnListener );

		LoadAllButtonListener loadAllBtnListener = new LoadAllButtonListener( 
				m_smdlSearchProvider, m_novSearchDataModel );
		m_loadAll.addSelectionListener( loadAllBtnListener );

		NextPageButtonListener nextPageBtnListener = new NextPageButtonListener( 
				m_smdlSearchProvider, m_novSearchDataModel );
		m_nextPage.addSelectionListener( nextPageBtnListener );

		PreviousPageButtonListener prevPageBtnListener = new PreviousPageButtonListener(  m_novSearchDataModel );
		m_prevPage.addSelectionListener( prevPageBtnListener );
		
		ExportButtonListener exportPageBtnListener = new ExportButtonListener( m_novSearchDataModel );
		m_export.addSelectionListener( exportPageBtnListener );
	}
	
	public void setDataForExecution( INOVSearchProvider2 smdlSearchProvider, NovSearchDataModel novSearchDataModel ) 
	{
		m_smdlSearchProvider = smdlSearchProvider;
		m_novSearchDataModel = novSearchDataModel;
		addListeners();
	}
	
	public void updateControlState()
	{
		int respIndex = m_novSearchDataModel.getResponseIndex();
		int pageSize = m_novSearchDataModel.getPageSize();
		
		if( respIndex > 0 )
		{
			m_export.setEnabled( true );
		}
		else
		{
			m_export.setEnabled( false );
		}
		
		if( respIndex >= pageSize )
		{
			m_nextPage.setEnabled( true );
			m_loadAll.setEnabled( true );
		}
		else
		{
			m_nextPage.setEnabled( false );
			m_loadAll.setEnabled( false );
			m_prevPage.setEnabled( false );
			//m_export.setEnabled( false );
		}
		
		if( respIndex > pageSize )
			m_prevPage.setEnabled( true );
		else
			m_prevPage.setEnabled( false );
		
		if( respIndex % pageSize > 0  )
			m_nextPage.setEnabled( false );

		/// Nitin :TODO Did this as a workaround.
		if( m_novSearchDataModel.getReasonForChange() == NovSearchDataModel.DataModelChangeReason.NOV_PREV )
			m_nextPage.setEnabled( true );
	}
}
