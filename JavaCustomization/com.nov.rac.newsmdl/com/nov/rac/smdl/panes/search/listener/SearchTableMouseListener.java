package com.nov.rac.smdl.panes.search.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.nov.quicksearch.table.NOVSearchTable;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
//import com.teamcenter.rac.aif.ApplicationDef;//TC10.1 Upgrade
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.services.IOpenService;

public class SearchTableMouseListener extends MouseAdapter 
{
	NOVSearchTable m_smdlSearchTable;
	
	public SearchTableMouseListener()
	{
	}
	public void mousePressed(MouseEvent event)
	{
		m_smdlSearchTable = (NOVSearchTable) event.getComponent();
		int clickCnt = event.getClickCount();
		if( clickCnt == 2 )
		{
			TCComponent theComponent = getLowLevelItem();

			/// Nitin :  This is not proper solution..Need to remove once we get the proper solution.
			/*ApplicationDef appDef = null;
			appDef = AIFUtility.getAIFApplicationDefMgr().getApplicationDefByPerspectiveId( "com.nov.rac.smdl.perspectives.SMDLPerspective" );
			if( appDef.getPluginName() != null && !appDef.getPluginName().equals("") )
				appDef.openApplication( new InterfaceAIFComponent[]{theComponent} );*///TC10.1 Upgrade
			
			//NOVOpenObjectUtils.invokeObjectApplication( session, theComponent );
			
			IOpenService openPerspective = PerspectiveDefHelper.getOpenService("com.nov.rac.smdl.perspectives.SMDLPerspective");//TC10.1 Upgrade
            openPerspective.open(theComponent);//TC10.1 Upgrade
		}
	}
	
	private TCComponent getLowLevelItem()
	{
		TCComponent theComponent = null;
		int iSelectdRow  = m_smdlSearchTable.getSelectedRow();
		TCSession session = (TCSession) AIFUtility.getDefaultSession();

		int nPUIDCol = m_smdlSearchTable.getColumnIndexFromDataModel( "puid" );
		String sPUID = m_smdlSearchTable.getValueAtDataModel( iSelectdRow, nPUIDCol );	
		
		int nDPIDCol = m_smdlSearchTable.getColumnIndexFromDataModel( "dp_id" );
		String sDPID = m_smdlSearchTable.getValueAtDataModel( iSelectdRow, nDPIDCol );	

		TCComponentItemType itemType = null;
		
		try
		{
			if( sDPID != null && !sDPID.trim().isEmpty() )
			{
				itemType = ( TCComponentItemType )session.getTypeService().getTypeComponent(
						NOV4ComponentDP.DP_OBJECT_TYPE_NAME );
				theComponent = itemType.find( sDPID ); 
			}
			
			int nSMDLIDCol = m_smdlSearchTable.getColumnIndexFromDataModel( "smdl_id" );
			String sSMDLID = m_smdlSearchTable.getValueAtDataModel( iSelectdRow, nSMDLIDCol );	
			if( sSMDLID != null && !sSMDLID.trim().isEmpty() )
			{
				itemType = ( TCComponentItemType )session.getTypeService().getTypeComponent(
						NOV4ComponentSMDL.SMDL_OBJECT_TYPE_NAME );
				
				theComponent = itemType.find( sSMDLID ); 
			}
			
			if( theComponent == null )
				theComponent = session.stringToComponent( sPUID );
		}
		catch( TCException exp )
		{
			exp.printStackTrace();
		}
		
		return theComponent;
	}
}