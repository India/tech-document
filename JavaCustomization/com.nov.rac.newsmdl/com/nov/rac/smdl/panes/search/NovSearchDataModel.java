package com.nov.rac.smdl.panes.search;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.patterns.observer.ISubject;

public class NovSearchDataModel implements ISubject
{
	private Vector<IObserver> m_observers = new Vector<IObserver>();	 
	private INOVResultSet m_resultSet;
	private int m_responseIndex = 0;
	private int m_totalResultCount = 0;
	private int m_pageSize = 30;

	/// Reason for the change in the NovSearchDataModel. This will ensure the updating of the Observer as per the change happened.   
	public enum DataModelChangeReason
	{
		NOV_NONE,
		NOV_LOAD,
		NOV_CLEAR,
		NOV_LOAD_ALL,
		NOV_NEXT,
		NOV_PREV,
		NOV_EXPORT
	};
	DataModelChangeReason m_reasonForChange = DataModelChangeReason.NOV_NONE;
	
	public enum InputDataState
	{
		NONE,
		CLEAR,
		EXPORT
	};
	InputDataState m_inputDataState = InputDataState.NONE;

	public NovSearchDataModel()
	{
	}

	public void setReasonForChange( DataModelChangeReason reason )
	{
		m_reasonForChange = reason;
	}

	public DataModelChangeReason getReasonForChange()
	{
		return m_reasonForChange;
	}

	public void addResultSet( INOVResultSet resultSet )
	{
		/// Set the result set and update the observers if there is any change in the INOVResultSet.
		// Nitin:Removed to satisfy NextPage and PrevPage.
		//if( m_resultSet != resultSet )
		{
			String[] rowData = resultSet.getRowData().toArray( new String[resultSet.getRowData().size()] );
			m_resultSet.add( rowData, resultSet.getRows() );
			notifyObserver();
		}
	}
	
	public void setResultSet( INOVResultSet resultSet )
	{
		/// Set the result set and update the observers if there is any change in the INOVResultSet.
		// Nitin:Removed to satisfy NextPage and PrevPage.
		//if( m_resultSet != resultSet )
		{
			m_resultSet = resultSet;
			notifyObserver();
		}
	}

	public void setInputDataState( InputDataState inputDataSet )
	{
		if( m_inputDataState != inputDataSet )
		{
			m_inputDataState = inputDataSet;

			if( inputDataSet != InputDataState.NONE )
				notifyObserver();
		}
	}

	public INOVResultSet getResultSet()
	{
		return m_resultSet;
	}

	public void setResponseIndex( int respIndex )
	{
		m_responseIndex = respIndex;
	}
	
	public int getResponseIndex()
	{
		return m_responseIndex;
	}

	public void setTotalResultCount( int totalCount )
	{
		m_totalResultCount = totalCount;
	}
	
	public int getTotalResultCount()
	{
		return m_totalResultCount;
	}
	
	public void setPageSize( int pageSize )
	{
		m_pageSize = pageSize;
	}
	
	public int getPageSize()
	{
		return m_pageSize;
	}		
	
	public void notifyObserver()
	{
		for( int index = m_observers.size() - 1; index >= 0; --index )
		{
			m_observers.get( index ).update();
		}
	}

	public void registerObserver( IObserver observer )
	{
		if( observer != null )
		{
			m_observers.add( observer );
		}
	}

	public void removeObserver( IObserver observer )
	{
		if( observer != null )
		{
			m_observers.remove( observer );
		}
	}
}