package com.nov.rac.smdl.panes.search.listener;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;

public class SMDLKeyBoardEventLoadAllListener extends SMDLKeyBoardEventListener {

	public SMDLKeyBoardEventLoadAllListener(INOVSearchProvider2 searchProvider,	NovSearchDataModel novSearchDataModel)
	{
		super(searchProvider, novSearchDataModel);
		m_loadIndex= -2;
	}

}
