package com.nov.rac.smdl.panes.search.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

import com.nov.quicksearch.table.NOVSearchTable;

public class SMDLProductModelRenderer extends DefaultTableCellRenderer
{
	private static final long serialVersionUID = 1L;
	
    private Color m_alternateBackground;
    
	public SMDLProductModelRenderer()
	{
		super();
		
		m_alternateBackground = new Color( 191,214,248 );
	}	
	
	public Component getTableCellRendererComponent( JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column )
	{
		if( value != null )
		{
			NOVSearchTable searchTable = ( NOVSearchTable )table;
			int columnIndex = searchTable.getColumnIndexFromDataModel( "nov4_basedon_std_prod" );
			String productModelData = ( String )searchTable.getValueAtDataModel( row, columnIndex ); 

			String[] productModel = productModelData.split( ":" );
			String tempProdModel;
			if( productModel.length > 1 )
			{
				tempProdModel = productModel[0];
				setValue( productModel[0] );
			}
			else
			{
				tempProdModel = "";
				setValue( "" );
			}
			table.setValueAt( tempProdModel, row, column );
		}
        setbackground( table, isSelected, hasFocus, row, column );
		return this;
	}
	
	protected void setbackground( JTable jtable, boolean flag, boolean flag1,
		int i, int j )
	{
		if(flag)
		{
		    super.setForeground(jtable.getSelectionForeground());
		    super.setBackground(jtable.getSelectionBackground());
		}
		else
		{
		    super.setForeground( jtable.getForeground());
		    super.setBackground( i % 2 != 1 ? jtable.getBackground() : m_alternateBackground );
		}
		if(flag1)
		{
		    setBorder(UIManager.getBorder( "Table.focusCellHighlightBorder" ) );
		    if( jtable.isCellEditable( i, j ) )
		    {
		        super.setForeground(UIManager.getColor("Table.focusCellForeground"));
		        super.setBackground(UIManager.getColor("Table.focusCellBackground"));
		    }
		}
		else
		{
		    setBorder(noFocusBorder);
		}
	}
	
	protected void setValue( Object obj )
    {
        setText( obj != null ? obj.toString() : "" );
    }
	
}
