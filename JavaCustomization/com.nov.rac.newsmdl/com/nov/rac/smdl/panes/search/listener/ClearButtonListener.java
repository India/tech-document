package com.nov.rac.smdl.panes.search.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.InputDataState;

public class ClearButtonListener implements SelectionListener
{
	private NovSearchDataModel m_novSearchDataModel;
	
	public ClearButtonListener( NovSearchDataModel novSearchDataModel )
	{
		super();

		m_novSearchDataModel = novSearchDataModel; 
	}

	public void widgetDefaultSelected( SelectionEvent selectionEvent )
	{
	}

	public void widgetSelected( SelectionEvent selectionEvent )
	{
		execute();
	}

	private void execute()
	{
		m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_CLEAR );
		m_novSearchDataModel.setInputDataState( InputDataState.CLEAR );
	}
}
