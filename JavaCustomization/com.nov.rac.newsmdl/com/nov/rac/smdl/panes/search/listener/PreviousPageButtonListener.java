package com.nov.rac.smdl.panes.search.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;

public class PreviousPageButtonListener implements SelectionListener
{
	private NovSearchDataModel m_novSearchDataModel;
	
	public PreviousPageButtonListener( NovSearchDataModel novSearchDataModel )
	{
		super();

		m_novSearchDataModel = novSearchDataModel; 
	}
	
	public void widgetDefaultSelected( SelectionEvent selectionevent )
	{
	}

	public void widgetSelected( SelectionEvent selectionevent )
	{
		execute();
	}

	private void execute()
	{
		m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_PREV );
		INOVResultSet searchResult = m_novSearchDataModel.getResultSet();
		m_novSearchDataModel.setResultSet( searchResult );
	}
}
