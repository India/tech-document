package com.nov.rac.smdl.panes.search.listener;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.quicksearch.services.utils.NOVSearchErrorCodes;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LoadAllButtonListener implements SelectionListener
{
	/// Provides the Input for the execution of the search.
	private INOVSearchProvider2 m_searchProvider;
	
	private NovSearchDataModel m_novSearchDataModel;
	
	public LoadAllButtonListener( INOVSearchProvider2 searchProvider, NovSearchDataModel novSearchDataModel )
	{
		super();

		m_searchProvider = searchProvider;
		m_novSearchDataModel = novSearchDataModel; 
	}
	
	public void widgetDefaultSelected( SelectionEvent selectionevent )
	{
	}

	public void widgetSelected( SelectionEvent selectionevent )
	{
		execute();
	}
	
	protected void execute()
	{
		m_novSearchDataModel.setResponseIndex( 0 );
		INOVResultSet resultSet = m_novSearchDataModel.getResultSet();
		if(null!=resultSet)
		{
			resultSet.clear();
		}
		
		INOVSearchResult searchResult = null;
		try
		{
			if( m_searchProvider != null )
			{
				if( m_searchProvider.validateInput() == false )
				{
                    return;
				}
				searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, INOVSearchProvider.INIT_LOAD_ALL );
			}
		}
		catch( TCException tcExp )
		{
			if( tcExp.getErrorCode() == NOVSearchErrorCodes.TOO_MANY_OBJECTS_FOUND.getErrorCode() )
			{
				String str = tcExp.getMessage();
				Registry theReg = Registry.getRegistry( this );
				String strErrorText = theReg.getString( "TOO_MANY_OBJECTS_FOUND.ERROR" );
				
				str = str + ", " + strErrorText; 
				/*if( m_searchProvider instanceof SearchInputDataComposite )
				{
					SearchInputDataComposite inputComposite = ( SearchInputDataComposite )m_searchProvider;
				*/
					
					boolean bResult = MessageDialog.openConfirm( AIFUtility.getActiveDesktop().getShell(),
							theReg.getString( "MESSAGE.TITLE" ), str );	
					if(bResult)
					{
					  try 
					  {
							searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, INOVSearchProvider.LOAD_ALL );
					  } 
					  catch (Exception e1) 
					  {
					     e1.printStackTrace();
					  }	
					}				
				/*}*/
			}
		}
		catch( Exception e ) 
		{
			e.printStackTrace();
		}
		
		if( searchResult != null )
		{
			INOVResultSet theResultSet = searchResult.getResultSet();
			m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_LOAD_ALL );
			m_novSearchDataModel.setResultSet( theResultSet );
		}
	}
}