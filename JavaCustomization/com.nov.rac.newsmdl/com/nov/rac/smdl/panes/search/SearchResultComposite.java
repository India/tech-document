package com.nov.rac.smdl.panes.search;

import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.util.Arrays;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import com.nov.quicksearch.export.NOVExportSearchResultUtils;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.table.NOVNameRendererEx;
import com.nov.quicksearch.table.NOVSearchTable;
import com.nov.quicksearch.table.searchResultData;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.InputDataState;
import com.nov.rac.smdl.panes.search.renderer.SMDLProductModelRenderer;
import com.nov.rac.smdl.panes.search.renderer.SMDLProductNameRenderer;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class SearchResultComposite extends Composite implements IObserver
{
 	private NOVSearchTable m_smdlSearchTable;
 	private NovSearchDataModel m_novSearchDataModel;
 	private int m_totalRows;
 	
 	protected Registry appReg = null;
 	
	public SearchResultComposite( Composite parent, int style /*, NovSearchDataModel novSearchDataModel*/ )
	{
		super( parent, style );
		appReg = Registry.getRegistry( this );
		
		setLayout( new FillLayout() );
		createUI( this, style );
	}
	
	private void createUI( Composite parent, int style )
	{
		TabFolder tabFolder = new TabFolder( parent, style );
		TabItem tabItem = new TabItem( tabFolder, style );
		tabItem.setText( appReg.getString( "SearchResult.Label", "Search Result" ) );
		
		Composite comp = new Composite( tabFolder, SWT.NONE );
		comp.setLayout( new GridLayout( 1, false ) );
		comp.setLayoutData( new GridData( SWT.FILL, SWT.FILL, true, true, 1, 10 ) );
	    createSearchTable( comp, style );
		tabItem.setControl( comp );
	}

	private void createSearchTable( Composite parent, int style )
	{
		Composite tablePanel = new Composite( parent, SWT.EMBEDDED );
		
		tablePanel.setLayoutData( new GridData( SWT.FILL, SWT.FILL, true, true, 1, 10 ) );
		
		
		TCSession session = ( TCSession )AIFUtility.getDefaultSession();		

		String strCol[][] = NOVSearchTable.getPropertyTableDisplayKey( NOVSMDLConstants.PREF_SMDL_SEARCH );
		m_smdlSearchTable = new NOVSearchTable( session, strCol[0] );

		m_smdlSearchTable.setColumnNames( strCol[0], NOVSMDLConstants.PREF_SMDL_SEARCH );
		//Deepthi -> Jira 3.16; Reordering column as per user preference
		String[] strColNames=null;	
		strColNames= getSMDLTableColumns();
        if(strColNames != null)
        {
        	reOrderColumns(strColNames);
        }
		
		/// Setting the column widts from the preference.
		TCPreferenceService prefServ = ( ( TCSession ) AIFUtility.getDefaultSession() ).getPreferenceService();
		String colWidthPrefName= "NOV4_"+ NOVSMDLConstants.PREF_SMDL_SEARCH + "_ColumnWidthPreferences";
		String[] columnWidths = prefServ.getStringArray( TCPreferenceService.TC_preference_all, colWidthPrefName );

		//Deepthi -> Jira 3.20; added the check to prevent the Search view crash if there is a mismatch in site-user preference for column width
/*		if(strCol[1].length != columnWidths.length)
		{
			setNewColumnWidths();
			columnWidths = prefServ.getStringArray( TCPreferenceService.TC_preference_user, colWidthPrefName );
		}*/		
		//setColumnWidths( columnWidths );
		
		m_smdlSearchTable.hideColumns( NOVSMDLConstants.PREF_SMDL_SEARCH );
		setColumnWidths( columnWidths );
		ScrollPagePane tablePane = new ScrollPagePane( m_smdlSearchTable );

		SoftBevelBorder softBorder = new SoftBevelBorder( SoftBevelBorder.RAISED );
        java.awt.Color highlightInnerColor = softBorder.getHighlightInnerColor();
        java.awt.Color shadowOuterColor = softBorder.getShadowOuterColor();
        java.awt.Color shadowInnerColor = softBorder.getShadowInnerColor();
        SoftBevelBorder borderToSetToComp = new SoftBevelBorder(
                SoftBevelBorder.RAISED, new java.awt.Color( 233 , 233 , 218 ),
                highlightInnerColor, shadowOuterColor, shadowInnerColor);
        tablePane.setBorder( borderToSetToComp );
        tablePane.getViewport().setBackground( java.awt.Color.WHITE );
        
       	SWTUIUtilities.embed( tablePanel, tablePane, false );
       	
		assignColumnRenderer();
	}
	
	//Deepthi -> Jira 3.16; Column reorder based on user preference
    public void reOrderColumns(String columnNames[])
	{
		for (int newIndex = 0; newIndex < columnNames.length; newIndex++)
		{
			try
			{				
				String columnName = columnNames[newIndex];	
				int index = m_smdlSearchTable.getColumnIndex(columnName);
				m_smdlSearchTable.moveColumn(index, newIndex);
			}
			catch(IllegalArgumentException e) 
			{
				e.printStackTrace();
			}
		}
	}

	private String[] getSMDLTableColumns() 
	{
		String[] strColNames = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();		
		String colDispPrefName= "NOV4_"+ NOVSMDLConstants.PREF_SMDL_SEARCH + "_ColumnPreferences";
		strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_user, colDispPrefName);
		return strColNames;
	}
	
	public void setNovSearchDataModel( NovSearchDataModel novSearchDataModel )
	{
		m_novSearchDataModel = novSearchDataModel;
		
		/// Register the View(Observer) to the NovSearchDataModel(Subject).
		m_novSearchDataModel.registerObserver( this );
	}
	
	//TODO: Abstract these operation so as to remove this Switch case thing.
	public void update()
	{
		int pageSize = m_novSearchDataModel.getPageSize();
		INOVResultSet searchResult = m_novSearchDataModel.getResultSet();
		int respIndex = m_novSearchDataModel.getResponseIndex();
		
		switch( m_novSearchDataModel.getReasonForChange() )
		{
			case NOV_CLEAR:
			{
				clearTableData();
				break;
			}
			case NOV_LOAD:
			{
				if( searchResult.getRows() > 0 )
				{
					m_totalRows = addRowsToTable( searchResult, respIndex, pageSize );
					m_novSearchDataModel.setResponseIndex( m_totalRows );
				}
				else
				{
					clearTableData();
				}
				break;
			}
			case NOV_LOAD_ALL:
			{
				if( searchResult.getRows() > 0 )
				{
					m_totalRows = addRowsToTable( searchResult, respIndex, pageSize, -2 );
					m_novSearchDataModel.setResponseIndex( m_totalRows );
				}
				
				break;
			}
			case NOV_NEXT:
			{
				if( searchResult.getRows() > 0 )
				{
					m_totalRows = addRowsToTable( searchResult, respIndex, pageSize, 0 );
					m_novSearchDataModel.setResponseIndex( m_totalRows );
				}

				break;
			}
			case NOV_PREV:
			{
				int respIndexPrev = respIndex - ( 2 * pageSize );
				
				if( respIndexPrev < 0 )
					respIndexPrev = 0;
				
				if( searchResult.getRows() > 0 )
				{
					m_totalRows = addRowsToTable( searchResult, respIndexPrev, pageSize, 0 );
					m_novSearchDataModel.setResponseIndex( m_totalRows );
				}
				
				break;
			}
			case NOV_EXPORT:
			{
				setProductModelNameData();
				exportTableData();
				m_novSearchDataModel.setInputDataState( InputDataState.NONE );
				break;
			}
		}
		
		if( m_novSearchDataModel.getResultSet() != null &&
			m_novSearchDataModel.getResultSet().getRows() <= 0 &&
			m_novSearchDataModel.getReasonForChange() != DataModelChangeReason.NOV_CLEAR )
		{
			MessageDialog.openInformation(this.getShell(),
					appReg.getString( "MESSAGE.TITLE" ), appReg.getString( "NO_OBJECTS_FOUND.MSG" ) );
			return;
		}
	}
	
	//TODO: make this generic so that it should work for the all columns which has the renderer's ( For all other searches as well )  
	private void setProductModelNameData()
	{
		int columnIndex = m_smdlSearchTable.getColumnIndexFromDataModel( "nov4_basedon_std_prod" );
		// get the column number for prod_model.
		int prodModelColNum = m_smdlSearchTable.getColumnIndex( "prod_model" );
		// get the column number for theprod_name.
		int prodModelNameColNum = m_smdlSearchTable.getColumnIndex( "prod_name" );
		
		for( int rowIndex = 0; rowIndex < m_totalRows; ++rowIndex )
		{
			String productModelNameData = ( String )m_smdlSearchTable.getValueAtDataModel( rowIndex, columnIndex ); 
	
			String[] productModelNameArr = productModelNameData.split( ":" );
			m_smdlSearchTable.setValueAt( productModelNameArr[0], rowIndex, prodModelColNum );
			
			if( productModelNameArr.length > 1 )
			{
				// 0 -- ProductModel
				// 1 - ProductName
				m_smdlSearchTable.setValueAt( productModelNameArr[1], rowIndex, prodModelNameColNum );
			}
		}
	}

	private void clearTableData()
	{
		if( m_smdlSearchTable.getRowCount() > 0 )
		{
			m_smdlSearchTable.removeAllRows();
			m_novSearchDataModel.setResponseIndex( 0 );
		}
	}
	
	private void exportTableData()
	{
		NOVExportSearchResultUtils.exportSearchResultToExcel( this, m_smdlSearchTable );
	}

	public int addRowsToTable( INOVResultSet theResSet, int currIndex, int pageMaxSize )
	{
		int maxRows30 = pageMaxSize;
		int nStartIndex = 0;
		int nEndIndex = 0;

    	if(maxRows30 > (theResSet.getRows() - currIndex) )
    	{
    		maxRows30 = (theResSet.getRows() - currIndex);
    	}

		// start Index
		nStartIndex = currIndex;
		nEndIndex = currIndex + maxRows30;
		
    	//int nRowInxToVectorInx = 0;
		String [] strRowsTemp = new String[1];
    	String [] strRows = (String []) (theResSet.getRowData().toArray(strRowsTemp));
    	
    	//remove all the rows..
    	m_smdlSearchTable.removeAllRows();
    	searchResultData result = new searchResultData( strRows, nStartIndex ,nEndIndex, theResSet.getCols() );
    	m_smdlSearchTable.addRow(result);
    	
    	return nEndIndex;
	}

	public int addRowsToTable( INOVResultSet theResSet, int currIndex,int pageMaxSize, int m_loadall )
	{
		assert( theResSet != null );
		
		int maxRows30 = pageMaxSize;
		int nStartIndex = 0;
		int nEndIndex = 0;

		int nTotalCount = theResSet.getRows();
		if( m_loadall >= 0 )
		{
			if( maxRows30 > ( theResSet.getRows() - currIndex ) )
			{
				maxRows30 = ( theResSet.getRows() - currIndex );
			}
		
			// start Index
			nStartIndex = currIndex;
			nEndIndex = currIndex + maxRows30;
		}

		String [] strRowsTemp = new String[1];
		String [] strRows = ( String [] ) (theResSet.getRowData().toArray( strRowsTemp ) );
		//remove all the rows..
		m_smdlSearchTable.removeAllRows();
		
		if(m_loadall < 0)
        {
			nStartIndex=0;
			nEndIndex=0;
			maxRows30 =pageMaxSize;
			while( nEndIndex < nTotalCount ){
				
				if( maxRows30 > ( nTotalCount - currIndex ) )
				{
					maxRows30 = (nTotalCount - currIndex);
				}
			
				// start Index
				nStartIndex = currIndex;
				nEndIndex = currIndex + maxRows30;
				currIndex = nEndIndex;
			
				searchResultData result = new searchResultData( strRows, nStartIndex ,nEndIndex, theResSet.getCols() );
				
				m_smdlSearchTable.addRow(result);
			}
        }
		else
		{
			searchResultData result = new searchResultData( strRows, nStartIndex ,nEndIndex, theResSet.getCols() );
			m_smdlSearchTable.addRow( result );
		}

		return nEndIndex;
	}
	
	private void assignColumnRenderer()
	{
		AbstractTCTableCellRenderer cellRendererOrder = new NOVNameRendererEx();
		
		( ( NOVNameRendererEx )cellRendererOrder ).setObjectTypeColumnName( "order_type" );
		
		int nObject_NameOrder = m_smdlSearchTable.getColumnIndex( "order_id" );
		
		TableColumn objectNameColOrder = m_smdlSearchTable.getColumnModel().getColumn( nObject_NameOrder );
		
		objectNameColOrder.setCellRenderer( cellRendererOrder );
		
		{
			AbstractTCTableCellRenderer cellRendererDP = new NOVNameRendererEx();
			
			((NOVNameRendererEx)cellRendererDP).setObjectTypeColumnName( "dp_type" );		
			
			int nObject_NameDP = m_smdlSearchTable.getColumnIndex( "dp_id" );
			
			TableColumn objectNameColDP = m_smdlSearchTable.getColumnModel().getColumn( nObject_NameDP );
			
			objectNameColDP.setCellRenderer(cellRendererDP);
		}
		
		{
			AbstractTCTableCellRenderer cellRendererSMDL = new NOVNameRendererEx();
			
			((NOVNameRendererEx)cellRendererSMDL).setObjectTypeColumnName( "smdl_type" );			
			
			int nObject_NameSMDL = m_smdlSearchTable.getColumnIndex( "smdl_id" );
			
			TableColumn objectNameColSMDL = m_smdlSearchTable.getColumnModel().getColumn(nObject_NameSMDL);
			
			objectNameColSMDL.setCellRenderer(cellRendererSMDL);
		}
		
		SMDLProductModelRenderer prodModelCellRenderer = new SMDLProductModelRenderer();
		
		m_smdlSearchTable.setDefaultRenderer( String.class, prodModelCellRenderer );
		
		// get the column prod_model and assign the renderer to it.
		int prodModelColNum = m_smdlSearchTable.getColumnIndex( "prod_model" );
		
		TableColumn prodModelColumn = m_smdlSearchTable.getColumnModel().getColumn( prodModelColNum );
		
		prodModelColumn.setCellRenderer( prodModelCellRenderer );

		SMDLProductNameRenderer prodNameCellRenderer = new SMDLProductNameRenderer();
		
		m_smdlSearchTable.setDefaultRenderer( String.class, prodNameCellRenderer );
		
		// get the column nov4_basedon_std_prod and assign the renderer to it.
		int prodModelNameColNum = m_smdlSearchTable.getColumnIndex( "prod_name" );
		
		TableColumn prodModelNameColumn = m_smdlSearchTable.getColumnModel().getColumn( prodModelNameColNum );
		
		prodModelNameColumn.setCellRenderer( prodNameCellRenderer );
	}	
	//Mohan - Method to register mouse listener on search table
	public void addSearchTableMouselistener(MouseListener tableMouseListener)
	{
		m_smdlSearchTable.addMouseListener(tableMouseListener);
	}
	public NOVSearchTable getSearchTable()
	{
		return m_smdlSearchTable;
	}
	
	public void dispose()
	{
		m_novSearchDataModel.removeObserver( this );
		
		setNewColumnWidths();
		setNewColumnNames();
	}
	
	private void setColumnWidths( String[] columnWidths )
	{
/*		TCPreferenceService prefServ = ( ( TCSession ) AIFUtility.getDefaultSession() ).getPreferenceService();

		String colDispPrefName = "NOV4_"+ NOVSMDLConstants.PREF_SMDL_SEARCH + "_ColumnDisplayPreferences";
	    
	    String[] colDispNames = prefServ.getStringArray( TCPreferenceService.TC_preference_user,
	    		colDispPrefName ); */
	    
		int colCount = m_smdlSearchTable.getColumnCount();

		for( int index = 0; index < colCount; ++index )
		{
/*			String colName = m_smdlSearchTable.getColumnName( index );
			if( Arrays.asList( colDispNames ).contains( colName ) )
			{
				TableColumn col = m_smdlSearchTable.getColumnModel().getColumn( index );
				int columnLocation = getColumnIndex( colDispNames, colName );
				col.setPreferredWidth( Integer.parseInt( ( columnWidths[columnLocation] ) ) );
			}*/
				TableColumn col = m_smdlSearchTable.getColumnModel().getColumn( index );
				col.setPreferredWidth(Integer.parseInt(columnWidths[index]));
		}
	}
	
	private void setNewColumnWidths()
	{
		TCPreferenceService prefServ = ( ( TCSession ) AIFUtility.getDefaultSession() ).getPreferenceService();

		String colWidthPrefName = "NOV4_"+ NOVSMDLConstants.PREF_SMDL_SEARCH + "_ColumnWidthPreferences";
/*		String colDispPrefName = "NOV4_"+ NOVSMDLConstants.PREF_SMDL_SEARCH + "_ColumnDisplayPreferences";
	    
	    String[] colDispNames = prefServ.getStringArray( TCPreferenceService.TC_preference_user,
	    		colDispPrefName ); 
	    
		String strCol[][] = NOVSearchTable.getPropertyTableDisplayKey( NOVSMDLConstants.PREF_SMDL_SEARCH );
*/	    
		int colCount = m_smdlSearchTable.getColumnCount();
		String widths[] = new String[colCount];		
		for( int index = 0; index < colCount; ++index )
		{
/*			String colName = m_smdlSearchTable.getColumnName( index );
			if( Arrays.asList( colDispNames ).contains( colName ) )
			{
				TableColumn col = m_smdlSearchTable.getColumnModel().getColumn( index );
				int columnLocation = getColumnIndex( colDispNames, colName );
				strCol[1][columnLocation] = Integer.toString( col.getWidth() );
			}*/	
			TableColumn col = m_smdlSearchTable.getColumnModel().getColumn( index );
			widths[index] = Integer.toString( col.getWidth() );
		}

		try
		{
			prefServ.setStringArray( TCPreferenceService.TC_preference_user, colWidthPrefName, widths );
		}
		catch( TCException e )
		{
			e.printStackTrace();
		}
	}
	//Deepthi -> Jira 3.16 ; Save column order in user preference
	private void setNewColumnNames() {

		String colDispPrefName = "NOV4_" + NOVSMDLConstants.PREF_SMDL_SEARCH
				+ "_ColumnPreferences";
		
		TCPreferenceService prefServ = ((TCSession) AIFUtility
				.getDefaultSession()).getPreferenceService();
		
		int colCount = m_smdlSearchTable.getColumnCount();
		String as[] = m_smdlSearchTable.getColumnOtherIds();

		for (int i = 0; i < colCount; ++i) {
			String[] splitedString = as[i].toString().split("\\.");
			if (splitedString.length > 1)
				as[i] = splitedString[1];
		}

		try {

			prefServ.setStringArray(TCPreferenceService.TC_preference_user,
					colDispPrefName, as);
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private int getColumnIndex( String[] columnNames, String toFindColumn )
	{
		int location = 0;
		
		for( int index = 0; index < columnNames.length; ++index )
		{
			if( columnNames[index].equalsIgnoreCase( toFindColumn ) )
			{
				location = index;
				break;
			}
		}
		
		return location;
	}
}