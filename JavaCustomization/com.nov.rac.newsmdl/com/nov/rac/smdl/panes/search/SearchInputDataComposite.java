package com.nov.rac.smdl.panes.search;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

//import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.InputDataState;
import com.nov.rac.smdl.panes.search.listener.SMDLKeyBoardEventListener;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SearchInputDataComposite extends Composite implements INOVSearchProvider2, IObserver
{
	///Composites used for creating the SearchInputData Composite.
	private Composite m_mainComposite;		
	private Composite m_inputComposite;	

	///Search ToolBar Components.
	private SearchToolBarComposite m_compToolBar;  

	///Search Heading Components.
	private Composite m_compHead;
	private Label m_headingLabel;
	private Label m_resultInfoLabel;
	
	///Search Input Left Panel Components.
	private Composite m_leftComposite;
	private Text m_orderID;
	private Text m_dpID;
	private Text m_smdlID;
	private Text m_salesOrderNum;
	private Text m_engJobNum;   

	///Search Input Right Panel Components.
	private Composite m_rightComposite;
	private Combo m_customerNameCombo;
	private Combo m_plantNameCombo;
	private Combo m_stdProdNameCombo;
	private Combo m_stdProdModelCombo;
	private Combo m_smdlTypeCombo;

	private String m_strOrderID;
	private String m_strDPID;
	private String m_strSMDLID;
	private String m_strSalesOrderNum;
	private String m_strEngJobNum;
	private String m_strCustomerName;
	private String m_strRigName;

	private String m_strStdProdName;
	private String m_strStdProdModel;
	private String m_strSMDLType;
	
	/// Data model for the view. Based on this data model the view will present itself.
	private NovSearchDataModel m_novSearchDataModel;
	
	private String[] m_actualStdProductData;
	private String[] m_prodModelNames; 
	private String[] m_prodName;
	
	private String[] m_custNames;
	private String[] m_plantNames;
	
	int m_prevNameIndex = -1;
	int m_prevModelIndex = -1;
	
	SMDLKeyBoardEventListener m_keyBoardListener;
	
	protected Registry   m_appReg = null;
	
	final private Color m_backgroundColor = new Color( null, 220, 230, 250 );
	
	final private Color m_foregroundColor = new Color( null, 0, 0, 0 );
	
	final private int m_idTextLimit = 128;
	
	final private int m_orderNumTextLimit = 60;
	
	final private int m_engJobNumTextLimit = 32;
	
	final private int m_bindVarCount = 10;
	
	final private int m_handlerCount = 3;
	
	public SearchInputDataComposite( Composite parent, int style )
	{
		super( parent, style );
		m_appReg = Registry.getRegistry( this );
		
		createUI( parent, style );
	}

	///Creates the UI for the Search Input Data.
	private void createUI( Composite parent, int style )
	{
        setLayout( new FillLayout() );
        
        m_mainComposite = new Composite( this, SWT.CENTER );
        m_mainComposite.setLayout( new GridLayout( 1, true ) );     
        
        createSearchToolBar( m_mainComposite, style );
        createSearchHeading( m_mainComposite, style );
        
        m_inputComposite = new Composite( m_mainComposite, SWT.NONE );
        m_inputComposite.setLayoutData( new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 ) );
        m_inputComposite.setLayout( new GridLayout( 2, true ) );
        
		crateLeftInputPanelUI( m_inputComposite, style );
		crateRightInputPanelUI( m_inputComposite, style );
		
		populateInputData();
	}

	///Creates the Search ToolBar Composite and its components.
	private void createSearchToolBar( Composite parent, int style )
	{
		m_compToolBar = new SearchToolBarComposite( parent, SWT.NONE );
		m_compToolBar.setLayoutData( new GridData( SWT.LEFT, SWT.CENTER, true, false, 1, 1 ) );
	}

	/// Creates the Search Heading Composite and its components.
	private void createSearchHeading( Composite parent, int style )
	{
		m_compHead = new Composite( parent, SWT.BORDER );
		m_compHead.setLayout( new GridLayout( 2, true ) );
		GridData data = new GridData();
		data.horizontalAlignment = SWT.FILL;
		data.grabExcessHorizontalSpace = true;
		m_compHead.setLayoutData( data );
		m_compHead.setBackground( m_backgroundColor );
		
		m_headingLabel = new Label( m_compHead, SWT.CENTER | SWT.BOLD );
		m_headingLabel.setLayoutData( new GridData( SWT.LEFT, SWT.CENTER, true, false, 1, 1 ) );
		m_headingLabel.setBackground( m_backgroundColor );
		
		Font font = m_compHead.getShell().getDisplay().getSystemFont();
		m_headingLabel.setFont( font );
		m_headingLabel.setText( m_appReg.getString( "SMDLSearch.Label" ) );
		
		createSearchResultInfo( m_compHead, style );
	}

	private void createSearchResultInfo( Composite parent, int style  )
	{
		m_resultInfoLabel = new Label( parent, SWT.CENTER );
		m_resultInfoLabel.setEnabled( true );
		GridData gd_m_resultInfoLabel = new GridData( SWT.CENTER, SWT.CENTER, true, false, 1, 1 );
		gd_m_resultInfoLabel.widthHint = 300;
		m_resultInfoLabel.setLayoutData( gd_m_resultInfoLabel );
		
		m_resultInfoLabel.setText( "" );
		Font font = m_resultInfoLabel.getShell().getDisplay().getSystemFont();
		m_headingLabel.setFont( font );
		m_resultInfoLabel.setBackground( m_backgroundColor );
		m_resultInfoLabel.setForeground( m_foregroundColor );
	}
	
	///Creates the Left Input Composite and its components.
	private void crateLeftInputPanelUI( Composite parent, int style )
	{
        m_leftComposite = new Composite( parent, SWT.NONE );
        m_leftComposite.setLayoutData( new GridData( SWT.FILL, SWT.LEFT, true, false, 1, 1 ) );
	    GridLayout layout = new GridLayout( 2, false );
	    m_leftComposite.setLayout( layout );

	    Label orderLabel = new Label( m_leftComposite, SWT.LEFT );
		orderLabel.setText( m_appReg.getString( "OrderID.LABEL" ) );
		m_orderID = new Text( m_leftComposite, SWT.BORDER | SWT.LEFT );
		m_orderID.setLayoutData( new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 ) );
		m_orderID.setTextLimit( m_idTextLimit );
		
		Label dpLabel = new Label( m_leftComposite, SWT.LEFT );
		dpLabel.setText( m_appReg.getString( "DeliveredProductID.LABEL" ) );
		m_dpID = new Text( m_leftComposite, SWT.BORDER | SWT.LEFT );
		m_dpID.setLayoutData( new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 ) );
		m_dpID.setTextLimit( m_idTextLimit );

		Label smdlLabel = new Label( m_leftComposite, SWT.LEFT );
		smdlLabel.setText( m_appReg.getString( "SMDLID.LABEL" ) );
		m_smdlID = new Text( m_leftComposite, SWT.BORDER | SWT.LEFT );
		m_smdlID.setLayoutData( new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 ) );
		m_smdlID.setTextLimit( m_idTextLimit );
		
		Label salesOrderNumLabel = new Label( m_leftComposite, SWT.LEFT );
		salesOrderNumLabel.setText( m_appReg.getString( "SalesOrderNumber.LABEL" ) );
		m_salesOrderNum = new Text( m_leftComposite, SWT.BORDER | SWT.LEFT );
		m_salesOrderNum.setLayoutData( new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 ) );
		m_salesOrderNum.setTextLimit( m_orderNumTextLimit );
	    
		Label m_engJobNumLabel = new Label( m_leftComposite, SWT.LEFT );
		m_engJobNumLabel.setText( m_appReg.getString( "EngineeringJobNumber.LABEL" ) );
		m_engJobNum = new Text( m_leftComposite, SWT.BORDER | SWT.LEFT );
		m_engJobNum.setLayoutData( new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 ) );
		//Text limit
		m_engJobNum.setTextLimit( m_engJobNumTextLimit );

	}
	
	///Creates the Right Input Composite and its components.
	private void crateRightInputPanelUI( Composite parent, int style )
	{
		m_rightComposite = new Composite( parent, SWT.NONE );
		GridLayout layout = new GridLayout( 2, false );
		m_rightComposite.setLayout( layout );
		m_rightComposite.setLayoutData( new GridData( SWT.FILL, SWT.TOP, true, true, 1, 1 ) );
		
		Label custNameLabel = new Label( m_rightComposite, SWT.RIGHT );
		custNameLabel.setText( m_appReg.getString( "CustomerName.LABEL" ) );
		m_customerNameCombo = new Combo( m_rightComposite, SWT.LEFT );
		m_customerNameCombo.setLayoutData( new GridData( SWT.FILL, SWT.TOP, true, true, 1, 1 ) );
		// Deepthi - Jira issue 3.2
		m_customerNameCombo.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent arg0)
			{
				populatePlantNameData();
			}
			
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
			}
		});
		m_customerNameCombo.addFocusListener(new FocusListener()
		{
			public void focusGained(FocusEvent arg0)
			{
			}
			public void focusLost(FocusEvent arg0)
			{
				int custIdx = m_customerNameCombo.indexOf( m_customerNameCombo.getText() );
				if( custIdx < 0 )
				{
					populateActualPlantData();
				}
				else
				{
					populatePlantNameData();
				}
			}
		});		
				
		Label plantLabel = new Label( m_rightComposite, SWT.RIGHT );
		plantLabel.setText( m_appReg.getString( "PlantName.LABEL" ) );
		m_plantNameCombo = new Combo( m_rightComposite, SWT.LEFT );
		m_plantNameCombo.setLayoutData( new GridData( SWT.FILL, SWT.TOP, true, true, 1, 1 ) );				
				
		Label stdProdLabel = new Label( m_rightComposite, SWT.RIGHT );
		stdProdLabel.setText( m_appReg.getString( "StandardProductName.LABEL" ) );
		m_stdProdNameCombo = new Combo( m_rightComposite, SWT.LEFT );
		m_stdProdNameCombo.setLayoutData( new GridData( SWT.FILL, SWT.TOP, true, true, 1, 1 ) );
		
		m_stdProdNameCombo.addSelectionListener( new SelectionListener()
		{
			public void widgetSelected(SelectionEvent arg0)
			{
				
				populateStadardProdModelData();
			}
			
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
			}
		});
		// Deepthi - Jira issue 3.2
		m_stdProdNameCombo.addFocusListener(new FocusListener()
		{
			public void focusGained(FocusEvent arg0)
			{
			}

			public void focusLost(FocusEvent arg0)
			{
				int nameSelIndex = m_stdProdNameCombo.indexOf( m_stdProdNameCombo.getText() );
				if( nameSelIndex <= 0 )
				{					
					populateActualStdProdModelData();
				}
				else
				{
					populateStadardProdModelData();
				}
			}			
		});
		
		Label stdProdModelLabel = new Label( m_rightComposite, SWT.RIGHT );
		stdProdModelLabel.setText( m_appReg.getString( "StandardProductModel.LABEL" ) );
		m_stdProdModelCombo = new Combo( m_rightComposite, SWT.LEFT );
		m_stdProdModelCombo.setLayoutData( new GridData( SWT.FILL, SWT.TOP, true, true, 1, 1 ) );
		
		Label m_smdlTypeLabel = new Label( m_rightComposite, SWT.RIGHT );
		m_smdlTypeLabel.setText( m_appReg.getString( "SMDLType.LABEL" ) );
		m_smdlTypeCombo = new Combo( m_rightComposite, SWT.LEFT );
		m_smdlTypeCombo.setLayoutData( new GridData( SWT.FILL, SWT.TOP, true, true, 1, 1 ) );
	}
	
	private void addKeyBoardListener()
	{
		m_keyBoardListener = new SMDLKeyBoardEventListener( this, m_novSearchDataModel ); 
		
		m_orderID.addKeyListener( m_keyBoardListener );
		m_dpID.addKeyListener( m_keyBoardListener );
		m_smdlID.addKeyListener( m_keyBoardListener );
		m_salesOrderNum.addKeyListener( m_keyBoardListener );
		m_engJobNum.addKeyListener( m_keyBoardListener );

		m_customerNameCombo.addKeyListener( m_keyBoardListener );
		m_plantNameCombo.addKeyListener( m_keyBoardListener );
		m_stdProdNameCombo.addKeyListener( m_keyBoardListener );
		m_stdProdModelCombo.addKeyListener( m_keyBoardListener );
		m_smdlTypeCombo.addKeyListener( m_keyBoardListener );
	}
	
	public void populateInputData()
	{
		// This will update the UI asynchronously, e.g. the calling thread will continue
		Display.getDefault().asyncExec(new Runnable() {
			public void run() 
			{
				ProgressMonitorDialog createSearchInput = new ProgressMonitorDialog( Display.getDefault().getActiveShell() );
				
				/// Single Instance of the SMDLSearchInputDataProvider.
				SMDLSearchInputDataProvider searchInputData = SMDLSearchInputDataProvider.getInstance();
		
				try
				{
					createSearchInput.run( true, false, searchInputData );
				}
				catch( Exception e1 )
				{
		            e1.printStackTrace();
		        }
				
				m_custNames = searchInputData.getCustomerNames();	
				m_plantNames = searchInputData.getRigNames();	
				populateActualCustomerData();
				populateActualPlantData();
			
				m_actualStdProductData = searchInputData.getStandardProductData();
				
				createStadardProdModelData();
				
				/// Populate the Product MOdel Data based on the Product Names.
				populateActualStdProdNameData();
				populateActualStdProdModelData();
			
				String[] smdlTypes = searchInputData.getSMDLTypes();
				m_smdlTypeCombo.setItems( smdlTypes );
				NOVSMDLComboUtils.setAutoComboArray( m_smdlTypeCombo, smdlTypes );
			}
		});		
	}
	
	private void createStadardProdModelData()
	{
		m_prodModelNames = new String[m_actualStdProductData.length];
		m_prodName = new String[m_actualStdProductData.length];

		for( int index = 0; index < m_actualStdProductData.length; ++index )
		{
			String[] splitedString = m_actualStdProductData[index].split( ":" );
			if( splitedString.length > 1 )
			{
				m_prodModelNames[index] = splitedString[0];
				m_prodName[index] = splitedString[1];
			}
			else
			{
				m_prodModelNames[index] = "";
				m_prodName[index] = "";
			}
		}
		
        Set hashSetProdName = new HashSet( Arrays.asList( m_prodName ) );
        m_prodName = ( String[] ) hashSetProdName.toArray( new String[hashSetProdName.size()] );
  
        Set hashSetProdModel = new HashSet( Arrays.asList( m_prodModelNames ) );
        m_prodModelNames = ( String[] ) hashSetProdModel.toArray( new String[hashSetProdModel.size()] );
        
        sortProductModelData();
	}
	
	private void sortProductModelData()
	{
		Arrays.sort( m_prodModelNames );
		Arrays.sort( m_prodName );
	}
	
	private void populateStadardProdNameData()
	{
		int selIndex = m_stdProdModelCombo.getSelectionIndex();
		int selNameIndex = m_stdProdNameCombo.getSelectionIndex();

		if( ( selIndex != -1 || selIndex == -1 ) && ( selNameIndex != 0 && selNameIndex != -1 ) )
		{
			return;
		}
		if( ( selIndex > 0 ) )
		{
			m_prevModelIndex = selIndex;
			m_stdProdNameCombo.removeAll();
			String selProdModel = m_stdProdModelCombo.getItem( selIndex );
			for( int index = 0; index < m_actualStdProductData.length; ++index )
			{
				if( index == 0 )
				{
					m_stdProdNameCombo.add( "" );
				}
				
				String[] splitedString = m_actualStdProductData[index].split( ":" );
				
				if( splitedString.length > 1 && splitedString[0].equals( selProdModel ) )
				{
					m_stdProdNameCombo.add( splitedString[1] );
				}
			}
		}
	}
	
	private void populateStadardProdModelData()
	{
		int selIndex = m_stdProdNameCombo.indexOf( m_stdProdNameCombo.getText() );

		if( selIndex == -1 || selIndex == 0 )
		{
			//populateActualStdProdData();			
			populateActualStdProdModelData();
		}
		else
		{
			m_stdProdModelCombo.removeAll();
			String selProdName = m_stdProdNameCombo.getItem( selIndex );
			for( int index = 0; index < m_actualStdProductData.length; ++index )
			{
				String[] splitedString = m_actualStdProductData[index].split( ":" );
				
				if( splitedString.length > 1 && splitedString[1].equals( selProdName ) )
				{
					m_stdProdModelCombo.add( splitedString[0] );
				}
			}
		}
	}
	
	private void populateActualStdProdModelData()
	{
		String productModelName = null;
		if( !( m_stdProdModelCombo.getText().trim().equalsIgnoreCase( "" ) ) )
		{
			productModelName =  m_stdProdModelCombo.getText();
		}
		
		//m_stdProdModelCombo.setItems( m_prodModelNames );
		NOVSMDLComboUtils.setAutoComboArray( m_stdProdModelCombo, m_prodModelNames );
		
		if( productModelName != null && !( productModelName.trim().equalsIgnoreCase( "" ) ) )
		{
			int custIdx = m_stdProdModelCombo.indexOf( productModelName );
			m_stdProdModelCombo.select( custIdx );
		}
	}
	
	private void populateActualStdProdNameData()
	{
		m_stdProdNameCombo.setItems( m_prodName );
		NOVSMDLComboUtils.setAutoComboArray( m_stdProdNameCombo, m_prodName );
	}	
	
	private void populateActualCustomerData()
	{						
		m_customerNameCombo.setItems( m_custNames );
		NOVSMDLComboUtils.setAutoComboArray( m_customerNameCombo, m_custNames );		
	}
	
	private void populateActualPlantData()
	{
		String plantName = null;
		if( !( m_plantNameCombo.getText().trim().equalsIgnoreCase( "" ) ) )
		{
			plantName =  m_plantNameCombo.getText();
		}
		
		//m_plantNameCombo.setItems( m_plantNames );
		NOVSMDLComboUtils.setAutoComboArray( m_plantNameCombo, m_plantNames );
		
		if( plantName != null && !( plantName.trim().equalsIgnoreCase( "" ) ) )
		{
			int custIdx = m_plantNameCombo.indexOf( plantName );
			m_plantNameCombo.select( custIdx );
		}
	}
	
	private void populatePlantNameData()
	{
		int custIdx = m_customerNameCombo.indexOf( m_customerNameCombo.getText() );

		if( custIdx == -1 ) 
		{
			populateActualPlantData();
		} 
		else 
		{
			SMDLSearchInputDataProvider searchInputData = SMDLSearchInputDataProvider.getInstance();
			String customerName = m_customerNameCombo.getItem( custIdx );
			String[] cplants = searchInputData.getRigNames( customerName );
			m_plantNameCombo.removeAll();
			NOVSMDLComboUtils.setAutoComboArray(m_plantNameCombo, cplants);
		}
	}
	
	/// The Executor( in this case the SelectionListeners ) of the any action will require the following 
	/// INOVSearchProvider : For executing the query.
	/// NovSearchDataModel : For updating the data ( data will update the respective views ).
	public void setDataForExecution()
	{
		addKeyBoardListener();
		m_compToolBar.setDataForExecution( this, m_novSearchDataModel );
	}

	public boolean validateInput()
	{
		boolean retValue = true;
		
		m_strOrderID = m_orderID.getText().trim();
		m_strDPID = m_dpID.getText().trim();
		m_strSMDLID = m_smdlID.getText().trim();
		m_strSalesOrderNum = m_salesOrderNum.getText().trim();
		m_strEngJobNum = m_engJobNum.getText().trim();

		if( m_customerNameCombo.getText() != null )
		{
	    	m_strCustomerName = m_customerNameCombo.getText().trim();
		}
	    else
	    {
	    	m_strCustomerName ="";
	    }
	    
	    if( m_plantNameCombo.getText() != null)
	    {
	    	m_strRigName = m_plantNameCombo.getText().trim();
	    }
	    else
	    {
	    	m_strRigName = "";
	    }

		if( m_stdProdModelCombo.getText() != null )
		{
	    	m_strStdProdModel = m_stdProdModelCombo.getText().trim();
		}
	    else
	    {
	    	m_strStdProdModel = "";
	    }
	    
	    if( m_stdProdNameCombo.getText() != null )
	    {
	    	m_strStdProdName = m_stdProdNameCombo.getText().trim();
	    }
	    else
	    {
	    	m_strStdProdName = "";
	    }
	    
	    if( m_smdlTypeCombo.getText() != null )
	    {
	    	m_strSMDLType = m_smdlTypeCombo.getText().trim();
	    }
	    else
	    {
	    	m_strSMDLType = "";
	    }
	    
		if( m_strOrderID.isEmpty() && m_strDPID.isEmpty() && m_strSMDLID.isEmpty() &&
			m_strSalesOrderNum.isEmpty() && m_strEngJobNum.isEmpty() &&	
			m_strCustomerName.isEmpty() && m_strRigName.isEmpty() && m_strStdProdModel.isEmpty() &&
			m_strStdProdName.isEmpty() && m_strSalesOrderNum.isEmpty() && m_strSMDLType.isEmpty()
		  )
		{
			Registry reg = Registry.getRegistry( this );
            MessageBox.post( reg.getString( "NO_INPUT_CRITERIA_FOUND.MSG" ),
            		reg.getString( "MESSAGE.TITLE" ), 1 );
			retValue = false;
		}
		
		return retValue;		
	}
	
	public QuickBindVariable[] getBindVariables()
	{
		if( m_strOrderID == null  )
		{
			m_strOrderID = "";
		}
				
		if( m_strDPID == null )
		{
			m_strDPID = "";
		}
		
		if( m_strSMDLID == null  )
		{
			m_strSMDLID = "";
		}
		
		if( m_strSalesOrderNum == null || m_strSalesOrderNum.isEmpty() )
		{
			m_strSalesOrderNum = "";
		}

		if( m_strEngJobNum == null || m_strEngJobNum.isEmpty() )
		{
			m_strEngJobNum = "";
		}
		
		if( m_strCustomerName == null || m_strCustomerName.isEmpty() )
		{
			m_strCustomerName = "";
		}
		
		if( m_strRigName == null || m_strRigName.isEmpty() )
		{
			m_strRigName = "";
		}
		
		if( m_strStdProdName == null || m_strStdProdName.isEmpty() )
		{
			m_strStdProdName = "";
		}
		
		if( m_strStdProdModel == null || m_strStdProdModel.isEmpty() )
		{
			m_strStdProdModel = "";
		}
		
		if( m_strSMDLType == null || m_strSMDLType.isEmpty() )
		{
			m_strSMDLType = "";
		}

		String itemType = getBusinessObject();
		
		QuickSearchService.QuickBindVariable [] allBindVars = new QuickSearchService.QuickBindVariable[m_bindVarCount];		
		
		// Item.object_type
		allBindVars[0] = new QuickSearchService.QuickBindVariable();
		allBindVars[0].nVarType = POM_string;
		allBindVars[0].nVarSize = 1;
		allBindVars[0].strList = new String[] { itemType };
		
		// Order.item_id
		allBindVars[1] = new QuickSearchService.QuickBindVariable();
		allBindVars[1].nVarType = POM_string;
		allBindVars[1].nVarSize = 1;
		allBindVars[1].strList = new String[]{ m_strOrderID };
		
		// DeliverdProduct.item_id
		allBindVars[2] = new QuickSearchService.QuickBindVariable();
		allBindVars[2].nVarType = POM_string;
		allBindVars[2].nVarSize = 1;
		allBindVars[2].strList = new String[]{ m_strDPID };
		
		// SMDL.item_id
		allBindVars[3] = new QuickSearchService.QuickBindVariable();
		allBindVars[3].nVarType = POM_string;
		allBindVars[3].nVarSize = 1;
		allBindVars[3].strList = new String[]{ m_strSMDLID };
		
		// Order.nov4_sales_order_number
		allBindVars[4] = new QuickSearchService.QuickBindVariable();
		allBindVars[4].nVarType = POM_string;
		allBindVars[4].nVarSize = 1;
		allBindVars[4].strList = new String[]{ m_strSalesOrderNum };

		// DeliverdProduct.nov4_eng_job_numbers
		allBindVars[5] = new QuickSearchService.QuickBindVariable();
		allBindVars[5].nVarType = POM_string;
		allBindVars[5].nVarSize = 1;
		allBindVars[5].strList = new String[]{ m_strEngJobNum };
		
		// Order.nov4_customer
		allBindVars[6] = new QuickSearchService.QuickBindVariable();
		allBindVars[6].nVarType = POM_string;
		allBindVars[6].nVarSize = 1;
		allBindVars[6].strList = new String[]{ m_strCustomerName };
		
		// Order.nov4_plant_id_other
		allBindVars[7] = new QuickSearchService.QuickBindVariable();
		allBindVars[7].nVarType = POM_string;
		allBindVars[7].nVarSize = 1;
		allBindVars[7].strList = new String[]{ m_strRigName };

		/// Standard product model is combination of StandardProductName and StandardProductModel
		//  StandardProductModel:StandardProductName
		String stdProductModel = getStandardProductModel();
		
		// DeliverdProduct.nov4_basedon_std_prod
		allBindVars[8] = new QuickSearchService.QuickBindVariable();
		allBindVars[8].nVarType = POM_string;
		allBindVars[8].nVarSize = 1;
		allBindVars[8].strList = new String[]{ stdProductModel };

		// SMDL.smdl_type
		allBindVars[9] = new QuickSearchService.QuickBindVariable();
		allBindVars[9].nVarType = POM_string;
		allBindVars[9].nVarSize = 1;
		allBindVars[9].strList = new String[]{ m_strSMDLType };

        return allBindVars;		
	}

	public String getBusinessObject()
	{
		return "Order";
	}

	public QuickHandlerInfo[] getHandlerInfo()
	{
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[m_handlerCount];
		
		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-orderdpsmdl";
		allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5 };
		allHandlerInfo[0].listInsertAtIndex = new int[] {   1, 9, 2, 7, 3 };

		allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[1].handlerName = "NOVSRCH-get-dp";
		allHandlerInfo[1].listBindIndex = new int[] { 3, 4, 6, 9, 10 };
		allHandlerInfo[1].listReqdColumnIndex = new int[] { 1,  2,  3,  4,  5,  6,  7 };
		allHandlerInfo[1].listInsertAtIndex = new int[] {   10, 11, 6,  8,  12, 14, 13 };		
		
		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[2].handlerName = "NOVSRCH-get-dummy";
		allHandlerInfo[2].listBindIndex = new int[0];
		allHandlerInfo[2].listReqdColumnIndex = new int[] { 1, 2 };
		allHandlerInfo[2].listInsertAtIndex = new int[] { 4, 5 };
		
		return allHandlerInfo;
	}
	
	public String getStandardProductModel()
	{
		String actualStdProdModelName = null;
		
		if( !m_strStdProdName.equals( "" ) && !m_strStdProdModel.equals( "" ) )
		{
			actualStdProdModelName = m_strStdProdModel + ":" + m_strStdProdName;
		}
		else if( !m_strStdProdName.equals( "" ) )
		{
			actualStdProdModelName = "*" + m_strStdProdName;
		}
		else if( !m_strStdProdModel.equals( "" ) )
		{
			actualStdProdModelName = m_strStdProdModel + "*";
		}
		else
		{
			actualStdProdModelName = "";
		}

		return actualStdProdModelName;
	}		
	
	public void setNovSearchDataModel( NovSearchDataModel novSearchDataModel )
	{
		m_novSearchDataModel = novSearchDataModel;
		
		/// Register the View(Observer) to the NovSearchDataModel(Subject).
		m_novSearchDataModel.registerObserver( this );
	}
	
	public void update()
	{
		int respIndex = m_novSearchDataModel.getResponseIndex();
		int pageSize = m_novSearchDataModel.getPageSize();
		int totalCount = m_novSearchDataModel.getTotalResultCount();
		INOVResultSet searchResult = m_novSearchDataModel.getResultSet();
		
		switch( m_novSearchDataModel.getReasonForChange() )
		{
			case NOV_CLEAR:
			{
				clearInputData();
				break;
			}
			case NOV_LOAD:
			{
				if( searchResult.getRows() == 0 )
				{
					setSearchResultInfo( -1, respIndex, totalCount );
				}
				else
				{
					setSearchResultInfo( 0, respIndex, totalCount );
				}
				
				break;
			}
			case NOV_LOAD_ALL:
			{
				setSearchResultInfo( 0, totalCount, totalCount );
				break;
			}

			case NOV_NEXT:
			{
				int start = 0;
				
				if(  respIndex % pageSize != 0 )
				{
					start = ( respIndex / pageSize ) * pageSize;
				}
				else
				{
					start = respIndex - pageSize;
				}

				setSearchResultInfo( start, respIndex, totalCount );
				break;
			}
			case NOV_PREV:
			{
				int start = 0;
				
				if(  respIndex % pageSize != 0 )
				{
					start = ( respIndex / pageSize ) * pageSize;
					respIndex = start + pageSize;
				}
				else
				{
					start = respIndex - pageSize;
				}

				setSearchResultInfo( start, respIndex, totalCount );
				break;
			}
		}
		
		updateControlState();
	}

	private void setSearchResultInfo( int startIndex, int respIndex, int totalCount )
	{
		m_resultInfoLabel.setText( "( " + Integer.toString( startIndex + 1 ) +
				" ~ " + Integer.toString( respIndex ) +
				" / " + Integer.toString( totalCount ) + " )" );
	}

	
	private void clearInputData()
	{
		m_orderID.setText( "" );
		m_dpID.setText( "" );
		m_smdlID.setText( "" );
		m_salesOrderNum.setText( "" );
		m_engJobNum.setText( "" );

		m_customerNameCombo.setText( "" );
		m_plantNameCombo.setText( "" );
		m_stdProdNameCombo.setText( "" );
		m_stdProdModelCombo.setText( "" );
		m_smdlTypeCombo.setText( "" );	
		
		m_resultInfoLabel.setText( "" );
		
		m_novSearchDataModel.setInputDataState( InputDataState.NONE );

//      populateActualPlantData();
//    	populateActualStdProdModelData();
		Display.getCurrent().asyncExec((new Runnable() {
			@Override
			public void run() {
				populateActualPlantData();
				populateActualStdProdModelData();
			}
		}));

	}
	
	private void updateControlState()
	{
		m_compToolBar.updateControlState();
	}
	
	 public void dispose()
	 {
		 m_novSearchDataModel.removeObserver( this );
	 }
}