package com.nov.rac.smdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentSMDLHome;
import com.nov.rac.kernel.NOV4ComponentSMDLHomeType;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentFolderType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class SMDLHomePane extends Composite {

	private SMDLHomeTreeComposite m_smdlHome;
	
	public SMDLHomePane(Composite parent, int style) {
		super(parent, SWT.NONE);
		
		setLayout(new FillLayout());
		m_smdlHome= new SMDLHomeTreeComposite(this, SWT.NONE);
		
		openSMDLHomeFolder();
		
	}

	public SMDLHomeTreeComposite getSmdlHome() {
		return m_smdlHome;
	}

	
	public void openSMDLHomeFolder(){

		TCComponentFolder smdlHome = null;
		TCSession session = (TCSession) AIFUtility.getDefaultSession();
		try {
			
			TCComponentFolder userHomeFolder = session.getUser().getHomeFolder();

			smdlHome = (TCComponentFolder) userHomeFolder.getRelatedComponent(NOV4ComponentSMDLHome.SMDL_HOME_RELATION);

			if (smdlHome == null) {

				TCComponentFolderType smdlHomeType = (TCComponentFolderType) session
						.getTypeService().getTypeComponent(NOV4ComponentSMDLHome.SMDL_HOME_OBJECT_TYPE);

				smdlHome = smdlHomeType.create(NOV4ComponentSMDLHome.SMDL_HOME_NAME,
												NOV4ComponentSMDLHome.SMDL_HOME_DESC,
												NOV4ComponentSMDLHome.SMDL_HOME_OBJECT_TYPE);

				userHomeFolder.add(NOV4ComponentSMDLHome.SMDL_HOME_RELATION, smdlHome);		
			}

		} catch (TCException e) {

			e.printStackTrace();
		}
		
		getSmdlHome().populateTree(smdlHome);
	}
	

	
}
