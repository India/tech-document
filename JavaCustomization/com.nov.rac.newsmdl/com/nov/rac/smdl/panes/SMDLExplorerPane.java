package com.nov.rac.smdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.utilities.NOVSMDLTreeSOAHelper;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.kernel.TCComponent;

public class SMDLExplorerPane extends Composite {

	private SMDLExplorer m_smdlExplorer;


	public SMDLExplorerPane(Composite parent, int style) {
		super(parent, style);
		
		setLayout(new FillLayout());
		m_smdlExplorer= new SMDLExplorer(this, SWT.NONE);
	}


	public SMDLExplorer getSMDLExplorer() {
		return m_smdlExplorer;
	}
	
	public void open(TCComponent component){
		
		//call soa 			        				            	
        SMDLTreeOutput theTreeOutput = NOVSMDLTreeSOAHelper.getRelatedObjects(component);
		
		getSMDLExplorer().populateTree(theTreeOutput,component);
	}

}
