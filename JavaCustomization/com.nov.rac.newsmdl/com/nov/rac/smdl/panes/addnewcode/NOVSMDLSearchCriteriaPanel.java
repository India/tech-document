package com.nov.rac.smdl.panes.addnewcode;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.apache.log4j.Logger;

import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.InterfaceAIFOperationListener;
import com.teamcenter.rac.aif.kernel.AIFInputEvent;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.findwithproperties.TCAdhocQueryClause;
import com.teamcenter.rac.common.lov.LOVUIComponent;
import com.teamcenter.rac.explorer.common.AbstractOptionPanel;
import com.teamcenter.rac.explorer.common.AdhocQueryChangeListener;
import com.teamcenter.rac.explorer.common.AdhocSearchContext;
import com.teamcenter.rac.explorer.common.ApplicationMenuHolder;
import com.teamcenter.rac.explorer.common.ICSClassQueryBanner;
import com.teamcenter.rac.explorer.common.ICSClassQueryDialog;
import com.teamcenter.rac.explorer.common.TCDataPaneManager;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentContextList;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCDateFormat;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCICSClass;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCQueryClause;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ButtonLayout;
import com.teamcenter.rac.util.Cookie;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.HorizontalLayout;
import com.teamcenter.rac.util.HyperLink;
import com.teamcenter.rac.util.MLabel;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Painter;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.Separator;
import com.teamcenter.rac.util.Utilities;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.iTextField;

public class NOVSMDLSearchCriteriaPanel extends JPanel
    implements AdhocQueryChangeListener, AdhocSearchContext
{
	private static final long serialVersionUID = 1L;
	protected class QueryDetailDialog extends AbstractAIFDialog
    {
		private static final long serialVersionUID = 1L;

		public Icon getQueryIcon(Object obj)
        {
            ImageIcon imageicon = null;
            if(obj instanceof NOVSMDLSearchCriteriaPanel)
                obj = ((NOVSMDLSearchCriteriaPanel)obj).getQuery();
            if(obj instanceof TCComponentQuery)
            {
                TCComponentQuery tccomponentquery = (TCComponentQuery)obj;
                try
                {
                    String s = tccomponentquery.getProperty(TCComponentQuery.PROP_QUERY_FLAG);
                    if(s.equals(TCComponentQuery.QRY_DOMAIN_LOCAL))
                        imageicon = appReg.getImageIcon("localQuery.ICON");
                    else
                    if(s.equals(TCComponentQuery.QRY_DOMAIN_REMOTE))
                        imageicon = appReg.getImageIcon("remoteQuery.ICON");
                    else
                        imageicon = appReg.getImageIcon("userExitQuery.ICON");
                }
                catch(Exception exception)
                {
                    imageicon = TCTypeRenderer.getIcon(tccomponentquery, false);
                }
            } else
            if(obj instanceof TCComponent)
                imageicon = TCTypeRenderer.getIcon(obj, false);
            return imageicon;
        }

        private void addAdhocClauses(int i, TCAdhocQueryClause atcadhocqueryclause[], JPanel jpanel, List list, List list1, List list2, List list3)
        {
            try
            {
                for(int j = 0; j < atcadhocqueryclause.length; j++)
                {
                    String s = "";
                    s = atcadhocqueryclause[j].getLogicalOperator();
                    JLabel jlabel = new JLabel(s != null && s.length() > 0 ? s : "AND");
                    jpanel.add((new StringBuilder()).append(j + i + 1).append(".1.left.center").toString(), jlabel);
                    java.awt.Font font = jlabel.getFont();
                    font = new java.awt.Font(font.getName(), 2, font.getSize());
                    boolean flag = true;
                    jlabel.setFont(font);
                    jlabel.setEnabled(flag);
                    s = atcadhocqueryclause[j].getClauseName(null, true);
                    jlabel = new JLabel(s != null ? s : "");
                    jlabel.setFont(font);
                    jlabel.setEnabled(flag);
                    list.add(jlabel);
                    jpanel.add((new StringBuilder()).append(j + i + 1).append(".2.left.center").toString(), jlabel);
                    s = atcadhocqueryclause[j].getDisplayName();
                    if(s == null || s.length() <= 0)
                        s = atcadhocqueryclause[j].getClassName(true);
                    jlabel = new JLabel(s != null ? s : "");
                    jlabel.setFont(font);
                    jlabel.setEnabled(flag);
                    list1.add(jlabel);
                    jpanel.add((new StringBuilder()).append(j + i + 1).append(".3.left.center").toString(), jlabel);
                    s = atcadhocqueryclause[j].getMathOperator();
                    jlabel = new JLabel(s != null ? s : "");
                    jlabel.setFont(font);
                    jlabel.setEnabled(flag);
                    list2.add(jlabel);
                    jpanel.add((new StringBuilder()).append(j + i + 1).append(".4.left.center").toString(), jlabel);
                    s = atcadhocqueryclause[j].getValue();
                    jlabel = new JLabel(s != null ? s : "");
                    jlabel.setFont(font);
                    jlabel.setEnabled(flag);
                    list3.add(jlabel);
                    jpanel.add((new StringBuilder()).append(j + i + 1).append(".5.left.center").toString(), jlabel);
                }

            }
            catch(Exception exception) { }
        }

        protected TCComponentQuery query;
        final NOVSMDLSearchCriteriaPanel this$0;

        public QueryDetailDialog(java.awt.Frame frame, TCComponentQuery tccomponentquery, TCAdhocQueryClause atcadhocqueryclause[])
        {
            super(frame, true);
            this$0 = NOVSMDLSearchCriteriaPanel.this;
            query = tccomponentquery;
            setTitle((new StringBuilder()).append(appReg.getString("queryDetail")).append(" ... ").append(tccomponentquery).toString());
            JPanel jpanel = new JPanel(new VerticalLayout(2, 4, 4, 8, 2));
            getContentPane().add(jpanel);
            JButton jbutton = new JButton(appReg.getString("close"));
            jbutton.setMnemonic(appReg.getString("close.MNEMONIC").charAt(0));
            jbutton.addActionListener(new IC_DisposeActionListener());
            JPanel jpanel1 = new JPanel(new ButtonLayout());
            jpanel1.add(jbutton);
            JPanel jpanel2 = new JPanel(new PropertyLayout(10, 0, 2, 2, 2, 2));
            ArrayList arraylist = new ArrayList();
            ArrayList arraylist1 = new ArrayList();
            ArrayList arraylist2 = new ArrayList();
            ArrayList arraylist3 = new ArrayList();
            try
            {
                TCQueryClause atcqueryclause[] = tccomponentquery.describe();
                if(atcqueryclause != null)
                {
                    int i = 0;
                    TCQueryClause atcqueryclause1[] = atcqueryclause;
                    int k = atcqueryclause1.length;
                    for(int l = 0; l < k; l++)
                    {
                        TCQueryClause tcqueryclause = atcqueryclause1[l];
                        String s = tcqueryclause.getUserEntryNameDisplay();
                        String s1 = "";
                        if(criteriaTable.get(s) == null)
                            s1 = tcqueryclause.getDefaultValue();
                        else
                            s1 = getEntryValue(s);
                        String s2 = "";
                        JLabel jlabel1;
                        if(i == 0)
                        {
                            jlabel1 = new JLabel(" ");
                            jpanel2.add("1.1", jlabel1);
                        } else
                        {
                            s2 = tcqueryclause.getLogicalOperator();
                            jlabel1 = new JLabel(s2 != null ? s2 : "");
                            jpanel2.add((new StringBuilder()).append(i + 1).append(".1.left.center").toString(), jlabel1);
                        }
                        java.awt.Font font1 = jlabel1.getFont();
                        if(s == null || s.trim().length() <= 0)
                            font1 = new java.awt.Font(font1.getName(), 2, font1.getSize());
                        boolean flag = true;
                        jlabel1.setFont(font1);
                        jlabel1.setEnabled(flag);
                        s2 = tcqueryclause.getAttributeName();
                        jlabel1 = new JLabel(s2 != null ? s2 : "");
                        jlabel1.setFont(font1);
                        jlabel1.setEnabled(flag);
                        arraylist.add(jlabel1);
                        jpanel2.add((new StringBuilder()).append(i + 1).append(".2.left.center").toString(), jlabel1);
                        jlabel1 = new JLabel(s2 != null ? s : "");
                        jlabel1.setFont(font1);
                        jlabel1.setEnabled(flag);
                        arraylist1.add(jlabel1);
                        jpanel2.add((new StringBuilder()).append(i + 1).append(".3.left.center").toString(), jlabel1);
                        s2 = tcqueryclause.getMathOperator();
                        jlabel1 = new JLabel(s2 != null ? s2 : "");
                        jlabel1.setFont(font1);
                        jlabel1.setEnabled(flag);
                        arraylist2.add(jlabel1);
                        jpanel2.add((new StringBuilder()).append(i + 1).append(".4.left.center").toString(), jlabel1);
                        jlabel1 = new JLabel(s2 != null ? s1 : "");
                        jlabel1.setFont(font1);
                        jlabel1.setEnabled(flag);
                        arraylist3.add(jlabel1);
                        jpanel2.add((new StringBuilder()).append(i + 1).append(".5.left.center").toString(), jlabel1);
                        i++;
                    }

                    if(atcadhocqueryclause != null && atcadhocqueryclause.length > 0)
                        addAdhocClauses(atcqueryclause.length, atcadhocqueryclause, jpanel2, arraylist, arraylist1, arraylist2, arraylist3);
                    AbstractAIFDialog.setSameSize((JComponent[])(JComponent[])arraylist.toArray(new JComponent[arraylist.size()]), null);
                    AbstractAIFDialog.setSameSize((JComponent[])(JComponent[])arraylist1.toArray(new JComponent[arraylist1.size()]), null);
                    AbstractAIFDialog.setSameSize((JComponent[])(JComponent[])arraylist2.toArray(new JComponent[arraylist2.size()]), null);
                    AbstractAIFDialog.setSameSize((JComponent[])(JComponent[])arraylist3.toArray(new JComponent[arraylist3.size()]), null);
                }
            }
            catch(Exception exception) { }
            jpanel.add("left.nobind.left.top", new JLabel(getQueryIcon(tccomponentquery)));
            jpanel.add("top.bind", new Separator());
            try
            {
                String as[] = tccomponentquery.getProperties(NOVSMDLSearchCriteriaPanel.QUERY_PROPS);
                for(int j = 0; j < as.length; j++)
                    if(as[j] == null || as[j].trim().length() <= 0)
                        as[j] = " ";

                if(as != null)
                {
                    JPanel jpanel3 = new JPanel(new PropertyLayout(5, 2, 2, 2, 2, 2));
                    JLabel jlabel = new JLabel(appReg.getString("description"));
                    jpanel3.add("1.1.right.top", jlabel);
                    java.awt.Font font = jlabel.getFont();
                    font = new java.awt.Font(font.getName(), 1, font.getSize());
                    jlabel.setFont(font);
                    jpanel3.add("1.2.left.top", new MLabel(as[1]));
                    jlabel = new JLabel(appReg.getString("queryClass"));
                    jlabel.setFont(font);
                    jpanel3.add("2.1.right.top", jlabel);
                    jpanel3.add("2.2.left.top", new JLabel(as[2]));
                    jpanel.add("top.bind", jpanel3);
                }
            }
            catch(Exception exception1)
            {
                MessageBox.post(AbstractAIFDialog.getParentFrame(NOVSMDLSearchCriteriaPanel.this), exception1);
            }
            TitledBorder titledborder = new TitledBorder(BorderFactory.createEtchedBorder(), appReg.getString("searchCriteria"));
            JPanel jpanel4 = new JPanel(new java.awt.BorderLayout());
            jpanel4.setBorder(titledborder);
            jpanel4.add(new JScrollPane(jpanel2), "Center");
            jpanel4.setPreferredSize(AbstractAIFDialog.getSizeRelatedToScreen(0.45000000000000001D, 0.26000000000000001D));
            jpanel.add("unbound.bind.center.center", jpanel4);
            jpanel.add("bottom.bind", jpanel1);
            pack();
            centerToScreen();
        }
    }

    protected class MoreLessLabel extends HyperLink
    {

        public boolean isMoreDisplay()
        {
            String s = getText();
            if(s != null)
                return s.equals(more);
            else
                return false;
        }

        protected void changeStatus()
        {
            String s = less;
            if(!isMoreDisplay())
                s = more;
            setText(s);
        }

        private final String more;
        private final String less;
        final NOVSMDLSearchCriteriaPanel this$0;

        public MoreLessLabel()
        {
            super("        ");
            this$0 = NOVSMDLSearchCriteriaPanel.this;
            final MoreLessLabel this$1;
            this$1 = MoreLessLabel.this;
            more = appReg.getString("more");
            less = appReg.getString("less");
            setText(more);
            addMouseListener(new java.awt.event.MouseAdapter() {

                public void mousePressed(java.awt.event.MouseEvent mouseevent)
                {
                    changeStatus();
                }

                
            }
);
        }
    }

    protected class CriteriaPair
    {

        public JComponent getNameComponent()
        {
            return nameComp;
        }

        public JComponent getValueComponent()
        {
            return valueComp;
        }

        public boolean isFieldsVisible()
        {
            String s = getFieldValue(getValueComponent());
            boolean flag = s != null && s.trim().length() > 0;
            if(flag)
                return flag;
            String as[] = NOVSMDLSearchCriteriaPanel.cookie.getStringArray((new StringBuilder()).append(currentQueryString).append(NOVSMDLSearchCriteriaPanel.LATEST).toString());
            if(as != null)
            {
                int i = as.length - 1;
                do
                {
                    if(i < 0)
                        break;
                    if(userEntryName.equals(as[i]))
                    {
                        flag = true;
                        break;
                    }
                    i--;
                } while(true);
            }
            if(!flag)
            {
                int j = NOVSMDLSearchCriteriaPanel.cookie.getNumber((new StringBuilder()).append(currentQueryString).append(NOVSMDLSearchCriteriaPanel.TOTAL).toString());
                flag = j == 0;
                if(!flag)
                    flag = (double)NOVSMDLSearchCriteriaPanel.cookie.getNumber((new StringBuilder()).append(currentQueryString).append(".").append(userEntryName).append(NOVSMDLSearchCriteriaPanel.TOTAL).toString()) / (double)j > 0.5D;
            }
            return flag;
        }

        public String getValueString()
        {
            return getFieldValue(valueComp);
        }

        private JComponent nameComp;
        private JComponent valueComp;
        private String userEntryName;
        final NOVSMDLSearchCriteriaPanel this$0;

        public CriteriaPair(JComponent jcomponent, String s, JComponent jcomponent1)
        {
            super();
            this$0 = NOVSMDLSearchCriteriaPanel.this;
            nameComp = null;
            valueComp = null;
            userEntryName = null;
            nameComp = jcomponent;
            valueComp = jcomponent1;
            userEntryName = s;
        }
    }

    protected class BuildQueryFormOperation extends AbstractAIFOperation
    {

        public void executeOperation()
        {
            if(opListener != null)
                opListener.startOperation("");
            final String s;
            displayDetailsFlag = AbstractOptionPanel.isPreferenceTrue(session, "QRY_display_details", "1");
            try {
				currentQueryString = session.componentToString(currentQuery);
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            try {
				clauses = currentQuery.describe();
			
            s = session.getPreferenceService().getString(0, "WSOM_find_list_separator", "").trim();
            criteriaTable.clear();
            hasFixedEntry = false;
            if(clauses == null)
                return;
            int i = clauses.length;
			ArrayList arraylist = new ArrayList();
			for(int j = 0; j < i; j++)
			{
			    TCQueryClause tcqueryclause = clauses[j];
			    String s1 = tcqueryclause.getUserEntryNameDisplay();
			    TCQueryClause atcqueryclause[] = tcqueryclause.getInterdependentClauses();
			    if(s1.length() == 0)
			    {
			        hasFixedEntry = true;
			        if(!displayDetailsFlag)
			            continue;
			    }
			    JLabel jlabel;
			    if(displayDetailsFlag)
			        jlabel = new JLabel((new StringBuilder()).append(tcqueryclause.getLogicalOperator()).append(" ").append(tcqueryclause.getAttributeName()).append(" ").append(tcqueryclause.getMathOperator()).toString(), 4);
			    else
			        jlabel = new JLabel((new StringBuilder()).append(s1).append(":").toString(), 4);
			    com.teamcenter.rac.kernel.TCComponentListOfValues tccomponentlistofvalues = tcqueryclause.getLOV();
			    int k = tcqueryclause.getKeyLOVId();
			    if(NOVSMDLSearchCriteriaPanel.lovComboLimit < 0)
			    	NOVSMDLSearchCriteriaPanel.lovComboLimit = getLOVComboLimit();
			    if(tccomponentlistofvalues != null || k != 0)
			    {
			        final LOVUIComponent lovPopup = new LOVUIComponent(session, tccomponentlistofvalues, k,NOVSMDLSearchCriteriaPanel.this.lovComboLimit, tcqueryclause, atcqueryclause);
			        lovPopup.setMultipleDelimeter(s);
			        lovPopup.setEditable(true);
			        lovPopup.addKeyListener(new java.awt.event.KeyAdapter() {

			            public void keyPressed(java.awt.event.KeyEvent keyevent)
			            {
			                int k1 = keyevent.getKeyCode();
			                if(k1 == 10)
			                    buttonListener.actionPerformed(new java.awt.event.ActionEvent(lovPopup, 1, ""));
			            }

			            final LOVUIComponent val$lovPopup=lovPopup;
			            final BuildQueryFormOperation  this$1 = BuildQueryFormOperation.this;
			           
			
			        }
);
			        Registry registry = Registry.getRegistry("com.teamcenter.rac.util.combobox.combobox");
			        int l = registry.getInt("comboBoxDisplaySize", 15);
			        lovPopup.setMaximumRowCount(l);
			       
			        lovPopup.setPreferredSize(new Dimension(150,20));
			        System.out.println("size: "+lovPopup.getPreferredSize());
			        
			        final String entryValue = tcqueryclause.getDefaultValue();
			        final LOVUIComponent val$lovPopup;
			        final String val$entryValue;
			        final BuildQueryFormOperation this$1;
			        this$1 = BuildQueryFormOperation.this;
			       
			        if(entryValue.length() != 0)
			            SwingUtilities.invokeLater(new Runnable() {
			            	
			                public void run()
			                {
			                    lovPopup.setSelectedValue(entryValue);
			                   
			                }

			              
			
			            }
);
			        arraylist.add(new CriteriaPair(jlabel, s1, lovPopup));
			        criteriaTable.put(s1, lovPopup);
			        String s7 = generateToolTipText(tcqueryclause);
			        lovPopup.setToolTipText(s7);
			        jlabel.setToolTipText(s7);
			        lovPopup.addMouseListener(new java.awt.event.MouseAdapter() {

			            public void mousePressed(java.awt.event.MouseEvent mouseevent)
			            {
			                if(mouseevent.getModifiers() == 4)
			                    pasteMenu.show((java.awt.Component)mouseevent.getSource(), mouseevent.getX(), mouseevent.getY());
			            }

			            final BuildQueryFormOperation this$1= BuildQueryFormOperation.this;
			        }
);
			        continue;
			    }
			    if(tcqueryclause.getAttributeType() % 1000 == 2)
			    {
			        String s2 = tcqueryclause.getDefaultValue();
			        TCDateFormat tcdateformat = session.askTCDateFormat();
			        final DateButton dateButton = new DateButton(tcdateformat.askDefaultDateFormat());
			        dateButton.setDate(s2);
			        dateButton.addKeyListener(new java.awt.event.KeyAdapter() {

			            public void keyPressed(java.awt.event.KeyEvent keyevent)
			            {
			                int k1 = keyevent.getKeyCode();
			                if(k1 == 10)
			                    buttonListener.actionPerformed(new java.awt.event.ActionEvent(dateButton, 1, ""));
			            }

			            final DateButton val$dateButton = dateButton;
			            final BuildQueryFormOperation this$1= BuildQueryFormOperation.this;

			        }
);
			        arraylist.add(new CriteriaPair(jlabel, s1, dateButton));
			        criteriaTable.put(s1, dateButton);
			        String s5 = generateToolTipText(tcqueryclause);
			        dateButton.setToolTipText(s5);
			        jlabel.setToolTipText(s5);
			        continue;
			    }
			    if(currentQuery.toString().equalsIgnoreCase(appReg.getString("lineOfUsage.NAME")) && tcqueryclause.getAttributeName().equalsIgnoreCase("BomSolve.SOLVETYPE"))
			    {
			        String as[] = appReg.getStringArray("BomSolve.TYPES");
			        Vector vector = new Vector();
			        vector.add("");
			        String as1[] = as;
			        int i1 = as1.length;
			        for(int j1 = 0; j1 < i1; j1++)
			        {
			            String s8 = as1[j1];
			            vector.add(s8);
			        }

			        JComboBox jcombobox = new JComboBox(vector);
			        arraylist.add(new CriteriaPair(jlabel, s1, jcombobox));
			        criteriaTable.put(s1, jcombobox);
			        String s6 = generateToolTipText(tcqueryclause);
			        jcombobox.setToolTipText(s6);
			        jcombobox.setToolTipText(s6);
			        continue;
			    }
			    iTextField itextfield = new iTextField(12, NOVSMDLSearchCriteriaPanel.this);
			    String s3 = tcqueryclause.getDefaultValue();
			    if(s3.length() != 0)
			    {
			        itextfield.setText(s3);
			        if(s1.equals("") && displayDetailsFlag)
			            itextfield.setEnabled(false);
			    }
			    itextfield.addActionListener(new java.awt.event.ActionListener() {

			        public void actionPerformed(java.awt.event.ActionEvent actionevent)
			        {
			            buttonListener.actionPerformed(actionevent);
			        }

			        final BuildQueryFormOperation this$1= BuildQueryFormOperation.this;
			    }
);
			    arraylist.add(new CriteriaPair(jlabel, s1, itextfield));
			    criteriaTable.put(s1, itextfield);
			    String s4 = generateToolTipText(tcqueryclause);
			    itextfield.setToolTipText(s4);
			    jlabel.setToolTipText(s4);
			    itextfield.addMouseListener(new java.awt.event.MouseAdapter() {

			        public void mousePressed(java.awt.event.MouseEvent mouseevent)
			        {
			            if(mouseevent.getModifiers() == 4)
			                pasteMenu.show((java.awt.Component)mouseevent.getSource(), mouseevent.getX(), mouseevent.getY());
			        }

			        final BuildQueryFormOperation this$1= BuildQueryFormOperation.this;

			    }
);
			}

			criteriaPairs = (CriteriaPair[])(CriteriaPair[])arraylist.toArray(new CriteriaPair[arraylist.size()]);
			moreLessLabel = new MoreLessLabel();
			moreLessLabel.addMouseListener(new java.awt.event.MouseAdapter() {

			    public void mousePressed(java.awt.event.MouseEvent mouseevent)
			    {
			        SwingUtilities.invokeLater(new Runnable() {

			            public void run()
			            {
			                displayCriteria();
			            }

			        }
);
			    }

			}
);
            SwingUtilities.invokeLater(new Runnable() {

                public void run()
                {
                    initializeCriteria(names, values, adhocClauses);
                    setCursor(java.awt.Cursor.getPredefinedCursor(0));
                    panelBuiltDone = true;
                }

            }
);
            if(opListener != null)
                opListener.endOperation();
            } catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return;
        }
        
        private String names[];
        private String values[];
        private TCAdhocQueryClause adhocClauses[];
        final NOVSMDLSearchCriteriaPanel this$0;




        public BuildQueryFormOperation(String as[], String as1[], TCAdhocQueryClause atcadhocqueryclause[], String s)
        {
        	super(s);
        	this$0 = NOVSMDLSearchCriteriaPanel.this;
            names = as;
            values = as1;
            adhocClauses = atcadhocqueryclause;
        }
        
    }


    public NOVSMDLSearchCriteriaPanel(TCComponentQuery tccomponentquery)
    {
        this(tccomponentquery, null, null);
    }

    public NOVSMDLSearchCriteriaPanel(TCComponentQuery tccomponentquery, String as[], String as1[])
    {
        super(new VerticalLayout());
        criteriaTable = new HashMap();
        panelBuiltDone = false;
        hasFixedEntry = false;
        addHierarchyListener(new java.awt.event.HierarchyListener() {

            public void hierarchyChanged(java.awt.event.HierarchyEvent hierarchyevent)
            {
                if(hierarchyevent.getID() == 1400 && (hierarchyevent.getChangeFlags() & 2L) != 0L && hierarchyevent.getChanged().isDisplayable())
                    ((NOVSMDLSearchCriteriaPanel)hierarchyevent.getSource()).requestFocus();
            }

        }
);
        r = Registry.getRegistry(this);
        revalidate();
        initialNames = as;
        initialValues = as1;
        noCriteriaWarningMessage = r.getString("noCriteria");
        session = tccomponentquery.getSession();
        appReg = Registry.getRegistry(this);
        pasteMenu = new JPopupMenu();
        replaceText = new JMenuItem(appReg.getString("query.ReplaceText"));
        appendToText = new JMenuItem(appReg.getString("query.AppendText"));
        pasteMenu.add(replaceText);
        pasteMenu.add(appendToText);
        replaceText.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent actionevent)
            {
                pasteText(pasteMenu.getInvoker(), false);
            }
        }
);
        appendToText.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent actionevent)
            {
                pasteText(pasteMenu.getInvoker(), true);
            }

        }
);
        setOpaque(false);
        addAncestorListener(new AncestorListener() {

            public void ancestorAdded(AncestorEvent ancestorevent)
            {
                for(java.awt.Container container = getParent(); container != null; container = container.getParent())
                {
                    container.validate();
                    container.repaint();
                }

            }

            public void ancestorMoved(AncestorEvent ancestorevent)
            {
            }

            public void ancestorRemoved(AncestorEvent ancestorevent)
            {
            }
        }
);
        multiLevelResultsToggle = new JCheckBox("", false);
        multiLevelResultsToggle.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent actionevent)
            {
                multiLevelResultsSelection = multiLevelResultsToggle.isSelected();
            }

        }
);
        multiLevelResultsToggle.setToolTipText(appReg.getString("resultsTypeLabel.TIP"));
        multiLevelResultsLabel = new JLabel(appReg.getString("resultsTypeLabel"));
        try
        {
            isAMultiLevelResultsSearch = tccomponentquery.getProperty("results_type").equals("1");
            if(isAMultiLevelResultsSearch)
                multiLevelResultsSelection = true;
        }
        catch(Exception exception) { }
        buildPanel(tccomponentquery, null);
    }

    public void setNoCriteriaWarningMessage(String s)
    {
        noCriteriaWarningMessage = s;
    }

    public boolean isMultiLevelResultsSearch()
    {
        return isAMultiLevelResultsSearch;
    }

    public boolean isMultiLevelResultsSelected()
    {
        return multiLevelResultsSelection;
    }

    public void setMultiLevelResults(boolean flag)
    {
        if(isAMultiLevelResultsSearch)
        {
            multiLevelResultsToggle.setSelected(flag);
            multiLevelResultsSelection = flag;
        }
    }

    public void initializeCriteria(String as[], String as1[])
    {
        initializeCriteria(as, as1, null);
    }

    public void initializeCriteria(String as[], String as1[], TCAdhocQueryClause atcadhocqueryclause[])
    {
        boolean flag = true;
        flag = setCriteriaValues(as, as1, atcadhocqueryclause);
        displayCriteria();
        showLimitSearchSelections();
        if(flag)
        {
            displayICSClassQueryClauses(atcadhocqueryclause);
            if(inClassQueryDialog != null)
                inClassQueryDialog.loadAdhocQueryClauses(atcadhocqueryclause);
        }
        adhocQueryClauses = atcadhocqueryclause;
        requestFocus();
    }

    public void setCriteriaValues(String as[], String as1[])
    {
        setCriteriaValues(as, as1, null);
    }

    private boolean setCriteriaValues(String as[], String as1[], TCAdhocQueryClause atcadhocqueryclause[])
    {
        boolean flag = true;
        int i = -1;
        if(as != null && as.length > 0)
        {
            int j = as.length;
            int l = 0;
            do
            {
                if(l >= j)
                    break;
                if(as[l].equalsIgnoreCase(TCComponentQuery.ADHOC_ICSCLASS_ENTRY))
                {
                    i = l;
                    break;
                }
                l++;
            } while(true);
            flag = adhocQueryClauseChanged(atcadhocqueryclause);
            clear(flag);
        }
        if(as != null && as.length > 0)
        {
            for(int k = 0; k < as.length && k != i; k++)
                setFieldValue(criteriaTable.get(as[k]), as1[k]);

        }
        return flag;
    }

    protected void setDefaultCriteria()
    {
        clear();
        TCQueryClause atcqueryclause[] = clauses;
        int i = atcqueryclause.length;
        for(int j = 0; j < i; j++)
        {
            TCQueryClause tcqueryclause = atcqueryclause[j];
            String s = tcqueryclause.getDefaultValue();
            String s1 = tcqueryclause.getUserEntryNameDisplay();
            setFieldValue(criteriaTable.get(s1), s);
        }

    }

    protected void setDefaultMultiLevelResultsType()
    {
        setMultiLevelResults(true);
    }

    private boolean adhocQueryClauseChanged(TCAdhocQueryClause atcadhocqueryclause[])
    {
        boolean flag = true;
        if(adhocQueryClauses != null && atcadhocqueryclause != null)
        {
            if(adhocQueryClauses.length == atcadhocqueryclause.length)
            {
                flag = false;
                TCAdhocQueryClause atcadhocqueryclause1[] = atcadhocqueryclause;
                int i = atcadhocqueryclause1.length;
                int j = 0;
                do
                {
                    if(j >= i)
                        break;
                    TCAdhocQueryClause tcadhocqueryclause = atcadhocqueryclause1[j];
                    boolean flag1 = false;
                    TCAdhocQueryClause atcadhocqueryclause2[] = adhocQueryClauses;
                    int k = atcadhocqueryclause2.length;
                    int l = 0;
                    do
                    {
                        if(l >= k)
                            break;
                        TCAdhocQueryClause tcadhocqueryclause1 = atcadhocqueryclause2[l];
                        if(tcadhocqueryclause.isSame(tcadhocqueryclause1))
                        {
                            flag1 = true;
                            break;
                        }
                        l++;
                    } while(true);
                    if(!flag1)
                    {
                        flag = true;
                        break;
                    }
                    j++;
                } while(true);
            }
        } else
        if(adhocQueryClauses == null && atcadhocqueryclause == null)
            flag = false;
        return flag;
    }

    public String getEntryValue(String s)
    {
        String s1 = "";
        if(s != null)
        {
            Object obj = criteriaTable.get(s);
            if(obj != null)
                s1 = getFieldDisplayableValue(obj);
        }
        return s1;
    }

    public TCComponentQuery getQuery()
    {
        return currentQuery;
    }

    public void setButtonListener(java.awt.event.ActionListener actionlistener)
    {
        buttonListener = actionlistener;
    }

    protected String getFieldValue(Object obj)
    {
        String s = null;
        if(obj instanceof JTextField)
            s = ((JTextField)obj).getText();
        else
        if(obj instanceof LOVUIComponent)
        {
            Object obj1 = ((LOVUIComponent)obj).getValue();
            if(obj1 != null)
                s = obj1.toString();
        } else
        if(obj instanceof JTextArea)
            s = ((JTextArea)obj).getText();
        else
        if(obj instanceof DateButton)
            s = ((DateButton)obj).getDateString();
        else
        if(obj instanceof JComboBox)
        {
            s = ((JComboBox)obj).getName();
            s = (String)((JComboBox)obj).getSelectedItem();
        }
        return s != null ? s.trim() : null;
    }

    protected String getFieldDisplayableValue(Object obj)
    {
        String s = null;
        if(obj instanceof JTextField)
            s = ((JTextField)obj).getText();
        else
        if(obj instanceof LOVUIComponent)
        {
            String s1 = ((LOVUIComponent)obj).getText();
            if(s1 != null)
                s = s1.toString();
        } else
        if(obj instanceof JTextArea)
            s = ((JTextArea)obj).getText();
        else
        if(obj instanceof DateButton)
            s = ((DateButton)obj).getDateString();
        return s != null ? s.trim() : null;
    }

    protected void setFieldValue(Object obj, String s)
    {
        s = s != null ? s : "";
        if(obj instanceof JTextField)
            ((JTextField)obj).setText(s);
        else
        if(obj instanceof LOVUIComponent)
            ((LOVUIComponent)obj).setSelectedValue(s);
        else
        if(obj instanceof JTextArea)
            ((JTextArea)obj).setText(s);
        else
        if(obj instanceof DateButton)
            ((DateButton)obj).setDate(s);
    }

    public void getEntryValues(List list, List list1)
    {
        list.clear();
        list1.clear();
        Iterator iterator = criteriaTable.entrySet().iterator();
        do
        {
            if(!iterator.hasNext())
                break;
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            String s = (String)entry.getKey();
            Object obj1 = entry.getValue();
            String s2 = getFieldValue(obj1);
            if(s2 != null && s2.length() != 0)
            {
                list.add(s);
                list1.add(s2);
            }
        } while(true);
        if(adhocQueryClauses != null && adhocQueryClauses.length > 0)
        {
            list.add(TCComponentQuery.ADHOC_ICSCLASS_ENTRY);
            list1.add("");
            int i = adhocQueryClauses.length;
            Object obj = null;
            for(int j = 0; j < i; j++)
            {
                String s1 = adhocQueryClauses[j].getDisplayName();
                String s3 = adhocQueryClauses[j].getValue();
                if(s1.length() == 0 || s1.equals("") || s3.length() != 0)
                {
                    String s4 = adhocQueryClauses[j].getClauseName(obj);
                    String s5 = adhocQueryClauses[j].getClauseValue();
                    list.add(s4);
                    list1.add(s5);
                    obj = adhocQueryClauses[j].getClassObject();
                }
            }

        }
    }

    public TCAdhocQueryClause[] getAdhocQueryClauses()
    {
        return adhocQueryClauses;
    }

    public void getICSClassFieldValues(List list)
    {
        if(adhocValueFields != null && adhocValueFields.size() > 0)
        {
            int i = adhocValueFields.size();
            for(int j = 0; j < i; j++)
            {
                Object obj = adhocValueFields.get(j);
                String s = getFieldValue(obj);
                list.add(s);
            }

        }
    }

    public void setExpAppPanel(JPanel jpanel)
    {
        if(expAppPanel == null)
            expAppPanel = jpanel;
    }

    public void populateComponents()
    {
        int i = 0;
        boolean flag = false;
        int l1 = 0;
        if(clipboardComps != null)
            i += clipboardComps.length;
        if(applnComps != null)
            i += applnComps.length;
        if(priorSrchTags != null)
            i += priorSrchTags.length;
        if(refersComps != null)
            i += refersComps.length;
        if(workflowComps != null)
            i += workflowComps.length;
        if(pseTags != null)
            i += pseTags.length;
        inputComponents = new TCComponent[i];
        if(clipboardComps != null)
        {
            int j;
            for(j = 0; j < clipboardComps.length; j++)
                inputComponents[j] = clipboardComps[j];

            l1 = j;
        }
        if(applnComps != null)
        {
            int k;
            for(k = 0; k < applnComps.length; k++)
                inputComponents[k + l1] = applnComps[k];

            l1 = k + l1;
        }
        if(priorSrchTags != null)
        {
            int l;
            for(l = 0; l < priorSrchTags.length; l++)
                try
                {
                    TCComponent tccomponent = currentQuery.getComponentManager().getTCComponent(priorSrchTags[l]);
                    inputComponents[l + l1] = tccomponent;
                }
                catch(Exception exception) { }

            l1 = l + l1;
        }
        if(refersComps != null)
        {
            int i1;
            for(i1 = 0; i1 < refersComps.length; i1++)
                inputComponents[i1 + l1] = refersComps[i1];

            l1 = i1 + l1;
        }
        if(workflowComps != null)
        {
            int j1;
            for(j1 = 0; j1 < workflowComps.length; j1++)
                inputComponents[j1 + l1] = workflowComps[j1];

            l1 = j1 + l1;
        }
        if(pseTags != null)
        {
            int k1;
            for(k1 = 0; k1 < pseTags.length; k1++)
                try
                {
                    TCComponent tccomponent1 = currentQuery.getComponentManager().getTCComponent(pseTags[k1]);
                    inputComponents[k1 + l1] = tccomponent1;
                }
                catch(Exception exception1) { }

            l1 = k1 + l1;
        }
    }

    public TCComponentContextList executeSearch()
    {
        TCComponentContextList tccomponentcontextlist = null;
        String as[];
        String as1[];
        as = initialNames;
        as1 = initialValues;
        if(panelBuiltDone)
        {
            ArrayList arraylist = new ArrayList();
            ArrayList arraylist1 = new ArrayList();
            getEntryValues(arraylist, arraylist1);
            if(arraylist.size() > 0)
                as = (String[])(String[])arraylist.toArray(new String[arraylist.size()]);
            if(arraylist1.size() > 0)
                as1 = (String[])(String[])arraylist1.toArray(new String[arraylist1.size()]);
            storeQueryToCookie(as, as1);
        }
        if(as == null || as1 == null)
        {
            String s;
			try {
				s = currentQuery.getProperty(TCComponentQuery.PROP_QUERY_FLAG);
			
            if(criteriaTable.isEmpty() || hasFixedEntry || s.equals("32"))
            {
                as = new String[0];
                as1 = new String[0];
            } else
            {
                MessageBox.post(parent, appReg.getString("noCriteriaEntered.MESSAGE"), appReg.getString("message.TITLE"), 2);
                return tccomponentcontextlist;
            }
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        try
        {
            if(clipboardComps == null && applnComps == null && priorSrchTags == null && refersComps == null && workflowComps == null && pseTags == null)
            {
                tccomponentcontextlist = currentQuery.getExecuteResultsList(as, as1);
            } else
            {
                populateComponents();
                tccomponentcontextlist = currentQuery.getExecuteResultsList(as, as1, inputComponents);
            }
        }
        catch(TCException tcexception)
        {
            MessageBox.post(AbstractAIFDialog.getParentFrame(this), tcexception);
        }
        return tccomponentcontextlist;
    }

    public TCComponentContextList executeSearchTags()
    {
        TCComponentContextList tccomponentcontextlist = null;
        String as[];
        String as1[];
        as = initialNames;
        as1 = initialValues;
        if(panelBuiltDone)
        {
            ArrayList arraylist = new ArrayList();
            ArrayList arraylist1 = new ArrayList();
            getEntryValues(arraylist, arraylist1);
            if(arraylist.size() > 0)
                as = (String[])(String[])arraylist.toArray(new String[arraylist.size()]);
            if(arraylist1.size() > 0)
                as1 = (String[])(String[])arraylist1.toArray(new String[arraylist1.size()]);
            storeQueryToCookie(as, as1);
        }
        if(as == null || as1 == null)
        {
            String s;
			try {
				s = currentQuery.getProperty(TCComponentQuery.PROP_QUERY_FLAG);
			
            if(criteriaTable.isEmpty() || hasFixedEntry || s.equals("32"))
            {
                as = new String[0];
                as1 = new String[0];
            } else
            {
                MessageBox.post(parent, appReg.getString("noCriteriaEntered.MESSAGE"), appReg.getString("message.TITLE"), 2);
                return tccomponentcontextlist;
            }
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        try
        {
            if(clipboardComps == null && applnComps == null && priorSrchTags == null && refersComps == null && workflowComps == null && pseTags == null)
            {
                tccomponentcontextlist = currentQuery.getExecuteResultsList(as, as1);
            } else
            {
                String as2[] = getLimitListTags();
                tccomponentcontextlist = currentQuery.getExecuteResultsList(as, as1, as2);
            }
        }
        catch(TCException tcexception)
        {
            MessageBox.post(AbstractAIFDialog.getParentFrame(this), tcexception);
        }
        return tccomponentcontextlist;
    }

    public boolean hasTargetList()
    {
        return inputComponents != null;
    }

    public void buildPanel(TCComponentQuery tccomponentquery, String s)
    {
        if(currentQuery == tccomponentquery)
            return;
        String s1 = s != null && s.length() != 0 ? s : appReg.getString("buildQueryForm.MESSAGE");
        currentQuery = tccomponentquery;
        if(session == null)
            session = tccomponentquery.getSession();
        formPanel = new JPanel(new VerticalLayout(3, 4, 4, 4, 4));
        ImageIcon imageicon = r.getImageIcon("time.ICON");
        formPanel.add("top.nobind.center.center", new JLabel(s1, imageicon, 0));
        formPanel.setOpaque(false);
        inclassPanel = new JPanel(new VerticalLayout());
        inclassPanel.setOpaque(false);
        limitsearchPanel = new JPanel(new VerticalLayout());
        limitsearchPanel.setOpaque(false);
        nonCriteriaFormPanel = new JPanel(new VerticalLayout());
        nonCriteriaFormPanel.setOpaque(false);
        nonCriteriaSeparator = new JSeparator();
        add("top.bind", formPanel);
        add("bottom.bind.center.left", nonCriteriaFormPanel);
        add("unbound", inclassPanel);
        add("bottom.bind", limitsearchPanel);
        BuildQueryFormOperation buildqueryformoperation = new BuildQueryFormOperation(initialNames, initialValues, adhocQueryClauses, s1);
        session.queueOperation(buildqueryformoperation);
        Registry registry = Registry.getRegistry("com.teamcenter.rac.classification.common.common");
        try
        {
            ICSClassIcon = registry.getImageIcon("g4mTree.ClassNode.ICON");
            ICSViewIcon = registry.getImageIcon("g4mTree.ViewNode.ICON");
        }
        catch(Exception exception) { }
    }

    public void requestFocus()
    {
        if(criteriaPairs != null && criteriaPairs.length > 0)
        {
            CriteriaPair acriteriapair[] = criteriaPairs;
            int i = acriteriapair.length;
            int j = 0;
            do
            {
                if(j >= i)
                    break;
                CriteriaPair criteriapair = acriteriapair[j];
                JComponent jcomponent = criteriapair.getValueComponent();
                if(jcomponent != null && jcomponent.isVisible())
                {
                    jcomponent.requestFocusInWindow();
                    break;
                }
                j++;
            } while(true);
        }
    }

    public boolean isEmpty()
    {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        getEntryValues(arraylist, arraylist1);
        if(arraylist.isEmpty() || arraylist1.isEmpty())
        {
            String s = null;
            try
            {
                s = currentQuery.getProperty(TCComponentQuery.PROP_QUERY_FLAG);
            }
            catch(TCException tcexception) { }
            return !hasFixedEntry && (s == null || !s.equals("32"));
        } else
        {
            return false;
        }
    }

    public void clear()
    {
        clear(true);
    }

    private void clear(boolean flag)
    {
        Iterator iterator = criteriaTable.values().iterator();
        do
        {
            if(!iterator.hasNext())
                break;
            Object obj = iterator.next();
            if(obj instanceof JTextField)
                ((JTextField)obj).setText("");
            else
            if(obj instanceof LOVUIComponent)
            {
                LOVUIComponent lovuicomponent = (LOVUIComponent)obj;
                lovuicomponent.setText("");
            } else
            if(obj instanceof DateButton)
                ((DateButton)obj).setDate((Date)null);
        } while(true);
        if(flag && adhocValueFields != null && adhocValueFields.size() > 0)
        {
            int i = adhocValueFields.size();
            for(int j = 0; j < i; j++)
            {
                Object obj1 = adhocValueFields.get(j);
                if(obj1 instanceof JTextField)
                {
                    ((JTextField)obj1).setText("");
                    continue;
                }
                if(obj1 instanceof LOVUIComponent)
                {
                    LOVUIComponent lovuicomponent1 = (LOVUIComponent)obj1;
                    lovuicomponent1.setText("");
                }
            }

            inClassQueryDialog.clearValues();
        }
        requestFocus();
    }

    private void displayCriteria()
    {
        SwingUtilities.invokeLater(new Runnable() {

            public void run()
            {
                formPanel.removeAll();
                nonCriteriaFormPanel.removeAll();
                nonCriteriaSeparator.removeAll();
                formPanel.setLayout(new java.awt.GridBagLayout());
                nonCriteriaFormPanel.setLayout(new java.awt.GridBagLayout());
                java.awt.GridBagConstraints gridbagconstraints = new java.awt.GridBagConstraints();
                gridbagconstraints.fill = 2;
                gridbagconstraints.anchor = 22;
                gridbagconstraints.gridy = 0;
                gridbagconstraints.gridx = 0;
                gridbagconstraints.insets = new java.awt.Insets(2, 2, 2, 2);
                int i = 1;
                if(criteriaPairs != null)
                {
                    boolean flag = moreLessLabel.isMoreDisplay();
                    boolean flag1 = false;
                    boolean flag2 = false;
                    boolean flag3 = true;
                    int j = criteriaPairs.length;
                    for(int k = 0; k < j; k++)
                    {
                        boolean flag4 = criteriaPairs[k].isFieldsVisible();
                        flag2 = flag2 || !flag || flag4;
                        flag1 = flag1 || flag4;
                        flag3 = flag3 && flag4;
                        if(!flag || flag4)
                        {
                            JComponent jcomponent = criteriaPairs[k].getNameComponent();
                            jcomponent.setOpaque(false);
                            gridbagconstraints.gridx = 0;
                            gridbagconstraints.anchor = 22;
                            formPanel.add(jcomponent, gridbagconstraints);
                            JComponent jcomponent1 = criteriaPairs[k].getValueComponent();
                            jcomponent1.setVisible(true);
                            gridbagconstraints.gridx = 1;
                            gridbagconstraints.anchor = 21;
                            formPanel.add(jcomponent1, gridbagconstraints);
                            gridbagconstraints.gridy = gridbagconstraints.gridy + 1;
                            i++;
                        } else
                        {
                            criteriaPairs[k].getValueComponent().setVisible(false);
                        }
                    }

                    if(isAMultiLevelResultsSearch)
                    {
                        add("bottom", nonCriteriaSeparator);
                        java.awt.GridBagConstraints gridbagconstraints1 = new java.awt.GridBagConstraints();
                        gridbagconstraints1.fill = 2;
                        gridbagconstraints1.anchor = 21;
                        gridbagconstraints1.gridy = 0;
                        gridbagconstraints1.gridx = 0;
                        gridbagconstraints1.insets = new java.awt.Insets(2, 2, 2, 2);
                        nonCriteriaFormPanel.add(multiLevelResultsLabel, gridbagconstraints1);
                        gridbagconstraints1.gridx = 1;
                        gridbagconstraints1.anchor = 21;
                        multiLevelResultsToggle.setSelected(multiLevelResultsSelection);
                        nonCriteriaFormPanel.add(multiLevelResultsToggle, gridbagconstraints1);
                    }
                    if(!flag1 && moreLessLabel.isMoreDisplay())
                    {
                        moreLessLabel.changeStatus();
                        displayCriteria();
                    }
                    if(flag1 && !flag3)
                    {
                        gridbagconstraints.gridx = 0;
                        gridbagconstraints.anchor = 21;
                        gridbagconstraints.fill = 0;
                        formPanel.add(moreLessLabel, gridbagconstraints);
                    }
                }
                if(i == 1)
                {
                    formPanel.setLayout(new VerticalLayout(3, 4, 4, 4, 4));
                    formPanel.add("top.nobind", new MLabel(noCriteriaWarningMessage));
                }
                formPanel.setMaximumSize(formPanel.getSize());
                requestFocus();
                revalidate();
                repaint();
            }

            final NOVSMDLSearchCriteriaPanel this$0 = NOVSMDLSearchCriteriaPanel.this;
        }
);
    }

    public static void setTimePeriod(int i)
    {
        if(i > 0)
            timePeriod = i * 24 * 3600 * 1000;
    }

    public void storeQueryToCookie()
    {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        getEntryValues(arraylist, arraylist1);
        String as[] = (String[])(String[])arraylist.toArray(new String[arraylist.size()]);
        String as1[] = (String[])(String[])arraylist1.toArray(new String[arraylist1.size()]);
        storeQueryToCookie(as, as1);
    }

    public void storeQueryToCookie(String as[], String as1[])
    {
        if(as == null || as1 == null)
            return;
        int i = as.length;
        int j = -1;
        int k = 0;
        do
        {
            if(k >= i)
                break;
            if(as[k].equalsIgnoreCase(TCComponentQuery.ADHOC_ICSCLASS_ENTRY))
            {
                j = k;
                break;
            }
            k++;
        } while(true);
        String as2[] = as;
        String as3[] = as1;
        if(j >= 0)
        {
            as2 = new String[j];
            as3 = new String[j];
            System.arraycopy(as, 0, as2, 0, j);
            System.arraycopy(as1, 0, as3, 0, j);
        }
        if(as2 != null)
        {
            cookie.setStringArray((new StringBuilder()).append(currentQueryString).append(LATEST).toString(), as2);
            cookie.setStringArray((new StringBuilder()).append(currentQueryString).append(VALUE).toString(), as3);
            String s = (new StringBuilder()).append(currentQueryString).append(TOTAL).toString();
            cookie.setString(s, cookie.getNumber(s) + 1);
            String as4[] = as2;
            int l = as4.length;
            for(int i1 = 0; i1 < l; i1++)
            {
                String s1 = as4[i1];
                if(s1 != null && s1.trim().length() > 0)
                {
                    String s2 = (new StringBuilder()).append(currentQueryString).append(".").append(s1).append(TOTAL).toString();
                    int j1 = cookie.getNumber(s2);
                    cookie.setString(s2, j1 + 1);
                }
            }

        }
    }

    public static void closeCookie()
    {
        try
        {
            cookie.close();
        }
        catch(Exception exception) { }
    }

    public TCQueryClause[] getQueryClauses()
    {
        return clauses;
    }

    public void showICSClassQueryDialog(java.awt.Frame frame)
    {
        try
        {
            if(inClassQueryDialog == null)
            {
                String s = appReg.getString("query.Advanced");
                inClassQueryDialog = new ICSClassQueryDialog(s, frame, session, this);
                inClassQueryDialog.addICSQueryChangeListener(this);
            } else
            {
                if(adhocValueFields != null)
                    inClassQueryDialog.updateICSClassQueryClauses(adhocEntryNames, null);
                inClassQueryDialog.reinitialize();
            }
            inClassQueryDialog.setVisible(true);
            inClassQueryDialog.requestFocus();
        }
        catch(Exception exception)
        {
            MessageBox.post(exception);
        }
    }

    public void updateICSClassQueryClauses()
    {
        if(inClassQueryDialog != null)
            inClassQueryDialog.updateICSClassQueryClauses(adhocEntryNames, null);
        else
            adhocQueryClauses = null;
    }

    private void displayICSClassQueryClauses(TCAdhocQueryClause atcadhocqueryclause[])
    {
        if(adhocValueFields == null)
        {
            adhocValueFields = new ArrayList();
            adhocEntryNames = new ArrayList();
        } else
        {
            adhocValueFields.clear();
            adhocEntryNames.clear();
        }
        inclassPanel.removeAll();
        inclassPanel.setLayout(new VerticalLayout());
        if(atcadhocqueryclause != null && atcadhocqueryclause.length > 0)
        {
            int i = 1;
            TCICSClass tcicsclass = null;
            JPanel jpanel = null;
            JPanel jpanel1 = null;
            int j = 0;
            boolean flag = true;
            TCAdhocQueryClause atcadhocqueryclause1[] = atcadhocqueryclause;
            int k = atcadhocqueryclause1.length;
            for(int l = 0; l < k; l++)
            {
                TCAdhocQueryClause tcadhocqueryclause = atcadhocqueryclause1[l];
                TCICSClass tcicsclass1 = (TCICSClass)tcadhocqueryclause.getClassObject();
                if(flag || tcicsclass != tcicsclass1)
                {
                    flag = false;
                    jpanel = new JPanel(new VerticalLayout());
                    jpanel.setOpaque(false);
                    String s = tcicsclass1.askClassType();
                    JLabel jlabel1;
                    ICSClassQueryBanner icsclassquerybanner;
                    if(s.equals("Class"))
                    {
                        jlabel1 = new JLabel(tcicsclass1.toString(), ICSClassIcon, 2);
                        icsclassquerybanner = new ICSClassQueryBanner(j, tcicsclass1.toString(), ICSClassIcon);
                    } else
                    if(s.equals("View"))
                    {
                        jlabel1 = new JLabel(tcicsclass1.toString(), ICSViewIcon, 2);
                        icsclassquerybanner = new ICSClassQueryBanner(j, tcicsclass1.toString(), ICSViewIcon);
                    } else
                    {
                        jlabel1 = new JLabel(tcicsclass1.toString());
                        icsclassquerybanner = new ICSClassQueryBanner(j, tcicsclass1.toString(), ICSClassIcon);
                    }
                    j++;
                    icsclassquerybanner.setCloseListener(new java.awt.event.ActionListener() {

                        public void actionPerformed(java.awt.event.ActionEvent actionevent)
                        {
                            ICSClassQueryBanner icsclassquerybanner1 = (ICSClassQueryBanner)actionevent.getSource();
                            removeICSClassQuery(icsclassquerybanner1.getIndex());
                        }

                        final NOVSMDLSearchCriteriaPanel this$0= NOVSMDLSearchCriteriaPanel.this;
                    }
);
                    jpanel.add("top.bind", new Separator());
                    JPanel jpanel2 = new JPanel(new HorizontalLayout(3, 0, 0, 4, 4));
                    jpanel2.setOpaque(false);
                    jpanel2.add("left.nobind", jlabel1);
                    jpanel2.add("right.bind", icsclassquerybanner);
                    jpanel.add("top.bind", jpanel2);
                    inclassPanel.add("top.bind", jpanel);
                    jpanel1 = null;
                    tcicsclass = tcicsclass1;
                    i = 1;
                }
                String s1 = tcadhocqueryclause.getDisplayName();
                if(s1.length() == 0 || s1.equals(""))
                    continue;
                if(jpanel1 == null)
                {
                    jpanel1 = new JPanel(new PropertyLayout(3, 3, 0, 0, 4, 4));
                    jpanel1.setOpaque(false);
                    jpanel.add("top.bind", jpanel1);
                }
                JLabel jlabel;
                if(displayDetailsFlag)
                    jlabel = new JLabel((new StringBuilder()).append(tcadhocqueryclause.getLogicalOperator()).append(" ").append(tcadhocqueryclause.getAttributeName()).append(" ").append(tcadhocqueryclause.getMathOperator()).toString());
                else
                    jlabel = new JLabel((new StringBuilder()).append(s1).append(":").toString());
                String s2 = tcadhocqueryclause.getAttributeName();
                int i1 = tcicsclass1.getKeyLOV(s2);
                Object obj;
                if(i1 < 0)
                {
                    if(lovComboLimit < 0)
                        lovComboLimit = getLOVComboLimit();
                    final LOVUIComponent lovPopup = new LOVUIComponent(session, null, i1, lovComboLimit);
                    lovPopup.setEditable(true);
                    lovPopup.addKeyListener(new java.awt.event.KeyAdapter() {

                        public void keyPressed(java.awt.event.KeyEvent keyevent)
                        {
                            int j1 = keyevent.getKeyCode();
                            if(j1 == 10)
                                buttonListener.actionPerformed(new java.awt.event.ActionEvent(lovPopup, 1, ""));
                        }

                        final LOVUIComponent val$lovPopup = lovPopup;
                        final NOVSMDLSearchCriteriaPanel this$0 =NOVSMDLSearchCriteriaPanel.this;

                    }
);
                    final String entryValue = tcadhocqueryclause.getValue();
                    if(entryValue.length() != 0)
                        SwingUtilities.invokeLater(new Runnable() {

                            public void run()
                            {
                                lovPopup.setSelectedValue(entryValue);
                            }

                            final LOVUIComponent val$lovPopup =lovPopup;
                            final String val$entryValue=entryValue;
                            final NOVSMDLSearchCriteriaPanel this$0 =  NOVSMDLSearchCriteriaPanel.this;;

            
                        }
);
                    obj = lovPopup;
                } else
                {
                    JTextField jtextfield = new JTextField(12);
                    jtextfield.setText(tcadhocqueryclause.getValue());
                    jtextfield.addActionListener(new java.awt.event.ActionListener() {

                        public void actionPerformed(java.awt.event.ActionEvent actionevent)
                        {
                            buttonListener.actionPerformed(actionevent);
                        }

                        final NOVSMDLSearchCriteriaPanel this$0 =  NOVSMDLSearchCriteriaPanel.this;;
                    }
);
                    obj = jtextfield;
                }
                String s3 = (new StringBuilder()).append(i).append(".1.RIGHT.CENTER.PREFERRED.PREFERRED").toString();
                jpanel1.add(s3, jlabel);
                s3 = (new StringBuilder()).append(i).append(".2.LEFT.CENTER").toString();
                jpanel1.add(s3, ((java.awt.Component) (obj)));
                adhocValueFields.add(obj);
                adhocEntryNames.add(s1);
                i++;
            }

        }
        requestFocus();
        revalidate();
        repaint();
    }

    private void removeICSClassQuery(int i)
    {
        updateICSClassQueryClauses();
        if(i >= 0)
            inClassQueryDialog.removeICSClassQuery(i);
    }

    public void showLimitSearchSelections()
    {
        if(inClassQueryDialog != null && inClassQueryDialog.getPseButton() != null && inClassQueryDialog.getPseButton().limList != null)
        {
            String as[] = inClassQueryDialog.getPseTags();
            as = new String[inClassQueryDialog.getPseButton().limList.size()];
            int i = 0;
            for(Iterator iterator = inClassQueryDialog.getPseButton().limList.iterator(); iterator.hasNext();)
            {
                Object obj3 = iterator.next();
                as[i++] = (String)obj3;
            }

            inClassQueryDialog.setPseTags(as);
            repaint();
        }
        limitsearchPanel.removeAll();
        limitsearchPanel.setLayout(new VerticalLayout());
        Object obj = null;
        Object obj1 = null;
        Object obj2 = null;
        Object obj4 = null;
        Object obj5 = null;
        Object obj6 = null;
        if(inClassQueryDialog != null && inClassQueryDialog.getClipboardComps() != null)
        {
            String s = (new StringBuilder()).append(" ").append(appReg.getString("query.fromClipboard")).append(" : ").append(inClassQueryDialog.getClipboardComps().length).append(" ").append(appReg.getString("query.object")).toString();
            JPanel jpanel = new JPanel(new VerticalLayout());
            jpanel.setOpaque(false);
            JPanel jpanel6 = createPanel(0, s, jpanel);
            jpanel.add("top.bind", new Separator());
            jpanel.add("top.bind", jpanel6);
            limitsearchPanel.add("top.bind", jpanel);
        }
        if(inClassQueryDialog != null && inClassQueryDialog.getPriorSrchTags() != null)
        {
            String s1 = (new StringBuilder()).append(" ").append(appReg.getString("query.fromPrior")).append(" : ").append(inClassQueryDialog.getPriorSrchTags().length).append(" ").append(appReg.getString("query.object")).toString();
            JPanel jpanel1 = new JPanel(new VerticalLayout());
            jpanel1.setOpaque(false);
            JPanel jpanel7 = createPanel(1, s1, jpanel1);
            jpanel1.add("top.bind", new Separator());
            jpanel1.add("top.bind", jpanel7);
            limitsearchPanel.add("top.bind", jpanel1);
        }
        if(inClassQueryDialog != null && inClassQueryDialog.getRefersComps() != null)
        {
            String s2 = (new StringBuilder()).append(" ").append(appReg.getString("query.fromReferencers")).append(" : ").append(inClassQueryDialog.getRefersComps().length).append(" ").append(appReg.getString("query.object")).toString();
            JPanel jpanel2 = new JPanel(new VerticalLayout());
            jpanel2.setOpaque(false);
            JPanel jpanel8 = createPanel(2, s2, jpanel2);
            jpanel2.add("top.bind", new Separator());
            jpanel2.add("top.bind", jpanel8);
            limitsearchPanel.add("top.bind", jpanel2);
        }
        if(inClassQueryDialog != null && inClassQueryDialog.getWorkflowComps() != null)
        {
            String s3 = (new StringBuilder()).append(" ").append(appReg.getString("query.fromWorkflow")).append(" : ").append(inClassQueryDialog.getWorkflowComps().length).append(" ").append(appReg.getString("query.object")).toString();
            JPanel jpanel3 = new JPanel(new VerticalLayout());
            jpanel3.setOpaque(false);
            JPanel jpanel9 = createPanel(3, s3, jpanel3);
            jpanel3.add("top.bind", new Separator());
            jpanel3.add("top.bind", jpanel9);
            limitsearchPanel.add("top.bind", jpanel3);
        }
        if(inClassQueryDialog != null && inClassQueryDialog.getPseTags() != null)
        {
            String s4 = (new StringBuilder()).append(" ").append(appReg.getString("query.fromPse")).append(" : ").append(inClassQueryDialog.getPseTags().length).append(" ").append(appReg.getString("query.object")).toString();
            JPanel jpanel4 = new JPanel(new VerticalLayout());
            jpanel4.setOpaque(false);
            JPanel jpanel10 = createPanel(4, s4, jpanel4);
            jpanel4.add("top.bind", new Separator());
            jpanel4.add("top.bind", jpanel10);
            limitsearchPanel.add("top.bind", jpanel4);
        }
        if(inClassQueryDialog != null && inClassQueryDialog.getApplnComps() != null)
        {
            String s5 = (new StringBuilder()).append(" ").append(appReg.getString("query.fromAppl")).append(" : ").append(inClassQueryDialog.getApplnComps().length).append(" ").append(appReg.getString("query.object")).toString();
            JPanel jpanel5 = new JPanel(new VerticalLayout());
            jpanel5.setOpaque(false);
            JPanel jpanel11 = createPanel(5, s5, jpanel5);
            jpanel5.add("top.bind", new Separator());
            jpanel5.add("top.bind", jpanel11);
            limitsearchPanel.add("top.bind", jpanel5);
        }
        requestFocus();
        revalidate();
        repaint();
    }

    private JPanel createPanel(int i, String s, JPanel jpanel)
    {
        JLabel jlabel = new JLabel(s, 2);
        JPanel jpanel1 = new JPanel(new HorizontalLayout(3, 0, 0, 4, 4));
        jpanel1.setOpaque(false);
        ICSClassQueryBanner icsclassquerybanner = new ICSClassQueryBanner(i, jpanel, s, ICSClassIcon);
        icsclassquerybanner.setCloseListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent actionevent)
            {
                ICSClassQueryBanner icsclassquerybanner1 = (ICSClassQueryBanner)actionevent.getSource();
                limitsearchPanel.remove(icsclassquerybanner1.getICSClassPanel());
                revalidate();
                repaint();
                if(icsclassquerybanner1.getIndex() == 0)
                {
                    inClassQueryDialog.clearClipboardComps();
                    clipboardComps = null;
                }
                if(icsclassquerybanner1.getIndex() == 1)
                {
                    inClassQueryDialog.clearPriorSrchComps();
                    priorSrchComps = null;
                }
                if(icsclassquerybanner1.getIndex() == 2)
                {
                    inClassQueryDialog.clearReferenceComps();
                    refersComps = null;
                }
                if(icsclassquerybanner1.getIndex() == 3)
                {
                    inClassQueryDialog.clearWorkflowComps();
                    workflowComps = null;
                }
                if(icsclassquerybanner1.getIndex() == 4)
                {
                    inClassQueryDialog.clearPSEComps();
                    pseComps = null;
                }
                if(icsclassquerybanner1.getIndex() == 5)
                {
                    inClassQueryDialog.clearApplnComps();
                    applnComps = null;
                }
                if(inClassQueryDialog.allEmptyComponent())
                {
                    inClassQueryDialog.clearInputComps();
                    inputComponents = null;
                }
                repaint();
            }

        }
);
        jpanel1.add("left.nobind", jlabel);
        jpanel1.add("right.bind", icsclassquerybanner);
        return jpanel1;
    }

    private int getLOVComboLimit()
    {
        int i = 0;
        if(session != null)
            try
            {
                String s = session.getPreferenceService().getString(1, "lovPopupButtonThreshHold");
                if(s != null)
                    i = Integer.parseInt(s);
            }
            catch(Exception exception) { }
        return i;
    }

    private String generateToolTipText(TCQueryClause tcqueryclause)
    {
        try
        {
            TCPreferenceService tcpreferenceservice = session.getPreferenceService();
            String s = null;
            boolean flag = false;
            switch(tcqueryclause.getAttributeType() % 1000)
            {
            case 2: // '\002'
                s = appReg.getString("QTip.date");
                break;

            case 1: // '\001'
                s = appReg.getString("QTip.char");
                break;

            case 5: // '\005'
            case 7: // '\007'
                s = appReg.getString("QTip.int");
                break;

            case 3: // '\003'
            case 4: // '\004'
                s = appReg.getString("QTip.float");
                break;

            case 8: // '\b'
                s = appReg.getString("QTip.string");
                flag = true;
                break;

            case 6: // '\006'
                s = appReg.getString("QTip.logical");
                break;

            default:
                s = " ";
                break;
            }
            String s1 = "";
            if(flag)
            {
                String s2 = tcpreferenceservice.getString(0, "TC_pattern_match_style");
                if(s2.equals("1"))
                    s1 = (new StringBuilder()).append(appReg.getString("QTip.wildcardHead")).append(appReg.getString("QTip.wildcard1")).toString();
                else
                if(s2.equals("2"))
                    s1 = (new StringBuilder()).append(appReg.getString("QTip.wildcardHead")).append(appReg.getString("QTip.wildcard2")).toString();
                else
                if(s2.equals("3"))
                    s1 = (new StringBuilder()).append(appReg.getString("QTip.wildcardHead")).append(appReg.getString("QTip.wildcard3")).toString();
            } else
            {
                s1 = appReg.getString("QTip.noWildCard");
            }
            String s3 = null;
            if(tcqueryclause.getMathOperator().compareTo("=") != 0 && tcqueryclause.getMathOperator().compareTo("!=") != 0)
            {
                s3 = appReg.getString("QTip.noMulti-OP");
            } else
            {
                String s4 = tcpreferenceservice.getString(0, "WSOM_find_list_separator");
                if(s4.length() > 0)
                    s3 = (new StringBuilder()).append(appReg.getString("QTip.Multi")).append(" ").append(s4).append(" ").append(appReg.getString("QTip.Multi2")).toString();
                else
                    s3 = appReg.getString("QTip.noMulti-SEP");
            }
            String s5 = "";
            if(tcqueryclause.getLOV() != null)
                s5 = appReg.getString("QTip.lov");
            String s6 = null;
            if(s5.length() == 0)
                s6 = (new StringBuilder()).append(s).append("\n").append(s1).append("\n").append(s3).toString();
            else
                s6 = (new StringBuilder()).append(s).append("\n").append(s5).append("\n").append(s1).append("\n").append(s3).toString();
            return Utilities.getToolTipHTML(s6, 2);
        }
        catch(Exception exception)
        {
            return "";
        }
    }

    private void pasteText(java.awt.Component component, boolean flag)
    {
        try
        {
            String s = session.getPreferenceService().getString(0, "WSOM_find_list_separator");
            java.awt.datatransfer.Transferable transferable = java.awt.Toolkit.getDefaultToolkit().getSystemClipboard().getContents(this);
            String s1 = (String)transferable.getTransferData(java.awt.datatransfer.DataFlavor.stringFlavor);
            String s2 = null;
            StringTokenizer stringtokenizer = new StringTokenizer(s1, "\t");
            if(stringtokenizer.hasMoreTokens())
                s2 = stringtokenizer.nextToken();
            while(stringtokenizer.hasMoreTokens()) 
            {
                s2 = s2.concat(s);
                s2 = s2.concat(stringtokenizer.nextToken());
            }
            if(component instanceof JTextField)
            {
                JTextField jtextfield = (JTextField)component;
                if(flag)
                {
                    String s3 = ((JTextField)component).getText();
                    if(s3.length() > 0)
                        jtextfield.setText((new StringBuilder()).append(s3).append(s).append(s2).toString());
                    else
                        jtextfield.setText(s2);
                } else
                {
                    jtextfield.setText(s2);
                }
            } else
            if(component instanceof LOVUIComponent)
            {
                LOVUIComponent lovuicomponent = (LOVUIComponent)component;
                if(flag)
                {
                    String s4 = lovuicomponent.getText();
                    if(s4.length() > 0)
                        lovuicomponent.setText((new StringBuilder()).append(s4).append(s).append(s2).toString());
                    else
                        lovuicomponent.setText(s2);
                } else
                {
                    lovuicomponent.setText(s2);
                }
            }
        }
        catch(Exception exception) { }
    }

    public void updateQueryClauseForm(String as[], String as1[], TCAdhocQueryClause atcadhocqueryclause[])
    {
        String s = appReg.getString("buildQueryForm.MESSAGE");
        BuildQueryFormOperation buildqueryformoperation = new BuildQueryFormOperation(as, as1, atcadhocqueryclause, s);
        session.queueOperation(buildqueryformoperation);
    }

    public int getLimitListClipboardCnt()
    {
        if(clipboardComps != null)
            return clipboardComps.length;
        else
            return 0;
    }

    public TCComponent[] getLimitListClipboard()
    {
        return clipboardComps;
    }

    public void setLimitListClipboard(TCComponent atccomponent[])
    {
        clipboardComps = atccomponent;
        if(inClassQueryDialog != null)
            inClassQueryDialog.setLimitListClipboard(atccomponent);
    }

    public int getLimitListApplnsCnt()
    {
        if(applnComps != null)
            return applnComps.length;
        else
            return 0;
    }

    public TCComponent[] getLimitListApplns()
    {
        return applnComps;
    }

    public void setLimitListApplns(TCComponent atccomponent[])
    {
        applnComps = atccomponent;
        if(inClassQueryDialog != null)
            inClassQueryDialog.setLimitListApplns(atccomponent);
    }

    public int getLimitListPriorSrchCnt()
    {
        if(priorSrchTags != null)
            return priorSrchTags.length;
        else
            return 0;
    }

    public String[] getLimitListPriorSrchTags()
    {
        return priorSrchTags;
    }

    public TCComponent[] getLimitListPriorSrch()
    {
        priorSrchComps = new TCComponent[priorSrchTags.length];
        for(int i = 0; i < priorSrchTags.length; i++)
            try
            {
                TCComponent tccomponent = currentQuery.getComponentManager().getTCComponent(priorSrchTags[i]);
                priorSrchComps[i] = tccomponent;
            }
            catch(Exception exception) { }

        return priorSrchComps;
    }

    public void setLimitListPriorSrch(TCComponent atccomponent[])
    {
        priorSrchComps = atccomponent;
        priorSrchTags = new String[atccomponent.length];
        for(int i = 0; i < pseComps.length; i++)
            priorSrchTags[i] = atccomponent[i].getUid();

    }

    public void setLimitListPriorSrch(String as[])
    {
        priorSrchTags = as;
        if(inClassQueryDialog != null)
            inClassQueryDialog.setLimitListPriorSrch(as);
    }

    public int getLimitListRefersCnt()
    {
        if(refersComps != null)
            return refersComps.length;
        else
            return 0;
    }

    public TCComponent[] getLimitListRefers()
    {
        return refersComps;
    }

    public void setLimitListRefers(TCComponent atccomponent[])
    {
        refersComps = atccomponent;
        if(inClassQueryDialog != null)
            inClassQueryDialog.setLimitListRefers(atccomponent);
    }

    public int getLimitListWorkflowCnt()
    {
        if(workflowComps != null)
            return workflowComps.length;
        else
            return 0;
    }

    public TCComponent[] getLimitListWorkflow()
    {
        return workflowComps;
    }

    public void setLimitListWorkflow(TCComponent atccomponent[])
    {
        workflowComps = atccomponent;
        if(inClassQueryDialog != null)
            inClassQueryDialog.setLimitListWorkflow(atccomponent);
    }

    public int getLimitListPSECnt()
    {
        if(pseTags != null)
            return pseTags.length;
        else
            return 0;
    }

    public TCComponent[] getLimitListPSE()
    {
        pseComps = new TCComponent[pseTags.length];
        for(int i = 0; i < pseTags.length; i++)
            try
            {
                TCComponent tccomponent = currentQuery.getComponentManager().getTCComponent(pseTags[i]);
                pseComps[i] = tccomponent;
            }
            catch(Exception exception) { }

        return pseComps;
    }

    public String[] getLimitListPSETags()
    {
        return pseTags;
    }

    public void setLimitListPSE(TCComponent atccomponent[])
    {
        pseComps = atccomponent;
        pseTags = new String[pseComps.length];
        for(int i = 0; i < pseComps.length; i++)
            pseTags[i] = pseComps[i].getUid();

    }

    public void setLimitListPSE(String as[])
    {
        pseTags = as;
        if(inClassQueryDialog != null)
            inClassQueryDialog.setLimitListPSE(as);
    }

    public int getLimitListInputCnt()
    {
        if(inputComponents != null)
            return inputComponents.length;
        else
            return 0;
    }

    public TCComponent[] getLimitList()
    {
        return inputComponents;
    }

    public String[] getLimitListTags()
    {
        int i = getLimitListClipboardCnt() + getLimitListApplnsCnt() + getLimitListPriorSrchCnt() + getLimitListRefersCnt() + getLimitListWorkflowCnt() + getLimitListPSECnt();
        String as[] = new String[i];
        int j = 0;
        if(clipboardComps != null)
        {
            TCComponent atccomponent[] = clipboardComps;
            int k = atccomponent.length;
            for(int i2 = 0; i2 < k; i2++)
            {
                TCComponent tccomponent = atccomponent[i2];
                as[j] = tccomponent.getUid();
                j++;
            }

        }
        if(applnComps != null)
        {
            TCComponent atccomponent1[] = applnComps;
            int l = atccomponent1.length;
            for(int j2 = 0; j2 < l; j2++)
            {
                TCComponent tccomponent1 = atccomponent1[j2];
                as[j] = tccomponent1.getUid();
                j++;
            }

        }
        if(priorSrchTags != null)
        {
            String as1[] = priorSrchTags;
            int i1 = as1.length;
            for(int k2 = 0; k2 < i1; k2++)
            {
                String s = as1[k2];
                as[j] = s;
                j++;
            }

        }
        if(refersComps != null)
        {
            TCComponent atccomponent2[] = refersComps;
            int j1 = atccomponent2.length;
            for(int l2 = 0; l2 < j1; l2++)
            {
                TCComponent tccomponent2 = atccomponent2[l2];
                as[j] = tccomponent2.getUid();
                j++;
            }

        }
        if(workflowComps != null)
        {
            TCComponent atccomponent3[] = workflowComps;
            int k1 = atccomponent3.length;
            for(int i3 = 0; i3 < k1; i3++)
            {
                TCComponent tccomponent3 = atccomponent3[i3];
                as[j] = tccomponent3.getUid();
                j++;
            }

        }
        if(pseTags != null)
        {
            String as2[] = pseTags;
            int l1 = as2.length;
            for(int j3 = 0; j3 < l1; j3++)
            {
                String s1 = as2[j3];
                as[j] = s1;
                j++;
            }

        }
        return as;
    }

    public int getLimitListTagCnt()
    {
        int i = getLimitListClipboardCnt() + getLimitListApplnsCnt() + getLimitListPriorSrchCnt() + getLimitListRefersCnt() + getLimitListWorkflowCnt() + getLimitListPSECnt();
        return i;
    }

    public JComponent[] getValueComponents()
    {
        JComponent ajcomponent[] = new JComponent[criteriaPairs.length];
        for(int i = 0; i < criteriaPairs.length; i++)
            ajcomponent[i] = criteriaPairs[i].getValueComponent();

        return ajcomponent;
    }

    public void updateInput(AIFInputEvent aifinputevent)
    {
        String s = aifinputevent.getProperty().toString();
        Object obj = aifinputevent.getInputValue();
        Object obj1 = null;
        TCComponent tccomponent = (TCComponent)aifinputevent.getComponent();
        if(tccomponent.equals(currentQuery))
        {
            Object obj2 = criteriaTable.get(s);
            if(obj2 != null)
            {
                String s1;
                if(obj != null && (obj instanceof List))
                {
                    if(((List)obj).size() > 0)
                        s1 = ((List)obj).get(0).toString();
                    else
                        s1 = "";
                } else
                {
                    s1 = obj == null ? "" : obj.toString();
                }
                setFieldValue(obj2, s1);
            }
        }
    }

    public void paintComponent(java.awt.Graphics g)
    {
        if(moreLessLabel != null && !moreLessLabel.isMoreDisplay())
            Painter.paintVerticalGradient(this, g, java.awt.Color.white, new java.awt.Color(232, 242, 255));
        super.paintComponent(g);
    }

    public void adhocQueryChanged(TCAdhocQueryClause atcadhocqueryclause[])
    {
        adhocQueryClauses = atcadhocqueryclause;
        displayICSClassQueryClauses(atcadhocqueryclause);
        showLimitSearchSelections();
        applnComps = inClassQueryDialog.getApplnComps();
        clipboardComps = inClassQueryDialog.getClipboardComps();
        inputComponents = inClassQueryDialog.getInputComponents();
        priorSrchComps = inClassQueryDialog.getLimitListPriorSrch();
        pseComps = inClassQueryDialog.getLimitListPSE();
        refersComps = inClassQueryDialog.getLimitListRefers();
        workflowComps = inClassQueryDialog.getLimitListWorkflow();
        priorSrchTags = inClassQueryDialog.getPriorSrchTags();
        pseTags = inClassQueryDialog.getPseTags();
        revalidate();
    }

    public List getApplications()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add(0, "               ");
        List list = ApplicationMenuHolder.getApplications();
        if(list != null && !list.isEmpty())
            arraylist.addAll(list);
        return arraylist;
    }

    public List getComponentsInApplications(Object aobj[])
    {
        ArrayList arraylist = new ArrayList();
        Object aobj1[] = aobj;
        int i = aobj1.length;
        for(int j = 0; j < i; j++)
        {
            Object obj = aobj1[j];
            if(!(obj instanceof ApplicationMenuHolder))
                continue;
            ApplicationMenuHolder applicationmenuholder = (ApplicationMenuHolder)obj;
            com.teamcenter.rac.aif.kernel.InterfaceAIFComponent ainterfaceaifcomponent[] = applicationmenuholder.getTargetComponents();
            if(ainterfaceaifcomponent == null)
                continue;
            com.teamcenter.rac.aif.kernel.InterfaceAIFComponent ainterfaceaifcomponent1[] = ainterfaceaifcomponent;
            int k = ainterfaceaifcomponent1.length;
            for(int l = 0; l < k; l++)
            {
                com.teamcenter.rac.aif.kernel.InterfaceAIFComponent interfaceaifcomponent = ainterfaceaifcomponent1[l];
                arraylist.add(interfaceaifcomponent);
            }

        }

        return arraylist;
    }

    //@IMP = Need to remove comment this portion once Binder Panel is added
    public List getComponentsInPriorSearches(Object aobj[])
    {
        ArrayList arraylist = new ArrayList();
        Object aobj1[] = aobj;
        int i = aobj1.length;
		return arraylist;
    }

    public List getComponentsReferenced(Object aobj[])
    {
        ArrayList arraylist = new ArrayList();
        Object aobj1[] = aobj;
        int i = aobj1.length;
        return arraylist;
    }

    public List getPriorSearches()
    {
        TCDataPaneManager tcdatapanemanager = null;
        try
        {
            tcdatapanemanager = (TCDataPaneManager)Utilities.invokeMethod(expAppPanel, "getDataPaneMgr", new Object[0]);
        }
        catch(Exception exception)
        {
            logger.error(exception.getClass().getName(), exception);
        }
        int i = tcdatapanemanager.getPaneCount();
        ArrayList arraylist = new ArrayList();
        arraylist.add("               ");

        return arraylist;
    }

    public List getReferencedSearches()
    {
        TCDataPaneManager tcdatapanemanager = null;
        try
        {
            tcdatapanemanager = (TCDataPaneManager)Utilities.invokeMethod(expAppPanel, "getDataPaneMgr", new Object[0]);
        }
        catch(Exception exception)
        {
            logger.error(exception.getClass().getName(), exception);
        }
        int i = tcdatapanemanager.getPaneCount();
        ArrayList arraylist = new ArrayList();
        arraylist.add("               ");
        return arraylist;
    }

    private static final Logger logger = Logger.getLogger("com/nov/rac/smdl/common/NOVSMDLSearchCriteriaPanel");
    public static final String FIND_LIST_SEPERATOR = "WSOM_find_list_separator";
    public static final String PATTERN_MATCH_STYLE_PREF = "TC_pattern_match_style";
    public static int lovComboLimit = -1;
    protected static String TOTAL = ".TOTAL";
    protected static String LATEST = ".LATEST";
    protected static String VALUE = ".VALUE";
    protected static long timePeriod = 0x337f9800L;
    protected static Cookie cookie = Cookie.getCookie("SearchCriteria", true);
    public static String QUERY_PROPS[] = {
        "query_name", "query_desc", "query_class"
    };
    protected java.awt.Frame parent;
    protected Registry appReg;
    public TCSession session;
    protected TCComponentQuery currentQuery;
    protected String currentQueryString;
    private final Map criteriaTable;
    protected boolean displayDetailsFlag;
    protected String initialNames[];
    protected String initialValues[];
    protected java.awt.event.ActionListener buttonListener;
    protected CriteriaPair criteriaPairs[];
    protected Registry r;
    protected MoreLessLabel moreLessLabel;
    private JCheckBox multiLevelResultsToggle;
    private JLabel multiLevelResultsLabel;
    private boolean multiLevelResultsSelection;
    private boolean isAMultiLevelResultsSearch;
    private TCQueryClause clauses[];
    private JPanel formPanel;
    private JPanel nonCriteriaFormPanel;
    private JSeparator nonCriteriaSeparator;
    private JPanel inclassPanel;
    private JPanel limitsearchPanel;
    private com.teamcenter.rac.explorer.common.ICSClassQueryDialog inClassQueryDialog;
    private ImageIcon ICSClassIcon;
    private ImageIcon ICSViewIcon;
    private List adhocEntryNames;
    private List adhocValueFields;
    private TCAdhocQueryClause adhocQueryClauses[];
    protected boolean panelBuiltDone;
    private boolean hasFixedEntry;
    protected InterfaceAIFOperationListener opListener;
    private JPopupMenu pasteMenu;
    private JMenuItem replaceText;
    private String noCriteriaWarningMessage;
    private JMenuItem appendToText;
    private TCComponent clipboardComps[];
    private TCComponent applnComps[];
    private TCComponent priorSrchComps[];
    private TCComponent refersComps[];
    private TCComponent workflowComps[];
    private TCComponent pseComps[];
    private TCComponent inputComponents[];
    private String priorSrchTags[];
    private String pseTags[];
    protected JPanel expAppPanel;
    protected TCDataPaneManager expTabPane;
}




























