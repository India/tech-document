package com.nov.rac.smdl.panes.addnewcode;

import javax.swing.JMenuItem;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;

public class NOVSMDLQueryMenuItem extends JMenuItem
{
    public NOVSMDLQueryMenuItem(TCComponent imancomponent)
    {
        super(imancomponent.toString());
        query = (TCComponentQuery) imancomponent;
        try
        {
            setToolTipText(imancomponent.getProperty("query_desc"));
        }
        catch ( Exception exception )
        {
        }
    }

    public TCComponentQuery getQuery()
    {
        return query;
    }

    private static final long  serialVersionUID = 1L;
    private TCComponentQuery query;
}
