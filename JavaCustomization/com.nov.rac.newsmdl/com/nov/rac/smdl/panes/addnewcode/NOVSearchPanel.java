package com.nov.rac.smdl.panes.addnewcode;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.createsmdl.NOVSMDLNewEditDialog;
import com.nov.rac.utilities.utils.MessageBox;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.MRUButton;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ConfirmationDialog;
import com.teamcenter.rac.util.ImageUtilities;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SplitPane;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.balloon.Balloon;
import com.teamcenter.rac.util.tabbedpane.Tab;
import com.teamcenter.rac.util.tabbedpane.iTabbedPane;

public class NOVSearchPanel extends JPanel
{
    public NOVSearchPanel getSearchPanel()
    {
        if ( queryPanel == null )
        {
            queryPanel = new NOVSMDLSearchPanel(session , newEditDlg.desktop ,
                    true , true , false);
            queryPanel.setApplicationPanel(NOVSearchPanel.this);
            splitPane.setLeftComponent(queryPanel);
            queryPanel.getBanner().setCloseListener(new ActionListener() {
                public void actionPerformed(ActionEvent e )
                {
                    // 
                }
            });
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                setQueryPanelVisible(true);
                revalidate();
                repaint();
            }
        });
        return this;
    }

    boolean tabbedPaneRequired = false;

    public NOVSearchPanel(TCSession tcsession, boolean tabbedPaneRequired)
    {
        super(new BorderLayout());
        this.tabbedPaneRequired = tabbedPaneRequired;
        appReg = Registry.getRegistry(this);
        session = tcsession;
        splitPane = new SplitPane(0);
        searchTabPane = new iTabbedPane() {
            private static final long serialVersionUID = 1L;

            public void tabSelected(Tab tab )
            {
                final NOVSearchResultsPanel p = (NOVSearchResultsPanel) tab.getPanel();
                super.tabSelected(tab);
                Runnable runnable = new Runnable() {
                    public void run()
                    {
                        int selectedTabCnt = searchTabPane.getSelectedIndex();
                        System.out.println("selectedTabCnt" + selectedTabCnt);
                        p.openComponent();
                    }
                };
                if ( SwingUtilities.isEventDispatchThread() )
                    runnable.run();
                else
                    SwingUtilities.invokeLater(runnable);
            }
        };
        popupMenu = new NOVSearchPopupMenu(AIFUtility.getActiveDesktop());
        mouseAdapter = new MouseAdapter() {
            public void mousePressed(MouseEvent mouseevent )
            {
                int i = mouseevent.getModifiers();
                if ( i == 4 )
                    popupMenu.show((Component) mouseevent.getSource(),
                            mouseevent.getX(), mouseevent.getY());
            }
        };
        if ( ! AIFUtility.getAIFPortal().getAuthenticator().isRestrictedMode() )
        {
            searchDesktopPanel = new JPanel(new VerticalLayout(1 , 0 , 1 , 1 ,1));
            searchDesktopPanel.add(searchTabPane, "unbound");
            searchDesktopPanel = new JPanel(new VerticalLayout(1 , 0 , 1 , 1 ,1));
            searchDesktopPanel.add(searchTabPane, "unbound");
            if ( queryPanel == null )
            {
                queryPanel = new NOVSMDLSearchPanel(session,AIFUtility.getActiveDesktop(), true , true , false);
                queryPanel.setApplicationPanel(NOVSearchPanel.this);
                splitPane.setLeftComponent(queryPanel);
                queryPanel.getBanner().setCloseListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e )
                    {
                        // searchButton.doClick();
                    }
                });
            }
            SwingUtilities.invokeLater(new Runnable() {
                public void run()
                {
                    setQueryPanelVisible(true);
                    revalidate();
                    repaint();
                }
            });
            splitPane.setDividerLocation(0.5);
            add("Center", splitPane);
            revalidate();
            repaint();
        }
    }

    public Icon getTabIcon(Object obj )
    {
        ImageIcon imageicon = null;
        if ( obj instanceof TCComponentQuery )
        {
            TCComponentQuery imancomponentquery = (TCComponentQuery) obj;
            try
            {
                String s = imancomponentquery.getProperty(TCComponentQuery.PROP_QUERY_FLAG);
                if ( s.equals(TCComponentQuery.QRY_DOMAIN_LOCAL) )
                    imageicon = appReg.getImageIcon("localQuery.ICON");
                else if ( s.equals(TCComponentQuery.QRY_DOMAIN_REMOTE) )
                    imageicon = appReg.getImageIcon("remoteQuery.ICON");
                else
                    imageicon = appReg.getImageIcon("userExitQuery.ICON");
            }
            catch ( Exception exception )
            {
                imageicon = TCTypeRenderer.getIcon(imancomponentquery, false);
            }
        }
        else if ( obj instanceof TCComponent )
            imageicon = TCTypeRenderer.getIcon(obj, false);
        return imageicon;
    }

    public NOVSearchResultsPanel createTab(Object obj , String s , String as[] ,
            String as1[] , boolean flag , boolean tabbedPaneRequired )
    {
        Object obj1 = null;
        if ( obj instanceof NOVSMDLSearchCriteriaPanel )
        {
            obj1 = new NOVSearchResultsPanel(AIFUtility.getCurrentApplication() ,
                    (NOVSMDLSearchCriteriaPanel) obj , tabbedPaneRequired);
        }
        else if ( obj instanceof TCComponentQuery )
        {
            obj1 = new NOVSearchResultsPanel(AIFUtility.getCurrentApplication() , (TCComponent) obj ,
                    as , as1 , true , tabbedPaneRequired);
        }
        if ( obj1 != null )
        {
            Object obj3 = getTabIcon(obj);
            if ( s == null || s.length() <= 0 && ((NOVSearchResultsPanel) (obj1)).getComponentToOpen() != null )
            {
                TCComponent imancomponent1 = ((NOVSearchResultsPanel) (obj1)).getComponentToOpen();
                if ( imancomponent1 instanceof TCComponentQuery )
                {
                    int i = 1;
                    int j = searchTabPane.getTabCount();
                    for ( int k = 0; k < j; k++ )
                    {
                        NOVSearchResultsPanel navigatorpanel = getTabPanel(k);
                        if ( navigatorpanel == null ) 
                        		continue;
                        TCComponent imancomponent2 = navigatorpanel.getComponentToOpen();
                        if ( (imancomponent2 instanceof TCComponentQuery) && imancomponent2 == imancomponent1 ) 
                        	    i++;
                    }
                    boolean flag1 = false;
                    while ( !flag1 )
                    {
                        s = imancomponent1.toString() + " (" + i + ")";
                        int l = 0;
                        for ( l = 0; l < j; l++ )
                        {
                            NOVSearchResultsPanel navigatorpanel1 = getTabPanel(l);
                            if ( navigatorpanel1 == null ) 
                            	continue;
                            TCComponent imancomponent3 = navigatorpanel1.getComponentToOpen();
                            if ( (imancomponent3 instanceof TCComponentQuery) 
                            	  && imancomponent3 == imancomponent1  && s.equals(navigatorpanel1.toString()) )
                            	break;
                        }
                        if ( l >= j )
                            flag1 = true;
                        else
                            i++;
                    }
                }
            }
            if ( searchTabPane.getComponentCount() == 0 )
                ((NOVSearchResultsPanel) (obj1)).openComponent();
            if ( !((NOVSearchResultsPanel) (obj1)).isLoaded() )
            {
                ImageIcon imageicon = appReg.getImageIcon("notLoaded.ICON");
                ImageIcon imageicon1 = (ImageIcon) obj3;
                if ( imageicon != null && imageicon1 != null )
                    obj3 = ImageUtilities.overlay(imageicon1.getImage(),
                            imageicon.getImage(), getBackground());
            }
            Tab tab = null;
            if ( obj == null )
            {
                Object obj4;
                try
                {
                    obj4 = Registry.getImageIconFromPath(appReg.getString("homeButton.ICON"));
                    ((NOVSearchResultsPanel) (obj1)).setPanelIcon(((Icon) (obj4)));
                    ((NOVSearchResultsPanel) (obj1)).setName("SMDL");
                    tab = searchTabPane.addTab("SMDL", ((Icon) obj4),((Component) (obj1)));
                    tab.setSelected(true);
                }
                catch ( Exception e )
                {
                    e.printStackTrace();
                }
            }
            else
            {
                ((NOVSearchResultsPanel) (obj1)).setPanelIcon(((Icon) (obj3)));
                ((NOVSearchResultsPanel) (obj1)).setName(s);
                tab = searchTabPane.addTab(s,
                        ((Icon) (flag ? ((NOVSearchResultsPanel) (obj1))
                                .getPanelIcon() : ((Icon) (obj3)))),
                        ((Component) (obj1)), ((NOVSearchResultsPanel) (obj1)).getToolTip());
                tab.addMouseListener(mouseAdapter);
            }
            if ( flag )
            {
                ((NOVSearchResultsPanel) (obj1)).openComponent();
                searchTabPane.setSelectedComponent(((Component) (obj1)));
            }
        }
        Container container = getParent();
        if ( container != null )
        {
            container.validate();
            container.repaint();
        }
        System.out.println("Its in run of invokelater...");
        if ( splitPane.getRightComponent() != searchDesktopPanel )
            add(searchDesktopPanel, "Center");
        else
            splitPane.setRightComponent(searchDesktopPanel);
        revalidate();
        repaint();
        return ((NOVSearchResultsPanel) (obj1));
    }
    protected NOVSearchResultsPanel getTabPanel(int i )
    {
        NOVSearchResultsPanel navigatorpanel = null;
        if ( i >= 0 && i < searchTabPane.getTabCount() )
        {
            Component component = searchTabPane.getComponentAt(i);
            if ( component instanceof NOVSearchResultsPanel )
                navigatorpanel = (NOVSearchResultsPanel) component;
        }
        return navigatorpanel;
    }

    public void setQueryPanelVisible(boolean flag )
    {
        if ( flag )
        {
            splitPane.setLeftComponent(queryPanel);
            splitPane.setRightComponent(searchDesktopPanel);
            splitPane.setDividerLocation(0.5);
            add("Center", splitPane);
            queryPanel.requestionInputFocus();
        }
        else
        {
            remove(splitPane);
            add("Center", searchDesktopPanel);
        }
        getParent().doLayout();
        getParent().validate();
        getParent().repaint();
        if ( flag && !shown )
        {
            shown = true;
            IWorkbench iworkbench = PlatformUI.getWorkbench();
            IWorkbenchWindow iworkbenchwindow = iworkbench == null ? null : iworkbench.getActiveWorkbenchWindow();
            Registry registry = Registry.getRegistry(this);
            if(iworkbenchwindow != null)
            {
            	Balloon.post(queryPanel.getQuerySelectionButton(), registry
            			.getString("querySelection.TITLE"), registry
            			.getString("querySelection.MESSAGE"), Balloon.SOUTH, true,
            			"SearchCriteriaButton",iworkbenchwindow.getShell());
            }
        }
    }

    public void openSearch(NOVSMDLSearchCriteriaPanel searchcriteriapanel )
    {
        if ( searchcriteriapanel == null || searchcriteriapanel.isEmpty() )
        {   
        	NOVSMDLNewEditDialog smdlEditDlg = getDocumentEditDialog(searchcriteriapanel);
        	MessageBox.post(smdlEditDlg, appReg.getString("search.TITLE"), appReg.getString("emptyCriteria"), 
        			        MessageBox.INFORMATION);
        	
            return;
        }
        else
        {
            final NOVSMDLSearchCriteriaPanel p = searchcriteriapanel;
            session.queueOperation(new AbstractAIFOperation() {
                public void executeOperation()
                {
                    session.setStatus(appReg.getString("performSearch"));
                    createTab(p, null, null, null, true, tabbedPaneRequired);
                    session.setReadyStatus();
                }
            });
            return;
        }
    }

    public void refreshSearch(NOVSMDLSearchCriteriaPanel searchcriteriapanel )
    {
        if ( searchcriteriapanel == null || searchcriteriapanel.isEmpty() )
        {
        	NOVSMDLNewEditDialog smdlEditDlg = getDocumentEditDialog(searchcriteriapanel);
        	MessageBox.post(smdlEditDlg, appReg.getString("search.TITLE"), appReg.getString("emptyCriteria"), 
			        MessageBox.INFORMATION);
            return;
        }
        final NOVSMDLSearchCriteriaPanel p = searchcriteriapanel;
        NOVSearchResultsPanel searchPanel = (NOVSearchResultsPanel) searchTabPane.getSelectedComponent();
        /////changes done for null pointer exception
        if(searchPanel !=null)
        {
	        if ( searchPanel.getOpenedComponent() instanceof com.teamcenter.rac.kernel.TCComponentQuery )
	        {
	        	if ( searchPanel.getOpenedComponent() != searchcriteriapanel.getQuery() )
	            {
	                int i = ConfirmationDialog.post(AIFUtility.getActiveDesktop(),
	                        appReg.getString("refresh.TITLE"), appReg.getString("refresh.CONFIRM"));
	                if ( i == 1 ) 
	                	return;
	            }
	            session.queueOperation(new AbstractAIFOperation() {
	                public void executeOperation()
	                {
	                    session.setStatus(appReg.getString("performRefresh"));
	                    NOVSearchResultsPanel searchPanel = (NOVSearchResultsPanel) searchTabPane.getSelectedComponent();
	                    searchPanel.setSearchCriteriaPanel(p);
	                    searchPanel.setToolTip();
	                    session.setReadyStatus();
	                }
	            });
	        }
	        else
	        {
	            session.queueOperation(new AbstractAIFOperation() {
	                public void executeOperation()
	                {
	                    session.setStatus(appReg.getString("performRefresh"));
	                    createTab(p, null, null, null, true, tabbedPaneRequired);
	                    session.setReadyStatus();
	                }
	            });
	        }
        }
        /////changes done for null pointer exception
        else
        {
        	session.queueOperation(new AbstractAIFOperation() 
        	{
                public void executeOperation()
                {
                    session.setStatus(appReg.getString("performRefresh"));
                    createTab(p, null, null, null, true, tabbedPaneRequired);
                    session.setReadyStatus();
                }
            });
        }
        
    }
    public NOVSearchResultsPanel getVisiblePanel()
    {
        Component component = searchTabPane.getSelectedComponent();
        if ( component instanceof NOVSearchResultsPanel )
            return (NOVSearchResultsPanel) component;
        else
            return null;
    }
    private NOVSMDLNewEditDialog getDocumentEditDialog(Component comp)
	{
    	Component parent= comp.getParent();
    	NOVSMDLNewEditDialog editDialog = null;
		while (parent!=null && ! (parent instanceof NOVSMDLNewEditDialog )) 
		{			
			parent  =	parent.getParent();	
		}		
		if(parent!=null && parent instanceof NOVSMDLNewEditDialog)
		{
			editDialog = (NOVSMDLNewEditDialog)parent;
		}
		return editDialog;
	}
    private static final long          serialVersionUID = 1L;
    protected Registry                 appReg;
    protected TCSession              session;
    protected MRUButton                mruButton;
    public NOVSMDLSearchPanel          queryPanel       = null;
    protected iTabbedPane              searchTabPane;
    protected JPanel                   searchDesktopPanel;
    protected SplitPane                splitPane;
    protected JButton                  openHomeButton;
    protected JButton                  addNewButton;
    protected JButton                  openInboxButton;
    protected JPopupMenu               popupMenu;
    protected MouseAdapter             mouseAdapter;
    private static boolean             shown            = false;
    public static NOVSMDLNewEditDialog newEditDlg       = null;
    
}
