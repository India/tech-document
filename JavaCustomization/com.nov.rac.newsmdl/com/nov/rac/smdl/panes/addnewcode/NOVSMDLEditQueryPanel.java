package com.nov.rac.smdl.panes.addnewcode;

import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.querybuilder.AbstractQueryBuilderApplicationPanel;


public class NOVSMDLEditQueryPanel extends AbstractQueryBuilderApplicationPanel
{
    /**
     * 
     */
    public NOVSMDLEditQueryPanel(com.teamcenter.rac.aif.AIFDesktop aifdesktop,
            TCSession imansession, com.teamcenter.rac.util.Registry registry)
    {
        super(aifdesktop , imansession , registry , false);
        queryTypeComboBox.setEnabled(false);
    }

    public void setQuery(TCComponentQuery imancomponentquery )
    {
        editPanelQuery = imancomponentquery;
        queryTypeComboBox.setEnabled(false);
        super.startPopulateBuilderOperation(imancomponentquery);
    }

    public void setQuery(TCComponentQuery imancomponentquery , String s )
    {
        setQueryName(s);
        setQuery(imancomponentquery);
    }

    private static final long serialVersionUID = 1L;
    NOVSMDLEditQueryPanel     editQueryPanel;
}

