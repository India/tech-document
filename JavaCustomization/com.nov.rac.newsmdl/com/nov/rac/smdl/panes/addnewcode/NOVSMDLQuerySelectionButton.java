
package com.nov.rac.smdl.panes.addnewcode;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;

import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.kernel.AIFComponentCreateEvent;
import com.teamcenter.rac.aif.kernel.AIFComponentDeleteEvent;
import com.teamcenter.rac.aif.kernel.AIFComponentEvent;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponentEventListener;
import com.teamcenter.rac.explorer.common.AbstractOptionPanel;
import com.teamcenter.rac.kernel.SessionChangedEvent;
import com.teamcenter.rac.kernel.SessionChangedListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.InterfaceSignalOnClose;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.Utilities;

public class NOVSMDLQuerySelectionButton extends JLabel implements
    	InterfaceSignalOnClose, PropertyChangeListener,
        InterfaceAIFComponentEventListener, SessionChangedListener
{
    public NOVSMDLQuerySelectionButton(TCSession s)
    {
        super();
        m_reg = Registry.getRegistry(this);
        session = s;
        session.addAIFComponentEventListener(this);
        session.addSessionChangeListener(this);
        try
        {
            if ( session != null )
                session.getPreferenceService().addPropertyChangeListener(this);
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
            MessageBox.post(AbstractAIFDialog.getParentFrame(this), ex);
        }
        initialize();
        
        loadQueries();
        repaint();
    }

    public NOVSMDLQuerySelectionButton(TCSession s, String[] queryTypes)
    {
        this(s);
    }

    public Dimension getPreferredSize()
    {
        Dimension d = super.getPreferredSize();
        d.width += 15;
        d.height += 6;
        return (d);
    }
    private void initialize()
    {
        loaded = false;
        i = m_reg.getImageIcon("search.ICON");
                
        fontName = m_reg.getString("QueryNameFont.NAME", "Dialog");
        fontSize = m_reg.getInt("QueryNameFont.SIZE", 10);
                
        setIcon(i);
        setIconTextGap(1);
        pm = new JPopupMenu();
    }
    public TCComponentQuery getSelectedQuery()
    {
        return (selectedQuery);
    }

    public void setSelectedQuery(TCComponentQuery q )
    {
        if ( q != null )
        {
            this.setText(q.toString());
            selectedQuery = q;
        }
        else
        {
            this.setText(" ");
            selectedQuery = null;
        }
        String desc = ""; // the query description
        if ( selectedQuery != null )
        {
            try
            {
                desc = selectedQuery.getProperty("query_desc");
                desc.trim();
            }
            catch ( TCException ex )
            {
                ex.printStackTrace();
                MessageBox.post(AbstractAIFDialog.getParentFrame(this), ex);
            }
        }
        if ( desc.length() > 0 )
        {
            setToolTipText(desc);
        }
        else
        {
            setToolTipText(this.getText());
        }
    }

    private void loadQueries()
    {
        try
        {
        	ArrayList<TCComponent> queryList = new ArrayList<TCComponent>();
        	if(m_documentsQuery == null)
        	{
        		TCComponentQueryType tc = (TCComponentQueryType) session.getTypeComponent("ImanQuery");            
        		m_documentsQuery = tc.find(m_reg.getString("docSearchQuery.NAME"));
        	}            
            if ( m_documentsQuery != null )
            {
                queryList.add(m_documentsQuery);
            }
            if(queryList.size() == 0)
            {
                MessageBox.post(m_reg.getString("noQueryFound.MSG"),
                        		"Search ERROR...", MessageBox.ERROR);
                pm.setVisible(false);
                return;
            }
            favoriteQueries = new TCComponentQuery[queryList.size()];
            queryList.toArray(favoriteQueries);
            
            pm.setVisible(false);
            pm.removeAll();        
           
            for ( int j = 0; j < favoriteQueries.length; j++ )
            {
            	m_menuItem = new NOVSMDLQueryMenuItem(favoriteQueries[j]);            			
            	Font font = Utilities.validateFont(fontName, Font.PLAIN, 
            						fontSize, favoriteQueries[j].toString());
            	if ( font != null )
            	{
            		m_menuItem.setFont(font);
            	}
            	pm.add(m_menuItem);
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    public void propertyChange(PropertyChangeEvent evt )
    {
        String prefName = evt.getPropertyName();
        if ( prefName.equals(AbstractOptionPanel.FAVORITE_QUERY_PREF) )
        {
            refresh();
        }
    }

    public void processComponentEvents(AIFComponentEvent[] events )
    {
        TCComponent eventComp = null;
        for ( int i = 0; i < events.length; i++ )
        {
            eventComp = (TCComponent) events[i].getComponent();
            if ( eventComp != null
                    && eventComp instanceof TCComponentQuery
                    && (events[i] instanceof AIFComponentDeleteEvent || events[i] instanceof AIFComponentCreateEvent) )
            {
                loaded = false;
                break;
            }
        }
    } // End of process component events

    public void sessionChanged(SessionChangedEvent e )
    {
        refresh();
    }

    public void closeSignaled()
    {
        try
        {
            if ( session != null )
            {
                session.removeAIFComponentEventListener(this);
                session.removeSessionChangeListener(this);
                session.getPreferenceService().removePropertyChangeListener(
                        this);
                session = null;
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
            MessageBox.post(AbstractAIFDialog.getParentFrame(this), ex);
        }
    }

    public void refresh()
    {
        loaded = false;
    }
    public NOVSMDLQueryMenuItem getMenuItem()
    {
    	return m_menuItem;
    }

    private static final long      serialVersionUID  = 1L;
    private ImageIcon              i;
    private JPopupMenu             pm;
    private boolean                loaded;
    private TCSession            session;
    private TCComponentQuery     selectedQuery;
    protected TCComponentQuery[] favoriteQueries;
    private String                 fontName          = null;
    private int                    fontSize          = 10;
    private NOVSMDLQueryMenuItem m_menuItem;
    private static TCComponent m_documentsQuery = null;
    private Registry m_reg;
}
