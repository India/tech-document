package com.nov.rac.smdl.panes.addnewcode;

import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.popupmenu.AbstractTCComponentPopupMenu;
import com.teamcenter.rac.util.ButtonLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.TextPrinter;
import com.teamcenter.rac.util.Utilities;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.tabbedpane.Tab;
import com.teamcenter.rac.util.tabbedpane.iTabbedPane;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.*;

public class NOVSearchPopupMenu extends AbstractTCComponentPopupMenu
{
    protected class NewNameDialog extends AbstractAIFDialog
    {
        public void setVisible(boolean flag )
        {
            super.setVisible(flag);
            if ( flag ) inputField.setText(panel.toString());
        }

        public boolean isValid()
        {
            return bOk;
        }

        public String getInput()
        {
            return inputField.getText();
        }

        public NewNameDialog(Frame frame, NOVSearchResultsPanel navigatorpanel)
        {
            super(frame , true);
            bOk = false;
            inputField = null;
            panel = navigatorpanel;
            setTitle(r.getString("rename") + " " + navigatorpanel.toString());
            JPanel jpanel = new JPanel(new VerticalLayout(10 , 10 , 10 , 10 ,
                    10));
            getContentPane().add(jpanel);
            final JButton okButton = new JButton(r.getString("ok"));
            okButton.setMnemonic(r.getString("ok.MNEMONIC").charAt(0));
            okButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionevent )
                {
                    bOk = true;
                    panel.setName(getInput());
                    disposeDialog();
                }
            });
            inputField = new JTextField(navigatorpanel.toString() , 15);
            inputField.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionevent )
                {
                    okButton.doClick();
                }
            });
            /*
             * JButton jbutton = new JButton(r.getString("cancel"));
             * jbutton.setMnemonic(r.getString("cancel.MNEMONIC").charAt(0));
             * jbutton.addActionListener(new
             * com.ugsolutions.aif.AbstractAIFDialog);
             */
            JPanel jpanel1 = new JPanel(new ButtonLayout());
            jpanel1.add(okButton);
            // jpanel1.add(jbutton);
            jpanel.add("unbound.bind.center.center", inputField);
            jpanel.add("bottom.bind", jpanel1);
            pack();
            centerToScreen();
            inputField.requestFocus();
        }

        private static final long       serialVersionUID = 1L;
        protected boolean               bOk;
        protected JTextField            inputField;
        protected NOVSearchResultsPanel panel;
    }

    protected class CompareMenuItem extends JMenuItem
    {
        public CompareMenuItem(NOVSearchResultsPanel navigatorpanel)
        {
            super(navigatorpanel.toString() + " ..." , navigatorpanel
                    .getPanelIcon());
            navigatorPanel = navigatorpanel;
            addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionevent )
                {
                    navigatorPanel.getSession().queueOperation(
                            new AbstractAIFOperation(r.getString("comparing")) {
                                public void executeOperation()
                                {
                                    compareToAction((NOVSearchResultsPanel) tab
                                            .getPanel(), navigatorPanel);
                                }
                            });
                }
            });
        }

        private static final long          serialVersionUID = 1L;
        NOVSearchResultsPanel navigatorPanel;
    }

    public NOVSearchPopupMenu()
    {
        this(null);
    }

    public NOVSearchPopupMenu(Frame frame)
    {
        close = null;
        closeAll = null;
        rename = null;
        refresh = null;
        saveSearch = null;
        printSearch = null;
        moveLeft = null;
        moveRight = null;
        moveFirst = null;
        moveLast = null;
        parent = frame;
        r = Registry.getRegistry(this);
        addPopupHeader(r.getString("explorer.POPUP_TITLE"));
        close = new JMenuItem(r.getString("close") , r
                .getImageIcon(/*r.getString*/("close.ICON")));
        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        closeAction();
                    }
                });
                thread.start();
            }
        });
        closeAll = new JMenuItem(r.getString("closeAll") , r
                .getImageIcon("closeAll.ICON"));
        closeAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        closeAllAction();
                    }
                });
                thread.start();
            }
        });
        rename = new JMenuItem(r.getString("rename") , r
                .getImageIcon("rename.ICON"));
        rename.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                renameAction();
            }
        });
        refresh = new JMenuItem(r.getString("refresh") , r
                .getImageIcon("refreshButton.ICON"));
        refresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                refreshAction();
            }
        });
        compareToMenu = new JMenu(r.getString("compareTo"));
        moveMenu = new JMenu(r.getString("moveTo"));
        moveLeft = new JMenuItem(r.getString("moveLeft") , r.getImageIcon(
                "left.ICON", 12, 12));
        moveLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        moveLeftAction();
                    }
                });
                thread.start();
            }
        });
        moveRight = new JMenuItem(r.getString("moveRight") , r.getImageIcon(
                "right.ICON", 12, 12));
        moveRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        moveRightAction();
                    }
                });
                thread.start();
            }
        });
        moveFirst = new JMenuItem(r.getString("moveFirst") , r.getImageIcon(
                "first.ICON", 12, 12));
        moveFirst.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        moveFirstAction();
                    }
                });
                thread.start();
            }
        });
        moveLast = new JMenuItem(r.getString("moveLast") , r.getImageIcon(
                "last.ICON", 12, 12));
        moveLast.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        moveLastAction();
                    }
                });
                thread.start();
            }
        });
        moveMenu.add(moveLeft);
        moveMenu.add(moveRight);
        // moveMenu.add(moveFirst);
        moveMenu.add(moveLast);
        saveSearch = new JMenuItem(r.getString("saveSearch") , r
                .getImageIcon("save.ICON"));
        saveSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                saveSearchAction();
            }
        });
        printSearch = new JMenuItem(r.getString("print") , r
                .getImageIcon("print.ICON"));
        printSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                Thread thread = new Thread(new Runnable() {
                    public void run()
                    {
                        printSearchAction();
                    }
                });
                thread.start();
            }
        });
        projMenu = new JMenu(r.getString("project.NAME"));
        projMenu.setMnemonic(r.getChar("project.MNEMONIC", 'P'));
        projMenu.setActionCommand("projMenu");
        JMenuItem jmenuitem = addMenuItem(projMenu, "assignToProjectAction");
        jmenuitem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                doProjectAction("AssignToProject");
            }
        });
        JMenuItem jmenuitem1 = addMenuItem(projMenu, "removeFromProjectAction");
        jmenuitem1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                doProjectAction("RemoveFromProject");
            }
        });
        add(close);
        add(closeAll);
        add(refresh);
        addSeparator();
        add(moveMenu);
        addSeparator();
    }

    public void show(Component component , int i , int j )
    {
        if ( !(component instanceof Tab) )
        {
            return;
        }
        else
        {
            tab = (Tab) component;
            tabPane = tab.getTabbedPane();
            validateMenus();
            super.show(component, i, j);
            return;
        }
    }

    protected void closeAction()
    {
        tabPane.remove(tab);
    }

    protected void closeAllAction()
    {
        int i = tabPane.getTabCount();
        for ( int j = i; j > 2; j-- )
        {
            if ( j != 0 )
            {
                Component component = ((NOVSearchResultsPanel) tabPane
                        .getComponentAt(j - 1));
                tabPane.remove(component);
            }
        }
    }

    protected void refreshAction()
    {
        final NOVSearchResultsPanel np = (NOVSearchResultsPanel) tab.getPanel();
        Thread thread = new Thread() {
            public void run()
            {
                np.getSession().queueOperation(
                        new AbstractAIFOperation(r.getString("refresh")
                                + " ...") {
                            public void executeOperation() throws Exception
                            {
                                np.refresh();
                            }
                        });
            }
        };
        thread.start();
    }

    protected void saveSearchAction()
    {
        Thread thread = new Thread() {
            public void run()
            {
                NOVSearchResultsPanel navigatorpanel = (NOVSearchResultsPanel) tab
                        .getPanel();
            }
        };
        thread.start();
    }

    private void doProjectAction(final String actionCode )
    {
        Thread thread = new Thread() {
            public void run()
            {
                NOVSearchResultsPanel navigatorpanel = (NOVSearchResultsPanel) tab
                        .getPanel();
                Frame frame = parent == null ? Utilities
                        .getParentFrame(navigatorpanel) : parent;
            }
        };
        thread.start();
    }

    protected void printSearchAction()
    {
        NOVSearchResultsPanel navigatorpanel = (NOVSearchResultsPanel) tab
                .getPanel();
        Vector vector = navigatorpanel.getCurrentLoadingObjects();
        if ( vector != null && vector.size() > 0 )
        {
            String s = "";
            int i = vector.size();
            for ( int j = 0; j < i; j++ )
                s = s + (j != 0 ? "\n" : "") + vector.elementAt(j);
            TextPrinter.print(s, navigatorpanel.toString());
        }
    }

    protected void moveLeftAction()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                tabPane.moveLeft(tab);
                tabPane.validate();
                tabPane.repaint();
            }
        });
    }

    protected void moveRightAction()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                tabPane.moveRight(tab);
                tabPane.validate();
                tabPane.repaint();
            }
        });
    }

    protected void moveFirstAction()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                tabPane.moveFirst(tab);
                tabPane.validate();
                tabPane.repaint();
            }
        });
    }

    protected void moveLastAction()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                tabPane.moveLast(tab);
                tabPane.validate();
                tabPane.repaint();
            }
        });
    }

    protected void renameAction()
    {
        Thread thread = new Thread() {
            public void run()
            {
                NOVSearchResultsPanel navigatorpanel = (NOVSearchResultsPanel) tab
                        .getPanel();
                NewNameDialog newnamedialog = new NewNameDialog(parent ,
                        navigatorpanel);
                SwingUtilities.invokeLater(newnamedialog);
            }
        };
        thread.start();
    }

    protected void compareToAction(NOVSearchResultsPanel navigatorpanel ,
            NOVSearchResultsPanel navigatorpanel1 )
    {
        if ( !navigatorpanel.isLoaded() ) navigatorpanel.preLoad();
        if ( !navigatorpanel1.isLoaded() ) navigatorpanel1.preLoad();
    }

    protected void validateMenus()
    {
        int i = tabPane.indexOfTab(tab);
        Tab tabAtLeft = tabPane.getTabAt(0);
        //Mohan
        /*if ( iTabbedPane.getTabComponent(tabAtLeft) instanceof NOVSMDLTablePanel )
            moveLeft.setEnabled(i > 1);
        else*/
            moveLeft.setEnabled(i > 0);
        moveFirst.setEnabled(i > 0);
        close.setEnabled(tabPane.getTabCount() > 0);
        // @@ Customised for DSE
        closeAll.setEnabled(tabPane.getTabCount() > 1);
        boolean flag = i != tabPane.getTabCount() - 1;
        moveRight.setEnabled(flag);
        moveLast.setEnabled(flag);
        moveMenu.setEnabled(i != 0 || flag);
        NOVSearchResultsPanel navigatorpanel = (NOVSearchResultsPanel) tab
                .getPanel();
        boolean flag1 = navigatorpanel != null
                && navigatorpanel.isExecutedQueryPanel();
        saveSearch.setEnabled(flag1);
        printSearch.setEnabled(flag1);
        projMenu.setEnabled(flag1);
        rename.setEnabled(navigatorpanel.isQueryPanel());
        refresh.setEnabled(navigatorpanel.isQueryPanel());
        compareToMenu.removeAll();
        int j = tabPane.getTabCount();
        for ( int k = 0; k < j; k++ )
        {
            Component component = tabPane.getComponentAt(k);
            if ( !(component instanceof NOVSearchResultsPanel) ) continue;
            NOVSearchResultsPanel navigatorpanel1 = (NOVSearchResultsPanel) component;
            if ( navigatorpanel1 != navigatorpanel )
                compareToMenu.add(new CompareMenuItem(navigatorpanel1));
        }
        compareToMenu.setEnabled(compareToMenu.getMenuComponentCount() > 0);
    }

    private static final long serialVersionUID = 1L;
    protected JMenuItem       close            = null;
    protected JMenuItem       closeAll         = null;
    protected JMenuItem       rename           = null;
    protected JMenuItem       refresh          = null;
    protected JMenuItem       saveSearch       = null;
    protected JMenuItem       printSearch      = null;
    protected JMenuItem       moveLeft         = null;
    protected JMenuItem       moveRight        = null;
    protected JMenuItem       moveFirst        = null;
    protected JMenuItem       moveLast         = null;
    protected JMenu           moveMenu         = null;
    protected JMenu           compareToMenu    = null;
    protected iTabbedPane     tabPane          = null;
    protected Tab             tab              = null;
    protected Frame           parent           = null;
    protected Registry        r                = null;
    protected JMenu           projMenu         = null;
}
