package com.nov.rac.smdl.panes.addnewcode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.teamcenter.rac.util.ColorUtilities;
import com.teamcenter.rac.util.Painter;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.Utilities;

public class NOVSMDLSearchBanner extends JComponent
{
    public NOVSMDLSearchBanner()
    {
        i = null;
        font = null;
        SystemColor systemcolor = SystemColor.activeCaption;
        end = ColorUtilities.contrast(systemcolor, 0.20000000000000001D);
        start = ColorUtilities.darken(end, 0.25D);
        start = ColorUtilities.contrast(start, -0.5D);
        end = ColorUtilities.contrast(end, -0.29999999999999999D);
        closeListener = null;
        Registry registry = Registry.getRegistry(this);
        i = registry.getImageIcon("find.ICON");
        title = registry.getString("title");
        String s = registry.getString("SearchBannerFont.NAME", "Dialog");
        int j = registry.getInt("SearchBannerFont.SIZE", 14);
        font = Utilities.validateFont(s, 0, j, title);
        addListeners();
    }

    public NOVSMDLSearchBanner(ImageIcon imageicon, String s)
    {
        this();
        i = imageicon;
        title = s;
    }

    private void addListeners()
    {
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseevent )
            {
                Point point = new Point(mouseevent.getX() , mouseevent.getY());
                if ( closeRectangle == null || closeListener == null ) return;
                if ( closeRectangle.contains(point) )
                    closeListener.actionPerformed(new ActionEvent(this , 1001 ,
                            "Close Search Panel"));
            }

            public void mouseExited(MouseEvent mouseevent )
            {
                insideClose = false;
                repaint();
            }
        });
        addMouseMotionListener(new MouseMotionListener() {
            public void mouseMoved(MouseEvent mouseevent )
            {
                if ( closeRectangle == null ) return;
                Point point = new Point(mouseevent.getX() , mouseevent.getY());
                if ( closeRectangle.contains(point) )
                {
                    insideClose = true;
                    repaint();
                }
                else
                {
                    insideClose = false;
                    repaint();
                }
            }

            public void mouseDragged(MouseEvent mouseevent )
            {
            }
        });
    }

    public Dimension getPreferredSize()
    {
        Dimension dimension = super.getPreferredSize();
        if ( null != i )
        {
            dimension.width = i.getIconWidth() + 30;
            dimension.height = i.getIconHeight() + 8;
        }
        return dimension;
    }

    public void setCloseListener(ActionListener actionlistener )
    {
        closeListener = actionlistener;
    }

    public void paint(Graphics g )
    {
        Painter.paintVerticalGradient(this, g, start, Color.white);
        Dimension dimension = getSize();
        if ( null != i )
        {
            int j = dimension.width - 20 - i.getIconWidth();
            int k = (dimension.height - i.getIconHeight()) / 2;
            g.drawImage(i.getImage(), j, k, i.getIconWidth(),
                    i.getIconHeight(), this);
        }
        g.setColor(Color.white);
        if ( font == null ) font = getFont();
        if ( font != null )
        {
            g.setFont(font);
            int l = font.getSize() - 3;
            g.drawString(title, 4, dimension.height - l);
        }
        else
        {
            g.drawString(title, 4, dimension.height);
        }
        g.drawLine(dimension.width - 5, 5, dimension.width - 10, 10);
        g.drawLine(dimension.width - 10, 5, dimension.width - 5, 10);
        if ( insideClose )
        {
            g.setColor(start);
            g.drawLine(dimension.width - 3, 3, dimension.width - 12, 3);
            g.drawLine(dimension.width - 12, 3, dimension.width - 12, 12);
            g.setColor(end);
            g.drawLine(dimension.width - 12, 12, dimension.width - 3, 12);
            g.drawLine(dimension.width - 3, 12, dimension.width - 3, 3);
        }
        closeRectangle = new Rectangle(dimension.width - 10 , 5 , 10 , 10);
        super.paint(g);
    }

    private static final long serialVersionUID = 1L;
    private ImageIcon         i;
    private String            title;
    private ActionListener    closeListener;
    private Rectangle         closeRectangle;
    private boolean           insideClose;
    private Font              font;
    private Color             start;
    private Color             end;
}
