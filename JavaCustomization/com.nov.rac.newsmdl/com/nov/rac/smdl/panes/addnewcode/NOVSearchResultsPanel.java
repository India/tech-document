package com.nov.rac.smdl.panes.addnewcode;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import com.nov.rac.smdl.createsmdl.NOVSMDLNewEditDialog;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.aif.common.QueryCondition;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.AbstractTCApplication;
import com.teamcenter.rac.common.ExpansionRule;
import com.teamcenter.rac.common.MRUButton;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTreeCellRenderer;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.common.TCTreeOpenEvent;
import com.teamcenter.rac.common.TCTreeOpenListener;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.findwithproperties.TCAdhocQueryClause;
import com.teamcenter.rac.common.referencers.ReferencersPanel;
import com.teamcenter.rac.common.referencers.SelectReferencerNodeEvent;
import com.teamcenter.rac.common.referencers.SelectReferencerNodeListener;
import com.teamcenter.rac.common.tcviewer.TCViewerPanel;
import com.teamcenter.rac.explorer.common.AbstractOptionPanel;
import com.teamcenter.rac.explorer.common.DisplayDataTable;
import com.teamcenter.rac.kernel.SystemMonitorInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentContextList;
import com.teamcenter.rac.kernel.TCComponentPff;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCICSClass;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCTypeService;
import com.teamcenter.rac.util.Cookie;
import com.teamcenter.rac.util.HorizontalLayout;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SplitPane;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;
import com.teamcenter.rac.util.tabbedpane.Tab;
import com.teamcenter.rac.util.tabbedpane.iTabbedPane;


public class NOVSearchResultsPanel extends JPanel implements
        TreeSelectionListener, MouseListener, SelectReferencerNodeListener
{
    public NOVSearchResultsPanel(int i)
    {
        super();
    }

    protected int loadingThreshHold = 0;
    protected int getLoadingThreshHold()
    {
        if ( loadingThreshHold < 0 )
            try
            {
                String s = session.getPreferenceService().getString(1,
                        AbstractOptionPanel.SEARCH_LIMIT_PREF);
                if ( s != null && s.length() > 0 )
                    loadingThreshHold = Integer.parseInt(s);
                else
                    loadingThreshHold = 50;
            }
            catch ( Exception exception )
            {
                loadingThreshHold = 50;
            }
        loadingThreshHold = loadingThreshHold <= 0 ? 50 : loadingThreshHold;
        return loadingThreshHold;
    }

    public TCComponent getComponentToOpen()
    {
        return openComponent;
    }

    public TCSession getSession()
    {
        return session;
    }

    public Icon getPanelIcon()
    {
        return panelIcon;
    }

    public void refresh()
    {
        System.out.println("Inside refresh......");
        
         if (isVisible() && (getOpenedComponent() instanceof
         TCComponentQuery)) { openedComponent = null; openComponent(); }
         
    }

    public boolean isQueryPanel()
    {
        return getComponentToOpen() instanceof TCComponentQuery;
    }

    public Vector getCurrentLoadingObjects()
    {
        AIFTreeNode aiftreenode = searchCompTree.getRootNode();
        if ( aiftreenode instanceof TCTreeNode )
        {
            TCTreeNode imantreenode = (TCTreeNode) aiftreenode;
            Vector vector = new Vector();
            Enumeration enumeration = imantreenode.children();
            do
            {
                if ( !enumeration.hasMoreElements() ) break;
                TCTreeNode imantreenode1 = (TCTreeNode) enumeration
                        .nextElement();
                AIFComponentContext aifcomponentcontext = imantreenode1
                        .getContextComponent();
                if ( aifcomponentcontext.getComponent() != null )
                    vector.add(aifcomponentcontext.getComponent());
            } while ( true );
            return vector;
        }
        else
        {
            return null;
        }
    }
    public boolean isExecutedQueryPanel()
    {
        return queryResults != null
                && (getComponentToOpen() instanceof TCComponentQuery);
    }

    public boolean preLoad()
    {
        if ( getComponentToOpen() instanceof TCComponentQuery )
            return isValidSearchResult(
                    (TCComponentQuery) getComponentToOpen(), names, values,
                    false);
        else
            return true;
    }

    public NOVSearchResultsPanel(AbstractAIFUIApplication smdlApp,
            NOVSMDLSearchCriteriaPanel searchcriteriapanel,
            boolean tabbedPaneRequired)
    {
        this(smdlApp , ((TCComponent) (searchcriteriapanel.getQuery())) ,
                true , tabbedPaneRequired);
        setSearchCriteriaPanel(searchcriteriapanel);
    }

    public NOVSearchResultsPanel(
            AbstractAIFUIApplication abstractaifuiapplication,
            TCComponent imancomponent, String as[], String as1[], boolean x,
            boolean tabbedPaneRequired)
    {
        this(abstractaifuiapplication , imancomponent , true ,
                tabbedPaneRequired);
        if ( imancomponent instanceof TCComponentQuery )
        {
            names = as;
            values = as1;
            String s1 = appReg.getString("queryTable.qname") + " = ";
            try
            {
                s1 = s1 + imancomponent.toString() + "\n";
            }
            catch ( Exception exception )
            {
            }
            for ( int i = 0; i < as.length; i++ )
                s1 = s1 + as[i] + " = " + as1[i] + "\n";
            if ( criteriaPanel != null
                    && criteriaPanel.getLimitListTagCnt() > 0 )
            {
                s1 = addLimitContent(s1);
                setLimitContentLists();
            }
        }
    }

    boolean tabbedPaneRequired = false;

    public NOVSearchResultsPanel(
            AbstractAIFUIApplication abstractaifuiapplication,
            TCComponent imancomponent, boolean b,
            boolean argtabbedPaneRequired)
    {
        super(null);
        this.tabbedPaneRequired = argtabbedPaneRequired;
        loadingThreshHold = -1;
        itemRevisionTree = null;
        theTuples = null;
        theTuplesArray = null;
        numTuples = 0;
        numInTuple = 0;
        numTuplesDone = 0;
        limitClip = null;
        limitAppl = null;
        limitPrior = null;
        limitRefrs = null;
        limitWkfl = null;
        limitPse = null;
        resultsIterator = null;
        totalLimitPreference = -1;
        panelTitle = null;
        panelIcon = null;
        loadedObjs = null;
        selectedPff = null;
        adHocState = false;
        queryResults = null;
        adhocQueryClauses = null;
        queryResultTypeComp = null;
        openComponent = imancomponent;
        smdlAPP = abstractaifuiapplication;
               
        eventHandler = new EventHandler();
        appReg = Registry.getRegistry(this);
        session = (TCSession) abstractaifuiapplication.getSession();
        searchCompTree = new TCTree(session , true , true) {
            protected Object[] getChildren(AIFTreeNode node )
            {
                if ( node.getLevel() == 0
                        && getComponentToOpen() instanceof TCComponentQuery )
                {
                    return loadedObjs;
                }
                else
                {
                    return super.getChildren(node);
                }
            }
            private static final long serialVersionUID = 1L;
        };
        // Add pre-expansion event listener
        searchCompTree.setCellRenderer(new TCTreeCellRenderer() {
            private static final long serialVersionUID = 1L;

            public Component getTreeCellRendererComponent(JTree tree ,
                    Object value , boolean isSelected , boolean isExpanded ,
                    boolean isLeaf , int row , boolean hasFocus )
            {
                if ( null != tree )
                {
                    super.getTreeCellRendererComponent(tree, value, isSelected,
                            isExpanded, isLeaf, row, hasFocus);
                    if ( row == 0 ) setIcon(getPanelIcon());
                }
                return this;
            }
        });
        searchCompTree.registerSecondaryIconProperty("release_status_list");
        searchCompTree.registerThirdIconProperty("process_stage_list");
        ExpansionRule expansionrule = searchCompTree.getExpansionRule();
        com.teamcenter.rac.kernel.TypeInfo typeinfo = null;
        try
        {
            TCTypeService imantypeservice = session.getTypeService();
            typeinfo = imantypeservice.getTypesInfoForClass("EPMTask");
        }
        catch ( TCException imanexception )
        {
            MessageBox messagebox = new MessageBox(imanexception);
            messagebox.setVisible(true);
            return;
        }
        String as[] = typeinfo.getTypeNames();
        if ( as != null )
        {
            for ( int i = 0; i < as.length; i++ )
            {
                expansionrule.addRule(as[i], "root_target_attachments");
                expansionrule.addRule(as[i], "root_reference_attachments");
                expansionrule.addRule(as[i], "release_status_attachments");
            }
        }
        searchCompTree.addTreeOpenListener(new TCTreeOpenListener() {
            public void openTreeNode(TCTreeOpenEvent imantreeopenevent )
            {
                TCComponent imancomponent1 = imantreeopenevent
                        .getComponentToOpen();
                try
                {
                    AbstractAIFCommand abstractaifcommand = session
                            .getOpenCommand(new Object[]
                            { smdlAPP.getDesktop(), imancomponent1 });
                    abstractaifcommand.executeModeless();
                }
                catch ( Exception exception1 )
                {
                    MessageBox.post(smdlAPP.getDesktop(), exception1);
                }
            }
        });
        String as1[] = appReg.getStringArray("explorer.OPEN", ",");
        if ( as1 != null )
        {
            for ( int j = 0; j < as1.length; j++ )
                searchCompTree.addOpenRule(as1[j]);
        }
        searchCompTree.setSortable(true);
        if ( !tabbedPaneRequired )
        {
            searchCompTree.addTreeSelectionListener(this);
            searchCompTree.addMouseListener(this);
        }
        else
        {
            searchCompTree.addTreeSelectionListener(eventHandler);
            searchCompTree.addMouseListener(eventHandler);
        }
        navigatorTreeScrollPane = new ScrollPagePane(searchCompTree);
        if ( session != null )
        {
            session.addAIFComponentEventListener(searchCompTree);
            try
            {
                TCPreferenceService imanpreferenceservice = session
                        .getPreferenceService();
                imanpreferenceservice.addPropertyChangeListener(searchCompTree);
                searchCompTree.setDisplayingOrder(imanpreferenceservice
                        .getString(1, "treeDisplayOrder"));
            }
            catch ( Exception exception )
            {
            }
        }
        findInDisplayPanel = searchCompTree.getFindInDisplayPanel(false, false);
        findInDisplayPanel.setBorder(BorderFactory.createLoweredBevelBorder());
        previousButton = navigatorTreeScrollPane.getPageUp();
        previousButton.setToolTipText(appReg.getString("previousButton.TIP"));
        previousButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                session.queueOperation(new AbstractAIFOperation(appReg
                        .getString("loading.MSG")) {
                    public void executeOperation() throws Exception
                    {
                        processSearch((TCComponentQuery) openedComponent,
                                names, values, -1);
                    }
                });
            }
        });
        nextButton = navigatorTreeScrollPane.getPageDown();
        nextButton.setToolTipText(appReg.getString("nextButton.TIP"));
        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                session.queueOperation(new AbstractAIFOperation(appReg
                        .getString("loading.MSG")) {
                    public void executeOperation() throws Exception
                    {
                        processSearch((TCComponentQuery) openedComponent,
                                names, values, 1);
                    }
                });
            }
        });
        allButton = navigatorTreeScrollPane.getLoadAll();
        allButton.setToolTipText(appReg.getString("allButton.TIP"));
        allButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionevent )
            {
                session.queueOperation(new AbstractAIFOperation(appReg
                        .getString("loading.MSG")) {
                    public void executeOperation() throws Exception
                    {
                        processSearch((TCComponentQuery) openedComponent,
                                names, values, 0);
                    }
                });
            }
        });
        // Modified for Property panel
        if ( tabbedPaneRequired )
        {
            System.out.println("tabbedPaneRequired: " + tabbedPaneRequired);
            String as2[] = getPropertyTableDisplayKey(openComponent);
            if ( openComponent instanceof TCComponentQuery )
            {
                queryResultTypeComp = getQueryResultType((TCComponentQuery) openComponent);
                propertyTable = new TCTable(session , as2[0] , as2[1] ,
                        queryResultTypeComp);
            }
            else
                propertyTable = new TCTable(session , "NavigatorColumnShown" ,
                        "NavigatorColumnWidths");
            ListSelectionModel listselectionmodel = propertyTable
                    .getSelectionModel();
            listselectionmodel.addListSelectionListener(eventHandler);
            propertyTable.addMouseListener(eventHandler);
            propertyTable.setEditable(true);
            if ( session != null )
                session.addAIFComponentEventListener(propertyTable);
           // infoCenter = new TCInformationCenter(smdlAPP);
            viewerPanel = new TCViewerPanel((AbstractTCApplication) smdlAPP);
            referencersPanel = new ReferencersPanel(smdlAPP , false , false ,
                    false , false);
            referencersPanel.addSelectReferencerNodeListener(this);
            JPanel jpanel = new JPanel();
            jpanel.setLayout(new HorizontalLayout(2));
            jpanel.setBorder(BorderFactory.createLoweredBevelBorder());
            findInDisplayText = new JTextField(10);
            findInDisplayText.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionevent )
                {
                    processFindInDisplay(findInDisplayText.getText());
                }
            });
            JButton jbutton = new JButton(appReg
                    .getImageIcon("findInDisplay.ICON"));
            jbutton.setMargin(new Insets(0 , 0 , 0 , 0));
            jbutton.setFocusPainted(false);
            jbutton.setToolTipText(appReg.getString("findInDisplay"));
            jbutton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionevent )
                {
                    processFindInDisplay(findInDisplayText.getText());
                }
            });
            mruButton = new MRUButton(session);
            mruButton.setSuggestedVerticalAlignment(1);
            jpanel.add("right.nobind", mruButton);
            jpanel.add("right.nobind", jbutton);
            jpanel.add("unbound.nobind", findInDisplayText);
            JPanel jpanel1 = new JPanel(new BorderLayout());
            jpanel1.setMinimumSize(new Dimension(0 , 0));
            jpanel1.add(navigatorTreeScrollPane, "Center");
            JTabbedPane jtabbedpane = new JTabbedPane(1);
            jtabbedpane.setMinimumSize(new Dimension(0 , 0));
            JPanel jpanel2 = new JPanel(
                    new com.teamcenter.rac.util.HorizontalLayout(15 , 5 , 2 , 2 , 2));
            jpanel2.setBorder(BorderFactory.createLoweredBevelBorder());
            tableLabelPart1 = new JLabel(appReg.getString("tableLabel"));
            tableLabelPart2 = new JLabel();
            jpanel2.add("left", tableLabelPart1);
            jpanel2.add("left", tableLabelPart2);
            JPanel jpanel3 = propertyTable.getThreshHoldManagerPanel();
            jpanel3.add("North", jpanel2);
            jtabbedpane.addTab(appReg.getString("propertiesTab"), appReg
                    .getImageIcon("propertiesTab.ICON"), jpanel3, appReg
                    .getString("propertiesTab.TIP", null));
            jtabbedpane.addTab(appReg.getString("browseTab"), appReg
                    .getImageIcon("browseTab.ICON"), viewerPanel, appReg
                    .getString("browseTab.TIP", null));
            jtabbedpane.addTab(appReg.getString("referencersTab"), appReg
                    .getImageIcon("referencersTab.ICON"), referencersPanel,
                    appReg.getString("referencersTab.TIP", null));
            splitPane = new SplitPane(0);
            splitPane.setLeftComponent(jpanel1);
            splitPane.setRightComponent(jtabbedpane);
            splitPane.setDividerLocation(0.25D);
            setLayout(new BorderLayout());
            add("Center", splitPane);
        }
        else
        {
            splitPane = new SplitPane(0);
            treePanel = new JPanel(new BorderLayout());
            treePanel.setMinimumSize(new Dimension(0 , 0));
            treePanel.add(navigatorTreeScrollPane, "Center");
            splitPane.setLeftComponent(treePanel);
            splitPane.setDividerLocation(0.25D);
            setLayout(new BorderLayout());
            add("Center", treePanel);
            String s1 = getComponentString(openComponent);
            double d = cookie.getDoubleNumber(s1 + "." + "SLIDER_LOCATION");
            if ( d > 0.01D ) splitPane.setDividerLocation(d);
        }
    }

    private String[] getPropertyTableDisplayKey(TCComponent imancomponent )
    {
        String as[] = new String[2];
        String s = null;
        if ( imancomponent instanceof TCComponentQuery )
            s = imancomponent.toString();
        else if ( imancomponent != null ) s = imancomponent.getType();
        if ( s != null && s.length() > 0 )
        {
            s = s + "_";
            as[0] = s + "ColumnPreferences";
            as[1] = s + "ColumnWidthPreferences";
        }
        else
        {
            as[0] = "InBoxColumnShow";
            as[1] = "InBoxColumnWidths";
        }
        return as;
    }

    private class EventHandler extends MouseAdapter implements
            TreeSelectionListener, ListSelectionListener
    {
        public void valueChanged(TreeSelectionEvent treeselectionevent )
        {
            try
            {
                final AIFComponentContext contexts[] = searchCompTree
                        .getSelectedContexts();
                if ( searchCompTree.getSelectionCount() >= 0 )
                {
                    smdlAPP.getSession().queueOperation(
                            new AbstractAIFOperation() {
                                public void executeOperation() throws Exception
                                {
                                    if ( contexts != null
                                            && contexts.length >= 2 )
                                    {
                                        tableLabelPart1.setText(appReg
                                                .getString("tableLabel"));
                                        tableLabelPart2.setText("");
                                        tableLabelPart2.setIcon(null);
                                        propertyTable.clearRoot();
                                        propertyTable.addRows(contexts);
                                        //infoCenter.setComponent(null);
                                    }
                                    else if ( contexts != null
                                            && contexts.length == 1
                                            && contexts[0].getComponent() != null )
                                    {
                                        tableLabelPart1
                                                .setText(appReg
                                                        .getString("tableLabelOneSelection"));
                                        System.out
                                                .println("Component in str format: "
                                                        + contexts[0]
                                                                .toString());
                                        tableLabelPart2.setText(contexts[0]
                                                .getComponent().toString());
                                        tableLabelPart2
                                                .setIcon(TCTypeRenderer
                                                        .getIcon(
                                                                contexts[0]
                                                                        .getComponent(),
                                                                false));
                                        propertyTable.setRoot(contexts[0]);
                                     /*   infoCenter
                                                .setComponent((TCComponent) contexts[0]
                                                        .getComponent());*/
                                    }
                                    else if ( searchCompTree.isRowSelected(0)
                                            && searchCompTree
                                                    .getSelectionCount() == 1
                                            && (openComponent instanceof TCComponentQuery) )
                                    {
                                        propertyTable.removeAllRows();
                                        propertyTable.filteringTable(
                                                new QueryCondition[1], true);
                                       TCTreeNode imantreenode = (TCTreeNode) searchCompTree
                                                .getRootNode();
                                        tableLabelPart1.setText(appReg
                                                .getString("tableLabel"));
                                        tableLabelPart2.setText(imantreenode
                                                .toString());
                                        tableLabelPart2.setIcon(getPanelIcon());
                                        int j = imantreenode.getChildCount();
                                        if ( j > 0 )
                                        {
                                            Vector vector = new Vector();
                                            for ( int k = 0; k < j; k++ )
                                            {
                                            	TCTreeNode imantreenode1 = (TCTreeNode) imantreenode
                                                        .getChildAt(k);
                                                AIFComponentContext aifcomponentcontext = imantreenode1
                                                        .getContextComponent();
                                                if ( aifcomponentcontext != null )
                                                    vector
                                                            .add(aifcomponentcontext);
                                            }
                                            if ( !vector.isEmpty() )
                                            {
                                                AIFComponentContext aaifcomponentcontext1[] = (AIFComponentContext[]) vector
                                                        .toArray(new AIFComponentContext[vector
                                                                .size()]);
                                                propertyTable
                                                        .addRows(aaifcomponentcontext1);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tableLabelPart1.setText(appReg
                                                .getString("tableLabel"));
                                        tableLabelPart2.setText("");
                                        tableLabelPart2.setIcon(null);
                                        propertyTable.clearRoot();
                                    }
                                }
                            });
                    if ( contexts != null && contexts.length >= 1 )
                    {
                        InterfaceAIFComponent ainterfaceaifcomponent[] = new InterfaceAIFComponent[contexts.length];
                        for ( int i = 0; i < contexts.length; i++ )
                            ainterfaceaifcomponent[i] = contexts[i]
                                    .getComponent();
                        if ( ainterfaceaifcomponent.length == 1 )
                            //viewerPanel.setComponent(ainterfaceaifcomponent[0]);
                            viewerPanel.setInput(ainterfaceaifcomponent[0]);//TC10.1 Upgrade
                        else
                            //viewerPanel.setComponents(ainterfaceaifcomponent);
                            viewerPanel.setInput(ainterfaceaifcomponent);//TC10.1 Upgrade
                        referencersPanel
                                .setComponent((TCComponent) ainterfaceaifcomponent[0]);
                    }
                    else
                    {
                        //viewerPanel.setComponent(null);
                        viewerPanel.setInput(null);//TC10.1 Upgrade
                        referencersPanel.setComponent(null);
                    }
                }
            }
            catch ( Exception exception )
            {
            }
        }

        public void valueChanged(ListSelectionEvent listselectionevent )
        {
            if ( !listselectionevent.getValueIsAdjusting() )
            {
                InterfaceAIFComponent ainterfaceaifcomponent[] = propertyTable
                        .getSelectedComponents();
                try
                {
                	TCComponent imancomponent = null;
                    InterfaceAIFComponent ainterfaceaifcomponent1[] = null;
                    if ( ainterfaceaifcomponent != null
                            && ainterfaceaifcomponent.length >= 1 )
                    {
                        imancomponent = (TCComponent) ainterfaceaifcomponent[0];
                        searchCompTree
                                .removeTreeSelectionListener(eventHandler);
                        searchCompTree.clearSelection();
                        searchCompTree.addTreeSelectionListener(eventHandler);
                        ainterfaceaifcomponent1 = ainterfaceaifcomponent;
                    }
                    else
                    {
                        AIFComponentContext aaifcomponentcontext[] = searchCompTree
                                .getSelectedContexts();
                        if ( aaifcomponentcontext != null
                                && aaifcomponentcontext.length >= 1 )
                            imancomponent = (TCComponent) aaifcomponentcontext[0]
                                    .getComponent();
                        ainterfaceaifcomponent1 = new InterfaceAIFComponent[aaifcomponentcontext.length];
                        for ( int i = 0; i < aaifcomponentcontext.length; i++ )
                            ainterfaceaifcomponent1[i] = aaifcomponentcontext[i]
                                    .getComponent();
                    }
                    if ( ainterfaceaifcomponent1 != null )
                        if ( ainterfaceaifcomponent1.length == 1 )
                            /*viewerPanel.setComponent(ainterfaceaifcomponent1[0]);*/
                            viewerPanel.setInput(ainterfaceaifcomponent1[0]);//TC10.1 Upgrade    
                        else
                            //viewerPanel.setComponents(ainterfaceaifcomponent1);
                            viewerPanel.setInput(ainterfaceaifcomponent1);//TC10.1 Upgrade
                    referencersPanel.setComponent(imancomponent);
                }
                catch ( Exception exception )
                {
                }
            }
        }

        public synchronized void mousePressed(MouseEvent mouseevent )
        {
            int i;
            InterfaceAIFComponent ainterfaceaifcomponent[] = null;
            i = mouseevent.getClickCount();
            if ( i != 1 || mouseevent.getSource() != propertyTable )
                ainterfaceaifcomponent = propertyTable.getSelectedComponents();
            if ( ainterfaceaifcomponent == null
                    || ainterfaceaifcomponent.length < 1 )
            {
                return;
            }
            TCComponent imancomponent = (TCComponent) ainterfaceaifcomponent[0];
            if ( i < 2 || mouseevent.getSource() != propertyTable )
                ainterfaceaifcomponent = propertyTable.getSelectedComponents();
            if ( ainterfaceaifcomponent == null
                    || ainterfaceaifcomponent.length < 1 )
            {
                return;
            }
            try
            {
                java.awt.Point point = mouseevent.getPoint();
                int j = propertyTable.rowAtPoint(point);
                int k = propertyTable.columnAtPoint(point);
                TCComponent imancomponent1 = (TCComponent) ainterfaceaifcomponent[0];
                if ( j >= 0 && k >= 0 && !propertyTable.isCellEditable(j, k) )
                {
                    TCSession imansession = (TCSession) smdlAPP
                            .getSession();
                    AbstractAIFCommand abstractaifcommand = imansession
                            .getOpenCommand(new Object[]
                            { smdlAPP.getDesktop(), imancomponent1 });
                    abstractaifcommand.executeModeless();
                }
            }
            catch ( Exception exception )
            {
            }
        }

        private EventHandler()
        {
            super();
        }
    }

    public TCComponent getTargetComponent()
    {
        AIFComponentContext aaifcomponentcontext[] = getTargetContexts();
        TCComponent imancomponent = null;
        if ( aaifcomponentcontext != null && aaifcomponentcontext.length > 0 )
        {
            int i = 0;
            do
            {
                if ( i >= aaifcomponentcontext.length ) break;
                if ( aaifcomponentcontext[i].getComponent() instanceof TCComponent )
                {
                    imancomponent = (TCComponent) aaifcomponentcontext[i]
                            .getComponent();
                    break;
                }
                i++;
            } while ( true );
        }
        return imancomponent;
    }

    public AIFComponentContext[] getTargetContexts()
    {
        AIFComponentContext aaifcomponentcontext[] = null;
        if ( referencersPanel.isVisible() )
        {
            aaifcomponentcontext = referencersPanel.getTargetContexts();
            AIFComponentContext aaifcomponentcontext1[] = searchCompTree
                    .getSelectedContexts();
            if ( aaifcomponentcontext1 != null && aaifcomponentcontext != null )
                aaifcomponentcontext = aaifcomponentcontext1;
        }
        if ( aaifcomponentcontext == null && propertyTable.isVisible() )
            aaifcomponentcontext = propertyTable.getSelectedContextObjects();
        if ( aaifcomponentcontext == null && viewerPanel.isVisible() )
            aaifcomponentcontext = viewerPanel.getTargetContexts();
        if ( aaifcomponentcontext == null )
            aaifcomponentcontext = searchCompTree.getSelectedContexts();
        return aaifcomponentcontext;
    }

    private void processFindInDisplay(String s )
    {
        TreeNode treenode = searchCompTree.findNode(s);
        if ( treenode != null )
        {
            DefaultTreeModel defaulttreemodel = (DefaultTreeModel) searchCompTree
                    .getModel();
            AIFTreeNode aiftreenode = (AIFTreeNode) treenode.getParent();
            if ( aiftreenode != null )
            {
                TreePath treepath = aiftreenode.getTreePath();
                if ( searchCompTree.isCollapsed(treepath) )
                    searchCompTree.expandPath(treepath);
            }
            TreeNode atreenode[] = defaulttreemodel.getPathToRoot(treenode);
            TreePath treepath1 = new TreePath(atreenode);
            searchCompTree.scrollPathToVisible(treepath1);
            searchCompTree.setSelectionPath(treepath1);
        }
        else
        {
            String s1 = appReg.getString("findInDisplay.NodeNotFound.TITLE");
            String s2 = appReg.getString("findInDisplay.NodeNotFound.MESSAGE");
            MessageBox messagebox = new MessageBox(s2 , s1 , 1);
            messagebox.setModal(true);
            messagebox.setVisible(true);
        }
    }

    private String getComponentString(TCComponent imancomponent )
    {
        if ( imancomponent == null ) return null;
        String s = "___inbox___";
        if ( !(imancomponent instanceof com.teamcenter.rac.kernel.TCComponentTaskInBox) )
            try
            {
                s = imancomponent.getSession().componentToString(
                        imancomponent);
            }
            catch ( Exception exception )
            {
            }
        return s;
    }
    
    protected int getTotalLimitPreference()
    {
        try
        {
            Vector vector = new Vector<Object>();/*session.getUserSessionInformation();*/
            Boolean boolean1 = (Boolean) vector.elementAt(3);
            if ( boolean1.booleanValue() ) return 0;
        }
        catch ( Exception exception )
        {
        }
        if ( totalLimitPreference < 0 )
            try
            {
                String s = session.getPreferenceService().getString(4,
                        "WSOM_find_set_total_limit");
                if ( s != null ) totalLimitPreference = Integer.parseInt(s);
            }
            catch ( Exception exception1 )
            {
            }
        return totalLimitPreference;
    }

    private void processSearch(TCComponentQuery imancomponentquery ,
            String as[] , String as1[] , int i )
    {
        try
        {
            int j = resultsIterator != null ? searchCompTree.getRootNode()
                    .getChildCount() : 0;
            if ( !isValidSearchResult(imancomponentquery, as, as1, true) )
            {
                return;
            }
            navigatorTreeScrollPane.setUpDownVisible(true);
            loadedObjs = null;
            String s = null;
            if ( resultsIterator != null )
            {
                if ( i == 0 )
                {
                    if ( getTotalLimitPreference() > 0 )
                        loadedObjs = resultsIterator
                                .toArray(getTotalLimitPreference());
                    else
                        loadedObjs = resultsIterator.toArray();
                    previousButton.setEnabled(false);
                    nextButton.setEnabled(false);
                    allButton.setEnabled(false);
                }
                else
                {
                    if ( i == -1 && resultsIterator.hasPrevious() )
                        loadedObjs = resultsIterator.previousPage();
                    else if ( i == 1 && resultsIterator.hasNextPage(j) )
                        if ( getTotalLimitPreference() > 0 )
                            loadedObjs = resultsIterator.nexPage(j,
                                    getTotalLimitPreference());
                        else
                            loadedObjs = resultsIterator.nexPage(j);
                    previousButton.setEnabled(resultsIterator.hasPrevious());
                    if ( getTotalLimitPreference() > 0 )
                        nextButton.setEnabled(resultsIterator.hasNextPage(
                                loadedObjs != null ? loadedObjs.length : 0,
                                getTotalLimitPreference()));
                    else
                        nextButton
                                .setEnabled(resultsIterator
                                        .hasNextPage(loadedObjs != null ? loadedObjs.length
                                                : 0));
                    allButton.setEnabled(previousButton.isEnabled()
                            || nextButton.isEnabled());
                }
                if ( loadedObjs == null )
                {
                    return;
                }
                s = getSearchRootName(i == 0, loadedObjs);
            }
            else
            {
               // s = "0 " + appReg.getString("rootNodeName");TCDECREL 2454
                s=appReg.getString("NoResultFound.MSG");
            }
            TCTreeNode imantreenode = new TCTreeNode(s);
            searchCompTree.setRoot(imantreenode);
            if ( loadedObjs == null )
            {
                return;
            }
            else
            {
                AbstractAIFOperation abstractaifoperation = new AbstractAIFOperation() {
                    public void executeOperation() throws Exception
                    {
                    	if(propertyTable != null)
                    	{
                    		propertyTable.clear();
                    		try
                    		{
                    			propertyTable.addRows(loadedObjs);
                    		}
                    		catch ( Exception exception )
                    		{
                    		}
                    	}
                    }
                };
                session.queueOperation(abstractaifoperation);
                return;
            }
        }
        catch ( Exception ex )
        {
            System.out.println("inside processSearch Exception");
            ex.printStackTrace();
        }
    }

    /**
     * getSearchRootName
     * 
     * @param flag
     * @param aobj
     * @return
     */
    protected String getSearchRootName(boolean flag , Object aobj[] )
    {
        String s = "";
        int i = 0;
        if ( resultsIterator != null )
        {
            s = resultsIterator.getListCount() + " "
                    + appReg.getString("rootNodeName");
            if ( !flag )
                i = resultsIterator.nextIndex() >= 0 ? resultsIterator
                        .nextIndex() : 0;
            i++;
            int j = 0;
            if ( aobj != null )
            {
                j = aobj.length;
            }
            else
            {
                TCTreeNode imantreenode = (TCTreeNode) searchCompTree
                        .getRootNode();
                j = imantreenode.getChildCount();
            }
            int k = (i + j) - 1;
            s = i + " ~ " + k + " / " + resultsIterator.getListCount();
            s = getComponentToOpen() + "( " + s + " )";
        }
        return s;
    }

    /**
     * @param imancomponentquery
     * @return
     */
    private TCComponentType getQueryResultType(
    		TCComponentQuery imancomponentquery )
    {
        TCComponentType imancomponenttype = null;
        try
        {
            String s = imancomponentquery
                    .getProperty(TCComponentQuery.PROP_QUERY_CLASS);
            String s1 = imancomponentquery
                    .getProperty(TCComponentQuery.PROP_QUERY_FLAG);
            if ( s.equals("PublicationRecord") )
                s = "PublishedObject";
            else if ( s1.equals("8") || s1.equals("32") )
            {
                s = imancomponentquery.getProperty("query_name");
                if ( s.length() < 32 )
                    s = s + "_";
                else
                    s = s.substring(0, 31) + "_";
                s = s.replace('.', '_');
            }
            else if ( TCICSClass.trimICSClassName(s) != null ) s = "icm0";
            /*imancomponentquery.getSession().getTypeService()
                    .initTypeInfoByNames(new String[]
                    { s });*/
            
            imancomponentquery.getSession().getTypeService().getObjectTypeUidsForQueryResult(
                    new String[]{ s }, imancomponentquery );//TC10.1 Upgrade
            
            imancomponenttype = imancomponentquery.getSession()
                    .getTypeComponent(s);
        }
        catch ( TCException imanexception )
        {
            imancomponenttype = null;
        }
        return imancomponenttype;
    }

    /**
     * @param imancomponentquery
     * @param as
     * @param as1
     * @param flag
     * @return
     */
    public boolean isValidSearchResult(TCComponentQuery imancomponentquery ,
            String as[] , String as1[] , boolean flag )
    {
        boolean flag1 = true;
        if ( resultsIterator == null )
        {
            try
            {
                if ( criteriaPanel != null
                        && criteriaPanel.getLimitListTagCnt() > 0 )
                {
                    String as2[] = criteriaPanel.getLimitListTags();
                    queryResults = imancomponentquery.getExecuteResultsList(as,
                            as1, as2);
                }
                else
                {
                    try
                    {
                        if ( null != as && null != as1
                                && imancomponentquery != null )
                            queryResults = imancomponentquery
                                    .getExecuteResultsList(as, as1);
                    }
                    catch ( Exception ex )
                    {
                        return false;
                    }
                }
                if ( criteriaPanel != null )
                    criteriaPanel.storeQueryToCookie();
            }
            catch ( final TCException ex )
            {
                if ( flag ) SwingUtilities.invokeLater(new Runnable() {
                    public void run()
                    {
                        MessageBox.post(smdlAPP.getDesktop(), ex);
                    }
                });
                return false;
            }
            if ( queryResults != null && queryResults.getListCount() > 0 )
            {
                if ( queryResultTypeComp == null && imancomponentquery != null )
                {
                    queryResultTypeComp = getQueryResultType(imancomponentquery);
                }
                resultsIterator = queryResults.getIterator();
                resultsIterator.setPageSize(getLoadLimitPreference());
                if ( getTotalLimitPreference() > 0
                        && queryResults.getListCount() > getTotalLimitPreference() )
                    MessageBox.post(smdlAPP.getDesktop(), queryResults
                            .getListCount()
                            + " "
                            + appReg.getString("search.siteLimitMSG1")
                            + " \n"
                            + appReg.getString("search.siteLimitMSG2")
                            + " "
                            + totalLimitPreference
                            + "\n"
                            + totalLimitPreference
                            + " "
                            + appReg.getString("search.siteLimitMSG3"), appReg
                            .getString("search.siteLimitMSG4"), appReg
                            .getString("search.TITLE"), 1);
                try
                {
                    if ( theTuplesArray != null )
                    {
                        int ai[] = new int[2];
                        String as3[] = null;
                        if ( criteriaPanel != null )
                            as3 = criteriaPanel.getLimitListTags();
                        if ( as3 != null )
                            theTuplesArray = imancomponentquery
                                    .execute_tuples_noload(as, as1, as3, ai);
                        else
                            theTuplesArray = imancomponentquery
                                    .execute_tuples_noload(as, as1, ai);
                        numTuples = ai[0];
                        numInTuple = ai[1];
                    }
                }
                catch ( Exception exception )
                {
                }
            }
            else
            {
            	//TCDECREL 2454
            	return true;
            }
        }
        if ( resultsIterator == null )
            flag1 = false;
        else
            flag1 = true;
        return flag1;
    }

    /**
     * setPanelIcon
     * 
     * @param icon
     */
    public void setPanelIcon(Icon icon )
    {
        panelIcon = icon;
    }

    /**
     * @return
     */
    protected int getLoadLimitPreference()
    {
        if ( loadLimitPreference < 0 )
            try
            {
                session.getPreferenceService().getStrings(
                        new int[]
                        { 4, 1 },
                        new String[]
                        { "WSOM_find_set_total_limit",
                                AbstractOptionPanel.SEARCH_LIMIT_PREF });
                String s = session.getPreferenceService().getString(1,
                        AbstractOptionPanel.SEARCH_LIMIT_PREF);
                if ( s != null ) loadLimitPreference = Integer.parseInt(s);
            }
            catch ( Exception exception )
            {
            }
        return loadLimitPreference <= 0 ? 30 : loadLimitPreference;
    }

    /**
     * addLimitContent
     * 
     * @param s
     * @return
     */
    private String addLimitContent(String s )
    {
        String s1 = s + "The Search was limited by: \n";
        int i = criteriaPanel.getLimitListClipboardCnt();
        if ( i > 0 )
            s1 = s1 + "   " + Integer.toString(i) + " from Clipboard\n";
        i = criteriaPanel.getLimitListApplnsCnt();
        if ( i > 0 )
            s1 = s1 + "   " + Integer.toString(i) + " from Applications\n";
        i = criteriaPanel.getLimitListPriorSrchCnt();
        if ( i > 0 )
            s1 = s1 + "   " + Integer.toString(i) + " from Prior search\n";
        i = criteriaPanel.getLimitListRefersCnt();
        if ( i > 0 )
            s1 = s1 + "   " + Integer.toString(i) + " from Referencers \n";
        i = criteriaPanel.getLimitListWorkflowCnt();
        if ( i > 0 )
            s1 = s1 + "   " + Integer.toString(i) + " from Workflow \n";
        i = criteriaPanel.getLimitListPSECnt();
        if ( i > 0 )
            s1 = s1 + "   " + Integer.toString(i) + " from Workflow \n";
        return s1;
    }

    /**
     * setLimitContentLists
     */
    private void setLimitContentLists()
    {
        limitClip = criteriaPanel.getLimitListClipboard();
        limitAppl = criteriaPanel.getLimitListApplns();
        limitPrior = criteriaPanel.getLimitListPriorSrchTags();
        limitRefrs = criteriaPanel.getLimitListRefers();
        limitWkfl = criteriaPanel.getLimitListWorkflow();
        limitPse = criteriaPanel.getLimitListPSETags();
    }
    public void close()
    {}
    public void setSearchCriteriaPanel(
            NOVSMDLSearchCriteriaPanel searchcriteriapanel )
    {
        if ( searchcriteriapanel != null )
        {
            criteriaPanel = searchcriteriapanel;
            openComponent = searchcriteriapanel.getQuery();
            openedComponent = null;
            Vector vector = new Vector();
            Vector vector1 = new Vector();
            searchcriteriapanel.getEntryValues(vector, vector1);
            names = (String[]) vector.toArray(new String[vector.size()]);
            values = (String[]) vector1.toArray(new String[vector1.size()]);
            setLimitContentLists();
            adhocQueryClauses = searchcriteriapanel.getAdhocQueryClauses();
            openComponent();
            String s1 = appReg.getString("queryTable.qname") + " = ";
            try
            {
                s1 = s1
                        + ((TCComponentQuery) openComponent).getProperty(
                                "query_name").toString() + "\n";
            }
            catch ( Exception exception )
            {
            }
            for ( int i = 0; i < names.length; i++ )
                s1 = s1 + names[i] + " = " + values[i] + "\n";
        }
    }

    public synchronized void openComponent()
    {
        if ( openComponent != null && openComponent != openedComponent )
        {
            openedComponent = openComponent;
            if ( openComponent instanceof TCComponentQuery )
            {
                smdlAPP.getSession().queueOperation(
                        new AbstractAIFOperation(appReg.getString("search.MSG")
                                + openComponent) {
                            public void executeOperation() throws Exception
                            {
                                boolean flag = false;
                                SystemMonitorInfo systemmonitorinfo = new SystemMonitorInfo();
                                try
                                {
                                    if ( null != searchCompTree )
                                    {
                                        resultsIterator = null;
                                        /*TCTreeNode imantreenode = new TCTreeNode(
                                                startMessage);*/
                                        TCTreeNode imantreenode = new TCTreeNode(
                                                getStartMessage());//TC10.1 Upgrade
                                        searchCompTree.setRoot(imantreenode);
                                        searchCompTree.validate();
                                        searchCompTree.repaint();
                                        processSearch(
                                                (TCComponentQuery) openComponent,
                                                names, values, 1);
                                        
                                        setToolTip();
                                        Tab tab1 = iTabbedPane
                                                .getComponentTab(NOVSearchResultsPanel.this);
                                        if ( tab1 != null )
                                            if ( criteriaPanel != null
                                                    && criteriaPanel
                                                            .hasTargetList() )
                                                tab1
                                                        .setIcon(appReg
                                                                .getImageIcon("targetQuery.ICON"));
                                            else
                                                tab1.setIcon(getPanelIcon());
                                    }
                                    if ( flag )
                                    {
                                        String s1 = "";
                                        if ( queryResults != null )
                                        {
                                            for ( int k = 0; k < values.length - 1; k++ )
                                                s1 = s1 + values[k] + ";";
                                            s1 = s1 + values[values.length - 1];
                                        }
                                        systemmonitorinfo.getMetricvalues()[0]
                                                .numericid32(0);
                                        systemmonitorinfo.getMetricvalues()[1]
                                                .counter32(queryResults
                                                        .getListCount());
                                        /*systemmonitorinfo.getString32()
                                                .string_value(s1.getBytes());*/
                                        systemmonitorinfo.setString32(s1);//TC10.1 Upgrade
                                    }
                                }
                                catch ( Exception exception2 )
                                {
                                    exception2.printStackTrace();
                                }
                            }
                            private static final long serialVersionUID = 1L;
                        });
            }
        }
    }

    public void setToolTip()
    {
        Tab tab = iTabbedPane.getComponentTab(this);
        if ( tab != null ) tab.setToolTipText(getToolTip());
    }

    public boolean isLoaded()
    {
        return getComponentToOpen() == getOpenedComponent();
    }

    public TCComponent getOpenedComponent()
    {
        return openedComponent;
    }

    public String getToolTip()
    {
        String s = null;
        if ( openComponent instanceof TCComponentQuery )
        {
            String s1 = appReg.getString("status");
            s = "<P align=center><STRONG><EM><FONT size=3>" + openComponent
                    + "</FONT></EM></STRONG><br>";
            if ( isLoaded() )
                s = s
                        + "<STRONG><FONT size=2>"
                        + s1
                        + "&nbsp;"
                        + (resultsIterator != null ? resultsIterator
                                .getListCount() : 0) + " "
                        + appReg.getString("foundObjects") + "</FONT></STRONG>";
            else
                s = s + "<STRONG><FONT size=2>" + s1 + "&nbsp;"
                        + appReg.getString("notExecuted") + "</FONT></STRONG>";
            s = s + "<TABLE>";
            if ( names != null && names.length > 0 )
            {
                for ( int i = names.length - 1; i >= 0; i-- )
                    s = s
                            + "<TR><TD><P align=right><FONT size=2>"
                            + names[i]
                            + "</FONT></P></TD><TD><FONT size=2>=</FONT></TD><TD><FONT size=2>"
                            + values[i] + "</FONT></TD></TR>";
            }
            s = s + "</TABLE>";
            if ( criteriaPanel != null
                    && criteriaPanel.getLimitListTagCnt() > 0 )
            {
                s = s
                        + "<P align=center><STRONG><EM><FONT size=3>The Search was limited by:</FONT></EM></STRONG>";
                s = s + "<TABLE>";
                int j = criteriaPanel.getLimitListClipboardCnt();
                if ( j > 0 )
                    s = s + "<TR><TD><P align=right><FONT size=2>   "
                            + Integer.toString(j) + " objects"
                            + " from Clipboard </FONT></TD></TR>";
                j = criteriaPanel.getLimitListApplnsCnt();
                if ( j > 0 )
                    s = s + "<TR><TD><P align=right><FONT size=2>   "
                            + Integer.toString(j) + " objects"
                            + " from Application </FONT></TD></TR>";
                j = criteriaPanel.getLimitListPriorSrchCnt();
                if ( j > 0 )
                    s = s + "<TR><TD><P align=right><FONT size=2>   "
                            + Integer.toString(j) + " objects"
                            + " from Prior Search </FONT></TD></TR>";
                j = criteriaPanel.getLimitListRefersCnt();
                if ( j > 0 )
                    s = s + "<TR><TD><P align=right><FONT size=2>   "
                            + Integer.toString(j) + " objects"
                            + " from Referencers </FONT></TD></TR>";
                j = criteriaPanel.getLimitListWorkflowCnt();
                if ( j > 0 )
                    s = s + "<TR><TD><P align=right><FONT size=2>   "
                            + Integer.toString(j) + " objects"
                            + " from Workflows </FONT></TD></TR>";
                j = criteriaPanel.getLimitListPSECnt();
                if ( j > 0 )
                    s = s + "<TR><TD><P align=right><FONT size=2>   "
                            + Integer.toString(j) + " objects"
                            + " from PSE </FONT></TD></TR>";
                s = s + "</TABLE>";
            }
            s = "<html><body>" + s + "</P></body></html>";
        }
        return s;
    }
    private void populateSelectedDocument()
    {
    	TCComponent selectedDocComponent = null;
        try
        {
            if ( searchCompTree != null )
            {
            	NOVSMDLNewEditDialog docEditDialog = getDocumentEditDialog(searchCompTree);
                selectedDocComponent = searchCompTree.getSelectedTCComponent();
                if(docEditDialog != null)
                {
                	docEditDialog.validateSelectedComponent(selectedDocComponent);
                }
            }
        }
        catch ( Exception exception )
        {
            exception.printStackTrace();
        }
    }

    public void valueChanged(TreeSelectionEvent arg0 )
    {
    	if(bIs_MousePressed == false)
		{
    		populateSelectedDocument();
		}
    	bIs_MousePressed = false;
    }

    public void mouseClicked(MouseEvent arg0 )
    {
        // TODO Auto-generated method stub
    }

    public void mouseEntered(MouseEvent arg0 )
    {
        // TODO Auto-generated method stub
    }

    public void mouseExited(MouseEvent arg0 )
    {
        // TODO Auto-generated method stub
    }

    public void mousePressed(MouseEvent mouseevent )
    {
        int i = mouseevent.getModifiers();
        if ( i == 4 )
        {
            searchCompTree.removeTreeSelectionListener(this);
        }
        bIs_MousePressed = true;
        populateSelectedDocument();
    }

    public void mouseReleased(MouseEvent arg0 )
    {
        // TODO Auto-generated method stub
        searchCompTree.addTreeSelectionListener(this);
        bIs_MousePressed = false;
    }

    public NOVSMDLSearchCriteriaPanel getSearchCriteriaPanel()
    {
        if ( openComponent instanceof TCComponentQuery )
        {
            if ( criteriaPanel != null )
            {
                criteriaPanel.initializeCriteria(names, values,
                        adhocQueryClauses);
                criteriaPanel.setLimitListClipboard(limitClip);
                criteriaPanel.setLimitListApplns(limitAppl);
                criteriaPanel.setLimitListPriorSrch(limitPrior);
                criteriaPanel.setLimitListRefers(limitRefrs);
                criteriaPanel.setLimitListWorkflow(limitWkfl);
                criteriaPanel.setLimitListPSE(limitPse);
                criteriaPanel.showLimitSearchSelections();
            }
            else
            {
                criteriaPanel = new NOVSMDLSearchCriteriaPanel(
                        (TCComponentQuery) openComponent , names , values);
                criteriaPanel.setLimitListClipboard(limitClip);
                criteriaPanel.setLimitListApplns(limitAppl);
                criteriaPanel.setLimitListPriorSrch(limitPrior);
                criteriaPanel.setLimitListRefers(limitRefrs);
                criteriaPanel.setLimitListWorkflow(limitWkfl);
                criteriaPanel.setLimitListPSE(limitPse);
                criteriaPanel.showLimitSearchSelections();
            }
            return criteriaPanel;
        }
        else
        {
            return null;
        }
    }
    public void setName(String s)
    {
        panelTitle = s;
        Tab tab = iTabbedPane.getComponentTab(this);
        if (tab != null)
            tab.setText(s);
    }

    /**
     * 
     * getPreferenceSavingInfo
     * @return
     */
    public Object[] getPreferenceSavingInfo()
    {
        System.out.println();
        if (!(openComponent instanceof TCComponentQuery))
            return null;
        iTabbedPane itabbedpane = iTabbedPane.getTabbedPane(this);
        int i = 0;
        for (int j = itabbedpane.indexOfComponent(this) - 1; j >= 0; j--)
        { 
            Component component = itabbedpane.getComponentAt(j);
            if (component instanceof NOVSearchResultsPanel && (((NOVSearchResultsPanel)component)
                    .getComponentToOpen() instanceof TCComponentQuery))
                i++;
        }
        Object aobj[] = null;
        try
        {
            String s = session.getUser().getUserId() + "_query_NOVSMDL_" + i;
            String s1 = s + "_names";
            String s2 = s + "_values";
            String as[] = { session.componentToString(openComponent),panelTitle };
            String as1[] = names;
            String as2[] = values;
            if (names != null)
            {
                int k = -1;
                int l = 0;
                do
                {
                    if (l >= names.length)
                        break;
                    if (names[l]
                            .equalsIgnoreCase(TCComponentQuery.ADHOC_ICSCLASS_ENTRY))
                    {
                        k = l;
                        break;
                    }
                    l++;
                }
                while (true);
                if (k >= 0)
                {
                    as1 = new String[k];
                    as2 = new String[k];
                    System.arraycopy(names, 0, as1, 0, k);
                    System.arraycopy(values, 0, as2, 0, k);
                }
            }
            String as3[] = { s, s1, s2 };
            Object aobj1[] = { as, as1, as2 };
            aobj = (new Object[] { as3, aobj1 });
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        return aobj;
    }

    /**
     * 
     * saveQuery
     * @return
     * @throws IMANException
     */
    public boolean saveQuery() throws TCException
    {
        boolean flag = openComponent instanceof TCComponentQuery;
        if (!flag)
            return flag;
        iTabbedPane itabbedpane = iTabbedPane.getTabbedPane(this);
        int i = 0;
        for (int j = itabbedpane.indexOfComponent(this) - 1; j >= 0; j--)
        {
            if (!(((NOVSearchResultsPanel) itabbedpane.getComponentAt(j))
                    .getComponentToOpen() instanceof TCComponentQuery))
                continue;
            if(null!=((NOVSearchResultsPanel) itabbedpane.getComponentAt(j))
                    .getSearchCriteriaPanel())
                if (((NOVSearchResultsPanel) itabbedpane.getComponentAt(j))
                    .getSearchCriteriaPanel().hasTargetList())
                    return flag;
            i++;
        }
        String s1 = session.getUser().getUserId() + "_query_NOVSMDL" + i;
        String s2 = s1 + "_names";
        String s3 = s1 + "_values";
        TCPreferenceService imanpreferenceservice = session
                .getPreferenceService();
        String as[] = { session.componentToString(openComponent), toString() };
        imanpreferenceservice.setStringArray(1, s1, as);
        imanpreferenceservice.setStringArray(1, s2, names);
        imanpreferenceservice.setStringArray(1, s3, values);
        return flag;
    }

    private static final long serialVersionUID = 1L;

    public void processSelectReferencerNode(SelectReferencerNodeEvent arg0 )
    {
        // TODO Auto-generated method stub
    }
    private NOVSMDLNewEditDialog getDocumentEditDialog(Component comp)
	{
    	Component parent= null;
		Component component = comp.getParent();
		do 
		{			
			parent=	component.getParent();
			component=parent;	
		}while (parent!=null && ! (parent instanceof NOVSMDLNewEditDialog ));
		
		return (NOVSMDLNewEditDialog)parent;
	}
    public TCTree                           searchCompTree      = null;
    protected ScrollPagePane                  navigatorTreeScrollPane;
    protected TCTree                        itemRevisionTree;
    protected TCViewerPanel                 viewerPanel;
    protected TCViewerPanel                 formViewerPanel;
    protected ReferencersPanel                referencersPanel;
    protected JPanel                          treePanel;
    protected JPanel                          findInDisplayPanel;
    protected com.teamcenter.rac.util.SplitPane                       splitPane;
    protected TCComponent                   openComponent;
    protected TCSession                     session;
    TCComponent                             limitClip[];
    TCComponent                             limitAppl[];
    String                                    limitPrior[];
    TCComponent                             limitRefrs[];
    TCComponent                             limitWkfl[];
    String                                    limitPse[];
    protected AIFTreeNode                     openCompRootNode;
    protected TCComponent                   openedComponent;
    protected String                          names[];
    protected String                          values[];
    TCComponent                             theTuples[];
    String                                    theTuplesArray[];
    int                                       numTuples;
    int                                       numInTuple;
    int                                       numTuplesDone;
    protected NOVSMDLSearchCriteriaPanel       criteriaPanel;
    protected com.teamcenter.rac.aif.kernel.AIFComponentContextListIterator resultsIterator;
    protected int                             totalLimitPreference;
    protected JButton                         previousButton;
    protected JButton                         nextButton;
    protected JButton                         allButton;
    protected String                          panelTitle;
    protected Icon                            panelIcon;
    protected AIFComponentContext             loadedObjs[];
    public static boolean                     shown               = false;
    public boolean                            isdataset           = false;
    protected AbstractAIFUIApplication        smdlAPP;
    protected DisplayDataTable                displaydataTable;
    protected ScrollPagePane                  dataDisplayScrollPane;
    protected JPanel                          innerPanel;
    protected JPanel                          buttonPanel;
    protected JButton                         adHocButton;
    protected TCComponentPff                  selectedPff;
    protected JButton                         dd_previousButton;
    protected JButton                         dd_nextButton;
    protected JButton                         dd_allButton;
    protected boolean                         adHocState;
    protected JLabel                          dispalyTableSumery;
    protected TCComponentContextList        queryResults;
    private TCAdhocQueryClause              adhocQueryClauses[];
    private TCComponentType                 queryResultTypeComp;
    public static int                         loadLimitPreference = -1;
    protected Registry                        appReg              = null;
    public static com.teamcenter.rac.util.Cookie cookie  = Cookie.getCookie("SMDL",true);
    private TCTable                         propertyTable;
    private MRUButton                         mruButton;
    private JTextField                        findInDisplayText;
    private JLabel                            tableLabelPart1;
    private JLabel                            tableLabelPart2;
    private EventHandler                      eventHandler;
    private boolean 						  bIs_MousePressed = false;

}
