package com.nov.rac.smdl.panes.addnewcode;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.nov.rac.smdl.panes.addnewcode.NOVSearchPanel;
import com.nov.rac.smdl.panes.addnewcode.NOVSearchResultsPanel;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.InterfaceAIFOperationListener;
import com.teamcenter.rac.aif.kernel.AIFComponentEvent;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponentEventListener;
import com.teamcenter.rac.common.findwithproperties.TCAdhocQueryClause;
import com.teamcenter.rac.explorer.common.AbstractOptionPanel;
import com.teamcenter.rac.explorer.common.OptionPopupButton;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.HorizontalLayout;
import com.teamcenter.rac.util.HyperLink;
import com.teamcenter.rac.util.InterfaceSignalOnClose;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.iTextField;

public class NOVSMDLSearchPanel extends JPanel implements
        InterfaceSignalOnClose, InterfaceAIFComponentEventListener,
        InterfaceAIFOperationListener, PropertyChangeListener
{
    public NOVSMDLSearchPanel(TCSession session, JFrame f)
    {
        this(session , f , true , true , true);
    }

    public NOVSMDLSearchPanel(TCSession session, JFrame f, boolean addBanner,
            boolean addButtons, boolean addOptionButton)
    {
        this(session , f , addBanner , addButtons , addOptionButton , null);
    }

    public NOVSMDLSearchPanel(TCSession session, JFrame f, boolean addBanner,
            boolean addButtons, boolean addOptionButton,
            String[] savedQueryClassNames)
    {
        super(new VerticalLayout(2 , 0 , 0 , 0 , 0));
        this.session = session;
        this.desktop = f;
        initPanel(addBanner, addButtons, addOptionButton, savedQueryClassNames);
        getAndLoadQuery();
    }

    private void initPanel(boolean addBanner , boolean addButtons ,
            boolean addOptionButton , String[] queryClassNames )
    {
        // SS changes this to final for SDLC 1379
        final Registry r = Registry.getRegistry(this);
        loadingFormMsg = r.getString("buildQueryForm.MESSAGE") + " ";
        timeIcon = r.getImageIcon("time.ICON");
        setOpaque(false);
        setBackground(Color.white);
        loadingStatusPanel = new JPanel(new BorderLayout()) {
            /**
             * 
             */
            private static final long serialVersionUID = 1L;

            public void paintComponent(Graphics g )
            {
                super.paintComponent(g);
            }
        };
        loadingStatusPanel.setOpaque(false);
        if ( addBanner )
        {
            banner = new NOVSMDLSearchBanner();
        }
        selectionButton = new NOVSMDLQuerySelectionButton(session ,queryClassNames);
        
        criteriaScroll = new JScrollPane();
        JPanel buttonPanel = new JPanel(new HorizontalLayout(1 , 2 , 2 , 0 , 0));
        if ( addButtons )
        {
            buttonPanel.add("unbound.nobind", selectionButton);
            // SS adds this for SDLC 1379
            final JButton lockButton = AbstractOptionPanel.createButton(r.getImageIcon("lockButton.ICON"), 
            															r.getString("lockButton.TIP"));
            lockButton.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e )
                {
                    if ( criteriaPanel != null )
                    {
                        if ( isLocked == false )
                        {
                            isLocked = true;
                            lockButton.setToolTipText(r.getString("locked.TIP"));
                        }
                        else if ( isLocked == true )
                        {
                            isLocked = false;
                            lockButton.setToolTipText(r.getString("unlocked.TIP"));
                        }
                    }
                }
            });
            // END ss
            executionButton = AbstractOptionPanel.createButton(r.getImageIcon("executionButton.ICON"), 
            												   r.getString("executionButton.TIP"));
            executionButton.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e )
                {
                    if ( criteriaPanel != null )
                    {
                        criteriaPanel.storeQueryToCookie();
                        criteriaPanel.updateICSClassQueryClauses();
                    }
                    searchAction();
                }
            });
            refreshButton = AbstractOptionPanel.createButton(r.getImageIcon("refreshButton.ICON"), 
            												 r.getString("refreshButton.TIP"));
            refreshButton.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e )
                {
                    if ( criteriaPanel != null )
                    {
                        criteriaPanel.storeQueryToCookie();
                        criteriaPanel.updateICSClassQueryClauses();
                    }
                    refreshAction();
                }
            });
            JButton clearButton = AbstractOptionPanel.createButton(r.getImageIcon("clearButton.ICON"), 
            													   r.getString("clearButton.TIP"));
            clearButton.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e )
                {
                    if ( criteriaPanel != null )
                    {
                        criteriaPanel.clear();
                    }
                }
            });
            buttonPanel.add("right.nobind", clearButton);
            buttonPanel.add("right.nobind", refreshButton);
            buttonPanel.add("right.nobind", executionButton);
            // SS adds this for SDLC 1379
            buttonPanel.add("right.nobind", lockButton);
            // End ss
        }
        else
        {
            buttonPanel.add("left.nobind", selectionButton);
        }
        JPanel optionPanel = null;
        if ( addOptionButton )
        {
            optionPanel = new JPanel(new HorizontalLayout(1 , 2 , 2 , 0 , 0));
            // in-CLASS license checking
            advancedLabel = new HyperLink(r.getString("advancedQuery.LABEL", "Advanced..."));
            advancedLabel.setEnabled(false);
            advancedLabel.setUnderLine(false);
            advancedLabel.setToolTipText(r.getString("advancedQuery.TIP"));
            advancedLabel.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e )
                {
                    if ( criteriaPanel != null )
                    {
                        criteriaPanel.showICSClassQueryDialog(desktop);
                    }
                }
            });
            optionButton = new OptionPopupButton(session);
            editButton = new NOVSMDLEditPopupButton((AIFDesktop) desktop , session);
            // sdlc1254-b adds tooltip...
            editButton.setToolTipText(r.getString("editbutton.TIP"));
            optionPanel.add("left.nobind", advancedLabel);
            optionPanel.add("right.nobind", optionButton);
            // sdlc1254-b - only allow special people to edit!
            if ( isAllowedToEdit() )
            {
                optionPanel.add("right.nobind", editButton);
            }
        }
        if ( banner != null )
        {
            add("top.bind", banner);
        }
        add("top.bind", buttonPanel);
        add("unbound", criteriaScroll);
        if ( addOptionButton )
        {
            add("bottom.bind", optionPanel);
        }
        setPreferredSize(new Dimension(150 , 100));
        // sdlc1254 adds registration as an event listener.
        // this allows updates when queries are altered/deleted
        session.addAIFComponentEventListener(this);
        // pr4809537 introduces a preference listener
        try
        {
            TCPreferenceService pref = session.getPreferenceService();
            pref.addPropertyChangeListener(this);
        }
        catch ( Exception ie )
        {
            ie.printStackTrace();
            // on err do nothing - if things go stale, no biggie
        }
        if ( editButton != null )
        {
            setSuggestedNameIndex(); // sdlc1254-b will suggest names for the
                                        // user...
            editButton.setSuggestedName(getSuggestedName());
        }
    }
    
    /**
     * The methods to load the query
     * 
     * @param query -
     *            The query to construct the panel
     * @return None
     */
    public void loadQuery(TCComponentQuery query )
    {
        if ( query == null )
        {
            // System.out.println( "Query is null" );
            return;
        }
        loadingStatusPanel.removeAll();
        loadingStatusPanel.setLayout(new VerticalLayout(2 , 5 , 5 , 10 , 10));
        loadingStatusPanel.add("top", new JLabel(loadingFormMsg
                + query.toString() , timeIcon , JLabel.CENTER));
        loadingStatusPanel.revalidate();
        criteriaScroll.setViewportView(loadingStatusPanel);
        criteriaPanel = new NOVSMDLSearchCriteriaPanel(query);
        criteriaPanel.setButtonListener(new ActionListener() {
            public void actionPerformed(ActionEvent e )
            {
                 if ( appPanel1 != null )
                {
                    Component comp = null;
                    NOVSearchResultsPanel panel = null;
                    if ( appPanel1 != null )
                    {
                        comp = appPanel1.getVisiblePanel();
                        panel = (appPanel1 != null) ? ((comp instanceof NOVSearchResultsPanel) ? (NOVSearchResultsPanel) comp
                                : null)
                                : null;
                    }
                    TCComponent comp1 = panel == null ? null : panel
                            .getComponentToOpen();
                    TCComponent comp2 = criteriaPanel.getQuery();
                    boolean b = comp1 != null && comp2 != null
                            && comp1.equals(comp2);
                    if ( b && refreshButton != null )
                    {
                        refreshButton.doClick();
                    }
                    else if ( executionButton != null )
                    {
                        executionButton.doClick();
                    }
                }
            }
        });
        criteriaScroll.setViewportView(criteriaPanel);
        if ( optionButton != null )
        {
            optionButton.setQuery(query);
        }
        if ( editButton != null )
        {
            String sugName = getSuggestedName();
            editButton.setQuery(query, sugName); // sdlc1254-b adds suggested
                                                    // name
        }
        // IMAN_classification grm checking
        if ( advancedLabel != null )
        {
            advancedLabel.setEnabled(enableAdvanced(query));
        }
        criteriaPanel.requestFocus();
    }

    /**
     * The methos to unload the query We need to do this if we detect that the
     * query currently loaded has been deleted sdlc1254 adds this method
     * 
     * @return None
     */
    public void unLoadQuery()
    {
        loadingStatusPanel.removeAll();
        loadingStatusPanel.setLayout(new VerticalLayout(2 , 5 , 5 , 10 , 10));
        loadingStatusPanel.revalidate();
        criteriaScroll.setViewportView(loadingStatusPanel);
        criteriaPanel = null;
    }

    /**
     * Return a reference to the query selection button.
     */
    public NOVSMDLQuerySelectionButton getQuerySelectionButton()
    {
        return (selectionButton);
    }

    private boolean enableAdvanced(TCComponentQuery query )
    {
        boolean enable = false;
        try
        {
            String queryFlag = query
                    .getProperty(TCComponentQuery.PROP_QUERY_FLAG);
            if ( queryFlag.equals(TCComponentQuery.QRY_DOMAIN_LOCAL) )
            {
                enable = true;
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
        return enable;
    }

    /**
     * Method to set the criteria panel
     * 
     * @param panel -
     *            The new criteria panel
     * @return None
     */
    public void setCriteriaPanel(NOVSMDLSearchCriteriaPanel panel )
    {
        if ( panel != null )
        {
            final ActionListener l = new ActionListener() {
                public void actionPerformed(ActionEvent e )
                {
                    if ( executionButton != null )
                    {
                        Component comp = appPanel1.getVisiblePanel();
                        if ( comp instanceof NOVSearchResultsPanel )
                        {
                            NOVSearchResultsPanel panel = null;
                            if ( appPanel1 != null )
                                panel = (NOVSearchResultsPanel) appPanel1.getVisiblePanel();
                            TCComponent comp1 = panel == null ? null : panel.getComponentToOpen();
                            TCComponent comp2 = criteriaPanel.getQuery();
                            boolean b = comp1 != null && comp2 != null
                                    && comp1.equals(comp2);
                            if ( b && refreshButton != null )
                            {
                                refreshButton.doClick();
                            }
                            else if ( executionButton != null )
                            {
                                executionButton.doClick();
                            }
                        }
                    }
                }
            };
            TCComponentQuery query = panel.getQuery();
            panel.setButtonListener(l);
            criteriaPanel = panel;
            criteriaScroll.setViewportView(panel);
            criteriaScroll.validate();
            selectionButton.setText(query.toString());
            if ( optionButton != null )
            {
                optionButton.setQuery(query);
            }
            // ss ads this for SDLC 1254 - PR 4614088
            if ( editButton != null )
            {
                String sugName = getSuggestedName();
                editButton.setQuery(query, sugName); // sdlc1254-b adds
                                                        // suggested name
            }
            // IMAN_classification grm checking
            if ( advancedLabel != null )
            {
                advancedLabel.setEnabled(enableAdvanced(query));
            }
            selectionButton.validate();
            selectionButton.repaint();
            requestionInputFocus();
        }
    }

    /**
     * Request the focus for the criteria panel
     */
    public void requestionInputFocus()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                if ( criteriaPanel != null )
                {
                    iTextField.transferFocus(criteriaPanel, true);
                    criteriaPanel.requestFocus();
                }
            }
        });
    }
    public void setApplicationPanel(NOVSearchPanel panel )
    {
        appPanel1 = panel;
    }

    /**
     * The method to do the search
     * 
     * @param None
     * @return None
     */
    public void searchAction()
    {
        if ( appPanel1 != null )
        {
            startOperation("");
            appPanel1.openSearch(criteriaPanel);
            endOperation();
        }
    }

    /**
     * The method to do the refresh
     * 
     * @param None
     * @return None
     */
    public void refreshAction()
    {
        if ( appPanel1 != null )
        {
            startOperation("");
            appPanel1.refreshSearch(criteriaPanel);
            endOperation();
        }
    }

    /**
     * Accessor to get the search banner
     * 
     * @param None
     * @return - Search banner
     */
    public NOVSMDLSearchBanner getBanner()
    {
        return banner;
    }

    /**
     * Refresh the Queries
     * 
     * @param None
     * @return None
     */
    public void refresh()
    {
        selectionButton.refresh();
    }

    /**
     * Accessor to get the SearchCriteriaPanel
     * 
     * @param None
     * @return - SearchCriteriaPanel
     */
    public NOVSMDLSearchCriteriaPanel getSearchCriteriaPanel()
    {
        return criteriaPanel;
    }

    /**
     * Method to set the selected saved query on QuerySelectionButton
     * 
     * @param query -
     *            selected IMANComponentQuery
     * @return None
     */
    public void setSelectedQuery(TCComponentQuery query )
    {
        if ( selectionButton != null )
        {
            selectionButton.setSelectedQuery(query);
        }
    }
    public void propertyChange(PropertyChangeEvent evt )
    {
        String prefName = evt.getPropertyName();
        if ( prefName == null || prefName.length() <= 0 )
        {
            return;
        }
        if ( (prefName.equals(PATTERN_MATCH_STYLE_PREF))
                || (prefName.equals(FIND_LIST_SEPERATOR)) )
        {
            // regenerqate criteria panel....
            if ( criteriaPanel != null )
            {
                // need to update the criteria panel.
                Vector nameVector = new Vector();
                Vector valueVector = new Vector();
                String[] names = null;
                String[] values = null;
                TCAdhocQueryClause[] adhocQueryClauses = null;
                // save the values in there so far...
                criteriaPanel.getEntryValues(nameVector, valueVector);
                if ( nameVector.size() > 0 )
                    names = (String[]) nameVector.toArray(new String[nameVector
                            .size()]);
                if ( valueVector.size() > 0 )
                    values = (String[]) valueVector
                            .toArray(new String[valueVector.size()]);
                adhocQueryClauses = criteriaPanel.getAdhocQueryClauses();
                // Since a SearchCriteriaPanel can be shared by mutiple
                // NavigatorPanels,
                // we need to reload query clauses and query form for this
                // SearchCriteriaPanel,
                // rather than creating a new one.
                criteriaPanel.updateQueryClauseForm(names, values,
                        adhocQueryClauses);
                // set up buttons....
                NOVSMDLSearchPanel.this.session.setReadyStatus();
            }
        }
    }
    public void processComponentEvents(AIFComponentEvent[] events )
    {
    }

    // sdrc1254 adds signalOnClose interface support
    // Implement the method for interface signal on close
    public void closeSignaled()
    {
        if ( session != null )
        {
            session.removeAIFComponentEventListener(this);
            TCPreferenceService pref = session.getPreferenceService();
			pref.removePropertyChangeListener(this);
        }
    }

    // sdrc1254-b adds isAllowedToEdit to limit who gets to edit queries
    public boolean isAllowedToEdit()
    {
        if ( session != null )
        {
            try
            {
                if ( session.isUserSystemAdmin() )
                {
                    return true;
                }
                // ok not a sys admin - but am I special (listed in site admin
                // as a special user
                String[] specialUsers = session.getPreferenceService()
                        .getStringArray(
                                TCPreferenceService.TC_preference_site,
                                EDIT_USERS);
                for ( int ii = 0; ii < specialUsers.length; ii++ )
                {
                    if ( session.getUser().getUserId().compareTo(
                            specialUsers[ii]) == 0 ) return true;
                }
            }
            catch ( TCException e )
            {
                ; // who cares...
            }
        }
        return false;
    }

    // sdrc1254-b adds get suggested name
    private String getSuggestedName()
    {
        try
        {
            String returnName = " ";
            String userName = session.getUser().getUserId();
            if ( suggestedNameIndex >= 0 )
            {
                returnName = userName + "_"
                        + Integer.toString(suggestedNameIndex);
            }
            return returnName;
        }
        catch ( TCException e )
        {
            ; // who cares...
        }
        return " ";
    }
    private void setSuggestedNameIndex()
    {
        String[] names = null;
        try
        {
            TCComponentQueryType tc = (TCComponentQueryType) session
                    .getTypeComponent("ImanQuery");
            TCComponent[] queries = tc.extent();
            List<TCComponent> queriesList = Arrays.asList(queries);//TC10.1 Upgrade
            if ( queries != null && queries.length > 0 )
            {
                names = new String[queries.length];
                // Portal Performance Improvement V9.0
                // Use getPropertiesSet() to get all the toString() values
                String[] propertyName = new String[1];
                propertyName[0] = "query_name";
                /*String[][] propertyValues = TCComponentType.getPropertiesSet(
                        queries, propertyName);*/
                String[][] propertyValues = TCComponentType.getPropertiesSet(
                        queriesList, propertyName);//TC10.1 Upgrade
                for ( int ii = 0; ii < queries.length; ii++ )
                {
                    names[ii] = propertyValues[ii][0];
                }
            }
        }
        catch ( Exception e )
        {
            return; // no names then no suggestions
        }
        // rip through names looking to set suggestedNameIndex
        for ( int ii = 0; ii < names.length; ii++ )
        {
            try
            {
                String userName_underscore = session.getUser().getUserId()
                        + "_";
                int indi = names[ii].indexOf(userName_underscore);
                if ( indi == 0 )
                {
                    String truncate = names[ii].substring(userName_underscore
                            .length());
                    int newNum = Integer.parseInt(truncate);
                    if ( newNum >= suggestedNameIndex )
                        suggestedNameIndex = newNum + 1;
                }
            }
            catch ( Exception e )
            {
                ; // not of the form usrname_#### - no problemo - just goon
            }
        }
        // failsafe
        if ( suggestedNameIndex < 0 ) suggestedNameIndex = 0;
        return;
    }

    /**
     * This method disable the button when do the operation.
     * 
     * @return void
     */
    public void startOperation(String startMessage )
    {
        if ( isShowing() )
        {
            // MXI, 13-Mar-2003
            // Don't call methods on buttons which might not be there!
            if ( selectionButton != null ) selectionButton.setEnabled(false);
            if ( executionButton != null ) executionButton.setEnabled(false);
            if ( refreshButton != null ) refreshButton.setEnabled(false);
        }
    }

    /**
     * This method enabled the button after the operatoin is done.
     * 
     * @return void
     */
    public void endOperation()
    {
        if ( isShowing() )
        {
            if ( selectionButton != null ) selectionButton.setEnabled(true);
            if ( executionButton != null ) executionButton.setEnabled(true);
            if ( refreshButton != null ) refreshButton.setEnabled(true);
            SwingUtilities.invokeLater(new Runnable() {
                public void run()
                {
                    if ( criteriaPanel != null )
                    {
                        iTextField.transferFocus(criteriaPanel, true);
                        criteriaPanel.requestFocus();
                    }
                }
            });
        }
    }

    // SS adds this for SDLC 1379
    public boolean isLocked()
    {
        return isLocked;
    }
    private void getAndLoadQuery()
    {
    	NOVSMDLQueryMenuItem menuItem = selectionButton.getMenuItem();
    	if(menuItem != null)
    	{
    		final TCComponentQuery q = menuItem.getQuery();
    		this.setSelectedQuery(q);
    		AbstractAIFOperation op = new AbstractAIFOperation() 
    		{
    			public void executeOperation()
    			{
    				if ( criteriaPanel != null
    						&& criteriaPanel.getQuery() == q )
    				{
    					return;
    				}
    				NOVSMDLSearchPanel.this.session.setStatus(loadingFormMsg + q.toString());
    				loadQuery(q);
    				NOVSMDLSearchPanel.this.session.setReadyStatus();
    			}
    		};
    		op.addOperationListener(NOVSMDLSearchPanel.this);
    		NOVSMDLSearchPanel.this.session.queueOperation(op);
    	}
    }

    private static final long             serialVersionUID         = 1L;
    protected TCSession                 session;
    protected JFrame                      desktop                  = null;
    final protected static String         EDIT_USERS               = "WSOM_find_editUserList";
    public final static String            PATTERN_MATCH_STYLE_PREF = "TC_pattern_match_style";
    public final static String            FIND_LIST_SEPERATOR      = "WSOM_find_list_separator";
    protected NOVSMDLSearchBanner         banner                   = null;
    protected NOVSMDLQuerySelectionButton selectionButton          = null;
    protected OptionPopupButton           optionButton             = null;
    protected NOVSMDLEditPopupButton      editButton               = null;
    protected NOVSMDLSearchCriteriaPanel  criteriaPanel            = null;
    protected JButton                     executionButton          = null ,refreshButton = null;
    protected JScrollPane                 criteriaScroll;
    protected NOVSearchPanel              appPanel1;
    private HyperLink                     advancedLabel            = null;
    protected String                      loadingFormMsg;
    protected ImageIcon                   timeIcon;
    protected JPanel                      loadingStatusPanel;
    private int                           suggestedNameIndex       = -1;
    private boolean                       isLocked                 = false;
    // End ss
}
