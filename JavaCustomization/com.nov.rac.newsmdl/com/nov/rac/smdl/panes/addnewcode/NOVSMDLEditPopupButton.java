package com.nov.rac.smdl.panes.addnewcode;

import java.awt.Dimension;
import javax.swing.JPanel;

import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLEditPopupButton extends com.teamcenter.rac.util.AbstractPopupButton
{
    public NOVSMDLEditPopupButton(AIFDesktop aifdesktop, TCSession imansession)
    {
        suggestedName = null;
        desktop = aifdesktop;
        session = imansession;
        localRegistry = Registry.getRegistry(this);
        setText(localRegistry.getString("edit.TITLE"));
    }

    public void initPopupWindow()
    {
        registry = Registry
                .getRegistry("com.teamcenter.rac.querybuilder.querybuilder");
        JPanel jpanel = getPanel();
        editQueryPanel = new NOVSMDLEditQueryPanel(desktop , session , registry);
        jpanel.add(editQueryPanel, "Center");
        setPopupTitle(getText());
        Dimension dimension = desktop.getSize();
        Dimension dimension1 = new Dimension();
        int i = (int) (0.69999999999999996D * dimension.getWidth());
        int j = (int) (0.80000000000000004D * dimension.getHeight());
        dimension1.setSize(i, j);
        jpanel.setPreferredSize(dimension1);
    }

    public void postUp()
    {
        super.postUp();
        setPinnedUp(true);
        editQueryPanel.setQuery(query, suggestedName);
    }

    public void setQuery(TCComponentQuery imancomponentquery )
    {
        query = imancomponentquery;
        if ( editQueryPanel != null )
            editQueryPanel.setQuery(imancomponentquery);
    }

    public void setQuery(TCComponentQuery imancomponentquery , String s )
    {
        query = imancomponentquery;
        suggestedName = s;
        if ( editQueryPanel != null )
            editQueryPanel.setQuery(imancomponentquery, suggestedName);
    }

    public void setSuggestedName(String s )
    {
        suggestedName = s;
    }

    public void beforePopupWindowGoesDown()
    {
    }

    private static final long       serialVersionUID = 1L;
    protected TCComponentQuery    query;
    protected String                suggestedName;
    protected TCSession           session;
    protected Registry              registry;
    protected Registry              localRegistry;
    private AIFDesktop              desktop;
    protected NOVSMDLEditQueryPanel editQueryPanel;
}