package com.nov.rac.smdl.panes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.utilities.SMDLHomeTree;
import com.nov.rac.smdl.views.SMDLExplorerView;
import com.nov.rac.smdl.views.SMDLHomeView;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.util.SWTUIUtilities;

public class SMDLHomeTreeComposite extends Composite
{

	private SMDLHomeTree m_tcTree= null;
	
	
	public SMDLHomeTreeComposite(Composite parent, int style) {
		
		super(parent, SWT.EMBEDDED);
		
		createTree();
	}


	public void populateTree(SMDLTreeOutput relations ){
		
		//getTree().setSMDLTreeOutput(relations);
		populateTree(relations.smdlOrderObject);
		
	}
	

	public void populateTree(TCComponent component) {
		m_tcTree.setRoot(new TCTreeNode(component));
		m_tcTree.setRootVisible(true);
	    m_tcTree.setScrollsOnExpand(true);
	}
	
	public void populateTree(TCComponentFolder component) {
		TCTreeNode node=new TCTreeNode(component);
		node.setNodeIcon(TCTypeRenderer.getTypeIcon("HomeFolder", "Folder"));
		m_tcTree.setRoot(node);
		m_tcTree.setRootVisible(true);
	    m_tcTree.setScrollsOnExpand(true);
	}


	private void createTree() {
		m_tcTree = new SMDLHomeTree();
		JScrollPane treePane= new JScrollPane(m_tcTree);
		SWTUIUtilities.embed( this, treePane, false);
		setLocation(10, 10);
		setLayout(new FillLayout());
		addListeners();
	}

	public TCTree getTree() {
		return m_tcTree;
	}

	private void addListeners()
	{
		m_tcTree.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent me )
		    {        
				if ( me.getClickCount() == 1 )
				{
					AIFTreeNode selectedNode = (AIFTreeNode) m_tcTree.getSelectedNode();
					if(selectedNode != null)
					{
						openView((TCComponent)selectedNode.getComponent());
					}
				}
		    }
		});
	}
	private void openView(TCComponent component)
	{
		final TCComponent comp = component;
		Display.getDefault().syncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPage page=iw.getActivePage();
//				new SMDLHomeView().openView(page, comp);
			}
		});
	}


}

