package com.nov.rac.smdl.panes;

import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.NOVTablePopupMenu;
import com.nov.rac.smdl.common.SMDLTreePopupMenu;
import com.nov.rac.smdl.utilities.NOVCreateSMDLHelper;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.utilities.NOVSMDLTree;
import com.nov.rac.smdl.views.SMDLExplorerView;
import com.nov4.services.rac.smdl._2010_09.NOV4SMDLDataManagement.SMDLTreeOutput;
import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.SWTUIUtilities;

public class SMDLExplorer extends Composite
{

	private NOVSMDLTree m_tcTree= null;
	
	
	public NOVSMDLTree getNOVSMDLTree() {
		return m_tcTree;
	}



	public SMDLExplorer(Composite parent, int style) {
		
		super(parent, SWT.EMBEDDED);
		
		createTree();
	}

	public void populateTree(SMDLTreeOutput relations,TCComponent selectedComp)
	{		
		getNOVSMDLTree().setSMDLTreeOutput(relations);
		populateTree(relations,relations.smdlOrderObject,selectedComp);
	}
	
	public void populateTree(SMDLTreeOutput relations, TCComponent orderComponent,TCComponent selectedComp) 
	{
		m_tcTree.setRoot(new TCTreeNode(orderComponent));
		m_tcTree.setRootVisible(true);
	    m_tcTree.setScrollsOnExpand(true);
	    
	    findTreeRowToExpand(relations, orderComponent,selectedComp);
	}
	public void findTreeRowToExpand(SMDLTreeOutput relations,TCComponent orderComponent,TCComponent selectedComp)
	{
		int treePathRow      = 0;
		int rowIndxToSelect  = 0;
		boolean bOfTypeRev = false;
		
		List<ISMDLObject> parentObjList = new ArrayList<ISMDLObject>();		
		
		TCComponentItem itemComp = null;
		try 
		{
			if((TCComponent)selectedComp instanceof TCComponentItemRevision)
			{
				itemComp = ((TCComponentItemRevision) selectedComp).getItem();
				bOfTypeRev = true;
			}
			else
			{
				itemComp = (TCComponentItem) selectedComp;
			}
			parentObjList = getParentObjs(itemComp);
			int iObjSize = parentObjList.size();
			if(iObjSize > 0)
			{
				for(int indx=0;indx<iObjSize;indx++)
				{
					if(indx < iObjSize-1)
					{
						treePathRow += getNodeIndex(relations,(TCComponent)parentObjList.get(indx),(TCComponent)parentObjList.get(indx+1))+1;
						expandTree(treePathRow);
					}
					else
					{
						try 
						{
							Thread.sleep(10);
						} 
						catch (InterruptedException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						rowIndxToSelect = treePathRow + getNodeIndex(relations,(TCComponent)parentObjList.get(indx),selectedComp)+1;
						selectTreeNode(rowIndxToSelect);
					}
				}
			}
			else
			{
				selectTreeNode(iObjSize);
			}
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getRevisionIndex(TCComponentItem itemComp, TCComponentItemRevision selectedComp)
	{
		int iRevIndex = 0;
		try
		{
			List<Object> childList = Arrays.asList((Object[])(itemComp.getTCProperty("revision_list").getReferenceValueArray()));
			iRevIndex = childList.indexOf(selectedComp);
		}
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return iRevIndex;
	}
	public List<ISMDLObject> getParentObjs(TCComponent selectedComp)
	{
		List<ISMDLObject> parentObjList = new ArrayList<ISMDLObject>();
		if(selectedComp instanceof ISMDLObject)
		{
			ISMDLObject smdlObj = (ISMDLObject)selectedComp;			
			smdlObj = (ISMDLObject)smdlObj.getParent();
			while(smdlObj != null)
			{
				parentObjList.add(smdlObj);
				smdlObj = (ISMDLObject)smdlObj.getParent();
			}
		}
		Collections.reverse(parentObjList);
		return parentObjList;
	}
	 public int getNodeIndex(SMDLTreeOutput relations,TCComponent parentComp,TCComponent childComp)
	 {
		 int iNodeIndex = 0;
		 List<Object> childList = Arrays.asList((Object[])relations.smdlRelatedObjects.get(parentComp));			
		 iNodeIndex = childList.indexOf(childComp);
		 return iNodeIndex;
	 }
	public void expandTree(int treePathRow)
	{
		final int pathRow =  treePathRow;
		try {
			SwingUtilities.invokeAndWait(new Runnable() 
			{
				public void run()
				{
					TreePath trpath = m_tcTree.getPathForRow(pathRow);
					m_tcTree.expand(trpath);
				}
			});
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void selectTreeNode(int rowIndxToSelect)
	{		
		final int rowIndex =  rowIndxToSelect;
		try {
			SwingUtilities.invokeAndWait(new Runnable() 
			{
				public void run()
				{
					m_tcTree.setSelectionRow(rowIndex);
				}
			});
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public NOV4ComponentDP getParentObj(TCComponent selectedComp)
	{
		ISMDLObject smdlObj = (ISMDLObject)selectedComp;
		NOV4ComponentDP dpObj = null;
		if(selectedComp instanceof ISMDLObject)
		{
			dpObj = (NOV4ComponentDP) smdlObj.getParent();			
		}			
		return dpObj;
	}

	private void createTree() {
		m_tcTree = new NOVSMDLTree();
		JScrollPane treePane= new JScrollPane(m_tcTree);
		SWTUIUtilities.embed( this, treePane, false);
		setLocation(10, 10);
		setLayout(new FillLayout());
		
		m_tcTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		addListeners();
	}
	private void addListeners()
	{
		m_tcTree.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent me )
		    {        
				if (me.getButton() == MouseEvent.BUTTON3)
				{
					
					if(((TCComponent)m_tcTree.getSelectedNode().getComponent()).getType().toString().compareTo("SMDL")==0 ||
							((TCComponent)m_tcTree.getSelectedNode().getComponent()).getType().toString().compareTo("SMDL Revision")==0)
					{
						
						Point p = new Point(me.getX(), me.getY());
						
						TCComponent iSelectedComp = (TCComponent) m_tcTree.getSelectedComponent();
						if (iSelectedComp != null) 
						{
							SMDLTreePopupMenu popupMenu  = new SMDLTreePopupMenu(m_tcTree,iSelectedComp);
							if (popupMenu != null	&& popupMenu.getComponentCount() > 0) 
							{
								popupMenu.show(m_tcTree, p.x, p.y);
							}
						}
					}
				}
			}
		});
		m_tcTree.addTreeSelectionListener(new TreeSelectionListener() 
		{
			public void valueChanged(TreeSelectionEvent e) 
			{
				AIFTreeNode selectedNode = (AIFTreeNode) m_tcTree.getSelectedNode();
				if(selectedNode != null)
				{
					TCComponent comp = (TCComponent)selectedNode.getComponent();
					openView(comp);
				}
		    }
		});
	}
	private void openView(TCComponent component)
	{
		final TCComponent comp = component;
		Display.getDefault().syncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPage page=iw.getActivePage();
//				NOVSMDLExplorerViewHelper.openView(page, comp);
			}
		});
	}

}

