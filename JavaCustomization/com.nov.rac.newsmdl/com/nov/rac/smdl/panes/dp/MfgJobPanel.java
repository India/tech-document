package com.nov.rac.smdl.panes.dp;

import java.util.Date;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.NOVDateButtonComposite;
import com.nov.rac.smdl.common.NOVIntTextField;
import com.nov.rac.smdl.common.listeners.NOVVerifyListener;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class MfgJobPanel extends Composite {

	private Composite m_mfgJobPanel;
	private Label m_pfatDateLabel;
	private NOVDateButtonComposite m_pfatDateComposite;
	private Label m_fatDateLabel;
	private NOVDateButtonComposite m_fatDateComposite;
	private Label m_menuJobNoLabel;
	private Composite m_manuJobNoComposite;
	
	private QCPanel m_qcRequirements;
	private Registry m_Registry;
	private Label m_contractDateLabel;
	private NOVDateButtonComposite m_contractDateComposite;
	//private NOVIntTextField m_quantity;
	private Text m_quantity;
	private Label m_quantityLabel;
	private String m_dataModelName;

	public MfgJobPanel(Composite parent, int style) {
		
		super(parent, style);
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		//createUI(this);		
	}

	public void createUI()
	{
		
		m_mfgJobPanel = this;
		GridLayout mfgJobPanelGridLayout= new GridLayout(1, false);
		setLayout(mfgJobPanelGridLayout);
		GridData gd_mfgJobPanel = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		m_mfgJobPanel.setLayoutData(gd_mfgJobPanel);
		
		Composite datePanel = new Composite(m_mfgJobPanel,SWT.BORDER);
		datePanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		datePanel.setLayout(new GridLayout(2, true));
		
		Composite qcPanel = new Composite(m_mfgJobPanel,SWT.BORDER);
		qcPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		qcPanel.setLayout(new GridLayout(1, true));
		
		Composite quantityPanel = new Composite(m_mfgJobPanel,SWT.NONE);
		quantityPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		quantityPanel.setLayout(new GridLayout(2, false));
		
		m_quantityLabel = new Label(quantityPanel, SWT.NONE);
		m_quantityLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		
		/*Composite m_quantityComposite = new Composite(quantityPanel, SWT.EMBEDDED);
		//m_quantity = new NOVIntTextField(12);
		m_quantity = new Text();
		GridData gd_m_quantity = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_m_quantity.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_quantityComposite, 13);
		m_quantityComposite.setLayoutData(gd_m_quantity);
		SWTUIUtilities.embed( m_quantityComposite, m_quantity, false);*/
		m_quantity = new Text(quantityPanel, SWT.BORDER);
		GridData gd_m_quantity = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_m_quantity.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_quantity, 10);
		m_quantity.setLayoutData(gd_m_quantity);
		
		//Text Limit
		m_quantity.setTextLimit(3);
		
		m_pfatDateLabel = new Label(datePanel, SWT.NONE);
		m_pfatDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_pfatDateComposite=new NOVDateButtonComposite(datePanel, SWT.NONE);
		GridData gd_pfatDateComposite = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_pfatDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_pfatDateComposite, 13);
		m_pfatDateComposite.setLayoutData(gd_pfatDateComposite);
		
		m_pfatDateComposite.setAttributeName(NOV4ComponentDP.PFAT_DATE);
	//	m_pfatDateComposite.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
	//	m_pfatDateComposite.createDataBinding();
		
		m_fatDateLabel = new Label(datePanel, SWT.NONE);
		m_fatDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_fatDateComposite = new NOVDateButtonComposite(datePanel, SWT.NONE);
		GridData gd_fatDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_fatDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_fatDateComposite, 13);
		m_fatDateComposite.setLayoutData(gd_fatDateComposite);
		
		m_fatDateComposite.setAttributeName(NOV4ComponentDP.FAT_DATE);
	//	m_fatDateComposite.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
	//	m_fatDateComposite.createDataBinding();
		
		m_contractDateLabel = new Label(datePanel, SWT.NONE);
		m_contractDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_contractDateComposite = new NOVDateButtonComposite(datePanel, SWT.NONE);
		GridData gd_contractDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_contractDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_contractDateComposite, 13);
		m_contractDateComposite.setLayoutData(gd_contractDateComposite);
		
		m_contractDateComposite.setAttributeName(NOV4ComponentDP.CONTRACT_DELIVERY_DATE);
	//	m_contractDateComposite.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
	//	m_contractDateComposite.createDataBinding();
		
		m_qcRequirements = new QCPanel(qcPanel, SWT.NONE);
		m_qcRequirements.createUI();
			
		setLabels();
		addListeners();
	}

	private void setLabels() {
		
		m_pfatDateLabel.setText(m_Registry.getString("PFAT_Date.Label"));
		m_fatDateLabel.setText(m_Registry.getString("FAT_Date.Label"));
		m_contractDateLabel.setText(m_Registry.getString("Contract_Date.Label"));
		m_quantityLabel.setText(m_Registry.getString("Quantity.Label"));
	}
	
	private void addListeners() {
		
		NOVVerifyListener verifylistener = new NOVVerifyListener();
		
		m_quantity.addVerifyListener(verifylistener);
		
	}
	
	public void setEditability(int mode)
	{
		if (mode==DPOrderPanel.CREATE_DP_DISPLAY)
		{
			m_contractDateComposite.getDateButton().setEnabled(true);
		}
		else if (mode==DPOrderPanel.DP_FORM_DISPLAY)
		{
			m_contractDateComposite.getDateButton().setEnabled(false);
		}
	}
	
	public void clearPanelValues()
	{
		m_pfatDateComposite.setDateString("");
		m_fatDateComposite.setDateString("");
		m_contractDateComposite.setDateString("");
		m_qcRequirements.clearPanelValues();
		m_quantity.setText("");
	}

	public void populate(TCComponent deliveredProduct)
	{
		
		try {
			Date pfatDate = deliveredProduct.getTCProperty(NOV4ComponentDP.PFAT_DATE).getDateValue();
			if (pfatDate != null)
			{
				m_pfatDateComposite.setDate(pfatDate);
			}
					
			Date fatDate = deliveredProduct.getTCProperty(NOV4ComponentDP.FAT_DATE).getDateValue();
			if(fatDate != null)
			{
				m_fatDateComposite.setDate(fatDate);
			}
			
			
			Date contractDate = deliveredProduct.getTCProperty(NOV4ComponentDP.CONTRACT_DELIVERY_DATE).getDateValue();
			if(contractDate != null)
			{
				m_contractDateComposite.setDate(contractDate);
			}
			
			Integer quantity= deliveredProduct.getTCProperty(NOV4ComponentDP.QUANTITY).getIntValue();
			m_quantity.setText(quantity.toString());
			
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		m_qcRequirements.populate(deliveredProduct);
		
	
	}

	public void populateCreateInObject(CreateInObjectHelper transferObject) {
		
		m_qcRequirements.populate(transferObject);
		
		Date pfatDate = m_pfatDateComposite.getDate();
		
		//TCDECREL-4526 Commented out the null check part
		//if(null!=pfatDate)
		{
			transferObject.setDateProps(NOV4ComponentDP.PFAT_DATE, pfatDate,CreateInObjectHelper.SET_PROPERTY);
		}
		
		Date fatDate = m_fatDateComposite.getDate();
		//if(null!=fatDate)
		{
			transferObject.setDateProps(NOV4ComponentDP.FAT_DATE, fatDate,CreateInObjectHelper.SET_PROPERTY);
		}
		
		Date contractDate = m_contractDateComposite.getDate();
		//if(null!=contractDate)
		{
			transferObject.setDateProps(NOV4ComponentDP.CONTRACT_DELIVERY_DATE, contractDate,CreateInObjectHelper.SET_PROPERTY);
		}
		
		//int quantity= m_quantity.getValue();
		int quantity= 0;
		if(m_quantity.getText().length() > 0)
		{
			quantity= Integer.parseInt(m_quantity.getText());
		}
		transferObject.setIntProps(NOV4ComponentDP.QUANTITY ,quantity , CreateInObjectHelper.SET_PROPERTY);
	}
	
	public void setContractDate(Date contractDate) 
	{
		if(contractDate != null)
		{
			m_contractDateComposite.setDate(contractDate);
		}
	}

	public String validateMandatoryFields() 
	{
		String invalidInputs ="";
		if(null==m_contractDateComposite.getDate())
		{
			invalidInputs= m_Registry.getString("Contract_Date.Name");
			
		}
		return invalidInputs;
	}

	public void markMandatoryFields()
	{
		ControlDecoration dec = new ControlDecoration(m_contractDateLabel, SWT.TOP | SWT.RIGHT);
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry.getDefault().getFieldDecoration("NOV_DEC_REQUIRED");
		dec.setImage(reqFieldIndicator.getImage());
		dec.setDescriptionText(reqFieldIndicator.getDescription());		
	}


	public void createDataBinding()
	{
		m_pfatDateComposite.createDataBinding();
		m_fatDateComposite.createDataBinding();
		m_contractDateComposite.createDataBinding();
		
		m_qcRequirements.createDataBinding();
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		//dataBindingHelper.bindData(m_quantity,NOV4ComponentDP.QUANTITY,DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_quantity,NOV4ComponentDP.QUANTITY,m_dataModelName);
	}

	public void setBindingContext(String dataModelName) 
	{
		m_dataModelName = dataModelName;	
		m_pfatDateComposite.setBindingContext(dataModelName);
		m_fatDateComposite.setBindingContext(dataModelName);
		m_contractDateComposite.setBindingContext(dataModelName);
		m_qcRequirements.setBindingContext(dataModelName);
	}

}
