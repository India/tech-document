package com.nov.rac.smdl.panes.dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class QCPanel extends Composite {

	private Composite m_qcPanel;

	private Button m_absCheckButton;
	private Button m_atexCheckButton;
	private Button m_dnvCheckButton;
	private Button m_iecCheckButton;
	private Button m_ndtCheckButton;
	private Button m_thirdPartyCheckButton;
	private Button m_otherCheckButton;
	
	private HashMap<String, Button> m_mapCheckBtns;
	
	private Label mPanelLabel;
	private Text m_Othertext;
	private Registry m_Registry = null;

	private String m_dataModelName;

	public QCPanel(Composite parent, int style)
	{
		super(parent, style);
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		
		//createUI(this);		
	}

	public void createUI()
	{
		m_qcPanel = this;
		setLayout(new GridLayout(1, false));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		Composite labelPanel = new Composite(m_qcPanel,SWT.NONE);
		labelPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		labelPanel.setLayout(new GridLayout(1, false));
		
		mPanelLabel = new Label(labelPanel, SWT.NONE);
		mPanelLabel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		Composite buttonPanel = new Composite(m_qcPanel,SWT.NONE);
		buttonPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		buttonPanel.setLayout(new GridLayout(2, true));
		
		m_absCheckButton = new Button(buttonPanel, SWT.CHECK | SWT.RIGHT);
		
		m_atexCheckButton = new Button(buttonPanel, SWT.CHECK | SWT.RIGHT);
		
		m_dnvCheckButton = new Button(buttonPanel, SWT.CHECK | SWT.RIGHT);
		
		m_iecCheckButton = new Button(buttonPanel, SWT.CHECK | SWT.RIGHT);
		
		m_ndtCheckButton = new Button(buttonPanel, SWT.CHECK | SWT.RIGHT);
		
		m_thirdPartyCheckButton = new Button(buttonPanel, SWT.CHECK | SWT.RIGHT);
		
		Composite otherButtonPanel = new Composite(m_qcPanel,SWT.NONE);
		otherButtonPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		otherButtonPanel.setLayout(new GridLayout(2, false));
		
		m_otherCheckButton = new Button(otherButtonPanel, SWT.CHECK | SWT.RIGHT);
		
		m_Othertext = new Text(otherButtonPanel, SWT.BORDER);
		GridData gd_m_Othertext = new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1);
		gd_m_Othertext.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_Othertext, 10);
		m_Othertext.setLayoutData(gd_m_Othertext);
		
		//Text Limit
		m_Othertext.setTextLimit(32);
		
		setLabels();
		
		//createDataBinding();
		
	}

	private void setLabels() 
	{
		m_mapCheckBtns = new HashMap<String, Button>();
		mPanelLabel.setText(m_Registry.getString("QC_requirements.Label"));
		
		m_absCheckButton.setText(m_Registry.getString("QC_ABS.Label"));
		m_mapCheckBtns.put("ABS",m_absCheckButton);
		
		m_atexCheckButton.setText(m_Registry.getString("QC_ATEX.Label"));
		m_mapCheckBtns.put("ATEX",m_atexCheckButton);
		
		m_dnvCheckButton.setText(m_Registry.getString("QC_DnV.Label"));
		m_mapCheckBtns.put("DnV",m_dnvCheckButton);
		
		m_iecCheckButton.setText(m_Registry.getString("QC_IEC.Label"));
		m_mapCheckBtns.put("IEC",m_iecCheckButton);
		
		m_ndtCheckButton.setText(m_Registry.getString("QC_NDT.Label"));
		m_mapCheckBtns.put("NDT",m_ndtCheckButton);
		
		m_thirdPartyCheckButton.setText(m_Registry.getString("QC_3rd_Party.Label"));
		m_mapCheckBtns.put("3rd Party",m_thirdPartyCheckButton);
		
		m_otherCheckButton.setText(m_Registry.getString("QC_Other.Label"));
		
	}
	
	public void populate(TCComponent deliveredProduct)
	{
		try {
			String[] qcRequirementVals = deliveredProduct.getTCProperty(NOV4ComponentDP.QC_REQUIREMENTS).getStringValueArray();
			
			for(int iCount = 0; iCount < qcRequirementVals.length; iCount++){
				
				Button targetButton = m_mapCheckBtns.get(qcRequirementVals[iCount]);
				
				if(targetButton != null){
					targetButton.setSelection(true);
				}
				else{
					m_otherCheckButton.setSelection(true);
					m_Othertext.setText(qcRequirementVals[iCount]);
				}
			}
			
		} catch (TCException e) {
			
			e.printStackTrace();
		}	
	}
	
	private String[] getQCSelectionValues()
	{
		ArrayList<String> qcSelections = new ArrayList();
		
		Set keys=m_mapCheckBtns.keySet();
		Iterator keysIterator=keys.iterator();
		
		while (keysIterator.hasNext())
		{
			String key= (String)keysIterator.next();
			Object obj = m_mapCheckBtns.get(key);
			if( obj instanceof  Button)
			{
				Button button = (Button)obj;
				if(button.getSelection())
				{
					qcSelections.add(key);
				}
			}
			
		}
		
		if(m_otherCheckButton.getSelection())
		{
			if(!m_Othertext.getText().isEmpty())
			{
				qcSelections.add(m_Othertext.getText());
			}
		}
		
		String[] objs=new String [qcSelections.size()];
		qcSelections.toArray(objs);
		
		return objs;
	}

	public void populate(CreateInObjectHelper transferObject)
	{
		transferObject.setStringArrayProps(NOV4ComponentDP.QC_REQUIREMENTS,getQCSelectionValues(),CreateInObjectHelper.SET_PROPERTY );
		
	}

	public void clearPanelValues() 
	{
		
		Set keys=m_mapCheckBtns.keySet();
		Iterator keysIterator=keys.iterator();
		
		while (keysIterator.hasNext())
		{
			String key= (String)keysIterator.next();
			Object obj = m_mapCheckBtns.get(key);
			if( obj instanceof  Button)
			{
				Button button = (Button)obj;
				button.setSelection(false);
			}
			m_otherCheckButton.setSelection(false);
			m_Othertext.setText("");
		}
		
	}
	
	public void createDataBinding(){
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		/*dataBindingHelper.bindData(m_absCheckButton,"ABS",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_atexCheckButton,"ATEX",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_dnvCheckButton,"DnV",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_iecCheckButton,"IEC",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_ndtCheckButton,"NDT",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_thirdPartyCheckButton,"3rd Party",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_otherCheckButton,"Other",DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_Othertext,"Other Text",DeliveredProductPane.DATA_MODEL_NAME);*/
		
		dataBindingHelper.bindData(m_absCheckButton,NOV4ComponentDP.QC_REQ_ABS,m_dataModelName);
		dataBindingHelper.bindData(m_atexCheckButton,NOV4ComponentDP.QC_REQ_ATEX,m_dataModelName);
		dataBindingHelper.bindData(m_dnvCheckButton,NOV4ComponentDP.QC_REQ_DnV,m_dataModelName);
		dataBindingHelper.bindData(m_iecCheckButton,NOV4ComponentDP.QC_REQ_IEC,m_dataModelName);
		dataBindingHelper.bindData(m_ndtCheckButton,NOV4ComponentDP.QC_REQ_NDT,m_dataModelName);
		dataBindingHelper.bindData(m_thirdPartyCheckButton,NOV4ComponentDP.QC_REQ_THIRD_PARTY,m_dataModelName);
		dataBindingHelper.bindData(m_otherCheckButton,"Other",m_dataModelName);
		dataBindingHelper.bindData(m_Othertext,"Other Text",m_dataModelName);
	}

	public void setBindingContext(String dataModelName)
	{
		m_dataModelName = dataModelName;
	}
}
