package com.nov.rac.smdl.panes.dp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.NOVArrayTable;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class SerialNoPanel extends Composite
{
	
	private Composite m_dpSerialNoPanel;
	private NoDescTable m_serialNoTable;
	private Registry m_registry;
	

	public SerialNoPanel(Composite parent, int style)
	{
		super(parent,style);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");	
		//createUI(this);
			
	}

	public void createUI() 
	{
		m_dpSerialNoPanel = this;
		this.setLayout(new GridLayout(1, false));
		GridData gd_dpSerialNoPanel = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		m_dpSerialNoPanel.setLayoutData(gd_dpSerialNoPanel);
		
		m_serialNoTable= new NoDescTable(m_dpSerialNoPanel,SWT.BORDER);
		m_serialNoTable.setAttributeName(NOV4ComponentDP.DP_SERIAL_NUMBER);
		//m_serialNoTable.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
		m_serialNoTable.createUI(m_serialNoTable);
		//Text Limit
		m_serialNoTable.setTextLimit(64);
		
		setLabels();
	}

	private void setLabels() 
	{
		m_serialNoTable.setNumberLabel(m_registry.getString("Serial_No.Label"));
		m_serialNoTable.setDescLabel(m_registry.getString("description.Label"));
	}

	public void clearPanelValues ()
	{
		//m_serialNoTable.clearAllRows();
		m_serialNoTable.clearPanelValues();
	}
	
	public void populate(TCComponent deliveredProduct)
	{
	
		String[] strArr =null;
		try 
		{
			strArr = (String[]) deliveredProduct.getTCProperty("nov4_serial_number").getPropertyValue();
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		m_serialNoTable.setArrayValues(strArr);
		
	}

	public void populateCreateInObject(CreateInObjectHelper transferObject) {
		
		transferObject.setStringArrayProps(NOV4ComponentDP.DP_SERIAL_NUMBER, m_serialNoTable.getArrayValues(),CreateInObjectHelper.SET_PROPERTY);
	}

	public void setBindingContext(String dataModelName)
	{
		m_serialNoTable.setBindingContext(dataModelName);
	}

	public void createDataBinding() 
	{
		m_serialNoTable.createDataBinding();
		
	}


}
