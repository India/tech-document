package com.nov.rac.smdl.panes.dp;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JTable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.kernel.AbstractAIFSession;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class DPSMDLTablePanel extends Composite
{
	private TCTable m_smdlTable = null;
	private DPSMDLTablePanel m_smdlTablePanel;
	private Label m_smdlTableLabel;
	private String[] m_columnNames;
	private String[] m_colDisplayNames;
	private Registry m_registry;
	
	private PropertyChangeListener 	m_PropertyListeners = null;

	public DPSMDLTablePanel(Composite parent, int style) {
		super(parent, style);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		//createUI(this);
	}

	public void createUI() {
		
		m_smdlTablePanel =this ;
		m_smdlTablePanel.setLayout(new GridLayout(1, true));
	
		//GridData gd_smdlTablePanel = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		//TCDECREL-3011
		GridData gd_smdlTablePanel = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_smdlTablePanel.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_smdlTablePanel, 110);//TCDECREL-3011
		m_smdlTablePanel.setLayoutData(gd_smdlTablePanel);
		
		m_smdlTableLabel = new Label(m_smdlTablePanel, SWT.NONE);
		//m_smdlTableLabel.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));
		setLabels();
		
		Composite smdlTableComposite = new Composite(m_smdlTablePanel, SWT.EMBEDDED);
		smdlTableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createSMDLTable();
		
		ScrollPagePane tablePane= new ScrollPagePane(m_smdlTable);
				
		//todo check if border required 
        
       	SWTUIUtilities.embed( smdlTableComposite, tablePane, false);
			
	}
	

	private void setLabels()
	{
		m_smdlTableLabel.setText(m_registry.getString("DP_SMDL_Table.Label"));
		
		String prefName= m_registry.getString("tableColumnPreference.Name");
		m_columnNames=PreferenceHelper.getStringValues(prefName, TCPreferenceService.TC_preference_site);
		
		m_colDisplayNames=m_registry.getStringArray("DP_SMDL_Table_DisplayColumns.Name");
		
	}

	private void createSMDLTable() 
	{
		
		
		m_smdlTable = new TCTable(m_columnNames , m_colDisplayNames)
        {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int arg0 , int arg1 )
            {
                return false;
            }    
        };
        //Mohan - TCDECREL-2649
        AbstractAIFSession session = AIFUtility.getDefaultSession();
        m_smdlTable.setSession(session);
        
        m_smdlTable.setBackground(Color.WHITE);
       ((JTable) m_smdlTable).setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        m_smdlTable.setEditable(false);            
	}
	
	

	public void populate(TCComponent deliveredProduct)
	{
		try {
			TCComponent[] smdlObjects=deliveredProduct.getTCProperty("_DeliveredProdSMDL_").getReferenceValueArray();
			
			for(TCComponent smdlObject:smdlObjects)
			{
				Object[] smdlProps=getProperties(smdlObject);
				m_smdlTable.addRow(smdlProps);
			}
			
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		
	}
	
	public void addCreatedSMDL(TCComponent smdlObject)
	{
		Object[] smdlProps=getProperties(smdlObject);
		m_smdlTable.addRow(smdlProps);
		
		firePropertyChange(this, "AddSMDLObjectOperation","" , smdlObject);
		smdlObject.fireComponentChangeEvent();
		
	}
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		m_PropertyListeners.propertyChange( changeEvent );
	}
	
	public void clearValues()
	{
		m_smdlTable.removeAllRows();
	}
	
	private Object[] getProperties(TCComponent smdlObject)
	{
		// to do pass latest attribute names
		Object[] smdlProps = new Object[m_columnNames.length];
	
		try 
		{
			for(int iCnt = 0; iCnt<m_columnNames.length;iCnt++)
			{
				if(m_columnNames[iCnt].equals("release_statuses")){
					
					TCComponent[] statusList = smdlObject.getTCProperty(m_columnNames[iCnt]).getReferenceValueArray();
					
					if(statusList.length > 0){
						smdlProps[iCnt] = statusList[statusList.length-1].getTCProperty(NOV4ComponentDP.SMDL_STATUS_NAME).toString();
					}
				}
				else
				{
					smdlProps[iCnt] = smdlObject.getTCProperty(m_columnNames[iCnt]).getPropertyValue();	
				}
			}			
			
		} catch (TCException e) {
		
			e.printStackTrace();
		}
	
		return smdlProps;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		
		m_PropertyListeners = listener;
	}


}
