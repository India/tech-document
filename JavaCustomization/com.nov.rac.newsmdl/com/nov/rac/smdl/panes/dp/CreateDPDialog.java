package com.nov.rac.smdl.panes.dp;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.common.SMDLDataModelConstants;
import com.nov.rac.smdl.panes.order.CreateOrderDialog;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class CreateDPDialog extends AbstractSWTDialog {

	private Registry m_registry;
	private DPOrderPanel m_dpPane;
	private TCComponent m_orderObject;
	private TCSession m_session;
	private CreateInObjectHelper m_transferObject = null;
	private CreateOrderDialog m_createOrdDlg = null;
	
	private ArrayList<PropertyChangeListener> m_listeners = null;
	private DataBindingModel m_theDataModel;
	
	public CreateDPDialog(Shell arg0, int arg1) {
		super(arg0, arg1);
		m_registry = Registry.getRegistry(this);
	    m_session= (TCSession)AIFUtility.getDefaultSession();
	}

	@Override
	protected Control createDialogArea(Composite parent) 
	{
		setTitle();
		
		//TCDECREL-3009 
		/*Composite composite = new Composite(parent, SWT.NONE);
		
		GridLayout gd = new GridLayout(1, false);
		composite.setLayout(gd);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		//addHeader Image
		Image headerImage = m_registry.getImage("newDPCreation_header.ICON");
		Label  header= new Label(composite, SWT.IMAGE_PNG);
		header.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		header.setImage(headerImage);*/
		//TCDECREL-3009 ends
		
		ScrolledComposite comp = new ScrolledComposite( parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		comp.setLayout(new GridLayout(1, true));
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		//create dataModel & register it 
		m_theDataModel = new DataBindingModel();
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.registerDataModel(SMDLDataModelConstants.CREATE_DP_DATA_MODEL_NAME,m_theDataModel);
					
		m_dpPane = new DPOrderPanel( comp, SWT.NONE );
		m_dpPane.createUI();
		m_dpPane.setBindingContext(SMDLDataModelConstants.CREATE_DP_DATA_MODEL_NAME);
		m_dpPane.createDataBinding();
	
		comp.setContent(m_dpPane);
		comp.setExpandHorizontal(true);
		comp.setExpandVertical(true);
		int heightHint=SWTUIHelper.convertVerticalDLUsToPixels(m_dpPane, 300);
		int widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_dpPane, 700);
		comp.setMinSize(m_dpPane.computeSize(widthHint,heightHint));
		comp.pack();
		
		m_dpPane.markMandatoryFields();
		/*m_dpPane.getDpIDPanel().setEditability(DPOrderPanel.CREATE_DP_DISPLAY);
		m_dpPane.getWTYPanel().setEditability(DPOrderPanel.CREATE_DP_DISPLAY);
		m_dpPane.getMfgPanel().setEditability(DPOrderPanel.CREATE_DP_DISPLAY);*/
		m_dpPane.setEditability(DPOrderPanel.CREATE_DP_DISPLAY);
		m_dpPane.getWTYPanel().setVisibility(DPOrderPanel.CREATE_DP_DISPLAY);
		
		populateDPID();
		
		populateContractDate();
		
		return parent;
	}
	
	@Override
	protected Point getInitialSize() 
	{
		Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;
        int rectHeight = rect.height;
    
        int width = ((size.width /2)) + (rectwidth /4);
        int height = ((size.height ))- (rectHeight/2);;
     
		return new Point(width, height);

	}
	
	@Override
	protected Point getInitialLocation(Point point)
	{
		
		// Move the dialog to the center of the top level shell.
        Rectangle shellBounds = getShell().getParent().getBounds();
        Point dialogSize = getInitialSize();

        int x= shellBounds.x + (shellBounds.width - dialogSize.x) / 2;
        int y= shellBounds.y + (shellBounds.height - dialogSize.y) / 2;
        Point location = new Point(x,y);
		
        return location;
	}

	private void populateContractDate() 
	{
		if(m_orderObject!=null)
		{
			try 
			{
				Date contractDate =m_orderObject.getTCProperty(NOV4ComponentOrder.CONTRACT_DELIVERY_DATE).getDateValue();
				m_dpPane.getMfgPanel().setContractDate(contractDate);
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	private void populateDPID() 
	{
		TCComponentItemType itemType;
		try 
		{
			itemType = (TCComponentItemType) (m_session.getTypeComponent(NOV4ComponentDP.DP_OBJECT_TYPE_NAME));
			String dPItemId = itemType.getNewID();
			m_dpPane.getDpIDPanel().setDPIDText(dPItemId);
		} catch (TCException e)
		{
			e.printStackTrace();
		}
	}

	
	
	@Override
	public boolean close() 
	{
		
		boolean returnVal = super.close();
		//unregister data model
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.unregisterDataModel(SMDLDataModelConstants.CREATE_DP_DATA_MODEL_NAME);
		
		return returnVal;
	}

	@Override
	protected void okPressed() {
		
		String invalidInputs=validateInputs();
		if(invalidInputs.length()==0)
		{
		
			if(m_createOrdDlg != null && m_createOrdDlg instanceof CreateOrderDialog) {
				m_transferObject = new CreateInObjectHelper(NOV4ComponentDP.DP_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_CREATE);
				m_dpPane.populateCreateInObject(m_transferObject);
				m_createOrdDlg.setDPObjectHelper(m_transferObject);
			} else {
				ProgressMonitorDialog createDPDialog=new ProgressMonitorDialog(this.getShell());
				
				CreateInObjectHelper dpCreateInHelper = createOperationInput();
				
				CreateOrUpdateOperation createDPOp = new CreateOrUpdateOperation(dpCreateInHelper);
	            try
	            {
	                //createDPOp.executeOperation();
	            	 createDPDialog.run(true, false,createDPOp);
	            }
	            catch ( Exception e1 )
	            {
	                e1.printStackTrace();
	            }
	            
	            TCComponent[] 	createdComp = CreateObjectsSOAHelper.getCreatedOrUpdatedObjects(dpCreateInHelper);
	            TCComponentItem dpItemObject = null;
	            
	            int iCompLength = createdComp.length;
	    		for(int indx=0;indx<iCompLength;indx++)
	    		{	    			
	    			if(createdComp[indx] instanceof TCComponentItem)
	    			{
	    				dpItemObject = (TCComponentItem)createdComp[indx];
	    			}
	    		}
	            
	            firePropertyChange(this, "CreateDPOperation","" , dpItemObject);
	            
			}
			
	      super.okPressed();
		}
		else
		{
			MessageBox.post(m_registry.getString("FillMandatoryFields.MSG")+"\n"+invalidInputs,"Error" ,MessageBox.ERROR);
		}
	}
	
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}

	private String validateInputs() 
	{
		String invalidInputs= ""; 
		
		invalidInputs= m_dpPane.validateMandatoryFields();
		
		return invalidInputs;
		
	}

	private CreateInObjectHelper createOperationInput()
	{
		
		m_transferObject= new CreateInObjectHelper(NOV4ComponentDP.DP_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_CREATE);
		
		m_dpPane.populateCreateInObject(m_transferObject);
		m_transferObject.setTagProps(NOV4ComponentDP.DP_ORDER_RELATION, m_orderObject, CreateInObjectHelper.SET_PROPERTY);
		m_transferObject.setRelationName(NOV4ComponentOrder.ORDER_DP_RELATION);
		m_transferObject.setParentObject( m_orderObject);	
				
		return m_transferObject;
	}

	private void setTitle()
	{
		
		String strDialogTitle = m_registry.getString("createDp.TITLE", null);
			
		this.getShell().setText(strDialogTitle);
								
	}

	public void setOrder(TCComponent orderObject)
	{
		m_orderObject = orderObject;
	}
	
	public CreateInObjectHelper getTransferObject() 
	{
		return m_transferObject;
	}

	public void setOrderDialog(CreateOrderDialog createOrdDlg)
	{
		m_createOrdDlg = createOrdDlg;
	}

	public void addPropertyChangeListeners(	ArrayList<PropertyChangeListener> listeners) {
		m_listeners = listeners;
	}

}
