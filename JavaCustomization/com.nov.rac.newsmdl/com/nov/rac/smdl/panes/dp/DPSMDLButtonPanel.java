package com.nov.rac.smdl.panes.dp;

import java.beans.PropertyChangeListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.createsmdl.listener.NOVCreateSMDLListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class DPSMDLButtonPanel extends Composite {

	private Button m_createSMDLButton;
	//private Button m_saveButton;
	private Registry m_registry;
	
	private NOVCreateSMDLListener m_createSMDLListeners = null;

	public DPSMDLButtonPanel(Composite parent, int style) {
		super(parent, style);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		//createUI(this);
	}

	public void createUI() {
		
		DPSMDLButtonPanel dpsmdlButtonPanel=this;
		dpsmdlButtonPanel.setLayout(new GridLayout(1, true));
		
		GridData gd_dpsmdlButtonPanel = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		dpsmdlButtonPanel.setLayoutData(gd_dpsmdlButtonPanel);
		
		createButtonPanel(dpsmdlButtonPanel);
		
	}

	
	private void createButtonPanel(Composite dpsmdlButtonPanel) 
	{
		Composite buttonPanel = new Composite(dpsmdlButtonPanel, SWT.NONE);
				
        GridLayout gd_buttonLayout = new GridLayout(2, true);
        buttonPanel.setLayout(gd_buttonLayout);
        buttonPanel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
          	
		m_createSMDLButton = new Button(buttonPanel, SWT.NONE);
		//m_saveButton = new Button(buttonPanel, SWT.NONE);
		
		setLabels();
		addListeners();
	}

	private void addListeners()
	{
		//m_saveButton.addSelectionListener( new NOVSaveDPListener());
		
		m_createSMDLListeners = new NOVCreateSMDLListener();
		
		m_createSMDLButton.addSelectionListener(m_createSMDLListeners);
		
	}

	private void setLabels()
	{
		m_createSMDLButton.setText(m_registry.getString("Create_SMDL_Button.Label"));
	//	m_saveButton.setText(m_registry.getString("Save_Button.Label"));
	}

	public void populate(TCComponent m_dpSOAPlainObject)
	{
	//	m_saveButton.setData(m_dpSOAPlainObject);
		m_createSMDLButton.setData(m_dpSOAPlainObject);
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener)
	{
		m_createSMDLListeners.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener)
	{
		m_createSMDLListeners.removePropertyChangeListener(listener);
	}
}
