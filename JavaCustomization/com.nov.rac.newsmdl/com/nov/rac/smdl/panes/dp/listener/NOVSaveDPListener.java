package com.nov.rac.smdl.panes.dp.listener;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVSaveDPListener implements SelectionListener {

	private DeliveredProductPane m_dpPane;
	private TCComponent m_dpComponent;

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0)
	{
		

	}

	@Override
	public void widgetSelected(SelectionEvent event)
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			
			final Object finalTarget = target;
			// Kishor Ahuja
			Display.getCurrent().syncExec( new Runnable() {
				
				@Override
				public void run() 
				{
					Button saveButton= (Button)finalTarget;
					
					m_dpPane= getDPPanel(saveButton);
					
					Object dpObject=saveButton.getData();
					if(dpObject instanceof NOV4ComponentDP)
					{
						m_dpComponent= (TCComponent)dpObject;
						
						ProgressMonitorDialog saveDPDialog=new ProgressMonitorDialog(saveButton.getShell());
						CreateOrUpdateOperation createDPOp = new CreateOrUpdateOperation(createOperationInput());
						
						try
			            {
			                //createDPOp.executeOperation();
							saveDPDialog.run(true, false,createDPOp);
			            }
			            catch ( Exception e1 )
			            {
			                e1.printStackTrace();
			            }
					}
				
					
				}
			});
//			Button saveButton= (Button)target;
//			
//			m_dpPane= getDPPanel(saveButton);
//			
//			Object dpObject=saveButton.getData();
//			if(dpObject instanceof NOV4ComponentDP)
//			{
//				m_dpComponent= (TCComponent)dpObject;
//				
//				ProgressMonitorDialog saveDPDialog=new ProgressMonitorDialog(saveButton.getShell());
//				CreateOrUpdateOperation createDPOp = new CreateOrUpdateOperation(createOperationInput());
//				
//				try
//	            {
//	                //createDPOp.executeOperation();
//					saveDPDialog.run(true, false,createDPOp);
//	            }
//	            catch ( Exception e1 )
//	            {
//	                e1.printStackTrace();
//	            }
//			}
					
		}
	}

	private DeliveredProductPane getDPPanel(Button saveButton)
	{
		Composite parent= null;
		Composite component = saveButton.getParent();
		do {
			
			parent=	component.getParent();
			component=parent;
	
		}while (parent!=null && ! (parent instanceof DeliveredProductPane ));
		
		return (DeliveredProductPane)parent;
	}

	private CreateInObjectHelper createOperationInput()
	{
		CreateInObjectHelper transferObject= new CreateInObjectHelper(NOV4ComponentDP.DP_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
			
		m_dpPane.populateCreateInObject(transferObject);
		transferObject.setTargetObject(m_dpComponent);
				
		return transferObject;
	}

}
