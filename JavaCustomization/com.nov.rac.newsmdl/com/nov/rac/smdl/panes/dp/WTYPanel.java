package com.nov.rac.smdl.panes.dp;

import java.util.Date;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.LinkedDateButtonComposite;
import com.nov.rac.smdl.common.NOVDateButtonComposite;
import com.nov.rac.smdl.common.listeners.NOVVerifyListener;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class WTYPanel extends Composite {

	private Composite m_wtyPanel;
	private Label m_shipDateLabel;
	
	private LinkedDateButtonComposite m_shipDateComposite;
	private Label m_comissionDateLabel;
	
	private LinkedDateButtonComposite m_comissionDateComposite;
	private Label m_reFurbDateLabel;
	private NOVDateButtonComposite m_reFurbDateComposite;
	private Label m_shipWTYMonthsLabel;
	private Text m_shipWTYMonthsText;
	private Label m_comWTYMonthsLabel;
	private Text m_comWTYMonthsText;
	private Label m_shipWTYExpireLabel;
	private NOVDateButtonComposite m_shipWTYExpireComposite;
	private Label m_comWTYExpireLabel;
	private NOVDateButtonComposite m_comWTYExpireComposite;
	private Registry m_registry;

	private VerifyListener  m_verifylistener = null;
	private String m_dataModelName; 
	

	public WTYPanel(Composite parent, int style) {
		super(parent, SWT.NONE);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		this.setLayout(new GridLayout(1, true));
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		//createUI(this);
		
	}

	public void createUI()
	{
		Composite parent= this;
		m_wtyPanel = new Composite (parent, SWT.BORDER);
		GridLayout gridLayout = new GridLayout(2, true);
		m_wtyPanel.setLayout(gridLayout);
	
		
		GridData gd_wtyPanel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		m_wtyPanel.setLayoutData(gd_wtyPanel);
		
		m_shipDateLabel = new Label(m_wtyPanel, SWT.NONE);
		m_shipDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		
		m_shipDateComposite=new LinkedDateButtonComposite(m_wtyPanel, SWT.NONE);
		GridData gd_shipDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_shipDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_shipDateComposite, 13);
		m_shipDateComposite.setLayoutData(gd_shipDateComposite);
		
		m_shipDateComposite.setAttributeName(NOV4ComponentDP.SHIP_DATE);
	//	m_shipDateComposite.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
	//	m_shipDateComposite.createDataBinding();
        		
		
		m_comissionDateLabel = new Label(m_wtyPanel, SWT.NONE);
		m_comissionDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		
		m_comissionDateComposite = new LinkedDateButtonComposite(m_wtyPanel, SWT.NONE);
		GridData gd_comissionDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_comissionDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_comissionDateComposite, 13);
		m_comissionDateComposite.setLayoutData(gd_comissionDateComposite);
		
		m_comissionDateComposite.setAttributeName(NOV4ComponentDP.COMMISSION_DATE);
		//m_comissionDateComposite.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
		//m_comissionDateComposite.createDataBinding();
		
		m_reFurbDateLabel = new Label(m_wtyPanel, SWT.NONE);
		m_reFurbDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_reFurbDateComposite = new NOVDateButtonComposite(m_wtyPanel, SWT.NONE);
		GridData gd_reFurbDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_reFurbDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_reFurbDateComposite, 13);
		m_reFurbDateComposite.setLayoutData(gd_reFurbDateComposite);
		
		m_reFurbDateComposite.setAttributeName(NOV4ComponentDP.REFURB_DATE);
		//m_reFurbDateComposite.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
		//m_reFurbDateComposite.createDataBinding();
		
		m_shipWTYMonthsLabel = new Label(m_wtyPanel, SWT.NONE);
		m_shipWTYMonthsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
//		Composite shipWTYMonthsComposite = new Composite(m_wtyPanel, SWT.EMBEDDED);
//		m_shipWTYMonthsText=  new NOVIntTextField(12);
//		GridData gd_shipWTYMonthsText = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
//		//gd_shipWTYMonthsText.heightHint = 25;
//		gd_shipWTYMonthsText.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(shipWTYMonthsComposite, 13);
//		shipWTYMonthsComposite.setLayoutData(gd_shipWTYMonthsText);
//		SWTUIUtilities.embed( shipWTYMonthsComposite, m_shipWTYMonthsText, false);
		
		m_shipWTYMonthsText = new Text(m_wtyPanel,SWT.BORDER);
		
		
		m_shipWTYMonthsText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		//Text Limit
		m_shipWTYMonthsText.setTextLimit(3);
		
		m_comWTYMonthsLabel = new Label(m_wtyPanel, SWT.NONE);
		m_comWTYMonthsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
//		Composite comWTYMonthsComposite = new Composite(m_wtyPanel, SWT.EMBEDDED);
//		m_comWTYMonthsText = new NOVIntTextField(12);
//		GridData gd_comWTYMonthsText = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
//		//gd_comWTYMonthsText.heightHint = 25;
//		gd_comWTYMonthsText.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(comWTYMonthsComposite, 13);
//		comWTYMonthsComposite.setLayoutData(gd_comWTYMonthsText);
//		SWTUIUtilities.embed( comWTYMonthsComposite, m_comWTYMonthsText, false);
		m_comWTYMonthsText = new Text(m_wtyPanel,SWT.BORDER);
		m_comWTYMonthsText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		//Text Limit
		m_comWTYMonthsText.setTextLimit(3);
		
		m_shipWTYExpireLabel = new Label(m_wtyPanel, SWT.NONE);
		m_shipWTYExpireLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_shipWTYExpireComposite =  new NOVDateButtonComposite(m_wtyPanel, SWT.NONE);
		GridData gd_shipWTYExpireText = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_shipWTYExpireText.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_shipWTYExpireComposite, 13);
		m_shipWTYExpireComposite.setLayoutData(gd_shipWTYExpireText);
		
		m_comWTYExpireLabel = new Label(m_wtyPanel, SWT.NONE);
		m_comWTYExpireLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_comWTYExpireComposite = new NOVDateButtonComposite(m_wtyPanel, SWT.NONE);
		GridData gd_comWTYExpireText = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_comWTYExpireText.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_comWTYExpireComposite, 13);
		m_comWTYExpireComposite.setLayoutData(gd_comWTYExpireText);
		
		setLabels();
		
		m_shipDateComposite.linkDateButton(m_shipWTYExpireComposite.getDateButton());
		m_shipDateComposite.linkTextComponent( m_shipWTYMonthsText);
		
		m_comissionDateComposite.linkDateButton(m_comWTYExpireComposite.getDateButton());
		m_comissionDateComposite.linkTextComponent( m_comWTYMonthsText);
		
	//	createDataBinding();
		
		addListeners();
		
	}

	

	private void addListeners() {
		
		m_verifylistener = new NOVVerifyListener();
		
		m_comWTYMonthsText.addVerifyListener(m_verifylistener);
		
		m_shipWTYMonthsText.addVerifyListener(m_verifylistener);
		
	}

//	protected void populateExpiryDateFields() {
//		
//		Calendar cal= Calendar.getInstance();
//		
//		Date shipDate = m_shipDateComposite.getDate();
//		Date commDate = m_comissionDateComposite.getDate();
//		
//		int shipWTYMonths = 0;
//		int commWTYMonths = 0;
//		
//		if(null!=shipDate)
//		{
//			shipWTYMonths = Integer.parseInt(m_shipWTYMonthsText.getText());
//			cal.setTime(shipDate);
//			cal.add(Calendar.MONTH, shipWTYMonths);
//			m_shipWTYExpireComposite.setDate(cal.getTime());
//		}
//		
//		if(null!=commDate)
//		{
//			commWTYMonths = Integer.parseInt(m_shipWTYMonthsText.getText());
//			cal.setTime(commDate);
//			cal.add(Calendar.MONTH, commWTYMonths);
//			m_comWTYExpireComposite.setDate(cal.getTime());
//		}
//		
//	}

	public void setEditability(int formMode)
	{
		m_shipWTYExpireComposite.getDateButton().setEnabled(false);
		m_comWTYExpireComposite.getDateButton().setEnabled(false);
		
		if(formMode==DPOrderPanel.DP_FORM_DISPLAY)
		{
			m_shipWTYMonthsText.setEditable(true);
			m_comWTYMonthsText.setEditable(true);
			
		}else if (formMode==DPOrderPanel.CREATE_DP_DISPLAY)
		{
			m_shipWTYMonthsText.setEditable(true);
			m_comWTYMonthsText.setEditable(true);
		}
	}

	public void setVisibility(int formMode)
	{
		if (formMode==DPOrderPanel.CREATE_DP_DISPLAY)
		{
			m_shipWTYExpireLabel.setVisible(false);
			m_shipWTYExpireComposite.setVisible(false);
			m_comWTYExpireLabel.setVisible(false);
			m_comWTYExpireComposite.setVisible(false);
		}
	}
	
	private void setLabels()
	{
		m_shipDateLabel.setText(m_registry.getString("Ship_Date.Label"));
		m_comissionDateLabel.setText(m_registry.getString("Comm_Date.Label"));
		m_reFurbDateLabel.setText(m_registry.getString("Refurb_Date.Label"));
		m_shipWTYMonthsLabel.setText(m_registry.getString("Ship_WTY_Months.Label"));
		m_comWTYMonthsLabel.setText(m_registry.getString("Comm_WTY_Months.Label"));
		m_shipWTYExpireLabel.setText(m_registry.getString("Ship_WTY_Expire.Label"));
		m_comWTYExpireLabel.setText(m_registry.getString("Comm_WTY_Expire.Label"));
	}

	public void clearPanelValues()
	{
		m_shipDateComposite.setDate(null);
		m_comissionDateComposite.setDate(null);
		m_reFurbDateComposite.setDate(null);
		m_shipWTYMonthsText.setText("");
		m_comWTYMonthsText.setText("");
		m_shipWTYExpireComposite.setDate(null);
		m_comWTYExpireComposite.setDate(null);
	}
	
	public void populate(TCComponent deliveredProduct) 
	{
		try 
		{
			Date shipDate = (Date) deliveredProduct.getTCProperty(NOV4ComponentDP.SHIP_DATE).getPropertyValue();
			m_shipDateComposite.setDate(shipDate);
			
			Date commDate = (Date)deliveredProduct.getTCProperty(NOV4ComponentDP.COMMISSION_DATE).getPropertyValue();
			m_comissionDateComposite.setDate(commDate);
			
			Date refurbDate = (Date)deliveredProduct.getTCProperty(NOV4ComponentDP.REFURB_DATE).getPropertyValue();
			m_reFurbDateComposite.setDate(refurbDate);
			
			Integer shipWTYMonths = (Integer) deliveredProduct.getTCProperty(NOV4ComponentDP.SHIP_WTY ).getPropertyValue();
			m_shipWTYMonthsText.setText(String.valueOf(shipWTYMonths));
			
			Integer commWTYMonths = (Integer) deliveredProduct.getTCProperty(NOV4ComponentDP.COMMISSION_WTY ).getPropertyValue();
			m_comWTYMonthsText.setText(String.valueOf(commWTYMonths));
			
			
		} catch (TCException e)
		{
			e.printStackTrace();
		}
	}	
	public void setAttrValues(TCComponent deliveredProduct) 
	{
			
		try 
		{
			Date shipDate = m_shipDateComposite.getDate();
			deliveredProduct.setDateProperty(NOV4ComponentDP.SHIP_DATE, shipDate);
			
			Date commDate = m_comissionDateComposite.getDate();
			deliveredProduct.setDateProperty(NOV4ComponentDP.COMMISSION_DATE, commDate);
						
			Date refurbDate = m_reFurbDateComposite.getDate();
			deliveredProduct.setDateProperty(NOV4ComponentDP.REFURB_DATE, refurbDate);
			
			String shipWtyMonths = m_shipWTYMonthsText.getText();
			if(shipWtyMonths.length()>0)
			{
				deliveredProduct.setIntProperty(NOV4ComponentDP.SHIP_WTY,Integer.parseInt(shipWtyMonths));
			}
								
			String commWTYMonths = m_comWTYMonthsText.getText();
			if(commWTYMonths.length()>0)
			{
				deliveredProduct.setIntProperty(NOV4ComponentDP.COMMISSION_WTY,Integer.parseInt(commWTYMonths));
			}			
			
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
	}

	public void populateCreateInObject(CreateInObjectHelper transferObject) 
	{
		Date shipDate = m_shipDateComposite.getDate();
		//if(null!=shipDate)
		{
			transferObject.setDateProps(NOV4ComponentDP.SHIP_DATE, shipDate,CreateInObjectHelper.SET_PROPERTY);
		}
		
		Date commDate = m_comissionDateComposite.getDate();
		//if(null!=commDate)
		{
			transferObject.setDateProps(NOV4ComponentDP.COMMISSION_DATE, commDate,CreateInObjectHelper.SET_PROPERTY);
		}

		Date refurbDate = m_reFurbDateComposite.getDate();
		//if(null!=refurbDate)
		{
			transferObject.setDateProps(NOV4ComponentDP.REFURB_DATE, refurbDate,CreateInObjectHelper.SET_PROPERTY);
		}
		
		int iShipWTY = 0;
		int iComWTY = 0;
		
		if(m_shipWTYMonthsText.getText().length() > 0){
			iShipWTY = Integer.parseInt(m_shipWTYMonthsText.getText());
		}
		
		if(m_comWTYMonthsText.getText().length() > 0){
			iComWTY = Integer.parseInt(m_comWTYMonthsText.getText());
		}
		
		transferObject.setIntProps(NOV4ComponentDP.SHIP_WTY,iShipWTY,CreateInObjectHelper.SET_PROPERTY);
		transferObject.setIntProps(NOV4ComponentDP.COMMISSION_WTY, iComWTY,CreateInObjectHelper.SET_PROPERTY);
		
	}
	
	public void createDataBinding()
	{
		m_shipDateComposite.createDataBinding();
		m_comissionDateComposite.createDataBinding();
		m_reFurbDateComposite.createDataBinding();
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		/*dataBindingHelper.bindData(m_shipWTYMonthsText,NOV4ComponentDP.SHIP_WTY,DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_comWTYMonthsText,NOV4ComponentDP.COMMISSION_WTY,DeliveredProductPane.DATA_MODEL_NAME);*/
		dataBindingHelper.bindData(m_shipWTYMonthsText,NOV4ComponentDP.SHIP_WTY,m_dataModelName);
		dataBindingHelper.bindData(m_comWTYMonthsText,NOV4ComponentDP.COMMISSION_WTY,m_dataModelName);
		
	}

	public void setBindingContext(String dataModelName) 
	{
		m_dataModelName = dataModelName;
		m_shipDateComposite.setBindingContext(dataModelName);
		m_comissionDateComposite.setBindingContext(dataModelName);
		m_reFurbDateComposite.setBindingContext(dataModelName);
	}

}
