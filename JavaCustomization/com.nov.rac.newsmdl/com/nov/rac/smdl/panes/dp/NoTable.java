package com.nov.rac.smdl.panes.dp;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.SWTUIUtilities;

public class NoTable extends NoDescTable {

	public NoTable(Composite parent, int style)
	{
		super(parent, style);
	}

	protected void setLayout()
	{
		m_arrayPanel.setLayout(new GridLayout(1, false));
		GridData gd_Panel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		m_arrayPanel.setLayoutData(gd_Panel);
	}
	
	@Override
	protected void createInputPanel() 
	{
		Composite inputPanel = new Composite (m_arrayPanel, SWT.None);
		inputPanel.setLayout(new GridLayout(2, false));
		inputPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		m_numberTxt = new Text(inputPanel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_numberTxt = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_numberTxt.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_numberTxt, 25);
		m_numberTxt.setLayoutData( gd_numberTxt);
		
		Image addIcon = m_Registry.getImage("plus.ICON");
		m_addRowButton = new Button(inputPanel, SWT.NONE);
		m_addRowButton.setImage(addIcon);
		GridData buttonLayoutData= new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1);
		buttonLayoutData.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_addRowButton, 15);
		buttonLayoutData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_addRowButton, 18);
		m_addRowButton.setLayoutData (buttonLayoutData);
	}

	protected void createOutputPanel()
	{
		Composite valuePanelWrap = new Composite (m_arrayPanel, SWT.NONE);
		valuePanelWrap.setLayout(new GridLayout(2, false));
		valuePanelWrap.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
//		Composite valuePanel = new Composite (valuePanelWrap, SWT.EMBEDDED);
//		valuePanel.setLayout(new GridLayout(1, true));
//		GridData gd_valuePanel=new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
//		gd_valuePanel.heightHint=SWTUIHelper.convertVerticalDLUsToPixels(valuePanel, 35);
//		valuePanel.setLayoutData(gd_valuePanel);
//		createTable();
//		JScrollPane scrollpane = new JScrollPane(m_table);
//		scrollpane.setColumnHeaderView(null);
//		SWTUIUtilities.embed( valuePanel,scrollpane , false);
		m_listBox = new List(valuePanelWrap,SWT.SINGLE | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		
		GridData listLayoutData=new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		listLayoutData.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_listBox, 35);
		m_listBox.setLayoutData(listLayoutData);
		
		Image removeIcon = m_Registry.getImage("minus.ICON");
		m_remRowButton = new Button(valuePanelWrap, SWT.NONE);
		m_remRowButton.setImage(removeIcon);
		GridData buttonLayoutData1=new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1);
		buttonLayoutData1.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_addRowButton, 15);
		buttonLayoutData1.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_addRowButton, 18);
		m_remRowButton.setLayoutData (buttonLayoutData1);
	}
	@Override
	protected void createLabelPanel() {
		
		Composite labelPanel = new Composite (m_arrayPanel, SWT.None);
		GridLayout rl_labelPanel = new GridLayout(1, false);
		labelPanel.setLayout(rl_labelPanel);
		labelPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		m_numberLabel = new Label(labelPanel, SWT.NONE);
		
	}
	
	
	
	@Override
	protected void attachAddButtonListener()
	{
		 m_addRowButton.addSelectionListener(new SelectionListener() {
	          	@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					
				}

				@Override
				public void widgetSelected(SelectionEvent arg0) {
					 if ( m_numberTxt.getText().equals("") )
		                {
		                    MessageBox.post(m_Registry.getString("noFieldsValueError.MSG"),
		                            "Warning", 1);
		                    return;
		                }
		                else
		                {
		                    String cellValueStr = null;
		                    cellValueStr = m_numberTxt.getText();
		                   
		                    Object[] cellObj1;
		                    if ( cellValueStr != null )
		                    {
		                        cellObj1 = new Object[]
		                        { cellValueStr };
		                        addRow(cellObj1);
		                        m_numberTxt.setText("");
		                    }
		                }
					
				}
	        });
	     	        
	       /*
	        m_remRowButton.addSelectionListener(new SelectionListener()
	        {
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void widgetSelected(SelectionEvent arg0) {
					 int selectedrow = m_listBox.getSelectionIndex();
		                if(selectedrow<0)
		                {
		                	 MessageBox.post(m_Registry.getString("selectRow.MSG"),
			                            "Warning", 1);
			                    return;
		                }
		                //((DefaultTableModel) m_table.getModel()).removeRow(selectedrow);
		                m_listBox.remove(selectedrow);
					
				}
	        });*/

	}

	public void setLabel(String label)
	{
		m_numberLabel.setText(label);
	}

	@Override
	protected void clearFieldValues() {
		
		 m_numberTxt.setText("");
	}

	
	
	

}
