package com.nov.rac.smdl.panes.dp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.smdl.panes.order.OrderCustPanel;
import com.nov.rac.smdl.panes.order.OrderExtPanel;
import com.nov.rac.smdl.panes.order.OrderIDPanel;
import com.nov.rac.smdl.panes.order.OrderSalesPanel;
import com.teamcenter.rac.kernel.TCComponent;

public class DPOrderPanel extends Composite 
{
	private OrderIDPanel orderIdPanel;
	private OrderCustPanel orderCustomerPanel;
	private OrderSalesPanel orderSalesPanel;
	private OrderExtPanel orderExtPanel;
	private LocationPanel engJobPanel;
	private MfgJobPanel mfgJobPanel;
	private WTYPanel wtyPanel;
	private DeliveredProductIDPanel dpIDPanel;
	private SerialNoPanel dpSerialNoPanel;
	private Composite dpPanel;
	
	public static int DP_FORM_DISPLAY 		=	0;
	public static int CREATE_DP_DISPLAY 	=	1;

	public DPOrderPanel(Composite parent, int style) {
		super(parent, style);
		
		//createUI( this);
	}

	public void createUI() 
	{
		DPOrderPanel dpOrderPanel = this;
		dpOrderPanel.setLayout(new GridLayout(1, true));
		
		GridData gd_dpOrderPanel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		dpOrderPanel.setLayoutData(gd_dpOrderPanel);
		
		createDeliverdProdcutPanel(dpOrderPanel);
		
		createShipPropertyPanel(dpOrderPanel);
		
		/*if(mode== DP_FORM_DISPLAY)
		{
			createOrderPanel(dpOrderPanel);
		}*/
	}

	private void createOrderPanel(Composite parent)
	{
		orderIdPanel = new OrderIDPanel(parent, SWT.BORDER);
		orderIdPanel.setEditability(false);
		
		orderCustomerPanel = new OrderCustPanel(parent, SWT.BORDER);
		orderCustomerPanel.setEditability(false);
		
		orderSalesPanel = new OrderSalesPanel(parent, SWT.BORDER);
		orderSalesPanel.setEditability(false);
		
		orderExtPanel = new OrderExtPanel(parent, SWT.BORDER);
		orderExtPanel.setEditability(false);
		
	}

	private void createShipPropertyPanel(Composite parent)
	{
		Composite shipPropertyPanel = new Composite(parent, SWT.BORDER);
		shipPropertyPanel.setLayout(new GridLayout(2, false));
		shipPropertyPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		engJobPanel= new LocationPanel(shipPropertyPanel, SWT.NONE);
		engJobPanel.createUI();
		engJobPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		engJobPanel.setLayout(new GridLayout(1, true));
		
		Composite shipDatePanel = new Composite(shipPropertyPanel, SWT.NONE);
		shipDatePanel.setLayout(new GridLayout(2, true));
		shipDatePanel.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		
		mfgJobPanel= new MfgJobPanel(shipDatePanel, SWT.NONE);
		mfgJobPanel.createUI();
		mfgJobPanel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		wtyPanel= new WTYPanel(shipDatePanel,SWT.NONE);
		wtyPanel.createUI();
		wtyPanel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false, 1, 1));
		
	}


	private void createDeliverdProdcutPanel(Composite parent) 
	{
		dpPanel = new Composite(parent, SWT.BORDER);
		dpPanel.setLayout(new GridLayout(2, true));
		
		dpPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		dpIDPanel = new DeliveredProductIDPanel(dpPanel, SWT.BORDER);
		dpIDPanel.createUI();
		dpIDPanel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		

		dpSerialNoPanel= new SerialNoPanel(dpPanel, SWT.NONE);
		dpSerialNoPanel.createUI();
		dpSerialNoPanel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
	}


	public void populate(TCComponent m_dpSOAPlainObject) 
	{
	
		if( m_dpSOAPlainObject instanceof NOV4ComponentDP )
		{
			NOV4ComponentDP dpObject = (NOV4ComponentDP) m_dpSOAPlainObject;
			populateDPPanels(m_dpSOAPlainObject);
						
			/*NOV4ComponentOrder order4DP =  (NOV4ComponentOrder) dpObject.getOrder();
			populateOrderPanels(order4DP);*/
		}
		
		
	}

	public void clearPanelValues() 
	{
		dpIDPanel.clearPanelValues();
		dpSerialNoPanel.clearPanelValues();
		engJobPanel.clearPanelValues();
		mfgJobPanel.clearPanelValues();
		wtyPanel.clearPanelValues();
		
	}

	public void populateOrderPanels(TCComponent order4DP)
	{
		// START -- Kishor Ahuja get Order object and populate Order Composites
		if( order4DP instanceof NOV4ComponentOrder )
		{
			orderIdPanel.populate(order4DP);
			orderCustomerPanel.populate(order4DP);
			orderSalesPanel.populate(order4DP);
			orderExtPanel.populate(order4DP);
		}
	
		// END -- Kishor Ahuja get Order object and populate Order Composites
	}

	public void populateDPPanels(TCComponent m_dpSOAPlainObject)
	{
		dpIDPanel.populate(m_dpSOAPlainObject);
		dpSerialNoPanel.populate(m_dpSOAPlainObject);
		engJobPanel.populate(m_dpSOAPlainObject);
		mfgJobPanel.populate(m_dpSOAPlainObject);
		wtyPanel.populate(m_dpSOAPlainObject);
	}
	
	public DeliveredProductIDPanel getDpIDPanel()
	{
		return dpIDPanel;
	}
	
	public WTYPanel getWTYPanel()
	{
		return wtyPanel;
	}

	public MfgJobPanel getMfgPanel()
	{
		return mfgJobPanel;
	}
	
	public void populateCreateInObject(CreateInObjectHelper transferObject)
	{
		dpIDPanel.populateCreateInObject(transferObject);
		dpSerialNoPanel.populateCreateInObject(transferObject);
		engJobPanel.populateCreateInObject(transferObject);
		mfgJobPanel.populateCreateInObject(transferObject);
		wtyPanel.populateCreateInObject(transferObject);
	}

	public String validateMandatoryFields()
	{
		StringBuffer invalidInputs=new StringBuffer();
		
		invalidInputs.append(dpIDPanel.validateMandatoryFields());
		if(mfgJobPanel.validateMandatoryFields().length()>0)
		{
			if(invalidInputs.length()!=0)
			{
				invalidInputs.append(",");
			}
			invalidInputs.append( mfgJobPanel.validateMandatoryFields());
		}
				
		return invalidInputs.toString();
	}

	public void markMandatoryFields() 
	{
		dpIDPanel.markMandatoryFields();
		mfgJobPanel.markMandatoryFields();
	}

	public void setEditability(int formMode) 
	{
		dpIDPanel.setEditability(formMode);
		mfgJobPanel.setEditability(formMode);
		wtyPanel.setEditability(formMode);
		
	}

	public void setBindingContext(String dataModelName) 
	{
		dpIDPanel.setBindingContext(dataModelName);
		dpSerialNoPanel.setBindingContext(dataModelName);
		engJobPanel.setBindingContext(dataModelName);
		mfgJobPanel.setBindingContext(dataModelName);
		wtyPanel.setBindingContext(dataModelName);
	}

	public void createDataBinding() 
	{
		dpIDPanel.createDataBinding();
		dpSerialNoPanel.createDataBinding();
		engJobPanel.createDataBinding();
		mfgJobPanel.createDataBinding();
		wtyPanel.createDataBinding();
	}
	//ankita
	public boolean validateSave()
	{
		if(dpIDPanel.validateSave())
		{
			return true;
		}
		return false;
	}
}
