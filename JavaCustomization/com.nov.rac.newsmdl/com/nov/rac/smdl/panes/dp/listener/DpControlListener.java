package com.nov.rac.smdl.panes.dp.listener;

import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;

public class DpControlListener implements ControlListener
{
	private ScrolledComposite m_scrComp;
	
	public DpControlListener( final ScrolledComposite scrComp )
	{
		m_scrComp = scrComp;
	}

	public void controlMoved( ControlEvent ctrlEvent )
	{
	}

	public void controlResized( ControlEvent ctrlEvent )
	{
		Object srcObject = ( Object )ctrlEvent.getSource();
        if( srcObject instanceof Composite )
        {
        	Composite scrolledComp = ( Composite ) ctrlEvent.getSource();
			Point pt = scrolledComp.getSize();
			
			m_scrComp.setMinSize( pt.x, pt.y );
        }
	}
}
