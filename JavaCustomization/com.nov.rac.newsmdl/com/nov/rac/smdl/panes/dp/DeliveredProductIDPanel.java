package com.nov.rac.smdl.panes.dp;

import org.eclipse.jface.fieldassist.AutoCompleteField;
import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.NOVAbstractNameValidator;
import com.nov.rac.smdl.common.NOVArrayHelper;
import com.nov.rac.smdl.common.StandardProductList;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.utilities.utils.ValidateHelper;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DeliveredProductIDPanel extends Composite {
	private Composite m_dpIDPanel;
	private Text m_dpIDText;
	private Text m_dpDescTextArea;
	private Label m_dpIDLabel;
	private Label m_dpDescLabel;
	private Label m_stdProductLabel;
	private Combo m_basedOnStdProd;
	public StandardProductList m_stdProductList = null;
	private Registry m_registry;
	private String m_dataModelName;

	private StringValidationToolkit m_strValToolkit = null;
	private boolean m_valid = false;

	/*
	 * static int DP_FORM_DISPLAY = 0; static int CREATE_DP_DISPLAY = 0;
	 */

	public DeliveredProductIDPanel(Composite parent, int style) {
		super(parent, style);

		m_registry = Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		// createUI(this);

	}

	public void createUI() {
		Composite deliveredProductIDPanel = this;

		deliveredProductIDPanel.setLayout(new GridLayout(1, false));
		m_dpIDPanel = new Composite(deliveredProductIDPanel, SWT.None);

		m_dpIDPanel.setLayout(new GridLayout(2, false));
		GridData gd_dpIDPanel = new GridData(SWT.FILL, SWT.FILL, true, true, 1,
				1);
		m_dpIDPanel.setLayoutData(gd_dpIDPanel);

		m_dpIDLabel = new Label(m_dpIDPanel, SWT.NONE);
		GridData gd_m_dpIDLabel = new GridData(SWT.LEFT, SWT.TOP, false, false,
				1, 1);
		m_dpIDLabel.setLayoutData(gd_m_dpIDLabel);

		m_dpIDText = new Text(m_dpIDPanel, SWT.BORDER);
		m_dpIDText.setTextLimit(32);
		m_dpIDText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false,
				1, 1));

		m_dpDescLabel = new Label(m_dpIDPanel, SWT.NONE);
		GridData gd_m_dpDescLabel = new GridData(SWT.LEFT, SWT.TOP, false,
				false, 1, 1);
		m_dpDescLabel.setLayoutData(gd_m_dpDescLabel);

		m_dpDescTextArea = new Text(m_dpIDPanel, SWT.MULTI | SWT.BORDER
				| SWT.WRAP | SWT.V_SCROLL);
		m_dpDescTextArea.setTextLimit(240);
		GridData gd_dpDescTextArea = new GridData(SWT.FILL, SWT.FILL, true,
				false, 1, 1);
		gd_dpDescTextArea.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(
				m_dpDescTextArea, 30);
		m_dpDescTextArea.setLayoutData(gd_dpDescTextArea);

		m_stdProductLabel = new Label(m_dpIDPanel, SWT.NONE);
		GridData gd_m_stdProductLabel = new GridData(SWT.LEFT, SWT.TOP, false,
				false, 1, 1);
		m_stdProductLabel.setLayoutData(gd_m_stdProductLabel);

		// TCDECREL-3394
		m_basedOnStdProd = new Combo(m_dpIDPanel, /* SWT.DROP_DOWN | SWT.READ_ONLY */SWT.NONE);
		m_basedOnStdProd.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,false, 1, 1));

	
		populateLOVs();
		setLabels();

		// createDataBinding();
	}

	class CustomComboContentAdapter extends ComboContentAdapter 
	{	
		String content = "";
		String[] values;

		public CustomComboContentAdapter(String[] values) 
		{
			super();
			this.values = values;
		}

		@Override
		public String getControlContents(Control control) 
		{
			String newContent = super.getControlContents(control);
			if (isValidPrefix(newContent)) 
			{
				content = newContent;
			} 
			else 
			{
				setControlContents(control, content, content.length());
			}
			return content;

		}
		
		boolean isValidPrefix(String prefix)
		{
			boolean isValid = false;
			if (prefix.equals("")) 
			{
				isValid = true;
			} 
			else 
			{
				for (String string : values) 
				{
					if (string.regionMatches(true, 0, prefix, 0,prefix.length())) 
					{
						isValid = true;
						break;
					}
				}
			}
			return isValid;
		}
		
	}

	private void populateLOVs() {
		m_stdProductList = new StandardProductList();	
		
		//TCDECREL-3394
		String[] values = m_stdProductList.getStringArray();
		if (m_basedOnStdProd != null && values.length > 0) 
		{
			m_basedOnStdProd.setItems(values);
			new AutoCompleteField(m_basedOnStdProd,new CustomComboContentAdapter(values), values);
		}
		

	}

	public void setEditability(int formMode) {
		m_dpIDText.setEditable(false);
		if (formMode == DPOrderPanel.DP_FORM_DISPLAY) {
			m_basedOnStdProd.setEnabled(false);
		} else if (formMode == DPOrderPanel.CREATE_DP_DISPLAY) {
			m_basedOnStdProd.setEnabled(true);
		}

	}

	private void setLabels() {
		m_dpIDLabel.setText(m_registry.getString("DP_Id.Label"));
		m_dpDescLabel.setText(m_registry.getString("DP_Desc.Label"));
		m_stdProductLabel.setText(m_registry.getString("Based_On_Prod.Label"));
	}

	public void clearPanelValues() {
		m_dpIDText.setText("");
		m_dpDescTextArea.setText("");
		m_basedOnStdProd.deselectAll();
	}

	public void populate(TCComponent deliveredProduct) {
		try {
			String dpId = (String) deliveredProduct.getTCProperty("item_id")
					.getPropertyValue();
			m_dpIDText.setText(dpId);

			String desc = (String) deliveredProduct
					.getTCProperty("object_desc").getStringValue();
			if (desc != null)
				m_dpDescTextArea.setText(desc);

			String stdProd = (String) deliveredProduct.getTCProperty(
					"nov4_basedon_std_prod").getPropertyValue();

			if (stdProd != null) {
				m_basedOnStdProd.setText(stdProd);
			}
		} catch (TCException e) {

			e.printStackTrace();
		}

	}

	public void setDPIDText(String dpId) {
		m_dpIDText.setText(dpId);
	}

	public CreateInObjectHelper populateCreateInObject(
			CreateInObjectHelper object) {
		object.setStringProps(NOV4ComponentDP.DP_NUMBER, m_dpIDText.getText(),
				CreateInObjectHelper.SET_CREATE_IN);
		object.setStringProps(NOV4ComponentDP.DP_DESC,
				m_dpDescTextArea.getText(), CreateInObjectHelper.SET_PROPERTY);

		// TCDECREL-3394
		// if(m_basedOnStdProd.getSelectionIndex()!=-1)
		if (m_basedOnStdProd.indexOf(m_basedOnStdProd.getText()) != -1) {
			// String
			// basedOnProd=m_basedOnStdProd.getItem(m_basedOnStdProd.getSelectionIndex());
			String basedOnProd = m_basedOnStdProd.getItem(m_basedOnStdProd
					.indexOf(m_basedOnStdProd.getText()));
			object.setStringProps(NOV4ComponentDP.DP_BASED_ON_PROD,
					basedOnProd, CreateInObjectHelper.SET_PROPERTY);
		}

		return object;
	}

	public String validateMandatoryFields() {
		StringBuffer invalidInputs = new StringBuffer();
		if (m_dpIDText.getText().length() == 0) {
			invalidInputs.append(m_registry.getString("DP_Id.Name"));
			/*
			 * Color red = new Color (Display.getCurrent(), 255, 0, 0);
			 * m_dpIDLabel.setForeground(red); m_dpIDLabel.setBackground(red);
			 */

		}
		// TCDECREL-3394
		// int index =m_basedOnStdProd.getSelectionIndex();
		int index = m_basedOnStdProd.indexOf(m_basedOnStdProd.getText());
		if (index == -1 || m_basedOnStdProd.getItem(index).length() == 0) {
			if (invalidInputs.length() != 0) {
				invalidInputs.append(",");
			}
			invalidInputs.append(m_registry.getString("Based_On_Prod.Name"));
		}

		return invalidInputs.toString();
	}

	public void markMandatoryFields() {
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry
				.getDefault().getFieldDecoration("NOV_DEC_REQUIRED");

		ControlDecoration decIDText = new ControlDecoration(m_dpIDLabel,
				SWT.TOP | SWT.RIGHT);
		decIDText.setImage(reqFieldIndicator.getImage());
		decIDText.setDescriptionText(reqFieldIndicator.getDescription());

		ControlDecoration decProduct = new ControlDecoration(m_stdProductLabel,
				SWT.TOP | SWT.RIGHT);
		decProduct.setImage(reqFieldIndicator.getImage());
		decProduct.setDescriptionText(reqFieldIndicator.getDescription());

	}

	public void createDataBinding() {

		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		// dataBindingHelper.bindData(m_dpDescTextArea,NOV4ComponentDP.DP_DESC,DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_dpDescTextArea, NOV4ComponentDP.DP_DESC,
				m_dataModelName);
	}

	public void setBindingContext(String dataModelName) {
		m_dataModelName = dataModelName;
	}

	// ankita
	public boolean validateSave() {
		boolean value = true;
		if (m_dpIDText.getText().length() == 0) {
			value = false;

		}
		return value;
	}

}
