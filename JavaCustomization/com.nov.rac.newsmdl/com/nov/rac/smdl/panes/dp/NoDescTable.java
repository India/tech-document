package com.nov.rac.smdl.panes.dp;



import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NoDescTable extends Composite 
{
	protected Composite m_arrayPanel ;
	protected Registry m_Registry;
	protected Button m_addRowButton;
	protected Text m_numberTxt;
	private Text m_desctext;
	protected Button m_remRowButton;
//	protected List m_listBox;
	protected List m_listBox;
	protected Label m_numberLabel;
	private Label m_descLabel;
	
	private String 				m_AttributeName = null;
	private String				m_dataModelName = null;
	//private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	
	public NoDescTable(Composite parent, int style) 
	{
		super(parent, style);
		m_Registry= Registry.getRegistry(this);
	}

	public void createUI(NoDescTable swtArrayTable) 
	{
		m_arrayPanel = swtArrayTable;
		setLayout();
		
		createLabelPanel();
		
		createInputPanel();
		
		createOutputPanel();
		
		addListeners();
		
		//createDataBinding();		
	}

	protected void setLayout()
	{
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 0;
		m_arrayPanel.setLayout(gridLayout);
		GridData gd_Panel = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		m_arrayPanel.setLayoutData(gd_Panel);
	}

	protected void createOutputPanel()
	{
		Composite valuePanelWrap = new Composite (m_arrayPanel, SWT.NONE);
		valuePanelWrap.setLayout(new GridLayout(2, false));
		valuePanelWrap.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
//		Composite valuePanel = new Composite (valuePanelWrap, SWT.EMBEDDED);
//		valuePanel.setLayout(new GridLayout(1, true));
//		GridData gd_valuePanel=new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
//		gd_valuePanel.heightHint=SWTUIHelper.convertVerticalDLUsToPixels(valuePanel, 25);
//		valuePanel.setLayoutData(gd_valuePanel);
//		createTable();
//		JScrollPane scrollpane = new JScrollPane(m_table);
//		scrollpane.setColumnHeaderView(null);
//		SWTUIUtilities.embed( valuePanel,scrollpane , false);
		m_listBox = new List(valuePanelWrap,SWT.SINGLE | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
				
		GridData listLayoutData=new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		listLayoutData.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_listBox, 25);
//		listLayoutData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_addRowButton, 18);
		m_listBox.setLayoutData(listLayoutData);
		
		Image removeIcon = m_Registry.getImage("minus.ICON");
		m_remRowButton = new Button(valuePanelWrap, SWT.NONE);
		m_remRowButton.setImage(removeIcon);
		GridData buttonLayoutData1=new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1);
		buttonLayoutData1.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_addRowButton, 15);
		buttonLayoutData1.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_addRowButton, 18);
		m_remRowButton.setLayoutData (buttonLayoutData1);
	}

	protected void createInputPanel()
	{
		Composite inputPanel = new Composite (m_arrayPanel, SWT.None);
		inputPanel.setLayout(new GridLayout(3, false));
		inputPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		
		m_numberTxt = new Text(inputPanel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_numberTxt = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_numberTxt.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_numberTxt, 20);
		m_numberTxt.setLayoutData( gd_numberTxt);
		
		
		m_desctext = new Text(inputPanel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_desctext = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_desctext.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_desctext, 20);
		m_desctext.setLayoutData( gd_desctext);
		m_desctext.setTextLimit(12);    
		
		Image addIcon = m_Registry.getImage("plus.ICON");
		m_addRowButton = new Button(inputPanel, SWT.NONE);
		m_addRowButton.setImage(addIcon);
		GridData buttonLayoutData= new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1);
		buttonLayoutData.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_addRowButton, 15);
		buttonLayoutData.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_addRowButton, 18);
		m_addRowButton.setLayoutData (buttonLayoutData);
	}

	protected void createLabelPanel() 
	{
		Composite labelPanel = new Composite (m_arrayPanel, SWT.None);
		GridLayout rl_labelPanel = new GridLayout(4, true);
		labelPanel.setLayout(rl_labelPanel);
		labelPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_numberLabel = new Label(labelPanel, SWT.NONE);
		
		Label label = new Label(labelPanel, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		m_descLabel = new Label(labelPanel, SWT.NONE);
		new Label(labelPanel, SWT.NONE);
	}

	
    public void clearAllRows() 
    {
    		m_listBox.removeAll();
    	
	}
    
	protected void addListeners()
	{
			
	        attachAddButtonListener();
	     	        
	        attachRemoveButtonListner();

	      
	}

	protected void attachRemoveButtonListner()
	{
		
		m_remRowButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				 int selectedrow = m_listBox.getSelectionIndex();
		            if(selectedrow<0)
		            {
		            	 MessageBox.post(m_Registry.getString("selectRow.MSG"),
		                            "Warning", 1);
		                    return;
		            }
		            removeRow(selectedrow);
			}
		});
	}

	protected void attachAddButtonListener() 
	{
		m_addRowButton.addSelectionListener(new SelectionListener() {
		  	@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				 if ( m_numberTxt.getText().equals("") )
		            {
		                MessageBox.post(m_Registry.getString("noFieldsValueError.MSG"),
		                        "Warning", 1);
		                return;
		            }
		            else
		            {
		                String cellValueStr = null;
		                if ( m_desctext.getText().equals("") )
		                    cellValueStr = m_numberTxt.getText();
		                else
		                    cellValueStr = m_numberTxt.getText() + ":"
		                            + m_desctext.getText();
		                Object[] cellObj1;
		                if ( cellValueStr != null )
		                {
		                    cellObj1 = new Object[]
		                    { cellValueStr };
		                    addRow(cellObj1);
		                    m_numberTxt.setText("");
		                    m_desctext.setText("");
		                }
		            }
				
			}
		});
	}
	
	
    /**
     * @desc This Method Returns table
     * @return JTable.
     */
//    public JTable getTable()
//    {
//        return m_table;
//    }

    /**
     * @desc This Method Adds row based on 1d or 2D
     * @return void.
     */
    protected void addRow(Object[] cellObj )
    {
    	DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		DataBindingModel model= (DataBindingModel) dataBindingRegistry.getDataModel(m_dataModelName);

		for ( int index = 0; index < cellObj.length; ++index )
        {
        	String newValue = (String) cellObj[index];
			m_listBox.add(newValue);
        	/*String[] oldValues=m_listBox.getItems();
        	
        	java.util.List<String> newValues= Arrays.asList(oldValues);
        	newValues.add(newValue);*/
        	
    		model.setProperty(m_AttributeName, newValue);
			//firePropertyChange(m_listBox, m_AttributeName,null,newValue);
        }
    }
    
    
	protected void removeRow(int selectedrow)
	{
		m_listBox.remove(selectedrow);
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		DataBindingModel model= (DataBindingModel) dataBindingRegistry.getDataModel(m_dataModelName);
		model.setProperty(m_AttributeName, null);
	}

    public String[] getArrayValues()
    {
        String[] strAttr = new String[m_listBox.getItemCount()];
        for ( int i = 0; i < strAttr.length; i++ )
        {
            strAttr[i] = new String();
            strAttr[i] = m_listBox.getItem(i).toString();
        }
        return strAttr;
    }


    public void setArrayValues(String[] strArr )
    {
        addRow(strArr);
    }

	
	public void setDescLabel(String string) 
	{
		m_descLabel.setText(string);
		
	}

	public void setNumberLabel(String string) 
	{
		m_numberLabel.setText(string);
		
	}
	
	public void setAttributeName(String attrName){
		m_AttributeName = attrName;
	}

	public void setBindingContext(String dataModelName)
	{
		m_dataModelName = dataModelName;
		
	}
	
	public void createDataBinding(){
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		dataBindingHelper.bindData(m_listBox,m_AttributeName,m_dataModelName);
	}
	
	public void setTextLimit(int limit) 
	{
		m_numberTxt.setTextLimit(limit);
	}
//Ankita
	 public void clearPanelValues()
	 {
		 clearAllRows();
		 clearFieldValues();
	 }
	 protected void clearFieldValues()
	 {
		 m_numberTxt.setText("");
		 m_desctext.setText("");
		 
	 }

/*	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
	}
	 
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}*/
	
}
