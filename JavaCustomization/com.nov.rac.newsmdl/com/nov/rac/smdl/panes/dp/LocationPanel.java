package com.nov.rac.smdl.panes.dp;

import java.awt.Dimension;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.common.AbstractReferenceLOVList;
import com.nov.rac.smdl.common.NOVArrayTable;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ArraySorter;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;


public class LocationPanel extends Composite 
{
	
	private Combo m_assemLocCombo = null;
	private Combo m_mfgLocCombo = null;
	private Combo m_engLocCombo = null;
	 
	private TCSession m_session;
	
	private Composite m_engJobPanel;
	private Composite comboBoxPanel;
	private Composite jobNoPanel;
	
	private Label m_engLocationLabel;
	private Label m_manLocationLabel;
	private Label m_assembLocationLabel;
	
	private /*NOVArrayTable*/NoTable m_engJobNoTable;
	private /*NOVArrayTable*/NoTable m_manuJobNoTable;
	private Registry m_registry;
	private String m_dataModelName;
		
	private static AbstractReferenceLOVList m_locations = null;
	private static String[] m_locarr = null;

	public LocationPanel(Composite parent, int style) {
		super(parent, style);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.panes.dp.dp");
		m_session=(TCSession)AIFUtility.getDefaultSession();
		//createUI(this);
	}

	public void createUI() {
		
		m_engJobPanel = this;
		m_engJobPanel.setLayout(new GridLayout(1, true));
	
		GridData gd_engJobPanel=new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		m_engJobPanel.setLayoutData(gd_engJobPanel);
		
		populateLOVs();
		
		comboBoxPanel = new Composite(m_engJobPanel,SWT.NONE);
		comboBoxPanel.setLayout(new GridLayout(2, false));
		comboBoxPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		jobNoPanel = new Composite(m_engJobPanel,SWT.NONE);
		jobNoPanel.setLayout(new GridLayout(2, true));
		jobNoPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		m_engLocationLabel = new Label(comboBoxPanel, SWT.NONE);
		
		m_engLocCombo = new Combo(comboBoxPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
		m_engLocCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		NOVSMDLComboUtils.setAutoComboArray(m_engLocCombo, m_locarr);
		
				
		m_manLocationLabel = new Label(comboBoxPanel, SWT.NONE);
		
		m_mfgLocCombo = new Combo(comboBoxPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
		m_mfgLocCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		NOVSMDLComboUtils.setAutoComboArray(m_mfgLocCombo, m_locarr);
		
		m_assembLocationLabel = new Label(comboBoxPanel, SWT.NONE);
	
		m_assemLocCombo = new Combo(comboBoxPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
		m_assemLocCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		NOVSMDLComboUtils.setAutoComboArray(m_assemLocCombo, m_locarr);
		
		m_engJobNoTable = new NoTable(jobNoPanel, SWT.BORDER);
		m_engJobNoTable.setAttributeName(NOV4ComponentDP.DP_ENG_JOB_NUMBERS);
	//	m_engJobNoTable.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
		m_engJobNoTable.createUI(m_engJobNoTable);
		
		m_engJobNoTable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		//Text limit
		m_engJobNoTable.setTextLimit(32);
				
		m_manuJobNoTable = new NoTable(jobNoPanel, SWT.BORDER);
		m_manuJobNoTable.setAttributeName(NOV4ComponentDP.MFG_JOB_NUMBERS);
	//	m_manuJobNoTable.setBindingContext(DeliveredProductPane.DATA_MODEL_NAME);
		m_manuJobNoTable.createUI(m_manuJobNoTable);
		m_manuJobNoTable.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		//Text limit
		m_manuJobNoTable.setTextLimit(64);
		
		setLabels();
		
		//createDataBinding();
	}

	private void setLabels()
	{
		m_engLocationLabel.setText(m_registry.getString("Eng_Locaation.Label"));
		m_manLocationLabel.setText(m_registry.getString("Mfg_Location.Label"));
		m_assembLocationLabel.setText(m_registry.getString("Assy_Location.Label"));
		
		m_engJobNoTable.setLabel(m_registry.getString("Eng_Job_No.Label"));
		m_manuJobNoTable.setLabel(m_registry.getString("Mfg_Job_No.Label"));
		
	}
	
	private void populateLOVs()
	{

        if( m_locations == null )
        {
	        m_locations = new AbstractReferenceLOVList();
	        m_locations.initClass( m_session, NOV4ComponentDP.NOV_LOCATIONS, NOV4ComponentDP.HCMNAME );
	        m_locarr = m_locations.getList();
	        ArraySorter.sort(m_locarr);
        }
        
	}

	public void clearPanelValues()
	{
		m_engLocCombo.deselectAll();
		m_mfgLocCombo.deselectAll();
		m_assemLocCombo.deselectAll();
		m_engJobNoTable.clearAllRows();
		//ankita
		m_engJobNoTable.clearFieldValues();
		m_manuJobNoTable.clearFieldValues();
		m_manuJobNoTable.clearAllRows();
				
	}
	
	public void populate( TCComponent deliveredProduct ) 
	{
		try
		{
			TCComponent engLocationComp = ( TCComponent )deliveredProduct.getTCProperty(
					NOV4ComponentDP.ENG_LOCATIONS ).getReferenceValue();
			TCComponent mfgLocationComp = ( TCComponent )deliveredProduct.getTCProperty( 
					NOV4ComponentDP.MFG_LOCATIONS ).getReferenceValue();
			TCComponent asmLocationComp = ( TCComponent )deliveredProduct.getTCProperty(
					NOV4ComponentDP.ASSY_LOCATION ).getReferenceValue();		

			if( engLocationComp != null )
			{
				String enggLocation = engLocationComp.getTCProperty(
						NOV4ComponentDP.HCMNAME ).toString();
			
				m_engLocCombo.setText(enggLocation);
			}
			
			if( mfgLocationComp != null )
			{			
				String mfgLocation = mfgLocationComp.getTCProperty(
						NOV4ComponentDP.HCMNAME ).toString();
				m_mfgLocCombo.setText( mfgLocation );
			}
			
			if( asmLocationComp != null )
			{				
				String asmLocation = asmLocationComp.getTCProperty(
						NOV4ComponentDP.HCMNAME ).toString();
				//m_assemLocCombo.setSelectedItem( asmLocation );
				m_assemLocCombo.setText(asmLocation);
			}
		}
		catch( TCException tce )
		{
			tce.printStackTrace();
		}
		 
		
        String[] engJobNos =null, manuJobNos= null;
		try 
		{
			engJobNos = (String[]) deliveredProduct.getTCProperty(
					NOV4ComponentDP.DP_ENG_JOB_NUMBERS ).getPropertyValue();
			manuJobNos = (String[]) deliveredProduct.getTCProperty(
					NOV4ComponentDP.MFG_JOB_NUMBERS ).getPropertyValue();
			
		}
		catch( TCException e )
		{
			e.printStackTrace();
		}
		
		
		m_engJobNoTable.setArrayValues(engJobNos);
		
		m_manuJobNoTable.setArrayValues(manuJobNos);
	}

	
	public void populateCreateInObject(CreateInObjectHelper transferObject)
	{
		
		if(m_engLocCombo.getSelectionIndex()!=-1)
		{
			String engLocation=m_engLocCombo.getItem(m_engLocCombo.getSelectionIndex());
			TCComponent engLocationComp = ( TCComponent )m_locations.getReferenceFromString(engLocation);
			transferObject.setTagProps(NOV4ComponentDP.ENG_LOCATIONS, engLocationComp, CreateInObjectHelper.SET_PROPERTY);			
		}
		
		if(m_mfgLocCombo.getSelectionIndex()!=-1)
		{
			String mfgLocation=m_mfgLocCombo.getItem(m_mfgLocCombo.getSelectionIndex());
			TCComponent mfgLocationComp = ( TCComponent )m_locations.getReferenceFromString(mfgLocation);
			transferObject.setTagProps(NOV4ComponentDP.MFG_LOCATIONS, mfgLocationComp, CreateInObjectHelper.SET_PROPERTY);
		}
		
		if(m_assemLocCombo.getSelectionIndex()!=-1)
		{
			String asmLocation=m_assemLocCombo.getItem(m_assemLocCombo.getSelectionIndex());
			TCComponent asmLocationComp = ( TCComponent )m_locations.getReferenceFromString(asmLocation);
			transferObject.setTagProps(NOV4ComponentDP.ASSY_LOCATION, asmLocationComp, CreateInObjectHelper.SET_PROPERTY);
		}
		
		transferObject.setStringArrayProps(NOV4ComponentDP.DP_ENG_JOB_NUMBERS, m_engJobNoTable.getArrayValues(),CreateInObjectHelper.SET_PROPERTY);
		transferObject.setStringArrayProps(NOV4ComponentDP.MFG_JOB_NUMBERS, m_manuJobNoTable.getArrayValues(),CreateInObjectHelper.SET_PROPERTY);
		
	}
	
	public void createDataBinding(){

		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		
		/*dataBindingHelper.bindData(m_assemLocCombo,NOV4ComponentDP.ASSY_LOCATION,DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_mfgLocCombo,NOV4ComponentDP.MFG_LOCATIONS,DeliveredProductPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_engLocCombo,NOV4ComponentDP.ENG_LOCATIONS,DeliveredProductPane.DATA_MODEL_NAME);*/
		dataBindingHelper.bindData(m_assemLocCombo,NOV4ComponentDP.ASSY_LOCATION,m_dataModelName);
		dataBindingHelper.bindData(m_mfgLocCombo,NOV4ComponentDP.MFG_LOCATIONS,m_dataModelName);
		dataBindingHelper.bindData(m_engLocCombo,NOV4ComponentDP.ENG_LOCATIONS,m_dataModelName);
		
		m_engJobNoTable.createDataBinding();
		m_manuJobNoTable.createDataBinding();
	}

	public void setBindingContext(String dataModelName) 
	{
		m_dataModelName = dataModelName;	
		m_engJobNoTable.setBindingContext(dataModelName);
		m_manuJobNoTable.setBindingContext(dataModelName);
	}

	
}
