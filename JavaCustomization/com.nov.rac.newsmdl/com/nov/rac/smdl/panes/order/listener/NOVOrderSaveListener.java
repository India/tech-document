package com.nov.rac.smdl.panes.order.listener;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.panes.order.OrderPanel;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVOrderSaveListener implements SelectionListener {

	private OrderPanel m_orderPanel;
	private TCComponent m_orderComponent;
	private Registry m_appReg;
	
	public NOVOrderSaveListener() {
		m_appReg = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {

	}

	@Override
	public void widgetSelected(SelectionEvent event)
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button saveButton= (Button)target;
			
			m_orderPanel= getOrderPanel(saveButton);
			
			String invalidMandatoryInputs= "";
			invalidMandatoryInputs= m_orderPanel.validateMandatoryFields();
			if(invalidMandatoryInputs.length()==0) {
				
				String invalidFieldValues= "";
				invalidFieldValues= m_orderPanel.validateFieldValues();
				if(invalidFieldValues.length()==0) {				

					Object orderObject=saveButton.getData();
					if(orderObject instanceof NOV4ComponentOrder)
					{
						m_orderComponent= (TCComponent)orderObject;
						ProgressMonitorDialog saveOrderDialog=new ProgressMonitorDialog(saveButton.getShell());
						CreateOrUpdateOperation createOrderOp = new CreateOrUpdateOperation(createOperationInput());
			            try {
			            	saveOrderDialog.run(true, false,createOrderOp);
			            }
			            catch ( Exception e1 )
			            {
			                e1.printStackTrace();
			            }
					}
				} else {
					MessageBox.post(invalidFieldValues, "Error" ,MessageBox.ERROR);
				}
		    } else {
				MessageBox.post(m_appReg.getString("FillMandatoryFields.MSG")+"\n"+invalidMandatoryInputs, "Error" ,MessageBox.ERROR);
			}
			
		}
	}

	private OrderPanel getOrderPanel(Button saveButton)
	{
		Composite parent= null;
		Composite component = saveButton.getParent();
		do {
			
			parent=	component.getParent();
			component=parent;
	
		}while (parent!=null && ! (parent instanceof OrderPanel ));
		
		return (OrderPanel)parent;
	}

	private CreateInObjectHelper createOperationInput()
	{
		CreateInObjectHelper transferObject= new CreateInObjectHelper(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
			
		m_orderPanel.populateCreateInObject(transferObject);
		transferObject.setTargetObject(m_orderComponent);
				
		return transferObject;
	}

}