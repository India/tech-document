package com.nov.rac.smdl.panes.order;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;


public class OrderSalesPanel extends Composite {
	private Label m_salesLabel;
	private Label m_docCtrlJobLabel;
	private Text m_docCtrlJobText;
	private Label m_ordMgrLabel;
	private Label m_docCtrlLabel;
	private Label m_prjMgrLabel;
	private Label m_ordMailLabel;
	private Label m_icCntMgrLabel;
	private Label m_icMailLabel;

	protected Registry m_appReg = null;
	
	private NOVSMDLEmailIDComponent m_salComp;
	//private NOVSMDLEmailIDComponent m_docCtrlJobComp;
	private NOVSMDLEmailIDComponent m_ordMgrComp;
	private NOVSMDLEmailIDComponent m_docCtrlComp;
	private NOVSMDLEmailIDComponent m_prjMgrComp;
	private NOVSMDLEmailIDComponent m_ordMailComp;
	private NOVSMDLEmailIDComponent m_icCntMgrComp;
	private NOVSMDLEmailIDComponent m_icMailComp;
	private String m_dataModelName;

	public OrderSalesPanel (Composite parent, int style){
		super( parent, style );
	    m_appReg = Registry.getRegistry(this);
		//createUI(this);
	}
	
	public void createUI()
	{
		OrderSalesPanel ordSalesPanel = this;
		ordSalesPanel.setLayout(new GridLayout(2, true));
		ordSalesPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		getComp1(ordSalesPanel);
		getComp2(ordSalesPanel);
		
	//	createDataBinding();
	}
	
	private void getComp1(OrderSalesPanel ordSalesPanel) {
	
		Composite comp1 = new Composite(ordSalesPanel,SWT.NONE);
		comp1.setLayout(new GridLayout(2, false));
		comp1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_salesLabel = new Label(comp1, SWT.NONE);
		//m_salesLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_salesLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_salesLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_salesLabel, 85);
		m_salesLabel.setLayoutData(gd_m_salesLabel);			
		m_salesLabel.setText(m_appReg.getString("Salesman.LABEL"));
		
		m_salComp = new NOVSMDLEmailIDComponent(comp1, SWT.NONE);
		m_salComp.setAttributeName(NOV4ComponentOrder.SALESMAN);
//		m_salComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_salComp.createUI();
		m_salComp.setIsUser(true);
		
		m_ordMgrLabel = new Label(comp1, SWT.NONE);
		//m_ordMgrLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_ordMgrLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_ordMgrLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_ordMgrLabel, 85);
		m_ordMgrLabel.setLayoutData(gd_m_ordMgrLabel);		
		m_ordMgrLabel.setText(m_appReg.getString("OrdMgr.LABEL"));
		
		m_ordMgrComp = new NOVSMDLEmailIDComponent(comp1, SWT.NONE);
		m_ordMgrComp.setAttributeName(NOV4ComponentOrder.ORDER_MANAGER);
	//	m_ordMgrComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_ordMgrComp.createUI();
		
		m_ordMgrComp.setIsUser(true);
				
		m_prjMgrLabel = new Label(comp1, SWT.NONE);
		//m_prjMgrLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_prjMgrLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_prjMgrLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_prjMgrLabel, 85);
		m_prjMgrLabel.setLayoutData(gd_m_prjMgrLabel);		
		m_prjMgrLabel.setText(m_appReg.getString("ProjMgr.LABEL"));
		
		m_prjMgrComp = new NOVSMDLEmailIDComponent(comp1, SWT.NONE);
		m_prjMgrComp.setAttributeName(NOV4ComponentOrder.PROJECT_MANAGER);
	//	m_prjMgrComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_prjMgrComp.createUI();
		
		m_prjMgrComp.setIsUser(true);
		
		m_icCntMgrLabel = new Label(comp1, SWT.NONE);
		//m_icCntMgrLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_icCntMgrLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_icCntMgrLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_icCntMgrLabel, 85);
		m_icCntMgrLabel.setLayoutData(gd_m_icCntMgrLabel);	
		m_icCntMgrLabel.setText(m_appReg.getString("ICConMgr.LABEL"));
		
		m_icCntMgrComp = new NOVSMDLEmailIDComponent(comp1, SWT.NONE);
		m_icCntMgrComp.setAttributeName(NOV4ComponentOrder.IC_CONTACT_MANAGER);
	//	m_icCntMgrComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_icCntMgrComp.createUI();
		
		m_icCntMgrComp.setIsUser(true);
	
	}
	
	private void getComp2(OrderSalesPanel ordSalesPanel) {
	
		Composite comp2 = new Composite(ordSalesPanel,SWT.NONE);
		comp2.setLayout(new GridLayout(2, false));
		comp2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_docCtrlJobLabel = new Label(comp2, SWT.NONE);
		//m_docCtrlJobLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_docCtrlJobLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_docCtrlJobLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_docCtrlJobLabel, 95);
		m_docCtrlJobLabel.setLayoutData(gd_m_docCtrlJobLabel);		
		m_docCtrlJobLabel.setText(m_appReg.getString("DocCtrlJob.LABEL"));

		Composite docCtrlTextComp = new Composite(comp2, SWT.NONE);
		docCtrlTextComp.setLayout(new GridLayout(1, true));
		docCtrlTextComp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		m_docCtrlJobText = new Text(docCtrlTextComp, SWT.BORDER);
		m_docCtrlJobText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_docCtrlLabel = new Label(comp2, SWT.NONE);
		//m_docCtrlLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_docCtrlLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_docCtrlLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_docCtrlLabel, 95);
		m_docCtrlLabel.setLayoutData(gd_m_docCtrlLabel);		
		m_docCtrlLabel.setText(m_appReg.getString("DocController.LABEL"));
		
		m_docCtrlComp = new NOVSMDLEmailIDComponent(comp2, SWT.NONE);
		m_docCtrlComp.setAttributeName(NOV4ComponentOrder.DOCUMENT_CONTROLLER);
	//	m_docCtrlComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_docCtrlComp.createUI();
		
		m_docCtrlComp.setIsUser(true);

		m_ordMailLabel = new Label(comp2, SWT.NONE);
		m_ordMailLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
//		GridData gd_m_ordMailLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
//		gd_m_ordMailLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_ordMailLabel, 95);
//		m_ordMailLabel.setLayoutData(gd_m_ordMailLabel);		
		m_ordMailLabel.setText(m_appReg.getString("OrdMbx.LABEL"));
		
		m_ordMailComp = new NOVSMDLEmailIDComponent(comp2, SWT.NONE);
		m_ordMailComp.setAttributeName(NOV4ComponentOrder.ORDER_MAILBOX);
	//	m_ordMailComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_ordMailComp.createUI();
		
		m_ordMailComp.setIsUser(false);

		m_icMailLabel = new Label(comp2, SWT.NONE);
		//m_icMailLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_icMailLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_m_icMailLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_icMailLabel, 95);
		m_icMailLabel.setLayoutData(gd_m_icMailLabel);		
		m_icMailLabel.setText(m_appReg.getString("ICMbx.LABEL"));
		
		m_icMailComp = new NOVSMDLEmailIDComponent(comp2, SWT.NONE);
		m_icMailComp.setAttributeName(NOV4ComponentOrder.IC_MAILBOX);
	//	m_icMailComp.setBindingContext(OrderPane.DATA_MODEL_NAME);
		m_icMailComp.createUI();
		
		m_icMailComp.setIsUser(false);
	}
	
	
	public void setEditability(Boolean editOrd)
	{
		m_salComp.setEnabled(editOrd);
		//m_docCtrlJobComp.setEnabled(editOrd);
		m_docCtrlJobText.setEnabled(editOrd);
		m_ordMgrComp.setEnabled(editOrd);
		m_docCtrlComp.setEnabled(editOrd);
		m_prjMgrComp.setEnabled(editOrd);
		m_ordMailComp.setEnabled(editOrd);
		m_icCntMgrComp.setEnabled(editOrd);
		m_icMailComp.setEnabled(editOrd);
	}
	
	public void clearPanelValues()
	{
		m_salComp.setTextVal("");
		m_docCtrlJobText.setText("");
		m_ordMgrComp.setTextVal("");
		m_docCtrlComp.setTextVal("");
		m_prjMgrComp.setTextVal("");
		m_ordMailComp.setTextVal("");
		m_icCntMgrComp.setTextVal("");
		m_icMailComp.setTextVal("");
	}	
	
	
	public void populate(TCComponent order) {
		try {
			if (order != null) {
				String salesStr = (String) order.getTCProperty(NOV4ComponentOrder.SALESMAN).getPropertyValue();
				if(salesStr!=null){
					m_salComp.setTextVal(salesStr);
				}
				
				String docCtrlJobStr = (String) order.getTCProperty(NOV4ComponentOrder.DOCUMENT_CONTROL).getPropertyValue();
				if(docCtrlJobStr!=null){
					m_docCtrlJobText.setText(docCtrlJobStr);
				}
				
				String ordMgrStr = (String) order.getTCProperty(NOV4ComponentOrder.ORDER_MANAGER).getPropertyValue();
				if(ordMgrStr!=null){
					m_ordMgrComp.setTextVal(ordMgrStr);
				}		
				
				String docCtrlStr = (String) order.getTCProperty(NOV4ComponentOrder.DOCUMENT_CONTROLLER).getPropertyValue();
				if(docCtrlStr!=null){
					m_docCtrlComp.setTextVal(docCtrlStr);
				}
				
				String prjMgrStr = (String) order.getTCProperty(NOV4ComponentOrder.PROJECT_MANAGER).getPropertyValue();
				if(prjMgrStr!=null){
					m_prjMgrComp.setTextVal(prjMgrStr);
				}
				
				String ordMailStr = (String) order.getTCProperty(NOV4ComponentOrder.ORDER_MAILBOX).getPropertyValue();
				if(ordMailStr!=null){
					m_ordMailComp.setTextVal(ordMailStr);
				}
				
				String icCntMgrStr = (String) order.getTCProperty(NOV4ComponentOrder.IC_CONTACT_MANAGER).getPropertyValue();
				if(icCntMgrStr!=null){
					m_icCntMgrComp.setTextVal(icCntMgrStr);
				}
				
				String icMailStr = (String) order.getTCProperty(NOV4ComponentOrder.IC_MAILBOX).getPropertyValue();
				if(icMailStr!=null) {
					m_icMailComp.setTextVal(icMailStr);
				}
			}
		} catch (TCException e) {
			e.printStackTrace();
		}
	}
	
	public CreateInObjectHelper populateCreateInObject(CreateInObjectHelper object)
	{
		if(m_salComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.SALESMAN,m_salComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_docCtrlJobText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.DOCUMENT_CONTROL,m_docCtrlJobText.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_ordMgrComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.ORDER_MANAGER,m_ordMgrComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_docCtrlComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.DOCUMENT_CONTROLLER,m_docCtrlComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_prjMgrComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.PROJECT_MANAGER,m_prjMgrComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_ordMailComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.ORDER_MAILBOX,m_ordMailComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_icCntMgrComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.IC_CONTACT_MANAGER,m_icCntMgrComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_icMailComp.getTextVal() != null) {
			object.setStringProps(NOV4ComponentOrder.IC_MAILBOX,m_icMailComp.getTextVal(),CreateInObjectHelper.SET_PROPERTY);
		}

		return object;
	}
	
	public void markMandatoryFields()
	{
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry.getDefault().getFieldDecoration("NOV_DEC_REQUIRED");
		
		ControlDecoration decTxtFld = new ControlDecoration(m_ordMailLabel, SWT.TOP | SWT.RIGHT);		
		decTxtFld.setImage(reqFieldIndicator.getImage());
		decTxtFld.setDescriptionText(reqFieldIndicator.getDescription());		
		
	}
	
	public String validateMandatoryFields() 
	{
		StringBuffer invalidInputs = new StringBuffer();
		if(m_ordMailComp.getTextVal() == null || (m_ordMailComp.getTextVal()).length() == 0) {
			invalidInputs.append(m_appReg.getString("MandatoryFldOrdMbx.LABEL"));
		}
		return invalidInputs.toString();
	}

	public void createDataBinding()
	{
		m_salComp.createDataBinding();
		m_ordMgrComp.createDataBinding();
		m_prjMgrComp.createDataBinding();
		m_icCntMgrComp.createDataBinding();
		m_docCtrlComp.createDataBinding();
		m_ordMailComp.createDataBinding();
		m_icMailComp.createDataBinding();
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		//dataBindingHelper.bindData(m_docCtrlJobText,NOV4ComponentOrder.DOCUMENT_CONTROL,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_docCtrlJobText,NOV4ComponentOrder.DOCUMENT_CONTROL,m_dataModelName);
	}

	public void setBindingContext(String orderDataModelName)
	{
		m_dataModelName = orderDataModelName;
		m_salComp.setBindingContext(orderDataModelName);
		m_ordMgrComp.setBindingContext(orderDataModelName);
		m_prjMgrComp.setBindingContext(orderDataModelName);
		m_icCntMgrComp.setBindingContext(orderDataModelName);
		m_docCtrlComp.setBindingContext(orderDataModelName);
		m_ordMailComp.setBindingContext(orderDataModelName);
		m_icMailComp.setBindingContext(orderDataModelName);
	}
}
