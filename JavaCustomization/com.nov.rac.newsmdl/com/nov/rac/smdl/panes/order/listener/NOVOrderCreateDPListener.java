package com.nov.rac.smdl.panes.order.listener;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;

import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.panes.dp.CreateDPDialog;
import com.nov.rac.smdl.panes.order.CreateOrderDialog;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVOrderCreateDPListener implements SelectionListener {
	
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	
	public NOVOrderCreateDPListener() {
	
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		
		Object target=event.getSource();
		
		if (target instanceof Button)
		{
			Button createDpButton=((Button)target);
			Object orderObject=createDpButton.getData();
			if (orderObject instanceof CreateOrderDialog) { // For Order Creation from Create Order Dialog
				CreateDPDialog createDP = new CreateDPDialog(createDpButton.getDisplay().getActiveShell(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL |SWT.RESIZE );
				createDP.setOrderDialog((CreateOrderDialog) orderObject);
				createDP.open();
				//((CreateOrderDialog) orderObject).setDPObjectHelper(createDP.getTransferObject());

			} else if(orderObject instanceof NOV4ComponentOrder) { // For DP Creation from Order Form
				CreateDPDialog createDP = new CreateDPDialog(createDpButton.getDisplay().getActiveShell(), SWT.DIALOG_TRIM 	| SWT.APPLICATION_MODAL |SWT.RESIZE);
				createDP.setOrder((TCComponent)orderObject);
				
				createDP.addPropertyChangeListeners(m_listeners);
				
				createDP.open();
			}
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {

		if(!m_listeners.contains(listener) ){
			m_listeners.add(listener);
		}
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		m_listeners.remove(listener);
	}
}
