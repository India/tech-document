package com.nov.rac.smdl.panes.order;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

import javax.swing.JTable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.NOVSMDLOrderDPTable;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.smdl.common.editors.NOVSMDLRemoveDPButtonEditor;
import com.nov.rac.smdl.common.renderers.NOVSMDLRemoveDPButtonRenderer;
import com.nov.rac.smdl.utilities.NOVSMDLOrderHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class OrderDPTablePanel extends Composite {

	private Label m_dpOnOrderLabel;
	private NOVSMDLOrderDPTable m_ordDPTable;
	private String[] m_columnNames;

	private Composite m_ordDpTableComposite;
	protected Registry m_appReg = null;
	protected TCSession m_session = null;
	
	private PropertyChangeListener 	m_PropertyListeners = null;
	
	public OrderDPTablePanel (Composite parent, int style){
		super(parent, style);
		m_session= (TCSession)AIFUtility.getDefaultSession();
		m_appReg = Registry.getRegistry(this);
		//createUI(this);
	}
	
	public void createUI()
	{
		OrderDPTablePanel ordDPTabPanel =this;
		ordDPTabPanel.setLayout(new GridLayout(1, true));
		GridData gd = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		//gd.heightHint = 250;
		gd.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(ordDPTabPanel, 100);
		ordDPTabPanel.setLayoutData(gd);
		
		m_dpOnOrderLabel = new Label(ordDPTabPanel, SWT.NONE);
		m_dpOnOrderLabel.setText(m_appReg.getString("DPOnOrder.LABEL"));
		
		m_ordDpTableComposite = new Composite(ordDPTabPanel, SWT.EMBEDDED);
		GridData gd_m_ordDpTableComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		m_ordDpTableComposite.setLayoutData(gd_m_ordDpTableComposite);

        createOrdDPTable();
		
		ScrollPagePane tablePane= new ScrollPagePane(m_ordDPTable);
		SWTUIUtilities.embed( m_ordDpTableComposite, tablePane, false);
	}
	
	public void setEditability(Boolean editOrd)
	{
		m_ordDpTableComposite.setEnabled(editOrd);
	}
		
	private void createOrdDPTable() 
	{
		
//		m_columnNames =new String[] { "item_id", "nov4_basedon_std_prod", "current_name", 
//				"nov4_contract_delivery_date","Remove"};
//        String[] colDisplayNames = new String[] { m_appReg.getString("DPID.LABEL"), 
//        		m_appReg.getString("DPStdProdModel.LABEL"), m_appReg.getString("DPName.LABEL"),  
//        		m_appReg.getString("DPProdContDelDate.LABEL"),m_appReg.getString("DPRemove.LABEL")};
        
		m_columnNames = NOVSMDLOrderHelper.getOrderDPTableColumnNames();
		String[] colDisplayNames = NOVSMDLOrderHelper.getOrderDPTableColumnDisplayNames();
			
        m_ordDPTable = new NOVSMDLOrderDPTable(m_session, colDisplayNames);
        m_ordDPTable.setRowHeight(22);

        m_ordDPTable.getColumn(m_appReg.getString("DPRemove.LABEL")).setMaxWidth(70);
		m_ordDPTable.getColumn(m_appReg.getString("DPRemove.LABEL")).setCellRenderer(new NOVSMDLRemoveDPButtonRenderer());
		m_ordDPTable.getColumn(m_appReg.getString("DPRemove.LABEL")).setCellEditor(new NOVSMDLRemoveDPButtonEditor(m_ordDPTable));
		
        m_ordDPTable.setBackground(Color.WHITE);
            
       ((JTable) m_ordDPTable).setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	}

	public void clearValues()
	{
		m_ordDPTable.removeAllRows();
	}
	
	public void populate(TCComponent order) {
		try {
			if(order != null) {
				TCComponent[] dpObjects=order.getTCProperty("_OrderDeliveredProduct_").getReferenceValueArray();
				for(TCComponent dpObject:dpObjects) {
					Object[] dpProps=getProperties(dpObject);
					m_ordDPTable.addRow(dpProps);
				}
			}
			
		} catch (TCException e) {
			
			e.printStackTrace();
		}
	}
	
	public void populateDP(CreateInObjectHelper dpObjHelper) {
		if(dpObjHelper != null) {
			
			Object[] dpProps = new Object[m_columnNames.length];
			 
			dpProps[0] = dpObjHelper.getStringProperty(NOV4ComponentDP.DP_NUMBER);
			
			String basedOnProd = dpObjHelper.getStringProperty(NOV4ComponentDP.DP_BASED_ON_PROD);
			
			if(basedOnProd != null){
				String[] temp = basedOnProd.split(":");
				
				dpProps[1] = temp[0];
				
				if(temp.length >1){
					dpProps[2] = temp[1];
				}
			}
			
			dpProps[3] = dpObjHelper.getDateProperty(NOV4ComponentDP.CONTRACT_DELIVERY_DATE);

//			Map dpPropMap=dpObjHelper.getNameValueMap();
//
//			if(!dpPropMap.isEmpty()) {
//				com.teamcenter.services.rac.core._2007_01.DataManagement.VecStruct valueStrVec = null;
//				valueStrVec = (com.teamcenter.services.rac.core._2007_01.DataManagement.VecStruct)dpPropMap.get("nov4_basedon_std_prod");
//				if(valueStrVec != null) {
//					String[] temp = valueStrVec.stringVec[0].split(":");
//					dpProps[1] = temp[0];
//					dpProps[2] = temp[1];
//					valueStrVec = null;
//				}
//				
//				valueStrVec = (com.teamcenter.services.rac.core._2007_01.DataManagement.VecStruct)dpPropMap.get("nov4_contract_delivery_date");
//				if(valueStrVec != null) {
//					dpProps[3] = valueStrVec.stringVec[0];
//				}
				m_ordDPTable.addRow(dpProps);
//			}
		}
	}
	
	public TCTable getOrdDPTable() {
		return m_ordDPTable;
	}
	
	private Object[] getProperties(TCComponent dpObject)
	{
		Object[] dpProps = new Object[m_columnNames.length-1];
		try {
			dpProps[0] = dpObject.getTCProperty("item_id").getPropertyValue();
			String basedOnStr = (String)dpObject.getTCProperty("nov4_basedon_std_prod").getPropertyValue();
			if(basedOnStr != null) {
				String[] temp = basedOnStr.split(":");
				dpProps[1] = temp[0];
				if(temp.length>1)
				{
					dpProps[2] = temp[1];
				}
				
			}
			dpProps[3] = dpObject.getTCProperty("nov4_contract_delivery_date").getPropertyValue();
//			for(int iCnt = 0; iCnt<m_columnNames.length-1;iCnt++){
//				dpProps[iCnt] = dpObject.getTCProperty(m_columnNames[iCnt]).getPropertyValue();	
//			}			
		} catch (TCException e) {
			e.printStackTrace();
		}
		return dpProps;
	}

	public void updateDPTable(TCComponent theCreatedObject) {
		Object[] dpProps=getProperties(theCreatedObject);
		m_ordDPTable.addRow(dpProps);
		
		firePropertyChange(this, "AddSMDLObjectOperation","" , theCreatedObject);
		theCreatedObject.fireComponentChangeEvent();
	}
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		m_PropertyListeners.propertyChange( changeEvent );
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		m_PropertyListeners = listener;
	}
}
