package com.nov.rac.smdl.panes.order;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class OrderExtPanel extends Composite {
	private Text m_extProjText;
	private Text m_projSiteText;
	private Text m_extCntText;
	
	private Label m_extProjLabel;
	private Label m_projSiteLabel;
	private Label m_cntDestLabel;
	private Label m_extCntLabel;
	
	private Combo m_cntDestCombo = null;
	protected Registry m_appReg = null;
	private String m_dataModelName;
	
	public OrderExtPanel (Composite parent, int style){
		super(parent, style);
		m_appReg = Registry.getRegistry(this);
		//createUI(this);
	}
	
	public void createUI()
	{
		OrderExtPanel ordExtPanel = this;
		ordExtPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		ordExtPanel.setLayout(new GridLayout(2, true));
		
		getComp1(ordExtPanel);
		getComp2(ordExtPanel);
		
		//createDataBinding();
	}
	
	private void getComp1(OrderExtPanel ordExtPanel){
		Composite comp1 = new Composite(ordExtPanel,SWT.NONE);
		comp1.setLayout(new GridLayout(2, false));
		comp1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_cntDestLabel = new Label(comp1, SWT.NONE);
		//m_cntDestLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_cntDestLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_cntDestLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_cntDestLabel, 85);
		m_cntDestLabel.setLayoutData(gd_m_cntDestLabel);			
		m_cntDestLabel.setText(m_appReg.getString("CntDest.LABEL"));
		
		m_cntDestCombo = new Combo(comp1, SWT.DROP_DOWN | SWT.BORDER);
		m_cntDestCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		populateLOVs();
		
		m_extProjLabel = new Label(comp1, SWT.NONE);
		//m_extProjLabel.setLayoutData(new GridData(SWT.LEFT, SWT.LEFT, false, false, 1, 1));
		GridData gd_m_extProjLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_extProjLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_extProjLabel, 85);
		m_extProjLabel.setLayoutData(gd_m_extProjLabel);		
		m_extProjLabel.setText(m_appReg.getString("ExtProj.LABEL"));

		m_extProjText = new Text(comp1, SWT.BORDER);
		m_extProjText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
	}
	
	private void getComp2(OrderExtPanel ordExtPanel){
		
		Composite comp2 = new Composite(ordExtPanel,SWT.NONE);
		comp2.setLayout(new GridLayout(2, false));
		comp2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_projSiteLabel = new Label(comp2, SWT.NONE);
		//m_projSiteLabel.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, false, false, 1, 1));
		GridData gd_m_projSiteLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_projSiteLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_projSiteLabel, 95);
		m_projSiteLabel.setLayoutData(gd_m_projSiteLabel);			
		m_projSiteLabel.setText(m_appReg.getString("ProjSite.LABEL"));
		
		m_projSiteText = new Text(comp2, SWT.BORDER);
		m_projSiteText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		m_extCntLabel = new Label(comp2, SWT.NONE);
		//m_extCntLabel.setLayoutData(new GridData(SWT.LEFT, SWT.LEFT, false, false, 1, 1));
		GridData gd_m_extCntLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_extCntLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_extCntLabel, 95);
		m_extCntLabel.setLayoutData(gd_m_extCntLabel);		
		m_extCntLabel.setText(m_appReg.getString("ExtCont.LABEL"));
		
		m_extCntText = new Text(comp2, SWT.BORDER);
		m_extCntText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
	}
	
	public void setEditability(Boolean editOrd)
	{
			m_extProjText.setEditable(editOrd);
			m_projSiteText.setEditable(editOrd);
			m_cntDestCombo.setEnabled(editOrd);
			m_extCntText.setEditable(editOrd);
	}
	
	private void populateLOVs() {
		LOVObject[] arrLOVObjs = LOVUtils.getLOVObjectsForLOV("Nov4_Ultimate_Destination");
		NOVSMDLComboUtils.setAutoComboLOV(m_cntDestCombo, arrLOVObjs);
	}
	
	public void clearPanelValues()
	{
		m_extProjText.setText("");
		m_cntDestCombo.deselectAll();
		m_projSiteText.setText("");
		m_extCntText.setText("");
	}	
	
	public void populate(TCComponent order) {
		try {
			if(order != null) {	
				String extProjStr = (String) order.getTCProperty(NOV4ComponentOrder.EXTERNAL_PROJECT_NUM).getPropertyValue();
				if(extProjStr!=null){
					m_extProjText.setText(extProjStr);
				}	
				
				String cntDestCmdStr = (String) order.getTCProperty(NOV4ComponentOrder.COUNTRY_DESTINATION).getPropertyValue();
				if(cntDestCmdStr!=null) {
					int nIndex = m_cntDestCombo.indexOf(cntDestCmdStr);
					if(nIndex > -1) {
						m_cntDestCombo.select(nIndex);
					}
				}
				
				String projSiteStr = (String) order.getTCProperty(NOV4ComponentOrder.PROJECT_SITE).getPropertyValue();
				if(projSiteStr!=null){
					m_projSiteText.setText(projSiteStr);
				}
				
				String extCntStr = (String) order.getTCProperty(NOV4ComponentOrder.EXTERNAL_CONTACT).getPropertyValue();
				if(extCntStr!=null){
					m_extCntText.setText(extCntStr);
				}
			  }
		} catch (TCException e) {
			e.printStackTrace();
		}		
	}
	
	public CreateInObjectHelper populateCreateInObject(CreateInObjectHelper object)
	{
		if(m_extProjText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.EXTERNAL_PROJECT_NUM,m_extProjText.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		
		int cntDestIdx = m_cntDestCombo.indexOf(m_cntDestCombo.getText());
		if(cntDestIdx == -1) {
			m_cntDestCombo.setText("");
		}
		if(m_cntDestCombo.getItems().length > 0 && m_cntDestCombo.getSelectionIndex() != -1) {
			object.setStringProps(NOV4ComponentOrder.COUNTRY_DESTINATION,m_cntDestCombo.getItem(m_cntDestCombo.getSelectionIndex()),CreateInObjectHelper.SET_PROPERTY);
		} else if(!m_cntDestCombo.getText().isEmpty()) {
			object.setStringProps(NOV4ComponentOrder.COUNTRY_DESTINATION,m_cntDestCombo.getText(),CreateInObjectHelper.SET_PROPERTY);
		} 
		if(m_projSiteText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.PROJECT_SITE,m_projSiteText.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_extCntText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.EXTERNAL_CONTACT,m_extCntText.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		return object;
	}
	
	public String validateFieldValues() 
	{
		StringBuffer invalidFieldValues= new StringBuffer();
		
		if(m_cntDestCombo.indexOf(m_cntDestCombo.getText()) == -1) {
			if(invalidFieldValues.length()!=0) {
				invalidFieldValues.append("\n");
			}			
			invalidFieldValues.append(m_appReg.getString("InvalidCountryValue.MSG"));
		}
		return invalidFieldValues.toString();
	}

	public void createDataBinding(){

		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		
		/*dataBindingHelper.bindData(m_extProjText,NOV4ComponentOrder.EXTERNAL_PROJECT_NUM,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_cntDestCombo,NOV4ComponentOrder.COUNTRY_DESTINATION,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_projSiteText,NOV4ComponentOrder.PROJECT_SITE,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_extCntText, NOV4ComponentOrder.EXTERNAL_CONTACT,OrderPane.DATA_MODEL_NAME);*/
		
		dataBindingHelper.bindData(m_extProjText,NOV4ComponentOrder.EXTERNAL_PROJECT_NUM,m_dataModelName);
		dataBindingHelper.bindData(m_cntDestCombo,NOV4ComponentOrder.COUNTRY_DESTINATION,m_dataModelName);
		dataBindingHelper.bindData(m_projSiteText,NOV4ComponentOrder.PROJECT_SITE,m_dataModelName);
		dataBindingHelper.bindData(m_extCntText, NOV4ComponentOrder.EXTERNAL_CONTACT,m_dataModelName);
		
	}

	public void setBindingContext(String orderDataModelName)
	{
		m_dataModelName = orderDataModelName;
	}
	
	
}