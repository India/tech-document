package com.nov.rac.smdl.panes.order;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.panes.order.listener.NOVOrderCreateDPListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class OrderDPButtonPanel extends Composite {
	private Button m_createDPButton;
	//private Button m_saveOrderButton;
	
	private NOVOrderCreateDPListener m_CreateDPListeners = null;

	protected Registry m_appReg = null;

	public OrderDPButtonPanel (Composite parent, int style){
		super(parent, style);
		m_appReg = Registry.getRegistry(this);
		//createUI(this);
	}
	
	public void createUI()
	{
		OrderDPButtonPanel ordBtnPanel=this;
		ordBtnPanel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		setLayout(new GridLayout(2, false));
		
		m_createDPButton = new Button(ordBtnPanel, SWT.PUSH);
		m_createDPButton.setText(m_appReg.getString("OrdCreateDP.LABEL"));
		
	/*	m_saveOrderButton = new Button(ordBtnPanel, SWT.PUSH);
		m_saveOrderButton.setText(m_appReg.getString("OrdSave.LABEL"));*/
				
		addListeners();
	}
	
	public void setEditability(Boolean editOrd)
	{
		m_createDPButton.setEnabled(editOrd);
		//m_saveOrderButton.setEnabled(editOrd);
	}
	
	public void setOrderCreation(CreateOrderDialog createOrdDlg)
	{
			m_createDPButton.setData(createOrdDlg);
		//	m_saveOrderButton.setVisible(false);
	}
	
	private void addListeners()
	{
		m_CreateDPListeners = new NOVOrderCreateDPListener();
		m_createDPButton.addSelectionListener(m_CreateDPListeners);
		//m_saveOrderButton.addSelectionListener( new NOVOrderSaveListener());
	}
	
	public void populate(TCComponent orderSOAPlainObject) 
	{
		m_createDPButton.setData(orderSOAPlainObject);
		//m_saveOrderButton.setData(orderSOAPlainObject);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		m_CreateDPListeners.addPropertyChangeListener(listener);
	}
}
