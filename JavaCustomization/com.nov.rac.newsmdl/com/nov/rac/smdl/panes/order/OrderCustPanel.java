package com.nov.rac.smdl.panes.order;

import java.util.Date;
import java.util.Vector;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.common.NOVAbstractNameValidator;
import com.nov.rac.smdl.common.NOVArrayHelper;
import com.nov.rac.smdl.common.NOVDateButtonComposite;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.smdl.panes.order.listener.NOVOrderCustomerComboListener;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.smdl.utilities.NOVSMDLOrderHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;

public class OrderCustPanel extends Composite {
	private Text m_salesText;
	private Text m_purchOrdText;

	private Label m_custLabel;
	private Label m_salesLabel;
	private Label m_plantLabel;
	private Label m_cntAwdDateLabel;
	private Label m_purchOrdLabel;
	private Label m_cntDelDateLabel;

	private NOVDateButtonComposite m_cntDelDateComposite;
	private NOVDateButtonComposite m_cntAwdDateComposite;
	
	private Combo m_custCombo;
	private Combo m_plantCombo;

	protected Registry m_appReg = null;
	private String m_dataModelName;
	
	private String[] m_customerNames = null;
	private String[] m_plantNames = null;
	
	protected StringValidationToolkit m_strValToolkit = null;

	public OrderCustPanel (Composite parent, int style){
		super( parent, style );
		m_appReg = Registry.getRegistry(this);
		//createUI(this);
	}

	public void createUI()
	{
		OrderCustPanel ordCustPanel=this;
		ordCustPanel.setLayout(new GridLayout(2, true));
		ordCustPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		getComp1(ordCustPanel);
		getComp2(ordCustPanel);
		
		//createDataBinding();
	}
	
	private void getComp1(OrderCustPanel ordCustPanel){
	
		Composite comp1 = new Composite(ordCustPanel,SWT.NONE);
		comp1.setLayout(new GridLayout(2, false));
		comp1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_custLabel = new Label(comp1, SWT.NONE);
		m_custLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		//GridData gd_m_custLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		//gd_m_custLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_custLabel, 85);
		//m_custLabel.setLayoutData(gd_m_custLabel);	
		m_custLabel.setText(m_appReg.getString("Customer.LABEL"));

		m_custCombo = new Combo(comp1, SWT.DROP_DOWN | SWT.BORDER);
		m_custCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		m_plantLabel = new Label(comp1, SWT.NONE);
		m_plantLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		//GridData gd_m_plantLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		//gd_m_plantLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_plantLabel, 85);
		//m_plantLabel.setLayoutData(gd_m_plantLabel);			
		m_plantLabel.setText(m_appReg.getString("PlantRig.LABEL"));
		
		m_plantCombo = new Combo(comp1, SWT.DROP_DOWN | SWT.BORDER);
		m_plantCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		populateLOVs();
		m_strValToolkit = new StringValidationToolkit(SWT.TOP | SWT.RIGHT,2, false);
		final IFieldValidator custComboValidator = new NOVAbstractNameValidator()
		{			
			public String getErrorMessage() 
			{
				errorMessage = m_appReg.getString("InvalidCustValue.MSG");
				return errorMessage;
			}
			@Override
			public boolean isValid(String contents) 
			{		
				boolean custNameValid = validateName(contents);
				
				processUpdate(custNameValid);
		
				return custNameValid;
			}
			
		};
		final ValidatingField<String> comboField = m_strValToolkit.createField(m_custCombo, custComboValidator, true, "");


		m_purchOrdLabel = new Label(comp1, SWT.NONE);
		//m_purchOrdLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_purchOrdLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_purchOrdLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_purchOrdLabel, 85);
		m_purchOrdLabel.setLayoutData(gd_m_purchOrdLabel);			
		m_purchOrdLabel.setText(m_appReg.getString("PurchOrder.LABEL"));

		m_purchOrdText = new Text(comp1, SWT.BORDER);
		m_purchOrdText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		//addListeners();
		//populateLOVs();
	}
	
	private void getComp2(OrderCustPanel ordCustPanel){
		
		Composite comp2 = new Composite(ordCustPanel,SWT.NONE);
		comp2.setLayout(new GridLayout(2, false));
		comp2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_salesLabel = new Label(comp2, SWT.NONE);
		//m_salesLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_salesLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_salesLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_salesLabel, 95);
		m_salesLabel.setLayoutData(gd_m_salesLabel);		
		m_salesLabel.setText(m_appReg.getString("SalesOrder.LABEL"));
		
		m_salesText = new Text(comp2, SWT.BORDER);
		m_salesText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		m_cntAwdDateLabel = new Label(comp2, SWT.NONE);
		m_cntAwdDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		//GridData gd_m_cntAwdDateLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		//gd_m_cntAwdDateLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_cntAwdDateLabel, 95);
		//m_cntAwdDateLabel.setLayoutData(gd_m_cntAwdDateLabel);		
		m_cntAwdDateLabel.setText(m_appReg.getString("ContAwdDate.LABEL"));
		
		m_cntAwdDateComposite=new NOVDateButtonComposite(comp2, SWT.NONE);
		GridData gd_cntAwdDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		//gd_cntAwdDateComposite.heightHint = 25;
		gd_cntAwdDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_cntAwdDateComposite, 15);
		m_cntAwdDateComposite.setLayoutData(gd_cntAwdDateComposite);
		m_cntAwdDateComposite.getDateButton().setDisplayFormat("dd-MMM-yyyy");
		
		m_cntAwdDateComposite.setAttributeName(NOV4ComponentOrder.CONTRACT_AWARD_DATE);
	//	m_cntAwdDateComposite.setBindingContext(OrderPane.DATA_MODEL_NAME);
	//	m_cntAwdDateComposite.createDataBinding();

		m_cntDelDateLabel = new Label(comp2, SWT.NONE);
		m_cntDelDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		//GridData gd_m_cntDelDateLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		//gd_m_cntDelDateLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_cntDelDateLabel, 95);
		//m_cntDelDateLabel.setLayoutData(gd_m_cntDelDateLabel);			
		m_cntDelDateLabel.setText(m_appReg.getString("ContDelDate.LABEL"));

		m_cntDelDateComposite=new NOVDateButtonComposite(comp2, SWT.NONE);
		GridData gd_cntDelDateComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		//gd_cntDelDateComposite.heightHint = 25;
		gd_cntDelDateComposite.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_cntDelDateComposite, 15);
		m_cntDelDateComposite.setLayoutData(gd_cntDelDateComposite);
		m_cntDelDateComposite.getDateButton().setDisplayFormat("dd-MMM-yyyy");
		
		m_cntDelDateComposite.setAttributeName(NOV4ComponentOrder.CONTRACT_DELIVERY_DATE);
	//	m_cntDelDateComposite.setBindingContext(OrderPane.DATA_MODEL_NAME);
	//	m_cntDelDateComposite.createDataBinding();

	}
	
	private void addListeners()
	{
		m_custCombo.addSelectionListener(new NOVOrderCustomerComboListener());
	}
	
	private void populateLOVs() {
        Vector custVec = (NOVSMDLOrderHelper.getCustomerList()).getOrderedVector();
        NOVSMDLComboUtils.setAutoComboVector(m_custCombo, custVec);
        
		m_customerNames = new String[custVec.size()];
		for (int inx = 0; inx < custVec.size(); inx++) {
			m_customerNames[inx] = custVec.elementAt(inx).toString();
		}
    
        Vector plantVec = (NOVSMDLOrderHelper.getPlantList()).getOrderedVector();
        NOVSMDLComboUtils.setAutoComboVector(m_plantCombo, plantVec);
        
        m_plantNames = new String[plantVec.size()];
		for (int inx = 0; inx < plantVec.size(); inx++) {
			m_plantNames[inx] = plantVec.elementAt(inx).toString();
		}
	}

	public Combo getPlantCombo() {
		return m_plantCombo;
	}
	
	public void setEditability(Boolean editOrd)
	{
			m_custCombo.setEnabled(editOrd);
			m_salesText.setEditable(editOrd);
			m_plantCombo.setEnabled(editOrd);
			m_cntAwdDateComposite.setEnabled(editOrd);
			m_purchOrdText.setEditable(editOrd);
			m_cntDelDateComposite.setEnabled(editOrd);
	}
	
	public void clearPanelValues()
	{
		m_custCombo.deselectAll();
		m_salesText.setText("");
		m_plantCombo.deselectAll();
		m_cntAwdDateComposite.setDateString("");
		m_purchOrdText.setText("");
		m_cntDelDateComposite.setDateString("");
	}
	
	public void populate(TCComponent order) {
		try {
			
			if(order!=null) {
				String custCmbStr = (String) order.getTCProperty(NOV4ComponentOrder.CUSTOMER).getPropertyValue();
	            if(custCmbStr!=null){
					int nIndex = m_custCombo.indexOf(custCmbStr);
					if(nIndex > -1) {
						m_custCombo.select(nIndex);
					}
	            }
	            
				Integer plantId = (Integer) order.getTCProperty(NOV4ComponentOrder.PLANT_ID_OTHER).getPropertyValue();
				String plantIdStr = (NOVSMDLOrderHelper.getPlantList()).getStringFromReference(plantId);
	            if(plantIdStr!=null){
					int nIndex = m_plantCombo.indexOf(plantIdStr);
					if(nIndex > -1) {
						m_plantCombo.select(nIndex);
					}
	            }
	            
				String purchOrdstr = (String) order.getTCProperty(NOV4ComponentOrder.PURCHASE_ORDER_NUMBER).getPropertyValue();
				if(purchOrdstr!=null) {
					m_purchOrdText.setText(purchOrdstr);
				}
						
				String salesOrdStr = (String) order.getTCProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER).getPropertyValue();
				if(salesOrdStr!=null) {
					m_salesText.setText(salesOrdStr);
				}	
				
				Date cntAwdDate = order.getTCProperty(NOV4ComponentOrder.CONTRACT_AWARD_DATE).getDateValue();
				if (cntAwdDate != null) {
					m_cntAwdDateComposite.setDate(cntAwdDate);
				}
				
				Date cntDelDate = order.getTCProperty(NOV4ComponentOrder.CONTRACT_DELIVERY_DATE).getDateValue();
				if(cntDelDate != null) {
					m_cntDelDateComposite.setDate(cntDelDate);
				}
			}
			
		} catch (TCException e) {
			e.printStackTrace();
		}
	}
	
	public CreateInObjectHelper populateCreateInObject(CreateInObjectHelper object)
	{
		String customername = null;
		if(m_custCombo.getItems().length > 0 && m_custCombo.getSelectionIndex()!=-1) {
			customername = m_custCombo.getItem(m_custCombo.getSelectionIndex());
			object.setStringProps(NOV4ComponentOrder.CUSTOMER,m_custCombo.getItem(m_custCombo.getSelectionIndex()),CreateInObjectHelper.SET_PROPERTY);
		} else if(!m_custCombo.getText().isEmpty()) {
			customername = m_custCombo.getText();
			object.setStringProps(NOV4ComponentOrder.CUSTOMER,m_custCombo.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		
		if(m_plantCombo.getItems().length > 0 && m_plantCombo.getSelectionIndex()!=-1) {
//          Integer plantIdVal = (NOVSMDLOrderHelper.getPlantList()).getReferenceFromString((String)m_plantCombo.getItem(m_plantCombo.getSelectionIndex()));
			Integer plantIdVal = (NOVSMDLOrderHelper.getPlantList()).getIDFromPlantAndCustomer((String) m_plantCombo.getItem(m_plantCombo.getSelectionIndex()),customername);
			object.setIntProps(NOV4ComponentOrder.PLANT_ID_OTHER,plantIdVal.intValue(),CreateInObjectHelper.SET_PROPERTY);
		} else if(!m_plantCombo.getText().isEmpty()) {
//			Integer plantIdVal = (NOVSMDLOrderHelper.getPlantList()).getReferenceFromString((String)m_plantCombo.getText());
			Integer plantIdVal = (NOVSMDLOrderHelper.getPlantList()).getIDFromPlantAndCustomer((String) m_plantCombo.getText(),customername);
			object.setIntProps(NOV4ComponentOrder.PLANT_ID_OTHER,plantIdVal.intValue(),CreateInObjectHelper.SET_PROPERTY);
		}
		
		if(m_purchOrdText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.PURCHASE_ORDER_NUMBER,m_purchOrdText.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_salesText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.SALES_ORDER_NUMBER,m_salesText.getText(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_cntAwdDateComposite.getDate() != null){
			object.setDateProps(NOV4ComponentOrder.CONTRACT_AWARD_DATE, m_cntAwdDateComposite.getDate(),CreateInObjectHelper.SET_PROPERTY);
		}
		if(m_cntDelDateComposite.getDate() != null){
			object.setDateProps(NOV4ComponentOrder.CONTRACT_DELIVERY_DATE, m_cntDelDateComposite.getDate(),CreateInObjectHelper.SET_PROPERTY);
		}
		return object;
	}
	
	public void markMandatoryFields()
	{
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry.getDefault().getFieldDecoration("NOV_DEC_REQUIRED");

		ControlDecoration decCustCombo = new ControlDecoration(m_custLabel, SWT.TOP | SWT.RIGHT);
		decCustCombo.setImage(reqFieldIndicator.getImage());
		decCustCombo.setDescriptionText(reqFieldIndicator.getDescription());
		
		ControlDecoration decPlantCombo = new ControlDecoration(m_plantLabel, SWT.TOP | SWT.RIGHT);
		decPlantCombo.setImage(reqFieldIndicator.getImage());
		decPlantCombo.setDescriptionText(reqFieldIndicator.getDescription());
		
		ControlDecoration decCntAwdDate = new ControlDecoration(m_cntAwdDateLabel, SWT.TOP | SWT.RIGHT);
		decCntAwdDate.setImage(reqFieldIndicator.getImage());
		decCntAwdDate.setDescriptionText(reqFieldIndicator.getDescription());
		
		ControlDecoration decCntDelDate = new ControlDecoration(m_cntDelDateLabel, SWT.TOP | SWT.RIGHT);
		decCntDelDate.setImage(reqFieldIndicator.getImage());
		decCntDelDate.setDescriptionText(reqFieldIndicator.getDescription());
	}	
	
	public String validateMandatoryFields() 
	{
		StringBuffer invalidInputs= new StringBuffer();
		
		if(m_custCombo.getSelectionIndex() == -1 && 
				m_custCombo.getText().isEmpty()) {
			invalidInputs.append(m_appReg.getString("MandatoryFldCustomer.LABEL"));
		}
		
		if (m_plantCombo.getSelectionIndex() == -1 && 
				m_plantCombo.getText().isEmpty()) {
			if(invalidInputs.length()!=0){
				invalidInputs.append(", ");
			}
			invalidInputs.append(m_appReg.getString("MandatoryFldPlantRig.LABEL"));
		}
		
		if(m_cntAwdDateComposite.getDate() == null) {
			if(invalidInputs.length()!=0){
				invalidInputs.append(", ");
			}
			invalidInputs.append(m_appReg.getString("MandatoryFldContAwdDate.LABEL"));
		}
		if (m_cntDelDateComposite.getDate() == null) {
			if(invalidInputs.length()!=0){
				invalidInputs.append(", ");
			}
			invalidInputs.append(m_appReg.getString("MandatoryFldContDelDate.LABEL"));
		}

		return invalidInputs.toString();
	}
	
	public String validateFieldValues() 
	{
		StringBuffer invalidFieldValues= new StringBuffer();
		
		if(m_custCombo.indexOf(m_custCombo.getText()) == -1) {
			if(invalidFieldValues.length()!=0) {
				invalidFieldValues.append("\n");
			}				
			invalidFieldValues.append(m_appReg.getString("InvalidCustValue.MSG"));
		}
		
		if(m_plantCombo.indexOf(m_plantCombo.getText()) == -1) {
			if(invalidFieldValues.length()!=0) {
				invalidFieldValues.append("\n");
			}
			invalidFieldValues.append(m_appReg.getString("InvalidPlantValue.MSG"));
		}

		if(m_cntAwdDateComposite.getDate()!=null && m_cntDelDateComposite.getDate()!=null) {
	    	if(m_cntDelDateComposite.getDate().before(m_cntAwdDateComposite.getDate())) {
				if(invalidFieldValues.length()!=0) {
					invalidFieldValues.append("\n");
				}	    		
	    		invalidFieldValues.append(m_appReg.getString("CntDelDateBeforeCntAwdDate.MSG"));
	    	}
	    }
		return invalidFieldValues.toString();
	}

	public void createDataBinding()
	{
		m_cntAwdDateComposite.createDataBinding();
		m_cntDelDateComposite.createDataBinding();
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		
	/*	dataBindingHelper.bindData(m_salesText,NOV4ComponentOrder.SALES_ORDER_NUMBER,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_purchOrdText,NOV4ComponentOrder.PURCHASE_ORDER_NUMBER,OrderPane.DATA_MODEL_NAME);
		
		dataBindingHelper.bindData(m_custCombo,NOV4ComponentOrder.CUSTOMER,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_plantCombo, NOV4ComponentOrder.PLANT_ID_OTHER,OrderPane.DATA_MODEL_NAME);*/
		
		dataBindingHelper.bindData(m_salesText,NOV4ComponentOrder.SALES_ORDER_NUMBER,m_dataModelName);
		dataBindingHelper.bindData(m_purchOrdText,NOV4ComponentOrder.PURCHASE_ORDER_NUMBER,m_dataModelName);
		
		dataBindingHelper.bindData(m_custCombo,NOV4ComponentOrder.CUSTOMER,m_dataModelName);
		dataBindingHelper.bindData(m_plantCombo, NOV4ComponentOrder.PLANT_ID_OTHER,m_dataModelName);
		
	}

	public void setBindingContext(String orderDataModelName)
	{
		m_dataModelName = orderDataModelName;
		m_cntAwdDateComposite.setBindingContext(orderDataModelName);
		m_cntDelDateComposite.setBindingContext(orderDataModelName);
	}
	
	private void populateActualPlantData()
	{
		String plantName = null;
		if( !( m_plantCombo.getText().trim().equalsIgnoreCase( "" ) ) )
		{
			plantName =  m_plantCombo.getText();
		}
		NOVSMDLComboUtils.setAutoComboArray( m_plantCombo, m_plantNames );
		
		if( plantName != null && !( plantName.trim().equalsIgnoreCase( "" ) ) )
		{
			int custIdx = m_plantCombo.indexOf( plantName );
			m_plantCombo.select( custIdx );
		}
	}
	
	private void populatePlantNameData() {
		int custIdx = m_custCombo.indexOf(m_custCombo.getText());

		if (custIdx == -1) 
		{
			populateActualPlantData();
		} 
		else 
		{
			m_plantCombo.removeAll();
			String[] cplants = (NOVSMDLOrderHelper.getPlantList())
					.getList((NOVSMDLOrderHelper.getCustomerList())
							.getReferenceFromString(
									m_custCombo.getItem(custIdx))
							.intValue());
			NOVSMDLComboUtils.setAutoComboArray(m_plantCombo, cplants);
		}
	}

	protected boolean validateName(String name) {

		boolean valid = false;

		if (NOVArrayHelper.searchInArray(name, m_custCombo.getItems())) {
			valid = true;
		}

		return valid;
	}
	
	protected void processUpdate(boolean isCustNamevalid) {
		if (isCustNamevalid) {
			populatePlantNameData();
		} else {
			populateActualPlantData();
		}
	}
}
