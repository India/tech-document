package com.nov.rac.smdl.panes.order;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.text.JTextComponent;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.interfaces.IAddressBook;
import com.nov.rac.interfaces.IPropertyChangeNotifier;
import com.nov.rac.smdl.common.NOVAddressBookListener;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.utilities.addressbook.dialog.SimpleAddressBookDialog;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLEmailIDComponent extends Composite implements PropertyChangeListener {

	private Text 				m_textField; 
	private String 				m_textVal;
	private Button 				m_Button;
	private boolean 			m_isUser;
	protected Registry 			m_appReg = null;
	protected FieldDecoration 	m_reqFieldIndicator = null;
	
	private String 				m_AttributeName = null;
	private String				m_dataModelName = null;
	
	public NOVSMDLEmailIDComponent (Composite parent, int style) {
		super(parent, style);
	    m_appReg = Registry.getRegistry(this);
	    m_reqFieldIndicator = FieldDecorationRegistry.getDefault(). getFieldDecoration(FieldDecorationRegistry.DEC_REQUIRED);
	    
//		createUI(this);
	}
	
	public void createUI()
	{
		NOVSMDLEmailIDComponent emailIDComponent = this;
		
		emailIDComponent.setLayout(new GridLayout(2, false));
		emailIDComponent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		m_textField = new Text(emailIDComponent, SWT.BORDER);
		m_textField.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		m_textField.setEditable(false);

		m_Button = new Button(emailIDComponent, SWT.NONE);
		m_Button.setImage(m_appReg.getImage("AddrButton.ICON"));
		int width = SWTUIHelper.convertHorizontalDLUsToPixels(m_Button, 20);
		int height = SWTUIHelper.convertVerticalDLUsToPixels(m_Button, 12);
		m_Button.setLayoutData(new GridData(width,height));
		
		addListener();
		
		//createDataBinding();
	}
	
    private void addListener()
    {
        m_Button.setData("OrdSalesEMail");
        // m_Button.addListener(SWT.Selection, new
        // NOVAddressBookListener(m_textField));
        m_Button.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                openAddressBookDialog("Addressbook.CLASS");
            }
        });
        
        m_Button.setVisible(true);
    }
	
	public void setTextVal(String textVal){
		m_textField.setText(textVal);
		m_textVal = textVal;
	}
	
	public String getTextVal(){
		return m_textVal;
	}
	
	public void setIsUser(boolean isUser) {
		m_isUser = isUser;
	}
	
	public boolean getIsUser() {
		return m_isUser;
	}
	
	public void setAttributeName(String attrName){
		m_AttributeName = attrName;
	}

	public void setBindingContext(String dataModelName){
		m_dataModelName = dataModelName;
	}
	
    private void openAddressBookDialog(String context)
    {
        try
        {
            String viewerClass = m_appReg.getString(context);
            Object[] params = new Object[1];
            params[0] = AIFDesktop.getActiveDesktop().getShell();
            
            IAddressBook addressBookDialog = (IAddressBook) Instancer.newInstanceEx(viewerClass, params);
            
            addressBookDialog.setButtonsForDialog(SimpleAddressBookDialog.OK_BUTTON_ID
                     | SimpleAddressBookDialog.CANCEL_BUTTON_ID);
            
            if (addressBookDialog instanceof IPropertyChangeNotifier)
            {
                ((IPropertyChangeNotifier) addressBookDialog).addPropertyChangeListener(NOVSMDLEmailIDComponent.this);
            }
            
            addressBookDialog.open();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
	
	public void createDataBinding(){
		
		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		dataBindingHelper.bindData(m_textField,m_AttributeName,m_dataModelName);
	}

    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
        if(event.getPropertyName().equalsIgnoreCase(SimpleAddressBookDialog.ADDRESS_BOOK_MAIL_IDS_SELECTED))
        {
            String[] selectedIds = (String[]) event.getNewValue();
            if(selectedIds != null && selectedIds.length > 0)
            {
                m_textField.setText(selectedIds[0]);
            }
        }
        
    }

}
