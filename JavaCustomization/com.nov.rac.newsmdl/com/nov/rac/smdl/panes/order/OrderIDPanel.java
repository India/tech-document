package com.nov.rac.smdl.panes.order;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class OrderIDPanel extends Composite {
	private Text m_ordIDText;
	private Label m_ordIDLabel;
	private Label m_ordTypLabel;
	private Combo m_ordTypCombo;
	
 	protected Registry m_appReg = null;
	private String m_dataModelName;

	public OrderIDPanel (Composite parent, int style){
		super( parent, style );
	    m_appReg = Registry.getRegistry(this);
		//createUI(this);
	}
		
	public void createUI()
	{
		OrderIDPanel ordIdPanel = this;
		ordIdPanel.setLayout(new GridLayout(2, true));
		ordIdPanel.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		getComp1(ordIdPanel);
		getComp2(ordIdPanel);
		
		//createDataBinding();
	}	
	
	private void getComp1(OrderIDPanel ordIdPanel){
		Composite comp1 = new Composite(ordIdPanel,SWT.NONE);
		comp1.setLayout(new GridLayout(2, false));
		comp1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_ordIDLabel = new Label(comp1, SWT.NONE);
		//m_ordIDLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		GridData gd_m_ordIDLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_ordIDLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_ordIDLabel, 85);
		m_ordIDLabel.setLayoutData(gd_m_ordIDLabel);		
		m_ordIDLabel.setText(m_appReg.getString("OrdID.LABEL"));
		
		m_ordIDText = new Text(comp1, SWT.BORDER);
		m_ordIDText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
	}
	
	private void getComp2(OrderIDPanel ordIdPanel){
		Composite comp2 = new Composite(ordIdPanel,SWT.NONE);
		comp2.setLayout(new GridLayout(2, false));
		comp2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_ordTypLabel = new Label(comp2, SWT.NONE);
		GridData gd_m_ordTypLabel = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_m_ordTypLabel.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_ordTypLabel, 95);
		m_ordTypLabel.setLayoutData(gd_m_ordTypLabel);		
		m_ordTypLabel.setText(m_appReg.getString("OrdType.LABEL"));
		
		m_ordTypCombo = new Combo(comp2, SWT.DROP_DOWN | SWT.BORDER);
		m_ordTypCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		populateLOVs();
	}
	
	private void populateLOVs() {
		LOVObject[] arrLOVObjs = LOVUtils.getLOVObjectsForLOV("Order_Type");
		NOVSMDLComboUtils.setAutoComboLOV(m_ordTypCombo, arrLOVObjs);
	}

	public void setEditability(Boolean editOrd)
	{
		m_ordIDText.setEditable(false);
		m_ordTypCombo.setEnabled(editOrd);
	}
	
	public void clearPanelValues()
	{
		m_ordIDText.setText("");
		m_ordTypCombo.deselectAll();
	}
	
	public void populate(TCComponent order) {
		try  {
			if(order!=null){
				String ordId =(String) order.getTCProperty(NOV4ComponentOrder.ORDER_NUMBER).getPropertyValue();
				m_ordIDText.setText(ordId);
			
				String ordTypCmbStr = (String) order.getTCProperty(NOV4ComponentOrder.ORDER_TYPE).getPropertyValue();
				if(ordTypCmbStr!=null) {
					int nIndex = m_ordTypCombo.indexOf(ordTypCmbStr);
					if(nIndex > -1) {
						m_ordTypCombo.select(nIndex);
					}
				}
			}
		} catch (TCException e)	{
			e.printStackTrace();
		}
	}
	
	public CreateInObjectHelper populateCreateInObject(CreateInObjectHelper object)
	{
		if(m_ordIDText.getText() != null) {
			object.setStringProps(NOV4ComponentOrder.ORDER_NUMBER,m_ordIDText.getText(),CreateInObjectHelper.SET_CREATE_IN);
		}
		
		if(m_ordTypCombo.getItems().length > 0 && m_ordTypCombo.getSelectionIndex() != -1) {
			object.setStringProps(NOV4ComponentOrder.ORDER_TYPE,m_ordTypCombo.getItem(m_ordTypCombo.getSelectionIndex()),CreateInObjectHelper.SET_PROPERTY);
		} else if(!m_ordTypCombo.getText().isEmpty()) {
			object.setStringProps(NOV4ComponentOrder.ORDER_TYPE,m_ordTypCombo.getText(),CreateInObjectHelper.SET_PROPERTY);
		} 
		
		return object;
	}
	
    public void setOrderItemId(String ordItemID)
    {
       m_ordIDText.setText(ordItemID);
    }
    
	public String validateFieldValues() 
	{
		StringBuffer invalidFieldValues= new StringBuffer();
		
		if(m_ordTypCombo.indexOf(m_ordTypCombo.getText()) == -1) {
			invalidFieldValues.append(m_appReg.getString("InvalidOrdTypeValue.MSG"));
		}
		return invalidFieldValues.toString();
	}
	
	public void createDataBinding(){

		SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
		
		//dataBindingHelper.bindData(m_ordTypCombo,NOV4ComponentOrder.ORDER_TYPE,OrderPane.DATA_MODEL_NAME);
		dataBindingHelper.bindData(m_ordTypCombo,NOV4ComponentOrder.ORDER_TYPE,m_dataModelName);
		
	}

	public void setBindingContext(String orderDataModelName)
	{
		m_dataModelName = orderDataModelName;
	}

	public boolean validateSave()
	{
		boolean value=true;
		if(m_ordIDText.getText().length()==0 )
		{
			value=false;
			
		}
		return value;
	}
}
