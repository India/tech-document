package com.nov.rac.smdl.panes.order.listener;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;

import com.nov.rac.smdl.panes.order.OrderCustPanel;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.smdl.utilities.NOVSMDLOrderHelper;

public class NOVOrderCustomerComboListener implements SelectionListener {

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		Object target=event.getSource();
		
		if (target instanceof Combo)
		{
			Combo custCombo=((Combo)target);
			int custIdx = custCombo.indexOf(custCombo.getText());

			Combo plantCombo = ((OrderCustPanel)custCombo.getParent().getParent()).getPlantCombo();
			if (plantCombo != null) {
    			plantCombo.removeAll();
    			String[] cplants = (NOVSMDLOrderHelper.getPlantList()).getList((NOVSMDLOrderHelper.getCustomerList()).getReferenceFromString(custCombo.getItem(custIdx)).intValue());
    			NOVSMDLComboUtils.setAutoComboArray(plantCombo, cplants);
    		}
			
    		if (custCombo.getItem(custIdx) != null && !((String)custCombo.getItem(custIdx)).equals("")) {
    			plantCombo.setEnabled(true);
    		} else {
    			plantCombo.setEnabled(false);
    		}			
		}
	}
}
