package com.nov.rac.smdl.panes.order;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDLHome;
import com.nov.rac.smdl.common.SMDLDataModelConstants;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.utilities.databinding.DataBindingModel;
import com.nov.rac.utilities.databinding.DataBindingRegistry;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class CreateOrderDialog extends AbstractSWTDialog {

	private Registry m_appReg;
	private OrderPanel m_orderPanel;
	private CreateInObjectHelper m_ordObjectHelper;
	private CreateInObjectHelper[] m_dpObjectHelper;
	private Map<String, CreateInObjectHelper> m_dpObjHelperMap = new HashMap();
	private TCSession m_session;
	private PropertyChangeListener m_listeners = null;
	private DataBindingModel m_theDataModel;
	
	public CreateOrderDialog(Shell arg0) {
		super(arg0);
	    m_appReg = Registry.getRegistry(this);
        m_session = (TCSession)AIFUtility.getDefaultSession();
	}

	protected Control createDialogArea(Composite parent)
	{
		setTitle();
		
		//TCDECREL-3009 
		/*Composite composite = new Composite(parent, SWT.NONE);

		GridLayout gd = new GridLayout(1, false);
		composite.setLayout(gd);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		//addHeader Image
		GImage headerImage = m_appReg.getImage("newOrderCreation_header.ICON");
		Label header= new Label(composite, SWT.IMAGE_PNG);
		header.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		header.setImage(headerImage);*/
		//TCDECREL-3009  ends

		ScrolledComposite comp = new ScrolledComposite( parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		//create dataModel & register it 
		m_theDataModel = new DataBindingModel();
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.registerDataModel(SMDLDataModelConstants.CREATE_ORDER_DATA_MODEL_NAME,m_theDataModel);
		
		m_orderPanel = new OrderPanel( comp, SWT.NONE );
		m_orderPanel.createUI();
		m_orderPanel.clearPanelValues();
		m_orderPanel.getOrderButtonPanel().setOrderCreation(this);
		m_orderPanel.getOrderDPTablePanel().getOrdDPTable().setExtraData(m_appReg.getString("RemDPFromOrdDlg.LABEL"));
		m_orderPanel.markMandatoryFields();

		populateOrderID();

		comp.setContent(m_orderPanel);
		comp.setExpandHorizontal(true);
		comp.setExpandVertical(true);
		comp.setMinSize(m_orderPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		return parent;
	}
	
	@Override
	public boolean close() 
	{
		
		boolean returnVal = super.close();
		//unregister data model
		DataBindingRegistry dataBindingRegistry =  DataBindingRegistry.getRegistry();
		dataBindingRegistry.unregisterDataModel(SMDLDataModelConstants.CREATE_ORDER_DATA_MODEL_NAME);
		
		return returnVal;
	}


	private void populateOrderID() {
        TCComponentItemType itemType;
        try {
    		itemType = (TCComponentItemType) m_session.getTypeComponent(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME);
			String orderItemId = itemType.getNewID();
			m_orderPanel.getOrderIDPanel().setOrderItemId(orderItemId);
        } catch (TCException tce) {
            tce.printStackTrace();
        }
	}


	@Override
	protected void okPressed() {

		String invalidMandatoryInputs=validateMandatoryInputs();
		if(invalidMandatoryInputs.length()==0) {

			String invalidFieldValues=validateFieldValues();
			if(invalidFieldValues.length()==0) {
	
				TCTable ordDPTable = null;
				ordDPTable = m_orderPanel.getOrderDPTablePanel().getOrdDPTable();
	
				String itemID[] = new String[ordDPTable.getRowCount()];
				for(int i=0;i<ordDPTable.getRowCount();i++) {
					itemID[i] = (String) ordDPTable.getValueAt(i,0);
				}
				List dpIDList = Arrays.asList(itemID);
				m_dpObjHelperMap.keySet().retainAll(dpIDList);
	
				if(!m_dpObjHelperMap.isEmpty()) {
	
					ProgressMonitorDialog createOrderDialog=new ProgressMonitorDialog(this.getShell());
					CreateOrUpdateOperation createOrderOp = new CreateOrUpdateOperation(createOrderOperationInput());
	
					try {
						createOrderDialog.run(true, false,createOrderOp);
						TCComponent targetObject = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(m_ordObjectHelper);
						if(null != targetObject ) {
							TCComponentFolder userHomeFolder = null;
							userHomeFolder = m_session.getUser().getHomeFolder();
							if(userHomeFolder != null) {
								TCComponentFolder smdlHomeFolder = null;
								smdlHomeFolder = (TCComponentFolder) userHomeFolder.getRelatedComponent(NOV4ComponentSMDLHome.SMDL_HOME_RELATION);
								if(smdlHomeFolder != null) {
									smdlHomeFolder.add("contents",targetObject, 0);
                                    firePropertyChange(this, "AddSMDLObjectOperation", "" , targetObject);									
								}
							}
						}
			        } catch ( Exception e1 ) {
			            e1.printStackTrace();
			    		showErrorMessage(m_appReg.getString("OrderCreationFailed.MSG") + " " + e1.getMessage());
			        }
	
					CreateOrUpdateOperation createOrderDPOp = new CreateOrUpdateOperation(createOrderDPOperationInput());
			        try {
						createOrderDialog.run(true, false,createOrderDPOp);
			        } catch ( Exception e2 ) {
			            e2.printStackTrace();
			    		showErrorMessage(m_appReg.getString("DPCreationFailed.MSG") + " " + e2.getMessage());
			        }
			        super.okPressed();
	
				} else {
		    		showErrorMessage(m_appReg.getString("CreateAtleastOneDP.MSG"));
				}
			} else {
	    		showErrorMessage(invalidFieldValues);
			}
		} else {
    		showErrorMessage(m_appReg.getString("FillMandatoryFields.MSG")+"\n"+invalidMandatoryInputs);
		}
	}

    protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
    {
          PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
          if(m_listeners != null) {
        	  m_listeners.propertyChange( changeEvent );
          }
    }     
    
    public void addPropertyChangeListeners(   PropertyChangeListener listeners) {
          m_listeners = listeners;
    }
	
	private void showErrorMessage(String errStr) {
		MessageBox.post(this.getShell(), errStr, "Error", MessageBox.ERROR);
	}
	
	private String validateMandatoryInputs()
	{
		String invalidInputs= "";
		invalidInputs= m_orderPanel.validateMandatoryFields();
		return invalidInputs;
	}
	
	private String validateFieldValues() {
		String invalidFieldValues= "";
		invalidFieldValues= m_orderPanel.validateFieldValues();
		return invalidFieldValues;
	}
	
	private CreateInObjectHelper createOrderOperationInput()	{

		m_ordObjectHelper= new CreateInObjectHelper(NOV4ComponentOrder.ORDER_OBJECT_TYPE_NAME, CreateInObjectHelper.OPERATION_CREATE);
		m_orderPanel.populateCreateInObject(m_ordObjectHelper);

		return m_ordObjectHelper;
	}

	private CreateInObjectHelper[] createOrderDPOperationInput()	{

		int dpObjSize = m_dpObjHelperMap.size();
		m_dpObjectHelper = new CreateInObjectHelper[dpObjSize];
		int i = 0;
		Iterator mapItr = m_dpObjHelperMap.keySet().iterator();
		while (mapItr.hasNext()) {
			
			TCComponent[] createObjects = CreateObjectsSOAHelper.getCreatedOrUpdatedObjects(m_ordObjectHelper);
			
			TCComponent theOrderObj = null;
			
			if(createObjects != null && createObjects.length > 0){
				theOrderObj = createObjects[0];
			}
			
			m_dpObjectHelper[i] = m_dpObjHelperMap.get(mapItr.next());
			m_dpObjectHelper[i].setTagProps(NOV4ComponentDP.DP_ORDER_RELATION, theOrderObj, CreateInObjectHelper.SET_PROPERTY);
			m_dpObjectHelper[i].setRelationName(NOV4ComponentOrder.ORDER_DP_RELATION);
			m_dpObjectHelper[i].setParentObject(theOrderObj);	
			i++;
		}

		return m_dpObjectHelper;
	}

	private void setTitle() {
		String strDialogTitle = m_appReg.getString("createOrder.TITLE", null);
		this.getShell().setText(strDialogTitle);
	}

	public void setDPObjectHelper(CreateInObjectHelper dpObjectHelper)
	{
		String itemId = dpObjectHelper.getStringProperty(NOV4ComponentDP.DP_NUMBER);
		m_dpObjHelperMap.put(itemId, dpObjectHelper);
		m_orderPanel.getOrderDPTablePanel().populateDP(dpObjectHelper);
	}
}
