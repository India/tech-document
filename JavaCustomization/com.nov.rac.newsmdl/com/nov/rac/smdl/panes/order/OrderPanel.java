package com.nov.rac.smdl.panes.order;

import java.beans.PropertyChangeListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.smdl.utilities.NOVSMDLOrderHelper;
import com.nov.rac.smdl.views.SMDLComponentView;
import com.teamcenter.rac.kernel.TCComponent;

public class OrderPanel extends Composite {

	private OrderIDPanel m_ordIdPanelUI;
	private OrderCustPanel m_ordCustPanelUI;
	private OrderSalesPanel m_ordSalesPanelUI;
	private OrderExtPanel m_ordExtPanelUI;
	private OrderDPTablePanel m_ordDPTablePanelUI;
	private OrderDPButtonPanel m_ordDPButtonPanelUI;

	private boolean m_editOrder = false;

	public static int CREATE_ORDER = 1;

	public OrderPanel(Composite parent, int style) {
		super(parent, style);
		m_editOrder = NOVSMDLOrderHelper.isOrderEditable();
		//createUI(this);
	}

	public void createUI() 
	{
		Composite parent= this;
		setLayout(new GridLayout(1, true));

		createOrdIdPanel(parent);
		createOrdCustPanel(parent);
		createOrdSalesPanel(parent);
		createOrdExtPanel(parent);
		createOrdDPTablePanel(parent);
		createOrdDPButtonPanel(parent);

		markMandatoryFields();
		setEditability(m_editOrder);
	}

	private void createOrdIdPanel(Composite parent) {
		m_ordIdPanelUI = new OrderIDPanel(parent, SWT.BORDER);
		m_ordIdPanelUI.createUI();
	}

	private void createOrdCustPanel(Composite parent) {
		m_ordCustPanelUI = new OrderCustPanel(parent, SWT.BORDER);
		m_ordCustPanelUI.createUI();
	}

	private void createOrdSalesPanel(Composite parent) {
		m_ordSalesPanelUI = new OrderSalesPanel(parent, SWT.BORDER);
		m_ordSalesPanelUI.createUI();
	}

	private void createOrdExtPanel(Composite parent) {
		m_ordExtPanelUI = new OrderExtPanel(parent, SWT.BORDER);
		m_ordExtPanelUI.createUI();
	}

	private void createOrdDPTablePanel(Composite parent) {
		m_ordDPTablePanelUI = new OrderDPTablePanel(parent, SWT.BORDER);
		m_ordDPTablePanelUI.createUI();
	}

	private void createOrdDPButtonPanel(Composite parent) {
		m_ordDPButtonPanelUI = new OrderDPButtonPanel(parent, SWT.NONE);
		m_ordDPButtonPanelUI.createUI();
	}

	public void setEditability(boolean m_editOrder) {
		m_ordIdPanelUI.setEditability(m_editOrder);
		m_ordCustPanelUI.setEditability(m_editOrder);
		m_ordSalesPanelUI.setEditability(m_editOrder);
		m_ordExtPanelUI.setEditability(m_editOrder);
		m_ordDPTablePanelUI.setEditability(m_editOrder);
		m_ordDPButtonPanelUI.setEditability(m_editOrder);
	}

	public void clearPanelValues() {
		m_ordIdPanelUI.clearPanelValues();
		m_ordCustPanelUI.clearPanelValues();
		m_ordSalesPanelUI.clearPanelValues();
		m_ordExtPanelUI.clearPanelValues();
		m_ordDPTablePanelUI.clearValues();
	}

	public void populate(TCComponent m_orderSOAPlainObject) {
		if (m_orderSOAPlainObject instanceof NOV4ComponentOrder) {
			m_ordIdPanelUI.populate(m_orderSOAPlainObject);
			m_ordCustPanelUI.populate(m_orderSOAPlainObject);
			m_ordSalesPanelUI.populate(m_orderSOAPlainObject);
			m_ordExtPanelUI.populate(m_orderSOAPlainObject);
			m_ordDPTablePanelUI.populate(m_orderSOAPlainObject);
			m_ordDPButtonPanelUI.populate(m_orderSOAPlainObject);
		}
	}

	public OrderIDPanel getOrderIDPanel() {
		return m_ordIdPanelUI;
	}

	public OrderExtPanel getOrderExtPanel() {
		return m_ordExtPanelUI;
	}

	public OrderCustPanel getOrderCustPanel() {
		return m_ordCustPanelUI;
	}

	public OrderDPButtonPanel getOrderButtonPanel() {
		return m_ordDPButtonPanelUI;
	}

	public OrderDPTablePanel getOrderDPTablePanel() {
		return m_ordDPTablePanelUI;
	}

	public void populateCreateInObject(CreateInObjectHelper transferObject) {
		m_ordIdPanelUI.populateCreateInObject(transferObject);
		m_ordCustPanelUI.populateCreateInObject(transferObject);
		m_ordSalesPanelUI.populateCreateInObject(transferObject);
		m_ordExtPanelUI.populateCreateInObject(transferObject);
	}

	public void markMandatoryFields() {
		m_ordCustPanelUI.markMandatoryFields();
		m_ordSalesPanelUI.markMandatoryFields();
	}

	public String validateMandatoryFields() {
		StringBuffer invalidInputs = new StringBuffer();

		invalidInputs.append(m_ordCustPanelUI.validateMandatoryFields());
		if (invalidInputs.length() != 0) {
			invalidInputs.append(", ");
		}
		invalidInputs.append(m_ordSalesPanelUI.validateMandatoryFields());

		return invalidInputs.toString();
	}

	public String validateFieldValues() {
		StringBuffer invalidFieldValues = new StringBuffer();

		invalidFieldValues.append(m_ordIdPanelUI.validateFieldValues());
		if (invalidFieldValues.length() != 0) {
			invalidFieldValues.append("\n");
		}
		invalidFieldValues.append(m_ordCustPanelUI.validateFieldValues());
		if (invalidFieldValues.length() != 0) {
			invalidFieldValues.append("\n");
		}
		invalidFieldValues.append(m_ordExtPanelUI.validateFieldValues());

		return invalidFieldValues.toString();
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		if (listener instanceof SMDLComponentView) {
			m_ordDPTablePanelUI.addPropertyChangeListener(listener);
		} else {
			m_ordDPButtonPanelUI.addPropertyChangeListener(listener);
		}
	}

	public void updateDPTable(TCComponent theCreatedObject) {
		m_ordDPTablePanelUI.updateDPTable(theCreatedObject);
	}

	public void setBindingContext(String orderDataModelName) 
	{
		m_ordIdPanelUI.setBindingContext(orderDataModelName);
		m_ordCustPanelUI.setBindingContext(orderDataModelName);
		m_ordSalesPanelUI.setBindingContext(orderDataModelName);
		m_ordExtPanelUI.setBindingContext(orderDataModelName);
		
	}

	public void createDataBinding()
	{
		m_ordIdPanelUI.createDataBinding();
		m_ordCustPanelUI.createDataBinding();
		m_ordSalesPanelUI.createDataBinding();
		m_ordExtPanelUI.createDataBinding();
		
	}

	public boolean validateSave()
	{
		if(m_ordIdPanelUI.validateSave())
		{
			return true;
		}
		return false;
	}
}