package com.nov.rac.smdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

//import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.SMDLSearchInputDataProvider;
import com.nov.rac.smdl.panes.search.SearchInputDataComposite;
import com.nov.rac.smdl.panes.search.SearchResultComposite;
import com.nov.rac.smdl.panes.search.listener.SearchTableMouseListener;

public class SMDLSearchPane extends Composite
{
	private SearchInputDataComposite m_searchInputComposite;
	private SearchResultComposite m_searchResultComposite;

	private NovSearchDataModel m_novSearchDataModel;

	//TODO : NITIN Check this.
	//private INOVResultSet m_novResultSet;

	public SMDLSearchPane( Composite parent, int style )
	{
		super( parent, style );

		createUI( this, style );
	}

	private void createUI( Composite parent, int style )
	{
		setLayout( new FillLayout() );

		/// Creates the composite for the inputs of the search.
		createSearchDataComposites( parent, style );
	}
	
	private void createSearchDataComposites( Composite parent, int style )
	{
		final SashForm resizableSash = new SashForm( parent, SWT.VERTICAL );
		Color sashColor = parent.getDisplay().getSystemColor( SWT.COLOR_GRAY );
		resizableSash.setBackground( sashColor );
		
		///Creates the search Input Composite.
		m_searchInputComposite = new SearchInputDataComposite( resizableSash, SWT.NONE );
		
		///Creates the search Result Composite.
		m_searchResultComposite = new SearchResultComposite( resizableSash, SWT.NONE ); 
		
		//Mohan - Added a generic method to register the mouse listener to Search table
		m_searchResultComposite.addSearchTableMouselistener(new SearchTableMouseListener());
		
		Rectangle rect = parent.getShell().getBounds();
		resizableSash.setWeights( new int[] { rect.height / 2, rect.height } );
		
		m_novSearchDataModel = new NovSearchDataModel();

		/// Set the data model to the Views for Presenting themselves. 
		m_searchInputComposite.setNovSearchDataModel( m_novSearchDataModel );
		m_searchResultComposite.setNovSearchDataModel( m_novSearchDataModel );

		/// This need to be called after setting the data model for the views.
		m_searchInputComposite.setDataForExecution();
	}
	
//	public void setSearchInputDataProvider( SMDLSearchInputDataProvider searchInputData )
//	{
//		m_searchInputComposite.populateInputData( searchInputData );		
//	}
	
	 public void dispose()
	 {
		 m_searchResultComposite.dispose();
		 m_searchInputComposite.dispose();
	 }
}