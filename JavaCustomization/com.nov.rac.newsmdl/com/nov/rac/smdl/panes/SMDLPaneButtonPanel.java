package com.nov.rac.smdl.panes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.common.NOVSMDLSaveListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class SMDLPaneButtonPanel extends Composite 
{
	private Button m_saveButton;
	private Registry m_registry;
	public SMDLPaneButtonPanel(Composite parent, int style) 
	{
		super(parent, style);
		m_registry= Registry.getRegistry(this);
		createUI(this);		
	}
	private void createUI(SMDLPaneButtonPanel smdlButtonPanel) 
	{		
		smdlButtonPanel.setLayout(new GridLayout(1, true));
		
		GridData gd_smdlButtonPanel = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		smdlButtonPanel.setLayoutData(gd_smdlButtonPanel);
		
		createButtonPanel(smdlButtonPanel);		
	}	
	private void createButtonPanel(Composite smdlButtonPanel) 
	{
		Composite buttonPanel = new Composite(smdlButtonPanel, SWT.NONE);
				
        GridLayout gd_buttonLayout = new GridLayout(1, true);
        buttonPanel.setLayout(gd_buttonLayout);
        buttonPanel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
          	
		m_saveButton = new Button(buttonPanel, SWT.NONE);
		
		setLabels();
		addListeners();
	}

	private void addListeners()
	{
		m_saveButton.addSelectionListener(new NOVSMDLSaveListener());
	}

	private void setLabels()
	{
		m_saveButton.setText(m_registry.getString("SMDL_Save_Button.Label"));
	}
	public void populateSMDL(TCComponent smdlRev)
	{
		m_saveButton.setData(smdlRev);
	}
}
