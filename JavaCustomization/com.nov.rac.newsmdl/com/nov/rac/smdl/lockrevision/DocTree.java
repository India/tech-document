package com.nov.rac.smdl.lockrevision;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;

public class DocTree extends TCTree {


	@Override
	public Object[] getChildren(Object component, Object arg1) {
		// TODO Auto-generated method stub
	
		Object[] docChildren = super.getChildren(component, arg1);
		Vector<TCComponent> docRevision = new Vector<TCComponent>();
		Vector<TCComponent> docReleasedRevision = new Vector<TCComponent>();
		String[] propertyList = {"release_status_list"};
		String[][] propertyValue = null;
		
		for (int i = 0; i < docChildren.length; i++) {
			
			TCComponent docChild = null;
			
			docChild = getComponent(docChildren[i]);
			
			if(docChild instanceof TCComponentItemRevision)
			{
				docRevision.add((TCComponentItemRevision) docChild);
				
			}
		}
		
		try {
			TCComponent[] documentRevisionsArr = docRevision.toArray(new TCComponent[docRevision.size()]);
			List<TCComponent> documentRevisionsArrList = Arrays.asList(documentRevisionsArr);//TC10.1 Upgrade
			propertyValue = TCComponentType.getPropertiesSet(documentRevisionsArrList, propertyList);//TC10.1 Upgrade
			
			for (int i = 0; i < documentRevisionsArr.length; i++) {
				
				if(propertyValue[i][0]!= "")
				{
					docReleasedRevision.add(documentRevisionsArr[i]); 
				}
			}
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return docReleasedRevision.toArray();
	}
	
	public TCComponent getComponent(Object obj)
	{
		if(obj instanceof TCComponent)
		{
			return (TCComponent) obj;
		}
		else if(obj instanceof AIFComponentContext)
		{
			return (TCComponent) ((AIFComponentContext) obj).getComponent();
			
		}
		
		return null;
	}
}
