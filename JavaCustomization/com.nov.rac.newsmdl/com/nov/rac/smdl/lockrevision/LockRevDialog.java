package com.nov.rac.smdl.lockrevision;

import javax.swing.JScrollPane;
import javax.swing.tree.TreeSelectionModel;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;

import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.common.TCTreeNode;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;



public class LockRevDialog extends AbstractSWTDialog {

	private DocTree m_tcTree= null;
	private TCComponentItemRevision m_selectedComponent = null;
	private Registry m_registry=null;
	
	public LockRevDialog(Shell arg0) {
		super(arg0);
		//setBlockOnOpen(false);
		
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL | SWT.CENTER);
		
		m_registry = Registry.getRegistry("com.nov.rac.smdl.lockrevision.lockrevision");

	}

	
	@Override
	protected Point getInitialSize() {
		// TODO Auto-generated method stub
		Monitor monitor = this.getShell().getMonitor();

		Rectangle size = monitor.getBounds();

		Rectangle rect = this.getShell().getBounds();

		int rectwidth = rect.width;

		int width = ((size.width ) / 2) - (rectwidth / 5);

		int height = ((size.height ) / 2) - (rectwidth / 12);

		return new Point(width, height);


	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// TODO Auto-generated method stub
		
		setTitle();
		
		Composite container = (Composite) super.createDialogArea(parent);
		
		container.setLayout(new FillLayout());
		
		Composite composite = new Composite(container, SWT.EMBEDDED);
		
		composite.setLayout(new FillLayout());
		
		
		m_tcTree = new DocTree();
		JScrollPane treePane= new JScrollPane(m_tcTree);
		SWTUIUtilities.embed( composite, treePane, false);
		m_tcTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		composite.pack();
		container.pack();
		parent.pack();
		
		return parent;
	}

	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// TODO Auto-generated method stub
		String lockButtonLabel = m_registry.getString("lockbutton.Label", "Lock");
		createButton(parent, 0, lockButtonLabel, true);
        createButton(parent, 1, IDialogConstants.CANCEL_LABEL, false);
	}


	@Override
	protected void okPressed() {
		// TODO Auto-generated method stub
		
		AIFTreeNode selectedNode = (AIFTreeNode) m_tcTree.getSelectedNode();
		
		if(selectedNode != null)
		{
			TCComponent selectedComponent = (TCComponent) selectedNode.getComponent();
			
			if(selectedComponent instanceof TCComponentItemRevision)
			{	
				m_selectedComponent = (TCComponentItemRevision) selectedComponent;
				super.okPressed();
				return;
			}
			
		}
		
		com.teamcenter.rac.util.MessageBox.post("Select a Document Revision to lock", "Error", com.teamcenter.rac.util.MessageBox.INFORMATION);	
	}
	
	public TCComponentItemRevision getSelectedComponent() {
		return m_selectedComponent;
	}

	
	public void populateTree(TCComponentItem component)
	{
		TCTreeNode node=new TCTreeNode(component);
		m_tcTree.setRoot(node);
		m_tcTree.setRootVisible(true);
	    m_tcTree.setScrollsOnExpand(true);  
		
	}
	
	private void setTitle()
	{		
		String strDialogTitle = m_registry.getString("lockRevision.TITLE", "Lock Revision");
			
		this.getShell().setText(strDialogTitle);
								
	}
	
}
