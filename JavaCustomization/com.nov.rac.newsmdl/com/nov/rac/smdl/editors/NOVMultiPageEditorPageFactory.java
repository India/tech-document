package com.nov.rac.smdl.editors;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import com.teamcenter.rac.ui.editors.TCMultiPageEditorPageFactory;

public class NOVMultiPageEditorPageFactory extends TCMultiPageEditorPageFactory 
{
	
	public NOVMultiPageEditorPageFactory()
	{
		System.out.println("here....");
	}

	@Override
	public Object create() throws CoreException {
		//return super.create();
		Object obj = null;
        if(m_configElement != null)
        {
            String s = m_configElement.getAttribute("name");
            if(m_id != null)
               
                if(m_id.equals("com.nov.rac.smdl.editors.ViewerPage"))
                    obj = new NOVViewerFormPage(m_id, s);
        }
        return obj;
		
	}

	@Override
	public void setInitializationData(
			IConfigurationElement iconfigurationelement, String s, Object obj)
			throws CoreException {
		//super.setInitializationData(iconfigurationelement, s, obj);
		
		m_configElement = iconfigurationelement;
	        if(obj instanceof String)
	            m_id = (String)obj;
	}

	
	
	
	private IConfigurationElement m_configElement;
    private String m_id;
	
}
