package com.nov.rac.smdl.editors;

import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.panes.SMDLPane;
import com.nov.rac.smdl.views.SMDLView;
import com.teamcenter.rac.common.tcviewer.TCViewerPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.ui.common.RACUIUtil;
import com.teamcenter.rac.ui.editors.ViewerFormPage;
import com.teamcenter.rac.util.SWTUIUtilities;

public class NOVViewerFormPage extends ViewerFormPage
{

	public NOVViewerFormPage(String s, String s1) {
		super(s, s1);
		
	}

	@Override
	protected void createPageContent(Composite composite)
	{
		
		System.out.println("my page content creation");
		
		Composite parent = new Composite(composite, SWT.NONE);
		
		new SMDLPane(parent, SWT.NONE);
        
        
      /*  SWTUIUtilities.embed(composite1, tcViewerPanel);
        final TCComponent cmp = RACUIUtil.getComponent(getEditorInput());
        if(cmp != null)
            SwingUtilities.invokeLater(new Runnable() {

                public void run()
                {
                    tcViewerPanel.setComponent(cmp);
                }

                final TCViewerPanel val$tcViewerPanel;
                final TCComponent val$cmp;
                final ViewerFormPage this$0;

            
            {
                this$0 = ViewerFormPage.this;
                tcViewerPanel = tcviewerpanel;
                cmp = tccomponent;
                super();
            }
            }*/
	}

}
