package com.nov.rac.smdl.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPlaceholderFolderLayout;

import com.teamcenter.rac.aifrcp.perspective.AbstractRACPerspective;

public class MyPerspective extends AbstractRACPerspective {

	
	public static final String ID = "com.nov.rac.smdl.perspectives.MyPerspective";
	/**
	 * Creates the initial layout for a page.
	 */
	
	public MyPerspective()
	{
		
	}

	public void createInitialLayout(IPageLayout ipagelayout)
    {
		//IPageLayout.
        ipagelayout.setEditorAreaVisible(false);
        IFolderLayout ifolderlayout = ipagelayout.createFolder("SMDLFolder", 1, 0.95F, "org.eclipse.ui.editorss");
        ifolderlayout.addView("com.nov.rac.smdl.views.SMDLView");
               
        
        IFolderLayout ifolderlayout1 = ipagelayout.createFolder("TreeFolder", 1, 0.35F, "SMDLFolder");
       // ifolderlayout1.addPlaceholder("com.nov.rac.smdl.views.SMDLHomeView");
        ifolderlayout1.addView("com.nov.rac.smdl.views.SMDLHomeView");
      //  ifolderlayout1.addPlaceholder("com.nov.rac.smdl.views.SMDLSearchView");
        ifolderlayout1.addView("com.nov.rac.smdl.views.SMDLSearchView");
 
        
        IPlaceholderFolderLayout iplaceholderfolderlayout = ipagelayout.createPlaceholderFolder("TableFolder", 4, 0.25F, "SMDLFolder");
        iplaceholderfolderlayout.addPlaceholder("com.nov.rac.smdl.views.SMDLSearchResultView");
        ifolderlayout1.addView("com.nov.rac.smdl.views.SMDLSearchView");
        iplaceholderfolderlayout.addPlaceholder("com.teamcenter.*");
    }

    public static final String TREE_FOLDER = "TreeFolder";
    public static final String SMDL_FOLDER = "SMDLFolder";
    public static final String TABLE_FOLDER = "TableFolder";

}
