package com.nov.rac.smdl.perspectives;

import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.views.OrderView;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.util.AdapterUtil;
import com.teamcenter.rac.util.Registry;

public class SMDLViewPartListener implements IPartListener2{
	
	private static SMDLViewPartListener m_smdlPartListener = null;
//	private Registry 					m_Registry = null;

	private SMDLViewPartListener() {
//		m_Registry = Registry.getRegistry(this);
	}
	
	public static SMDLViewPartListener getSMDLPartListener(){
		if(m_smdlPartListener == null){
			m_smdlPartListener = new SMDLViewPartListener();
		}
		return m_smdlPartListener;
	}

	@Override
	public void partActivated(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partActivated::" + iworkbenchpartreference.isDirty());
		
//		IWorkbenchPart activePart = iworkbenchpartreference.getPart(false);
//		
//		savePartView(activePart);
//		
//		System.out.println(iworkbenchpartreference.getId() + " - partActivated:post save:" + iworkbenchpartreference.isDirty());

	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partBroughtToTop");
	}

	@Override
	public void partClosed(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partClosed");
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partDeactivated: " + iworkbenchpartreference.isDirty());
		
//		IWorkbenchPart activePart = iworkbenchpartreference.getPart(false);
//		
//		savePartView(activePart);
		
//		System.out.println(iworkbenchpartreference.getId() + " - partDeactivated:post Save:: " + iworkbenchpartreference.isDirty());
		
//		if(iworkbenchpartreference.isDirty()){
//			IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//			IWorkbenchPage page=iw.getActivePage();
//			
//			page.activate(activePart);
//		}
	}

	@Override
	public void partOpened(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partOpened");
	}

	@Override
	public void partHidden(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partHidden::" +  iworkbenchpartreference.isDirty());
		
//		IWorkbenchPart activePart = iworkbenchpartreference.getPart(false);
//		
//		savePartView(activePart);
		
//		System.out.println(iworkbenchpartreference.getId() + " - partHidden::post save  " +  iworkbenchpartreference.isDirty());
		
//		if(iworkbenchpartreference.isDirty()){
//			IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//			IWorkbenchPage page=iw.getActivePage();
//			
//			page.activate(activePart);
//		}
	}

	@Override
	public void partVisible(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partVisible");
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference iworkbenchpartreference) {
		// TODO Auto-generated method stub
//		System.out.println(iworkbenchpartreference.getId() + " - partInputChanged");
	}
	
//	public void savePartView(IWorkbenchPart activePart){
//
//		
//		ISaveablePart savablePart = (ISaveablePart) AdapterUtil.getAdapter(activePart, ISaveablePart.class);
//		if(savablePart != null)
//		{			
//			System.out.println("The View Dirty: " + savablePart.isDirty());
//			
//			if(savablePart.isDirty()){
//				
//				Shell parentShell = activePart.getSite().getShell();
//				
//				MessageBox confirmMessageBox = new MessageBox(parentShell, SWT.APPLICATION_MODAL|SWT.YES| SWT.NO);
//		        
//				confirmMessageBox.setText(m_Registry.getString("SaveDialog.TITLE"));
//				confirmMessageBox.setMessage(m_Registry.getString("SaveDialog.MSG"));
//				
//		        int buttonID = confirmMessageBox.open();
//		        
//		        if(buttonID == SWT.YES){
//		        	savablePart.doSave(null);
//		        }
//			}
//		}
//	}

}
