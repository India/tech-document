package com.nov.rac.smdl.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.teamcenter.rac.aifrcp.perspective.AbstractRACPerspective;

public class SMDLPerspective extends AbstractRACPerspective {
	
	public static final String ID = "com.nov.rac.smdl.perspectives.SMDLPerspective";

	/**
	 * Creates the initial layout for a page.
	 */
	/*public void createInitialLayout(IPageLayout layout) 
	{
		super.createInitialLayout(layout);
		
		String editorArea = layout.getEditorArea();
		addFastViews(layout);
		addViewShortcuts(layout);
		addPerspectiveShortcuts(layout);
	}

	*//**
	 * Add fast views to the perspective.
	 *//*
	private void addFastViews(IPageLayout layout) {
	}

	*//**
	 * Add view shortcuts to the perspective.
	 *//*
	private void addViewShortcuts(IPageLayout layout) {
	}

	*//**
	 * Add perspective shortcuts to the perspective.
	 *//*
	private void addPerspectiveShortcuts(IPageLayout layout) {
	}
*/
	public SMDLPerspective()
	{
		
	}
	

	/**
	 * Creates the initial layout for a page.
	 */
	public void createInitialLayout(IPageLayout ipagelayout) 
	{
			ipagelayout.setEditorAreaVisible(false);
			
			IFolderLayout ifolderlayout1 = ipagelayout.createFolder("SMDLFolder", 1, 0.95F, "org.eclipse.ui.editorss");
			ifolderlayout1.addView("com.nov.rac.smdl.views.OrderView");
			ifolderlayout1.addView("com.nov.rac.smdl.views.DeliveredProductView");
			ifolderlayout1.addView("com.nov.rac.smdl.views.SMDLView");
			ifolderlayout1.addView("com.nov.rac.smdl.views.SMDLSearchView");
			
			  
	        IFolderLayout ifolderlayout2 = ipagelayout.createFolder("TreeFolder", 1, 0.35F, "SMDLFolder");
	        ifolderlayout2.addView("com.nov.rac.smdl.views.SMDLHomeView");
	        ifolderlayout2.addPlaceholder("com.nov.rac.smdl.views.SMDLExplorerView:*");
	        
	        SMDLViewPartListener smdlViewListener = SMDLViewPartListener.getSMDLPartListener();
	        
			IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			IWorkbenchPage page = iw.getActivePage();

			page.removePartListener(smdlViewListener);
			
			page.addPartListener(smdlViewListener);
	}
	
}
