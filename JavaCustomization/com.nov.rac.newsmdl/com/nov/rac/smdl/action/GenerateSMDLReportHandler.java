package com.nov.rac.smdl.action;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;


import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import com.noi.util.components.JIBPlantLOVList;
import com.nov.quicksearch.export.ExportTableData;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLObjectSOAHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class GenerateSMDLReportHandler extends AbstractHandler
{
	private class SMDLCodeComparator implements Comparator<TCComponent> 
	{
		@Override
		public int compare(TCComponent obj1, TCComponent obj2) 
		{	
			String strA = null;
			String strB = null;
			try 
			{
				strA = obj1.getProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER);	
				strB = obj2.getProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER);
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
			return strA.compareToIgnoreCase(strB);
		}
	}
	public GenerateSMDLReportHandler()
	{
		super();
		registry = Registry.getRegistry(this);
	}
	
	@Override
	public  Object execute(ExecutionEvent executionevent)throws ExecutionException
	{		
		TCComponent targetObject = (TCComponent) AIFUtility.getTargetComponent();
		TCComponentItemRevision itmRev = null;
		
		if(targetObject instanceof TCComponentItem)
		{
			TCComponentItem item  = (TCComponentItem) targetObject;
			TCComponent[] smdl_Rev_list=null;
			try
			{
				smdl_Rev_list = item.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				itmRev = (TCComponentItemRevision) smdl_Rev_list[smdl_Rev_list.length -1];
			} 
			catch (TCException tcException)
			{
				
				tcException.printStackTrace();
			}
		}
		else
		{
			itmRev=(TCComponentItemRevision)targetObject;
		}

		m_smdlSOAPlainObject = NOVSMDLObjectSOAHelper.getSMDLRevisionProps(itmRev);
		
		exportSMDLReportToExcel( m_smdlSOAPlainObject);
		
		return null;
		
	}
	
	private void exportSMDLReportToExcel(TCComponent m_soaObj) 
	{
		try 
		{
			Shell parentShell = AIFUtility.getActiveDesktop().getShell(); 
			FileDialog fd = new FileDialog(parentShell, SWT.SAVE);
	        fd.setText(registry.getString("Save.TITLE"));
	        fd.setFilterPath(System.getProperty("user.home"));
	        String[] filterExt = { "*.xls" };
	        fd.setFilterExtensions(filterExt);
	        //added
	        String sFileName= m_soaObj.getTCProperty(NOV4ComponentSMDL.SMDL_ID).getStringValue();
			fd.setFileName(sFileName);
	        String fileName = fd.open();
	        
	        if( fileName != null && !fileName.isEmpty())
	        {
	        	java.io.File file = new File(fileName);
			
				exportData( file, m_soaObj);
	        }
		}
		catch (TCException e)
		{
			e.printStackTrace();
		}
		catch (IOException ioexception)
		{
			ioexception.printStackTrace();
		}
	}

	private void exportData(final File file, final TCComponent m_soaObj) throws IOException 
	{
		final FileOutputStream out = new FileOutputStream(file);
    	
		final HSSFWorkbook wb = new HSSFWorkbook();
		final HSSFSheet sheet = wb.createSheet();
		wb.setSheetName(0, registry.getString("Sheet.TITLE"));	
		
		Display.getCurrent().syncExec( new Runnable() 
		{
		
			@Override
			public void run() 
			{
				try 
				{
					createInfoPanelBody(sheet, m_soaObj);
				
					ExportTableData exp;
					String[][] smdlTableData = getSMDLTableColumns();
					if(smdlTableData!=null)
					{
						exp = new ExportTableData( getSMDLHeaderColumnNames(),smdlTableData );
						exp.exportTableDataToExcel(file,wb);
					}
					
					wb.write(out);
					out.close();
				} catch (TCException e) 
				{
					e.printStackTrace();
				
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		});
		
		
		//MessageBox.post("Data successfully exported to " + file.toString(), "Information", MessageBox.INFORMATION);
		
	}

	private void getPlantLOVList()
	{
		if (m_soaPlantList == null)
		{
			m_soaPlantList = new JIBPlantLOVList();
			m_soaPlantList.init();
		}
	}
	
	
	private void createInfoPanelBody(HSSFSheet sheet,TCComponent m_soaObj) throws TCException 
	{		
		registry = Registry.getRegistry("com.nov.rac.smdl.panes.panes");
		getPlantLOVList();
		m_SMDLItem  = m_soaObj.getTCProperty(NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
		TCComponent m_OrderObject = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.ORDER_REF).getReferenceValue();
		TCComponent m_DPObject = m_SMDLItem.getTCProperty(NOV4ComponentSMDL.DP_REF).getReferenceValue();
		
		HSSFCellStyle cs = sheet.getWorkbook().createCellStyle();
		HSSFFont font = sheet.getWorkbook().createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		cs.setFont(font);
		
		HSSFRow row = sheet.createRow(0);
		row.createCell(0).setCellValue(registry.getString("SMDL_Title.Label"));	
		row.getCell(0).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(0,0,0,2));		
		String smdlID = m_soaObj.getTCProperty(NOV4ComponentSMDL.SMDL_ID).getStringValue();
		String smdlRevId = m_soaObj.getTCProperty(NOV4ComponentSMDL.SMDL_REVISION_ID).getStringValue();
		
		row.createCell(3).setCellValue(smdlID +"/" +smdlRevId);	
		row.createCell(4).setCellValue(registry.getString("SMDL_Desc.Label"));	
		row.getCell(4).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(0,0,4,13));
		row.createCell(14).setCellValue(m_SMDLItem.getTCProperty(NOV4ComponentSMDL.SMDL_DESC).getStringValue());
		row.getCell(14).getCellStyle().setWrapText(true);
		
		HSSFRow row1 = sheet.createRow(1);
		row1.createCell(0).setCellValue(registry.getString("SMDL_Customer.Label"));
		row1.getCell(0).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(1,1,0,2));
		row1.createCell(3).setCellValue(m_OrderObject.getTCProperty(NOV4ComponentOrder.CUSTOMER).getStringValue());
		row1.createCell(4).setCellValue(registry.getString("SMDL_SalesOrder.Label"));
		row1.getCell(4).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(1,1,4,6));
		row1.createCell(7).setCellValue(m_OrderObject.getTCProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER).getStringValue());
		row1.createCell(8).setCellValue("Order Id");
		row1.getCell(8).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(1,1,8,10));
		row1.createCell(11).setCellValue(m_OrderObject.getTCProperty(NOV4ComponentOrder.ORDER_NUMBER).getStringValue());
		row1.createCell(12).setCellValue(registry.getString("SMDL_SMDLType.Label"));
		row1.getCell(12).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(1,1,12,13));
		row1.createCell(14).setCellValue(m_SMDLItem.getTCProperty(NOV4ComponentSMDL.SMDL_TYPE).getStringValue());
		
		HSSFRow row2 = sheet.createRow(2);
		int plantIdInt = m_OrderObject.getTCProperty(NOV4ComponentOrder.PLANT_ID_OTHER).getIntValue();
		String plantIdStr = m_soaPlantList.getStringFromReference(plantIdInt);
		row2.createCell(0).setCellValue(registry.getString("SMDL_Rig.Label"));
		row2.getCell(0).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(2,2,0,2));
		row2.createCell(3).setCellValue(plantIdStr);
		row2.createCell(4).setCellValue(registry.getString("SMDL_EngJob.Label"));
		row2.getCell(4).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(2,2,4,6));
		String[] strEngJobNumbes = m_DPObject.getTCProperty(NOV4ComponentDP.DP_ENG_JOB_NUMBERS).getStringValueArray();

	/*	for(int iCnt = 0; iCnt < strEngJobNumbes.length; iCnt++)
		{
		
			if(iCnt != strEngJobNumbes.length -1)
			{
				engJobNum=( strEngJobNumbes[iCnt] + ", ");
			}
			else
			{
				engJobNum=(engJobNum + strEngJobNumbes[iCnt]);
			}
		}*/
		if(strEngJobNumbes.length>0)
		{
			StringBuffer engNumbers = new StringBuffer();
			
			engNumbers.append(strEngJobNumbes[0]);
			
			for(int iCnt = 1; iCnt < strEngJobNumbes.length; iCnt++)
			{
				engNumbers.append(",");
				engNumbers.append(strEngJobNumbes[iCnt]);
			}
			engJobNum = engNumbers.toString();
		}
		row2.createCell(7).setCellValue(engJobNum);
		row2.createCell(8).setCellValue("Delivered Product Id");
		row2.getCell(8).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(2,2,8,10));
		row2.createCell(11).setCellValue(m_DPObject.getTCProperty("item_id").getPropertyValue().toString());
		
		HSSFRow row3 = sheet.createRow(3);
		row3.createCell(0).setCellValue(registry.getString("SMDL_Product.Label"));
		row3.getCell(0).setCellStyle(cs);
		sheet.addMergedRegion(new CellRangeAddress(3,3,0,2));
		row3.createCell(3).setCellValue(m_DPObject.getTCProperty(NOV4ComponentDP.DP_BASED_ON_PROD).getStringValue());
	
	}
		
	private String[] getSMDLHeaderColumnNames() 
	{
		String[] strColNames = null;
		registry = Registry.getRegistry("com.nov.rac.smdl.common.common");
		TCPreferenceService tablePrefService = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = registry.getString("table_column_display_names.PREF", null);
		strColNames = tablePrefService.getStringArray(TCPreferenceService.TC_preference_site, smdlTableColumnPref);
		
		return strColNames;

	}
	
	private String[][] getSMDLTableColumns() throws TCException 
	{
		String[] strColNames=null;
		String[][] strColValues=null;
		
		TCComponent[] codeFormObjs = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
		List<TCComponent> codeFormObjsList = Arrays.asList(codeFormObjs);//TC10.1 Upgrade
		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = NOVSMDLConstants.NOVSMDL_TABLECOLUMNS_PREF;
		strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_site, smdlTableColumnPref);	

	    //TCComponentType.getPropertiesSet(codeFormObjs, strColNames);
	    TCComponentType.getPropertiesSet(codeFormObjsList, strColNames);//TC10.1 Upgrade
		
		Arrays.sort(codeFormObjs, new SMDLCodeComparator());
		//strColValues=TCComponentType.getPropertiesSet(codeFormObjs, strColNames);
		strColValues=TCComponentType.getPropertiesSet(codeFormObjsList, strColNames);//TC10.1 Upgrade
		
		setCodeFormObjects(strColNames, codeFormObjs,strColValues);
		return strColValues;
	}

	private void setCodeFormObjects(String[] strColNames,TCComponent[] codeFormObjs,String[][] strColumnValues) throws TCException 
	{	
		Vector<Integer> nReqdColList = new Vector<Integer>();
		
		int nDescIndex = -1;
	    int nRelStatusIndex = -1;
	    int nDocRevIndex = -1;
	    int nDocNameIndex = -1;
	    int nDocIDIndex = -1;
	    
	    for(int colnum=0; colnum<strColNames.length; colnum++)
	    {
	    	//if((strColNames[colnum]).equals(NOV4ComponentSMDL.SMDL_DESC))
	    	if((strColNames[colnum]).equals(NOV4ComponentSMDL.SMDL_CODEFORM_DOC_DESC))
			{
	    		nDescIndex = colnum;
	    		
	    		// store the index of the column in array.
	    		nReqdColList.add( colnum );
			}
	    	
//	    	if((strColNames[colnum]).equals(NOV4ComponentSMDL.NOV_RELEASE_STATUS))
	    	if((strColNames[colnum]).equals(NOV4ComponentSMDL.NOV_RELEASE_STATUS))
	    	{
	    		nRelStatusIndex = colnum;
	    		
	    		// store the index of the column in array.
	    		nReqdColList.add( colnum );
	    	}
	    	
	    	if(strColNames[colnum].equals(NOV4ComponentSMDL.DOCUMENT_REV_REF))
	    	{
	    		nDocRevIndex = colnum;
	    		
	    		// store the index of the column in array.
	    		nReqdColList.add( colnum );
	    	}
	    	
	    	// if((strColNames[colnum]).equals(NOV4ComponentSMDL.SMDL_NAME))
	    	if((strColNames[colnum]).equals(NOV4ComponentSMDL.SMDL_CODEFORM_DOC_NAME))
	    	 {
	    		 nDocNameIndex = colnum;
	    		
	    		// store the index of the column in array.
	    		nReqdColList.add( colnum );
	    	 }
	    	
	    	if((strColNames[colnum]).equals(NOV4ComponentSMDL.DOCUMENT_REF))
	    	 {
	    		nDocIDIndex = colnum;
	    		
	    		// store the index of the column in array.
	    		nReqdColList.add( colnum );
	    	 }
	    	
	    }

		for (int rownum=0; rownum<codeFormObjs.length; rownum++)
        {
			 String m_docStatus=null; 
			 String m_smdlDocRev=null;
		 	 String sDocRevId = null;
			 Object docStatus = null;
		 	 String sDocTitle = null;
			 String sDocRevDesc = null;
			 String sDocId = null;
			 
			 TCComponent smdlDoc = codeFormObjs[rownum].getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
			 
			 if(smdlDoc != null)
			 {
				 sDocId = smdlDoc.getTCProperty(NOVSMDLConstants.DOCUMENT_NUMBER).getStringValue();
				 smdlDocRev = codeFormObjs[rownum].getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if(smdlDocRev != null)
				{
					sDocRevId = smdlDocRev.getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();
					TCComponent[] statusList = smdlDocRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();						
					if(statusList.length > 0)
					{
						docStatus = statusList[statusList.length-1].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS_NAME).getPropertyValue();
					}
					sDocRevDesc = smdlDocRev.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC).getStringValue();
				}
				else
				{
					TCComponent[] doc_Rev_list = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					TCComponentItemRevision docRevision = (TCComponentItemRevision) doc_Rev_list[doc_Rev_list.length -1];
				
					//update status column 
					if(docRevision != null)
					{
						sDocRevId = docRevision.getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();
						TCComponent[] statusList = docRevision.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
						if(statusList.length > 0)
						{
							docStatus = statusList[statusList.length-1].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS_NAME).getPropertyValue();
						}
						sDocRevDesc = docRevision.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC).getStringValue();
					}
				
		        	
				}
				sDocTitle = smdlDoc.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();
				
			}
				
			int colnum = -1;
			 for(int inx=0; inx<nReqdColList.size(); inx++)		 
			 {
				 colnum =  nReqdColList.elementAt(inx);
				 
				 if((nDescIndex == colnum))
				 {
					  strColumnValues[rownum][colnum]=sDocRevDesc;
					 
				  }
				 if( nRelStatusIndex == colnum )
				 {
				     if(docStatus==null)
					  {
						  m_docStatus="";
					  }
					  else
					  {
						  m_docStatus=docStatus.toString();
			          }
					  strColumnValues[rownum][colnum]= m_docStatus;				
				 }
				 
				  if(nDocRevIndex == colnum)
				  {
					  if(sDocRevId==null)
					  {
						  m_smdlDocRev="";
					  }
					  else
					  {
						  m_smdlDocRev=sDocRevId;
					  }
					  strColumnValues[rownum][colnum]= m_smdlDocRev;
									
				  }
			      if(nDocNameIndex == colnum)
				  {
				      strColumnValues[rownum][colnum]= sDocTitle;
						
				  }
			      
			      if(nDocIDIndex == colnum)
				  {
				      strColumnValues[rownum][colnum]= sDocId;
						
				  }
								 
			   }
	   }
	}

	private Registry registry;
	private static JIBPlantLOVList m_soaPlantList = null;
	private TCComponent m_smdlSOAPlainObject = null;
	//private TCComponentItemRevision itmRev = null;
	private TCComponent m_SMDLItem=null;
	private TCComponent smdlDocRev = null;
	private String engJobNum=null;

}
