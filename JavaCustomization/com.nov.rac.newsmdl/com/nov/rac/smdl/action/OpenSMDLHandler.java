package com.nov.rac.smdl.action;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.close.SMDLCloseDialog;
import com.nov.rac.smdl.close.SMDLOpenDialog;
import com.nov.rac.smdl.services.OpenSMDLOperation;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevisionType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseResponse2;

public class OpenSMDLHandler extends AbstractHandler
{

	private Registry registry;
	
	public OpenSMDLHandler()
	{
		super();
		registry = Registry.getRegistry(this);
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		
		InterfaceAIFComponent[] selComps = AIFUtility.getTargetComponents();
		
		final Shell parentShell = AIFUtility.getActiveDesktop().getShell(); 
		
		final TCComponent targetComp = (TCComponent) selComps[0];
		
		final SMDLOperationHelper smdlHelper = new SMDLOperationHelper(targetComp);
		
		if( smdlHelper.getSMDLItemRev() == null )
			return null;
		
		if(smdlHelper.isSMDLClosed()){
//			MessageBox confirmMessageBox = new MessageBox(parentShell, SWT.PRIMARY_MODAL|SWT.YES| SWT.NO);
//	        
//			confirmMessageBox.setText(registry.getString("Note.TITLE"));
//			confirmMessageBox.setMessage(registry.getString("smdlOpenNote.MSG"));
//			
//	        int buttonID = confirmMessageBox.open();
//	        
//	        if(buttonID == SWT.YES){
			
			SMDLOpenDialog smdlOpenDialog = new SMDLOpenDialog(parentShell, SWT.DIALOG_TRIM | SWT.RESIZE |SWT.APPLICATION_MODAL);

			smdlOpenDialog.create();

			smdlOpenDialog.populate(smdlHelper.getSMDLItemRev());

			smdlOpenDialog.open();
				
//	        }
	        
//	        switch(buttonID)
//	        {
//	          case SWT.YES:
//							try {
//								ProgressMonitorDialog openStatusDialog=new ProgressMonitorDialog(parentShell);
//								//openStatusDialog.setOpenOnRun(false);
//								openStatusDialog.run(true, false,new OpenSMDLOperation((TCComponent) targetComp));
//							} catch (InvocationTargetException e) {
//								
//								e.printStackTrace();
//							} catch (InterruptedException e) {
//								
//								e.printStackTrace();
//							}
//	        	  
//				        	 break;
//	          case SWT.NO:
//				            // cancel smdl open ...
//				            break;
//	        }
		}
		else{
			MessageBox errorMessageBox = new MessageBox(parentShell, SWT.ICON_INFORMATION);
			errorMessageBox.setText(registry.getString("Information.TITLE"));
			errorMessageBox.setMessage(registry.getString("smdlOpenInfo.MSG"));
			errorMessageBox.open();
		}	
		
		return null;
	}


}
