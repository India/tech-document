package com.nov.rac.smdl.action;

import java.beans.PropertyChangeListener;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.panes.order.CreateOrderDialog;
import com.nov.rac.smdl.views.SMDLHomeView;
import com.teamcenter.rac.util.AdapterUtil;

public class OrderCreateHandler extends AbstractHandler 
{
    public OrderCreateHandler()
    {
    	super();
    }
	 
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{		
		CreateOrderDialog ordCreateDlg = new CreateOrderDialog(Display.getCurrent().getActiveShell());
		ordCreateDlg.create();

		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IWorkbenchPart activePart = null;
		activePart = page.findView(SMDLHomeView.ID);
		if(activePart != null) {
		    PropertyChangeListener theListener = (PropertyChangeListener) AdapterUtil.getAdapter(activePart, PropertyChangeListener.class);
		    ordCreateDlg.addPropertyChangeListeners(theListener);     
		}

		ordCreateDlg.open();				

	 return null;
	}
}