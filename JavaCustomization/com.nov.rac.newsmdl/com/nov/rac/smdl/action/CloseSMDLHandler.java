package com.nov.rac.smdl.action;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.close.SMDLCloseDialog;
import com.nov.rac.smdl.close.SMDLCloseWizard;
import com.nov.rac.smdl.close.SMDLCloseWizardDialog;
import com.nov.rac.smdl.services.CloseSMDLOperation;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class CloseSMDLHandler extends AbstractHandler 
{
	private Registry m_Registry = null;
	
	public CloseSMDLHandler()
	{
		super();
		m_Registry = Registry.getRegistry(this);
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		InterfaceAIFComponent[] selComps = AIFUtility.getTargetComponents();
		
		TCComponent targetComp = null;
		
		if(selComps != null && selComps.length > 0)
			targetComp = (TCComponent) selComps[0];
		
		Shell parentShell = AIFUtility.getActiveDesktop().getShell(); 
		
		SMDLOperationHelper smdlHelper = new SMDLOperationHelper(targetComp);
		
		if( smdlHelper.getSMDLItemRev() == null )
			return null;
		
		if(smdlHelper.isSMDLClosed()){
			MessageBox errorMessageBox = new MessageBox(parentShell, SWT.ICON_INFORMATION);
			errorMessageBox.setText(m_Registry.getString("Information.TITLE"));
			errorMessageBox.setMessage(m_Registry.getString("smdlCloseInfo.MSG"));
			errorMessageBox.open();
		}
		else {
		
			if(smdlHelper.isLatestSMDLRevReleased()){
				
				SMDLCloseDialog smdlCloseDialog = new SMDLCloseDialog(parentShell, SWT.DIALOG_TRIM | SWT.RESIZE |SWT.APPLICATION_MODAL);
				
				smdlCloseDialog.create();
				
				boolean bClosable = smdlCloseDialog.populate(smdlHelper.getSMDLItemRev());
				
				if(bClosable){
					smdlCloseDialog.open();
				}
				else{
					smdlCloseDialog.close();
				}
				
			}
			else{
				MessageBox errorMessageBox = new MessageBox(parentShell, SWT.ERROR);
				errorMessageBox.setText(m_Registry.getString("Error.TITLE"));
				errorMessageBox.setMessage(m_Registry.getString("smdlReleseError.MSG"));
				errorMessageBox.open();				
			}
				
			
		}

		return null;
	}
	
}
