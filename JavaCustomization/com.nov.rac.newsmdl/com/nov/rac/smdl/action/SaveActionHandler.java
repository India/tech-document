package com.nov.rac.smdl.action;


import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

public class SaveActionHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		
		final IWorkbenchPart activePart= PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
		if(activePart instanceof ISaveablePart)
		{
			SaveOperation saveOperation = new SaveOperation((ISaveablePart)activePart);
			ProgressMonitorDialog saveDPDialog=new ProgressMonitorDialog(activePart.getSite().getShell());
			try 
			{
				saveDPDialog.run(false, false, saveOperation);
			} 
			catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
			
		}
		
		return null;
	}

	private class SaveOperation implements IRunnableWithProgress
	{
		private ISaveablePart m_activepart;
		
		public SaveOperation(ISaveablePart part)
		{
			m_activepart= part;
		}

		@Override
		public void run(IProgressMonitor progressMonitor)
				throws InvocationTargetException, InterruptedException {
			final IProgressMonitor finalprogressMonitor = progressMonitor;
			Display.getCurrent().asyncExec( new Runnable() {

				@Override
				public void run() 
				{

					m_activepart.doSave(finalprogressMonitor);
				}
				
			});

		}
		
	}

}
