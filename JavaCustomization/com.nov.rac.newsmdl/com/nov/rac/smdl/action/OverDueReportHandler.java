package com.nov.rac.smdl.action;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.smdl.reports.ReportsDialog;
public class OverDueReportHandler extends AbstractHandler
{

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        
        String reportType;

        final IWorkbenchWindow window = HandlerUtil
                .getActiveWorkbenchWindowChecked(event);
        reportType = event.getCommand().getId();
        ReportsDialog reportsDialog = new ReportsDialog(window
                .getShell(), false);
        reportsDialog.setReportType(reportType);
        reportsDialog.create();
        reportsDialog.open();       
        
        return null;
    }

}
