package com.nov.rac.smdl.action;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.providers.providers.ISMDLContentProvider;
import com.nov.rac.smdl.providers.providers.SMDLExplorerContentProvider;
import com.nov.rac.smdl.providers.providers.SMDLHomeContentProvider;
import com.nov.rac.smdl.utilities.ComponentHelper;
import com.nov.rac.smdl.views.SMDLComponentView;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class RefreshActionHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IContentProvider theContentProvider = null;

		Object selectedObject = null;

		IWorkbenchPart activePart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();

		if(activePart != null && activePart instanceof SMDLComponentView){

			StructuredViewer theViewer = ((SMDLComponentView)activePart).getViewer();

			theContentProvider = theViewer.getContentProvider();

			selectedObject = theViewer.getSelection();

			if(selectedObject != null && theContentProvider != null){

				Object firstElement = ((TreeSelection)selectedObject).getFirstElement();

				TCComponent smdlObject = ComponentHelper.getComponent(firstElement);
				try {
					smdlObject.refresh();
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				((ISMDLContentProvider)theContentProvider).loadSMDLHierarchy(smdlObject);
			}

			theViewer.refresh(true);
		}

		return null;
	}
}
