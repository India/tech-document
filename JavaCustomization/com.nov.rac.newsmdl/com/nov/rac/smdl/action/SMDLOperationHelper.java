package com.nov.rac.smdl.action;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SMDLOperationHelper {
	
	private TCComponent m_SMDLItem = null;
	private TCComponent m_SMDLItemRev = null;
	
	private String		m_SMDLItemStatus = null;
	private String		m_SMDLItemRevStatus = null;
	
	public static int	SMDL_CLOSE = 0;
	public static int	SMDL_OPEN = 1;

	private Registry m_registry;
	
	public SMDLOperationHelper(TCComponent targetObject){

		m_registry = Registry.getRegistry( this );

		m_SMDLItem = loadSMDLItem(targetObject);
		
		m_SMDLItemRev = loadSMDLItemRev(targetObject);
	}

	public String getSMDLItemStatus() {
		
		if( m_SMDLItemStatus == null && m_SMDLItem != null )
		{
			m_SMDLItemStatus = new String("");
			try {
				TCComponent[] statusList = m_SMDLItem.getTCProperty(NOVSMDLConstants.SMDL_RELEASE_STATUS).getReferenceValueArray();
				
				if (statusList != null && statusList.length > 0){
					m_SMDLItemStatus  = statusList[statusList.length -1].getTCProperty(NOVSMDLConstants.REL_STATUS_NAME).getStringValue();
				}
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return m_SMDLItemStatus;
	}
	
	public String getSMDLRevisionStatus() {
		
		if( m_SMDLItemRevStatus == null && m_SMDLItemRev!= null )
		{
			m_SMDLItemRevStatus = new String("");
			try {
				TCComponent[] statusList = m_SMDLItemRev.getTCProperty(NOVSMDLConstants.SMDL_RELEASE_STATUS).getReferenceValueArray();
				
				if (statusList != null && statusList.length > 0){
					m_SMDLItemRevStatus  = statusList[statusList.length -1].getTCProperty(NOVSMDLConstants.REL_STATUS_NAME).getStringValue();
				}
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return m_SMDLItemRevStatus;
	}
	
	public Boolean isSMDLClosed (){
		
		boolean bStatus = false;
		
		String smdlStatus = getSMDLItemStatus();
		
		if(smdlStatus.compareTo(NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME) == 0){
			bStatus = true;
		}
		
		return bStatus;		
	}
	
	public Boolean isLatestSMDLRevReleased (){
		
		boolean bStatus = false;
		
		String smdlRevStatus = getSMDLRevisionStatus();
		
		if( smdlRevStatus != null && smdlRevStatus.length() > 0 )
		{
			bStatus = true;
		}
		
		return bStatus;		
	}

	public TCComponent loadSMDLItem(TCComponent targetComp) {
		
		TCComponent itemObject = null;
		
		if(targetComp instanceof NOV4ComponentSMDL){
			itemObject = targetComp;
		}
		else{
			try {
				itemObject = ((TCComponentItemRevision)targetComp).getItem();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return itemObject;
	}
	
	public TCComponent getSMDLItemRev()
	{
		return m_SMDLItemRev;
	}
	
	private TCComponent loadSMDLItemRev(TCComponent targetComp) {
		
		TCComponent itemRevObject = null;
		
		if(targetComp instanceof NOV4ComponentSMDLRevision){
			try {
				targetComp = ((NOV4ComponentSMDLRevision) targetComp).getItem();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			itemRevObject = ((TCComponentItem)targetComp).getLatestItemRevision();
		}
		catch( TCException e )
		{
			//e.printStackTrace();
			MessageBox.post( e.getDetailsMessage(), m_registry.getString( "TCException.TITLE" ), 1 );
		}
		
		return itemRevObject;
	}
}
