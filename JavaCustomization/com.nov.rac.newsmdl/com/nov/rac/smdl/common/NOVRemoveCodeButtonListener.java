package com.nov.rac.smdl.common;

import javax.swing.table.TableModel;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.util.Registry;

public class NOVRemoveCodeButtonListener implements SelectionListener
{
	private NOVSMDLTable m_smdlTable;
	private Registry m_reg;
	public NOVRemoveCodeButtonListener()
	{
		m_reg = Registry.getRegistry(this);
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) 
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button removeCodeButton= (Button)target;
			m_smdlTable=(NOVSMDLTable)removeCodeButton.getData();		
			removeSelectedRow();			
		}
	}
	private void removeSelectedRow()
	{
		 Shell shell = Display.getCurrent().getActiveShell();
		 TableModel tableModel = m_smdlTable.getModel();
		 if(tableModel instanceof NOVSMDLTableModel)
		 {
			 NOVSMDLTableModel smdlTableModel = (NOVSMDLTableModel)tableModel;
			 int iTableRowCount = smdlTableModel.getRowCount();
			 int iSelectedRows[] = m_smdlTable.getSelectedRows();
			 int iSelectedRowSize = iSelectedRows.length;
			 if ( iTableRowCount == 0 )
			 {
				 MessageDialog.openWarning(shell, "Warning ! SMDL...", m_reg.getString("EmptySMDL.MSG"));
			 }
			 else if ( iSelectedRowSize == 0 )
			 {
				 MessageDialog.openError(shell, "Error ! SMDL...", m_reg.getString("CodeSelectionError.MSG"));
			 }
			 else
			 {
				 boolean bResponse = MessageDialog.openConfirm(shell, "Confirm", m_reg.getString("RemoveCodeConfirm.MSG"));
				 if(bResponse == true)
				 {
					 for ( int i = 0,index = iSelectedRowSize - 1; i < iSelectedRowSize; i++,index--)
					 {
						 smdlTableModel.removeRow(iSelectedRows[index]);
					 }
					 smdlTableModel.fireTableRowsDeleted(0, iTableRowCount);
				 }
			 }
		 }
	}
}
