package com.nov.rac.smdl.common;

import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.databinding.SMDLDataBindingHelper;
import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.SWTUIUtilities;

public class NOVDateButtonComposite extends Composite
{
		protected DateButton m_dateButton = null;
		
		private String 				m_AttributeName = null;
		private String				m_dataModelName = null;
		
		public DateButton getDateButton() {
			return m_dateButton;
		}
		
			
		public NOVDateButtonComposite(Composite parent)
		{
			  super(parent, SWT.EMBEDDED);
		}
	
		public NOVDateButtonComposite(Composite parent, int style)
		{
		     super(parent,style | SWT.EMBEDDED);
		     m_dateButton = new DateButton();
		     
		     //TCDECREL-2447 data selection dialogs
		     NOVDateButtonHelper.addDateButtonListener(m_dateButton);
		     
		     SWTUIUtilities.embed( this, m_dateButton, false);
		     
		     initDateControl();
		}
		
		public String getDateString()
		{
			return m_dateButton.getDateString();
		}
		public void setDateString(String date)
		{
			m_dateButton.setDate(date);
		}
		
		public void setDate(Date date)
		{
			m_dateButton.setDate(date);
		}
		public Date getDate()
		{
			return m_dateButton.getDate();
		}
		
		public void initDateControl()
		{
			m_dateButton.setDate("");
			m_dateButton.setDisplayFormat("dd-MMM-yyyy");

		}
		
		public void setAttributeName(String attrName){
			m_AttributeName = attrName;
		}

		public void setBindingContext(String dataModelName){
			m_dataModelName = dataModelName;
		}
		
		public void createDataBinding(){
			
			SMDLDataBindingHelper dataBindingHelper = new SMDLDataBindingHelper();
			dataBindingHelper.bindData(m_dateButton,m_AttributeName,m_dataModelName);
		}
		
		
}
