package com.nov.rac.smdl.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.eclipse.swt.widgets.*;

import javax.swing.JMenuItem;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.utilities.NOVOpenObjectUtils;
import com.nov.rac.smdl.utilities.NOVSMDLTableHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.popupmenu.AbstractTCComponentPopupMenu;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.nov.rac.smdl.lockrevision.LockRevDialog;

public class NOVTablePopupMenu extends AbstractTCComponentPopupMenu
{
	private static final long serialVersionUID = 1L;
	private JMenuItem workOnDocItem = null;
	private JMenuItem lockRevisionItem =null;
	private NOVSMDLTable smdlTable = null;
	
	private Registry m_registry = null;
	
	public NOVTablePopupMenu(final TCSession  tcsession,AIFTable table, final int iSelectedRow)
     {
		 super();
		
		 m_registry = Registry.getRegistry("com.nov.rac.smdl.common.common");
		 
		 smdlTable =(NOVSMDLTable)table;
		 if( smdlTable != null)
		 {
			 addPopupHeader("SMDL");
			 workOnDocItem=addMenuItem(this, "WorkOnDocAction");
			 workOnDocItem.setText(m_registry.getString("WorkOnDocument.Label"));
			 
			 lockRevisionItem=addMenuItem(this, "LockRevisionAction");
			 int idocRevCol =smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_REV_REF);
			 Object rev = smdlTable.getValueAt(iSelectedRow, idocRevCol );
			 
			 if(rev != " ")
			 {
				 lockRevisionItem.setText(m_registry.getString("unlockrevision.Label","Lock Revision"));				 
			 }
			 else
			 {
				 lockRevisionItem.setText(m_registry.getString("lockrevision.Label","Unlock Revision"));
			 }
			 
			 
			 int idocCol =smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
			 Object obj = smdlTable.getValueAtDataModel(iSelectedRow, idocCol );		
			
			 if(obj != null && obj.toString().trim().length()>0)
			 {				 
				final TCComponentItem docItem = (TCComponentItem)obj;
				 
				workOnDocItem.addActionListener(new ActionListener() 
				 {
					 public void actionPerformed(ActionEvent actionevent)
					 {						 
						 NOVOpenObjectUtils.invokeObjectApplication(tcsession, docItem);
					 }
				 });
				 
				int idocColumn =smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
				final TCComponentItem documentItem = (TCComponentItem) smdlTable.getValueAtDataModel(iSelectedRow, idocColumn );
				final int  idocRevColumn =smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_REV_REF);
				lockRevisionItem.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e)
					{
						
						// TODO Auto-generated method stub
						if(lockRevisionItem.getText()== m_registry.getString("lockrevision.Label") )	
						{
							Display.getDefault().asyncExec(new Runnable() {
								public void run()
								{
									
									Shell localShell = Display.getDefault().getActiveShell();
									
									try 
									{
										TCComponentItemRevision[] m_releasedItemRevisions= documentItem.getReleasedItemRevisions();
										if(m_releasedItemRevisions.length ==0)
										{
											MessageBox.post(m_registry.getString("CannotLockRevision.MSG"),m_registry.getString("CannotLockRevision.TITLE"), MessageBox.ERROR);
											
											return;

										}
										//TCDECREL-2450- For single release revision lock 
										else if(m_releasedItemRevisions.length ==1)
										{							
											int idocRevColumn =smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_REV_REF);
											smdlTable.setValueAt(m_releasedItemRevisions[0], iSelectedRow, idocRevColumn);
											smdlTable.repaint();
											
										}
										
										else
										{
											LockRevDialog lockRev = new LockRevDialog(localShell);
											lockRev.create();
											lockRev.populateTree(documentItem);
											int returnCode = lockRev.open();
											
											if (returnCode == 0) 
											{
												TCComponentItemRevision docRevision = lockRev.getSelectedComponent();
												int idocRevColumn =smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_REV_REF);
												smdlTable.setValueAt(docRevision, iSelectedRow, idocRevColumn);
												smdlTable.repaint();
											}
										}
										
										NOVSMDLTableHelper.save(smdlTable, iSelectedRow, idocRevColumn);
										
									} 
									catch (TCException exception) 
									{
										MessageBox.post(exception);
									}
								}
								
				
							});
							
						}
						else
						{
							int idocRevColumn =smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_REV_REF);
							smdlTable.setValueAt(null, iSelectedRow, idocRevColumn);
							smdlTable.repaint();
							try 
							{
								NOVSMDLTableHelper.save(smdlTable, iSelectedRow, idocRevColumn);
							} 
							catch (TCException exception1) 
							{								
								MessageBox.post(exception1);
							}
						}
							
					}
				});

			 }
			 else
			 {
				 workOnDocItem.setEnabled(false);
				 lockRevisionItem.setEnabled(false);
			 } 
		 }
		
     }
	
}