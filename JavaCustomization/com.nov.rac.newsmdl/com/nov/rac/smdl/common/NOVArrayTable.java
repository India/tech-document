/*================================================================================
                             Copyright (c) 2008 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVArrayTable
 Package Name: com.nov.iman.smdl.common.CreateDP
 ================================================================================
                             Modification Log
 ================================================================================
 Revision       Date       Author        Description
 1.0         2009/09/10    AparnaG     Initial Creation
 ================================================================================*/
package com.nov.rac.smdl.common;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

public class NOVArrayTable extends JPanel
{
	Registry reg = Registry.getRegistry(this);
	private boolean isDescRequired ;
	private JButton addRowButton;
	private JButton remRowButton;
	private JPanel buttonPanel;
	private JScrollPane m_scrollpane;
	
	public NOVArrayTable(boolean isDescReq, int iSize)
    {
		isDescRequired = isDescReq;
        numberTxt.setLineWrap(true);
        desctext.setLineWrap(true);
		if(isDescRequired)
        {
			//Nataraj : Commented to allow letters in number field
        	//numberTxt.setDocument(new IntTextDocument(30));
		}
        desctext.setLengthLimit(iSize);        
        descScPane = new JScrollPane(desctext);
        descScPane
                .setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		if(isDescRequired)
        {
			numScPane = new JScrollPane(numberTxt);
	        numScPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		}
		else
        {
			numberTxt.setLengthLimit(iSize);
			numScPane = new JScrollPane(numberTxt);
	        numScPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        }
        columnNames = new String[]
        { " " };
        cellObj = new Object[]
        { "" };
        DefaultTableModel tableModel = new DefaultTableModel(columnNames , 0);
        table = new JTable(tableModel) {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row , int column )
            {
                return true;
            }
        };
        buildNumberDescPanel();
        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
     
        
        /** Create and Set image icon on to add and remove button */
        createButtons();
        
        table.setTableHeader(null);
        table.setRowSelectionAllowed(true);
        m_scrollpane = new JScrollPane(table);
        m_scrollpane.setColumnHeaderView(null);
        

        setLayout(m_scrollpane);
        
    }


	private void setLayout(JScrollPane scrollpane)
	{
	    this.setLayout(new PropertyLayout(4 , 4 , 4 , 4 , 4 , 4));

	   this.add("1.1.left.top", numberDescPanel);
	   this.add("1.2.left.top",addRowButton );
	   this.add("2.1.left.top", scrollpane);
	   this.add("2.2.right.bottom", remRowButton);
           
	}

	
	
	@Override
	public void setPreferredSize(Dimension dimension) {
	
		super.setPreferredSize(dimension);
		
		int x= (int) dimension.getWidth();
		int y= (int) dimension.getHeight();
		
		if(isDescRequired)
        {
			numScPane.setPreferredSize(new Dimension(x/2 , y/3));
        	descScPane.setPreferredSize(new Dimension(x/2 , y/3));
            m_scrollpane.setPreferredSize(new Dimension(x , (int) (y/1.5)));
        }
        else
        {
        	numScPane.setPreferredSize(new Dimension(x , y/3));
            m_scrollpane.setPreferredSize(new Dimension(x , (int) (y/1.5)));
        }
		
	}


	private void createButtons()
	{
			ImageIcon addIcon = reg.getImageIcon("plus.ICON");
	        ImageIcon removeIcon = reg.getImageIcon ("minus.ICON");
	        addRowButton = new JButton(addIcon);
	        
	        addRowButton.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent arg0 )
	            {
	                if ( numberTxt.getText().equals("") )
	                {
	                    MessageBox.post("Please provide the number field value",
	                            "Warning", 1);
	                    return;
	                }
	                else
	                {
	                    String cellValueStr = null;
	                    if ( desctext.getText().equals("") )
	                        cellValueStr = numberTxt.getText();
	                    else
	                        cellValueStr = numberTxt.getText() + ":"
	                                + desctext.getText();
	                    Object[] cellObj1;
	                    if ( cellValueStr != null )
	                    {
	                        cellObj1 = new Object[]
	                        { cellValueStr };
	                        addRow(cellObj1);
	                        numberTxt.setText("");
	                        desctext.setText("");
	                    }
	                }
	            }
	        });
	        addRowButton.setPreferredSize(new Dimension(22 , 22));
	        
	        remRowButton = new JButton(removeIcon);
	       
	        remRowButton.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent arg0 )
	            {
	                int selectedrow = table.getSelectedRow();
	                if(selectedrow<0)
	                {
	                	 MessageBox.post("Please select row to remove",
		                            "Warning", 1);
		                    return;
	                }
	                ((DefaultTableModel) table.getModel()).removeRow(selectedrow);
	            }
	        });
	        remRowButton.setPreferredSize(new Dimension(22 , 22));
	      
	}
	
	
    /**
     * @desc This Method Returns table
     * @return JTable.
     */
    public JTable getTable()
    {
        return table;
    }

    /**
     * @desc This Method Adds row based on 1d or 2D
     * @return void.
     */
    private void addRow(Object[] cellObj )
    {
        for ( int index = 0; index < cellObj.length; ++index )
        {
        	((DefaultTableModel) table.getModel()).addRow( new Object[]{ cellObj[index] } );
        }
    }

    public String[] getArrayValues()
    {
        String[] strAttr = new String[table.getRowCount()];
        for ( int i = 0; i < strAttr.length; i++ )
        {
            strAttr[i] = new String();
            strAttr[i] = table.getValueAt(i, 0).toString();
        }
        return strAttr;
    }


    public void setArrayValues(String[] strArr )
    {
        addRow(strArr);
    }
    
    public void clearAllRows() 
    {
    	DefaultTableModel tabelModel=(DefaultTableModel) table.getModel(); 
    	
    	 while(tabelModel.getRowCount()>0) 
    	 {
    		 tabelModel.removeRow(0);
    	 }
	}


    public void setNumberLabel(String sLabelName)
    {
    	numberLabel.setText(sLabelName);
    }
    public void buildNumberDescPanel()
    {
        numberDescPanel = new JPanel(new PropertyLayout(4 , 4 , 4 , 4 , 4 , 4));
        
        descLabel.setText("Description:");
        
        // Add Number and description to numberDescPanel
        if(isDescRequired)
        {
            numberDescPanel.add("1.1.left.top", numberLabel);
            numberDescPanel.add("1.2.left.top", descLabel);
            numberDescPanel.add("2.1.left.top", numScPane);
            numberDescPanel.add("2.2.left.top", descScPane);
        }
        else
        {
            numberDescPanel.add("1.1.left.top", numberLabel);
            numberDescPanel.add("2.1.left.top", numScPane);
        }
    }

    class IntTextDocument extends PlainDocument
    {
        private int     max;
        public IntTextDocument(int maxLength)
        {
            max = maxLength;
        }

        public void insertString(int offs , String str , AttributeSet a )
                throws BadLocationException
        {
            if ( getLength() + str.length() > max )
            {
                java.awt.Toolkit.getDefaultToolkit().beep();
            }
            else
            {
                char[] source = str.toCharArray();
                char[] result = new char[source.length];
                int j = 0;
                for ( int i = 0; i < result.length; i++ )
                {
                    if ( Character.isDigit(source[i]) )
                    {
                        result[j++] = source[i];
                    }
                }
                super.insertString(offs, new String(result , 0 , j), a);
            }
        }
        private static final long serialVersionUID = 1000L;
    }

    private static final long serialVersionUID = 1L;
    String[]                  columnNames;
    Object[]                  cellObj;
    JTable                    table;
    DefaultCellEditor         integerEditor;
    DefaultCellEditor         floatEditor;
    private JLabel            numberLabel      = new JLabel();
    private JLabel            descLabel        = new JLabel();
    private iTextArea         numberTxt        = new iTextArea(15 , 2);
    private JScrollPane       numScPane        = null;
    private iTextArea         desctext         = new iTextArea(15 , 2);
    private JScrollPane       descScPane       = null;
    private JPanel            numberDescPanel  = null;
 }