package com.nov.rac.smdl.common.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.record.formula.functions.T;

public class DataProvider implements DataService
{
    private List<String> data;
   
    public DataProvider()
    {
        data = new ArrayList<String>();
    }
    
    public DataProvider(Collection<String> data)
    {
    	setData(data);
    }
    
    public Collection<String> getData()
    {
        return data;
    }

    public void setData(Collection<String> data)
    {
        this.data =  new ArrayList<String>(data);       
        
        Collections.sort(this.data, new Comparator<String>() {
			
			@Override
			public int compare(String o1, String o2) 
			{
				return o1.compareToIgnoreCase(o2);
			}
		});
    }
    
    
    /**
     * Gets the matching data.
     * 
     * @param string
     *            the string
     * @return the filtered data
     */
    public List<String> getMatches(String string)
    {
        Vector<String> v = new Vector<String>();      
        int index = Collections.binarySearch(data, string, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) 
			{				
				String lowerO1 = o1.toLowerCase();
				String lowerO2 = o2.toLowerCase();
				
				return (lowerO1.compareTo(lowerO2));	
			}
		});
        
        if( index < 0)        
        {
        	index = index + 1;
        }
        
        int startIndex = Math.abs(index);
        int maxIndex = data.size();
        for (int inx = startIndex; inx< maxIndex; inx++ )
        {
        	String stringValue = data.get(inx);
            if (stringValue.toLowerCase().startsWith(string.toLowerCase()))
            {
               v.add(stringValue);
               continue;
            }
            break;
        }
        return v;
    }
}

