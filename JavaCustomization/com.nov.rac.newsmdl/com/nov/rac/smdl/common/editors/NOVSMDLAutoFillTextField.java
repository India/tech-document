package com.nov.rac.smdl.common.editors;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * The Class NOVSMDLAutoFillTextField - This class autofills itself for matching string
 * from given objects.
 */
public class NOVSMDLAutoFillTextField extends JTextField
{
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The popcombobox. */
    private JComboBox popcombobox;
    
    /** The popupmenu. */
    private BasicComboPopup popupmenu;
    
    /** The data. */
    private DataService dataService;
    
    /** The key event. */
    private boolean keyEvent = false;
    
    /**
     * Inits the.
     * 
     * @param dataService
     *            the dataService
     */
    private void init(DataService dataService )
    {
        this.dataService = dataService;
        getDocument().addDocumentListener(new MyDocumentListener());
        
        this.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                requestFocus();
            }
        });
        
        this.addKeyListener(new KeyListener()
        {
            
            public void keyPressed(KeyEvent e)
            {
                keyEvent = true;
            }
            
            public void keyTyped(KeyEvent e)
            {
                keyEvent = true;
            }
            
            public void keyReleased(KeyEvent ke)
            {
                keyEvent = true;
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_DOWN)
                {
                    int index = popcombobox.getSelectedIndex();
                    if (!(popcombobox.getItemCount() == index + 1))
                        popcombobox.setSelectedIndex(popcombobox.getSelectedIndex() + 1);
                }
                if (keyCode == KeyEvent.VK_UP)
                {
                    if (!(popcombobox.getSelectedIndex() == 0))
                        popcombobox.setSelectedIndex(popcombobox.getSelectedIndex() - 1);
                }
                if (keyCode == KeyEvent.VK_ENTER || keyCode == KeyEvent.VK_TAB)
                {
                    if (popupmenu.isVisible())
                        setText();
                }
            }
        });
        
    }
    
    /**
     * Instantiates a new auto fill text field.
     * 
     * @param dataService
     *            the dataService
     */
    public NOVSMDLAutoFillTextField(DataService dataService )
    {
        super();
        init(dataService);
    }
    
    /**
     * Instantiates a new auto fill text field.
     * 
     * @param doc
     *            the doc
     * @param text
     *            the text
     * @param columns
     *            the columns
     * @param dataService
     *            the dataService
     */
    public NOVSMDLAutoFillTextField(Document doc, String text, int columns, DataService dataService )
    {
        super(doc, text, columns);
        init(dataService);
    }
    
    /**
     * Instantiates a new auto fill text field.
     * 
     * @param columns
     *            the columns
     * @param dataService
     *            the dataService
     */
    public NOVSMDLAutoFillTextField(int columns, DataService dataService )
    {
        super(columns);
        init(dataService);
    }
    
    /**
     * Instantiates a new auto fill text field.
     * 
     * @param text
     *            the text
     * @param columns
     *            the columns
     * @param dataService
     *            the dataService
     */
    public NOVSMDLAutoFillTextField(String text, int columns, DataService dataService )
    {
        super(text, columns);
        init(dataService);
    }
    
    /**
     * Instantiates a new auto fill text field.
     * 
     * @param text
     *            the text
     * @param dataService
     *            the dataService
     */
    public NOVSMDLAutoFillTextField(String text, DataService dataService )
    {
        super(text);
        init(dataService);
    }
    
    /**
     * The listener interface for receiving myDocument events. The class that is
     * interested in processing a myDocument event implements this interface,
     * and the object created with that class is registered with a component
     * using the component's <code>addMyDocumentListener<code> method. When
     * the myDocument event occurs, that object's appropriate
     * method is invoked.
     * 
     * @see MyDocumentEvent
     */
    private class MyDocumentListener implements DocumentListener
    {
        /*
         * (non-Javadoc)
         * @see
         * javax.swing.event.DocumentListener#insertUpdate(javax.swing.event
         * .DocumentEvent)
         */
        public void insertUpdate(DocumentEvent e)
        {
            updateLog(e, "inserted into");
        }
        
        /*
         * (non-Javadoc)
         * @see
         * javax.swing.event.DocumentListener#removeUpdate(javax.swing.event
         * .DocumentEvent)
         */
        public void removeUpdate(DocumentEvent e)
        {
            updateLog(e, "removed from");
        }
        
        /*
         * (non-Javadoc)
         * @see
         * javax.swing.event.DocumentListener#changedUpdate(javax.swing.event
         * .DocumentEvent)
         */
        public void changedUpdate(DocumentEvent e)
        {
            // Plain text components don't fire these events.
        }
        
        /**
         * Update log.
         * 
         * @param e
         *            the e
         * @param action
         *            the action
         */
        public void updateLog(DocumentEvent e, String action)
        {
            Document doc = (Document) e.getDocument();
            // openPopup(data);
            try
            {
                String string = doc.getText(0, doc.getLength());
                
                if (popupmenu != null)
                {
                    popupmenu.hide();
                }
                if (string == null || string.equals(""))
                    return;
                List<String> list = dataService.getMatches(string);
                openPopup(list);
            }
            catch (BadLocationException e1)
            {
                e1.printStackTrace();
            }
        }
    }
    
    /**
     * Sets the text.
     */
    private void setText()
    {
        setText(popcombobox.getSelectedItem().toString());
        if (popupmenu != null)
        {
            popupmenu.hide();
        }
    }
    
    
    /**
     * Open popup.
     * 
     * @param v
     *            the v
     */
    @SuppressWarnings("rawtypes")
    private void openPopup(List list)
    {
        popcombobox = new JComboBox(list.toArray());
        popupmenu = new BasicComboPopup(popcombobox);
        popcombobox.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent me)
            {
                if (!keyEvent)
                {
                    setText();
                    setFocusable(false);
                    setFocusable(true);
                    requestFocus(true);
                    popupmenu.hide();
                }
                keyEvent = false;
            }
        });
        
        if (list.size() < 5)
        {
            int height = list.size() * 18;
            popupmenu.setPreferredSize(new Dimension(160, height));
        }
        else
        {
            popupmenu.setPreferredSize(new Dimension(160, 100));
        }
        int x = 0;
        try
        {
            if ((this).getUI().modelToView((this), 0) != null)
            {
                x = (this).getUI().modelToView((this), 0).x;
                if ((this).isShowing())
                    popupmenu.show(this, x, this.getHeight());
            }
        }
        catch (BadLocationException e)
        {
            e.printStackTrace();
        }
    }
}
