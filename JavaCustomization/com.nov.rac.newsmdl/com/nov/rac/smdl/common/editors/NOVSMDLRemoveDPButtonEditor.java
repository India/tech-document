package com.nov.rac.smdl.common.editors;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.common.NOVSMDLOrderDPTable;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.views.SMDLExplorerView;
import com.nov.rac.smdl.views.SMDLHomeView;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectOperation;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.AdapterUtil;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLRemoveDPButtonEditor extends AbstractCellEditor implements
TableCellEditor, ActionListener
{
	private static final long serialVersionUID = 1L;

	private JButton m_tblButton;
	private NOVSMDLOrderDPTable m_srcTable ;
	private ImageIcon m_minusIcon = null;
	private Registry m_registry = null;
	private TCSession m_session = null;
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	TCComponent m_dpItem;
	TCComponent m_orderItem;

	public NOVSMDLRemoveDPButtonEditor(NOVSMDLOrderDPTable sourcetable) {

		m_srcTable = sourcetable;

		if (m_srcTable != null)
		{
			m_registry = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
			m_session=(TCSession)AIFUtility.getDefaultSession();

			try {
				m_minusIcon=m_registry.getImageIcon("minus.ICON");
			} catch ( Exception ie ) {
				System.out.println(ie);
			}

			m_tblButton = new JButton();
			m_tblButton.setMinimumSize(new Dimension(18 , 20));
			m_tblButton.setPreferredSize(new Dimension(18 , 20));
			m_tblButton.setMaximumSize(new Dimension(18 , 20));
			m_tblButton.setBackground(Color.WHITE);
			m_tblButton.addActionListener(this);
			m_tblButton.setIcon(m_minusIcon);
		}

	}

	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		return m_tblButton;
	}

	public Object getCellEditorValue() {
		return null;
	}

	public void actionPerformed(ActionEvent e) {
		final int selRow = m_srcTable.getSelectedRow();
		if(selRow >= 0) {
			if(((String)m_srcTable.getExtraData()).equalsIgnoreCase(m_registry.getString("RemDPFromOrdDlg.LABEL"))) {
				m_srcTable.removeRow(selRow); // Only from Order Creation UI
			} else {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						Shell shell = Display.getDefault().getActiveShell();
						
						MessageBox confirmMessageBox = new MessageBox( shell, SWT.APPLICATION_MODAL|SWT.YES| SWT.NO |SWT.CLOSE);

						confirmMessageBox.setText( m_registry.getString( "ConfirmMessageBox.Text" ) );
						confirmMessageBox.setMessage( m_registry.getString("RemoveDPConfirm.MSG") );

						int buttonID = confirmMessageBox.open();

						if(buttonID == SWT.YES)
						{
							try
							{
								removeTableRow(selRow); // Remove relation between Order and DP in database
								m_srcTable.removeRow(selRow);
	
//								IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//								final IWorkbenchPage page = iw.getActivePage();
//								IViewReference iVwRef[] = page.getViewReferences();
//	
//								if(iVwRef.length>0) {
//									for(int i=0;i<iVwRef.length;i++) {
//										String viewId = iVwRef[i].getId();
//										if(viewId.equalsIgnoreCase(SMDLExplorerView.ID) || viewId.equalsIgnoreCase(SMDLHomeView.ID)) {
//											IWorkbenchPart activePart = iVwRef[i].getPart(false);
//											PropertyChangeListener theListener = (PropertyChangeListener) AdapterUtil.getAdapter(activePart, PropertyChangeListener.class);
//											addPropertyChangeListener(theListener);
//										}
//									}
//								}
								
								PropertyChangeListener lastListener = NOVSMDLExplorerViewHelper.getLastListener();
								addPropertyChangeListener( lastListener );
								firePropertyChange(this, "RemoveSMDLObjectOperation", m_orderItem , m_dpItem);
								m_dpItem.fireComponentDeleteEvent();
								removePropertyChangeListener();
							}
							catch( TCException tcException )
							{
								com.teamcenter.rac.util.MessageBox.post( tcException );
							}						}
					}
				});
			}
		}
		fireEditingStopped();
	}

	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners ) {
			listener.propertyChange( changeEvent );
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		if(!m_listeners.contains(listener) ){
			m_listeners.add(listener);
		}
	}

	public void removePropertyChangeListener()
	{
		m_listeners.clear();
	}

	private void removeTableRow(int selRow) throws TCException 
	{
		DeleteObjectOperation delRelOrderDPOp = new DeleteObjectOperation(deleteRelationOperationInput(selRow));

		delRelOrderDPOp.executeOperation();
	}
	
	private DeleteObjectHelper deleteRelationOperationInput(int selRow)
	{

		int col = m_srcTable.getColumnIndex(m_registry.getString("DPID.LABEL"));

		DeleteObjectHelper transferObject= new DeleteObjectHelper(DeleteObjectHelper.OPERATION_DELETE_RELATION);

		TCComponentItemType dpItemType;

		try {
			dpItemType = (TCComponentItemType)m_session.getTypeService().getTypeComponent("DeliveredProduct");
			m_dpItem = dpItemType.find((String)m_srcTable.getValueAt(selRow, col));
			if(m_dpItem != null) {
				m_orderItem = ((NOV4ComponentDP) m_dpItem).getOrder();
			}
		} catch (TCException e) {
			e.printStackTrace();
		}

		transferObject.setPrimaryObject(m_orderItem);
		transferObject.setSecondaryObject(m_dpItem);
		transferObject.setRelationName("_OrderDeliveredProduct_");

		return transferObject;
	}
}