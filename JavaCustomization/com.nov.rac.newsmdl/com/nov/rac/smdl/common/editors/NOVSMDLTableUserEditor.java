/*================================================================================
                             Copyright (c) 2009 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVSMDLTableUserEditor
 Package Name: com.nov.iman.smdl.commands.CreateSmdl
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         17-Aug-09       Aparnag      Initial creation
 ================================================================================*/

package com.nov.rac.smdl.common.editors;

import java.awt.Component;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentUserType;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.UserList;


public class NOVSMDLTableUserEditor extends DefaultCellEditor
 {
    private static final long serialVersionUID = 1L;
   
    
       public NOVSMDLTableUserEditor()
    {
        super(new NOVSMDLAutoFillTextField(new MyNOVDocument(dataProvider), "", 20, dataProvider));
        try
        {
        	TCSession session= (TCSession)AIFUtility.getDefaultSession();
            userType = (TCComponentUserType) session.getTypeComponent("User");

        }
        catch ( Exception e )
        {
        }
        tf = (NOVSMDLAutoFillTextField) getComponent();
        tf.setEnabled(true);
    }

    public Component getTableCellEditorComponent(JTable table , Object value ,
            boolean arg2 , int row , int column )
    {
      
        UserList userList = null;

        try
        {
        	//NOVSMDLTable smdlTable=(NOVSMDLTable)table;
        	//int colForGroup=smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.GROUP_NAME);
        	
           // String groupvalue = table.getModel().getValueAt(row, colForGroup) == null ? ""
           //         : table.getModel().getValueAt(row, colForGroup).toString().trim();
           // if ( groupvalue.length() > 0 )
               // userList = userType.getUserList("*", groupvalue, "*");
            //else
                userList = userType.getUserList("*", "*", "*");
        }
        catch ( Exception e )
        {
            e.printStackTrace();

        }
        if ( userList != null )
        {
            String[] list;
            list = userList.getFormattedList();
            m_userAutoFillist = new Vector<String>();
            for ( int k = 0; k < list.length; k++ )
            {
            	m_userAutoFillist.add(list[k]);
            }
        }

        dataProvider.setData(m_userAutoFillist);
        return super.getTableCellEditorComponent(table,
                value.toString().trim(), arg2, row, column);
    }
    
    public boolean isCellEditable(EventObject anEvent )
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject anEvent )
    {
        return true;
    }
 
    public NOVSMDLAutoFillTextField getTf()
    {
        return tf;
    }
    @Override
    public boolean stopCellEditing() 
    {
    	boolean retValue = false;
    	Object cellValue = getCellEditorValue();	
    	int indx = m_userAutoFillist.indexOf(cellValue.toString());
    	if(indx != -1)
    	{
    		retValue=super.stopCellEditing();
    	}
    	return retValue;
    }
    
    public boolean stopCellEditingOnClosing() {

        boolean retValue = false;
        Object cellValue = getCellEditorValue();
        String cellText = cellValue.toString();
        if ("".equals(cellText)) 
        {
              retValue = super.stopCellEditing();
        }
        int indx = m_userAutoFillist.indexOf(cellText);
        if (indx != -1) 
        {
              retValue = super.stopCellEditing();
        }
        if (indx == -1)
        {
              tf.setText("");
              retValue = super.stopCellEditing();
        }

        return retValue;
  }

    private TCComponentUserType   userType;
    Vector<String> m_userAutoFillist = null;
    
    private static DataProvider dataProvider= new DataProvider();
    private NOVSMDLAutoFillTextField tf = null;
}