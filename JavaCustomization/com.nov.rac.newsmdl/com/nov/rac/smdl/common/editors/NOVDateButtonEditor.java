package com.nov.rac.smdl.common.editors;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.teamcenter.rac.util.DateButton;

public class NOVDateButtonEditor extends DateButton implements TableCellEditor
{
    private SimpleDateFormat m_formatter;
    public NOVDateButtonEditor()
    {
    	super();
        this.setBackground(Color.white);
        this.setBorderPainted(false);
        setFormatter();
      
        try
        {
            this.addPropertyChangeListener(new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent evt )
                {
                    // Special check for OK Button
                    if ( evt.getPropertyName().equals("text") )
                    {
                        fireEditingStopped();
                    }
                }
            });
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
    
    private void setFormatter()
    {
    	 m_formatter= new SimpleDateFormat();
         String pattern="dd-MMM-yyyy";
         m_formatter.applyPattern(pattern);
         this.setDisplayFormatter(m_formatter);
    }

    public Component getTableCellEditorComponent(JTable table , Object value ,
            boolean isSelected , int row , final int column )
    {
        m_irowIndex = row;
        m_icolIndex = column;
        NOVSMDLTable smdlTable = null;
        if(table != null && table instanceof NOVSMDLTable)
    	{
    		smdlTable = (NOVSMDLTable) table;
    	}
        if(value!=null)
        {
        	Date dateToset=null;
        	if ( value instanceof DateButton )
        	{
        		this.setDate(((DateButton) value).getDate());
        		this.setText(((DateButton) value).getText());
        	}
        	else if( value instanceof Date)
        	{
        		dateToset=(Date)value;	        		
        		this.setDate(dateToset);
        		this.setBorderPainted(false);
        	}
        	else if(value instanceof String)
        	{
        		this.setDate(value.toString());
        	}
        }
        if(smdlTable != null) //TCDECREL-2512
        {
        	int iContractDateCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOC_CONTRACT_DATE);
        	int iPalnnedDateCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOC_PLANNED_DATE);
        	if(column == iContractDateCol)
        	{
        		TableCellEditor cellEditor = smdlTable.getCellEditor(row, iPalnnedDateCol);
        		if(cellEditor != null)
        		{
        			cellEditor.cancelCellEditing();
        		}
        	}
        	else if (column == iPalnnedDateCol)
        	{
        		TableCellEditor cellEditor = smdlTable.getCellEditor(row, iContractDateCol);
        		if(cellEditor != null)
        		{
        			cellEditor.cancelCellEditing();
        		}
        	}
        }
        return this;
    }

    public Object getCellEditorValue()
    {
        postDown();
        return getDate();
    }

    public boolean isCellEditable(EventObject anEvent )
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject anEvent )
    {
        this.repaint();
        return true;
    }

    public boolean stopCellEditing()
    {
        return true;
    }

    public void cancelCellEditing()
    {
        fireEditingCanceled();
    }

    private void fireEditingCanceled()
    {
        CellEditorListener listener;
        Object[] listeners = listenerList.getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(m_changeEvent);
            }
        }
    }

    public void addCellEditorListener(CellEditorListener listener )
    {
        listenerList.add(CellEditorListener.class, listener);
    }

    public void removeCellEditorListener(CellEditorListener listener )
    {
        listenerList.remove(CellEditorListener.class, listener);
    }

    protected void fireEditingStopped()
    {
        this.repaint();
        postDown();
        CellEditorListener listener;
        Object[] listeners = listenerList.getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(m_changeEvent);
            }
        }
    }    
    protected ChangeEvent     m_changeEvent      = new ChangeEvent(this);
    int                       m_irowIndex        = -1;
    int                       m_icolIndex        = -1;
    private static final long serialVersionUID = -697898812006419074L;
}
