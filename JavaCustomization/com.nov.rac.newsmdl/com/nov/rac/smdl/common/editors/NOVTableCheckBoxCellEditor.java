package com.nov.rac.smdl.common.editors;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

public class NOVTableCheckBoxCellEditor extends JCheckBox implements TableCellEditor
{

	private static final long serialVersionUID = 1L;

	public NOVTableCheckBoxCellEditor(){
		super();
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, 
			int row, int column) {
		if (value == null) {
			return this;
		}

		if (column == 0) {
			this.setSelected(((Boolean) value).booleanValue());
		} else {
			this.setSelected(false);
		}

		return this;
	}

	public void addCellEditorListener(CellEditorListener celleditorlistener) {
		// TODO Auto-generated method stub
	}

	public void cancelCellEditing() {
		// TODO Auto-generated method stub
	}

	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isCellEditable(EventObject eventobject) {
		// TODO Auto-generated method stub
		return true;
	}

	public void removeCellEditorListener(CellEditorListener celleditorlistener) {
		// TODO Auto-generated method stub
		
	}

	public boolean shouldSelectCell(EventObject eventobject) {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean stopCellEditing() {
		// TODO Auto-generated method stub
		return false;
	}

}
