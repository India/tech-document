/*
 * ================================================================================
 *                   Copyright (c) 2009 National Oilwell Varco
 *                   Unpublished - All rights reserved 
 * ================================================================================
 * File Description: 
 * File Name : NOVSMDLAutofilEditorl.java 
 * Package Name:com.nov.iman.smdl.common
 * ================================================================================
 *                              Modification Log
 * ==================================================================================
 * Revision        Date             Author      Description 
 *    1.0       09/10/2009       aparnaG     File added to SVN
 * ================================================================================
 */
package com.nov.rac.smdl.common.editors;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.teamcenter.rac.util.iTextField;

/**
 * Class NOVSMDLAutofilEditorl
 * 
 * @author aparnaG
 */
public class NOVSMDLAutofilEditorl extends iTextField
{
    /**
     * Class NOVDocument
     *
     */
    class NOVDocument extends PlainDocument
    {
        public void replace(int start , int end , String str ,
                AttributeSet attrbset ) throws BadLocationException
        {
            super.remove(start, end);
            insertString(start, str, attrbset);
        }

        public void remove(int i , int j ) throws BadLocationException
        {
            if ( popupmenu != null )
            {
                popupmenu.hide();
            }
            super.remove(i, j);
        }

        public void insertString(int offset , String str ,
                AttributeSet attributeset ) throws BadLocationException
        {
            if ( popupmenu != null )
            {
                popupmenu.hide();
            }
            if ( str == null || str.equals("") ) return;
            Vector list = compareStr(getText(0, offset) + str);
            int len = (offset + str.length()) - 1;
            if ( list.size() == 0 )
            {
            	super.insertString(offset, "", attributeset);
            	list = data;
            }
            else
            {
            	String s2 = list.get(0).toString();
            	super.remove(0, getLength());
            	super.insertString(0, s2, attributeset);
            }
            
            if(bIsPopupRequired == true)
            {
            	addPopupMenu(list);
            }
            
            // requestFocusInWindow();
          /*  setSelectionStart(len + 1);
            setSelectionEnd(getLength());*/
            
            setSelectionStart(0);
            setSelectionEnd(0);
                
        }
        private static final long serialVersionUID = 1L;
    }

    public void addPopupMenu(Vector v )
    {
       
        popcombobox = new JComboBox(v.toArray());
        popupmenu = new BasicComboPopup(popcombobox);
        popcombobox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent me )
            {
                if ( !keyEvent )
                {
                    setText(popcombobox.getSelectedItem().toString());
                    setFocusable(false);
                    setFocusable(true);
                    requestFocus(true);
                    popupmenu.hide();
                }
                keyEvent = false;
            }
        });
        popupmenu.addMouseListener(new MouseAdapter() {
            public void mouseclicked(MouseEvent me )
            {
                setstr();
            }
        });
        if ( v.size() < 5 )
        {
            int height = v.size() * 18;
            popupmenu.setPreferredSize(new Dimension(160 , height));
        }
        else
        {
            popupmenu.setPreferredSize(new Dimension(160 , 100));
        }
        int x = 0;
        try
        {
            if ( ((JTextField) this).getUI()
                    .modelToView(((JTextField) this), 0) != null )
            {
                x = ((JTextField) this).getUI().modelToView(
                        ((JTextField) this), 0).x;
                if ( ((JTextField) this).isShowing() )
                    popupmenu.show(this, x, this.getHeight());
            }
        }
        catch ( BadLocationException e )
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Constructor: NOVSMDLAutofilEditorl
     * @param list
     */
    public NOVSMDLAutofilEditorl(Vector list, boolean bIsPopUpReq)
    {
        super(25);
        bIsPopupRequired = bIsPopUpReq;
        setDocument(new NOVDocument());
      
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e )
            {
            	bIsPopupRequired = true;
                int keyCode = e.getKeyCode();
                if ( keyCode == KeyEvent.VK_TAB )
                {
                    System.out.println("Tab pressed>");
                }
            }

            public void keyTyped(KeyEvent e )
            {
            	bIsPopupRequired = true;
                int keyCode = e.getKeyCode();
                if ( keyCode == KeyEvent.VK_TAB )
                {
                    System.out.println("Tab typed>");
                }
            }

            public void keyReleased(KeyEvent ke )
            {
            	bIsPopupRequired = true;
                int keyCode = ke.getKeyCode();
                if ( keyCode == KeyEvent.VK_DOWN )
                {
                    keyEvent = true;
                    int index = popcombobox.getSelectedIndex();
                    if ( !(popcombobox.getItemCount() == index + 1) )
                        popcombobox.setSelectedIndex(popcombobox
                                .getSelectedIndex() + 1);
                }
                if ( keyCode == KeyEvent.VK_UP )
                {
                    keyEvent = true;
                    if ( !(popcombobox.getSelectedIndex() == 0) )
                        popcombobox.setSelectedIndex(popcombobox
                                .getSelectedIndex() - 1);
                }
                if ( keyCode == KeyEvent.VK_ENTER )
                {
                    keyEvent = true;
                    if ( popupmenu.isVisible() )
                        setText(popcombobox.getSelectedItem().toString());
                    if ( popupmenu != null )
                    {
                        popupmenu.hide();
                    }
                }
                if ( keyCode == KeyEvent.VK_TAB )
                {
                    keyEvent = true;
                    if ( popupmenu != null )
                    {
                        popupmenu.hide();
                    }
                }
            }
        });
        addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e )
            {
                if ( popupmenu != null )
                {
                    popupmenu.hide();
                }
            }
        });
        data = list;
    }

    public void setstr()
    {
        setText(popcombobox.getSelectedItem().toString());
        if ( popupmenu != null )
        {
            popupmenu.hide();
        }
    }

    private Vector compareStr(String str )
    {
        Vector v = new Vector();
        for ( int i = 0; i < data.size(); i++ )
        {
            String s1 = data.get(i).toString();
            if ( s1 != null )
            {
                if ( s1.toLowerCase().startsWith(str.toLowerCase()) )
                    v.add(s1);
            }
        }
        Comparator comparator = Collections.reverseOrder();
        Collections.sort(v, comparator);
        return v;
    }

    public void resetData(Vector list )
    {
        data = list;
    }
    
    private static final long serialVersionUID = 1L;
    private Vector           data;
    public BasicComboPopup popupmenu   = null;
    private JComboBox      popcombobox = null;
    boolean                keyEvent    = false;
    private boolean  bIsPopupRequired  = true; 
}

