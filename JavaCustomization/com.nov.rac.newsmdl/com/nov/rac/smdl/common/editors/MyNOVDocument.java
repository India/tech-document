package com.nov.rac.smdl.common.editors;

import java.util.List;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class MyNOVDocument extends PlainDocument
{
    /** The data. */
    private DataService data;
    
    public MyNOVDocument(DataService data)
    {
        super();
        this.data = data;
    }
    
    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
        String string = getText(0, getLength()) + str;
        List<String> list = data.getMatches(string);
        if (!list.isEmpty())
            super.insertString(offs, str, a);
    }
}

