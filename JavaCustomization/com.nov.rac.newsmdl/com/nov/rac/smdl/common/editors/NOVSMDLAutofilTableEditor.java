/*================================================================================
                             Copyright (c) 2009 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVSMDLAutofilTableEditor
 Package Name: com.nov.iman.smdl.commands.CreateSmdl
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         17-Aug-09       Aparnag      Initial creation
 ================================================================================*/
package com.nov.rac.smdl.common.editors;

import java.awt.Component;
import java.util.Collections;
import java.util.EventObject;
import java.util.Set;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;

import com.nov.rac.smdl.utilities.NOVGroupDataHelper;



public class NOVSMDLAutofilTableEditor extends DefaultCellEditor
{

    private static final long serialVersionUID = 1L;
    private static Vector<String> m_groupList = new Vector<String>();
  
    public NOVSMDLAutofilTableEditor()
    {
        super(new NOVSMDLAutofilEditorl(getGroupList(),true));
        m_textField = (NOVSMDLAutofilEditorl) getComponent();
        m_textField.setEnabled(true);
    }
    
    private static Vector<String> getGroupList()
    {
    	    	
    	Set<String> groupsSet = NOVGroupDataHelper.getGroupData().keySet();
    	String[] sGroupData = (String[]) groupsSet.toArray(new String[groupsSet.size()]);
    	Collections.addAll(m_groupList, sGroupData);
    	
    	return m_groupList;
    }
    
	public Component getTableCellEditorComponent(JTable arg0 , Object arg1 ,
            boolean arg2 , int arg3 , int arg4 )
    {
       
        String value = arg1.toString().trim();
        return super.getTableCellEditorComponent(arg0, value, arg2, arg3, arg4);
    }

    
    
    public NOVSMDLAutofilEditorl getTextField() {
		return m_textField;
	}

	
	public boolean isCellEditable(EventObject anEvent )
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject anEvent )
    {
        return true;
    }
    @Override
    public boolean stopCellEditing() 
    {
    	Object sObj = getCellEditorValue();
    	int indx = m_groupList.indexOf(sObj.toString());
    	if(indx == -1)
    	{
    		m_textField.setText("");
    	}
    	return super.stopCellEditing();
    }
    
    private NOVSMDLAutofilEditorl   m_textField         = null;
  
}