package com.nov.rac.smdl.common.editors;

import java.util.List;

public interface DataService 
{
	 List<String> getMatches(String string);
}
