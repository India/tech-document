package com.nov.rac.smdl.common;

import java.util.Hashtable;
import java.util.Vector;

import javax.swing.event.TableModelListener;

import com.teamcenter.rac.aif.common.AIFIdentifier;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTableModel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;

public class NOVSMDLTableModel extends TCTableModel
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector<TableModelListener> m_ignoreListeners = new Vector<TableModelListener>();
	
	public NOVSMDLTableModel()
    {
        super();
    }

    public NOVSMDLTableModel(String[] data)
    {
        super(data);
    }

    public NOVSMDLTableModel(String as[][])
	{
	   super(as);	    
	}
    public NOVSMDLTableLine createLine(Object obj)
	{
    	NOVSMDLTableLine aiftableline = null;
		Object [] strObj = null;

		if(obj instanceof Object[])
		{
			strObj = ((Object [])obj);	
			if(strObj != null)
			{
				int i = strObj.length;
				int j = getColumnCount();
				Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
				for(int k = 0; k < j && k < i; k++)
				{
					if(strObj[k] != null)
					{
						hashtable.put(getColumnIdentifier(k), strObj[k]);
					}
				}       
				aiftableline = new NOVSMDLTableLine(hashtable);
			}
		}
		else 
		{
			aiftableline = (NOVSMDLTableLine) super.createLine(obj);
		}
		return aiftableline;
	} 

    protected NOVSMDLTableLine[] createLines(Object obj)
    {
    	NOVSMDLTableLine aaiftableline[] = null;

    	if( obj instanceof Object [][] )
        {
    		Object [][] objArrArr = (Object [][])obj;    		
            int i = objArrArr.length;
            aaiftableline = new NOVSMDLTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	NOVSMDLTableLine newLine = createLine( objArrArr[j] );
                aaiftableline[j] = newLine;
            }
        } 
    	else
    	{
    		aaiftableline = (NOVSMDLTableLine[]) super.createLines(obj);
    	}    	
    	return aaiftableline;
    }
    @Override
    public void sortByColumns(AIFIdentifier[] smdlTableIdentifiers) 
    {
    	int iListenerCount = m_ignoreListeners.size();
    	
    	// Ignore these listeners while sorting.
    	for(int indx=0;indx<iListenerCount;indx++)
    	{
    		removeTableModelListener(m_ignoreListeners.get(indx));
    	}
    	
    	//super.sortByColumns(aaifidentifier);
    	
    	localSortByColumns(smdlTableIdentifiers);
    	    	
    	
    	//Add listeners back once sorting is done.
    	for(int indx=0;indx<iListenerCount;indx++)
    	{    		
    		addTableModelListener(m_ignoreListeners.get(indx));
    	}		
    }
    
    public void ignoreWhileSorting(TableModelListener modelListener)
    {
    	m_ignoreListeners.add(modelListener);
    }
    
    public boolean isTCComponentProperty(String sColName)
    {
    	AIFTableLine smdlTableline = null;
        Object obj = null;
        //int iTableLineCount = componentData.size();
        int iTableLineCount = getRowCount(); //TC10.1 Upgrade
        
        for(int indx=0;indx<iTableLineCount;indx++)
        {
        	//smdlTableline = (AIFTableLine)componentData.elementAt(indx);
        	smdlTableline = getRowLine(indx);//TC10.1 Upgrade
            obj = smdlTableline.getProperty(sColName);
            if(obj != null  && (obj instanceof TCComponent))
            {
            	return true;
            }
        }    	
        return false;
    }
    private void localSortByColumns(AIFIdentifier[] smdlTableIdentifiers)
    {
    	//comparingIdentifiers = smdlTableIdentifiers;//TC10.1 Upgrade
        if(getRowCount() <= 1)
        {
            return;
        }
        if(smdlTableIdentifiers != null)
        {
            AIFIdentifier localIdentifiers[] = smdlTableIdentifiers;
            int j = localIdentifiers.length;
            for(int l = 0; l < j; l++)
            {
                AIFIdentifier tableIdentifier = localIdentifiers[l];
                if(tableIdentifier.getPropertyType() == -1)
                {                	                    
                	boolean bIsTCComponentProp = isTCComponentProperty((String)tableIdentifier.getColumnId());
                    tableIdentifier.setPropertyType(bIsTCComponentProp ? TCProperty.PROP_typed_reference : TCProperty.PROP_string);
                }
                if(tableIdentifier.getPropertyType() == 2)
                {
                    cacheProperties(new AIFIdentifier[] {tableIdentifier });
                }
            }

        }
        //int i = componentData != null ? componentData.size() : 0;
        int i = getRowCount();//TC10.1 Upgrade
        for(int k = 0; k < i; k++)
        {
            AIFTableLine smdlTableline = getRowLine(k);
            if(smdlTableline != null)
            {
            	//smdlTableline.setComparingProperties(comparingIdentifiers);
            	smdlTableline.setComparingProperties(smdlTableIdentifiers);//TC10.1 Upgrade
            }
        }        
        sort();
    }
    
  

}