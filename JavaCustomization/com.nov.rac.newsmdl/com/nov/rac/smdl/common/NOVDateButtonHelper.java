package com.nov.rac.smdl.common;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.teamcenter.rac.util.DateButton;

public class NOVDateButtonHelper
{
	public static void addDateButtonListener(final DateButton dateButton)
	{
		dateButton.addPropertyChangeListener(new PropertyChangeListener() 
        {
        	public void propertyChange(PropertyChangeEvent evt )
        	{
        		if ( evt.getPropertyName().equals("text") )
        		{        			
        			dateButton.postDown();
        		}
        	}
        });
	}
	
}