package com.nov.rac.smdl.common;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.teamcenter.rac.util.Painter;
public class NOVCodeComboAutoComplete extends JComboBox
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector<String>  m_vItems = new Vector<String>();
	private JTextField 		m_iText;
	private boolean         m_required         = false;
	private int 			MAX_ROWCOUNT = 5;
	public NOVCodeComboAutoComplete(Vector<String> vItems)
	{
		super(vItems);
		
		setEditable(true);
		setRequired(true);
		
		if (getItemCount() > MAX_ROWCOUNT)
		{
			setMaximumRowCount(MAX_ROWCOUNT);
		}
		else
		{
			setMaximumRowCount(getItemCount());
		}
		
		m_vItems.addAll(vItems);
		
		putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
				
		if(getEditor() != null)
		{
			m_iText = (JTextField)getEditor().getEditorComponent();
		}
		
		if(m_iText != null)
		{
			m_iText.setText("");
			m_iText.addKeyListener(new KeyAdapter()
			{
				public void keyReleased(KeyEvent e)
				{
					String key = Character.toString(e.getKeyChar());
					int keyCode = e.getKeyCode();
					if (keyCode == KeyEvent.VK_ENTER)
					{
						setSelectedItem(getSelectedItem());
					}
					else if(keyCode == KeyEvent.VK_DELETE || keyCode == KeyEvent.VK_BACK_SPACE)
					{
						if( m_iText.getText().trim().isEmpty() )
						{
							String sVal = "";
							int iSelectionEnd = m_vItems.get(0).length();
							dispComboBoxPopup( m_vItems, sVal, 0, iSelectionEnd );
						}
						
						if (isPopupVisible())
						{
							hidePopup();
						}
					}
					else if (  (e.getKeyChar() >= 'A' && e.getKeyChar() <= 'Z')
							|| (e.getKeyChar() >= 'a' && e.getKeyChar() <= 'z') 
							|| (e.getKeyChar() >= '0' && e.getKeyChar() <= '9')
							)
					{
						String toCompare = "";
						if (null != m_iText.getText()&& m_iText.getText().length() > 1)
						{
							toCompare = m_iText.getText().toString();
						}
						else
						{
							toCompare = key;
						}
						Vector<String> vItems = getItemsToRefillCombo(toCompare);
						int iSelectionStart = toCompare.length();
						int iSize = vItems.size();
						if(iSize > 0)
						{
							int i;
							for (i = 0; i < iSize; i++)
							{
								String sVal = vItems.get(i);
								if (sVal.toLowerCase().startsWith(toCompare.toLowerCase()))
								{
									int iSelectionEnd = sVal.length();									
									sVal = setStr(sVal);									
									dispComboBoxPopup(vItems,sVal,iSelectionStart,iSelectionEnd);									
									break;
								}
								else
								{
									m_iText.setText("");
								}
							}
						}
						else
						{
							String sVal = "";
							int iSelectionEnd = m_vItems.get(0).length();
							dispComboBoxPopup(m_vItems,sVal,iSelectionStart,iSelectionEnd);
						}
					}
				}
			});
		}
	}
	public Vector<String> getItemsToRefillCombo(String toCompare)
	{
		Vector<String> list = new Vector<String>();
		for (int i = 0; i < m_vItems.size(); i++) 
		{
			if (m_vItems.get(i).toLowerCase().startsWith(
					toCompare.toLowerCase())) 
			{
				list.add(m_vItems.get(i));
			}
		}
		return list;
	}
	@Override
	public void setSelectedItem(Object obj) 
	{
		// TODO Auto-generated method stub
		if(obj != null)
		{
			String sValue = setStr((String) obj);
			super.setSelectedItem(sValue);
		}
		else
		{
			super.setSelectedItem(obj);
		}
	}
	private void dispComboBoxPopup(Vector<String> vItems, String sValue,int iSelectionStart, int iSelectionEnd)
	{
		int i;
		this.removeAllItems();
		int iLength = vItems.size();
		for (i = 0; i < iLength; i++)
		{
			this.addItem(vItems.get(i));
		}
		m_iText.setText(sValue);
		m_iText.setSelectedTextColor(Color.WHITE);
		m_iText.setSelectionColor(Color.blue);
		m_iText.setSelectionStart(iSelectionStart);
		m_iText.setSelectionEnd(iSelectionEnd);
		
		if (iLength < MAX_ROWCOUNT)
		{
			this.setMaximumRowCount(iLength);
		}
		else
		{
			this.setMaximumRowCount(MAX_ROWCOUNT);
		}
		showPopup();
	}
	private String setStr(String sCode)
	{
		String sValue = sCode;
		if(sValue.indexOf("=")!=-1)
		{
			sValue = sValue.substring(0,sValue.indexOf("="));
		}
		return sValue;
	}
	public void setRequired(boolean required)
	{
		this.m_required=required;
	}
	public boolean isRequired()
	{
		return this.m_required;
	}

	public void paint(Graphics g) 
	{
		// TODO Auto-generated method stub
		super.paint(g);
		if(m_required)
			Painter.paintIsRequired(this, g);
	}
}
