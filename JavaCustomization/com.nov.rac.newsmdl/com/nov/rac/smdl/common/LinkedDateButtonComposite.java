package com.nov.rac.smdl.common;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.SWTUIUtilities;

public class LinkedDateButtonComposite extends NOVDateButtonComposite
{

	protected DateButton m_LinkedButton= null;
	protected NOVIntTextField m_LinkedText=null;
	protected Text				m_swtText = null;
	protected int m_intMonths = 0;
	
	public LinkedDateButtonComposite(Composite parent, int style)
	{
	     super(parent);
	     m_dateButton = new CustomDateButton();
	     
	   //TCDECREL-2447 data selection dialogs
	     NOVDateButtonHelper.addDateButtonListener(m_dateButton);
	     
	     SWTUIUtilities.embed( this, m_dateButton, false);
	     
	     initDateControl();
	     
	}

	public void linkDateButton(DateButton dateButton){
		m_LinkedButton=dateButton;
	}
	

	
	public void linkTextComponent(Text text)
	{
		
		m_swtText = text;
		
		m_swtText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent event) 
			{
				
				Text component = (Text) event.getSource();
				
					if(component.getText().length() > 0)
					{	
						m_intMonths = Integer.parseInt(component.getText());
					}
					else
					{
						m_intMonths = 0;
					}
					Date date = getDate();
					updateLinkedComponent(date);
			}
		});
	}

	class CustomDateButton extends DateButton
	{

		@Override
		public void setText(String s) {
			
			super.setText(s);
			
			Date date=getDate();
			updateLinkedComponent(date);
		}
		
	}
	
	
	private void updateLinkedComponent(Date date)
	{
		//Date date = getDate();
		if (null != m_LinkedButton && null != m_swtText) {
			if (date != null) 
			{
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);

				if (m_intMonths > 0) {

					cal.add(Calendar.MONTH, m_intMonths);
				}
				m_LinkedButton.setDate(cal.getTime());

			} else 
			{
				m_LinkedButton.setDate("");
			}
		}
	}

}
