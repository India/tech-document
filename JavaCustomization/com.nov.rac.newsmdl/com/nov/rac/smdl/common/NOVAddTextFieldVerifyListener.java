package com.nov.rac.smdl.common;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;

public class NOVAddTextFieldVerifyListener implements VerifyListener 
{
	public NOVAddTextFieldVerifyListener()
	{
	}
	public void verifyText(VerifyEvent event) 
	{
		event.text = event.text.toUpperCase();
	}
}
