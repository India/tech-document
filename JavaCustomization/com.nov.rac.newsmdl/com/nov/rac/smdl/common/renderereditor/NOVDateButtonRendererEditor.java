package com.nov.rac.smdl.common.renderereditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.util.DateButton;

public class NOVDateButtonRendererEditor implements TableCellEditor,  TableCellRenderer, IStrikeThrough
{
	
	private PrivateDateButton m_dateButton ;	
	private SimpleDateFormat m_formatter;
	int                       irowIndex        = -1;
	int                       icolIndex        = -1;
	private static final long serialVersionUID = -697898812006419074L;
	private boolean 			m_bIsStrikeThrough = false;
	private boolean             m_bCodeAdded       = false;
	private NOVCreateSMDLTable m_smdlTable;
	private boolean             m_bRowUpdated      = false;
    public NOVDateButtonRendererEditor()
    {
    	m_dateButton= new PrivateDateButton();
    	m_dateButton.setBackground(Color.white);
    	m_dateButton.setBorderPainted(false);
        setFormatter();
        try
        {
        	m_dateButton.addPropertyChangeListener(new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent evt )
                {
                    // Special check for OK Button
                    if ( evt.getPropertyName().equals("text") )
                        fireEditingStopped();
                }
            });
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }  
    public Component getTableCellEditorComponent(JTable table , Object value ,
            boolean isSelected , int row , int column )
    {
        irowIndex = row;
        icolIndex = column;
        if(value!=null)
        {
        	Date dateToset=null;
	        if ( value instanceof DateButton )
	        {
	        	m_dateButton.setDate(((DateButton) value).getDate());
	        	m_dateButton.setText(((DateButton) value).getText());
	        }
	        else if( value instanceof Date)
	        {
	        		dateToset=(Date)value;
	        	        	      		
	        		m_dateButton.setDate(dateToset);
	                m_dateButton.setBorderPainted(false);
	        }
	        else if(value instanceof String)
	        {
	        	if(((String) value).trim().length()>0)
	        	{
		           //	dateToset=new Date(value.toString());
	        		m_dateButton.setDate(value.toString());
	        	}
	        }
        }  
        return m_dateButton;
    }
    
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
    	 if ( value != null )
    	 {
	        if ( value instanceof DateButton )
	        {
	                if ( value instanceof Component )
	                {
	                	m_dateButton.setDate(((DateButton) value).getDate());
	                	m_dateButton.setText(((DateButton) value).getText());
	                }
	                else
	                {
	                	m_dateButton.setDate(((DateButton) value).getDate());
	                    return m_dateButton;
	                }
	                m_dateButton.setBorderPainted(false);
	           
	        }
	        else if( value instanceof Date)
	        {
	        	m_dateButton.setDate((Date)value);
	        	m_dateButton.setBorderPainted(false);
	        }
	        else if( value instanceof String)
	        {
	        	m_dateButton.setDate(value.toString());
	        	m_dateButton.setBorderPainted(false);
	        }
    	 }
    	 if(table instanceof NOVCreateSMDLTable)    
    	 {
    		 m_smdlTable = (NOVCreateSMDLTable)table;
    		 AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		 NOVCreateSMDLTableLine smdlTableLine = null;
    		 if(tableLine instanceof NOVCreateSMDLTableLine)
    		 {
    			 smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			 m_bIsStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			 m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
     			 m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		 }
    	 }
    	 if (m_bCodeAdded)
    	 {
    		 m_dateButton.setBackground(new Color(170, 255, 170));
    	 }
    	 else if(m_bRowUpdated)
    	 {
    		 m_dateButton.setBackground(Color.yellow);
    	 }
		return m_dateButton;  
    }
    private void setFormatter()
    {
    	 m_formatter= new SimpleDateFormat();
         String pattern="dd-MMM-yyyy";
         m_formatter.applyPattern(pattern);
         m_dateButton.setDisplayFormatter(m_formatter);
    }

    public Object getCellEditorValue()
    {
    	m_dateButton.postDown();
        return m_dateButton.getDate();
    }

    public boolean isCellEditable(EventObject anEvent )
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject anEvent )
    {
    	m_dateButton.repaint();
        return true;
    }

    public boolean stopCellEditing()
    {
    	fireEditingStopped();
        return true;
    }

    public void cancelCellEditing()
    {
        fireEditingCanceled();
    }

    private void fireEditingCanceled()
    {
        CellEditorListener listener;
       
        Object[] listeners = m_dateButton.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
            	if (m_dateButton.changeEvent == null)
            		m_dateButton.changeEvent = new ChangeEvent(m_dateButton);
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(m_dateButton.changeEvent);
            }
        }
    }

    public void addCellEditorListener(CellEditorListener listener )
    {
    	m_dateButton.getEventListenerList().add(CellEditorListener.class, listener);
    }

    public void removeCellEditorListener(CellEditorListener listener )
    {
    	m_dateButton.getEventListenerList().remove(CellEditorListener.class, listener);
    }
    public CellEditorListener[] getCellEditorListeners() 
    {
        return (CellEditorListener[])m_dateButton.getEventListenerList().getListeners(CellEditorListener.class);
    }

    protected void fireEditingStopped()
    {
    	m_dateButton.repaint();
    	m_dateButton.postDown();
        CellEditorListener listener;
        Object[] listeners = m_dateButton.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
            	if (m_dateButton.changeEvent == null)
            		m_dateButton.changeEvent = new ChangeEvent(m_dateButton);
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(m_dateButton.changeEvent);
            }
        }
    }
       
    private class PrivateDateButton extends DateButton
    {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		protected ChangeEvent changeEvent = null;
		public PrivateDateButton()
		{
			super();
			//this.setMandatory(true);
		}
		public EventListenerList getEventListenerList()
    	{
    		return listenerList;
    	}
    	public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bIsStrikeThrough)
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
    }
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bIsStrikeThrough = bIsStrikeThrough;
	};    
}

