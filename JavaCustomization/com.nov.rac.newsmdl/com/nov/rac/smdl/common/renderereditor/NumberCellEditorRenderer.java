package com.nov.rac.smdl.common.renderereditor;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.util.iTextField;

public class NumberCellEditorRenderer extends DefaultCellEditor implements TableCellRenderer
{
    private static final long serialVersionUID = 1L;
    protected iTextField m_iTextField;
    
    public NumberCellEditorRenderer()
    {
    	 super(new iTextField());
         m_iTextField = (iTextField) this.getComponent();
         m_iTextField.addKeyListener(new NOVNOWKeyListener());
    }
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus ,
            int rowIndex , int vColIndex )
    {
        try
        {
        	m_iTextField.setText(value.toString().trim());
        
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return m_iTextField;
    }
    
    
    public Component getTableCellEditorComponent(JTable table ,
            Object value , boolean isSelected , int row , int column )
    {
        	m_iTextField.setText(value.toString().trim());
       
        return m_iTextField;
    }

    public Object getCellEditorValue()
    {
        String no = m_iTextField.getText();
        try 
        {
        	int number = Integer.parseInt(no);
    		
    		return number;
        }
        catch(NumberFormatException e)
        {
        	return 0;
        }
		
    }
    
    
    public class NOVNOWKeyListener implements KeyListener
    {
        public void keyPressed(KeyEvent e )
        {
            novWeeksfieldValidation(e);
        }

        public void keyReleased(KeyEvent e )
        {
          
        }

        public void keyTyped(KeyEvent e )
        {
           novWeeksfieldValidation(e);
        }

        private void novWeeksfieldValidation(KeyEvent e )
        {
            char c = e.getKeyChar();
            
            if ( !(Character.isDigit(c)) && (c!=KeyEvent.VK_ENTER)
                && ((c != KeyEvent.VK_BACK_SPACE) && (c != KeyEvent.VK_DELETE))) 
            {
                e.consume();
            }
        }
    }
    
}
