package com.nov.rac.smdl.common.renderereditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableModel;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLCodeCellRendererEditor implements TableCellRenderer,ActionListener, TableCellEditor, IStrikeThrough
{
    private static final long serialVersionUID = 1L;
    private boolean                 m_bRemovedCode     = false;
    private JLabel                  m_Text;
    private NovCodeColPanel 		m_Panel;
    private JButton                 m_removeCodeBtn;
    private ImageIcon               m_minusIcon        = null;
    private Color                   m_alternateBackground = Color.white;   
    private Registry 				m_registry = null;
    private NOVCreateSMDLTable 		m_smdlTable;
    protected ChangeEvent     		changeEvent = new ChangeEvent(this);
    private boolean 				m_bCodeStrikeThrough = false;
    private boolean                 m_bCodeAdded       = false;
    private boolean                 m_bRowUpdated      = false;
    
    public NOVSMDLCodeCellRendererEditor()
    {
    	super();
    	m_registry = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
    	m_alternateBackground = new Color(191,214,248);
    	m_Panel = new NovCodeColPanel();
    	m_Panel.setLayout(new BoxLayout(m_Panel , BoxLayout.LINE_AXIS));
    	m_Text = new JLabel();
    	m_Text.setMinimumSize(new Dimension(80 , 20));
        try
        {
            m_minusIcon =m_registry.getImageIcon("minus.ICON");        	
        }
        catch ( Exception ie )
        {
            System.out.println(ie);
        }
        m_removeCodeBtn = new JButton(m_minusIcon);
                
        m_removeCodeBtn.setMinimumSize(new Dimension(18 , 20));
        m_removeCodeBtn.setPreferredSize(new Dimension(18 , 20));
        m_removeCodeBtn.setMaximumSize(new Dimension(18 , 20));  
        m_removeCodeBtn.setBackground(Color.WHITE);
        
        m_removeCodeBtn.setActionCommand("RemoveCode");
        m_removeCodeBtn.addActionListener(this);
        
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_removeCodeBtn);
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_Text);        
    }
    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
    	if ( comp != null )
    	{
    		ImageIcon compIcon;
    		String strCompType = comp.getType();
    		compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
    		return compIcon;
    	}
    	else
    		return null;
    }
    protected void setValueIcon(Object arg0 , Icon arg1 )
	{
    	if ( arg0 != null )
	    {
	    	m_Text.setText(arg0.toString());
	    	m_Text.setIcon(null);
	    }
	}
    protected void initiateIcons() 
    {
		// TODO Auto-generated method stub		
	}
    public Component getTableCellRendererComponent(JTable table , Object value ,
            boolean isSelected , boolean hasFocus , int row , int column )
    {    	
    	if(table instanceof NOVCreateSMDLTable)
    	{
    		m_smdlTable = (NOVCreateSMDLTable)table;    		
    		AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			m_bCodeStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
    			m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		}
    	}    	
        if ( isSelected )
        {
        	m_Panel.setForeground(table.getSelectionForeground());
        	m_Panel.setBackground(table.getSelectionBackground());
        }
        else
        {
        	m_Panel.setForeground(table.getForeground());
        	m_Panel.setBackground(row % 2 != 1 ? table.getBackground() : m_alternateBackground);
        }
        if ( hasFocus )
        {
        	m_Panel.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        	if ( table.isCellEditable(row, column) )
        	{
        		m_Panel.setForeground(UIManager.getColor("Table.focusCellForeground"));
        		m_Panel.setBackground(UIManager.getColor("Table.focusCellBackground"));
        	}
        }
        else
        {
        	m_Panel.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
        }   
        if (m_bCodeAdded)
		{
			m_Panel.setBackground(new Color(170, 255, 170));
		}
        else if(m_bRowUpdated)
        {
        	m_Panel.setBackground(Color.yellow);
        }
        String s = (String)value;
        m_Text.setText(s != null ? s : "");
        return m_Panel;
    }
  	@Override
	public Component getTableCellEditorComponent(JTable jtable, Object obj,
			boolean flag, int i, int j) 
  	{	 
  		m_Text.setText((String) obj);
  		m_Panel.setForeground(jtable.getSelectionForeground());
  		m_Panel.setBackground(jtable.getSelectionBackground());        
        return m_Panel;
	}
  	public boolean isCellEditable(EventObject anEvent )
    {
  		return true;
    }
  	public Object getCellEditorValue()
    {       
  		if(m_smdlTable != null)
  		{
  			int iCodeCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.SMDL_CODE_NUMBER);
  			Object codeValue = m_smdlTable.getValueAtDataModel(m_smdlTable.getEditingRow(), iCodeCol);
  			return codeValue;
  		}
  		else
  		{
  			return null;
  		}
    }
  	@Override
	public void actionPerformed(ActionEvent actionevent) 
  	{
		// TODO Auto-generated method stub
  		if ( actionevent.getSource() instanceof JButton )
        {
            if ( actionevent.getActionCommand().equalsIgnoreCase("RemoveCode") )
            {
            	int editingRow = m_smdlTable.getEditingRow();
            	if ( editingRow >= 0 )
            	{          
            		fireEditingStopped();
            		stopCellEditing();            		
            		strikeThroughTableCols(editingRow);            		     		
            	}
            }
        }
	}
  	private boolean getRemovedStatus(int editingRow)
  	{
  		boolean bStatus = false;
  		
  		AIFTableLine tableLine = m_smdlTable.getRowLine(editingRow);
		NOVCreateSMDLTableLine smdlTableLine = null;
		if(tableLine instanceof NOVCreateSMDLTableLine)
		{
			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
			boolean bIsCodeRemoved = smdlTableLine.getCodeRemovedStatus();
			smdlTableLine.setCodeRemovedStatus(!bIsCodeRemoved);
		}
		if(smdlTableLine!=null)
		{
			bStatus = smdlTableLine.getCodeRemovedStatus();
		}
		return bStatus;
  	}
  	private void strikeThroughTableCols(int editingRow)
  	{
  		String sStrikeThroughCols[] = getStrikeThroughTableColumns();
		TableColumnModel tableColModel = m_smdlTable.getColumnModel(); 
		TableModel tableModel = m_smdlTable.getModel();
		if(sStrikeThroughCols != null)
		{
			m_bRemovedCode = getRemovedStatus(editingRow);
			
			for(int iCount=0;iCount<sStrikeThroughCols.length;iCount++)
			{
				int iColIndx = m_smdlTable.getColumnIndexFromDataModel(sStrikeThroughCols[iCount]);
				TableCellRenderer tablecellrenderer = tableColModel.getColumn(iColIndx).getCellRenderer();
				if(tablecellrenderer instanceof IStrikeThrough)
				{
					IStrikeThrough strikeThru = (IStrikeThrough) tablecellrenderer;
					strikeThru.strikeThrough(m_bRemovedCode);
					if(tableModel instanceof NOVCreateSMDLTableModel)
					{
						((NOVCreateSMDLTableModel)tableModel).fireTableCellUpdated(editingRow, iColIndx);
					}
				}
			} 
		}
  	}
  	private String[] getStrikeThroughTableColumns()
	{
		String[] strColNames = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = NOVSMDLConstants.NOVSMDL_STRIKETHROUGH_ALLTABLECOLUMNS_PREF;
		strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_site, smdlTableColumnPref);
		if (null != strColNames && strColNames.length == 0) 
		{
			String sError = smdlTableColumnPref+ " " +m_registry.getString("prefMissing.MSG");
			MessageBox.post(sError, "ERROR",MessageBox.ERROR);
		}
		return strColNames;
	}
  	@Override
	public void addCellEditorListener(CellEditorListener listener )
    {
		m_Panel.getEventListenerList().add(CellEditorListener.class, listener);
    }

	@Override
	public void cancelCellEditing() 
	{
		// TODO Auto-generated method stub
		fireEditingCanceled();
	}
	public void fireEditingCanceled()
    {
        CellEditorListener listener;
        Object[] listeners = m_Panel.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(changeEvent);
            }
        }
    }
	@Override
	public void removeCellEditorListener(CellEditorListener listener )
	{
		m_Panel.getEventListenerList().remove(CellEditorListener.class, listener);
	}
	@Override
	public boolean shouldSelectCell(EventObject eventobject) {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean stopCellEditing() {
		// TODO Auto-generated method stub
		return true;
	}
	protected void fireEditingStopped()
    {
        CellEditorListener listener;
        Object[] listeners = m_Panel.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(changeEvent);
            }
        }
    }
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bCodeStrikeThrough = bIsStrikeThrough;
	}
	private class NovCodeColPanel extends JPanel
	{		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public EventListenerList getEventListenerList()
		{			
			return listenerList;			
		}
		public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bCodeStrikeThrough )
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
	};
}
