package com.nov.rac.smdl.common.renderereditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableModel;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVDocNoCellRendererEditor implements ActionListener, TableCellEditor,TableCellRenderer, IStrikeThrough
{
    private static final long serialVersionUID = 1L;
    private boolean                 m_bRemovedDoc     = false;
    private JLabel                  m_Text;
    private DocNoColPanel           m_Panel;
    private JButton                 m_removeCodeBtn;
    private ImageIcon               m_minusIcon        = null;
    private Color                   m_alternateBackground = Color.white;   
    private Registry 				m_registry = null;
    private NOVCreateSMDLTable 		m_smdlTable;
    protected ChangeEvent     		changeEvent = new ChangeEvent(this);
    private boolean 				m_bCodeStrikeThrough = false;
    private boolean 				m_bDocStrikeThrough  = false;
    private boolean                 m_bCodeAdded         = false;
    private boolean                 m_bRowUpdated        = false;
    public NOVDocNoCellRendererEditor()
    {
    	super();
    	m_registry = Registry.getRegistry("com.nov.rac.smdl.createsmdl.createsmdl");
    	m_alternateBackground = new Color(191,214,248);
    	m_Panel = new DocNoColPanel();
    	m_Panel.setLayout(new BoxLayout(m_Panel , BoxLayout.LINE_AXIS));
    	m_Text = new JLabel();
    	m_Text.setMinimumSize(new Dimension(80 , 20));
        try
        {
            m_minusIcon =m_registry.getImageIcon("minus.ICON");        	
        }
        catch ( Exception ie )
        {
            System.out.println(ie);
        }
        m_removeCodeBtn = new JButton(m_minusIcon);
                
        m_removeCodeBtn.setMinimumSize(new Dimension(18 , 20));
        m_removeCodeBtn.setPreferredSize(new Dimension(18 , 20));
        m_removeCodeBtn.setMaximumSize(new Dimension(18 , 20));  
        m_removeCodeBtn.setBackground(Color.WHITE);
        
        m_removeCodeBtn.setActionCommand("RemoveDoc");
        m_removeCodeBtn.addActionListener(this);
        
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_removeCodeBtn);
        m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_Panel.add(m_Text);
    }

    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
        if ( comp != null )
        {
            ImageIcon compIcon;
            String strCompType = comp.getType();
            compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
            return compIcon;
        }
        else
            return null;
    }

    protected void setValueIcon(Object arg0 , Icon arg1 )
    {
        System.out.println("setValueIcon....");
        ImageIcon docIcon = m_registry.getImageIcon("document.ICON");
        if ( arg0 != null )
        {
        	m_Text.setText(arg0.toString());
        	m_Text.setIcon(docIcon);
        }
    }
    @Override
	public Component getTableCellRendererComponent(JTable jtable , Object obj ,
            boolean isSelected , boolean hasFocus , int row , int col ) 
	{
		TCComponent tccomponent = null;
		if(jtable instanceof NOVCreateSMDLTable)    
		{
			m_smdlTable = (NOVCreateSMDLTable)jtable;
			
			if (obj instanceof TCComponentItem) 
			{
				tccomponent = (TCComponent)obj;
			}
			AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		NOVCreateSMDLTableLine smdlTableLine = null;
    		if(tableLine instanceof NOVCreateSMDLTableLine)
    		{
    			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			m_bCodeStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			m_bDocStrikeThrough = smdlTableLine.getDocRemovedStatus();
    			m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
    			m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		}
		}
		m_Panel.setBackground(row % 2 != 1 ? Color.white : (new Color(191 , 214 , 248)));
    	m_removeCodeBtn.setForeground(new JButton().getForeground());     
    	if ( isSelected )
        {
    		m_Panel.setForeground(jtable.getSelectionForeground());
    		m_Panel.setBackground(jtable.getSelectionBackground());
        }
        else
        {
        	m_Panel.setForeground(jtable.getForeground());
        	m_Panel.setBackground(row % 2 != 1 ? jtable.getBackground() : m_alternateBackground);
        }
    	if ( hasFocus )
    	{
    		m_Panel.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
    		if ( jtable.isCellEditable(row, col) )
    		{
    			m_Panel.setForeground(UIManager.getColor("Table.focusCellForeground"));
    			m_Panel.setBackground(UIManager.getColor("Table.focusCellBackground"));
    		}
    	}
    	else
    	{
    		m_Panel.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
    	}
    	Icon icon = getDisplayIcon(tccomponent, obj);
    	String s = null;
    	if(tccomponent != null)
    	{
    		s = tccomponent.toString();
    	}
        m_Text.setText(s != null ? s : "");
        m_Text.setIcon(icon);    
        int k = m_Panel.getFontMetrics(m_Panel.getFont()).stringWidth(m_Text.getText());
        int l = jtable.getCellRect(row, col, false).width;
        if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
        {
        	m_Panel.setToolTipText(s);
        }
        else
        {
        	m_Panel.setToolTipText(null);
        }
        m_Panel.setSize(150, m_Panel.getHeight());  
        if (m_bCodeAdded)
		{
			m_Panel.setBackground(new Color(170, 255, 170));
		}
        else if(m_bRowUpdated)
        {
        	m_Panel.setBackground(Color.yellow);
        }
    	return m_Panel;
	}
    public Component getTableCellEditorComponent(JTable table , Object value ,
            boolean isSelected , int row , int column )
    {
        TCComponent tccomponent = null;
   
    	if (value instanceof TCComponentItem) 
    	{
    		tccomponent = (TCComponent) value;
		}
        Icon icon = getDisplayIcon(tccomponent, value);
        String s = null;
        if(tccomponent!=null)
        {
        	s = tccomponent.toString();
        	m_Text.setText(s != null ? s : "");
        }
        else
        {
        	m_Text.setText("");
        }
        m_Text.setIcon(icon);
        m_Panel.setForeground(table.getSelectionForeground());
        m_Panel.setBackground(table.getSelectionBackground());
        
        return m_Panel;
    }

    public Object getCellEditorValue()
    {
    	if(m_smdlTable != null)
    	{
    		int iDocCol = m_smdlTable.getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
    		Object doc =  m_smdlTable.getValueAtDataModel(m_smdlTable.getEditingRow(), iDocCol);
    		return doc;
    	}
    	else
    	{
    		return null;
    	}
    }
    @Override
	public boolean isCellEditable(EventObject arg0) 
	{
		// TODO Auto-generated method stub
		return true;
	}
    @Override
	public void addCellEditorListener(CellEditorListener listener )
    {
		m_Panel.getEventListenerList().add(CellEditorListener.class, listener);
    }

	@Override
	public void cancelCellEditing() 
	{
		// TODO Auto-generated method stub
		fireEditingCanceled();
	}
	public void fireEditingCanceled()
    {
        CellEditorListener listener;
        Object[] listeners = m_Panel.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(changeEvent);
            }
        }
    }
	@Override
	public void removeCellEditorListener(CellEditorListener listener )
	{
		m_Panel.getEventListenerList().remove(CellEditorListener.class, listener);
	}
	@Override
	public boolean shouldSelectCell(EventObject eventobject) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		// TODO Auto-generated method stub
		return true;
	}
	protected void fireEditingStopped()
    {
        CellEditorListener listener;
        Object[] listeners = m_Panel.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(changeEvent);
            }
        }
    }
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bCodeStrikeThrough = bIsStrikeThrough;
	}
	public void actionPerformed(ActionEvent e )
    {
        if ( e.getSource() instanceof JButton )
        {
            if ( e.getActionCommand().equalsIgnoreCase("RemoveDoc") )
            {
            	int editingRow = m_smdlTable.getEditingRow();
            	if(editingRow>=0)
            	{
            		fireEditingStopped();
            		stopCellEditing();
            		strikeThroughTableCols(editingRow);
            	}
            }
        }
    }
	private boolean getStatus(int editingRow)
  	{
  		boolean bStatus = false;
  		
  		AIFTableLine tableLine = m_smdlTable.getRowLine(editingRow);
		NOVCreateSMDLTableLine smdlTableLine = null;
		if(tableLine instanceof NOVCreateSMDLTableLine)
		{
			smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
			boolean bIsCodeRemoved = smdlTableLine.getDocRemovedStatus();
			smdlTableLine.setDocRemovedStatus(!bIsCodeRemoved);
		}
		if(smdlTableLine!=null)
		{
			bStatus = smdlTableLine.getDocRemovedStatus();
		}
		return bStatus;
  	}
	private void strikeThroughTableCols(int editingRow)
  	{
  		String sStrikeThroughCols[] = getDocColsToStrikeThrough();
		TableColumnModel tableColModel = m_smdlTable.getColumnModel(); 
		TableModel tableModel = m_smdlTable.getModel();
		
		if(sStrikeThroughCols != null)
		{
			m_bRemovedDoc = getStatus(editingRow);
			
			for(int iCount=0;iCount<sStrikeThroughCols.length;iCount++)
			{
				int iColIndx = m_smdlTable.getColumnIndexFromDataModel(sStrikeThroughCols[iCount]);
				TableCellRenderer tablecellrenderer = tableColModel.getColumn(iColIndx).getCellRenderer();
				if(tablecellrenderer instanceof IStrikeThrough)
				{
					IStrikeThrough strikeThru = (IStrikeThrough) tablecellrenderer;
					strikeThru.strikeThrough(m_bRemovedDoc);
					if(tableModel instanceof NOVCreateSMDLTableModel)
					{
						((NOVCreateSMDLTableModel)tableModel).fireTableCellUpdated(editingRow, iColIndx);
					}
				}
			} 
		}
  	}
	private String[] getDocColsToStrikeThrough()
	{
		String[] strColNames = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
		String smdlTableColumnPref = NOVSMDLConstants.NOVSMDL_STRIKETHROUGH_DOCUMNETCOLUMNS_PREF;
		strColNames = prefServ.getStringArray(TCPreferenceService.TC_preference_site, smdlTableColumnPref);
		if (null != strColNames && strColNames.length == 0) 
		{
			String sError = smdlTableColumnPref+ " " +m_registry.getString("prefMissing.MSG");
			MessageBox.post(sError, "ERROR",MessageBox.ERROR);
		}
		return strColNames;
	}
	private class DocNoColPanel extends JPanel
	{		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public EventListenerList getEventListenerList()
		{			
			return listenerList;			
		}
		public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bCodeStrikeThrough || m_bDocStrikeThrough)
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
	};
}
