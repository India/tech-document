package com.nov.rac.smdl.common.renderereditor;

public interface IStrikeThrough 
{	
	public void strikeThrough(boolean bIsStrikeThrough);	
}