package com.nov.rac.smdl.common.renderereditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.common.NOVSMDLOrderDPTable;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLRemoveDPRendererEditor implements TableCellRenderer, ActionListener, TableCellEditor
{
    private static final long serialVersionUID = 1L;
    private JButton                 m_removeCodeBtn;
    private ImageIcon               m_minusIcon        = null;
    private NOVSMDLOrderDPTable 	m_smdlOrderDPTable;
    private Registry 				m_registry = null;
    protected ChangeEvent     		changeEvent = new ChangeEvent(this);
    private JPanel 	m_Panel;

    public NOVSMDLRemoveDPRendererEditor()
    {
    	super();
    	m_registry = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
    	m_Panel = new JPanel();
    	try {
    	    m_minusIcon=m_registry.getImageIcon("minus.ICON");        	
    	} catch ( Exception ie ) {
    	    System.out.println(ie);
    	}
    	m_removeCodeBtn = new JButton(m_minusIcon);
    	m_removeCodeBtn.setMinimumSize(new Dimension(15 , 18));
    	m_removeCodeBtn.setPreferredSize(new Dimension(15 , 18));
    	m_removeCodeBtn.setMaximumSize(new Dimension(15 , 1));  
    	m_removeCodeBtn.setBackground(Color.WHITE);
    	m_removeCodeBtn.setActionCommand("RemoveCode");
    	m_removeCodeBtn.addActionListener(this);

    	m_Panel.add(Box.createRigidArea(new Dimension(0 , 5)));
    	m_Panel.add(m_removeCodeBtn);    	
    }

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		if(table instanceof NOVSMDLOrderDPTable) {
    		m_smdlOrderDPTable = (NOVSMDLOrderDPTable)table;   
    	}
    	
	    if (isSelected) {
	    	m_Panel.setForeground(table.getSelectionForeground());
	    	m_Panel.setBackground(table.getSelectionBackground());
        } else {
	    	  m_Panel.setForeground(table.getForeground());
	    	  m_Panel.setBackground(UIManager.getColor("Button.background"));
	    }
	    
	  return m_Panel;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object obj,
			boolean flag, int i, int j) {
  	  m_Panel.setForeground(table.getSelectionForeground());
  	  m_Panel.setBackground(table.getSelectionBackground()); 
  		
  	  m_Panel.setForeground(table.getForeground());
	  m_Panel.setBackground(UIManager.getColor("Button.background"));
  		
      return m_Panel;
	}
	
	@Override
  	public boolean isCellEditable(EventObject anEvent )
    {
  		return true;
    }
    
	@Override
	public void actionPerformed(ActionEvent actionevent) {
      if(actionevent.getSource() instanceof JButton) {
         if(actionevent.getActionCommand().equalsIgnoreCase("RemoveCode")) {
          	int editingRow = m_smdlOrderDPTable.getEditingRow();
               if(editingRow >= 0) { 
            	  m_smdlOrderDPTable.removeRow(editingRow);
            	  stopCellEditing();
            	}
            }
        }
	}
  	
	@Override
	public boolean stopCellEditing() {
      return true;
	}
	
	@Override
	public void addCellEditorListener(CellEditorListener arg0) {
	
	}
	
	@Override
	public void cancelCellEditing() {
	
	}
	
	@Override
	public Object getCellEditorValue() {
	  return true;
	}
	
	@Override
	public void removeCellEditorListener(CellEditorListener arg0) {
	}
	
	@Override
	public boolean shouldSelectCell(EventObject arg0) {
		return false;
	}
}
