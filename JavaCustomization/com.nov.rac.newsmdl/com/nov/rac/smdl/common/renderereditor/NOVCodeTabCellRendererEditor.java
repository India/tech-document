package com.nov.rac.smdl.common.renderereditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.util.EventObject;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.common.NOVTextFieldLimit;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTable;
import com.nov.rac.smdl.createsmdl.NOVCreateSMDLTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.iTextField;

public class NOVCodeTabCellRendererEditor implements TableCellRenderer, TableCellEditor,IStrikeThrough
{
	
	private TextField m_iTextField ;	
	private static final long serialVersionUID = -697898812006419074L;
	private boolean 			m_bIsStrikeThrough = false;
	private boolean             m_bCodeAdded       = false;
	private NOVCreateSMDLTable m_smdlTable;
	private boolean             m_bRowUpdated      = false;
	private Color               m_alternateBackground = Color.white;
    public NOVCodeTabCellRendererEditor()
    {
    	m_alternateBackground = new Color(191,214,248);
    	m_iTextField = new TextField();
    	m_iTextField.setDocument(new NOVTextFieldLimit(1, true));
    }  
    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
    	if ( comp != null )
    	{
    		ImageIcon compIcon;
    		String strCompType = comp.getType();
    		compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
    		return compIcon;
    	}
    	else
    		return null;
    }
    protected void setValueIcon(Object arg0 , Icon arg1 )
	{
    	if ( arg0 != null )
	    {
    		m_iTextField.setText(arg0.toString());
	    }
	}    
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
    	 String s = (String)value;
    	 m_iTextField.setText(s != null ? s : "");
    	 if(table instanceof NOVCreateSMDLTable)    
    	 {
    		 m_smdlTable = (NOVCreateSMDLTable)table;
    		 AIFTableLine tableLine = m_smdlTable.getRowLine(row);
    		 NOVCreateSMDLTableLine smdlTableLine = null;
    		 if(tableLine instanceof NOVCreateSMDLTableLine)
    		 {
    			 smdlTableLine = (NOVCreateSMDLTableLine)tableLine;
    			 m_bIsStrikeThrough = smdlTableLine.getCodeRemovedStatus();
    			 m_bCodeAdded = smdlTableLine.getCodeAddedStatus();
     			 m_bRowUpdated = smdlTableLine.getRowUpdatedStatus();
    		 }
    	 }
    	 if ( isSelected )
         {
    		m_iTextField.setForeground(table.getSelectionForeground());
         	m_iTextField.setBackground(table.getSelectionBackground());
         }
         else
         {
        	m_iTextField.setForeground(table.getForeground());
         	m_iTextField.setBackground(row % 2 != 1 ? table.getBackground() : m_alternateBackground);
         }
         if ( hasFocus )
         {
        	 m_iTextField.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
         	if ( table.isCellEditable(row, column) )
         	{
         		m_iTextField.setForeground(UIManager.getColor("Table.focusCellForeground"));
         		m_iTextField.setBackground(UIManager.getColor("Table.focusCellBackground"));
         	}
         }
         else
         {
        	 m_iTextField.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
         }
        
    	 if (m_bCodeAdded)
    	 {
    		 m_iTextField.setBackground(new Color(170, 255, 170));
    	 }
    	 else if(m_bRowUpdated)
    	 {
    		 m_iTextField.setBackground(Color.yellow);
    	 }
    	 return m_iTextField;
    }   
	@Override
	public void strikeThrough(boolean bIsStrikeThrough) 
	{
		// TODO Auto-generated method stub
		m_bIsStrikeThrough = bIsStrikeThrough;
	}
       
    @Override
	public Component getTableCellEditorComponent(JTable jtable, Object obj,
			boolean flag, int i, int j) 
  	{	 
		String value = (String) obj;
		m_iTextField.setText(value!=null?value:"");
		m_iTextField.setForeground(jtable.getSelectionForeground());
		m_iTextField.setBackground(jtable.getSelectionBackground());        
        return m_iTextField;
	}
	@Override
	public void addCellEditorListener(CellEditorListener listener) 
	{
		m_iTextField.getEventListenerList().add(CellEditorListener.class, listener);
	}
	@Override
	public Object getCellEditorValue() 
	{
		return m_iTextField.getText();
	}
	@Override
	public boolean isCellEditable(EventObject arg0) 
	{
		return true;
	}
	@Override
	public void removeCellEditorListener(CellEditorListener listener) {
		
		m_iTextField.getEventListenerList().remove(CellEditorListener.class, listener);
	}
	@Override
	public boolean shouldSelectCell(java.util.EventObject eventobject) 
	{
		m_iTextField.repaint();
	    return true;
	}
	@Override
	public boolean stopCellEditing() 
	{
		fireEditingStopped();
		return true;
	}
	@Override
	public void cancelCellEditing() 
	{		 
		fireEditingCanceled();
	}
	
	private void fireEditingCanceled()
	{
		CellEditorListener listener;

		Object[] listeners = m_iTextField.getEventListenerList().getListenerList();
		for ( int i = 0; i < listeners.length; i++ )
		{
			if ( listeners[i] == CellEditorListener.class )
			{
				if (m_iTextField.changeEvent == null)
					m_iTextField.changeEvent = new ChangeEvent(m_iTextField);
				listener = (CellEditorListener) listeners[i + 1];
				listener.editingCanceled(m_iTextField.changeEvent);
			}
		}
	}	
	private void fireEditingStopped()
	{
		m_iTextField.repaint();
		CellEditorListener listener;
		Object[] listeners = m_iTextField.getEventListenerList().getListenerList();
		for ( int i = 0; i < listeners.length; i++ )
		{
			if ( listeners[i] == CellEditorListener.class )
			{
				if (m_iTextField.changeEvent == null)
					m_iTextField.changeEvent = new ChangeEvent(m_iTextField);
				listener = (CellEditorListener) listeners[i + 1];
				listener.editingStopped(m_iTextField.changeEvent);
			}
		}
	}
	private class TextField extends iTextField
    {
    	/**
		 * 
		 */
    	public TextField()
    	{
    		super();
    	}
		private static final long serialVersionUID = 1L;
		protected ChangeEvent changeEvent = null;
		public void paintComponent(Graphics g )
        {
        	super.paintComponent(g);
            if ( m_bIsStrikeThrough)
            {
                g.setColor(Color.RED);
                int midpoint = getHeight() / 2;
                g.drawLine(0, midpoint, getWidth() - 1, midpoint);
            }
        }
		public EventListenerList getEventListenerList()
    	{
    		return listenerList;
    	}
    }
}