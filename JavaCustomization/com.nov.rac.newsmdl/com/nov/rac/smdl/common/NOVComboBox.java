/*================================================================================
                     Copyright (c) 2009 National Oilwell Varco
                     Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVComboBox.java
 Package Name: com.nov.iman.commands.newitem
 ================================================================================
 Modification Log
 ================================================================================
 Revision       Date         Author        Description  
 1.0            2009/02/04   harshadam     Initial Creation
 1.1            2009/02/05   harshadam     Code Cleanup,Format    
 ================================================================================*/

package com.nov.rac.smdl.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import com.teamcenter.rac.util.Painter;

public class NOVComboBox extends JComboBox
{
    public NOVComboBox(Vector arg0)
    {
        super(arg0);
        if ( !arg0.isEmpty() )
        {
            setEditor(new ComboBoxEditorExample(arg0 , (String) arg0.get(0)));
            setEditable(true);
        }
        if ( getItemCount() > 5 )
            setMaximumRowCount(5);
        else
            setMaximumRowCount(getItemCount());
    }
    
    public NOVComboBox(Vector arg0, String sInitialValue, boolean bMandatory)
    {
        super(arg0);
        if ( !arg0.isEmpty() )
        {
            setEditor(new ComboBoxEditorExample(arg0 ,sInitialValue));
            setEditable(true);
        }
        setMaximumRowCount(getItemCount());
        setRequired(bMandatory);
        
        if(sInitialValue==null || sInitialValue.length()==0)
          setSelectedIndex(-1);
        else
            setSelectedItem(sInitialValue);
        
        if ( getItemCount() > 5 )
            setMaximumRowCount(5);
        else
            setMaximumRowCount(getItemCount());
    }

    public void panelFocusLost()
    {
        if ( NOVComboBox.this.isPopupVisible() )
        {
            NOVComboBox.this
                    .setSelectedItem(NOVComboBox.this.getSelectedItem());
            NOVComboBox.this.getEditor().setItem(
                    NOVComboBox.this.getSelectedItem());
            NOVComboBox.this.hidePopup();
        }
        else
        {
            NOVComboBox.this.getEditor().setItem(
                    NOVComboBox.this.getSelectedItem());
        }
    }

  /*  @Override
    public void setEnabled(boolean arg0) {
    	
    	super.setEnabled(arg0);
    	 ((ComboBoxEditorExample)NOVComboBox.this.getEditor()).panel.textField.setEnabled(arg0);
    }*/
    
    class ComboBoxEditorExample implements ComboBoxEditor
    {
        Vector     vector;
        ImagePanel panel;

        /**
         * @param m
         * @param defaultChoice
         */
        public ComboBoxEditorExample(Vector m, String defaultChoice)
        {
            vector = m;
            panel = new ImagePanel(defaultChoice);
        }

        /**
         * 
         */
        public void setItem(Object anObject )
        {
            if(panel.doNotSetText)
            {
            if ( anObject != null)
            {
                 //System.out.println("setItem....." + anObject.toString());
                if ( vector.indexOf(anObject.toString()) != -1 )
                {
                    panel.setText(anObject.toString());
                }
                else
                    panel.setText("");
            }
            else
                panel.setText("");
            }
        }

        public Component getEditorComponent()
        {
            return panel;
        }

        public Object getItem()
        {
            return panel.getText();
        }

        public void selectAll()
        {
            panel.selectAll();
        }

        public void addActionListener(ActionListener l )
        {
        }

        public void removeActionListener(ActionListener l )
        {
        }

        /**
         * We create our own inner class to handle setting and repainting the
         * text.
         */
        class ImagePanel extends JPanel implements KeyListener
        {
            /**
             * @param initialEntry
             */
            public ImagePanel(String sInitialEntry)
            {
                String initialEntry = sInitialEntry==null?"":sInitialEntry;
                UOMComboFlag = true;
                UOWComboFlag = false;
                compCnt = NOVComboBox.this.getModel().getSize();
                if ( compCnt > 0 )
                {
                    items = new String[compCnt];
                    for ( int j = 0; j < compCnt; j++ )
                    {
                        items[j] = NOVComboBox.this.getModel().getElementAt(j)
                                .toString();
                        vItems.add(j, items[j]);
                    }
                }
                setLayout(new BorderLayout());
                textField = new NOVTextField(initialEntry , UOMComboFlag ,
                        UOWComboFlag , NOVComboBox.this);
                textField.setColumns(45);
                textField.setBorder(new BevelBorder(BevelBorder.LOWERED));
                add(textField, BorderLayout.WEST);
            }

            /**
             * setText
             * 
             * @param s
             */
            public void setText(String s )
            {
                if ( doNotSetText )
                {
                    textField.removeKeyListener(ImagePanel.this);
                    textField.setText("");
                    textField.setText(s);
                    textField.addKeyListener(ImagePanel.this);
                }
                else
                    doNotSetText = true;
            }

            public String getText()
            {
                return (textField.getText());
            }

            public void selectAll()
            {
                textField.selectAll();
            }

            public void addActionListener(ActionListener l )
            {
                textField.addKeyListener(this);
            }

            /**
             * removeActionListener
             * 
             * @param l
             *            void
             */
            public void removeActionListener(ActionListener l )
            {
                textField.removeActionListener(l);
            }

            /**
             * 
             */
            public void keyPressed(KeyEvent e )
            {
                int keycode = e.getKeyCode();
                if ( keycode == KeyEvent.VK_ENTER )
                {
                    if ( NOVComboBox.this.isPopupVisible() )
                    {
                        if ( textField.getSelectedText() != null )
                        {
                            textField.setSelectedTextColor(Color.BLACK);
                            textField.setSelectionColor(Color.WHITE);
                            textField.setSelectionStart(textField
                                    .getSelectionStart());
                            textField.setSelectionEnd(textField
                                    .getSelectionEnd());
                        }
                        NOVComboBox.this.setSelectedItem(NOVComboBox.this
                                .getSelectedItem());
                        NOVComboBox.this.getEditor().setItem(
                                NOVComboBox.this.getSelectedItem());
                        NOVComboBox.this.hidePopup();
                    }
                }
                if ( keycode == KeyEvent.VK_TAB )
                {
                    if ( NOVComboBox.this.isPopupVisible() )
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
            }

            /*******************************************************************
             * // keyReleased
             */
            public void keyReleased(KeyEvent e )
            {
                String key = Character.toString(e.getKeyChar());
                int keyCode = e.getKeyCode();
                if ( keyCode == KeyEvent.VK_TAB )
                {
                    if ( NOVComboBox.this.isPopupVisible() )
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
                else if ( keyCode == KeyEvent.VK_ENTER )
                {
                    if ( NOVComboBox.this.isPopupVisible() )
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
                else if ( keyCode == KeyEvent.VK_DOWN )
                {
                    int index = NOVComboBox.this.getSelectedIndex();
                    if ( !(NOVComboBox.this.getItemCount() == index) )
                        NOVComboBox.this.setSelectedItem(NOVComboBox.this
                                .getSelectedItem());
                }
                else if ( keyCode == KeyEvent.VK_UP )
                {
                    if ( !(NOVComboBox.this.getSelectedIndex() == 0) )
                        NOVComboBox.this.setSelectedItem(NOVComboBox.this
                                .getSelectedItem());
                }
                else if ( (e.getKeyChar() >= 'A' && e.getKeyChar() <= 'Z')
                        || (e.getKeyChar() >= 'a' && e.getKeyChar() <= 'z') )
                {
                    String toCompare = "";
                    if ( null != textField.getText()
                            && textField.getText().length() > 1 )
                        toCompare = textField.getText().toString();
                    else
                        toCompare = key;
                    int i;
                    for ( i = 0; i < items.length; i++ )
                    {
                        if ( items[i].toLowerCase().startsWith(
                                toCompare.toLowerCase()) )
                        {
                            NOVComboBox.this.removeAllItems();
                            String[] sItems = getCompToRefillCombo(toCompare);
                            for ( int k = 0; k < sItems.length; k++ )
                                NOVComboBox.this.addItem(sItems[k]);
                            NOVComboBox.this.setSelectedItem(items[i]);
                            if ( sItems.length > 1 )
                            {
                                int length = items[i].length();
                                textField.setSelectedTextColor(Color.WHITE);
                                textField.setSelectionColor(Color.blue);
                                textField.setSelectionStart(toCompare.length());
                                textField.setSelectionEnd(length);
                            }
                            if ( sItems.length < 5 )
                                NOVComboBox.this
                                        .setMaximumRowCount(sItems.length);
                            else
                                NOVComboBox.this.setMaximumRowCount(5);
                            NOVComboBox.this.showPopup();
                            break;
                        }
                    }
                    if ( i == items.length )
                    {
                        NOVComboBox.this.setSelectedIndex(-1);
                        if ( NOVComboBox.this.isPopupVisible() )
                        {
                            //NOVComboBox.this.setSelectedIndex(-1);
                            NOVComboBox.this.hidePopup();
                            this.setText(toCompare);
                        }
                    }
                }
                else if ( (e.getKeyChar() == '\b' || e.getKeyCode() == KeyEvent.VK_DELETE)
                        && (textField.getText().length() >= 1) )
                {
                    String toCompare = "";
                    if ( textField.getText().length() >= 1 )
                        toCompare = textField.getText().toString();
                    int i;
                    for ( i = 0; i < items.length; i++ )
                    {
                        if ( items[i].toLowerCase().startsWith(
                                toCompare.toLowerCase()) )
                        {
                            NOVComboBox.this.removeAllItems();
                            String[] sItems = getCompToRefillCombo(toCompare);
                            for ( int k = 0; k < sItems.length; k++ )
                            {
                                doNotSetText = false;
                                NOVComboBox.this.addItem(sItems[k]);
                            }
                            doNotSetText = true;
                            
                            if ( sItems.length < 5 )
                                NOVComboBox.this
                                        .setMaximumRowCount(sItems.length);
                            else
                                NOVComboBox.this.setMaximumRowCount(5);
                            NOVComboBox.this.showPopup();
                            this.setText(toCompare);
                           // NOVComboBox.this.setSelectedIndex(0);
                            return;
                        }
                    }
                   /* if ( i == items.length )
                    {
                        if ( NOVComboBox.this.isPopupVisible() )
                        {
                            NOVComboBox.this.setSelectedIndex(-1);
                            NOVComboBox.this.hidePopup();
                        }
                    }*/
                }
                else if( textField.getText().length() == 0 )
                {
                    if ( NOVComboBox.this.isPopupVisible() )
                    {
                        NOVComboBox.this.hidePopup();
                    }
                }
                if ( textField.getText().length() == 0 )
                {
                    NOVComboBox.this.removeAllItems();
                    for ( int j = 0; j < items.length; j++ )
                    {
                        doNotSetText = false;
                        NOVComboBox.this.addItem(items[j]);
                      //  NOVComboBox.this.setSelectedIndex(-1);
                    }
                    doNotSetText = true;
                    NOVComboBox.this.setSelectedIndex(0);
                    NOVComboBox.this.setMaximumRowCount(5);
                    NOVComboBox.this.showPopup();
                }
            }

            public String[] getCompToRefillCombo(String toCompare )
            {
                List list = new LinkedList();
                for ( int i = 0; i < items.length; i++ )
                {
                    if ( items[i].toLowerCase().startsWith(
                            toCompare.toLowerCase()) )
                    {
                        list.add(items[i]);
                    }
                }
                String[] strReturn = (String[]) list.toArray(new String[list
                        .size()]);
                return strReturn;
            }

            /**
             * keyTyped
             */
            public void keyTyped(KeyEvent e )
            {
            }
            
            /**
             * class variables
             */
            private static final long serialVersionUID = 1L;
            int                       compCnt;
            String[]                  items;
            Vector                    vItems           = new Vector();
           public  boolean                   doNotSetText     = true;
            JLabel                    imageIconLabel;
            NOVTextField              textField;
            boolean                   UOMComboFlag     = true;
            boolean                   UOWComboFlag     = false;
        }
    }
    public void setRequired(boolean required)
    {
        this.required=required;
    }
    public boolean isRequired()
    {
        return this.required;
    }

    public void paint(Graphics g) 
    {
        // TODO Auto-generated method stub
        super.paint(g);
        if(required)
            Painter.paintIsRequired(this, g);
    }
    private static final long serialVersionUID = 1L;
    private boolean           required         = false;
    
}
