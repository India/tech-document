package com.nov.rac.smdl.common;

import java.util.Hashtable;
import java.util.Vector;

import com.teamcenter.rac.aif.common.AIFIdentifier;
import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.common.TCTableLine;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;

public class NOVSMDLTableLine extends TCTableLine 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean m_bTableLineAdded = false;
	private boolean m_bTableLineUpdated = false;
	private TCComponent m_tableLineCodeObject = null;
	private Vector m_identifierList = new Vector();
	private Vector m_tcCompIdentifierList = new Vector();
	public NOVSMDLTableLine(Hashtable<String, Object> hashtable)
	{
		super(hashtable);
	}
	public boolean getTableLineAddedStatus()
	{
		return m_bTableLineAdded;
	}
	public void setTableLineAddedStatus(boolean bLineAdded)
	{
		m_bTableLineAdded = bLineAdded;
	}
	public boolean getTableLineUpdatedStatus()
	{
		return m_bTableLineUpdated;
	}
	public void setTableLineUpdatedStatus(boolean bLineUpdated)
	{
		m_bTableLineUpdated = bLineUpdated;
	}
	public TCComponent getTableLineCodeObject()
	{
		return m_tableLineCodeObject;
	}
	public void setTableLineCodeObject(TCComponent lineCodeObject)
	{
		m_tableLineCodeObject = lineCodeObject;
	}
	@Override
	public void setProperty(String s, Object obj) 
	{
		super.setProperty(s, obj);
		m_bTableLineUpdated = true;
	}
	public int compareTo(Object obj)
    {		
        int iComparisionResult = 1;        
        if(comparingIdentifiers != null && comparingIdentifiers.length > 0)
        {        	
        	int iIdentifierLength = comparingIdentifiers.length;
        	m_tcCompIdentifierList.clear();
        	m_identifierList.clear();
        	for(int indx = 0; indx<iIdentifierLength;indx++)
        	{
        		int propType = comparingIdentifiers[indx].getPropertyType();
        		if(propType == TCProperty.PROP_typed_reference)
        		{
        			m_tcCompIdentifierList.add(comparingIdentifiers[indx]);
        		}
        		else
        		{
        			m_identifierList.add(comparingIdentifiers[indx]);
        		}
        	}
        }
        comparingIdentifiers = (AIFIdentifier[]) m_identifierList.toArray(new AIFIdentifier[m_identifierList.size()]);

        // Kishor Ahuja : if tcCompIdentifierList has entried, then compare the entried using toString().
        int iCompIdentifierSize = m_tcCompIdentifierList.size();
        if(iCompIdentifierSize>0)
        {
        	Object tableLinePropValue1 = null;
        	Object tableLinePropValue2 = null;
        	if(obj instanceof AIFTreeNode)
        	{
        		AIFTreeNode smdlTableTreeNode = (AIFTreeNode)obj;
        		for(int inx=0;inx<iCompIdentifierSize;inx++)
        		{
        			AIFIdentifier smdlTableIdentifier = (AIFIdentifier) m_tcCompIdentifierList.get(inx);

        			tableLinePropValue1 = getProperty((String)smdlTableIdentifier.getColumnId());
        			tableLinePropValue2 = smdlTableTreeNode.getProperty((String)smdlTableIdentifier.getColumnId());

        			if(tableLinePropValue1 != null && tableLinePropValue2 != null)
        			{
        				String sObject1 = tableLinePropValue1.toString();
        				String sObject2 = tableLinePropValue2.toString();
        				iComparisionResult = sObject1.compareTo(sObject2);
        				if(iComparisionResult != 0)
        				{
        					return iComparisionResult;
        				}
        			}
        			else if(tableLinePropValue1 == tableLinePropValue2)
        			{
        				iComparisionResult = 0;
        			}
        			else if(tableLinePropValue1 == null)
        			{
        				iComparisionResult = -1;
        			}
        			else
        			{
        				iComparisionResult = 1;
        			}
        			if(iComparisionResult != 0)
        			{
        				return iComparisionResult;
        			}
        		}
        	}
        }
        return super.compareTo(obj);
    }
}
