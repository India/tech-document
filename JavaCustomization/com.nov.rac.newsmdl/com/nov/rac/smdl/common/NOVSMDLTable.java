/*================================================================================
                             Copyright (c) 2009 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : IMANCustomTable
 Package Name: com.nov.iman.smdl.common
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         2009/03/25    HarshadaM     File added in SVN
   1.1         2009/03/25    HarshadaM     File modified for custom table
 ================================================================================*/
package com.nov.rac.smdl.common;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.ToolTipManager;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCTextService;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

/**
 * File TCCustomTable
 */
public class NOVSMDLTable extends TCTable
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public NOVSMDLTable(TCSession session, String columns[])
    {
        super(session , columns);
        //smdlAppReg =Registry.getRegistry("com.nov.rac.smdl.common.common");
        smdlAppReg = getRegistry();
       
        setModel(session, columns);
        setSession( session );
        
        assignColumnRenderer();
        assignCellEditor();
        setColumnWidths();
    }
    
    public NOVSMDLTable(TCSession session, String columns[], String dispColumns[])
    {
        super( columns, dispColumns );
        //smdlAppReg =Registry.getRegistry("com.nov.rac.smdl.common.common");
        smdlAppReg = getRegistry();
       
        setModel(session, columns);
        setSession( session );
        
        assignColumnRenderer();
        assignCellEditor();
        setColumnWidths();
    }
    
	protected void setModel(TCSession session, String[] columns) {
		if(session != null)
        {
			String strDisplayToken = getDisplayNamesToken();
			
            String as1[][] = getDisplayableHeaderValue(session, columns, strDisplayToken);
            dataModel = new NOVSMDLTableModel(as1);
        }
        else
        {
            dataModel = new NOVSMDLTableModel(columns);
        }
        
        setModel(dataModel);
	}
	
    public void reOrderColumns(String columnNames[])
	{
		TableColumnModel model = getColumnModel();

		for (int newIndex = 0; newIndex < columnNames.length; newIndex++)
		{
			try
			{
				String columnName = columnNames[newIndex];
				int index = model.getColumnIndex(columnName);
				this.moveColumn(index, newIndex);
			}
			catch(IllegalArgumentException e) 
			{
				e.printStackTrace();
			}
		}
	}
    
    public void assignColumnRenderer()
    {
        ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        String s = r.getString("default.RENDERER");
       
        
        for(int j = 0; j < i; j++)
        {
           	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".RENDERER").toString(), s);
           
            try
            {
            	TableCellRenderer abstracttctablecellrenderer = (TableCellRenderer)Instancer.newInstance(s1);
                if(abstracttctablecellrenderer != null)
                    getColumnModel().getColumn(j).setCellRenderer(abstracttctablecellrenderer);
            }
            catch(Exception exception)
            {
                
            }
        }

        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }
    
    public void assignCellEditor()
    {
        ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        
        for(int j = 0; j < i; j++)
        {
        	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".EDITOR").toString(),null);
            if(s1 != null && s1.length() > 0)
            {
	            try
	            {
	            	TableCellEditor cellEditor = (TableCellEditor)Instancer.newInstance(s1);
	               
	                if(getColumnModel().getColumn(j).getCellEditor() == null)
	                {
	                    getColumnModel().getColumn(j).setCellEditor(cellEditor);
	                }
	            }
	           catch(Exception exception)
	            {
	                
	            }
            }
            
        }

        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }
    protected String[][] getDisplayableHeaderValue(TCSession tcsession, String as[], String strDisplayToken)
    {
        if(as == null)
            return (String[][])null;
    	
        int i = as.length;
        
        String as_display_names[] = null;
		if(strDisplayToken != null )
		{
			//TCPreferenceService prefServ = ((TCSession)session).getPreferenceService();
			TCPreferenceService prefServ = ((TCSession)tcsession).getPreferenceService();//TC10.1 Upgrade
			String as_display_pref = strDisplayToken;
			as_display_names = prefServ.getStringArray(TCPreferenceService.TC_preference_site, as_display_pref);
		}
    	
        String as1[] = new String[i];
        for(int l = 0; l < i; l++)
        {
            int k = as[l].indexOf(".");
            as1[l] = as[l].substring(k + 1);
        }
			
        String as2[];
        if(as_display_names == null )
        {
	        try
		        {
	            TCTextService tctextservice = tcsession.getTextService();
	            as2 = tctextservice.getTextValues(as);
		        }
		        catch(TCException tcexception)
		        {
	            as2 = as1;
	            return (String[][])null;
	        }
        }
        else
        {
        	as2 = as_display_names;
        }
        
        String as3[][] = new String[i][3];
        for(int i1 = 0; i1 < i; i1++)
	        {
            if(as2[i1] == null || as2[i1].length() <= 0)
                as3[i1][0] = as1[i1];
	            else
                as3[i1][0] = as2[i1];
            as3[i1][1] = as1[i1];
            as3[i1][2] = as[i1];
	}
        return as3;
	}
    
	@Override
	public boolean isCellEditable(int row, int col) {
		
		//TODO Nams find condition if ( bSmdlmodf ) return false;
		
		String columnName =getColumnPropertyName(col);
      
        String[] editableColumns=smdlAppReg.getStringArray("editableColumns.Name");
        if (Arrays.asList(editableColumns).contains(columnName) ) 
        {
            return true;
        }
        else
            return false;
	}

	public Object getValueAtDataModel(int iRow, int iCol)
	{
		Object retValue = null;
		
		retValue = getModel().getValueAt(convertRowIndexToModel(iRow), iCol);
	
		return retValue;
	}
	
	public int getColumnIndexFromDataModel( String theColName )
	{
	    int nColIndex = -1;
	    
	    int nColCount = dataModel.getColumnCount();
	    
	    for( int inx = 0; inx< nColCount; inx++ )
	    {
	        if( theColName.equals(dataModel.getColumnIdentifier(inx)) )
	        {
	        	nColIndex = inx;
	        	
	        	break;
	        }
	    }
	    
	    if(nColIndex== -1)
	    {
	    	nColIndex = getColumnIndexFromOtherIdentifier(theColName);
	    }
	    
	    return nColIndex;
	}
	
	
	
	public int getColumnIndexFromOtherIdentifier( String theColName )
	{
	    int nColIndex = -1;
	    
	    int nColCount = dataModel.getColumnCount();
	    
	    for( int inx = 0; inx< nColCount; inx++ )
	    {
	        if( theColName.equals(dataModel.getColumnOtherId((inx))))
	        {
	        	nColIndex = inx;
	        		        	
	        	break;
	        }
	    }
	    
	    return nColIndex;
	}


	public void setColumnWidths()
    {
        DefaultTableColumnModel defaulttablecolumnmodel = (DefaultTableColumnModel) getColumnModel();
        charWidth = (int) 8.5;
        int i = defaulttablecolumnmodel.getColumnCount();
        for ( int j = 0; j < i; j++ )
        {
            TableColumn tablecolumn = defaulttablecolumnmodel.getColumn(j);
            try
            {
                int integer = tablecolumn.getHeaderValue().toString().length();
                tablecolumn.setPreferredWidth((integer * charWidth) + 20);
            }
            catch ( Exception exception )
            {
            }
        }
        currentColumnNames = new String[i];
        currentColumnWidths = new int[i];
        for ( int k = 0; k < i; k++ )
        {
            TableColumn tablecolumn1 = defaulttablecolumnmodel.getColumn(k);
            int l = tablecolumn1.getWidth();
            currentColumnWidths[k] = l / charWidth;
            currentColumnNames[k] = getColumnName(k);
        }
    }
	public void createDefaultColumnsFromModel()
    {
        TableModel tablemodel = getModel();

        if( tablemodel != null && tablemodel instanceof NOVSMDLTableModel )
        {
        	NOVSMDLTableModel theTableModel = (NOVSMDLTableModel)tablemodel;
          
            for(TableColumnModel tablecolumnmodel = getColumnModel(); tablecolumnmodel.getColumnCount() > 0; tablecolumnmodel.removeColumn(tablecolumnmodel.getColumn(0)));
            for(int i = 0; i < theTableModel.getColumnCount(); i++)
            {
                TableColumn tablecolumn = new TableColumn(i);
              
                tablecolumn.setIdentifier( theTableModel.getColumnIdentifier(i));
                tablecolumn.setHeaderValue( theTableModel.getColumnName(i) );
                addColumn(tablecolumn);
            }
        	
        }
    }
	protected void addMouseListenerToHeaderInTable()
    {
        listMouseListener = new MouseAdapter() 
        {

            public void mouseClicked(MouseEvent mouseevent)
            {
                int i = mouseevent.getModifiers();
                if(i == mouseevent.MOUSE_EVENT_MASK)
                {
                    if(!bSorted || getRowCount() <= 0)
                        return;
                    TableColumnModel tablecolumnmodel = getColumnModel();
                    int j = tablecolumnmodel.getColumnIndexAtX(mouseevent.getX());
                    if(mouseevent.getClickCount() == 1 && j != -1)
                        sortColumn(j);
                }
            }

            public void mouseReleased(MouseEvent mouseevent)
            {
                int i = mouseevent.getModifiers();
                if(i == mouseevent.MOUSE_EVENT_MASK && mouseevent.getClickCount() != 2)
                {
                    saveTableInfoToPref();
                }
            }

            public void mousePressed(MouseEvent mouseevent)
            {
                int i = mouseevent.getModifiers();
                TableColumnModel tablecolumnmodel = getColumnModel();
                int j = tablecolumnmodel.getColumnIndexAtX(mouseevent.getX());
                if(i == mouseevent.FOCUS_EVENT_MASK)
                {
                	saveTableInfoToPref();
                } 
                else if(i == mouseevent.MOUSE_EVENT_MASK && mouseevent.getClickCount() == 2)
                {
                    fitColumn(j);
                }
            }
        };
        JTableHeader jtableheader = getTableHeader();
        jtableheader.addMouseListener(listMouseListener);
    }
	public void saveTableInfoToPref()
	{      
	   if(m_tableColNamesPref!=null)
	   {
		   
	        String as[] = getColumnOtherIds();
	        TCPreferenceService prefServ = ((TCSession) AIFUtility.getDefaultSession()).getPreferenceService();
	        try 
	        {
	        	//String sTableColPref = NOVSMDLConstants.NOVSMDL_TABLECOLUMNS_PREF;
	        	//prefServ.setStringArray(TCPreferenceService.TC_preference_user, sTableColPref, as);
	        	String sTableColPref = m_tableColNamesPref;
				prefServ.setStringArray(m_tableColNamesPrefLevel, sTableColPref, as);
			} 
	        catch (TCException e) 
	        {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	}

	public void setTableColumnNamePreference(String prefName, int prefLevel)
	{
		m_tableColNamesPref = prefName;
		m_tableColNamesPrefLevel = prefLevel;
	}
	
	protected Registry getRegistry()
	{
		return Registry.getRegistry(this);
	}
	
	protected String getDisplayNamesToken()
	{
		Registry reg = getRegistry();
		
		String strDisplayToken = reg.getString("table_column_display_names.PREF", null);
		
		return strDisplayToken;
	}
    private Registry smdlAppReg      = null;
    
    private String m_tableColNamesPref = null;
    
    private int m_tableColNamesPrefLevel = TCPreferenceService.TC_preference_all;
 
}
