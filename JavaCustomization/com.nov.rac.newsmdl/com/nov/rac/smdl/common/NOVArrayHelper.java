package com.nov.rac.smdl.common;

import java.util.Arrays;
import java.util.List;

public class NOVArrayHelper 
{

	public static boolean searchInArray(String name, String[] array)
	{
		boolean contains = false;
		if(name!=null)
		{
			List<String> stringList= Arrays.asList(array);
			/*int index= Collections.binarySearch(stringList, name);
			
			if(index <0)
			{
				contains= false;
			}*/
			//this is case insensitive contains= ((ArrayList)stringList).contains(name);
			
			for(int index=0;index<array.length;index++)
			{
				if(name.equalsIgnoreCase(array[index]))
				{
					contains=true;
					break;
				}
			}
		}
		return contains;
	}
	
	
	public static boolean startsWithInArray(String name, String[] array)
	{
		boolean contains = false;
		if(name!=null)
		{
			List<String> stringList= Arrays.asList(array);
			/*int index= Collections.binarySearch(stringList, name);
			
			if(index <0)
			{
				contains= false;
			}*/
			//this is case insensitive contains= ((ArrayList)stringList).contains(name);
			
			for(int index=0;index<array.length;index++)
			{
				if((array[index]).startsWith(name))
				{
					contains=true;
					break;
				}
			}
		}
		return contains;
	}
}
