package com.nov.rac.smdl.common;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVListPopUpDialog extends AbstractSWTDialog
{
	private List m_List;
	private String sSelectedItem = null;
	public NOVListPopUpDialog(Shell parent) 
	{
		super(parent);
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL | SWT.CENTER);
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Point getInitialSize() {
		// TODO Auto-generated method stub
		Monitor monitor = this.getShell().getMonitor();

		Rectangle size = monitor.getBounds();

		Rectangle rect = this.getShell().getBounds();

		int rectwidth = rect.width;

		int width = ((size.width ) / 2) - (rectwidth / 3);

		int height = ((size.height ) / 2) - (rectwidth / 8);

		return new Point(width, height);


	}
	@Override
	protected Control createDialogArea(Composite parent) 
	{		
		Composite container = (Composite) super.createDialogArea(parent);
		
		container.setLayout(new FillLayout());
		
		Composite composite = new Composite(container, SWT.EMBEDDED);
		
		composite.setLayout(new FillLayout());
		
		m_List = new List(composite, SWT.H_SCROLL | SWT.V_SCROLL);
		
		m_List.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent selectionevent) 
			{
				sSelectedItem = m_List.getItem(m_List.getSelectionIndex());
				close();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent selectionevent) 
			{
			}
		});
		composite.pack();
		container.pack();
		parent.pack();
		
		return parent;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) 
	{		
	}
	public void populateList(String[] sValuesList)
	{
		Arrays.sort(sValuesList);
		m_List.setItems(sValuesList);
	}
	public String getSelectedValue()
	{
		return sSelectedItem;
	}
	public void setTitle(String sDialogTitle)
	{					
		this.getShell().setText(sDialogTitle);								
	}
}