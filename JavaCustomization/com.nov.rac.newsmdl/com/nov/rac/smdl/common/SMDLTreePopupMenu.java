package com.nov.rac.smdl.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JScrollPane;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.action.CloseSMDLHandler;
import com.nov.rac.smdl.action.SMDLOperationHelper;
import com.nov.rac.smdl.close.SMDLCloseWizard;
import com.nov.rac.smdl.close.SMDLCloseWizardDialog;
import com.nov.rac.smdl.utilities.NOVOpenObjectUtils;
import com.nov.rac.smdl.utilities.NOVSMDLTree;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.popupmenu.AbstractTCComponentPopupMenu;

public class SMDLTreePopupMenu extends AbstractTCComponentPopupMenu
{
	private static final long serialVersionUID = 1L;
	
	private JMenuItem closeSMDL = null;
	private JMenuItem openSMDL = null;
	
	private NOVSMDLTree m_smdlTree = null;
	
	public SMDLTreePopupMenu(NOVSMDLTree tcTree, final TCComponent iSelectedComp) {

		super();
		m_smdlTree = tcTree;
		addPopupHeader("Open/Close SMDL");

		closeSMDL = addMenuItem(this, "Close SMDL");
		closeSMDL.setText("Close SMDL");

		openSMDL = addMenuItem(this, "Open SMDL");
		openSMDL.setText("Open SMDL");

		if (iSelectedComp != null) {
			closeSMDL.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent actionevent) {

					final Display display = Display.getDefault();
					if (display != null) {
						display.asyncExec(new Runnable() {
							public void run() {
								
//								CloseSMDLHandler closeHandler = new CloseSMDLHandler();
//								
//								try {
//									closeHandler.execute(null);
//								} catch (ExecutionException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
								
								SMDLCloseWizard wizard = new SMDLCloseWizard(
										null);

//								InterfaceAIFComponent[] selComps = AIFUtility
//										.getTargetComponents();

								SMDLCloseWizardDialog dialog = new SMDLCloseWizardDialog(
										display.getActiveShell(), wizard);

								dialog.create();
								wizard.populateSMDLWizard((TCComponent) m_smdlTree.getSelectedComponent());

								dialog.open();
							}
						});
					}
				}
			});
		} else {
			closeSMDL.setEnabled(false);
		}
	}

}