package com.nov.rac.smdl.common;

import com.richclientgui.toolbox.validation.validator.IFieldValidator;


public abstract class NOVAbstractNameValidator implements IFieldValidator<String> 
{
	protected String errorMessage = "";
	protected boolean valid = false;
	
	
	
	public String getErrorMessage() 
	{
		return errorMessage;
	}
	 
	public String getWarningMessage() {
	   return "";
	}
	
	public boolean warningExist(String contents) 
	{
		return false;
	}

	abstract public boolean isValid(String arg0) ;

 
}