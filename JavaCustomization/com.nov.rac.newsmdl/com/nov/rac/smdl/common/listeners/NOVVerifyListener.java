package com.nov.rac.smdl.common.listeners;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;

public class NOVVerifyListener implements VerifyListener{

	@Override
	public void verifyText(VerifyEvent event) {
		
		try{
    		if(event.text.length() > 0){
    			int value = Integer.parseInt(event.text);
    		}
    		event.doit = true;
    	}
    	catch (NumberFormatException e) {
    		event.doit = false;
		}
	}

}
