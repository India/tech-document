package com.nov.rac.smdl.common;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class SMDLCloseTable extends TCTable{
	
	private Registry m_Registry      = null;
	
	public SMDLCloseTable(TCSession session, String columns[])
    {
        super(session , columns);
       	m_Registry =Registry.getRegistry("com.nov.rac.smdl.close.close");
       	setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
        setColumnWidths();
    }
	
	public boolean isCellEditable(int row, int col) {
		
		
		String columnName =getColumnPropertyName(col);
      
        String editableColumns = m_Registry.getString("UnlockRev_COL.Label");
        if (editableColumns.compareTo(columnName) == 0) 
        {
            return true;
        }
        else
            return false;
	}

	@Override
	protected void addMouseListenerToHeaderInTable() {
		
	}
	
	public void setColumnWidths()
    {
        DefaultTableColumnModel defaulttablecolumnmodel = (DefaultTableColumnModel) getColumnModel();
        charWidth = (int) 8.5;
        int i = defaulttablecolumnmodel.getColumnCount();
        for ( int j = 0; j < i; j++ )
        {
            TableColumn tablecolumn = defaulttablecolumnmodel.getColumn(j);
            try
            {
                int integer = tablecolumn.getHeaderValue().toString().length();
                tablecolumn.setPreferredWidth((integer * charWidth) + 20);
            }
            catch ( Exception exception )
            {
            }
        }
        currentColumnNames = new String[i];
        currentColumnWidths = new int[i];
        for ( int k = 0; k < i; k++ )
        {
            TableColumn tablecolumn1 = defaulttablecolumnmodel.getColumn(k);
            int l = tablecolumn1.getWidth();
            currentColumnWidths[k] = l / charWidth;
            currentColumnNames[k] = getColumnName(k);
        }
    }
	
	
}
