package com.nov.rac.smdl.common;

import java.awt.Frame;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.noi.rac.form.compound.component.AddressBookComponent;
import com.nov.rac.smdl.panes.order.NOVSMDLEmailIDComponent;
import com.teamcenter.rac.util.UIUtilities;

public class NOVAddressBookListener implements Listener {

	private Text m_wgtText = null;
	protected boolean dirty= false;
	private AddressBookComponent m_addrBookCmp = null;
	NOVSMDLEmailIDComponent smdlEmailIDCmp;
	
	public NOVAddressBookListener(Text txt){
		m_wgtText = txt;
	}
	
	public void handleEvent(Event event) {
		if(m_wgtText != null){
			if(event.widget.getData().equals("OrdSalesEMail")){
				final Display display = Display.getCurrent();
				smdlEmailIDCmp = (NOVSMDLEmailIDComponent)m_wgtText.getParent();
				final boolean isUser = smdlEmailIDCmp.getIsUser();
				final boolean[] blockSWT = new boolean[] {true};
				
				Composite composite = new Composite(display.getActiveShell(), SWT.EMBEDDED);
				final Frame frame = SWT_AWT.new_Frame(composite);
				
                if(display != null) {
                   java.awt.EventQueue.invokeLater(new Runnable() {
                      public void run() {
                    	  m_addrBookCmp = new AddressBookComponent(frame, isUser);
                    	  m_addrBookCmp.pack();
                    	  UIUtilities.centerToScreen(m_addrBookCmp);
                    	  m_addrBookCmp.setVisible(true);
                    	  
                    	  display.syncExec(new Runnable() {
	                        public void run() {
	                           blockSWT[0] = false;
	                        }
	                     });

                    	  display.asyncExec(new Runnable() {
 	                        public void run() {
 	                        	if(m_addrBookCmp.getWgtTextValue() != null) {
 	                        		smdlEmailIDCmp.setTextVal(m_addrBookCmp.getWgtTextValue());
 	                        	}
 	                        }
 	                     });
                       }
                    });         
                    while(blockSWT[0]) {
                       try {
                          if (!display.readAndDispatch()) {
                             display.sleep();
                          }
                       } catch (Throwable e) {
                           e.printStackTrace();
                       }
                    }
                }
			}
		}
	}
}
