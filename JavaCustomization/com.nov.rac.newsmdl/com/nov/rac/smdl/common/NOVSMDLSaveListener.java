package com.nov.rac.smdl.common;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.panes.SMDLPane;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class NOVSMDLSaveListener implements SelectionListener {

	private SMDLPane m_smdlPane;
	private TCComponentItemRevision m_smdlRev;
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0)
	{		

	}
	@Override
	public void widgetSelected(SelectionEvent event)
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button saveButton= (Button)target;					
			m_smdlPane= getSMDLPanel(saveButton);
			m_smdlRev = (TCComponentItemRevision) saveButton.getData();
			int iNoOfCodesAttachedToSMDLRev = 0;
			try 
			{
				TCComponent[] codesAttachedToSMDLRev = m_smdlRev.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
				iNoOfCodesAttachedToSMDLRev = codesAttachedToSMDLRev.length;
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}
			HashSet<TCComponent> codeObjects = new HashSet<TCComponent>();
			codeObjects = 	m_smdlPane.getCodeObjects();
			
			CreateInObjectHelper[] createCodeInput = m_smdlPane.populateCreateInObject();
			int iCodeInputLength = createCodeInput.length;
			TCComponent[] createdCodeForms = new TCComponent[iCodeInputLength];  
	
			if((iCodeInputLength > 0) || (codeObjects.size() != iNoOfCodesAttachedToSMDLRev))
			{
				ProgressMonitorDialog saveSMDLDialog=new ProgressMonitorDialog(saveButton.getShell());			

				if(iCodeInputLength > 0)
				{
					CreateOrUpdateOperation createCodeOp = new CreateOrUpdateOperation(createCodeInput);
					try
					{
						saveSMDLDialog.run(true, false,createCodeOp);
					}
					catch ( Exception e1 )
					{
						e1.printStackTrace();
					}
					// Get all code form objects 
					for(int inx=0; inx < iCodeInputLength; inx++)
					{
						createdCodeForms[inx] = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(createCodeInput[inx]);
					}
					codeObjects.addAll(Arrays.asList(createdCodeForms));
				}
								
				HashSet<TCComponent> documentObjects = new HashSet<TCComponent>();
				documentObjects = m_smdlPane.getDocuments();
				
				CreateInObjectHelper smdlRevUpdateObject = new CreateInObjectHelper(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE,CreateInObjectHelper.OPERATION_UPDATE);
				
				smdlRevUpdateObject.setTagArrayProps(NOV4ComponentSMDL.SMDL_CODES, codeObjects.toArray(new TCComponent[codeObjects.size()]), CreateInObjectHelper.SET_PROPERTY);
				
				smdlRevUpdateObject.setTagArrayProps(NOV4ComponentSMDLRevision.SMDLREV_DOCUMENT_RELATION,documentObjects.toArray(new TCComponent[documentObjects.size()]) , CreateInObjectHelper.SET_PROPERTY);

				smdlRevUpdateObject.setTargetObject(m_smdlRev);

				CreateOrUpdateOperation updateSMDLRevOp = new CreateOrUpdateOperation(smdlRevUpdateObject);
				try 
				{
					saveSMDLDialog.run(true, false,updateSMDLRevOp);		        	
				} 
				catch ( Exception e1 ) 
				{
					e1.printStackTrace();
					MessageBox.post("SMDL Rev is NOT updated: " + e1.getMessage(), null, MessageBox.ERROR);
				}				
			}
		} 
	}
	private SMDLPane getSMDLPanel(Button saveButton)
	{
		Composite parent= null;
		Composite component = saveButton.getParent();
		do {
			
			parent=	component.getParent();
			component=parent;
	
		}while (parent!=null && ! (parent instanceof SMDLPane ));
		
		return (SMDLPane)parent;
	}
}
