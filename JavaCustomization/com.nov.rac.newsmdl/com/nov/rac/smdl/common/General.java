/*
 * Created on May 19, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.nov.rac.smdl.common;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCSession;

public class General {

    public static TCComponent[] execute(TCSession session,String[] names,String[] values) 
    {
        try {
            TCComponentQueryType qt = 
                   (TCComponentQueryType)session.getTypeComponent("ImanQuery");
            TCComponentQuery gen = (TCComponentQuery)qt.find("General...");
            if (gen != null) {
                return (gen.execute(names,values));
            }
        }
        catch (Exception e)
        {
            System.out.println("Could not execute general query: "+e);
        }
        return (TCComponent[])null;
    }
}
