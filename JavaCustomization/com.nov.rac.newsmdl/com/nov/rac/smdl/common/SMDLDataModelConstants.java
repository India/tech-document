package com.nov.rac.smdl.common;

public class SMDLDataModelConstants 
{
	//Order Related
	public static final String 	ORDER_DATA_MODEL_NAME = "ORDER";
	public static final String 	CREATE_ORDER_DATA_MODEL_NAME = "CREATE_ORDER";
	
	//DP related
	public static final String 	DP_DATA_MODEL_NAME = "DELIVERED_PRODUCT";
	public static final String CREATE_DP_DATA_MODEL_NAME = "CREATE_DP";
	
	//SMDL Related
	public static final String 	SMDL_DATA_MODEL_NAME = "SMDL";
}
