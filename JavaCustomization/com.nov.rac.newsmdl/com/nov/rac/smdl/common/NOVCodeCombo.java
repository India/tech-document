/*================================================================================
                     Copyright (c) 2009 National Oilwell Varco
                     Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVComboBox.java
 Package Name: com.noi.iman.commands.newrsitem
 ================================================================================
 Modification Log
 ================================================================================
 Revision       Date         Author        Description  
 1.0            2009/02/04   harshadam     Initial Creation
 1.1            2009/02/05   harshadam     Code Cleanup,Formatting     
 ================================================================================*/
package com.nov.rac.smdl.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import com.nov.rac.smdl.common.NOVTextField;
import com.nov.rac.smdl.createsmdl.NOVSMDLNewEditDialog;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.util.Painter;

/**
 * @author harshadam
 * 
 */
public class NOVCodeCombo extends JComboBox {
	private static final long serialVersionUID = 1L;

	public NOVCodeCombo(Vector arg0) 
	{
		super(arg0);
		setEditor(new ComboBoxEditorExample(arg0, (String) arg0.get(0)));
		setEditable(true);
		setRequired(true);
		if (getItemCount() > 5)
			setMaximumRowCount(5);
		else
			setMaximumRowCount(getItemCount());
	}

	public void panelFocusLost() 
	{
		if (NOVCodeCombo.this.isPopupVisible()) 
		{
			NOVCodeCombo.this.setSelectedItem(NOVCodeCombo.this.getSelectedItem());
			NOVCodeCombo.this.getEditor().setItem(NOVCodeCombo.this.getSelectedItem());
			NOVCodeCombo.this.hidePopup();
		}
	}
	private NOVSMDLNewEditDialog getDocumentEditDialog()
	{
		Component parent = NOVCodeCombo.this.getParent();
		while (parent!=null && !(parent instanceof NOVSMDLNewEditDialog ))
		{			
			parent=	parent.getParent();	
		}		
		return (NOVSMDLNewEditDialog)parent;
	}

	class ComboBoxEditorExample implements ComboBoxEditor 
	{
		Vector vector;
		ImagePanel panel;
		public ComboBoxEditorExample(Vector m, String defaultChoice) 
		{
			vector = m;
			panel = new ImagePanel(defaultChoice);
		}
		public void setItem(Object anObject) 
		{
			if (anObject != null) 
			{
				if (vector.indexOf(anObject.toString()) != -1) 
				{
					panel.setText(anObject.toString());
				}
				else{
					for(int index=0;index<vector.size();index++)
					{
						if(vector.elementAt(index).toString().startsWith(anObject.toString()))
						{
							panel.setText(anObject.toString());
							break;
						}
					}
				}
			}
			else
				panel.setText("");
		}

		public Component getEditorComponent() 
		{
			return panel;
		}

		public Object getItem() 
		{
			return panel.getText();
		}

		public void selectAll() 
		{
			panel.selectAll();
		}

		public void addActionListener(ActionListener l) 
		{
		}

		public void removeActionListener(ActionListener l) {
		}
		class ImagePanel extends JPanel implements KeyListener 
		{
			private static final long serialVersionUID = 1L;
			int compCnt;
			String[] items;
			Vector vItems = new Vector();
			boolean doNotSetText = true;
			JLabel imageIconLabel;
			NOVTextField textField;
			boolean UOMComboFlag = true;
			boolean UOWComboFlag = false;
			public ImagePanel(String initialEntry) 
			{
				UOMComboFlag = true;
				UOWComboFlag = false;
				compCnt = NOVCodeCombo.this.getModel().getSize();
				if (compCnt > 0) 
				{
					items = new String[compCnt];
					for (int j = 0; j < compCnt; j++) 
					{
						items[j] = NOVCodeCombo.this.getModel().getElementAt(j).toString();
						vItems.add(j, items[j]);
					}
				}
				setLayout(new BorderLayout());
				textField = new NOVTextField("", UOMComboFlag,UOWComboFlag, NOVCodeCombo.this);
				textField.setColumns(45);
				textField.setBorder(new BevelBorder(BevelBorder.LOWERED));
				add(textField, BorderLayout.WEST);
			}

			private Component getModel() 
			{
				return null;
			}
			public void setText(String s) 
			{
				if (doNotSetText)
				{
					textField.removeKeyListener(ImagePanel.this);
					textField.setText("");
					if(s.indexOf("=")!=-1)
					{
						s = s.substring(0,s.indexOf("="));
					}
					textField.setText(s);
					NOVSMDLNewEditDialog novsmdlNewEditDialog = getDocumentEditDialog();
					if(null!=novsmdlNewEditDialog)
					{
						novsmdlNewEditDialog.populateDateInfoForCode(s);
					}
					textField.addKeyListener(ImagePanel.this);
				}
			}

			public String getText() 
			{
				return (textField.getText());
			}

			public void selectAll() 
			{
				textField.selectAll();
			}

			public void addActionListener(ActionListener l) 
			{
				textField.addKeyListener(this);
			}
			public void removeActionListener(ActionListener l) 
			{
				textField.removeActionListener(l);
			}
			public void keyPressed(KeyEvent e) 
			{
				int keycode = e.getKeyCode();
				if (keycode == KeyEvent.VK_ENTER) {
					if (NOVCodeCombo.this.isPopupVisible()) 
					{
						if (textField.getSelectedText() != null)
						{
							textField.setSelectedTextColor(Color.BLACK);
							textField.setSelectionColor(Color.WHITE);
							textField.setSelectionStart(textField.getSelectionStart());
							textField.setSelectionEnd(textField.getSelectionEnd());
						}
						NOVCodeCombo.this.hidePopup();
					}
				}
				if (keycode == KeyEvent.VK_TAB) 
				{
					if (NOVCodeCombo.this.isPopupVisible()) {
						NOVCodeCombo.this.hidePopup();
					}
				}
			}
			public void keyReleased(KeyEvent e)
			{
				String key = Character.toString(e.getKeyChar());
				int keyCode = e.getKeyCode();
				if (keyCode == KeyEvent.VK_TAB) 
				{
					if (NOVCodeCombo.this.isPopupVisible()) 
					{
						NOVCodeCombo.this.hidePopup();
					}
				} 
				else if (keyCode == KeyEvent.VK_ENTER) 
				{
					if (NOVCodeCombo.this.isPopupVisible())
						NOVCodeCombo.this.setSelectedItem(NOVCodeCombo.this.getSelectedItem());
					if (NOVCodeCombo.this.isPopupVisible()) 
					{
						NOVCodeCombo.this.hidePopup();
					}
				} 
				else if (keyCode == KeyEvent.VK_DOWN) 
				{
					int index = NOVCodeCombo.this.getSelectedIndex();
					if (!(NOVCodeCombo.this.getItemCount() == index))
						NOVCodeCombo.this.setSelectedItem(NOVCodeCombo.this.getSelectedItem());
				}
				else if (keyCode == KeyEvent.VK_UP) 
				{
					if (!(NOVCodeCombo.this.getSelectedIndex() == 0))
						NOVCodeCombo.this.setSelectedItem(NOVCodeCombo.this.getSelectedItem());
				} 
				else if ((e.getKeyChar() >= 'A' && e.getKeyChar() <= 'Z')
						|| (e.getKeyChar() >= 'a' && e.getKeyChar() <= 'z') || ( (e.getKeyChar() >= '0' && e.getKeyChar() <= '9')))
				{
					String toCompare = "";
					if (null != textField.getText()
							&& textField.getText().length() > 1)
						toCompare = textField.getText().toString();
					else
						toCompare = key;
					int i;
					for (i = 0; i < items.length; i++) 
					{
						if (items[i].toLowerCase().startsWith(toCompare.toLowerCase())) 
						{
							NOVCodeCombo.this.removeAllItems();
							String[] sItems = getCompToRefillCombo(toCompare);
							for (int k = 0; k < sItems.length; k++)
								NOVCodeCombo.this.addItem(sItems[k]);
							NOVCodeCombo.this.setSelectedItem(items[i]);
							if (sItems.length > 1) 
							{
								int length = items[i].length();
								textField.setSelectedTextColor(Color.WHITE);
								textField.setSelectionColor(Color.blue);
								textField.setSelectionStart(toCompare.length());
								textField.setSelectionEnd(length);
							}
							if (sItems.length < 5)
								NOVCodeCombo.this.setMaximumRowCount(sItems.length);
							else
								NOVCodeCombo.this.setMaximumRowCount(5);
							NOVCodeCombo.this.showPopup();
							break;
						}
						else
							textField.setText("");
					}
					if (i == items.length) 
					{
						if (NOVCodeCombo.this.isPopupVisible()) 
						{
							NOVCodeCombo.this.hidePopup();
						}
					}
				} 
				else if ((e.getKeyChar() == '\b' || e.getKeyCode() == KeyEvent.VK_DELETE)
						&& (textField.getText().length() >= 1)) 
				{
					String toCompare = "";
					if (textField.getText().length() >= 1)
						toCompare = textField.getText().toString();
					int i;
					for (i = 0; i < items.length; i++) {
						if (items[i].toLowerCase().startsWith(toCompare.toLowerCase())) 
						{
							doNotSetText = false;
							NOVCodeCombo.this.removeAllItems();
							String[] sItems = getCompToRefillCombo(toCompare);
							for (int k = 0; k < sItems.length; k++) {
								doNotSetText = false;
								NOVCodeCombo.this.addItem(sItems[k]);
							}
							if (sItems.length < 5)
								NOVCodeCombo.this
										.setMaximumRowCount(sItems.length);
							else
								NOVCodeCombo.this.setMaximumRowCount(5);
							NOVCodeCombo.this.showPopup();
							return;
						}
					}
					if (i == items.length) 
					{
						if (NOVCodeCombo.this.isPopupVisible()) 
						{
							NOVCodeCombo.this.hidePopup();
						}
					}
					
				} 
				else 
				{
					if (NOVCodeCombo.this.isPopupVisible()) 
					{
						NOVCodeCombo.this.hidePopup();
					}
				}
				if (textField.getText().length() == 0) 
				{
					NOVCodeCombo.this.removeAllItems();
					for (int j = 0; j < items.length; j++) 
					{
						doNotSetText = false;
						NOVCodeCombo.this.addItem(items[j]);
					}
					NOVCodeCombo.this.setMaximumRowCount(5);
					NOVCodeCombo.this.showPopup();
				}
				doNotSetText = true;
			}

			public String[] getCompToRefillCombo(String toCompare) 
			{
				List list = new LinkedList();
				for (int i = 0; i < items.length; i++) {
					if (items[i].toLowerCase().startsWith(
							toCompare.toLowerCase())) {
						list.add(items[i]);
					}
				}
				String[] strReturn = (String[]) list.toArray(new String[list
						.size()]);
				return strReturn;
			}

			/**
			 * keyTyped
			 */
			public void keyTyped(KeyEvent e) {
				// System.out.println("Key Typed...." + e.getKeyChar());
			}
		}
	}
	
	  public void setRequired(boolean required)
	    {
	        this.required=required;
	    }
	    public boolean isRequired()
	    {
	        return this.required;
	    }

	    public void paint(Graphics g) 
	    {
	        // TODO Auto-generated method stub
	        super.paint(g);
	        if(required)
	            Painter.paintIsRequired(this, g);
	    }
	    
	    private boolean           required         = false;
}
