/*================================================================================
                     Copyright (c) 2009 National Oilwell Varco
                     Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVTextField.java
 Package Name: com.nov.iman.commands.newitem
 ================================================================================
 Modification Log
 ================================================================================
 Revision       Date         Author        Description  
 1.0            2009/01/29   harshadam     Initial Creation
 1.1            2009/02/03   varunk        Code Cleanup     
 1.2            2009/02/03   harshadam     Review & Code Format                     
 ================================================================================*/
package com.nov.rac.smdl.common;

import javax.swing.JTextField;
import java.awt.event.*;

/**
 * NOVTextField : class NOVTextField extends JTextField
 * 
 * @author harshadam
 */
public class NOVTextField extends JTextField
{
    /**
     * NOVTextField : constructor NOVTextField
     * 
     * @param text :
     *            string
     * @param argUOMComboFlag :
     *            boolean
     * @param argUOWComboFlag :
     *            boolean
     * @param argitemPanel :
     *            {@link NOVDashAttributePanel}
     */
    public NOVTextField(String text, boolean argUOMComboFlag,
            boolean argUOWComboFlag, NOVComboBox argitemPanel)
    {
        super(text);
        itemPanel = argitemPanel;
        /**
         * addMouseListener
         */
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me )
            {
                mouseFlag = true;
            }
        });
        /**
         * addFocusListener
         */
        addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent fe )
            {
                if ( mouseFlag )
                    mouseFlag = false;
                else
                    selectAll();
            }

            public void focusLost(FocusEvent fe )
            {
                super.focusLost(fe);
                itemPanel.panelFocusLost();
            }
        });
    }
    public NOVTextField(String text, boolean argUOMComboFlag,
			boolean argUOWComboFlag, NOVCodeCombo argitemPanel) {
		super(text);
		UOMComboFlag = argUOMComboFlag;
		UOWComboFlag = argUOWComboFlag;
		codeComboPanel = argitemPanel;
		/**
		 * addMouseListener
		 */
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				mouseFlag = true;
			}
		});
		/**
		 * addFocusListener
		 */
		addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent fe) {
				if (mouseFlag)
					mouseFlag = false;
				else
					selectAll();
			}

			public void focusLost(FocusEvent fe) {
				// TODO Auto-generated method stub
				super.focusLost(fe);
				codeComboPanel.panelFocusLost(/* UOMComboFlag, UOWComboFlag */);
			}
		});
	}
    
    private static final long serialVersionUID = 1L;
    private boolean           mouseFlag;
    private NOVComboBox       itemPanel;
    private boolean UOMComboFlag;
	private boolean UOWComboFlag;
	private NOVCodeCombo codeComboPanel;
}
