package com.nov.rac.smdl.common;


import java.awt.Frame;

import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.createsmdl.NOVSMDLNewEditDialog;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVAddCodeButtonListener implements SelectionListener
{
	private NOVSMDLTable m_smdlTable;
	private NOVSMDLNewEditDialog m_newEditDlg;
	private TCComponent m_dpComponent = null;
	private Shell m_shell;
	public NOVAddCodeButtonListener(NOVSMDLTable smdlTable)
	{
		m_smdlTable = smdlTable;
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void widgetSelected(SelectionEvent event) 
	{
		Object target=event.getSource();
		if (target instanceof Button)
		{
			Button addCodeButton= (Button)target;
			m_shell = addCodeButton.getShell();
			m_shell.setCursor(new Cursor(m_shell.getDisplay(),SWT.CURSOR_WAIT));
			m_dpComponent=(TCComponent)addCodeButton.getData();
			invokeDocEditDialog();
		}
	}
	protected void invokeDocEditDialog()
	{
		final Display display = Display.getCurrent();
		
		final Shell localShell = new Shell(m_shell,SWT.EMBEDDED | SWT.APPLICATION_MODAL | SWT.NO_BACKGROUND);
		
	    Frame frame = SWT_AWT.new_Frame(localShell);
		
	    m_newEditDlg = new NOVSMDLNewEditDialog(frame, false,
				null, m_smdlTable,m_dpComponent);
	    
		m_shell.setCursor(new Cursor(display,SWT.CURSOR_ARROW));
		
		//To fix SWT and SWING Modality issue
		localShell.setBounds(0, 0, 0, 0);
		localShell.setVisible(true);
		
		//Mohan - Invoking a thread inside another thread is not feasible solution so need to
	    //revisit the logic of disposing SWING and SWT dialogs(as per code review comments)
		SwingUtilities.invokeLater(new Runnable() 
		{
			public void run()
			{								
				m_newEditDlg.setModal(true);
				m_newEditDlg.setVisible(true);
				display.syncExec(new Runnable() {					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						if(localShell != null && !localShell.isDisposed())
						{
							localShell.dispose();
						}
					}
				});
			}
		});
	}
}
