package com.nov.rac.smdl.common;

import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLOrderDPTable extends NOVSMDLTable {
	
	private static final long serialVersionUID = 1L;
	
	private Registry m_Registry = null;
	
	public NOVSMDLOrderDPTable (TCSession session, String columns[])
    {
        super(session , columns);
        m_Registry =Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
    }
	
	public boolean isCellEditable(int row, int col) {
		
		String columnName=getColumnPropertyName(col);
	      
        String editableColumns = m_Registry.getString("DPRemove.LABEL");
		if (editableColumns.compareTo(columnName) == 0) {
            return true;
        } else {
            return false;
        }
	}
	
	protected String getDisplayNamesToken() {
		String strDisplayToken = getRegistry().getString("OrderDPTableColDispNames.PREF", null);
		return strDisplayToken;		
	}	
}
