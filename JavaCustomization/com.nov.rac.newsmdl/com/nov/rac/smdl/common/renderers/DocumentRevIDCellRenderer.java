package com.nov.rac.smdl.common.renderers;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocumentRevIDCellRenderer extends AbstractTCTableCellRenderer {
    private static final long serialVersionUID = 1L;
	private Registry appReg;

    public DocumentRevIDCellRenderer()
    {
        super();
        alternateBackground = new Color(191,214,248);
       /* alternateBackground = appReg.getColor("anotherBackground",
                alternateBackground);*/
        
         appReg=Registry.getRegistry("com.nov.rac.smdl.common.common");
    }

    protected String getDisplayText(Object obj)
    {
    	String docRevId = null;
        if(obj == null)
        {
            return "";
        } else if(obj instanceof TCComponentItemRevision )
        {
        	
            try {
				 docRevId = ((TCComponentItemRevision)obj).getProperty("item_revision_id");
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return docRevId;
        
    }

    public Component getTableCellRendererComponent(JTable jtable ,
            Object obj , boolean flag , boolean flag1 , int i , int j )
    {
    	TCComponent theComponent = null;
       /* if ( jtable instanceof TCTable )
        {
            if ( obj instanceof AIFComponentContext )
            {
                TCcomponent = (TCComponent) ((AIFComponentContext) obj)
                        .getComponent();
            }
            else if ( null != (TCComponent) ((TCTable) jtable)
                    .getRowComponent(i) )
                TCcomponent = (TCComponent) ((TCTable) jtable)
                        .getRowComponent(i);
            else
                TCcomponent = (TCComponent) ((NOVSMDLTable) jtable).mapRowComp
                        .get(Integer.toString(i));
        }*/
    	
		if (obj instanceof TCComponentItemRevision) {
			theComponent = (TCComponent) obj;
		}
         
        
    	
        if ( flag )
        {
            super.setForeground(jtable.getSelectionForeground());
            super.setBackground(jtable.getSelectionBackground());
        }
        else
        {
            super.setForeground(jtable.getForeground());
            super.setBackground(i % 2 != 1 ? jtable.getBackground()
                    : alternateBackground);
        }
        if ( flag1 )
        {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if ( jtable.isCellEditable(i, j) )
            {
                super.setForeground(UIManager
                        .getColor("Table.focusCellForeground"));
                super.setBackground(UIManager
                        .getColor("Table.focusCellBackground"));
            }
        }
        else
        {
            setBorder(noFocusBorder);
        }
        Icon icon = getDisplayIcon(theComponent, obj);
        String s = getDisplayText(obj);
        setText(s != null ? s : "");
        setIcon(icon);
        int k = getFontMetrics(getFont()).stringWidth(getText());
        int l = jtable.getCellRect(i, j, false).width;
        if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
            setToolTipText(s);
        else
            setToolTipText(null);
        return this;
    }

    protected void initiateIcons()
    {
    }

	@Override
	protected Icon getDisplayIcon(TCComponent arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}
}
