package com.nov.rac.smdl.common.renderers;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.teamcenter.rac.util.Registry;

public class TableHeaderRenderer implements TableCellRenderer, MouseListener {

	private static final long	serialVersionUID = 1L;
	private JCheckBox			m_checkBox;
	private boolean 			m_mousePressed = false;
	private int					m_column = -1;
	
	private Registry 			m_Registry = null;

	public TableHeaderRenderer(ActionListener actionListener) {
		super();
		
		m_checkBox = new JCheckBox();
		m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
		
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.close.close");
		m_checkBox.setText(m_Registry.getString("UnlockRev_COL.Label"));
		
		m_checkBox.addActionListener(actionListener);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
	      boolean isSelected, boolean hasFocus, int row, int column) 
	{
		JTableHeader header = table.getTableHeader();
		header.setLayout(new BorderLayout()); 
		
		m_checkBox.setForeground(header.getForeground());   
		m_checkBox.setBackground(header.getBackground());   
		m_checkBox.setFont(header.getFont());   

		header.addMouseListener(this);
		m_column = column;
	    return m_checkBox;
    }
	
	protected void handleClickEvent(MouseEvent mouseevent) {
		if (m_mousePressed) {
			m_mousePressed = false;
			JTableHeader header = (JTableHeader) (mouseevent.getSource());
			JTable tableView = header.getTable();
			TableColumnModel columnModel = tableView.getColumnModel();

			int viewColumn = columnModel.getColumnIndexAtX(mouseevent.getX());

			int column = tableView.convertColumnIndexToModel(viewColumn);

			if (viewColumn == this.m_column && mouseevent.getClickCount() == 1
					&& column != -1) {
				m_checkBox.doClick();
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
		handleClickEvent(mouseevent);
		((JTableHeader) mouseevent.getSource()).repaint();
	}

	@Override
	public void mouseEntered(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
		m_mousePressed = true;
	}

	@Override
	public void mouseReleased(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
	}
}

