package com.nov.rac.smdl.common.renderers;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocumentDescCellRenderer extends AbstractTCTableCellRenderer
{
    private static final long serialVersionUID = 1L;
	private Registry appReg;

    public DocumentDescCellRenderer()
    {
        super();
        alternateBackground = new Color(191,214,248);
        appReg=Registry.getRegistry("com.nov.rac.smdl.common.common");
    }

   
     
    protected String getDisplayText(Object obj, JTable jtable,int iSelectedRow)
    {
    	String sDocRevDesc = null;
    	
        if(obj == null || (obj instanceof String))
        {
        	int idocColumn =((NOVSMDLTable)jtable).getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
        	TCComponentItem documentItem = null;
        	Object docObj= ((NOVSMDLTable)jtable).getValueAtDataModel(iSelectedRow, idocColumn );
        	
             
             if(docObj instanceof TCComponentItem )
             {
          	   documentItem  = (TCComponentItem) docObj;
             }
			
        	if(documentItem!=null)
        	{
				try 
				{
					//TCComponentItemRevision docRevision = documentItem.getLatestItemRevision();
					
					TCComponent[] doc_Rev_list = documentItem.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					TCComponentItemRevision docRevision = (TCComponentItemRevision) doc_Rev_list[doc_Rev_list.length -1];
				
					//update desc column 
					
					if(docRevision != null)
					{
						sDocRevDesc = docRevision.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC).getStringValue();
					}
				} catch (TCException e1) 
				{
					e1.printStackTrace();
				}
        	}
			
        	return sDocRevDesc;
        }
       
        return sDocRevDesc;
        
    }

    public Component getTableCellRendererComponent(JTable jtable ,
            Object obj , boolean flag , boolean flag1 , int i , int j )
    {
    	TCComponent theComponent = null;
         	
		if (obj instanceof TCComponentItemRevision) {
			theComponent = (TCComponent) obj;
		}
         
         if ( flag )
        {
            super.setForeground(jtable.getSelectionForeground());
            super.setBackground(jtable.getSelectionBackground());
        }
        else
        {
            super.setForeground(jtable.getForeground());
            super.setBackground(i % 2 != 1 ? jtable.getBackground()
                    : alternateBackground);
        }
        if ( flag1 )
        {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if ( jtable.isCellEditable(i, j) )
            {
                super.setForeground(UIManager
                        .getColor("Table.focusCellForeground"));
                super.setBackground(UIManager
                        .getColor("Table.focusCellBackground"));
            }
        }
        else
        {
            setBorder(noFocusBorder);
        }
        Icon icon = getDisplayIcon(theComponent, obj);
      
        String s = getDisplayText(obj,jtable,i);
        setText(s /*!= null ? s : ""*/);
        setIcon(icon);
        
        if(s != null)
        {
	        int k = getFontMetrics(getFont()).stringWidth(getText());
	        int l = jtable.getCellRect(i, j, false).width;
	        if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
	            setToolTipText(s);
        }
        else
        {
            setToolTipText(null);
        }
        return this;
    }

    
	protected void initiateIcons()
    {
    }



	@Override
	protected Icon getDisplayIcon(TCComponent arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}
}

