package com.nov.rac.smdl.common.renderers;

import java.awt.Component;

import javax.swing.AbstractButton;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;


public class NOVTableCheckBoxCellRenderer extends DefaultCellEditor implements TableCellRenderer {

	private static final long serialVersionUID = 1L;
	private JCheckBox m_checkBox = null;

	public NOVTableCheckBoxCellRenderer() {
		super(new JCheckBox());
		
		m_checkBox = ((JCheckBox) getComponent());
		m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
//		m_checkBox.setText("Yes");
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
	      boolean isSelected, boolean hasFocus, int row, int column) 
	{
	    if (isSelected) {
	    	m_checkBox.setForeground(table.getSelectionForeground());
	    	m_checkBox.setBackground(table.getSelectionBackground());
	    } else {
	    	m_checkBox.setForeground(table.getForeground());
	    	m_checkBox.setBackground(table.getBackground());
	    }
	    if(value != null)
	    	m_checkBox.setSelected(((Boolean) value).booleanValue());
	    
	    return m_checkBox;
    }
}

