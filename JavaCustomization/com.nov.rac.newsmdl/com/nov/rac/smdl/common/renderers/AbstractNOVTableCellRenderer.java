package com.nov.rac.smdl.common.renderers;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.nov.rac.smdl.common.NOVSMDLTable;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;

public abstract class AbstractNOVTableCellRenderer extends AbstractTCTableCellRenderer 
{
	private static final long serialVersionUID = 1L;

	public AbstractNOVTableCellRenderer()
	{
		
	}
	
	
	protected abstract String getCellValue(JTable jtable,Object obj , int iRow, int iCol); 
	
	
	 public Component getTableCellRendererComponent(JTable jtable, Object obj, boolean flag, boolean flag1, int i, int j)
	    {
	        String rowType = null;
	        if(jtable instanceof NOVSMDLTable)
	        {
	        	
	        	//rowType = (String)((NOVSearchTable)jtable).getRowType(i);
	        	rowType = getCellValue(jtable,obj,i,j);
	        }
	        setbackground(jtable, flag, flag1, i, j);
	       
	        Icon icon = getDisplayIcon(rowType, obj);
	        
	        setText(jtable, obj, i, j, icon);
	       
	        return this;
	    }

	protected void setText(JTable jtable, Object obj, int i, int j, Icon icon) {
		String s = getDisplayText(obj);

		setText(s != null ? s : "");
		setIcon(icon);
		int k = getFontMetrics(getFont()).stringWidth(getText());
		int l = jtable.getCellRect(i, j, false).width;
		if(k + (icon != null ? icon.getIconWidth() : 0) > l - 8)
		    setToolTipText(s);
		else
		    setToolTipText(null);
	}

	protected void setbackground(JTable jtable, boolean flag, boolean flag1,
			int i, int j) {
		if(flag)
		{
		    super.setForeground(jtable.getSelectionForeground());
		    super.setBackground(jtable.getSelectionBackground());
		} else
		{
		    super.setForeground(jtable.getForeground());
		    super.setBackground(i % 2 != 1 ? jtable.getBackground() : alternateBackground);
		}
		if(flag1)
		{
		    setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
		    if(jtable.isCellEditable(i, j))
		    {
		        super.setForeground(UIManager.getColor("Table.focusCellForeground"));
		        super.setBackground(UIManager.getColor("Table.focusCellBackground"));
		    }
		} else
		{
		    setBorder(noFocusBorder);
		}
	}
	 
	 protected abstract Icon getDisplayIcon(String rowType, Object obj);
	 

}
