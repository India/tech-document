package com.nov.rac.smdl.common.renderers;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.Registry;

public class NOVSMDLCodeRenderer extends AbstractTCTableCellRenderer
{
    private static final long serialVersionUID = 1L;
	private Registry appReg;

    public NOVSMDLCodeRenderer()
    {
        super();
        alternateBackground = new Color(191,214,248);
        appReg=Registry.getRegistry("com.nov.rac.smdl.common.common");
    }

   
     
    protected String getDisplayText(Object obj, JTable jtable,int iSelectedRow)
    {
    	String s = (String)obj;
        String[] codeList = NOVCodeHelper.getCodeList();
        for (int inx = 0; inx < codeList.length; inx++)
        {
        	String sNovCode = codeList[inx].substring(0, codeList[inx].indexOf('='));
        	if(sNovCode.compareToIgnoreCase(s) == 0)
        	{
        		s = codeList[inx];
        		break;
        	}  	
		}
        return s;        
    }

    public Component getTableCellRendererComponent(JTable jtable ,
            Object obj , boolean flag , boolean flag1 , int i , int j )
    {
    	TCComponent theComponent = null;
         	
		if (obj instanceof TCComponentItem) 
		{
			theComponent = (TCComponent) obj;
		}
         
         if ( flag )
        {
            super.setForeground(jtable.getSelectionForeground());
            super.setBackground(jtable.getSelectionBackground());
        }
        else
        {
            super.setForeground(jtable.getForeground());
            super.setBackground(i % 2 != 1 ? jtable.getBackground()
                    : alternateBackground);
        }
        if ( flag1 )
        {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if ( jtable.isCellEditable(i, j) )
            {
                super.setForeground(UIManager
                        .getColor("Table.focusCellForeground"));
                super.setBackground(UIManager
                        .getColor("Table.focusCellBackground"));
            }
        }
        else
        {
            setBorder(noFocusBorder);
        }
        Icon icon = getDisplayIcon(theComponent, obj);
      
        String novCode = (String)jtable.getValueAt(i, j);        
        String s = getDisplayText(novCode,jtable,i);
        setText(s /*!= null ? s : ""*/);
        setIcon(icon);
        
        if(s != null)
        {
	        int k = getFontMetrics(getFont()).stringWidth(getText());
	        int l = jtable.getCellRect(i, j, false).width;
	        if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
	            setToolTipText(s);
        }
        else
        {
            setToolTipText(null);
        }
        return this;
    }

    
	protected void initiateIcons()
    {
    }



	@Override
	protected Icon getDisplayIcon(TCComponent arg0, Object arg1) {
		// TODO Auto-generated method stub
		return null;
	}
}

