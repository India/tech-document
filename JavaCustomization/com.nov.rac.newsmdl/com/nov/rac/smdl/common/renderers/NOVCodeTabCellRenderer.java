package com.nov.rac.smdl.common.renderers;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.iTextField;

public class NOVCodeTabCellRenderer implements TableCellRenderer
{
	
	private TextField m_iTextField ;	
	private static final long serialVersionUID = -697898812006419074L;
	private Color               m_alternateBackground = Color.white;
    public NOVCodeTabCellRenderer()
    {
    	m_alternateBackground = new Color(191,214,248);
    	m_iTextField = new TextField();
    }  
    protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
    	if ( comp != null )
    	{
    		ImageIcon compIcon;
    		String strCompType = comp.getType();
    		compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
    		return compIcon;
    	}
    	else
    		return null;
    }
    protected void setValueIcon(Object arg0 , Icon arg1 )
	{
    	if ( arg0 != null )
	    {
    		m_iTextField.setText(arg0.toString());
	    }
	}    
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
    	 String s = (String)value;
    	 m_iTextField.setText(s != null ? s : "");
    	 if ( isSelected )
         {
    		m_iTextField.setForeground(table.getSelectionForeground());
         	m_iTextField.setBackground(table.getSelectionBackground());
         }
         else
         {
        	m_iTextField.setForeground(table.getForeground());
         	m_iTextField.setBackground(row % 2 != 1 ? table.getBackground() : m_alternateBackground);
         }
         if ( hasFocus )
         {
        	 m_iTextField.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
         	if ( table.isCellEditable(row, column) )
         	{
         		m_iTextField.setForeground(UIManager.getColor("Table.focusCellForeground"));
         		m_iTextField.setBackground(UIManager.getColor("Table.focusCellBackground"));
         	}
         }
         else
         {
        	 m_iTextField.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
         }
    	 return m_iTextField;
    }   
	private class TextField extends iTextField
    {
    	/**
		 * 
		 */
    	public TextField()
    	{
    		super();
    	}
		private static final long serialVersionUID = 1L;
    }
}