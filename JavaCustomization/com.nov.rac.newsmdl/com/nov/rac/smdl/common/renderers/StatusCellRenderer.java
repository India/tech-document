package com.nov.rac.smdl.common.renderers;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

public class StatusCellRenderer extends AbstractTCTableCellRenderer
{
	 private Registry appReg;

	public StatusCellRenderer()
     {
         super();
         appReg=Registry.getRegistry("com.nov.rac.smdl.common.common");
     }
	
        private static final long serialVersionUID = 1L;

        public Component getTableCellRendererComponent(
                JTable jtable , Object obj , boolean flag ,
                boolean flag1 , int i , int j )
        {
            if ( flag )
            {
                super.setForeground(jtable
                        .getSelectionForeground());
                super.setBackground(jtable
                        .getSelectionBackground());
            }
            else
            {
                super.setForeground(jtable.getForeground());
                super.setBackground(i % 2 != 1 ? jtable
                        .getBackground() : alternateBackground);
            }
            if ( flag1 )
            {
                setBorder(UIManager
                        .getBorder("Table.focusCellHighlightBorder"));
                if ( jtable.isCellEditable(i, j) )
                {
                    super
                            .setForeground(UIManager
                                    .getColor("Table.focusCellForeground"));
                    super
                            .setBackground(UIManager
                                    .getColor("Table.focusCellBackground"));
                }
            }
            else
            {
                setBorder(noFocusBorder);
            }
            Icon icon = getDisplayIcon(null, obj);
            String s = getDisplayText(obj);
            setText(s != null ? s : "");
            setIcon(icon);
            int k = getFontMetrics(getFont()).stringWidth(
                    getText());
            int l = jtable.getCellRect(i, j, false).width;
            if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
                setToolTipText(s);
            else
                setToolTipText(null);
            return this;
        }

        protected Icon getDisplayIcon(TCComponent arg0 ,
                Object arg1 )
        {
            ImageIcon statusIcon = null;
            if ( arg1 instanceof String )
            {
                String status = ((String) arg1).trim();
                if ( status.length() > 0 )
                {
                    statusIcon = appReg.getImageIcon(("status.ICON"));
                }
            }
            return statusIcon;
        }

        protected void initiateIcons()
        {
        }

        protected void setValueIcon(Object arg0 , Icon arg1 )
        {
            ImageIcon statusIcon =
            	appReg.getImageIcon(/*appReg
                            .getString*/("status.ICON"));
            if ( arg0 != null )
                iconTable.put(arg0.toString(), statusIcon);
        }
    }