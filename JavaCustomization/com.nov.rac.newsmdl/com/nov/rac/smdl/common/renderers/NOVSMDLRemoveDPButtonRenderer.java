package com.nov.rac.smdl.common.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import com.teamcenter.rac.util.Registry;

 public class NOVSMDLRemoveDPButtonRenderer implements TableCellRenderer 
{
	private static final long serialVersionUID = 1L;
	private JButton m_tblButton;
	private Registry m_registry = null;
	private ImageIcon m_minusIcon = null;
	
	public NOVSMDLRemoveDPButtonRenderer()
	{
    	m_registry = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");

    	try {
    	    m_minusIcon=m_registry.getImageIcon("minus.ICON");        	
    	} catch ( Exception ie ) {
    	    System.out.println(ie);
    	}
		
		m_tblButton = new JButton();
		m_tblButton.setMinimumSize(new Dimension(18 , 20));
		m_tblButton.setPreferredSize(new Dimension(18 , 20));
		m_tblButton.setMaximumSize(new Dimension(18 , 20));
		m_tblButton.setBackground(Color.WHITE);
		
		m_tblButton.setIcon(m_minusIcon);
		m_tblButton.setActionCommand("removeRow");
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) 
	{
		return m_tblButton;
	}
 }