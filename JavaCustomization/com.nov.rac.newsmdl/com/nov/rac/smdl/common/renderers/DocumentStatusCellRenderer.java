package com.nov.rac.smdl.common.renderers;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class DocumentStatusCellRenderer extends AbstractTCTableCellRenderer
{
	 private Registry appReg;

	public DocumentStatusCellRenderer()
    {
        super();
        appReg=Registry.getRegistry("com.nov.rac.smdl.common.common");
    }
	
       private static final long serialVersionUID = 1L;

       public Component getTableCellRendererComponent(
               JTable jtable , Object obj , boolean flag ,
               boolean flag1 , int i , int j )
       {
           if ( flag )
           {
               super.setForeground(jtable
                       .getSelectionForeground());
               super.setBackground(jtable
                       .getSelectionBackground());
           }
           else
           {
               super.setForeground(jtable.getForeground());
               super.setBackground(i % 2 != 1 ? jtable
                       .getBackground() : alternateBackground);
           }
           if ( flag1 )
           {
               setBorder(UIManager
                       .getBorder("Table.focusCellHighlightBorder"));
               if ( jtable.isCellEditable(i, j) )
               {
                   super
                           .setForeground(UIManager
                                   .getColor("Table.focusCellForeground"));
                   super
                           .setBackground(UIManager
                                   .getColor("Table.focusCellBackground"));
               }
           }
           else
           {
               setBorder(noFocusBorder);
           }
           
           //added to fetch the locked revision
           String docStatus = "";
           TCComponentItem documentItem= null;
           TCComponentItemRevision documentItemRev =null;
           int idocColumn =((NOVSMDLTable)jtable).getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REF);
           Object docObj= ((NOVSMDLTable)jtable).getValueAtDataModel(i, idocColumn );
        
           if(docObj instanceof TCComponentItem )
           {
        	   documentItem  = (TCComponentItem) docObj;
           }
           
           if(documentItem != null)
           {
	           int docRevColumn =((NOVSMDLTable)jtable).getColumnIndexFromDataModel(NOV4ComponentSMDL.DOCUMENT_REV_REF);
	           Object docRevObj =((NOVSMDLTable)jtable).getValueAtDataModel(i, docRevColumn );
	           if(docRevObj instanceof TCComponentItemRevision)
	           {
	        	   documentItemRev= (TCComponentItemRevision) docRevObj;
	           }
	           
	           try 
	    	   {
	        	   if(documentItemRev== null)
	               {
	        		   //documentItemRev= documentItem.getLatestItemRevision();
	        		   
	        		   TCComponent[] doc_Rev_list = documentItem.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
	        		   documentItemRev = (TCComponentItemRevision) doc_Rev_list[doc_Rev_list.length -1];	               
	               }
	        	   TCComponent[] statusList = documentItemRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();						
					if(statusList.length > 0)
					{
						docStatus = (String)statusList[statusList.length-1].getProperty(NOVSMDLConstants.DOCUMENT_STATUS_NAME);
					}
	           
				   
	    	   } catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	    	   }
           }
          
       		
            
           
           Icon icon = getDisplayIcon(null, docStatus);
           String s = getDisplayText(docStatus);
           setText(s != null ? s : "");
           setIcon(icon);
           int k = getFontMetrics(getFont()).stringWidth(
                   getText());
           int l = jtable.getCellRect(i, j, false).width;
           if ( k + (icon != null ? icon.getIconWidth() : 0) > l - 8 )
               setToolTipText(s);
           else
               setToolTipText(null);
           return this;
       }

       protected Icon getDisplayIcon(TCComponent arg0 ,
               Object arg1 )
       {
           ImageIcon statusIcon = null;
           if ( arg1 instanceof String )
           {
               String status = ((String) arg1).trim();
               if ( status.length() > 0 )
               {
                   statusIcon = appReg.getImageIcon(("status.ICON"));
            	  /* String release_status_full_name = "release_status_list." + status;
            	   statusIcon = TCTypeRenderer.getTypeIcon(release_status_full_name, null);*/
                   
               }
           }
           return statusIcon;
       }

       protected void initiateIcons()
       {
       }

       protected void setValueIcon(Object arg0 , Icon arg1 )
       {
           ImageIcon statusIcon =
           	appReg.getImageIcon(/*appReg
                           .getString*/("status.ICON"));
           if ( arg0 != null )
               iconTable.put(arg0.toString(), statusIcon);
       }
   }