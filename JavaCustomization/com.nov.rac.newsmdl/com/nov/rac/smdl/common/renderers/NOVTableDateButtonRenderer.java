package com.nov.rac.smdl.common.renderers;

import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.util.DateButton;

public class NOVTableDateButtonRenderer extends DateButton implements
        TableCellRenderer

{
	public NOVTableDateButtonRenderer()
	{
		super();
		this.setDisplayFormat("dd-MMM-yyyy");
	}
    /**
     *
     */
    private static final long serialVersionUID = 1L;

   
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
    	 if ( value != null )
    	 {
	        if ( value instanceof DateButton )
	        {
	                if ( value instanceof Component )
	                {
	                    this.setDate(((DateButton) value).getDate());
	                    this.setText(((DateButton) value).getText());
	                }
	                else
	                {
	                    this.setDate(((DateButton) value).getDate());
	                    return this;
	                }
	            this.setBorderPainted(false);
	           
	        }
	        else if( value instanceof Date)
	        {
	                this.setDate((Date)value);
	                this.setBorderPainted(false);
	        }
	        else if( value instanceof String)
	        {
	                this.setDate(value.toString());
	                this.setBorderPainted(false);
	        }
    	 }
		return this;  
    }


	
	
}