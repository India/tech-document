package com.nov.rac.smdl.common;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class NOVTextFieldLimit extends PlainDocument 
{
	private static final long serialVersionUID = 1L;
	private int limit;
    private boolean toUppercase = false;	   
    public NOVTextFieldLimit(int limit) 
    {
    	super();
    	this.limit = limit;
    }	    
    public NOVTextFieldLimit(int limit, boolean upper) 
    {
    	super();
    	this.limit = limit;
    	toUppercase = upper;
    }	  
    public void insertString(int offset, String  str, AttributeSet attr)
    throws BadLocationException 
    {
    	if (str == null) 
    		return;
    	if ((getLength() + str.length()) <= limit) 
    	{
    		if (!isIntNumber(str))
    		{
    			if (toUppercase) 
    				str = str.toUpperCase();
    			super.insertString(offset, str, attr);
    		}
    	}
    }
    public boolean isIntNumber(String num)
    {
    	try
    	{
    		Integer.parseInt(num);
    	} 
    	catch(NumberFormatException nfe) 
    	{
    		return false;
    	}
    	return true;
    }
}
