package com.nov.rac.smdl.common;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.createsmdl.NOVSMDLNewEditDialog;
import com.nov.rac.smdl.utilities.NOVGroupDataHelper;
import com.nov.rac.smdl.utilities.NOVUserDataHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;


public class NOVSMDLTableMouseListener extends MouseAdapter 
{
	private NOVSMDLTable smdlTable = null;
	private NOVTablePopupMenu popupMenu = null;
	private TCSession session;
	private NOVSMDLNewEditDialog    m_newEditDlg;
	private  TCComponent m_dpComp;
	private Registry m_reg;
	private Shell m_modalShell;
	public NOVSMDLTableMouseListener(TCSession tcsession)
	{
		session = tcsession;
		m_reg = Registry.getRegistry(this);
	}
	public void mousePressed(MouseEvent e) 
	{
		// TODO Auto-generated method stub
		smdlTable = (NOVSMDLTable) e.getComponent();
		if (e.isPopupTrigger())
		{
			int iSelectedCol = smdlTable.getColumnModel().getColumnIndexAtX(e.getX());
			int iGroupCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.GROUP_NAME);
			int iUserCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);

			if(iSelectedCol == iGroupCol)
			{
				Set<String> groupsSet = NOVGroupDataHelper.getGroupData().keySet();
				String[] sGroupData = (String[]) groupsSet.toArray(new String[groupsSet.size()]);
				String sGroupTitle = m_reg.getString("groupDialog.TITLE");
				showListPopupDialog(e,iGroupCol,sGroupData,sGroupTitle);
			}
			else if(iSelectedCol == iUserCol)
			{
				String sUserDialogTitle=null;
				//String sGroup = smdlTable.getValueAt(smdlTable.getSelectedRow(), iGroupCol).toString();
				//Vector<String> sUsers = NOVUserDataHelper.getUsers(sGroup);
				Vector<String> sUsers = NOVUserDataHelper.getUpdateUserList();
				String[] sUserData = sUsers.toArray(new String[sUsers.size()]);
//				if(sGroup!=null && sGroup.trim().length()>0)
//				{
//					sUserDialogTitle = m_reg.getString("groupUsersDialog.TITLE");
//				}
//				else
//				{
					sUserDialogTitle = m_reg.getString("allUsersDialog.TITLE");
				//}
				showListPopupDialog(e,iUserCol,sUserData,sUserDialogTitle);
			}
			else
			{
			showPopup(e);
		}
		}
		else
		{
			int clickCnt = e.getClickCount();
			if ( clickCnt == 2)
			{
				launchDocumentEditDialog(e);
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) 
	{
		// TODO Auto-generated method stub
		if (e.isPopupTrigger())
		{
			int iSelectedCol = smdlTable.getColumnModel().getColumnIndexAtX(e.getX());
			int iGroupCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.GROUP_NAME);
			int iUserCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_RESPONSIBLE);

			if(iSelectedCol == iGroupCol)
			{
				Set<String> groupsSet = NOVGroupDataHelper.getGroupData().keySet();
				String[] sGroupData = (String[]) groupsSet.toArray(new String[groupsSet.size()]);
				String sGroupTitle = m_reg.getString("groupDialog.TITLE");
				showListPopupDialog(e,iGroupCol,sGroupData,sGroupTitle);
			}
			else if(iSelectedCol == iUserCol)
			{
				String sUserDialogTitle=null;
				//String sGroup = smdlTable.getValueAt(smdlTable.getSelectedRow(), iGroupCol).toString();
				//Vector<String> sUsers = NOVUserDataHelper.getUsers(sGroup);
				Vector<String> sUsers = NOVUserDataHelper.getUpdateUserList();
				String[] sUserData = sUsers.toArray(new String[sUsers.size()]);
			
				/*if(sGroup!=null && sGroup.trim().length()>0)
				{
					sUserDialogTitle = m_reg.getString("groupUsersDialog.TITLE");
				}
				else
				{*/
					sUserDialogTitle = m_reg.getString("allUsersDialog.TITLE");
				//}
				showListPopupDialog(e,iUserCol,sUserData,sUserDialogTitle);
			}
			else
			{
			showPopup(e);
		}
	}
	}
	public void showPopup(MouseEvent e) 
	{
		if (smdlTable.isEnabled()) 
		{
			Point p = new Point(e.getX(), e.getY());
			
			int iSelectedRow = smdlTable.rowAtPoint(p);
			if (iSelectedRow >= 0 && iSelectedRow < smdlTable.getRowCount()) 
			{
				popupMenu  = new NOVTablePopupMenu(session,smdlTable,iSelectedRow);
				if (popupMenu != null	&& popupMenu.getComponentCount() > 0) 
				{
					popupMenu.show(smdlTable, p.x, p.y);
				}
			}
		}
	}
	public void launchDocumentEditDialog(MouseEvent e) 
	{
		String[] documentEditDialogColumns=m_reg.getStringArray("documentEditColumns.Name");
		final int iDocCol = smdlTable.getColumnIndex(NOV4ComponentSMDL.DOCUMENT_REF);
		int iSelectedCol = smdlTable.getSelectedColumn();
		
		if(iSelectedCol !=-1)
		{
			String columnName =smdlTable.getColumnPropertyName(iSelectedCol);
			if (Arrays.asList(documentEditDialogColumns).contains(columnName) ) 
			{
				final Display display = Display.getDefault();
				display.syncExec(new Runnable() 
				{
					public void run() 
					{					
						int rowIndex = smdlTable.getSelectedRow();
						if ( rowIndex >= 0 )
						{
							TCComponent docComp = null;
							Object objDoc = smdlTable.getValueAt(rowIndex, iDocCol);
							
							if(objDoc.toString().trim().length()>0)
							{
								docComp = (TCComponent)objDoc;
							}
							Shell activeShell = display.getActiveShell();
							activeShell.setCursor(new Cursor(display,SWT.CURSOR_WAIT));
							
							m_modalShell = new Shell(activeShell,SWT.EMBEDDED | SWT.APPLICATION_MODAL | SWT.NO_BACKGROUND);
							Frame frame = SWT_AWT.new_Frame(m_modalShell);
							
							m_newEditDlg = new NOVSMDLNewEditDialog(frame , true , docComp,smdlTable,m_dpComp);
							
							activeShell.setCursor(new Cursor(display,SWT.CURSOR_ARROW));
							
							//To fix SWT and SWING Modality issue
							m_modalShell.setBounds(0, 0, 0, 0);
							m_modalShell.setVisible(true);
						}
					}
				});
				
				m_newEditDlg.setVisible(true);
				
				display.syncExec(new Runnable() 
				{
					public void run() 
					{
						if(m_modalShell != null && !m_modalShell.isDisposed())
						{
							m_modalShell.dispose();
						}
					}
				});
			}
		}
		else
		{
			return;
		}
	}
	public void setDPComponent(TCComponent dpComp)
	{
		m_dpComp = dpComp;
	}
	private void showListPopupDialog(final MouseEvent me, final int iSelectedCol,final String[] sValuesList,final String sDialogTitle)
    {
		Display.getDefault().asyncExec(new Runnable() 
		{
			public void run()
			{
				Shell localShell = Display.getDefault().getActiveShell();
				NOVListPopUpDialog popupDialog = new NOVListPopUpDialog(localShell);
				popupDialog.create();
				popupDialog.setTitle(sDialogTitle);
				popupDialog.populateList(sValuesList);
				
				int x = smdlTable.getLocationOnScreen().x + me.getX();
                int y = smdlTable.getLocationOnScreen().y + me.getY();
                popupDialog.getShell().setLocation(x, y);
                
				int iReturnCode = popupDialog.open();
				if (iReturnCode == 0)
				{
					int iSelectedRow = smdlTable.getSelectedRow();
					String sSelectedValue = popupDialog.getSelectedValue();
					smdlTable.setValueAt(sSelectedValue,iSelectedRow, iSelectedCol);
					smdlTable.dataModel.fireTableCellUpdated(iSelectedRow, iSelectedCol);
					smdlTable.clearSelection();
				}
			}
		});
    }
}