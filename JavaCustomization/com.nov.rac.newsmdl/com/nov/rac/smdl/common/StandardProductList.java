package com.nov.rac.smdl.common;

import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.noi.util.AutofilEditorl;

public class StandardProductList {

	private Vector vList = new Vector();
	private LinkedList productList = null;
	
	public StandardProductList() {
		try {
			URL plisturl = new URL("http://cetws.wch.nov.com:7081/cetgws/GetPPProductList.jsp");
			byte[] plistbuff = new byte[4096];
			InputStream is = plisturl.openStream();
			StringBuffer plistdata= new StringBuffer();

			do {
				int bread = is.read(plistbuff,0,4096);
				if (bread == 0) continue;
				if (bread == -1) break;
				plistdata.append(new String(plistbuff,0,bread));
				
			} while (is.available() >= 0);
			is.close();
			productList = new LinkedList();
			productList.add("");
			vList.add("");
			String[] plistarr = plistdata.toString().split("~");
			com.teamcenter.rac.util.ArraySorter.sort(plistarr);
			for (int i=0;i<plistarr.length;i++) {
				String[] obarr = plistarr[i].split("\\^");
				if (obarr != null && obarr.length > 1) {
					String prod = obarr[0].trim()+":"+obarr[1].trim();
					productList.add(prod);	
					vList.add(prod);
				}
			}
			
		}
		catch (Exception e) {
			System.out.println("Error get product list. "+e);
		}
	}
	
	public List getLinkedList() { return productList; }
	
	public String[] getStringArray() {
		
		/*vList.add("prod1");
		vList.add("prod2");*/
		
		String[] arr = new String[vList.size()];
		for (int i=0;i<vList.size();i++)
			arr[i] = (String)vList.get(i);
		return arr;
	}
	public Vector getVector() { return vList; }
}
