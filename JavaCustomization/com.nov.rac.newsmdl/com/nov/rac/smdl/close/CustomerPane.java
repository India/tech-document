package com.nov.rac.smdl.close;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.noi.util.components.JIBPlantLOVList;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class CustomerPane extends Composite {

	private Text m_txtCustomer;
	private Text m_txtPlant;
	private Text m_txtProduct;
	
	private Label m_lblCustomer;
	private Label m_lblPlant;
	private Label m_lblProduct; 
	
	private static JIBPlantLOVList m_soaPlantList = null;
	
	private Registry m_Registry;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public CustomerPane(Composite parent, int style) {
		super(parent, style);
		
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.close.close");
		
		createUI(this);

		populateLabels();

	}
	
	private void createUI(Composite parent) {
		
		setLayout(new GridLayout(2,false));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_lblCustomer = new Label(parent, SWT.NONE);
				
		m_txtCustomer = new Text(parent, SWT.BORDER);
		m_txtCustomer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		m_lblPlant = new Label(parent, SWT.NONE);
		
		m_txtPlant = new Text(parent, SWT.BORDER);
		m_txtPlant.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		m_lblProduct = new Label(parent, SWT.NONE);
		
		m_txtProduct = new Text(parent, SWT.BORDER);
		m_txtProduct.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		
	}
	
	private void populateLOVs() {

		if (m_soaPlantList == null) {
			m_soaPlantList = new JIBPlantLOVList();
			m_soaPlantList.init();
		}
	}
	

	private void populateLabels() {
		m_lblCustomer.setText(m_Registry.getString("SMDLCust.Label"));
		
		m_lblPlant.setText(m_Registry.getString("SMDLPlant.Label"));
		
		m_lblProduct.setText(m_Registry.getString("SMDLProduct.Label"));		
	}

	public void populateTextVals(TCComponent smdlRevision) {

		populateLOVs();
		TCComponent smdlItem;
		try {
			smdlItem = smdlRevision.getTCProperty(
					NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
			
			TCComponent dpObject = smdlItem.getTCProperty(
					NOV4ComponentSMDL.DP_REF).getReferenceValue();
			
			TCComponent orderObject = smdlItem.getTCProperty(
					NOV4ComponentSMDL.ORDER_REF).getReferenceValue();
			
			// Get DP Object
			if(dpObject != null){
				m_txtProduct.setText(dpObject.getTCProperty(NOV4ComponentDP.DP_BASED_ON_PROD).getStringValue());
			}
			
			if(orderObject != null){
				m_txtCustomer.setText(orderObject.getTCProperty(NOV4ComponentOrder.CUSTOMER).getStringValue());
				
				int plantIdInt = orderObject.getTCProperty(NOV4ComponentOrder.PLANT_ID_OTHER).getIntValue();
				String plantIdStr = m_soaPlantList.getStringFromReference(plantIdInt);
				
				if(plantIdStr != null)				
					m_txtPlant.setText(plantIdStr);
			}
		}catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
