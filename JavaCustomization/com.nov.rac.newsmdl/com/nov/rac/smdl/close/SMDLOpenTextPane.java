package com.nov.rac.smdl.close;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.teamcenter.rac.util.Registry;

public class SMDLOpenTextPane extends Composite {

	private Label m_lblTextInfo;
	
	private Label m_lblTextDocRev;
	
	private Registry m_Registry;	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public SMDLOpenTextPane(Composite parent, int style) {
		super(parent, style);
		
		m_Registry = Registry.getRegistry(this);
		
		createUI(this);

		populateLabels();

	}
	
private void createUI(Composite parent) {
		
		parent.setLayout(new GridLayout(1, false));
		
		GridData gd_titleBlock = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		parent.setLayoutData(gd_titleBlock);
		
		GridData gd_titleBlock1 = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		m_lblTextInfo = new Label(parent, SWT.NONE);
		m_lblTextInfo.setLayoutData(gd_titleBlock1);
		
		FontData fontData = new FontData();
		fontData.setStyle(SWT.BOLD);
		Font txtFont = new Font(getDisplay(),fontData);
		
		m_lblTextInfo.setFont(txtFont);
				
		GridData gd_titleBlock2 = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		m_lblTextDocRev = new Label(parent, SWT.NONE);			
		m_lblTextDocRev.setLayoutData(gd_titleBlock2);
	}
	

	private void populateLabels() {
		m_lblTextInfo.setText(m_Registry.getString("NewSMDLRevText.Label"));
		
		m_lblTextDocRev.setText(m_Registry.getString("UnlockDocsText.Label"));
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
