package com.nov.rac.smdl.close;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableColumn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.patterns.observer.ISubject;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.common.SMDLCloseTable;
import com.nov.rac.smdl.common.renderers.NOVTableCheckBoxCellRenderer;
import com.nov.rac.smdl.common.renderers.TableHeaderRenderer;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class DocumentsTable extends Composite implements ISubject{

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	
	private SMDLCloseTable  	m_documentsTable  = null;
	private Registry 			m_Registry = null;
	private NOVTableCheckBoxCellRenderer 	m_docTableCellEditor = null;
	private Vector<TCComponent> m_InProgressDocRevs = null;
	private TCComponent[] 		m_ReleasedDocRevs = null;
	private TableHeaderRenderer m_tableHeaderRenderer = null;
	private boolean				m_AllReleased = false;
	private boolean				m_SelectAll = false;
	
	private int					m_ChkBoxColIndex = -1;
	private int					m_LatestRelColIndex = -1;
	
	private List<IObserver> 	m_Observers = new ArrayList<IObserver>();
		
	class SelectAllActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (m_SelectAll) {
				m_SelectAll = false;
			} else {
				m_SelectAll = true;
			}
			selectAllDocuments(m_SelectAll);
		}
	}
	
	public DocumentsTable(Composite parent, int style) {
		super(parent, style);
		
		m_Registry = Registry.getRegistry(this);

		createUI(this);
	}

	private void createUI(Composite parent) {
		
		setLayout(new GridLayout(1,true));
		setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createDocTable(parent);
				
		ScrollPagePane tablePane= new ScrollPagePane(m_documentsTable);
		
        TableColumn tableColumn = m_documentsTable.getColumn(m_Registry.getString("ShowLatestRelRev_COL.Label"));
        
        tableColumn.setCellRenderer(m_docTableCellEditor);
        tableColumn.setCellEditor(m_docTableCellEditor);
        
        m_tableHeaderRenderer = new TableHeaderRenderer(new SelectAllActionListener());
        tableColumn.setHeaderRenderer(m_tableHeaderRenderer);  
		
       	SWTUIUtilities.embed( parent, tablePane, false);
		
	}
	

	 
	
	private void createDocTable(Composite parent)
	{
		String[] strColNames= {
								m_Registry.getString("DocNumber_COL.Label"),
								m_Registry.getString("Revision_COL.Label"),
								m_Registry.getString("Status_COL.Label"),
								m_Registry.getString("DocTitle_COL.Label"),
								m_Registry.getString("LatestRelRev_COL.Label"),
								m_Registry.getString("ShowLatestRelRev_COL.Label")
								};
		
		TCSession session= (TCSession)AIFUtility.getDefaultSession();
		
        m_documentsTable = new SMDLCloseTable(session , strColNames);
        
        m_ChkBoxColIndex = m_documentsTable.getColumn(m_Registry.getString("ShowLatestRelRev_COL.Label")).getModelIndex();

        m_LatestRelColIndex = m_documentsTable.getColumn(m_Registry.getString("LatestRelRev_COL.Label")).getModelIndex();

        m_docTableCellEditor = createTableCellEditor();
	}

	private NOVTableCheckBoxCellRenderer createTableCellEditor() {
		
		m_docTableCellEditor = new NOVTableCheckBoxCellRenderer();
		
		m_docTableCellEditor.addCellEditorListener(new CellEditorListener() {
			
			@Override
			public void editingStopped(ChangeEvent changeevent) {
				
				int[] selectedRows = m_documentsTable.getSelectedRows();
				
				showLatestReleasedRevs(selectedRows);
			}

			@Override
			public void editingCanceled(ChangeEvent changeevent) {
			}
		});
		
		return m_docTableCellEditor;
	}
	
	
	private void checkAllReleased() {
		boolean bAllReleased = true;
		
		for(int iCnt=0; iCnt < m_ReleasedDocRevs.length; iCnt++){
			if(m_ReleasedDocRevs[iCnt] == null){
				bAllReleased = false;
				break;
			}
		}
		m_AllReleased = bAllReleased;
		this.notifyObserver();		
//		m_AllReleased = true;
	}
	
	public void selectAllDocuments(boolean bSelectAll){
		int[] selectedRows = new int[m_documentsTable.getRowCount()];
		
		for(int iCnt = 0; iCnt < m_documentsTable.getRowCount(); iCnt++){
			selectedRows[iCnt] = iCnt;
			
			m_documentsTable.getModel().setValueAt(bSelectAll, iCnt, m_ChkBoxColIndex);
		}
		showLatestReleasedRevs(selectedRows);
	}
	
	private void showLatestReleasedRevs(int[] selectedRows) {
		
		if(m_ReleasedDocRevs == null)
			m_ReleasedDocRevs = new TCComponent[m_InProgressDocRevs.size()];
				
		for(int iCnt = 0; iCnt < selectedRows.length;iCnt++){
			Boolean val = (Boolean) m_documentsTable.getModel().getValueAt(selectedRows[iCnt], m_ChkBoxColIndex);
			
			if(val){
				try {
					TCComponent DocRef = m_InProgressDocRevs.get(selectedRows[iCnt]).getTCProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF).getReferenceValue();
					
					TCComponent[] revList = DocRef.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					
					for(int i = revList.length -1; i >= 0; i--){
						TCComponent[] relStatus = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
						if(relStatus!= null && relStatus.length > 0){
							m_ReleasedDocRevs[selectedRows[iCnt]] = revList[i];
							
							String revId = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();
							
							m_documentsTable.getModel().setValueAt(revId,selectedRows[iCnt], m_LatestRelColIndex);
							m_documentsTable.repaint();
							break;
						}
					}
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else{
				m_ReleasedDocRevs[selectedRows[iCnt]] = null;
				m_documentsTable.getModel().setValueAt("",selectedRows[iCnt], m_LatestRelColIndex);
				m_documentsTable.repaint();
			}
			
		}
		checkAllReleased();
	}

	public void populateDocTable(TCComponent smdlRevision){
		
		//Get the typed reference for all code forms attached to SMDL Revision
		m_documentsTable.removeAllRows();
		
        if (smdlRevision != null){
        	try {     		
        		
				TCComponent[] codeFormObjs = smdlRevision.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
				System.out.println(codeFormObjs.length);
				m_InProgressDocRevs =  getInProgressDocumentsRevs(codeFormObjs);
				
				if(m_InProgressDocRevs.size() == 0){
					m_AllReleased = true;
					this.notifyObserver();
				}
				
				for (int iCount = 0; iCount < m_InProgressDocRevs.size(); iCount++) {

					Vector<Object> codeFormAttr = new Vector<Object>();
					// 1. Document Number
					codeFormAttr.add(m_InProgressDocRevs.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_NUMBER)
							.getStringValue());

					// 2. Revision ID

					codeFormAttr.add(m_InProgressDocRevs.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID)
							.getStringValue());

					TCComponent[] docRevStatus = m_InProgressDocRevs
							.get(iCount).getTCProperty(
									NOVSMDLConstants.DOCUMENT_STATUS)
							.getReferenceValueArray();

					// 3. Document Status
					if (docRevStatus != null && docRevStatus.length > 0)
						codeFormAttr
								.add(docRevStatus[docRevStatus.length - 1]
										.getTCProperty(
												NOVSMDLConstants.REL_STATUS_NAME)
										.getStringValue());
					else
						codeFormAttr.add("");

					// 4. Document Title
					codeFormAttr.add(m_InProgressDocRevs.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE)
							.getStringValue());

					// 5. Latest Released Rev
					codeFormAttr.add("");

					// 6. Show Latest Released Revision ?
					codeFormAttr.add(false);

					Object[] rowObjects = codeFormAttr.toArray();

					m_documentsTable.addRow(rowObjects);
				}
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
						
	}

	private Vector<TCComponent> getInProgressDocumentsRevs(TCComponent[] codeFormObjs) {
		
		m_InProgressDocRevs = new Vector<TCComponent>();
		
		for(int iCount = 0;iCount < codeFormObjs.length; iCount++ ){
			try {
				
				TCComponent smdlDoc = codeFormObjs[iCount].getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
				
				TCComponent docLatestRev = null;
				TCComponent[] docRevisionList = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				
				docLatestRev = codeFormObjs[iCount].getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if (docLatestRev == null)
					docLatestRev = docRevisionList[docRevisionList.length -1];
				
				TCComponent[] docRevStatus = docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
				
				if(docRevStatus != null && docRevStatus.length == 0){
				
					if(!m_InProgressDocRevs.contains(smdlDoc))
						m_InProgressDocRevs.add(docLatestRev);
				}
				
			} catch (TCException e) {
				e.printStackTrace();
			}
			
		}
		
		return m_InProgressDocRevs;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public boolean is_AllReleased() {
		return m_AllReleased;
	}

	public void notifyObserver() 
	{
		Iterator<IObserver> i = m_Observers.iterator();
		while (i.hasNext())
		{
			IObserver o = (IObserver) i.next();
			o.update();
		}
	}

	
	public void registerObserver(IObserver ob) 
	{
		m_Observers.add(ob);		
	}

	
	public void removeObserver(IObserver ob) 
	{
		m_Observers.remove(ob);				
	}

	public boolean is_SelectAll() {
		return m_SelectAll;
	}
	
}
