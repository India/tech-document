package com.nov.rac.smdl.close;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.patterns.observer.ISubject;
import com.teamcenter.rac.util.Registry;

public class SMDLCloseCheckBoxPane extends Composite {

	private Button 		m_btnTextInfo;
	private Registry 	m_Registry;
	
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public SMDLCloseCheckBoxPane(Composite parent, int style) {
		super(parent, style);
		
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.close.close");
		
		createUI(this);

		populateLabels();

	}
	
	private void createUI(Composite parent) {
		
		parent.setLayout(new GridLayout(1, false));
		
		GridData gd_titleBlock = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		parent.setLayoutData(gd_titleBlock);
		
//		setLayout(new RowLayout(SWT.VERTICAL));
		
		m_btnTextInfo = new Button(parent, SWT.CHECK);
		
		m_btnTextInfo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {

				firePropertyChange(m_btnTextInfo, "enableOperation","" , ((Button)selectionevent.getSource()).getSelection());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent selectionevent) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
	}
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}

	private void populateLabels() {
		m_btnTextInfo.setText(m_Registry.getString("SMDLCheckInfo.Label"));
	}

	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}



}
