package com.nov.rac.smdl.close;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.workflow.commands.newprocess.NewProcessCommand;

public class SMDLCloseWizardDialog extends WizardDialog{

	public SMDLCloseWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void nextPressed() {
		// TODO Auto-generated method stub
		super.nextPressed();
		
		NewProcessCommand processCmd = new NewProcessCommand(AIFUtility.getActiveDesktop(),
											AIFUtility.getCurrentApplication(),
											((SMDLCloseWizard)this.getWizard()).getTargetComp());


		this.getShell().dispose();
		
//		IApplicationDefService iapplicationdefservice = AifrcpPlugin.getApplicationDefService();
//        String s = "com.teamcenter.rac.explorer.ExplorerApplication";
//        //String s = "com.nov.rac.smdl.SMDLApplication";
//
//        ApplicationDef applicationdef = iapplicationdefservice.getApplicationDefByKey(s);
//        if(applicationdef != null)
//            s = applicationdef.getPerspectiveId();
//		
//		String s1 = (new StringBuilder()).append("(perspectiveId=").append(s).append(")").toString();
//		
//        final IAspectUIService iaspectuiservice = (IAspectUIService)OSGIUtil.getService(AifrcpPlugin.getDefault(),
//        												com.teamcenter.rac.services.IAspectUIService.class.getName(), s1);
//        
//        SwingUtilities.invokeLater(new Runnable() {
//			
//			@Override
//			public void run() {
//				iaspectuiservice.performAction("newProcessAction");
//			}
//		}); 
//		
//		org.osgi.framework.ServiceReference servicereference = null;
//      org.osgi.framework.ServiceReference aservicereference[] = null;		
//		BundleContext bundlecontext = AifrcpPlugin.getDefault().getBundle().getBundleContext();
//		
//		try {
//			aservicereference = bundlecontext.getServiceReferences(IExplorerUIService.class.getName(), null);
//		} catch (InvalidSyntaxException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        if(aservicereference != null && aservicereference.length != 0){
//        	servicereference = aservicereference[0];
//        }
//        
//        if(servicereference != null){        	
//        	final IAspectUIService iaspectuiservice1 = (IAspectUIService) bundlecontext.getService(servicereference);
//        	
//        	SwingUtilities.invokeLater(new Runnable() {
//    			
//    			@Override
//    			public void run() {
//    				iaspectuiservice1.performAction("newProcessAction");
//    			}
//    		}); 
//        }

	}
	
	

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// TODO Auto-generated method stub
		super.createButtonsForButtonBar(parent);
		
		Button finishButton = getButton(IDialogConstants.FINISH_ID);
		finishButton.setVisible(false);
		
		Button backButton = getButton(IDialogConstants.BACK_ID);
		backButton.setVisible(false);
	}

	

}
