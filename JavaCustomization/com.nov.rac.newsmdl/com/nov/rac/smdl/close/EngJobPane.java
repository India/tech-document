package com.nov.rac.smdl.close;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class EngJobPane extends Composite {

	private List m_txtEngJob;
	private Text m_txtSalesOrder;
	private Text m_txtDocControl;
	
	private Label m_lblEngJob;
	private Label m_lblSalesOrder;
	private Label m_lblDocControl; 
	
	private Registry m_Registry;	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public EngJobPane(Composite parent, int style) {
		super(parent, style);
		
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.close.close");
		
		createUI(this);

		populateLabels();

	}
	
	private void createUI(Composite parent) {
		
		setLayout(new GridLayout(2,false));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_lblEngJob = new Label(parent, SWT.NONE);
				
		m_txtEngJob = new List(parent, SWT.V_SCROLL|SWT.BORDER);
		m_txtEngJob.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		
		m_lblSalesOrder = new Label(parent, SWT.NONE);
		
		m_txtSalesOrder = new Text(parent, SWT.BORDER);
		m_txtSalesOrder.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		m_lblDocControl = new Label(parent, SWT.NONE);
		
		m_txtDocControl = new Text(parent, SWT.BORDER);
		m_txtDocControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		
	}
	

	private void populateLabels() {
		m_lblEngJob.setText(m_Registry.getString("SMDLEngJob.Label"));
		
		m_lblSalesOrder.setText(m_Registry.getString("SMDLPlant.Label"));
		
		m_lblDocControl.setText(m_Registry.getString("SMDLProduct.Label"));		
	}


	public void populateTextvals(TCComponent smdlRevision) {
		TCComponent smdlItem;
		try {
			smdlItem = smdlRevision.getTCProperty(
					NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
			
			TCComponent dpObject = smdlItem.getTCProperty(
					NOV4ComponentSMDL.DP_REF).getReferenceValue();
			
			TCComponent orderObject = smdlItem.getTCProperty(
					NOV4ComponentSMDL.ORDER_REF).getReferenceValue();
			
			if(dpObject != null){
				
				String[] strEngJobNumbes = dpObject.getTCProperty(NOV4ComponentDP.DP_ENG_JOB_NUMBERS).getStringValueArray();
				if(strEngJobNumbes != null && strEngJobNumbes.length > 0)
					m_txtEngJob.setItems(strEngJobNumbes);		
			}
			
			// Get Order Object
			if(orderObject != null){				
				m_txtSalesOrder.setText(orderObject.getTCProperty(NOV4ComponentOrder.SALES_ORDER_NUMBER).getStringValue());
			}
		}catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
