package com.nov.rac.smdl.close;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.action.SMDLOperationHelper;
import com.nov.rac.smdl.common.SMDLCloseTable;
import com.nov.rac.smdl.common.renderers.DocumentRevCellRenderer;
import com.nov.rac.smdl.common.renderers.NOVTableCheckBoxCellRenderer;
import com.nov.rac.smdl.common.renderers.TableHeaderRenderer;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.smdl.utilities.ReviseHelper;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.patterns.observer.ISubject;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.services.TransferInfoToWebServerHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.SetReleaseStatusResponse;
import com.teamcenter.soa.common.PolicyProperty;

public class SMDLDocsTableComposite extends Composite{

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	
	protected SMDLCloseTable  				m_documentsTable  = null;
	protected Registry 						m_Registry = null;
	protected NOVTableCheckBoxCellRenderer 	m_docTableCellEditor = null;
	protected Vector<TCComponent>			m_DocRevsInContext = null;
	protected Vector<TCComponent>			m_EmptyCodeForms = null;
	
	protected TCComponent 					m_smdlSOAPlainObject = null;
	protected TCComponent[] 				m_ReleasedDocRevs = null;
	
	protected ArrayList <TCComponent>		m_ReleasedDocRevisions = new ArrayList<TCComponent>(); 
	
	protected Map<TCComponent, String>		m_codeFormDocMap = new HashMap();
	protected TableHeaderRenderer 			m_tableHeaderRenderer = null;
	private boolean							m_AllReleased = false;
	boolean									m_SelectAll = false;
	
	private boolean							m_unlockSelected = false;
	
	protected int							m_ChkBoxColIndex = -1;
	private int								m_LatestRelColIndex = -1;
//	private int								m_tableContext = -1;
		
	
	public SMDLDocsTableComposite(Composite parent, int style) {
		super(parent, style);
		
		m_Registry = Registry.getRegistry(this);

	}
	
	public Vector<TCComponent> getCodeForms(TCComponent smdlRevision){
		
		Vector<TCComponent> codeForms = null;
		// Get all the properties for the SMDL Revision object
		m_smdlSOAPlainObject = getSMDLRevisionProps(smdlRevision);
		
		//Get the typed reference for all code forms attached to SMDL Revision
		m_documentsTable.removeAllRows();
		
        if (m_smdlSOAPlainObject != null){
        	try {     		
        		
        		TCComponent[] codeFormObjs = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
				
				codeForms = new Vector<TCComponent>();
				m_DocRevsInContext = new Vector<TCComponent>();
				
				for(int iCnt = 0; iCnt < codeFormObjs.length; iCnt++){
					codeForms.add(codeFormObjs[iCnt]);
				}
				
        	}catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return codeForms;
	}

	public void populateDocTable(Vector<TCComponent> m_DocRevsInContext){
		
		try {     		

			for (int iCount = 0; iCount < m_DocRevsInContext.size(); iCount++) {

				Vector<Object> codeFormAttr = new Vector<Object>();
				// 1. Document Number
				codeFormAttr.add(m_DocRevsInContext.get(iCount)
						.getTCProperty(NOVSMDLConstants.DOCUMENT_NUMBER)
						.getStringValue());

				// 2. Revision ID

				codeFormAttr.add(m_DocRevsInContext.get(iCount));

				TCComponent[] docRevStatus = m_DocRevsInContext
						.get(iCount).getTCProperty(
								NOVSMDLConstants.DOCUMENT_STATUS)
								.getReferenceValueArray();

				// 3. Document Name
				codeFormAttr.add(m_DocRevsInContext.get(iCount)
						.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE)
						.getStringValue());

				// 4. Document Desc
				codeFormAttr.add(m_DocRevsInContext.get(iCount)
						.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC)
						.getStringValue());

				// 5. Unlock the revision ?
				codeFormAttr.add(false);


				Object[] rowObjects = codeFormAttr.toArray();

				m_documentsTable.addRow(rowObjects);
			}

		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	protected TCComponent getSMDLRevisionProps(TCComponent smdlRevObject) {
		
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(smdlRevObject);
		
		String[] smdlAttributeList = getAttributeList();
		
		NOVSMDLPolicy smdlPolicy = createSMDLPolicy();
		
		TCComponent smdlRevision = dmSOAHelper.getSMDLProperties(smdlRevObject,smdlAttributeList,smdlPolicy);
		
		return smdlRevision;
	}

	private String[] getAttributeList() {

		String[] smdlAttributeList = {
										NOV4ComponentSMDL.SMDL_CODES 
									  };
		return smdlAttributeList;
	}

	public NOVSMDLPolicy createSMDLPolicy() {
		NOVSMDLPolicy m_SMDLPolicy;
			
		m_SMDLPolicy = new NOVSMDLPolicy();

		// Add Property policy for SMDL Code Form object
		Vector<NOVSMDLPolicyProperty> codeFormPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REF,PolicyProperty.WITH_PROPERTIES));
		codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] codeformPolicyPropsArr = codeFormPolicyProps.toArray(new NOVSMDLPolicyProperty[codeFormPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME, codeformPolicyPropsArr);

		// Add Property policy for Documents object
		Vector<NOVSMDLPolicyProperty> docsPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_NUMBER,PolicyProperty.AS_ATTRIBUTE));
		docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_TITLE,PolicyProperty.AS_ATTRIBUTE));
		docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REVISION_LIST,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] docsPolicyPropsArr = docsPolicyProps.toArray(new NOVSMDLPolicyProperty[docsPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOC_OBJECT_TYPE_NAME, docsPolicyPropsArr);


		// Add Property policy for Documents Revision object
		Vector<NOVSMDLPolicyProperty> docsRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_REV_ID,PolicyProperty.AS_ATTRIBUTE));
		docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_STATUS,PolicyProperty.WITH_PROPERTIES));
		docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF,PolicyProperty.AS_ATTRIBUTE));
		
		NOVSMDLPolicyProperty[] docsRevPolicyPropsArr = docsRevPolicyProps.toArray(new NOVSMDLPolicyProperty[docsRevPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOCREV_OBJECT_TYPE_NAME, docsRevPolicyPropsArr);

		// Add Property policy for Release Status object
		Vector<NOVSMDLPolicyProperty> relStatusPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		relStatusPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REL_STATUS_NAME,PolicyProperty.AS_ATTRIBUTE));
		
		NOVSMDLPolicyProperty[] relStatusPolicyPropsArr = relStatusPolicyProps.toArray(new NOVSMDLPolicyProperty[relStatusPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.REL_STATUS_OBJECT_TYPE_NAME, relStatusPolicyPropsArr);

		
		// Add Property policy for Release Status object
		Vector<NOVSMDLPolicyProperty> smdlRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.PROP_ITEM,PolicyProperty.WITH_PROPERTIES));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_ITEM_ID));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_REV_ID,PolicyProperty.AS_ATTRIBUTE));

		NOVSMDLPolicyProperty[] smdlRevPolicyPropsArr = smdlRevPolicyProps.toArray(new NOVSMDLPolicyProperty[smdlRevPolicyProps.size()]);

		m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE, smdlRevPolicyPropsArr);
		
		
		return m_SMDLPolicy;
	}

	
	

	public void sendSMDLNotification(String notificationContext) {
		
		
		TCSession session = m_smdlSOAPlainObject.getSession(); 
		
		TCUserService usrsrc = session.getUserService();
		if(usrsrc != null)
		{
			String[] notificationData = new String[4];
			
			notificationData[0] = notificationContext;
			try {
				notificationData[1] = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.SMDLREV_ITEM_ID).getStringValue();
				notificationData[2] = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.SMDLREV_REV_ID).getStringValue();
				notificationData[3] = session.getUserName();
				
				//usrsrc.call("NOV4_send_SMDL2_notification",new Object[] {notificationData});
                String context = PreferenceHelper.getStringValue("CETGWSServices_context", TCPreferenceService.TC_preference_site);
                TransferInfoToWebServerHelper transferInfoToWebServerHelper = new TransferInfoToWebServerHelper();
                
                transferInfoToWebServerHelper.setParams( "?" + notificationData[1] +"&" + notificationData[2] + "&u" + notificationData[3] );
                transferInfoToWebServerHelper.setContext(context);
                transferInfoToWebServerHelper.setServeletName(notificationContext);
                
                transferInfoToWebServerHelper.transferInfoToWebService();				
				
			} catch (TCException e1) {

				e1.printStackTrace();
			}
		}
	}
	
	
	protected TCComponent getCodeForm(String docName)
	{
		TCComponent codeForm = null;
		Set keys=m_codeFormDocMap.keySet();
		Iterator keysIterator=keys.iterator();
		
		while (keysIterator.hasNext())
		{
			TCComponent key= (TCComponent)keysIterator.next();
			Object obj = m_codeFormDocMap.get(key);
			if( obj.equals(docName))
			{
				codeForm= key;
			}
			
		}
		return codeForm;
	}

}
