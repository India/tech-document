package com.nov.rac.smdl.close;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CellEditor.LayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.action.SMDLOperationHelper;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class SMDLCloseDialog extends AbstractSWTDialog implements PropertyChangeListener{

	private SMDLCloseTableComposite 			m_DocTablePane = null;
		
	private Registry 				m_registry;
	
	public SMDLCloseDialog(Shell shell, int iType) {
		super(shell, iType);
		m_registry = Registry.getRegistry(this);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		createButton(parent, IDialogConstants.OK_ID, "Close SMDL", true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        
        getButton(IDialogConstants.OK_ID).setEnabled(false);
	}
	
	

	@Override
	protected Control createButtonBar(Composite composite) {
		
		Composite composite1 = new Composite(composite, SWT.CENTER);
        GridLayout gridlayout = new GridLayout();
        composite1.setLayout(gridlayout);
        
        GridData griddata = new GridData();
        griddata.grabExcessHorizontalSpace = true;
        griddata.horizontalAlignment = SWT.CENTER;
        
        composite1.setLayoutData(griddata);
        
        createButtonsForButtonBar(composite1);
        return composite1;
	}

	@Override
	protected Point getInitialSize() {
		// TODO Auto-generated method stub
		Monitor monitor = this.getShell().getMonitor();

		Rectangle size = monitor.getBounds();

		Rectangle rect = this.getShell().getBounds();

		int rectwidth = rect.width;

		int width = ((size.width ) / 2);

		int height = ((size.height ) / 2);

		return new Point(width, height);


	}

	@Override
	protected Control createDialogArea(Composite parent) {
		
		setTitle();

		SMDLCloseTextPane smdlCloseTextPane = new SMDLCloseTextPane(parent, SWT.CENTER);
		
		m_DocTablePane = new SMDLCloseTableComposite(parent, SWT.EMBEDDED);
		
		SMDLCloseCheckBoxPane checkBoxPane = new SMDLCloseCheckBoxPane(parent, SWT.CENTER);
		checkBoxPane.registerPropertyChangeListener(this);
		
		return parent;
	}
	
	

	@Override
	protected void okPressed() {
		// Close SMDL Button pressed Event
		
		m_DocTablePane.closeSMDL();
			
		super.okPressed();
	}

	public boolean populate(TCComponent smdlItemRev) {
		
		m_DocTablePane.populateDocTable(smdlItemRev);
		
		boolean bClosable = m_DocTablePane.isSMDLClosable();
					
		return bClosable;
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals("enableOperation") )
		{
			Boolean enable = (Boolean) event.getNewValue();
			
			if(enable){
				Boolean bClosable = m_DocTablePane.isSMDLClosable();
				
				if(bClosable)
					getButton(IDialogConstants.OK_ID).setEnabled(enable);
			}
			else
				getButton(IDialogConstants.OK_ID).setEnabled(enable);
				
		}
	}
	
	private void setTitle()
	{		
		String strDialogTitle = m_registry.getString("CloseSMDL.TITLE", null);
			
		this.getShell().setText(strDialogTitle);
								
	}
	
}
