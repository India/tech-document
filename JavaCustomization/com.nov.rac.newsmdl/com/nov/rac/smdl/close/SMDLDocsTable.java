package com.nov.rac.smdl.close;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.action.SMDLOperationHelper;
import com.nov.rac.smdl.common.SMDLCloseTable;
import com.nov.rac.smdl.common.renderers.NOVTableCheckBoxCellRenderer;
import com.nov.rac.smdl.common.renderers.TableHeaderRenderer;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.nov.rac.smdl.utilities.ReviseHelper;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.patterns.observer.ISubject;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.services.TransferInfoToWebServerHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.SetReleaseStatusResponse;
import com.teamcenter.soa.common.PolicyProperty;

public class SMDLDocsTable extends Composite{

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	
	private SMDLCloseTable  				m_documentsTable  = null;
	private Registry 						m_Registry = null;
	private NOVTableCheckBoxCellRenderer 	m_docTableCellEditor = null;
	protected Vector<TCComponent>			m_DocRevsInContext = null;
	
	private TCComponent 					m_smdlSOAPlainObject = null;
	private TCComponent[] 					m_ReleasedDocRevs = null;
	
	private ArrayList <TCComponent>			m_ReleasedDocRevisions = new ArrayList<TCComponent>(); 
	
	private Map<TCComponent, String>		m_codeFormDocMap = new HashMap();
	private TableHeaderRenderer 			m_tableHeaderRenderer = null;
	private boolean							m_AllReleased = false;
	private boolean							m_SelectAll = false;
	
	private boolean							m_unlockSelected = false;
	
	private int								m_ChkBoxColIndex = -1;
	private int								m_LatestRelColIndex = -1;
	private int								m_tableContext = -1;
		
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	
	class SelectAllActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (m_SelectAll) {
				m_SelectAll = false;
			} else {
				m_SelectAll = true;
			}
			selectAllDocuments(m_SelectAll);
		}
	}
	
	public SMDLDocsTable(Composite parent, int style, int tableContext) {
		super(parent, style);
		
		m_tableContext = tableContext;
		
		m_Registry = Registry.getRegistry(this);

		createUI(this);
	}

	private void createUI(Composite parent) {
		
		parent.setLayout(new GridLayout(1,true));
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createDocTable(parent);
				
		ScrollPagePane tablePane= new ScrollPagePane(m_documentsTable);
		              
        tablePane.setSize(m_documentsTable.getColumnModel().getTotalColumnWidth(), m_documentsTable.getColumnModel().getTotalColumnWidth());
        
        TableColumn tableColumn = m_documentsTable.getColumn(m_Registry.getString("UnlockRev_COL.Label"));
        
        tableColumn.setCellRenderer(m_docTableCellEditor);
        tableColumn.setCellEditor(m_docTableCellEditor);
        
        m_tableHeaderRenderer = new TableHeaderRenderer(new SelectAllActionListener());
        tableColumn.setHeaderRenderer(m_tableHeaderRenderer);  
		
        m_ChkBoxColIndex = m_documentsTable.getColumnIndex(m_Registry.getString("UnlockRev_COL.Label"));
        
       	SWTUIUtilities.embed( parent, tablePane, false);
		
	}	 
	
	private void createDocTable(Composite parent)
	{
		String[] strColNames= {
				m_Registry.getString("DocNumber_COL.Label"),
				m_Registry.getString("Revision_COL.Label"),
				m_Registry.getString("DocTitle_COL.Label"),
				m_Registry.getString("DocDesc_COL.Label"),
				m_Registry.getString("UnlockRev_COL.Label")
				};
		
		TCSession session= (TCSession)AIFUtility.getDefaultSession();
		
        m_documentsTable = new SMDLCloseTable(session , strColNames);
        
        m_docTableCellEditor = createTableCellEditor();
	}

	private NOVTableCheckBoxCellRenderer createTableCellEditor() {
		
		m_docTableCellEditor = new NOVTableCheckBoxCellRenderer();
		
		m_docTableCellEditor.addCellEditorListener(new CellEditorListener() {
			
			@Override
			public void editingStopped(ChangeEvent changeevent) {
				
				int[] selectedRows = m_documentsTable.getSelectedRows();
				
				showLatestReleasedRevs(selectedRows);
			}

			@Override
			public void editingCanceled(ChangeEvent changeevent) {
			}
		});
		
		return m_docTableCellEditor;
	}
	
	
	private boolean checkAllReleased() {
		boolean m_AllReleased = true;
		
		if(m_ReleasedDocRevs != null){
			for(int iCnt=0; iCnt < m_ReleasedDocRevs.length; iCnt++){
				if(m_ReleasedDocRevs[iCnt] == null){
					m_AllReleased = false;
					break;
				}
			}
		}
		return m_AllReleased;
	}
	
	public void selectAllDocuments(boolean bSelectAll){
		int[] selectedRows = new int[m_documentsTable.getRowCount()];
		
		for(int iCnt = 0; iCnt < m_documentsTable.getRowCount(); iCnt++){
			selectedRows[iCnt] = iCnt;
			if(m_tableContext == SMDLOperationHelper.SMDL_OPEN){
				m_documentsTable.getModel().setValueAt(bSelectAll, iCnt, m_ChkBoxColIndex);
			}
		}
		showLatestReleasedRevs(selectedRows);
	}
	
	private void showLatestReleasedRevs(int[] selectedRows) {
		
		if(m_ReleasedDocRevs == null)
			m_ReleasedDocRevs = new TCComponent[m_DocRevsInContext.size()];
				
		for(int iCnt = 0; iCnt < selectedRows.length;iCnt++)
		{
			Boolean val = false;
			if(m_tableContext == SMDLOperationHelper.SMDL_OPEN){
				val = (Boolean) m_documentsTable.getModel().getValueAt(selectedRows[iCnt], m_ChkBoxColIndex);
			}
			else{
				val = true;
			}
				
			
			if(val){
				try {
					TCComponent DocRef = m_DocRevsInContext.get(selectedRows[iCnt]).getTCProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF).getReferenceValue();
					
					TCComponent[] revList = DocRef.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					
					for(int i = revList.length -1; i >= 0; i--){
						TCComponent[] relStatus = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
						if(relStatus!= null && relStatus.length > 0){
							m_ReleasedDocRevs[selectedRows[iCnt]] = revList[i];
							
							String revId = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();
							
							m_documentsTable.repaint();
							break;
						}
					}
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				m_ReleasedDocRevs[selectedRows[iCnt]] = null;
				m_documentsTable.repaint();
			}
			
		}
		
		if(m_tableContext == SMDLOperationHelper.SMDL_OPEN) {
			
			boolean bEnableOpen = isDocumentSelected();
			
			firePropertyChange(m_documentsTable, "enableOpenOperation","" , bEnableOpen);
		}
		
		checkAllReleased();
	}

	private boolean isDocumentSelected() {
		
		boolean bEnableOpen = false;
		
		for(int iCnt = 0; iCnt < m_documentsTable.getRowCount();iCnt++)
		{
			bEnableOpen = (Boolean) m_documentsTable.getModel().getValueAt(iCnt, m_ChkBoxColIndex);
			if(bEnableOpen)
				break;
		}
		
		return bEnableOpen;
	}

	public void populateDocTable(TCComponent smdlRevision, int m_DialogID){
		
		// Get all the properties for the SMDL Revision object
		m_smdlSOAPlainObject = getSMDLRevisionProps(smdlRevision);
		
		//Get the typed reference for all code forms attached to SMDL Revision
		m_documentsTable.removeAllRows();
		
        if (m_smdlSOAPlainObject != null){
        	try {     		
        		
				TCComponent[] codeFormObjs = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
				
				Vector<TCComponent> codeForms = new Vector<TCComponent>();
				m_DocRevsInContext = new Vector<TCComponent>();
				
//				if(m_DialogID == SMDLOperationHelper.SMDL_CLOSE){
					for(int iCnt = 0; iCnt < codeFormObjs.length; iCnt++){
						codeForms.add(codeFormObjs[iCnt]);
					}
					
					m_DocRevsInContext =  getInProgressDocumentsRevs(codeForms,m_DialogID);
//				}
//				else{
//					for(int iCnt = 0; iCnt < codeFormObjs.length; iCnt++){
//						m_DocRevsInContext.insertElementAt(codeFormObjs[iCnt],iCnt);
//					}
//				}
				
				if(m_DocRevsInContext.size() == 0){
					m_AllReleased = true;
				}

				for (int iCount = 0; iCount < m_DocRevsInContext.size(); iCount++) {

					Vector<Object> codeFormAttr = new Vector<Object>();
					// 1. Document Number
					codeFormAttr.add(m_DocRevsInContext.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_NUMBER)
							.getStringValue());

					// 2. Revision ID

					codeFormAttr.add(m_DocRevsInContext.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID)
							.getStringValue());

					TCComponent[] docRevStatus = m_DocRevsInContext
							.get(iCount).getTCProperty(
									NOVSMDLConstants.DOCUMENT_STATUS)
							.getReferenceValueArray();

					// 3. Document Name
					codeFormAttr.add(m_DocRevsInContext.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE)
							.getStringValue());
					
					// 4. Document Desc
					codeFormAttr.add(m_DocRevsInContext.get(iCount)
							.getTCProperty(NOVSMDLConstants.DOCUMENT_OBJECT_DESC)
							.getStringValue());
					
					// 5. Unlock the revision ?
					codeFormAttr.add(false);
					

					Object[] rowObjects = codeFormAttr.toArray();

					m_documentsTable.addRow(rowObjects);
				}
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        if(m_DialogID == SMDLOperationHelper.SMDL_CLOSE){
        	TableColumn tableColumn = m_documentsTable.getColumn(m_Registry.getString("UnlockRev_COL.Label"));
        	m_documentsTable.removeColumn(tableColumn);
        }
						
	}

	private Vector<TCComponent> getInProgressDocumentsRevs(Vector<TCComponent> codeFormObjs, int m_DialogID) {
		
		for(int iCount = 0;iCount < codeFormObjs.size(); iCount++ ){
			try {
				
				TCComponent smdlDoc = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
				
				if(smdlDoc== null)
				{
					continue;
				}
				
				TCComponent docLatestRev = null;
				TCComponent[] docRevisionList = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				
				docLatestRev = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if (docLatestRev == null)
					docLatestRev = docRevisionList[docRevisionList.length -1];
				
				TCComponent[] docRevStatus = docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
				
				m_codeFormDocMap.put(codeFormObjs.get(iCount), docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue());
				if(m_DialogID == SMDLOperationHelper.SMDL_CLOSE)
				{
					if(docRevStatus != null && docRevStatus.length == 0)
					{
					
						if(!m_DocRevsInContext.contains(smdlDoc))
						{
							m_DocRevsInContext.add(docLatestRev);
							//m_codeFormDocMap.put(codeFormObjs.get(iCount), docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue());
						}
							
					}
					else
					{
						m_ReleasedDocRevisions.add(docLatestRev);
//						codeFormObjs.remove(iCount);
					}
				}
				else
				{
					m_DocRevsInContext.add(docLatestRev);
					//m_codeFormDocMap.put(codeFormObjs.get(iCount), docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue());
				}
				
				//
				
			} catch (TCException e) {
				e.printStackTrace();
			}
			
		}
		
		return m_DocRevsInContext;
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public boolean is_AllReleased() {
		return m_AllReleased;
	}

	public boolean is_SelectAll() {
		return m_SelectAll;
	}
	
	protected TCComponent getSMDLRevisionProps(TCComponent smdlRevObject) {
		
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(smdlRevObject);
		
		String[] smdlAttributeList = getAttributeList();
		
		NOVSMDLPolicy smdlPolicy = createSMDLPolicy();
		
		TCComponent smdlRevision = dmSOAHelper.getSMDLProperties(smdlRevObject,smdlAttributeList,smdlPolicy);
		
		return smdlRevision;
	}

	private String[] getAttributeList() {

		String[] smdlAttributeList = {
										NOV4ComponentSMDL.SMDL_CODES 
									  };
		return smdlAttributeList;
	}

	public NOVSMDLPolicy createSMDLPolicy() {
		NOVSMDLPolicy m_SMDLPolicy;
			
		m_SMDLPolicy = new NOVSMDLPolicy();

		// Add Property policy for SMDL Code Form object
		Vector<NOVSMDLPolicyProperty> codeFormPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REF,PolicyProperty.WITH_PROPERTIES));
		codeFormPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] codeformPolicyPropsArr = codeFormPolicyProps.toArray(new NOVSMDLPolicyProperty[codeFormPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME, codeformPolicyPropsArr);

		// Add Property policy for Documents object
		Vector<NOVSMDLPolicyProperty> docsPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_NUMBER,PolicyProperty.AS_ATTRIBUTE));
		docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_TITLE,PolicyProperty.AS_ATTRIBUTE));
		docsPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REVISION_LIST,PolicyProperty.WITH_PROPERTIES));
		
		NOVSMDLPolicyProperty[] docsPolicyPropsArr = docsPolicyProps.toArray(new NOVSMDLPolicyProperty[docsPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOC_OBJECT_TYPE_NAME, docsPolicyPropsArr);


		// Add Property policy for Documents Revision object
		Vector<NOVSMDLPolicyProperty> docsRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_REV_ID,PolicyProperty.AS_ATTRIBUTE));
		docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_STATUS,PolicyProperty.WITH_PROPERTIES));
		docsRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF,PolicyProperty.AS_ATTRIBUTE));
		
		NOVSMDLPolicyProperty[] docsRevPolicyPropsArr = docsRevPolicyProps.toArray(new NOVSMDLPolicyProperty[docsRevPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOCREV_OBJECT_TYPE_NAME, docsRevPolicyPropsArr);

		// Add Property policy for Release Status object
		Vector<NOVSMDLPolicyProperty> relStatusPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		relStatusPolicyProps.addElement(new NOVSMDLPolicyProperty(NOVSMDLConstants.REL_STATUS_NAME,PolicyProperty.AS_ATTRIBUTE));
		
		NOVSMDLPolicyProperty[] relStatusPolicyPropsArr = relStatusPolicyProps.toArray(new NOVSMDLPolicyProperty[relStatusPolicyProps.size()]);
		
		m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.REL_STATUS_OBJECT_TYPE_NAME, relStatusPolicyPropsArr);

		
		// Add Property policy for Release Status object
		Vector<NOVSMDLPolicyProperty> smdlRevPolicyProps = new Vector<NOVSMDLPolicyProperty>();
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.PROP_ITEM,PolicyProperty.WITH_PROPERTIES));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_ITEM_ID));
		smdlRevPolicyProps.addElement(new NOVSMDLPolicyProperty(NOV4ComponentSMDLRevision.SMDLREV_REV_ID,PolicyProperty.AS_ATTRIBUTE));

		NOVSMDLPolicyProperty[] smdlRevPolicyPropsArr = smdlRevPolicyProps.toArray(new NOVSMDLPolicyProperty[smdlRevPolicyProps.size()]);

		m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDLRevision.SMDLREV_OBJECT_TYPE, smdlRevPolicyPropsArr);
		
		
		return m_SMDLPolicy;
	}

	public boolean isSMDLClosable() {
		
		selectAllDocuments(true);
		
		boolean bCloseSMDL = checkAllReleased();
				
		if(!bCloseSMDL){
			Shell parentShell = AIFUtility.getActiveDesktop().getShell();
			MessageBox errorMessageBox = new MessageBox(parentShell, SWT.ERROR);
			errorMessageBox.setText(m_Registry.getString("Error.TITLE"));
			errorMessageBox.setMessage(m_Registry.getString("DocReleaseError.MSG"));
			errorMessageBox.open();	
		}

		return bCloseSMDL;
	}
	
	public void closeSMDLRevision() {
		
		m_ReleasedDocRevisions.addAll(m_ReleasedDocRevisions.size(),Arrays.asList(m_ReleasedDocRevs));
		
		//if(m_DocRevsInContext.size() > 0)
		if(m_ReleasedDocRevisions.size() > 0)
		{		
		
			//CreateInObjectHelper[] smdlCodeCreateInObjects = new CreateInObjectHelper[m_DocRevsInContext.size()];
			CreateInObjectHelper[] smdlCodeCreateInObjects = new CreateInObjectHelper[m_ReleasedDocRevisions.size()];
			TCComponent codeForm = null;
			TCComponent docRev= null;
			for(int iCnt = 0; iCnt </*m_DocRevsInContext*/m_ReleasedDocRevisions.size(); iCnt++)
			{
				docRev= /*m_DocRevsInContext*/m_ReleasedDocRevisions.get(iCnt);
			
				try 
				{
					String docName= docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();
					codeForm=getCodeForm(docName);
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				smdlCodeCreateInObjects[iCnt] = new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
				//smdlCodeCreateInObjects[iCnt].setTargetObject(m_DocRevsInContext.get(iCnt));
				smdlCodeCreateInObjects[iCnt].setTargetObject(codeForm);
				smdlCodeCreateInObjects[iCnt].setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF, /*m_ReleasedDocRevs*/m_ReleasedDocRevisions.get(iCnt), CreateInObjectHelper.SET_PROPERTY);
			}
			
			ProgressMonitorDialog closeSMDLDialog=new ProgressMonitorDialog(this.getShell());		
			CreateOrUpdateOperation createCodeOp = new CreateOrUpdateOperation(smdlCodeCreateInObjects);
			
			try {
				closeSMDLDialog.run(true, false,createCodeOp);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// Get SMDL Item from SOA Plain Object
		TCComponent smdlItem;
		try {
			smdlItem = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.PROP_ITEM).getReferenceValue();
			applyReleaseStatus(smdlItem);
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void applyReleaseStatus(TCComponent smdlItem) {
		
		WorkflowService workFlowService = WorkflowService.getService(smdlItem.getSession());
		
		ReleaseStatusInput relStatusInput = new ReleaseStatusInput();
		
		relStatusInput.objects = new TCComponent[] {smdlItem};
		
		ReleaseStatusOption relStatusOption = new ReleaseStatusOption();
//		relStatusOption.existingreleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.newReleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.operation = "Append";
		
		relStatusInput.operations = new ReleaseStatusOption[]{relStatusOption};
		
		try {
			SetReleaseStatusResponse theRersponse = workFlowService.setReleaseStatus(new ReleaseStatusInput[] {relStatusInput});
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public void sendSMDLNotification(String notificationContext) {
		
		
		TCSession session = m_smdlSOAPlainObject.getSession(); 
		
		TCUserService usrsrc = session.getUserService();
		if(usrsrc != null)
		{
			String[] notificationData = new String[4];
			
			notificationData[0] = notificationContext;
			try {
				notificationData[1] = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.SMDLREV_ITEM_ID).getStringValue();
				notificationData[2] = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.SMDLREV_REV_ID).getStringValue();
				notificationData[3] = session.getUserName();
				
				//usrsrc.call("NOV4_send_SMDL2_notification",new Object[] {notificationData});
				
	            String context = PreferenceHelper.getStringValue("CETGWSServices_context", TCPreferenceService.TC_preference_site);
	            TransferInfoToWebServerHelper transferInfoToWebServerHelper = new TransferInfoToWebServerHelper();
	            
	            transferInfoToWebServerHelper.setContext(context);
	            transferInfoToWebServerHelper.setServeletName(notificationContext);
	            transferInfoToWebServerHelper.setParams("?" +  notificationData[1] +"&" + notificationData[2] + "&u" + notificationData[3] );
	            
	            
	            transferInfoToWebServerHelper.transferInfoToWebService();
				
			} catch (TCException e1) {

				e1.printStackTrace();
			}
		}
	}
	
	public void openSMDLRevision() {
		
		TCSession session = m_smdlSOAPlainObject.getSession();
		
		WorkflowService workFlowService = WorkflowService.getService(session);
		
		ReleaseStatusInput relStatusInput = new ReleaseStatusInput();
		
		TCComponentItem smdlItem = null;
		try {
			smdlItem = (TCComponentItem) m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.PROP_ITEM).getReferenceValue();
			
		} catch (TCException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		relStatusInput.objects = new TCComponent[] {smdlItem };
		
		ReleaseStatusOption relStatusOption = new ReleaseStatusOption();
		relStatusOption.existingreleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.newReleaseStatusTypeName = "";
		relStatusOption.operation = "Delete";
		
		relStatusInput.operations = new ReleaseStatusOption[]{relStatusOption};
		
		try {
			SetReleaseStatusResponse theRersponse = workFlowService.setReleaseStatus(new ReleaseStatusInput[] {relStatusInput});
			
			//	smdlItem.revise(smdlItem.getNewRev(),smdlItem.getTCProperty(NOV4ComponentSMDL.SMDL_NAME).getStringValue(),null);
			
			//namrata changes start
			
			ReviseHelper.revise(smdlItem);
			
			//unlock the selected doc revisions
			unlockDocumentRevisions(smdlItem);
			
			//namrata changes end
			
		}
		catch (TCException e) 
		{
			com.teamcenter.rac.util.MessageBox.post(e);
		}
		catch (ServiceException e)
		{
			e.printStackTrace();
		} /*catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}

	private void unlockDocumentRevisions(TCComponentItem smdlItem) 
	{
		int rowCount= m_documentsTable.getRowCount();
		
		ArrayList<CreateInObjectHelper> smdlCodeCreateInObjects = new ArrayList<CreateInObjectHelper>();
		
		for(int row = 0; row <rowCount; row++)
		{		
			boolean unlockRevSelected =(Boolean) m_documentsTable.getValueAt(row , 4);
			if(unlockRevSelected)
			{
				String docName= (String) m_documentsTable.getValueAt(row , 2);
				
				TCComponent codeForm= getLatestCodeForm(docName, smdlItem);
				if(codeForm!=null)
				{
					CreateInObjectHelper smdlCodeCreateInObject = new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);					
					smdlCodeCreateInObject.setTargetObject(codeForm);
					smdlCodeCreateInObject.setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF,null, CreateInObjectHelper.SET_PROPERTY);
					smdlCodeCreateInObjects.add(smdlCodeCreateInObject);
				}
			}
		}
			
		ProgressMonitorDialog openSMDLDialog=new ProgressMonitorDialog(this.getShell());		
		CreateInObjectHelper[] codeObjects = new CreateInObjectHelper[smdlCodeCreateInObjects.size()];
		CreateOrUpdateOperation createCodeOp = new CreateOrUpdateOperation(smdlCodeCreateInObjects.toArray(codeObjects));
		
		try {
			openSMDLDialog.run(true, false,createCodeOp);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
		
	}

	private TCComponent getCodeForm(String docName)
	{
		TCComponent codeForm = null;
		Set keys=m_codeFormDocMap.keySet();
		Iterator keysIterator=keys.iterator();
		
		while (keysIterator.hasNext())
		{
			TCComponent key= (TCComponent)keysIterator.next();
			Object obj = m_codeFormDocMap.get(key);
			if( obj.equals(docName))
			{
				codeForm= key;
			}
			
		}
		return codeForm;
	}

	private TCComponent getLatestCodeForm(String docName, TCComponentItem smdlItem)
	{
		TCComponent codeForm = null;
		TCComponentItemRevision currentRev = null;
		
		try
		{
			TCComponent oldcodeForm = getCodeForm(docName);

			Object oldCode= oldcodeForm.getTCProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER)
					.getPropertyValue();

			currentRev = smdlItem.getLatestItemRevision();
			TCComponent[] codeFormObjs = null;

			codeFormObjs = currentRev.getTCProperty(
					NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();

			if (codeFormObjs != null)
			{
				String sDocTitle=null;
				Object newCode = null;
				for (int count = 0; count < codeFormObjs.length; count++)
				{
					TCComponent currentCodeForm = codeFormObjs[count];
					
					newCode= currentCodeForm.getTCProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER).getPropertyValue();
					TCComponent smdlDoc = currentCodeForm.getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
					if(smdlDoc!=null)
					{
						 sDocTitle = smdlDoc.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();
					}
					//if (currentCodeForm.equals(oldcodeForm))
					if(newCode.equals(oldCode) && sDocTitle.equals(docName))
					{
						codeForm = currentCodeForm;
						break;
					}
				}
			}
		
		}
		catch( TCException e )
		{
			e.printStackTrace();
		}
		
		
		return codeForm;
	}
	
	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
	}
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}
	
}
