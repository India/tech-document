package com.nov.rac.smdl.close;

import java.util.Vector;

import javax.swing.table.TableColumn;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class SMDLOpenTable extends SMDLDocsTable{

	public SMDLOpenTable(Composite parent, int style, int tableContext) {
		super(parent, style, tableContext);
	}
	
	
	private Vector<TCComponent> getInProgressDocumentsRevs(Vector<TCComponent> codeFormObjs) {
		
		for(int iCount = 0;iCount < codeFormObjs.size(); iCount++ ){
			try {
				
				TCComponent smdlDoc = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
				
				TCComponent docLatestRev = null;
				TCComponent[] docRevisionList = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				
				docLatestRev = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if (docLatestRev == null)
					docLatestRev = docRevisionList[docRevisionList.length -1];
				
				TCComponent[] docRevStatus = docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
				
				if(!m_DocRevsInContext.contains(smdlDoc))
						m_DocRevsInContext.add(docLatestRev);
				
			} catch (TCException e) {
				e.printStackTrace();
			}
			
		}
		
		return m_DocRevsInContext;
	}
}
