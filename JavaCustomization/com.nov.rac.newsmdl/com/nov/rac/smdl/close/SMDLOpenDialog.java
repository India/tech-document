package com.nov.rac.smdl.close;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CellEditor.LayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.action.SMDLOperationHelper;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class SMDLOpenDialog extends AbstractSWTDialog implements PropertyChangeListener{

	private SMDLOpenTableComposite 	m_DocTablePane = null;
	public static final String		OPEN_NOTIFICATION_CONTEXT = "SendNotificationForOpenSMDL";
	private Registry 				m_registry;
	
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	
	public SMDLOpenDialog(Shell shell, int iType) {
		super(shell, iType);
		m_registry = Registry.getRegistry(this);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		createButton(parent, IDialogConstants.OK_ID, "Open SMDL", true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        
        getButton(IDialogConstants.OK_ID).setEnabled(false);
	}
	
	

	@Override
	protected Control createButtonBar(Composite composite) {
		
		Composite composite1 = new Composite(composite, SWT.CENTER);
        GridLayout gridlayout = new GridLayout();
        composite1.setLayout(gridlayout);
        
        GridData griddata = new GridData();
        griddata.grabExcessHorizontalSpace = true;
        griddata.horizontalAlignment = SWT.CENTER;
        
        composite1.setLayoutData(griddata);
        
        createButtonsForButtonBar(composite1);
        return composite1;
	}

	@Override
	protected Point getInitialSize() {
		// TODO Auto-generated method stub
		Monitor monitor = this.getShell().getMonitor();

		Rectangle size = monitor.getBounds();

		Rectangle rect = this.getShell().getBounds();

		int rectwidth = rect.width;

		int width = ((size.width ) / 2);

		int height = ((size.height ) / 2);

		return new Point(width, height);


	}

	@Override
	protected Control createDialogArea(Composite parent) {
		
		setTitle();
		
		SMDLOpenTextPane smdlOpenTextPane = new SMDLOpenTextPane(parent, SWT.LEFT);

		m_DocTablePane = new SMDLOpenTableComposite(parent, SWT.EMBEDDED, SMDLOperationHelper.SMDL_OPEN);
		
		m_DocTablePane.registerPropertyChangeListener(this);
		
		return parent;
	}
	
	

	@Override
	protected void okPressed() {
		// Open SMDL Button pressed Event

		m_DocTablePane.openSMDLRevision();

		// Send the Notification for SMDL Open
		m_DocTablePane.sendSMDLNotification(OPEN_NOTIFICATION_CONTEXT);
		
		super.okPressed();
	}

	public void populate(TCComponent smdlItemRev) {
		
		m_DocTablePane.populateDocTable(smdlItemRev);				
	}
	
	
	private void setTitle()
	{		
		String strDialogTitle = m_registry.getString("OpenSMDL.TITLE", null);
			
		this.getShell().setText(strDialogTitle);
								
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("enableOpenOperation") )
		{
			final Boolean enable = (Boolean) event.getNewValue();
			
			Display.getDefault().syncExec(new Runnable() {
				public void run() { 
			
					if(enable){
						getButton(IDialogConstants.OK_ID).setEnabled(enable);
					}
					else
						getButton(IDialogConstants.OK_ID).setEnabled(enable);
				}
			});
			
		}
		
	}
	
}
