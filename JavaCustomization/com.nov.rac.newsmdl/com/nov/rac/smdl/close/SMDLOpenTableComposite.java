package com.nov.rac.smdl.close;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.common.SMDLCloseTable;
import com.nov.rac.smdl.common.renderers.DocumentRevCellRenderer;
import com.nov.rac.smdl.common.renderers.NOVTableCheckBoxCellRenderer;
import com.nov.rac.smdl.common.renderers.TableHeaderRenderer;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.ReviseHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.SetReleaseStatusResponse;

public class SMDLOpenTableComposite extends SMDLDocsTableComposite{
	
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();

	public SMDLOpenTableComposite(Composite parent, int style, int tableContext) {
		super(parent, style);
		
		createUI(this);
	}
	
	public void populateDocTable(TCComponent smdlRevision){
		
		Vector<TCComponent> theCodeForms = getCodeForms(smdlRevision);
		
		m_DocRevsInContext =  getInProgressDocumentsRevs(theCodeForms);
		
		super.populateDocTable(m_DocRevsInContext);
	}
	
	private Vector<TCComponent> getInProgressDocumentsRevs(Vector<TCComponent> codeFormObjs) {
		
		for(int iCount = 0;iCount < codeFormObjs.size(); iCount++ ){
			try {
				
				TCComponent smdlDoc = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
				
				// Check if any Document available on Code Form
				if(smdlDoc == null){
					m_EmptyCodeForms = new Vector<TCComponent>();
					m_EmptyCodeForms.add(codeFormObjs.get(iCount));
					continue;
				}
				
				TCComponent docLatestRev = null;
				TCComponent[] docRevisionList = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				
				docLatestRev = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if (docLatestRev == null)
					docLatestRev = docRevisionList[docRevisionList.length -1];
				
				TCComponent[] docRevStatus = docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
				
				m_codeFormDocMap.put(codeFormObjs.get(iCount), docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue());
				
				if(!m_DocRevsInContext.contains(smdlDoc))
						m_DocRevsInContext.add(docLatestRev);
				
			} catch (TCException e) {
				e.printStackTrace();
			}
			
		}
		
		return m_DocRevsInContext;
	}
	
	private void createUI(Composite parent) {
		
		parent.setLayout(new GridLayout(1,true));
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createDocTable(parent);
				
		ScrollPagePane tablePane= new ScrollPagePane(m_documentsTable);
		              
        tablePane.setSize(m_documentsTable.getColumnModel().getTotalColumnWidth(), m_documentsTable.getColumnModel().getTotalColumnWidth());
        
        TableColumn tableColumn = m_documentsTable.getColumn(m_Registry.getString("UnlockRev_COL.Label"));
        
        tableColumn.setCellRenderer(m_docTableCellEditor);
        tableColumn.setCellEditor(m_docTableCellEditor);
        
        // Adding Cell Renderer for Document Revision Column
        TableColumn docRevColumn = m_documentsTable.getColumn(m_Registry.getString("Revision_COL.Label"));
        docRevColumn.setCellRenderer(new DocumentRevCellRenderer());
        
        m_tableHeaderRenderer = new TableHeaderRenderer(new SelectAllActionListener());
        tableColumn.setHeaderRenderer(m_tableHeaderRenderer);  
        
        m_ChkBoxColIndex = m_documentsTable.getColumnIndex(m_Registry.getString("UnlockRev_COL.Label"));
        
       	SWTUIUtilities.embed( parent, tablePane, false);		
	}
	
	private void createDocTable(Composite parent)
	{
		String[] strColNames= {
				m_Registry.getString("DocNumber_COL.Label"),
				m_Registry.getString("Revision_COL.Label"),
				m_Registry.getString("DocTitle_COL.Label"),
				m_Registry.getString("DocDesc_COL.Label"),
				m_Registry.getString("UnlockRev_COL.Label")
				};
		
		TCSession session= (TCSession)AIFUtility.getDefaultSession();
		
        m_documentsTable = new SMDLCloseTable(session , strColNames);
        
        m_docTableCellEditor = createTableCellEditor();
	}
	
	private NOVTableCheckBoxCellRenderer createTableCellEditor() {
		
		m_docTableCellEditor = new NOVTableCheckBoxCellRenderer();
		
		m_docTableCellEditor.addCellEditorListener(new CellEditorListener() {
			
			@Override
			public void editingStopped(ChangeEvent changeevent) {
				
				int[] selectedRows = m_documentsTable.getSelectedRows();
				
				showLatestReleasedRevs(selectedRows);
			}

			@Override
			public void editingCanceled(ChangeEvent changeevent) {
			}
		});
		
		return m_docTableCellEditor;
	}
	
	public void selectAllDocuments(boolean bSelectAll){
		int[] selectedRows = new int[m_documentsTable.getRowCount()];
		
		for(int iCnt = 0; iCnt < m_documentsTable.getRowCount(); iCnt++){
			selectedRows[iCnt] = iCnt;
			m_documentsTable.getModel().setValueAt(bSelectAll, iCnt, m_ChkBoxColIndex);
		}
		showLatestReleasedRevs(selectedRows);
	}
	
	class SelectAllActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (m_SelectAll) {
				m_SelectAll = false;
			} else {
				m_SelectAll = true;
			}
			selectAllDocuments(m_SelectAll);
		}
	}
	
	void showLatestReleasedRevs(int[] selectedRows) {
		
		if(m_ReleasedDocRevs == null)
			m_ReleasedDocRevs = new TCComponent[m_DocRevsInContext.size()];
				
		for(int iCnt = 0; iCnt < selectedRows.length;iCnt++)
		{
			Boolean val = false;

			val = (Boolean) m_documentsTable.getModel().getValueAt(selectedRows[iCnt], m_ChkBoxColIndex);
							
			
			if(val){
				try {
					TCComponent DocRef = m_DocRevsInContext.get(selectedRows[iCnt]).getTCProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF).getReferenceValue();
					
					TCComponent[] revList = DocRef.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
					
					for(int i = revList.length -1; i >= 0; i--){
						TCComponent[] relStatus = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
						if(relStatus!= null && relStatus.length > 0){
							m_ReleasedDocRevs[selectedRows[iCnt]] = revList[i];
							
							String revId = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();
							
							m_documentsTable.repaint();
							break;
						}
					}
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				m_ReleasedDocRevs[selectedRows[iCnt]] = null;
				m_documentsTable.repaint();
			}
			
		}
		
		
		boolean bEnableOpen = isDocumentSelected();
			
		firePropertyChange(m_documentsTable, "enableOpenOperation","" , bEnableOpen);
		
	}
	
	public void openSMDLRevision() {
		
		TCSession session = m_smdlSOAPlainObject.getSession();
		
		WorkflowService workFlowService = WorkflowService.getService(session);
		
		ReleaseStatusInput relStatusInput = new ReleaseStatusInput();
		
		TCComponentItem smdlItem = null;
		try {
			smdlItem = (TCComponentItem) m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.PROP_ITEM).getReferenceValue();
			
		} catch (TCException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		relStatusInput.objects = new TCComponent[] {smdlItem };
		
		ReleaseStatusOption relStatusOption = new ReleaseStatusOption();
		relStatusOption.existingreleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.newReleaseStatusTypeName = "";
		relStatusOption.operation = "Delete";
		
		relStatusInput.operations = new ReleaseStatusOption[]{relStatusOption};
		
		try {
			SetReleaseStatusResponse theRersponse = workFlowService.setReleaseStatus(new ReleaseStatusInput[] {relStatusInput});
			
			//	smdlItem.revise(smdlItem.getNewRev(),smdlItem.getTCProperty(NOV4ComponentSMDL.SMDL_NAME).getStringValue(),null);
			
			//namrata changes start
		
			ReviseHelper.revise(smdlItem);
			
			//unlock the selected doc revisions
			unlockDocumentRevisions(smdlItem);
			//namrata changes end
			
		}catch (TCException e) 
		{
			MessageBox.post(e);
		}
		catch (ServiceException e)
		{
			e.printStackTrace();
		}
	}

	private void unlockDocumentRevisions(TCComponentItem smdlItem) 
	{
		int rowCount= m_documentsTable.getRowCount();
		
		ArrayList<CreateInObjectHelper> smdlCodeCreateInObjects = new ArrayList<CreateInObjectHelper>();
		
		for(int row = 0; row <rowCount; row++)
		{		
			boolean unlockRevSelected =(Boolean) m_documentsTable.getValueAt(row , 4);
			if(unlockRevSelected)
			{
				String docName= (String) m_documentsTable.getValueAt(row , 2);
				
				TCComponent codeForm= getLatestCodeForm(docName, smdlItem);
				if(codeForm!=null)
				{
					CreateInObjectHelper smdlCodeCreateInObject = new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);					
					smdlCodeCreateInObject.setTargetObject(codeForm);
					smdlCodeCreateInObject.setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF,null, CreateInObjectHelper.SET_PROPERTY);
					smdlCodeCreateInObjects.add(smdlCodeCreateInObject);
				}
			}
		}
			
		ProgressMonitorDialog openSMDLDialog=new ProgressMonitorDialog(this.getShell());		
		CreateInObjectHelper[] codeObjects = new CreateInObjectHelper[smdlCodeCreateInObjects.size()];
		CreateOrUpdateOperation createCodeOp = new CreateOrUpdateOperation(smdlCodeCreateInObjects.toArray(codeObjects));
		
		try {
			openSMDLDialog.run(true, false,createCodeOp);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	
		
	}

	private TCComponent getLatestCodeForm(String docName, TCComponentItem smdlItem)
	{
		TCComponent codeForm = null;
		TCComponentItemRevision currentRev = null;
		
		try
		{
			TCComponent oldcodeForm = getCodeForm(docName);

			Object oldCode= oldcodeForm.getTCProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER)
					.getPropertyValue();

			currentRev = smdlItem.getLatestItemRevision();
			TCComponent[] codeFormObjs = null;

			codeFormObjs = currentRev.getTCProperty(
					NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();

			if (codeFormObjs != null)
			{
				String sDocTitle=null;
				Object newCode = null;
				for (int count = 0; count < codeFormObjs.length; count++)
				{
					TCComponent currentCodeForm = codeFormObjs[count];
					
					newCode= currentCodeForm.getTCProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER).getPropertyValue();
					TCComponent smdlDoc = currentCodeForm.getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
					if(smdlDoc!=null)
					{
						 sDocTitle = smdlDoc.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();
					}
					//if (currentCodeForm.equals(oldcodeForm))
					if(newCode.equals(oldCode) && sDocTitle.equals(docName))
					{
						codeForm = currentCodeForm;
						break;
					}
				}
			}
		
		}
		catch( TCException e )
		{
			e.printStackTrace();
		}
		
		
		return codeForm;
	}
	
	protected boolean isDocumentSelected() {
		
		boolean bEnableOpen = false;
		
		for(int iCnt = 0; iCnt < m_documentsTable.getRowCount();iCnt++)
		{
			bEnableOpen = (Boolean) m_documentsTable.getModel().getValueAt(iCnt, m_ChkBoxColIndex);
			if(bEnableOpen)
				break;
		}
		
		return bEnableOpen;
	}
	
	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
	}
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}
	
}
