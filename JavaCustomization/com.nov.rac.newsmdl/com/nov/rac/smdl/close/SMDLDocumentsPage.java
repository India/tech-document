package com.nov.rac.smdl.close;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.utilities.NOVDataManagementServiceHelper;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLPolicyProperty;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.soa.common.PolicyProperty;

public class SMDLDocumentsPage extends WizardPage implements IObserver{
		
	/**
	 * Create the wizard.
	 */
		
	private DocumentsTable m_docTablePane;
	private SMDLCloseUpperPane m_smdlUpperPane;
	private SMDLCloseTextPane m_smdlCloseTextPane;
    private TCComponent   m_smdlSOAPlainObject = null; 
    private NOVSMDLPolicy m_SMDLPolicy = null;
    
	public SMDLDocumentsPage() {
		super("SMDLDocumentsPage");
		setPageComplete(false);
		setTitle("Close SMDL");
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		
		Composite container = new Composite(parent, SWT.NULL);
		setControl(container);
		
		container.setLayout(new GridLayout(1,false));
		container.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_smdlUpperPane = new SMDLCloseUpperPane(container, SWT.NONE);
		
		m_smdlCloseTextPane = new SMDLCloseTextPane(container, SWT.NONE);

		m_docTablePane = new DocumentsTable(container, SWT.EMBEDDED);
		m_docTablePane.registerObserver(this);
	}
	
	public void populateSMDLDocuments(InterfaceAIFComponent smdlRevision){
		
		m_smdlSOAPlainObject = getSMDLRevisionProps((TCComponent) smdlRevision);
		
		m_smdlUpperPane.populateUpperPane(m_smdlSOAPlainObject);
		
		m_docTablePane.populateDocTable(m_smdlSOAPlainObject);
	}
	
	private TCComponent getSMDLRevisionProps(TCComponent smdlRevObject) {
		
		NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(smdlRevObject);
		
		String[] smdlAttributeList = getAttributeList();
		
		NOVSMDLPolicy smdlPolicy = createSMDLPolicy();
		
		m_smdlSOAPlainObject = dmSOAHelper.getSMDLProperties(smdlRevObject,smdlAttributeList,smdlPolicy);
		
		return m_smdlSOAPlainObject;
	}

	private String[] getAttributeList() {

		String[] smdlAttributeList = {
										NOV4ComponentSMDL.SMDL_CODES 
									  };
		return smdlAttributeList;
	}

	public NOVSMDLPolicy createSMDLPolicy() {
		if(m_SMDLPolicy == null){
			
			m_SMDLPolicy = new NOVSMDLPolicy();
							
			// Add Property policy for SMDL Code Form object
			NOVSMDLPolicyProperty codeFormPolicyProps[] = new NOVSMDLPolicyProperty[2];
			codeFormPolicyProps[0] = new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REF,PolicyProperty.WITH_PROPERTIES);
			codeFormPolicyProps[1] = new NOVSMDLPolicyProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF,PolicyProperty.WITH_PROPERTIES);
			m_SMDLPolicy.addPropertiesPolicy(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME, codeFormPolicyProps);
			
			// Add Property policy for Documents object
			NOVSMDLPolicyProperty docsPolicyProps[] = new NOVSMDLPolicyProperty[3];
			docsPolicyProps[0] = new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_NUMBER,PolicyProperty.AS_ATTRIBUTE);
			docsPolicyProps[1] = new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_TITLE,PolicyProperty.AS_ATTRIBUTE);
			docsPolicyProps[2] = new NOVSMDLPolicyProperty(NOVSMDLConstants.REVISION_LIST,PolicyProperty.WITH_PROPERTIES);
			m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOC_OBJECT_TYPE_NAME, docsPolicyProps);

			
			// Add Property policy for Documents Revision object
			NOVSMDLPolicyProperty docsRevPolicyProps[] = new NOVSMDLPolicyProperty[3];
			docsRevPolicyProps[0] = new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_REV_ID,PolicyProperty.AS_ATTRIBUTE);
			docsRevPolicyProps[1] = new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_STATUS,PolicyProperty.WITH_PROPERTIES);
			docsRevPolicyProps[2] = new NOVSMDLPolicyProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF,PolicyProperty.AS_ATTRIBUTE);
			m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.DOCREV_OBJECT_TYPE_NAME, docsRevPolicyProps);
			
			// Add Property policy for Release Status object
			NOVSMDLPolicyProperty relStatusPolicyProps[] = new NOVSMDLPolicyProperty[1];
			relStatusPolicyProps[0] = new NOVSMDLPolicyProperty(NOVSMDLConstants.REL_STATUS_NAME,PolicyProperty.AS_ATTRIBUTE);			
			m_SMDLPolicy.addPropertiesPolicy(NOVSMDLConstants.REL_STATUS_OBJECT_TYPE_NAME, relStatusPolicyProps);


		}
		
		return m_SMDLPolicy;
	}

	@Override
	public boolean canFlipToNextPage() {
		if (m_docTablePane.is_AllReleased())
			return true;
		else
			return false;
	}

	@Override
	public void update() {
			this.getShell().getDisplay().syncExec(new Runnable(){
			public void run() {
				// TODO Auto-generated method stub
				getWizard().getContainer().updateButtons();	
			}});
	}
	
}
