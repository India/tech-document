package com.nov.rac.smdl.close;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;

import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.patterns.observer.ISubject;

public class SMDLCloseSlectionListener implements SelectionListener, ISubject{
	
	private List<IObserver> 	m_Observers = new ArrayList<IObserver>();
	
	public void notifyObserver() 
	{
		Iterator<IObserver> i = m_Observers.iterator();
		while (i.hasNext())
		{
			IObserver o = (IObserver) i.next();
			o.update();
		}
	}

	
	public void registerObserver(IObserver ob) 
	{
		m_Observers.add(ob);		
	}

	
	public void removeObserver(IObserver ob) 
	{
		m_Observers.remove(ob);				
	}


	@Override
	public void widgetSelected(SelectionEvent selectionevent) {
		
		if(selectionevent.getSource() instanceof Button){
			if(((Button)selectionevent.getSource()).getSelection()){
				this.notifyObserver();
			}
		}
		
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent selectionevent) {
		
	}

}
