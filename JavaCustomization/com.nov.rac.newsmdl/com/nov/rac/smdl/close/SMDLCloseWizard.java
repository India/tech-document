package com.nov.rac.smdl.close;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class SMDLCloseWizard extends Wizard {
	
	private SMDLDocumentsPage 	m_smdlDocsPage = null;
	private InterfaceAIFComponent m_TargetComp = null;

	public SMDLCloseWizard(Shell shell) {
		setWindowTitle("Close SMDL Wizard");
		setForcePreviousAndNextButtons(true);
	}

	@Override
	public void addPages() {
		m_smdlDocsPage = new SMDLDocumentsPage();
		addPage(m_smdlDocsPage);	
	}

	@Override
	public boolean performFinish() {
		return true;
	}

	@Override
	public boolean canFinish() {
		// TODO Auto-generated method stub
		return true;
	}

	public void populateSMDLWizard(InterfaceAIFComponent selComps) {
		if (selComps instanceof TCComponentItem){
			try {
				TCComponent smdlRev = ((TCComponentItem)selComps).getLatestItemRevision();
				m_smdlDocsPage.populateSMDLDocuments(smdlRev);
				m_TargetComp = smdlRev;
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(selComps instanceof TCComponentItemRevision){
			m_smdlDocsPage.populateSMDLDocuments(selComps);
			m_TargetComp = selComps;
		}
		
	}

	public InterfaceAIFComponent[] getTargetComp() {
		return new InterfaceAIFComponent[]{m_TargetComp};
	}

}
