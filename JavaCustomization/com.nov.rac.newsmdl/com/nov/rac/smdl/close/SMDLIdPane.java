package com.nov.rac.smdl.close;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class SMDLIdPane extends Composite {

	private Text m_txtSMDLId;
	private Text m_txtSMDLType;
	private Text m_txtSMDLDesc;
	
	private Label m_lblSMDLId;


	private Label m_lblSMDLType;
	private Label m_lblSMDLDesc; 
	
	private Registry m_Registry;	
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public SMDLIdPane(Composite parent, int style) {
		super(parent, style);
			
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.close.close");
		
		createUI(this);

		populateLabels();
	}

	private void createUI(Composite parent) {
		
		setLayout(new GridLayout(2,false));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		
		m_lblSMDLId = new Label(parent, SWT.NONE);
				
		m_txtSMDLId = new Text(parent, SWT.BORDER);
		m_txtSMDLId.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));
		
		m_lblSMDLType = new Label(parent, SWT.NONE);
		
		m_txtSMDLType = new Text(parent, SWT.BORDER);
		m_txtSMDLType.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));
		
		m_lblSMDLDesc = new Label(parent, SWT.NONE);
		
		m_txtSMDLDesc = new Text(parent, SWT.BORDER);
		m_txtSMDLDesc.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false, 1, 1));
		
		
	}
	

	private void populateLabels() {
		m_lblSMDLId.setText(m_Registry.getString("SMDLId.Label"));
		
		m_lblSMDLType.setText(m_Registry.getString("SMDLType.Label"));
		
		m_lblSMDLDesc.setText(m_Registry.getString("SMDLdesc.Label"));		
	}

	public void populateTextVals(TCComponent smdlRevision) {
		TCComponent smdlItem;
		try {
			smdlItem = smdlRevision.getTCProperty(
					NOV4ComponentSMDL.SMDL_ITEM_REF).getReferenceValue();
			
			// Populate SMDL ID Pane
			m_txtSMDLId.setText(smdlRevision.getTCProperty(
					NOV4ComponentSMDL.SMDL_ID).getStringValue());

			m_txtSMDLDesc.setText(smdlRevision.getTCProperty(
					NOV4ComponentSMDL.SMDL_DESC).getStringValue());

			m_txtSMDLType.setText(smdlItem.getTCProperty(
					NOV4ComponentSMDL.SMDL_TYPE).getStringValue());
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
