package com.nov.rac.smdl.close;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.table.TableColumn;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import com.nov.rac.smdl.close.SMDLCloseTableComposite;
import com.nov.rac.smdl.common.SMDLCloseTable;
import com.nov.rac.smdl.common.renderers.DocumentNoCellRenderer;
import com.nov.rac.smdl.common.renderers.DocumentRevCellRenderer;
import com.nov.rac.smdl.common.renderers.DocumentRevIDCellRenderer;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.utilities.NOVSMDLConstants;
import com.nov.rac.utilities.services.SOAHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.SetReleaseStatusResponse;
import com.teamcenter.soa.client.model.ErrorStack;

public class SMDLCloseTableComposite extends SMDLDocsTableComposite{
	
	private String		CLOSE_NOTIFICATION_CONTEXT = "SendNotificationForCloseSMDL";
	public SMDLCloseTableComposite(Composite parent, int style) {
		super(parent, style);
		
		createUI(this);
	}
	
	public void populateDocTable(TCComponent smdlRevision){
		
		Vector<TCComponent> theCodeForms = getCodeForms(smdlRevision);
		
		m_DocRevsInContext =  getInProgressDocumentsRevs(theCodeForms);
		
		super.populateDocTable(m_DocRevsInContext);
	}
	
	private void createUI(Composite parent) {
		
		parent.setLayout(new GridLayout(1,true));
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createDocTable(parent);
				
		ScrollPagePane tablePane= new ScrollPagePane(m_documentsTable);
		              
        tablePane.setSize(m_documentsTable.getColumnModel().getTotalColumnWidth(), m_documentsTable.getColumnModel().getTotalColumnWidth());
        
        // Adding Cell Renderer for Document Revision Column
        TableColumn docRevColumn = m_documentsTable.getColumn(m_Registry.getString("Revision_COL.Label"));
        docRevColumn.setCellRenderer(new DocumentRevIDCellRenderer());
        
       	SWTUIUtilities.embed( parent, tablePane, false);
		
	}
	
	
	private void createDocTable(Composite parent)
	{
		String[] strColNames= {
				m_Registry.getString("DocNumber_COL.Label"),
				m_Registry.getString("Revision_COL.Label"),
				m_Registry.getString("DocTitle_COL.Label"),
				m_Registry.getString("DocDesc_COL.Label")
				};
		
		TCSession session= (TCSession)AIFUtility.getDefaultSession();
		
		m_documentsTable = new SMDLCloseTable(session , strColNames);  
	}

	void showLatestReleasedRevs(int[] selectedRows) {
		
		if(m_ReleasedDocRevs == null)
			m_ReleasedDocRevs = new TCComponent[m_DocRevsInContext.size()];
				
		for(int iCnt = 0; iCnt < selectedRows.length;iCnt++)
		{
			try {
				TCComponent DocRef = m_DocRevsInContext.get(selectedRows[iCnt]).getTCProperty(NOVSMDLConstants.DOCUMENT_ITEM_REF).getReferenceValue();

				TCComponent[] revList = DocRef.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();

				for(int i = revList.length -1; i >= 0; i--){
					TCComponent[] relStatus = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
					if(relStatus!= null && relStatus.length > 0){
						m_ReleasedDocRevs[selectedRows[iCnt]] = revList[i];

						String revId = revList[i].getTCProperty(NOVSMDLConstants.DOCUMENT_REV_ID).getStringValue();

						m_documentsTable.repaint();
						break;
					}
				}
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}
		
		checkAllReleased();
	}
	
	protected boolean checkAllReleased() {
		boolean m_AllReleased = true;
		
		if(m_ReleasedDocRevs != null && m_ReleasedDocRevs.length > 0){
			for(int iCnt=0; iCnt < m_ReleasedDocRevs.length; iCnt++){
				if(m_ReleasedDocRevs[iCnt] == null){
					m_AllReleased = false;
					break;
				}
			}
		}
		else if(m_ReleasedDocRevisions != null && m_ReleasedDocRevisions.size() > 0){
			for(int iCnt=0; iCnt < m_ReleasedDocRevisions.size(); iCnt++){
				if(m_ReleasedDocRevisions.get(iCnt) == null){
					m_AllReleased = false;
					break;
				}
			}
		}		
		else{
			m_AllReleased = false;
		}
		
		return m_AllReleased;
	}
	
	public boolean isSMDLClosable() {
		
		selectAllDocuments(true);
		
		boolean bCloseSMDL = checkAllReleased();
				
		if(!bCloseSMDL){
			Shell parentShell = AIFUtility.getActiveDesktop().getShell();
			MessageBox errorMessageBox = new MessageBox(parentShell, SWT.ERROR);
			errorMessageBox.setText(m_Registry.getString("Error.TITLE"));
			errorMessageBox.setMessage(m_Registry.getString("DocReleaseError.MSG"));
			errorMessageBox.open();	
		}

		return bCloseSMDL;
	}
	
	public void selectAllDocuments(boolean bSelectAll){
		int[] selectedRows = new int[m_documentsTable.getRowCount()];
		
		for(int iCnt = 0; iCnt < m_documentsTable.getRowCount(); iCnt++){
			selectedRows[iCnt] = iCnt;
		}
		showLatestReleasedRevs(selectedRows);
	}
	
	protected void closeSMDLRevision() throws TCException 
	{
		
		m_ReleasedDocRevisions.addAll(m_ReleasedDocRevisions.size(),Arrays.asList(m_ReleasedDocRevs));
		
		//if(m_DocRevsInContext.size() > 0)
		if(m_ReleasedDocRevisions.size() > 0)
		{		
		
			//CreateInObjectHelper[] smdlCodeCreateInObjects = new CreateInObjectHelper[m_DocRevsInContext.size()];
			CreateInObjectHelper[] smdlCodeCreateInObjects = new CreateInObjectHelper[m_ReleasedDocRevisions.size()];
			TCComponent codeForm = null;
			TCComponent docRev= null;
			for(int iCnt = 0; iCnt </*m_DocRevsInContext*/m_ReleasedDocRevisions.size(); iCnt++)
			{
				docRev= /*m_DocRevsInContext*/m_ReleasedDocRevisions.get(iCnt);
			
				try 
				{
					String docName= docRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue();
					codeForm = getCodeForm(docName);
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				smdlCodeCreateInObjects[iCnt] = new CreateInObjectHelper(NOV4ComponentSMDL.CODEFORM_OBJECT_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
				//smdlCodeCreateInObjects[iCnt].setTargetObject(m_DocRevsInContext.get(iCnt));
				smdlCodeCreateInObjects[iCnt].setTargetObject(codeForm);
				smdlCodeCreateInObjects[iCnt].setTagProps(NOV4ComponentSMDL.DOCUMENT_REV_REF, /*m_ReleasedDocRevs*/m_ReleasedDocRevisions.get(iCnt), CreateInObjectHelper.SET_PROPERTY);
			}
			
			ProgressMonitorDialog closeSMDLDialog=new ProgressMonitorDialog(this.getShell());		
			CreateOrUpdateOperation createCodeOp = new CreateOrUpdateOperation(smdlCodeCreateInObjects);
			
			try {
				closeSMDLDialog.run(true, false,createCodeOp);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// Get SMDL Item from SOA Plain Object
		TCComponent smdlItem;
		smdlItem = m_smdlSOAPlainObject.getTCProperty(NOV4ComponentSMDLRevision.PROP_ITEM).getReferenceValue();
		applyReleaseStatus(smdlItem);
		
	}

	private void applyReleaseStatus(TCComponent smdlItem) throws TCException
	{
		
		WorkflowService workFlowService = WorkflowService.getService(smdlItem.getSession());
		
		ReleaseStatusInput relStatusInput = new ReleaseStatusInput();
		
		relStatusInput.objects = new TCComponent[] {smdlItem};
		
		ReleaseStatusOption relStatusOption = new ReleaseStatusOption();
//		relStatusOption.existingreleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.newReleaseStatusTypeName = NOV4ComponentSMDL.SMDL_CLOSED_STATUS_NAME;
		relStatusOption.operation = "Append";
		
		relStatusInput.operations = new ReleaseStatusOption[]{relStatusOption};
		
		try {
			SetReleaseStatusResponse theRersponse = workFlowService.setReleaseStatus(new ReleaseStatusInput[] {relStatusInput});
			
			
			ArrayList<String> errorMessages = new ArrayList<String>();
			for(ErrorStack errorStack: SOAHelper.getErrors (theRersponse.serviceData)) 
			{				
					Collections.addAll(errorMessages, errorStack.getMessages());
			}
			
			SOAHelper.raiseTCException(errorMessages);
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
		
	private Vector<TCComponent> getInProgressDocumentsRevs(Vector<TCComponent> codeFormObjs) {
		
		for(int iCount = 0;iCount < codeFormObjs.size(); iCount++ ){
			try {
				
				TCComponent smdlDoc = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
				
				// Check if any Document available on Code Form
				if(smdlDoc == null){
					m_EmptyCodeForms = new Vector<TCComponent>();
					m_EmptyCodeForms.add(codeFormObjs.get(iCount));
					continue;
				}
				
				TCComponent docLatestRev = null;
				TCComponent[] docRevisionList = smdlDoc.getTCProperty(NOVSMDLConstants.REVISION_LIST).getReferenceValueArray();
				
				docLatestRev = codeFormObjs.get(iCount).getTCProperty(NOV4ComponentSMDL.DOCUMENT_REV_REF).getReferenceValue();
				if (docLatestRev == null)
					docLatestRev = docRevisionList[docRevisionList.length -1];
				
				TCComponent[] docRevStatus = docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_STATUS).getReferenceValueArray();
				
				m_codeFormDocMap.put(codeFormObjs.get(iCount), docLatestRev.getTCProperty(NOVSMDLConstants.DOCUMENT_TITLE).getStringValue());

				if(docRevStatus != null && docRevStatus.length == 0)
				{

					if(!m_DocRevsInContext.contains(smdlDoc))
					{
						m_DocRevsInContext.add(docLatestRev);
					}

				}
				else
				{
					m_ReleasedDocRevisions.add(docLatestRev);
				}
				
				
			} catch (TCException e) {
				e.printStackTrace();
			}
			
		}
		
		return m_DocRevsInContext;
	}
	public void closeSMDL()
	{
		try
		{
			closeSMDLRevision();
			// Send the Notification for SMDL Close
			sendSMDLNotification(CLOSE_NOTIFICATION_CONTEXT);
		}
		catch(TCException ex)
		{
			com.teamcenter.rac.util.MessageBox.post(ex);
		}	
	}
	
}
