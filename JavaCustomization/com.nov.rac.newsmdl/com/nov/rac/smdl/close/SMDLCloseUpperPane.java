package com.nov.rac.smdl.close;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.noi.util.components.JIBPlantLOVList;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class SMDLCloseUpperPane extends Composite {

	private SMDLIdPane m_smdlIdPane;
	private CustomerPane m_custPane;
	private EngJobPane m_engJobPane;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public SMDLCloseUpperPane(Composite parent, int style) {
		super(parent, style);
	
		createUI(this);
	}

	private void createUI(Composite parent){

		setLayout(new GridLayout(3,true));
		setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		m_smdlIdPane = new SMDLIdPane(parent, SWT.NONE);
		
		m_custPane = new CustomerPane(parent, SWT.NONE);

		m_engJobPane = new EngJobPane(parent, SWT.NONE);
		
	}
	
	public void populateUpperPane(TCComponent smdlRevision){
		
		m_smdlIdPane.populateTextVals(smdlRevision);
		
		m_custPane.populateTextVals(smdlRevision);
		
		m_engJobPane.populateTextvals(smdlRevision);
		
	}

}
