package com.nov.rac.smdl.reports.panels;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.AbstractReportDataProvider;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public class FileTypePanel extends AbstractReportDataProvider
{
    private Registry m_registry;
    private Button m_plannedIssueDateButton;
    private Button m_orderButton;
    private String m_fileType;
    private Label m_titleLabel;

    public FileTypePanel(Composite parent, int style) 
    {
        super(parent,style);
        m_registry= Registry.getRegistry("com.nov.rac.smdl.reports.panels.panels");
        createUI();
    }

    
    private void createUI()
    {
        Composite fileTypePanel= this;
        fileTypePanel.setLayout(new GridLayout(1, true));
        GridData gd_fileTypePanel = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        fileTypePanel.setLayoutData(gd_fileTypePanel);
        
        Composite fileTypeTitlePanel=new Composite(fileTypePanel, SWT.NONE);
        Composite fileTypeFiledsPanel=new Composite(fileTypePanel, SWT.NONE);
        
        createTitlePanel(fileTypeTitlePanel);
        
        createFiledsPanel(fileTypeFiledsPanel);
        
        setLabels();
        
    }


    private void createFiledsPanel(Composite fileTypeFiledsPanel) 
    {
        fileTypeFiledsPanel.setLayout(new GridLayout(1, true));
        GridData gd_reportsTypeFiledsPanel = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        fileTypeFiledsPanel.setLayoutData(gd_reportsTypeFiledsPanel);
        
        m_plannedIssueDateButton = new Button(fileTypeFiledsPanel, SWT.RADIO);
        m_plannedIssueDateButton.setSelection(true);
        m_orderButton = new Button(fileTypeFiledsPanel, SWT.RADIO);
        
    }


    private void createTitlePanel(Composite fileTypeTitlePanel) 
    {
        fileTypeTitlePanel.setLayout(new GridLayout(1, true));
        GridData gd_reportsTypeTitlePanel= new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1);
        fileTypeTitlePanel.setLayoutData(gd_reportsTypeTitlePanel);
        
        m_titleLabel = new Label(fileTypeTitlePanel, SWT.NONE);
        FontData[] fD = m_titleLabel.getFont().getFontData();
        fD[0].setStyle(SWT.BOLD);
        m_titleLabel.setFont( new Font(getDisplay(),fD[0]));
    }


    private void setLabels()
    {
        m_titleLabel.setText(m_registry.getString("fileTypePanel.Label"));
        m_plannedIssueDateButton.setText(m_registry.getString("plannedIssueDateButton.Label"));
        m_orderButton.setText(m_registry.getString("orderButton.Label"));
    }


    @Override
    public void firePropertyChange() 
    {
        if(m_plannedIssueDateButton.getSelection())
        {
            m_fileType = ReportsConstants.BY_PLANNED_DATE;
        }
        else if(m_orderButton.getSelection())
        {
            m_fileType =ReportsConstants.BY_ORDER;
        }
        firePropertyChange(this, ReportsConstants.FILE_TYPE,"",m_fileType);
    }
    
    public void docResponsibleOptionSelected(boolean option)
    {
        if(!option)
        {
            m_plannedIssueDateButton.setSelection(true);
            m_orderButton.setSelection(false);
            m_orderButton.setEnabled(false);
            m_orderButton.setGrayed(true);
        }
        else
        {
            m_orderButton.setEnabled(true);
            m_orderButton.setGrayed(false);
        }
    }


    @Override
    public void populateReportData(IPropertyMap dataMap) 
    {
        // TODO NOTHING
        
    }


    @Override
    protected boolean isValidState() {
        return true;
    }



}
