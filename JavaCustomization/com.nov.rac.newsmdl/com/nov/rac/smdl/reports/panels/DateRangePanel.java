package com.nov.rac.smdl.reports.panels;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.common.NOVDateButtonComposite;
import com.nov.rac.smdl.reports.AbstractReportDataProvider;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.util.Registry;

public class DateRangePanel extends AbstractReportDataProvider
{
	private Registry m_registry;

	private Label m_titleLabel;

	private Label m_fromDateLabel;

	private NOVDateButtonComposite m_fromDateCalendar;
	private NOVDateButtonComposite m_toDateCalendar;

	private Label m_toDateLabel;

	public DateRangePanel(Composite parent, int style) 
	{
		super(parent,style);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.reports.panels.panels");
		createUI();
	}

    
    private void createUI()
    {
        Composite dateRangePanel= this;
        dateRangePanel.setLayout(new GridLayout(1, true));
        GridData gd_dateRangePanel = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        dateRangePanel.setLayoutData(gd_dateRangePanel);
        
        Composite dateRangeTitlePanel=new Composite(dateRangePanel, SWT.NONE);
        Composite dateRangeFiledsPanel=new Composite(dateRangePanel, SWT.NONE);
        
        createTitlePanel(dateRangeTitlePanel);
        
        createFiledsPanel(dateRangeFiledsPanel);
        
        setLabels();
        
        setDefaultValues();     
    }

	private void createFiledsPanel(Composite dateRangeFiledsPanel)
	{
		dateRangeFiledsPanel.setLayout(new GridLayout(2, true));
		GridData gd_dateRangeFiledsPanel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		dateRangeFiledsPanel.setLayoutData(gd_dateRangeFiledsPanel);
		
		Composite fromDateComposite = new Composite(dateRangeFiledsPanel, SWT.NONE);
		Composite toDateComposite = new Composite(dateRangeFiledsPanel, SWT.NONE);
		
		fromDateComposite.setLayout(new GridLayout(2, false));
		GridData gd_fromDateComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		fromDateComposite.setLayoutData(gd_fromDateComposite);
		
		m_fromDateLabel = new Label(fromDateComposite, SWT.NONE);
		m_fromDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_fromDateCalendar=new NOVDateButtonComposite(fromDateComposite, SWT.NONE);
		GridData gd_fromDateCalendar = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_fromDateCalendar.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_fromDateCalendar, m_heightHintPixels);
		gd_fromDateCalendar.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_fromDateCalendar, m_widthHintPixels);
		m_fromDateCalendar.setLayoutData(gd_fromDateCalendar);
		
		toDateComposite.setLayout(new GridLayout(2, false));
		GridData gd_toDateComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		toDateComposite.setLayoutData(gd_toDateComposite);
		
		m_toDateLabel = new Label(toDateComposite, SWT.NONE);
		m_toDateLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_toDateCalendar=new NOVDateButtonComposite(toDateComposite, SWT.NONE);
		GridData gd_toDateCalendar = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_toDateCalendar.heightHint = SWTUIHelper.convertVerticalDLUsToPixels(m_toDateCalendar, m_heightHintPixels);
		gd_toDateCalendar.widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_toDateCalendar, m_widthHintPixels);
		m_toDateCalendar.setLayoutData(gd_toDateCalendar);
	}

	private void createTitlePanel(Composite dateRangeTitlePanel) 
	{
		dateRangeTitlePanel.setLayout(new GridLayout(1, true));
		GridData gd_dateRangeTitlePanel= new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1);
		dateRangeTitlePanel.setLayoutData(gd_dateRangeTitlePanel);
		
		m_titleLabel = new Label(dateRangeTitlePanel, SWT.NONE);
		FontData[] fD = m_titleLabel.getFont().getFontData();
		fD[0].setStyle(SWT.BOLD);
		m_titleLabel.setFont( new Font(getDisplay(),fD[0]));
	}

	private void setLabels()
	{
		m_titleLabel.setText(m_registry.getString("dateRangePanel.Label"));
		m_fromDateLabel.setText(m_registry.getString("fromDate.Label"));
		m_toDateLabel.setText(m_registry.getString("toDate.Label"));
	}

    
    private void setDefaultValues()
    {
        Calendar calendar = Calendar.getInstance(); 
        Date today = calendar.getTime();   
        
        calendar.add(Calendar.DAY_OF_MONTH, m_amountOfTime);
        Date toDate = calendar.getTime();
        
        m_fromDateCalendar.setDate(today);
        m_toDateCalendar.setDate(toDate);
        
    }

	@Override
	public void populateReportData(IPropertyMap dataMap) 
	{
		Date fromDate = m_fromDateCalendar.getDate();
		Date toDate = m_toDateCalendar.getDate();
		
		dataMap.setDateArray(ReportsConstants.DATE_RANGE, new Date[]{fromDate,toDate});
		
		/*String fromDate = m_fromDateCalendar.getDateString();
		String toDate = m_toDateCalendar.getDateString();
		
		dataMap.setStringArray(ReportsConstants.DATE_RANGE, new String[]{fromDate,toDate});*/
	}


	@Override
	protected boolean isValidState() {
		return true;
	}



	/*public ArrayList <QuickSearchService.QuickBindVariable>  getBindVariables()
	{
		String fromDate = m_fromDateCalendar.getDateString();
		String toDate = m_toDateCalendar.getDateString();
      	 
		 ArrayList <QuickSearchService.QuickBindVariable> bindVars = new ArrayList<QuickSearchService.QuickBindVariable> ();
		
		//dateRange
		 QuickSearchService.QuickBindVariable dateVar = new QuickSearchService.QuickBindVariable ();
		 dateVar.nVarType = INOVSearchProvider.POM_date;
		 dateVar.nVarSize = 2;
		 dateVar.strList = new String[] {fromDate,toDate};
		 
		 bindVars.add(dateVar);
		
		 return bindVars;
	}*/

	private final int m_heightHintPixels = 13;
	private final int m_widthHintPixels = 100;
	private final int m_amountOfTime = 7;
	
}

