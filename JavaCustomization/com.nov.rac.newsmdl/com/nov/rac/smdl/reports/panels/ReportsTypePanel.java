package com.nov.rac.smdl.reports.panels;

import java.util.Arrays;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.AbstractReportDataProvider;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.utils.UIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.NameIconIdStruct;
import com.teamcenter.rac.kernel.TCComponentUserType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ArraySorter;
import com.teamcenter.rac.util.Registry;

public class ReportsTypePanel  extends AbstractReportDataProvider
{
    private Button m_docResponsibleButton;
    private Button m_managerReportButton;
    private Button m_hiLevelReportButton;
    
    //TCDECREL-6132: Introduced getter, setter for accessibility in ReportsDialog.
    public Combo getM_docResponsiblCombo()
    {
        return m_docResponsiblCombo;
    }
    
    public void setM_docResponsiblCombo(Combo m_docResponsiblCombo)
    {
        this.m_docResponsiblCombo = m_docResponsiblCombo;
    }


    public Combo getM_managerReportCombo()
    {
        return m_managerReportCombo;
    }


    public void setM_managerReportCombo(Combo m_managerReportCombo)
    {
        this.m_managerReportCombo = m_managerReportCombo;
    }


    public Combo getM_hiLevelReportCombo()
    {
        return m_hiLevelReportCombo;
    }


    public void setM_hiLevelReportCombo(Combo m_hiLevelReportCombo)
    {
        this.m_hiLevelReportCombo = m_hiLevelReportCombo;
    }
    public Vector<String> getM_docResponsibleList()
    {
        return m_docResponsibleList;
    }


    public void setM_docResponsibleList(Vector<String> m_docResponsibleList)
    {
        this.m_docResponsibleList = m_docResponsibleList;
    }


    public Vector<String> getM_managersL1List()
    {
        return m_managersL1List;
    }


    public void setM_managersL1List(Vector<String> m_managersL1List)
    {
        this.m_managersL1List = m_managersL1List;
    }


    public Vector<String> getM_highLevelList()
    {
        return m_highLevelList;
    }


    public void setM_highLevelList(Vector<String> m_highLevelList)
    {
        this.m_highLevelList = m_highLevelList;
    }

    private Combo m_docResponsiblCombo;
    private Combo m_managerReportCombo;
    private Combo m_hiLevelReportCombo;
    private Label m_titleLabel;
    private String m_reportLevel ;
    private Vector<String> m_docResponsibleList;
    private Vector<String> m_managersL1List;
    private Vector<String> m_highLevelList;
    
    private String m_prefManagerList;
    private String m_prefHighLevelList;
    
    public ReportsTypePanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry("com.nov.rac.smdl.reports.panels.panels");
        m_prefManagerList = "SMDL_Reports_GroupLevel1_list";
        m_prefHighLevelList = "SMDL_Reports_GroupLevel2_list";
        createUI();
    }

    
    private void createUI()
    {
        Composite reportsTypePanel= this;
        reportsTypePanel.setLayout(new GridLayout(1, true));
        GridData gd_reportsTypePanel = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        reportsTypePanel.setLayoutData(gd_reportsTypePanel);
        
        Composite reportsTypeTitlePanel=new Composite(reportsTypePanel, SWT.NONE);
        Composite reportsTypeFiledsPanel=new Composite(reportsTypePanel, SWT.NONE);
        
        createTitlePanel(reportsTypeTitlePanel);
        
        createFiledsPanel(reportsTypeFiledsPanel);
        
        populateListValues();

        setLabels();
        
        addComboFiltersFilters();
        
        setDefaultValues();
        
        //addlisteners();
    }
    
    private void addComboFiltersFilters()
    {
        UIHelper.setAutoComboArray(m_docResponsiblCombo, m_docResponsibleList); 
        UIHelper.setAutoComboArray(m_managerReportCombo, m_managersL1List);
        UIHelper.setAutoComboArray(m_hiLevelReportCombo, m_highLevelList);
    }


    private void setDefaultValues()
    {
        m_docResponsibleButton.setSelection(true);
        String defaultDocResponsibleValue = getDocResponsibleDefaultvalue();
        m_docResponsiblCombo.setText(defaultDocResponsibleValue);
        m_managerReportCombo.setEnabled(false);
        m_hiLevelReportCombo.setEnabled(false);
        m_reportLevel= ReportsConstants.DOCRESPONSIBLE_TYPE;     
    }


    private void createFiledsPanel(Composite reportsTypeFiledsPanel) 
    
    {
        reportsTypeFiledsPanel.setLayout(new GridLayout(2, true));
        GridData gd_reportsTypeFiledsPanel = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        reportsTypeFiledsPanel.setLayoutData(gd_reportsTypeFiledsPanel);
        
        GridData gd_text = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        m_docResponsibleButton = new Button(reportsTypeFiledsPanel, SWT.RADIO);
        
        m_docResponsiblCombo = new Combo(reportsTypeFiledsPanel, SWT.BORDER);
        m_docResponsiblCombo.setLayoutData(gd_text);
        
        m_managerReportButton = new Button(reportsTypeFiledsPanel, SWT.RADIO);
        m_managerReportCombo = new Combo(reportsTypeFiledsPanel, SWT.BORDER);
        m_managerReportCombo.setLayoutData(gd_text);
        m_hiLevelReportButton = new Button(reportsTypeFiledsPanel, SWT.RADIO);
        m_hiLevelReportCombo = new Combo(reportsTypeFiledsPanel, SWT.BORDER);
        m_hiLevelReportCombo.setLayoutData(gd_text);
    }


    private void createTitlePanel(Composite reportsTypeTitlePanel) 
    {
        reportsTypeTitlePanel.setLayout(new GridLayout(1, true));
        GridData gd_reportsTypeTitlePanel= new GridData(SWT.LEFT, SWT.FILL, true, true, 1, 1);
        reportsTypeTitlePanel.setLayoutData(gd_reportsTypeTitlePanel);
        
        m_titleLabel = new Label(reportsTypeTitlePanel, SWT.NONE);
        FontData[] fD = m_titleLabel.getFont().getFontData();
        fD[0].setStyle(SWT.BOLD);
        m_titleLabel.setFont( new Font(getDisplay(),fD[0]));
    }

    private void populateListValues()
    {   
        populateDocumentResponsibleList();
        populateGroupLevel1List();
        populateGroupLevel2List();        
    }
    
    private void populateGroupLevel1List()
    {
        
        String[] groups = PreferenceUtils.getStringValues(TCPreferenceService.TC_preference_site, m_prefManagerList);
        
        if (null != groups)
        {
            ArraySorter.sort(groups);
            m_managersL1List = new Vector<String>(Arrays.asList(groups));
            NOVSMDLComboUtils.setAutoComboArray(m_managerReportCombo, groups);
        }
    }
    
    private void populateGroupLevel2List()
    {
        
        String[] groups = PreferenceUtils.getStringValues(TCPreferenceService.TC_preference_site, m_prefHighLevelList);
        
        if (null != groups)
        {
            ArraySorter.sort(groups);
            m_highLevelList = new Vector<String>(Arrays.asList(groups));
            NOVSMDLComboUtils.setAutoComboArray(m_hiLevelReportCombo, groups);
        }
    }

    private void populateDocumentResponsibleList() 
    {
        String[] users = null;
        if (users == null)
        {
            TCComponentUserType tccomponentusertype;
            TCSession session = (TCSession) AIFUtility.getDefaultSession();

            try 
            {
                tccomponentusertype = (TCComponentUserType) session.getTypeComponent("User");
                
                NameIconIdStruct anameiconidstruct[] = tccomponentusertype.getUserAndIconIdListByUser(null);
                
                users = new String[anameiconidstruct.length];
                
                for (int inx = 0; inx < anameiconidstruct.length; inx++)
                {
                    users[inx] = anameiconidstruct[inx].getObjectName();
                }
                
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        } 

        ArraySorter.sort(users);
        if(null != users)
        {
            m_docResponsibleList = new Vector<String>(Arrays.asList(users));
            NOVSMDLComboUtils.setAutoComboArray(m_docResponsiblCombo, users);
        }
        
    }


    private String getDocResponsibleDefaultvalue()
    {
        TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
        String loggedInUserName = null;
        
        loggedInUserName = theSession.getUser().toString();
        
        return  loggedInUserName;
    }

    private void setLabels()
    {
        m_titleLabel.setText(m_registry.getString("reportsTypePanel.Label"));
        m_docResponsibleButton.setText(m_registry.getString("docResponsible.Label"));
        m_managerReportButton.setText(m_registry.getString("managerReport.Label"));
        m_hiLevelReportButton.setText(m_registry.getString("hiLevelReport.Label"));
    }

    

    public void addlisteners(final FileTypePanel filePanel)
    {
        
        SelectionListener listener = new SelectionListener()
        {
            
            public void widgetSelected(SelectionEvent event)
            {
                if (m_docResponsibleButton.getSelection())
                {
                    
                    m_docResponsiblCombo.setEnabled(true);
                    m_managerReportCombo.setEnabled(false);
                    m_hiLevelReportCombo.setEnabled(false);
                    m_docResponsiblCombo.setText(getDocResponsibleDefaultvalue());
                    m_managerReportCombo.setText("");
                    m_hiLevelReportCombo.setText("");
                    filePanel.docResponsibleOptionSelected(true);
                    m_reportLevel = ReportsConstants.DOCRESPONSIBLE_TYPE;
                }
                
                else if (m_managerReportButton.getSelection())
                {
                    m_docResponsiblCombo.setEnabled(false);
                    m_managerReportCombo.setEnabled(true);
                    m_hiLevelReportCombo.setEnabled(false);
                    m_docResponsiblCombo.setText("");
                    m_hiLevelReportCombo.setText("");
                    filePanel.docResponsibleOptionSelected(false);
                    m_reportLevel = ReportsConstants.MANAGER_TYPE;
                }
                else if (m_hiLevelReportButton.getSelection())
                {
                    m_docResponsiblCombo.setEnabled(false);
                    m_managerReportCombo.setEnabled(false);
                    m_hiLevelReportCombo.setEnabled(true);
                    m_docResponsiblCombo.setText("");
                    m_managerReportCombo.setText("");
                    filePanel.docResponsibleOptionSelected(false);
                    m_reportLevel = ReportsConstants.HIGHLEVEL_TYPE;
                }
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                // TODO Auto-generated method stub
                
            }
            
        };
        
        m_hiLevelReportButton.addSelectionListener(listener);
        m_managerReportButton.addSelectionListener(listener);
    }


    @Override
    public void firePropertyChange() 
    {
        firePropertyChange(this, ReportsConstants.REPORT_LEVEL,"",m_reportLevel);
    }


    @Override
    public void populateReportData(IPropertyMap dataMap) 
    {
        String[] users =  getDocResponsibleUsers(); 
        dataMap.setStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE,users);
        
        dataMap.setStringArray(m_prefManagerList, m_managersL1List.toArray(new String[m_managersL1List.size()]));
        dataMap.setStringArray(m_prefHighLevelList, m_highLevelList.toArray(new String[m_highLevelList.size()]));
        
    }


    private String[] getDocResponsibleUsers() 
    {
        String[] users = null;
        if (m_docResponsibleButton.getSelection())
         {
            users= new String[]{m_docResponsiblCombo.getText()};
         }
         else if (m_managerReportButton.getSelection())
         {
             users= new String[]{ m_managerReportCombo.getText()};
         }
         else if (m_hiLevelReportButton.getSelection())
         {
             users= new String[]{m_hiLevelReportCombo.getText()};
         }
        
        return users;
    }
        
    @Override
    protected boolean isValidState()
    {
        boolean stat = false;
        if(m_docResponsibleButton.getSelection())
        {
            String selectUser = m_docResponsiblCombo.getText();
            stat = m_docResponsibleList.contains(selectUser);
        }
        else if(m_managerReportButton.getSelection())
        {
            String selectUser = m_managerReportCombo.getText();
            stat = m_managersL1List.contains(selectUser);
            
        }
        else if(m_hiLevelReportButton.getSelection())
        {
            String selectgroup = m_hiLevelReportCombo.getText();
            stat = m_highLevelList.contains(selectgroup);
        }
        return stat;
    }
    
}

