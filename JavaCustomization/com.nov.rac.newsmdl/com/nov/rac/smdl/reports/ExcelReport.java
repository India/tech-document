package com.nov.rac.smdl.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

abstract public class ExcelReport
{
	private Registry m_registry;
	private HSSFWorkbook m_workBook  = null;
	private Shell m_shell;

	public ExcelReport(Shell shell)
	{
		m_shell= shell;
		m_registry = Registry.getRegistry(this);
	}

	public boolean exportDataToExcel() throws IOException
	{
	    boolean success = Boolean.TRUE;
		/* Opens File Save dialog to select File name */
		FileDialog fd = new FileDialog(m_shell, SWT.SAVE);
		fd.setText(m_registry.getString("Save.TITLE"));
		fd.setFilterPath(System.getProperty("user.home"));
		String[] filterExt = { "*.xls" };
		fd.setFilterExtensions(filterExt);

		String fileName = fd.open();

		if( fileName != null && !fileName.isEmpty())
		{
		    java.io.File file = new File(fileName);
			// Write the output to a file
			try
			{
			    FileOutputStream out = new FileOutputStream(file);
			    m_workBook = new HSSFWorkbook();
				HSSFSheet sheet = m_workBook.createSheet();
				m_workBook.setSheetName(0, m_registry.getString("Sheet.TITLE"));

				createContent(sheet);

				m_workBook.write(out);
				out.close();
				MessageBox.post(Display.getDefault().getActiveShell(),"Data successfully exported to " + file.toString(), 
				        "Information", MessageBox.INFORMATION);	
				success = false;
			}
			catch (FileNotFoundException ex)
            {
                success = false;
                MessageBox.post(Display.getDefault().getActiveShell(),"The file " + file.toString()+" is probably open in another window. " +
                		"Please close the file and try again or save it to a new file. ", 
                        "Information", MessageBox.INFORMATION);
                
            }
			catch (Exception e)
			{
			    success = false;
				e.printStackTrace();
			}
		}
		else if(fileName == null)
        {
            success = false;
        }
		return success;
	}

	    abstract public void createContent(HSSFSheet sheet);

	    protected void createTableHeader(HSSFSheet sheet, String[] headerTitle)
	    {
	        if (headerTitle != null && headerTitle.length > 0)
	        {
	            int tableColWidth[] = getColumnWidth(headerTitle);

	            // Create Header cell style
	            HSSFCellStyle csHeader = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex,
	                    HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(),
	                    HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_CENTER, false);

	            // Write Header row
	            ExcelWriter.createHeader(sheet, csHeader, headerTitle, tableColWidth);
	        }
	    }

	    protected void createTableHeader(HSSFSheet sheet, String[] headerTitle, HSSFCellStyle csHeader)
	    {
	        if (headerTitle != null && headerTitle.length > 0)
	        {
	            int tableColWidth[] = getColumnWidth(headerTitle);

	            // Write Header row
	            ExcelWriter.createHeader(sheet, csHeader, headerTitle, tableColWidth);
	        }
	    }

	    protected void createTableBody(HSSFSheet sheet, String[][] sData, int[] colType)
	    {
	        if (sData != null)
	        {
	            // Create Body cell styles
	            HSSFCellStyle csBody = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableBodyIndex,
	                    HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.WHITE().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
	                    HSSFCellStyle.ALIGN_LEFT, false);
	            HSSFCellStyle csBodyAlt = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableBodyIndex,
	                    HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.GREY_25_PERCENT().getIndex(),
	                    HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);

	            // Write Table data
	            ExcelWriter.createBody(sheet, csBody, csBodyAlt, sData, colType);
	        }
	    }

	    protected void createTableBody(HSSFSheet sheet, String[][] sData, int[] colType, HSSFCellStyle csBody,
	            HSSFCellStyle csBodyAlt)
	    {
	        int nStartRow = 0;
	        int nEndRow = sData.length;
	        int nStartColumn = 0;
	        int nEndColumn = sData[0].length;

	        // Write Table data
	        createTableBody(sheet, sData, colType, csBody, csBodyAlt, nStartRow, nEndRow, nStartColumn, nEndColumn);

	    }

	    protected void createTableBody(HSSFSheet sheet, String[][] sData, int[] colType, HSSFCellStyle csBody,
	            HSSFCellStyle csBodyAlt, int nStartRow, int nEndRow, int nStartColumn, int nEndColumn)
	    {
	        if (sData != null)
	        {
	            // Write Table data
	            ExcelWriter.createBody(sheet, csBody, csBodyAlt, sData, nStartRow, nEndRow, nStartColumn, nEndColumn,
	                    colType);
	        }
	    }


	    protected int[] getColumnWidth(String[] headerTitle)
	    {
	        int iHeaderCount = headerTitle.length;
	        int[] tableColWidth = new int[iHeaderCount];
	        for (int iHeaderIndex = 0; iHeaderIndex < iHeaderCount; iHeaderIndex++)
	        {
	            tableColWidth[iHeaderIndex] = headerTitle[iHeaderIndex].length() * m_colWidthConstant;
	        }
	        return tableColWidth;
	    }

	    private final int m_tableHeaderIndex = 11;
	    private final int m_tableBodyIndex = 10;
	    private final int m_colWidthConstant = 500;
}
