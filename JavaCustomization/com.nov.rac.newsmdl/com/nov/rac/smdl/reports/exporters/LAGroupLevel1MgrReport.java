package com.nov.rac.smdl.reports.exporters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;

public class LAGroupLevel1MgrReport extends AbstractGroupLevelReport
{
    
    public LAGroupLevel1MgrReport(Shell shell, IPropertyMap propMap, String[][] sData)
    {
        super(shell, propMap, sData);
    }
    
    @Override
    protected void createDateHeader(HSSFRow row, HSSFCellStyle headerStyle, HSSFCellStyle dateStyle)
    {
        
        HSSFCell cell = row.createCell(m_dateRangeTitleIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DATE_RANGE + ".TITLE"));
        cell.setCellStyle(headerStyle);
        
        cell = row.createCell(m_dateRangeFromValIndex);
        if (m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0] != null)
        {
            cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0]);
            cell.setCellStyle(dateStyle);
        }
        
        cell = row.createCell(m_dateRangeToTitleIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.TO + ".TITLE"));
        cell.setCellStyle(headerStyle);
        
        cell = row.createCell(m_dateRangeToValIndex);
        if (m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1] != null)
        {
            cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1]);
            cell.setCellStyle(dateStyle);
        }
    }
    
    @Override
    protected void init()
    {
        String[] sCols = m_registry.getStringArray("LookAheadGroupLevel1MgrReport.COLUMNS");
        m_sHeader = new String[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; ++i)
        {
            m_sHeader[i] = m_registry.getString(sCols[i] + ".TITLE");
        }
        
        m_iColumnType = new int[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; ++i)
        {
            m_iColumnType[i] = Integer.parseInt(m_registry.getString(sCols[i] + ".TYPE"));
        }
        
        m_reportTitle = m_registry.getString("LookAheadGroupLevel1MgrReport.REPORTTITLE");
        
    }
    
    protected void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data,
            int nStartRow, int nEndRow, int nStartColumn, int nEndColumn, int[] colTypes)
    {
        HSSFCellStyle[][] cellStyles = ExcelWriter.getFormatters(sheet, cellStyle, cellAltStyle);
        DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        
        int iRowToInsertData = sheet.getPhysicalNumberOfRows();
        for (int rowNum = 0; rowNum < nEndRow; rowNum++)
        {
            HSSFRow row = sheet.createRow(iRowToInsertData + rowNum);
            
            for (int colNum = nStartColumn; colNum < nEndColumn; colNum++)
            {
                HSSFCell cell = null;
                HSSFCellStyle cs = cellStyles[rowNum % 2][colTypes[colNum] - 1];
                cell = row.createCell(colNum);
                
                try
                {
                    if (!data[nStartRow][colNum].isEmpty())
                    {
                        switch (colTypes[colNum])
                        {
                            case 1:
                                cell.setCellValue(data[nStartRow][colNum]);
                                cell.setCellStyle(cs);
                                break;
                            
                            case 2:
                                Date dt = (Date) dateFormatter.parse(data[nStartRow][colNum]);
                                cell.setCellValue(dt);
                                cell.setCellStyle(cs);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                if (colNum == m_mergeStartIndex)
                {
                    for (int iCells = colNum; iCells < m_mergeEndIndex; iCells++)
                    {
                        cell = row.createCell(iCells + m_incrementalIndex);
                        ExcelWriter.setThinBorderStyle(cell, cellStyle);
                    }
                    sheet.addMergedRegion(new CellRangeAddress(cell.getRowIndex(), cell.getRowIndex(), colNum,
                            m_mergeEndIndex));
                    
                }
                
                ExcelWriter.setThinBorderStyle(cell, cs);
            }
            nStartRow++;
        }
    }
    
    protected void createHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle, int[] colWidth)
    {
        int iHeaderRow = sheet.getPhysicalNumberOfRows();
        HSSFRow row = sheet.createRow(iHeaderRow);
        int iHeaderLength = headerTitle.length;
        for (int iColIndex = 0; iColIndex < iHeaderLength; iColIndex++)
        {
            
            HSSFCell cell = row.createCell(iColIndex);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(headerTitle[iColIndex]);
            if (iColIndex == m_mergeStartIndex)
            {
                for (int iCells = iColIndex; iCells < m_mergeEndIndex; iCells++)
                {
                    cell = row.createCell(iCells + m_incrementalIndex);
                    ExcelWriter.setThinBorderStyle(cell, cellStyle);
                }
                sheet.addMergedRegion(new CellRangeAddress(cell.getRowIndex(), cell.getRowIndex(), iColIndex,
                        m_mergeEndIndex));
            }
            sheet.setColumnWidth(iColIndex, colWidth[iColIndex]);
            ExcelWriter.setThinBorderStyle(cell, cellStyle);
        }
    }
    
    private final int m_dateRangeTitleIndex = 4;
    private final int m_dateRangeFromValIndex = 5;
    private final int m_dateRangeToTitleIndex = 6;
    private final int m_dateRangeToValIndex = 7;
    private final int m_mergeStartIndex = 5;
    private final int m_mergeEndIndex = 7;
    private final int m_incrementalIndex = 1;
    
}
