package com.nov.rac.smdl.reports.exporters;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ExcelReport;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractGroupLevelReport extends ExcelReport
{
    
    public AbstractGroupLevelReport(Shell shell, IPropertyMap propMap, String[][] sData)
    {
        super(shell);
        m_propMap = propMap;
        m_sData = sData;
        m_registry = Registry.getRegistry(this);
    }
    
    @Override
    public void createContent(HSSFSheet sheet)
    {
        init();
        createReportHeader(sheet);
        createTableHeader(sheet, m_sHeader);
        createTableBody(sheet, m_sData, m_iColumnType);
    }
    
    protected void createTableHeader(HSSFSheet sheet, String[] headerTitle)
    {
        if (headerTitle != null && headerTitle.length > 0)
        {
            int tableColWidth[] = getColumnWidth(headerTitle);
            
            // Create Header cell style
            HSSFCellStyle csHeader = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex,
                    HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(),
                    HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_CENTER, false);
            
            // Write Header row
            createHeader(sheet, csHeader, headerTitle, tableColWidth);
        }
    }
    
    protected abstract void createHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle, int[] colWidth);
    
    protected void createTableBody(HSSFSheet sheet, String[][] sData, int[] colType)
    {
        if (sData != null)
        {
            // Create Body cell styles
            HSSFCellStyle csBody = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableBodyIndex,
                    HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.WHITE().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                    HSSFCellStyle.ALIGN_LEFT, false);
            HSSFCellStyle csBodyAlt = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableBodyIndex,
                    HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.GREY_25_PERCENT().getIndex(),
                    HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);
            
            // Write Table data
            createBody(sheet, csBody, csBodyAlt, sData, colType);
        }
    }
    
    protected void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data,
            int[] colTypes)
    {
        int nStartRow = 0;
        int nEndRow = data.length;
        int nStartColumn = 0;
        int nEndColumn = data[0].length;
        
        createBody(sheet, cellStyle, cellAltStyle, data, nStartRow, nEndRow, nStartColumn, nEndColumn, colTypes);
    }
    
    protected abstract void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data,
            int nStartRow, int nEndRow, int nStartColumn, int nEndColumn, int[] colTypes);
    
    private void createReportHeader(HSSFSheet sheet)
    {
        HSSFRow row = null;
        HSSFCell cell = null;
        int iHeaderRow = 0;
        CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
        
        // Create Header cell style
        HSSFCellStyle csHeaderTitle = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex,
                HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_LEFT, false);
        HSSFCellStyle csHeaderValueString = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri",
                (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, (short) 0, (short) 0, HSSFCellStyle.ALIGN_LEFT,
                false);
        HSSFCellStyle csHeaderValueDate = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri",
                (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, (short) 0, (short) 0, HSSFCellStyle.ALIGN_LEFT,
                false);
        csHeaderValueDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mmm/yyyy"));
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(0);
        cell.setCellValue(m_reportTitle);
        cell.setCellStyle(csHeaderTitle);
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), m_reportTitleStartIndex,
                m_reportTitleEndIndex));
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(0);
        cell.setCellValue(m_registry.getString(ReportsConstants.REPORT + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        
        cell = row.createCell(1);
        cell.setCellValue(m_propMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[0]);
        cell.setCellStyle(csHeaderValueString);
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), m_reportRespStartIndex,
                m_reportRespEndIndex));
        
        createDateHeader(row, csHeaderTitle, csHeaderValueDate);
        
        // Creating an empty row
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        
    }
    
    protected abstract void createDateHeader(HSSFRow row, HSSFCellStyle headerStyle, HSSFCellStyle dateStyle);
    
    protected abstract void init();
    
    private String[][] m_sData;
    protected IPropertyMap m_propMap;
    protected String[] m_sHeader;
    protected int[] m_iColumnType;
    protected Registry m_registry;
    protected String m_reportTitle;
    private final int m_tableHeaderIndex = 11;
    private final int m_tableBodyIndex = 10;
    private final int m_reportTitleStartIndex = 0;
    private final int m_reportTitleEndIndex = 4;
    private final int m_reportRespStartIndex = 1;
    private final int m_reportRespEndIndex = 3;
    
}
