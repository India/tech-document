package com.nov.rac.smdl.reports.exporters;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ExcelReport;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public class LAGroupLevel2MgrReport extends ExcelReport
{
    
    public LAGroupLevel2MgrReport(Shell shell, IPropertyMap propMap, String[][] sData)
    {
        super(shell);
        m_propMap = propMap;
        m_sData = sData;
        m_registry = Registry.getRegistry(this);
    }
    
    public void createContent(HSSFSheet sheet)
    {
        init();
        createReportHeader(sheet);
        createTableHeader(sheet, m_sHeader);
        createTableBody(sheet, m_sData, m_iColumnType);
    }
    
    protected void init()
    {
        String[] sCols = m_registry.getStringArray("LookAheadGroupLevel2MgrReport.COLUMNS");
        m_sHeader = new String[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; ++i)
        {
            m_sHeader[i] = m_registry.getString(sCols[i] + ".TITLE");
        }
        
        m_iColumnType = new int[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; ++i)
        {
            m_iColumnType[i] = Integer.parseInt(m_registry.getString(sCols[i] + ".TYPE"));
        }
        
        m_reportTitle = m_registry.getString("LookAheadGroupLevel2MgrReport.REPORTTITLE");
    }
    
    private void createReportHeader(HSSFSheet sheet)
    {
        HSSFRow row = null;
        HSSFCell cell = null;
        int iHeaderRow = 0;
        CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
        
        // Create Header cell style
        HSSFCellStyle csHeaderTitle = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex,
                HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_LEFT, false);
        HSSFCellStyle csHeaderValueString = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri",
                (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, (short) 0, (short) 0, HSSFCellStyle.ALIGN_LEFT,
                false);
        HSSFCellStyle csHeaderValueDate = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri",
                (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, (short) 0, (short) 0, HSSFCellStyle.ALIGN_LEFT,
                false);
        csHeaderValueDate.setDataFormat(createHelper.createDataFormat().getFormat("dd/mmm/yyyy"));
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(0);
        cell.setCellValue(m_reportTitle);
        cell.setCellStyle(csHeaderTitle);
        ExcelWriter.setThinBorderStyle(cell, csHeaderTitle);
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), m_reportTitleStartIndex,
                m_reportTitleEndIndex));
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(0);
        cell.setCellValue(m_registry.getString(ReportsConstants.REPORT + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        ExcelWriter.setThinBorderStyle(cell, csHeaderTitle);
        
        cell = row.createCell(1);
        cell.setCellValue(m_propMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[0]);
        cell.setCellStyle(csHeaderValueString);
        ExcelWriter.setThinBorderStyle(cell, csHeaderValueString);
        for (int i = cell.getColumnIndex() + 1; i <= m_reportRespEndIndex; ++i)
        {
            cell = row.createCell(i);
            ExcelWriter.setThinBorderStyle(cell, csHeaderValueString);
        }
        
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), m_reportRespStartIndex,
                m_reportRespEndIndex));
        createDateHeader(row, csHeaderTitle, csHeaderValueDate);
        
        // Creating an empty row
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        
    }
    
    protected void createTableHeader(HSSFSheet sheet, String[] headerTitle)
    {
        if (headerTitle != null && headerTitle.length > 0)
        {
            int tableColWidth[] = getColumnWidth(headerTitle);
            
            // Create Header cell style
            HSSFCellStyle csHeader = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex,
                    HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(),
                    HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_CENTER, false);
            
            // Write Header row
            createHeader(sheet, csHeader, headerTitle, tableColWidth);
        }
    }
    
    protected void createTableBody(HSSFSheet sheet, String[][] sData, int[] colType)
    {
        if (sData != null)
        {
            // Create Body cell styles
            HSSFCellStyle csBody = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableBodyIndex,
                    HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.WHITE().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                    HSSFCellStyle.ALIGN_LEFT, false);
            HSSFCellStyle csBodyAlt = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableBodyIndex,
                    HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.GREY_25_PERCENT().getIndex(),
                    HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);
            
            // Write Table data
            createBody(sheet, csBody, csBodyAlt, sData, colType);
        }
    }
    
    protected void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data,
            int[] colTypes)
    {
        int nStartRow = 0;
        int nEndRow = data.length;
        int nStartColumn = 0;
        int nEndColumn = data[0].length;
        
        createBody(sheet, cellStyle, cellAltStyle, data, nStartRow, nEndRow, nStartColumn, nEndColumn, colTypes);
    }
    
    protected void createHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle, int[] colWidth)
    {
        int iHeaderRow = sheet.getPhysicalNumberOfRows();
        HSSFRow row = sheet.createRow(iHeaderRow);
        int iHeaderLength = headerTitle.length;
        int cellmerge = 0;
        for (int iColIndex = 0; iColIndex < iHeaderLength; iColIndex++)
        {
            HSSFCell cell = row.createCell(cellmerge);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(headerTitle[iColIndex]);
            ExcelWriter.setThinBorderStyle(cell, cellStyle);
            
            switch (iColIndex)
            {
                case m_iColNumZero:
                    sheet.setColumnWidth(iColIndex, colWidth[iColIndex]);
                    cellmerge++;
                    break;
                case m_iColNumOne:
                case m_iColNumTwo:
                    sheet.setColumnWidth(iColIndex, colWidth[iColIndex] / 2);
                    cell = row.createCell(cellmerge + 1);
                    ExcelWriter.setThinBorderStyle(cell, cellStyle);
                    sheet.setColumnWidth(cell.getColumnIndex(), colWidth[iColIndex] / 2);
                    sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), cellmerge,
                            cellmerge + 1));
                    cellmerge += m_iColNumTwo;
                    break;
                case m_iColNumThree:
                    sheet.setColumnWidth(iColIndex, colWidth[iColIndex] / m_iColNumThree);
                    cell = row.createCell(cellmerge + 1);
                    
                    ExcelWriter.setThinBorderStyle(cell, cellStyle);
                    sheet.setColumnWidth(cell.getColumnIndex(), colWidth[iColIndex] / m_iColNumThree);
                    cell = row.createCell(cellmerge + 2);
                    ExcelWriter.setThinBorderStyle(cell, cellStyle);
                    sheet.setColumnWidth(cell.getColumnIndex(), colWidth[iColIndex] / m_iColNumThree);
                    sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), cellmerge, cellmerge
                            + m_iColNumTwo));
                    break;
            }
        }
        
    }
    
    protected void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data,
            int nStartRow, int nEndRow, int nStartColumn, int nEndColumn, int[] colTypes)
    {
        HSSFCellStyle[][] cellStyles = ExcelWriter.getFormatters(sheet, cellStyle, cellAltStyle);
        
        int iRowToInsertData = sheet.getPhysicalNumberOfRows();
        for (int rowNum = 0; rowNum < nEndRow; rowNum++)
        {
            HSSFRow row = sheet.createRow(iRowToInsertData + rowNum);
            int cellIndex = 0;
            for (int colNum = nStartColumn; colNum < nEndColumn; colNum++)
            {
                HSSFCell cell = null;
                HSSFCellStyle cs = cellStyles[rowNum % 2][colTypes[colNum] - 1];
                cell = row.createCell(cellIndex);
                try
                {
                    if (!data[nStartRow][colNum].isEmpty())
                    {
                        switch (colTypes[colNum])
                        {
                            case 1:
                                cell.setCellValue(data[nStartRow][colNum]);
                                cell.setCellStyle(cs);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                ExcelWriter.setThinBorderStyle(cell, cs);
                switch (colNum)
                {
                    case m_iColNumZero:
                        cellIndex++;
                        break;
                    case m_iColNumOne:
                    case m_iColNumTwo:
                        cell = row.createCell(cellIndex + m_iColNumOne);
                        // sheet.setColumnWidth(iColIndex, colWidth[iColIndex]);
                        ExcelWriter.setThinBorderStyle(cell, cellStyle);
                        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), cellIndex,
                                cellIndex + m_iColNumOne));
                        cellIndex += m_iColNumTwo;
                        break;
                    case m_iColNumThree:
                        cell = row.createCell(cellIndex + m_iColNumOne);
                        ExcelWriter.setThinBorderStyle(cell, cellStyle);
                        
                        cell = row.createCell(cellIndex + m_iColNumTwo);
                        // sheet.setColumnWidth(iColIndex, colWidth[iColIndex]);
                        ExcelWriter.setThinBorderStyle(cell, cellStyle);
                        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), cellIndex,
                                cellIndex + m_iColNumTwo));
                        break;
                }
                
            }
            nStartRow++;
        }
        
    }
    
    protected void createDateHeader(HSSFRow row, HSSFCellStyle headerStyle, HSSFCellStyle dateStyle)
    {
        HSSFCell cell = row.createCell(m_dateRangeTitleIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DATE_RANGE + ".TITLE"));
        cell.setCellStyle(headerStyle);
        ExcelWriter.setThinBorderStyle(cell, headerStyle);
        cell = row.createCell(m_dateRangeFromValIndex);
        if (m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0] != null)
        {
            cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0]);
            cell.setCellStyle(dateStyle);
            ExcelWriter.setThinBorderStyle(cell, dateStyle);
        }
        
        cell = row.createCell(m_dateRangeToTitleIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.TO + ".TITLE"));
        cell.setCellStyle(headerStyle);
        ExcelWriter.setThinBorderStyle(cell, headerStyle);
        cell = row.createCell(m_dateRangeToValIndex);
        if (m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1] != null)
        {
            cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1]);
            cell.setCellStyle(dateStyle);
            ExcelWriter.setThinBorderStyle(cell, dateStyle);
        }
        
    }
    
    private String[][] m_sData;
    protected IPropertyMap m_propMap;
    protected String[] m_sHeader;
    protected int[] m_iColumnType;
    protected Registry m_registry;
    protected String m_reportTitle;
    private final int m_tableHeaderIndex = 11;
    private final int m_tableBodyIndex = 10;
    private final int m_reportTitleStartIndex = 0;
    private final int m_reportTitleEndIndex = 4;
    private final int m_reportRespStartIndex = 1;
    private final int m_reportRespEndIndex = 3;
    private final int m_dateRangeTitleIndex = 4;
    private final int m_dateRangeFromValIndex = 5;
    private final int m_dateRangeToTitleIndex = 6;
    private final int m_dateRangeToValIndex = 7;
    private final int m_iColNumZero = 0;
    private final int m_iColNumOne = 1;
    private final int m_iColNumTwo = 2;
    private final int m_iColNumThree = 3;
    
}
