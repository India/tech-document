package com.nov.rac.smdl.reports.exporters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ExcelReport;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public class OverDueReportByDate extends ExcelReport 
{
    public OverDueReportByDate(Shell shell, IPropertyMap propMap,  String[][] sData) 
    {
        super(shell);
        m_registry = Registry.getRegistry(this);
        m_propMap = propMap;
        m_sData = sData;
    }
    
    @Override
    public void createContent(HSSFSheet sheet)
    {
        init();
        createReportHeader(sheet);
        createTableHeader(sheet, m_sHeader);
        createTableBody(sheet, m_sData, m_iColumnType); 
    }
    
    private void init()
    {
        String[] sCols = m_registry.getStringArray("OverDueReportByDate.COLUMNS");
        m_sHeader = new String[sCols.length];
        
        for(int i = 0, colCount = sCols.length; i<colCount; i++)
        {
            m_sHeader[i] = m_registry.getString(sCols[i] + ".TITLE");
        }
        
        m_iColumnType = new int[sCols.length];      
        for(int i = 0, colCount = sCols.length; i<colCount; i++)
        {
            m_iColumnType[i] = Integer.parseInt(m_registry.getString(sCols[i] + ".TYPE"));
        }           
    }
    
    protected void createReportHeader(HSSFSheet sheet)
    {
        HSSFRow row = null;
        HSSFCell cell = null;
        int iHeaderRow = 0;
        CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");  
        Calendar currentDate = Calendar.getInstance();   
        
                
        //Create Header cell style
        HSSFCellStyle csHeaderTitle = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_reportHeaderCols, HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);
        HSSFCellStyle csHeaderValueString = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_reportHeaderCols, HSSFFont.BOLDWEIGHT_BOLD, (short)0, (short)0, HSSFCellStyle.ALIGN_LEFT, false);
        HSSFCellStyle csHeaderValueDate = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_reportHeaderCols, HSSFFont.BOLDWEIGHT_BOLD, (short)0, (short)0, HSSFCellStyle.ALIGN_LEFT, false);
        csHeaderValueDate.setDataFormat(createHelper.createDataFormat().getFormat("dd-MMM-yyyy"));
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(m_reportTitleIndex);
        cell.setCellValue(m_registry.getString("OverDueReportByDate.REPORTTITLE"));
        cell.setCellStyle(csHeaderTitle);
        sheet.addMergedRegion(new CellRangeAddress( row.getRowNum(), row.getRowNum(), m_reportTitleStartIndex, m_reportTitleEndIndex ));
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(m_docRespLabelIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DOCRESPONSIBLE_TYPE + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        
        cell = row.createCell(m_docRespValueIndex);
        cell.setCellValue(m_propMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[m_docRespLabelIndex]);
        cell.setCellStyle(csHeaderValueString);
        sheet.addMergedRegion(new CellRangeAddress( row.getRowNum(), row.getRowNum(), m_docResponsibleTypeStartIndex, m_docResponsibleTypeEndIndex ));
        
        cell = row.createCell(m_dateCellIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DATE + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        
        cell = row.createCell(m_dateValueIndex);
        cell.setCellValue(currentDate.getTime());
        cell.setCellStyle(csHeaderValueDate);
        
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
    }
    
    
    private Registry m_registry;
    private IPropertyMap m_propMap;
    private String[] m_sHeader;
    private int[] m_iColumnType;
    private String[][] m_sData;
    private final int m_reportHeaderCols = 11;
    private final int m_docRespLabelIndex = 0;
    private final int m_docRespValueIndex = 1;
    private final int m_reportTitleIndex = 0;
    private final int m_dateCellIndex = 4;
    private final int m_dateValueIndex = 5;
    private final int m_reportTitleStartIndex = 0;
    private final int m_reportTitleEndIndex = 4;
    private final int m_docResponsibleTypeStartIndex = 1;
    private final int m_docResponsibleTypeEndIndex = 3;
}
