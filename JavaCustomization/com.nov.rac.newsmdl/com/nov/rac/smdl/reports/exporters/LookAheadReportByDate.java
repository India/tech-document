package com.nov.rac.smdl.reports.exporters;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ExcelReport;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public class LookAheadReportByDate extends ExcelReport 
{
	private Registry m_registry;
	private IPropertyMap m_propMap;
	private String[] m_sHeader;
	private int[] m_iColumnType;
	private String[][] m_sData;
	
	public LookAheadReportByDate(Shell shell, IPropertyMap propMap,  String[][] sData) 
	{
		super(shell);
		m_registry = Registry.getRegistry(this);
		m_propMap = propMap;
		m_sData = sData;
	}
	
	@Override
	public void createContent(HSSFSheet sheet)
	{
		init();
		createReportHeader(sheet);
		createTableHeader(sheet, m_sHeader);
		createTableBody(sheet, m_sData, m_iColumnType);	
	}
	
	private void init()
	{
		String[] sCols = m_registry.getStringArray("LookAheadReportByDate.COLUMNS");
		m_sHeader = new String[sCols.length];
		
		for(int i = 0, colCount = sCols.length; i<colCount; i++)
		{
			m_sHeader[i] = m_registry.getString(sCols[i] + ".TITLE");
		}
		
		m_iColumnType = new int[sCols.length];		
		for(int i = 0, colCount = sCols.length; i<colCount; i++)
		{
			m_iColumnType[i] = Integer.parseInt(m_registry.getString(sCols[i] + ".TYPE"));
		}			
	}
	
	protected void createReportHeader(HSSFSheet sheet)
	{
		HSSFRow row = null;
		HSSFCell cell = null;
		int iHeaderRow = 0;
		CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
				
		//Create Header cell style
		HSSFCellStyle csHeaderTitle = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);
		HSSFCellStyle csHeaderValueString = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, (short)0, (short)0, HSSFCellStyle.ALIGN_LEFT, false);
		HSSFCellStyle csHeaderValueDate = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_tableHeaderIndex, HSSFFont.BOLDWEIGHT_BOLD, (short)0, (short)0, HSSFCellStyle.ALIGN_LEFT, false);
		csHeaderValueDate.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy"));
		
		iHeaderRow = sheet.getPhysicalNumberOfRows();
		row = sheet.createRow(iHeaderRow);
		cell = row.createCell(0);
		cell.setCellValue(m_registry.getString("LookAheadReportByDate.REPORTTITLE"));
		cell.setCellStyle(csHeaderTitle);
		sheet.addMergedRegion(new CellRangeAddress( row.getRowNum(), row.getRowNum(), 0, m_reportTitleEndIndex ));
		
		iHeaderRow = sheet.getPhysicalNumberOfRows();
		row = sheet.createRow(iHeaderRow);
		cell = row.createCell(0);
		cell.setCellValue(m_registry.getString(ReportsConstants.DOCRESPONSIBLE_TYPE + ".TITLE"));
		cell.setCellStyle(csHeaderTitle);
		
		cell = row.createCell(1);
		cell.setCellValue(m_propMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[0]);
		cell.setCellStyle(csHeaderValueString);
		sheet.addMergedRegion(new CellRangeAddress( row.getRowNum(), row.getRowNum(), 1, m_reportDocRespIndex ));
		
		cell = row.createCell(m_dateRangeTitleIndex);
		cell.setCellValue(m_registry.getString(ReportsConstants.DATE_RANGE + ".TITLE"));
		cell.setCellStyle(csHeaderTitle);
		
		cell = row.createCell(m_dateRangeFromValIndex);
		if(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0]!=null)
		{
			cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0]);
			cell.setCellStyle(csHeaderValueDate);
		}
		
		cell = row.createCell(m_dateRangeToTitleIndex);
		cell.setCellValue(m_registry.getString(ReportsConstants.TO + ".TITLE"));
		cell.setCellStyle(csHeaderTitle);
		
		cell = row.createCell(m_dateRangeToValIndex);
		if(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1]!=null)
		{
			cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1]);	
			cell.setCellStyle(csHeaderValueDate);
		}
		
		iHeaderRow = sheet.getPhysicalNumberOfRows();
		row = sheet.createRow(iHeaderRow);
	}	
	
	 private final int m_tableHeaderIndex = 11;
	private final int m_dateRangeTitleIndex = 4;
	private final int m_reportTitleEndIndex = 4;
	private final int m_reportDocRespIndex = 3;
    private final int m_dateRangeFromValIndex = 5;
    private final int m_dateRangeToTitleIndex = 6;
    private final int m_dateRangeToValIndex = 7;
}
