package com.nov.rac.smdl.reports.exporters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;

public class ODGroupLevel2MgrReport extends AbstractGroupLevelReport
{
    
    public ODGroupLevel2MgrReport(Shell shell, IPropertyMap propMap, String[][] sData)
    {
        super(shell, propMap, sData);
    }
    
    @Override
    protected void createDateHeader(HSSFRow row, HSSFCellStyle headerStyle, HSSFCellStyle dateStyle)
    {
        HSSFCell cell = row.createCell(m_dateRangeTitleIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DATE + ".TITLE"));
        cell.setCellStyle(headerStyle);
        
        cell = row.createCell(m_dateRangeValueIndex);
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar currentDate = Calendar.getInstance();
        String curDate = df.format(currentDate.getTime());
        cell.setCellValue(curDate);
        cell.setCellStyle(dateStyle);
        
    }
    
    @Override
    protected void init()
    {
        
        String[] sCols = m_registry.getStringArray("OverdueGroupLevel2MgrReport.COLUMNS");
        m_sHeader = new String[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; ++i)
        {
            m_sHeader[i] = m_registry.getString(sCols[i] + ".TITLE");
        }
        
        m_iColumnType = new int[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; ++i)
        {
            m_iColumnType[i] = Integer.parseInt(m_registry.getString(sCols[i] + ".TYPE"));
        }
        
        m_reportTitle = m_registry.getString("OverDueGroupLevel2MgrReport.REPORTTITLE");
        
    }
    
    protected void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data,
            int nStartRow, int nEndRow, int nStartColumn, int nEndColumn, int[] colTypes)
    {
        HSSFCellStyle[][] cellStyles = ExcelWriter.getFormatters(sheet, cellStyle, cellAltStyle);
        DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        
        int iRowToInsertData = sheet.getPhysicalNumberOfRows();
        for (int rowNum = 0; rowNum < nEndRow; rowNum++)
        {
            HSSFRow row = sheet.createRow(iRowToInsertData + rowNum);
            
            for (int colNum = nStartColumn; colNum < nEndColumn; colNum++)
            {
                HSSFCell cell = row.createCell(colNum);
                HSSFCellStyle cs = cellStyles[rowNum % 2][colTypes[colNum] - 1];
                
                try
                {
                    if (!data[nStartRow][colNum].isEmpty())
                    {
                        switch (colTypes[colNum])
                        {
                            case 1:
                                cell.setCellValue(data[nStartRow][colNum]);
                                cell.setCellStyle(cs);
                                break;
                            
                            case 2:
                                Date dt = (Date) dateFormatter.parse(data[nStartRow][colNum]);
                                cell.setCellValue(dt);
                                cell.setCellStyle(cs);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                
                ExcelWriter.setThinBorderStyle(cell, cs);
            }
            nStartRow++;
        }
    }
    
    protected void createHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle, int[] colWidth)
    {
        int iHeaderRow = sheet.getPhysicalNumberOfRows();
        HSSFRow row = sheet.createRow(iHeaderRow);
        int iHeaderLength = headerTitle.length;
        for (int iColIndex = 0; iColIndex < iHeaderLength; iColIndex++)
        {
            HSSFCell cell = row.createCell(iColIndex);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(headerTitle[iColIndex]);
            sheet.setColumnWidth(iColIndex, colWidth[iColIndex]);
            ExcelWriter.setThinBorderStyle(cell, cellStyle);
        }
    }
    
    private final int m_dateRangeTitleIndex = 4;
    private final int m_dateRangeValueIndex = 5;
    
}
