package com.nov.rac.smdl.reports.exporters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ExcelReport;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractReportByOrder extends ExcelReport
{
    
    public AbstractReportByOrder(Shell shell, IPropertyMap propMap, String[][] sData)
    {
        super(shell);
        m_propMap = propMap;
        m_sData = sData;
    }
    
    @Override
    public void createContent(HSSFSheet sheet)
    {
        int iRowLength = m_sData.length;
        String smdlRefStr = new String("");
        Vector<String[]> vecDocData = new Vector<String[]>();
        
        init();
        
        // Create Header cell style
        HSSFCellStyle csHeader = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_headerStyleIndex,
                HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_CENTER, false);
        
        // Create Body cell styles
        HSSFCellStyle csBody = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_bodyStyleIndex,
                HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.WHITE().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_LEFT, false);
        HSSFCellStyle csBodyAlt = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_bodyStyleIndex,
                HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.GREY_25_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_LEFT, false);
        
        createReportHeader(sheet);
        // This loop is responsible for writing the actual data into the XLS
        for (int rowNum = 0; rowNum < iRowLength; rowNum++)
        {
            boolean isDocHeaderCreated = false;
            boolean isOrdHeaderCreated = false;
            String strSMDLId = m_sData[rowNum][m_smdlIndex];
            
            if (!smdlRefStr.equals(strSMDLId))
            {
                printDocCodes(vecDocData, isDocHeaderCreated, sheet, csHeader, csBody, csBodyAlt);
                smdlRefStr = strSMDLId;
                if (!isOrdHeaderCreated)
                {
                    createTableHeader(sheet, m_sOrderHeader, csHeader);
                    isDocHeaderCreated = true;
                }
                createTableBody(sheet, m_sData, m_iColumnType, csBody, csBodyAlt, rowNum, m_incrementalIndex,
                        m_orderStartIndex, m_orderEndIndex);
                populateVecDocData(rowNum, m_sData, vecDocData);
            }
            else
            {
                populateVecDocData(rowNum, m_sData, vecDocData);
            }
            
            if (rowNum == iRowLength - m_incrementalIndex)
            {
                printDocCodes(vecDocData, false, sheet, csHeader, csBody, csBodyAlt);
            }
        }
    }
    
    private void printDocCodes(Vector<String[]> vecDocData, boolean isDocHeaderCreated, HSSFSheet sheet,
            HSSFCellStyle csHeader, HSSFCellStyle csBody, HSSFCellStyle csBodyAlt)
    {
        int iDocRowNum = 0;
        if (vecDocData.size() == 0) {return;}        
        String[][] docData = new String[vecDocData.size()][m_docCodeCols];
        Iterator<String[]> strIter = vecDocData.iterator();
        while (strIter.hasNext())
        {
            String[] record = strIter.next();
            for (int i = 0; i < record.length; i++)
            {
                docData[iDocRowNum][i] = record[i];
            }
            iDocRowNum++;
        }
        vecDocData.clear();
        if (!isDocHeaderCreated)
        {
            createDocCodeHeader(sheet, csHeader, m_sDocHeader, getColumnWidth(m_sDocHeader));
            isDocHeaderCreated = true;
        }
        createDocCodeBody(sheet, csBody, csBodyAlt, docData, m_iDocColumnType);    
    }
    
    protected int[] getColumnWidth(String[] headerTitle)
    {
        int iHeaderCount = headerTitle.length;
        int[] tableColWidth = new int[iHeaderCount];
        for (int iHeaderIndex = 0; iHeaderIndex < iHeaderCount; iHeaderIndex++)
        {
            tableColWidth[iHeaderIndex] = headerTitle[iHeaderIndex].length() * m_colWidth;
        }
        return tableColWidth;
    }
    
    private void populateVecDocData(int rowNum, String[][] m_sData, Vector<String[]> vecDocData)
    {
        int iFlag = 0;
        String[] docRowData = new String[m_docCodeCols];
        for (int colNum = m_docCodeStartIndex; colNum < m_docCodeEndIndex; colNum++)
        {
            docRowData[iFlag] = m_sData[rowNum][colNum];
            iFlag++;
        }
        vecDocData.add(docRowData);
        
    }
    
    public void createDocCodeHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle, int[] colWidth)
    {
        int iHeaderRow = sheet.getPhysicalNumberOfRows();
        HSSFRow row = sheet.createRow(iHeaderRow);
        row = sheet.createRow(iHeaderRow + m_incrementalIndex);
        
        int iHeaderLength = headerTitle.length;
        iHeaderLength = iHeaderLength + m_mergeCellIndex;
        int iTitleIndex = 0;
        for (int iColIndex = 0; iColIndex < iHeaderLength; iColIndex++)
        {
            
            HSSFCell cell = row.createCell(iColIndex);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(headerTitle[iTitleIndex]);
            if (iTitleIndex == m_incrementalIndex || iTitleIndex == m_mergeCellIndex)
            {
                sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), iColIndex, iColIndex
                        + m_incrementalIndex));
                iColIndex = iColIndex + m_incrementalIndex;
            }
            sheet.setColumnWidth(iColIndex, colWidth[iTitleIndex]);
            
            ExcelWriter.setThinBorderStyle(cell, cellStyle);
            iTitleIndex++;
        }
    }
    
    public void createDocCodeBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle,
            String[][] data, int[] colTypes)
    {
        HSSFCellStyle[][] cellStyles = ExcelWriter.getFormatters(sheet, cellStyle, cellAltStyle);
        DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        
        int iRowToInsertData = sheet.getPhysicalNumberOfRows();
        
        int iRowLength = data.length;
        for (int rowNum = 0; rowNum < iRowLength; rowNum++)
        {
            HSSFRow row = sheet.createRow(iRowToInsertData + rowNum);
            
            int iColLength = data[rowNum].length;
            int iColIndex = 0;
            for (int colNum = 0; colNum < iColLength + m_mergeCellIndex; colNum++)
            {
                HSSFCell cell = row.createCell(colNum);
                HSSFCellStyle cs = cellStyles[rowNum % m_mergeCellIndex][colTypes[iColIndex] - m_incrementalIndex];
                ExcelWriter.setThinBorderStyle(cell, cs);
                
                try
                {
                    if (!data[rowNum][iColIndex].isEmpty())
                    {
                        switch (colTypes[iColIndex])
                        {
                            case m_reportColumnString:
                                cell.setCellValue(data[rowNum][iColIndex]);
                                break;
                            
                            case m_reportColumnDate:
                                Date dt = null;
                                dt = (Date) dateFormatter.parse(data[rowNum][iColIndex]);
                                cell.setCellValue(dt);
                                break;
                        }
                    }
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }
                if (iColIndex == m_docCodeMergeDocIndex || iColIndex == m_docCodeMergeDocNameIndex)
                {
                    cell = row.createCell(colNum + m_incrementalIndex);
                    ExcelWriter.setThinBorderStyle(cell, cs);
                    sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), colNum, colNum
                            + m_incrementalIndex));
                    colNum = colNum + m_incrementalIndex;
                }
                iColIndex++;
            }
        }
        HSSFRow row = sheet.createRow(iRowToInsertData + iRowLength);
        row = sheet.createRow(iRowToInsertData + 1 + iRowLength);
    }
    
    abstract protected void init();
    abstract protected void createReportHeader(HSSFSheet sheet);   
    
    protected Registry m_registry;
    private IPropertyMap m_propMap;
    protected String[] m_sHeader;
    protected String[] m_sOrderHeader;
    protected String[] m_sDocHeader;
    protected int[] m_iColumnType;
    protected int[] m_iOrderColumnType;
    protected int[] m_iDocColumnType;
    private String[][] m_sData;
    private final int m_smdlIndex = 4;
    private final int m_incrementalIndex = 1;
    private final int m_orderStartIndex = 0;
    private final int m_orderEndIndex = 6;
    private final int m_docCodeStartIndex = 6;
    private final int m_docCodeEndIndex = 11;
    private final int m_docCodeCols = 5;
    private final int m_mergeCellIndex = 2;
    private final int m_colWidth = 500;
    private final int m_reportColumnString = 1;
    private final int m_reportColumnDate = 2;
    private final int m_docCodeMergeDocIndex = 1;
    private final int m_docCodeMergeDocNameIndex = 2;
    private final int m_headerStyleIndex = 11;
    private final int m_bodyStyleIndex = 10;    
}
