package com.nov.rac.smdl.reports.exporters;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.ExcelWriter;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public class LookAheadReportByOrder extends AbstractReportByOrder
{    
    public LookAheadReportByOrder(Shell shell, IPropertyMap propMap, String[][] sData)
    {
        super(shell, propMap, sData);
        //TODO : This can also be placed in the AbstractReportByOrder
        m_registry = Registry.getRegistry(this);
        m_propMap = propMap;
    }
    
    @Override
    protected void init()
    {
        String[] sCols = m_registry.getStringArray("LookAheadReportByOrder.COLUMNS");
        m_sHeader = new String[sCols.length];
        
        for (int i = 0, colCount = sCols.length; i < colCount; i++)
        {
            m_sHeader[i] = m_registry.getString(sCols[i] + ".TITLE");
        }
        
        m_iColumnType = new int[sCols.length];
        for (int i = 0, colCount = sCols.length; i < colCount; i++)
        {
            m_iColumnType[i] = Integer.parseInt(m_registry.getString(sCols[i] + ".TYPE"));
        }
        
        // Get the Columns for Order and Document from the registry
        String[] sOrderCols = m_registry.getStringArray("LookAheadReportByOrder.ORDER.COLUMNS");
        m_sOrderHeader = new String[sOrderCols.length];
        
        for (int i = 0; i < sOrderCols.length; i++)
        {
            // TODO : Change is required at this place this is responsible for writing the header for the column.
            m_sOrderHeader[i] = m_registry.getString(sOrderCols[i] + ".TITLE");
        }
        
        m_iOrderColumnType = new int[sOrderCols.length];
        for (int i = 0; i < sOrderCols.length; i++)
        {
            m_iOrderColumnType[i] = Integer.parseInt(m_registry.getString(sOrderCols[i] + ".TYPE"));
        }
        
        // Get the Columns for Order and Document from the registry
        String[] sDocCols = m_registry.getStringArray("LookAheadReportByOrder.DOC.COLUMNS");
        m_sDocHeader = new String[sDocCols.length];
        
        for (int i = 0; i < sDocCols.length; i++)
        {
            m_sDocHeader[i] = m_registry.getString(sDocCols[i] + ".TITLE");
        }
        
        m_iDocColumnType = new int[sDocCols.length];
        for (int i = 0; i < sDocCols.length; i++)
        {
            m_iDocColumnType[i] = Integer.parseInt(m_registry.getString(sDocCols[i] + ".TYPE"));
        }
    }
    
    
    protected void createReportHeader(HSSFSheet sheet)
    {
        HSSFRow row = null;
        HSSFCell cell = null;
        int iHeaderRow = 0;
        CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
        DataFormat format = sheet.getWorkbook().createDataFormat();
        
        // Create Header cell style
        HSSFCellStyle csHeaderTitle = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri", (short) m_reportHeaderCols,
                HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_LEFT, false);
        HSSFCellStyle csHeaderValueString = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri",
                (short) m_reportHeaderCols, HSSFFont.BOLDWEIGHT_BOLD, (short) 0, (short) 0, HSSFCellStyle.ALIGN_LEFT,
                false);
        HSSFCellStyle csHeaderValueDate = ExcelWriter.cellStyle(sheet.getWorkbook(), "Calibri",
                (short) m_reportHeaderCols, HSSFFont.BOLDWEIGHT_BOLD, (short) 0, (short) 0, HSSFCellStyle.ALIGN_LEFT,
                false);
        csHeaderValueDate.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy"));
        
        /* Report Header Title */
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(m_docRespLabelIndex);
        cell.setCellValue(m_registry.getString("LookAheadReportByOrder.REPORTTITLE"));
        cell.setCellStyle(csHeaderTitle);
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), m_reportTitleStartIndex,
                m_reportTitleEndIndex));
        
        /* Report For: Label */
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
        cell = row.createCell(m_docRespLabelIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DOCRESPONSIBLE_TYPE + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        
        /* Report For: Value */
        cell = row.createCell(m_docRespValueIndex);
        cell.setCellValue(m_propMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[m_docRespLabelIndex]);
        cell.setCellStyle(csHeaderValueString);
        sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), m_docRespTypeStartIndex,
                m_docRespTypeEndIndex));
        
        /* Date Range: Label */
        cell = row.createCell(m_fromDateRangeCellIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.DATE_RANGE + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        
        /* From Date Value */
        cell = row.createCell(m_fromDateRangeValueIndex);
        if (m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0] != null)
        {
            cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[0]);
            cell.setCellStyle(csHeaderValueDate);
        }
        
        /* To: Label */
        cell = row.createCell(m_toDateRangeCellIndex);
        cell.setCellValue(m_registry.getString(ReportsConstants.TO + ".TITLE"));
        cell.setCellStyle(csHeaderTitle);
        
        /* To Date Value */
        cell = row.createCell(m_toDateRangeValueIndex);
        if (m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1] != null)
        {
            cell.setCellValue(m_propMap.getDateArray(ReportsConstants.DATE_RANGE)[1]);
            cell.setCellStyle(csHeaderValueDate);
        }
        
        /* Blank Row */
        iHeaderRow = sheet.getPhysicalNumberOfRows();
        row = sheet.createRow(iHeaderRow);
    }
    
    private IPropertyMap m_propMap;
    private final int m_reportHeaderCols = 11;
    private final int m_reportTitleEndIndex = 6;
    private final int m_reportTitleStartIndex = 0;
    private final int m_docRespTypeStartIndex = 1;
    private final int m_docRespTypeEndIndex = 2;
    private final int m_docRespLabelIndex = 0;
    private final int m_docRespValueIndex = 1;
    private final int m_fromDateRangeCellIndex = 3;
    private final int m_fromDateRangeValueIndex = 4;
    private final int m_toDateRangeCellIndex = 5;
    private final int m_toDateRangeValueIndex = 6;
}
