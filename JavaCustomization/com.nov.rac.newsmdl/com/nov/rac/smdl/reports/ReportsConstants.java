package com.nov.rac.smdl.reports;

public class ReportsConstants 
{
		public static String REPORT_TYPE = "reportType";
		public static String REPORT_LEVEL = "reportLevel";
		public static String DOCRESPONSIBLE_TYPE = "docResponsible";
		public static String MANAGER_TYPE ="manager";
		public static String HIGHLEVEL_TYPE ="managerlevel2";
		public static String DATE_RANGE = "dateRange";
		public static String DATE = "date";
		public static String FILE_TYPE = "fileType";
		public static String BY_PLANNED_DATE = "byDate";
		public static String BY_ORDER = "byOrder";
		public static String TO = "to";
		public static String REPORT = "report";
		
		/*public static String REPORT_TYPE_PANEL = "reportType";
		public static String DATE_RANGE_PANEL = "dateRange";
		public static String FILE_TYPE_PANEL = "fileType";*/
}
