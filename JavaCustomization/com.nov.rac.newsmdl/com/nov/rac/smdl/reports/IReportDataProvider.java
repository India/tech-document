package com.nov.rac.smdl.reports;

import com.nov.rac.propertymap.IPropertyMap;

public interface IReportDataProvider
{
	public void populateReportData (IPropertyMap dataMap);
	//public ArrayList <QuickSearchService.QuickBindVariable>getBindVariables();
}
