package com.nov.rac.smdl.reports;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.interfaces.IPropertyChangeNotifier;
import com.nov.rac.smdl.reports.operations.CreateReportOperationDelegate;
import com.nov.rac.smdl.reports.panels.DateRangePanel;
import com.nov.rac.smdl.reports.panels.FileTypePanel;
import com.nov.rac.smdl.reports.panels.ReportsTypePanel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class ReportsDialog extends AbstractSWTDialog implements IPropertyChangeNotifier
{
    public ReportsDialog(Shell shell, boolean isLookAhead)
    {
        super(shell);
        m_isLookAheadReport = isLookAhead;
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
        m_registry = Registry.getRegistry("com.nov.rac.smdl.reports.reports");
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        if (m_isLookAheadReport)
        {
            m_title = m_registry.getString("createLookAheadReport.title");
        }
        else
        {
            m_title = m_registry.getString("createOverDueReport.title");
        }
        
        setTitle(m_title);
        
        Label separator1 = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
        separator1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        
        ScrolledComposite scrolledComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
        scrolledComposite.setLayout(new GridLayout(1, true));
        scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        Composite reportsPane = new Composite(scrolledComposite, SWT.BORDER);
        reportsPane.setLayout(new GridLayout(1, true));
        reportsPane.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
        
        createSubPanels(reportsPane);
        
        scrolledComposite.setContent(reportsPane);
        scrolledComposite.setExpandHorizontal(true);
        scrolledComposite.setExpandVertical(true);
        scrolledComposite.setMinSize(reportsPane.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        scrolledComposite.pack();
        
        m_operationDelegate.setParentShell(this.getShell());
        addreportDataProviders();
        registerListeners();
        
        return parent;
    }
    
    protected void createSubPanels(Composite reportsPane)
    {
        m_reportsTypePanel = new ReportsTypePanel(reportsPane, SWT.NONE);
       
        if (m_isLookAheadReport)
        {
            m_dateRangePanel = new DateRangePanel(reportsPane, SWT.NONE);
        }
        m_fileTypePanel = new FileTypePanel(reportsPane, SWT.NONE);
        ((ReportsTypePanel)m_reportsTypePanel).addlisteners((FileTypePanel)m_fileTypePanel);
    }
    
    private void addreportDataProviders()
    {
        m_operationDelegate.addOperationInputProvider(m_reportsTypePanel);
        if (m_isLookAheadReport)
        {
            m_operationDelegate.addOperationInputProvider(m_dateRangePanel);
        }
        // m_operationDelegate.addOperationInputProvider(fileTypePanel);
    }
    
    @Override
    protected Point getInitialLocation(Point point)
    {
        
        // Move the dialog to the center of the top level shell.
        Rectangle shellBounds = getShell().getParent().getBounds();
        Point dialogSize = getInitialSize();
        
        int x = shellBounds.x + (shellBounds.width - dialogSize.x) / 2;
        int y = shellBounds.y + (shellBounds.height - dialogSize.y) / 2;
        Point location = new Point(x, y);
        
        return location;
    }
    
    @Override
    protected Control createButtonBar(Composite parent)
    {
        Composite composite = new Composite(parent, 0);
        GridLayout layout = new GridLayout();
        layout.numColumns = 0;
        layout.makeColumnsEqualWidth = true;
        layout.marginWidth = convertHorizontalDLUsToPixels(m_marginWidthPixels);
        layout.marginHeight = convertVerticalDLUsToPixels(m_marginHeightPixels);
        layout.horizontalSpacing = convertHorizontalDLUsToPixels(m_horizontalSpacingPixels);
        layout.verticalSpacing = convertVerticalDLUsToPixels(m_verticalSpacingPixels);
        composite.setLayout(layout);
        
        GridData data = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
        composite.setLayoutData(data);
        
        composite.setFont(parent.getFont());
        createButtonsForButtonBar(composite);
        return composite;
    }
    
    @Override
    protected void createButtonsForButtonBar(final Composite parent)
    {
        String operationLabel = "createReport.Label";
        m_saveButton = createButton(parent, IDialogConstants.OK_ID, m_registry.getString(operationLabel), true);
        m_cancelButton = createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
        
    }
    
    public void setTitle(String title)
    {
        this.getShell().setText(title);
    }
    
    @Override
    protected void okPressed()
    {
        //TCDECREL-6132 : Check if the selected value is correct.
        //Invoke execute operation only on true.
        if(isCorrectValueSelected())
        {
            m_saveButton.setFocus();
            firePropertyChange();
            boolean success = m_operationDelegate.executeOperation();
            if (success)
            {
                super.okPressed();
            }
        }
        else
        {
            //Message box for invalid state
            MessageBox.post(AIFUtility.getActiveDesktop().getShell(),
                            m_registry.getString("NOT_VALID_INPUT_FOR_SEARCH.Msg"),
                            "Information", MessageBox.INFORMATION);
        }
    }
    
    /*private boolean isValidState() {
        boolean state = m_reportsTypePanel.isValidState() &&
                        m_fileTypePanel.isValidState();
    if(state && null != m_dateRangePanel)
    {
        state = m_dateRangePanel.isValidState();
    }
    return state;
    }*/
    
    //TCDECREL-6132: Method to check if value in combo box selected is correct.
    private boolean isCorrectValueSelected(){
       boolean correctValueSelected = false;
       if(((ReportsTypePanel)m_reportsTypePanel).getM_docResponsiblCombo().getText().length() > 0 )
        {
            String docResp = ((ReportsTypePanel)m_reportsTypePanel).getM_docResponsiblCombo().getText();
            if(((ReportsTypePanel)m_reportsTypePanel).getM_docResponsibleList().contains(docResp))
            {
                correctValueSelected = true;                      
            }
            
        }
        else if(((ReportsTypePanel)m_reportsTypePanel).getM_managerReportCombo().getText().length() > 0 )
        {
            String managerL1 = ((ReportsTypePanel)m_reportsTypePanel).getM_managerReportCombo().getText();
            if(((ReportsTypePanel)m_reportsTypePanel).getM_managersL1List().contains(managerL1))
            {
                correctValueSelected = true;                      
            }
            
        }
        else if(((ReportsTypePanel)m_reportsTypePanel).getM_hiLevelReportCombo().getText().length() > 0 )
        {
            String managerL2 = ((ReportsTypePanel)m_reportsTypePanel).getM_hiLevelReportCombo().getText();
            if(((ReportsTypePanel)m_reportsTypePanel).getM_highLevelList().contains(managerL2))
            {
                correctValueSelected = true;                      
            }
            
        }
        return correctValueSelected;    
    }
    
    public void setReportType(String type)
    {
        m_reportType = type;
    }
    
    @Override
    protected void cancelPressed()
    {
        
        boolean yes = MessageDialog.openQuestion(AIFUtility.getActiveDesktop().getShell(),
                m_registry.getString("cancel.TITLE"), m_registry.getString("cancel.MSG"));
        if (yes)
        {
            super.cancelPressed();
        }
    }
    
    protected void firePropertyChange(Object source, String propertyName, Object oldValue, Object newValue)
    {
        PropertyChangeEvent changeEvent = new PropertyChangeEvent(source, propertyName, oldValue, newValue);
        for (PropertyChangeListener listener : m_listeners)
        {
            listener.propertyChange(changeEvent);
        }
    }
    
    public void firePropertyChange()
    {
        firePropertyChange(this, ReportsConstants.REPORT_TYPE, "", m_reportType);
        m_reportsTypePanel.firePropertyChange();
        m_fileTypePanel.firePropertyChange();
        
    }
    
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        m_listeners.add(listener);
    }
    
    public void registerListeners()
    {
        PropertyChangeListener listener = (PropertyChangeListener) m_operationDelegate;
        addPropertyChangeListener(listener);
        m_reportsTypePanel.addPropertyChangeListener(listener);
        m_fileTypePanel.addPropertyChangeListener(listener);
    }
    
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        m_listeners.remove(listener);
    }
    
    private final int m_marginWidthPixels = 7;
    
    private final int m_marginHeightPixels = 7;
    
    private final int m_horizontalSpacingPixels = 4;
    
    private final int m_verticalSpacingPixels = 4;
    
    protected Registry m_registry = null;
    
    protected String m_title = null;
    
    protected boolean m_isLookAheadReport = false;
    
    private Button m_saveButton;
    
    private Button m_cancelButton;
    
    protected AbstractReportDataProvider m_reportsTypePanel;
    
    protected AbstractReportDataProvider m_dateRangePanel;
    
    protected AbstractReportDataProvider m_fileTypePanel;
    
    private String m_reportType = null;
    
    protected CreateReportOperationDelegate m_operationDelegate = new CreateReportOperationDelegate();;
    
    private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
}
