package com.nov.rac.smdl.reports.operations;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.quicksearch.services.utils.Utils;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ExcelReport;
import com.nov.rac.smdl.reports.IReportDataProvider;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.teamcenter.rac.util.Registry;

public class CreateReportOperationDelegate implements PropertyChangeListener
{	
	public CreateReportOperationDelegate()
	{
		m_dataProviders = new ArrayList<IReportDataProvider>();
		m_registry  = Registry.getRegistry("com.nov.rac.smdl.reports.reports");
	}
	
	public void setParentShell(Shell shell) 
	{
		m_shell = shell;
	}
	 

	public boolean executeOperation() 
	{
		boolean success= true;
		m_dataMap = getInputData();
		INOVSearchProvider searchService = getSearchProvider();
		INOVSearchResult resultSet = null;
		if(searchService!= null)
		{
			try 
			{
				resultSet= NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL);
				if(resultSet!=null && resultSet.getResultSet().getRows()>0)
				{
					ExcelReport exporter = getExporter(resultSet);
					if(exporter!= null)
					{
					    success = exporter.exportDataToExcel();
					}
					else
					{
						success= false;
						MessageDialog.openInformation(m_shell,
								m_registry.getString( "MESSAGE.TITLE" ), m_registry.getString( "NO_RESULTS_EXPORTER.Msg" ) );
					}
				}
				else
				{
					success= false;
					MessageDialog.openInformation(m_shell,
							m_registry.getString( "MESSAGE.TITLE" ), m_registry.getString( "NO_OBJECTS_FOUND.MSG" ) );
				}
				
			} catch (Exception e) 
			{
				success = false;
				e.printStackTrace();
			}
			
		}
		else
		{
			success= false;
			MessageDialog.openInformation(m_shell,
					m_registry.getString( "MESSAGE.TITLE" ), m_registry.getString( "NO_SEARCH_PROVIDER.Msg" ) );
		}
		return success;
	}

	
	private ExcelReport getExporter(INOVSearchResult resultSet) 
	{
		ExcelReport exporter = null;
		
		StringBuffer exporterType = new StringBuffer().append(m_reportType).append(".").append(m_reportLevel).append(".").append(m_fileType).append(".EXPORTER");
		
		Object [] oprParams = new Object[m_exporterTypeParamCount];
		
		try 
		{
			oprParams[0] = m_shell;
			oprParams[1] = m_dataMap;
			oprParams[2] = Utils.getSearchResultMatrix(resultSet);
			
			exporter = (ExcelReport) m_registry.newInstanceForEx(exporterType.toString(),oprParams);
		}  catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return exporter ;
	}

	private INOVSearchProvider getSearchProvider() 
	{
		INOVSearchProvider provider = null;
		
		
		StringBuffer providerType = new StringBuffer().append(m_reportType).append(".").append(m_reportLevel).append(".").append(m_fileType).append(".SEARCH_PROVIDER");
		
		Object [] oprParams = new Object[1];
		oprParams[0] = m_dataMap;
		
		try 
		{
			provider = (INOVSearchProvider) m_registry.newInstanceForEx(providerType.toString(),oprParams);
		}  catch (Exception e)
		{
			e.printStackTrace();
		}
		return provider ;
	}

    
	private IPropertyMap getInputData()
	{
		IPropertyMap dataMap = new SimplePropertyMap();
		for(IReportDataProvider provider : m_dataProviders)
		{
			provider.populateReportData(dataMap);
		}
		return dataMap;
	}

	@Override
	public void propertyChange(java.beans.PropertyChangeEvent event) 
	{
		String propertyName = event.getPropertyName();
		if (propertyName.equals(ReportsConstants.REPORT_TYPE))
		{
			m_reportType = (String) event.getNewValue();
		} 
		else if (propertyName.equals(ReportsConstants.REPORT_LEVEL))
		{
			m_reportLevel = (String) event.getNewValue();
		} 
		else if (propertyName.equals(ReportsConstants.FILE_TYPE))
		{
			m_fileType = (String) event.getNewValue();
		}
	}

	

	public void addOperationInputProvider(IReportDataProvider provider)
	{
		m_dataProviders.add(provider);
	}


	private final int m_exporterTypeParamCount = 3;
	private String m_reportType;
	private String m_reportLevel;
	private String m_fileType;
	private IPropertyMap m_dataMap;
	private Shell m_shell = null;
	private ArrayList<IReportDataProvider> m_dataProviders = null;
	private Registry m_registry = null;
	
     
}
