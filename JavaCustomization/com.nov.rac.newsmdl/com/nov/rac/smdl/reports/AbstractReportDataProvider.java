package com.nov.rac.smdl.reports;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.interfaces.IPropertyChangeNotifier;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractReportDataProvider  extends Composite implements IReportDataProvider , IPropertyChangeNotifier
{

	public AbstractReportDataProvider(Composite parent, int style) 
	{
		super(parent,style);
		m_registry= Registry.getRegistry("com.nov.rac.smdl.reports.panels.panels");
	}

	@Override
	public void firePropertyChange() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) 
	{
		m_listeners.add( listener );
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.remove( listener );
	}

	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}
	
	protected abstract boolean isValidState();
	

	//abstract public ArrayList<QuickBindVariable> getBindVariables() ;
	
	protected ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();
	protected Registry m_registry;
}
