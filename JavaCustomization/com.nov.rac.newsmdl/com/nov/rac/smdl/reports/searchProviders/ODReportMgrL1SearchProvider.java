package com.nov.rac.smdl.reports.searchProviders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.nov.rac.smdl.reports.exporters.LAGroupLevel1MgrReport;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class ODReportMgrL1SearchProvider implements INOVSearchProvider 
{
    private SimplePropertyMap m_dataMap = null;
    
    public ODReportMgrL1SearchProvider(SimplePropertyMap map)
    {
        m_dataMap = map;
    }
    @Override
    public String getBusinessObject() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public QuickBindVariable[] getBindVariables() {
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        
        String[] users = m_dataMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE);
        users = LAReportMgrL1SearchProvider.getAllMembersFor(users[0]);
        if (users.length > 0)
        {
            allBindVars[0] = new QuickSearchService.QuickBindVariable();
            allBindVars[0].nVarType = POM_string;
            allBindVars[0].nVarSize = users.length;
            allBindVars[0].strList = users;
        }
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar currentDate = Calendar.getInstance();
        String curDate = df.format(currentDate.getTime());
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        System.out.println("Printing the current date " + curDate);
        allBindVars[1].nVarType = POM_date;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { curDate };
        
        return allBindVars;
    }

    @Override
    public QuickHandlerInfo[] getHandlerInfo() {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[3];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-smdl-overdue-manager-report";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 5, 7, 3 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-smdl-manager-report-order-dp";
        //allHandlerInfo[1].listBindIndex = new int[] { 1 };
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1, 2, 3};
        allHandlerInfo[1].listInsertAtIndex = new int[] { 4, 1, 2 };
        
        allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-smdl-overdue-manager-report-docDue-per-contract";
        allHandlerInfo[2].listBindIndex = new int[] {1, 2};
        allHandlerInfo[2].listReqdColumnIndex = new int[] { 1};
        allHandlerInfo[2].listInsertAtIndex = new int[] { 6 };
        
        return allHandlerInfo;
    }

}
