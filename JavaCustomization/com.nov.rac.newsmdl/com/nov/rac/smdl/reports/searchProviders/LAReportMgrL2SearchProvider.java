package com.nov.rac.smdl.reports.searchProviders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentAliasList;
import com.teamcenter.rac.kernel.TCComponentAliasListType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class LAReportMgrL2SearchProvider implements INOVSearchProvider
{
    private SimplePropertyMap m_dataMap = null;
    
    public LAReportMgrL2SearchProvider(SimplePropertyMap map)
    {
        m_dataMap = map;
    }
    
    @Override
    public String getBusinessObject()
    {
        return null;
    }
    
    private static void getContents(TCComponentAliasListType aliasType, String aliasName, Vector<String> allAliases,
            Vector<String> refAllMembers)
    {
        try
        {
            TCComponentAliasList aliasList = aliasType.find(aliasName);
            if (aliasList != null)
            {
                String[] contents = aliasList.getTCProperty("listOfMembers").getStringValueArray();
                for (String s : contents)
                {
                    if (allAliases.contains(s))
                    {
                        getContents(aliasType, s, allAliases, refAllMembers);
                    }
                    else
                    {
                        refAllMembers.add(s);
                    }
                    
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public static String[] getAllGroupsFor(String aliasName, String[] groups)
    {
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        TCComponentAliasListType aliasType = null;

        try
        {
            aliasType = (TCComponentAliasListType) session.getTypeComponent("ImanAliasList");
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        Vector<String> listOfL2Members = new Vector<String>(Arrays.asList(groups));
        Vector<String> refAllSubGroups = new Vector<String>();
        
        getContents(aliasType, aliasName, listOfL2Members, refAllSubGroups);
        
        int len = refAllSubGroups.size();
        String[] allmembers = new String[len];
        for (int i = 0; i < len; ++i)
        {
            allmembers[i] = refAllSubGroups.elementAt(i);
        }
        return allmembers;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        String selectedGroup = m_dataMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[0];
        
        if (null != selectedGroup && !selectedGroup.isEmpty())
        {
            String[] allGroupNames = m_dataMap.getStringArray("SMDL_Reports_GroupLevel2_list");
            String[] members = getAllGroupsFor(selectedGroup, allGroupNames);
            
            if (members.length > 0)
            {
                allBindVars[0] = new QuickSearchService.QuickBindVariable();
                allBindVars[0].nVarType = POM_string;
                allBindVars[0].nVarSize = members.length;
                allBindVars[0].strList = members;
            }
            Date[] dateRange = m_dataMap.getDateArray(ReportsConstants.DATE_RANGE);
            if (dateRange.length > 0)
            {
                DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String fromDate = "*";
                String toDate = "*";
                if (null != dateRange[0])
                {
                    fromDate = df.format(dateRange[0]);
                }
                if (null != dateRange[1])
                {
                    toDate = df.format(dateRange[1]);
                }
                allBindVars[1] = new QuickSearchService.QuickBindVariable();
                allBindVars[1].nVarType = POM_date;
                allBindVars[1].nVarSize = dateRange.length;
                allBindVars[1].strList = new String[] { fromDate, toDate };
                
            }
        }
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-smdl-lookahead-highLevel-manager-report";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 3, 4 };
        return allHandlerInfo;
    }
    
}
