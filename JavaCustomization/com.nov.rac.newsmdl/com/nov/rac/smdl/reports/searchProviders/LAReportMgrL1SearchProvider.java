package com.nov.rac.smdl.reports.searchProviders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.eclipse.swt.widgets.Display;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentAliasList;
import com.teamcenter.rac.kernel.TCComponentAliasListType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;

public class LAReportMgrL1SearchProvider implements INOVSearchProvider
{
    
    private SimplePropertyMap m_dataMap = null;
    
    public LAReportMgrL1SearchProvider(SimplePropertyMap m_dataMap)
    {
        this.m_dataMap = m_dataMap;
    }
    
    @Override
    public String getBusinessObject()
    {
        return null;
    }
    
    private static void getContents(TCComponentAliasListType aliasType, String aliasName, Vector<String> allAliases,
            Vector<String> refAllMembers)
    {
        try
        {
            TCComponentAliasList aliasList = aliasType.find(aliasName);
            if (aliasList != null)
            {
                String[] contents = aliasList.getTCProperty("listOfMembers").getStringValueArray();
                for (String s : contents)
                {
                    if (allAliases.contains(s))
                    {
                        getContents(aliasType, s, allAliases, refAllMembers);
                    }
                    else
                    {
                        refAllMembers.add(s);
                    }
                    
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public static String[] getAllMembersFor(String aliasName)
    {
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        TCComponentAliasListType aliasType = null;
        TCComponent allAliasesComp[] = null;
        try
        {
            aliasType = (TCComponentAliasListType) session.getTypeComponent("ImanAliasList");
            allAliasesComp = aliasType.extent();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        Vector<String> allAliases = new Vector<String>();
        Vector<String> refAllMembers = new Vector<String>();
        for (TCComponent comp : allAliasesComp)
        {
            allAliases.add(comp.toDisplayString());
        }
        getContents(aliasType, aliasName, allAliases, refAllMembers);
        int len = refAllMembers.size();
        String[] allmembers = new String[len];
        for (int i = 0; i < len; ++i)
        {
            allmembers[i] = refAllMembers.elementAt(i);
        }
        return allmembers;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        String selectedGroup = m_dataMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[0];        
        if (null != selectedGroup && !selectedGroup.isEmpty())
        {
            String[] members = getAllMembersFor(selectedGroup);
            
            if (members.length > 0)
            {
                allBindVars[0] = new QuickSearchService.QuickBindVariable();
                allBindVars[0].nVarType = POM_string;
                allBindVars[0].nVarSize = members.length;
                allBindVars[0].strList = members;
            }
            Date[] dateRange = m_dataMap.getDateArray(ReportsConstants.DATE_RANGE);
            if (dateRange.length > 0)
            {
                DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String fromDate = "*";
                String toDate = "*";
                if (null != dateRange[0])
                {
                    fromDate = df.format(dateRange[0]);
                }
                if (null != dateRange[1])
                {
                    toDate = df.format(dateRange[1]);
                }
                allBindVars[1] = new QuickSearchService.QuickBindVariable();
                allBindVars[1].nVarType = POM_date;
                allBindVars[1].nVarSize = dateRange.length;
                allBindVars[1].strList = new String[] { fromDate, toDate };
                
            }
        }
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-smdl-lookahead-manager-report";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 5, 6, 3 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-smdl-manager-report-order-dp";
        // allHandlerInfo[1].listBindIndex = new int[] { 1 };
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1, 2, 3 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 4, 1, 2 };
        
        return allHandlerInfo;
    }
    
}
