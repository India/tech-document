package com.nov.rac.smdl.reports.searchProviders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class ODReportMgrL2SearchProvider implements INOVSearchProvider
{
    private SimplePropertyMap m_dataMap = null;
    
    public ODReportMgrL2SearchProvider(SimplePropertyMap map)
    {
        m_dataMap = map;
    }
    
    @Override
    public String getBusinessObject()
    {
        return null;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        String selectedGroup = m_dataMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE)[0];      
        if (null != selectedGroup && !selectedGroup.isEmpty())
        {
            String[] allGroupNames = m_dataMap.getStringArray("SMDL_Reports_GroupLevel2_list");
            String[] members = LAReportMgrL2SearchProvider.getAllGroupsFor(selectedGroup, allGroupNames);
            if (members.length > 0)
            {
                allBindVars[0] = new QuickSearchService.QuickBindVariable();
                allBindVars[0].nVarType = POM_string;
                allBindVars[0].nVarSize = members.length;
                allBindVars[0].strList = members;
            }
            
            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            Calendar currentDate = Calendar.getInstance();
            String curDate = df.format(currentDate.getTime());
            allBindVars[1] = new QuickSearchService.QuickBindVariable();
            allBindVars[1].nVarType = POM_date;
            allBindVars[1].nVarSize = 1;
            allBindVars[1].strList = new String[] { curDate };
        }
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-smdl-overdue-highLevel-manager-report";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 5, 4 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-smdl-overdue-highLevel-manager-report-per-planned";
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1 , 2 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 6, 3 };
        
        return allHandlerInfo;
    }
    
}
