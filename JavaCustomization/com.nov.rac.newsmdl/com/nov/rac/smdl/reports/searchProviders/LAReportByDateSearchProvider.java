package com.nov.rac.smdl.reports.searchProviders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class LAReportByDateSearchProvider implements INOVSearchProvider
{

    public LAReportByDateSearchProvider(SimplePropertyMap map)
    {
        m_dataMap = map;
    }
    
    /*public void setDataMap (SimplePropertyMap map)
    {
        m_dataMap = map;
    }
    */
    @Override
    public String getBusinessObject() 
    {
        
        return null;
    }

    @Override
    public QuickBindVariable[] getBindVariables()
    {
        //ArrayList <QuickSearchService.QuickBindVariable> bindVars = new ArrayList<QuickSearchService.QuickBindVariable>();
        //allBindVars = bindVars.toArray(allBindVars);

        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];

        String[] users = m_dataMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE);
        if(users.length >0)
        {
            allBindVars[0] = new QuickSearchService.QuickBindVariable();
            allBindVars[0].nVarType = POM_string;
            allBindVars[0].nVarSize = users.length;
            allBindVars[0].strList = users;
        }

        Date[] dateRange = m_dataMap.getDateArray(ReportsConstants.DATE_RANGE);
        //String[] dateRange = m_dataMap.getStringArray(ReportsConstants.DATE_RANGE);
        if(dateRange!= null && dateRange.length>0)
        {
            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");  
            String fromDate = "*"; 
            String toDate = "*";
            if(dateRange[0]!= null)
            {
                fromDate = df.format(dateRange[0]);
            }
            if(dateRange[1]!=null)
            {
                toDate = df.format(dateRange[1]);
            }
            allBindVars[1] = new QuickSearchService.QuickBindVariable();
            allBindVars[1].nVarType = POM_date;
            allBindVars[1].nVarSize = dateRange.length;
            //allBindVars[1].strList = new String[] {dateRange[0].toString(), dateRange[1].toString()};
            allBindVars[1].strList = new String[] {fromDate, toDate};
        }
        return allBindVars;

    }

    @Override
    public QuickHandlerInfo[] getHandlerInfo() 
    {
         QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
         
         allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
         allHandlerInfo[0].handlerName = "NOVSRCH-smdl-lookahead-report-by-planneddate";
         allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
         allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 , 11 };
         allHandlerInfo[0].listInsertAtIndex = new int[] {   8, 9, 10, 7, 1, 5, 4, 11, 2, 3 , 6};
         
         return allHandlerInfo;
    }

    SimplePropertyMap m_dataMap = new SimplePropertyMap();
}
