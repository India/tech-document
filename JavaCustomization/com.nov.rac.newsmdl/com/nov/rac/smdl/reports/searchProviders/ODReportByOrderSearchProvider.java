package com.nov.rac.smdl.reports.searchProviders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.smdl.reports.ReportsConstants;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class ODReportByOrderSearchProvider implements INOVSearchProvider
{
    private SimplePropertyMap m_dataMap = new SimplePropertyMap();
    
    public ODReportByOrderSearchProvider(SimplePropertyMap map)
    {
        m_dataMap = map;
    }
    
    @Override
    public String getBusinessObject()
    {
        return null;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        
        String[] users = m_dataMap.getStringArray(ReportsConstants.DOCRESPONSIBLE_TYPE);
        if (users.length > 0)
        {
            allBindVars[0] = new QuickSearchService.QuickBindVariable();
            allBindVars[0].nVarType = POM_string;
            allBindVars[0].nVarSize = users.length;
            allBindVars[0].strList = users;
        }
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar currentDate = Calendar.getInstance();
        String curDate = df.format(currentDate.getTime());
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_date;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { curDate };
        
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-smdl-overdue-report-by-order";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 4, 3, 1, 5, 7, 11, 10, 2, 8, 9, 6 };
        
        return allHandlerInfo;
    }
}
