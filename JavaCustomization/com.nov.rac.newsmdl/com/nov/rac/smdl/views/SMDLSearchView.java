package com.nov.rac.smdl.views;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.part.ViewPart;

import com.nov.rac.smdl.panes.SMDLSearchPane;
import com.nov.rac.smdl.panes.search.SMDLSearchInputDataProvider;

public class SMDLSearchView extends ViewPart {

	public static final String ID = "com.nov.rac.smdl.views.SMDLSearchView"; //$NON-NLS-1$

	private SMDLSearchPane container = null;
	
	public SMDLSearchView() {
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl( Composite parent )
	{
		//Composite container = new Composite(parent, SWT.NONE);
		
		container = new SMDLSearchPane( parent, SWT.NONE );
		
//		ProgressMonitorDialog createSearchInput = new ProgressMonitorDialog( Display.getDefault().getActiveShell() );
//		
//		/// Single Instance of the SMDLSearchInputDataProvider.
//		SMDLSearchInputDataProvider searchInputData = SMDLSearchInputDataProvider.getInstance();
//
//		try
//		{
//        	container.setSearchInputDataProvider( searchInputData );
//			createSearchInput.run( true, false, searchInputData );
//		}
//		catch( Exception e1 )
//		{
//            e1.printStackTrace();
//        }
		
		createActions();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars()
				.getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars()
				.getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
		container.setFocus();
	}

	 public void dispose()
	 {
		 container.dispose();
	 }
}
