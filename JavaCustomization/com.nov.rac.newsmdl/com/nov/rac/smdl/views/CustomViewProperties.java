package com.nov.rac.smdl.views;

public enum CustomViewProperties 
{
	PROP_IS_SAVEABLE ("isSaveable"),
    PROP_NOT_SAVEABLE_REASON ("notSaveableReason");
	
	private String property;    
	CustomViewProperties(String property) 
	{     
		this.property = property;   
	}    
	public String getProperty() 
	{     
		return this.property;   
	} 
}
