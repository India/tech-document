package com.nov.rac.smdl.views;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import com.nov.rac.kernel.NOV4ComponentSMDLHome;
import com.nov.rac.smdl.providers.providers.SMDLHomeContentProvider;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentFolderType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.providers.TCComponentLabelProvider;

public class SMDLHomeView extends SMDLComponentView /*implements PropertyChangeListener*/{
	
	private String 			m_HomeName = "";
	private TCComponent		m_SMDLHomeFolder = null;

	public static final String ID = "com.nov.rac.smdl.views.SMDLHomeView"; //$NON-NLS-1$

	public SMDLHomeView() {
		super();
	}
	
	protected void setProviders(StructuredViewer structuredviewer) {

		SMDLHomeContentProvider smdlContentProvider = new SMDLHomeContentProvider();

		structuredviewer.setContentProvider(smdlContentProvider);

		structuredviewer.setLabelProvider(new TCComponentLabelProvider());
	}
	
	@Override
	public void init(IViewSite iviewsite) throws PartInitException {

		super.init(iviewsite);
		
		m_SMDLHomeFolder = getHomeFolder();
		
		setPartName(m_HomeName);
	}

	
	@Override
	//protected void setTitleImage(Image titleImage) {
	public void setTitleImage(Image titleImage) { //TC10.1 Upgrade
		
		if(m_SMDLHomeFolder != null){
			Image titleImage1 = TCTypeRenderer.getTypeImage(m_SMDLHomeFolder.getTypeComponent(), null);
			super.setTitleImage(titleImage1);
		}
	}
	
	

	@Override
	protected void setInput(Object obj) {
		// TODO Auto-generated method stub
		super.setInput(obj);
		
		//Expanding the SMDL Home Tree upto all order level. 
		TreePath treePath = new TreePath(new Object[]{m_SMDLHomeFolder});
						
		getTreeViewer().expandToLevel(treePath, 1);
	}

	@Override
	protected ISelection getSelectionToProcessOnPartActivation(
			IWorkbenchPage iworkbenchpage) {
	
		return super.getSelectionToProcessOnPartActivation(iworkbenchpage);
	}

	
	private TCComponent getHomeFolder(){
		
			try {
				TCSession session = (TCSession) AIFUtility.getDefaultSession();
				TCComponentFolder userHomeFolder = session.getUser().getHomeFolder();
	
				m_SMDLHomeFolder = (TCComponentFolder) userHomeFolder.getRelatedComponent(NOV4ComponentSMDLHome.SMDL_HOME_RELATION);
				
				if(m_SMDLHomeFolder == null){
					TCComponentFolderType smdlHomeType = (TCComponentFolderType) session
							.getTypeService().getTypeComponent(
									NOV4ComponentSMDLHome.SMDL_HOME_OBJECT_TYPE);
	
					m_SMDLHomeFolder = smdlHomeType.create(
							NOV4ComponentSMDLHome.SMDL_HOME_NAME,
							NOV4ComponentSMDLHome.SMDL_HOME_DESC,
							NOV4ComponentSMDLHome.SMDL_HOME_OBJECT_TYPE);
	
					userHomeFolder.add(NOV4ComponentSMDLHome.SMDL_HOME_RELATION,
							m_SMDLHomeFolder);
				}
					
				m_HomeName = m_SMDLHomeFolder.getProperty("object_string");
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return m_SMDLHomeFolder;
	}
	
	@Override
	protected void setPartName(String partName) {
		// TODO Auto-generated method stub
		super.setPartName(m_HomeName);
	}

	
}
