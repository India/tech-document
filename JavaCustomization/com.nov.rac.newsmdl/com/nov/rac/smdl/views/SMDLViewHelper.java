package com.nov.rac.smdl.views;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.teamcenter.rac.kernel.TCComponent;


public class SMDLViewHelper
{
	//Changes done for Views disable(passing the last selected object in the tree and the class whose view needs to be open)
	
	public static TCComponent processViewSelection(ISMDLObject lastSelectedObject, Class clazz  )
	{
		TCComponent selectionObject = null;
		if(NOV4ComponentOrder.class.getName().equals(clazz.getName()))
		{
			selectionObject = lastSelectedObject.getOrder();
		}
		else if(NOV4ComponentDP.class.getName().equals(clazz.getName()))
		{
			if(lastSelectedObject instanceof NOV4ComponentSMDL)
			{
				selectionObject = lastSelectedObject.getParent();
			}
			if( lastSelectedObject instanceof NOV4ComponentSMDLRevision)
			{
				TCComponent dpObject = lastSelectedObject.getParent();
				selectionObject=((ISMDLObject) dpObject).getParent();
			}
		}
		else if(NOV4ComponentSMDL.class.getName().equals(clazz.getName()))
		{
			selectionObject= null;
		}

		return selectionObject;
	}
}