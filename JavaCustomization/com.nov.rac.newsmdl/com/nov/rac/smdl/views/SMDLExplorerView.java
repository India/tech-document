package com.nov.rac.smdl.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.smdl.providers.providers.SMDLExplorerContentProvider;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.providers.TCComponentLabelProvider;
import com.teamcenter.rac.providers.node.IFirstLevelNodeDescriptor;
import com.teamcenter.rac.providers.node.NavigatorRoot;
import com.teamcenter.rac.ui.common.RACUIUtil;
import com.teamcenter.rac.ui.views.TCComponentView;

public class SMDLExplorerView extends SMDLComponentView //implements PropertyChangeListener
{
	
	public static final String ID = "com.nov.rac.smdl.views.SMDLExplorerView";
	
	private TCComponent m_targetOrder = null;
	private TCComponent m_targetObject = null;
	
	
	private	List<ISMDLObject> m_parentList = null;
	
	private Map m_relatedObjects = null;
	
	
	public SMDLExplorerView()
	{
		super();	
	}
	
	 protected void setProviders(StructuredViewer structuredviewer){
		 
		SMDLExplorerContentProvider smdlContentProvider = new SMDLExplorerContentProvider();

		structuredviewer.setContentProvider(smdlContentProvider);
		structuredviewer.setLabelProvider(new TCComponentLabelProvider());

	}
	
	@Override
	public void init(IViewSite iviewsite) throws PartInitException {
		
		super.init(iviewsite);
	}
	

	public void open(TCComponent component){
	
		
		String partName="";
		try {
			partName=component.getProperty("object_string");
		} catch (TCException e) {
			e.printStackTrace();
		}
		
		setPartName(partName);
		Image titleImage=TCTypeRenderer.getTypeImage(component.getType(), null);
		setTitleImage(titleImage);
		
		//Jira Issue TCDECREL-2462
		// Replace this code with some common code..
		List<ISMDLObject> theList = getParentObjs(component);
		TreePath theTreePath = new TreePath(theList.toArray());
		TreeSelection theSelectionNode=new TreeSelection(theTreePath);
		processSingleSelection(theSelectionNode);
	}
	
	
	public List<ISMDLObject> getParentObjs(TCComponent selectedComp)
	{
		List<ISMDLObject> parentObjList = new ArrayList<ISMDLObject>();
		if(selectedComp instanceof ISMDLObject)
		{
			ISMDLObject smdlObj = (ISMDLObject)selectedComp;	
			
			parentObjList.add(smdlObj);
			
			smdlObj = (ISMDLObject)smdlObj.getParent();
			while(smdlObj != null)
			{
				parentObjList.add(smdlObj);
				smdlObj = (ISMDLObject)smdlObj.getParent();
			}
		}
		Collections.reverse(parentObjList);
		return parentObjList;
	}
	
	public Object getInitialInput(IMemento imemento)
    {
		
		Object returnObject = null;
		
		if(m_targetOrder == null){
		
			String secondaryId = ((IViewSite) getSite()).getSecondaryId();
			
			if(secondaryId != null && secondaryId.length() > 0){
			
				try {
					m_targetObject = getTCSession().stringToComponent(secondaryId);
				} catch (TCException e) {
					e.printStackTrace();
				}
			}
			m_targetOrder = ((ISMDLObject)m_targetObject).getOrder();
		}
					
		return m_targetOrder;
		
    }

	@Override
	protected void setInput(Object obj) {
		
		TCComponent tccomponent = RACUIUtil.getComponent(obj);
		
		AIFComponentContext aifcomponentcontext = new AIFComponentContext(null, tccomponent, "");
		
		IC_ViewRoot ic_viewroot = new IC_ViewRoot(this, aifcomponentcontext);
		
        getTreeViewer().setInput(ic_viewroot);
        
        m_parentList = new ArrayList<ISMDLObject>();
		
		m_parentList = getParentObjs(m_targetObject);
		
		TreePath treePath = new TreePath(m_parentList.toArray());
		
		getTreeViewer().expandToLevel(treePath, 0);
		
		TreeItem selectItem = (TreeItem) getTreeViewer().testFindItem(treePath.getLastSegment());
		
		if (selectItem != null){
			getTreeViewer().getTree().select(selectItem);
		}
		
	}
	
	
	
	
	private class IC_ViewRoot extends NavigatorRoot
	{

		public List getFirstLevelDataObjects()
		{
			return firstLevelDataObjects;
		}

		final TCComponentView this$0;

		public IC_ViewRoot(IViewPart iviewpart, Object obj)
		{
			super(iviewpart);
			this$0 = SMDLExplorerView.this;
			firstLevelDescriptorObjects = new ArrayList(0);
			IC_FirstLevelNodeDescriptor ic_firstlevelnodedescriptor = new IC_FirstLevelNodeDescriptor(obj);
			firstLevelDescriptorObjects.add(ic_firstlevelnodedescriptor);
			firstLevelDataObjects = new ArrayList(0);
			firstLevelDataObjects.add(ic_firstlevelnodedescriptor.getData());
		}
	}

	private class IC_FirstLevelNodeDescriptor
	implements IFirstLevelNodeDescriptor
	{

		public Object getData()
		{
			return m_object;
		}

		private Object m_object;
		final TCComponentView this$0;

		public IC_FirstLevelNodeDescriptor(Object obj)
		{
			super();
			this$0 = SMDLExplorerView.this;
			m_object = null;
			m_object = obj;
		}
	}
	

}
