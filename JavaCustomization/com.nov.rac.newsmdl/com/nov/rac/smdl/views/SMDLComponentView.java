package com.nov.rac.smdl.views;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPartService;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentSMDLHome;
import com.nov.rac.smdl.providers.providers.ISMDLContentProvider;
import com.nov.rac.smdl.utilities.NOVOpenObjectUtils;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.utilities.utils.TCServicesHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.AIFComponentEvent;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.common.IContextInputService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.providers.node.NavigatorRoot;
import com.teamcenter.rac.ui.views.TCComponentView;
import com.teamcenter.rac.ui.views.WorkspaceTreeViewPart;
import com.teamcenter.rac.util.AdapterUtil;

public class SMDLComponentView extends TCComponentView implements PropertyChangeListener{

	protected ISMDLObject m_CurrentSelected = null;
	
	private static /*ISelection*/ Object	m_SelectionObj =null;
	private static IWorkbenchPart m_SelectedView=null;

	public SMDLComponentView(){
		super();
	}

	

	//@Override //TC10.1 Upgrade
	protected void processSingleSelection(Object theSelection)
	{
		if( getLastSelectedNode()==null || !(theSelection.equals(getLastSelectedNode())))
		{
			if(theSelection instanceof ISelection)
			{
				openView( (ISelection) theSelection );
			}
			
		}
		else
		{
			IWorkbenchWindow iwindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			iwindow.getActivePage();
			IWorkbenchPage page=iwindow.getActivePage();;
			IWorkbenchPart activePart = page.getActivePart();

			ISMDLObject theObject = getISMDLObject(theSelection);
			
			if(theObject != null)
			{
			    IViewPart viewPart = NOVSMDLExplorerViewHelper.getSMDLView(	page, theObject);
			    
			    if(viewPart != null)
			    {
			    	page.bringToTop(viewPart);
			    }
			}
		}
	}



	private ISMDLObject getISMDLObject(Object theObject)
	{
		ISMDLObject iSMDLObj = null;		
		if(theObject != null)
		{
			Object theTCObject = getTCComponent(theObject);

			if(theTCObject instanceof ISMDLObject)
			{	
				iSMDLObj = (ISMDLObject)theTCObject;
			}
			else if(!(theTCObject instanceof NOV4ComponentSMDLHome ))
			{
				if( theObject instanceof TreeSelection)
				{
					iSMDLObj = getParentSMDLObject(theObject);
				}
			}
		}
		return iSMDLObj;
	}
	
	private ISMDLObject getParentSMDLObject(Object theObject) 
	{	
		ISMDLObject parentISMDLObject = null;
	
		int parentsCount = 0;
		TreePath[] treePath = ((TreeSelection) theObject).getPaths();
		if(treePath.length!=0)
		{
			parentsCount = treePath[0].getSegmentCount();
			
			for(int inx=parentsCount-1; inx>=0; inx--)
			{
				Object parentObject = treePath[0].getSegment(inx);
				
				if(parentObject instanceof ISMDLObject)
				{
					parentISMDLObject = (ISMDLObject)parentObject;
					break;
				}				
			}		
		}		
		return parentISMDLObject;
	}

	private Object getTCComponent(Object theObject)
	{
		if(theObject != null)
		{
			if(theObject instanceof TreeSelection){
				theObject = ((TreeSelection)theObject).getFirstElement();
			}
			
			if( theObject instanceof AIFComponentContext)
			{
				theObject = ((TCComponent) (((AIFComponentContext) theObject).getComponent()));
			}
		}
		return theObject;
	}
	
	@Override
	public void selectionChanged(IWorkbenchPart iworkbenchpart,
			ISelection iselection) 
	{
			if(iworkbenchpart == this )
			{
				processSingleSelection(iselection);
			}
	}	

	
	@Override
	protected int getViewerStyleBits() {
		return SWT.SINGLE;
	}

	@Override
	protected void processDoubleClick(List<Object> list) {

		Object selectedObject = list.get(0);
		
		TCComponent component =(TCComponent) getTCComponent(selectedObject);

		if(component instanceof NOV4ComponentSMDLHome || component instanceof ISMDLObject)
		{
			processSingleSelection(selectedObject);
		}
		else
		{		
			NOVOpenObjectUtils.invokeObjectApplication(getTCSession(),component);
		}
	}


	//@Override //TC10.1 Upgrade
	protected void setInput(Object obj) {
		if(obj != null){
			getTreeViewer().setInput(obj);
		}
	}
	
	@Override
	public void partVisible(IWorkbenchPartReference iworkbenchpartreference)
	{
		 if(iworkbenchpartreference.getPart(false) == this)
	        {
	            ISelection iselection = getSelectionToProcessOnPartActivation(iworkbenchpartreference.getPage());
	            getTreeViewer().setSelection(iselection);
	        }
	
	}

	protected ISelection getSelectionToProcessOnPartActivation(IWorkbenchPage iworkbenchpage)
    {
		
		ISelection iselection = null;
		
        if(iworkbenchpage == null) 
        {
			iworkbenchpage = getActivePage();
		}
        
        if(iworkbenchpage != null)
        {
        	iselection = getTreeViewer().getSelection();
        	 if(iselection != null && !iselection.isEmpty()) 
        	 {
				return iselection;
        	 }
        	
        	iselection = iworkbenchpage.getSelection();
                        
            if(iselection != null && !iselection.isEmpty()) 
            {
				return iselection;
			}
        
            //iselection = getSelectionFromContextInputService();
            
            //TC10.1 Upgrade
            IContextInputService icontextinputservice = TCServicesHelper.getContextInputService();
            
            if(icontextinputservice != null)
            {
                iselection =  icontextinputservice.getInput();
            }
        }
        
        
        return iselection;
    }
	
	 private IWorkbenchPage getActivePage()
	    {
	        IWorkbenchPage iworkbenchpage = null;
	        IWorkbench iworkbench = PlatformUI.getWorkbench();
	        if(iworkbench != null)
	        {
	            IWorkbenchWindow iworkbenchwindow = iworkbench.getActiveWorkbenchWindow();
	            if(iworkbenchwindow != null) 
	            {
					iworkbenchpage = iworkbenchwindow.getActivePage();
				}
	        }
	        return iworkbenchpage;
	    }
	
	public void openView(final ISelection currentSelection)
	{
		ISMDLObject component = getISMDLObject(currentSelection);
		
			if(component != null && component instanceof ISMDLObject /*&& !component.equals(m_CurrentSelected)*/){
			
			IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			final IWorkbenchPage page =iw.getActivePage();
			
			// Check if save operation required for the last edited view
			boolean bViewDirty = false;
			
			ISMDLObject lastEditedObject = NOVSMDLExplorerViewHelper.getLastSelectedObject();
			
			if(/*m_CurrentSelected*/ lastEditedObject!= null){

				//bViewDirty = NOVSMDLExplorerViewHelper.saveView(page, m_CurrentSelected);
				
				bViewDirty = NOVSMDLExplorerViewHelper.saveView(page,lastEditedObject);
				
			}

			if(bViewDirty == false){

				m_CurrentSelected = (ISMDLObject)component;
				final TCComponent openComponent = (TCComponent) component;
				final IWorkbenchPart currentView = this;
				// Show Busy indicator while the view is loaded for SMDL Object

				BusyIndicator.showWhile(Display.getDefault(), new Runnable() {
					public void run() {
						Display.getDefault().asyncExec(new Runnable() {

							public void run() {

								IWorkbenchPart activePart = page.getActivePart();

								PropertyChangeListener theListener = (PropertyChangeListener) AdapterUtil.getAdapter(activePart, PropertyChangeListener.class);

								if(theListener != null){
//									NOVSMDLExplorerViewHelper.openView(page, theListener ,(TCComponent) m_CurrentSelected);
									NOVSMDLExplorerViewHelper.openView(page, theListener , openComponent);
									
									setLastSelectedNode(currentSelection);
									setLastSelectedView(currentView);
								}
								
								//page.activate(activePart);
							}
						});
					}
				});
				
			}
			else
			{	
				Display.getDefault().asyncExec(new Runnable() {

					@Override
					public void run() 
					{
						restoreOldViewState(page);
						
					}
					
				});
							
			}
			
		}
	}



	private void restoreOldViewState(final IWorkbenchPage page)
	{
		
		IWorkbenchPart lastSelectedView = getLastSelectedView();
		//switching selection within view
		SMDLComponentView restoreToview = this;
		
		//switching selection across views
		if(lastSelectedView != null && !this.equals(lastSelectedView))
		{
			if(lastSelectedView instanceof SMDLComponentView)
			{
				restoreToview =(SMDLComponentView)lastSelectedView;
			}
		}
		
		//clear all listeners, to disable selection change notifications while restoring
		restoreToview.cleanupListeners();
		
		//restore view 
		if(lastSelectedView != null && !this.equals(lastSelectedView))
		{
			synchronized(lastSelectedView)
			{
					page.bringToTop(lastSelectedView);
					//page.activate(lastSelectedView);
			}
		}
		// restore selection
		//StructuredSelection lastSelectionNode=new StructuredSelection(NOVSMDLExplorerViewHelper.getLastSelectedObject());
	    ISelection lastSelectionNode = (ISelection) getLastSelectedNode();
		restoreToview.getTreeViewer().setSelection(lastSelectionNode);
		//setLastSelectedNode(lastSelectionNode);
		//enable all listeners
		restoreToview.setupListeners();
		
	}

	
	public Object getInitialInput(IMemento imemento)
    {
		return new NavigatorRoot(this);
    }

	@Override
	public void propertyChange(PropertyChangeEvent event) {

		if(event.getPropertyName().equals("AddSMDLObjectOperation") )
		{
			TCComponent theCreatedObject = (TCComponent) event.getNewValue();

			TCComponent parentObject = ((ISMDLObject)theCreatedObject).getParent();

			ISMDLContentProvider contentProvider = (ISMDLContentProvider) getTreeViewer().getContentProvider();

			contentProvider.addChild(parentObject, theCreatedObject);

			getTreeViewer().refresh();

		} else if (event.getPropertyName().equals("RemoveSMDLObjectOperation")) {
			
			TCComponent theRemovedObject = (TCComponent) event.getNewValue();
			
			TCComponent parentObject = (TCComponent) event.getOldValue();
			
			ISMDLContentProvider contentProvider = (ISMDLContentProvider) getTreeViewer().getContentProvider();
			
			contentProvider.removeChild(parentObject, theRemovedObject);
			
			getTreeViewer().refresh();
		}


	}

	// To call AIF change, delete, child count events
	// This is used to refresh SMDLHome view for all events like cut, paste, delete, revise etc
    protected void processSortedEvents(AIFComponentEvent aaifcomponentevent[])
    {
        super.processSortedEvents(aaifcomponentevent);
        getViewer().refresh();
    }
    public ISMDLObject getSelection()
    {
    	return m_CurrentSelected;
    	
    }
	public static Object getLastSelectedNode()
	{
		
	     return m_SelectionObj;		
	   
	}
	
	public static void setLastSelectedNode(Object pageSelection)
	{
		m_SelectionObj = pageSelection;		
	}
	private void setLastSelectedView(IWorkbenchPart iworkbenchpart)
	{
		 m_SelectedView = iworkbenchpart;	
	}
	private  IWorkbenchPart getLastSelectedView()
	{
		return  m_SelectedView ;	
	}



	@Override
	public void doSave(IProgressMonitor iprogressmonitor) 
	{
		ISaveablePart saveablePart= getSaveablePart();
		if(saveablePart!=null)
		{
			saveablePart.doSave(iprogressmonitor);
		}
	}



	@Override
	public boolean isDirty() 
	{
		boolean isDirtyFlag= false;
		ISaveablePart saveablePart= getSaveablePart();
		if(saveablePart!=null)
		{
			isDirtyFlag= saveablePart.isDirty();
		}
			
		return isDirtyFlag;
	}


	private ISaveablePart getSaveablePart()
	{
		ISaveablePart saveablePart= null;
		ISelection currentSelection= getViewer().getSelection();
		ISMDLObject component = getISMDLObject(currentSelection);
		
		if(component!=null)
		{
			IWorkbenchWindow iwindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			iwindow.getActivePage();
			IWorkbenchPage page=iwindow.getActivePage();;
					
		    IViewPart viewPart = NOVSMDLExplorerViewHelper.getSMDLView(	page, component);
		    
		    if(viewPart instanceof ISaveablePart)
		    {
		    	saveablePart = (ISaveablePart)viewPart;
		    	//((ISaveablePart)viewPart).doSave(iprogressmonitor);
		    }
		}
		return saveablePart;
	}

}
