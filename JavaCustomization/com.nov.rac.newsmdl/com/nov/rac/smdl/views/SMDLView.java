package com.nov.rac.smdl.views;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.part.ViewPart;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.panes.SMDLPane;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class SMDLView extends ViewPart implements ISaveablePart{

	public static final String 		ID = "com.nov.rac.smdl.views.SMDLView"; //$NON-NLS-1$
	private SMDLPane 				m_container;

	public SMDLView() {
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		
		m_container = new SMDLPane(parent, SWT.NONE);
		createActions();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	public SMDLPane getContainer() {
		return m_container;
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars()
				.getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars()
				.getMenuManager();
	}

	@Override
	public void setFocus() 
	{
		synchronized (this) 
		{
			//TCDECREL1528 views disable
			ISMDLObject lastSelectedObject = NOVSMDLExplorerViewHelper.getLastSelectedObject();
			if(!(lastSelectedObject instanceof NOV4ComponentSMDL)&& !(lastSelectedObject instanceof NOV4ComponentSMDLRevision))
			{
			
				TCComponent viewSelectionObject = (TCComponent) SMDLViewHelper.processViewSelection(lastSelectedObject,NOV4ComponentSMDL.class );
				if(viewSelectionObject==null)
				{
					this.getContainer().clearPanelValues();
					this.getContainer().setEnabled(false);
				}
				else
				{
					this.getContainer().setEnabled(true);
					this.getContainer().populateSMDL(viewSelectionObject);
				}
			}
		}
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor) 
	{
		m_container.save(progressMonitor);
	}

	@Override
	public void doSaveAs() 
	{
		
	}

	@Override
	public boolean isDirty() 
	{
		boolean localIsModelDirty = m_container.isDirty();
		
		TCComponent tcComponent = m_container.getComponent();		
		if(localIsModelDirty && tcComponent != null )
	    {
	        TCComponent[] statusList = null;
			try 
			{
				statusList = tcComponent.getTCProperty(NOV4ComponentSMDLRevision.SMDLREV_RELEASE_STATUS).getReferenceValueArray();
			}
			catch (TCException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String isSaveableProp = CustomViewProperties.PROP_IS_SAVEABLE.getProperty();
        	String isNotSaveableProp = CustomViewProperties.PROP_NOT_SAVEABLE_REASON.getProperty();
	        // If you get a release status, no need to save the SMDL Rev.
	        if (statusList != null && statusList.length > 0)
	        {
	        	Registry appReg = Registry.getRegistry("com.nov.rac.smdl.common.common");
				setPartProperty(isSaveableProp, "false");
				setPartProperty(isNotSaveableProp, appReg.getString("SMDLRELEASEDREV.MSG"));
	        }
	        else
	        {
	        	setPartProperty(isSaveableProp, null);
				setPartProperty(isNotSaveableProp, null);
	        }
	    }
		
		return localIsModelDirty;
	}

	@Override
	public boolean isSaveAsAllowed() 
	{
		return false;
	}

	@Override
	public boolean isSaveOnCloseNeeded()
	{
		return m_container.isDirty();
	}
	
	public void setDirty(boolean flag){
		
		m_container.setDirty(flag);
	}

}

