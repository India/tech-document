package com.nov.rac.smdl.views;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.part.ViewPart;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentOrder;
import com.nov.rac.smdl.panes.OrderPane;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.smdl.utilities.NOVSMDLOrderHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.SessionChangedEvent;
import com.teamcenter.rac.kernel.SessionChangedListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;

public class OrderView extends ViewPart implements ISaveablePart, SessionChangedListener
{

	public static final String 					ID = "com.nov.rac.smdl.views.OrderView";
	private OrderPane 							m_container;
	private ArrayList<PropertyChangeListener> 	m_listeners = new ArrayList<PropertyChangeListener>();

	private TCSession m_session = (TCSession)AIFUtility.getDefaultSession();
	private ISMDLObject m_selectedobj =null;
	//private Registry m_appReg;
	private PropertyChangeListener m_lastListener =null;

    public OrderView()
	{
		//m_appReg = Registry.getRegistry("com.nov.rac.smdl.panes.order.order");
	}

	public void createPartControl(Composite parent) {

		parent.setLayout( new FillLayout());

		ScrolledComposite comp = new ScrolledComposite( parent, SWT.V_SCROLL | SWT.H_SCROLL);
		comp.setAlwaysShowScrollBars( true );
		comp.setLayout( new FillLayout() );

		m_container = new OrderPane(comp, SWT.NONE);
		m_container.createUI();

		comp.setContent( m_container );
		comp.setExpandHorizontal( true );
		comp.setExpandVertical( true );
		comp.setMinSize(m_container.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		//container.addControlListener( new OrderControlListener( comp ) );
		comp.pack();

		//getViewSite().getPage().addSelectionListener(this);


		createActions();
		initializeToolBar();
		initializeMenu();

		this.m_session.addSessionChangeListener(this);

//		IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//		IWorkbenchPage page = iw.getActivePage();
//
//		page.addPartListener(this);
	}

	public OrderPane getContainer() {
		return m_container;
	}

	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
	}

	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars()
				.getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars()
				.getMenuManager();
	}

	@Override
	public void setFocus() 
	{
		synchronized (this) 
		{
			//TCDECREL1528 views disable
			ISMDLObject lastSelectedObject = NOVSMDLExplorerViewHelper.getLastSelectedObject();
			if(!(lastSelectedObject instanceof NOV4ComponentOrder))
			{
				if(lastSelectedObject!= m_selectedobj)
				{
					m_selectedobj = lastSelectedObject;
					TCComponent viewSelectionObject = (TCComponent) SMDLViewHelper.processViewSelection(lastSelectedObject,NOV4ComponentOrder.class );
					this.getContainer().clearPanelValues();
					if(viewSelectionObject!=null)
					{
						this.getContainer().setEnabled(false);
						this.getContainer().populateOrder(viewSelectionObject);					
					}
				}
			}
			if(lastSelectedObject==null)
			{
				this.getContainer().clearPanelValues();
				this.getContainer().setEnabled(false);
			}
			
			PropertyChangeListener lastListener = NOVSMDLExplorerViewHelper.getLastListener();
			if((lastListener != null) && (lastListener != m_lastListener))
			{
				m_lastListener = lastListener;
				addPropertyChangeListener(lastListener);
			}
		}
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor)
	{
		m_container.save(progressMonitor);
//		m_container.setDirty(false);
	}

	@Override
	public void doSaveAs()
	{

	}

	@Override
	public boolean isDirty()
	{
		return m_container.isDirty();
	}

	@Override
	public boolean isSaveAsAllowed()
	{
		return false;
	}

	@Override
	public boolean isSaveOnCloseNeeded()
	{
		return m_container.isDirty();
	}
	
	public void setDirty(boolean flag){
		
		m_container.setDirty(flag);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		m_container.addPropertyChangeListener(listener);
	}

	@Override
	public void sessionChanged(SessionChangedEvent arg0) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				boolean editOrder = NOVSMDLOrderHelper.isOrderEditable();
				m_container.setEditability(editOrder);
			}
		});
	}
}
