package com.nov.rac.smdl.views;

import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.part.ViewPart;

import com.nov.rac.kernel.ISMDLObject;
import com.nov.rac.kernel.NOV4ComponentDP;
import com.nov.rac.smdl.panes.DeliveredProductPane;
import com.nov.rac.smdl.utilities.NOVSMDLExplorerViewHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCComponent;

public class DeliveredProductView extends ViewPart implements ISaveablePart
{

	public static final String 		ID = "com.nov.rac.smdl.views.DeliveredProductView";
	private DeliveredProductPane 	m_container;
	private ISMDLObject m_selectedobj =null;
	private PropertyChangeListener m_lastListener =null;

	public DeliveredProductView() {
	}

	public void createPartControl( Composite parent )
	{
		parent.setLayout( new FillLayout() );
		
		ScrolledComposite comp = new ScrolledComposite( parent, SWT.V_SCROLL | SWT.H_SCROLL);
		comp.setAlwaysShowScrollBars( true );
		comp.setLayout( new FillLayout() );
		
		m_container = new DeliveredProductPane( comp, SWT.NONE );
		m_container.createUI();
		
		comp.setContent( m_container );
		comp.setExpandHorizontal( true );
		comp.setExpandVertical( true );
		//comp.setMinSize(m_container.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		int heightHint=SWTUIHelper.convertVerticalDLUsToPixels(m_container, 500);
		int widthHint = SWTUIHelper.convertHorizontalDLUsToPixels(m_container, 700);
		comp.setMinSize(m_container.computeSize(widthHint,heightHint));
		comp.pack();
		///Added the controller for getting the scroll bar.
		//m_container.addControlListener( new DpControlListener( comp ) );
		
		createActions();
		initializeToolBar();
		initializeMenu();
		
	}
	
	public DeliveredProductPane getContainer() {
		return m_container;
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars()
				.getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars()
				.getMenuManager();
	}

	@Override
	public void setFocus()
	{
		//TCDECREL1528 views disable
		synchronized (this) 
		{
			
			ISMDLObject lastSelectedObject = NOVSMDLExplorerViewHelper.getLastSelectedObject();
					
			if(!(lastSelectedObject instanceof NOV4ComponentDP))
			{
				TCComponent viewSelectionObject = (TCComponent) SMDLViewHelper.processViewSelection(lastSelectedObject,NOV4ComponentDP.class);
	
				if(lastSelectedObject!= m_selectedobj)
				{
					m_selectedobj = lastSelectedObject;
					//TCComponent viewSelectionObject = (TCComponent) SMDLViewHelper.processViewSelection(lastSelectedObject,NOV4ComponentDP.class);
	
					this.getContainer().clearPanelValues();
					if(viewSelectionObject!=null)
					{
						this.getContainer().setEnabled(false);
						this.getContainer().populateDP(viewSelectionObject);
					} 
				}
				else if(viewSelectionObject==null)
				{
					this.getContainer().clearPanelValues();
					this.getContainer().setEnabled(false);			
				}		
			}
			
			PropertyChangeListener lastListener = NOVSMDLExplorerViewHelper.getLastListener();
			if((lastListener != null) && (lastListener != m_lastListener))
			{
				m_lastListener = lastListener;
				addPropertyChangeListener(lastListener);
			}
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		m_container.addPropertyChangeListener(listener);
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor)
	{
		m_container.save(progressMonitor);
	}

	@Override
	public void doSaveAs() 
	{
		
	}

	@Override
	public boolean isDirty() {
	return m_container.isDirty();
		
		
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public boolean isSaveOnCloseNeeded() {
		return m_container.isDirty();
	}

	
	public void setDirty(boolean flag){
		
		m_container.setDirty(flag);
	}
	////ankita
	
}
