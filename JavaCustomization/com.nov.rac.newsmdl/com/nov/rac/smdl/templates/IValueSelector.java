package com.nov.rac.smdl.templates;

import com.teamcenter.rac.kernel.TCComponent;

public interface IValueSelector
{

	void setValue(TCComponent component);
}
