package com.nov.rac.smdl.templates;

import com.teamcenter.rac.aif.common.AIFTable;

public interface ITableInputProvider 
{
	public AIFTable getTable();
}
