package com.nov.rac.smdl.templates;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;


public abstract class AbstractOperationDelegate implements IOperationDelegate 
{
	  protected Shell m_shell = null;
      protected Map<String, IOperationInputProvider> m_oprInputProviders;

      public AbstractOperationDelegate (Shell theShell)
      {
            m_oprInputProviders = new HashMap<String, IOperationInputProvider>();
            m_shell  = theShell;
      }
      
      
      @Override
      public void addOperationInputProvider( String name,
                                                 IOperationInputProvider provider)
      {
            m_oprInputProviders.put(name, provider);
      }                                                                                      
}
