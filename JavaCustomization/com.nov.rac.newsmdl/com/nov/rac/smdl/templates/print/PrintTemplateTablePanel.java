package com.nov.rac.smdl.templates.print;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.templates.ITableInputProvider;
import com.nov.rac.smdl.templates.SimpleSelectionTemplateTablePanel;
import com.teamcenter.rac.aif.common.AIFTable;


public class PrintTemplateTablePanel extends SimpleSelectionTemplateTablePanel implements ITableInputProvider
{

	public PrintTemplateTablePanel(Composite parent, int style) 
	{
		super(parent, style);
	}
		
	@Override
	public AIFTable getTable() 
	{
		return m_smdlTable;
	}
}
