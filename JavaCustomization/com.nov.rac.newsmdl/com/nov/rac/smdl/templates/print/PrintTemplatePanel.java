package com.nov.rac.smdl.templates.print;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.templates.SimpleSelectionTemplatePanel;


public class PrintTemplatePanel extends SimpleSelectionTemplatePanel 
{	
	public PrintTemplatePanel( Composite parent, int style )
	{
		super(parent, style);
	}
	
	protected void setLabels() 
	{
		m_nameLabel.setText( m_registry.getString("printTemplateName.Label") );
		m_descLabel.setText( m_registry.getString("templateDesc.Label") );
	}
	
	@Override
	protected String doGetErrorMessage() 
	{
		String errorMessage = m_registry.getString("Template_Name_Print.Error");
		return errorMessage;
	}
}
