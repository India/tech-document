package com.nov.rac.smdl.templates.print;

import javax.swing.JTable;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.export.NOVExportSearchResultUtils;
import com.nov.rac.smdl.templates.AbstractOperationDelegate;
import com.nov.rac.smdl.templates.IOperationInputProvider;
import com.nov.rac.smdl.templates.ITableInputProvider;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class PrintTemplateOperationDelegate extends AbstractOperationDelegate 
{
	private TCComponentItem m_templateItem;
	public PrintTemplateOperationDelegate(Shell theShell)
	{
		super(theShell);
	}
	
	
	@Override
	public boolean executeOperation()
	{	
		boolean bCloseDlg = false;
		
		IOperationInputProvider upperPanel = m_oprInputProviders.get(TemplateConstants.TEMPLATE_PANEL);
		m_templateItem = (TCComponentItem) upperPanel.getComponent();
		try 
		{
			final String sTemplateName = m_templateItem.getTCProperty("object_name").getStringValue();
			
			final IOperationInputProvider tablePanel = m_oprInputProviders.get(TemplateConstants.TEMPLATE_TABLE_PANEL);
			if(tablePanel instanceof ITableInputProvider)
			{
				Display.getCurrent().syncExec( new Runnable() 
				{
					@Override
					public void run() 
					{
						AIFTable table = ((ITableInputProvider)tablePanel).getTable();
						String sHeader[] = {sTemplateName};
						
						NOVExportSearchResultUtils.exportData(table,sHeader,sTemplateName);
					}
				});
			}			
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return bCloseDlg;
	}


	@Override
	public CreateInObjectHelper[] getOperationInput() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
