package com.nov.rac.smdl.templates.renderereditors;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;

import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.teamcenter.rac.util.Painter;


	public class ContractDateEditor extends DefaultCellEditor 
	{
	    public ContractDateEditor()
	    {
	        super(new JComboBox(getItems() ) {
	        	 public void paint(Graphics g )
	                {
	                    super.paint(g);
	                    Painter.paintIsRequired(this, g);
	                }
	        });
	        
	        
	    }

		private static String[] getItems() {
			
			return NOVCodeHelper.loadIssueDateLOV();
		}

		@Override
		public Component getTableCellEditorComponent(JTable jtable, Object obj,
				boolean flag, int i, int j) {
			
			return super.getTableCellEditorComponent(jtable, obj, flag, i, j);
		}

		
		
		
	}