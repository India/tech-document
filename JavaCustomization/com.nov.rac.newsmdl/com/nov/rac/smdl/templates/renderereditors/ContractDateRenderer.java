package com.nov.rac.smdl.templates.renderereditors;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.teamcenter.rac.util.Painter;
import com.teamcenter.rac.util.combobox.iComboBox;

public class ContractDateRenderer extends iComboBox implements TableCellRenderer  /*DefaultTableCellRenderer*/ 
{
    private static final long serialVersionUID = 1L;
    private boolean required;

    public ContractDateRenderer()
    {
        super();
    	for(String string : getListItems())
		{
    		this.addItem(string);
		}
    	this.setRequired(true);
    }
    
    
    private String[] getListItems()
    {
    	return NOVCodeHelper.loadIssueDateLOV();
	}


	public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
    	
        if ( isSelected )
        {
            setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        }
        else
        {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }
      
       if(value.toString().trim().length()>0)
       {
    	   this.setSelectedItem(value);
       }
       else
       {
    	   this.setSelectedIndex(-1);
       }
    
        return this;
    }

	
	 public void setRequired(boolean required )
     {
         this.required = required;
     }

     public boolean isRequired()
     {
         return this.required;
     }


	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		 if ( required )
         {
             Painter.paintIsRequired(this, g);
         }
		
	}

          
    
}