package com.nov.rac.smdl.templates.renderereditors;


import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;

import com.nov.rac.smdl.common.NOVTableCellLimit;
import com.teamcenter.rac.util.iTextField;

public class NOVLineItemNumberCellEditor implements TableCellEditor
{
	
	private TextField m_iTextField ;	
	
    public NOVLineItemNumberCellEditor()
    {
    	m_iTextField = new TextField();
    	m_iTextField.setDocument(new NOVTableCellLimit(10));
    }       
    @Override
	public Component getTableCellEditorComponent(JTable jtable, Object obj,
			boolean flag, int i, int j) 
  	{	 
		String value = (String) obj;
		
		m_iTextField.setText(value != null ? value : "");
		m_iTextField.setForeground(Color.BLACK);
		m_iTextField.setBackground(Color.WHITE); 
		m_iTextField.setCaretColor(Color.WHITE);
		
        return m_iTextField;
	}
	@Override
	public void addCellEditorListener(CellEditorListener listener) 
	{
		m_iTextField.getEventListenerList().add(CellEditorListener.class, listener);
	}
	@Override
	public Object getCellEditorValue() 
	{
		return m_iTextField.getText();
	}
	@Override
	public boolean isCellEditable(EventObject arg0) 
	{
		return true;
	}
	@Override
	public void removeCellEditorListener(CellEditorListener listener) {
		
		m_iTextField.getEventListenerList().remove(CellEditorListener.class, listener);
	}
	@Override
	public boolean shouldSelectCell(java.util.EventObject eventobject) 
	{
		m_iTextField.repaint();
	    return true;
	}
	@Override
	public boolean stopCellEditing() 
	{
		fireEditingStopped();
		return true;
	}
	@Override
	public void cancelCellEditing() 
	{		 
		fireEditingCanceled();
	}
	
	private void fireEditingCanceled()
	{
		CellEditorListener listener;

		Object[] listeners = m_iTextField.getEventListenerList().getListenerList();
		for ( int i = 0; i < listeners.length; i++ )
		{
			if ( listeners[i] == CellEditorListener.class )
			{
				if (m_iTextField.changeEvent == null)
					m_iTextField.changeEvent = new ChangeEvent(m_iTextField);
				listener = (CellEditorListener) listeners[i + 1];
				listener.editingCanceled(m_iTextField.changeEvent);
			}
		}
	}	
	private void fireEditingStopped()
	{
		m_iTextField.repaint();
		CellEditorListener listener;
		Object[] listeners = m_iTextField.getEventListenerList().getListenerList();
		for ( int i = 0; i < listeners.length; i++ )
		{
			if ( listeners[i] == CellEditorListener.class )
			{
				if (m_iTextField.changeEvent == null)
					m_iTextField.changeEvent = new ChangeEvent(m_iTextField);
				listener = (CellEditorListener) listeners[i + 1];
				listener.editingStopped(m_iTextField.changeEvent);
			}
		}
	}
	private class TextField extends iTextField
    {
    	/**
		 * 
		 */
    	public TextField()
    	{
    		super();
    	}
		private static final long serialVersionUID = 1L;
		protected ChangeEvent changeEvent = null;
		public EventListenerList getEventListenerList()
    	{
    		return listenerList;
    	}
    }
}