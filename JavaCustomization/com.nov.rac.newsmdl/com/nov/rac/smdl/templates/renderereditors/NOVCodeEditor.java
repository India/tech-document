package com.nov.rac.smdl.templates.renderereditors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;

import com.nov.rac.smdl.common.NOVCodeComboAutoComplete;
import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;


public class NOVCodeEditor extends DefaultCellEditor 
{

    private Registry m_smdlAppReg;
    private TCSession m_session;
    private String[] m_issueDateLOVList;
    private HashMap novCodeIssueDate = new HashMap();
    private String[] novCodeList;
    private JTable m_table;
    
    protected EventListenerList listenerList = new EventListenerList();
    protected ChangeEvent changeEvent = new ChangeEvent(this);
    private NOVCodeComboAutoComplete m_codeComboBox;
    
    public NOVCodeEditor(){
        super(new JComboBox());
        
        //m_codeComboBox = (JComboBox) this.getComponent();
        Vector<String> vCodeInfo = new Vector<String>();
        Collections.addAll(vCodeInfo , NOVCodeHelper.getCodeList());
        m_codeComboBox = new NOVCodeComboAutoComplete(vCodeInfo);
        m_smdlAppReg =Registry.getRegistry("com.nov.rac.smdl.common.common");
        m_session= (TCSession) AIFUtility.getDefaultSession();
        
        populateLOVS();
    
        initialiseComboBox();
        m_codeComboBox.setVisible(true);
        m_codeComboBox.setModel(new DefaultComboBoxModel(novCodeList));
        
        m_codeComboBox.addActionListener(new ActionListener() 
        {           
            @Override
            public void actionPerformed(ActionEvent actionevent) 
            {
               
                String sCode = (String) m_codeComboBox.getSelectedItem();
                String novCode=null;
                if(sCode != null && ! sCode.isEmpty())
                {
                	if ( sCode.toString().indexOf("=") != -1 )
                	{
                          novCode = sCode.substring(0 , sCode.toString().indexOf("="));              
                	}
                }
                updateColumns(m_table, novCode);
            }
        });
       
    }
    
        
        private void initialiseComboBox() {
            
            for (String string : novCodeList)
            {
                m_codeComboBox.addItem(string);
            }
            
        }

        private void populateLOVS() 
        {
            novCodeList= NOVCodeHelper.getCodeList();
            novCodeIssueDate = NOVCodeHelper.getCodeIssueDate();
            m_issueDateLOVList= NOVCodeHelper.loadIssueDateLOV();
        }
        
        private static final long serialVersionUID = 1L;

        
        @Override
        public Component getTableCellEditorComponent(JTable jtable, Object obj,
                boolean flag, int i, int j) 
        {
            
            m_table=jtable;
            
            
            return  m_codeComboBox;
            
        }

        private void updateColumns(JTable table, Object obj) 
        {
             int selectedRow = table.getSelectedRow();
             
             int codeColumn = table.getColumnModel().getColumnIndex("nov4_novcode");
             int issueDateColumn = table.getColumnModel().getColumnIndex("nov4_contract_date");
             int weeksColumn = table.getColumnModel().getColumnIndex("nov4_weeks");
             
             String strNovCode=(String)obj;
             
             if(novCodeIssueDate.get(strNovCode)!=null)
              {
                  int row = selectedRow;
                  String dateWeeks[]= NOVCodeHelper.getDateWeeks(strNovCode,m_issueDateLOVList);
                  String issueDate = dateWeeks[0];
                  String weeks = dateWeeks[1];
                  
                  table.getModel().setValueAt(strNovCode, row, codeColumn);
                  table.getModel().setValueAt(issueDate, row, issueDateColumn);
                  table.getModel().setValueAt(weeks, row,weeksColumn);
                  
              }
             
            table.repaint();
        }


        public String getIssueDateToSet(String toCompare)
        {
            for(int index=0;index<m_issueDateLOVList.length;index++)
            {
                if(m_issueDateLOVList[index].indexOf(toCompare)!=-1)
                    return m_issueDateLOVList[index];
            }
            return null;
        }
        
        

        @Override
        public void addCellEditorListener(CellEditorListener celleditorlistener) {
            listenerList.add(CellEditorListener.class, celleditorlistener);
        }

        @Override
        public void removeCellEditorListener(
                CellEditorListener celleditorlistener) {
            listenerList.remove(CellEditorListener.class, celleditorlistener);
            
        }
        
        @Override
        public boolean shouldSelectCell(EventObject eventobject) {
            
            return true;
        }

        @Override
        public boolean isCellEditable(EventObject eventobject) {
            
            return true;
        }


        @Override
        public void cancelCellEditing() {
             fireEditingCanceled();
        }

        @Override
        public boolean stopCellEditing() {
            
            fireEditingStopped();
            return true;
        }


        protected void fireEditingStopped() {

            CellEditorListener listener;
            Object[] listeners = listenerList.getListenerList();
            for (int i = 0; i < listeners.length; i++) {
              if (listeners[i] == CellEditorListener.class) {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(changeEvent);
              }
            }
          }

          protected void fireEditingCanceled() {
            CellEditorListener listener;
            Object[] listeners = listenerList.getListenerList();
            for (int i = 0; i < listeners.length; i++) {
              if (listeners[i] == CellEditorListener.class) {
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(changeEvent);
              }
            }
          }


        @Override
        public Object getCellEditorValue() {
            return m_codeComboBox.getSelectedItem();
        }

        
        
        




}
