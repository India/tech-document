package com.nov.rac.smdl.templates.renderereditors;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.teamcenter.rac.util.iTextField;

public class NOVCodeRenderer extends DefaultTableCellRenderer  {
   
	private iTextField m_text = new iTextField("");
	
    public NOVCodeRenderer() 
    { 
    	super(); 
    	m_text.setRequired(true);
    }



	@Override
	public Component getTableCellRendererComponent(JTable jtable, Object obj,
			boolean flag, boolean flag1, int i, int j) {
		
			if ( flag )
	        {
	            setForeground(jtable.getSelectionForeground());
	            super.setBackground(jtable.getSelectionBackground());
	        }
	        else
	        {
	            setForeground(jtable.getForeground());
	            setBackground(jtable.getBackground());
	        }
      
			
			
			if (obj instanceof String)
	       {
			//TCDECREL3386 changes for displaying nov code + description	
			/* String s = (String)obj;
		        String[] codeList = NOVCodeHelper.getCodeList();
		        for (int inx = 0; inx < codeList.length; inx++)
		        {
		        	String sNovCode = codeList[inx].substring(0, codeList[inx].indexOf('='));
		        	if(sNovCode.compareToIgnoreCase(s) == 0)
		        	{
		        		s = codeList[inx];
		        		break;
		        	}  	
				}*/
		        
			   if(((String) obj).trim().length()==0)
			   {
				   m_text.setText("");
			   }
			   else if ( obj.toString().indexOf("=") != -1 )
	           {
		           	String novCode = ((String) obj).substring(0,obj.toString().indexOf("="));
		           	m_text.setText(novCode);
	           }
			   else
			   {
				   m_text.setText(obj.toString());
			   }
	       }
		
		return m_text;
	}

	
    
    
    
}