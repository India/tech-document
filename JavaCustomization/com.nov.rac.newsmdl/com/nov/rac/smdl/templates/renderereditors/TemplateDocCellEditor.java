/*================================================================================
                             Copyright (c) 2009 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVTemplateDocCellEditor
 Package Name: com.nov.iman.smdl.commands.CodeTemplateEditor
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         2009/03/25    HarshadaM     File added in SVN
   1.1         2009/03/25    HarshadaM     File modified for custom table
 ================================================================================*/

package com.nov.rac.smdl.templates.renderereditors;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.EventObject;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.nov.rac.smdl.templates.IValueSelector;
import com.nov.rac.smdl.templates.listeners.DocumentSearchListener;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class TemplateDocCellEditor implements IValueSelector, TableCellEditor, TableCellRenderer
{
  
    private JTable m_table;
	public TemplateDocCellEditor( )
    {
    	mRegistry= Registry.getRegistry("com.nov.rac.smdl.common.common");
    	session = (TCSession)AIFUtility.getDefaultSession();
        createUI();

    }

	private void createUI() 
	{
		m_EditorPanel = new JPanel();
        m_EditorPanel.setLayout(new BoxLayout(m_EditorPanel , BoxLayout.LINE_AXIS));
        m_Text = new CustomJLabel();
        m_Text.setMinimumSize(new Dimension(80 , 20));
        
        m_DisplayText  = new CustomJLabel();
        m_Text.setMinimumSize(new Dimension(80 , 20));

        try
        {
        	searchIcon = mRegistry.getImageIcon("searchButton.ICON");
        }
        catch ( Exception ie )
        {
            ie.printStackTrace();
        }

        searchDocBtn = new JButton(searchIcon);
        searchDocBtn.setActionCommand("SearchDoc");
        searchDocBtn.setMinimumSize(new Dimension(18 , 20));
        searchDocBtn.setPreferredSize(new Dimension(18 , 20));
        searchDocBtn.setMaximumSize(new Dimension(18 , 20));
        searchDocBtn.setBackground(Color.WHITE);
        searchDocBtn.addActionListener(new DocumentSearchListener(this));
        
        m_EditorPanel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_EditorPanel.add(searchDocBtn);
        m_EditorPanel.add(m_Text);
        
        m_RendererPanel = new JPanel();
        m_RendererPanel.setLayout(new BoxLayout(m_RendererPanel , BoxLayout.LINE_AXIS));
        searchDisplayBtn = new JButton(searchIcon);
        searchDisplayBtn.setMinimumSize(new Dimension(18 , 20));
        searchDisplayBtn.setPreferredSize(new Dimension(18 , 20));
        searchDisplayBtn.setMaximumSize(new Dimension(18 , 20));
        searchDisplayBtn.setBackground(Color.WHITE);
        
        m_RendererPanel.add(Box.createRigidArea(new Dimension(0 , 5)));
        m_RendererPanel.add(searchDisplayBtn);
        m_RendererPanel.add(m_DisplayText);
        
	}


    protected Icon getDisplayIcon(TCComponent comp )
    {
        if ( comp != null )
        {
            ImageIcon compIcon;
            String strCompType = comp.getType();
            compIcon = TCTypeRenderer.getTypeIcon(strCompType, null);
            return compIcon;
        }
        else
            return null;
    }


    protected void setValueIcon(Object arg0 , Icon arg1 )
    {
     
        ImageIcon docIcon = mRegistry.getImageIcon(("documents.ICON"));
        if ( arg0 != null )
        {
            m_Text.setText(arg0.toString());
            m_Text.setIcon(docIcon);
        }
    }


    public Component getTableCellEditorComponent(JTable table , Object value ,
            boolean isSelected , int row , int column )
    {
    	m_table= table;
        TCComponent imancomponent = null;
        if ( table instanceof TCTable )
        {
   
           if (value != null && value instanceof TCComponent)
            {
            	imancomponent= (TCComponent) value;
            }
        }
        Icon icon = getDisplayIcon(imancomponent);
      
		String s = imancomponent != null ? imancomponent.toString() : "";
      
        m_Text.setText(s != null ? s : "");
		m_Text.putClientProperty(s, imancomponent);
        m_Text.setIcon(icon);
  
        m_EditorPanel.setForeground(table.getSelectionForeground());
        m_EditorPanel.setBackground(table.getSelectionBackground());
        
        return m_EditorPanel;
    }

    
    @Override
	public Component getTableCellRendererComponent(JTable jtable, Object obj,
			boolean flag, boolean flag1, int i, int j) {
		
    
    	  TCComponent imancomponent = null;
          if ( obj != null)
          {
     
             if (obj instanceof TCComponent)
              {
              	imancomponent= (TCComponent) obj;
              }
          }
          Icon icon = getDisplayIcon(imancomponent);
        
          String s = imancomponent != null ? imancomponent.toString() :
           "";
          searchDisplayBtn.setForeground(new JButton().getForeground());
          m_DisplayText.setText(s != null ? s : "");
          m_DisplayText.setIcon(icon);
       
          m_RendererPanel.revalidate();
          m_RendererPanel.repaint();
          
          return m_RendererPanel;
	}
 
    
    public void setValue(TCComponent component)
    {
		
		m_table.setValueAt(component,m_table.getSelectedRow(),m_table.getSelectedColumn());
		String s = component != null ? component.toString() : "";

		m_Text.setText(s != null ? s : "");
		m_Text.putClientProperty(s, component);
		fireEditingStopped();
	}
    
    public Object getCellEditorValue()
    {
    	return m_Text.getClientProperty(m_Text.getText());
    }

    
    public boolean isCellEditable(EventObject anEvent )
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject anEvent )
    {
    	m_EditorPanel.repaint();
        return true;
    }

    public boolean stopCellEditing()
    {
    	fireEditingStopped();
        return true;
    }

    public void cancelCellEditing()
    {
        fireEditingCanceled();
    }

    private void fireEditingCanceled()
    {
        CellEditorListener listener;
       
        Object[] listeners = m_Text.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
            	if (m_Text.changeEvent == null)
            		m_Text.changeEvent = new ChangeEvent(m_Text);
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingCanceled(m_Text.changeEvent);
            }
        }
    }

    protected void fireEditingStopped()
    {
    	m_Text.repaint();
    	
        CellEditorListener listener;
        Object[] listeners = m_Text.getEventListenerList().getListenerList();
        for ( int i = 0; i < listeners.length; i++ )
        {
            if ( listeners[i] == CellEditorListener.class )
            {
            	if (m_Text.changeEvent == null)
            		m_Text.changeEvent = new ChangeEvent(m_Text);
                listener = (CellEditorListener) listeners[i + 1];
                listener.editingStopped(m_Text.changeEvent);
            }
        }
    }
    
    public void addCellEditorListener(CellEditorListener listener )
    {
    	m_Text.getEventListenerList().add(CellEditorListener.class, listener);
    }

    public void removeCellEditorListener(CellEditorListener listener )
    {
    	m_Text.getEventListenerList().remove(CellEditorListener.class, listener);
    }
    public CellEditorListener[] getCellEditorListeners() 
    {
        return (CellEditorListener[])m_Text.getEventListenerList().getListeners(CellEditorListener.class);
    }

    
       
    private class CustomJLabel extends JLabel
    {
    
		private static final long serialVersionUID = 1L;
		protected ChangeEvent changeEvent = null;
		public CustomJLabel()
		{
			super();
		}
		public EventListenerList getEventListenerList()
    	{
    		return listenerList;
    	}
    	
    }

	private JPanel m_EditorPanel;
	private JPanel m_RendererPanel;
	private CustomJLabel m_Text;
	private CustomJLabel m_DisplayText;
	private JButton searchDocBtn;
	private JButton searchDisplayBtn;
	private ImageIcon searchIcon = null;
	private TCComponent m_selectedComponent= null;

	private static final long serialVersionUID = 1L;
    Registry mRegistry = null;
	private TCSession session;
	
	

}
