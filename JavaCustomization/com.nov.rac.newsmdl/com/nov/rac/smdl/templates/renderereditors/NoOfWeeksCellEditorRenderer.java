package com.nov.rac.smdl.templates.renderereditors;

import com.nov.rac.smdl.common.renderereditor.NumberCellEditorRenderer;

public class NoOfWeeksCellEditorRenderer extends NumberCellEditorRenderer
{

	public NoOfWeeksCellEditorRenderer()
	{
		super();
	    m_iTextField.setRequired(true);
	}

}
