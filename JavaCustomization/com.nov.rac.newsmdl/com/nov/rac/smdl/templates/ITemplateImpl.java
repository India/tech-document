package com.nov.rac.smdl.templates;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.util.Registry;

public abstract class ITemplateImpl extends Composite implements ITemplate
{
	protected TemplateNameCombo m_templateNames = null;
	protected Registry m_registry = null;
	
	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();	

	public ITemplateImpl( Composite parent, int style )
	{
		super( parent, style );
		
		m_registry  = Registry.getRegistry( "com.nov.rac.smdl.templates.templates" );
	}
	
	abstract public CreateInObjectHelper[] populateOperationInput( CreateInObjectHelper[] createInObjectHelpers );
	

	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
	}

	@Override
	public void setChangedProperty()
	{
		if( m_templateNames != null )
		{
			int selectionIndex= m_templateNames.getSelectionIndex();
			if( selectionIndex!= -1 )
			{
				Object newValue = m_templateNames.getItem(selectionIndex);
				firePropertyChange( m_templateNames, "templateName","" , newValue );
			}
		}
	}

	abstract public String validateData();
	
	
	protected void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}
	
	protected boolean systemContainsName(String name)
	{
		boolean contains = false;
		if(name!=null)
		{
			String[] systemTemplateNames= null;
			if(m_templateNames!=null)
			{
				systemTemplateNames=m_templateNames.getItems();
			}
			
			for(int index=0;index<systemTemplateNames.length;index++)
			{
				if(name.equalsIgnoreCase(systemTemplateNames[index]))
				{
					contains=true;
					break;
				}
			}
		}
		return contains;
	}
}
