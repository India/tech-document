package com.nov.rac.smdl.templates;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.smdl.templates.dialogs.SMDLTemplateDialog;
import com.teamcenter.rac.util.Registry;

public class SMDLTemplateHandler extends AbstractHandler 
{

	private Registry m_registry = null;
	
	public SMDLTemplateHandler()
	{
		super();
		m_registry=Registry.getRegistry("com.nov.rac.smdl.templates.templates");
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		
		
		
		String operationType;

		final IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		operationType = event.getCommand().getId();

		SMDLTemplateDialog templateDialog = new SMDLTemplateDialog(window
				.getShell());
		templateDialog.setOperationType(operationType);
		templateDialog.create();
		templateDialog.open();
		
		
	
		return null;
	}

}
