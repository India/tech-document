package com.nov.rac.smdl.templates.savesmdlas;

import java.util.Vector;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.smdl.templates.TemplateTablePanel;
import com.nov.rac.smdl.utilities.NOVCodeHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;

public class SaveSMDLAsTemplateTablePanel extends TemplateTablePanel {

	public SaveSMDLAsTemplateTablePanel(Composite parent, int style)
	{
		super(parent, style);
	}

	@Override
	public void setTargetObject(TCComponent targetObject) {
		
		super.setTargetObject(targetObject);
		
		populateSOAComponent();
	}

	@Override
	protected void addTableRows(TCComponent component) 
	{
		
		m_smdlTable.removeAllRows();
		
	       if (component != null)
	       {
	       	try
	       	{
	       		TCComponent[] dataObjs = null;
	       
	       			dataObjs = component.getTCProperty(NOV4ComponentSMDL.SMDL_CODES).getReferenceValueArray();
	       			
					
					for(int iCount = 0;iCount < dataObjs.length; iCount++ )
					{
						Vector<Object> structureDataAttributes = new Vector<Object>();
						String novCode = (String) dataObjs[iCount].getTCProperty(NOV4ComponentSMDL.SMDL_CODE_NUMBER).getPropertyValue();
						structureDataAttributes.addElement(novCode);
						structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentSMDL.ADDITIONAL_CODE).getPropertyValue());
					
						TCComponent smdlDoc = dataObjs[iCount].getTCProperty(NOV4ComponentSMDL.DOCUMENT_REF).getReferenceValue();
						if(smdlDoc != null){
							structureDataAttributes.addElement(smdlDoc);
						}
						else
							structureDataAttributes.addElement("");
						
						
						structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentSMDL.COMMENTS).getPropertyValue());
						
						String[] dateweeks= NOVCodeHelper.getDateWeeks(novCode, null);
					/*	structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_DOC_CONTRACT_DATE).getPropertyValue());
						structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_WEEKS).getPropertyValue());*/
						structureDataAttributes.addElement(dateweeks[0]);
						structureDataAttributes.addElement(dateweeks[1]);
						
						structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentSMDL.LINE_ITEM_NUMBER).getPropertyValue());
						structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentSMDL.CODEFORM_TAB).getPropertyValue());
						
									
						m_smdlTable.addRow((Object)structureDataAttributes);
					}
					
				} catch (TCException e) {
					
					e.printStackTrace();
				}
	       }
		
	}
	
}
