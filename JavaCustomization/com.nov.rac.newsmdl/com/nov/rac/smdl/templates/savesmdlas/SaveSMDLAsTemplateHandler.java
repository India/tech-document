package com.nov.rac.smdl.templates.savesmdlas;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.kernel.NOV4ComponentSMDL;
import com.nov.rac.kernel.NOV4ComponentSMDLRevision;
import com.nov.rac.smdl.templates.dialogs.SMDLTemplateDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SaveSMDLAsTemplateHandler extends AbstractHandler 
{

	private Registry m_registry = null;
	
	public SaveSMDLAsTemplateHandler()
	{
		super();
		m_registry=Registry.getRegistry("com.nov.rac.smdl.templates.templates");
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		
		String operationType;

		final IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		operationType = event.getCommand().getId();

		TCComponent item = (TCComponent) AIFUtility.getTargetComponent();
		if (item == null || !(item instanceof NOV4ComponentSMDL) && !(item instanceof NOV4ComponentSMDLRevision)) {
			MessageBox.post(m_registry.getString("NO_SMDL_Selected.ERROR"),
					m_registry.getString("Error.TITLE"), MessageBox.ERROR);

			return null;
		}

		SMDLTemplateDialog templateDialog = new SMDLTemplateDialog(window
				.getShell());
		templateDialog.setOperationType(operationType);
	
		templateDialog.setTargetObject(((TCComponent)item));
		
		templateDialog.create();
		templateDialog.open();
	
		return null;
	}

}