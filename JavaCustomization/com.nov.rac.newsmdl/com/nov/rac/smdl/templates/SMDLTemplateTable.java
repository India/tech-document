package com.nov.rac.smdl.templates;

import java.util.Arrays;

import javax.swing.ToolTipManager;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class SMDLTemplateTable extends NOVSMDLTable   {

	private Registry m_Registry;
	
	

	public SMDLTemplateTable(TCSession session, String[] columns) 
	{
		super(session, columns);
		m_Registry=Registry.getRegistry("com.nov.rac.smdl.templates.templates");
		
		assignCellRendererEditor();
		
		this.getTableHeader().setReorderingAllowed(false);
		setAutoResizeMode(AUTO_RESIZE_LAST_COLUMN);
	}
	
	
	@Override
	protected void setModel(TCSession session, String[] columns) {
		
		if(session != null)
        {
			String strDisplayToken = getDisplayNamesToken();
			
            String as1[][] = getDisplayableHeaderValue(session, columns, strDisplayToken);
            dataModel = new SMDLTemplateTableModel(as1);
        }
        else
        {
            dataModel = new SMDLTemplateTableModel(columns);
        }
        
        setModel(dataModel);
        
	}



	public void setColumnWidth(String[] columnWidths)
	{
		int width=0;
		for(int i=0;i<columnWidths.length;i++)
		{
			TableColumn col = this.getColumnModel().getColumn(i);
			width = Integer.parseInt(columnWidths[i]);
			col.setPreferredWidth(width);
		//	col.setPreferredWidth(SWTUIHelper.convertHorizontalDLUsToPixels(new Control(), width));
		}
	}

	private void assignCellRendererEditor() 
	{
		ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
       // String s = r.getString("default.RENDERER");      
        
        for(int j = 0; j < i; j++)
        {
           	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".RENDEREREDITOR").toString(), null);
           
           	if(s1!=null)
           	{
	            try
	            {
	            	Object tableCellRendererEditor = Instancer.newInstance(s1);
	            	if(tableCellRendererEditor != null)
	            	{
	            		if(tableCellRendererEditor instanceof TableCellRenderer)
	            		{
	            			getColumnModel().getColumn(j).setCellRenderer((TableCellRenderer)tableCellRendererEditor);
	            		}
	            		if(tableCellRendererEditor instanceof TableCellEditor)
	            		{
	            			getColumnModel().getColumn(j).setCellEditor((TableCellEditor)tableCellRendererEditor);
	            		}
	            	}
	            }
	            catch(Exception exception)
	            {
	                
	            }
           	}
        }
        
        
        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
		
	}

	
	@Override
	public boolean isCellEditable(int row, int col) {
		
		
		String columnName =getColumnPropertyName(col);
      
        String[] editableColumns=m_Registry.getStringArray("editableColumns.Name");
        if (Arrays.asList(editableColumns).contains(columnName) ) 
        {
            return true;
        }
        else
            return false;
		
		
	}


	/*@Override
	protected void resizeAndRepaint() {
		// TODO Auto-generated method stub
		super.resizeAndRepaint();
	}
	*/
	
	
}
