package com.nov.rac.smdl.templates;

import java.util.Hashtable;

import com.nov.rac.smdl.common.NOVSMDLTableLine;
import com.teamcenter.rac.common.TCTableLine;

//public class SMDLTemplateTableLine extends TCTableLine
public class SMDLTemplateTableLine extends NOVSMDLTableLine
{

	public SMDLTemplateTableLine(Hashtable<String, Object> hashtable) {
		super(hashtable);
	}

	public Object getProperty(String s)
	{
        Object obj = null;        
        if(s != null)
        {
           obj = propertyValues.get(s);
        }       
        
        if(obj!= null && obj.equals(""))
        {
        	obj=null;
        }
        return obj;
    }
	
}
