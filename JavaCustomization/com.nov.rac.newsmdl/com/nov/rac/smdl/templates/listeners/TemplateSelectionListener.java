package com.nov.rac.smdl.templates.listeners;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.rac.smdl.templates.ITemplate;

public class TemplateSelectionListener implements SelectionListener
{
	ITemplate m_template = null;
	
	public TemplateSelectionListener( ITemplate _template )
	{
		super();
		
		m_template = _template;
	}
	
	public void widgetDefaultSelected( SelectionEvent arg0 )
	{
	}

	public void widgetSelected( SelectionEvent arg0 )
	{
		m_template.setChangedProperty();
	}
}
