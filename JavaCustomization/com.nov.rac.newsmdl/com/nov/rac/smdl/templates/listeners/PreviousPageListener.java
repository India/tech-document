package com.nov.rac.smdl.templates.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;

public class PreviousPageListener implements ActionListener {

	
	private NovSearchDataModel m_novSearchDataModel;
	
	public PreviousPageListener( NovSearchDataModel novSearchDataModel )
	{
		super();

		m_novSearchDataModel = novSearchDataModel; 
	}
	@Override
	public void actionPerformed(ActionEvent actionevent) {
		
		execute();
	}

	
	private void execute()
	{
		m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_PREV );
		INOVResultSet searchResult = m_novSearchDataModel.getResultSet();
		m_novSearchDataModel.setResultSet( searchResult );
	}
	
}
