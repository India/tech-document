package com.nov.rac.smdl.templates.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.smdl.templates.IValueSelector;
import com.nov.rac.smdl.templates.dialogs.DocumentSearchDialog;

public class DocumentSearchListener implements ActionListener {
	IValueSelector m_Container;

	public DocumentSearchListener(IValueSelector container) {
		m_Container = container;
	}

	@Override
	public void actionPerformed(ActionEvent actionevent) {

		if (actionevent.getSource() instanceof JButton) {

			if (actionevent.getActionCommand().equalsIgnoreCase("SearchDoc")) 
			{
				final JButton searchDocBtn = (JButton) actionevent.getSource();
				/*int x = searchDocBtn.getLocationOnScreen().x;
				int y = searchDocBtn.getLocationOnScreen().y;*/
						
				
				Display.getDefault().asyncExec(new Runnable() {

					public void run()

					{
						DocumentSearchDialog docSearchDialog = new DocumentSearchDialog(
								PlatformUI.getWorkbench().getDisplay().getActiveShell(), SWT.DIALOG_TRIM  | SWT.RESIZE | SWT.APPLICATION_MODAL,m_Container);
						
						docSearchDialog.create();
						int dialogHeight= docSearchDialog.getShell().getBounds().height;
						int x = searchDocBtn.getLocationOnScreen().x;
						int y = searchDocBtn.getLocationOnScreen().y - dialogHeight;
						docSearchDialog.getShell().setLocation(x, y);
					
					
						docSearchDialog.open();
						
						
					}
				});
			
			}
		}

	}


}
