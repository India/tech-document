package com.nov.rac.smdl.templates.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.NovSearchDataModel.DataModelChangeReason;

public class NextPageListener implements ActionListener {

	private INOVSearchProvider m_searchProvider;
	
	private NovSearchDataModel m_novSearchDataModel;
	
	public NextPageListener( INOVSearchProvider searchProvider, NovSearchDataModel novSearchDataModel )
	{
		super();

		m_searchProvider = searchProvider;
		m_novSearchDataModel = novSearchDataModel; 
	}
	

	@Override
	public void actionPerformed(ActionEvent actionevent) {
		
		execute();
	}
		
	
	private void execute()
	{
		int iOldCurrentIndex = m_novSearchDataModel.getResponseIndex();
		
		if( ( iOldCurrentIndex + 1 ) >= m_novSearchDataModel.getResultSet().getRows() )
		{
			INOVSearchResult searchResult = null;
			try
			{
				if( m_searchProvider != null )
				{
					searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, iOldCurrentIndex );
				}
			}
			catch( Exception exception ) 
			{
				exception.printStackTrace();
			}
			INOVResultSet theResultSet = searchResult.getResultSet();

			m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_NEXT );
			m_novSearchDataModel.addResultSet( theResultSet );
		}
		else
		{
			m_novSearchDataModel.setReasonForChange( DataModelChangeReason.NOV_NEXT );
			INOVResultSet searchResult = m_novSearchDataModel.getResultSet();
			m_novSearchDataModel.setResultSet( searchResult );
		}
	}
}
