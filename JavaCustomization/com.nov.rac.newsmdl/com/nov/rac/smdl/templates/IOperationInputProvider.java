package com.nov.rac.smdl.templates;

import java.awt.Composite;

import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;

public interface IOperationInputProvider  
{
    public CreateInObjectHelper[] populateOperationInput(CreateInObjectHelper[] helper);
    
	public TCComponent getComponent();
}
