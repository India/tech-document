package com.nov.rac.smdl.templates;

import org.eclipse.swt.widgets.Composite;


public class SimpleSelectionTemplateTablePanel extends TemplateTablePanel {

	public SimpleSelectionTemplateTablePanel(Composite parent, int style) {
		super(parent, style);
		disableTableEdit();
	}

	private void disableTableEdit() 
	{
		m_smdlTable.setEditable(false);
		m_smdlTable.setEnabled(false);
	}

	@Override
	protected void addListeners() {
		
		
	}

	@Override
	protected void createButtonPanel() {
		
		
	}
	
	
	

}
