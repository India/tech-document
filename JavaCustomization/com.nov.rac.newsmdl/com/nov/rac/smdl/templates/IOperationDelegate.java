package com.nov.rac.smdl.templates;

import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;

public interface IOperationDelegate 
{
	public boolean executeOperation();
	public CreateInObjectHelper[] getOperationInput();
	public void addOperationInputProvider(String name, IOperationInputProvider provider);
	
}
