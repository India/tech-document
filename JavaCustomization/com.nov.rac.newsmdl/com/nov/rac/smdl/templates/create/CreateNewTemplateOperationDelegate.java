package com.nov.rac.smdl.templates.create;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.kernel.NOV4ComponentTemplateRevision;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.templates.AbstractOperationDelegate;
import com.nov.rac.smdl.templates.IOperationInputProvider;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.MessageBox;

public class CreateNewTemplateOperationDelegate extends AbstractOperationDelegate
{
	
	public CreateNewTemplateOperationDelegate(Shell theShell)
	{
		super(theShell);
	}
	
	@Override
	public boolean executeOperation() 
	{
     	ProgressMonitorDialog  templateDialog = new ProgressMonitorDialog( m_shell );

    	CreateInObjectHelper[] inputHelpers = getOperationInput();
		CreateOrUpdateOperation templateOp = new CreateOrUpdateOperation( inputHelpers );
    			
		try 
		{
			templateDialog.run(true, false, templateOp);
			
			processOperationOutput(inputHelpers);
		} catch (Exception e1) 
		{
			e1.printStackTrace();
		}
		
		return true;
	}


	@Override
	public CreateInObjectHelper[] getOperationInput() 
	{	
		ArrayList<CreateInObjectHelper> transferObjects = new ArrayList<CreateInObjectHelper>();
		
		
		CreateInObjectHelper templateObject = new CreateInObjectHelper( NOV4ComponentTemplate.TEMPLATE_OBJECT_TYPE_NAME,
                                                                          CreateInObjectHelper.OPERATION_CREATE);

       // Get the IOperationInputProvider for a given name. (eg ITemplate)
		IOperationInputProvider upperPanel = m_oprInputProviders.get(TemplateConstants.TEMPLATE_PANEL);

		CreateInObjectHelper[] helperInput = new CreateInObjectHelper[]{templateObject};
		upperPanel.populateOperationInput(helperInput);

		CreateInObjectHelper templateRevObject = new CreateInObjectHelper( NOV4ComponentTemplateRevision.TEMPLATE_REVISION_TYPE_NAME,
                                                                             CreateInObjectHelper.OPERATION_CREATE);
		templateObject.setCompoundCreateInput("revision", templateRevObject);
		
		//add template obj to arraylist 
		transferObjects.add(helperInput[0]);
		
       // Get the IOperationInputProvider for a given name. (eg Table Panel)
        IOperationInputProvider tablePanel = m_oprInputProviders.get(TemplateConstants.TEMPLATE_TABLE_PANEL);
        
        CreateInObjectHelper[] structureDatas= tablePanel.populateOperationInput(null);	
        transferObjects.addAll(transferObjects.size(), Arrays.asList(structureDatas));

        CreateInObjectHelper[] inputs = new CreateInObjectHelper[transferObjects.size()];
        inputs=(CreateInObjectHelper[]) transferObjects.toArray(inputs);
        
        return inputs;
	}
	

	private void processOperationOutput(CreateInObjectHelper[] inputHelpers) 
	{
		TCComponent[] createdComp = CreateObjectsSOAHelper.getCreatedOrUpdatedObjects(inputHelpers[0]);
		TCComponentItemRevision templateRevision = null;
		int iCompLength = createdComp.length;
		
		for(int indx=0;indx<iCompLength;indx++)
		{
			if(createdComp[indx] instanceof TCComponentItemRevision)
			{
				templateRevision = (TCComponentItemRevision)createdComp[indx];
				break;
			}
		}
		
		// Get all structure data objects from inputHelpers 
		TCComponent[] structureDatas = new TCComponent[ inputHelpers.length - 1];  
		
		for(int inx=0; inx < inputHelpers.length - 1; inx++)
		{
			structureDatas[inx] = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(inputHelpers[ inx + 1]);
		}
		
		// Get the Rev and add structure data to nov4_smdl_structure_data property.
		
		CreateInObjectHelper templateRevUpdateObj = new CreateInObjectHelper(NOV4ComponentTemplateRevision.TEMPLATE_REVISION_TYPE_NAME,CreateInObjectHelper.OPERATION_UPDATE);
		
		templateRevUpdateObj.setTargetObject(templateRevision);
		
		templateRevUpdateObj.setTagArrayProps(NOV4ComponentTemplateRevision.TEMPLATE_STRUCTURE_DATA, structureDatas, CreateInObjectHelper.SET_PROPERTY);
		
		CreateOrUpdateOperation updateSMDLOp = new CreateOrUpdateOperation(templateRevUpdateObj);
		ProgressMonitorDialog  templateCreationDialog = new ProgressMonitorDialog( m_shell );
		try 
		{
			templateCreationDialog.run(true, false,updateSMDLOp);		        	
		} 
		catch ( Exception e1 ) 
		{
			e1.printStackTrace();
			MessageBox.post("template structure data NOT created: " + e1.getMessage(), null, MessageBox.ERROR);
		}
				
	}

}
