package com.nov.rac.smdl.templates.create;

import java.beans.PropertyChangeEvent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.smdl.common.NOVAbstractNameValidator;
import com.nov.rac.smdl.common.NOVArrayHelper;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.smdl.templates.TemplateImpl;
import com.nov.rac.smdl.templates.TemplateNameCombo;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.kernel.TCComponent;


public class CreateTemplatePanel extends TemplateImpl 
{
	private CreateTemplatePanel m_createTemplatePanel;
	private Label m_nameLabel;
	private Label m_descLabel;
	private Text m_tempDescTextArea;
	protected String m_templateName;
	private Label m_errorMsgDummyLabel;
	private Label m_errorMsgLabel;

	public CreateTemplatePanel( Composite parent, int style )
	{
		super(parent, style);
		createUI(this);
	}

	private void createUI(CreateTemplatePanel createTemplatePanel) 
	{
		m_createTemplatePanel = createTemplatePanel;
		setLayout(new GridLayout(1, false));
		setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1));
		
		Composite panel = new Composite(m_createTemplatePanel,SWT.NONE);
		panel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		panel.setLayout(new GridLayout(2, false));
		
		m_errorMsgDummyLabel = new Label(panel, SWT.NONE);
		m_errorMsgDummyLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		m_errorMsgLabel = new Label(panel, SWT.NONE);
		m_errorMsgLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_nameLabel = new Label(panel, SWT.NONE);
		m_nameLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		m_templateNames = new TemplateNameCombo(panel, SWT.BORDER);
		m_templateNames.setTextLimit(50);
		GridData gd_nameText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_nameText.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_templateNames,140);
		m_templateNames.setLayoutData(gd_nameText);
		
		m_strValToolkit = new StringValidationToolkit(SWT.TOP | SWT.RIGHT,2, true);
		
		final IFieldValidator createComboValidator = new NOVAbstractNameValidator()
		{
						
			public String getErrorMessage() 
			{
				errorMessage =  m_registry.getString("Template_Name_exists.Error");
				return errorMessage;
			}
			 
			@Override
			public boolean isValid(String contents) {
				valid= validateTemplateName(contents);
				processUpdate(valid);
				setErrorMessage(valid); //TCDECREL-3063
				return valid;
			}
			
		};

		final ValidatingField<String> comboField = m_strValToolkit.createField(m_templateNames, createComboValidator, true, "");
		
				
		m_descLabel = new Label(panel, SWT.NONE);
		m_descLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1)); 
		
		m_tempDescTextArea = new Text(panel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		m_tempDescTextArea.setTextLimit(240);
		GridData gd_tempDescTextArea = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tempDescTextArea.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_tempDescTextArea, 30);
		gd_tempDescTextArea.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_tempDescTextArea,140);
		m_tempDescTextArea.setLayoutData( gd_tempDescTextArea);
		
		setLabels();
		
	}




	private void setLabels() 
	{
		m_nameLabel.setText( m_registry.getString("templateName.Label") );
		m_descLabel.setText( m_registry.getString("templateDesc.Label") );
		
		m_errorMsgLabel.setText(m_registry.getString("Template_Name_exists.Error"));
		m_errorMsgLabel.setForeground(m_errorMsgLabel.getDisplay().getSystemColor(SWT.COLOR_RED));
	}
	
	
	protected boolean validateTemplateName(String name) 
	{
			
		boolean valid= false;
		
			if(	! NOVArrayHelper.searchInArray(name, m_templateNames.getItems()) && name.trim().length()>0)
			{
				valid= true;
			}
		
		return valid;	
	}
	
	protected void processUpdate(boolean valid)
	{
		if (valid) 
		{
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , true);
		}
		else
		{
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , false);
		}
	}
	
	
	public String validateData()
	{
		return "";	
	}
	
	@Override
	
	public CreateInObjectHelper[] populateOperationInput(CreateInObjectHelper[] transferObjects) {
		
			String templateName = m_templateNames.getText();
			if(templateName!= null)
			{
				transferObjects[0].setStringProps(NOV4ComponentTemplate.TEMPLATE_NAME,templateName, CreateInObjectHelper.SET_CREATE_IN);			
			}
			
			if(m_tempDescTextArea.getText()!=null)
			{
				transferObjects[0].setStringProps(NOV4ComponentTemplate.TEMPLATE_DESC,m_tempDescTextArea.getText(),CreateInObjectHelper.SET_CREATE_IN);
			}
		
		return transferObjects;
	}

	@Override
	public TCComponent getComponent()
	{
		return null;
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private void setErrorMessage(boolean isValid)
	{
		if(m_errorMsgLabel != null)
		{
			if (!isValid && !m_templateNames.getText().trim().isEmpty()) 
			{
				m_errorMsgLabel.setVisible(true);
			}
			else
			{
				m_errorMsgLabel.setVisible(false);
			}
		}
	}
}
