package com.nov.rac.smdl.templates;

import java.util.Hashtable;

import com.nov.rac.smdl.common.NOVSMDLTableModel;

public class SMDLTemplateTableModel extends NOVSMDLTableModel 
{

	private static final long serialVersionUID = 1L;

	public SMDLTemplateTableModel() {
		super();
	}

	public SMDLTemplateTableModel(String as[][]) {
		super(as);
	}

	public SMDLTemplateTableModel(String as[]) {
		super(as);
	}
	
	public SMDLTemplateTableLine createLine(Object obj)
	{
		SMDLTemplateTableLine aiftableline = null;
		Object [] strObj = null;

		if(obj instanceof Object[])
		{
			strObj = ((Object [])obj);	
			if(strObj != null)
			{
				int i = strObj.length;
				int j = getColumnCount();
				Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
				for(int k = 0; k < j && k < i; k++)
				{
					if(strObj[k] != null)
					{
						hashtable.put(getColumnIdentifier(k), strObj[k]);
					}
				}       
				aiftableline = new SMDLTemplateTableLine(hashtable);
			}
		}
		else 
		{
			aiftableline = (SMDLTemplateTableLine) super.createLine(obj);
		}
	
		return aiftableline;
	} 

    protected SMDLTemplateTableLine[] createLines(Object obj)
    {
    	SMDLTemplateTableLine aaiftableline[] = null;

    	if( obj instanceof Object [][] )
        {
    		Object [][] objArrArr = (Object [][])obj;    		
            int i = objArrArr.length;
            aaiftableline = new SMDLTemplateTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	SMDLTemplateTableLine newLine = createLine( objArrArr[j] );
                aaiftableline[j] = newLine;
            }
        } 
    	else
    	{
    		aaiftableline = (SMDLTemplateTableLine[]) super.createLines(obj);
    	}  	
    	return aaiftableline;
    }

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		super.setValueAt(arg0, arg1, arg2);
		fireTableCellUpdated(arg1, arg2);
	}

    
    
    
}
