package com.nov.rac.smdl.templates.revise;

import java.beans.PropertyChangeEvent;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.smdl.common.NOVAbstractNameValidator;
import com.nov.rac.smdl.common.NOVArrayHelper;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.smdl.templates.TemplateImpl;
import com.nov.rac.smdl.templates.TemplateNameCombo;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;


public class ReviseTemplatePanel extends TemplateImpl 
{
	private ReviseTemplatePanel m_createTemplatePanel = null;
	private Label m_nameLabel = null;
	private Label m_descLabel = null;
	private Text m_tempDescTextArea = null;
	private TCComponentItem m_templateItem = null;
	protected String m_templateName = null;
	
	
	public ReviseTemplatePanel( Composite parent, int style ) 
	{
		super( parent, style );
		createUI( this );
	}

	private void createUI( ReviseTemplatePanel reviseTemplatePanel )
	{
		m_createTemplatePanel = reviseTemplatePanel;
		setLayout( new GridLayout( 1, false ) );
		setLayoutData( new GridData( SWT.CENTER, SWT.TOP, false, false, 1, 1 ) );
		
		Composite panel = new Composite( m_createTemplatePanel, SWT.NONE );
		panel.setLayoutData( new GridData( SWT.CENTER, SWT.CENTER, false, false, 1, 1 ) );
		panel.setLayout( new GridLayout( 2, false ) );
		
		m_nameLabel = new Label( panel, SWT.NONE );
		m_nameLabel.setLayoutData( new GridData( SWT.RIGHT, SWT.CENTER, false, false, 1, 1 ) );
		
		m_templateNames = new TemplateNameCombo( panel, SWT.NONE );
		
		GridData gd_combo = new GridData( SWT.FILL, SWT.CENTER, true, false, 1, 1 );
		gd_combo.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_templateNames,140);
		m_templateNames.setLayoutData( gd_combo );
		
		m_strValToolkit = new StringValidationToolkit(SWT.TOP | SWT.RIGHT,2, true);
		
		final IFieldValidator reviseComboValidator = new NOVAbstractNameValidator()
		{
						
			public String getErrorMessage() 
			{
				errorMessage = m_registry.getString( "Template_Name_Revise.Error" );
				return errorMessage;
			}
			 
			@Override
			public boolean isValid(String contents) {
				valid= validateTemplateName(contents);
				processUpdate(valid,contents);
				return valid;
			}
			
		};

		final ValidatingField<String> comboField = m_strValToolkit.createField(m_templateNames, reviseComboValidator, true, "");
	
		m_descLabel = new Label( panel, SWT.NONE);
		m_descLabel.setLayoutData (new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1 )  ); 
		
		m_tempDescTextArea = new Text( panel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL );
		m_tempDescTextArea.setTextLimit(240);
		GridData gd_tempDescTextArea = new GridData( SWT.LEFT, SWT.CENTER, false, false, 1, 1 );
		gd_tempDescTextArea.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_tempDescTextArea, 30);
		gd_tempDescTextArea.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_tempDescTextArea,140);
		m_tempDescTextArea.setLayoutData( gd_tempDescTextArea );
		
		setLabels();
		
	}
	
	private void setLabels() 
	{
		m_nameLabel.setText( m_registry.getString( "reviseTemplateName.Label" ) );
		m_descLabel.setText( m_registry.getString( "templateDesc.Label" ) );
	}
	
	
	protected boolean validateTemplateName(String name)
	{
		boolean valid = false;
		

			if(	NOVArrayHelper.searchInArray(name, m_templateNames.getItems()) )
			{
				valid= true;
			}
		
		return valid;
	}
	
	public String validateData()
	{
		return "";
	}
	
	@Override
	public CreateInObjectHelper[] populateOperationInput( CreateInObjectHelper[] transferObjects )
	{
		if( m_templateNames.getSelectionIndex()!= -1 )
		{
			int selectionIndex = m_templateNames.getSelectionIndex();
			String templateName = ( String )m_templateNames.getItem( selectionIndex );
			transferObjects[0].setStringProps( NOV4ComponentTemplate.TEMPLATE_NAME,templateName, CreateInObjectHelper.SET_CREATE_IN );			
		}
		transferObjects[0].setStringProps( NOV4ComponentTemplate.TEMPLATE_DESC, m_tempDescTextArea.getText(), CreateInObjectHelper.SET_CREATE_IN );
		
		return transferObjects;
	}
	
	@Override
	public TCComponent getComponent()
	{
		return m_templateItem;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) 
	{
		if( event.getPropertyName().equals( TemplateConstants.TEMPLATE_NAME_CHANGE ) )
		{
			TCComponentItem templateItem = ( TCComponentItem )event.getNewValue();
			if( templateItem != null )
			{
				try
				{
					m_tempDescTextArea.setText( templateItem.getProperty( 
							NOV4ComponentTemplate.TEMPLATE_DESC ) );
				}
				catch( TCException e )
				{
					e.printStackTrace();
				}
			}
			else
			{
				m_tempDescTextArea.setText("");
			}
		}
		
	}

	private void processUpdate(boolean valid, String name)
	{
		
		if( valid )
		{
			//get the template Component from its name
			m_templateItem = ( TCComponentItem )m_templateNames.getData( name );
			
			firePropertyChange( m_templateNames, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , m_templateItem );
			firePropertyChange( m_templateNames, TemplateConstants.ENABLE_OPERATION, "" , true );
		}
		else
		{
			firePropertyChange( m_templateNames, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , null );
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , false);
		}
	}
	
	
}