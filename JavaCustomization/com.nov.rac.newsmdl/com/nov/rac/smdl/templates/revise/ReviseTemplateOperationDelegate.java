package com.nov.rac.smdl.templates.revise;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.kernel.NOV4ComponentTemplateRevision;
import com.nov.rac.smdl.services.CreateOrUpdateOperation;
import com.nov.rac.smdl.templates.AbstractOperationDelegate;
import com.nov.rac.smdl.templates.IOperationInputProvider;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.smdl.utilities.ReviseHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class ReviseTemplateOperationDelegate extends AbstractOperationDelegate
{
	private TCComponentItemRevision m_newTemplateRev = null;
	private TCComponentItem m_templateItem = null;

	public ReviseTemplateOperationDelegate( Shell theShell )
	{
		super(theShell);
	}
	
	@Override
	public boolean executeOperation() 
	{
		boolean success = true;
     	ProgressMonitorDialog  templateDialog = new ProgressMonitorDialog( m_shell );

     		try 
     		{
				revise();
			
		    	CreateInObjectHelper[] inputHelpers = getOperationInput();
				CreateOrUpdateOperation templateOp = new CreateOrUpdateOperation( inputHelpers );
	    
				templateDialog.run( true, false, templateOp );
				updateData( inputHelpers );
				
			}
     		catch (TCException e) 
     		{
     			success= false;
     			MessageBox.post(e);
     		}
			catch( Exception e1 ) 
			{
				e1.printStackTrace();
			}
     
		return success;
	}

	private void revise() throws TCException
	{
	    // Get the IOperationInputProvider for a given name. (eg ITemplate)
		IOperationInputProvider upperPanel = m_oprInputProviders.get( TemplateConstants.TEMPLATE_PANEL );
		m_templateItem = ( TCComponentItem )upperPanel.getComponent();
	
		ReviseHelper.revise(m_templateItem);
	
		/*try
		{*/
		m_newTemplateRev = m_templateItem.getLatestItemRevision();
		/*}
		catch( TCException e )
		{
			e.printStackTrace();
		}*/
	}
	
	@Override
	public CreateInObjectHelper[] getOperationInput() 
	{	
		ArrayList<CreateInObjectHelper> transferObjects = new ArrayList<CreateInObjectHelper>();

		// Get the IOperationInputProvider for a given name. (eg Table Panel)
        IOperationInputProvider tablePanel = m_oprInputProviders.get( TemplateConstants.TEMPLATE_TABLE_PANEL );
        
        CreateInObjectHelper[] structureDatas = tablePanel.populateOperationInput( null );
        transferObjects.addAll( transferObjects.size(), Arrays.asList( structureDatas ) );

        CreateInObjectHelper[] inputs = new CreateInObjectHelper[transferObjects.size()];
        inputs = ( CreateInObjectHelper[] ) transferObjects.toArray( inputs );
        
        return inputs;
	}

	private void updateData( CreateInObjectHelper[] inputHelpers ) 
	{
		ArrayList<CreateInObjectHelper> transferObjects = new ArrayList<CreateInObjectHelper>();
		
	    // Get the IOperationInputProvider for a given name. (eg ITemplate)
		IOperationInputProvider upperPanel = m_oprInputProviders.get( TemplateConstants.TEMPLATE_PANEL );

		// Updating the Template Object for the description.
		CreateInObjectHelper templateObject = new CreateInObjectHelper( NOV4ComponentTemplate.TEMPLATE_OBJECT_TYPE_NAME,
			CreateInObjectHelper.OPERATION_UPDATE );
		templateObject.setTargetObject( m_templateItem );

		CreateInObjectHelper[] helperInput = new CreateInObjectHelper[]{ templateObject };
		upperPanel.populateOperationInput( helperInput );

		//add template description to arraylist 
		transferObjects.add( helperInput[0] );
		
		// Get all structure data objects from inputHelpers 
		TCComponent[] structureDatas = new TCComponent[inputHelpers.length];  

		for( int inx = 0; inx < inputHelpers.length; ++inx )
		{
			structureDatas[inx] = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject( inputHelpers[inx] );
		}

		// Update the newly created revision with the structure data.
		CreateInObjectHelper templateRevUpdateObj = new CreateInObjectHelper(
				NOV4ComponentTemplateRevision.TEMPLATE_REVISION_TYPE_NAME,
				CreateInObjectHelper.OPERATION_UPDATE );
		templateRevUpdateObj.setTargetObject( m_newTemplateRev );

		templateRevUpdateObj.setTagArrayProps( NOV4ComponentTemplateRevision.TEMPLATE_STRUCTURE_DATA,
				structureDatas, CreateInObjectHelper.SET_PROPERTY );

		transferObjects.add( templateRevUpdateObj );

        CreateInObjectHelper[] inputs = new CreateInObjectHelper[transferObjects.size()];
        inputs = ( CreateInObjectHelper[] ) transferObjects.toArray( inputs );
		
		CreateOrUpdateOperation updateTempRevisionOperation = new CreateOrUpdateOperation( inputs );
		ProgressMonitorDialog  templateCreationDialog = new ProgressMonitorDialog( m_shell );
		try 
		{
			templateCreationDialog.run( true, false, updateTempRevisionOperation );       	
		} 
		catch ( Exception e1 ) 
		{
			e1.printStackTrace();
			MessageBox.post( "template structure data NOT created: " + e1.getMessage(), null, MessageBox.ERROR);
		}
	}
}