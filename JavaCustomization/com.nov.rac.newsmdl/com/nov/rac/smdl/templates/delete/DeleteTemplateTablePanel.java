package com.nov.rac.smdl.templates.delete;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.templates.SimpleSelectionTemplateTablePanel;

public class DeleteTemplateTablePanel extends SimpleSelectionTemplateTablePanel 
{
	public DeleteTemplateTablePanel(Composite parent, int style) 
	{
		super(parent, style);
	}
}
