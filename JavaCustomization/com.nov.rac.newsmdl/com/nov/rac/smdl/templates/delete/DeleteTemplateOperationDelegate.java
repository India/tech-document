package com.nov.rac.smdl.templates.delete;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.templates.AbstractOperationDelegate;
import com.nov.rac.smdl.templates.IOperationInputProvider;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class DeleteTemplateOperationDelegate extends AbstractOperationDelegate 
{

	private TCComponentItem m_templateItem;
	private Registry m_registry = null;
	private boolean m_success = true;

	public DeleteTemplateOperationDelegate(Shell theShell)
	{
		super(theShell);
		m_registry = Registry.getRegistry("com.nov.rac.smdl.templates.templates");
	}
	
	
	@Override
	public boolean executeOperation()
	{
		m_success = true; 
		
		boolean yes = MessageDialog.openQuestion( AIFUtility.getActiveDesktop().getShell(),
				m_registry.getString("delete.TITLE"), m_registry.getString("delete.MSG") );	
		if(!yes)
        {
			m_success = false;
        }
		else
        {
			ProgressMonitorDialog  templateDialog = new ProgressMonitorDialog( m_shell );
			
			IOperationInputProvider upperPanel = m_oprInputProviders.get( TemplateConstants.TEMPLATE_PANEL );
			m_templateItem = ( TCComponentItem )upperPanel.getComponent();
		
		
			IRunnableWithProgress templateDeleteOperation = new IRunnableWithProgress (){
				
			
				@Override
				public void run(IProgressMonitor iprogressmonitor)
						throws InvocationTargetException, InterruptedException 
				{
					try 
					{
						m_templateItem.delete();
					} catch (TCException e)
					{
						m_success =false;
						MessageBox.post(e);
					}
					
				}
				
				
			};
					
			try 
			{
				templateDialog.run(true, false, templateDeleteOperation);
				
				
			} catch (Exception e1) 
			{
				e1.printStackTrace();
			}
        }
		return m_success;
	}

	@Override
	public CreateInObjectHelper[] getOperationInput() {
		
		return null;
	}

}
