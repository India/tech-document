package com.nov.rac.smdl.templates.delete;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.templates.SimpleSelectionTemplatePanel;

public class DeleteTemplatePanel extends SimpleSelectionTemplatePanel
{

	public DeleteTemplatePanel(Composite parent, int style) 
	{
		super(parent, style);
		// TODO Auto-generated constructor stub
	}
	
	protected void setLabels() 
	{
		m_nameLabel.setText( m_registry.getString("deleteTemplateName.Label") );
		m_descLabel.setText( m_registry.getString("templateDesc.Label") );
	}
	
	@Override
	protected String doGetErrorMessage() 
	{
		String errorMessage = m_registry.getString("Template_Name_Delete.Error");
		return errorMessage;
	}
}
