package com.nov.rac.smdl.templates;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.common.NOVArrayHelper;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class TemplateNameComboWithUpdate extends TemplateNameCombo
{

	private ArrayList<PropertyChangeListener> m_listeners = new ArrayList<PropertyChangeListener>();	
	private Registry m_registry = null;
	
	public TemplateNameComboWithUpdate(Composite parent, int style)
	{
		super(parent, style);
		m_registry = Registry.getRegistry( "com.nov.rac.smdl.templates.templates" );
	}

	@Override
	public void setText(String string) 
	{
		super.setText(string);
		//this.setData("SelectionChanged");
		//processUpdate(string);
	}

	private void processUpdate(String string)
	{
		if( validateTemplateName(string) )
		{
			//get the template Component from its name
			TCComponentItem templateItem = ( TCComponentItem )this.getData( string );
					
			firePropertyChange( this, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , templateItem );
			firePropertyChange( this, TemplateConstants.ENABLE_OPERATION, "" , true );
		}
		else
		{
			firePropertyChange( this, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , null );
			firePropertyChange(this, TemplateConstants.ENABLE_OPERATION,"" , false);
		}
	}
	
	
	private boolean validateTemplateName(String name) 
	{
		boolean valid = false;
		/*List<String> names =Arrays.asList( this.getItems());
		if(names.contains(name))*/
		if(NOVArrayHelper.searchInArray(name, this.getItems()))
		{
			valid=true;
		}
		
		else 
		{
			MessageBox.post( m_registry.getString( "Template_Name_Revise.Error" ), 
        		m_registry.getString( "Error.TITLE" ),MessageBox.ERROR );
		}
	
		return valid;
		
	}

	public void registerPropertyChangeListener(PropertyChangeListener listener)
	{
		m_listeners.add( listener );
		
	}
	

	private void firePropertyChange( Object source, String propertyName, Object oldValue, Object newValue )
	{
		PropertyChangeEvent changeEvent = new PropertyChangeEvent( source, propertyName, oldValue, newValue );
		for( PropertyChangeListener listener:m_listeners )
		{
			listener.propertyChange( changeEvent );
		}
	}
}
