package com.nov.rac.smdl.templates.saveas;

import java.beans.PropertyChangeEvent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.smdl.common.NOVAbstractNameValidator;
import com.nov.rac.smdl.common.NOVArrayHelper;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.smdl.templates.TemplateImpl;
import com.nov.rac.smdl.templates.TemplateNameCombo;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class SaveAsSMDLTemplatePanel extends TemplateImpl 
{
	private SaveAsSMDLTemplatePanel m_saveAsSMDLTemplate;
	private Label m_saveAsLabel;
	private Label m_descLabel;
	private Text m_tempDescTextArea;
	private Label m_newNameLabel;
	private Text m_newTemplateName;
	protected String m_templateName;
	protected String m_newTemplateNameValue = null;
	private TCComponentItem m_templateItem;

	public SaveAsSMDLTemplatePanel(Composite parent, int style) 
	{
		super(parent, style);
		createUI(this);
	}

	private void createUI(SaveAsSMDLTemplatePanel saveAsSMDLTemplate)
	{
		m_saveAsSMDLTemplate = saveAsSMDLTemplate;
		setLayout(new GridLayout(1, false));
		setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1));
		
		Composite panel = new Composite(m_saveAsSMDLTemplate,SWT.NONE);
		panel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		panel.setLayout(new GridLayout(2, false));
		
		m_saveAsLabel = new Label(panel, SWT.NONE);
		m_saveAsLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		m_templateNames = new TemplateNameCombo( panel, SWT.NONE );
		GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_combo.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_templateNames,110);
		m_templateNames.setLayoutData(gd_combo);
		
		m_newNameLabel = new Label(panel, SWT.NONE);
		m_newNameLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		m_newTemplateName = new Text(panel, SWT.BORDER);
		m_newTemplateName.setTextLimit(50);
		GridData gd_nameText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_nameText.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_newTemplateName,155);
		m_newTemplateName.setLayoutData(gd_nameText);
		
		m_strValToolkit = new StringValidationToolkit(SWT.TOP | SWT.RIGHT,2, true);
		
		final IFieldValidator saveAsComboValidator = new NOVAbstractNameValidator()
		{
						
			public String getErrorMessage() 
			{
				errorMessage = m_registry.getString("Template_Name_Save_As.Error");
				return errorMessage;
			}
			 
			@Override
			public boolean isValid(String contents) 
			{
				valid= validateSaveAsTemplateName(contents);
				
				boolean newNameValid = validateTemplateName(m_newTemplateName.getText());
				
				processUpdate(valid,newNameValid,contents);
		
				return valid;
			}
			
		};

		final ValidatingField<String> comboField = m_strValToolkit.createField(m_templateNames, saveAsComboValidator, true, "");
		
		
		final IFieldValidator newNameValidator = new NOVAbstractNameValidator()
		{
						
			public String getErrorMessage() 
			{
				errorMessage = m_registry.getString("Template_Name_exists.Error");
				return errorMessage;
			}
			 
			@Override
			public boolean isValid(String contents) 
			{
				valid= validateTemplateName(contents);
				boolean saveAsTemplateNameValid= validateSaveAsTemplateName(m_templateNames.getText());
				processUpdate(saveAsTemplateNameValid,valid,null);
				return valid;
			}
			
		};

		final ValidatingField<String> newNameField = m_strValToolkit.createField(m_newTemplateName, newNameValidator, true, "");
		
		m_descLabel = new Label(panel, SWT.NONE);
		m_descLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1)); 
		
		m_tempDescTextArea = new Text(panel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		m_tempDescTextArea.setTextLimit(240);
		GridData gd_tempDescTextArea = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tempDescTextArea.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_tempDescTextArea, 30);
		gd_tempDescTextArea.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_tempDescTextArea,140);
		m_tempDescTextArea.setLayoutData( gd_tempDescTextArea);
		
		setLabels();
		
	}

	private void setLabels() 
	{
		m_saveAsLabel.setText(m_registry.getString("saveAsTemplateName.Label"));
		m_newNameLabel.setText(m_registry.getString("newTemplateName.Label"));
		m_descLabel.setText(m_registry.getString("templateDesc.Label"));
	}
	
	
	protected void processUpdate(boolean isSaveAsCombovalid,boolean isNewTemplateNamevalid, String name)
	{

		if(name != null )
		{
			if(isSaveAsCombovalid)
			{
				m_templateItem = ( TCComponentItem )m_templateNames.getData( name );
				
				firePropertyChange( m_templateNames, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , m_templateItem );
			}
			else
			{
				firePropertyChange(m_templateNames, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , null);
			}
		}
		
		if (isSaveAsCombovalid && isNewTemplateNamevalid ) 
		{
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , true);
		}
		else
		{
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , false);
		}
	}
	
	
	protected boolean validateSaveAsTemplateName(String name)
	{
		
		boolean valid= false;
		
			if(NOVArrayHelper.searchInArray(name, m_templateNames.getItems()))
			{
				valid= true;
			}
		
		return valid;
	}
	
	protected boolean validateTemplateName(String name)
	{
		
		boolean valid= false;
		
			if(	!NOVArrayHelper.searchInArray(name, m_templateNames.getItems()) && name.trim().length()>0)
			{
				valid= true;
			}
		
		return valid;
	}
	
	
	public String validateData()
	{
		
		StringBuffer  invalidInputs= new StringBuffer();
		if(m_newTemplateName.getText().length()==0 )
		{
			invalidInputs.append(m_registry.getString("newTemplateName.Name"));
			
		}
				
		return invalidInputs.toString();
		
	}
	

	@Override
	public CreateInObjectHelper[] populateOperationInput(CreateInObjectHelper[] transferObjects) {
		
		String templateName = m_newTemplateName.getText();
		if(templateName!=null)
		{
			transferObjects[0].setStringProps( NOV4ComponentTemplate.TEMPLATE_NAME, templateName, CreateInObjectHelper.SET_CREATE_IN );			
		}
		
		if(m_tempDescTextArea.getText()!=null)
		{
			transferObjects[0].setStringProps(NOV4ComponentTemplate.TEMPLATE_DESC,m_tempDescTextArea.getText(),CreateInObjectHelper.SET_CREATE_IN);
		}
		return transferObjects;
	}

	@Override
	public TCComponent getComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) 
	{
		if( event.getPropertyName().equals( TemplateConstants.TEMPLATE_NAME_CHANGE ) )
		{
			TCComponentItem templateItem = ( TCComponentItem )event.getNewValue();
			if( templateItem != null )
			{
				try
				{
					m_tempDescTextArea.setText( templateItem.getProperty( 
							NOV4ComponentTemplate.TEMPLATE_DESC ) );
				}
				catch( TCException e )
				{
					e.printStackTrace();
				}
			}
			else
			{
				m_tempDescTextArea.setText("");
			}
		}
		
	}
	
}

