package com.nov.rac.smdl.templates;

import java.beans.PropertyChangeListener;

public interface ITemplate extends IOperationInputProvider, PropertyChangeListener
{
	//public void populateOperationInput(CreateInObjectHelper createInObjectHelper);
	
	public void registerPropertyChangeListener(PropertyChangeListener listener);
	
	public void setChangedProperty();
	
	public String validateData();
}
