package com.nov.rac.smdl.templates;

import java.beans.PropertyChangeEvent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.smdl.common.NOVAbstractNameValidator;
import com.nov.rac.smdl.common.NOVArrayHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;


public class SimpleSelectionTemplatePanel extends TemplateImpl 
{
	private SimpleSelectionTemplatePanel m_selectionTemplatePanel;
	protected Label m_nameLabel;
	protected Label m_descLabel;
	protected String m_templateName;
	private Text m_tempDescTextArea;
	private TCComponent m_templateItem;

	public SimpleSelectionTemplatePanel( Composite parent, int style )
	{
		super(parent, style);
		createUI(this);
	}

	private void createUI(SimpleSelectionTemplatePanel selectionTemplatePanel) 
	{
		m_selectionTemplatePanel = selectionTemplatePanel;
		setLayout(new GridLayout(1, false));
		setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1));
		
		Composite panel = new Composite(m_selectionTemplatePanel,SWT.NONE);
		panel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		panel.setLayout(new GridLayout(2, false));
		
		m_nameLabel = new Label(panel, SWT.NONE);
		m_nameLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		m_templateNames = new TemplateNameCombo(panel, SWT.BORDER);
		m_templateNames.setTextLimit(50);
		GridData gd_nameText = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_nameText.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_templateNames,140);
		m_templateNames.setLayoutData(gd_nameText);
		
		m_strValToolkit = new StringValidationToolkit(SWT.TOP | SWT.RIGHT,2, true);
		
		final IFieldValidator selectionComboValidator = new NOVAbstractNameValidator()
		{
						
			public String getErrorMessage() 
			{
				return doGetErrorMessage();				
			}
			 
			@Override
			public boolean isValid(String contents) {
				valid= validateTemplateName(contents);
				processUpdate(valid,contents);
				return valid;
			}
			
		};

		final ValidatingField<String> comboField = m_strValToolkit.createField(m_templateNames, selectionComboValidator, true, "");
		
		m_descLabel = new Label(panel, SWT.NONE);
		m_descLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1)); 
		
		m_tempDescTextArea = new Text(panel, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		m_tempDescTextArea.setTextLimit(256);
		GridData gd_tempDescTextArea = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_tempDescTextArea.heightHint= SWTUIHelper.convertVerticalDLUsToPixels(m_tempDescTextArea, 30);
		gd_tempDescTextArea.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_tempDescTextArea,140);
		m_tempDescTextArea.setLayoutData( gd_tempDescTextArea);
		
		setLabels();
		
	}
	
	protected void setLabels() 
	{
		m_nameLabel.setText( m_registry.getString("templateName.Label") );
		m_descLabel.setText( m_registry.getString("templateDesc.Label") );
	}
	protected String doGetErrorMessage()
	{
		return null;
	}
	

	private void processUpdate(boolean valid, String name) 
	{
		// fire property change on listener
		if (valid)
		{
			//get the template Component from its name
			m_templateItem = ( TCComponentItem )m_templateNames.getData( name );
						
			firePropertyChange( m_templateNames, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , m_templateItem );
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , true);
		}
		else
		{
			firePropertyChange( m_templateNames, TemplateConstants.TEMPLATE_NAME_CHANGE,"" , null );
			firePropertyChange(m_templateNames, TemplateConstants.ENABLE_OPERATION,"" , false);
		}
	}
	
	
	protected boolean validateTemplateName(String name) 
	{
			
		boolean valid= false;
		
			if(	NOVArrayHelper.searchInArray(name, m_templateNames.getItems()) && name.trim().length()>0)
			{
				valid= true;
			}
		
		return valid;	
	}
	

	@Override
	public CreateInObjectHelper[] populateOperationInput(
			CreateInObjectHelper[] createInObjectHelpers) {
		
		return null;
	}

	@Override
	public String validateData() {
		
		return "";
	}

	@Override
	public TCComponent getComponent() {
		// TODO Auto-generated method stub
		return m_templateItem;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		if( event.getPropertyName().equals( TemplateConstants.TEMPLATE_NAME_CHANGE ) )
		{
			TCComponentItem templateItem = ( TCComponentItem )event.getNewValue();
			if( templateItem != null )
			{
				try
				{
					m_tempDescTextArea.setText( templateItem.getProperty( 
							NOV4ComponentTemplate.TEMPLATE_DESC ) );
				}
				catch( TCException e )
				{
					e.printStackTrace();
				}
			}
			else
			{
				m_tempDescTextArea.setText("");
			}
		}
	}

	
}
