package com.nov.rac.smdl.templates;

public class TemplateConstants 
{
	public static final String TEMPLATE_NAME_CHANGE = "templateNameChange";
	public static final String ENABLE_OPERATION = "enableOperation";
	public static final String TEMPLATE_PANEL = "templatePanel";
	public static final String TEMPLATE_TABLE_PANEL = "templateTablePanel";
	
}
