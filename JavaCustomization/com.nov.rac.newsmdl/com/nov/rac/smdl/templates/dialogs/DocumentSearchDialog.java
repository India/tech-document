package com.nov.rac.smdl.templates.dialogs;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.BevelBorder;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.smdl.panes.search.NovSearchDataModel;
import com.nov.rac.smdl.panes.search.listener.LoadAllButtonListener;
import com.nov.rac.smdl.panes.search.listener.SMDLKeyBoardEventListener;
import com.nov.rac.smdl.panes.search.listener.SMDLKeyBoardEventLoadAllListener;
import com.nov.rac.smdl.templates.IValueSelector;
import com.nov.rac.utilities.patterns.observer.IObserver;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class DocumentSearchDialog extends /*JDialog*/ /*AbstractAIFDialog*/ AbstractSWTDialog implements INOVSearchProvider2, IObserver
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Text m_docId;
	private Image searchIcon;
	private Registry mRegistry;
	private JScrollPane searchScrollPane = null;
	private TCSession m_Session= null;
	private String m_sItemId;
	private String m_sItemType;
	private Label m_searchStatusLbl;
	private TCComponent m_selectedComp=null;
	private IValueSelector mContainer;
	private Label m_docIDLabel;
	private Button searchDocBtn1;
	private NovSearchDataModel m_searchResults;
	private TCTree documentSearchTree;
	private JButton m_previousButton;
	private JButton m_nextButton;


	public DocumentSearchDialog(Shell arg0, int arg1, IValueSelector container) 
	{
			super(arg0, arg1);
			mRegistry = Registry.getRegistry("com.nov.rac.smdl.templates.templates");
			m_Session= (TCSession)AIFUtility.getDefaultSession();
			mContainer= container;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) 
	{
		setTitle();
		
		Composite dialogcomposite = new Composite(parent, SWT.NONE);
		
		GridLayout gd = new GridLayout(1, false);
		dialogcomposite.setLayout(gd);
		dialogcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite searchPanel = new Composite(dialogcomposite, SWT.NONE);
		GridLayout gd_searchPanel = new GridLayout(3, false);
		searchPanel.setLayout(gd_searchPanel);
		searchPanel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		m_docIDLabel = new Label(searchPanel, SWT.NONE);
		m_docIDLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		m_docIDLabel.setText(mRegistry.getString("documentID.Label"));
		
		m_docId = new Text(searchPanel, SWT.BORDER);
		GridData gd_m_docIDTextt = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
		gd_m_docIDTextt.widthHint=SWTUIHelper.convertHorizontalDLUsToPixels(m_docId, 100);
		m_docId.setLayoutData(gd_m_docIDTextt);
		
		searchIcon = mRegistry.getImage("searchButton.ICON");
		searchDocBtn1 = new Button(searchPanel,SWT.BORDER);
		GridData gd_searchDocBtn1 = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		searchDocBtn1.setLayoutData(gd_searchDocBtn1);
		searchDocBtn1.setImage(searchIcon);
		
		Composite treePanel = new Composite(dialogcomposite, SWT.BORDER | SWT.EMBEDDED);
		GridLayout gd_treePanel = new GridLayout(1, false);
		treePanel.setLayout(gd_treePanel);
		treePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			
		documentSearchTree = new  DocumentsTree();
			/*new TCTree()
			{
				private static final long serialVersionUID = 1L;
				protected void setExpandedState(TreePath arg0, boolean arg1) 
				{
					Object[] obj = arg0.getPath();
					if (obj!=null && obj[0] instanceof TCTreeNode) 
					{
						TCTreeNode node = (TCTreeNode)obj[0];
						if (arg1 && (!node.isRoot())) 
						{
							super.setExpandedState(arg0, false);
						}
					}
					else
					{
						super.setExpandedState(arg0, true);
					}
				}
			};*/
	    	DocumentTreeNode rootNode = new DocumentTreeNode(mRegistry.getString("documentsTree.Title", null));
	    	documentSearchTree.setRoot(rootNode);
		  //documentSearchTree.getRootNode().setTitle(mRegistry.getString("documentsTree.Title", null));
		  
		  documentSearchTree.setRootVisible(true);
		  documentSearchTree.setScrollsOnExpand(true);
		  
	      JScrollPane searchScrollPane = new JScrollPane(documentSearchTree);
	      searchScrollPane.setBorder(new BevelBorder(0));
	      searchScrollPane
				.setHorizontalScrollBarPolicy(ScrollPagePane.HORIZONTAL_SCROLLBAR_ALWAYS);
	      searchScrollPane
				.setVerticalScrollBarPolicy(ScrollPagePane.VERTICAL_SCROLLBAR_ALWAYS);
		/*  com.teamcenter.rac.util.scrollpage.ScrollPagePane  searchScrollPane = new ScrollPagePane(documentSearchTree);
		  m_previousButton = searchScrollPane.getPageUp();
	      m_previousButton.setToolTipText("Load previous 30 SMDL");
	      
	      m_nextButton = searchScrollPane.getPageDown();
	      m_nextButton.setToolTipText("Load next 30 ");
	      
	      
	      searchScrollPane.getPagingPanel().remove(
	    		  searchScrollPane.getLoadAll());
	      searchScrollPane.setUpDownVisible(true);
	        
	      searchScrollPane.setUpDownVisible(true);*/
	  	  
	      SWTUIUtilities.embed( treePanel, searchScrollPane, false);
	      
	      //Search Status 
	      Composite errorComp = new Composite(dialogcomposite, SWT.NONE);
	      GridLayout gd_errorComp = new GridLayout(1, false);
	      errorComp.setLayout(gd_errorComp);
	      errorComp.setLayoutData(new GridData(SWT.LEFT, SWT.BOTTOM, true, false, 1, 1));
		  
		  m_searchStatusLbl = new Label(errorComp,SWT.NONE);
		  m_searchStatusLbl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		  m_searchStatusLbl.setText(mRegistry.getString("no_result_found.MSG"));
		  m_searchStatusLbl.setVisible(false);
	      addlisteners();

		return parent;
	}


	private void setTitle()
	{
		String strDialogTitle = mRegistry.getString("documentsSearchDialog.Title", null);
		this.getShell().setText(strDialogTitle);
	}

	 
	private void addlisteners() 
	{
		m_searchResults= new NovSearchDataModel();
		m_searchResults.setTotalResultCount(0);
		LoadAllButtonListener searchListener = new LoadAllButtonListener(this, m_searchResults);
		m_searchResults.registerObserver(this);
	
		
		searchDocBtn1.addSelectionListener(searchListener);
		
		SMDLKeyBoardEventListener keySearchListener = new SMDLKeyBoardEventLoadAllListener(this, m_searchResults);
		m_docId.addKeyListener(keySearchListener) ;
	    
		documentSearchTree.addMouseListener(new MouseAdapter() 
		{			
			public void mouseClicked(MouseEvent e) 
			{
				if (e.getClickCount()==2) 
				{
					TCTree source = (TCTree)e.getSource();
					
					//- Fetch the selected TCComponent from TCTree - Code starts here
									
					InterfaceAIFComponent selcomp = null;
					AIFTreeNode selNode = source.getSelectedNode();
					
					// Kishor Ahuja : TCDECREL : 2595
					if((selNode!= null) && (selNode.isRoot() == false)) // When Selected node is NOT root.
					{
						//Fetch TCComponnent from PUID
						try 
						{
							selcomp = m_Session.stringToComponent(selNode.getClientData().toString()); //0th row is the root; so need to subtract 1
							m_selectedComp=(TCComponent) selcomp;
							mContainer.setValue(m_selectedComp);
							
						} catch (TCException ex) 
						{
							ex.printStackTrace();
							
						}
					}
					
					if (selcomp == null) 
					{
						return ;
					}
					
					Display.getDefault().asyncExec(new Runnable() {

						public void run()

						{
							DocumentSearchDialog.this.close();
						}
					});
				}
			}
		});
		
		
		/* NextPageListener nextPageBtnListener = new NextPageListener( 
					this, m_searchResults );
	      m_previousButton.addActionListener( nextPageBtnListener );

		PreviousPageListener prevPageBtnListener = new PreviousPageListener(  m_searchResults );
			m_nextButton.addActionListener( prevPageBtnListener );*/
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent)
	{
		
	}
	

	@Override
	protected Point getInitialSize() {
		
		Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;

        int width = ((size.width ) / 2) - (rectwidth / 6);
        int height = ((size.height ) / 2);
        
		return new Point(width, height);
	}

	public void clearTreeItems()
    {
		AIFTreeNode rootNode = documentSearchTree.getRootNode();
		if (documentSearchTree.getRootNode().getChildCount() > 0) {
			documentSearchTree.removeAllChildren(rootNode);
		}
     }
	
	   
	public QuickSearchService.QuickBindVariable[] getBindVariables() {
	
		m_sItemType = "Documents";

		if (m_sItemId == null || m_sItemId.isEmpty())
			m_sItemId = "*";

		// Construct Bind Variables
		QuickSearchService.QuickBindVariable [] allBindVars =null;
		allBindVars = new QuickSearchService.QuickBindVariable[12];
		
		//m_sItemID
		allBindVars[0] = new QuickSearchService.QuickBindVariable();
		allBindVars[0].nVarType = POM_string;
		allBindVars[0].nVarSize = 1;
		allBindVars[0].strList = new String[]{m_sItemId};
		
		//m_sItemName
		allBindVars[1] = new QuickSearchService.QuickBindVariable();
		allBindVars[1].nVarType = POM_string;
		allBindVars[1].nVarSize = 1;
		allBindVars[1].strList = new String[] {"*"};
		
		//itemType
		allBindVars[2] = new QuickSearchService.QuickBindVariable();
		allBindVars[2].nVarType = POM_string;
		allBindVars[2].nVarSize = 1;
		allBindVars[2].strList = new String[]{m_sItemType};
		
		//m_sCreatedAfter,m_sCreatedBefore				
		allBindVars[3] = new QuickSearchService.QuickBindVariable();
		allBindVars[3].nVarType = POM_date;
		allBindVars[3].nVarSize = 2;
		allBindVars[3].strList = new String[] {"*","*"};
		
		//m_sItemDesc
		allBindVars[4] = new QuickSearchService.QuickBindVariable();
		allBindVars[4].nVarType = POM_string;
		allBindVars[4].nVarSize = 1;
		allBindVars[4].strList = new String[] {"*"};
		
		//m_sReleaseStatus
		allBindVars[5] = new QuickSearchService.QuickBindVariable();
		allBindVars[5].nVarType = POM_string;
		allBindVars[5].nVarSize = 1;
		allBindVars[5].strList = new String[] {"*"};
		
		//m_sOwningUser
		allBindVars[6] = new QuickSearchService.QuickBindVariable();
		allBindVars[6].nVarType = POM_string;
		allBindVars[6].nVarSize = 1;
		allBindVars[6].strList = new String[] {"*"};
	
		//m_grpPuid
		String strGroupPUID = "";
		/*Utils.getCurrentGroupPUID();*/
		allBindVars[7] = new QuickSearchService.QuickBindVariable();
		allBindVars[7].nVarType = POM_typed_reference;
		allBindVars[7].nVarSize = 1;
		allBindVars[7].strList = new String[] {strGroupPUID};
		
		//RSOne 4 alternate ID
		allBindVars[8] = new QuickSearchService.QuickBindVariable();
		allBindVars[8].nVarType = POM_string;
		allBindVars[8].nVarSize = 1;
		allBindVars[8].strList = new String[] {"*"};
		
		//m_sLastOwningUser
		allBindVars[9] = new QuickSearchService.QuickBindVariable();
		allBindVars[9].nVarType = POM_string;
		allBindVars[9].nVarSize = 1;
		allBindVars[9].strList = new String[] {"*"};
		
		//Send Alternate ID selection
		allBindVars[10] = new QuickSearchService.QuickBindVariable();
		allBindVars[10].nVarType = POM_string;
		allBindVars[10].nVarSize = 1;
		allBindVars[10].strList = new String[] {"*"};
		
		allBindVars[11] = new QuickSearchService.QuickBindVariable();
        allBindVars[11].nVarType = POM_string;
        allBindVars[11].nVarSize = 1;
        allBindVars[11].strList = new String[] {"*"};
        
		return allBindVars;
	}
	

	@Override
	public String getBusinessObject() 
	{
		return m_sItemType;
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo() 
	{
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
		
		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
		allHandlerInfo[0].listBindIndex = new int[] {1,2,3,4,5,6,7,8,10,11,12};
		allHandlerInfo[0].listReqdColumnIndex = new int[]{1,2};
		allHandlerInfo[0].listInsertAtIndex = new int[] {2,1};
			
		return allHandlerInfo;
	}
	
	@Override
	public boolean validateInput() 
	{
		m_sItemId = m_docId.getText();
		
        //if wild card exists in search string, remove the wild cards and verify string length.
        //it should be min. 3 characters long.- Else throw error.
        char[] arr = m_sItemId.trim().toCharArray();
        String inputId = "";
        for(int i=0; i<arr.length; i++)
        {
                if(arr[i] != '*')
                {
                        inputId = inputId + arr[i];
                }
        }
              
		if(inputId.trim().length()<3)
		{
			MessageBox.post( mRegistry.getString("MinimumSearchChars.Error"), 
					mRegistry.getString("Error.TITLE"),MessageBox.ERROR);
			return false;
		}
			
		return true;
	}

	@Override
	public void update()
	{
		
		DocumentTreeNode rootNode =  (DocumentTreeNode) documentSearchTree.getRootNode();
		//rootNode.removeAllChildren();
		
		clearTreeItems();
		
		int childCnt = 0;//documentSearchTree.getRootNode().getChildCount();
		DefaultTreeModel defTreeModel = (DefaultTreeModel) documentSearchTree.getModel();
		
		INOVResultSet results = m_searchResults.getResultSet();
		int resultCount= results.getRows();//m_searchResults.getTotalResultCount();
		System.out.println("total results found are: "+resultCount);
		rootNode.setChildrenCount(resultCount);
		DocumentTreeNode[] resultnodes = new DocumentTreeNode[resultCount];
		
		//Setting icon to Item type icon
		final CustomTreeCellRenderer renderer = new CustomTreeCellRenderer();
        renderer.setRendererIcon(TCTypeRenderer.getTypeIcon(m_sItemType, null));
        documentSearchTree.setCellRenderer(renderer);	
		
		if(results != null && results.getRows() > 0)
		{
			Vector<String> rowData = results.getRowData();
			
			/*int pageSize = m_searchResults.getPageSize();
			int respIndex = m_searchResults.getResponseIndex();*/
			
			for (int i = 0; i < resultCount; i++)
			//for (int i = respIndex; i < pageSize; i++)
			{
				String objPUID = rowData.elementAt(3*i);
				String objID = rowData.elementAt(3*i+1);
				String objNAME = rowData.elementAt(3*i+2);
				
				DocumentTreeNode childNode = new DocumentTreeNode(objID + "-" + objNAME);
				childNode.setClientData(objPUID);
				
				resultnodes[i]=childNode;
				/*defTreeModel.insertNodeInto(childNode, rootNode, childCnt);					
				//defTreeModel.nodesWereInserted(rootNode, new int[] { childCnt });
		        childCnt++;*/
		        
			}
			
			//add all newly created doc nodes to rootNod
			rootNode.addChildren(resultnodes);
			
			//rootNode.refresh();
			
			((DefaultTreeModel) documentSearchTree.getModel()).reload();
			m_searchStatusLbl.setVisible(false);
		}	
		else
		{
			m_searchStatusLbl.setVisible(true);
		}		
	}

	
	 private class CustomTreeCellRenderer extends DefaultTreeCellRenderer
	 {

	 	private static final long serialVersionUID = 1L;
	 	ImageIcon rendererIcon;

	     public void setRendererIcon(ImageIcon myIcon){
	          this.rendererIcon = myIcon;
	     };

	     public Component getTreeCellRendererComponent(JTree tree,
	         Object value, boolean selected, boolean expanded,
	         boolean leaf, int row, boolean hasFocus){

	         Component ret = super.getTreeCellRendererComponent(tree, value,
	         selected, expanded, leaf, row, hasFocus);

	         JLabel label = (JLabel) ret ;

	         label.setIcon( rendererIcon ) ;

	         return ret;
	     }
	 }

	public TCComponent getSelectedComponent() {
		return m_selectedComp;
	}

	

}


