package com.nov.rac.smdl.templates.dialogs;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import com.teamcenter.rac.common.TCTree;
import com.teamcenter.rac.common.TCTreeNode;

public class DocumentsTree extends TCTree 
{
	private static final long serialVersionUID = 1L;
	
	public DocumentsTree()
	{
		super();
	}
	
	protected void setExpandedState(TreePath arg0, boolean arg1) 
	{
		Object[] obj = arg0.getPath();
		if (obj!=null && obj[0] instanceof TCTreeNode) 
		{
			TCTreeNode node = (TCTreeNode)obj[0];
			if (arg1 && (!node.isRoot())) 
			{
				super.setExpandedState(arg0, false);
			}
		}
		else
		{
			super.setExpandedState(arg0, true);
		}
	}

	@Override
	public void addSelectedNodes(TreeNode[] arg0) {
		
		super.addSelectedNodes(arg0);
	}
	
	
	
}
