package com.nov.rac.smdl.templates.dialogs;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.smdl.common.NOVSMDLTable;
import com.nov.rac.smdl.templates.IOperationDelegate;
import com.nov.rac.smdl.templates.ITemplate;
import com.nov.rac.smdl.templates.TemplateConstants;
import com.nov.rac.smdl.templates.TemplateTablePanel;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class SMDLTemplateDialog extends AbstractSWTDialog implements PropertyChangeListener
{

	private String m_operationType= null;
	
	private Registry m_registry = null;
	
	private NOVSMDLTable m_smdlTable;

	private ITemplate m_templatePanel;

	private TemplateTablePanel m_tablePanel;
	
	private IOperationDelegate m_operationDelegate = null;
	
	private CreateInObjectHelper m_transferObject = null;

	private CreateInObjectHelper m_TemplateRevObject;

	private Button saveButton;

	private Button cancelButton;

	private TCComponent m_TargetObject = null;
	
	public SMDLTemplateDialog(Shell shell)
	{
		super(shell);
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
		m_registry  = Registry.getRegistry("com.nov.rac.smdl.templates.templates");
	}
	

	@Override
	protected Control createDialogArea(Composite parent)
	{
		setTitle();
		
		//TCDECREL-3009 
	/*	Composite composite = new Composite(parent, SWT.NONE);
		GridLayout gd = new GridLayout(1, false);
		composite.setLayout(gd);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));

		//addHeader Image
		Image headerImage = m_registry.getImage("codeTemplate.ICON");
		Label  header= new Label(composite, SWT.IMAGE_PNG);
		header.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, false, 1, 1));
		header.setImage(headerImage);*/
		//TCDECREL-3009 ends 

		String strPanelName = m_operationType;
		
		Label separator1= new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
		separator1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		ScrolledComposite scrolledComposite = new ScrolledComposite( parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		scrolledComposite.setLayout(new GridLayout(1,true));
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite templatePane = new Composite(scrolledComposite,SWT.BORDER);
		templatePane.setLayout(new GridLayout(1,true));
		templatePane.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Object [] params = new Object[2];
		params[0] = templatePane;
		params[1] = SWT.NONE;
		try 
		{
			m_templatePanel =(ITemplate)m_registry.newInstanceFor(strPanelName, params);
			((Composite) m_templatePanel).setLayout(new GridLayout(1,true));
			((Composite) m_templatePanel).setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, false, 1, 1));
		
			Label separator2= new Label(templatePane, SWT.HORIZONTAL | SWT.SEPARATOR);
			separator2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			
			String strTablePanelName = m_operationType+".TABLE";
			m_tablePanel = (TemplateTablePanel) m_registry.newInstanceFor(strTablePanelName, params);
			m_tablePanel.setLayout(new GridLayout(1,true));
			m_tablePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			
			String strOperationName = m_operationType+".OPERATION";
			Object [] oprParams = new Object[1];
			oprParams[0] = this.getShell();
		
			m_operationDelegate = (IOperationDelegate) m_registry.newInstanceForEx(strOperationName, oprParams);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		m_operationDelegate.addOperationInputProvider(TemplateConstants.TEMPLATE_PANEL, m_templatePanel);
		m_operationDelegate.addOperationInputProvider(TemplateConstants.TEMPLATE_TABLE_PANEL, m_tablePanel);
		
		if(m_TargetObject!= null)
		{
			m_tablePanel.setTargetObject(m_TargetObject);
		}
	
		m_templatePanel.registerPropertyChangeListener((PropertyChangeListener) m_templatePanel);
		m_templatePanel.registerPropertyChangeListener(m_tablePanel);
		m_templatePanel.registerPropertyChangeListener(this);
		

		scrolledComposite.setContent(templatePane);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setMinSize(templatePane.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		scrolledComposite.pack();
		
		return parent;
	}
	
	
	@Override
	protected Point getInitialSize() 
	{
		Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;
        int rectHeight = rect.height;

        int width = ((size.width )) - (rectwidth / 6);
        int height = ((size.height ))- (rectHeight/2);;
        
		return new Point(width, height);

	}

	@Override
	protected Point getInitialLocation(Point point)
	{
		
		// Move the dialog to the center of the top level shell.
        Rectangle shellBounds = getShell().getParent().getBounds();
        Point dialogSize = getInitialSize();

        int x= shellBounds.x + (shellBounds.width - dialogSize.x) / 2;
        int y= shellBounds.y + (shellBounds.height - dialogSize.y) / 2;
        Point location = new Point(x,y);
		
        return location;
	}


	@Override
	protected Control createButtonBar(Composite parent) 
	{
		/*Composite composite = new Composite(parent, 0);
        GridLayout layout = new GridLayout();
        layout.numColumns = 0;
        layout.makeColumnsEqualWidth = true;
        layout.marginWidth = convertHorizontalDLUsToPixels(7);
        layout.marginHeight = convertVerticalDLUsToPixels(7);
        layout.horizontalSpacing = convertHorizontalDLUsToPixels(4);
        layout.verticalSpacing = convertVerticalDLUsToPixels(4);
        composite.setLayout(layout);
    
        GridData data = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        composite.setLayoutData(data);
        
        composite.setFont(parent.getFont());*/
		TemplateButtonBar composite = new TemplateButtonBar(parent, 0);
        createButtonsForButtonBar(composite);
        return composite;
	}

	@Override
	protected void createButtonsForButtonBar(final Composite parent)
	{
		 String operationLabel= m_operationType+".Label"; 
		 saveButton =  createButton(parent, IDialogConstants.OK_ID, m_registry.getString(operationLabel), true);
	     cancelButton=  createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	     
	     saveButton.setEnabled(false);
	}
	

	private void setTitle() 
	{
		String title = 	m_registry.getString(m_operationType+".Title");
		
		this.getShell().setText(title);
		
	}
	
	public void setOperationType(String mOperationType)
	{
		m_operationType = mOperationType;
	}

	@Override
	protected void okPressed() 
	{
		saveButton.setFocus();
		if(validateInputs())
		{
			boolean success= m_operationDelegate.executeOperation();
			if(success)
			{
				super.okPressed();
			}
		}
		
	}

	

	@Override
	protected void cancelPressed() 
	{
	
		boolean yes = MessageDialog.openQuestion( AIFUtility.getActiveDesktop().getShell(),
				m_registry.getString("cancel.TITLE"), m_registry.getString("cancel.MSG") );	
		if(yes)
        {
        	super.cancelPressed();
        }
	}


	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals(TemplateConstants.ENABLE_OPERATION) )
		{
			Boolean enable= (Boolean) event.getNewValue();
			saveButton.setEnabled(enable);
		}
		
	}
	
	private boolean validateInputs() 
	{
		boolean valid= true;
		StringBuffer invalidInputs=new StringBuffer();
		
		boolean panelMandatoryMissing = false, tableMandatoryMissing = false;
		if(m_templatePanel.validateData().trim().length()>0)
		{
			panelMandatoryMissing= true;
			invalidInputs.append(m_templatePanel.validateData());
		}
		
		if(m_tablePanel.validateData().trim().length()>0)
		{
			tableMandatoryMissing= true;
			if(invalidInputs.length()!=0)
			{
				invalidInputs.append(",");
			}
			invalidInputs.append( m_tablePanel.validateData());
		}
		
		if(m_tablePanel.validateNOOfWeeks().trim().length()>0)
		{
			if(invalidInputs.length()!=0)
			{
				invalidInputs.append("\n\n");
			}
			invalidInputs.append(m_tablePanel.validateNOOfWeeks());
		}
	        
		if(invalidInputs.length()!=0)
		{
			valid = false;
			if(panelMandatoryMissing || tableMandatoryMissing)
			{
				MessageBox.post(m_registry.getString("FillMandatoryFields.MSG")+"\n\n"+invalidInputs,"Error" ,MessageBox.ERROR);
			}
			else
			{
				MessageBox.post(invalidInputs.toString(),"Error" ,MessageBox.ERROR);
			}
			
		}
				
		return valid;
	}


	public void setTargetObject(TCComponent targetObject) {
		
		m_TargetObject  = targetObject;
		
	}
	
}
