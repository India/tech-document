package com.nov.rac.smdl.templates.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class TemplateButtonBar extends Composite{

	public TemplateButtonBar(Composite parent, int style) {
		super(parent, style);
		
		//Composite composite= this;
		this.setFont(parent.getFont());
		
		createUI(this);
		
	}

	private void createUI(TemplateButtonBar composite) {
		
		GC gc = new GC(composite);
		
        FontMetrics fontMetrics = gc.getFontMetrics();
        gc.dispose();
		
		 GridLayout layout = new GridLayout();
	        layout.numColumns = 0;
	        layout.makeColumnsEqualWidth = true;
	        layout.marginWidth = Dialog.convertHorizontalDLUsToPixels(fontMetrics, 7);
	        layout.marginHeight = Dialog.convertVerticalDLUsToPixels(fontMetrics,7);
	        layout.horizontalSpacing = Dialog.convertHorizontalDLUsToPixels(fontMetrics,4);
	        layout.verticalSpacing = Dialog.convertVerticalDLUsToPixels(fontMetrics,4);
	        composite.setLayout(layout);
	    
	        GridData data = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
	        composite.setLayoutData(data);
	        
		
	}

}
