package com.nov.rac.smdl.templates.dialogs;

import java.util.Vector;

import com.teamcenter.rac.aif.common.AIFTreeNode;
import com.teamcenter.rac.common.TCTreeNode;

public class DocumentTreeNode extends TCTreeNode 
{

	
	 public DocumentTreeNode(Object obj)
	 {
		 super(obj);
	 }

	@Override
	public void addChildren(AIFTreeNode[] arg0) {
		
		super.addChildren(arg0);
	}
	
	public void setChildrenCount(int count)
	{
		children= new Vector(count);
	}
	
	 
}
