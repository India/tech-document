package com.nov.rac.smdl.templates;

import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.kernel.NOV4ComponentTemplate;
import com.nov.rac.kernel.NOV4ComponentTemplateRevision;
import com.nov.rac.smdl.templates.renderereditors.TemplateDocCellEditor;
import com.nov.rac.smdl.utilities.NOV4TemplateHelper;
import com.nov.rac.smdl.utilities.NOVSMDLObjectSOAHelper;
import com.nov.rac.smdl.utilities.NOVSMDLPolicy;
import com.nov.rac.smdl.utilities.NOVSMDLTableHelper;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class TemplateTablePanel extends Composite implements PropertyChangeListener, IOperationInputProvider
{

	private Registry m_Registry;
	protected SMDLTemplateTable m_smdlTable = null;
	private TCSession m_session;
	private TemplateTablePanel m_tablePanel;
	private Button m_addCodeButton;
	private Button m_removeCodeButton;
	private Button m_removeDocumentButton;
	//private final static int DOCUMENT_COLUMN = 2;
	private final static String DOCUMENT_COLUMN_NAME = "nov4_document";
	
	private TCComponent m_templateSOAPlainObject = null;
	private NOVSMDLPolicy m_TemplatePolicy = null;
	private TCComponent m_TargetObject = null;
	protected Composite buttonPanel;

	public TemplateTablePanel(Composite parent, int style) 
	{
		super(parent, style);
		m_Registry = Registry.getRegistry(this);
		m_session = (TCSession)AIFUtility.getDefaultSession();
		createUI(this);
	}

	private void createUI(TemplateTablePanel templateTablePanel) 
	{
		
		m_tablePanel = templateTablePanel;
		m_tablePanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		m_tablePanel.setLayout(new GridLayout(1, false));
		
		createButtonPanel();
		
		Composite tablePaneComposite = new Composite(m_tablePanel, SWT.EMBEDDED);
		tablePaneComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		String prefName= m_Registry.getString("tableColumnPreference.Name");
		String[] strColNames=PreferenceHelper.getStringValues(prefName, TCPreferenceService.TC_preference_site);
		
		m_smdlTable = new SMDLTemplateTable(m_session , strColNames);
		ScrollPagePane tablePane= new ScrollPagePane(m_smdlTable);
		SWTUIUtilities.embed( tablePaneComposite, tablePane, false);
		
		String colWidthPrefName= m_Registry.getString("tableColumnWidthPreference.Name");
		String[] columnsWidth = PreferenceHelper.getStringValues(colWidthPrefName,TCPreferenceService.TC_preference_site);
		
		if(strColNames.length != columnsWidth.length )
		{
			MessageBox.post(m_Registry.getString("NO_PREFERENCE.Error"),m_Registry.getString("Error.TITLE"), MessageBox.ERROR);
		}else if (strColNames.length == columnsWidth.length)
		{
			m_smdlTable.setColumnWidth(columnsWidth);
		}
		
		addListeners();
	}

	protected void createButtonPanel() 
	{
		buttonPanel = new Composite(m_tablePanel,SWT.NONE);
		RowLayout rl_buttonPanel = new RowLayout(SWT.HORIZONTAL);
		rl_buttonPanel.pack = false;
		buttonPanel.setLayout(rl_buttonPanel);
		
		m_addCodeButton = new Button(buttonPanel, SWT.CENTER);
		m_addCodeButton.setText(m_Registry.getString("addCodeButton.Label"));
		
		m_removeCodeButton = new Button(buttonPanel, SWT.CENTER);
		m_removeCodeButton.setText(m_Registry.getString("removeCodeButton.Label"));
		
		m_removeDocumentButton = new Button(buttonPanel, SWT.CENTER);
		m_removeDocumentButton.setText(m_Registry.getString("removeDocButton.Label"));
	}

	
	
	
	protected void addListeners() 
	{
		m_addCodeButton.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				addOperation();

			}
		});
		
		m_removeCodeButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(m_smdlTable!=null)
				{
					removeCodeOperation();
					m_smdlTable.repaint();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {
				
			}
		});
		
		m_removeDocumentButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				
			}

			@Override
			public void widgetSelected(SelectionEvent event) {
				if(m_smdlTable!=null)
				{
					removeDocumentOperation();
				}
			}
			
		});
		
	}

	protected void removeDocumentOperation() {
		
		// Stop cell editind, before reading data from table.
		NOVSMDLTableHelper.stopTableCellEditing(m_smdlTable);
		
		int rowCnt = m_smdlTable.getSelectedRowCount();
        
        if(rowCnt==1)
        {
             int selectedRowIndex = m_smdlTable.getSelectedRow();
             int column =  m_smdlTable.getColumnIndex(DOCUMENT_COLUMN_NAME);
           
            Object value= m_smdlTable.getValueAtDataModel(selectedRowIndex, column);
            if(value==null || value.toString().trim().length()==0)
            {
            	MessageBox.post(m_Registry.getString("NO_Document_Selected.Error"), 
            			m_Registry.getString("Error.TITLE"),MessageBox.ERROR);
            }else
            {
            	 TCComponent comp= null;
            	
            	
         /*   	java.awt.EventQueue.invokeLater(new Runnable() {
            		public void run() {
            			m_smdlTable.getModel().setValueAt(comp, selectedRowIndex, column);
            		}
            		});*/
            	
            
              	/*m_smdlTable.setValueAt(comp, selectedRowIndex, column);
              	 * //m_smdlTable.dataModel.setValueAt(comp, selectedRowIndex, column);
              	m_smdlTable.validate();
               	m_smdlTable.revalidate();
               	m_smdlTable.repaint();
               	m_smdlTable.updateUI();
               	 m_smdlTable.setValueAt("newComment", selectedRowIndex, 3);
            	 */
            	 
            	 m_smdlTable.dataModel.setValueAt(comp, selectedRowIndex, column);
            	 TemplateDocCellEditor editor = (TemplateDocCellEditor)m_smdlTable.getCellEditor(selectedRowIndex, column);
            	 editor.setValue(null);
            }
          
        }
		if( rowCnt ==0 || rowCnt>1 )
        {
        	MessageBox.post(m_Registry.getString("NO_Document_Selected.Error"), 
        			m_Registry.getString("Error.TITLE"),MessageBox.ERROR);
        }
        
    
	}

	protected void addOperation()
	{
		
		Object[] obj= new String[m_smdlTable.getColumnCount()];
        m_smdlTable.addRow(obj);
		
	}
	

	protected void removeCodeOperation() 
	{
		// Stop cell editind, before reading data from table.
		NOVSMDLTableHelper.stopTableCellEditing(m_smdlTable);
		
		int rowCnt = m_smdlTable.getSelectedRowCount();
        if( rowCnt == 0)
        {
        	MessageBox.post(m_Registry.getString("NO_Row_Selected.ERROR"), 
        			m_Registry.getString("Error.TITLE"),MessageBox.ERROR);
        }
        else if(rowCnt>0)
        {
            int[] selectedRowIndex = m_smdlTable.getSelectedRows();
            
            for(int index=rowCnt-1;index>=0;index--)
            {
            	m_smdlTable.dataModel.removeRow(selectedRowIndex[index]);
            	//m_smdlTable.removeRow(selectedRowIndex[index]);
            }
        }
        
		
	}


	@Override
	public void propertyChange( java.beans.PropertyChangeEvent event ) {
		
		//if( event.getPropertyName().equals( "templateName" ) )
		if( event.getPropertyName().equals( TemplateConstants.TEMPLATE_NAME_CHANGE ) )
		{
			TCComponentItem templateItem = ( TCComponentItem )event.getNewValue();
			if( templateItem == null )
			{
				m_smdlTable.removeAllRows();
			}
			else
			{
				populateSOAComponent( templateItem);
			}
		}
	}

	protected void populateSOAComponent( TCComponentItem templateItem ) 
	{
		TCComponentItemRevision templateRevision = null;
		try
		{
			templateRevision = templateItem.getLatestItemRevision();
		}
		catch( TCException e )
		{
			e.printStackTrace();
		}
		
		if( templateRevision != null )
		{
			NOV4TemplateHelper.getTemplateRevisionProps( templateRevision );
			populateTable( templateRevision );
		}
	}
	
	protected void populateSOAComponent()
	{
		
		TCComponent SOASMDLRevComponent = null;
		
		if(m_TargetObject!= null)
		{
			TCComponentItemRevision smdlRevObject = null;
			try 
			{
				if(m_TargetObject instanceof TCComponentItemRevision)
				{
					smdlRevObject = (TCComponentItemRevision)m_TargetObject;
				}
				else if(m_TargetObject instanceof TCComponentItem)
				{
					smdlRevObject = ((TCComponentItem)m_TargetObject).getLatestItemRevision();
				}
				
				//get the smdl Rev properties loaded
				SOASMDLRevComponent = NOVSMDLObjectSOAHelper.getSMDLRevisionProps(smdlRevObject);
				
				populateTable(SOASMDLRevComponent);
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	
	protected void populateTable( TCComponent SOATemplateComponent )
	{
		addTableRows( SOATemplateComponent );
	}
	
	protected void addTableRows(TCComponent component) 
	{
		
		 //Get the typed reference for all structure data attached to template Revision
		m_smdlTable.removeAllRows();
		
       if (component != null)
       {
       	try
       	{
       		TCComponent[] dataObjs = null;
       
       		
       			dataObjs = component.getTCProperty(NOV4ComponentTemplateRevision.TEMPLATE_STRUCTURE_DATA).getReferenceValueArray();
				
				for(int iCount = 0;iCount < dataObjs.length; iCount++ ){
					
					Vector<Object> structureDataAttributes = new Vector<Object>();
					
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_CODE).getPropertyValue());
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_ADDITIONAL_CODE).getPropertyValue());
				
					
					TCComponent smdlDoc = dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_DOCUMENT_REF).getReferenceValue();
					if(smdlDoc != null){
						structureDataAttributes.addElement(smdlDoc);
					}
					else
						structureDataAttributes.addElement("");
					
					
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_COMMENTS).getPropertyValue());
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_DOC_CONTRACT_DATE).getPropertyValue());
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_WEEKS).getPropertyValue());
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_LINE_ITEM_NUMBER).getPropertyValue());
					structureDataAttributes.addElement(dataObjs[iCount].getTCProperty(NOV4ComponentTemplate.TEMPLATE_TAB).getPropertyValue());
					
								
					m_smdlTable.addRow((Object)structureDataAttributes);
				}
				
			} catch (TCException e) {
				
				e.printStackTrace();
			}
       }
		
	}

	public String validateData() 
	{
		StringBuffer  invalidInputs= new StringBuffer();
		Set<String>invalidColumns= new TreeSet<String>();
		
		// Stop cell editind, before reading data from table.
		NOVSMDLTableHelper.stopTableCellEditing(m_smdlTable);
		
		int rows= m_smdlTable.getRowCount();
		String[] mandatoryColumnNames =m_Registry.getStringArray("mandatoryColumns.Name");
	      
		for(int row= 0;row<rows;row++)
		{
			for(int index=0;index<mandatoryColumnNames.length;index++)
			{
				String columnName = mandatoryColumnNames[index];
				int column = m_smdlTable.getColumnIndex(columnName);
				Object cellData= m_smdlTable.getValueAt(row, column);
				if(cellData.toString().trim().length()==0)
				{
					invalidColumns.add(m_smdlTable.getColumnName(column));
				}
			}
			
		}
		
		Iterator iterator = invalidColumns.iterator();
        while (iterator.hasNext()) 
        {
			if(invalidInputs.length()!=0){
				invalidInputs.append(",");
			}
			invalidInputs.append(iterator.next());
        }
		
    
        
		return invalidInputs.toString();
	}

	public String validateNOOfWeeks() {
		
		StringBuffer  invalidInputs= new StringBuffer();
		
		// Stop cell editind, before reading data from table.
		NOVSMDLTableHelper.stopTableCellEditing(m_smdlTable);
		
		int rows= m_smdlTable.getRowCount();
		String columnName = "nov4_weeks";
		int column = m_smdlTable.getColumnIndex(columnName);
		boolean invalid= false;
		for(int row= 0;row<rows;row++)
		{
				Object cellData=  m_smdlTable.getValueAt(row, column);
		
				try
				{
					int value= Integer.valueOf(cellData.toString());
					if(value<1 || value >50 )
					{
						invalid= true;
						if(invalidInputs.length()!=0)
						{
							invalidInputs.append(", ");
						}
						int rowno= row+1;
						invalidInputs.append(rowno);
					}
				}
				catch (Exception e)
				{
					
				}
				
		}
		
		if(invalid)
		{
			return m_Registry.getString("WRONG_NOV_WEEKS.Error")+invalidInputs.toString();
		}
		else
		{
			return "";
		}
		
	}

	public void setTargetObject(TCComponent targetObject) 
	{
			m_TargetObject = targetObject;
	}

	

	@Override
	public CreateInObjectHelper[] populateOperationInput(CreateInObjectHelper[] inputHelpers) 
	{
		// Stop cell editind, before reading data from table.
		NOVSMDLTableHelper.stopTableCellEditing(m_smdlTable);
		
		int rowCount= m_smdlTable.getRowCount();
		CreateInObjectHelper[] rowObjects= new CreateInObjectHelper[rowCount];
		
		for(int row=0;row<rowCount;row++)
		{
			CreateInObjectHelper rowObject= new CreateInObjectHelper(NOV4ComponentTemplate.TEMPLATE_STRUCTURE_DATA_TYPE,
					CreateInObjectHelper.OPERATION_CREATE);
			
			AIFTableLine tableLine= m_smdlTable.getRowLine(row);
			SMDLTemplateTableLine smdlTableLine = null;
    		
			if(tableLine instanceof SMDLTemplateTableLine)
    		{
    			smdlTableLine = (SMDLTemplateTableLine)tableLine;
    			
    			//novCode
    			String templateCode = (String) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_CODE);
    			int codeIndex= templateCode.toString().indexOf("=");
    			String novCode= templateCode;
				if(codeIndex!=-1)
    			{
    				novCode = templateCode.substring(0,codeIndex);
    			}
    			
    			rowObject.setStringProps(NOV4ComponentTemplate.TEMPLATE_CODE, novCode, CreateInObjectHelper.SET_PROPERTY);
    			
    			
    			//Additional Code
    			String additionalCode = (String) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_ADDITIONAL_CODE);
    			if(additionalCode!=null)
    			{
    				rowObject.setStringProps(NOV4ComponentTemplate.TEMPLATE_ADDITIONAL_CODE, additionalCode, CreateInObjectHelper.SET_PROPERTY);
    			}
    			
    			//Document
    			TCComponent document = (TCComponent) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_DOCUMENT_REF);
    			//if(document!=null)
    			{
    				rowObject.setTagProps(NOV4ComponentTemplate.TEMPLATE_DOCUMENT_REF, document, CreateInObjectHelper.SET_PROPERTY);
    			}
    			
    			//Comments
    			String comments = (String) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_COMMENTS);
    			if(comments!=null)
    			{
    				rowObject.setStringProps(NOV4ComponentTemplate.TEMPLATE_COMMENTS, comments, CreateInObjectHelper.SET_PROPERTY);
    			}
    			
    			//Contract Date 
    			String contractDate = (String) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_DOC_CONTRACT_DATE);
    			rowObject.setStringProps(NOV4ComponentTemplate.TEMPLATE_DOC_CONTRACT_DATE, contractDate, CreateInObjectHelper.SET_PROPERTY);
    			
    			//No Of Weeks
    			Object weeks= tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_WEEKS);
				if (weeks != null)
				{
					Integer noOfWeeks = 0;
					if(weeks instanceof String)
					{
						noOfWeeks =Integer.valueOf((String) weeks);
					}else
					{
						noOfWeeks=(Integer) weeks;
					}
					
					rowObject.setIntProps(NOV4ComponentTemplate.TEMPLATE_WEEKS,
							noOfWeeks, CreateInObjectHelper.SET_PROPERTY);
				}
				
				//Line item number
				String lineItemNumber = (String) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_LINE_ITEM_NUMBER);
				if(lineItemNumber!=null)
				{
					rowObject.setStringProps(NOV4ComponentTemplate.TEMPLATE_LINE_ITEM_NUMBER, lineItemNumber, CreateInObjectHelper.SET_PROPERTY);
				}
				//nov4_tab
    			String tab = (String) tableLine.getProperty(NOV4ComponentTemplate.TEMPLATE_TAB);
    			if(tab!=null)
    			{
    				rowObject.setStringProps(NOV4ComponentTemplate.TEMPLATE_TAB, tab, CreateInObjectHelper.SET_PROPERTY);
    			}
				
    		}
    		rowObjects[row]=rowObject;
		}
		
		return rowObjects;
	}

	@Override
	public TCComponent getComponent()
	{
		return null;
	}
	
}
