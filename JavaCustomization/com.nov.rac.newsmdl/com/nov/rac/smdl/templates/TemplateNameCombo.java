package com.nov.rac.smdl.templates;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.smdl.utilities.NOV4TemplateHelper;
import com.nov.rac.smdl.utilities.NOVSMDLComboUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class TemplateNameCombo extends Combo {
	
	private Registry m_Registry;

	class StringComparator implements Comparator<String> 
	{
		  public int compare(String strA, String strB) {
		    return strA.compareToIgnoreCase(strB);
		  }
	}
	private TCSession m_session;

	public TemplateNameCombo(Composite parent, int style) {
		super(parent, style);
		m_Registry = Registry.getRegistry("com.nov.rac.smdl.templates.templates");
		m_session= (TCSession)AIFUtility.getDefaultSession();
		initialiseValues();
	}
	
	private void initialiseValues() 
	{
		//TCDECREL-3129
		TCSession session = (TCSession) AIFUtility.getDefaultSession();
		if(session != null)
		{
			TCComponent[] components = null;
			String[] adminGroups = m_Registry.getStringArray("adminGroups.Name");

			if (Arrays.asList(adminGroups).contains(session.getGroup().toString()) ) 
		  	{
			  components = NOV4TemplateHelper.getAdminTemplates();
		  	}
		  	else
		  	{
			  components = NOV4TemplateHelper.getTemplatesForLoggedInGroup();
		  	}
			Vector<String> templateNames = new Vector<String>();
			
			for(int i =0;i<components.length;i++)
			{
				try
				{
					String name = 	components[i].getProperty("object_name");
					templateNames.add(name);
					this.setData(name, 	components[i]);
					
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//TCDECREL3157
			
			Collections.sort(templateNames, new StringComparator());
			NOVSMDLComboUtils.setAutoComboVector(this, templateNames);
		}
			
	}
	

	@Override
	protected void checkSubclass() {
		// TODO Auto-generated method stub
		//super.checkSubclass();
	}

}
