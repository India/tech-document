package com.nov.rac.kernel;

import com.teamcenter.rac.kernel.TCComponentItem;

public class NOV4ComponentTemplate extends TCComponentItem
{
	public NOV4ComponentTemplate()
	{
		super();
	}
	
	
	public final static String TEMPLATE_CODE = "nov4_novcode";
	public final static String TEMPLATE_ADDITIONAL_CODE = "nov4_additional_code";
	public final static String TEMPLATE_DOCUMENT_REF = "nov4_document";
	public final static String TEMPLATE_DOC_CONTRACT_DATE = "nov4_contract_date";
	public final static String TEMPLATE_COMMENTS = "nov4_comments";
	public final static String TEMPLATE_WEEKS = "nov4_weeks";
	public final static String TEMPLATE_LINE_ITEM_NUMBER= "nov4_line_item_number";
	public final static String TEMPLATE_TAB= "nov4_tab";
	
	public final static String TEMPLATE_STRUCTURE_DATA_TYPE = "Nov4SMDLStructureData";

	public static final String TEMPLATE_OBJECT_TYPE_NAME = "Nov4Template";	
	
	public final static String TEMPLATE_NAME = "object_name";
	public final static String TEMPLATE_DESC = "object_desc";
}
