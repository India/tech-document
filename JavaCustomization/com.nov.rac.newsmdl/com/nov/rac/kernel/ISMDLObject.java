package com.nov.rac.kernel;

import com.teamcenter.rac.kernel.TCComponent;

public interface ISMDLObject {

	
	public TCComponent getOrder();
	
	public TCComponent getParent();
	
}
