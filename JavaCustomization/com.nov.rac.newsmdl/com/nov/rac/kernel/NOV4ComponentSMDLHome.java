package com.nov.rac.kernel;

import com.teamcenter.rac.kernel.TCComponentFolder;

public class NOV4ComponentSMDLHome extends TCComponentFolder{

	public NOV4ComponentSMDLHome()
	{
		super();
	}

	public final static String SMDL_HOME_RELATION = "Nov4_SMDL_home_relation";
	public final static String SMDL_HOME_OBJECT_TYPE = "Nov4SMDLHome";
	public final static String SMDL_HOME_NAME = "SMDLHome";
	public final static String SMDL_HOME_DESC = "SMDL Home Folder";
	public final static String SMDL_HOME_CLASS = "Folder";
	
	
}
