package com.nov.rac.kernel;

import com.teamcenter.rac.kernel.TCComponentItemRevision;

public class NOV4ComponentTemplateRevision extends TCComponentItemRevision {

	public NOV4ComponentTemplateRevision()
	{
		super();
	}
	
		public final static String TEMPLATE_REVISION_TYPE_NAME ="Nov4Template Revision";
		
		public final static String TEMPLATE_STRUCTURE_DATA = "nov4_smdl_structure_data";
}
