package com.nov.rac.kernel;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class NOV4ComponentSMDL extends TCComponentItem implements ISMDLObject {

	public NOV4ComponentSMDL()
	{
		super();
	}

	@Override
	public TCComponent getOrder() {
		
		TCComponent dpObject = getDeliveredProduct();
		return ((NOV4ComponentDP)dpObject).getOrder();
	}

	private TCComponent getDeliveredProduct() {
		String[] types ={"DeliveredProduct"};
		String[] relations={"_DeliveredProdSMDL_"};
		TCComponent orderObejct=null, dpObject=null;
		try {
			AIFComponentContext[] context=this.whereReferencedByTypeRelation(types, relations);
			
			for(int inx=0;inx<context.length;inx++)
			{
				String relationName=(String)context[inx].getContextDisplayName();
				if(relationName.equals("_DeliveredProdSMDL_"))
				{
					dpObject=(TCComponent) context[inx].getComponent();
				}
			}
			
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		return dpObject;
	}

	@Override
	public TCComponent getParent() {
		
		return getDeliveredProduct();
	}


	//SMDL Attributes
	public final static String SMDL_ID = "item_id";
	public final static String SMDL_NAME = "object_name";
	public final static String SMDL_ITEM_REF = "items_tag";
	public final static String SMDL_REVISION_ID = "item_revision_id";
	public final static String SMDL_CODES = "nov4_codes";
	public final static String SMDL_TYPE = "nov4_smdl_type";
	public final static String DP_REF = "nov4_dp_properties";
	public final static String ORDER_REF = "nov4_order_properties";
	public final static String CODE_FORM_RELATION = "_SMDLRevCodeForm_";
	public final static String SMDL_OBJECT_TYPE_NAME = "SMDL";
	public final static String SMDLREV_OBJECT_TYPE_NAME = "SMDL Revision";
	public final static String SMDL_RELEASE_STATUS = "release_status_list";
	public final static String CODEFORM_OBJECT_TYPE_NAME = "_smdlcodeform_";
	public final static String SMDL_DESC = "object_desc";
	public final static String SMDL_REV_DOC_RELATION ="Nov4SMDLRevDocuments";
	
	public final static String SMDL_OBJECT_STRING = "object_string";
	public final static String SMDL_REL_STATUSES ="release_statuses";
	
	public final static String SMDL_CLOSED_STATUS_NAME ="Closed";
	public final static String DOC_RESPONSIBLE ="nov4_res_person";
	
	//Code Form Attributes
	public final static String SMDL_CODE_NUMBER = "nov_smdl_code";
	public final static String CUSTOMER_CODE = "nov_customer_code";
	public final static String ADDITIONAL_CODE = "nov4_additional_code";
	public final static String DOC_CONTRACT_DATE = "nov_contract_date";
	public final static String DOC_PLANNED_DATE = "nov_planned_date";
	public final static String COMMENTS = "nov_comments";
	public final static String GROUP_NAME = "nov_group";
	public final static String DOCUMENT_RESPONSIBLE = "nov_res_person";
	public final static String DOCUMENT_REF = "nov4_document";
	public final static String DOCUMENT_REV_REF = "nov4_document_rev";
	public final static String LINE_ITEM_NUMBER = "nov4_line_item_number";
	public final static String CODEFORM_TAB = "nov4_tab";
	
	//public final static String NOV_RELEASE_STATUS = "nov_release_statuses";
	public final static String NOV_RELEASE_STATUS = "nov4_document_rev.last_release_status";
	public final static String SMDL_CODEFORM_DOC_NAME = "nov4_document.object_name";
	public final static String SMDL_CODEFORM_DOC_DESC = "nov4_document_rev.object_desc";
	
	public final static String NOV_CODEFORM_NUM = "nov_code_form_number";
	
	public final static String SMDL_REVISION_LIST = "revision_list";
	
}
