package com.nov.rac.kernel;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;

public class NOV4ComponentOrder extends TCComponentItem implements ISMDLObject{

	public NOV4ComponentOrder()
	{
		super();
	}

	@Override
	public TCComponent getOrder() {
		return this;
	}

	@Override
	public TCComponent getParent() {
	
		return null;
	}

	public final static String ORDER_OBJECT_TYPE_NAME = "Order";
	
	public final static String ORDER_NUMBER = "item_id";
	public final static String ORDER_TYPE = "nov4_order_type";
	public final static String CUSTOMER = "nov4_customer";
	public final static String PLANT_ID_OTHER = "nov4_plant_id_other";
	public final static String PURCHASE_ORDER_NUMBER = "nov4_purchase_order_number";
	public final static String SALES_ORDER_NUMBER = "nov4_sales_order_number";
	public final static String CONTRACT_AWARD_DATE = "nov4_contract_award_date";
	public final static String CONTRACT_DELIVERY_DATE = "nov4_contract_delivery_date";
	public final static String SALESMAN = "nov4_salesman";
	public final static String ORDER_MANAGER = "nov4_order_manager";
	public final static String PROJECT_MANAGER = "nov4_project_manager";
	public final static String IC_CONTACT_MANAGER = "nov4_ic_contact_manager";
	public final static String DOCUMENT_CONTROL = "nov4_document_control";
	public final static String DOCUMENT_CONTROLLER = "nov4_document_controller";
	public final static String ORDER_MAILBOX = "nov4_order_mailbox";
	public final static String IC_MAILBOX = "nov4_ic_mailbox";
	public final static String COUNTRY_DESTINATION = "nov4_country_destination";
	public final static String EXTERNAL_CONTACT = "nov4_external_contact";
	public final static String EXTERNAL_PROJECT_NUM = "nov4_external_project_num";
	public final static String PROJECT_SITE = "nov4_project_site";
	public final static String ORDER_DP_RELATION = "_OrderDeliveredProduct_";
	
	public final static String ORDER_RELEASE_STATUS = "release_status_list";
	public final static String ORDER_PROCESS_STATUS = "process_stage_list";
}
