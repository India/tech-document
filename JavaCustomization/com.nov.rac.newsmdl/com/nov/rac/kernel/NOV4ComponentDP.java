package com.nov.rac.kernel;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;

public class NOV4ComponentDP extends TCComponentItem implements ISMDLObject{

	public NOV4ComponentDP()
	{
		super();
	}

	@Override
	public TCComponent getOrder() {
		
		String[] types ={"Order"};
		String[] relations={"_OrderDeliveredProduct_"};
		TCComponent orderObejct=null;
		try {
			AIFComponentContext[] context=this.whereReferencedByTypeRelation(types, relations);
			
			for(int inx=0;inx<context.length;inx++)
			{
				String relationName=(String)context[inx].getContextDisplayName();
				if(relationName.equals("_OrderDeliveredProduct_"))
				{
					orderObejct=(TCComponent) context[inx].getComponent();
				}
				//orderObejct=context[inx].getComponent();
			}
			
			System.out.println(".."+context[0].getComponent());
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		return orderObejct;
	}

	@Override
	public TCComponent getParent() {
		
		return getOrder();
	}

	
	public final static String DP_OBJECT_TYPE_NAME = "DeliveredProduct";
	
	public final static String DP_NUMBER = "item_id";	
	public final static String DP_DESC = "object_desc";
	public final static String DP_BASED_ON_PROD = "nov4_basedon_std_prod";
	public final static String DP_SERIAL_NUMBER = "nov4_serial_number";
	public final static String ENG_LOCATIONS = "nov4_eng_location";
	public final static String MFG_LOCATIONS = "nov4_mfg_location";
	public final static String ASSY_LOCATION = "nov4_assembly_location";
	public final static String DP_ENG_JOB_NUMBERS = "nov4_eng_job_numbers";
	public final static String MFG_JOB_NUMBERS = "nov4_mfg_job_numbers";
	public final static String PFAT_DATE = "nov4_pfat_date";
	public final static String FAT_DATE = "nov4_fat_date";
	public final static String SHIP_DATE = "nov4_ship_date";
	public final static String COMMISSION_DATE = "nov4_commission_date";
	public final static String REFURB_DATE  = "nov4_refurb_date";
	public final static String QC_REQUIREMENTS = "nov4_qc_requirements";
	
	public final static String QC_REQ_ABS = "ABS";
	public final static String QC_REQ_ATEX = "ATEX";
	public final static String QC_REQ_DnV = "DnV";
	public final static String QC_REQ_IEC = "IEC";
	public final static String QC_REQ_NDT = "NDT";
	public final static String QC_REQ_THIRD_PARTY = "3rd Party";
	
	public final static String QUANTITY = "nov4_quantity";
	public final static String SHIP_WTY = "nov4_ship_wty";
	public final static String COMMISSION_WTY = "nov4_commission_wty";
	public final static String DP_NAME = "object_name";
	public final static String CONTRACT_DELIVERY_DATE = "nov4_contract_delivery_date";
	public final static String DP_SMDL_RELATION = "_DeliveredProdSMDL_";
	public final static String DP_ORDER_RELATION = "nov4_order_properties";

	public final static String HCMNAME = "hcmname";
	public final static String NOV_LOCATIONS = "_novlocations_";

	public static final String SMDL_STATUS_NAME = "name";
	
	public final static String DP_RELEASE_STATUS = "release_status_list";
	public final static String DP_PROCESS_STATUS = "process_stage_list";
}
