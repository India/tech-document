package com.nov.rac.kernel;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class NOV4ComponentSMDLRevision extends TCComponentItemRevision implements ISMDLObject{

	public NOV4ComponentSMDLRevision()
	{
		super();
	}
	public final static String SMDLREV_DOCUMENT_RELATION = "_BinderRevDocuments_";
	public final static String SMDLREV_OBJECT_TYPE = "SMDL Revision";
	public final static String SMDLREV_ITEM_ID = "item_id";
	public final static String SMDLREV_REV_ID = "item_revision_id";
	public final static String SMDLREV_NAME = "object_name";
	public final static String SMDLREV_TYPE = "object_type";
	public final static String SMDLREV_RELEASE_STATUS = "release_status_list";
	public final static String SMDLREV_PROCESS_STAGELIST = "process_stage_list";
	public final static String SMDLREV_OBJECT_STRING = "object_string";
	public final static String SMDLREV_OWNING_SITE = "owning_site";
	public final static String SMDLREV_IS_VI = "is_vi";
	public final static String SMDLREV_HAS_MODULE_VARIANT = "has_variant_module";
	
	@Override
	public TCComponent getOrder() {

		TCComponent smdlObject = getParent();
		TCComponent orderObject = null;
		
		if(smdlObject != null && smdlObject instanceof ISMDLObject){
			orderObject = ((ISMDLObject)smdlObject).getOrder();
		}
		
		return orderObject;
	}
	@Override
	public TCComponent getParent() {

		TCComponent parentSMDL = null; 
		
		try {
			parentSMDL = this.getItem();
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return parentSMDL;
	}

}
