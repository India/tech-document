/**
 * 
 */
package com.nov.rac.dhl.ecr.helper;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;

/**
 * @author kabades
 * 
 */
public class NOVECRHelper
{
    public static String appendStringToCloseECR(String strReasonForCloseECR, String prefixString)
    {
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        String userID = session.getUser().toString();
        userID = userID.substring(0, userID.indexOf("(") - 1);
        String comment = strReasonForCloseECR +"\n"+ prefixString + " " + userID;
        return comment;
        
    }
}
