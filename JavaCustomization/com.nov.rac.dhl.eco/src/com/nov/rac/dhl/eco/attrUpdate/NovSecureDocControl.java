package com.nov.rac.dhl.eco.attrUpdate;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.teamcenter.rac.kernel.TCSession;

public class NovSecureDocControl extends Composite 
{
	Button[] radiobtns = new Button[2];
	
	
	public NovSecureDocControl(Composite parent, TCSession session) 
	{

		super(parent,SWT.BORDER);

		
		this.setLayout(new GridLayout(2, true));
		//this.setLayout(new RowLayout());
		//new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);

		radiobtns[0]=new Button(this, SWT.RADIO);
		radiobtns[0].setText("True");
		
		radiobtns[1]=new Button(this, SWT.RADIO);
		radiobtns[1].setText("False");	
	}
	
	public boolean getPullDocValue()
	{
		return radiobtns[0].getSelection();
	}
}//NovPullDocControl
