package com.nov.rac.dhl.eco.validations.addecr;

import java.util.Hashtable;

import com.teamcenter.rac.common.TCTableModel;

public class NOVCustomTableModel extends TCTableModel
{
    private static final long serialVersionUID = 1L;
	public NOVCustomTableModel()
    {
        super();
    }

    public NOVCustomTableModel(String[] data)
    {
        super(data);
    }

    public NOVCustomTableModel(String as[][])
	{
	   super(as);	    
	}
    public NOVCustomTableLine createLine(Object obj)
	{
    	NOVCustomTableLine aiftableline = null;
		Object [] strObj = null;

		if(obj instanceof Object[])
		{
			strObj = ((Object [])obj);	
			if(strObj != null)
			{
				int i = strObj.length;
				int j = getColumnCount();
				Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
				for(int k = 0; k < j && k < i; k++)
				{
					if(strObj[k] != null)
					{
						hashtable.put(getColumnIdentifier(k), strObj[k]);
					}
				}       
				aiftableline = new NOVCustomTableLine(hashtable);
			}
		}
		else 
		{
			aiftableline = (NOVCustomTableLine) super.createLine(obj);
		}
		return aiftableline;
	} 

    protected NOVCustomTableLine[] createLines(Object obj)
    {
    	NOVCustomTableLine aaiftableline[] = null;

    	if( obj instanceof Object [][] )
        {
    		Object [][] objArrArr = (Object [][])obj;    		
            int i = objArrArr.length;
            aaiftableline = new NOVCustomTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	NOVCustomTableLine newLine = createLine( objArrArr[j] );
                aaiftableline[j] = newLine;
            }
        } 
    	else
    	{
    		aaiftableline = (NOVCustomTableLine[]) super.createLines(obj);
    	}    	
    	return aaiftableline;
    }
}