package com.nov.rac.dhl.eco.validations.addecr;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVItemSelectionDialog extends AbstractSWTDialog
{
    private NOVItemSelectionComposite m_itemSelectionComposite;
    private Registry m_reg;
    public Button majorRevRadiButton;//7804
    public Button minorRevRadiButton;//7804
    public int iRevType;//7804
    public String sTypeOfChange=null;
    public Button okButton;
    //7804-added type of the form to the constructor
    public NOVItemSelectionDialog(Shell parentShell,String sTypeofChange)
    {
        super(parentShell);
        sTypeOfChange=sTypeofChange;//7804
        setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.CENTER);
        
        m_reg = Registry.getRegistry(this);
    }
    @Override
    protected Control createDialogArea(Composite parent)
    {
    	setTitle();
    	
        Composite dialogcomposite = new Composite(parent, SWT.NONE);        
        GridLayout gd = new GridLayout(1, false);
        dialogcomposite.setLayout(gd);
        dialogcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        m_itemSelectionComposite = new NOVItemSelectionComposite(dialogcomposite,SWT.NONE);
        addListener();//7804
        
        return parent;
    }
    //7804-start
    public void addListener()
    {
    	 final NOVCustomTable itemSelectionTable = getItemSelectionTable();
    	 itemSelectionTable.addMouseListener(new MouseListener() {
 			
 			@Override
 			public void mouseReleased(MouseEvent mouseevent) {
 				// TODO Auto-generated method stub
 				
 			}
 			
 			@Override
 			public void mousePressed(MouseEvent mouseevent) {
 				// TODO Auto-generated method stub
 				
 			}
 			
 			@Override
 			public void mouseExited(MouseEvent mouseevent) {
 				// TODO Auto-generated method stub
 				
 			}
 			
 			@Override
 			public void mouseEntered(MouseEvent mouseevent) {
 				// TODO Auto-generated method stub
 				
 			}
 			
 			@Override
 			public void mouseClicked(MouseEvent mouseevent) {
 				if(sTypeOfChange.equalsIgnoreCase("General"))
 				{
 					boolean isEnable=false;
 	 		        int iRowCount = itemSelectionTable.getRowCount();
 	 		        for(int i=0; i<iRowCount; i++)
 	 	            {
 	 		        	 boolean colValue = (Boolean) itemSelectionTable.getValueAt(i, 0);
 	 	 		    	 if(colValue)
 	 	  		        {
 	 	 		    		isEnable=true;
 	 	  		        	break;
 	 	  		        }
 	 	            }
 	 		        if(isEnable && getRevType().equalsIgnoreCase(""))
 	 		         {
 	 		        	Display.getDefault().asyncExec(new Runnable() {
 	 		        	    public void run() {
 	 		        	    	
 	 		        	    	okButton.setEnabled(false);
 	 		        	    }
 	 		        	}); 
 	 		         }
 	 		         else
 	 		         {
 	 		        	Display.getDefault().asyncExec(new Runnable() {
 	 		        	    public void run() {
 	 		        	    	
 	 		        	    	okButton.setEnabled(true);
 	 		        	    	
 	 		        	    }
 	 		        	});  
 	 		         }
 	 			}
 				}
 				
 		});
    	 
         
    }
    protected Control createButtonBar(Composite composite)
    {
    	
    	Composite composite1 = new Composite(composite, 0);
        GridLayout gridlayout = new GridLayout();
        gridlayout.numColumns = 3;
        gridlayout.makeColumnsEqualWidth = false;
        gridlayout.marginWidth = 5;
        gridlayout.marginHeight = 0;
        gridlayout.marginTop = 5;
        gridlayout.marginBottom = 5;
        gridlayout.horizontalSpacing = convertHorizontalDLUsToPixels(4);
        gridlayout.verticalSpacing = convertVerticalDLUsToPixels(4);
        composite1.setLayout(gridlayout);
        GridData griddata = new GridData(128);
        composite1.setLayoutData(griddata);
        createButtonsForSelectionDialog(composite1);
        return composite1;
    	
    }
    protected void createButtonsForSelectionDialog(Composite parent)
    {
    	if(sTypeOfChange.equalsIgnoreCase("General"))
    	{
	    	((GridLayout)parent.getLayout()).numColumns++;
	    	//radio button-1
	    	majorRevRadiButton = new Button(parent, SWT.RADIO);
	    	majorRevRadiButton.setText("Create Major Revision");
	    	majorRevRadiButton.setFont(JFaceResources.getDialogFont());
	    	majorRevRadiButton.setData(new Integer(2));
	    	majorRevRadiButton.addSelectionListener(new SelectionAdapter() {
	    		
	            public void widgetSelected(SelectionEvent event)
	            {
	            	iRevType= ((Integer)event.widget.getData()).intValue();
	            	okButton.setEnabled(true);
	              
	            }
	
	        });
	
	       setButtonLayouts(majorRevRadiButton);
	      //radio button-2
	    	minorRevRadiButton = new Button(parent, SWT.RADIO);
	    	minorRevRadiButton.setText("Create Minor Revision");
	    	minorRevRadiButton.setFont(JFaceResources.getDialogFont());
	    	minorRevRadiButton.setData(new Integer(3));
	    	minorRevRadiButton.addSelectionListener(new SelectionAdapter() {
	    		
	            public void widgetSelected(SelectionEvent event)
	            {
	            	iRevType=((Integer)event.widget.getData()).intValue();
	            	okButton.setEnabled(true);
	            	
	            }
	
	        });
	        setButtonLayouts(minorRevRadiButton);
    	}
    	
    	//ok button
        okButton = new Button(parent,SWT.PUSH);
        okButton.setText("OK");
        okButton.setData(new Integer(0));
        //button.setSize(15, 5);
      
        okButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent event)
            {
                buttonPressed(((Integer)event.widget.getData()).intValue());
            }

        });
      
        
        setButtonLayouts(okButton);
        //cancel button
        
        Button cancelButton = new Button(parent, SWT.PUSH);
        cancelButton.setText("Cancel");
        cancelButton.setData(new Integer(1));
        
        cancelButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent event)
            {
                buttonPressed(((Integer)event.widget.getData()).intValue());
            }

        });
        
       
        setButtonLayouts(cancelButton);
       
    }
    protected void setButtonLayouts(Button button)
    {
        GridData data = new GridData(250);
        int widthHint = convertHorizontalDLUsToPixels(60);
        Point minSize = button.computeSize(-1, -1, true);
        data.widthHint = Math.max(widthHint, minSize.x);
        button.setLayoutData(data);
    }
  public  String getRevType()
  {
	 if(iRevType == 2)
		 return "Create Major Revision";
	 else if(iRevType == 3)
	 {
		 return "Create Minor Revision";
	 }
	 else
		 return "";
		 
  }
  
    //7804-end
    protected Point getInitialSize() 
    {        
        Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;

        int width = ((size.width )/4) + (rectwidth / 4);
        int height = ((size.height ) / 2);
        
        return new Point(width, height);
    }
    public void populateItemTable(Object[] ecrs)
    {
        m_itemSelectionComposite.populateItemTable(ecrs);
    }
    public NOVCustomTable getItemSelectionTable()
    {
        return m_itemSelectionComposite.getItemSelectionTable();
    }
    public Map<String, Vector<TCComponent>> getCRRevInfo()
    {
        NOVCustomTable itemSelectionTable = getItemSelectionTable();
        Map<String, Vector<TCComponent>> crItemRevMap = new HashMap<String, Vector<TCComponent>>();
        int iNoOfRows = itemSelectionTable.getRowCount();
        NOVCustomTableLine itemTableLine = null;
        for(int indx=0;indx<iNoOfRows;indx++)
        {
            boolean bChkBoxValue = (Boolean)itemSelectionTable.getValueAt(indx, 0);
            if(bChkBoxValue)
            {
                AIFTableLine tableLine = itemSelectionTable.getRowLine(indx);
                itemTableLine = (NOVCustomTableLine)tableLine;
                TCComponent revObject = itemTableLine.getTableLineRevObject();
                TCComponent formObject = itemTableLine.getTableLineFormObject();
                
                String crStr = null;
                try
                {
                    crStr = revObject.getTCProperty("object_string").getStringValue()+"::"+itemSelectionTable.getValueAt(indx, 3);
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Vector<TCComponent> tarCompVec = new Vector<TCComponent>();
                tarCompVec.add(0, (TCComponent) revObject);
                tarCompVec.add(1, formObject);
                crItemRevMap.put(crStr, tarCompVec);
            }
        }
        return crItemRevMap;
    }
    
    private void setTitle() 
    {
		String strDialogTitle = m_reg.getString("ItemSelectionDialogTitle.MSG");
		this.getShell().setText(strDialogTitle);
	}
}
