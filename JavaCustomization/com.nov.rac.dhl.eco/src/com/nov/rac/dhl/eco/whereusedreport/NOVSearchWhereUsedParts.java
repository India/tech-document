package com.nov.rac.dhl.eco.whereusedreport;

import java.util.HashMap;
import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;

public class NOVSearchWhereUsedParts implements INOVSearchProvider
{
    private     TCComponent  m_itemRevWhereUsed; 
    HashMap<String, Vector<Object>> mapUsedParts = new HashMap<String, Vector<Object>>();
    private Vector<String> m_whereUsedItemsRowData = null;
    private int m_whereUsedItemsRowCount            = 0;
    private int m_whereUsedItemsColCount            = 0;
    
    public NOVSearchWhereUsedParts(TCComponent selectedComp)
    {
        super();
        this.m_itemRevWhereUsed = selectedComp;
    }

    public HashMap<String, Vector<Object>> executeWhereUsedPart()
    {
        if(m_itemRevWhereUsed == null)
        {
            return null;
        }
        
        INOVSearchProvider searchService = this;
        INOVSearchResult searchResult = null;
        try
        {
            searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL);
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }               
             
        if(searchResult != null && searchResult.getResponse().nRows > 0)
        {
            INOVResultSet theResSet = searchResult.getResultSet();
            m_whereUsedItemsRowData = theResSet.getRowData();
            m_whereUsedItemsRowCount = theResSet.getRows();
            m_whereUsedItemsColCount = theResSet.getCols();
            
            populateWhereUsedTable();
        }
        
        return mapUsedParts;
    }


    public void populateWhereUsedTable()
    {        
        try 
        {
            
            if(m_whereUsedItemsRowData == null)
            {
                return;
            }
            
            for (int i = 0; i < m_whereUsedItemsRowCount; i++) 
            {   
                int rowIndex = m_whereUsedItemsColCount * i;
                String itemId = m_whereUsedItemsRowData.get(rowIndex + 1); 
                String itemName = m_whereUsedItemsRowData.get(rowIndex + 2);
                String itemRevId = m_whereUsedItemsRowData.get(rowIndex + 3);
                String itemSeq = m_whereUsedItemsRowData.get(rowIndex + 4);
                String itemQty= m_whereUsedItemsRowData.get(rowIndex + 5);
                String lifeCycle = m_whereUsedItemsRowData.get(rowIndex + 6);
                String sites = m_whereUsedItemsRowData.get(rowIndex + 7);
                String strPartSubType = m_whereUsedItemsRowData.get(rowIndex + 8);
                                
                Vector<Object> rowData = new Vector<Object>();
                rowData.add(itemId); 
                rowData.add(itemName);
                rowData.add(itemRevId);     
                rowData.add(strPartSubType);                    
                rowData.add(sites);  
                rowData.add(lifeCycle);     
                rowData.add(itemSeq);  
                if(Double.valueOf(itemQty) < 0)
                    rowData.add(""); 
                else
                    rowData.add(itemQty); 
                
                mapUsedParts.put(itemId,rowData);
            } 
            
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        } 
        
        return;
    }
    



    @Override
    public String getBusinessObject()
    {
        return "Nov4Part";
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        String strItemPUID = null;
        //7301, including ItemRevPuid as well
        String strItemRevPUID = null;
        try
        {
            strItemPUID = ((TCComponentItemRevision) m_itemRevWhereUsed).getItem().getUid();
            //7301, including ItemRevPuid as well
            strItemRevPUID = ((TCComponentItemRevision) m_itemRevWhereUsed).getUid();
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[3];
        
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_typed_reference;
        bindVars[0].nVarSize = 1;
        //7301, including ItemRevPuid as well
        bindVars[0].strList = new String[]{strItemPUID, strItemRevPUID};
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[]{"Nov4Part"};
        
        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[]{"Non-Engineering"};
        
        return bindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[6];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-where-used-parts";  
        allHandlerInfo[0].listBindIndex = new int[] {1};                
        allHandlerInfo[0].listReqdColumnIndex = new int[]{1,2,3,4,5}; 
        allHandlerInfo[0].listInsertAtIndex = new int[] {1,2,3,4,5}; 
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-release-status";
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] {2};
        allHandlerInfo[1].listInsertAtIndex = new int[] {6};
        
        /*allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-DH-sites-props-engg-nonengg";
        allHandlerInfo[2].listBindIndex = new int[0];
        allHandlerInfo[2].listReqdColumnIndex = new int[] {1};
        allHandlerInfo[2].listInsertAtIndex = new int[] {7};*/
        
        allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-DH-sites-props";
        allHandlerInfo[2].listBindIndex = new int[]{2};
        allHandlerInfo[2].listReqdColumnIndex = new int[] {1};
        allHandlerInfo[2].listInsertAtIndex = new int[] {7};
        
        allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-DH-sites-props";
        allHandlerInfo[3].listBindIndex = new int[]{3};
        allHandlerInfo[3].listReqdColumnIndex = new int[] {1};
        allHandlerInfo[3].listInsertAtIndex = new int[] {7};
        
        /*allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-DH-master-props-eng-nonengg";
        allHandlerInfo[4].listBindIndex = new int[0];
        allHandlerInfo[4].listReqdColumnIndex = new int[] { 1 }; // PartSubType
        allHandlerInfo[4].listInsertAtIndex = new int[] { 8 };*/ // PartSubType
        
        allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-DH-master-props";
        allHandlerInfo[4].listBindIndex = new int[]{2};
        allHandlerInfo[4].listReqdColumnIndex = new int[] { 1 }; // PartSubType
        allHandlerInfo[4].listInsertAtIndex = new int[] { 8 }; // PartSubType
        
        allHandlerInfo[5] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[5].handlerName = "NOVSRCH-get-DH-master-props";
        allHandlerInfo[5].listBindIndex = new int[]{3};
        allHandlerInfo[5].listReqdColumnIndex = new int[] { 1 }; // PartSubType
        allHandlerInfo[5].listInsertAtIndex = new int[] { 8 }; // PartSubType
        
        return allHandlerInfo ;
    }
    
}
