package com.nov.rac.dhl.eco.wizard;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;

public class NOVECOWizard extends Wizard
{
    CreInitChangeObjPage initPage;
    CRSelectionPage creChObjPage;
    NovNovelSelectionPage novelProjectPage;
    ReasonforChPage reason4ChangePage;
    CRItemAdditionPage addItemPage;
    TCUserService userService;
    Registry registry = Registry.getRegistry(this);
    
    public NOVECOWizard(Shell shell)
    {
        super.setWindowTitle("Engineering Change Object Creation");
        // Add the pages
        initPage = new CreInitChangeObjPage();
        creChObjPage = new CRSelectionPage();
        novelProjectPage = new NovNovelSelectionPage();
        reason4ChangePage = new ReasonforChPage();
        addItemPage = new CRItemAdditionPage();
        
        addPage(initPage);
        addPage(creChObjPage);
        addPage(novelProjectPage);
        addPage(addItemPage);
        addPage(reason4ChangePage);
        
        Image novimage = new Image(shell.getDisplay(), getClass().getResourceAsStream("nov.png"));
        setDefaultPageImageDescriptor(ImageDescriptor.createFromImage(novimage));
        
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        userService = session.getUserService();
    }
    
    @Override
    public void createPageControls(Composite pageContainer)
    {
        super.createPageControls(pageContainer);
        
        creChObjPage.crSelectpanel.registerObserver(addItemPage.itemAdditionPanel);
        
        addItemPage.itemAdditionPanel.registerObserver(creChObjPage.crSelectpanel);
        
    }
    
    @Override
    public boolean performCancel()
    {
        TCComponentForm changeform = initPage.initialpanel.getChangeForm(true);
        TCComponent[] forms = new TCComponent[1];
        forms[0] = changeform;
        if (changeform != null)
        {
            try
            {
                AIFComponentContext[] relComps = changeform.whereReferenced();
                Vector<TCComponent> datasets = new Vector<TCComponent>();
                for (int i = 0; i < relComps.length; i++)
                {
                    if (relComps[i].getContext().equals("_CustomSubForm_"))
                    {
                        datasets.add((TCComponent) relComps[i].getComponent());
                    }
                }
                if (datasets.size() > 0)
                {
                    TCComponent[] tcDatasets = datasets.toArray(new TCComponent[datasets.size()]);
                    
                    // changeform.cutOperation("_CustomSubForm_", tcDatasets);
                    for (int i = 0; i < tcDatasets.length; i++)
                    {                        
                        DeleteRelationHelper delete = new DeleteRelationHelper(
                        		tcDatasets[i],changeform, "_CustomSubForm_");
                        delete.deleteRelation();
                        
                        tcDatasets[i].delete();
                    }
                }
                // once all the datasets are deleted then change form is deleted
                changeform.delete();
                
                return true;
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        return true;
    }
    
    @Override
    public boolean performFinish()
    {
        if (isMandatoryValuesFilled())
        {
        	//Map<String, String> mapPropertyECO = new HashMap<String, String>();//KLOC2139-commented  
        	Map<String, String> itemsWithRevisionIdMap=new HashMap<String, String>();//7804
        	Map<String, TCComponent> itemIdsWithRevIdMap=new HashMap<String, TCComponent>();//7804
            //String reason4ChgStr = "";//KLOC2139-commented 
            //String sRevType=null;//7804
            Map<String, String> mapItemsWithRevType = null;//7804
            // String comments = "";//soma
          //KLOC2139-commented 
           /* if (reason4ChangePage.reasonPanel.reasonforChange.getText().trim().length() == 0)
            {
                reason4ChgStr = creChObjPage.crSelectpanel.getReason4ChangeString();
            }
            else
            {
                reason4ChgStr = reason4ChangePage.reasonPanel.reasonforChange.getText();
            }*/
            
            TCComponent cECOForm = initPage.initialpanel.getChangeForm(false);
            TCComponent[] caTargetItems = addItemPage.itemAdditionPanel.getTargetComponents();
            //7804-start
            for(int i=0;i<caTargetItems.length;i++)
            {
            	try {
            		TCComponentItemRevision itemRev=(TCComponentItemRevision)caTargetItems[i];
					String itemId=caTargetItems[i].getProperty("item_id");
					itemIdsWithRevIdMap.put(itemId, itemRev);
				  
				} catch (TCException e) {
					
					e.printStackTrace();
				}
            }
            //7804-end
            //7804-revType,mapItemsWithRevType should be for only General ECO
            if( initPage.initialpanel.getTypeOfChange().equalsIgnoreCase("General") )
            {
            	//sRevType=addItemPage.itemAdditionPanel.getRevisionType();//7804
                mapItemsWithRevType=addItemPage.itemAdditionPanel.getItemRevTypeMap();//7804
                itemsWithRevisionIdMap=getRevisionIdsMap(mapItemsWithRevType,itemIdsWithRevIdMap);//7804
            }
            
            TCComponent[] caSelectedECRs = creChObjPage.crSelectpanel.getSelectedCRComponents();
            TCComponent[] caSelectedNovelForms =  novelProjectPage.m_novelSearchPanel.getSelectedFormsArray();
            //Vector<TCComponentForm> selforms = getSelectedForms();  
            //KLOC2139-commented  
           /* mapPropertyECO.put("changecat", initPage.initialpanel.getChangeCategory());
            mapPropertyECO.put("reasoncode", initPage.initialpanel.getReasonCode());
            mapPropertyECO.put("group", initPage.initialpanel.getGroup());
            mapPropertyECO.put("reasonforchange", reason4ChgStr);
            mapPropertyECO.put("changetype", initPage.initialpanel.getTypeOfChange());
            mapPropertyECO.put("wfname", initPage.initialpanel.getWorkFlow());
            mapPropertyECO.put("additionalcomments", reason4ChangePage.reasonPanel.addtnlCommentsText.getText());
            mapPropertyECO.put("nov4_product_line", initPage.initialpanel.getProductLine());
            mapPropertyECO.put("nov4_ecoclassification", initPage.initialpanel.getECOClassification());*/
            
            Map<String, String> mapPropertyECO=fillInputMap();//KLOC2139
            this.getShell().setCursor(new Cursor(this.getShell().getDisplay(), SWT.CURSOR_WAIT));
            	NOVCreateECOHelper helperObj = new NOVCreateECOHelper();
            	//7804-added two more input arguments(sRevType,mapItemsWithRevType) to the below function call
            	helperObj.createECO(cECOForm, caTargetItems, caSelectedECRs,caSelectedNovelForms, mapPropertyECO,itemsWithRevisionIdMap);
            	
            	if(helperObj.isAllCreateECOSuccess())
            	{  
            		try
            		{				    
            			MessageBox.post(initPage.initialpanel.getWorkFlow() + " process has been initiated.\n"
            					+ cECOForm.getProperty("object_string")
            					+ " has been created and placed in your Worklist.\n"
            					+ "Please go to your Worklist (Inbox) to assign users and move the workflow forward.",
            					"Info", MessageBox.INFORMATION);

            			return true;
            		}
            		catch (Exception e)
            		{
            			MessageBox.post("Could not update ECO form property, distribution", "Error", MessageBox.ERROR);
            			//e.printStackTrace();
            			return true;
            		}
            	}

            this.getShell().setCursor(new Cursor(this.getShell().getDisplay(), SWT.CURSOR_ARROW));
        }
        return false;
    }
    //KLOC2139
    public Map<String, String> fillInputMap()
    {
        Map<String, String> mapPropertyECO = new HashMap<String, String>();  
        String reason4ChgStr = "";
        if (reason4ChangePage.reasonPanel.reasonforChange.getText().trim().length() == 0)
        {
            reason4ChgStr = creChObjPage.crSelectpanel.getReason4ChangeString();
        }
        else
        {
            reason4ChgStr = reason4ChangePage.reasonPanel.reasonforChange.getText();
        }
        mapPropertyECO.put("changecat", initPage.initialpanel.getChangeCategory());
        mapPropertyECO.put("reasoncode", initPage.initialpanel.getReasonCode());
        mapPropertyECO.put("group", initPage.initialpanel.getGroup());
        mapPropertyECO.put("reasonforchange", reason4ChgStr);
        mapPropertyECO.put("changetype", initPage.initialpanel.getTypeOfChange());
        mapPropertyECO.put("wfname", initPage.initialpanel.getWorkFlow());
        mapPropertyECO.put("additionalcomments", reason4ChangePage.reasonPanel.addtnlCommentsText.getText());
        mapPropertyECO.put("nov4_product_line", initPage.initialpanel.getProductLine());
        mapPropertyECO.put("nov4_ecoclassification", initPage.initialpanel.getECOClassification());
        
       return  mapPropertyECO;
    }
    //7804-test-start
    public Map<String , String> getRevisionIdsMap(Map<String,String> itemsWithRevTypeMap,Map<String,TCComponent> itemIdsWithRevIdsMap)
    {
    	Map<String,String> itemsIdsWithNextMajorRevIds= new HashMap<String, String>();
    	if(itemsWithRevTypeMap !=null)
    	{
    	    Object[] keySet=itemsWithRevTypeMap.keySet().toArray();
            NOVCreateECOHelper helperObj = new NOVCreateECOHelper();
            for(int i=0;i<keySet.length;i++ )
            {
                String revType=itemsWithRevTypeMap.get(keySet[i]);
                
                if(revType.equalsIgnoreCase("Create Major Revision"))
                {
                    TCComponent revId=itemIdsWithRevIdsMap.get(keySet[i]);
                    //KLOC2141-commented below lines of code 
                    /*if(revId!=null)
                    {
                        Vector<TCComponent> partFamilyComps = null;
                        try
                        {
                            partFamilyComps = NOVECOHelper.getAllPartFamilyMembers(revId);
                            if(!partFamilyComps.contains(revId))
                                partFamilyComps.add(revId);
                        }
                        catch (TCException e)
                        {
                            e.printStackTrace();
                        }
                        String suggestedRevId=helperObj.getNextMajorRevisionId(partFamilyComps);*/
                        String suggestedRevId=getMajorRevId(revId,helperObj);// //KLOC2141-commented above lines of code and added new fun call
                        itemsIdsWithNextMajorRevIds.put(keySet[i].toString(), suggestedRevId);  
                    //}
                }
                else if(revType.equalsIgnoreCase("Create Minor Revision"))
                {
                    TCComponent compRev=itemIdsWithRevIdsMap.get(keySet[i]);
                    /*if(compRev!=null)
                    {
                        String revId= null;
                        try
                        {
                            revId = compRev.getProperty("current_revision_id").toString();
                        }
                        catch (TCException e)
                        {
                            e.printStackTrace();
                        }
                        String suggestedRevId=helperObj.getNextMinorRevisionId(revId);*/
                        String suggestedRevId= getMinorRevId(compRev,helperObj);////KLOC2141-commented above lines of code and added new fun call
                        itemsIdsWithNextMajorRevIds.put(keySet[i].toString(), suggestedRevId);
                   // }
                }
            }
    	}
    	return itemsIdsWithNextMajorRevIds;
    }
    //KLOC2141-start
    private String getMajorRevId(TCComponent comRev,NOVCreateECOHelper helperObj )
    {
        if(comRev!=null)
        {
            Vector<TCComponent> partFamilyComps = null;
            try
            {
                partFamilyComps = NOVECOHelper.getAllPartFamilyMembers(comRev);
                if(!partFamilyComps.contains(comRev))
                    partFamilyComps.add(comRev);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            return helperObj.getNextMajorRevisionId(partFamilyComps);
        }
        else
            return "";
    }
    private String getMinorRevId(TCComponent compRev,NOVCreateECOHelper helperObj)
    {
        if(compRev!=null)
        {
            String revId= null;
            try
            {
                revId = compRev.getProperty("current_revision_id").toString();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            return helperObj.getNextMinorRevisionId(revId);
        }
        else
            return "";
    }
  //KLOC2141-end
    //7804-test-end
    private String getAddressList(String typeofChange, String workflow, String reasonCode)
    {
        String addressList = null;
        if (typeofChange.equalsIgnoreCase("General") && (workflow.equalsIgnoreCase("RH Document Release"))
                && (reasonCode.length() > 0 && reasonCode.equalsIgnoreCase("Waiver/Deviation")))
        {
            addressList = registry.getString(typeofChange + "|" + workflow + "|" + reasonCode);
        }
        else
        {
            addressList = registry.getString(typeofChange + "|" + workflow);
        }
        
        /*
         * if (typeofChange.equalsIgnoreCase("Lifecycle")&&
         * (workflow.equalsIgnoreCase
         * ("RH Lifecycle Change Process")||workflow.equalsIgnoreCase
         * ("RH Lifecycle Process"))&& (reasonCode.length()>0)) { addressList =
         * "3c � ReedHycalog:Lifecycle Chg"; } else
         * if(typeofChange.equalsIgnoreCase("General")&&
         * (workflow.equalsIgnoreCase("RH CMT"))&& (reasonCode.length()>0)) {
         * addressList = "3C � ReedHycalog:CMT Notify"; } else
         * if(typeofChange.equalsIgnoreCase("General")&&
         * (workflow.equalsIgnoreCase("RH Type 1 Release"))&&
         * (reasonCode.length()>0)) { addressList =
         * "3C � ReedHycalog:Type 1 Notify"; } else
         * if(typeofChange.equalsIgnoreCase("General")&&
         * (workflow.equalsIgnoreCase("RH Type 2 Release"))&&
         * (reasonCode.length()>0)) { addressList =
         * "3C � ReedHycalog:Type 2 Notify"; } else
         * if(typeofChange.equalsIgnoreCase("General")&&
         * (workflow.equalsIgnoreCase("RH Type 3 Release"))&&
         * (reasonCode.length()>0)) { addressList =
         * "3C � ReedHycalog:Type 3 Notify"; } else
         * if(typeofChange.equalsIgnoreCase("General")&&
         * (workflow.equalsIgnoreCase("RH Document Release"))&&
         * (reasonCode.length()>0 &&
         * !reasonCode.equalsIgnoreCase("Waiver/Deviation"))) { addressList =
         * "3C � ReedHycalog:Docs Notify"; } else
         * if(typeofChange.equalsIgnoreCase("General")&&
         * (workflow.equalsIgnoreCase("RH Document Release"))&&
         * (reasonCode.equalsIgnoreCase("Waiver/Deviation"))) { addressList =
         * "3C � ReedHycalog:Waivers/Devs"; }
         */
        return addressList;
    }
    
    private boolean isMandatoryValuesFilled()
    {
        if ((!(initPage.initialpanel.getTypeOfChange().toString().length() > 0)))
        {
            MessageBox.post("Select type of change.", "Warning", MessageBox.WARNING);
        }
        else if ((!(initPage.initialpanel.getWorkFlow().toString().length() > 0)))
        {
            MessageBox.post("Select workflow template.", "Warning", MessageBox.WARNING);
        }
        else if ((!(initPage.initialpanel.getChangeCategory().toString().length() > 0)))
        {
            MessageBox.post("Select Change Category.", "Warning", MessageBox.WARNING);
        }
        else if ((!(initPage.initialpanel.getReasonCode().toString().length() > 0)))
        {
            MessageBox.post("Select Reason Code.", "Warning", MessageBox.WARNING);
        }
        // Rakesh - 27/12/2011 - DHL ECO 2.0 Product Line mandatory check
        else if ((!(initPage.initialpanel.getProductLine().toString().length() > 0)))
        {
            MessageBox.post("Select Product Line.", "Warning", MessageBox.WARNING);
        }
        else if ((!(initPage.initialpanel.getGroup().toString().length() > 0)))
        {
            MessageBox.post("Select Group.", "Warning", MessageBox.WARNING);
        }
        else if ((!(reason4ChangePage.reasonPanel.addtnlCommentsText.getText().trim().length() > 0))) // Rakesh
                                                                                                      // -
                                                                                                      // Added
                                                                                                      // check
                                                                                                      // for
                                                                                                      // Additional
                                                                                                      // reason
                                                                                                      // for
                                                                                                      // change
        {
            MessageBox.post("Enter Additional Reason for Change.", "Warning", MessageBox.WARNING);
        }
        /*
         * else if(((creChObjPage.crSelectpanel.getSelectedCRs().length == 0))
         * && ((addItemPage.itemAdditionPanel.getTargetItems().length==0))) {
         * MessageBox.post(
         * "Select Change Request(s) or Problem Item(s) to initiate the Change process."
         * , "Warning", MessageBox.WARNING); }
         */
        else
        {
            return true;
        }
        return false;
    }
    
    /***************** Rakesh - 10/11/2011 - DHL-ECO-208 development ***********************/
    /************* Disable Finish button if mandatory fields are not filled ****************/
    @Override
    public boolean canFinish()
    {
        return (initPage.initialpanel.isMandatoryValuesFilled() && reason4ChangePage.reasonPanel
                .isMandatoryValuesFilled());
    }
    /***************************************************************************************/
}
