package com.nov.rac.dhl.eco.attrUpdate;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import com.nov.rac.utilities.services.ExecuteExternalCommandHelper;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCComponentUnitOfMeasure;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVUpdateUOMDialog extends AbstractSWTDialog
{
	
	private 	TCComponentItem 		m_item;
	private		TCComponent 			m_target;
	private     String					m_strSelected	="";	
	private     String					m_selItemId;
	private 	String					m_path;
	private 	TCComponentUnitOfMeasure m_uomValue = null;
	private     Vector<Object>   m_uomvector = new Vector<Object>();
	private		Label	m_attrLabel;
	private		Label	m_newLabel;
	private		Label	m_itemLabel;
	private 	Label 	m_curUOMLabel;
	private 	Label   m_newUOMLabel;
	private     Text 	m_selItem;
	private		Text    m_curUOMText;	
	private 	Combo 	m_newUOM  ;
	private	    Map<String, String> m_uomMap = new HashMap<String, String>();
	private 	Registry  	m_reg = Registry.getRegistry(this);

	public NOVUpdateUOMDialog(Shell shell, TCComponent tcComponent, TCComponentItem item) 
	{
		// TODO Auto-generated constructor stub
		super(shell);
		m_item 			= item;	
		m_target		= tcComponent;
		setShellStyle(SWT.TITLE | SWT.BORDER 
	    		  | SWT.APPLICATION_MODAL | SWT.CENTER | SWT.ICON_INFORMATION |SWT.CLOSE);
		
	}

	@Override
	protected void cancelPressed() {
		// TODO Auto-generated method stub
		boolean yes = MessageDialog.openQuestion( AIFUtility.getActiveDesktop().getShell(),
				m_reg.getString("cancel.TITLE"), m_reg.getString("cancel.MSG") );	
		if(yes)
        {
        	super.cancelPressed();
        }
		//super.cancelPressed();
	}



	@Override
	protected Control createDialogArea(Composite parent) {
		// TODO Auto-generated method stub	
		
		setTitle();
		this.getShell().setLayout(new GridLayout(1, true));
		try
		{ 
		    //display attributes
		    final Composite propComp = new Composite(parent, SWT.BORDER); 
		    propComp.setLayout(new GridLayout(2, false));
		    propComp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,1,1));    
		  
		    m_attrLabel = new Label(propComp, SWT.NULL);
		    m_newLabel = new Label(propComp, SWT.NULL);	
		    m_itemLabel = new Label(propComp, SWT.NULL);
			m_selItem = new Text(propComp,SWT.BORDER);
		    m_curUOMLabel = new Label(propComp, SWT.NULL);
		    m_curUOMText = new Text(propComp,SWT.BORDER);	
		    m_newUOMLabel = new Label(propComp, SWT.NULL);			
			m_newUOM = new Combo(propComp,SWT.DROP_DOWN);	
			m_selItem.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,1,1));
			m_curUOMText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,1,1));
			
		    setLabels();
		    setText();
		    setFont();			
			populateLov();
			addListeners();
		 }
		 catch (Exception e) {
				// TODO: handle exception
		    	e.printStackTrace();
			}
		
		return parent ;
	}

	protected void okPressed() 
	{
	 	update();

		super.okPressed();
	}
	
	private void getUOMValue(TCComponentItem item, String selectedStr) {
		// TODO Auto-generated method stub

	   	try
	   	{

			//boolean isUOMDef = false;
			String uomDesc =selectedStr;
			String uomSymbol = m_uomMap.get(uomDesc);
			TCComponentType UOMType = m_target.getSession().getTypeService().getTypeComponent("UnitOfMeasure");
			TCComponent[] uomList = UOMType.extent();
			//uomValue = (TCComponentUnitOfMeasure) uomList[index];
			//TCProperty uom = item.getTCProperty("uom_tag");
				for (int index = 0; index < uomList.length; index++) 
				{
					
					if (uomList[index].getProperty("symbol").toString().equals(uomSymbol))					
						m_uomValue = (TCComponentUnitOfMeasure) uomList[index];
				}
		
	   	}
	   	catch(Exception ex1)
	   	{
	   		ex1.printStackTrace();
	   	}
		
	}

	//set lov for new combo
	private void populateLov( ) {
		
		TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName(m_target.getSession(), "_RSOne_UnitsOfMeasure_");
		if (lovValues != null)
		{
			ListOfValuesInfo lovInfo = null;
			try {
				lovInfo = lovValues.getListOfValues();
			} catch (TCException e) {
				e.printStackTrace();
			}
			Object[] lovS = lovInfo.getListOfValues();
			String[] lovDesc = lovInfo.getDescriptions();
			if (lovS.length > 0) {
				for (int i = 0; i < lovS.length; i++) {
					String[] splitDesc = null;
					if (lovDesc != null && lovDesc[i] != null) {
						splitDesc = lovDesc[i].split(",");
					}
					for (int inx = 0; inx < splitDesc.length; inx++) {
						if (splitDesc != null
								&& splitDesc[inx].trim().equalsIgnoreCase(m_reg.getString("downhole.NAME"))
								|| splitDesc[inx].trim().equalsIgnoreCase("")) 
						{
							m_uomvector.add(lovS[i]);
							m_uomMap.put(lovS[i].toString(), splitDesc[0]);
							break;
						}
					}
				}
			}
		}
		String []struomVector =(String[]) m_uomvector.toArray(new String[m_uomvector.size()]);
		m_newUOM.setItems(struomVector);
		
	}



	protected void update() 
	{
		// TODO Auto-generated method stub
		if(m_strSelected!=null && m_strSelected.length()>0)
		{
			m_path = m_reg.getString("ExePath");			
			ExecuteExternalCommandHelper executeExternalCommandHelper = new ExecuteExternalCommandHelper();
	
			try {
				executeExternalCommandHelper.executeExternalCommand(m_path + " " + "\"" + m_selItemId + "\"" + " " + m_uomValue, ExecuteExternalCommandHelper.MODE_SYNCHRONOUS);
				m_item.refresh();
				
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	private void setTitle() 
	{
		String title =m_reg.getString("updateUOM.TITLE");		
		this.getShell().setText(title);		
	}
	private void setLabels() 
	{
		m_attrLabel.setText(m_reg.getString("attribute.NAME"));
		m_newLabel.setText(m_reg.getString("value.NAME"));
		m_itemLabel.setText(m_reg.getString("selectedItem.NAME"));
		m_curUOMLabel.setText(m_reg.getString("currentUOM.NAME"));
		m_newUOMLabel.setText(m_reg.getString("newUOM.NAME"));
		
	}
	private void setText() 
	{
		try
		{
			if(m_target!=null)
			{
				m_selItemId = m_target.getProperty("item_id").toString();
				m_selItem.setText(m_selItemId);
				String curUOMValue = m_target.getProperty(m_reg.getString("uomProperty.NAME"));		    		
			    m_curUOMText.setText(curUOMValue);
							
			}
			m_selItem.setTextLimit(30); 
			m_selItem.setEditable(false); 
			m_curUOMText.setTextLimit(30);
			m_curUOMText.setEditable(false); 
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void setFont() 
	{
		m_attrLabel.setFont(new Font(this.getShell().getDisplay(), "", 8,1));
		m_newLabel.setFont(new Font(this.getShell().getDisplay(), "", 8,1));
		m_itemLabel.setFont(new Font(this.getShell().getDisplay(), "", 8,1));
		m_newUOMLabel.setFont(new Font(this.getShell().getDisplay(), "", 8,1));
		m_curUOMLabel.setFont(new Font(this.getShell().getDisplay(), "", 8,1));
		
	}
	
	private void addListeners() 
	{
		m_newUOM.addSelectionListener(new SelectionListener() {				
					
					public void widgetSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub
						if(m_newUOM.getSelectionIndex()>-1)
						{
							m_strSelected = m_newUOM.getText();
							if(m_strSelected!=null && m_item!=null )
							{
								getUOMValue(m_item,m_strSelected);
							}
						}
					}
					
					
					public void widgetDefaultSelected(SelectionEvent arg0) 
					{
						// TODO Auto-generated method stub
						m_newUOM.setText("");
						
					}
				});
	}
}
