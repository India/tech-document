package com.nov.rac.dhl.eco.whereusedreport;

import java.util.HashMap;
import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NOVSearchWhereUsedDocumentRev implements INOVSearchProvider {

	 private     TCComponent  m_itemRevWhereUsed; 
	 HashMap<String, Vector<Object>> mapUsedParts = new HashMap<String, Vector<Object>>();
	 private Vector<String> m_whereUsedItemsRowData = null;
	 private int m_whereUsedItemsRowCount            = 0;
	 private int m_whereUsedItemsColCount            = 0;
	    
	 public NOVSearchWhereUsedDocumentRev(TCComponent selectedComp)
	 {
	       super();     
	       this.m_itemRevWhereUsed = selectedComp;
	 }
	 
	 public HashMap<String, Vector<Object>> executeWhereUsedPart()
	 {
		 if(m_itemRevWhereUsed == null)
	     {
			 return null;
	     }
	        
	     INOVSearchProvider searchService = this;
	     INOVSearchResult searchResult = null;
	     try
	     {	    	         
	    	 searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL);
	    	 
	     }
	     catch (Exception e) 
	     {
	         e.printStackTrace();
	     }               
	             
	     if(searchResult != null && searchResult.getResponse().nRows > 0)
	     {
	    	 INOVResultSet theResSet = searchResult.getResultSet();
	         m_whereUsedItemsRowData = theResSet.getRowData();
	         m_whereUsedItemsRowCount = theResSet.getRows();
	         m_whereUsedItemsColCount = theResSet.getCols();
	            
	         populateWhereUsedTable();
	      }
	        
	     return mapUsedParts;
	  }
	 
	    public void populateWhereUsedTable()
	    {        
	        try 
	        {
	            
	            if(m_whereUsedItemsRowData == null)
	            {
	                return;
	            }
	            
	            String itemId = null;
	            for (int i = 0; i < m_whereUsedItemsRowCount; i++) 
	            {   
	                int rowIndex = m_whereUsedItemsColCount * i;
	                String itemPuid = m_whereUsedItemsRowData.get(rowIndex + 0);	
	                if(!itemPuid.isEmpty())
	                {
	                	TCSession session =  (TCSession) AIFUtility.getCurrentApplication().getSession();
	                	TCComponent item = session.stringToComponent(itemPuid);
	                	itemId = item.getProperty("item_id");
	                	 //itemId = itemPuid;
	                }
	                String itemName = m_whereUsedItemsRowData.get(rowIndex + 1);
	                String itemRevId = m_whereUsedItemsRowData.get(rowIndex + 2);  
	                String itemSeq = null;
	                String itemQty= null;
	                String lifeCycle =null;;
	                String sites = null;;
	                String strPartSubType = null;
	               
	                                
	                Vector<Object> rowData = new Vector<Object>();
	                rowData.add(itemId); 
	                rowData.add(itemName);
	                rowData.add(itemRevId);    
	                rowData.add(strPartSubType);                    
	                rowData.add(sites);  
	                rowData.add(lifeCycle);     
	                rowData.add(itemSeq); 
	                rowData.add(itemQty);
	                
	                
	                mapUsedParts.put(itemId,rowData);
	            } 
	            
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        } 
	        
	        return;
	    }
	       
	    
	 
	 
	@Override
	public String getBusinessObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QuickBindVariable[] getBindVariables() {
		// TODO Auto-generated method stub
		
		String strItemRevPUID = null;
        try
        {        	        	
            strItemRevPUID = ((TCComponentItemRevision) m_itemRevWhereUsed).getItem().getUid();
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[2];   
              
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 2;
        bindVars[0].strList = new String[] { "RelatedDefiningDocument" ,"RelatedDocuments" };
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_typed_reference;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[]{strItemRevPUID};        
        
        return bindVars;
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo() {
		// TODO Auto-generated method stub
		
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = null;
		
		allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-partfamily";  
        allHandlerInfo[0].listBindIndex = new int[] {1,2};                
        allHandlerInfo[0].listReqdColumnIndex = new int[]{1,3}; 
        allHandlerInfo[0].listInsertAtIndex = new int[] {1,2}; 
        
        
		return allHandlerInfo;
	}

}
