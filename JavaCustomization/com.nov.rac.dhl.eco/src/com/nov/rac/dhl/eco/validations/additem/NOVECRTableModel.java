package com.nov.rac.dhl.eco.validations.additem;

import java.util.Hashtable;

import com.teamcenter.rac.common.TCTableModel;

public class NOVECRTableModel extends TCTableModel
{
    private static final long serialVersionUID = 1L;
	public NOVECRTableModel()
    {
        super();
    }

    public NOVECRTableModel(String[] data)
    {
        super(data);
    }

    public NOVECRTableModel(String as[][])
	{
	   super(as);	    
	}
    public NOVECRTableLine createLine(Object obj)
	{
    	NOVECRTableLine aiftableline = null;
		Object [] strObj = null;

		if(obj instanceof Object[])
		{
			strObj = ((Object [])obj);	
			if(strObj != null)
			{
				int i = strObj.length;
				int j = getColumnCount();
				Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
				for(int k = 0; k < j && k < i; k++)
				{
					if(strObj[k] != null)
					{
						hashtable.put(getColumnIdentifier(k), strObj[k]);
					}
				}       
				aiftableline = new NOVECRTableLine(hashtable);
			}
		}
		else 
		{
			aiftableline = (NOVECRTableLine) super.createLine(obj);
		}
		return aiftableline;
	} 

    protected NOVECRTableLine[] createLines(Object obj)
    {
    	NOVECRTableLine aaiftableline[] = null;

    	if( obj instanceof Object [][] )
        {
    		Object [][] objArrArr = (Object [][])obj;    		
            int i = objArrArr.length;
            aaiftableline = new NOVECRTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	NOVECRTableLine newLine = createLine( objArrArr[j] );
                aaiftableline[j] = newLine;
            }
        } 
    	else
    	{
    		aaiftableline = (NOVECRTableLine[]) super.createLines(obj);
    	}    	
    	return aaiftableline;
    }
}