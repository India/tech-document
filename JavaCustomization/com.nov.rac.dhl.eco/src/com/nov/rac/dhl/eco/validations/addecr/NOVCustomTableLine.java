package com.nov.rac.dhl.eco.validations.addecr;

import java.util.Hashtable;

import com.teamcenter.rac.common.TCTableLine;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVCustomTableLine extends TCTableLine 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String m_sTableLineToolTipText = "";
	private TCComponent m_itemRevComp = null;
	private TCComponent m_formComp = null;
	private boolean m_bLineEditableStatus = true;
	public NOVCustomTableLine(Hashtable<String, Object> hashtable)
	{
		super(hashtable);
	}
	public String getTableLineToolTipText()
	{
		return m_sTableLineToolTipText;
	}
	public void setTableLineToolTipText(String sLineText)
	{
	    m_sTableLineToolTipText = sLineText;
	}
	public TCComponent getTableLineRevObject()
	{
	    return m_itemRevComp;
	}
	public void setTableLineRevObject(TCComponent tcComponent)
	{
	    m_itemRevComp = tcComponent;
	}
	public TCComponent getTableLineFormObject()
    {
        return m_formComp;
    }
    public void setTableLineFormObject(TCComponent tcComponent)
    {
        m_formComp = tcComponent;
    }
    public boolean getTableLineEditableStatus()
    {
        return m_bLineEditableStatus;
    }
    public void setTableLineEditableStatus(boolean bStatus)
    {
        m_bLineEditableStatus = bStatus;
    }
}
