package com.nov.rac.dhl.eco.helper;

import java.util.logging.*;
import java.io.*;

public class NOVLogger {

public static Logger NOVLog;

static {
    try {
      boolean append = true;
      FileHandler fh = new FileHandler("c:\\temp\\TestLog.log", append);
      fh.setFormatter(new Formatter() {
         public String format(LogRecord rec) {
            StringBuffer buf = new StringBuffer(1000);
            buf.append(new java.util.Date());
            buf.append(' ');
            buf.append(rec.getLevel());
            buf.append(' ');
            buf.append(formatMessage(rec));
            buf.append('\n');
            return buf.toString();
            }
          });
      NOVLog = Logger.getLogger("NOVLogger");
      NOVLog.addHandler(fh);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
}
}

