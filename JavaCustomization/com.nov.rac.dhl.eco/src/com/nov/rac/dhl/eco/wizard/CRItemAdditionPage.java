package com.nov.rac.dhl.eco.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.noi.rac.dhl.eco.util.components.NOVCRItemSelectionPanel;
import com.teamcenter.rac.util.SWTUIUtilities;

public class CRItemAdditionPage extends WizardPage 
{
  public NOVCRItemSelectionPanel itemAdditionPanel;
  public static final String PAGE_NAME = "CRItemAdditionPage";
  public CRItemAdditionPage() 
  {
    super(PAGE_NAME);
    setTitle("Target Selection");//7786
	setMessage(" Search and add Additional Items to initiate change process. \n Right click on the target item to remove.");
  }

  public void createControl(Composite parent) 
  {
		Composite swtSwingComp = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
	    swtSwingComp.setSize(500, 350);
	    swtSwingComp.setLocation(20, 20);
	    itemAdditionPanel = new NOVCRItemSelectionPanel();
		SWTUIUtilities.embed(swtSwingComp, itemAdditionPanel,false);
	    setControl(swtSwingComp);
  }
}