package com.nov.rac.dhl.eco.whereusedreport.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.nov.rac.dhl.eco.helper.NOVOpenExcelOnTableExport;
import com.teamcenter.rac.aif.common.AIFTable;

public class ExportToExcelButtonListener implements ActionListener
{
    private AIFTable m_whereUsedTable;
    
    public ExportToExcelButtonListener(AIFTable table)
    {
        m_whereUsedTable = table;
    }
    
    @Override
    public void actionPerformed(ActionEvent actionevent)
    {
        NOVOpenExcelOnTableExport.getInstance().exportTableDataToExcel(m_whereUsedTable);
    }
}
