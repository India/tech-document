package com.nov.rac.dhl.eco.attrUpdate;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;


public class NOVDocumentCAT extends Composite
{
	//private String[] docSubTypeStrArr;
	private TCProperty prop;
	private TCSession session;
	

	protected CLabel m_Label = new CLabel(this, SWT.NONE);
	protected Text m_text = new Text( this, SWT.BORDER | SWT.LEFT );
	//protected CCombo  m_combo = new CCombo(this, SWT.DROP_DOWN | SWT.BORDER | SWT.LEFT);
	Color color = new Color(null, 255, 255, 255);

	public NOVDocumentCAT(Composite parent, TCSession session,TCProperty prop) 
	{
		//prop=prop;
		 super(parent,SWT.NONE);
		 this.prop = prop;
		 this.session=session;
		 //GridData gd_combo = new GridData(SWT.LEFT, SWT.LEFT, true, false, 1, 1);
		 m_text.setBackground(color);         
        // m_combo.setLayoutData(gd_combo);
		this.setLayout(new GridLayout(2, false));
		load();		
	}
	
	public void load() 
	{
		String propValue = getProperty().getStringValue();		
		m_text.setText(propValue);
		
		m_text.setEnabled(false);
	}
		
		
	protected TCProperty getProperty()
	{
		return prop;
	}
	protected TCSession getSession()
	{
		return session;
	}
	
	protected void savePropertyValue() 
	{
	
		
	    String strCurrSelectedValue = m_text.getText();
	    String[] splits =strCurrSelectedValue.split("-");
	    
	    if(splits[0] != null)
	    {
	    	try 
	    	{
				getProperty().setStringValue( splits[0] );
			} 
	    	catch (TCException e) 
	    	{			
				e.printStackTrace();
			}
	    }
		
	}
	
        
		
	
}
	


