package com.nov.rac.dhl.eco.validations.additem;

import java.util.Map;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVECRSelectionDialog extends AbstractSWTDialog
{
    private NOVECRSelectionComposite m_ecrSelectionComposite;
    private Registry m_reg;
    public NOVECRSelectionDialog(Shell parentShell)
    {
        super(parentShell);
        
        setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.CENTER);
        
        m_reg = Registry.getRegistry(this);
    }
    @Override
    protected Control createDialogArea(Composite parent)
    {
    	setTitle();
    	
        Composite dialogcomposite = new Composite(parent, SWT.NONE);        
        GridLayout gd = new GridLayout(1, false);
        dialogcomposite.setLayout(gd);
        dialogcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        m_ecrSelectionComposite = new NOVECRSelectionComposite(dialogcomposite,SWT.NONE);
        
        return parent;
    }
    protected Point getInitialSize() 
    {        
        Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;

        int width = ((size.width )/4) + (rectwidth / 4);
        int height = ((size.height ) / 2);
        
        return new Point(width, height);
    }
    public void populateECRTable(String sChangeType,Map<TCComponent,Vector<TCComponent>> itemRevECRMap)
    {
        m_ecrSelectionComposite.populateECRTable(sChangeType,itemRevECRMap);
    }
    public NOVECRTable getECRSelectionTable()
    {
        return m_ecrSelectionComposite.getECRSelectionTable();
    }
    private void setTitle() 
    {
		String strDialogTitle = m_reg.getString("ECRSelectionDialogTitle.MSG");
		this.getShell().setText(strDialogTitle);
	}
}
