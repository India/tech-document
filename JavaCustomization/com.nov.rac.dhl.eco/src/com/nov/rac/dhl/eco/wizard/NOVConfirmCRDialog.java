package com.nov.rac.dhl.eco.wizard;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.dhl.eco.util.components.NOVCRManagementPanel;
import com.nov.rac.dhl.ecr.helper.NOVECRHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVConfirmCRDialog extends AbstractSWTDialog 
{
	private Button okButton = null;
	private Button cancelButton = null;
	private JPanel panel = null;
	private iTextArea reasonTextArea = null;
	private JScrollPane reasonScrollPane = null;
	protected Registry reg = Registry.getRegistry(this);
	NOVCRManagementDialog parent = null;
	
	public NOVConfirmCRDialog(NOVCRManagementDialog parent) 
	{		
		super(parent.getShell());
		this.parent = parent;
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent)
    {
		cancelButton = createButton(parent, 1, IDialogConstants.CANCEL_LABEL, false);
		okButton = createButton(parent, 0, IDialogConstants.OK_LABEL, true);
    }	
	
	@Override
	protected void okPressed()
    {
		if(!(reasonTextArea.getText().toString().length()>0))
		{
			MessageBox.post("Please enter Closing comments.", "Warning", MessageBox.WARNING);
			NOVConfirmCRDialog.this.getShell().forceActive();
		}
		else
		{
			parent.setEcrClosingReason(NOVECRHelper.appendStringToCloseECR(reasonTextArea.getText(),reg.getString("ECR_ClosedBy.MESSAGE")));
			setReturnCode(SWT.OK);
		    close();
		}				
    }
	
	@Override
	protected Control createDialogArea(final Composite parent) 
	{
		setTitle();
		
		Composite composite = new Composite(parent, SWT.EMBEDDED);
		
		GridLayout gd = new GridLayout(2, true);
		composite.setLayout(gd);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		panel = new JPanel(new VerticalLayout());
		JLabel lblHeader = new JLabel("Set status of selected ECRs to Closed?");
		lblHeader.setAlignmentY(SwingConstants.LEFT);
		lblHeader.setPreferredSize(new Dimension(383, 30));
		lblHeader.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		
		JLabel lblReasonPanel = new JLabel("Reason for closing ECRs:");
		lblReasonPanel.setAlignmentY(SwingConstants.LEFT);
		lblReasonPanel.setPreferredSize(new Dimension(383, 20));
		
		reasonTextArea = new iTextArea();
		reasonTextArea.setLineWrap(true);
		reasonTextArea.setWrapStyleWord(true);
		reasonScrollPane = new JScrollPane(reasonTextArea);
		reasonScrollPane.setPreferredSize(new Dimension(383, 100));
		reasonTextArea.setRequired(true);
		
		panel.add("top.nobind.center.center",lblHeader);
		panel.add("top.nobind.center.center",lblReasonPanel);
		panel.add("top.nobind.center.center",reasonScrollPane);
		
		SWTUIUtilities.embed(composite, panel, false);
		
		return parent;
	}
	
	@Override
	protected Point getInitialSize() 
	{
		Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        
        int width = 430; //(size.width ) / 2;
        int height = 240; //(size.height ) / 2;
       		
		return new Point(width, height);
		
	}
	
	public void setTitle()
	{
		String strDialogTitle = "Confirm ECR Closure"; //reg.getString("CR_Management.DIALOG_TITLE");
		
		if(strDialogTitle == null)
		{
			strDialogTitle = reg.getString("Default.DIALOG_TITLE");
		}
		
		if(strDialogTitle != null)
		{
			this.getShell().setText(strDialogTitle);
		}						
	}

}
