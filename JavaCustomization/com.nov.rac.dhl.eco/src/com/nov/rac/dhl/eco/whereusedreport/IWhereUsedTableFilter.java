package com.nov.rac.dhl.eco.whereusedreport;

public interface IWhereUsedTableFilter 
{
	public void filterSearchResults();
}
