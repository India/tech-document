 package com.nov.rac.dhl.eco.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import com.nov.rac.dhl.eco.attrUpdate.NOVUpdateUOMDialog;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentUser;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVUpdateUOMHandler extends AbstractHandler  {
	Registry 			registry 	= Registry.getRegistry(this);

	
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		// TODO Auto-generated method stub	
		
		try
		{
			
			InterfaceAIFComponent[] comps 	= AIFUtility.getTargetComponents();
			//validate targets'components 
			InterfaceAIFComponent validComp = validateComponents(comps);
			
			if(validComp!=null)
			{
				String objType = validComp.getType();
				TCSession session =(TCSession) validComp.getSession();
				
				
				//if( NOVECOHelper.isDHGroup() &&  NOVECOHelper.isAllowedRole("_NOV_DH_update_uom_roles_") ) //If it is DH group with  RH Approver role
				if(isUpdateUOMUserGroup(session))
				{
						if(objType.startsWith(registry.getString("NovItem"))||				
										objType.startsWith(registry.getString("NonEngItem")) )
						{
							TCComponentItemRevision latestRev	= null;
							TCComponentItem			item	 	= null;
							TCComponentForm 		imf			= null;
							TCComponentForm 		irmf		= null;
							
							String modifprop = registry.getString("ModifiableProp");
							if(validComp instanceof TCComponentItemRevision)
							{
								item = ((TCComponentItemRevision)validComp).getItem(); 
								latestRev 	= item.getLatestItemRevision();
								irmf 		= (TCComponentForm)(latestRev.getRelatedComponent(registry.getString("RevMasterFromRelation")));						
								
								if(validComp != latestRev)
								{
									MessageBox.post(registry.getString("Invalid_Object_Selected.ERROR") ,										
										registry.getString("Error.TITLE"), MessageBox.ERROR);
									return null;
									
								}							
								if((item.getProperty(modifprop).length() == 0 )	|| (irmf.getProperty(modifprop).length() == 0 ) )
								{
									MessageBox.post(registry.getString("Access_Denied.ERROR"),
											registry.getString("Error.TITLE"),MessageBox.ERROR);
									return null;
										
								}
								
							}
							else if (validComp instanceof TCComponentItem)
							{
								//itemORitemrev = "item";
								item = (TCComponentItem)validComp;
								imf  = (TCComponentForm)(item.getRelatedComponent(registry.getString("MasterFormRelation")));
								if(item.getProperty(modifprop).length() == 0 || (imf.getProperty(modifprop).length() == 0 ))
								{
									MessageBox.post(registry.getString("Access_Denied.ERROR"),
											registry.getString("Error.TITLE"),MessageBox.ERROR);
									return null;
								}
							}
							
							IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
							NOVUpdateUOMDialog dialog = new NOVUpdateUOMDialog(window.getShell(), (TCComponent)validComp, item);
							dialog.create();
							dialog.open();	
							
						
						}
						else
						{
							MessageBox.post(registry.getString("Invalid_Object_Selected.ERROR"),
									registry.getString("Error.TITLE"),MessageBox.ERROR);
							return null;
						}
					
				}
				else
				{
					MessageBox.post(registry.getString("Access_Denied.ERROR"),
							registry.getString("Error.TITLE"),MessageBox.ERROR);
					return null;
				}
			}
					
		}
		catch ( TCException e)
		{
			MessageBox.post(e.getMessage() , registry.getString("Error.TITLE"),MessageBox.ERROR);
			e.printStackTrace();
		}
		return null;
			
	}
	
	
	private boolean isUpdateUOMUserGroup(TCSession session)
	{
		// TODO Auto-generated method stub
		String currentUsrGrp = session.getCurrentGroup().toString();
		String grpStr = currentUsrGrp.substring(0, 2);		
		String prefName =registry.getString("Update_UOM_Users.NAME") + grpStr;
		System.out.println("prefName : " + prefName);
		String[] grpStrArray = session.getPreferenceService().getStringArray(TCPreferenceService.TC_preference_site,
				prefName);
		
		String currentUser="";
		try {
			currentUser = session.getUser().getUserId();
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Current User : " + currentUser);
		//String currentUser = currentUser.toString();

		for (int i = 0; i < grpStrArray.length; i++) 
		{
			System.out.println("LOV User : " + grpStrArray[i]);
			if(currentUser.equalsIgnoreCase(grpStrArray[i]))
			{
				return true;
			}	
		}	
		return false;
	}

	public InterfaceAIFComponent  validateComponents(InterfaceAIFComponent[] comps) {
		// TODO Auto-generated method stub
		
		
		
		if(comps.length == 0)
		{
			
			MessageBox.post(registry.getString("NO_Object_Selected.ERROR"),
					registry.getString("Error.TITLE"),MessageBox.ERROR);
			
			return null;
		}
		if (comps.length>1) 
		{
		//err message
			
	         MessageBox.post(registry.getString("More_Than_One_Object_Selected.ERROR"),
						registry.getString("Error.TITLE"),MessageBox.ERROR);
	         return null;
			
		}
		
		TCSession session = (TCSession)comps[0].getSession();
		String currentUsrGrp = session.getCurrentGroup().toString();
		System.out.println("currentUsrGrp : " + currentUsrGrp);
		String owning_group="";
		try {
			owning_group = ((TCComponent)comps[0]).getTCProperty("owning_group").toString();
			System.out.println("owning_group : " + owning_group);
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!currentUsrGrp.equalsIgnoreCase(owning_group))
		{
	         MessageBox.post(registry.getString("The object is not owned by group you logged in"),
						registry.getString("Error.TITLE"),MessageBox.ERROR);
	         return null;
		}
		
		
		
		return comps[0];
		
	}
	
}
