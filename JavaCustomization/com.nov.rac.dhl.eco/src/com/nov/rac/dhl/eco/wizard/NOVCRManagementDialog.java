package com.nov.rac.dhl.eco.wizard;

import java.awt.HeadlessException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import schema.v1_3.webservices.tce.nov.com.CredentialsInfo;
import schema.v1_3.webservices.tce.nov.com.EngChangeRequestForm;

import com.noi.rac.dhl.eco.util.components.NOVCRManagementPanel;
import com.nov.www.cet.webservices.V1_3.CETGServicesProxy;
import com.nov.www.cet.webservices.V1_3.CETWSFault;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentSite;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCSiteInfo;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVCRManagementDialog extends AbstractSWTDialog
{
    private Button okButton = null;
    private Button cancelButton = null;
    private String ecrClosingReason;
    protected Registry reg = Registry.getRegistry(this);
    public NOVCRManagementPanel crPanel;
    
    public NOVCRManagementDialog(Shell parentShell)
    {
        super(parentShell);
        setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
        cancelButton = createButton(parent, 1, IDialogConstants.CANCEL_LABEL, false);
        okButton = createButton(parent, 0, "  Set Status to Closed  ", true);
    }
    
    @Override
    protected void okPressed()
    {
        if (closeECR())
        {
            crPanel.refreshCRTable();
            /*
             * setReturnCode(0); close();
             */
        }
    }
    
    public void setEcrClosingReason(String arg)
    {
        ecrClosingReason = arg;
    }
    
    /************************ Display ECR Closure confirmation dialog *****************************/
    public boolean closeECR()
    {
        TCComponent[] forms = crPanel.getCRList();
        if (forms == null || forms.length == 0)
        {
            MessageBox.post("Select CR(s) to close.", "Info", MessageBox.INFORMATION);
            return false;
        }
        
        try
        {
            NOVConfirmCRDialog confirmCRDialog = new NOVConfirmCRDialog(this);
            int response = confirmCRDialog.open();
            
            if (response == SWT.OK)
            {
                try
                {
                    String wsUrl = null;
                    TCSession session = (TCSession) AIFUtility.getDefaultSession();
                    TCPreferenceService prefService = session.getPreferenceService();
                    String userpwd = prefService.getString(TCPreferenceService.TC_preference_site,
                            "CETGServicesV1.3_PWD");
                    if (userpwd != null && userpwd.length() > 0)
                    {
                        // This URL points to Weblogic Test Server
                        TCComponentSite site = session.getCurrentSite();
                        TCSiteInfo info = site.getSiteInfo();
                        String database = info.getSiteName();
                        
                        if (database.indexOf("Test") != -1)
                            wsUrl = "http://srvhouplmt01:7081/cetgws/V1.3/CETGServices?WSDL";
                        else
                            wsUrl = "http://cetws.shp.nov.com:7081/cetgws/V1.3/CETGServices?WSDL";
                    }
                    
                    CETGServicesProxy proxy = new CETGServicesProxy();
                    proxy.setEndpoint(wsUrl);
                    System.out.println("wsURL=" + wsUrl);
                    CredentialsInfo credentials = proxy.getCredentials("webservices", userpwd);
                    System.out.println("cred = " + credentials.getUsername());
                    
                    EngChangeRequestForm data = new EngChangeRequestForm();
                    for (int i = 0; i < forms.length; i++)
                    {
                        data.setFormPuid(((TCComponentForm) forms[i]).getUid());
                        data.setEcrClosedBy(session.getUserName());
                        data.setClosureComments(ecrClosingReason);
                        data.setEcrStatus("Stopped");
                        String[] statusArr = new String[] { "Stopped" };
                        String objPUID = ((TCComponentForm) forms[i]).getUid();
                        boolean ret = proxy.setComponentStatus(credentials, objPUID, statusArr);
                        
                        String formPuid = proxy.saveOrUpdateEngChangeRequestForm(credentials, data);
                        
                        forms[i].refresh();
                        
                        System.out.println("Form Puid = " + formPuid);
                    }
                }
                catch (CETWSFault fault)
                {
                    System.out.println("Fault Message: " + fault.getCETWSFault());
                    return false;
                }
                catch (Exception e)
                {
                    System.out.println("Exception Message:" + e.getMessage());
                    e.printStackTrace();
                    return false;
                }
                
            }
            else
            {
                return false;
            }
        }
        catch (HeadlessException ex)
        {
            ex.printStackTrace();
        }
        return true;
    }
    
    /***********************************************************************************************/
    
    @Override
    protected Control createDialogArea(final Composite parent)
    {
        setTitle();
        
        Composite composite = new Composite(parent, SWT.EMBEDDED);
        
        GridLayout gd = new GridLayout(2, true);
        composite.setLayout(gd);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        crPanel = new NOVCRManagementPanel(this);
        
        SWTUIUtilities.embed(composite, crPanel, false);
        
        return parent;
    }
    
    @Override
    protected Point getInitialSize()
    {
        Monitor monitor = this.getShell().getMonitor();
        
        Rectangle size = monitor.getBounds();
        
        int width = 630; // (size.width ) / 2;
        int height = 670; // (size.height ) / 2;
        
        return new Point(width, height);
        
    }
    
    public void setTitle()
    {
        String strDialogTitle = "Change Request Management"; // reg.getString("CR_Management.DIALOG_TITLE");
        
        if (strDialogTitle == null)
        {
            strDialogTitle = reg.getString("Default.DIALOG_TITLE");
        }
        
        if (strDialogTitle != null)
        {
            this.getShell().setText(strDialogTitle);
        }
    }
    
}
