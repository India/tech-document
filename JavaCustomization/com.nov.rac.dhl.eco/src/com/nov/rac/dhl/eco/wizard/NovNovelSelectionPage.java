package com.nov.rac.dhl.eco.wizard;
/**
 * @author sachinkab
 *
 */
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;

import com.noi.rac.dhl.eco.util.components.NOVNovelProjectSearchPanel;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class NovNovelSelectionPage extends WizardPage
{
    public NOVNovelProjectSearchPanel m_novelSearchPanel;
    public static final String PAGE_NAME = "NovelSelectionPage";
    private Registry m_registry;
    private static final short SWTSWINGCOMPOSITE_WIDTH = 520;
    private static final short SWTSWINGCOMPOSITE_HEIGHT = 680;
       
    private static final short SWTSWINGCOMPOSITELOCATION_WIDTH = 15;
    private static final short SWTSWINGCOMPOSITELOCATION_HEIGHT = 20;
    private static final short SHELL_WIDTH = 650;
    private static final short SHELL_HEIGHT = 750;
    
    public NovNovelSelectionPage()
    {
        super(PAGE_NAME);
        m_registry = Registry.getRegistry(this);
        setTitle("Novel Engineering Project Details");//7786
        setMessage(m_registry.getString("NovelProjectMessage.Name"));
    }
    /*
     * (non-Javadoc)
     * Responisble for creating NOVNovelProjectSearchPanel 
     */
    public void createControl(Composite parent)
    {
        Composite swtSwingComp = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
        swtSwingComp.setSize(SWTSWINGCOMPOSITE_WIDTH, SWTSWINGCOMPOSITE_HEIGHT);
        swtSwingComp.setLocation(SWTSWINGCOMPOSITELOCATION_WIDTH, SWTSWINGCOMPOSITELOCATION_HEIGHT);
        m_novelSearchPanel = new NOVNovelProjectSearchPanel();
        SWTUIUtilities.embed(swtSwingComp, m_novelSearchPanel, false);
        getShell().setMinimumSize(SHELL_WIDTH,SHELL_HEIGHT);
        getShell().setSize(SHELL_WIDTH,SHELL_HEIGHT);
        
        Rectangle parentRect = getShell().getParent().getBounds();
        Rectangle childRect = getShell().getBounds();
        int x = parentRect.x + (parentRect.width - childRect.width) / 2;
        int y = parentRect.y + (parentRect.height - childRect.height) / 2;
       
        getShell().setLocation(x,y);
        
        setControl(swtSwingComp);
    }
}
