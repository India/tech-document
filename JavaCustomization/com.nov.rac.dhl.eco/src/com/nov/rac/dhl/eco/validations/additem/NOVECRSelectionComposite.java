package com.nov.rac.dhl.eco.validations.additem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Vector;

import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class NOVECRSelectionComposite extends Composite
{
    private Label m_titleLabel;
    private NOVECRTable m_ecrSelectionTable;
    private TCSession m_session;
    private Registry m_registry;
    private int m_iCheckboxColIndex = 0;
    private boolean  m_SelectAll = false;
    private TableColumn m_checkBoxCol;
    private final int m_iCheckBoxColWidth = 25;
    public NOVECRSelectionComposite(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry( this );
        m_session = (TCSession)AIFUtility.getDefaultSession();
        createUI( this);
    }
    private void createUI( Composite parent )
    {
        Composite itemSelectionComp = parent;
        itemSelectionComp.setLayout(new GridLayout(1,false));
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        itemSelectionComp.setLayoutData(gd_composite);
                 
        m_titleLabel = new Label(itemSelectionComp, SWT.NONE);
        
        setLabels();
        
        Composite tableComposite = new Composite(itemSelectionComp, SWT.NO_BACKGROUND | SWT.EMBEDDED);
        tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        createTable();
        
        setRendererEditor();
        
        setColumnWidth();
        
        addListeners();
                       
        ScrollPagePane tablePane= new ScrollPagePane(m_ecrSelectionTable);
                                       
        SWTUIUtilities.embed( tableComposite, tablePane, false);
    }
    private void setLabels()
    {
        m_titleLabel.setText(m_registry.getString("ECRSelectionLabel.MSG"));
    }
    private void createTable()
    {
        String[] strColNames=null;
                
        strColNames = m_registry.getStringArray("ECRSelectionTable.COLUMNS");
                
        m_ecrSelectionTable = new NOVECRTable(m_session , strColNames);
        
        m_ecrSelectionTable.setAutoResizeMode(TCTable.AUTO_RESIZE_ALL_COLUMNS);
        m_ecrSelectionTable.getTableHeader().setReorderingAllowed(false);
                        
        m_ecrSelectionTable.ignoreSortingColumn(m_iCheckboxColIndex);
    }
    private void setRendererEditor()
    {
        m_checkBoxCol = m_ecrSelectionTable.getColumnModel().getColumn(m_iCheckboxColIndex);
               
        m_checkBoxCol.setCellEditor(m_ecrSelectionTable.getDefaultEditor(Boolean.class));
               
        NOVECRTableHeaderRenderer tableHeaderRenderer = new NOVECRTableHeaderRenderer(new SelectAllListener());
        m_checkBoxCol.setHeaderRenderer(tableHeaderRenderer);        
    }
    private void setColumnWidth()
    {
        m_checkBoxCol.setResizable(false);
        m_checkBoxCol.setMaxWidth(100);
        m_checkBoxCol.setPreferredWidth(m_iCheckBoxColWidth);
    }
    private void addListeners()
    {
        //m_ecrSelectionTable.addMouseListener(new NOVECRTableMouseListener());
    }
    public void selectAllRows(boolean bSelectAll)
    {
        int iNoOfRows = m_ecrSelectionTable.getRowCount();
        for(int iCnt = 0; iCnt < iNoOfRows; iCnt++)
        {           
            m_ecrSelectionTable.setValueAt(bSelectAll, iCnt, m_iCheckboxColIndex);
        }
        m_ecrSelectionTable.repaint();
    }
    public void populateECRTable(String sChangeType,Map<TCComponent,Vector<TCComponent>> itemRevECRMap)
    {
        TableModel tableModel = m_ecrSelectionTable.getModel();
        try 
        {
            Object[] itemRevObjs = itemRevECRMap.keySet().toArray();
            int iNoOfRevs = itemRevObjs.length;
            for(int i=0;i<iNoOfRevs;i++)
            {
                TCComponentItemRevision itemRev = (TCComponentItemRevision) itemRevObjs[i];
            
                String sItemId = itemRev.getTCProperty("item_id").getStringValue();
                String sName = itemRev.getTCProperty("object_name").getStringValue();
                
                Object[] ecrs = itemRevECRMap.get(itemRevObjs[i]).toArray();
                int iNoOfiECRs = ecrs.length;
                for(int indx=0;indx<iNoOfiECRs;indx++)
                {
                    TCComponentForm ecrForm = (TCComponentForm)ecrs[indx];
                    
                    String sECR = ecrForm.getTCProperty("object_name").getStringValue();
                    Vector<Object> sECRInfoVect = new Vector<Object>();
                    sECRInfoVect.addElement(false);
                    sECRInfoVect.addElement(sECR);
                    sECRInfoVect.addElement(sItemId);
                    sECRInfoVect.addElement(sName);
                    
                    NOVECRTableLine tableLine = null;
                    if(tableModel instanceof NOVECRTableModel)
                    {
                        NOVECRTableModel ecrTableModel = (NOVECRTableModel)tableModel;
                        tableLine = ecrTableModel.createLine(sECRInfoVect.toArray());
                        tableLine.setTableLineFormObject(ecrForm);
                        tableLine.setTableLineRevObject(itemRev);
                        if(!NOVECOHelper.isECRValid(sChangeType, ecrForm))
                        {
                            tableLine.setTableLineEditableStatus(false);
                            tableLine.setTableLineToolTipText(m_registry.getString("ChangeTypeMismatch.MSG"));
                        }
                        m_ecrSelectionTable.addRow(tableLine);
                        sECRInfoVect.clear();
                    }            
                }
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
    public NOVECRTable getECRSelectionTable()
    {
        return m_ecrSelectionTable;
    }
    class SelectAllListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            // TODO Auto-generated method stub
            if (m_SelectAll) 
            {
                m_SelectAll = false;
            } 
            else 
            {
                m_SelectAll = true;
            }
            selectAllRows(m_SelectAll);
        }
    }    
}
