package com.nov.rac.dhl.eco.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import com.nov.rac.dhl.eco.lifecycletracking.NOVLifecycleTrackingDlg;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.util.MessageBox;

public class NOVLifecycleTrackingHandler extends AbstractHandler {

	public NOVLifecycleTrackingHandler() 
	{
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		
		InterfaceAIFComponent[] selComps = AIFUtility.getTargetComponents();
		
		if(selComps.length == 0)
		{
			//err message
			MessageBox.post("No objects Selected " +
					"\nSelect Nov4Part/Non-Engineering/Documents type item/revision.",
					"Error", MessageBox.ERROR);
			return null;
		}
		
		if (selComps.length>1) 
		{
			MessageBox.post("More than one object selected " +
					"\nSelect only one Nov4Part/Non-Engineering/Documents item/revision.",
					"Error", MessageBox.ERROR);
			return null;
		}
		
		if ((selComps[0].getType().startsWith("Nov4Part"))|| 
			(selComps[0].getType().startsWith("Non-Engineering"))||
			(selComps[0].getType().startsWith("Documents")))
		{
			if(selComps[0] instanceof TCComponentItemRevision || selComps[0] instanceof TCComponentItem)
			{
				IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
				
				NOVLifecycleTrackingDlg dialog = new NOVLifecycleTrackingDlg(window.getShell().getDisplay(),(TCComponent)selComps[0]);
			}
			else
			{
				MessageBox.post("Invalid object selected " +
					"\nSelect only Nov4Part/Non-Engineering/Documents type item/revision.",
					"Error", MessageBox.ERROR);
				return null;
			}
		}
		else
		{
			MessageBox.post("Invalid object selected " +
				"\nSelect only Nov4Part/Non-Engineering/Documents type item/revision.",
				"Error", MessageBox.ERROR);
			return null;
		}
		return null;
	}
}
