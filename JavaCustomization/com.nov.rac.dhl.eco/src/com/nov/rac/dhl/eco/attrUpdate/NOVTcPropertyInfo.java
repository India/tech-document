/* ============================================================
  
  File description: 

  Filename: NOVTcPropertyInfo.java 
  Module  : \NOV_ECO\DHL_ECO\com\nov\rac\dhl\eco\attrUpdate 

  NOVTcPropertyInfo loads the attribute elements on NOVUpdateAttrDialog, 
  based on the property type, getting the reqd controls dynamically. 

  Date         Developers    Description
  30-08-2010   Usha    	  Initial Creation

  $HISTORY
  ============================================================ */

package com.nov.rac.dhl.eco.attrUpdate;

import java.util.Arrays;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCTextService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVTcPropertyInfo
{
    Label lbl;
    Control newValField;
    TCProperty property;
    Composite shell;
    TCSession session;
    TCComponentItem item;
    TCComponentItemRevision rev;
    TCComponentForm irmf;
    TCComponentForm imf;
    TCTextService txtService;
    String strDesc;
    String strSites;
    String strPullDoc;
    String propName;
    Control nameFiled;
    Control name2Filed;
    String objType;
    TCComponent target;
    static NOVDocumentType docType1;
    static NOVDocumentCAT docCat1;
    NOVDocumentType docType;
    NOVDocumentCAT docCat;
    private Registry m_registry = null;
    public boolean m_isNameName2AttrEditable;
    private boolean m_isRevNameName2Editable;
    
    public NOVTcPropertyInfo(TCComponent targetCmp, TCComponentItem item, TCComponentItemRevision rev,
            TCComponentForm revMaster, TCComponentForm itemMaster, Composite shell, TCProperty property,
            boolean isNameName2AttrEditable)
    {
        this.shell = shell;
        this.property = property;
        this.item = item;
        this.rev = rev;
        irmf = revMaster;
        imf = itemMaster;
        propName = property.getPropertyName();
        objType = property.getTCComponent().getType();
        target = targetCmp;
        session = property.getDescriptor().getTypeComponent().getSession();
        this.txtService = property.getDescriptor().getTypeComponent().getSession().getTextService();
        m_registry = Registry.getRegistry(this);
        this.m_isNameName2AttrEditable = isNameName2AttrEditable;
        try
        {
            strDesc = txtService.getTextValue("object_desc");
            strSites = txtService.getTextValue("Sites");
            strPullDoc = txtService.getTextValue("PullDrawing");
        }
        catch (TCException e)
        {
            MessageBox.post(e.getMessage(), "Error", MessageBox.ERROR);
            e.printStackTrace();
        }
        
        lbl = new Label(shell, SWT.NULL);
        setLabel();//5025
        lbl.setFont(new Font(shell.getDisplay(), "", 8, 1));
        newValField = getControl(property);
        
    }
    //5025-start
    public void setLabel()
    {
    	
    	if(propName.equalsIgnoreCase(m_registry.getString("COMMENTS_ATTR_ENG.NAME")) || propName.equalsIgnoreCase(m_registry.getString("COMMENTS_ATTR_NONENG.NAME")))
    	{
    		lbl.setText("Vendor Comments");
    	}
    	else
    	{
        try {
			lbl.setText(txtService.getTextValue(property.getName()));
		} 
        catch (TCException e)
        {
            MessageBox.post(e.getMessage(), "Error", MessageBox.ERROR);
            e.printStackTrace();
        }
        
		
    	}
    }
    
    //5025-end
    
    public void loadinfo()
    {
        if (newValField instanceof Text)
        {
            boolean isEditable = false;
            boolean isItemRevision = false;
            String sPropValue = property.getStringValue();
            isItemRevision = validateTargetType() ? true : false;
          //5025-start
            if(propName.equalsIgnoreCase(m_registry.getString("COMMENTS_ATTR_ENG.NAME")) || propName.equalsIgnoreCase(m_registry.getString("COMMENTS_ATTR_NONENG.NAME")))
            {
            	createVendorCommentsComposite(sPropValue,isItemRevision,isEditable);
               
            }
            //5025-end
           
            else if ((propName.equalsIgnoreCase("object_desc")) || (propName.equalsIgnoreCase(strDesc))  )
            {
                ((Text) newValField).setText(sPropValue.toUpperCase());
                
                GridData gdData = new GridData(GridData.FILL_BOTH | GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL);
                gdData.widthHint = 170;
                gdData.heightHint = 56;
                gdData.horizontalAlignment = GridData.FILL;
                gdData.verticalAlignment = GridData.FILL;
                gdData.horizontalSpan = 1;
                ((Text) newValField).setLayoutData(gdData);
                ((Text) newValField).setTextLimit(240);
                isEditable = true;
                ((Text) newValField).setEnabled(isItemRevision);
                ((Text) newValField).setEditable(isItemRevision);
                ((Text) newValField).addVerifyListener(new VerifyListener()
                {
                    public void verifyText(VerifyEvent verEv)
                    {
                        verEv.text = verEv.text.toUpperCase();
                    }
                });
                
                ((Text) newValField).addKeyListener(new KeyListener()
                {
                    public void keyPressed(KeyEvent keyPressedEvnt)
                    {
                        Text newDesc = (Text) keyPressedEvnt.widget;
                        String sDescVal = newDesc.getText();
                        if (sDescVal.startsWith(" ") || sDescVal.startsWith("\r\n"))
                        {
                            newDesc.setText(sDescVal.trim());
                        }
                        return;
                    }
                    
                    public void keyReleased(KeyEvent keyReleasedEvnt)
                    {
                        
                        Text newDesc = (Text) keyReleasedEvnt.widget;
                        String sDescVal = newDesc.getText();
                        if (sDescVal.startsWith(" ") || sDescVal.startsWith("\r\n"))
                        {
                            newDesc.setText(sDescVal.trim());
                        }
                        updateNameAndName2BasedOnGrp(newDesc);// if(group!=33)populates
                                                              // Name&Name2
                                                              // fields
                        // populateNameAndName2(newDesc.getText());
                        
                        return;
                    }
                });
                
            }// end if desc
            else
            {
                ((Text) newValField).setText(sPropValue);
                
                GridData grdData = new GridData();
                int width = ((Text) newValField).getBorderWidth();
                grdData.widthHint = 150 * width;
                grdData.horizontalAlignment = GridData.FILL;
                ((Text) newValField).setLayoutData(grdData);
                ((Text) newValField).setTextLimit(30);
                setTextFieldEditableForGroup(isEditable, isItemRevision);
            }// end if Name/Name2
            /*
             * ((Text)newValField).setEnabled(isEditable);
             * ((Text)newValField).setEditable(isEditable);
             */
            // setTextFieldEditableForGroup(isEditable);
            
        }// TEXT
        
        // if(propName.equalsIgnoreCase(strSites))
        if (newValField instanceof NovSiteControl)
        {
            String[] currSiteVals = property.getStringValueArray();
            Vector<String> siteVec = new Vector<String>(Arrays.asList(currSiteVals));
            Button[] stBtns = ((NovSiteControl) newValField).checkboxes;
            for (int i = 0; i < stBtns.length; i++)
            {
                String siteVal = stBtns[i].getText();
                if (siteVec.contains(siteVal))
                    stBtns[i].setSelection(true);
            }
            if (target instanceof TCComponentItem)
                ((NovSiteControl) newValField).setEnabled(false);
            else
                ((NovSiteControl) newValField).setEnabled(true);
        }// Sites
        
        // if(propName.equalsIgnoreCase(strPullDoc))
        if (newValField instanceof NovPullDocControl)
        {
            Button[] pullDocBtns = ((NovPullDocControl) newValField).radiobtns;
            boolean val;
            try
            {
                val = property.getLogicalValue();
                
                if (target instanceof TCComponentItem)
                    ((NovPullDocControl) newValField).setEnabled(true);
                else
                    ((NovPullDocControl) newValField).setEnabled(false);
                
                if (val == true)
                    pullDocBtns[0].setSelection(true);
                
                else
                    pullDocBtns[1].setSelection(true);
                
            }
            catch (TCException e)
            {
                MessageBox.post(e.getMessage(), "Error", MessageBox.ERROR);
                e.printStackTrace();
            }
        }// PullDrawing
        
    }// loadinfo method
    
    public void saveinfo()
    {
        if (newValField instanceof Text)
        {
            if (!validateIsNameNameFieldsEditable())
            {
                try
                {
                    if (property.isModifiable())
                    {
                    	property.setStringValue(((Text) newValField).getText().trim());////4063-added trim method to remove white spaces from name2 field
                        
                    }
                }
                catch (TCException e)
                {
                    MessageBox.post(e.getMessage(), "Error", MessageBox.ERROR);
                    e.printStackTrace();
                }
            }
        }// TEXT
        if (newValField instanceof NovSiteControl)
        {
            String[] siteVals = ((NovSiteControl) newValField).getSiteValues();
            // TCDECREL-2602 : START : AKHILESH
            TCProperty propSites = null;
            try
            {
                propSites = irmf.getTCProperty("Sites");
                if (propSites != null && propSites.isModifiable())
                {
                    propSites.setStringValueArray(siteVals);
                }
                // TCDECREL-2602 : END : AKHILESH
            }
            catch (TCException e)
            {
                MessageBox.post(e.getMessage(), "Error", MessageBox.ERROR);
                e.printStackTrace();
            }
        }// Sites
        if (newValField instanceof NOVDocumentType)
        {
            // docType.savePropertyValue();
            
            // Vivek/Soma
            
            String strCurrSelectedValue = docType.m_combo.getItem(docType.m_combo.getSelectionIndex());
            TCProperty docType = null;
            try
            {
                docType = imf.getTCProperty("DocumentsType");
                if (docType != null && docType.isModifiable())
                {
                    docType.setStringValue(strCurrSelectedValue);
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            } // Vivek/Soma
        }// DocumentType
        if (newValField instanceof NOVDocumentCAT)
        {
            docCat.savePropertyValue();
        }// DocumentCategory
        if (newValField instanceof NovPullDocControl)
        {
            boolean val = ((NovPullDocControl) newValField).getPullDocValue();
            try
            {
                if (val)
                {
                    // MessageBox.post("pulldoc","Error",MessageBox.INFORMATION);
                    // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("pulldoc");
                    property.setLogicalValue(true);
                }
                else
                    property.setLogicalValue(false);
            }
            catch (TCException e)
            {
                MessageBox.post(e.getMessage(), "Error", MessageBox.ERROR);
                e.printStackTrace();
            }
        }// PullDoc
    }// saveinfo method
    
    private Control getControl(TCProperty prop)
    {
        int type = prop.getPropertyType();
        if (propName.equalsIgnoreCase(strSites))
            return new NovSiteControl(shell, prop.getTCComponent().getSession());
        // soma start
        
        if (propName.equalsIgnoreCase("DocumentsCAT"))
        {
            docCat = new NOVDocumentCAT(shell, prop.getTCComponent().getSession(), property);
            return docCat;
        }
        if (propName.equalsIgnoreCase("DocumentsType"))
        {
            try
            {
                docType = new NOVDocumentType(shell, prop.getTCComponent().getSession(), property, imf.getProperty(
                        "owning_group").substring(0, 2));
                
                // TCDECREL-3371 : If latest revision is released then disable
                // Document Type
                TCComponent[] statusList = rev.getTCProperty("release_status_list").getReferenceValueArray();
                if (statusList == null || statusList.length == 0)
                {
                    docType.setEnabled(true);
                }
                else
                {
                    docType.setEnabled(false);
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            return docType;
        }
        // soma end
        
        if (type == TCProperty.PROP_string)
        {
            Text txt;
            //5025-added vendor comments
            if ((propName.equalsIgnoreCase("object_desc")) || (propName.equalsIgnoreCase(strDesc))|| (propName.equalsIgnoreCase(m_registry.getString("COMMENTS_ATTR_ENG.NAME")))|| (propName.equalsIgnoreCase(m_registry.getString("COMMENTS_ATTR_NONENG.NAME"))) )
                txt = new Text(shell, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
            else
                txt = new Text(shell, SWT.BORDER);
            return txt;
        }
        if (type == TCProperty.PROP_logical)
        {
            // if(propName.equalsIgnoreCase("Secure"))
            // {
            // return new NovSecureDocControl(shell,
            // prop.getTCComponent().getSession());
            // }
            return new NovPullDocControl(shell, prop.getTCComponent().getSession());
        }
        return null;
    }// getcontrol method
    
    public void assignWidgetsForDesc(NOVUpdateAttrDialog updateDlg)
    {
        if (propName.equalsIgnoreCase("object_desc"))
        {
            for (int i = 0; i < updateDlg.infoObjs.length; i++)
            {
                if (updateDlg.infoObjs[i].propName.equalsIgnoreCase("object_name"))
                {
                    nameFiled = updateDlg.infoObjs[i].newValField;
                }
                else if (updateDlg.infoObjs[i].propName.equalsIgnoreCase("Name2"))
                {
                    name2Filed = updateDlg.infoObjs[i].newValField;
                }
            }
        }
    }// assignWidgetsForDesc method
    
    /*
     * public static void loadLovComponent(NOVUpdateAttrDialog updateDlg) { for
     * (int i = 0; i < updateDlg.infoObjs.length; i++) {
     * if(updateDlg.infoObjs[i].propName.equalsIgnoreCase("DocumentsType")) {
     * docType1 = updateDlg.infoObjs[i].docType; } else
     * if(updateDlg.infoObjs[i].propName.equalsIgnoreCase("DocumentsCAT")) {
     * docCat1 = updateDlg.infoObjs[i].docCat; } } }
     */
    // TCDECREL-3381:Start
    private void populateNameAndName2(String sDescValue)
    {
        String sNameFieldVal = "";
        String sName2FieldVal = "";
        int iDescLength = sDescValue.length();
        String[] sDescWithNewline = sDescValue.split("\n");
        if (sDescWithNewline != null && sDescWithNewline.length > 1)
        {
            sNameFieldVal = sDescWithNewline[0];
            sName2FieldVal = sDescWithNewline[1];
            
            int iNameLength = sNameFieldVal.length();
            if (iNameLength > 30)
            {
                sName2FieldVal = sNameFieldVal.substring(30, iNameLength);
                sNameFieldVal = sNameFieldVal.substring(0, 30);
            }
            if (sName2FieldVal.length() > 30)
            {
                sName2FieldVal = sName2FieldVal.substring(0, 30);
            }
        }
        else
        {
            sNameFieldVal = sDescValue.substring(0, Math.min(iDescLength, 30));
            if (iDescLength > 30)
            {
                sName2FieldVal = sDescValue.substring(30, Math.min(iDescLength, 60));
            }
        }
        if (nameFiled != null)
        {
            sNameFieldVal = sNameFieldVal.replaceAll("[\\r\\n]", " ");
            ((Text) nameFiled).setText(sNameFieldVal);
        }
        if (name2Filed != null)
        {
            sName2FieldVal = sName2FieldVal.replaceAll("[\\r\\n]", " ");
            ((Text) name2Filed).setText(sName2FieldVal.trim());//4509
        }
    }
    
    // TCDECREL-3381:End
    
    private void setTextFieldEditableForGroup(boolean isEditable, boolean isItemRevision)
    {
        if (!m_isNameName2AttrEditable)
        {
            ((Text) newValField).setEnabled(isEditable);
            ((Text) newValField).setEditable(isEditable);
            convertTOUppercase();
        }
        else
        {
            if (isItemRevision)
            {
                ((Text) newValField).setEnabled(true);
                ((Text) newValField).setEditable(true);
                convertTOUppercase();
            }
            else
            {
                ((Text) newValField).setEnabled(isEditable);
                ((Text) newValField).setEditable(isEditable);
            }
        }
    }
    
    private void convertTOUppercase()
    {
        ((Text) newValField).addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent verifyEve)
            {
                verifyEve.text = verifyEve.text.toUpperCase();
                
            }
        });
    }
    
    private boolean isNameTxtValueEmpty(String propName)
    {
        boolean isNameEmpty = false;
        if (propName.equalsIgnoreCase(m_registry.getString("objectName.NAME")))
        {
            isNameEmpty = ((Text) newValField).getText().trim().length() > 0 ? false : true;
        }
        return isNameEmpty;
    }
    
    private boolean validateIsNameNameFieldsEditable()
    {
        m_isRevNameName2Editable = false;
        if (m_isNameName2AttrEditable && isNameTxtValueEmpty(propName))// TCDECREL
                                                                       // - 4089
        {
            
            m_isRevNameName2Editable = true;
            
        }
        return m_isRevNameName2Editable;
    }
    
    public boolean getRevisionRevAttrFieldsForGrp33()
    {
        return m_isRevNameName2Editable;
    }
    
    private void updateNameAndName2BasedOnGrp(Text txtDescription)
    {
        if (!m_isNameName2AttrEditable)
        {
            populateNameAndName2(((Text) txtDescription).getText());
        }
    }
    
    private boolean validateTargetType()
    {
        if (target instanceof TCComponentItemRevision)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    //5025-start
    private void createVendorCommentsComposite(String spropertyValue, boolean isItemRevision,boolean isEditable)
    {
    	int WIDTH_HINT=170;
    	int HEIGHT_HINT=56;
        int TEXT_LIMIT=1900;
    	
    	//((Text) newValField).setText(sPropValue.toUpperCase());
    	//((Text) newValField).setText(spropertyValue);
        GridData gdData = new GridData(GridData.FILL_BOTH | GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL );
        gdData.widthHint = WIDTH_HINT;
        gdData.heightHint = HEIGHT_HINT;
        gdData.horizontalAlignment = GridData.FILL;
        gdData.verticalAlignment = GridData.FILL;
        gdData.horizontalSpan = 1;
        ((Text) newValField).setLayoutData(gdData);
        ((Text) newValField).setTextLimit(TEXT_LIMIT);
        isEditable = true;
        ((Text) newValField).setEnabled(isItemRevision);
        ((Text) newValField).setEditable(isItemRevision);
        ((Text) newValField).addVerifyListener(new VerifyListener()
        {
            public void verifyText(VerifyEvent verEv)
            {
                //verEv.text = verEv.text.toUpperCase();
            	verEv.text = verEv.text;
            }
        });
        
        ((Text) newValField).addKeyListener(new KeyListener()
        {
            public void keyPressed(KeyEvent keyPressedEvnt)
            {
                /*Text newComments = (Text) keyPressedEvnt.widget;
                String sCommentsVal = newComments.getText();
                if (sCommentsVal.startsWith(" ") || sCommentsVal.startsWith("\r\n"))
                {
                	newComments.setText(sCommentsVal.trim());
                }*/
            	vendorCommentsEvent(keyPressedEvnt);
                return;
            }
            
            public void keyReleased(KeyEvent keyReleasedEvnt)
            {
            	vendorCommentsEvent(keyReleasedEvnt);
                return;
            }
        });
        
        ((Text) newValField).setText(spropertyValue);
    }
    private void vendorCommentsEvent(KeyEvent event)
    {
    	Text newComments = (Text) event.widget;
        String sCommentsValue = newComments.getText();
        if (sCommentsValue.startsWith(" ") || sCommentsValue.startsWith("\r\n"))
        {
            newComments.setText(sCommentsValue.trim());
        }
    }
    
    //5025-end
}// NOVTcPropertyInfo class
