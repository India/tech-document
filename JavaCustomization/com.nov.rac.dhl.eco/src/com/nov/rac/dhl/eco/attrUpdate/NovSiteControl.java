/* ============================================================
   
   File description: 

   Filename: NovSiteControl.java 
   Module  : \NOV_ECO\DHL_ECO\com\nov\rac\dhl\eco\attrUpdate 

   NovSiteControl gets the controls for Sites field on NOVUpdateAttrDialog, 
   through NOVTcPropertyInfo. 

   Date         Developers    Description
   30-08-2010   Usha    	  Initial Creation

   $HISTORY
   ============================================================ */


package com.nov.rac.dhl.eco.attrUpdate;

import java.util.Vector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NovSiteControl extends Composite
{
	Button[] checkboxes;
	
	public	NovSiteControl(Composite composite,TCSession session) 
	{
		//session
		super(composite,SWT.BORDER);
		this.setLayout(new GridLayout(1, true));
		
		TCComponentListOfValues lovValues 	= TCComponentListOfValuesType.findLOVByName(
																	session, "DH_Sites");
		ListOfValuesInfo 		lovInfo 	= null;
		try 
		{
			lovInfo = lovValues.getListOfValues();
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
				
		Object[] lovS = lovInfo.getListOfValues();
		if (lovS.length > 0)
		{
			checkboxes = new Button[lovS.length];
			for (int i = 0; i < lovS.length; i++ )
			{
				checkboxes[i] = new Button(this, SWT.CHECK);
				checkboxes[i].setText(lovS[i].toString());	
			}
		}
	}//constructor

	public String[] getSiteValues()
	{
		
		Vector<String> siteValVec = new Vector<String>(); 
		for(int i = 0; i<checkboxes.length; i++ )
		{
			if( checkboxes[i].getSelection())
			{
				siteValVec.add(checkboxes[i].getText());
			}	
		}		
		return siteValVec.toArray(new String[siteValVec.size()]);	
	}//getSiteValues
	
}
