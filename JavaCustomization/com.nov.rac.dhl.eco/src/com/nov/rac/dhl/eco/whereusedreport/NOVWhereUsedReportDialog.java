package com.nov.rac.dhl.eco.whereusedreport;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.table.TableRowSorter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.dhl.eco.util.components.NOVWhereUsedFilterCriteria;
import com.nov.rac.dhl.eco.whereusedreport.listeners.ExportToExcelButtonListener;
import com.nov.rac.dhl.eco.whereusedreport.listeners.WhereUsedTableFilterListener;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.IPerspectiveDef;
//import com.teamcenter.rac.aif.ApplicationDef;TC10.1 Upgrade
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.aifrcp.SWTLegacyPopupListener;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCComponentRevisionRuleType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;

public class NOVWhereUsedReportDialog implements IWhereUsedTableFilter
{
    private TCComponent selComponent;
    Registry registry = Registry.getRegistry(this);
    String[] itemProps;
    String[] itemMasterProps;
    String[] itemRevMasterProps;
    List<String> list;
    TCTable m_table;
    JLabel m_itemRevlbl;
    JLabel m_revRulelbl;
    JLabel objsNotFoundlbl;
    JLabel m_numOfref;
    final Shell shell;
    TCSession session;
    Image docICON, prtICON, m_nonEnggPrtICON, m_purchasePrtICON, m_rawStockICON, m_cadOnlyICON;
    
    // TCDECREL-2931
    private JButton m_filterButton;
    private JButton m_exportButton;
    private NOVWhereUsedFilterCriteria m_filterCriteria;
    private WhereUsedTableFilterListener m_filterListener;
    private boolean m_bIsComponentsEditable = true;
    private AIFTableModel m_whrUsedTableModel;
    private TableRowSorter<AIFTableModel> m_whrUsedTableSorter;
    private boolean m_bIsPart = false;
    private TCComponent[] m_refComps = null;            //TCDECREL-4551
    HashMap<String, Vector<Object>> m_mapWhereUsedPart = new HashMap<String, Vector<Object>>();
    
    public NOVWhereUsedReportDialog(Display display, TCComponent selComp)
    {
        
        selComponent = selComp;
        session = selComp.getSession();
        
        if (selComponent != null /* && selComponent instanceof TCComponentItem */)
        {
            try
            {
                /*
                 * TCDECREL-3887: use the latest revision if selected Revision/
                 * Item
                 */
                if (selComponent instanceof TCComponentItem)
                    selComponent = ((TCComponentItem) selComponent).getLatestItemRevision();
                else if (selComponent instanceof TCComponentItemRevision)
                {
                    selComponent = ((TCComponentItemRevision) selComponent).getItem().getLatestItemRevision();
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        itemProps = registry.getStringArray("ItemRevProps", ",");
        itemMasterProps = registry.getStringArray("ItemMasterProps", ",");
        itemRevMasterProps = registry.getStringArray("ItemRevMasterProps", ",");
        
        shell = new Shell(display, SWT.CLOSE | SWT.APPLICATION_MODAL);
        // shell.setText("Where Used/Reference Report");
        // GridLayout layout = new GridLayout();
        // shell.setLayout(layout);
        // shell.setBackground(new Color(display, 50,100,127));
        // shell.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false,
        // false));
        docICON = new Image(display, getClass().getResourceAsStream("document_16.png"));
        prtICON = new Image(display, getClass().getResourceAsStream("part_16.png"));
        //TCDECREL-4889:Start
        m_nonEnggPrtICON = new Image(display, getClass().getResourceAsStream("noneng_item_16.png"));
        m_purchasePrtICON = new Image(display, getClass().getResourceAsStream("purchasedpart_16.png"));
        m_cadOnlyICON = new Image(display, getClass().getResourceAsStream("cadonlypart_16.png"));
        m_rawStockICON  = new Image(display, getClass().getResourceAsStream("rawstockpart_16.png"));
        //TCDECREL-4889:End
        // Image shellICON = new
        // Image(display,"com/nov/rac/eco/images/addnew.png");
        
        Composite tableComp = new Composite(shell, SWT.EMBEDDED | SWT.NO_BACKGROUND);
        // Composite tableComp = new Composite(shell, SWT.BORDER);
        // tableComp.setLocation(5, 20);
        tableComp.setSize(640, 500);
        tableComp.setBackground(new Color(display, 50, 100, 127));
        JPanel tablePanel = new JPanel();
        if (itemProps != null)
        {
            list = new ArrayList<String>(Arrays.asList(itemProps));
            if (itemMasterProps != null)
            {
                list.addAll(Arrays.asList(itemMasterProps));
            }
            if (itemRevMasterProps != null)
            {
                list.addAll(Arrays.asList(itemRevMasterProps));
            }
        }
        String[] columns = { "Item ID", "Item Name", "Rev", "PartSubType", "Sites", "Lifecycle","Find No" , "Quantity"};
        // String[] columns = list.toArray(new String[list.size()]);
        m_table = new TCTable(selComponent.getSession(), columns);
        m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane scPane = new JScrollPane(m_table);
        scPane.setPreferredSize(new Dimension(600, 450));
        
        m_whrUsedTableModel = (AIFTableModel) m_table.getModel();
        m_table.setSortEnabled(false);
        
        createWhereUsedTableSorter();
        
        // String str = "Item Revision: "+ selComponent.toString();
        JPanel lblPanel = new JPanel(new PropertyLayout(0, 0, 0, 0, 0, 0));
        
        m_itemRevlbl = new JLabel();
        m_revRulelbl = new JLabel();
        m_numOfref = new JLabel();
        
        lblPanel.add("1.1.left.center", m_itemRevlbl);
        lblPanel.add("2.1.left.center", m_numOfref);
        
        // TCDECREL-2931:Start
        JPanel btnPanel = new JPanel(new PropertyLayout(10, 0, 0, 0, 0, 0));
        m_filterButton = new JButton();
        m_exportButton = new JButton();
        
        btnPanel.add("1.1.left.center", m_filterButton);
        btnPanel.add("1.2.left.center", m_exportButton);
        
        JPanel lblBtnPanel = new JPanel(new PropertyLayout(20, 0, 0, 0, 0, 0));
        lblBtnPanel.add("1.1.left.center", lblPanel);
        lblBtnPanel.add("1.2.left.center", btnPanel);
        // TCDECREL-2931:End
        
        // @swamy
        
        m_table.addMouseListener((new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent mouseEvt)
            {
                // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info( "inside");
                // TCDECREL-5486 Remove the SWTLegacyPopupListener to avoid OOTB pop up in structure manager
                MouseListener[] listeners = m_table.getMouseListeners();
                if(listeners != null && listeners.length>0)
                {
                      for(int inxLisner =0; inxLisner < listeners.length; inxLisner++)
                      {
                        if(listeners[inxLisner] instanceof SWTLegacyPopupListener)
                        {
                          m_table.removeMouseListener(listeners[inxLisner]);
                        }
                      }
                }
                // TCDECREL-5486 (End)
                
                int selRow = m_table.getSelectedRow();
                Object[] rowVal = (Object[]) m_table.getRowData(selRow);
                final TCComponentItem[] sendToItem = new TCComponentItem[1];
                try
                {
                    TCComponentItemType compType = (TCComponentItemType) session.getTypeComponent("Item");
                    if(compType!= null) 
                    {
                        sendToItem[0] = compType.find((String) rowVal[0]);
                    }
                }
                catch (TCException e1)
                {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                
                if (SwingUtilities.isRightMouseButton(mouseEvt) && (mouseEvt.getClickCount() == 1) && selRow >= 0)
                {
                    // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(
                    // "RMB clicked");
                    
                    JPopupMenu popupMenu = new JPopupMenu();
                    JMenuItem item = new JMenuItem();
                    String initialText = "<html><b><font=8 color=red>Send to My Teamcenter</font></b>";
                    item.setText(initialText);
                    JMenuItem item2 = new JMenuItem();
                    String initialText2 = "<html><b><font=8 color=red>Send to Structure Manager</font></b>";
                    item2.setText(initialText2);
                    item.addActionListener(new ActionListener()
                    {
                        
                        public void actionPerformed(ActionEvent e)
                        {
                            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(
                            // "Menu item clicked");
                            // TODO Auto-generated method stub
                            
                            Object[] args = new Object[2];
                            args[0] = AIFUtility.getActiveDesktop();
                            args[1] = (InterfaceAIFComponent) sendToItem[0]; /*
                                                                              * theComponent
                                                                              * is
                                                                              * of
                                                                              * type
                                                                              * TCComponent
                                                                              * ,
                                                                              * this
                                                                              * should
                                                                              * have
                                                                              * the
                                                                              * component
                                                                              * that
                                                                              * you
                                                                              * intend
                                                                              * to
                                                                              * open
                                                                              */
                            
                            AbstractAIFCommand openCmd;
                            try
                            {
                                openCmd = session.getOpenCommand(args);
                                openCmd.executeModal();
                            }
                            catch (Exception e1)
                            {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }
                            
                            // ApplicationDef navigatorApp =
                            // AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey("com.teamcenter.rac.explorer.ExplorerApplication");
                            // if( navigatorApp == null)
                            // navigatorApp =
                            // AIFUtility.getAIFApplicationDefMgr().getApplicationDefByTitle("Navigator");
                            // navigatorApp.openApplication(
                            // (InterfaceAIFComponent[]) sendToItem);
                        }
                        
                    });
                    
                    item2.addActionListener(new ActionListener()
                    {
                        
                        public void actionPerformed(ActionEvent e)
                        {
                            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(
                            // "Menu item clicked");
                            // TODO Auto-generated method stub
                           /* TC10.1 Upgrade
                            * ApplicationDef navigatorApp = AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey(
                                    "com.teamcenter.rac.pse.PSEApplication");*/
                            // ApplicationDef navigatorApp =
                            // AIFUtility.getAIFApplicationDefMgr().getApplicationDefByTitle("explorer");
                            //navigatorApp.openApplication((InterfaceAIFComponent[]) sendToItem);TC10.1 Upgrade
                            
                            IPerspectiveDef perspectiveDef = PerspectiveDefHelper.getPerspectiveDefByLegacyAppID("com.teamcenter.rac.pse.PSEApplication");//TC10.1 Upgrade
                            perspectiveDef.openPerspective((InterfaceAIFComponent[]) sendToItem);//TC10.1 Upgrade
                        }
                        
                    });
                    
                    popupMenu.add(item);
                    popupMenu.add(item2);
                    popupMenu.show((java.awt.Component) m_table, mouseEvt.getX(), mouseEvt.getY());
                }
                
            }
        }));
        // @swamy End
        
        objsNotFoundlbl = new JLabel();
        objsNotFoundlbl.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 12));
        tablePanel.add(lblBtnPanel);
        tablePanel.add(scPane);
        // tablePanel.add(objsNotFoundlbl);
        
        m_table.setRowSorter(null);
        
        populateTableData(selComponent);
        
        SWTUIUtilities.embed(tableComp, tablePanel, false);
        
        Button buttonOK = new Button(shell, SWT.PUSH);
        buttonOK.setText("OK");
        buttonOK.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                shell.dispose();
            }
        });
        
        // TCDECREL-2931:Start
        if (m_table != null)
        {
            if (m_table.getRowCount() == 0)
            {
                m_bIsComponentsEditable = false;
            }
            else if (m_table.getRowSorter() == null)
            {
                m_table.setRowSorter(m_whrUsedTableSorter);
                m_whrUsedTableSorter.setSortable(0, false);
                m_whrUsedTableSorter.sort();
            }
        }
        setLabel();
        setEditability();
        setFontData();
        setImage();
        addListeners();
        
        // TCDECREL-2931:End
        
        shell.setSize(640, 540);
        centerToScreen();
        shell.open();
        
    }
    
    // TCDECREL-2931:Start
    private void setLabel()
    {
        m_filterButton.setText(registry.getString("FilterButton.NAME"));
        m_exportButton.setText(registry.getString("ExportButton.NAME"));
    }
    
    private void setEditability()
    {
        m_filterButton.setEnabled(m_bIsComponentsEditable);
        m_exportButton.setEnabled(m_bIsComponentsEditable);
    }
    
    private void setFontData()
    {
        Font font = new java.awt.Font("Dialog", java.awt.Font.BOLD, 12);
        
        m_itemRevlbl.setFont(font);
        m_numOfref.setFont(font);
        m_revRulelbl.setFont(font);
        
        m_filterButton.setFont(font);
        m_exportButton.setFont(font);
    }
    
    private void setImage()
    {
        ImageIcon imgExport = TCTypeRenderer.getTypeIcon("MSExcel", null);
        m_exportButton.setIcon(imgExport);
    }
    
    private void addListeners()
    {
        m_filterListener = new WhereUsedTableFilterListener(this);
        
        m_filterButton.addActionListener(m_filterListener);
        m_exportButton.addActionListener(new ExportToExcelButtonListener(m_table));
    }
    
    private void centerToScreen()
    {
        Monitor primary = shell.getDisplay().getPrimaryMonitor();
        Rectangle bounds = primary.getBounds();
        Rectangle rect = shell.getBounds();
        int x = bounds.x + (bounds.width - rect.width) / 2;
        int y = bounds.y + (bounds.height - rect.height) / 2;
        shell.setLocation(x, y);
        
    }
    
    // TCDECREL-2931:End
    
    // @Swamy : This part of the code is now pushed to server side
    boolean isLatestReleased(TCComponentItemRevision comp, Vector<TCComponent> refItems)
    {
        // try {
        // TCComponentItem item = comp.getItem();
        // if( !refItems.contains(item))
        // {
        // TCComponentItemRevision latestRev = item.getLatestItemRevision();
        // TCComponentItemRevision[] releasedRevs =
        // item.getReleasedItemRevisions();
        // for(int i=0;i<releasedRevs.length;i++)
        // {
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Revs - " +
        // releasedRevs[i].getProperty("item_revision_id"));
        // }
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("WreleasedRevs :  "
        // + releasedRevs.length);
        // if(releasedRevs.length > 0 )
        // {
        // if( releasedRevs[releasedRevs.length-1] == comp)
        // {
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Revs :  " +
        // comp.getProperty("item_revision_id") + " -- " +
        // releasedRevs[releasedRevs.length-1].getProperty("item_revision_id")
        // );
        // refItems.add(item);
        // // for latest released rev
        // return true;
        // } else if(latestRev == comp){
        // // for existing working rev
        // refItems.add(item);
        // return true;
        // }
        //
        // } else {
        // //For first rev
        // refItems.add(item);
        // return true;
        // }
        // }
        // } catch (TCException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        
        try
        {
            TCComponentItem item = comp.getItem();
            if (!refItems.contains(item))
            {
                TCComponentItemRevision latestRev = item.getLatestItemRevision();
                TCComponentItemRevision[] releasedRevs = item.getReleasedItemRevisions();
                for (int i = 0; i < releasedRevs.length; i++)
                {
                    /*
                     * com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Revs - "
                     * + releasedRevs[i].getProperty("item_revision_id"));
                     */
                }
                // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("WreleasedRevs :  "
                // + releasedRevs.length);
                if (releasedRevs.length > 0)
                {
                    /*
                     * com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Revs :  "
                     * + comp.getProperty("item_revision_id") + " -- " +
                     * releasedRevs[releasedRevs.length -
                     * 1].getProperty("item_revision_id"));
                     */
                    /*
                     * if( releasedRevs[releasedRevs.length-1] == comp) {
                     * com.nov
                     * .rac.dhl.eco.helper.NOVLogger.NOVLog.info("Revs :  " +
                     * comp.getProperty("item_revision_id") + " -- " +
                     * releasedRevs
                     * [releasedRevs.length-1].getProperty("item_revision_id")
                     * ); refItems.add(item); // for latest released rev return
                     * true; } else
                     */if (latestRev == comp)
                    {
                        
                        // for existing working rev
                        refItems.add(item);
                        return true;
                    }
                    
                }
                else
                {
                    // For first rev
                    refItems.add(item);
                    return true;
                }
            }
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return false;
    }
    
    private void populateTableData(TCComponent selComponent2)
    {
        //int refLength = 0;
        if (selComponent2 != null)
        {
            try
            {
                // String[] itemPropVals = null;
                // String[] itemMasterPropVals = null;
                // String[] itemMasterRevPropVals = null;
               // Vector<TCComponent> usdRefRevisions = new Vector<TCComponent>();
                // Vector<TCComponent> refItems = new Vector<TCComponent>();
                // TCComponent[] referencedComps;
                TCComponent[] refComps = null;
                if (selComponent2.getType().startsWith("Documents Revision"))
                {
                    // Before POM Query
                    /*
                     * refLength=0; shell.setText("Where Reference Report");
                     * revRulelbl.setText(
                     * "Revision Rule : Latest Released SMDL");
                     * itemRevlbl.setText
                     * ("Report Item : "+selComponent2.toString());
                     * itemRevlbl.setPreferredSize(new Dimension(520,20));
                     * //AIFComponentContext[] comps =
                     * ((TCComponentItemRevision)
                     * selComponent2).getItem().whereReferenced();
                     * com.nov.rac.dhl
                     * .eco.helper.NOVLogger.NOVLog.info("Where Ref "); String[]
                     * items = {"Nov4Part Revision","Non-Engineering Revision"};
                     * String[] relations =
                     * {"RelatedDefiningDocument","RelatedDocuments"};
                     * AIFComponentContext[] comps =
                     * ((TCComponentItemRevision)selComponent2
                     * ).getItem().whereReferencedByTypeRelation(items,
                     * relations);
                     * numOfref.setText("Total Number of references : " +
                     * comps.length);
                     * com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog
                     * .info("Where Ref No : "+comps.length); if (comps!=null )
                     * { for (int i = 0; i < comps.length; i++) { if
                     * (comps[i].getComponent() instanceof
                     * TCComponentItemRevision) { boolean isLatestRelRev =
                     * isLatestReleased
                     * ((TCComponentItemRevision)comps[i].getComponent
                     * (),refItems); if(isLatestRelRev) {
                     * usdRefRevisions.add((TCComponentItemRevision
                     * )comps[i].getComponent()); refLength++; } } } }
                     * numOfref.setText("Total Number of references : " +
                     * refLength);
                     * com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info
                     * ("Where Ref No : "+refLength);
                     */

                    // After Pom Query
                    
                    m_bIsPart = false;
                    
                    //refLength = 0;
                    shell.setText("Where Reference Report");
                    shell.setImage(docICON);
                    m_revRulelbl.setText("Revision Rule : Latest Released SMDL");
                    m_itemRevlbl.setText("Report Item : " + selComponent2.toString());
                    
                    final WhereUsedOperation whereUsed = new WhereUsedOperation(selComponent2);
                    final ProgressMonitorDialog  dialog = new ProgressMonitorDialog(AIFUtility.getActiveDesktop().getShell());
                    
                    try {
                        dialog.run(true, false, whereUsed);
                    } catch (InvocationTargetException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                   refComps =  m_refComps;
                    // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Start Prop : ");
                    //String[] reqdProperties = { "item_id", "object_name", "item_revision_id", "PartSubType", "Sites",
                    //        "Lifecycle" };
                    //String[][] retriviedProperties = null;
                    
                    String[] strItems = null; 
                    if(m_mapWhereUsedPart != null)
                    {
                        strItems =  m_mapWhereUsedPart.keySet().toArray(new String[m_mapWhereUsedPart.size()]);
                    
                        for (int ii = 0; ii < strItems.length;ii++)
                        {
                            Vector<String> rowData = new Vector<String>();
                            Vector<Object> vectStrRow = new Vector<Object>();
                            vectStrRow = m_mapWhereUsedPart.get(strItems[ii]);
                            rowData.add((String) vectStrRow.elementAt(0));
                            rowData.add((String) vectStrRow.elementAt(1));
                            rowData.add((String) vectStrRow.elementAt(2));
                            rowData.add((String) vectStrRow.elementAt(3));
                            rowData.add((String) vectStrRow.elementAt(4));
                            rowData.add((String) vectStrRow.elementAt(5));
                            rowData.add((String) vectStrRow.elementAt(6));
                            rowData.add((String) vectStrRow.elementAt(7));
                           
                            m_whrUsedTableModel.addRow(rowData);
                        }
                    }
                    
                    /*
                    //if(refComps!=null)
                    {
	                    try
	                    {
	                        List<TCComponent> refCompList = Arrays.asList(refComps);
	                        //retriviedProperties = TCComponentType.getPropertiesSet(refComps, reqdProperties);
	                        retriviedProperties = TCComponentType.getPropertiesSet(refCompList, reqdProperties);//TC10.1 Upgrade
	                    }
	                    catch (TCException e)
	                    {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                    }
	                    
	                    for (int ii = 0; ii < refComps.length; ii++)
	                    {
	                        Vector<String> rowData = new Vector<String>();
	                        rowData.add(retriviedProperties[ii][0]);
	                        rowData.add(retriviedProperties[ii][1]);
	                        rowData.add(retriviedProperties[ii][2]);
	                        rowData.add(retriviedProperties[ii][3]);
	                        rowData.add(retriviedProperties[ii][4]);
	                        rowData.add(retriviedProperties[ii][5]);
	                        rowData.add("");
	                        rowData.add("");
	                        m_whrUsedTableModel.addRow(rowData);
	                    }
                    }*/
                    // m_itemRevlbl.setPreferredSize(new Dimension(520, 20));
                    
                    // AIFComponentContext[] comps =
                    // ((TCComponentItemRevision)selComponent2).getItem().whereReferenced();
                    // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Where Ref : Before POMQuery");
                    
                    // Userservice POM enquiry for where referenced call
//                    Object[] objs = new Object[1];
//                    objs[0] = ((TCComponentItemRevision) selComponent2).getItem();
//                    Object retObjs = ((TCComponentItemRevision) selComponent2).getSession().getUserService()
//                            .call("NOV_WhereReferenced", objs);
//                    
//                    // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Where Ref : After POMQuery");
//                    
//                    if (retObjs != null)
//                    {
//                        if (retObjs instanceof TCComponent[])
//                        {
//                            
//                            refComps = (TCComponent[]) retObjs;
//                            // referencedComps = new
//                            // TCComponent[refComps.length];
//                            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("retObjs "
//                            // + refComps.length);
//                            for (int i = 0; i < refComps.length; i++)
//                            {
//                                if (refComps[i] instanceof TCComponentItemRevision)
//                                {
//                                    // boolean isLatestRelRev =
//                                    // isLatestReleased((TCComponentItemRevision)refComps[i],refItems);
//                                    // if(isLatestRelRev)
//                                    // {
//                                    usdRefRevisions.add((TCComponentItemRevision) refComps[i]);
//                                    refLength++;
//                                    // }
//                                }
//                            }
//                        }
//                    }
//                    
//                    m_numOfref.setText("Where-reference Count : " + refLength);
                    // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Where Ref No : "
                    // + refLength);
                    
                    // After Pom Query
                }
                else if (selComponent2.getType().startsWith("Nov4Part Revision")
                        || selComponent2.getType().startsWith("Non-Engineering Revision"))
                {
                    m_bIsPart = true;
                    shell.setText("Where Used Report");
                    //TCDECREL:4889:Start
                    TCComponent item = ((TCComponentItemRevision)selComponent2).getItem();
                    TCComponentForm masterForm = (TCComponentForm) item.getRelatedComponent("IMAN_master_form");
                    String itemType = masterForm.getFormTCProperty("rsone_itemtype").toString();
                    if(selComponent2.getType().startsWith("Nov4Part Revision"))
                    {
                        if(itemType.startsWith("Purchased"))
                        {
                            shell.setImage(m_purchasePrtICON);
                        }
                        else if(itemType.startsWith("CAD-Only"))
                        {
                            shell.setImage(m_cadOnlyICON);
                        }
                        else if(itemType.startsWith("Raw Stock"))
                        {
                            shell.setImage(m_rawStockICON);
                        }
                        else
                        {
                            shell.setImage(prtICON);
                        }
                    }
                    else{
                        shell.setImage(m_nonEnggPrtICON);
                    }
                    //TCDECREL:4889:End
                    m_itemRevlbl.setText("Report Item : " + selComponent2.toString());
                    m_revRulelbl.setText("Revision Rule : Latest Released");
                   //TCDECREL-4343 : Start
                    final WhereUsedOperation whereUsed = new WhereUsedOperation(selComponent2);
                    final ProgressMonitorDialog  dialog = new ProgressMonitorDialog(AIFUtility.getActiveDesktop().getShell());
                    
                    try {
                        dialog.run(true, false, whereUsed);
                    } catch (InvocationTargetException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    
                    String[] strItems = null; 
                    if(m_mapWhereUsedPart != null)
                    {
                        strItems =  m_mapWhereUsedPart.keySet().toArray(new String[m_mapWhereUsedPart.size()]);
                    
                        for (int ii = 0; ii < strItems.length;ii++)
                        {
                            Vector<String> rowData = new Vector<String>();
                            Vector<Object> vectStrRow = new Vector<Object>();
                            vectStrRow = m_mapWhereUsedPart.get(strItems[ii]);
                            rowData.add((String) vectStrRow.elementAt(0));
                            rowData.add((String) vectStrRow.elementAt(1));
                            rowData.add((String) vectStrRow.elementAt(2));
                            rowData.add((String) vectStrRow.elementAt(3));
                            rowData.add((String) vectStrRow.elementAt(4));
                            rowData.add((String) vectStrRow.elementAt(5));
                            rowData.add((String) vectStrRow.elementAt(6));
                            rowData.add((String) vectStrRow.elementAt(7));
                           
                            m_whrUsedTableModel.addRow(rowData);
                        }
                    }
                  //TCDECREL-4343 : End
//                    TCComponentRevisionRuleType revisionRuleType = (TCComponentRevisionRuleType) selComponent2
//                            .getSession().getTypeComponent("RevisionRule");
//                    TCComponent[] revRules = revisionRuleType.extent(true);
//                    TCComponentRevisionRule revisionRuleComp = null;
//                    for (int i = 0; i < revRules.length; i++)
//                    {
//                        // if
//                        // (revRules[i].getProperty("object_name").equalsIgnoreCase("Latest Released SMDL"))
//                        /*
//                         * TCDECREL-3887: using revision rule as:Working; Any
//                         * Status
//                         */
//                        if (revRules[i].getProperty("object_name").equalsIgnoreCase("Working; Any Status"))
//                        {
//                            revisionRuleComp = new TCComponentRevisionRule();
//                            revisionRuleComp = (TCComponentRevisionRule) revRules[i];
//                            break;
//                        }
//                    }
//                    // TCComponent[] whereUsedItems = null;
//                    if (revisionRuleComp != null)
//                    {
//                        refComps = ((TCComponentItemRevision) selComponent2).whereUsed(
//                                TCComponentItemRevision.WHERE_USED_CONFIGURED, revisionRuleComp);
//                    }
//                    else
//                    {
//                        refComps = ((TCComponentItemRevision) selComponent2)
//                                .whereUsed(TCComponentItemRevision.WHERE_USED_ALL);
//                    }
//                    
//                    m_numOfref.setText("Where-Used Count : " + refComps.length);
                    // if(whereUsedItems!=null)
                    // {
                    // for (int i = 0; i < whereUsedItems.length; i++)
                    // {
                    // if (whereUsedItems[i] instanceof TCComponentItemRevision)
                    // {
                    // usdRefRevisions.add((TCComponentItemRevision)whereUsedItems[i]);
                    // }
                    // }
                    // }
                }
                
                // String[] reqdProperties = { targetitemid, changedesc,
                // dispinstruction, inprocess,
                // infield, ininventory, assembled };
                //
                // String[][] retriviedProperties = null;
                // retriviedProperties = TCComponentType.getPropertiesSet(
                // dispcomp, reqdProperties );
               
              
                // System.out.println( "item_id" +
                // "object_name"+"item_revision_id"+ "PartSubType" + "Sites" +
                // "Lifecycle" );
                
                //for (int ii = 0; ii < refComps.length; ii++)
                               
                // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Stop Prop : ");
                
                // if (usdRefRevisions.size()>0)
                // {
                // for (int i = 0; i < usdRefRevisions.size(); i++)
                // {
                // TCComponent irComp = usdRefRevisions.get(i);
                //
                // if (itemProps!=null)
                // {
                // itemPropVals = irComp.getProperties(itemProps);
                // }
                // TCComponent masterRevForm =
                // irComp.getRelatedComponent("IMAN_master_form_rev");
                // if (itemRevMasterProps!=null)
                // {
                // itemMasterPropVals =
                // masterRevForm.getProperties(itemRevMasterProps);
                // }
                // TCComponent masterForm =
                // ((TCComponentItemRevision)irComp).getItem().getRelatedComponent("IMAN_master_form");
                // if (itemMasterProps!=null)
                // {
                // itemMasterRevPropVals =
                // masterForm.getProperties(itemMasterProps);
                // }
                // if (itemPropVals!=null)
                // {
                // List<String> propValues = new
                // ArrayList<String>(Arrays.asList(itemPropVals));
                //
                // if (itemMasterRevPropVals!=null)
                // {
                // propValues.addAll(Arrays.asList(itemMasterRevPropVals));
                // }
                // if (itemMasterPropVals!=null)
                // {
                // propValues.addAll(Arrays.asList(itemMasterPropVals));
                // }
                // Vector<String> rowData = new Vector<String>(propValues);
                // table.addRow(rowData);
                // }
                // }
                // }
                // else
                // {
                // objsNotFoundlbl.setText("This Object is not used in any assembly.");
                // }
                // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("end Prop : ");
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    @Override
	public void filterSearchResults() {
		m_filterCriteria = m_filterListener.getSearchCriteria();
		if (m_filterCriteria != null) {
			try {
				/* 1. Remove Row sorter from Where Used table */
				m_table.setRowSorter(null);

				/* 2. Construct Filter criteria and Add to sorter */
				Vector<String> siteFilter = m_filterCriteria
						.getSiteFilterValue();
				Vector<String> lcFilter = m_filterCriteria
						.getLifecycleFilterValue();
				Vector<RowFilter<AIFTableModel, Object>> mainFilterVector = new Vector<RowFilter<AIFTableModel, Object>>(
						2);

				if (!siteFilter.isEmpty()) {

					// Construct Filters for Sites
					int colIndex = m_whrUsedTableModel.findColumn("Sites");
					Vector<RowFilter<AIFTableModel, Object>> tempFilter = new Vector<RowFilter<AIFTableModel, Object>>(
							siteFilter.size());
					for (int i = 0; i < siteFilter.size(); i++) {
						RowFilter<AIFTableModel, Object> rf = RowFilter
								.regexFilter(siteFilter.get(i), colIndex);
						tempFilter.add(rf);
					}
					// TCDECREL-4941 : Start
					RowFilter<AIFTableModel, Object> rf = RowFilter
							.regexFilter("^\\s", colIndex);//TCDECREL-6180
					tempFilter.add(rf);
					// TCDECREL-4941 : End
					RowFilter<AIFTableModel, Object> mainSitesFilter = RowFilter
							.orFilter(tempFilter);
					mainFilterVector.add(mainSitesFilter);

				}
				// TCDECREL-4842 : Start
				else {

					int colIndex = m_whrUsedTableModel.findColumn("Sites");
					//TCDECREL-5627 : If we search with " ", it will match those records which has multiple sites,
					// Multiple sites are seperated by ", ". Instead use some dummy value to filter.
					RowFilter<AIFTableModel, Object> rf = RowFilter
							.regexFilter("^\\s", colIndex);//TCDECREL-6180
					mainFilterVector.add(rf);

				}
				// TCDECREL-4842 : End

				if (!lcFilter.isEmpty()) {
					// Construct Filters for LIfecycle
					int colIndex = m_whrUsedTableModel.findColumn("Lifecycle");
					Vector<RowFilter<AIFTableModel, Object>> tempFilter = new Vector<RowFilter<AIFTableModel, Object>>(
							lcFilter.size());
					for (int i = 0; i < lcFilter.size(); i++) {

						RowFilter<AIFTableModel, Object> rf = RowFilter
								.regexFilter("^"+lcFilter.get(i), colIndex); //TCDECREL-6180
						tempFilter.add(rf);

					}
					// TCDECREL-4941 : Start
					RowFilter<AIFTableModel, Object> rf = RowFilter
							.regexFilter("^\\s", colIndex); //TCDECREL-6180
					tempFilter.add(rf);
					// TCDECREL-4941 : End
					RowFilter<AIFTableModel, Object> mainLifecycleFilter = RowFilter
							.orFilter(tempFilter);
					mainFilterVector.add(mainLifecycleFilter);

				}
				// TCDECREL-4842 : Start
				else {
					int colIndex = m_whrUsedTableModel.findColumn("Lifecycle");
					RowFilter<AIFTableModel, Object> rf = RowFilter
							.regexFilter("^\\s", colIndex); //TCDECREL-6180
					mainFilterVector.add(rf);

				}
				// TCDECREL-4842 : End

				if (!mainFilterVector.isEmpty()) {
					RowFilter<AIFTableModel, Object> mainFilter = RowFilter
							.andFilter(mainFilterVector);
					m_whrUsedTableSorter.setRowFilter(mainFilter);
				} else {
					m_whrUsedTableSorter.setRowFilter(null);
				}

				/* 3. Apply sorter to Where Used table and sort by Item ID */
				m_table.setRowSorter(m_whrUsedTableSorter);
				m_whrUsedTableSorter.setSortable(0, false);
				m_whrUsedTableSorter.sort();

				setWhereUsedCountMsg();
			} catch (java.util.regex.PatternSyntaxException e) {
				return;
			}
		}
	}
    
    private void createWhereUsedTableSorter()
    {
        m_whrUsedTableSorter = new TableRowSorter<AIFTableModel>(m_whrUsedTableModel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                m_table.setRowSorter(this);
                this.sort();
                
                System.out.println("Running custom Sorting: Column " + column + order);
                
            }
        };
        
        List<SortKey> keys = new ArrayList<SortKey>();
        SortKey sortKey = new SortKey(m_whrUsedTableModel.findColumn("Item ID"), SortOrder.ASCENDING);
        keys.add(sortKey);
        m_whrUsedTableSorter.setSortKeys(keys);
        m_table.setRowSorter(m_whrUsedTableSorter);
        m_whrUsedTableSorter.setSortable(0, false);
    }
    
    private void setWhereUsedCountMsg()
    {
        int iTableCount = m_table.getRowCount();
        if (m_bIsPart)
        {
            m_numOfref.setText("Where-Used Count : " + iTableCount);
        }
        else
        {
            m_numOfref.setText("Where-reference Count : " + iTableCount);
        }
    }
    //TCDECREL-4551 : Start
    /**************************
     * Class    : WhereUsedOperation
     * Purpose  : 
     ***************************/
    private class WhereUsedOperation implements IRunnableWithProgress
    {
        private TCComponent m_selectedComp =null;

        public WhereUsedOperation(TCComponent selComponent)
        {
            m_selectedComp = selComponent;
        }

        @Override
        public void run(IProgressMonitor progressMonitor)
                throws InvocationTargetException, InterruptedException
        {
            progressMonitor.beginTask("Teamcenter Processing Where Used Report..",IProgressMonitor.UNKNOWN );
            
            final IProgressMonitor finalprogressMonitor = progressMonitor;
            
            try
            {
                getWhereusedRefData(m_selectedComp);
                //Thread.sleep(5000);
                finalprogressMonitor.done();
            } catch (Exception e) {
                e.printStackTrace();
                finalprogressMonitor.setCanceled(true);
                finalprogressMonitor.done();
            }
        }
    }
    /**************************
     * Function     : getWhereusedRefData()
     * Return Type  : void
     * Purpose      : get all where used\ref object
     * Parameter    : selectedComp     TCComponent
     ***************************/
    public void getWhereusedRefData(TCComponent selectedComp)
    {
         
        if (selectedComp.getType().startsWith("Documents Revision"))
		{		
			NOVSearchWhereUsedDocumentRev srchWhereUsedDocRev = new NOVSearchWhereUsedDocumentRev((TCComponentItemRevision) selectedComp);
			
			m_mapWhereUsedPart = srchWhereUsedDocRev.executeWhereUsedPart();
		    m_numOfref.setText("Where-Used Count : " + 0);
		      
		    if(m_mapWhereUsedPart != null)
		    {
		         m_numOfref.setText("Where-Used Count : " + m_mapWhereUsedPart.size());
		    }		  
		    
		 /* Vector<TCComponent> usdRefRevisions = new Vector<TCComponent>();
			
		    Object[] objs = new Object[1];
		    try {
				objs[0] = ((TCComponentItemRevision) selectedComp).getItem();
				
				Object retObjs = ((TCComponentItemRevision) selectedComp).getSession().getUserService()
						.call("NOV_WhereReferenced", objs);
		    
		    if (retObjs != null)
		    {
		        if (retObjs instanceof TCComponent[])
		        {
		        	TCComponent[] refComps = null;
		            refComps = (TCComponent[]) retObjs;
		            for (int i = 0; i < refComps.length; i++)
		            {
		                if (refComps[i] instanceof TCComponentItemRevision)
		                {
		                    usdRefRevisions.add((TCComponentItemRevision) refComps[i]);
		                }
		            }
		        }
		    }
		    
		    } catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		    
		    m_numOfref.setText("Where-reference Count : " + 0);
		    
		    if(null != usdRefRevisions && usdRefRevisions.size()>0)
		    {
		    	m_refComps  = usdRefRevisions.toArray(new TCComponent[usdRefRevisions.size()]);
		    	m_numOfref.setText("Where-reference Count : " + usdRefRevisions.size());
		    }*/
		    
		}
		else if (selectedComp.getType().startsWith("Nov4Part Revision")
		        || selectedComp.getType().startsWith("Non-Engineering Revision"))
		{                   

		    NOVSearchWhereUsedParts srchWhereUsedParts = new NOVSearchWhereUsedParts((TCComponentItemRevision) selectedComp);
		   
		    m_mapWhereUsedPart = srchWhereUsedParts.executeWhereUsedPart();
		    m_numOfref.setText("Where-Used Count : " + 0);
		    
		    if(m_mapWhereUsedPart != null)
		    {
		        m_numOfref.setText("Where-Used Count : " + m_mapWhereUsedPart.size());
		    }
		    
		  //TCDECREL-4343 : End
		}
    }
    //TCDECREL-4551 : End
}
