package com.nov.rac.dhl.eco.wizard;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.java.parsing.Attributes;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov4.services.rac.classification.NOV4ClassificationService;
import com.nov4.services.rac.classification._2010_09.NOV4Classification.UpdateICOAttributeInput;
import com.nov4.services.rac.classification._2010_09.NOV4Classification.UpdateICOResponse;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVImportICODialog 
{
    private String m_filePath="";
    private Registry m_reg = null;
    private TCComponent m_selectedComponent = null;
    private Shell m_shell = null;
	public NOVImportICODialog(TCComponent component) 
	{
		m_reg = Registry.getRegistry(this);
		m_selectedComponent = component;
		createContents();

	}
	private void createContents()
	{
		m_shell = new Shell(SWT.SHELL_TRIM | SWT.APPLICATION_MODAL);
		m_shell.setImage( Registry.getRegistry(AbstractAIFDialog.class ).getImage("aifDesktop.ICON" ) );
		m_shell.setText(m_reg.getString("ImportICODialog.TITLE"));
		m_shell.setLayout(new GridLayout(1, true));
		createContents(m_shell);
		m_shell.setSize(600, 175);
		NOVECOHelper.centerToScreen(m_shell);
		m_shell.open();
		while (!m_shell.isDisposed()) {
			if (!Display.getDefault().readAndDispatch())
				Display.getDefault().sleep();
		}
	}
	public Control createContents(Composite parent)
	{
		final Composite composite = new Composite(parent, SWT.NONE);
		        
		addSelectFilePanel(composite);
		
		addButtonPanel(composite);
		
		return composite;
	}
	protected Control addButtonPanel(Composite composite)
	{
		Composite buttonComposite = new Composite(composite, SWT.CENTER);
		
		buttonComposite.setLayout(new GridLayout(2,true));
        GridData gd_composite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        buttonComposite.setLayoutData(gd_composite);

		Button okButton = new Button(buttonComposite, SWT.PUSH);
		GridData gridData = new GridData(SWT.FILL,SWT.FILL,true,true,1,3);
		gridData.widthHint = 50;
		okButton.setLayoutData(gridData);
		
		okButton.setText(m_reg.getString("okButton.NAME"));
		okButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) 
			{
				if(m_filePath != null && m_filePath.length()>0)
				{
					parsheXMLAndUpdateICO(m_filePath);
					System.out.println(m_filePath);
					m_shell.dispose();
				}
				else
				{
					MessageBox.post(m_shell,m_reg.getString("NOXMLFileSelected.MSG"),m_reg.getString("NOXMLFileSelected.TITLE"),MessageBox.ERROR);
				}
			}
		});		
		Button cancelButton = new Button(buttonComposite, SWT.PUSH);
		cancelButton.setText(m_reg.getString("cancelButton.NAME"));
		
		cancelButton.setLayoutData(gridData);
		
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				m_shell.dispose();
			}
		});
		return composite;
	}
	protected Control addSelectFilePanel(Composite childComposite)
	{
		GridLayout layout = new GridLayout(2, false);
		layout.verticalSpacing = 10;
		childComposite.setLayout(layout);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, false,1,1);
		childComposite.setLayoutData(data);
		
		Label fileLbl = new Label(childComposite,SWT.BOLD);
		fileLbl.setText(m_reg.getString("SelectXMLFile.LABEL"));
		fileLbl.setSize(10,10);
		
		Composite pathComp = new Composite(childComposite, SWT.BOLD);
		pathComp.setLayout(new GridLayout(2,false));
		data = new GridData(SWT.FILL, SWT.FILL, true, false,1,1);
		pathComp.setLayoutData(data);
		
		final Text filePathTxt = new Text(pathComp,SWT.BOLD | SWT.BORDER | SWT.SCROLL_LINE);
		filePathTxt.setText("");
		filePathTxt.setSize(300, 10);
		data = new GridData(SWT.FILL, SWT.FILL, true, false,1,1);
		filePathTxt.setLayoutData(data);
		
		Button selectFileBtn = new Button(pathComp,SWT.PUSH);
		selectFileBtn.setText("...");
		selectFileBtn.setSize(5, 10);
		
		final Composite CHILDCOMP = childComposite;
		selectFileBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				String path = addFileDialog(CHILDCOMP);
				if(path != null)
				{
					filePathTxt.setText(path);
					filePathTxt.setEditable(false);
				}
			
			};
		});
		return childComposite;
 }

 	protected String addFileDialog(Composite childComposite)
 	{
        Shell shell = childComposite.getShell();
 		FileDialog fd = new FileDialog(shell, SWT.OPEN);
        fd.setText("Open");
        String[] filterExt = { "*.xml"};
        fd.setFilterExtensions(filterExt);
        String selected = fd.open();
        this.m_filePath = selected;
        return selected;
      }
     @SuppressWarnings("unchecked")
	private void parsheXMLAndUpdateICO(String filePath)
     {
    	 File file = new File(filePath);
    	 try 
    	 {
			JAXBContext jaxbContext = JAXBContext.newInstance(Attributes.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Attributes attributes = (Attributes) jaxbUnmarshaller.unmarshal(file);
			HashMap<String, String> AttrIdValMap = new HashMap<String, String>();
			HashMap<String, String> AttrIdNameMap = new HashMap<String, String>();
			List<Attributes.Attribute> attrList = attributes.getAttribute();
			Iterator<Attributes.Attribute> attrItr = attrList.iterator();
            while (attrItr.hasNext()) 
            {
            	Attributes.Attribute attr = attrItr.next();
            	if(attr.getValue().length()>0)
            	{
            		AttrIdValMap.put(attr.getID()+"", attr.getValue());
            		AttrIdNameMap.put(attr.getID()+"", attr.getName());
            	}      		
        	}
            updateICO(AttrIdValMap,AttrIdNameMap);
			
		} 
    	 catch (JAXBException e) 
    	 {
    		 String sMsg = "";
    		 if(e.getMessage() != null)
    		 {
    			 sMsg = e.getMessage()+"\n";
    		 }
    		 sMsg = sMsg+e.getLinkedException().getMessage();
    		 MessageBox.post(m_shell,sMsg,"Error...",MessageBox.ERROR);
		}
     }
     private void updateICO(HashMap<String,String> attrIdValMap,HashMap<String,String> attrIdNameMap)
     {
    	 m_shell.setCursor(new Cursor(m_shell.getDisplay(), SWT.CURSOR_WAIT));
    	 
     	 
    	 NOV4ClassificationService classService = NOV4ClassificationService.getService(m_selectedComponent.getSession());
			
    	 UpdateICOAttributeInput icoAttrInput = new UpdateICOAttributeInput();

    	 icoAttrInput.classifiedObjTags = new TCComponent[]{m_selectedComponent};
    	 icoAttrInput.icoAttrInfo = attrIdValMap;

    	 UpdateICOResponse updateResponse = classService.updateICO(icoAttrInput);
    	 
    	 m_shell.setCursor(new Cursor(m_shell.getDisplay(), SWT.CURSOR_ARROW));
    	 if(updateResponse.serviceData.sizeOfPartialErrors()>0)
    	 {
    		 String sMsg = updateResponse.serviceData.getPartialError(0).getMessages()[0]+"\n";

    		 for(int inx=0;inx<updateResponse.failedAttributeIds.length;inx++)
    		 {
    			 sMsg = sMsg + "\n" +attrIdNameMap.get(updateResponse.failedAttributeIds[inx]);
    		 }
    		 MessageBox.post(m_shell,sMsg,"Error...",MessageBox.ERROR);
    	 }
    	 else
    	 {
    		 MessageBox.post(m_shell,m_reg.getString("ClassAttrUpdateSuccess.MSG"),m_reg.getString("ClassAttrUpdateSuccess.TITLE"),MessageBox.INFORMATION);
    	 }
     }
}
  



