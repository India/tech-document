package com.nov.rac.dhl.eco.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;

public class NOVRemoveDatasetHandler extends AbstractHandler
{

	
	public Object execute(ExecutionEvent arg0) throws ExecutionException 
	{

		InterfaceAIFComponent[] selComps = AIFUtility.getTargetComponents();
		
		//Checking selection
		if(selComps.length == 0)
		{
			MessageBox.post("No objects Selected " +
					"\nSelect a Dataset.","Error", MessageBox.ERROR);
			return null;
		}
		
		//need to check group, allow only DHL 
		//Code need to added to check valid group
		
		boolean dhItemCreationFlag = false;
		TCSession session = ((TCSession)AIFUtility.getDefaultSession());
		String currentUsrGrp = session.getCurrentGroup().toString();
		String[] grpStrArray = session.getPreferenceService().getStringArray(TCPreferenceService.TC_preference_site,
                		"_NOV_DH_item_creation_groups_");
		
		for (int i = 0; i < grpStrArray.length; i++) 
		{
			if(currentUsrGrp.equals(grpStrArray[i]))
            {
                dhItemCreationFlag = true;
                break;
            }	
		}
		
		if (!dhItemCreationFlag) 
		{
			MessageBox.post("Not Valid operation for logged in group","Error", MessageBox.ERROR);
			return null;
		}
		//remove dataset from workflow
		if (selComps.length>0) 
		{
			String inValidaDatasets = "";
			for (int i = 0; i < selComps.length; i++) 
			{
				if(selComps[i] instanceof TCComponentDataset)
				{
					try 
					{
						TCComponentProcess tcJob = ((TCComponentDataset)selComps[i]).getCurrentJob();
						TCComponent[] inProcessComp = ((TCComponentDataset)selComps[i]).getTCProperty("process_stage_list").getReferenceValueArray();
						if (tcJob!=null && (inProcessComp.length > 0)) 
						{
							tcJob.getRootTask().removeAttachments(new TCComponent[]{(TCComponent)selComps[i]});	
							tcJob.getRootTask().refresh();
							((TCComponent)selComps[i]).refresh();
						}
						else
						{
							if (inValidaDatasets.length()>0) 
							{
								inValidaDatasets = inValidaDatasets+","+selComps[i].toString();	
							}
							else
							{
								inValidaDatasets = selComps[i].toString();
							}
						}
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			}
			if (inValidaDatasets.length()>0) 
			{
				MessageBox.post("Dataset(s) " +inValidaDatasets+
						" not in process.","Info", MessageBox.INFORMATION);
			}
		}		
		return null;
	}

}
