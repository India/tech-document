package com.nov.rac.dhl.eco.validations.removeitem;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.teamcenter.rac.kernel.TCComponent;

public class NOVRemoveECRTableHeaderRenderer implements TableCellRenderer, MouseListener {

	private static final long	serialVersionUID = 1L;
	private JCheckBox			m_checkBox;
	private boolean 			m_mousePressed = false;
	private int					m_column = -1;

	public NOVRemoveECRTableHeaderRenderer(ActionListener actionListener) 
	{
		m_checkBox = new JCheckBox();
		m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
		m_checkBox.addActionListener(actionListener);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
	      boolean isSelected, boolean hasFocus, int row, int column) 
	{
		JTableHeader header = table.getTableHeader();
		header.setLayout(new BorderLayout()); 
		
		m_checkBox.setForeground(header.getForeground());   
		m_checkBox.setBackground(header.getBackground());   
		m_checkBox.setFont(header.getFont());   

		header.addMouseListener(this);
		m_column = column;
	    return m_checkBox;
    }
	
	protected Icon getDisplayIcon(TCComponent comp , Object arg1 )
    {
        return null;
    }
	
	protected void handleClickEvent(MouseEvent mouseevent)
	{
		
		if (m_mousePressed) 
		{
			m_mousePressed = false;
			
			JTableHeader header = (JTableHeader) (mouseevent.getSource());
			NOVRemoveECRTable table = (NOVRemoveECRTable) header.getTable();
			TableColumnModel columnModel = table.getColumnModel();

			int viewColumn = columnModel.getColumnIndexAtX(mouseevent.getX());

			int column = table.convertColumnIndexToModel(viewColumn);

			if (viewColumn == this.m_column && column != -1 && m_checkBox.isEnabled()) 
			{
				m_checkBox.setSelected(!m_checkBox.isSelected());
				m_checkBox.repaint();
				TableModel tableModel = table.getModel();
				for(int i=0; i < tableModel.getRowCount(); i++)
				{
					tableModel.setValueAt(m_checkBox.isSelected(), i, column);
				}
				table.repaint();	
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
		handleClickEvent(mouseevent);
		((JTableHeader) mouseevent.getSource()).repaint();
	}

	@Override
	public void mouseEntered(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
		m_mousePressed = true;
	}

	@Override
	public void mouseReleased(MouseEvent mouseevent) {
		// TODO Auto-generated method stub
	}
	public void setValue(boolean bVal)
	{
	    if(m_checkBox.isEnabled())
	    {
	        m_checkBox.setSelected(bVal);
	        m_checkBox.repaint();
	    }
	}
	public void setEnabled(boolean bVal)
	{
	    m_checkBox.setEnabled(bVal);
        m_checkBox.repaint();
	}
}

