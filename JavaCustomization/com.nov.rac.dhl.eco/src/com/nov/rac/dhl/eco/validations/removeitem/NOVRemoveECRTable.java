package com.nov.rac.dhl.eco.validations.removeitem;

import java.util.Arrays;

import javax.swing.ToolTipManager;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class NOVRemoveECRTable extends TCTable
{
    private static final long serialVersionUID = 1L;
    private Registry m_registry      = null;
    private int m_iIgnoreSortColumn = -1;
    public NOVRemoveECRTable(TCSession session, String columns[])
    {
        super(columns);
        m_registry = getRegistry();
               
        setModel(columns);
        setSession( session );
        
        assignColumnRenderer();
        assignCellEditor();
        //setColumnWidths();
    }
      
	protected void setModel(String[] columns) 
	{
		dataModel = new NOVRemoveECRTableModel(columns);        
        setModel(dataModel);
	}
	
    public void assignColumnRenderer()
    {
        ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        String s = r.getString("default.RENDERER");
       
        
        for(int j = 0; j < i; j++)
        {
           	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".RENDERER").toString(), s);
           
            try
            {
            	TableCellRenderer abstracttctablecellrenderer = (TableCellRenderer)Instancer.newInstance(s1);
                if(abstracttctablecellrenderer != null)
                    getColumnModel().getColumn(j).setCellRenderer(abstracttctablecellrenderer);
            }
            catch(Exception exception)
            {
                
            }
        }

        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }
    
    public void assignCellEditor()
    {
        ToolTipManager.sharedInstance().unregisterComponent(this);
        int i = getColumnCount();
        Registry r= Registry.getRegistry(this);
        
        for(int j = 0; j < i; j++)
        {
        	String  s1 = r.getString((new StringBuilder()).append(getColumnPropertyName(j)).append(".EDITOR").toString(),null);
            if(s1 != null && s1.length() > 0)
            {
	            try
	            {
	            	TableCellEditor cellEditor = (TableCellEditor)Instancer.newInstance(s1);
	               
	                if(getColumnModel().getColumn(j).getCellEditor() == null)
	                {
	                    getColumnModel().getColumn(j).setCellEditor(cellEditor);
	                }
	            }
	           catch(Exception exception)
	            {
	                
	            }
            }
            
        }

        ToolTipManager.sharedInstance().registerComponent(this);
        validate();
        repaint();
    }
    @Override
    public boolean isCellEditable(int iRow, int iCol)
    {
        String columnName =getColumnPropertyName(iCol);
        
        String[] editableColumns=m_registry.getStringArray("editableColumns.Name");
                    
        if(!Arrays.asList(editableColumns).contains(columnName) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
	public Object getValueAtDataModel(int iRow, int iCol)
	{
		Object retValue = null;
		
		retValue = getModel().getValueAt(convertRowIndexToModel(iRow), iCol);
	
		return retValue;
	}
	
	public void setColumnWidths()
    {
        DefaultTableColumnModel defaulttablecolumnmodel = (DefaultTableColumnModel) getColumnModel();
        charWidth = (int) 8.5;
        int i = defaulttablecolumnmodel.getColumnCount();
        for ( int j = 0; j < i; j++ )
        {
            TableColumn tablecolumn = defaulttablecolumnmodel.getColumn(j);
            try
            {
                int integer = tablecolumn.getHeaderValue().toString().length();
                tablecolumn.setPreferredWidth((integer * charWidth) + 20);
            }
            catch ( Exception exception )
            {
            }
        }
        currentColumnNames = new String[i];
        currentColumnWidths = new int[i];
        for ( int k = 0; k < i; k++ )
        {
            TableColumn tablecolumn1 = defaulttablecolumnmodel.getColumn(k);
            int l = tablecolumn1.getWidth();
            currentColumnWidths[k] = l / charWidth;
            currentColumnNames[k] = getColumnName(k);
        }
    }
	
	protected Registry getRegistry()
	{
		return Registry.getRegistry(this);
	}    
	
	@Override
	public synchronized void sortColumn(int i)
	{
	   if(m_iIgnoreSortColumn != i)
	   {
	       super.sortColumn(i);
	   }
	}
	
	public void ignoreSortingColumn(int iCol)
	{
	    m_iIgnoreSortColumn = iCol;
	}
	
	@Override
	protected void setColumnRenderer(TableColumn tablecolumn)
	{
	    if(tablecolumn.getModelIndex() != m_iIgnoreSortColumn)
	    {
	        super.setColumnRenderer(tablecolumn);
	    }	   
	}
}
