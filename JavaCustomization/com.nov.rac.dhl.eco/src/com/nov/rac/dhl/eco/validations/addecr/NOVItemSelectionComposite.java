package com.nov.rac.dhl.eco.validations.addecr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class NOVItemSelectionComposite extends Composite
{
    private Label m_titleLabel;
    private NOVCustomTable m_itemSelectionTable;
    private TCSession m_session;
    private Registry m_registry;
    private int m_iCheckboxColIndex = 0;
    private boolean m_SelectAll = false;
    private TableColumn m_checkBoxCol;
    private final int m_iCheckBoxColWidth = 25;
    private Set<TCComponentItem> m_itemSet = null;
    
    public NOVItemSelectionComposite(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        m_session = (TCSession) AIFUtility.getDefaultSession();
        createUI(this);
    }
    
    private void createUI(Composite parent)
    {
        Composite itemSelectionComp = parent;
        itemSelectionComp.setLayout(new GridLayout(1, false));
        GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        itemSelectionComp.setLayoutData(gd_composite);
        
        m_titleLabel = new Label(itemSelectionComp, SWT.NONE);
        
        setLabels();
        
        Composite tableComposite = new Composite(itemSelectionComp, SWT.NO_BACKGROUND | SWT.EMBEDDED);
        tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        createTable();
        
        setRendererEditor();
        
        setColumnWidth();
        
        addListeners();
        
        ScrollPagePane tablePane = new ScrollPagePane(m_itemSelectionTable);
        
        SWTUIUtilities.embed(tableComposite, tablePane, false);
    }
    
    private void setLabels()
    {
        m_titleLabel.setText(m_registry.getString("ItemSelectionLabel.MSG"));
    }
    
    private void createTable()
    {
        String[] strColNames = null;
        
        strColNames = m_registry.getStringArray("ItemSelectionTable.COLUMNS");
        
        m_itemSelectionTable = new NOVCustomTable(m_session, strColNames);
        
        m_itemSelectionTable.setAutoResizeMode(TCTable.AUTO_RESIZE_ALL_COLUMNS);
        m_itemSelectionTable.getTableHeader().setReorderingAllowed(false);
        
        m_itemSelectionTable.ignoreSortingColumn(m_iCheckboxColIndex);
    }
    
    private void setRendererEditor()
    {
        m_checkBoxCol = m_itemSelectionTable.getColumnModel().getColumn(m_iCheckboxColIndex);
        
        m_checkBoxCol.setCellEditor(m_itemSelectionTable.getDefaultEditor(Boolean.class));
        // m_checkBoxCol.setCellRenderer(m_itemSelectionTable.getDefaultRenderer(Boolean.class));
        
        NOVTableHeaderRenderer tableHeaderRenderer = new NOVTableHeaderRenderer(new SelectAllListener());
        m_checkBoxCol.setHeaderRenderer(tableHeaderRenderer);
    }
    
    private void setColumnWidth()
    {
        m_checkBoxCol.setResizable(false);
        m_checkBoxCol.setMaxWidth(100);
        m_checkBoxCol.setPreferredWidth(m_iCheckBoxColWidth);
    }
    
    private void addListeners()
    {
        // m_itemSelectionTable.addMouseListener(new NOVTableMouseListener());
    }
    
    public void selectAllRows(boolean bSelectAll)
    {
        int iNoOfRows = m_itemSelectionTable.getRowCount();
        for (int iCnt = 0; iCnt < iNoOfRows; iCnt++)
        {
            m_itemSelectionTable.setValueAt(bSelectAll, iCnt, m_iCheckboxColIndex);
        }
        m_itemSelectionTable.repaint();
    }
    
    public void populateItemTable(Object[] ecrs)
    {
        TableModel tableModel = m_itemSelectionTable.getModel();
        
        for (int indx = 0; indx < ecrs.length; indx++)
        {
            TCComponentForm ecrForm = (TCComponentForm) ecrs[indx];
            try
            {
                String sECR = ecrForm.getTCProperty("object_name").getStringValue();
                Vector<Object> sECRInfoVect = new Vector<Object>();
                
                AIFComponentContext[] compConxt = ecrForm.whereReferenced();
                if (compConxt != null)
                {
                    m_itemSet = new HashSet<TCComponentItem>();
                    for (int j = 0; j < compConxt.length; j++)
                    {
                        InterfaceAIFComponent intAifComp = compConxt[j].getComponent();
                        if (intAifComp instanceof TCComponentItemRevision)
                        {
                            TCComponentItemRevision itemRevAttached = (TCComponentItemRevision) intAifComp;
                            
                            TCComponentItem item = itemRevAttached.getItem();
                            if (!isItemExist(item))
                            {
                                TCComponentItemRevision latestRev = item.getLatestItemRevision();
                                
                                boolean isInProcess = NOVECOHelper.isItemRevInProcess(latestRev);
                                
                                sECRInfoVect.addElement(false);
                                sECRInfoVect.addElement(latestRev.getTCProperty("item_id").getStringValue());
                                sECRInfoVect.addElement(latestRev.getTCProperty("object_name").getStringValue());
                                ;
                                sECRInfoVect.addElement(sECR);
                                
                                NOVCustomTableLine tableLine = null;
                                if (tableModel instanceof NOVCustomTableModel)
                                {
                                    NOVCustomTableModel itemTableModel = (NOVCustomTableModel) tableModel;
                                    tableLine = itemTableModel.createLine(sECRInfoVect.toArray());
                                    tableLine.setTableLineFormObject(ecrForm);
                                    tableLine.setTableLineRevObject(latestRev);
                                    if (isInProcess)
                                    {
                                        tableLine.setTableLineToolTipText(m_registry.getString("ItemRevInProcess.MSG"));
                                        tableLine.setTableLineEditableStatus(false);
                                    }
                                    m_itemSelectionTable.addRow(tableLine);
                                    sECRInfoVect.clear();
                                }
                            }// 4809 added
                            
                        }
                        
                    }
                    clearSetElements(m_itemSet);
                    
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public NOVCustomTable getItemSelectionTable()
    {
        return m_itemSelectionTable;
    }
    
    class SelectAllListener implements ActionListener
    {
        
        @Override
        public void actionPerformed(ActionEvent e)
        {
            // TODO Auto-generated method stub
            if (m_SelectAll)
            {
                m_SelectAll = false;
            }
            else
            {
                m_SelectAll = true;
            }
            selectAllRows(m_SelectAll);
        }
    }
    
    private boolean isItemExist(TCComponentItem item)
    {
        boolean isExist = true;
        if (m_itemSet.contains(item))
        {
            isExist = true;
        }
        else
        {
            m_itemSet.add(item);
            isExist = false;
        }
        
        return isExist;
    }
    
    private void clearSetElements(Set<TCComponentItem> m_itemSet)
    {
        if (m_itemSet.size() > 0)
        {
            m_itemSet.clear();
        }
    }
    
}
