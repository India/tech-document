package com.nov.rac.dhl.eco.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JComboBox;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.revisehelper.ReviseObjectsSOAHelper;
import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement;
import com.teamcenter.rac.aif.common.AIFIdentifier;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentRole;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.client.model.ErrorStack;
import com.nov.rac.dhl.eco.wizard.NOVCreateECOHelper;

public class NOVECOHelper
{
    private static String[] m_sTypeStr = { "_DHL_EngChangeForm_" };
    private static String[] m_sRelation = { "_ECNECR_" };
    public static String m_sPrefAddCompToTargetSOA = "addComponentToTargetSOA";
    
    public static void retriveLOVValues(TCSession tcSession, String LOV_NAME, JComboBox comboBox)
    {
        try
        {
            TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName(tcSession, LOV_NAME);
            //4682-added if condition to check the null value
            if(lovValues != null)
            {
            ListOfValuesInfo lovInfo = lovValues.getListOfValues();
            Object[] lovS = lovInfo.getListOfValues();
            if (lovS.length > 0)
            {
                comboBox.addItem("");
                for (int i = 0; i < lovS.length; i++)
                {
                    comboBox.addItem(lovS[i]);
                }
            }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    public static TCComponentItemRevision getRelatedPartRevision(TCComponentItemRevision docRev)
    {
        try
        {
            if (docRev != null)
            {
                // AIFComponentContext[] relatedPartRevs =
                // docRev.getItem().whereReferencedByTypeRelation(new
                // String[]{"Nov4Part","Non-Engineering"}, new
                // String[]{"RelatedDefiningDocument"});
                AIFComponentContext[] relatedPartRevs = docRev.getItem().whereReferenced();
                
                for (int i = 0; i < relatedPartRevs.length; i++)
                {
                    if (relatedPartRevs[i].getContext().equals("RelatedDefiningDocument"))
                    {
                        if (relatedPartRevs[i].getComponent() instanceof TCComponentItemRevision)
                        {
                            TCComponentItem relatePart = ((TCComponentItemRevision) relatedPartRevs[i].getComponent())
                                    .getItem();
                            String revId = docRev.getProperty("current_revision_id");
                            TCComponentItemRevision partRev = getItemRevision(relatePart, revId);
                            return partRev;
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public static TCComponentItemRevision getItemRevision(TCComponentItem rddItem, String RevId)
    {
        TCComponentItemRevision itemRev = null;
        try
        {
	    	//7804-start
	    	if(RevId.contains("."))
	    	{
	    		RevId=getRevisionId(RevId);
	    	}        	
            itemRev = rddItem.getLatestItemRevision().findRevision(RevId);
			String srddItemRevision = rddItem.getLatestItemRevision()
					.getProperty("current_revision_id");
			System.out.println(itemRev);
			// 7804-added condition to suppress Revision mismatch error
			if ((itemRev == null || !itemRev.equals(rddItem
					.getLatestItemRevision())&& (!RevId.contains(".") && !srddItemRevision.contains(".")))
					) {
				MessageBox.post("Item and RDD revision mismatch. The revision "
						+ RevId + " of item " + rddItem
						+ " is not the latest revision. ", "Warning",
						MessageBox.WARNING);
				return null;
			} else {
				return itemRev;
			}
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return itemRev;
    }
    //7804-start
    public static String getRevisionId(String sRevId)
    {
    	String sFinalRevId="";
    	StringTokenizer st = new StringTokenizer(sRevId,".",true);
    	while (st.hasMoreElements()) {
    		sFinalRevId=(String) st.nextElement();
			break;
		}
    	return sFinalRevId;
    	
    }
    //7804-end
    public static TCComponentItemRevision getItemRevisionofRevID(TCComponentItem rddItem, String RevId)
    {
        TCComponentItemRevision itemRev = null;
        try
        {
            AIFComponentContext[] revItems = rddItem.getChildren("revision_list");
            for (int k = 0; k < revItems.length; k++)
            {
                String revId = ((TCComponentItemRevision) revItems[k].getComponent())
                        .getProperty("current_revision_id");
                if (revId.compareTo(RevId) == 0)
                {
                    itemRev = (TCComponentItemRevision) revItems[k].getComponent();
                    break;
                }
            }
        }
        catch (TCException e)
        {
            System.out.println(e);
            
        }
        return itemRev;
    }
    
    public static short isItemRevSecObjectCheckedout(TCComponentItemRevision itemRev)
    {
        try
        {
            TCReservationService resService = itemRev.getSession().getReservationService();
            TCComponentItem item = itemRev.getItem();
            if (resService.isReserved(item))
            {
                return NOVECOConstants.Item_CheckedOut;
            }
            
            if (resService.isReserved(itemRev))
            {
                return NOVECOConstants.ItemRev_CheckedOut;
            }
            
            AIFComponentContext[] compCxts = itemRev.getSecondary();
            for (int i = 0; i < compCxts.length; i++)
            {
            	//6210-added condition for ECO
                if (resService.isReserved((TCComponent) compCxts[i].getComponent())
                        && (!compCxts[i].getContext().equals("RelatedDocuments")) && (!compCxts[i].getContext().equals("RelatedECN")))
                {
                    return NOVECOConstants.SecObjects_CheckedOut;
                }
            }
            
            AIFComponentContext contextRDD[] = itemRev.getRelated("RelatedDefiningDocument");
            String RevId = itemRev.getProperty("current_revision_id");
            for (int i = 0; i < contextRDD.length; i++)
            {
                InterfaceAIFComponent infComp = contextRDD[i].getComponent();
                if (infComp.getType().equals("Documents"))
                {
                    TCComponentItem rddcomp = (TCComponentItem) contextRDD[i].getComponent();
                    TCComponentItemRevision rddRev = getItemRevision(rddcomp, RevId);
                    if (resService.isReserved(rddRev))
                    {
                        return NOVECOConstants.RDD_DOCRev_CheckedOut;
                    }
                    AIFComponentContext[] rddcompCxts = rddRev.getSecondary();
                    for (int j = 0; j < rddcompCxts.length; j++)
                    {
                    	//6210-added condition for ECO
                        if (resService.isReserved((TCComponent) rddcompCxts[j].getComponent()) && (!compCxts[i].getContext().equals("RelatedECN")))
                        {
                            return NOVECOConstants.RDD_DOCRev_SecObjects_CheckedOut;
                        }
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return NOVECOConstants.AllObjects_CheckedIn;
    }
    
    public static boolean isItemRevInProcess(TCComponentItemRevision tcComponentItemRevision)
    {
        try
        {
            Vector<TCComponent> inProcessIRs = new Vector<TCComponent>(Arrays.asList(tcComponentItemRevision.getItem()
                    .getInProcessItemRevisions()));
            
            if (inProcessIRs != null)
            {
                if (inProcessIRs.contains(tcComponentItemRevision))
                {
                    return true;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public static boolean getRddRevsofDocument(Vector<TCComponent> revs, TCComponentItemRevision selcomp)
    {
        // Vector<TCComponent> revs = new Vector<TCComponent>();
        try
        {
            AIFComponentContext[] rddComps = selcomp.getSecondary();// Related("RelatedDefiningDocument");
            Vector<TCComponentItem> rdditems = new Vector<TCComponentItem>();
            for (int k = 0; k < rddComps.length; k++)
            {
                if (rddComps[k].getContext().toString().equalsIgnoreCase("RelatedDefiningDocument"))
                {
                    InterfaceAIFComponent infComp = rddComps[k].getComponent();
                    if (infComp.getType().equals("Documents"))
                    {
                        TCComponentItem rddItem = (TCComponentItem) rddComps[k].getComponent();
                        if (!rdditems.contains(rddItem))
                            rdditems.add(rddItem);
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (rddComps[k].getComponent() instanceof TCComponentDataset)
                {
                    if (!revs.contains((TCComponent) rddComps[k].getComponent()))
                    {
                        revs.add((TCComponent) rddComps[k].getComponent());
                    }
                }
            }
            TCComponent[] bvrs = selcomp.getTCProperty("structure_revisions").getReferenceValueArray();
            if (bvrs != null)
            {
                for (int i = 0; i < bvrs.length; i++)
                {
                    revs.add((TCComponent) bvrs[i]);
                }
            }
            TCComponent[] altenateIDs = selcomp.getTCProperty("altid_list").getReferenceValueArray();
            if (altenateIDs != null)
            {
                for (int x = 0; x < altenateIDs.length; x++)
                {
                    revs.add((TCComponent) altenateIDs[x]);
                }
            }
            for (int k = 0; k < rdditems.size(); k++)
            {
                TCComponentItem rddItem = rdditems.get(k);
                String RevId = selcomp.getProperty("current_revision_id");
                TCComponentItemRevision rddRev = getItemRevision(rddItem, RevId);
                AIFComponentContext[] docComps = rddRev.getSecondary();
                for (int inx = 0; inx < docComps.length; inx++)
                {
                    if (docComps[inx].getComponent() instanceof TCComponentDataset)
                    {
                        if (!revs.contains((TCComponent) docComps[inx].getComponent()))
                        {
                            revs.add((TCComponent) docComps[inx].getComponent());
                        }
                    }
                }
                revs.add(rddRev);
            }
        }
        catch (TCException tc)
        {
            System.out.println(tc);
        }
        return true;
    }
    
    public static TCComponentItem getRDDItem(TCComponentItemRevision selcomp)
    {
        // Vector<TCComponent> revs = new Vector<TCComponent>();
        TCComponentItem rddItem = null;
        try
        {
            AIFComponentContext[] rddComps = selcomp.getSecondary();// Related("RelatedDefiningDocument");
            Vector<TCComponentItem> rdditems = new Vector<TCComponentItem>();
            for (int k = 0; k < rddComps.length; k++)
            {
                if (rddComps[k].getContext().toString().equalsIgnoreCase("RelatedDefiningDocument"))
                {
                    InterfaceAIFComponent infComp = rddComps[k].getComponent();
                    if (infComp.getType().equals("Documents"))
                    {
                        rddItem = (TCComponentItem) rddComps[k].getComponent();
                        return rddItem;
                    }
                    
                }
            }
        }
        catch (TCException tc)
        {
            System.out.println(tc.errorStrings);
        }
        return null;
    }
    //7762-start
    public static String checkRDValidation(TCComponentItemRevision tcComponent,Vector<TCComponent> ecoTargets)
    {
        Vector<TCComponentItem> relatedDocs =getRDItem(tcComponent);
        if(relatedDocs.size()>0 && relatedDocs!=null )
        {
            String result=isRDReleased(relatedDocs);
            if(result!=null)
            {
                return result;
            }
            else 
            {
               return  checkRDIsAttachedToECO(relatedDocs,ecoTargets) ;
                
            }  
        }
        else
        {
            return "N/A";
        }
       
    }
    public static String checkRDIsAttachedToECO(Vector<TCComponentItem> relatedDocs,Vector<TCComponent> selecomp)
    {
        for(int i=0;i<relatedDocs.size();i++)
        {
            TCComponentItem rdItem=(TCComponentItem)relatedDocs.get(i);
            TCComponentItemRevision rdRev;
            try
            {
                rdRev = rdItem.getLatestItemRevision();
                if(isItemRevInProcess(rdRev)&& ! selecomp.contains(rdRev) )
                {
                    String currentJobOFRDD=rdRev.getProperty("current_job");
                    String[] sValues=currentJobOFRDD.split(" ");
                    String rdName=rdRev.getProperty("item_id");
                    return "Failed: "+rdName + " is attached to "+ sValues[0];
                }
                
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
        }
        return "Passed";
       
    }
   
    public static String isRDReleased(Vector<TCComponentItem> relatedDocs)
    {
       for(int i=0;i<relatedDocs.size();i++)
        {
            TCComponentItem rdItem=(TCComponentItem)relatedDocs.get(i);
            try
            {
                TCComponentItemRevision rdRev=rdItem.getLatestItemRevision();
                if(!isReleasedRevision(rdRev) && !isItemRevInProcess(rdRev))
                {
                    String rdName=rdRev.getProperty("item_id");
                    return "Failed: "+rdName+" is not Released";
                    
                }
               
            }
            catch (TCException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
        
    }
    public static Vector<TCComponentItem> getRDItem(TCComponentItemRevision selcomp)
    {
        TCComponentItem rdItem = null;
        Vector<TCComponentItem> relatedDocs=new Vector<TCComponentItem>();
        try
        {
            AIFComponentContext[] rdComps = selcomp.getSecondary();
            for (int k = 0; k < rdComps.length; k++)
            {
                if (rdComps[k].getContext().toString().equalsIgnoreCase("RelatedDocuments"))
                {
                    InterfaceAIFComponent infComp = rdComps[k].getComponent();
                    if (infComp.getType().equals("Documents"))
                    {
                        rdItem = (TCComponentItem) rdComps[k].getComponent();
                        relatedDocs.add(rdItem);
                        
                    }
                    
                }
            }
            return relatedDocs;
        }
        catch (TCException tc)
        {
            System.out.println(tc.errorStrings);
        }
        return null;
        
    }
    //7762-end
    public static boolean isReleasedRevision(TCComponentItemRevision itemRevision)
    {
        try
        {
            if (itemRevision != null)
            {
                TCComponentItemRevision[] releasedRevs = itemRevision.getItem().getReleasedItemRevisions();
                
                Vector<TCComponentItemRevision> relRevVec = new Vector<TCComponentItemRevision>(
                        Arrays.asList(releasedRevs));
                if (relRevVec.contains(itemRevision))
                {
                    return true;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return false;
        
    }
    /*
     * Function to check whether  ITEM is Archived or not
     */
    public static boolean isItemArchived(TCComponentItem item)//TCDECREL-7472
    {
    	boolean isArchived = false;
    	try
        {
    		TCComponent[] relStatusList;
            if (item != null)
            {
                 relStatusList = (item.getTCProperty("release_status_list").getReferenceValueArray());
                 for (int index = 0; index < relStatusList.length; ++index)
                 {
                     if (relStatusList[index].getProperty("name").equalsIgnoreCase("Archived"))
                     {
                    	 isArchived = true;
                         break;
                     }
                 }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isArchived;
    	
    }
    
    
    /*
     * function to check whether an eco is released or not
     */
    public static boolean isEcoReleased(TCComponentForm ecoForm)
    {
        try
        {
            if (ecoForm != null)
            {
                String ecoStatuses = ecoForm.getProperty("release_statuses");
                if (ecoStatuses.length() > 0)
                    return true;
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return false;
    }
    
    // soma
    /*
     * public static String getCRCommentString(TCComponent[] forms) { String
     * comments=""; if(forms!=null) { for (int i = 0; i < forms.length; i++) {
     * TCComponentForm crForm = null; if (forms[i] instanceof TCComponentForm) {
     * crForm = (TCComponentForm)forms[i]; } else { continue; } try { if
     * (crForm.isTypeOf("_EngChangeRequest_")) { String
     * crComments=crForm.getTCProperty("comments").getStringValue();
     * if(crComments!=null) { comments ="CR Comments:" + crComments +"\n"; } } }
     * catch (TCException e) { e.printStackTrace(); } } } return comments; }
     */

    public static String getReason4ChangeString(TCComponent[] forms)
    {
        String reason4Change = "";
        if (forms != null)
        {
            for (int i = 0; i < forms.length; i++)
            {
                TCComponentForm crForm = null;
                if (forms[i] instanceof TCComponentForm)
                {
                    crForm = (TCComponentForm) forms[i];
                }
                else
                {
                    continue;
                }
                
                try
                {
                    if (crForm.isTypeOf("_EngChangeRequest_"))
                    {
                        String crName = crForm.getTCProperty("object_name").getStringValue();
                        String crReason4Chn = crForm.getTCProperty("reason_for_change").getStringValue();
                        String crDetails = crForm.getTCProperty("change_desc").getStringValue();
                        String crComments = crForm.getTCProperty("comments").getStringValue();
                        
                        if (reason4Change.length() > 0)
                        {
                            reason4Change = reason4Change + "CR:" + crName + "\n";
                        }
                        else
                        {
                            reason4Change = "CR:" + crName + "\n";
                        }
                        if (crReason4Chn != null)
                        {
                            reason4Change = reason4Change + "Reason for change:" + crReason4Chn + "\n";
                        }
                        if (crDetails != null)
                        {
                            reason4Change = reason4Change + "CR Detail:" + crDetails + "\n";
                        }
                        if (crDetails != null)
                        {
                            reason4Change = reason4Change + "CR Comments:" + crComments + "\n";
                        }
                        
                        reason4Change = reason4Change + "---------------------------------------" + "\n";
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        return reason4Change;
    }
    
    public static boolean isValidECR(String typeOfChange, InterfaceAIFComponent component)
    {
        if (component.getType().equalsIgnoreCase("_EngChangeRequest_"))
        {
            try
            {
                String ecrType = component.getProperty("change_type");
                if (typeOfChange.equalsIgnoreCase("Lifecycle"))
                {
                    if (!ecrType.equalsIgnoreCase(typeOfChange))
                    {
                        MessageBox.post(
                                "The ECO type of change is " + typeOfChange + ", the ECR "
                                        + component.getProperty("object_name") + " is of type "
                                        + component.getProperty("change_type"), "Cannot add an Item",
                                MessageBox.WARNING);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return true;
    }
    
    public static String[] getLovList(TCComponentListOfValues lovComp, String sFilter)
    {
        Vector<String> sList = new Vector<String>();
        
        if (lovComp == null)
            return new String[0];
        
        if (sFilter == null)
            sFilter = "";
        
        try
        {
            ListOfValuesInfo lovInfo = lovComp.getListOfValues();
            String[] lovS = lovInfo.getStringListOfValues();
            String[] lovDesc = lovInfo.getDescriptions();
            for (int i = 0; i < lovS.length; i++)
            {
                if ((sFilter.isEmpty()) || (lovDesc == null) || (lovDesc.length == 0) || (lovDesc[i].isEmpty())
                        || (lovDesc[i].contains(sFilter)))
                    sList.add(lovS[i]);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return sList.toArray(new String[sList.size()]);
    }
    
    public static boolean isDHGroup()
    {
        TCSession session = ((TCSession) AIFUtility.getDefaultSession());
        String currentUsrGrp = session.getCurrentGroup().toString();
        String[] grpStrArray = session.getPreferenceService().getStringArray(TCPreferenceService.TC_preference_site,
                "_NOV_DH_item_creation_groups_");
        
        for (int i = 0; i < grpStrArray.length; i++)
        {
            if (currentUsrGrp.equalsIgnoreCase(grpStrArray[i]))
            {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isAllowedRole(String prefName) throws TCException
    {
        TCSession session = ((TCSession) AIFUtility.getDefaultSession());
        TCComponentGroup currentUsrGrp = session.getCurrentGroup();
        // String currentUserRole =
        // session.getRole().getStringProperty("role_name");
        String[] roleStrArray = session.getPreferenceService().getStringArray(TCPreferenceService.TC_preference_site,
                prefName);
        
        TCComponentRole[] userRoles = session.getUser().getRoles(currentUsrGrp);
        for (int i = 0; i < userRoles.length; i++)
        {
            for (int j = 0; j < roleStrArray.length; j++)
            {
                if (userRoles[i].toString().equalsIgnoreCase(roleStrArray[j]))
                {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public static String getRefECO(TCComponent comp)
    {
        String retStr = "";
        if (comp != null)
        {
            try
            {
                /*
                 * Rakesh - 26.06.2012 - To improve performance look for EPMTask
                 * only - Starts here
                 */
                /*
                 * AIFComponentContext[] contexts = comp.whereReferenced(); for
                 * (int i = 0; i < contexts.length; i++) {
                 * if((TCComponent)contexts[i].getComponent() instanceof
                 * TCComponentProcess) { TCComponentProcess currentProcess =
                 * (TCComponentProcess)contexts[i].getComponent();
                 * TCComponentTask
                 * rootTask=((TCComponentProcess)currentProcess).getRootTask();
                 * TCComponent[] wftargets =
                 * rootTask.getAttachments(TCAttachmentScope.LOCAL,
                 * TCAttachmentType.TARGET); for(int j=0;j<wftargets.length;j++)
                 * { if(wftargets[j] instanceof TCComponentForm) { retStr =
                 * wftargets[j].getProperty("object_name"); break; } } } }
                 */
                AIFComponentContext[] contexts = comp.whereReferencedByTypeRelation(new String[] { "EPMTask" }, null);
                if (contexts.length == 1)
                {
                    TCComponentTask rootTask = (TCComponentTask) contexts[0].getComponent();
                    TCComponent[] wftargets = rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
                    for (int j = 0; j < wftargets.length; j++)
                    {
                        if (wftargets[j] instanceof TCComponentForm)
                        {
                            retStr = wftargets[j].getProperty("object_name");
                            break;
                        }
                    }
                }
                /*
                 * Rakesh - 26.06.2012 - To improve performance look for EPMTask
                 * only - Ends here
                 */
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return retStr;
    }
    
    // TCDECREL-3079 : Start : 20-08-2012 : Akhilesh
    /*********************** SOA service call implementation *************************/
    public static boolean _addComponentToTarget(String context,TCComponentForm ecoForm, TCComponent[] selcomp,TCComponent[] selECRs)
    {
        
        NOV4ChangeManagement.ECOAddTargetsResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.ECOAddTargetInput input = new NOV4ChangeManagement.ECOAddTargetInput();
        
        input.context = context;
        input.ecoForm = ecoForm;
        input.targetItems = selcomp;
        input.ecrForms = selECRs;
        
        theResponse = service.addTargetsToECO(input);
        
        if (theResponse.serviceData.sizeOfPartialErrors() > 0) // Check for
                                                               // partial errors
        {
            String errorMsg = "Following item/item revision cannot be added to Target";
            for (int intx = 0; intx < theResponse.serviceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.serviceData.getPartialError(intx);
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
            MessageBox.post(errorMsg, "Cannot add Item to target", MessageBox.WARNING);
            // JOptionPane.showMessageDialog(this, errorMsg,
            // "Cannot add Item to target", JOptionPane.WARNING_MESSAGE);
            
            return false;
        }
        
        return true;
    }
    
    /**********************************************************************************/
    // TCDECREL-3079 : End
    
    // TCDECREL-3196 : Start
    /**
     * Add revision to target folder, after validating its release status.
     * 
     * @param selcomp
     * @param changeType
     * @return
     */
    public static boolean addValidRevisionToTarget(TCComponentItemRevision selcomp, TCComponentForm targetForm)
    {
        boolean isValidAdded = false;
        try
        {
            String changeType = targetForm.getTCProperty("changetype").getStringValue();
            TCComponent[] releaseStatus = selcomp.getTCProperty("release_status_list").getReferenceValueArray();
            if (changeType.equals("General"))
            {
                if (releaseStatus.length > 0)
                {
                    TCComponentItemRevision newRevision = getNewRevision(selcomp);
                    if (newRevision != null)
                    {
                        TCComponent dipsComp = null;
                        if (!newRevision.getType().equalsIgnoreCase("Documents Revision"))
                        {
                            dipsComp = createDispositionComp(selcomp, targetForm);
                            TCComponent[] dispComps = new TCComponent[1];
                            dispComps[0] = dipsComp;
                            dipsComp.setStringProperty("oldrev", selcomp.getProperty("current_revision_id"));
                            String relStatus = newRevision.getItem().getProperty("release_statuses");
                            dipsComp.setStringProperty("oldlifecycle", relStatus);
                        }
                        
                        isValidAdded = addToTargetList(newRevision, targetForm);
                        
                    }
                    else
                    {
                        isValidAdded = false;
                    }
                }
                else
                {
                    TCComponent[] revisioncnt = selcomp.getItem().getTCProperty("revision_list")
                            .getReferenceValueArray();
                    if ((revisioncnt.length > 1) && (!selcomp.getType().equalsIgnoreCase("Documents Revision")))
                    {
                        isValidAdded = addToTargetList(selcomp, targetForm);
                        
                        if (isValidAdded)
                        {
                            TCComponent dipsComp = createDispositionComp(selcomp, targetForm);
                            TCComponent[] dispComps = new TCComponent[1];
                            dispComps[0] = dipsComp;
                            String relStatus = selcomp.getItem().getProperty("release_statuses");
                            dipsComp.setStringProperty("oldlifecycle", relStatus);
                        }
                    }
                    else
                    {
                        isValidAdded = addToTargetList(selcomp, targetForm);
                    }
                }
            }
            else if (changeType.equals("Lifecycle"))
            {
                if (releaseStatus.length > 0)
                {
                    TCComponentItemRevision[] releasedRevs = selcomp.getItem().getReleasedItemRevisions();
                    if (selcomp.equals(releasedRevs[0]))
                    {
                        isValidAdded = addToTargetList(selcomp, targetForm);
                    }
                    else
                    {
                        MessageBox.post("Select Latest Released Revision.", "Info", MessageBox.INFORMATION);
                    }
                }
                else
                {
                    MessageBox.post("Select only Latest Released Revision for LifeCycle Change.", "Info",
                            MessageBox.INFORMATION);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isValidAdded;
    }
    
    /**
     * Add item revision to current process of Target ECO
     * 
     * @param selcomp
     * @return
     */
    public static boolean addToTargetList(TCComponentItemRevision selcomp, TCComponentForm targetECO)
    {
        Vector<TCComponent> revs = new Vector<TCComponent>();
        boolean isReleatedObjsAdded = NOVECOHelper.getRddRevsofDocument(revs, selcomp);
        if (isReleatedObjsAdded)
        {
            revs.add(selcomp);
            
            TCComponentProcess currentProcess = getCurrentProcess(targetECO);
            if (currentProcess != null)
            {
                try
                {
                    Vector<TCComponent> targetCompTobeAdded = new Vector<TCComponent>();
                    // TCComponent[] temptargetComp = revs.toArray(new
                    // TCComponent[revs.size()]);
                    // If same dataset is referenced in two revisions we need to
                    // filter for maintaining one object, so skip adding
                    // duplicate
                    TCComponent[] targets = currentProcess.getRootTask().getAttachments(TCAttachmentScope.LOCAL,
                            TCAttachmentType.TARGET);
                    Vector<TCComponent> ecoTargetsVec = new Vector<TCComponent>(Arrays.asList(targets));
                    Vector<Integer> attTypes = new Vector<Integer>();

                    for (int i = 0; i < revs.size(); i++)
                    {
                        if (!ecoTargetsVec.contains(revs.elementAt(i)))
                        {
                            targetCompTobeAdded.add(revs.elementAt(i));
							attTypes.add(TCAttachmentType.TARGET);
                        }
                    }
                    
					int iNoOfAttTypes = attTypes.size();
					int attachtypes[]=new int[iNoOfAttTypes];
					for(int i = 0;i<iNoOfAttTypes;i++)
					{
						attachtypes[i] = attTypes.get(i).intValue();
					}

                    TCComponent[] targetComp = targetCompTobeAdded.toArray(new TCComponent[targetCompTobeAdded.size()]);
                    currentProcess.getRootTask().refresh();
                    currentProcess.getRootTask().addAttachments(TCAttachmentScope.GLOBAL, targetComp, attachtypes);
                    AIFComponentContext[] cxtObjs = currentProcess.getRootTask().getChildren();
                    for (int i = 0; i < cxtObjs.length; i++)
                    {
                        if (cxtObjs[i].getContext().equals("pseudo_folder"))
                        {
                            ((TCComponent) cxtObjs[i].getComponent()).refresh();
                        }
                    }
                    currentProcess.getRootTask().refresh();
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            MessageBox.post("RelatedDefiningDocument relation contains other than Documents.", "Cannot add item",
                    MessageBox.WARNING);
        }
        
        return isReleatedObjsAdded;
    }
    
    /**
     * get the current process of Target ECO.
     * 
     * @return
     */
    public static TCComponentProcess getCurrentProcess(TCComponentForm targetECO)
    {
        TCComponent component = null;
        AIFComponentContext[] contexts;
        TCComponentProcess currentProcess = null;
        try
        {
            contexts = targetECO.whereReferenced();
            
            if (contexts != null)
            {
                for (int j = 0; j < contexts.length; j++)
                {
                    component = (TCComponent) contexts[j].getComponent();
                    if (component instanceof TCComponentProcess)
                    {
                        currentProcess = (TCComponentProcess) component;
                        break;
                    }
                }
            }
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return currentProcess;
    }
    
    /**
     * Get new revision of selected item revision
     * 
     * @param selcomp
     * @return
     */
    private static TCComponentItemRevision getNewRevision(TCComponentItemRevision selcomp)
    {
        TCComponentItemRevision revcomps = null;
        try
        {
           /* TCUserService userService = ((TCSession) AIFUtility.getDefaultSession()).getUserService();
            Object[] args = new Object[1];
            args[0] = new TCComponent[] { selcomp };
            Object comp = userService.call("NATOIL_deepcopyRev", args);
            if (comp != null)
            {
                TCComponent[] comps = (TCComponent[])comp;
                revcomps = (TCComponentItemRevision)comps[0];
            }
            /*
             * TCComponentItemRevision newRev =
             * selcomp.getItem().revise(selcomp.getItem().getNewRev(),
             * selcomp.getProperty("object_name"), ""); return newRev;
             */

			IPropertyMap reviseProperMap = new SimplePropertyMap();

			TCComponent revMasterForm = selcomp
					.getRelatedComponent("IMAN_master_form_rev");

			reviseProperMap.setComponent(selcomp);

			CreateInCompoundHelper revCompoundHelper = new CreateInCompoundHelper(
					reviseProperMap);
			revCompoundHelper.addCompound("ItemRevision",
					"IMAN_master_form_rev");

			IPropertyMap revMasterFormPropMap = reviseProperMap
					.getCompound("IMAN_master_form_rev");

			PropertyMapHelper.componentToMap(selcomp, new String[] { "item_id",
					"object_name", "object_desc" }, reviseProperMap);

			TCProperty[] tcPropertyObject = revMasterForm.getAllTCProperties();
			PropertyMapHelper.addPropertiesToMap(tcPropertyObject,
					revMasterFormPropMap);

			reviseProperMap.setCompound("IMAN_master_form_rev",
					revMasterFormPropMap);
			ReviseObjectsSOAHelper reviseHelper = new ReviseObjectsSOAHelper();
			reviseHelper.reviseObjects(new IPropertyMap[] { reviseProperMap });        	
        	
        }
        catch (TCException e)
        {
            System.out.println("New revision can not be created");
            MessageBox.post("Item Revision " + selcomp.toString()
                    + " cannot be revised. Please check the access. Save ECO form to refresh.", "Error",
                    MessageBox.ERROR);
            // e.printStackTrace();
        }
        return revcomps;
    }
    
    /**
     * Create disposition data.
     * 
     * @param selcomp
     * @return
     */
    public static TCComponent createDispositionComp(TCComponentItemRevision selcomp, TCComponentForm targetECO)
    {
        TCComponent comps = null;
        try
        {
           /* TCUserService userService = targetECO.getSession().getUserService();
            Object[] args = new Object[1];
            args[0] = new String[] { selcomp.getProperty("item_id") };
            Object comp = userService.call("NATOIL_createEcoDispositionData", args);
            if (comp != null)
            {
                TCComponent[] tcComp = (TCComponent[])comp;
				comps = tcComp[0];
            }
            */
        	
			CreateInObjectHelper createInObjectHelper = new CreateInObjectHelper(targetECO.getType(), CreateInObjectHelper.OPERATION_CREATE);
			
			createInObjectHelper.setString("targetitemid", selcomp.getProperty("item_id"));
			
			//set empty properties
			createInObjectHelper.setString("changedesc","");
			createInObjectHelper.setString("dispinstruction","");
			createInObjectHelper.setString("inprocess","");
			createInObjectHelper.setString("ininventory","");
			createInObjectHelper.setString("assembled","");
			createInObjectHelper.setString("infield","");
			createInObjectHelper.setString("nov4_ecodisposition","");
			createInObjectHelper.setString("oldrev","");
			createInObjectHelper.setString("oldlifecycle","");
			
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
			
			TCComponent[] createdObejcts = CreateObjectsSOAHelper.getCreatedOrUpdatedObjects(createInObjectHelper);
        
            if(createdObejcts != null && createdObejcts.length >0)
            {
               TCComponent partRevision = createdObejcts[0];
               if(partRevision instanceof TCComponentItemRevision)
               {
            	   comps = (TCComponentItemRevision) partRevision;
               }
            }			
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return comps;
    }
    
    // TCDECREL-3196 : End
    
    /**
     * Method validates whether current user belongs to Group 33 or not.
     * 
     * @param preference
     * @return true if current uuser group is Group 33 else rerune false
     * @author kabades
     */
    public static boolean isNameName2EditableGrp(String pref) // TCDECREL - 4089
    {
        
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        TCPreferenceService prefServ = session.getPreferenceService();
        String[] groupArr = prefServ.getStringArray(TCPreferenceService.TC_preference_site, pref);
        String currentUsrGrp = session.getCurrentGroup().toString().substring(0, 2);
        try
        {
            for (int i = 0; i < groupArr.length; i++)
            {
                if (currentUsrGrp.equals(groupArr[i]))
                {
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            
            e.printStackTrace();
        }
        return false;
    }
    
    public static TCComponentItemRevision findRDDforTargetItem(TCComponent itemRev)
    {
        TCComponentItemRevision rddDocRev = null;
        AIFComponentContext[] comps;
        try
        {
            comps = itemRev.getSecondary();
            
            for (int j = 0; j < comps.length; j++)
            {
                if (comps[j].getContext().equals("RelatedDefiningDocument"))
                {
                    InterfaceAIFComponent infDocItem = comps[j].getComponent();
                    if (infDocItem instanceof TCComponentItem && infDocItem.getType().equalsIgnoreCase("Documents"))
                    {
                        rddDocRev = ((TCComponentItem) infDocItem).getLatestItemRevision();
                        break;
                    }
                }
            }
            
        }
        catch (TCException e)
        {
            
            e.printStackTrace();
        }
        return rddDocRev;
    }
    
    // TCDECREL-3369
    public static AIFComponentContext[] getRDDItems(TCComponent itemRev) throws TCException
    {
        TCComponentItem targetItem = ((TCComponentItemRevision) itemRev).getItem();
        
        String[] types = { "Nov4Part Revision", "Purchased Part Revision", "Non-Engineering Revision" };
        String[] relations = { "RelatedDefiningDocument" };
        
        AIFComponentContext[] contextComp = targetItem.whereReferencedByTypeRelation(types, relations);
        
        return contextComp;
        
    }
    //TCDECREL-7804: Function to get all part family members by passing any part member
    public static Vector<TCComponent> getAllPartFamilyMembers(TCComponent item) throws TCException
    {
        Vector<TCComponent> partFamily = new Vector<TCComponent>();
        TCComponentItemRevision rddRev = null;
        if (!item.getProperty("object_type").contains("Documents"))
        {
            rddRev = findRDDforTargetItem(item);
        }
        else
        {
            rddRev = (TCComponentItemRevision) item;
        }
        if (rddRev != null)
        {
            AIFComponentContext[] comps = getRDDItems(rddRev);
            for (int i = 0; i < comps.length; i++)
            {
                partFamily.add((TCComponent) comps[i].getComponent());
            }
            partFamily.add(rddRev);
        }
        
        return partFamily;
    }
    
    //8334: Function to validate item rev for Minor rev(Item should have latest released rev)
    public static boolean isValidCompForMinorRev(TCComponentItemRevision itemRev, String sRevType)
    {
        boolean isValid = true;
        if(sRevType.equalsIgnoreCase("Create Minor Revision") && !isReleasedRevision(itemRev)
                            && !isItemRevInProcess(itemRev))
        {
            isValid = false;
        }
        return isValid;
    }
    
	public static boolean isECRValid(String typeOfChange,InterfaceAIFComponent component)
	{
		if (component.getType().equalsIgnoreCase("_EngChangeRequest_"))
		{
			try
			{
				String ecrType = component.getProperty("change_type");
				if (typeOfChange.equalsIgnoreCase("Lifecycle"))
				{
					if (!ecrType.equalsIgnoreCase(typeOfChange))
					{
						return false;
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return true;
    }
	public static void centerToScreen(Shell shell)
	{
		Rectangle bounds = getPrimaryMonitorBounds(shell);
	    
	    Rectangle shellBounds = shell.getBounds();   
	    	
		int x = bounds.x + (bounds.width - shellBounds.width) / 2;
	    int y = bounds.y + (bounds.height - shellBounds.height) / 2;
	    
	    shell.setLocation(x, y);
	}
	private static Rectangle getPrimaryMonitorBounds(Shell shell)
	{
		Monitor primary = shell.getDisplay().getPrimaryMonitor();
	    Rectangle bounds = primary.getBounds();
		
		return bounds;
	}
	//7804-start
	public static String addTargetsandCreateRevision(String context,TCComponentForm ecoForm, TCComponent[] selcomp,TCComponent[] selECRs, String sRevType)
    {
		String errorMsg = "";
        NOV4ChangeManagement.ECOAddTargetsResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.ECOAddTargetInput input = new NOV4ChangeManagement.ECOAddTargetInput();
        
        input.context = context;
        input.ecoForm = ecoForm;
        input.targetItems = selcomp;
        input.ecrForms = selECRs;
        if( sRevType !=null && sRevType.length() > 0)//7804
        {
        	input.revisionType =  sRevType;
        	input.itemsWithSuggestedRevIds= getNextRevIdsMap(selcomp,sRevType);
        }
                
        theResponse = service.addTargetsToECO(input);
        
        if (theResponse.serviceData.sizeOfPartialErrors() > 0) // Check for
                                                               // partial errors
        {
            errorMsg = "Following item/item revision cannot be added to Target";
            for (int intx = 0; intx < theResponse.serviceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.serviceData.getPartialError(intx);
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
        }        
        return errorMsg;
    }
	public static Map<String , String> getNextRevIdsMap(TCComponent[] selcomp,String sRevType)
	{
		Map<String,String> itemsIdsWithNextMajorRevIds= new HashMap<String, String>();
		NOVCreateECOHelper helperObj = new NOVCreateECOHelper();
		for(int i=0;i<selcomp.length;i++)
		{
			try {
				String sCurrentRevId;
				TCComponentItemRevision itemRev = null;
				String sItemId=selcomp[i].getProperty("item_id");
				itemRev=getItemRevision(selcomp[i]);
				sCurrentRevId=itemRev.getProperty("current_revision_id");
				//Klock138-moved this code to a new function called getItemRevision()
				/*if(selcomp[i] instanceof TCComponentItemRevision)
				{
				    itemRev = (TCComponentItemRevision)selcomp[i];
					sCurrentRevId=selcomp[i].getProperty("current_revision_id");
				}
				else
				{
					TCComponentItem item=(TCComponentItem)selcomp[i];
					itemRev=item.getLatestItemRevision();
					sCurrentRevId=itemRev.getProperty("current_revision_id");
				}*/
				
				if(sRevType.equalsIgnoreCase("Create Major Revision"))
				{
				    Vector<TCComponent> partFamilyComps = getAllPartFamilyMembers(itemRev);
				    if(!partFamilyComps.contains(itemRev))
				        partFamilyComps.add(itemRev);
				    String suggestedRevId=helperObj.getNextMajorRevisionId(partFamilyComps);
	    			for(int j = 0; j<partFamilyComps.size(); j++)
	    			{
	    			    String itemId = partFamilyComps.get(j).getProperty("item_id").toString();
	    			    itemsIdsWithNextMajorRevIds.put(itemId, suggestedRevId);
	    			}
				}
				else if(sRevType.equalsIgnoreCase("Create Minor Revision"))
				{
					String suggestedRevId=helperObj.getNextMinorRevisionId(sCurrentRevId);
	    			itemsIdsWithNextMajorRevIds.put(sItemId, suggestedRevId);
				}
			} catch (TCException e) {
				e.printStackTrace();
			}
			
		}
		return itemsIdsWithNextMajorRevIds;
	}
	//Klock138-start
	private static  TCComponentItemRevision getItemRevision(TCComponent selectedComponent)
	{
	    TCComponentItemRevision itemRev=null;
	    if(selectedComponent instanceof TCComponentItemRevision)
        {
            itemRev = (TCComponentItemRevision)selectedComponent;
            
        }
        else
        {
            TCComponentItem item=(TCComponentItem)selectedComponent;
            try
            {
                itemRev=item.getLatestItemRevision();
            }
            catch (TCException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
	    return itemRev;
	}
	//Klock138-end
	//7804-end
	public static String addTargetsToECO(String context,TCComponentForm ecoForm, TCComponent[] selcomp,TCComponent[] selECRs)
    {
		String errorMsg = "";
        NOV4ChangeManagement.ECOAddTargetsResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.ECOAddTargetInput input = new NOV4ChangeManagement.ECOAddTargetInput();
        
        input.context = context;
        input.ecoForm = ecoForm;
        input.targetItems = selcomp;
        input.ecrForms = selECRs;
        
        theResponse = service.addTargetsToECO(input);
        
        if (theResponse.serviceData.sizeOfPartialErrors() > 0) // Check for
                                                               // partial errors
        {
            errorMsg = "Following item/item revision cannot be added to Target";
            for (int intx = 0; intx < theResponse.serviceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.serviceData.getPartialError(intx);
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
        }        
        return errorMsg;
    }
	public static String addTargetItemRevsToECO(String context,TCComponentForm ecoForm, TCComponent[] selcomp,String mmrType)
    {
		String errorMsg = "";
        NOV4ChangeManagement.ECOAddTargetsResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.ECOAddTargetItems input = new NOV4ChangeManagement.ECOAddTargetItems();
        
        input.context = context;
        input.ecoForm = ecoForm;
        input.targetItems = selcomp;
        if( mmrType !=null && mmrType.length() > 0)//7805
        {
            input.revisionType =  mmrType;
            input.itemsWithSuggestedRevIds= getNextRevIdsMap(selcomp,mmrType);
        }
        
        theResponse = service.addTargetItemsToECO(input);
        
        if (theResponse.serviceData.sizeOfPartialErrors() > 0) // Check for
                                                               // partial errors
        {
        	errorMsg = "Following item/item revision cannot be added to Target";
            for (int intx = 0; intx < theResponse.serviceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.serviceData.getPartialError(intx);
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
        }
        
        return errorMsg;
    }
	public static void sortTableRowsInAscendingOrder(TCTable table, String sColumnName)
	{
		int iColIndex = table.getColumnIndex(sColumnName);	
		AIFIdentifier identifier = new AIFIdentifier(sColumnName, table.dataModel.getColumnOtherId(iColIndex));
		table.dataModel.sortByColumns((new AIFIdentifier[]{  identifier}));
	}

}



