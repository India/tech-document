package com.nov.rac.dhl.eco.validations.additem;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.TableColumn;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVECRTableMouseListener extends MouseAdapter
{
    private NOVECRTable m_ecrSelectionTable;
    private Shell m_shell = null;
    private Registry m_reg = null;
    public NOVECRTableMouseListener()
    {
        m_reg = Registry.getRegistry(this);
    }
    public void mousePressed(MouseEvent e) 
    {
    }
    public void mouseClicked(MouseEvent mouseEvent)
    {
        m_ecrSelectionTable = (NOVECRTable) mouseEvent.getComponent();
        setHeaderCheckbox();
        targetItemSelectionCheck();
    }
    private void targetItemSelectionCheck()
    {
        int iRowCount = m_ecrSelectionTable.getRowCount();
        int iSelectedRow = m_ecrSelectionTable.getSelectedRow();
        boolean selectedColValue = (Boolean) m_ecrSelectionTable.getValueAt(iSelectedRow, 0);
        
        final Display display = Display.getDefault();
        display.syncExec(new Runnable() 
        {
            public void run() 
            {   
                m_shell = display.getActiveShell();
            }
        });
        NOVECRTableLine ecrTableLine = null;
        NOVECRTableLine selectedTableLine = null;
        AIFTableLine tableLine = m_ecrSelectionTable.getRowLine(iSelectedRow);
        selectedTableLine = (NOVECRTableLine)tableLine;
        if(selectedColValue == true)
        {
            for(int i=0; i<iRowCount; i++)
            {
                boolean colValue = (Boolean) m_ecrSelectionTable.getValueAt(i, 0);
                AIFTableLine tabLine = m_ecrSelectionTable.getRowLine(i);
                ecrTableLine = (NOVECRTableLine)tabLine;
                if((i != iSelectedRow && colValue == true) && (selectedTableLine.getTableLineRevObject() == ecrTableLine.getTableLineRevObject()))
                {
                    m_ecrSelectionTable.setValueAt(false,iSelectedRow, 0);
                    m_ecrSelectionTable.repaint();
                    
                    MessageBox.post(m_shell,m_reg.getString("DuplicateECR.MSG"),"Warning..." ,MessageBox.WARNING);
                }
            } 
        }
    }
    private void setHeaderCheckbox() 
    {       
        boolean bSelectHeaderChkBox = true;
        
        int iRowCount = m_ecrSelectionTable.getRowCount();
        for(int i=0; i<iRowCount; i++)
        {
            boolean colValue = (Boolean) m_ecrSelectionTable.getValueAt(i, 0);
            if(!colValue)
            {
                bSelectHeaderChkBox = false;
                break;
            }
        }       
        setHeaderCheckbox(bSelectHeaderChkBox);
    }

    private void setHeaderCheckbox(boolean bSelectHeaderChkBox) 
    {
        TableColumn tabCol = m_ecrSelectionTable.getColumnModel().getColumn(0);
        ((NOVECRTableHeaderRenderer)tabCol.getHeaderRenderer()).setValue(bSelectHeaderChkBox);
        m_ecrSelectionTable.getTableHeader().repaint();
    }
}
