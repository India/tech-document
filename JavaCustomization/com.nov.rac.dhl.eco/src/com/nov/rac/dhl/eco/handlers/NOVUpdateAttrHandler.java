 /* ============================================================
   
   File description: 

   Filename: NOVUpdateAttrHandler.java 
   Module  : \NOV_ECO\DHL_ECO\com\nov\rac\dhl\eco\handlers 

   NOVUpdateAttrHandler is invoked when user selects 'UpdateAttributes' 
   option from custom 'NOVTools' menu 

   Date         Developers    Description
   24-08-2010   Usha    	  Initial Creation

   $HISTORY
   ============================================================ */


package com.nov.rac.dhl.eco.handlers;

import java.util.Vector;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.dhl.eco.attrUpdate.NOVUpdateAttrDialog;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVUpdateAttrHandler extends AbstractHandler 
{
	String itemORitemrev="";
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		Registry 			registry 	= Registry.getRegistry(this);
		InterfaceAIFComponent[] comps 	= AIFUtility.getTargetComponents();

		//validate targets' count - ensure only one target
		if(comps.length == 0)
		{
			//err message
			MessageBox.post("No objects Selected " +
					"\nSelect Nov4Part/Non-Engineering/Documents type item/revision to update attributes.",
					"Error", MessageBox.ERROR);
			return null;
		}
		if (comps.length>1) 
		{
			//err message
			MessageBox.post("More than one object selected " +
					"\nSelect only one Nov4Part/Non-Engineering/Documents item/revision to update attributes.",
					"Error", MessageBox.ERROR);
			return null;
		}
		//validate obj type - ensure target is:  Nov4Part/Non-Engineering/Document type item/revision
		String objType = comps[0].getType();
		if(objType.startsWith(registry.getString("NovItem"))||
				objType.startsWith(registry.getString("DocItem"))||
						objType.startsWith(registry.getString("NonEngItem")) )
		{
			TCComponentItemRevision latestRev	= null;
			TCComponentItem			item	 	= null;
			TCComponentForm 		imf			= null;
			TCComponentForm 		irmf		= null;
			try 
			{
				if(comps[0] instanceof TCComponentItemRevision)
				{
					itemORitemrev = "itemrev";
					item = ((TCComponentItemRevision)comps[0]).getItem(); 
				}
				else if (comps[0] instanceof TCComponentItem)
				{
					itemORitemrev = "item";
					item = (TCComponentItem)comps[0];
				}
				else
				{
					MessageBox.post("Invalid object selected " +
							"\nSelect only Nov4Part/Non-Engineering/Documents type item/revision to update attributes.",
							"Error", MessageBox.ERROR);
					return null;
				}	
				
				latestRev 	= item.getLatestItemRevision();
				if( (comps[0] instanceof TCComponentItemRevision) && (comps[0] != latestRev) )
				{
					MessageBox.post("Invalid object selected " +
							"\nSelect only latest revision to update attributes.",
							"Error", MessageBox.ERROR);
					return null;			
				}
				
				imf 		= (TCComponentForm)(item.getRelatedComponent(registry.getString("MasterFormRelation")));
				irmf 		= (TCComponentForm)(latestRev.getRelatedComponent
											(registry.getString("RevMasterFromRelation")));
				
				String modifprop = registry.getString("ModifiableProp");
			
				if(itemORitemrev == "item")
				{
					if( (item.getProperty(modifprop).length() == 0 ) 
							|| (imf.getProperty(modifprop).length() == 0 ) )
					{
						MessageBox.post("Access Denied " +
								"\nItem/Revision/Attachemnts are not modifiable.",
								"Error", MessageBox.ERROR);
						return null;
					}
				} else if (itemORitemrev == "itemrev")
				{
					if( (latestRev.getProperty(modifprop).length() == 0 ) 
						|| (irmf.getProperty(modifprop).length() == 0 )	)
					{
						MessageBox.post("Access Denied " +
								"\nItem/Revision/Attachemnts are not modifiable.",
								"Error", MessageBox.ERROR);
						return null;
					}
					
				}
				
//				if( (item.getProperty(modifprop).length() == 0 ) 
//						|| (latestRev.getProperty(modifprop).length() == 0 ) 
//						|| (imf.getProperty(modifprop).length() == 0 ) 
//						|| (irmf.getProperty(modifprop).length() == 0 ) 
//						)
//				{
//					MessageBox.post("Access Denied " +
//							"\nItem/Revision/Attachemnts are not modifiable.",
//							"Error", MessageBox.ERROR);
//					return null;
//				}
				
				//String propNames = registry.getString("object_name,object_desc,Name2,Sites");
				String ipForPropFile = objType+".ATTR";
				String propNames = registry.getString(ipForPropFile);
				String[] props = propNames.split(",");
                /*hide attributes  for new Group in update attributes dialog*/
                TCSession session = (TCSession)AIFUtility.getDefaultSession();
                TCPreferenceService prefService = session.getPreferenceService();
                String owningGrp = imf.getProperty("owning_group");;
                String hiddenAttribPrefName = objType+".HIDDEN_ATTRIBUTES"+"."+ owningGrp.substring(0, 2);
                String hiddenAttribList = prefService.getString(TCPreferenceService.TC_preference_site,hiddenAttribPrefName);               
                

				Vector<TCProperty> tcpross = new Vector<TCProperty>();
				for(int i= 0; i<props.length; i++)
				{
					 if(hiddenAttribList.contains(props[i]))
                         continue;
				
					TCProperty property = null;	
					//MessageBox.post(props[i], "", MessageBox.INFORMATION);
					if(props[i].equals("PullDrawing") || props[i].equals("secure"))
						property = imf.getTCProperty(props[i]);
					property = imf.getTCProperty(props[i]);
					//5025-start
					if(props[i].equals(registry.getString("COMMENTS_ATTR_ENG.NAME")) || props[i].equals(registry.getString("COMMENTS_ATTR_NONENG.NAME")) )	
					{
						property=latestRev.getTCProperty(props[i]);
					}
					//5025-end
						
					else 
					{
					    property = latestRev.getTCProperty(props[i]);
						if( property == null || (property.getPropertyDescriptor().getType2() == TCProperty.PROP_compound))		
						{
							//MessageBox.post(props[i]+"Not IR prop", "", MessageBox.INFORMATION);
							if( (irmf.getTCProperty(props[i])) == null)
							{
								//MessageBox.post(props[i]+"Not IRMF prop", "", MessageBox.INFORMATION);
								if( (imf.getTCProperty(props[i])) == null)
								{
									//MessageBox.post(props[i]+"Not IMF prop", "", MessageBox.INFORMATION);
									continue;
									
								}
								else
								{
									//MessageBox.post(props[i]+"IMF prop", "", MessageBox.INFORMATION);
									property = imf.getTCProperty(props[i]);
								}
							}
							else
								property = irmf.getTCProperty(props[i]);
						}
						else
							property = latestRev.getTCProperty(props[i]);
					}
					if(property != null)
						tcpross.add(property);
				}//for
				IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
				NOVUpdateAttrDialog dialog = new NOVUpdateAttrDialog(window.getShell(), (TCComponent)comps[0], tcpross,latestRev, item, imf, irmf);
				dialog.open();	
			} 
			catch (TCException e) 
			{
				MessageBox.post(e.getMessage() , "Error",MessageBox.ERROR);
				e.printStackTrace();
			}
		}//end if valid item/rev 
		else//if target is not Nov4Part/Non-Engineering/Document type item/revision
		{
			MessageBox.post("Invalid object selected " +
					"\nSelect only Nov4Part/Non-Engineering/Documents type item/revision to update attributes.",
					"Error", MessageBox.ERROR);
			return null;
		}	
		return null;
	}//execute
}//handler class
