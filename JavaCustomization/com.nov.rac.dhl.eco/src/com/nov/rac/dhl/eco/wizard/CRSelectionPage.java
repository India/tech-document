package com.nov.rac.dhl.eco.wizard;

import java.awt.Window;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;

import com.noi.rac.dhl.eco.util.components.NOVCRSelectionPanel;
import com.teamcenter.rac.util.SWTUIUtilities;

public class CRSelectionPage extends WizardPage
{
    public NOVCRSelectionPanel crSelectpanel;
    public static final String PAGE_NAME = "CreChangeObjPage";
    
    public CRSelectionPage()
    {
        super(PAGE_NAME);
        setTitle("Engineering Change Request Details");//7786
        setMessage("Select Open/Closed Change Requests and Press Next button.");
    }
    
    public void createControl(Composite parent)
    {
        Composite swtSwingComp = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
        //swtSwingComp.setSize(520, 450);
        swtSwingComp.setSize(520, 680);
        swtSwingComp.setLocation(15, 20);
        crSelectpanel = new NOVCRSelectionPanel();
        SWTUIUtilities.embed(swtSwingComp, crSelectpanel, false);
        
        //Point size =    swtSwingComp.getSize();
        getShell().setMinimumSize(550,720);
        getShell().setSize(550,720);
        
        Rectangle parentRect = getShell().getParent().getBounds();
        Rectangle childRect = getShell().getBounds();
        int x = parentRect.x + (parentRect.width - childRect.width) / 2;
        int y = parentRect.y + (parentRect.height - childRect.height) / 2;
       
        getShell().setLocation(x,y);
        
        setControl(swtSwingComp);
    }
}