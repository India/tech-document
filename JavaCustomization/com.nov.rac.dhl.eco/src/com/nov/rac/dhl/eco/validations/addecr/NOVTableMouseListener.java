package com.nov.rac.dhl.eco.validations.addecr;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.TableColumn;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVTableMouseListener extends MouseAdapter
{
    private NOVCustomTable m_itemSelectionTable;
    private Shell m_shell = null;
    private Registry m_reg = null;
    public NOVTableMouseListener()
    {
        m_reg = Registry.getRegistry(this);
    }
    public void mousePressed(MouseEvent e) 
    {
    }
    public void mouseClicked(MouseEvent mouseEvent)
    {
        m_itemSelectionTable = (NOVCustomTable) mouseEvent.getComponent();
        setHeaderCheckbox();
        //targetItemSelectionCheck();
    }
    private void targetItemSelectionCheck()
    {
        int iRowCount = m_itemSelectionTable.getRowCount();
        int iSelectedRow = m_itemSelectionTable.getSelectedRow();
        boolean selectedColValue = (Boolean) m_itemSelectionTable.getValueAt(iSelectedRow, 0);
        
        final Display display = Display.getDefault();
        display.syncExec(new Runnable() 
        {
            public void run() 
            {   
                m_shell = display.getActiveShell();
            }
        });
        NOVCustomTableLine itemTableLine = null;
        NOVCustomTableLine selectedTableLine = null;
        AIFTableLine tableLine = m_itemSelectionTable.getRowLine(iSelectedRow);
        selectedTableLine = (NOVCustomTableLine)tableLine;
        if(selectedColValue == true)
        {
            for(int i=0; i<iRowCount; i++)
            {
                boolean colValue = (Boolean) m_itemSelectionTable.getValueAt(i, 0);
                AIFTableLine tabLine = m_itemSelectionTable.getRowLine(i);
                itemTableLine = (NOVCustomTableLine)tabLine;
                if((i != iSelectedRow && colValue == true) && (selectedTableLine.getTableLineRevObject() == itemTableLine.getTableLineRevObject()))
                {
                    m_itemSelectionTable.setValueAt(false,iSelectedRow, 0);
                    m_itemSelectionTable.repaint();
                    
                    MessageBox.post(m_shell,m_reg.getString("DuplicateTargetItem.MSG"),"Warning..." ,MessageBox.WARNING);
                }
            } 
        }
    }
    private void setHeaderCheckbox() 
    {       
        boolean bSelectHeaderChkBox = true;
        
        int iRowCount = m_itemSelectionTable.getRowCount();
        for(int i=0; i<iRowCount; i++)
        {
            boolean colValue = (Boolean) m_itemSelectionTable.getValueAt(i, 0);
            if(!colValue)
            {
                bSelectHeaderChkBox = false;
                break;
            }
        }       
        setHeaderCheckbox(bSelectHeaderChkBox);
    }

    private void setHeaderCheckbox(boolean bSelectHeaderChkBox) 
    {
        TableColumn tabCol = m_itemSelectionTable.getColumnModel().getColumn(0);
        ((NOVTableHeaderRenderer)tabCol.getHeaderRenderer()).setValue(bSelectHeaderChkBox);
        m_itemSelectionTable.getTableHeader().repaint();
    }
}
