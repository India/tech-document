package com.nov.rac.dhl.eco.validations.addecr;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.aif.common.AIFTableLine;

public class NOVTableCheckBoxRenderer implements TableCellRenderer
{
    private static final long   serialVersionUID = 1L;
    private JCheckBox           m_checkBox;
    private NOVCustomTable m_itemSelectionTable;
    public NOVTableCheckBoxRenderer()
    {
        super();
        m_checkBox = new JCheckBox();
        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
    {
        boolean bStatus = true;
        setbackground(table, isSelected, hasFocus, row, column);
        
        TableColumn tabCol = table.getColumnModel().getColumn(0);
        JTableHeader tableHeader = table.getTableHeader();
        NOVTableHeaderRenderer checkBoxHeader = (NOVTableHeaderRenderer)tabCol.getHeaderRenderer();
        if(table instanceof NOVCustomTable)
        {
        	m_itemSelectionTable = (NOVCustomTable)table;            
            AIFTableLine tableLine = m_itemSelectionTable.getRowLine(row);
            NOVCustomTableLine itemTableLine = null;
            if(tableLine instanceof NOVCustomTableLine)
            {
                itemTableLine = (NOVCustomTableLine)tableLine;
                if(itemTableLine.getTableLineEditableStatus() == false)
                {    
                    bStatus = false;
                    checkBoxHeader.setValue(bStatus);  
                    checkBoxHeader.setEnabled(bStatus);
                }
                else
                {
                    bStatus = true;
                }
            }
            m_checkBox.setEnabled(bStatus);            
            tableHeader.repaint();
        } 
        m_checkBox.setSelected((Boolean)value);  
        m_checkBox.repaint();
        
        setHeaderCheckbox();
        
        return m_checkBox;
    }
    
    protected void setbackground(JTable jtable, boolean isSelected, boolean hasFocus,
            int row, int column) 
    {
        if(isSelected)
        {
            m_checkBox.setForeground(jtable.getSelectionForeground());
            m_checkBox.setBackground(jtable.getSelectionBackground());
        } 
        else
        {
            m_checkBox.setForeground(jtable.getForeground());
            m_checkBox.setBackground(jtable.getBackground());
        }
        if(hasFocus)
        {
            m_checkBox.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if(jtable.isCellEditable(row, column))
            {
                m_checkBox.setForeground(UIManager.getColor("Table.focusCellForeground"));
                m_checkBox.setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } 
        else
        {
            m_checkBox.setBorder(new EmptyBorder(0 , 0 , 1 , 1));
        }
    }  
    private void setHeaderCheckbox() 
    {       
        boolean bSelectHeaderChkBox = true;
        
        int iRowCount = m_itemSelectionTable.getRowCount();
        for(int i=0; i<iRowCount; i++)
        {
            boolean colValue = (Boolean) m_itemSelectionTable.getValueAt(i, 0);
            if(!colValue)
            {
                bSelectHeaderChkBox = false;
                break;
            }
        }       
        setHeaderCheckbox(bSelectHeaderChkBox);
    }

    private void setHeaderCheckbox(boolean bSelectHeaderChkBox) 
    {
        TableColumn tabCol = m_itemSelectionTable.getColumnModel().getColumn(0);
        ((NOVTableHeaderRenderer)tabCol.getHeaderRenderer()).setValue(bSelectHeaderChkBox);
        m_itemSelectionTable.getTableHeader().repaint();
    }
}
