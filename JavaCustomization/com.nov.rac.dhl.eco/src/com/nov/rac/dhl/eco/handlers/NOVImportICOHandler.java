package com.nov.rac.dhl.eco.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.dhl.eco.wizard.NOVImportICODialog;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;


public class NOVImportICOHandler extends AbstractHandler {
	Registry 			registry 	= Registry.getRegistry(this);
	private Shell m_shell = null;

	public NOVImportICOHandler() {
	
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		try
		{
			IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
			m_shell = window.getShell();
			
			InterfaceAIFComponent[] comps 	= AIFUtility.getTargetComponents();
			//validate targets'components 
			TCComponent validComp = validateComponents(comps);
			
			if(validComp!=null)
			{
				NOVImportICODialog dialog = new NOVImportICODialog(validComp);
			}
		}
		catch ( Exception e)
		{
			MessageBox.post(e.getMessage() , registry.getString("Error.TITLE"),MessageBox.ERROR);
			e.printStackTrace();
		}
	
    	return null;

	}
	public TCComponent  validateComponents(InterfaceAIFComponent[] comps) 
	{
		if(comps.length == 0)
		{
			
			MessageBox.post(registry.getString("NO_Object_Selected.ERROR"),registry.getString("Error.TITLE"),MessageBox.ERROR);
			
			return null;
		}
		if (comps.length>1) 
		{
			
	         MessageBox.post(registry.getString("More_Than_One_Object_Selected.ERROR"),
						registry.getString("Error.TITLE"),MessageBox.ERROR);
	         return null;
			
		}
		
		String objType = comps[0].getType();
		if((objType.startsWith(registry.getString("Nov4PartRev.TYPE")) == false) && (objType.startsWith(registry.getString("NonEngRev.TYPE")) == false))
		{
			MessageBox.post(registry.getString("InvalidImportICOObjects.MSG") , "Error",MessageBox.ERROR);
			return null;
		}
		
		TCSession session = (TCSession)comps[0].getSession();
		String currentUsrGrp = session.getCurrentGroup().toString();
		System.out.println("currentUsrGrp : " + currentUsrGrp);
		String owning_group="";
		TCComponent[] releaseStatus = null;
		try 
		{
			releaseStatus = ((TCComponent)comps[0]).getTCProperty("release_status_list").getReferenceValueArray();
			owning_group = ((TCComponent)comps[0]).getTCProperty("owning_group").toString();
			System.out.println("owning_group : " + owning_group);
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		if(!currentUsrGrp.equalsIgnoreCase(owning_group))
		{
	         MessageBox.post(registry.getString("The object is not owned by group you logged in"),
						registry.getString("Error.TITLE"),MessageBox.ERROR);
	         return null;
		}
		if(releaseStatus != null && releaseStatus.length>0)
		{			
			 MessageBox.post(registry.getString("ReleasedRevision.MSG"),registry.getString("Error.TITLE"),MessageBox.ERROR);
			 return null;
		}
		
		return (TCComponent) comps[0];		
	}

}