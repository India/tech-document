package com.nov.rac.dhl.eco.validations.removeitem;

import java.util.Hashtable;

import com.teamcenter.rac.common.TCTableModel;

public class NOVRemoveECRTableModel extends TCTableModel
{
    private static final long serialVersionUID = 1L;
	public NOVRemoveECRTableModel()
    {
        super();
    }

    public NOVRemoveECRTableModel(String[] data)
    {
        super(data);
    }

    public NOVRemoveECRTableModel(String as[][])
	{
	   super(as);	    
	}
    public NOVRemoveECRTableLine createLine(Object obj)
	{
    	NOVRemoveECRTableLine aiftableline = null;
		Object [] strObj = null;

		if(obj instanceof Object[])
		{
			strObj = ((Object [])obj);	
			if(strObj != null)
			{
				int i = strObj.length;
				int j = getColumnCount();
				Hashtable<String, Object> hashtable = new Hashtable<String, Object>();
				for(int k = 0; k < j && k < i; k++)
				{
					if(strObj[k] != null)
					{
						hashtable.put(getColumnIdentifier(k), strObj[k]);
					}
				}       
				aiftableline = new NOVRemoveECRTableLine(hashtable);
			}
		}
		else 
		{
			aiftableline = (NOVRemoveECRTableLine) super.createLine(obj);
		}
		return aiftableline;
	} 

    protected NOVRemoveECRTableLine[] createLines(Object obj)
    {
    	NOVRemoveECRTableLine aaiftableline[] = null;

    	if( obj instanceof Object [][] )
        {
    		Object [][] objArrArr = (Object [][])obj;    		
            int i = objArrArr.length;
            aaiftableline = new NOVRemoveECRTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	NOVRemoveECRTableLine newLine = createLine( objArrArr[j] );
                aaiftableline[j] = newLine;
            }
        } 
    	else
    	{
    		aaiftableline = (NOVRemoveECRTableLine[]) super.createLines(obj);
    	}    	
    	return aaiftableline;
    }
}