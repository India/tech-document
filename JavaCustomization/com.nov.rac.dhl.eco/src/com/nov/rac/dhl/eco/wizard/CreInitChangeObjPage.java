package com.nov.rac.dhl.eco.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.noi.rac.dhl.eco.util.components.IObserver;
import com.noi.rac.dhl.eco.util.components.NOVECOInitialPanel;
import com.teamcenter.rac.util.SWTUIUtilities;

public class CreInitChangeObjPage extends WizardPage implements IObserver
{
	public static final String PAGE_NAME = "CreInitChangeObjPage";
	public NOVECOInitialPanel initialpanel;
	public CreInitChangeObjPage() 
	{
		super(PAGE_NAME);
		setTitle("Engineering Change Object Creation");
		setMessage("Select Required Values and Press Next button.");
	}
	
	public void createControl(Composite parent) 
	{
		Composite swtinitialPageComp = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
		swtinitialPageComp.setSize(600, 350);
		swtinitialPageComp.setLocation(90, 40);
		initialpanel = new NOVECOInitialPanel();
		initialpanel.registerObserver(this); //Rakesh - DHL-208 - Register this as an Observer of the Panel
		SWTUIUtilities.embed(swtinitialPageComp, initialpanel,false);
	    
		setControl(swtinitialPageComp);
	}
	  
	/***************** Rakesh - 10/11/2011 - DHL-ECO-208 development ***********************/
	/************* Disable Next button if mandatory fields are not filled ******************/
	
	public boolean canFlipToNextPage()
	{    
		return initialpanel.isMandatoryValuesFilled();
	}	  
	/***************************************************************************************/
	
	/***************** Rakesh - 10/11/2011 - DHL-ECO-208 development ***********************/
	/************** Implement Observer to update Finish/Next button state ******************/
	
	public void update() 
	{
		this.getShell().getDisplay().syncExec(new Runnable(){

			
			public void run() {
				// TODO Auto-generated method stub
				getWizard().getContainer().updateButtons();	
			}});
		//getWizard().getContainer().updateButtons();		
	}
	/***************************************************************************************/
}