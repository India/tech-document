package com.nov.rac.dhl.eco.attrUpdate;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;


public class NOVDocumentType extends Composite
{
	private String[] docSubTypeStrArr;
	private TCProperty prop;
	private TCSession session;
	private String groupFilter;
	
	

	protected CLabel m_Label = new CLabel(this, SWT.NONE);
	protected CCombo  m_combo = new CCombo(this, SWT.DROP_DOWN | SWT.BORDER | SWT.LEFT);
	Color color = new Color(null, 255, 255, 255);

	public NOVDocumentType(Composite parent, TCSession session,TCProperty prop) 
	{
		this(parent, session, prop, null);
	}
	
	public NOVDocumentType(Composite parent, TCSession session,TCProperty prop, String groupFilter) 
	{
		//prop=prop;
		 super(parent,SWT.NONE);
		 this.prop = prop;
		 this.session=session;
		 this.groupFilter = groupFilter;
		 //GridData gd_combo = new GridData(SWT.LEFT, SWT.LEFT, true, false, 1, 1);
         m_combo.setBackground(color);         
        // m_combo.setLayoutData(gd_combo);
		this.setLayout(new GridLayout(2, false));
		setLovComponent();		
	}
	public void setLovComponent()
	{	
		TCComponentListOfValues lovValues = null;
		TCComponent theComp = getProperty().getTCComponent();		 
		try
		{
			
			TCProperty theDocCATProp  = null;
			String     strDocCATValue = null;
			
			if(theComp != null)
			{
				theDocCATProp = theComp.getTCProperty("DocumentsCAT");				
				strDocCATValue = theDocCATProp.getStringValue();			
			}			
			if(strDocCATValue != null)
			{				
				String strDocTypeLovName = strDocCATValue + "_Subtype_LOV_DHL";				
				 lovValues = TCComponentListOfValuesType.findLOVByName(session, strDocTypeLovName);	
				 load(lovValues);
				// m_combo.setText(getProperty().getStringValue());
			}
		}
		catch (TCException e)
		{
			e.printStackTrace();
		}		
			
		
	}
	public void load(TCComponentListOfValues lovs) 
	{	
	
		ListOfValuesInfo lovInfo;
		
		/*lovInfo = lovs.getListOfValues();
		String[] lovS = lovInfo.getStringListOfValues();*/
		
		String[] lovS = NOVECOHelper.getLovList(lovs, groupFilter);
		Vector<String> docTypeList = new Vector<String>();
		
		for (int i = 0; i < lovS.length; i++) 
		{
			docTypeList.add(lovS[i]);
		}
		docSubTypeStrArr = docTypeList.toArray(new String[docTypeList.size()]);
		
		m_combo.setItems(docSubTypeStrArr);		
		
		String strPropValue = getProperty().getStringValue();
		
		if(strPropValue != null)
		{
			int nIndex = m_combo.indexOf(strPropValue);
			//m_combo.setText(getProperty().getStringValue());
			m_combo.setText(strPropValue);
			if(nIndex > -1)
			{
				m_combo.select(nIndex);
			}
		}

	}
		
		
	protected TCProperty getProperty()
	{
		return prop;
	}
	
	protected TCSession getSession()
	{
		return session;
	}
	
	protected void savePropertyValue() 
	{
		String strCurrSelectedValue = m_combo.getItem( m_combo.getSelectionIndex() );
		//String strCurrSelectedValue = m_combo.getText();  
	    
	    if(strCurrSelectedValue != null)
	    {
	    	try 
	    	{
				getProperty().setStringValue( strCurrSelectedValue );
			} 
	    	catch (TCException e) 
	    	{			
				e.printStackTrace();
			}
	    
		}
	} 
	
	
}
	


