package com.nov.rac.dhl.eco.wizard;

import java.util.Map;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.dhl.eco.validations.addecr.NOVCustomTable;
import com.nov.rac.dhl.eco.validations.addecr.NOVItemSelectionDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;

public class NOVECOWizardDialog extends WizardDialog 
{
	public NOVECOWizardDialog(Shell parentShell, IWizard newWizard) 
	{
		super(parentShell, newWizard);
		

	}
	@Override
    protected Control createDialogArea(Composite parent) {
        Control ctrl = super.createDialogArea(parent);
        getProgressMonitor();
        return ctrl;
    }
    
    @Override
    protected IProgressMonitor getProgressMonitor() {
        ProgressMonitorPart monitor = (ProgressMonitorPart) super.getProgressMonitor();
        GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.heightHint = 0;
        monitor.setLayoutData(gridData);
        monitor.setVisible(false);
        return monitor;
    }
	protected void nextPressed()
    {
	    boolean bProceedToNextPage = true;
        String pageName = getWizard().getContainer().getCurrentPage().getName();
        
        IWizardPage iniPage = getWizard().getPage(CreInitChangeObjPage.PAGE_NAME);
        IWizardPage icrPage = getWizard().getPage(CRSelectionPage.PAGE_NAME);
		IWizardPage itemCRPage = getWizard().getPage(CRItemAdditionPage.PAGE_NAME);
		IWizardPage r4chgPage = getWizard().getPage(ReasonforChPage.PAGE_NAME);
		
		CreInitChangeObjPage initPage = (CreInitChangeObjPage)iniPage;
		CRSelectionPage crPage = (CRSelectionPage)icrPage;
		CRItemAdditionPage itemCrPage = (CRItemAdditionPage)itemCRPage;
		
		if(pageName.equals(CreInitChangeObjPage.PAGE_NAME))
		{
			if (initPage.initialpanel.isChangeTypeAltered) 
			{
				crPage.crSelectpanel.clearSelectedCRs();
				itemCrPage.itemAdditionPanel.clearSelectedItems();
				((ReasonforChPage)r4chgPage).reasonPanel.reasonforChange.setText("");
				((ReasonforChPage)r4chgPage).reasonPanel.addtnlCommentsText.setText("");
				initPage.initialpanel.isChangeTypeAltered=false;
			}
			crPage.crSelectpanel.setTypeofChange(initPage.initialpanel.getTypeOfChange());
		}
		if(pageName.equals(CRSelectionPage.PAGE_NAME))
        {
			//7804-added typeofChange argument to NOVItemSelectionDialog to add major/minor revisions for dhl
		    NOVItemSelectionDialog itemSelectionDlg = new NOVItemSelectionDialog(this.getShell(),initPage.initialpanel.getTypeOfChange());
            
            itemSelectionDlg.create();
            
            Object selectedECRs[] = crPage.crSelectpanel.getSelectedCRs();
          //5353-start
            Object removedECRs[]=crPage.crSelectpanel.getRemovedCRs();
            if(removedECRs.length!=0)
            {
            	itemCrPage.itemAdditionPanel.RemoveItemTable();
            }
            //5353-end
            itemSelectionDlg.populateItemTable(selectedECRs);
            
            NOVCustomTable itemSelectionTable = itemSelectionDlg.getItemSelectionTable();
            if(itemSelectionTable.getRowCount() > 0)
            {
                bProceedToNextPage = true;
                
                int iRetCode = itemSelectionDlg.open();                
                if(iRetCode == Dialog.OK)
                {
                    //TCDECREL-4795 : Start
                	//7804-added revision type argument to the below two function calls
                    Map<String, Vector<TCComponent>> itemRevFormMap = itemCrPage.itemAdditionPanel.updateItemRevCRMap(itemSelectionDlg.getCRRevInfo(),itemSelectionDlg.getRevType());
                    if(itemRevFormMap !=null)//7804
                    itemCrPage.itemAdditionPanel.populateItemTable(itemRevFormMap,itemSelectionDlg.getRevType());
                    
                    //TCDECREL-4795 : End
                }
                else
                {
                    bProceedToNextPage = false;
                }
            }
        }
		if(pageName.equals(CRItemAdditionPage.PAGE_NAME))
		{
			((ReasonforChPage)r4chgPage).reasonPanel.reasonforChange.setText(crPage.crSelectpanel.getReason4ChangeString());
			//((ReasonforChPage)r4chgPage).addtnlCommentsText.setText(crPage.crSelectpanel.getCRCommentString());//soma
		}
		itemCrPage.itemAdditionPanel.setCRSelectionPanel(crPage.crSelectpanel);
		
		if(bProceedToNextPage)
		{
			super.nextPressed();
		}
    }
	//TCDECREL-5889:Start
    @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
        createButton(parent, IDialogConstants.HELP_ID,
                "ECO Help", false);
        super.createButtonsForButtonBar(parent);
    }
    
	@Override
	protected void helpPressed()
	{
	    TCSession session = (TCSession) AIFUtility.getDefaultSession();
	    TCPreferenceService prefService = session.getPreferenceService();
	    String ECOHelp = prefService.getString(TCPreferenceService.TC_preference_site,"NOV_DHL_ECOHelp");
	    try
        {
            Runtime.getRuntime().exec("cmd /c start "+ ECOHelp);
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
	}
	//TCDECREL-5889:End
	
	protected void setButtonLayoutData(Button button)
    {
        GridData data = new GridData(256);
        int widthHint = convertHorizontalDLUsToPixels(48);
        widthHint = Math.min(widthHint, button.getDisplay().getBounds().width / 5);
        Point minSize = button.computeSize(-1, -1, true);
        data.widthHint = Math.max(widthHint, minSize.x);
        button.setLayoutData(data);
    }
}

