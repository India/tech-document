/* ============================================================
  
  File description: 

  Filename: NOVUpdateAttrDialog.java 
  Module  : \NOV_ECO\DHL_ECO\com\nov\rac\dhl\eco\attrUpdate 

  NOVUpdateAttrDialog is the dialog displayed when user selects 
  'UpdateAttributes' option from custom 'NOVTools' menu 

  Date         Developers    Description
  25-08-2010   Usha    	  Initial Creation

  $HISTORY
  ============================================================ */

package com.nov.rac.dhl.eco.attrUpdate;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVUpdateAttrDialog
{
    private Shell parent;
    private TCProperty[] props;
    private TCComponentItemRevision rev;
    private TCComponentItem item;
    private TCComponentForm imf;
    private TCComponentForm irmf;
    public NOVTcPropertyInfo[] infoObjs;
    public TCComponent target;
    public boolean m_isNameFieldTxtValueEmpty = false;
    private Registry m_registry = null;
    private boolean m_isNameName2AttrEditable = false;
    private Shell m_shell;
    
    public NOVUpdateAttrDialog(Shell shell, TCComponent targetCmpnt, Vector<TCProperty> tcProps,
            TCComponentItemRevision latestRev, TCComponentItem baseItem, TCComponentForm itemMaster,
            TCComponentForm revMaster)
    {
        rev = latestRev;
        item = baseItem;
        imf = itemMaster;
        irmf = revMaster;
        parent = shell;
        target = targetCmpnt;
        m_registry = Registry.getRegistry(this);
        props = new TCProperty[tcProps.size()];
        isNameName2Editable();
        for (int i = 0; i < tcProps.size(); i++)
            props[i] = tcProps.get(i);
        
    }// constructor
    
    // Make dialog visible.
    public void open()
    {
        // create shell and its label
        final Shell shell = new Shell(parent.getDisplay(), SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL | SWT.CENTER
                | SWT.ICON_INFORMATION | SWT.CLOSE);
        
        shell.setText(" Update Attributes ");
        shell.setLayout(new GridLayout(1, true));
        
        // give item rev name info...
        
        final Composite revComp = new Composite(shell, SWT.CENTER);
        revComp.setLayout(new GridLayout(1, true));
        revComp.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
        
        Label revLabel = new Label(revComp, SWT.CENTER);
        String str = "Item Revision: " + rev.toString();
        revLabel.setText(str);
        revLabel.setFont(new Font(shell.getDisplay(), "", 9, 1));
        
        // diaplay attributes
        final Composite propComp = new Composite(shell, SWT.BORDER);
        propComp.setLayout(new GridLayout(2, false));
        propComp.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
        
        Label attrLabel = new Label(propComp, SWT.NULL);
        attrLabel.setText("Attribute");
        attrLabel.setFont(new Font(shell.getDisplay(), "", 8, 1));
        Label newLabel = new Label(propComp, SWT.NULL);
        newLabel.setText("Value");
        newLabel.setFont(new Font(shell.getDisplay(), "", 8, 1));
        
        infoObjs = new NOVTcPropertyInfo[props.length];
        for (int i = 0; i < props.length; i++)
        {
            if (props[i].getPropertyName().equalsIgnoreCase("Secure"))
            {
                // Label separator = new Label(propComp, SWT.SEPARATOR |
                // SWT.HORIZONTAL | SWT.FILL);
                // separator.setLayoutData(new
                // GridData(GridData.FILL_HORIZONTAL));
                
                new Label(propComp, SWT.NULL);
                Label separator1 = new Label(propComp, SWT.SEPARATOR | SWT.HORIZONTAL | SWT.FILL);
                separator1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
                
                new Label(propComp, SWT.NULL);
                Label separator2 = new Label(propComp, SWT.SEPARATOR | SWT.HORIZONTAL | SWT.FILL);
                separator2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
                
                Label warningNone = new Label(propComp, SWT.NULL);
                Label warning = new Label(propComp, SWT.WRAP | SWT.BOLD | SWT.COLOR_DARK_RED);
                Color color = new Color(Display.getCurrent(), 200, 0, 0);
                warning.setText("Selecting TRUE below will make this document unavailable in the portal");
                warning.setForeground(color);
                
            }
            infoObjs[i] = new NOVTcPropertyInfo(target, item, rev, irmf, imf, propComp, props[i],
                    m_isNameName2AttrEditable);
            infoObjs[i].loadinfo();
        }
        for (int k = 0; k < props.length; k++)
        {
            infoObjs[k].assignWidgetsForDesc(this);
        }
        final Composite btnComp = new Composite(shell, SWT.CENTER);
        btnComp.setLayout(new GridLayout(2, true));
        btnComp.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
        
        Button buttonOK = new Button(btnComp, SWT.PUSH);
        buttonOK.setText("Update");
        m_shell = shell;
        buttonOK.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
            	String sMsg = validateMandatoryFields();
            	if(sMsg == null || sMsg.trim().length()==0)
            	{
            		update();
            	}
            	else
            	{
            		MessageBox.post(Display.getDefault().getActiveShell(), sMsg,"Warning...", MessageBox.WARNING);
            	}
                // shell.dispose();
            }
        });
        Button buttonCancel = new Button(btnComp, SWT.PUSH);
        buttonCancel.setText("Cancel");
        buttonCancel.addSelectionListener(new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                shell.dispose();
            }
        });
        
        shell.pack();
        shell.open();
        Display display = parent.getDisplay();
        while (!shell.isDisposed())
        {
            if (!display.readAndDispatch())
                display.sleep();
        }
        
    }// dialog open method
    
    // Updates the Values
    private void update()
    {
        try
        {
            for (int i = 0; i < props.length; i++)
            {
                infoObjs[i].saveinfo();
                m_isNameFieldTxtValueEmpty = infoObjs[i].getRevisionRevAttrFieldsForGrp33() ? true : false;
                if (m_isNameFieldTxtValueEmpty)
                {
                    break;
                }
            }
            if (!m_isNameFieldTxtValueEmpty)
            {
                // TCDECREL-2603 : START :Akhilesh
                if (target instanceof TCComponentItemRevision)
                {
                    MessageBox.post(parent, "Attributes update completed on: "
                            + "Item Revision and Item Revision Master", "Attributes Update Complete",
                            MessageBox.INFORMATION);
                }
                else
                {
                    MessageBox.post(parent, "Attributes update completed on: " + "Item and Item Master",
                            "Attributes Update Complete", MessageBox.INFORMATION);
                }
                m_shell.dispose();
            }
            else
            {
                MessageBox.post(Display.getDefault().getActiveShell(), m_registry.getString("nameFiledWarning.MSG"),
                        "Warning", MessageBox.WARNING);
            }
            // TCDECREL-2603 : END
        }
        catch (Exception ex)
        {
            MessageBox.post(ex.getMessage(), "Attributes Update failed", MessageBox.ERROR);
            ex.printStackTrace();
        }
    }// dialog update method
    
    private boolean isNameName2Editable()
    {
        String nameName2EditablePrefName = m_registry.getString("NOVDHLNameName2EditableGroup.NAME");
        m_isNameName2AttrEditable = NOVECOHelper.isNameName2EditableGrp(nameName2EditablePrefName);
        return m_isNameName2AttrEditable;
        
    }
    private String validateMandatoryFields()
    {
    	String sErrorMessage = "";
    	String sFieldName = "";
    	boolean isEmpty   = false;
    	for (int k = 0; k < props.length; k++)
        {
    		String sPropName = infoObjs[k].propName;
    		if((sPropName.equalsIgnoreCase("object_name")) || 
    		    (sPropName.equalsIgnoreCase("object_desc")) 
    		 )
    		{
    			isEmpty = ((Text)(infoObjs[k].newValField)).getText().trim().length() > 0 ? false : true;
    			if(isEmpty)
    			{
    			sFieldName = sFieldName.concat("\n");
				try {
					sFieldName = sFieldName.concat(infoObjs[k].txtService.getTextValue(sPropName));
				} catch (TCException e) {
					
					e.printStackTrace();
				} 
    			}
    		}
    		//4509-start
    		else if ((sPropName.equalsIgnoreCase("Sites")))
    		{
    			sFieldName=validateSiteField(sPropName,k,sFieldName);
    			  
    		}	
    		//4509-end	
    			
    			/*if(isEmpty)
    			{
    				try 
    				{
    					sFieldName = sFieldName.concat("\n");
    					sFieldName = sFieldName.concat(infoObjs[k].txtService.getTextValue(sPropName));    					
    				} 
    				catch (TCException e) 
    				{
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    			}*/
    		}
    		
    //}
    	if(sFieldName.length()>0)
    	{
    		sErrorMessage = m_registry.getString("mandatoryField.MSG");
    		sErrorMessage = sErrorMessage.concat(sFieldName);
    	}
    	return sErrorMessage;
    }
    
//4509-start
    private String validateSiteField(String sPropertyName,int count,String sErrorFields)
    {
    	String  sSiteField = "";
    	boolean flag;
    	flag=((NovSiteControl) (infoObjs[count].newValField)).getSiteValues().length==0 ? true: false;
    	if(flag)
    	{
    		sSiteField = sSiteField.concat("\n");
    		try {
				sSiteField = sSiteField.concat(infoObjs[count].txtService.getTextValue(sPropertyName));
			} catch (TCException e) {
				
				e.printStackTrace();
			}    
			
    		
    	return sErrorFields.concat(sSiteField);
    	}
    	else
    	{
    		return sErrorFields;
    	}
    	
    }
    //4509-end
}// dialog class

