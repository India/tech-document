package com.nov.rac.dhl.eco.whereusedreport.listeners;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.dhl.eco.util.components.NOVWhereUsedFilterCriteria;
import com.noi.rac.dhl.eco.util.components.NOVWhereUsedFilterDialog;
import com.nov.rac.dhl.eco.whereusedreport.IWhereUsedTableFilter;

public class WhereUsedTableFilterListener implements ActionListener
{
    private NOVWhereUsedFilterCriteria m_filterCriteria;
    private Shell m_modalShell;
    private NOVWhereUsedFilterDialog m_filterDlg;
    private IWhereUsedTableFilter m_whereUsedFilter;
    public WhereUsedTableFilterListener(IWhereUsedTableFilter filter)
    {
        m_whereUsedFilter = filter;
    }
    @Override
    public void actionPerformed(ActionEvent e)
    {
        JButton filterBtn = (JButton) e.getSource();
        
        if (m_filterCriteria == null)
        {
            m_filterCriteria = new NOVWhereUsedFilterCriteria();
        }
        
        final Display display = Display.getDefault();
        display.syncExec(new Runnable()
        {
            public void run()
            {
                Shell activeShell = display.getActiveShell();
                
                m_modalShell = new Shell(activeShell, SWT.EMBEDDED | SWT.APPLICATION_MODAL | SWT.NO_BACKGROUND);
                Frame frame = SWT_AWT.new_Frame(m_modalShell);
                
                m_filterDlg = new NOVWhereUsedFilterDialog(frame, m_filterCriteria);
                
                // To fix SWT and SWING Modality issue
                m_modalShell.setBounds(0, 0, 0, 0);
                m_modalShell.setVisible(true);
            }
        });
        
        final int hDisp = 280;
        final int vDisp = 25;
        int x = filterBtn.getLocationOnScreen().x - hDisp;
        int y = filterBtn.getLocationOnScreen().y + vDisp;
        Object obj = m_filterDlg.showDialog(x, y);
        m_filterCriteria = (NOVWhereUsedFilterCriteria) obj;
        
        // Modal shell has to be disposed once SWING dialog is disposed
        display.syncExec(new Runnable()
        {
            public void run()
            {
                if (m_modalShell != null && !m_modalShell.isDisposed())
                {
                    m_modalShell.dispose();
                }
            }
        });
        
        m_whereUsedFilter.filterSearchResults();
    }
    
    public NOVWhereUsedFilterCriteria getSearchCriteria()
    {
        return m_filterCriteria;
    }
}
