package com.nov.rac.dhl.eco.lifecycletracking;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class NOVLifecycleTrackingDlg 
{
	 
	private String reportStr ="";
	private TCComponent selComponent;
	public NOVLifecycleTrackingDlg(Display display, TCComponent selComp)
	{
		selComponent = selComp;
		final Shell shell = new Shell(display, SWT.CLOSE);
		shell.setText("Lifecycle History");
		GridLayout layout = new GridLayout(1, false);
		shell.setLayout(layout);
		
		try 
		{
			String itemstr = selComp.getProperty("object_string");
			
			final String text1 =" <FONT SIZE=\"6\" COLOR=#330033><center> Lifecycle History of <br>"+itemstr+"</center></FONT>"+
			"<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Item Revision</B></TD>" +
			"<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>ECO</B></TD>" +
			"<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Lifecycle-Old</B></TD>"+
		    "<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Lifecycle-New</B></TD>" +
		    "<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Date</B></TD></TR>"+getReportString()+"</table>";		
			
			Browser browser = new Browser(shell, SWT.NONE);
			browser.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true, 1, 1));
			browser.setText(text1);
			browser.setSize(680, 300);
			Button button = new Button (shell, SWT.PUSH);
			button.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
			button.setText("Save Report");
			button.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
			button.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event event) {
					FileDialog dialog = new FileDialog(shell, SWT.SAVE);
					String [] filterNames = new String [] {"html"};
					dialog.setFilterNames(filterNames);
					dialog.setText("Save");
					String filename = dialog.open();
					if (filename != null) 
					{
						File file = new File(filename);
						String nameOfFile = "";   
			            nameOfFile = file.getPath();
			            nameOfFile =nameOfFile+".html";
			            try 
						{   
							BufferedWriter out = new BufferedWriter(new FileWriter(nameOfFile));   
							out.write(text1);   
							out.close();   
						} 
						catch (IOException ex1) 
						{   
							MessageBox.post(ex1);
						}					
					}				
				}
			});
			shell.setSize(680, 350);
			shell.open();
		
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}


	private String getReportString() 
	{
		if (selComponent!=null) 
		{
			try 
			{
				TCComponent[] itemRevs = null;
				if (selComponent instanceof TCComponentItemRevision) 
				{
					itemRevs = new TCComponent[1];
					itemRevs[0] = selComponent;
				}
				else
				{
					itemRevs = selComponent.getTCProperty("revision_list").getReferenceValueArray();	
				}
				
				String itemID = selComponent.getProperty("item_id");
				//TCDECREL-2799 :Start : Akhilesh
				HashMap<InterfaceAIFComponent, Date> valueMap =new HashMap<InterfaceAIFComponent, Date>();
				ValueComparator bv = new ValueComparator(valueMap);
				TreeMap<InterfaceAIFComponent,Date>   sortedMap   =   new TreeMap<InterfaceAIFComponent, Date>(bv);
				
				HashMap<InterfaceAIFComponent, String> itemMap =new HashMap<InterfaceAIFComponent, String>();
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
				
				for (int i = 0; i < itemRevs.length; i++) 
				{
					AIFComponentContext[] comps = ((TCComponentItemRevision)itemRevs[i]).getRelated("RelatedECN");
					if (comps!=null && comps.length>0) 
					{
						for (int inx = 0; inx < comps.length; inx++) 
						{
						    //System.out.println( "Type : " + comps[inx].getComponent().getType() );
						    if (comps[inx].getComponent().getType().equalsIgnoreCase("_DHL_EngChangeForm_")) 
		                    {   //TCDECREL-4636 : Start
						        String dateReleased = comps[inx].getComponent().getProperty("date_released");
						        if(NOVECOHelper.isEcoReleased((TCComponentForm) comps[inx].getComponent()) && !dateReleased.isEmpty())
						        {
    						        //System.out.println( "values : " + comps[inx].getComponent().getProperty("date_released"));
        							try 
        							{
        								valueMap.put((TCComponent)comps[inx].getComponent(),dateFormat.parse(comps[inx].getComponent().getProperty("date_released")));
        								itemMap.put((TCComponent)comps[inx].getComponent(), itemRevs[i].toString());
        							}
        							catch (ParseException  pe)
        							{
        								pe.printStackTrace();
        							}
						        }
						      //TCDECREL-4636 : End
		                    }
						}
					}
				}
				
				sortedMap.putAll(valueMap);
				for(Map.Entry<InterfaceAIFComponent, Date> entry : sortedMap.entrySet())
				{
					if (entry.getKey().getType().equalsIgnoreCase("_DHL_EngChangeForm_")) 
					{
						TCComponentForm ecoform = (TCComponentForm)entry.getKey();						
						TCComponent[] lifecycleIns = ((TCComponentForm)entry.getKey()).getTCProperty("nov4_ecotgtlifecycledata").getReferenceValueArray();
						//TCComponent[] dispIns	   = ((TCComponentForm)entry.getKey()).getTCProperty("ectargetdisposition").getReferenceValueArray();
						
						for (int k = 0; k < lifecycleIns.length; k++) 
						{
							String targetID = lifecycleIns[k].getProperty("nov4_targetitemid");
							if (itemID.equals(targetID)) 
							{
								String oldlifecycle =lifecycleIns[k].getProperty("nov4_oldlifecycle").length()==0?"N/A":lifecycleIns[k].getProperty("nov4_oldlifecycle");
								String newlifecycle =lifecycleIns[k].getProperty("nov4_newlifecycle").length()==0?"N/A":lifecycleIns[k].getProperty("nov4_newlifecycle");
								
								reportStr= reportStr+"<TR><TD>"+itemMap.get(entry.getKey())+"</TD>"+
													 "<TD>"+entry.getKey().toString()+"</TD>"+
								                     "<TD>"+oldlifecycle+"</TD>"+
								                     "<TD>"+newlifecycle+"</TD>"+
								                     "<TD>"+lifecycleIns[k].getProperty("nov4_newlifecycledate")+"</TD>";
							}
						}
					}
				}
				/*for (int i = 0; i < itemRevs.length; i++) 
				{
					AIFComponentContext[] comps = ((TCComponentItemRevision)itemRevs[i]).getRelated("RelatedECN");
					if (comps!=null && comps.length>0) 
					{
						HashMap<String,InterfaceAIFComponent>   sortedMap   =   new   HashMap<String,InterfaceAIFComponent>();  
						for (int inx = 0; inx < comps.length; inx++) 
						{
							sortedMap.put(comps[inx].getComponent().getProperty("date_released"), (TCComponent)comps[inx].getComponent());
						}
						Object[] keys = sortedMap.keySet().toArray();
						Arrays.sort(keys);
						for (int j = 0; j < keys.length; j++) 
						{
							if (sortedMap.get(keys[j]).getType().equalsIgnoreCase("_DHL_EngChangeForm_")) 
							{
								//TCComponent[] lifecycleIns = ((TCComponentForm)comps[j].getComponent()).getTCProperty("ecotargetlifecycledata1").getReferenceValueArray();
								TCComponent[] lifecycleIns = ((TCComponentForm)sortedMap.get(keys[j])).getTCProperty("nov4_ecotgtlifecycledata").getReferenceValueArray();
								for (int k = 0; k < lifecycleIns.length; k++) 
								{
									String targetID = lifecycleIns[k].getProperty("nov4_targetitemid");
									if (itemID.equals(targetID)) 
									{
										String oldlifecycle =lifecycleIns[k].getProperty("nov4_oldlifecycle").length()==0?"N/A":lifecycleIns[k].getProperty("nov4_oldlifecycle");
										String newlifecycle =lifecycleIns[k].getProperty("nov4_newlifecycle").length()==0?"N/A":lifecycleIns[k].getProperty("nov4_newlifecycle");
										
										reportStr= reportStr+"<TR><TD>"+itemRevs[i].toString()+"</TD>"+
															 "<TD>"+sortedMap.get(keys[j]).toString()+"</TD>"+
										                     "<TD>"+oldlifecycle+"</TD>"+
										                     "<TD>"+newlifecycle+"</TD>"+
										                     "<TD>"+lifecycleIns[k].getProperty("nov4_newlifecycledate")+"</TD>";
									}
								}
							}
						}						
					}
					else
					{
//						AIFComponentContext[] masterformcomps =((TCComponentItemRevision)itemRevs[i]).getRelated("IMAN_master_form_rev");
//						InterfaceAIFComponent formComp = masterformcomps[0].getComponent();
//						if (formComp!=null) 
//						{
//							String lifecycle =((TCComponentForm)formComp).getProperty("Lifecycle").length()==0?"N/A":((TCComponentForm)formComp).getProperty("Lifecycle");
//							reportStr= reportStr+"<TR><TD>"+itemRevs[i].toString()+"</TD>"+"" +
//							                     "<TD>"+"N/A"+"</TD>"+
//							                     "<TD>"+"N/A"+"</TD>"+
//		                                         "<TD>"+lifecycle+"</TD>"+
//		                                         "<TD>"+"N/A"+"</TD>";	
//						}
					}
				}*/
									
			//TCDECREL-2799 :End
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return reportStr;
	}
	
	//TCDECREL-2799 :Start
	class ValueComparator implements Comparator
	{     
		HashMap<InterfaceAIFComponent, Date> baseMap = new HashMap<InterfaceAIFComponent, Date>();
		public ValueComparator(HashMap<InterfaceAIFComponent, Date> itemMap) {
			this.baseMap=itemMap;
		}
		
		//@Override
		public int compare(Object o1, Object o2) 
		{
			if (baseMap.get(o1).after(baseMap.get(o2)))
			{             
				return 1;        
			} 
			else if (baseMap.get(o1).equals(baseMap.get(o2))) 
			{             
				return 0;         
			} else 
			{             
				return -1;         
			} 
		} 
	} 
	//TCDECREL-2799 :End
}
