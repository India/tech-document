package com.nov.rac.dhl.eco.validations.additem;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVECRTableCommonCellRenderer extends AbstractTCTableCellRenderer
{
    public NOVECRTableCommonCellRenderer()
    {
        super();
    }
    public Component getTableCellRendererComponent(JTable table, Object value, 
            boolean isSelected, boolean hasFocus, int row, int column)
    {
        setbackground(table, isSelected, hasFocus, row, column);
               
        setText(table, value, row, column);
        
        if(table instanceof NOVECRTable)
        {
            NOVECRTable ecrTable = (NOVECRTable)table;            
            AIFTableLine tableLine = ecrTable.getRowLine(row);
            NOVECRTableLine ecrTableLine = null;
            if(tableLine instanceof NOVECRTableLine)
            {
                ecrTableLine = (NOVECRTableLine)tableLine;
                if(ecrTableLine.getTableLineEditableStatus() == false)
                {
                    super.setBackground(Color.lightGray);
                }
            }
        }  
        
        return this;
    }
    protected void setText(JTable jtable, Object obj, int row, int column) 
    {
        String s = getDisplayText(obj);

        setText(s != null ? s : "");
        setIcon(null);
        int k = getFontMetrics(getFont()).stringWidth(getText());
        int l = jtable.getCellRect(row, column, false).width;
        if(k > l - 8)
            setToolTipText(s);
        else
            setToolTipText(null);
    }

    protected void setbackground(JTable jtable, boolean isSelected, boolean hasFocus,
            int row, int column) {
        if(isSelected)
        {
            super.setForeground(jtable.getSelectionForeground());
            super.setBackground(jtable.getSelectionBackground());
        } 
        else
        {
            super.setForeground(jtable.getForeground());
            super.setBackground(row % 2 != 1 ? jtable.getBackground() : alternateBackground);
        }
        if(hasFocus)
        {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if(jtable.isCellEditable(row, column))
            {
                super.setForeground(UIManager.getColor("Table.focusCellForeground"));
                super.setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } 
        else
        {
            setBorder(noFocusBorder);
        }
    }
    @Override
    protected Icon getDisplayIcon(TCComponent arg0, Object arg1)
    {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    protected void initiateIcons()
    {
        // TODO Auto-generated method stub
        
    }
    
}
