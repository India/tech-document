package com.nov.rac.dhl.eco.validations.removeitem;

import java.util.Hashtable;

import com.teamcenter.rac.common.TCTableLine;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVRemoveECRTableLine extends TCTableLine 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TCComponent m_itemRevComp = null;
	private TCComponent m_formComp = null;
	public NOVRemoveECRTableLine(Hashtable<String, Object> hashtable)
	{
		super(hashtable);
	}
	public TCComponent getTableLineRevObject()
	{
	    return m_itemRevComp;
	}
	public void setTableLineRevObject(TCComponent tcComponent)
	{
	    m_itemRevComp = tcComponent;
	}
	public TCComponent getTableLineFormObject()
    {
        return m_formComp;
    }
    public void setTableLineFormObject(TCComponent tcComponent)
    {
        m_formComp = tcComponent;
    }
}
