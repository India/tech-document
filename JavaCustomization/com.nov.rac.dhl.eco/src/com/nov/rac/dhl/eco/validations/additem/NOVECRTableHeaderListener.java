package com.nov.rac.dhl.eco.validations.additem;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class NOVECRTableHeaderListener implements TableModelListener
{
    private final JTable m_table;
    private final int m_targetColumnIndex;
    public NOVECRTableHeaderListener(JTable table, int index) 
    {
        this.m_table = table;
        this.m_targetColumnIndex = index;
    }
    @Override
    public void tableChanged(TableModelEvent tablemodelevent)
    {
        if(tablemodelevent.getType()==TableModelEvent.UPDATE && tablemodelevent.getColumn()==m_targetColumnIndex) 
        {
            int vci = m_table.convertColumnIndexToView(m_targetColumnIndex);
            TableColumn column = m_table.getColumnModel().getColumn(vci);
            
            TableModel tableModel = m_table.getModel();
            for(int i=0; i<tableModel.getRowCount(); i++) 
            {
                Boolean celVal = (Boolean)tableModel.getValueAt(i, m_targetColumnIndex);
                if(celVal.booleanValue() == false)
                {
                    column.setHeaderValue(false);
                    break;
                }
            }
            column.setHeaderValue(true);            
            JTableHeader tableHeader = m_table.getTableHeader();
            tableHeader.repaint(tableHeader.getHeaderRect(vci));
        }
        
    }
    
}
