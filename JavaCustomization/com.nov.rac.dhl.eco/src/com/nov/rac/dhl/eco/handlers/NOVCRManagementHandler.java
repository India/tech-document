package com.nov.rac.dhl.eco.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.wizard.NOVCRManagementDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentRole;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;


public class NOVCRManagementHandler extends AbstractHandler 
{
	public NOVCRManagementHandler() 
	{
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
						
		try{
			/*System.out.println(" Group Name :" + session.getGroup().getGroupName());
			System.out.println(" Role Name :" + session.getRole().getStringProperty("role_name"));
						
			if( session.getGroup().getGroupName().equals("3C - ReedHycalog"))
			{
					if( session.getRole().getStringProperty("role_name").equals("RH Approver"))
					{
						NOVCRManagementDialog crManagementDialog = new NOVCRManagementDialog(window.getShell());
						crManagementDialog.open();
					}else 
					{
						MessageBox.post("Only RH Approver can close the ECR Manually" , "ERROR",MessageBox.ERROR);
					}
			}*/
			if( NOVECOHelper.isDHGroup() ) //If it is DH group
			{
					if( NOVECOHelper.isAllowedRole("_NOV_DH_ECR_Maintenance_roles_") ) //If it is RH Approver role or any other as defined by preference variable
					{
						NOVCRManagementDialog crManagementDialog = new NOVCRManagementDialog(window.getShell());
						crManagementDialog.open();
					}else 
					{
						MessageBox.post("Manual Closing of ECR is not available to the current user." , "ERROR",MessageBox.ERROR);
					}
			}
			
		} catch ( TCException e)
		{
			MessageBox.post(e.getMessage() , "Error",MessageBox.ERROR);
			e.printStackTrace();
		}
			
		return null;
	}
	
	protected NOVECOHelper helper = new NOVECOHelper();
	

}
