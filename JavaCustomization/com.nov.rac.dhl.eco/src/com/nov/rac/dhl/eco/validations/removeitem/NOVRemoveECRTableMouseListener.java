package com.nov.rac.dhl.eco.validations.removeitem;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.TableColumn;

public class NOVRemoveECRTableMouseListener extends MouseAdapter
{
    private NOVRemoveECRTable m_ecrSelectionTable;
    public NOVRemoveECRTableMouseListener()
    {
    }
    public void mousePressed(MouseEvent e) 
    {
    }
    public void mouseClicked(MouseEvent mouseEvent)
    {
        m_ecrSelectionTable = (NOVRemoveECRTable) mouseEvent.getComponent();
        setHeaderCheckbox();
    }
    
    private void setHeaderCheckbox() 
    {       
        boolean bSelectHeaderChkBox = true;
        
        int iRowCount = m_ecrSelectionTable.getRowCount();
        for(int i=0; i<iRowCount; i++)
        {
            boolean colValue = (Boolean) m_ecrSelectionTable.getValueAt(i, 0);
            if(!colValue)
            {
                bSelectHeaderChkBox = false;
                break;
            }
        }       
        setHeaderCheckbox(bSelectHeaderChkBox);
    }

    private void setHeaderCheckbox(boolean bSelectHeaderChkBox) 
    {
        TableColumn tabCol = m_ecrSelectionTable.getColumnModel().getColumn(0);
        ((NOVRemoveECRTableHeaderRenderer)tabCol.getHeaderRenderer()).setValue(bSelectHeaderChkBox);
        m_ecrSelectionTable.getTableHeader().repaint();
    }
}
