package com.nov.rac.dhl.eco.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import com.noi.rac.dhl.eco.util.components.IObserver;
import com.noi.rac.dhl.eco.util.components.NOVECOReasonPanel;
import com.teamcenter.rac.util.SWTUIUtilities;

public class ReasonforChPage extends WizardPage implements IObserver
{
	//public Text reasonforChange;
	//public Text addtnlCommentsText;
	public boolean changeStatus = false;
	public static final String PAGE_NAME = "ReasonforChPage";
	public NOVECOReasonPanel reasonPanel;
    public ReasonforChPage() 
    {
	    super(PAGE_NAME);
	    setTitle("Reason for Change details");//7786
		setMessage("Add Additional comments");
    }
    
    public void createControl(Composite parent) 
    {
    	Composite reasonPageComp = new Composite(parent, SWT.EMBEDDED | SWT.NO_BACKGROUND);
    	reasonPageComp.setSize(450, 350);
    	reasonPageComp.setLocation(20, 20);
    	reasonPanel = new NOVECOReasonPanel();
    	reasonPanel.registerObserver(this); 
		SWTUIUtilities.embed(reasonPageComp, reasonPanel, false);
	    
		setControl(reasonPageComp);
		
	    /*Composite composite = new Composite(parent, SWT.NONE);
	    composite.setLayout(new GridLayout(1, false));
	    composite.setSize(450,350);
	    composite.setLocation(20, 20);
	    Label lblAddReasonForChange = new Label(composite, SWT.LEFT);
	    lblAddReasonForChange.setText("Additional Reason for Change");
	    lblAddReasonForChange.setFont(new Font(Display.getCurrent(),"Arial", 9, SWT.BOLD ));
	    addtnlCommentsText = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
	    addtnlCommentsText.setLayoutData(new GridData(GridData.FILL_BOTH));	   
		
	    new Label(composite, SWT.LEFT).setText(" ");
	    
	    Label lblReasonForChange = new Label(composite, SWT.LEFT);
	    lblReasonForChange.setText("Reason for Change from ECR");
	    lblReasonForChange.setFont(new Font(Display.getCurrent(),"Arial", 9, SWT.BOLD ));
	    reasonforChange = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
	    reasonforChange.setLayoutData(new GridData(GridData.FILL_BOTH));
	    
	    setControl(composite);*/
    }
    	
	
	public void update() 
	{
		this.getShell().getDisplay().syncExec(new Runnable(){

		
			public void run() 
			{
				getWizard().getContainer().updateButtons();	
			}});		
	}
	
}