package com.nov.rac.dhl.eco.wizard;

import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import com.nov.rac.mmr.revisionstrategy.MajorDefaultRevisionStrategy;
import com.nov.rac.mmr.utils.MMRUtility;
import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.client.model.ErrorStack;

public class NOVCreateECOHelper {

	private boolean m_bIsAllValidateSuccess = false;
	private boolean m_bIsAllCreateECOSuccess = false;
	
	
    //7804-added two more input i.e, revType,mapItemsWithRevType to the below function call.
	public void createECO(TCComponent ecoForm, TCComponent[] targetItems, TCComponent[] selectedECRs, TCComponent[] selectedNovelForms, Map<String, String> mapECOProperty,Map<String, String> mapItemsWithRevIds)		// TBD - operation name
	{
		NOV4ChangeManagement.CreateECOandAddTargetsResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.CreateECOandAddTargetsInput input = new NOV4ChangeManagement.CreateECOandAddTargetsInput();
        
        input.targetItems = targetItems;
        input.ecrForms = selectedECRs;
        input.novelForms = selectedNovelForms;
        input.ecoForm = ecoForm;
        input.ecoProperties= mapECOProperty;
       
       
        //7804-test-adding mapItemsWithRevIds to input
        if(mapItemsWithRevIds!=null)
        {
        	input.itemsWithSuggestedRevIds=mapItemsWithRevIds;
        }
        
        theResponse = service.createECOandAddTargetsToECO(input);
        
        if (theResponse.serviceData.sizeOfPartialErrors() > 0) // Check for partial errors
        {
        	String errorMsg = "";
            for (int intx = 0; intx < theResponse.serviceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.serviceData.getPartialError(intx);
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
            MessageBox.post(errorMsg, "Cannot create ECO", MessageBox.WARNING);
            m_bIsAllCreateECOSuccess =  false;
        }
        else
        	m_bIsAllCreateECOSuccess =  true;
        
	}
	//7804-added input argument revType to the below function call
	public TCComponent[] validateTargetsToECO(TCComponent[] itemsTarget,Map<String, String> mapECOProperty,String sRevType)
	{
		TCComponent[] validTargetItems = null;
		
		NOV4ChangeManagement.ECOValidateTargetResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.ECOValidateTargetInput input = new NOV4ChangeManagement.ECOValidateTargetInput();
        
        input.targetItems = itemsTarget;
        input.stringProps = mapECOProperty;
        if( sRevType !=null && sRevType.length() > 0)
        {
        	input.revisionType = sRevType;//7804
        }
        theResponse = service.validateTargetToECO(input);
        
        validTargetItems = theResponse.validatedTargetItemOutput;
        //KLOC2136: moved the commented code to a new function
        validateTargetsAdded(theResponse,validTargetItems);
        /*if (theResponse.validateServiceData.sizeOfPartialErrors() > 0) // Check for partial errors
        {
            m_bIsAllValidateSuccess = false;
        	String errorMsg = "Following item/item revision cannot be added to Target";
            for (int intx = 0; intx < theResponse.validateServiceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.validateServiceData.getPartialError(intx);
                
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
          //6383-start
            if(errorMsg.contains("error_919706") && validTargetItems.length>0)
            {
            	m_bIsAllValidateSuccess = true;
            }
            else
            {
            	 MessageBox.post(errorMsg, "Cannot add Item to target", MessageBox.WARNING);
            }
            //6383-end
         }
        else
        {
        	m_bIsAllValidateSuccess = true;
        }*/
        
        return validTargetItems;
	}
    //7804-test-start
	private void validateTargetsAdded(NOV4ChangeManagement.ECOValidateTargetResponse theResponse,TCComponent[] validTargetItems)
	{
	    if (theResponse.validateServiceData.sizeOfPartialErrors() > 0) // Check for partial errors
        {
            m_bIsAllValidateSuccess = false;
            String errorMsg = "Following item/item revision cannot be added to Target";
            for (int intx = 0; intx < theResponse.validateServiceData.sizeOfPartialErrors(); intx++)
            {
                ErrorStack errorStack = theResponse.validateServiceData.getPartialError(intx);
                
                errorMsg = errorMsg + "\n" + (errorStack.getMessages())[0];
            }
          //6383-start
            if(errorMsg.contains("error_919706") && validTargetItems.length>0)
            {
                m_bIsAllValidateSuccess = true;
            }
            else
            {
                 MessageBox.post(errorMsg, "Cannot add Item to target", MessageBox.WARNING);
            }
            //6383-end
         }
        else
        {
            m_bIsAllValidateSuccess = true;
        }
	    
	}
	public String getNextMajorRevisionId(Vector<TCComponent> partFamilyComps)
	{
	    MajorDefaultRevisionStrategy majorObj = new MajorDefaultRevisionStrategy();
        return majorObj.getNextRevision(partFamilyComps);
	}
	
	public String getNextMinorRevisionId(String revId)
	{
		return incrementMinorRevision(formatMinorString(revId));
	}
    
    protected String incrementMinorRevision(String latestRev)
    {
        int length = latestRev.length();
        String primaryPart = latestRev.substring(0, latestRev.lastIndexOf("."));
        String secondaryPart = latestRev.substring(latestRev.lastIndexOf(".") + 1, length);
        
        String string = incrementRevision(secondaryPart);
        return (primaryPart + "." + string);
    }
    
    private String incrementRevision(String latestRev)
    {
        String incrementedRevision = getIncrementRevision(latestRev);
        
        // Method to handle Zero at unit's place
        // Zero is allowed at Unit's place iff and iff decades place is consumed
        // by non-zero digit.
        boolean isValid = isValidMajorRevision(incrementedRevision);
        if (!isValid)
        {
            // replace 0 by 1
            StringBuilder stringBuilder = new StringBuilder(incrementedRevision);
            int unitPlaceIndex = stringBuilder.length() - 1;
            stringBuilder.setCharAt(unitPlaceIndex, '1');
            incrementedRevision = stringBuilder.toString();
        }
        return incrementedRevision;
    }
    
    private String formatMinorString(String str)
    {
        if (!str.contains("."))
        {
            str = str + ".01";
        }
        return str;
    }
    
    private boolean isValidMajorRevision(String revision)
    {
        boolean isValid = true;
        // Code to check Zero at unit's place
        // Zero is allowed at Unit's place iff and iff decades place is
        // consumed
        // by non-zero digit.
        if (!MMRUtility.isNumeric(revision))
        {
            int unitPlaceIndex = revision.length() - 1;
            if (Character.getNumericValue(revision.charAt(unitPlaceIndex)) == 0)
            {
                int decadePlaceIndex = unitPlaceIndex - 1;
                int decadeValue = Character.getNumericValue(revision.charAt(decadePlaceIndex));
                if (decadeValue < 1 || decadeValue > 9)
                {
                    isValid = false;
                }
            }
        }
        return isValid;
    }
    
    private String getIncrementRevision(String latestRev)
    {
        StringBuilder stringBuilder = new StringBuilder(latestRev);
        int valueLength = stringBuilder.length();
        /*
         * if (getRevisionMaxLength() < valueLength) throw new
         * RuntimeException("Can not increment revision " + latestRev);
         */
        boolean isCarryForward = false;
        for (int index = valueLength - 1; index >= 0; index--)
        {
            char character = stringBuilder.charAt(index);
            String valueString = getReferenceString(character);
            int lengh = valueString.length();
            int currentCharIndex = valueString.indexOf(character);
            int newCharIndex = currentCharIndex;
            isCarryForward = false;
            char nextCharacter = '\0';
            do
            {
                newCharIndex = newCharIndex + 1;
                if (newCharIndex == lengh)
                {
                    newCharIndex = 0;
                    isCarryForward = true;
                }
                nextCharacter = valueString.charAt(newCharIndex);
            }
            while (isInDisAallowedList(nextCharacter));
            
            stringBuilder.setCharAt(index, nextCharacter);
            if (!isCarryForward)
            {
                break;
            }
            
        } // for
       if (isCarryForward)
       {
           int newCycleIndex = 0;
           char initialChar = stringBuilder.charAt(0);
           String valueString = getReferenceString(initialChar);
           if (Character.isDigit(initialChar))
           {
               newCycleIndex = 1;
           }
           char carryForwardChar = valueString.charAt(newCycleIndex);
           stringBuilder.insert(0, carryForwardChar);
            
        }
        return stringBuilder.toString();
    }
   
    private String getReferenceString(char c)
    {
        String valueString = null;
        if (Character.isDigit(c))
        {
            valueString = "0123456789";
        }
        else if (Character.isLowerCase(c))
        {
            valueString = "abcdefghijklmnopqrstuvwxyz";
        }
        else if (Character.isUpperCase(c))
        {
            valueString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        return valueString;
    }
    
    private boolean isInDisAallowedList(char c)
    {
        return isInDisAallowedList("" + c);
    }
    
    protected boolean isInDisAallowedList(String string)
    {
        return Arrays.asList(MMRUtility.getDisallowedRevisions()).contains(string);
    }
	//7804-test-end
	public boolean isAllValidateSuccess()
	{
		return m_bIsAllValidateSuccess;
	}

	public boolean isAllCreateECOSuccess()
	{
		return m_bIsAllCreateECOSuccess;
	}
}
