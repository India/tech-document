package com.nov.rac.dhl.eco.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.dhl.eco.wizard.NOVECOWizard;
import com.nov.rac.dhl.eco.wizard.NOVECOWizardDialog;


public class NOVChangeObjectHandler extends AbstractHandler {

	public NOVChangeObjectHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		NOVECOWizardDialog dialog = new NOVECOWizardDialog(window.getShell(), new NOVECOWizard(window.getShell()));
		dialog.setPageSize(560,538);
		dialog.open();		
		return null;
	}

}
