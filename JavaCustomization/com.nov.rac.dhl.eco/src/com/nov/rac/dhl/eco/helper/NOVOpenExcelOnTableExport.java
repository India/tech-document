package com.nov.rac.dhl.eco.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.teamcenter.rac.aif.AIFShell;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.util.InterfaceShellListener;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.nov.quicksearch.export.ExportTableData;

public class NOVOpenExcelOnTableExport implements InterfaceShellListener
{
    private AIFTable m_table;
    private File m_excelFile;
    private static NOVOpenExcelOnTableExport m_instance;
    
    private NOVOpenExcelOnTableExport()
    {
        
    }
    
    public static NOVOpenExcelOnTableExport getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new NOVOpenExcelOnTableExport();
        }
        return m_instance;
    }
    
    public void exportTableDataToExcel(AIFTable table)
    {
        m_table = table;
        exportData();
    }
    
    private void exportData()
    {
        try
        {
            String sFileName = System.getProperty("user.home");
            sFileName += "\\" + "SearchResult.xls";
            m_excelFile = new File(sFileName);
            
            final FileOutputStream outStream = new FileOutputStream(m_excelFile);
            
            final HSSFWorkbook workBook = new HSSFWorkbook();
            final HSSFSheet sheet = workBook.createSheet();
            
            ExportTableData exp = new ExportTableData(m_table);
            exp.createHeader(sheet);
            exp.createBody(sheet);
            
            workBook.write(outStream);
            outStream.close();
            
            openExcelFile(m_excelFile.getAbsolutePath());
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void openExcelFile(String filePath)
    {
        AIFShell aifshell = new AIFShell("");
        aifshell.setCommand(new String[] { filePath });
        aifshell.setAction("O");
        aifshell.setMimeType("MSExcel");
        aifshell.registerListener(this);
        aifshell.start();
    }
    
    @Override
    public void shellCompleted(int arg0) throws Exception
    {
        m_excelFile.delete();
    }
}
