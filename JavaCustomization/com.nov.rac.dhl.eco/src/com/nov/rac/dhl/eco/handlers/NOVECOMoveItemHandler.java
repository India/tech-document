package com.nov.rac.dhl.eco.handlers;

import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.dhl.eco.util.components.ItemSearchDialog;
import com.noi.rac.dhl.eco.util.components.MultiSelectCRItemDialog;
import com.noi.rac.dhl.eco.util.components.NOVCRItemSelectionPanel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetItemsPanel;
import com.noi.rac.dhl.eco.util.components.NOVECOsearchDialog;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;


/**
 * @author GuptaA2
 *
 */
public class NOVECOMoveItemHandler extends AbstractHandler 
{
	private TCComponent[] m_comp=null;
	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent executionevent)
			throws ExecutionException 
	{
		InterfaceAIFComponent[] selComps = null;
		selComps =  AIFUtility.getTargetComponents();
		if (selComps.length > 0)
		{
			m_comp = new TCComponent[selComps.length];
			for (int intx = 0; intx < selComps.length; intx++)
			{
				m_comp[intx] = (TCComponent) selComps[intx];
			}
		}
		
		//Checking selection
		if(selComps.length == 0)
		{
			MessageBox.post("No objects Selected " +
					"\nSelect a Item.","Error", MessageBox.ERROR);
			return null;
		}
		
		boolean dhItemCreationFlag = false;
		dhItemCreationFlag = NOVECOHelper.isDHGroup();
		
		if (!dhItemCreationFlag) 
		{
			MessageBox.post("Not Valid operation for logged in group","Error", MessageBox.ERROR);
			return null;
		}
		
		Shell parent = Display.getDefault().getActiveShell();
		
		try 
		{
			NOVECOsearchDialog searchDlg = new NOVECOsearchDialog(parent);
			searchDlg.setCompItem(m_comp);
			searchDlg.open();
			
		} catch (TCException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
