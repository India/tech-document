package com.nov.rac.dhl.eco.helper;

public class NOVECOConstants
{
    public static final String ChangeCatagory_LOV = "Change_Category_LOV";
    public static final String ReasonCodeDoc_LOV = "Reason_Code_Documentation";
    public static final String ReasonCodeGenReq_LOV = "Reason_Code_Gen_Req";
    public static final String ReasonCodeBitZip_LOV = "Reason_Code_Lane_Bitzip";
    public static final String ReasonCodeLane1_LOV = "Reason_Code_Lane1_Req";
    public static final String ReasonCodeLane2_LOV = "Reason_Code_Lane2_Req";
    public static final String ReasonCodeLane3_LOV = "Reason_Code_Lane3_Req";
    public static final String ReasonCodeLane4_LOV = "Reason_Code_Lane4_Req";
    public static final String ReasonCodeLifecycle_LOV = "Reason_Code_Lifecycle";
    public static final String ReasonCodeItemCreation_LOV = "Reason_Code_Item_Creation";
    public static final String ReasonCodeItemRevision_LOV = "Reason_Code_Item_Revision";
    public static final String GroupNames_LOV = "Group Names";
    public static final String ECNAvailability_LOV = "ECNAvailability";
    public static final String ECODISPOSITION_LOV = "Nov4_ECO_Disposition";//4474
    public static final String DHL_ECR_Groups = "DHL_ECR_Groups";
    public static final String ECR_Groups_base = "ECR_Groups_";
    public static final short Item_CheckedOut = 1;
    public static final short ItemRev_CheckedOut = 2;
    public static final short SecObjects_CheckedOut = 3;
    public static final short RDD_DOCRev_CheckedOut = 4;
    public static final short RDD_DOCRev_SecObjects_CheckedOut = 5;
    public static final short AllObjects_CheckedIn = 0;
    public static final String ProductLine_LOV = "Nov4_DHL_ProductLines";
    public static final String ECOCLASSIFICATION_LOV = "Nov4_ECO_Classification";//-4471
}
