package com.noi.rac.dhl.eco.util.components;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;

public class ECRTargetsTable extends JTreeTable
{
    private static final long               serialVersionUID = 1L;
    private NOVECRTreeTableModel            treeTableModel;
    public Set<TCComponent>                 targetItems;
    public Set<String>                      strTargetItemIds;
    public Vector<TCComponent>              targetCRs;
    public Vector<TCComponent>              removedCRs;
    public Map<String, Vector<TCComponent>> itemRevCRMap;
    private TCSession                       session;	//4686

    public ECRTargetsTable(TCSession session)
    {
        super(session);
        this.session=session;//4686
        targetItems = new HashSet<TCComponent>();
        strTargetItemIds = new HashSet<String>();
        targetCRs = new Vector<TCComponent>();
        removedCRs = new Vector<TCComponent>();
        itemRevCRMap = new HashMap<String, Vector<TCComponent>>();
        String[] columnNames =
        { "Item ID", "Name", "Description", "Type", "Category", "Revision",
                "Lifecycle" };
        treeTableModel = new NOVECRTreeTableModel(columnNames);
        setModel(treeTableModel);
        for ( int i = 0; i < columnNames.length; i++ )
        {
            TableColumn col = new TableColumn(i);
            addColumn(col);
        }
        getTree().setCellRenderer(new TargetIdRenderer(this));
        getColumnModel().getColumn(1).setCellRenderer(new RootCellRenderer());
        getColumnModel().getColumn(2).setCellRenderer(new RootCellRenderer());
        getColumnModel().getColumn(3).setCellRenderer(new RootCellRenderer());
        getColumnModel().getColumn(4).setCellRenderer(new RootCellRenderer());
        getColumnModel().getColumn(5).setCellRenderer(new RootCellRenderer());
        getColumnModel().getColumn(6).setCellRenderer(new RootCellRenderer());
        
      //4686-start
        setAutoResizeMode(JTreeTable.AUTO_RESIZE_ALL_COLUMNS);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent mouseEvt) {
				
				sendECRTargetsToMyTc(mouseEvt);
					
			}
				
			});//4686-end
    }
  //4686-start
    private void sendECRTargetsToMyTc(java.awt.event.MouseEvent mouseEvt)
    {
    	Object source = mouseEvt.getSource();
    	TreeTableNode[] treeNode = ((ECRTargetsTable) source).getSelectedNodes();
    	if (treeNode.length == 1)
    	{
    		if (treeNode[0] instanceof NovTreeTableNode)
    		{
    			if (!((TreeTableNode) treeNode[0].getParent()).isRoot())
    			{
    				return;
    			}
    		}
    	}
    	int selRow = ((ECRTargetsTable) source).getSelectedRow();

    		if (SwingUtilities.isRightMouseButton(mouseEvt)	&& (mouseEvt.getClickCount() == 1) && selRow > 0 ) 
    		{
    			JPopupMenu popupMenu = new JPopupMenu();
    			JMenuItem menuItem = new JMenuItem();
    			String menuLabel = "<html><b><font=8 color=red>Send to My Teamcenter</font></b>";
    			menuItem.setText(menuLabel);
    			menuItem.addActionListener(new ActionListener()
    			{
    				public void actionPerformed(ActionEvent e)
    				{
    					int row = ECRTargetsTable.this.getSelectedRow();
    					TCComponent remTarget = ((NovTreeTableNode) ECRTargetsTable.this.getNodeForRow(row)).itemRev;
    				    
    					Object[] args = new Object[2];
    					args[0] = AIFUtility.getActiveDesktop();
    					args[1] = (InterfaceAIFComponent) remTarget; 
    					AbstractAIFCommand openCmd;
    					try {
    						openCmd = session.getOpenCommand(args);
    						openCmd.executeModal();
    					} catch (Exception e1) {
    						
    						e1.printStackTrace();
    					}
    				}

    			});
    			popupMenu.add(menuItem);
    			popupMenu.show((java.awt.Component) source,
    			mouseEvt.getX(), mouseEvt.getY());

    		}	
    	
    }//4686-end

    /*
     * public void removeTarget(TCComponentItemRevision nItemRev,TCComponentForm
     * nCrForm) { int childcnt =
     * ((TreeTableNode)treeTableModel.getRoot()).getChildCount(); for (int i =
     * 0; i < childcnt; i++) { NovTreeTableNode treenode =
     * (NovTreeTableNode)((TreeTableNode
     * )treeTableModel.getRoot()).getChildAt(i); if ((treenode.itemRev ==
     * nItemRev )&&(treenode.crForm == nCrForm)) { removeTarget(treenode);
     * break; } } }
     */
    /*
     * public void removeTarget(NovTreeTableNode nodeToRemove) {
     * TCComponentItemRevision nItemRev = nodeToRemove.itemRev; TCComponentForm
     * nCrForm = nodeToRemove.crForm; try { if (nCrForm!=null) { String crStr =
     * nItemRev
     * .getProperty("object_string")+"::"+nCrForm.getProperty("object_name");
     * removedCRs.add(nCrForm); itemRevCRMap.remove(crStr);
     * targetItems.remove(nItemRev);
     * strTargetItemIds.remove(nItemRev.getProperty("item_id"));
     * targetCRs.remove(nCrForm); } else { targetItems.remove(nItemRev);
     * strTargetItemIds.remove(nItemRev.getProperty("item_id")); } } catch
     * (TCException e) { e.printStackTrace(); }
     * ((TreeTableNode)treeTableModel.getRoot()).remove(nodeToRemove); }
     */
    public void addTarget(TCComponentItemRevision itemRev)
    {
        try
        {
            String itemtype = ((TCComponentItemRevision) itemRev)
                    .getTCProperty("object_type").toString();
            //if ( !itemtype.equalsIgnoreCase("Documents Revision") )
            {
                NovTreeTableNode node1 = new NovTreeTableNode(
                        (TCComponentItemRevision) itemRev);
                String RevId = itemRev.getProperty("current_revision_id");
                AIFComponentContext contextRDD[] = itemRev
                        .getRelated("RelatedDefiningDocument");
                for ( int i = 0; i < contextRDD.length; i++ )
                {
                	InterfaceAIFComponent infComp = contextRDD[i].getComponent();
    				if (infComp.getType().equals("Documents")) 
    				{
	                    TCComponentItem rddcomp = (TCComponentItem) contextRDD[i].getComponent();
	                    TCComponentItemRevision rddRev = NOVECOHelper
	                            .getItemRevisionofRevID(rddcomp, RevId);
	                    // if(enformPanel.Entargets.contains(rddRev))
	                    {
	                        NovTreeTableNode nodeRDD = new NovTreeTableNode(rddRev);
	                        node1.add(nodeRDD);
	                        targetItems.add(rddRev);
	                        strTargetItemIds.add(rddRev.getProperty("item_id"));
	                    }
    				}
                }
                //targetItems.add(itemRev);
                //strTargetItemIds.add(itemRev.getProperty("item_id"));
                treeTableModel.addRoot(node1);
                updateUI();
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    class NOVECRTreeTableModel extends TreeTableModel
    {
        private static final long serialVersionUID = 1L;
        private Vector<String>    columnNamesVec;

        public NOVECRTreeTableModel(String columnNames[])
        {
            super();
            columnNamesVec = new Vector<String>();
            TreeTableNode top = new TreeTableNode();
            setRoot(top);
            top.setModel(this);
            for ( int i = 0; i < columnNames.length; i++ )
            {
                columnNamesVec.addElement(columnNames[i]);
                modelIndexToProperty.add(columnNames[i]);
            }
        }

        public void addRoot(TreeTableNode root )
        {
            TreeTableNode top = (TreeTableNode) getRoot();
            top.add(root);
        }

        public TreeTableNode getRoot(int index )
        {
            if ( ((TreeTableNode) getRoot()).getChildCount() > 0 )
                return (TreeTableNode) ((TreeTableNode) getRoot())
                        .getChildAt(index);
            else
                return null;
        }

        public int getColumnCount()
        {
            return columnNamesVec.size();
        }

        public String getColumnName(int column )
        {
            return (String) columnNamesVec.elementAt(column);
        }

        public void removeNode(int row )
        {
            // getNodeForRow(row).remove(arg0);
            ((TreeTableNode) getRoot()).remove(getNodeForRow(row));
        }
    }

    public class NovTreeTableNode extends TreeTableNode
    {
        private static final long      serialVersionUID = 1L;
        public TCComponentItemRevision itemRev;

        public NovTreeTableNode(TCComponentItemRevision itemRev2)
        {
            itemRev = itemRev2;
        }

        @ Override
        public String getProperty(String name )
        {
            try
            {
                String retStr = "";
                if ( name.equalsIgnoreCase("ITEM ID") )
                {
                    if ( itemRev != null )
                    {
                        return itemRev.getProperty("item_id");
                    }
                    return "";
                }
                else if ( name.equalsIgnoreCase("Name") )
                {
                    return itemRev.getProperty("object_name") == null ? ""
                            : itemRev.getProperty("object_name").toString();
                }
                else if ( name.equalsIgnoreCase("Type") )
                {
                    return itemRev.getProperty("object_type") == null ? ""
                            : itemRev.getProperty("object_type").toString();
                }
                else if ( name.equalsIgnoreCase("Revision") )
                {
                    return itemRev.getProperty("current_revision_id") == null ? ""
                            : itemRev.getProperty("current_revision_id")
                                    .toString();
                }
                else if ( name.equalsIgnoreCase("Lifecycle") )
                {
                    AIFComponentContext[] compConxt = ((TCComponentItemRevision) itemRev)
                            .getRelated("IMAN_master_form");
                    if ( compConxt.length > 0 )
                    {
                        return compConxt[0].getComponent().getProperty(
                                "Lifecycle");
                    }
                    return retStr;
                }
                else if ( name.equalsIgnoreCase("Category") )
                {
                    if(itemRev.getProperty("object_type") == "Documents Revision")
                    {
                        AIFComponentContext[] compConxt1 = ((TCComponentItemRevision) itemRev)
                        .getRelated("IMAN_master_form");
                        if(compConxt1.length > 0)
                        {
                            return compConxt1[0].getComponent().getProperty(
                            "DocumentsCAT");
                        }
                        return retStr;
                    }
                    else
                        return "";
                }
                else if ( name.equalsIgnoreCase("Description") )
                {
                    return itemRev.getProperty("object_desc") == null ? ""
                            : itemRev.getProperty("object_desc").toString();
                }
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
            return "";
        }

        public String getRelation()
        {
            return " ";
        }

        public String getType()
        {
            return itemRev.getType();
        }
    }

    class TargetIdRenderer extends TreeTableTreeCellRenderer
    {
        private static final long serialVersionUID = 1L;

        public TargetIdRenderer(JTreeTable arg0)
        {
            super(arg0);
        }

        public Component getTreeCellRendererComponent(JTree tree ,
                Object value , boolean sel , boolean expanded , boolean leaf ,
                int row , boolean hasFocus )
        {
            // Select the current value
            try
            {
                super.getTreeCellRendererComponent(tree, value, sel, expanded,
                        leaf, row, hasFocus);
                if ( row == 0 )
                {
                    // setIcon(TCTypeRenderer.getTypeIcon(tcSession.getTypeComponent("Job"),
                    // "Job"));
                    setText("Target Items");
                }
                else
                {
                    setText(((NovTreeTableNode) getNodeForRow(row)).itemRev
                            .getProperty("item_id"));
                    setIcon(TCTypeRenderer
                            .getIcon(((NovTreeTableNode) getNodeForRow(row)).itemRev));
                }
            }
            catch ( Exception e )
            {
                System.out.println(e);
            }
            return this;
        }
    }

    class RootCellRenderer extends JLabel implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;

        public RootCellRenderer()
        {
            super();
        }

        public Component getTableCellRendererComponent(JTable table ,
                Object value , boolean isSelected , boolean hasFocus , int row ,
                int column )
        {
            if ( row == 0 )
            {
                setText("");
                setBackground(table.getBackground());
                setForeground(table.getForeground());
            }
            else
            {
                setText(value.toString());
                if ( isSelected )
                {
                    setOpaque(true);
                    setBackground(table.getSelectionBackground());
                    setForeground(table.getForeground());
                }
                else
                {
                    setBackground(table.getBackground());
                    setForeground(table.getForeground());
                }
            }
            return this;
        }
    }
}
