/*================================================================================
                             Copyright (c) 2010 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVBomInfoTableRenderer
 Package Name: com.noi.rac.en.form.compound.component
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         2010/01/02    NatarajS      File added in SVN   
 ================================================================================*/
package com.noi.rac.dhl.eco.util.components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class NOVDHLBomInfoTableRenderer extends DefaultTableCellRenderer 
{
	private static final long serialVersionUID = 1L;
	public Set<String> qtyChangeSet;
	public Set<String> itemAddedSet;
	public Set<String> itemRemovedSet;
	public Set<String> revChangeSet;
	
	public StrikeThroughRenderer strikeThrouRenderer ;
	
	public NOVDHLBomInfoTableRenderer(NOVDHLBomInfoPanel novEnBomInfoPanel)
	{
		itemAddedSet = novEnBomInfoPanel.itemAddedSet;
		itemRemovedSet = novEnBomInfoPanel.itemRemovedSet;
		revChangeSet = novEnBomInfoPanel.revChangeSet;
		strikeThrouRenderer = new StrikeThroughRenderer();
	}
	
    public Component getTableCellRendererComponent(JTable table ,
            Object value , boolean isSelected , boolean hasFocus , int row ,
            int column )
    {
        
    	Object itemIdObj = table.getValueAt(row, 1);
    	String slNoObj = (String)table.getValueAt(row, 0);
    	if (itemAddedSet.contains(itemIdObj)) 
    	{
			setBackground(Color.GREEN);
			return super.getTableCellRendererComponent(table, value, isSelected,
	                hasFocus, row, column);
		}
    	else if ((itemRemovedSet.contains(itemIdObj)||(revChangeSet.contains(itemIdObj)))&& slNoObj.trim().length()>0)
    	{
    		return strikeThrouRenderer.getTableCellRendererComponent(table, value, isSelected,
	                hasFocus, row, column);
    	}
    	else if ((revChangeSet.contains(itemIdObj))&& (slNoObj.trim().length()==0))
    	{
    		setBackground(Color.YELLOW);
			return super.getTableCellRendererComponent(table, value, isSelected,
	                hasFocus, row, column);
    	}
    	setBackground(table.getBackground());
        Component comp=super.getTableCellRendererComponent(table, value, isSelected,
                hasFocus, row, column);
        
        return comp;
    }
    
    class StrikeThroughRenderer extends DefaultTableCellRenderer
    {
    	private static final long serialVersionUID = 1L;

		public void paintComponent(Graphics g )
        {
            super.paintComponent(g);
            Graphics2D g2=(Graphics2D)g;
            g2.setColor(Color.RED);
            int midpoint = getHeight() / 2;
            g2.setStroke(new BasicStroke(2)); 
            g2.drawLine(0, midpoint, getWidth() - 1, midpoint);
        }
    }
}