package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.teamcenter.rac.util.Registry;

public class TableButtonRenderer extends JButton implements TableCellRenderer 
{
	private static final long serialVersionUID = 1L;
	JButton tblButton;
	Registry registry = Registry.getRegistry(this);
	public TableButtonRenderer(boolean isPlusButton)
	{
		tblButton = new JButton();
		tblButton.setMinimumSize(new Dimension(18 , 20));
		tblButton.setPreferredSize(new Dimension(18 , 20));
		tblButton.setMaximumSize(new Dimension(18 , 20));
		tblButton.setBackground(Color.WHITE);
		
		if (isPlusButton) 
		{
			ImageIcon plusIcon = registry.getImageIcon("plus.ICON");
			tblButton.setIcon(plusIcon);
			tblButton.setActionCommand("addRow");	
		}
		else
		{
			ImageIcon minusIcon = registry.getImageIcon("minus.ICON");
			tblButton.setIcon(minusIcon);
			tblButton.setActionCommand("removeRow");
		}
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) 
	{
		return tblButton;
	}

}
