package com.noi.rac.dhl.eco.masschange.operations;

import java.util.List;
import java.util.Map;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.masschange.utils.NOVRDChangeHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.deleteHelper.CreateRelationObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.soa.client.model.ErrorStack;

/**
 * @author tripathim
 * 
 */
public class NOVAddRDOperation extends AbstractMassRDChangeOperation
{
    private TCComponent m_addRDComp;
    
    public NOVAddRDOperation(MassRDChangeComposite massRDChangeComposite, TCComponent[] selectedComps, String mmrRevType)
    {
        super(massRDChangeComposite, selectedComps, mmrRevType);
        m_addRDComp = massRDChangeComposite.getReplacementRD();
    }
    
    @Override
    public void performRDEditOperation()
    {
        try
        {
            Map<TCComponent, String> compsWithRevIdMap = NOVRDChangeHelper.fillAllComponentsWithRevid(m_selectedComps,
                    m_mmrRevType);
            
            List<ErrorStack> errorsStack = NOVRDChangeHelper.reviseObject(compsWithRevIdMap);
            if (!(errorsStack.size() > 0))
            {
                addRD();
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void addRD() throws TCException
    {
        CreateRelationObjectHelper[] creRelationObjectHelperArr = NOVRDChangeHelper.getCreateRDRelationObjects(
                m_selectedComps, m_addRDComp);
        CreateObjectsSOAHelper.createRelationObjects(creRelationObjectHelperArr);
    }
    
}
