/**
 * 
 */
package com.noi.rac.dhl.eco.util.components;

/**
 * @author deys
 *
 */
public interface ISubject2 
{
	public void notifyObserver(Object obj);
	public void registerObserver(IObserver2 subs);
	public void removeObserver(IObserver2 subs);
}
