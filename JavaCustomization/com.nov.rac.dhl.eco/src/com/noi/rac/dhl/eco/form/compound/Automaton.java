/*
 * Created on Jul 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.noi.rac.dhl.eco.form.compound;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.noi.rac.dhl.eco.form.compound.masters._EngChangeObjectForm_;
import com.noi.rac.dhl.eco.form.compound.panels.ItemRevisionPanel;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.form.compound.util.ItemRevisionHistory;
import com.noi.rac.form.compound.masters.IMasterForm;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.commands.open.OpenFormDialog;
import com.teamcenter.rac.form.AbstractTCForm;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;
 
/**
 * @author hughests Preferences - Java - Code Style - Code Templates
 */
public class Automaton extends AbstractTCForm
{
    /**
	 * 
	 */
    private static final long serialVersionUID       = 1L;
    private Registry          appReg;
    TCComponentForm           masterForm;
    TCComponent               item;
    LinkedList                subForms               = new LinkedList();
    LinkedList                activeMasters          = new LinkedList();
    LinkedList                itemRevisions          = new LinkedList();
    LinkedList                itemRevForms           = new LinkedList();
    LinkedList                activeRevisionSubForms = new LinkedList();
    IMasterForm               theMaster;
    JPanel                    revisionHistoryPanel;
    JTabbedPane               automatonTabbePane     = new JTabbedPane();
    TCReservationService      reserserv;

    /**
     * @throws java.lang.Exception
     */
    public Automaton() throws Exception
    {
        super();
    }

    /**
     * @param arg0
     * @throws java.lang.Exception
     */
    public Automaton(TCComponentForm arg0) throws Exception
    {
        super(arg0);
        
        //Mohan : All NOVLogger statements are commented out because it is failing in CITRIX
        
        //com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Automation Start");
        
        item = null;
        masterForm = arg0;
        reserserv = masterForm.getSession().getReservationService();
        AIFComponentContext[] arr = masterForm.whereReferenced();
        for ( int i = 0; i < arr.length; i++ )
        {
            if ( arr[i].getComponent() instanceof TCComponentItem )
            {
                item = (TCComponent) arr[i].getComponent();
            }
            else if ( arr[i].getComponent() instanceof TCComponentForm )
            {
                subForms.add(arr[i].getComponent());
            }
        }
        if ( item != null )
        {
            AIFComponentContext[] children = item.getChildren();
            for ( int i = 0; i < children.length; i++ )
            {
                if ( children[i].getComponent() instanceof TCComponentForm
                        && (TCComponentForm) children[i].getComponent() == masterForm )
                    continue;
                else if ( children[i].getComponent() instanceof TCComponentForm )
                {
                    subForms.add(children[i].getComponent());
                }
                else if ( children[i].getComponent() instanceof TCComponentItemRevision )
                {
                    itemRevisions.add((TCComponentItemRevision) children[i]
                            .getComponent());
                }
            }
        }
        
        //com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("loadRevisionHistory");
        loadRevisionHistory();
        
        //com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("loadForm");
        loadForm();
        
        //com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("createTabbedPane");
        createTabbedPane(activeMasters);
        this.revalidate();
        this.repaint();
        
        //com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Automation End");
    }

    /**
     * create all the tab content
     * 
     * @param forms
     *            contain all the tab UI
     */
    private void createTabbedPane(LinkedList forms )
    {
        ResourceBundle rg = ResourceBundle
                .getBundle("com.noi.rac.dhl.eco.form.form_locale");
        Dimension maxComponentDimension = new Dimension(0 , 0);
        Registry reg = Registry.getRegistry(this);
        ImageIcon tipIcon = reg.getImageIcon("TipOfTheDay.ICON");
        for ( Iterator it = forms.iterator(); it.hasNext(); )
        {
            IMasterForm currentForm = (IMasterForm) it.next();
            if ( currentForm != null )
            {
                JComponent currentPanel = currentForm.getPanel();
                maxSize(maxComponentDimension, currentPanel.getPreferredSize());
            }
        }
        maxSize(maxComponentDimension, revisionHistoryPanel.getPreferredSize());
        Dimension realDimension = new Dimension((int) (maxComponentDimension
                .getWidth() + 10) ,
                (int) (maxComponentDimension.getHeight() + 20));
        int tabNumber = 0;
        for ( Iterator it = forms.iterator(); it.hasNext(); )
        {
            IMasterForm currentForm = (IMasterForm) it.next();
            tabNumber++;
            JComponent currentPanel = currentForm.getPanel();
            JPanel masterPannel = new JPanel(new VerticalLayout(0 , 0 , 0 , 10 ,
                    0));
            masterPannel.setBackground(new Color(225 , 225 , 225));
            masterPannel.setPreferredSize(realDimension);
            masterPannel.add("top.nobind.center.center", currentPanel);
            if ( currentPanel instanceof _EngChangeObjectForm_Panel )
            {
                ((_EngChangeObjectForm_Panel) currentPanel).ecautomatonTabbePane
                        .insertTab(currentForm.getPanelName() == null ? "tabNo."+ tabNumber : rg.getString(currentForm.getPanelName()),
                                currentForm.getTabIcon() == null ? tipIcon: currentForm.getTabIcon(),masterPannel,
                                currentForm.getTip() == null ? "Group extended information": currentForm.getTip(), 0);
                //((_EngChangeObjectForm_Panel) currentPanel).ecautomatonTabbePane.setPreferredSize(realDimension);
                ((_EngChangeObjectForm_Panel) currentPanel).ecautomatonTabbePane.setBackground(new Color(225 , 225 , 225));
                add("top.nobind.center.center",((_EngChangeObjectForm_Panel) currentPanel).ecautomatonTabbePane);
                ((_EngChangeObjectForm_Panel) currentPanel).ecautomatonTabbePane.setSelectedIndex(0);
                try
                {
                    ((_EngChangeObjectForm_Panel) currentPanel).setFormModifiableProperty(reserserv.isReserved(masterForm)&& masterForm.okToModify());//4907
                }
                catch ( TCException e )
                {
                    e.printStackTrace();
                }
            }
            else
            {
                automatonTabbePane.addTab(currentForm.getPanelName() == null ? "tabNo."+ tabNumber : rg.getString(currentForm.getPanelName()),
                                currentForm.getTabIcon() == null ? tipIcon: currentForm.getTabIcon(),
                                masterPannel,currentForm.getTip() == null ? "Group extended information": currentForm.getTip());
                automatonTabbePane.setPreferredSize(realDimension);
                automatonTabbePane.setBackground(new Color(225 , 225 , 225));
                add("top.nobind.center.center", automatonTabbePane);
            }
        }
    }

    /**
     * @see com.ugsolutions.rac.form.AbstractTCForm#loadForm()
     */
    public void loadForm() throws TCException
    {
        boolean isLocked = false;
        appReg = Registry.getRegistry(this);
        setLayout(new VerticalLayout(0 , 0 , 0 , 0 , 0));
        JPanel imagePanel = new JPanel(
                new PropertyLayout(0 , 0 , 0 , 0 , 0 , 0));
        // Create the other property fields of the form
        JLabel imageLabel = new JLabel();
        String type = masterForm.getType();
        // add imagelabel to imagePanel
        imageLabel.setIcon(appReg.getImageIcon(type + ".ICON"));
        imagePanel.add("1.1.center.center.resizable.resizable", imageLabel);
        add("top.nobind.center.center", imagePanel);
        IMasterForm theMasterForm = null;
        try
        {
            try
            {
                boolean isLocked1 = masterForm.getLogicalProperty("Locked");
                boolean isLocked2 = masterForm.getLogicalProperty("locked");
                if ( isLocked1 || isLocked2 ) isLocked = true;
            }
            catch ( Exception e )
            {
                isLocked = false;
            }
            // new method to instance the concrete master form
            theMasterForm = getConcreteMasterForm(masterForm);
            theMaster = theMasterForm;
            activeMasters.add(theMasterForm);
            if ( isLocked )
            {
                theMasterForm.lockForm(isLocked);
            }
        }
        catch ( Exception e )
        {
            System.out.println("Hey meet with BIG TIME Exceptions.......");
            e.printStackTrace();
        }
        for ( int i = 0; i < subForms.size(); i++ )
        {
            IMasterForm subForm = null;
            try
            {
                TCComponentForm ancForm = (TCComponentForm) subForms.get(i);
                // new method to instance the concrete master form
                subForm = getConcreteMasterForm(ancForm);
                activeMasters.add(subForm);
                if ( isLocked )
                {
                    subForm.lockForm(isLocked);
                }
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

    // create revision tab content
    /**
     * retrieve all the revisions history information and draw the UI
     * 
     * @throws TCException
     */
    private void loadRevisionHistory() throws TCException
    {
        revisionHistoryPanel = new JPanel(new GridLayout(itemRevisions.size() ,
                1));
        revisionHistoryPanel.setBackground(new Color(225 , 225 , 225));
        revisionHistoryPanel.setBorder(null);
        for ( int i = 0; i < itemRevisions.size(); i++ )
        {
            try
            {
                TCComponentItemRevision rev = (TCComponentItemRevision) itemRevisions
                        .get(i);
                ItemRevisionHistory revHist = new ItemRevisionHistory();
                revHist.creationDate = rev.getProperty("creation_date");
                revHist.dateReleased = rev.getProperty("date_released");
                revHist.revisionId = rev.getProperty("item_revision_id");
                revHist.name = rev.getProperty("object_name");
                revHist.description = rev.getProperty("object_desc");
                JPanel aPanel = new JPanel();
                aPanel.setBackground(new Color(225 , 225 , 225));
                aPanel.setBorder(new javax.swing.border.LineBorder(
                        new java.awt.Color(0 , 0 , 0)));
                ItemRevisionPanel irp = new ItemRevisionPanel();
                irp.createUI(revHist);
                revisionHistoryPanel.add(aPanel);
                AIFComponentContext[] cc = rev
                        .getRelated("IMAN_master_form_rev");
                if ( cc.length == 1 )
                {
                    TCComponentForm revForm = (TCComponentForm) cc[0]
                            .getComponent();
                    itemRevForms.add(revHist);
                    AIFComponentContext[] arr = revForm.whereReferenced();
                    aPanel.setLayout(new GridLayout(arr.length , 1));
                    aPanel.add(irp);
                    for ( int j = 0; j < arr.length; j++ )
                    {
                        if ( arr[j].getComponent() instanceof TCComponentForm )
                        {
                            TCComponentForm ancForm = (TCComponentForm) arr[j]
                                    .getComponent();
                            revHist.addSubForm(ancForm);
                            IMasterForm subForm = getConcreteMasterForm(ancForm);
                            activeRevisionSubForms.add(subForm);
                            if ( !revHist.dateReleased.equals("") )
                            {
                                subForm.lockForm(true);
                            }
                            aPanel.add(subForm.getPanel());
                        }
                    }
                }
                else
                {
                    aPanel.setLayout(new GridLayout(1 , 1));
                    aPanel.add(irp);
                }
            }
            catch ( Exception e )
            {
                System.out
                        .println("Error getting or attaching Item Revision panels... "
                                + e);
                e.printStackTrace();
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.ugsolutions.rac.form.AbstractTCForm#saveForm()
     */
    public void saveForm()
    {
        for ( int i = 0; i < activeMasters.size(); i++ )
            ((IMasterForm) activeMasters.get(i)).saveForm();
        for ( int i = 0; i < activeRevisionSubForms.size(); i++ )
            ((IMasterForm) activeRevisionSubForms.get(i)).saveForm();
        
    }

    private void maxSize(Dimension cmax , Dimension omax )
    {
        if ( cmax.height < omax.height ) cmax.height = omax.height;
        if ( cmax.width < omax.width ) cmax.width = omax.width;
    }

    private IMasterForm getConcreteMasterForm(TCComponentForm paraForm )
    {
        IMasterForm theMasterForm = null;
        try
        {
            if ( appReg == null ) appReg = Registry.getRegistry(this);
            // appReg = Registry.getRegistry(this, "Automaton");
            String itemType = paraForm.getType();
            theMasterForm = (IMasterForm) appReg.newInstanceFor(itemType,
                    paraForm);
            return theMasterForm;
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
        return theMasterForm;
    }

    public void setFormReadWrite()
    {
        theMaster.setDisplayProperties();
        for ( int i = 0; i < activeMasters.size(); i++ )
            ((IMasterForm) activeMasters.get(i)).setDisplayProperties();
        for ( int i = 0; i < activeRevisionSubForms.size(); i++ )
            ((IMasterForm) activeRevisionSubForms.get(i))
                    .setDisplayProperties();
    }

    public boolean isFormSavable()
    {
        for ( int i = 0; i < activeMasters.size(); i++ )
        {
            if ( ((IMasterForm) activeMasters.get(i)) instanceof _EngChangeObjectForm_ )
            {
                return (((_EngChangeObjectForm_) activeMasters.get(i))
                        .isformSavable());
            }
        }
        return true;
    }

    // Methods added for making forms editable as user stands on it
    public void displayDialog(OpenFormDialog openformdialog , boolean flag )
    {
         
        try
        {
            if ( !reserserv.isReserved(masterForm) )
                if ( !(masterForm.getProperty("is_modifiable").length() == 0) ) // isModifiable(arg0))
                    reserserv.reserve(masterForm);
        }
        catch ( TCException e )
        {
            e.printStackTrace();
        }
        openformdialog.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent e )
            {
                try
                {
                    TCReservationService reserserv = masterForm.getSession()
                            .getReservationService();
                    if ( reserserv.isReserved(masterForm) )
                    {
                        reserserv.unreserve(masterForm);
                    }
                }
                catch ( TCException tc )
                {
                    System.out.println(tc);
                }
                super.windowClosed(e);
            }
        });
        super.displayDialog(openformdialog, flag);
    }

    public void setFormReadOnly()
    {
        // setModifiableComponents(this, false);
    }

    public void postLoadForm(Object obj )
    {
    }
}
