package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;

import com.noi.rac.commands.newdataset.NewDatasetDialog;
import com.noi.util.components.NOVJComboBox;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.utilities.common.NOVDataManagementServiceInfo;
import com.nov.rac.utilities.common.PreferenceUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTableLine;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentFormType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVECOInitialPanel extends JPanel implements ComponentListener, ISubject
{
    private static final long serialVersionUID = 1L;
    protected JPanel requiredPanel, supportFilesPanel;
    boolean required = true;
    private TCSession tcSession;
    public Map<String, TCComponentForm> formCompMap;
    private JButton addButton, removeButton;
    private NOVJComboBox workFlow;
    private NOVJComboBox typeOfChange;
    private NOVJComboBox reasonCode;
    private NOVJComboBox changeCat;
    private NOVJComboBox productLine;
    private NOVJComboBox m_panelECOClassification;// TCDECREL-4471
    private NOVJComboBox group;
    private TCComponentForm changeForm;
    private TCPreferenceService prefService;
    private Registry registry;
    private JPanel mainPanel;
    public boolean isChangeTypeAltered = false;
    private List<IObserver> observers = new ArrayList<IObserver>();
    private String typeOfChangePrefName;
    private String workflowPrefName;
    private String changeCatPref;
    
    public NOVECOInitialPanel()
    {
        tcSession = (TCSession) AIFUtility.getDefaultSession();
        prefService = tcSession.getPreferenceService();
        registry = Registry.getRegistry(this);
        addComponentListener(this);
        formCompMap = new HashMap<String, TCComponentForm>();
        initUI();
    }
    
    private void initUI()
    {
        // setLayout(new PropertyLayout());
        
        requiredPanel = new JPanel(new PropertyLayout(10, 10, 0, 0, 0, 0));
        supportFilesPanel = new JPanel(new PropertyLayout(10, 10, 0, 0, 0, 0));
        
        JLabel typeofchangeL = new JLabel("Type of Change:", JLabel.LEFT);
        JLabel workflowL = new JLabel("WorkFlow:", JLabel.LEFT);
        JLabel changecatL = new JLabel("Change Category:", JLabel.LEFT);
        JLabel reasoncodeL = new JLabel("Reason Code:", JLabel.LEFT);
        JLabel productLineL = new JLabel("Product Line:", JLabel.LEFT);
        
        JLabel ecoclassificationL = new JLabel(registry.getString("ECOClassificationLable.Name"), JLabel.LEFT);// TCDECREL-4471
        JLabel groupsL = new JLabel("Group:", JLabel.LEFT);
        
        JLabel suppFilesL = new JLabel("Supporting files ");
        suppFilesL.setFont(new Font("Arial", 1, 12));
        
        // typeOfChange = new NOVJComboBox(new String[]
        // {"","General","Lifecycle"});
        typeOfChange = new NOVJComboBox();
        workFlow = new NOVJComboBox();
        changeCat = new NOVJComboBox();
        reasonCode = new NOVJComboBox();
        productLine = new NOVJComboBox();
        m_panelECOClassification = new NOVJComboBox();// TCDECREL-4471
        
        populateTypeOfChange();
        typeOfChange.setRequired(true);
        typeOfChange.setPreferredSize(new Dimension(220, 21));
        typeOfChange.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                NOVJComboBox typChBox = (NOVJComboBox) itemEvt.getSource();
                /*
                 * Rakesh:100212 - Construct the Workflow LOV name from
                 * typeOfChange and Group - Starts here
                 */
                /*
                 * if (typChBox.getSelectedItem().equals("General")) { String[]
                 * workflows =
                 * prefService.getStringArray(TCPreferenceService.TC_preference_all
                 * , "ECO_General_Change_Workflows");
                 * populateWorkflowlist(workflows); isChangeTypeAltered = true;
                 * } else if(typChBox.getSelectedItem().equals("Lifecycle")) {
                 * String[] workflows =
                 * prefService.getStringArray(TCPreferenceService
                 * .TC_preference_all, "ECO_Lifecycle_Change_Workflows");
                 * populateWorkflowlist(workflows); isChangeTypeAltered = true;
                 * }
                 */
                String typeOfChange = typChBox.getSelectedItem().toString();
                if (!typeOfChange.isEmpty())
                {
                    workflowPrefName = "ECO_Workflows" + "." + tcSession.getCurrentGroup().toString().substring(0, 2)
                            + "." + typeOfChange;
                    String[] workflows = prefService.getStringArray(TCPreferenceService.TC_preference_all,
                            workflowPrefName);
                    populateWorkflowlist(workflows);
                    isChangeTypeAltered = true;
                }
                /*
                 * Rakesh:100212 - Construct the Workflow LOV name from
                 * typeOfChange and Group - Ends here
                 */
                else
                {
                    workFlow.removeAllItems();
                    changeCat.removeAllItems();
                    reasonCode.removeAllItems();
                    isChangeTypeAltered = true;
                }
                NOVECOInitialPanel.this.notifyObserver();
            }
        });
        
        // workFlow = new NOVJComboBox();
        workFlow.setRequired(true);
        workFlow.setPreferredSize(new Dimension(220, 21));
        workFlow.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent arg0)
            {
                /*
                 * Rakesh:100212 - Construct the Change Category LOV name from
                 * Workflow and Group - Starts here
                 */
                if (!getWorkFlow().isEmpty())
                {
                    // String changeCatPref = "ECO_ChangeCategory." +
                    // getWorkFlow();
                    changeCatPref = workflowPrefName + "." + getWorkFlow() + "." + "category";
                    String[] changeCatList = prefService.getStringArray(TCPreferenceService.TC_preference_all,
                            changeCatPref);
                    populateChangeCategory(changeCatList);
                }
                else
                {
                    changeCat.removeAllItems();
                    changeCat.addItem("");
                }
                /*
                 * if(getWorkFlow().equalsIgnoreCase("RH Document Release")) {
                 * if (changeCat.getItemCount()>0) { changeCat.removeAllItems();
                 * reasonCode.removeAllItems(); } changeCat.addItem("");
                 * changeCat.addItem("Documentation"); } else
                 * if(getWorkFlow().equalsIgnoreCase
                 * ("RH CMT Release")||getWorkFlow
                 * ().equalsIgnoreCase("RH Lifecycle Process")) { if
                 * (changeCat.getItemCount()>1) { changeCat.removeAllItems();
                 * reasonCode.removeAllItems(); } changeCat.addItem("");
                 * changeCat.addItem("General Request"); } else
                 * if(getWorkFlow().equalsIgnoreCase("RH Type 1 Release")) { if
                 * (changeCat.getItemCount()>1) { changeCat.removeAllItems();
                 * reasonCode.removeAllItems(); } changeCat.addItem("");
                 * changeCat.addItem("General Request");
                 * changeCat.addItem("Lane - Bitzip");
                 * changeCat.addItem("Lane 1 Request/SBR"); } else
                 * if(getWorkFlow().equalsIgnoreCase("RH Type 2 Release")) { if
                 * (changeCat.getItemCount()>1) { changeCat.removeAllItems();
                 * reasonCode.removeAllItems(); } changeCat.addItem("");
                 * changeCat.addItem("Lane - Bitzip");
                 * changeCat.addItem("Lane 2 Request"); } else
                 * if(getWorkFlow().equalsIgnoreCase("RH Type 3 Release")) { if
                 * (changeCat.getItemCount()>1) { changeCat.removeAllItems();
                 * reasonCode.removeAllItems(); } changeCat.addItem("");
                 * changeCat.addItem("Lane - Bitzip");
                 * changeCat.addItem("Lane 3 Request");
                 * changeCat.addItem("Lane 4 Request"); } else
                 * if(getWorkFlow().equalsIgnoreCase
                 * ("RH Lifecycle Change Process"
                 * )||getWorkFlow().equalsIgnoreCase("RH Lifecycle Process")) {
                 * if (changeCat.getItemCount()>1) { changeCat.removeAllItems();
                 * reasonCode.removeAllItems(); } changeCat.addItem("");
                 * changeCat.addItem("Lifecycle Change"); } else {
                 * changeCat.addItem(""); }
                 */
                /*
                 * Rakesh:100212 - Construct the Change Category LOV name from
                 * Workflow and Group - Ends here
                 */
                NOVECOInitialPanel.this.notifyObserver();
            }
            
        });
        
        // String[] workflows =
        // prefService.getStringArray(TCPreferenceService.TC_preference_all,
        // "ECO_General_Change_Workflows");
        // populateWorkflowlist(workflows);
        
        // changeCat = new NOVJComboBox();
        changeCat.setRequired(true);
        changeCat.setPreferredSize(new Dimension(220, 21));
        
        // reasonCode = new NOVJComboBox();
        reasonCode.setRequired(true);
        reasonCode.setPreferredSize(new Dimension(220, 21));
        
        /***************** Rakesh - 10/11/2011 - DHL-ECO-208 development ***********************/
        /************* Disable Next button if mandatory fields are not filled ******************/
        reasonCode.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                NOVECOInitialPanel.this.notifyObserver();
            }
        });
        /***************************************************************************************/
        
        changeCat.addItemListener(new ItemListener()
        {
            
            public void itemStateChanged(ItemEvent e)
            {
                if (getChangeCategory().equalsIgnoreCase("Documentation"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeDoc_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("General Request"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeGenReq_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane - Bitzip")
                        && getWorkFlow().equalsIgnoreCase("RH Type 1 Release"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane1_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane - Bitzip")
                        && getWorkFlow().equalsIgnoreCase("RH Type 2 Release"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane2_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane - Bitzip")
                        && getWorkFlow().equalsIgnoreCase("RH Type 3 Release"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane3_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane 1 Request/SBR"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane1_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane 2 Request"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane2_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane 3 Request"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane3_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lane 4 Request"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLane4_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Lifecycle Change"))
                {
                    reasonCode.removeAllItems();
                    reasonCode.addItem("Lifecycle Change");
                }
                /*
                 * Rakesh:130212 - Load Reason code for new Groups 1B/9A -
                 * Starts here
                 */
                else if (getChangeCategory().equalsIgnoreCase("Lifecycle"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeLifecycle_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Item Revision"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeItemRevision_LOV, reasonCode);
                }
                else if (getChangeCategory().equalsIgnoreCase("Item Creation"))
                {
                    reasonCode.removeAllItems();
                    NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ReasonCodeItemCreation_LOV, reasonCode);
                }
                /*
                 * Rakesh:130212 - Load Reason code for new Groups 1B/9A - Ends
                 * here
                 */
                NOVECOInitialPanel.this.notifyObserver();
            }
        });
        
        // Rakesh - 27/12/2011 - DHL ECO 2.0 - Product Line field added
        // productLine = new NOVJComboBox();
        productLine.setRequired(true);
        productLine.setPreferredSize(new Dimension(220, 21));
        NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ProductLine_LOV, productLine);
        productLine.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                NOVECOInitialPanel.this.notifyObserver();
            }
        });
        // Rakesh - 27/12/2011 - DHL ECO 2.0 - Product Line field added
        
        group = new NOVJComboBox();
        group.setRequired(true);
        group.setPreferredSize(new Dimension(220, 21));
        
        /* Rakesh:060312 - Group list is driven by preference - Starts here */
        // NOVECOHelper.retriveLOVValues(tcSession,NOVECOConstants.DHL_ECR_Groups,
        // group);
        String groupPrefName = NOVECOConstants.ECR_Groups_base + tcSession.getCurrentGroup().toString().substring(0, 2);
        NOVECOHelper.retriveLOVValues(tcSession, groupPrefName, group);
        /* Rakesh:060312 - Group list is driven by preference - Ends here */

        group.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                NOVECOInitialPanel.this.notifyObserver();
            }
        });
        // TCDECREL-4471-start
        
        m_panelECOClassification.setRequired(true);
        m_panelECOClassification.setPreferredSize(new Dimension(220, 21));
        NOVECOHelper.retriveLOVValues(tcSession, NOVECOConstants.ECOCLASSIFICATION_LOV, m_panelECOClassification);
        invokeClassificationListener();// TCDECREL-4471-End
        
        requiredPanel.add("1.1.left.center", productLineL);
        requiredPanel.add("1.2.right.center", productLine);
        requiredPanel.add("2.1.left.center", groupsL);
        requiredPanel.add("2.2.right.center", group);
        requiredPanel.add("3.1.left.center", typeofchangeL);
        requiredPanel.add("3.2.right.center", typeOfChange);
        requiredPanel.add("4.1.left.center", workflowL);
        requiredPanel.add("4.2.right.center", workFlow);
        requiredPanel.add("5.1.left.center", changecatL);
        requiredPanel.add("5.2.right.center", changeCat);
        requiredPanel.add("6.1.left.center", reasoncodeL);
        requiredPanel.add("6.2.right.center", reasonCode);
        requiredPanel.add("7.1.left.center", ecoclassificationL);// TCDECREL-4471
        requiredPanel.add("7.2.right.center", m_panelECOClassification);// TCDECREL-4471
        
        String[] columns = { "object_name", "object_type" };
        final TCTable table = new TCTable();
        table.setColumnNames(tcSession, columns, "Dataset");
        table.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent Evt)
            {
                TCTable table = (TCTable) Evt.getSource();
                int index = table.getSelectedRow();
                if (index != -1)
                {
                    removeButton.setEnabled(true);
                }
            }
        });
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane listScrollPane = new JScrollPane(table);
        listScrollPane.setPreferredSize(new Dimension(264, 125));
        addButton = new JButton();
        addButton.setIcon(registry.getImageIcon("add.ICON"));
        removeButton = new JButton();
        removeButton.setIcon(registry.getImageIcon("remove.ICON"));
        
        addButton.addActionListener(new ActionAdapter()
        {
            public void actionPerformed(ActionEvent e)
            {
                TCComponentForm form = getChangeForm(false);
                NewDatasetDialog dlg = new NewDatasetDialog(null, AIFUtility.getCurrentApplication(), form, false);
                dlg.setModal(true);
                if (Toolkit.getDefaultToolkit().getScreenResolution() > 96)
                {
                    dlg.setSize(600, 220);
                }
                else
                {
                    dlg.setSize(500, 220);
                }
                dlg.setDsName("Attachment");
                dlg.setVisible(true);
                if (dlg.getDataset() != null)
                {
                    Vector<TCComponent> v = new Vector<TCComponent>();
                    v.add(dlg.getDataset());
                    table.addRow(v);
                }
            }
        });
        
        removeButton.addActionListener(new ActionAdapter()
        {
            public void actionPerformed(ActionEvent e)
            {
                int index = table.getSelectedRow();
                if (index != -1)
                {
                    TCTableLine selObject = (TCTableLine) table.getRowLine(index);
                    TCComponent selComp = (TCComponent) selObject.getComponent();
                    try
                    {
                        table.removeRow(index);
                        selComp.delete();
                    }
                    catch (TCException e1)
                    {
                        e1.printStackTrace();
                    }
                }
            }
        });
        JPanel rtPanel = new JPanel(new BorderLayout());
        rtPanel.add(addButton, BorderLayout.NORTH);
        rtPanel.add(removeButton, BorderLayout.CENTER);
        supportFilesPanel.setBorder(new TitledBorder("Supporting files"));
        supportFilesPanel.add("1.1.center.center", listScrollPane);
        supportFilesPanel.add("1.2.center.center", rtPanel);
        requiredPanel.setBorder(new TitledBorder(""));
        
        mainPanel = new JPanel();
        mainPanel.setLayout(new VerticalLayout(0));
        mainPanel.add("top.nobind.center.center", requiredPanel);
        mainPanel.add("top.nobind.center.center", supportFilesPanel);
        this.add(mainPanel);
    }
    
    private void populateTypeOfChange()
    {
        /* Reset all fields */
        typeOfChange.removeAllItems();
        workFlow.removeAllItems();
        changeCat.removeAllItems();
        reasonCode.removeAllItems();
        
        /* Populate Change Type */
        typeOfChangePrefName = "ECO_Workflows" + "." + tcSession.getCurrentGroup().toString().substring(0, 2);
        String[] typeOfChangeList = prefService.getStringArray(TCPreferenceService.TC_preference_all,
                typeOfChangePrefName);
        typeOfChange.addItem("");
        for (int i = 0; i < typeOfChangeList.length; i++)
        {
            typeOfChange.addItem(typeOfChangeList[i]);
        }
    }
    
    private void populateWorkflowlist(String[] workflowTemplates)
    {
        workFlow.removeAllItems();
        changeCat.removeAllItems();
        reasonCode.removeAllItems();
        workFlow.addItem("");
        for (int i = 0; i < workflowTemplates.length; i++)
        {
            workFlow.addItem(workflowTemplates[i]);
        }
    }
    
    /* Rakesh:130212 - Populate Change Category from LOVs - Start here */
    private void populateChangeCategory(String[] changeCatList)
    {
        changeCat.removeAllItems();
        reasonCode.removeAllItems();
        changeCat.addItem("");
        for (int i = 0; i < changeCatList.length; i++)
        {
            changeCat.addItem(changeCatList[i]);
        }
    }
    
    /* Rakesh:130212 - Populate Change Category from LOVs - Ends here */

    public TCComponentForm getChangeForm(boolean isCanceled)
    {
        if (changeForm == null && !isCanceled)
        {
            try
            {
                TCComponentFormType formType = (TCComponentFormType) ((TCSession) AIFUtility.getDefaultSession())
                        .getTypeComponent("Form");
                
                NOVDataManagementServiceInfo novGetNextValueService = new NOVDataManagementServiceInfo();
         		
         		setDatatoService(novGetNextValueService, "ECO_SEQ",  "",  ""); 
         		     		
                /*
                TCUserService us = tcSession.getUserService();
                Object obj[] = new Object[1];
                obj[0] = "ect";
                Object comp = us.call("NATOIL_GenerateECOName", obj);
                
                if (comp != null)
                {
                    changeForm = formType.create(String.valueOf(comp), "_DHL_EngChangeForm_", "_DHL_EngChangeForm_");
                }
                else
                {
                    System.err.println("User service NATOIL_UpdateECOName returned null");
                }
                */
                String strECOName = novGetNextValueService.getNextValue();
                
                if (!strECOName.isEmpty())
                {
                    changeForm = formType.create(strECOName, "_DHL_EngChangeForm_", "_DHL_EngChangeForm_");
                }
                else
                {
                    System.err.println("ECO name SOA returned null");
                }
                
                
                
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        return changeForm;
    }
    
    /**
     * Set Input data to the service
     */   
    
    void setDatatoService(NOVDataManagementServiceInfo novGetNextValueService,String strCounterName,
 		   													String strPrefixExp, String strPostfixExp)
    {
 		int theScope = TCPreferenceService.TC_preference_site;	

 		novGetNextValueService.setContext("NAME");
 		novGetNextValueService.setCounterName(strCounterName);
 		novGetNextValueService.setDefaultCounterStartValue(1);
 		novGetNextValueService.setPrefix(strPrefixExp);
 		novGetNextValueService.setPostfix(strPostfixExp);
		novGetNextValueService.setAlternateIDContext(new String[] {""});
		
    }
    
    
    public String getChangeCategory()
    {
        return changeCat.getSelectedItem() == null ? "" : changeCat.getSelectedItem().toString();
    }
    
    public String getReasonCode()
    {
        return reasonCode.getSelectedItem() == null ? "" : reasonCode.getSelectedItem().toString();
    }
    
    // Rakesh - 27/12/2011 - DHL ECO 2.0 Product line field
    public String getProductLine()
    {
        return productLine.getSelectedItem() == null ? "" : productLine.getSelectedItem().toString();
    }
    
    // TCDECREL-4471
    public String getECOClassification()
    {
        
        return m_panelECOClassification.getSelectedItem() == null ? "" : m_panelECOClassification.getSelectedItem()
                .toString();
    }
    
    public String getWorkFlow()
    {
        return workFlow.getSelectedItem() == null ? "" : workFlow.getSelectedItem().toString();
    }
    
    public String getTypeOfChange()
    {
        return typeOfChange.getSelectedItem() == null ? "" : typeOfChange.getSelectedItem().toString();
    }
    
    public String getGroup()
    {
        return group.getSelectedItem() == null ? "" : group.getSelectedItem().toString();
    }
    
    public void componentHidden(ComponentEvent e)
    {
    }
    
    public void componentMoved(ComponentEvent e)
    {
    }
    
    public void componentResized(ComponentEvent e)
    {
        Dimension dim = NOVECOInitialPanel.this.getSize();
        int height = dim.height;
        int width = dim.width;
        mainPanel.setLocation(width / 2 - 150, height / 2 - 150);
    }
    
    public void componentShown(ComponentEvent e)
    {
    }
    
    /***************** Rakesh - 10/11/2011 - DHL-ECO-208 development ***********************/
    /******************* Validate if mandatory fields are not filled ***********************/
    public boolean isMandatoryValuesFilled()
    {
        if ((!(getTypeOfChange().toString().length() > 0)) || (!(getWorkFlow().toString().length() > 0))
                || (!(getChangeCategory().toString().length() > 0)) || (!(getReasonCode().toString().length() > 0))
                || (!(getProductLine().toString().length() > 0)) || (!(getECOClassification().toString().length() > 0))
                || (!(getGroup().toString().length() > 0)))
        {
            return false;
        }
        return true;
    }
    
    /***************************************************************************************/
    
    /***************** Rakesh - 10/11/2011 - DHL-ECO-208 development ***********************/
    /*************************** Implement Subject/Observer *******************************/
    
    public void notifyObserver()
    {
        Iterator<IObserver> i = observers.iterator();
        while (i.hasNext())
        {
            IObserver o = (IObserver) i.next();
            o.update();
        }
    }
    
    public void registerObserver(IObserver ob)
    {
        observers.add(ob);
    }
    
    public void removeObserver(IObserver ob)
    {
        observers.remove(ob);
    }
    
    // TCDECREL-4471-ECO Classification combobox listener
    private void invokeClassificationListener()
    {
        
        m_panelECOClassification.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                NOVECOInitialPanel.this.notifyObserver();
            }
        });
        
    }
    /***************************************************************************************/
}
