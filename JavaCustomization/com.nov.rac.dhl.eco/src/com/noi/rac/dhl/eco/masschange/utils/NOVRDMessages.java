/**
 * 
 */
package com.noi.rac.dhl.eco.masschange.utils;

/**
 * @author kabades
 *
 */

import java.util.ResourceBundle;
import com.teamcenter.rac.util.Registry;

public class NOVRDMessages
{
    
    public NOVRDMessages()
    {
    }
    
    public static String getString(String s)
    {
        return RESOURCE_BUNDLE.getString(s);
    }
    
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle("com.noi.rac.dhl.eco.masschange.dialogs.dialogs_locale");
    
    public static Registry getRegistry()
    {
        Registry registry = Registry.getRegistry("com.noi.rac.dhl.eco.util.components.components");
        return registry;
    }
    
}
