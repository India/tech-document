package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class TableButtonEditor extends AbstractCellEditor implements
		TableCellEditor, ActionListener 
{
	private static final long serialVersionUID = 1L;
	JButton tblButton;
	TCTable srcTable ;
	TCTable trgTable ;
	Registry registry ;
	Vector<TCComponentForm> addedCRsVector;
	Map<String, TCComponentForm> formsMap;
	NOVCRSelectionPanel crSelectionPanel;
	NOVCRManagementPanel crManagementPanel;
	
	/* Constructor for CR Selection from ECO creation process */
	public TableButtonEditor(NOVCRSelectionPanel crPanel,TCTable sourcetable,TCTable targettable) 
	{
		registry = Registry.getRegistry(this);
		crSelectionPanel = crPanel;
		formsMap = crPanel.formCompMap;
		addedCRsVector = crPanel.addedCRsVec;		
		srcTable = sourcetable;
		trgTable = targettable;
		tblButton = new JButton();
		tblButton.setMinimumSize(new Dimension(18 , 20));
		tblButton.setPreferredSize(new Dimension(18 , 20));
		tblButton.setMaximumSize(new Dimension(18 , 20));
		tblButton.setBackground(Color.WHITE);
		tblButton.addActionListener(this);
		if (sourcetable != null) 
		{
			ImageIcon plusIcon = registry.getImageIcon("plus.ICON");
			tblButton.setIcon(plusIcon);
			tblButton.setActionCommand("addRow");	
		}
		else
		{
			ImageIcon minusIcon = registry.getImageIcon("minus.ICON");
			tblButton.setIcon(minusIcon);
			tblButton.setActionCommand("removeRow");
		}
		
	}	
	
	/* Constructor for CR Selection from CR Management process */
	public TableButtonEditor(NOVCRManagementPanel crPanel,TCTable sourcetable,TCTable targettable) 
	{
		registry = Registry.getRegistry(this);
		crManagementPanel = crPanel;
		formsMap = crPanel.formCompMap;
		addedCRsVector = crPanel.addedCRsVec;		
		srcTable = sourcetable;
		trgTable = targettable;
		tblButton = new JButton();
		tblButton.setMinimumSize(new Dimension(18 , 20));
		tblButton.setPreferredSize(new Dimension(18 , 20));
		tblButton.setMaximumSize(new Dimension(18 , 20));
		tblButton.setBackground(Color.WHITE);
		tblButton.addActionListener(this);
		if (sourcetable != null) 
		{
			ImageIcon plusIcon = registry.getImageIcon("plus.ICON");
			tblButton.setIcon(plusIcon);
			tblButton.setActionCommand("addRow");	
		}
		else
		{
			ImageIcon minusIcon = registry.getImageIcon("minus.ICON");
			tblButton.setIcon(minusIcon);
			tblButton.setActionCommand("removeRow");
		}
		
	}	

	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) 
	{
		return tblButton;
	}

	public Object getCellEditorValue() 
	{
		return null;
	}

	public void actionPerformed(ActionEvent e) 
	{
	
		String actCmd = e.getActionCommand();
		if (actCmd.equals("addRow")) 
		{
			if (srcTable != null  && trgTable!=null) 
			{
				int selRow = srcTable.getSelectedRow();
				String crName = (String)srcTable.getValueAt(selRow, 1);
				if (isValidCR(crName)) 
				{
					Object[] rowdata = srcTable.getRowData(selRow);
					if(crManagementPanel != null) //Triggered from CR Management panel
					{
						if(rowdata[6].toString().trim().length()>0)
						{
							MessageBox.post("Selected CR cannot be closed since it has reference ECOs.", "Info", MessageBox.INFORMATION);
							return;
						}
					}
					addedCRsVector.add(formsMap.get(crName));
					trgTable.addRow(rowdata);
					trgTable.repaint();
					if(crSelectionPanel != null) //CR Selection panel from ECO creation
					{
						if (crSelectionPanel.removedCrs.contains(formsMap.get(crName))) 
						{
							crSelectionPanel.removedCrs.remove(formsMap.get(crName));
						}
						crSelectionPanel.notifyObserver();	
					}
					if(crManagementPanel != null) //CR Selection from CR Management
					{
						if (crManagementPanel.removedCrs.contains(formsMap.get(crName))) 
						{
							crManagementPanel.removedCrs.remove(formsMap.get(crName));
						}
						crManagementPanel.notifyObserver();	
					}
				}
			}
		}
		else if (actCmd.equals("removeRow")) 
		{
			int selRow = trgTable.getSelectedRow();
			if (formsMap.get(trgTable.getValueAt(selRow, 1))!=null)
			{
				if (addedCRsVector.contains(formsMap.get(trgTable.getValueAt(selRow, 1))))
				{
					if(crSelectionPanel != null) //CR Selection panel from ECO creation
					{
						addedCRsVector.remove(formsMap.get(trgTable.getValueAt(selRow, 1)));
						crSelectionPanel.removedCrs.add(formsMap.get(trgTable.getValueAt(selRow, 1)));
						trgTable.removeRow(selRow);
						crSelectionPanel.notifyObserver();
					}
					if(crManagementPanel != null) //CR Selection from CR Management
					{
						addedCRsVector.remove(formsMap.get(trgTable.getValueAt(selRow, 1)));
						crManagementPanel.removedCrs.add(formsMap.get(trgTable.getValueAt(selRow, 1)));
						trgTable.removeRow(selRow);
						crManagementPanel.notifyObserver();
					}
				}
			}
		}
		
	}

	private boolean isValidCR(String crName) 
	{
		TCComponentForm crForm =  formsMap.get(crName);
		if (crForm ==null) 
		{
			return false;
		}
		try 
		{
			if (addedCRsVector.contains(crForm)) 
			{
				MessageBox.post("The "+crName+" is already added to change process.","Cannot add CR",MessageBox.WARNING);
				return false;
			}
			AIFComponentContext[] compContexts = crForm.whereReferenced();
			Vector<TCComponentItemRevision> itemRevVec = new Vector<TCComponentItemRevision>();
			for (int i = 0; i < compContexts.length; i++) 
			{
				InterfaceAIFComponent intAifComp =   compContexts[i].getComponent();
				
				if (intAifComp instanceof TCComponentItemRevision) 
				{
					TCComponentItemRevision itemRevAttached = (TCComponentItemRevision)intAifComp;
					TCComponentItemRevision latestRev = itemRevAttached.getItem().getLatestItemRevision();
					
					if (NOVECOHelper.isItemRevSecObjectCheckedout((TCComponentItemRevision)latestRev)== NOVECOConstants.ItemRev_CheckedOut) 
					{
						MessageBox.post("The "+crName+" is associated to Revision "+ ((TCComponentItemRevision)intAifComp).getProperty("object_string")+ " and revision is Checked-Out.\nCannot be added to Engineering Change.",
								        "Cannot add CR",MessageBox.WARNING);
						return false;
					}
					if (NOVECOHelper.isItemRevSecObjectCheckedout((TCComponentItemRevision)latestRev)== NOVECOConstants.SecObjects_CheckedOut) 
					{
						MessageBox.post("The "+crName+" is associated to Revision "+ ((TCComponentItemRevision)latestRev).getProperty("object_string")+ " and  Dataset under revision is Checked-Out.\nCannot be added to Engineering Change.",
								        "Cannot add CR",MessageBox.WARNING);
						return false;
					}
					if (NOVECOHelper.isItemRevSecObjectCheckedout((TCComponentItemRevision)latestRev)== NOVECOConstants.RDD_DOCRev_CheckedOut) 
					{
						MessageBox.post("The "+crName+" is associated to Revision "+ ((TCComponentItemRevision)latestRev).getProperty("object_string")+ " and  its related refining doument revision is Checked-Out.\nCannot be added to Engineering Change.",
								        "Cannot add CR",MessageBox.WARNING);
						return false;
					}
					if (NOVECOHelper.isItemRevSecObjectCheckedout((TCComponentItemRevision)latestRev)== NOVECOConstants.RDD_DOCRev_SecObjects_CheckedOut) 
					{
						MessageBox.post("The "+crName+" is associated to Revision "+ ((TCComponentItemRevision)latestRev).getProperty("object_string")+ " and  its related refining doument revision's dataset is Checked-Out.\nCannot be added to Engineering Change.",
								        "Cannot add CR",MessageBox.WARNING);
						return false;
					}
					if ( (NOVECOHelper.isItemRevInProcess(latestRev)) )
					{
						//MessageBox.post("The Revision "+ itemRevVec.get(i).getProperty("object_string")+ " attached to "+crName+" is in another process.","Cannot add CR",MessageBox.WARNING);
						//return false;
						return true;
					}
					if (crSelectionPanel != null && crSelectionPanel.typeofChange.equals("Lifecycle")) //CR Selection panel from ECO creation
					{
						TCComponent[] releaseStats = ((TCComponentItemRevision)latestRev).getTCProperty("release_status_list").getReferenceValueArray();
						if (releaseStats.length==0) 
						{
							MessageBox.post("The Revision "+ ((TCComponentItemRevision)latestRev).getProperty("object_string")+ " attached to "+crName+" is not released.","Cannot add CR",MessageBox.WARNING);
							return false;
						}	
					}
					if (crManagementPanel != null) //CR Selection from CR Management
					{
						String sChangeType = crForm.getTCProperty("change_type").getStringValue();
						if(sChangeType.equals("Lifecycle"))
						{
							TCComponent[] releaseStats = ((TCComponentItemRevision)latestRev).getTCProperty("release_status_list").getReferenceValueArray();
							if (releaseStats.length==0) 
							{
								MessageBox.post("The Revision "+ ((TCComponentItemRevision)latestRev).getProperty("object_string")+ " attached to "+crName+" is not released.","Cannot add CR",MessageBox.WARNING);
								return false;
							}	
						}
					}	
					itemRevVec.add((TCComponentItemRevision)latestRev);					
				}
			}
			//TCDECREL-3412 Start
			return true;
			/*if (itemRevVec.size()==0) 
			{
				return true;
			}
			else
			{
				String revStr ="";
				for (int i = 0; i < itemRevVec.size(); i++) 
				{
					revStr = revStr + itemRevVec.get(i).getProperty("object_string")+"\n";				
				}
				MessageBox.post("The Revision "+ revStr+ " attached to "+crName+" will be added to Change process.","Item Added",MessageBox.INFORMATION);
				return true;	
			}*/
			//TCDECREL-3412 End
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}	

}
