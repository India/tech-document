package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.util.MessageBox;

public class NOVECOSyncTargetsDialog extends AbstractAIFDialog 
{
	private static final long serialVersionUID = 1L;
	
	public NOVECOSyncTargetsDialog(String syncMsgStr)
	{
		
		String retStr ="";
		String repotTitle="";
		if (syncMsgStr.trim().length()>0) 
		{
			String[] itemRec = syncMsgStr.split("!!");
			for (int i = 0; i < itemRec.length; i++) 
			{
				String[] indItemRec = itemRec[i].split("::");
				if (indItemRec.length ==3) 
				{
					retStr = retStr+"<TR><TD>"+indItemRec[0]+"</TD>"+"<TD>"+indItemRec[1]+"</TD>"+"<TD>"+indItemRec[2]+"</TD></TR>";
				}
			}
			repotTitle = "<FONT SIZE=\"6\" COLOR=#330033><center>Following Items/ECRs are removed from targets list</center></FONT>";
		}
		
		final String htmlText = repotTitle+ "<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Targets</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Type</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Remarks</B></TD>"+retStr+"</table>";
		JEditorPane editorPane = new JEditorPane();
		editorPane.setContentType("text/html");
		editorPane.setText(htmlText);
		editorPane.setEditable(false);
		JScrollPane scPane = new JScrollPane(editorPane);
		scPane.setPreferredSize(new Dimension(500, 250));
		JPanel panel = new JPanel();
		JButton saveBtn = new JButton("Save Report");
		
		saveBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JFileChooser fileChooser = new JFileChooser();
				int returnVal = fileChooser.showSaveDialog(NOVECOSyncTargetsDialog.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile(); //This grabs the File you typed
					String nameOfFile = "";   
		            nameOfFile = file.getPath();
		            nameOfFile =nameOfFile+".html";
		            try 
					{   
						BufferedWriter out = new BufferedWriter(new FileWriter(nameOfFile));   
						out.write(htmlText);   
						out.close();   
					} 
					catch (IOException ex1) 
					{   
						MessageBox.post(ex1);
					}					
				}
			}
		});
		
		JButton cancelBtn = new JButton("OK");
		cancelBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				NOVECOSyncTargetsDialog.this.dispose();
			}
		});
		panel.add(cancelBtn);
		panel.add(saveBtn);
		this.setTitle("ECO targets synchronization Report");
		getContentPane().add(scPane,BorderLayout.NORTH);
		getContentPane().add(panel,BorderLayout.CENTER);
		centerToScreen();
	}
	
	/*public static NOVECOSyncTargetsDialog getSingletonObject(String syncMsgStr) 
	{
		if (syncTargetsDlg == null) 
		{
			syncTargetsDlg = new NOVECOSyncTargetsDialog(syncMsgStr);
		}
		return syncTargetsDlg;
	}*/
 
}
