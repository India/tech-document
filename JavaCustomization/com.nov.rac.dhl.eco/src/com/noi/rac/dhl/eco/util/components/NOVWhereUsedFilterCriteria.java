package com.noi.rac.dhl.eco.util.components;

import java.util.Vector;


public class NOVWhereUsedFilterCriteria 
{
	private Vector<String> vecSites;
	private Vector<String> vecLifecycle;
	private boolean m_isSiteLifecycleSelectionsEmpty;//4344
	
	public NOVWhereUsedFilterCriteria()
	{
		vecSites = new Vector<String>();
		vecLifecycle = new Vector<String>();
	}
	
	public void setSiteFilterValue(Vector<String> v)
	{
		vecSites = v;
	}
	
	public void setLifecycleFilterValue(Vector<String> v)
	{
		vecLifecycle = v;
	}
	//4344-set value if the selection from site and lifecycle is empty
	public void setSelectionEmpty(boolean value)
	{
		m_isSiteLifecycleSelectionsEmpty=value;
					
	}
	//4344- returns the value of the selection
	public boolean isSelectionEmpty()
	{
		return m_isSiteLifecycleSelectionsEmpty;
	}
	public Vector<String> getSiteFilterValue()
	{
		return vecSites;
	}
	
	public Vector<String> getLifecycleFilterValue()
	{
		return vecLifecycle;
	}
	public void clearSearchCriteria()
	{
	   if(vecSites != null && vecLifecycle != null)
	   {
	       vecSites.clear();
	       vecLifecycle.clear();
	   }
	}
}
