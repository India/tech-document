package com.noi.rac.dhl.eco.util.components;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.tree.TreeNode;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.validations.removeitem.NOVRemoveECRSelectionDialog;
import com.nov.rac.dhl.eco.validations.removeitem.NOVRemoveECRTable;
import com.nov.rac.dhl.eco.validations.removeitem.NOVRemoveECRTableLine;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.nov.rac.utilities.services.workflow.WorkflowSOAHelper;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
//import com.teamcenter.rac.aif.ApplicationDef; //TC10.1 Upgrade

public class NOVECOTargetItemsTable extends JTreeTable {
	private static final long serialVersionUID = 1L;
	NOVEcoTrgTreeTableModel treeTableModel;
	NOVECOMoveTargetDialog moveTargetDlg = null; // TCDECREL-3079
	NOVECOTargetItemsTable novecoTrgTbl;	
	private TCSession session;	
	public HashMap<TCComponent, String> newLifecycleMap;
	public HashMap<TCComponent, String> currentStatMap;
	public HashMap<String, TCComponent> lifecycleMap = new HashMap<String, TCComponent>();
	private Vector<TCComponent> TargetsTobremoved = new Vector<TCComponent>();
	private Vector<TCComponentItemRevision> m_rddItemsVector = new Vector<TCComponentItemRevision>();
	public Set<TCComponent> targetItems = new HashSet<TCComponent>();
	public Set<String> strTargetItemIds = new HashSet<String>();
	private TCComponentTask roottask;
	_EngChangeObjectForm_Panel ecoFormPanel;
	private boolean isECOReleased = false;
	private boolean isDispostionTable = false;
	private int m_row = 0;	
	private String m_itemType;
	private String m_statusType;
	private String[] newlifecycStrArr;
	private String[] newlifecycStrArr_Doc = { "", "Standard", "Obsolete" };
	private String[] newlifecycStrArr_nonEng = { "", "Engineering", "Standard",
			"Phase Out", "Obsolete" };
	private Vector<TCComponent> m_selectedRevs = new Vector<TCComponent>();
	private Vector<TCComponent> m_selectedECRs = new Vector<TCComponent>();

	public NOVECOTargetItemsTable(
			_EngChangeObjectForm_Panel engChangeObjectFormPanel,
			TCSession tcSession, String columnNames[], boolean isDispostionTbl) {

		super(tcSession);

		this.session = tcSession;
		isDispostionTable = isDispostionTbl;
		ecoFormPanel = engChangeObjectFormPanel;
		newLifecycleMap = new HashMap<TCComponent, String>();
		currentStatMap = new HashMap<TCComponent, String>();
		treeTableModel = new NOVEcoTrgTreeTableModel(columnNames);
		setModel(treeTableModel);

		for (int i = 0; i < columnNames.length; i++) {
			TableColumn col = new TableColumn(i);
			addColumn(col);
		}
		getTree().setCellRenderer(new TargetIdRenderer(this));
		// set Renderer and editor for "future status" column
		// RootCellRenderer

		getColumnModel().getColumn(1).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(2).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(3).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(4).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(5).setCellRenderer(new RootCellRenderer());
		// getColumnModel().getColumn(6).setCellRenderer(new
		// RootCellRenderer());

		try {
			String statuses = ecoFormPanel.ecoForm
					.getProperty("release_statuses");
			if (statuses.length() > 0) {
				isECOReleased = true;
			}
			TCComponentListOfValues lovValues = TCComponentListOfValuesType
					.findLOVByName(tcSession, "DH_Lifecycle");

			/*
			 * ListOfValuesInfo lovInfo = lovValues.getListOfValues(); String[]
			 * lovS = lovInfo.getStringListOfValues();
			 */
			String[] lovS = NOVECOHelper.getLovList(lovValues,
					ecoFormPanel.ecoForm.getProperty("owning_group").toString()
							.substring(0, 2));

			Vector<String> newLifecycleVals = new Vector<String>();
			newLifecycleVals.add("");
			for (int i = 0; i < lovS.length; i++) {
				newLifecycleVals.add(lovS[i]);
			}
			newlifecycStrArr = newLifecycleVals
					.toArray(new String[newLifecycleVals.size()]);
			final JComboBox ststuscombo = new JComboBox(newlifecycStrArr);// soma
			// TableColumn col6=getColumnModel().getColumn(6);
			TableColumn col6 = getColumnModel().getColumn(5);
			col6.setCellRenderer(new ComboboxCellRenderer(newlifecycStrArr));
			col6.setCellEditor(new ComboboxCellEditor(ststuscombo));

		} catch (TCException e1) {
			e1.printStackTrace();
		}

		// getColumnModel().getColumn(1).setWidth(100);
		setAutoResizeMode(JTreeTable.AUTO_RESIZE_ALL_COLUMNS);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent mouseEvt) {
				Object source = mouseEvt.getSource();
				TreeTableNode[] treeNode = ((NOVECOTargetItemsTable) source)
						.getSelectedNodes();
				if (treeNode.length == 1) {
					if (treeNode[0] instanceof NovTreeTableNode) {
						if (!((TreeTableNode) treeNode[0].getParent()).isRoot()) {
							return;
						}
					}
				}
				int selRow = ((NOVECOTargetItemsTable) source).getSelectedRow();

				try {
					TCReservationService resService = ecoFormPanel.ecoForm
							.getSession().getReservationService();
					boolean isReserved = resService.isReserved(ecoFormPanel.ecoForm) &&  ecoFormPanel.ecoForm.okToModify();//4907
					
					/*
					 * DHL-202 display popoupmenu irrespective of whether form
					 * is checked in/out
					 */
					if (SwingUtilities.isRightMouseButton(mouseEvt)
							&& (mouseEvt.getClickCount() == 1) && selRow > 0 /*
																			 * &&(
																			 * isReserved
																			 * )
																			 */) {
						JPopupMenu popupMenu = new JPopupMenu();
						JMenuItem item = new JMenuItem();
						// ++
						JMenuItem item1 = new JMenuItem();
						String initialText1 = "<html><b><font=8 color=red>Send to My Teamcenter</font></b>";
						item1.setText(initialText1);
						JMenuItem item2 = new JMenuItem();
						String initialText2 = "<html><b><font=8 color=red>Send to Structure Manager</font></b>";
						item2.setText(initialText2);

						JMenuItem itemMove = new JMenuItem();

						item1.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(
								// "Menu item clicked");
								// TODO Auto-generated method stub
								int row = NOVECOTargetItemsTable.this
										.getSelectedRow();
								TCComponent remTarget = ((NovTreeTableNode) NOVECOTargetItemsTable.this
										.getNodeForRow(row)).targetRev;

								Object[] args = new Object[2];
								args[0] = AIFUtility.getActiveDesktop();
								args[1] = (InterfaceAIFComponent) remTarget; /*
																			 * theComponent
																			 * is
																			 * of
																			 * type
																			 * TCComponent
																			 * ,
																			 * this
																			 * should
																			 * have
																			 * the
																			 * component
																			 * that
																			 * you
																			 * intend
																			 * to
																			 * open
																			 */

								AbstractAIFCommand openCmd;
								try {
									openCmd = session.getOpenCommand(args);
									openCmd.executeModal();
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

								// ApplicationDef navigatorApp =
								// AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey("com.teamcenter.rac.explorer.ExplorerApplication");
								// if( navigatorApp == null)
								// navigatorApp =
								// AIFUtility.getAIFApplicationDefMgr().getApplicationDefByTitle("Navigator");
								// navigatorApp.openApplication(
								// (InterfaceAIFComponent[]) sendToItem);
							}

						});

						item2.addActionListener(new ActionListener() {

							public void actionPerformed(ActionEvent e) {
								// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(
								// "Menu item clicked");
								// TODO Auto-generated method stub
								int row = NOVECOTargetItemsTable.this
										.getSelectedRow();
								TCComponent remTarget = ((NovTreeTableNode) NOVECOTargetItemsTable.this
										.getNodeForRow(row)).targetRev;
								InterfaceAIFComponent comps[] = new InterfaceAIFComponent[1];
								comps[0] = (InterfaceAIFComponent) remTarget;
/*                              TC10.1 Upgrade
								ApplicationDef navigatorApp = AIFUtility
										.getAIFApplicationDefMgr()
										.getApplicationDefByKey(
												"com.teamcenter.rac.pse.PSEApplication");*/
								// ApplicationDef navigatorApp =
								// AIFUtility.getAIFApplicationDefMgr().getApplicationDefByTitle("explorer");
								//navigatorApp.openApplication(comps);TC10.1 Upgrade
								
								IPerspectiveDef perspectiveDef = PerspectiveDefHelper.getPerspectiveDefByLegacyAppID("com.teamcenter.rac.pse.PSEApplication");//TC10.1 Upgrade
                                perspectiveDef.openPerspective(comps);//TC10.1 Upgrade
							}

						});

						// ++
						// DHL-202 fix Remove targets option available for only
						// checkedout forms
						if (isReserved) {
							String initialText = "<html><b><font=8 color=red>Remove Target</font></b>";
							item.setText(initialText);

							String initialMoveText = "<html><b><font=8 color=red>Move to Other ECO</font></b>";
							itemMove.setText(initialMoveText);
							item.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									// Confirmation dialog
									int row = NOVECOTargetItemsTable.this
											.getSelectedRow();
									if (row != -1) 
									{
										int response = JOptionPane
												.showConfirmDialog(
														ecoFormPanel,
														"Do you want to remove the selected target from the current process?",
														"Confirm",
														JOptionPane.YES_NO_OPTION,
														JOptionPane.QUESTION_MESSAGE);
										if (response == JOptionPane.YES_OPTION)
										{
											try
											{
												
												//getTree().getSelectionPath();
												TCComponent remTarget = ((NovTreeTableNode) NOVECOTargetItemsTable.this.getNodeForRow(row)).targetRev;
												
												NOVECRSelectionDialogWorker objECRSelect = new NOVECRSelectionDialogWorker(remTarget);
												objECRSelect.execute();
											}
											catch(Exception ex)
											{
												ex.printStackTrace();
											}
										}
									}

								}
							});
							// TCDECREL-3079 : Start : 20-08-2012 : Akhilesh
							itemMove.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									Display.getDefault().asyncExec(
											new Runnable() {
												@Override
												public void run() {
													int row = NOVECOTargetItemsTable.this
															.getSelectedRow();
													if (row != -1) {
														TCComponent remTarget = ((NovTreeTableNode) NOVECOTargetItemsTable.this
																.getNodeForRow(row)).targetRev;
														Shell parent = Display
																.getDefault()
																.getActiveShell();

														try {
															String chngType = ecoFormPanel.ecoForm
																	.getProperty(
																			"changetype")
																	.toString();

															moveTargetDlg = new NOVECOMoveTargetDialog(
																	parent,
																	remTarget,
																	chngType,
																	ecoFormPanel);

															moveTargetDlg
																	.open();

															roottask = ecoFormPanel
																	.getProcess()
																	.getRootTask();

															AIFComponentContext[] cxtObjs = roottask
																	.getChildren();
															for (int i = 0; i < cxtObjs.length; i++) {
																if (cxtObjs[i]
																		.getContext()
																		.equals("pseudo_folder")) {
																	((TCComponent) cxtObjs[i]
																			.getComponent())
																			.refresh();
																}
															}
															roottask.refresh();

														} catch (TCException e1) {
															// TODO
															// Auto-generated
															// catch block
															e1.printStackTrace();
														}
													}
												}
											});
								}
							});
							popupMenu.add(item);
							popupMenu.add(itemMove);
							// TCDECREL-3079 : End
						}
						popupMenu.add(item1);
						popupMenu.add(item2);
						popupMenu.show((java.awt.Component) source,
								mouseEvt.getX(), mouseEvt.getY());

					}
				} catch (TCException e1) {
					e1.printStackTrace();
				}
			}
		});

	}

	public boolean isCellEditable(int row, int column) {
		if(isTextFieldNotEditable(row))//6112 
		{
			return false;
		}
		if (column == 5 ) {

			return true;
		}
		return false;
	}
	//6112 START
	public boolean isTextFieldNotEditable(int row)
	{
		boolean isNotToEditField = false;
		TCComponent revComp = ((NovTreeTableNode) getNodeForRow(row)).targetRev;
		TCComponent parentComp = null;
		if (((NovTreeTableNode) getNodeForRow(row)).isLeaf()) {
			TreeNode treeNode = getNodeForRow(row).getParent();
		
			if (treeNode instanceof NovTreeTableNode)
			{
				parentComp = ((NovTreeTableNode) getNodeForRow(row)
						.getParent()).targetRev;
			}
			if (revComp.getType().equals("Documents Revision")) {					
				if (parentComp != null) 
				{					
					if ((parentComp.getType().equals("Nov4Part Revision") || parentComp
							.getType().equals("Non-Engineering Revision"))) {
					}
					   isNotToEditField = true;
					}
			}
		}
		return isNotToEditField;
	}
	//6112 end
	
	

	public TCComponentItemRevision[] getTargetRevisions() {
		TCComponentItemRevision[] targetRevs = null;
		Object[] targetObjs = targetItems.toArray();
		if (targetObjs != null) {
			targetRevs = targetItems
					.toArray(new TCComponentItemRevision[targetObjs.length]);
		}
		return targetRevs;
	}

	/*
	 * public Vector<TCComponent> getRootTaskTargetRevisions() {
	 * Vector<TCComponent> targetCompsVec = new Vector<TCComponent>(); try {
	 * roottask = ecoFormPanel.getProcess().getRootTask(); roottask.refresh();
	 * TCComponent[] targetComps =
	 * roottask.getAttachments(TCAttachmentScope.GLOBAL,
	 * TCAttachmentType.TARGET);
	 * 
	 * for (int i = 0; i < targetComps.length; i++) { if (targetComps[i]
	 * instanceof TCComponentItemRevision) { targetCompsVec.add(targetComps[i]);
	 * } } } catch (TCException e) { e.printStackTrace(); } return
	 * targetCompsVec; }
	 */

	public void removeTargetsandCleanup(int row) {
		try {
			Vector<TCComponent> targetCompsVec = ecoFormPanel.getECOTargets();
			roottask = ecoFormPanel.getProcess().getRootTask();
			TCComponent[] targets = roottask.getAttachments(TCAttachmentScope.LOCAL,TCAttachmentType.TARGET);
			Vector<TCComponent> targetComps = new Vector<TCComponent>(
					Arrays.asList(targets));
			TCComponent remTarget = ((NovTreeTableNode) NOVECOTargetItemsTable.this
					.getNodeForRow(row)).targetRev;
			TCComponent targetDisp = ((NovTreeTableNode) NOVECOTargetItemsTable.this
					.getNodeForRow(row)).dispComp;
			deleteTargets((TCComponentItemRevision) remTarget);
			/*
			 * if(TargetsTobremoved.size()==targetCompsVec.size()) {
			 * TargetsTobremoved.removeAllElements(); MessageBox.post(
			 * "Removing this will remove all the targets from Process, It can not be deleted."
			 * , "Warning", MessageBox.WARNING); return; }
			 */
			// Remove from the Process

			// TCDECREL-3580 : Start : 09-oct-2012 : Akhilesh
			removeRelationTargetToECO(TargetsTobremoved);
			// TCDECREL-3580 : End

			
			if (targetCompsVec.size() > 0) {
				Vector<TCComponent> compsToRemove = new Vector<TCComponent>();
				for (int i = 0; i < TargetsTobremoved.size(); i++) {
					if (targetCompsVec.contains(TargetsTobremoved.get(i))) {
						compsToRemove.add(TargetsTobremoved.get(i));
						AIFComponentContext[] secObjs = TargetsTobremoved
								.get(i).getRelated();
						for (int j = 0; j < secObjs.length; j++) {
							if (targetComps.contains(secObjs[j].getComponent())) {
								if (!compsToRemove
										.contains((TCComponent) secObjs[j]
												.getComponent())) {
									compsToRemove.add((TCComponent) secObjs[j]
											.getComponent());
								}
							}
						}
					}
				}
				if (compsToRemove.size() > 0) 
				{
					
					roottask = ecoFormPanel.getProcess().getRootTask();

					WorkflowSOAHelper.removeAttachments(roottask, (TCComponent[]) compsToRemove.toArray());
					roottask.refresh();
				}
			}

			// Remove row from the table and remove it from the vector
			int childcount = ((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
					.getRoot()).getChildCount();
			for (int k = childcount - 1; k >= 0; k--) {
				// TCComponent
				// tobRemoved=((NovTreeTableNode)NOVECNTargetsTable.this.getNodeForRow(i)).context;
				NovTreeTableNode treeNode = (NovTreeTableNode) ((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
						.getRoot()).getChildAt(k);
				TCComponent tobRemoved = treeNode.targetRev;
				if (TargetsTobremoved.contains(tobRemoved)) {
					// if(!(tobRemoved.getProperty("object_type").equalsIgnoreCase("Documents Revision")))
					{
						/*((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
								.getRoot()).remove(treeNode);*/
						
						final NovTreeTableNode node = treeNode;
						if(SwingUtilities.isEventDispatchThread())
						{
							((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
									.getRoot()).remove(treeNode);
							updateUI();
						}
						else
						{
							SwingUtilities.invokeLater(new Runnable() 
							{								
								@Override
								public void run() 
								{
									((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
											.getRoot()).remove(node);
									updateUI();
								}
							});
						}
						
					}
					AIFComponentContext[] crFormsattached = null;
					if (targetDisp != null) {
						String oldRev = targetDisp.getProperty("oldrev");
						if (oldRev.trim().length() > 0) {
							TCComponentItemRevision oldItemRev = NOVECOHelper
									.getItemRevisionofRevID(
											((TCComponentItemRevision) tobRemoved)
													.getItem(), oldRev);
							crFormsattached = oldItemRev.getSecondary();
						} else {
							crFormsattached = tobRemoved.getSecondary();
						}
					} else {
						crFormsattached = tobRemoved.getSecondary();
					}

					for (int i = 0; i < crFormsattached.length; i++) {
						if ((crFormsattached[i].getContext()
								.equals("RelatedECN"))
								&& (ecoFormPanel.targetCRsPanel.getCRForms()
										.contains(crFormsattached[i]
												.getComponent()))) {
							DefaultListModel model = ((DefaultListModel) ecoFormPanel.targetCRsPanel.crsList
									.getModel());
							model.removeElement(crFormsattached[i]
									.getComponent());
							ecoFormPanel.ecoForm.remove("_ECNECR_",
									(TCComponent) crFormsattached[i]
											.getComponent());
						}
					}
					Vector<TCComponentForm> crForms = new Vector<TCComponentForm>();
					for (int i = 0; i < ecoFormPanel.targetCRsPanel.crsList
							.getModel().getSize(); i++) {
						crForms.add((TCComponentForm) ecoFormPanel.targetCRsPanel.crsList
								.getModel().getElementAt(i));
					}
					TCComponentForm[] crforms = crForms
							.toArray(new TCComponentForm[crForms.size()]);
					String reasonforChange = NOVECOHelper
							.getReason4ChangeString(crforms);
					ecoFormPanel.reasonforChange.setText(reasonforChange);
				}
			}

			
			// Remove from the Targets vector
			targetCompsVec.removeAll(TargetsTobremoved);
			// remove the disposition tab for the respective target
			for (int k = 0; k < TargetsTobremoved.size(); k++) {
				if (targetItems.contains(TargetsTobremoved.get(k))) {
					strTargetItemIds.remove(TargetsTobremoved.get(k)
							.getProperty("item_id"));
					targetItems.remove(TargetsTobremoved.get(k));
				}
				if (newLifecycleMap.containsKey(TargetsTobremoved.get(k))) {
					newLifecycleMap.remove(TargetsTobremoved.get(k));
				}
				NOVECOTargetItemsTable.this.ecoFormPanel
						.removeDispositionData(TargetsTobremoved.get(k));
			}
			NOVECOTargetItemsTable.this.ecoFormPanel.saveNewLifecycle();
			TargetsTobremoved.removeAllElements();
		} catch (TCException exp) {
			MessageBox.post(exp.getError(), "Error", MessageBox.ERROR);
			System.out.println(exp);
		}
	}

	// TCDECREL-3580 : Start : 09-Oct-2012 : Akhilesh
	/**
	 * Purpose : This Function will remove relation between current ECO and
	 * selected target item param : targetsToRemove Target Item Revision to be
	 * removed from ECO.
	 */
	private void removeRelationTargetToECO(Vector<TCComponent> targetsToRemove) {
		String formName = "";
		Vector<TCComponent> primaryComp = new Vector<TCComponent>();
		Vector<TCComponent> SecondaryComp = new Vector<TCComponent>();
		Vector<String> relationType = new Vector<String>();
		// boolean
		Object[] args = new Object[3];
		try {
			formName = ecoFormPanel.ecoForm.getTCProperty("object_name")
					.getStringValue();

			for (int i = 0; i < targetsToRemove.size(); i++) {
				TCComponent removeComp = targetsToRemove.get(i);
				if (removeComp instanceof TCComponentItemRevision) {
					AIFComponentContext[] comp = removeComp
							.getRelated("RelatedECN");
					for (int j = 0; j < comp.length; j++) {
						TCComponent relatedComp = (TCComponent) comp[j]
								.getComponent();
						if (relatedComp.getType().equalsIgnoreCase(
								"_DHL_EngChangeForm_")
								&& formName.equalsIgnoreCase(relatedComp
										.getTCProperty("object_name")
										.getStringValue())) {
							// removeComp.remove("RelatedECN", relatedComp);
							primaryComp.add(removeComp);
							SecondaryComp.add(relatedComp);
							relationType.add("RelatedECN");
						}
					}
				}
			}
			
			TCComponent[] m_primaryObjet =(TCComponent[])primaryComp.toArray();
		    TCComponent[] m_secondaryObjet =(TCComponent[])SecondaryComp.toArray();;
		    String[] m_relationType =(String[])relationType.toArray();
			DeleteRelationHelper delete = new DeleteRelationHelper(
										  m_primaryObjet, 
										  m_secondaryObjet, 
										  m_relationType);
			delete.deleteRelation();
			
			
			/*if (retObj != null && retObj instanceof Boolean) {

			}*/
		} catch (TCException e) {
			e.printStackTrace();
		}

	}

	// TCDECREL-3580 : End
	public void deleteTargets(TCComponentItemRevision childcomp) {
		try {
            TargetsTobremoved.add(childcomp);
            String RevId = childcomp.getProperty("current_revision_id");
            if ((!childcomp.getProperty("object_type").equalsIgnoreCase(
                    "Documents Revision") && !RevId.contains(".")) || getChangeType().equalsIgnoreCase("Lifecycle"))  //8344P: Put a check to avoid removing RDD for minor revision
            {
                AIFComponentContext[] rddComps = childcomp
                        .getRelated("RelatedDefiningDocument");
                
                for (int k = 0; k < rddComps.length; k++) {
                    InterfaceAIFComponent infComp = rddComps[k].getComponent();
                    if (infComp.getType().equals("Documents")) {
                        TCComponentItem rddItem = (TCComponentItem) rddComps[k]
                                .getComponent();
                        /*TCComponentItemRevision rddRev = getItemRevisionofRevID(
                                rddItem, RevId);--8924-commented*/
                        TCComponentItemRevision rddRev=rddItem.getLatestItemRevision();//8924
                        if ((rddRev != null)
                                && !(TargetsTobremoved.contains(rddRev))) {
                            if (ecoFormPanel.getECOTargets().contains(rddRev)) {
                                //8102:remove the RDD  in lifecycle eco only if its not related to a part family
                                if((getChangeType().equalsIgnoreCase("Lifecycle") && !validatePartFamilyForLifecycleChange(rddRev)) || getChangeType().equalsIgnoreCase("General") )
                                {
                                    TargetsTobremoved.add(rddRev);
                                }
                                
                            }
                        }
                    }
                    if (infComp instanceof TCComponentDataset) {
                        if (!TargetsTobremoved.contains(infComp)) {
                            TargetsTobremoved.add((TCComponent) infComp);
                        }
                    }
                }
            } else {
                // addRddRevsofDocument(childcomp, false);
            }
        } catch (TCException te) {
			System.out.println(te);
		}
	}

	public void addTarget(TCComponent itemRev, TCComponent dipsComp)
	{		
		try 
		{
			String itemtype = ((TCComponentItemRevision) itemRev).getTCProperty("object_type").toString();
			if (!itemtype.equalsIgnoreCase("Documents Revision"))
			{
				NovTreeTableNode node1 = new NovTreeTableNode(itemRev, dipsComp);
				String RevId = itemRev.getProperty("current_revision_id");
				AIFComponentContext contextRDD[] = itemRev.getRelated("RelatedDefiningDocument");
				
				for (int i = 0; i < contextRDD.length; i++) 
				{
					InterfaceAIFComponent infComp = contextRDD[i].getComponent();
					if ((infComp.getType().equals("Documents") && !(RevId.contains("."))) || (getChangeType().equalsIgnoreCase("Lifecycle") && infComp.getType().equals("Documents")  ))//7804-added a check to ignore mismatch error in case of minor revision 
					{
						TCComponentItem rddcomp = (TCComponentItem) contextRDD[i].getComponent();
						
						TCComponentItemRevision rddRev = getItemRevisionofRevID(rddcomp, RevId);//check RDD revid and Item rev ID same or not
						
						
						if (rddRev == null) 
						{
							MessageBox.post("The Revision mismatch. Item"
									+ ((TCComponentItemRevision) itemRev)
											.getItem().toString()
									+ " has Releated Defining Document and "
									+ rddcomp.toString()
									+ " doesnot have revision " + RevId,
									"Cannot add Item", MessageBox.WARNING); 
						}
						
						else if (ecoFormPanel.ecoTargets.contains(rddRev)) /*ecoFormPanel.getECOTargets().contains(rddRev)*/
						{
						    //8102:start-add rdd if its not in part family
						    if(getChangeType().equalsIgnoreCase("Lifecycle") && !(validatePartFamilyForLifecycleChange(rddRev)) && !isDispostionTable )
						    {
						        addRDDNode(node1, rddRev);
						    }
						    //8102:end
						    else if (!isDispostionTable /*&& !(validatePartFamilyForLifecycle(rddRev) ) */ && getChangeType().equalsIgnoreCase("General") ) 
						    {
						        //8102:add RDD in case of general
						      //7722-commented the check to allow the RDD in case of part family for lifecyle eco
	                            
	                            addRDDNode(node1, rddRev);
	                           
						    }
							
							
						} 
						else
						{// soma-TCDECREL-2807
						  //8102-start - add RDD only if its not in part family
	                        if(getChangeType().equalsIgnoreCase("Lifecycle") && !(validatePartFamilyForLifecycleChange(rddRev)) && !isDispostionTable)
	                         {
	                             addRDDNode(node1, rddRev);
	                            
	                         }
	                        //8102-end
						    if(!(validatePartFamilyForLifecycle(rddRev)) && !ecoFormPanel.isMoveToECOOp())
							{
								//7161-start
								String rddRevision=rddRev.getProperty("current_revision_id");
								String rddReleaseStatus=rddRev.getProperty("release_status_list");
								String itemReleaseStatus=itemRev.getProperty("release_status_list");
								//7804-commented revisions comparison check as its not needed for mmr
								if(!(/*RevId.equalsIgnoreCase(rddRevision) &&*/ rddReleaseStatus.contains("Released") && !itemReleaseStatus.contains("Released")))
								{
									if(!itemReleaseStatus.contains("Released"))
									{
											//7161-end 
											String sErrorMsg = NOVECOHelper.addTargetItemRevsToECO("NOVECOTargetItemsPanel",ecoFormPanel.ecoForm, new TCComponent[] { rddcomp }, "Create Major Revision"); //7805
											if(sErrorMsg != null && sErrorMsg.trim().length()>0)
											{
												JOptionPane.showMessageDialog(ecoFormPanel, sErrorMsg, "Add Item", JOptionPane.WARNING_MESSAGE);
											}
											else
											{
												addRDDNode(node1, rddRev);
											}
										
									}
								}
								
							}
						}
					}
				}
				targetItems.add(itemRev);
				strTargetItemIds.add(itemRev.getProperty("item_id"));
				treeTableModel.addRoot(node1);
				updateUI();
			} 
			else if (!isRDDofTargetItem(itemRev)) 
			{
				NovTreeTableNode nodeDoc = new NovTreeTableNode(itemRev, null);
				targetItems.add(itemRev);
				strTargetItemIds.add(itemRev.getProperty("item_id"));
				treeTableModel.addRoot(nodeDoc);
				updateUI();
			}
			else
			{
				if(validatePartFamilyForGeneral(itemRev))
				{
					NovTreeTableNode nodeDoc = new NovTreeTableNode(itemRev, null);
					treeTableModel.addRoot(nodeDoc);
					updateUI();
				}
				//7804-start-added to add the document to eco in case of minor
				else
				{
					if(itemRev.getProperty("current_revision_id").contains(".") && getChangeType().equalsIgnoreCase("General") )
					{
						NovTreeTableNode nodeDoc = new NovTreeTableNode(itemRev, null);
						treeTableModel.addRoot(nodeDoc);
						updateUI();
						
					}
					//7804-end
				}
                if(itemtype.equalsIgnoreCase("Documents Revision") && getChangeType().equalsIgnoreCase("Lifecycle") && (validatePartFamilyForLifecycleChange(itemRev)) && ecoFormPanel.ecoTargets.contains(itemRev) )
                {
                    NovTreeTableNode nodeDoc = new NovTreeTableNode(itemRev, null);
                    targetItems.add(itemRev);
                    strTargetItemIds.add(itemRev.getProperty("item_id"));
                    treeTableModel.addRoot(nodeDoc);
                    updateUI();
                    
                }
                //8102:end  
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	/*TCDECREL-3369 Validate if added target has multiple RDD Items*/ 
	public boolean  validatePartFamilyForGeneral(TCComponent itemRev) throws TCException 
	{
		int rddItemCount = 0;
		AIFComponentContext[] contextComp =  NOVECOHelper.getRDDItems(itemRev);
		if(contextComp != null && getChangeType().equalsIgnoreCase("General"))
		{
			for (int i = 0; i < contextComp.length; i++)
			{
				InterfaceAIFComponent infComp = contextComp[i].getComponent();	
				if (infComp instanceof TCComponentItemRevision)
				{
					if (ecoFormPanel.ecoTargets.contains(infComp)) 
					{
						//rddItemVector.add((TCComponent) infComp);
						rddItemCount++;
						if(rddItemCount >1)
							return true;						
						
					}
				}
				/*if(rddItemVector.size()>1)
					return true;*/
			}
		}

		return false;
	}
	
	private boolean  validatePartFamilyForLifecycle(TCComponent itemRev) throws TCException 
	{
		AIFComponentContext[] contextComp = NOVECOHelper.getRDDItems(itemRev);
		int rddItemCount	= 0;
		if(contextComp != null && getChangeType().equalsIgnoreCase("Lifecycle"))
		{
			for (int i = 0; i < contextComp.length; i++)
			{
				InterfaceAIFComponent infComp = contextComp[i].getComponent();	
				if (infComp instanceof TCComponentItemRevision)
				{					
					rddItemCount++;
					if(rddItemCount>1)
						return true;					
				}
			}
		}

		return false;
	}
	//8102-start
	private boolean  validatePartFamilyForLifecycleChange(TCComponent itemRev) throws TCException 
    {
	    AIFComponentContext[] contextComp = NOVECOHelper.getRDDItems(itemRev);
        int rddItemCount    = 0;
        TCComponentItem globalItem=null;
        if(contextComp != null && getChangeType().equalsIgnoreCase("Lifecycle"))
        {
            for (int i = 0; i < contextComp.length; i++)
            {
                InterfaceAIFComponent infComp = contextComp[i].getComponent();  
                
                if (infComp instanceof TCComponentItemRevision)
                {
                    TCComponentItem localItem=((TCComponentItemRevision) infComp).getItem();
                    if(globalItem !=null && !localItem.equals(globalItem))
                    {
                        rddItemCount++;
                    }
                    globalItem=localItem;
                    if(rddItemCount>=1)
                        return true;
                                    
                }
            }
        }

        return false;
    }
	//8102-end

	// TCDECREL-2807
	private void addRDDNode(NovTreeTableNode parentNode,
			TCComponentItemRevision rddRev) throws TCException {
		NovTreeTableNode nodeRDD = new NovTreeTableNode(rddRev, null);
		parentNode.add(nodeRDD);
		targetItems.add(rddRev);
		strTargetItemIds.add(rddRev.getProperty("item_id"));
	}

	// TCDECREL-2807

	private boolean isRDDofTargetItem(TCComponent itemRev) 
	{

		try 
		{
			AIFComponentContext[] contextComp = NOVECOHelper.getRDDItems(itemRev);
			if(contextComp != null)
			{
				for (int i = 0; i < contextComp.length; i++)
				{
					InterfaceAIFComponent infComp = contextComp[i].getComponent();	
					if (infComp instanceof TCComponentItemRevision)
					{
						if (ecoFormPanel.ecoTargets.contains(infComp)) 
						{
							return true;
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	public TCComponentItemRevision getItemRevisionofRevID(TCComponentItem rddItem, String RevId) 
	{
		TCComponentItemRevision itemRev = null;
		//8102-start
		if(RevId.contains("."))
		{
		    RevId=NOVECOHelper.getRevisionId(RevId);
		}
		try 
		{
			
			AIFComponentContext[] revItems = rddItem.getChildren("revision_list");
			
			for (int k = 0; k < revItems.length; k++)
			{
				String revId = ((TCComponentItemRevision) revItems[k]
				                          .getComponent()).getProperty("current_revision_id");
				//8102-start
				if(revId.contains("."))
		        {
				    revId=NOVECOHelper.getRevisionId(revId);
		        }
				//8102-end
				if (revId.compareTo(RevId) == 0) 
				{
					itemRev = (TCComponentItemRevision) revItems[k].getComponent();
					break;
				}
			}
		} 
		catch (TCException e)
		{
			System.out.println(e);

		}
		/*if (itemRev == null) 
		{

		}*/

		return itemRev;
	}

	@SuppressWarnings("unchecked")
	public class NOVEcoTrgTreeTableModel extends TreeTableModel {
		private static final long serialVersionUID = 1L;
		private Vector<String> columnNamesVec;

		public NOVEcoTrgTreeTableModel(String columnNames[]) {
			super();
			columnNamesVec = new Vector<String>();
			TreeTableNode top = new TreeTableNode();
			setRoot(top);
			top.setModel(this);
			for (int i = 0; i < columnNames.length; i++) {
				columnNamesVec.addElement(columnNames[i]);
				modelIndexToProperty.add(columnNames[i]);
			}

		}

		public void addRoot(TreeTableNode root) {
			TreeTableNode top = (TreeTableNode) getRoot();
			top.add(root);
		}

		public TreeTableNode getRoot(int index) {
			if (((TreeTableNode) getRoot()).getChildCount() > 0)
				return (TreeTableNode) ((TreeTableNode) getRoot())
						.getChildAt(index);
			else
				return null;
		}

		public int getColumnCount() {
			return columnNamesVec.size();
		}

		public String getColumnName(int column) {
			return (String) columnNamesVec.elementAt(column);
		}

		public void removeNode(int row) {
			// getNodeForRow(row).remove(arg0);
			((TreeTableNode) getRoot()).remove(getNodeForRow(row));
		}
	}

	public class NovTreeTableNode extends TreeTableNode {
		private static final long serialVersionUID = 1L;
		public TCComponent targetRev;
		public String itemID;
		public TCComponent dispComp;
		public TCComponent lifecycleInsComp;

		public NovTreeTableNode(TCComponent itemRev, TCComponent dipsComp) {
			try {
				targetRev = itemRev;
				itemID = targetRev.getProperty("item_id");
				lifecycleInsComp = lifecycleMap.get(itemID);
				dispComp = dipsComp;
			} catch (TCException e) {
				e.printStackTrace();
			}
		}

		@Override
		public String getProperty(String name) {
			try {
				if (targetRev == null) {
					return "";
				}
				String retStr = "";
				if (name.equalsIgnoreCase("Item Number")) {
					return targetRev.getProperty("item_id") == null ? ""
							: targetRev.getProperty("item_id").toString();
				}
				if (name.equalsIgnoreCase("Name")) {
					return targetRev.getProperty("object_name") == null ? ""
							: targetRev.getProperty("object_name").toString();
				}
				if (name.equalsIgnoreCase("Type")) {
					return targetRev.getProperty("object_type") == null ? ""
							: targetRev.getProperty("object_type").toString();
				}
				if (name.equalsIgnoreCase("Old Rev")) {
					String changeTyp = ecoFormPanel.typeofChange;

					if (dispComp != null) {
						retStr = dispComp.getProperty("oldrev") == null ? ""
								: dispComp.getProperty("oldrev");
						//6276-start
						if((retStr.trim().length()>0) &&changeTyp.equalsIgnoreCase("Lifecycle") && isECOReleased && lifecycleInsComp != null)
						{
							retStr= lifecycleInsComp.getProperty("nov4_oldrevision");
						}
						//6276-end
					} else if (isECOReleased && lifecycleInsComp != null) {
						// For released ECO get the value from
						// nov4_tgtlifecycledata attribute
						if (changeTyp.equalsIgnoreCase("Lifecycle")) {
							retStr = lifecycleInsComp
									.getProperty("nov4_oldrevision");
						} else {
							if (lifecycleInsComp
									.getProperty("nov4_oldrevision") != null
									&& (!lifecycleInsComp
											.getProperty("nov4_oldrevision")
											.equals(targetRev
													.getProperty("current_revision_id")))) {
								retStr = lifecycleInsComp
										.getProperty("nov4_oldrevision");
							} else {
								retStr = "";
							}
						}
					}
					if (!(retStr.trim().length() > 0)
							&& lifecycleInsComp == null) {
						if (changeTyp.equalsIgnoreCase("Lifecycle")) {
							retStr = targetRev
									.getProperty("current_revision_id") == null ? ""
									: targetRev.getProperty(
											"current_revision_id").toString();
						} else {
							TCComponent[] itemRev = ((TCComponentItemRevision) targetRev)
									.getTCProperty("revision_list")
									.getReferenceValueArray();
							Date curRevDate = targetRev
									.getDateProperty("creation_date");
							TCComponentItemRevision prevRev = (TCComponentItemRevision) itemRev[0];
							Date prevDate = prevRev
									.getDateProperty("creation_date");
							for (int k = 1; k < itemRev.length; k++) {
								Date compareDate = ((TCComponentItemRevision) itemRev[k])
										.getDateProperty("creation_date");
								if ((compareDate.before(curRevDate))
										&& (compareDate.after(prevDate))) {
									prevDate = compareDate;
									prevRev = (TCComponentItemRevision) itemRev[k];
								}
							}
							if (targetRev.equals(prevRev)) {
								retStr = "";
							} else {
								retStr = prevRev
										.getProperty("item_revision_id");
							}
						}
					}

					return retStr;
				}
				if (name.equalsIgnoreCase("New Rev")) {
					return targetRev.getProperty("current_revision_id") == null ? ""
							: targetRev.getProperty("current_revision_id")
									.toString();
				}
				if (name.equalsIgnoreCase("Old Lifecycle")) {
					// For Open ECO get the value from ectargetdisposition
					// attribute
					if (dispComp != null) {
						System.out.println(" Old LC - dispComp : "
								+ dispComp.getProperty("oldlifecycle"));
						retStr = dispComp.getProperty("oldlifecycle") == null ? ""
								: dispComp.getProperty("oldlifecycle");
					} else if (isECOReleased && lifecycleInsComp != null) {
						// For released ECO get the value from
						// nov4_tgtlifecycledata attribute
						if (lifecycleInsComp.getProperty("nov4_oldlifecycle") != null) {
							System.out.println("1");
							retStr = lifecycleInsComp
									.getProperty("nov4_oldlifecycle");
						} else {
							retStr = "";
						}
					} else if (!(retStr.trim().length() > 0)) {
						if (((TCComponentItemRevision) targetRev).getItem()
								.getTCProperty("revision_list")
								.getReferenceValueArray().length > 0) {
							retStr = ((TCComponentItemRevision) targetRev)
									.getItem().getProperty("release_statuses") == null ? ""
									: ((TCComponentItemRevision) targetRev)
											.getItem().getProperty(
													"release_statuses");
						}
					}
					if (retStr.length() > 0) // check
					{
						if (newLifecycleMap.get(targetRev) == null) {
							newLifecycleMap.put(targetRev, retStr);
							TCComponent relatedDocs[] = targetRev
									.getRelatedComponents("RelatedDefiningDocument");
							if (relatedDocs.length > 0)
							{
								for (int j = 0; j < relatedDocs.length; j++) 
								{
									if (relatedDocs[j].getType()
											.compareToIgnoreCase("Documents") == 0) 
									{
										TCComponentItem item = (TCComponentItem) relatedDocs[j];
										TCComponentItemRevision docrev = item.getLatestItemRevision();
										if(docrev != null)
										{
											getPartFamilyVector(docrev);
											if(!(m_rddItemsVector.size()>1))
												newLifecycleMap.put(docrev, retStr);
										}
										
									}
								}
							}
						}
					}
					return retStr;
				}
				if (name.equalsIgnoreCase("New Lifecycle")) {
					validate();
					return newLifecycleMap.get(targetRev) == null ? ""
							: newLifecycleMap.get(targetRev);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "";
		}

		public String getRelation() {
			return " ";
		}

		public String getType() {
			return targetRev.getType();
		}

	}

	class TargetIdRenderer extends TreeTableTreeCellRenderer {

		private static final long serialVersionUID = 1L;

		public TargetIdRenderer(JTreeTable arg0) {
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) {
			// Select the current value
			try {
				super.getTreeCellRendererComponent(tree, value, sel, expanded,
						leaf, row, hasFocus);
				if (row == 0) {
					setIcon(TCTypeRenderer.getTypeIcon(
							session.getTypeComponent("Job"), "Job"));
					setText("ECO Targets");
				} else {
					setText(((NovTreeTableNode) getNodeForRow(row)).targetRev
							.getProperty("item_id"));
					setIcon(TCTypeRenderer
							.getIcon(((NovTreeTableNode) getNodeForRow(row)).targetRev));
				}
			} catch (Exception e) {
				System.out.println(e);
			}
			return this;
		}
	}

	// if target is standalone document or non-engineering then lifecycle values
	// are Standard and Void
	public class ComboboxCellEditor extends DefaultCellEditor {
		private static final long serialVersionUID = 1L;

		public ComboboxCellEditor(JComboBox comboBox) {
			super(comboBox);
		}

		@SuppressWarnings("rawtypes")
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			TCComponent revComp = ((NovTreeTableNode) getNodeForRow(row)).targetRev;
			if (((NovTreeTableNode) getNodeForRow(row)).isLeaf()) {
				TreeNode treeNode = getNodeForRow(row).getParent();
				TCComponent parentComp = null;
				if (treeNode instanceof NovTreeTableNode) {
					parentComp = ((NovTreeTableNode) getNodeForRow(row)
							.getParent()).targetRev;
				}
				if (revComp.getType().equals("Documents Revision")) {					
					if (parentComp != null) 
					{					
						if ((parentComp.getType().equals("Nov4Part Revision") || parentComp
								.getType().equals("Non-Engineering Revision"))) {
							String lfvalue = newLifecycleMap.get(revComp) == null ? ""
									: newLifecycleMap.get(revComp);
							JTextField txtField = new JTextField();
							txtField.setText(lfvalue);
							txtField.setEditable(false);
							return txtField;
						}
					} else {
						loadValues((JComboBox) this.getComponent(),
								newlifecycStrArr_Doc);
					}
				} else if (revComp.getType().equals("Non-Engineering Revision")) {
					loadValues((JComboBox) this.getComponent(),
							newlifecycStrArr_nonEng);
				} else if (revComp.getType().equals("Nov4Part Revision")) {
					loadValues((JComboBox) this.getComponent(),
							newlifecycStrArr);
				}
			} else if (revComp.getType().equals("Non-Engineering Revision")) {
				loadValues((JComboBox) this.getComponent(),
						newlifecycStrArr_nonEng);
			} else if (revComp.getType().equals("Nov4Part Revision")) {
				loadValues((JComboBox) this.getComponent(), newlifecycStrArr);
			}

			return super.getTableCellEditorComponent(table, value, isSelected,
					row, column);
		}

		public boolean stopCellEditing() 
		{
			int row;			
			if (m_row > 0) 
			{
				row = m_row;
			} 
			else 
			{
				row = getEditingRow();
			}
			if (row >= 0) 
			{	
				
				/*TCDECREL-3369 Adding Part Family to ECO*/				
				TCComponent revComp = ((NovTreeTableNode) getNodeForRow(row)).targetRev;
				
				String selectedValue = (String) getCellEditorValue();			
				
				if(revComp != null)
				{
					String objectType = revComp.getType();
					
					if(objectType.equalsIgnoreCase("Documents Revision"))
					{
						
							newLifecycleMap.put(revComp, selectedValue);
							setStatusToPartFamily(row, selectedValue );

					}
					
					if(objectType.equalsIgnoreCase("Nov4Part Revision") || objectType.equalsIgnoreCase("Non-Engineering Revision"))
					{
						newLifecycleMap.put(revComp, selectedValue);
						setStatusToPartFamily(row, selectedValue );								
					}
				}
				//TCDECREL-4796 : Start
				//updateUI();	
				SwingUtilities.invokeLater(new Runnable() {
				    public void run() {
                        updateUI();
                      }
                    });
				//TCDECREL-4796 : End
				
				/*TCDECREL-3369 Adding Part Family to ECO*/			
				
				/*newLifecycleMap.put(((NovTreeTableNode) getNodeForRow(row)).targetRev,(String) getCellEditorValue());				
				int childCnt = ((NovTreeTableNode) getNodeForRow(row)).getChildCount();
				for (int i = 0; i < childCnt; i++)
				{
					TCComponent revComp = ((NovTreeTableNode) getNodeForRow(row).getChildAt(i)).targetRev;
					if (revComp.getType().equals("Documents Revision")) 
					{
						newLifecycleMap.put(revComp,(String) getCellEditorValue());
						break;
					}
				}*/
			
			}
			return super.stopCellEditing();
		}
	}
	
	private void loadValues(JComboBox cmbBox, String[] newlifecycStrArr) {
		if (newlifecycStrArr != null) {
			cmbBox.removeAllItems();
			for (int i = 0; i < newlifecycStrArr.length; i++) {
				cmbBox.addItem(newlifecycStrArr[i]);
			}
		}
	}	

	class ComboboxCellRenderer extends JComboBox implements TableCellRenderer {

		private static final long serialVersionUID = 1L;

		public ComboboxCellRenderer(String[] status) {
			super(status);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
				int column) 
		{
			if (row == 0)
				return null;

			if (isSelected) 
			{
				// setBackground(table.getSelectionBackground());
				setForeground(table.getForeground());
			} 
			else 
			{
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			// Select the current value
			if (!(value.toString().trim().length() > 0)) 
			{
				value = ComboboxCellRenderer.this.getItemAt(0);
			}
			TCComponent revComp = ((NovTreeTableNode) getNodeForRow(row)).targetRev;
			if (((NovTreeTableNode) getNodeForRow(row)).isLeaf()) 
			{
				TreeNode treeNode = getNodeForRow(row).getParent();
				TCComponent parentComp = null;
				if (treeNode instanceof NovTreeTableNode)
				{
					parentComp = ((NovTreeTableNode) getNodeForRow(row)
							.getParent()).targetRev;
				}
				if (revComp.getType().equals("Documents Revision"))
				{
					if (parentComp != null) {	
						
						if ((parentComp.getType().equals("Nov4Part Revision") || parentComp
								.getType().equals("Non-Engineering Revision"))) 
						{
							JTextField txtField = new JTextField();
							txtField.setText((String) value);
							txtField.setEditable(false);
							return txtField;
						}
					} 
					else 
					{
						loadValues(this, newlifecycStrArr_Doc);
					}
				} 
				else if (revComp.getType().equals("Non-Engineering Revision")) 
				{
					loadValues(this, newlifecycStrArr_nonEng);
				} 
				else if (revComp.getType().equals("Nov4Part Revision")) 
				{
					loadValues(this, newlifecycStrArr);
				}
			} 
			else if (revComp.getType().equals("Non-Engineering Revision")) 
			{
				loadValues((JComboBox) this, newlifecycStrArr_nonEng);
			} 
			else if (revComp.getType().equals("Nov4Part Revision"))
			{
				loadValues((JComboBox) this, newlifecycStrArr);

			}
			setSelectedItem(value);

			return this;
		}
	}

	class RootCellRenderer extends JLabel implements TableCellRenderer {

		private static final long serialVersionUID = 1L;

		public RootCellRenderer() {
			super();
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if (row == 0) {
				setText("");
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			} else {
				setText(value.toString());
				if (isSelected) {
					setOpaque(true);
					setBackground(table.getSelectionBackground());
					setForeground(table.getForeground());
				} else {
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
			}
			return this;
		}
	}

	public void setChangeObjectForm(
			_EngChangeObjectForm_Panel engChangeObjectFormPanel) {
		ecoFormPanel = engChangeObjectFormPanel;
	}

	public void setStatusToTargets(final String[] dialogData) 
	{
		try {
			m_itemType = "";
			m_statusType = "";
			// int i=1;
			

			if (dialogData != null && dialogData.length > 0) 
			{
				m_itemType = dialogData[0];
				m_statusType = dialogData[1];
				int rowCount = treeTableModel.getRowCount();
				Object rootNode = treeTableModel.getRoot();

				// System.out.println(rowCount);
				for (int i = 1; i < rowCount; i++) 
				{
					if (((NovTreeTableNode) this.getNodeForRow(i)).getParent()
							.equals(rootNode)) 
					{
						m_row = i;
						TCComponent target = ((NovTreeTableNode) this
								.getNodeForRow(i)).targetRev;
						String itemTypeStr = ((TCComponentItemRevision) target)
								.getItem().getType();
						if (itemTypeStr.equalsIgnoreCase(m_itemType)) 
						{
							ComboboxCellEditor editor = (ComboboxCellEditor) this
									.getCellEditor(i, 5);
							JComboBox combo = (JComboBox) editor.getComponent();
							combo.setSelectedItem(m_statusType);
							//editor.stopCellEditing();
							
						}
					}
				}
				m_row = 0;
				repaint();
				updateUI();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void setStatusToPartFamily(int row, String selectedValue) 
	{
		boolean hasPartFamily = false;
		int rowCount = getRowCount();
		Object rootNode = treeTableModel.getRoot();	
		int childCnt = ((NovTreeTableNode) getNodeForRow(row)).getChildCount();
		if(childCnt >0)
		{
			for (int i = 0; i < childCnt; i++)
			{
				hasPartFamily = false;
				TCComponent rddComp = ((NovTreeTableNode) getNodeForRow(row).getChildAt(i)).targetRev;
				if (rddComp.getType().equals("Documents Revision")) 
				{
					for(int j=1; j<rowCount; j++)
					{
						if(((NovTreeTableNode) getNodeForRow(j)).getParent().equals(rootNode))
						{
							TCComponent targetComp = ((NovTreeTableNode) getNodeForRow(j)).targetRev;											
							if(rddComp.equals(targetComp))
							{
								hasPartFamily = true;
								break;
							}
						}
					}
								
				}																
							
			}
						
		}
		if(!hasPartFamily)
		{
			int pchildCnt = ((NovTreeTableNode) getNodeForRow(row)).getChildCount();
			for (int i = 0; i < pchildCnt; i++)
			{
				TCComponent childComp = ((NovTreeTableNode) getNodeForRow(row).getChildAt(i)).targetRev;
				if (childComp.getType().equals("Documents Revision")) 
				{
					newLifecycleMap.put(childComp, selectedValue);
					break;
				}
			}
			
		}
	}
	
	
	private String getChangeType()
	{
		return ecoFormPanel.typeofChange;
	}
	
	private void getPartFamilyVector(TCComponentItemRevision rddRev) throws TCException
	{
		m_rddItemsVector.clear();
		AIFComponentContext[] contextComp = NOVECOHelper.getRDDItems(rddRev);		
		if(contextComp != null )
		{
			for (int i = 0; i < contextComp.length; i++)
			{
				InterfaceAIFComponent infComp = contextComp[i].getComponent();	
				if (infComp instanceof TCComponentItemRevision)
				{
					if (ecoFormPanel.ecoTargets.contains(infComp))
					{
						m_rddItemsVector.add((TCComponentItemRevision) infComp);
					}
					
				}
			}
		}
	}
	public void removeTargetsandCleanup(int iRow,TCComponent[] ecrs)
	{
		try
		{
			Vector<TCComponent> targetCompsVec = ecoFormPanel.getECOTargets();
			TCComponent[] targets = ecoFormPanel.getProcess().getRootTask().getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
			Vector<TCComponent> targetComps = new Vector<TCComponent>(Arrays.asList(targets));
			TCComponent remTarget=((NovTreeTableNode)NOVECOTargetItemsTable.this.getNodeForRow(iRow)).targetRev;

			deleteTargets((TCComponentItemRevision)remTarget);
		
			removeRelationTargetToECO(TargetsTobremoved);
				
			if (targetCompsVec.size()>0)
			{
				Vector<TCComponent> compsToRemove = new Vector<TCComponent>();
				for (int i = 0; i < TargetsTobremoved.size(); i++)
				{
					if (targetCompsVec.contains(TargetsTobremoved.get(i)))
					{
						compsToRemove.add(TargetsTobremoved.get(i));
						AIFComponentContext[] secObjs = TargetsTobremoved.get(i).getRelated();
						for (int j = 0; j < secObjs.length; j++)
						{
							if (targetComps.contains(secObjs[j].getComponent()))
							{
								if (!compsToRemove.contains((TCComponent)secObjs[j].getComponent()))
								{
									//6563-added check for ECO
									try {
										if(!(secObjs[j].getComponent().getProperty("object_type").equals("_DHL_EngChangeForm_")))
										{
										compsToRemove.add((TCComponent)secObjs[j].getComponent());
										}
									} catch (Exception e) {
										
										e.printStackTrace();
									}
								
								}
							}
						}
					}
				}
				if (compsToRemove.size()>0)
				{
					roottask = ecoFormPanel.getProcess().getRootTask();
					WorkflowSOAHelper.removeAttachments(roottask, (TCComponent[]) compsToRemove.toArray());
				
					roottask.refresh();
				}
			}

			//Remove row from the table and remove it from the vector
			int childcount=((TreeTableNode)((NOVEcoTrgTreeTableModel)getModel()).getRoot()).getChildCount();
			//8666-commented out the below lines of code
			/*for(int k=childcount-1;k>=0;k--)
			{
				NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)((NOVEcoTrgTreeTableModel)getModel()).getRoot()).getChildAt(k);
				TCComponent tobRemoved = treeNode.targetRev;
				if(TargetsTobremoved.contains(tobRemoved))
				{
					final NovTreeTableNode node = treeNode;
					if(SwingUtilities.isEventDispatchThread())
					{
						((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
								.getRoot()).remove(treeNode);
						updateUI();
					}
					else
					{
						SwingUtilities.invokeLater(new Runnable() 
						{								
							@Override
							public void run() 
							{
								((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel())
										.getRoot()).remove(node);
								updateUI();
							}
						});
					}
				}
			}*/
			//Remove ECR information
			if(ecrs != null)
			{
				int iECRCount = ecrs.length;
				for (int i = 0; i < iECRCount; i++)
				{
					Vector<TCComponent> targetPanelCRs = ecoFormPanel.targetCRsPanel.getCRForms();
					TCComponent ecrComp = (TCComponent) ecrs[i];
					if (targetPanelCRs.contains(ecrComp))
					{
						DefaultListModel model = ((DefaultListModel)ecoFormPanel.targetCRsPanel.crsList.getModel());
						model.removeElement(ecrComp);
						ecoFormPanel.ecoForm.remove("_ECNECR_", ecrComp);
					}
				}
			}
			Vector<TCComponentForm> crForms = new Vector<TCComponentForm>();
			int itargetCRsCount = ecoFormPanel.targetCRsPanel.crsList.getModel().getSize();
			for (int i = 0; i < itargetCRsCount; i++)
			{
				crForms.add((TCComponentForm)ecoFormPanel.targetCRsPanel.crsList.getModel().getElementAt(i));
			}
			TCComponentForm[] crforms = crForms.toArray(new TCComponentForm[crForms.size()]);
			String reasonforChange = NOVECOHelper.getReason4ChangeString(crforms);
			ecoFormPanel.reasonforChange.setText(reasonforChange);

			//Remove from the Targets vector
			targetCompsVec.removeAll(TargetsTobremoved);
			//remove the disposition tab for the respective target
			for(int k=0;k<TargetsTobremoved.size();k++)
			{
				if (targetItems.contains(TargetsTobremoved.get(k)))
				{
					strTargetItemIds.remove(TargetsTobremoved.get(k).getProperty("item_id"));
					targetItems.remove(TargetsTobremoved.get(k));
				}
				if (newLifecycleMap.containsKey(TargetsTobremoved.get(k)))
				{
					newLifecycleMap.remove(TargetsTobremoved.get(k));
				}
				NOVECOTargetItemsTable.this.ecoFormPanel.removeDispositionData(TargetsTobremoved.get(k));
			}
			NOVECOTargetItemsTable.this.ecoFormPanel.saveNewLifecycle();
			TargetsTobremoved.removeAllElements();
		}
		catch(TCException exp)
		{
			//MessageBox.post(exp.getError(),"Error",MessageBox.ERROR);
			System.out.println(exp);
		}
	}
	public Vector<TCComponent> getECRsList(TCComponent targetRev, TCComponent targetDisp)
	{
		Vector<TCComponent> ecrForms = new Vector<TCComponent>();
		AIFComponentContext[] crFormsAttached = null;
		try
		{
			if (targetDisp!=null)
			{
				String oldRev = targetDisp.getProperty("oldrev");
				if (oldRev.trim().length()>0)
				{
					TCComponentItemRevision oldItemRev =  NOVECOHelper.getItemRevisionofRevID(((TCComponentItemRevision)targetRev).getItem(), oldRev);
					crFormsAttached = oldItemRev.getSecondary();
				}
				else
				{
					crFormsAttached = targetRev.getSecondary();
				}
			}
			else
			{
				crFormsAttached  = targetRev.getSecondary();
			}
	
			int iECRsCount = crFormsAttached.length;
			Vector<TCComponent> targetPanelCRs = ecoFormPanel.targetCRsPanel.getCRForms();
			for (int i = 0; i < iECRsCount; i++)
			{
				TCComponent ecrComp = (TCComponent) crFormsAttached[i].getComponent();
				if ((crFormsAttached[i].getContext().equals("RelatedECN"))&&
						(targetPanelCRs.contains(ecrComp)))
				{
					ecrForms.add(ecrComp);
				}
			}
		}
		catch(TCException e)
		{
			e.printStackTrace();
		}
		return ecrForms;
	}
	protected Map<TCComponent,Vector<TCComponent>> getTargetRevsECRInfo()
	{
		Map<TCComponent,Vector<TCComponent>> itemRevECRMap = new HashMap<TCComponent,Vector<TCComponent>>();
		int iSelectedRow = this.getSelectedRow();
		
		TreeTableNode treeTblNode =  this.getNodeForRow(iSelectedRow);
		if (treeTblNode instanceof NovTreeTableNode)
		{
			TCComponent trgRev = ((NovTreeTableNode)treeTblNode).targetRev;

			TCComponent targetDisp = ((NovTreeTableNode)treeTblNode).dispComp;

			Vector<TCComponent> ecrs = getECRsList(trgRev,targetDisp); 
			if(ecrs.size()>0)
			{
				itemRevECRMap.put(trgRev,ecrs);
			}
		}
		return itemRevECRMap;
	}
	public void removePartFamily(TCComponent selectedComp)
	{
		try
		{
			if(getChangeType().equalsIgnoreCase("General"))
			{
				TCComponentItemRevision rddRev = NOVECOHelper.findRDDforTargetItem(selectedComp);
				if(rddRev != null  )
				{
					getPartFamilyVector(rddRev);
					if(m_rddItemsVector != null && m_rddItemsVector.size()>0)
					{
						int iObjFound = -1;
						if((iObjFound=m_rddItemsVector.indexOf(selectedComp))!=-1)
						{
							m_rddItemsVector.remove(iObjFound);
						}

						TCComponentItemRevision [] rddItemsArray = m_rddItemsVector.toArray(new TCComponentItemRevision[m_rddItemsVector.size()]);
						for(int j=0;j<rddItemsArray.length;j++)
						{
							ecoFormPanel.removeRevisionsFromTarget(rddItemsArray[j]);
						}
					}
				}
				
				else if(isRDDofTargetItem(selectedComp))
				{
					getPartFamilyVector((TCComponentItemRevision) selectedComp);
					if(m_rddItemsVector != null && m_rddItemsVector.size()>0)
					{
						int iObjFound = -1;
						if((iObjFound=m_rddItemsVector.indexOf(selectedComp))!=-1)
						{
							m_rddItemsVector.remove(iObjFound);
						}
						TCComponentItemRevision [] rddItemsArray = m_rddItemsVector.toArray(new TCComponentItemRevision[m_rddItemsVector.size()]);
						for(int j=0;j<rddItemsArray.length;j++)
						{
							ecoFormPanel.removeRevisionsFromTarget(rddItemsArray[j]);
						}
					}
				}
				/*else
				ecoFormPanel.removeRevisionsFromTarget((TCComponentItemRevision) remTarget);*/

			}
			/*else
			ecoFormPanel.removeRevisionsFromTarget((TCComponentItemRevision) remTarget);*/
		}
		catch(TCException ex)
		{
			ex.printStackTrace();
		}
        
	}
	public Vector<TCComponentItemRevision> getPartFamily(TCComponent selectedComp)
	{
		try
		{
			if(getChangeType().equalsIgnoreCase("General"))
			{
				m_rddItemsVector.clear();
				TCComponentItemRevision rddRev = NOVECOHelper.findRDDforTargetItem(selectedComp);
				if(rddRev != null  )
				{
					getPartFamilyVector(rddRev);
					if(m_rddItemsVector != null && m_rddItemsVector.size()>0)
					{
						int iObjFound = -1;
						if((iObjFound=m_rddItemsVector.indexOf(selectedComp))!=-1)
						{
							m_rddItemsVector.remove(iObjFound);
						}
					}
				}
				
				else if(isRDDofTargetItem(selectedComp))
				{
					getPartFamilyVector((TCComponentItemRevision) selectedComp);
					if(m_rddItemsVector != null && m_rddItemsVector.size()>0)
					{
						int iObjFound = -1;
						if((iObjFound=m_rddItemsVector.indexOf(selectedComp))!=-1)
						{
							m_rddItemsVector.remove(iObjFound);
						}
					}
				}
			}
		}
		catch(TCException ex)
		{
			ex.printStackTrace();
		}
		return m_rddItemsVector;
        
	}
	class NOVECRSelectionDialogWorker extends SwingWorker<Integer, Integer> 
    { 
        private int m_iRetCode = -1;
        private int m_iNoOfRows = 0;
        private NOVRemoveECRTable m_ecrSelectionTable = null;
        private NOVRemoveECRSelectionDialog m_ecrSelectionDlg = null;
        private TCComponent m_selectedComp = null;
        public NOVECRSelectionDialogWorker(TCComponent comp)
        {
        	m_selectedComp = comp;
        }
        protected Integer doInBackground() throws Exception 
        { 
            // Do a time-consuming task. 
                        
            final Display display = Display.getDefault();
            display.syncExec(new Runnable() 
            {
                public void run() 
                {
                	Map<TCComponent,Vector<TCComponent>> itemRevCRMap = getTargetRevsECRInfo();
    				if(itemRevCRMap.values().size()>0)
    				{
    					Shell shell = null;
    					shell = display.getActiveShell();
    					if(shell == null && ecoFormPanel != null && ecoFormPanel.isDisplayable())
                    	{
    						Canvas canvas = new Canvas();
    						ecoFormPanel.add(canvas);
    						ecoFormPanel.addNotify();

    						shell = SWT_AWT.new_Shell(display, canvas);
    						shell.setLayout(new GridLayout());                    		
                    	}
    					m_ecrSelectionDlg = new NOVRemoveECRSelectionDialog(shell);
    					m_ecrSelectionDlg.create();
    					
    					NOVECOHelper.centerToScreen(m_ecrSelectionDlg.getShell());

    					m_ecrSelectionDlg.populateECRTable(itemRevCRMap);
	
    					m_ecrSelectionTable = m_ecrSelectionDlg.getECRSelectionTable();
    					m_iNoOfRows = m_ecrSelectionTable.getRowCount();

    					m_iRetCode = m_ecrSelectionDlg.open();
    				}
                }               
            });           
            return m_iRetCode; 
        } 
        
        protected void done() 
        { 
            try 
            {
                if(get() == Dialog.OK)
                {
                	for(int indx=0;indx<m_iNoOfRows;indx++)
					{
						boolean bChkBoxValue = (Boolean)m_ecrSelectionTable.getValueAt(indx, 0);
						if(bChkBoxValue)
						{
							AIFTableLine tableLine = m_ecrSelectionTable.getRowLine(indx);
							NOVRemoveECRTableLine ecrTableLine = (NOVRemoveECRTableLine)tableLine;
							TCComponent formObject = ecrTableLine.getTableLineFormObject();
							TCComponent revObject = ecrTableLine.getTableLineRevObject();

							if(!m_selectedRevs.contains(revObject))
							{
								m_selectedRevs.add(revObject);
							}
							if(!m_selectedECRs.contains(formObject))
							{
								m_selectedECRs.add(formObject);
							}
						}
					}
                	Vector<TCComponentItemRevision> partFamily = getPartFamily(m_selectedComp);
                	//7722-added condition for lifecycle
                	String revId = m_selectedComp.getTCProperty("item_revision_id").toString();
                	if(getChangeType().equalsIgnoreCase("General") && !revId.contains("."))
                	{
                		for(int i=0;i<partFamily.size();i++)
                		{
                			ecoFormPanel.removeRevisionsFromTarget(partFamily.get(i),null);
                		}
                	}
                	ecoFormPanel.removeRevisionsFromTarget(m_selectedComp, m_selectedECRs);
                	ecoFormPanel.removeECRs(m_selectedECRs);
    				m_selectedECRs.clear();
    				}
                else if(get() == -1) //If no ecrs associated with item
                {
                    getPartFamilyVector((TCComponentItemRevision)m_selectedComp); //7804
                	Vector<TCComponentItemRevision> partFamily = getPartFamily(m_selectedComp);
                	String revId = m_selectedComp.getTCProperty("item_revision_id").toString();
                	//7722-added condition for lifecycle
                	if(getChangeType().equalsIgnoreCase("General") && !revId.contains("."))
                	{
                		for(int i=0;i<partFamily.size();i++)
                		{
                			ecoFormPanel.removeRevisionsFromTarget(partFamily.get(i),null);
                		}
                	}
                	ecoFormPanel.removeRevisionsFromTarget(m_selectedComp, m_selectedECRs);   
                	ecoFormPanel.removeECRs(m_selectedECRs);
                }
            } 
            catch (Exception e) 
            { 
                e.printStackTrace(); 
            } 
        } 
    }
	public void removeTargets(TCComponent remTarget)
	{
		Vector<TCComponent> targetCompsVec = ecoFormPanel.getECOTargets();
		TCComponent[] targets = null;
		try 
		{
			roottask = ecoFormPanel.getProcess().getRootTask();
			targets = roottask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);

			Vector<TCComponent> targetComps = new Vector<TCComponent>(Arrays.asList(targets));

			deleteTargets((TCComponentItemRevision)remTarget);

			removeRelationTargetToECO(TargetsTobremoved);

			if (targetCompsVec.size()>0)
			{
				Vector<TCComponent> compsToRemove = new Vector<TCComponent>();
				for (int i = 0; i < TargetsTobremoved.size(); i++)
				{
					if (targetCompsVec.contains(TargetsTobremoved.get(i)))
					{
						compsToRemove.add(TargetsTobremoved.get(i));
						AIFComponentContext[] secObjs = TargetsTobremoved.get(i).getRelated();
						for (int j = 0; j < secObjs.length; j++)
						{
							if (targetComps.contains(secObjs[j].getComponent()))
							{
								if (!compsToRemove.contains((TCComponent)secObjs[j].getComponent()))
								{
									compsToRemove.add((TCComponent)secObjs[j].getComponent());
								}
							}
						}
					}
				}
				if (compsToRemove.size()>0)
				{										
					WorkflowSOAHelper.removeAttachments(roottask, (TCComponent[]) compsToRemove.toArray());
					roottask.refresh();					
				}
			}

			//Remove row from the table and remove it from the vector
			//8666-commented below lines of code
			/*int childcount=((TreeTableNode)((NOVEcoTrgTreeTableModel)getModel()).getRoot()).getChildCount();
			for(int k=childcount-1;k>=0;k--)
			{
				NovTreeTableNode treeNode=(NovTreeTableNode)((TreeTableNode)((NOVEcoTrgTreeTableModel)getModel()).getRoot()).getChildAt(k);
				TCComponent tobRemoved = treeNode.targetRev;
				if(TargetsTobremoved.contains(tobRemoved))
				{
					final NovTreeTableNode node = treeNode;
					if(SwingUtilities.isEventDispatchThread())
					{
						((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel()).getRoot()).remove(treeNode);
						updateUI();
					}
					else
					{
						SwingUtilities.invokeLater(new Runnable() 
						{								
							@Override
							public void run() 
							{
								((TreeTableNode) ((NOVEcoTrgTreeTableModel) getModel()).getRoot()).remove(node);
								updateUI();
							}
						});
					}
				}
			}*/

			for(int k=0;k<TargetsTobremoved.size();k++)
			{
				if (targetItems.contains(TargetsTobremoved.get(k)))
				{
					strTargetItemIds.remove(TargetsTobremoved.get(k).getProperty("item_id"));
					targetItems.remove(TargetsTobremoved.get(k));
				}
				if (newLifecycleMap.containsKey(TargetsTobremoved.get(k)))
				{
					newLifecycleMap.remove(TargetsTobremoved.get(k));
				}
				NOVECOTargetItemsTable.this.ecoFormPanel.removeDispositionData(TargetsTobremoved.get(k));
			}
			NOVECOTargetItemsTable.this.ecoFormPanel.saveNewLifecycle();
			TargetsTobremoved.removeAllElements();
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
}
