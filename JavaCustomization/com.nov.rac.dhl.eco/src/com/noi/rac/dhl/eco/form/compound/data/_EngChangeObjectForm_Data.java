package com.noi.rac.dhl.eco.form.compound.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.object.policies.NOVObjectPolicy;
import com.nov.object.policies.NOVObjectPolicyProperty;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.soa.common.PolicyProperty;

public class _EngChangeObjectForm_Data
{
    private String loadECOFormByObjectPolicy = "";
    private NOVObjectPolicy m_objectPolicy;
    private TCComponentForm ecoForm;
    private _EngChangeObjectForm_Panel ecoFormPanel;
    private HashMap<String, Object> map;
    private Map<String, TCComponent> mapTarget = new HashMap<String, TCComponent>();
    private HashMap<String, Vector<String>> mapRevLc = new HashMap<String, Vector<String>>(); /*
                                                                                               * Vector
                                                                                               * :
                                                                                               * Old
                                                                                               * revision
                                                                                               * ,
                                                                                               * New
                                                                                               * revision
                                                                                               * ,
                                                                                               * Old
                                                                                               * Lifecycle
                                                                                               * ,
                                                                                               * New
                                                                                               * Lifecycle
                                                                                               */
    private HashMap<String, Vector<String>> mapNewRevLc = new HashMap<String, Vector<String>>();
    
    /************************************* Property and types *****************************************/
    /***************************************************************************************************
     * is_modifiable String object_name String release_status_list TCComponent[]
     * ectargetdisposition TCComponent[] changetype String changecat String
     * wfname String group String reasonCode String reasonforchange String
     * additionalcomments String distribution TCComponent[]
     ***************************************************************************************************/
    
    public _EngChangeObjectForm_Data(_EngChangeObjectForm_Panel ecoFormPanel)
    {
        this.ecoForm = ecoFormPanel.ecoForm;
        this.ecoFormPanel = ecoFormPanel;
        map = new HashMap<String, Object>();
        try
        {
            loadForm();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    /*
     * Function : loadForm() Parameters : None Return type : Void Purpose :
     * Loads ECO form by SOA or Object Policy or RAC calls
     */
    public void loadForm() throws TCException
    {
        /*
         * Read Preference variable NOV_load_ECO_Form_By_ObjectPolicy to load
         * ECO form by Object policy Following Valid values define how the ECO
         * form will be loaded 1: Load ECO form by Object Policy 2: Custom
         * Library 3: User Service Otherwise: RAC calls
         */
        TCPreferenceService prefService = ecoForm.getSession().getPreferenceService();
        loadECOFormByObjectPolicy = prefService.getString(TCPreferenceService.TC_preference_site,
                "NOV_DHL_ECO_data_source");
        
        if (loadECOFormByObjectPolicy.equalsIgnoreCase("1")) // Load by Object
                                                             // Policy
        {
            NOVDataManagementServiceHelper dmSOAHelper = new NOVDataManagementServiceHelper(ecoForm);
            
            String[] ecoAttributeList = { "is_modifiable", "object_name", "release_status_list", "ectargetdisposition",
                    "changetype", "wfname", "changecat", "group", "reasoncode", "reasonforchange",
                    "additionalcomments", "nov4_product_line", "distribution", "nov4_ecotgtlifecycledata",
                    "targetsnewlifecycle", "nov4_ecoclassification" };// added
                                                                      // new
                                                                      // attribute
                                                                      // name
                                                                      // -TCDECREL-4471
            
            NOVObjectPolicy ecoPolicy = createObjectPolicy();
            
            TCComponent comp = dmSOAHelper.getObjectProperties(ecoForm, ecoAttributeList, ecoPolicy);
            this.ecoForm = (TCComponentForm) comp;
        }
        
        /*
         * At this point "form" object is either loaded by Object policy; if
         * not, RAC calls will make Server calls; otherwise properties will be
         * fetched from the cache and map will be created
         */
        map.put("is_modifiable", ecoForm.getProperty("is_modifiable"));
        map.put("object_name", ecoForm.getProperty("object_name"));
        map.put("release_status_list", ecoForm.getTCProperty("release_status_list").getReferenceValueArray());
        map.put("ectargetdisposition", ecoForm.getTCProperty("ectargetdisposition").getReferenceValueArray());
        map.put("changetype", ecoForm.getProperty("changetype"));
        map.put("wfname", ecoForm.getProperty("wfname"));
        map.put("changecat", ecoForm.getProperty("changecat"));
        map.put("group", ecoForm.getProperty("group"));
        map.put("reasoncode", ecoForm.getProperty("reasoncode"));
        map.put("reasonforchange", ecoForm.getProperty("reasonforchange"));
        map.put("additionalcomments", ecoForm.getProperty("additionalcomments"));
        map.put("nov4_product_line", ecoForm.getProperty("nov4_product_line"));
        map.put("nov4_ecoclassification", ecoForm.getProperty("nov4_ecoclassification"));// TCDECREL-4471
        map.put("distribution", ecoForm.getTCProperty("distribution").getStringValueArray());
        TCComponent[] comps = ecoForm.getTCProperty("nov4_ecotgtlifecycledata").getReferenceValueArray();
        
        mapTarget = ecoFormPanel.ecoTargetsMap;
        for (int i = 0; i < comps.length; i++)
        {
            Vector<String> vecRevLc = new Vector<String>();
            String itemId = comps[i].getTCProperty("nov4_targetitemid").toString();
            vecRevLc.add(itemId);
            TCComponentItemRevision itemRev = (TCComponentItemRevision) mapTarget.get(itemId);
            String objname = itemRev.getProperty("object_name").toString();
            vecRevLc.add(objname);
            vecRevLc.add(comps[i].getTCProperty("nov4_oldrevision").toString());
            vecRevLc.add(comps[i].getTCProperty("nov4_oldlifecycle").toString());
            vecRevLc.add(comps[i].getTCProperty("nov4_newrevision").toString());
            vecRevLc.add(comps[i].getTCProperty("nov4_newlifecycle").toString());
            mapRevLc.put(comps[i].getTCProperty("nov4_targetitemid").toString(), vecRevLc);
        }
        map.put("nov4_ecotgtlifecycledata", mapRevLc);
        
        String[] uidLifecycleArr = ecoForm.getTCProperty("targetsnewlifecycle").getStringValueArray();
        for (int i = 0; i < uidLifecycleArr.length; i++)
        {
            Vector<String> vecNewRevLc = new Vector<String>();
            String ItemId = " ";
            String ItemName = " ";
            String oldRev = " ";
            String oldLifeCycle = " ";
            String newRev = " ";
            String newLifeCycle = " ";
            String[] uidLifeCycle = uidLifecycleArr[i].split("::");
            if (uidLifeCycle.length == 2)
            {
                TCComponent itemRev = ecoForm.getSession().stringToComponent(uidLifeCycle[0]);
                ItemId = itemRev.getProperty("item_id").toString();
                ItemName = itemRev.getProperty("object_name").toString();
                newRev = itemRev.getProperty("current_revision_id");
                newLifeCycle = uidLifeCycle[1].toString();
                if (mapRevLc.containsKey(ItemId))
                {
                    Vector<String> vectOldVal = mapRevLc.get(ItemId);
                    oldRev = vectOldVal.elementAt(2);
                    oldLifeCycle = vectOldVal.elementAt(3);
                }
            }
            vecNewRevLc.add(ItemId);
            vecNewRevLc.add(ItemName);
            vecNewRevLc.add(oldRev);
            vecNewRevLc.add(oldLifeCycle);
            vecNewRevLc.add(newRev);
            vecNewRevLc.add(newLifeCycle);
            mapNewRevLc.put(ItemId, vecNewRevLc);
        }
        map.put("targetsnewlifecycle", mapNewRevLc);
    }
    
    /*
     * Function : createObjectPolicy() Parameters : None Return type :
     * NOVObjectPolicy Purpose : Creates Object policy to specify which
     * attributes need to be fetched while fetching ECO form
     */
    public NOVObjectPolicy createObjectPolicy()
    {
        if (m_objectPolicy == null)
        {
            m_objectPolicy = new NOVObjectPolicy();
            
            // Add Property Policy for ECO form attributes
            NOVObjectPolicyProperty ecoFormPolicyProps[] = new NOVObjectPolicyProperty[14];
            ecoFormPolicyProps[0] = new NOVObjectPolicyProperty("changetype", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[1] = new NOVObjectPolicyProperty("changecat", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[2] = new NOVObjectPolicyProperty("wfname", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[3] = new NOVObjectPolicyProperty("group", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[4] = new NOVObjectPolicyProperty("reasoncode", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[5] = new NOVObjectPolicyProperty("ectargetdisposition", PolicyProperty.WITH_PROPERTIES);
            ecoFormPolicyProps[6] = new NOVObjectPolicyProperty("reasonforchange", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[7] = new NOVObjectPolicyProperty("additionalcomments", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[8] = new NOVObjectPolicyProperty("nov4_product_line", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[9] = new NOVObjectPolicyProperty("distribution", PolicyProperty.AS_ATTRIBUTE);
            ecoFormPolicyProps[10] = new NOVObjectPolicyProperty("nov4_oldnew_revlc", PolicyProperty.WITH_PROPERTIES);
            ecoFormPolicyProps[11] = new NOVObjectPolicyProperty("nov4_ecotgtlifecycledata",
                    PolicyProperty.WITH_PROPERTIES);
            ecoFormPolicyProps[12] = new NOVObjectPolicyProperty("targetsnewlifecycle", PolicyProperty.WITH_PROPERTIES);
            ecoFormPolicyProps[13] = new NOVObjectPolicyProperty("nov4_ecoclassification", PolicyProperty.AS_ATTRIBUTE);// -4471-Added
                                                                                                                        // for
                                                                                                                        // new
                                                                                                                        // classification
                                                                                                                        // attribute
            m_objectPolicy.addPropertiesPolicy("_DHL_EngChangeForm_", ecoFormPolicyProps);
            
            // Add Property policy for Target Disposition
            NOVObjectPolicyProperty dispPolicyProps[] = new NOVObjectPolicyProperty[2];
            dispPolicyProps[0] = new NOVObjectPolicyProperty("item_id", PolicyProperty.AS_ATTRIBUTE);
            dispPolicyProps[1] = new NOVObjectPolicyProperty("targetitemid", PolicyProperty.AS_ATTRIBUTE);
            m_objectPolicy.addPropertiesPolicy("_DHL_ectargetdisposition_", dispPolicyProps);
            
            // Add Property policy for Lifecycle data from
            // Nov4_DH_ectgtlifecycledata
            NOVObjectPolicyProperty lifecycleProps[] = new NOVObjectPolicyProperty[1];
            lifecycleProps[0] = new NOVObjectPolicyProperty("nov4_targetitemid", PolicyProperty.AS_ATTRIBUTE);
            m_objectPolicy.addPropertiesPolicy("Nov4_DH_ectgtlifecycledata", lifecycleProps);
            
            // Add Property policy for Item ID data from ItemRevision
            NOVObjectPolicyProperty targetProps[] = new NOVObjectPolicyProperty[2];
            targetProps[0] = new NOVObjectPolicyProperty("pid", PolicyProperty.AS_ATTRIBUTE);
            targetProps[1] = new NOVObjectPolicyProperty("item_id", PolicyProperty.AS_ATTRIBUTE);
            m_objectPolicy.addPropertiesPolicy("ItemRevision", targetProps);
        }
        
        return m_objectPolicy;
    }
    
    public Object getProperty(String prop)
    {
        return map.get(prop);
    }
    
    public TCComponentForm getForm()
    {
        return ecoForm;
    }
    
    public String getItemName()
    {
        return (String) getProperty("object_name");
    }
}
