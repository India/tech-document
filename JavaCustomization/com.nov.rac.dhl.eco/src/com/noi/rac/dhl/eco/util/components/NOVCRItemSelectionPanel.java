package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.noi.rac.dhl.eco.util.components.ECOWizardItemTable.NovTreeTableNode;
import com.noi.util.components.NOIJLabel;
import com.nov.rac.dhl.eco.wizard.NOVCreateECOHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Utilities;

public class NOVCRItemSelectionPanel extends JPanel implements IObserver,ISubject,ComponentListener
{
	private static final long serialVersionUID = 1L;
	JButton searchBtn;
	public ECOWizardItemTable targetTable;
	NOVCRSelectionPanel crSelectpanel;
	private ArrayList<IObserver> subList ;
	private JScrollPane trgPane;
	private ItemSearchDialog m_searchDlg = null;
	public String sRevType=null;//7804
	public NOVCRItemSelectionPanel()
	
	{
		subList =  new ArrayList<IObserver>();
		NOIJLabel addItemsLbl = new NOIJLabel("Add Additional Items:");
		searchBtn = new JButton("Search");
		addComponentListener(this);
		searchBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				//ItemSearchDialog searchDlg = ItemSearchDialog.getItemSeachDialog(NOVCRItemSelectionPanel.this);
				try 
                {
					m_searchDlg = new ItemSearchDialog(Utilities.getCurrentFrame(),NOVCRItemSelectionPanel.this);
				} 
				catch (TCException e1) 
				{
					e1.printStackTrace();
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
                m_searchDlg.setVisible(true);
                //7804
                if(crSelectpanel.typeofChange.equalsIgnoreCase("General"))
                {
                	  sRevType= m_searchDlg. getSelectedRevType();//7804
                }
              
                
			}
		});

		NOIJLabel changeItemsLbl = new NOIJLabel("Items Added to this change:");
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();

		targetTable = new ECOWizardItemTable(tcSession);
		targetTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		targetTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		trgPane = new JScrollPane(targetTable);
		//trgPane.setPreferredSize(new Dimension(550, 270));
		trgPane.getViewport().setBackground(Color.white);

		targetTable.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent mouseEvt)
			{
				Object source = mouseEvt.getSource();
				int selRow = ((ECOWizardItemTable)source).getSelectedRow();
				TreeTableNode[] treeNode = ((ECOWizardItemTable)source).getSelectedNodes();
				if (treeNode.length==1)
				{
					if(treeNode[0] instanceof NovTreeTableNode)
					{
						if (!((TreeTableNode)treeNode[0].getParent()).isRoot())
						{
							return;
						}
					}
				}
				if(SwingUtilities.isRightMouseButton(mouseEvt) && (mouseEvt.getClickCount() == 1)&& selRow >0)
				{
					JPopupMenu popupMenu = new JPopupMenu();
					JMenuItem item = new JMenuItem();
					String initialText = "<html><b><font=8 color=red>Remove Target</font></b>";
					item.setText(initialText);
					item.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e )
						{
							//Confirmation dialog
							int row=targetTable.getSelectedRow();
							if(row!=-1)
							{
								/*int response = JOptionPane.showConfirmDialog(null,
										"Do you want to remove the selected target ?", "Confirm",
										JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
								//if ( response == JOptionPane.YES_OPTION )*/
								//{
									TreeTableNode[] selectedNodes = targetTable.getSelectedNodes();
									if (selectedNodes.length==1)
									{
										NovTreeTableNode selNode = (NovTreeTableNode)selectedNodes[0];
										targetTable.removeTarget(selNode);
										
										//Remove part family components of selected component
										Vector<TCComponent> selComps = new Vector<TCComponent>();
										selComps.add(selNode.itemRev);
										
										TCComponent[] partsToRemove = getValidPartFamilyComponents(selComps);
										
										if(partsToRemove != null)
										{
											for(int i=0;i<partsToRemove.length;i++)
											{
												targetTable.removeTarget((TCComponentItemRevision) partsToRemove[i]);
											}
										}
										
										targetTable.updateUI();
										NOVCRItemSelectionPanel.this.notifyObserver();
									}
								//}
							}
						}
					});
					popupMenu.add(item);
					popupMenu.show((java.awt.Component)source, mouseEvt.getX(), mouseEvt.getY());
				}
			}
		});



		JPanel searchItemPanel = new JPanel(new PropertyLayout());
		searchItemPanel.add("1.1.left.center",addItemsLbl);
		searchItemPanel.add("1.2.left.center",searchBtn);

		JPanel trgItemPanel = new JPanel(new PropertyLayout());
		trgItemPanel.add("1.1.left.center",changeItemsLbl);
		trgItemPanel.add("2.1.left.center",trgPane);

		setLayout(new PropertyLayout());
		add("1.1.left.center",searchItemPanel);
		add("2.1.left.center",trgItemPanel);
	}

	public void clearSelectedItems()
	{
		targetTable.removeAllTargets();
	}

	public Object[] getTargetItems()
	{
		Object[] targets = null;

		if (targetTable.targetItems.toArray().length > 0)
		{
			targets =  targetTable.targetItems.toArray();
		}

		return  (Object[]) (targets== null ? new Object[0] : targets);
	}
	
	//TCDECREL-4398 - Method added for getting target items while creating ECO
	public TCComponent[] getTargetComponents()
    {
        TCComponent[] targets = null;

        if (targetTable.targetItems.toArray().length > 0)
        {
            targets =  targetTable.targetItems.toArray(new TCComponent[0]);
        }

        return  (TCComponent[]) (targets== null ? new TCComponent[0] : targets);
    }
    //7804-start
	public String getRevisionType()
	{
		if( m_searchDlg==null)
			return "";
		else
			return (String) m_searchDlg.m_mmrCmb.getSelectedItem();
		
	}
	public Map<String, String> getItemRevTypeMap()
	{
		if( m_searchDlg!=null)
		{
			return m_searchDlg.getItemWithRevTypeMap();
		}
		else if(targetTable !=null)
		{
		    return targetTable.getItemWithRevTypeMap();
		}
		else
		{
		    return null;
		}
		
	}
	//7804-end
	public void setCRSelectionPanel(NOVCRSelectionPanel crSelpanel)
	{
		crSelectpanel = crSelpanel;
	}

	public void update()
	{
		//System.out.println("Item Selection page is getting updated....");

		NOVCRSelectionPanel crSelectionPanel = null;
		for (int i = 0; i < subList.size(); i++)
		{
			IObserver obrPanel =  subList.get(i);
			if (obrPanel instanceof NOVCRSelectionPanel)
			{
				crSelectionPanel = (NOVCRSelectionPanel)obrPanel;
				break;
			}
		}

		if (crSelectionPanel!=null)
		{
			//Map<String, Vector<TCComponent>> itemRevFormMap = crSelectionPanel.buildCRItemRevMap();
			//populateItemTable(itemRevFormMap);
		}

	}

	public void notifyObserver()
	{
		for (int i = 0; i < subList.size(); i++)
		{
			subList.get(i).update();
		}
	}

	public void registerObserver(IObserver subs)
	{
		subList.add(subs);
	}

	public void removeObserver(IObserver subs)
	{

	}
    //7804-aded one more input argument for revType to the below function call
	public void populateItemTable(Map<String, Vector<TCComponent>> itemRevFormMap, String sRevType)
	{
		Set<String> targetItemRevs = targetTable.itemRevCRMap.keySet();
		TCComponent[] partFamilyComponents;//5353
		Set<String> crsItemRevs = itemRevFormMap.keySet();
		Object[] crItemObjs = crsItemRevs.toArray();
		Arrays.sort(crItemObjs);          //TCDECREL-4795
		
		//removing the targets if any CR has been removed
				//Set<String> afterAddnCRItemRevs = targetsTable.itemRevCRMap.keySet();
				Object[] afCrItemObjs = targetItemRevs.toArray();
				for (int i = 0; i < crItemObjs.length; i++)
				{
					for (int j = 0; j < afCrItemObjs.length; j++)
			 		{
						if (!crsItemRevs.contains(afCrItemObjs[j]))
						{
							Vector<TCComponent> compVec = targetTable.itemRevCRMap.get(afCrItemObjs[j]);
							//5353-start-removing part family 
							if(compVec !=null)
							{
								Vector<TCComponent> vectItemToAdd = new Vector<TCComponent>() ;
						 		vectItemToAdd.add(compVec.get(0));	
								partFamilyComponents= getValidPartFamilyComponents(vectItemToAdd);
								if(partFamilyComponents.length!=0)
								{
									for(int iCount=0;iCount<partFamilyComponents.length;iCount++)
									{
										targetTable.removeTarget((TCComponentItemRevision) partFamilyComponents[iCount]);
									}
								}
							}
							//5353-end
						//we need to remove the target
							if(compVec != null)
							{
								targetTable.removeTarget((TCComponentItemRevision)compVec.get(0), (TCComponentForm)compVec.get(1));
								updateTableUI();
								//targetTable.updateUI();
							}
						}
					}
				}
				if (crItemObjs.length == 0)
				{
					for (int j = 0; j < afCrItemObjs.length; j++)
			 		{

			 			Vector<TCComponent> compVec = targetTable.itemRevCRMap.get(afCrItemObjs[j]);
			 			Vector<TCComponent> vectItemToAdd = new Vector<TCComponent>() ;
			 			vectItemToAdd.add(compVec.get(0));
			 			//5353-start-removing part family 
						partFamilyComponents= getValidPartFamilyComponents(vectItemToAdd);
						if(partFamilyComponents.length!=0)
						{
							for(int iCount=0;iCount<partFamilyComponents.length;iCount++)
							{
								targetTable.removeTarget((TCComponentItemRevision) partFamilyComponents[iCount]);
							}
						}
						//5353-end
						//we need to remove the target
			 			if(compVec != null)
			 			{
							targetTable.removeTarget((TCComponentItemRevision)compVec.get(0), (TCComponentForm)compVec.get(1));
							updateTableUI();
							//targetTable.updateUI();
			 			}
					}
				}


		//Adding the targets
		//TCDECREL-4795 : Start
		for (int i = 0; i < crItemObjs.length; i++)
		{
			if (!targetItemRevs.contains(crItemObjs[i]))
			{
			    String strItemid = null;
				Vector<TCComponent> compVec = itemRevFormMap.get(crItemObjs[i]);
				try
                {
					strItemid = compVec.get(0).getProperty("item_id");
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
				//if(!targetTable.strTargetItemIds.contains(strItemid))
				//{
                //7804-added new function call addTargetsToItemsTable  only for general process
                if(crSelectpanel.typeofChange.equalsIgnoreCase("General"))
                {
                	targetTable.addTargetsToItemsTable((TCComponentItemRevision)compVec.get(0), (TCComponentForm)compVec.get(1),sRevType);
                }
                else
                {
                	targetTable.addTarget((TCComponentItemRevision)compVec.get(0), (TCComponentForm)compVec.get(1));
				 
                }
    			updateTableUI();
				//}
				//targetTable.updateUI();
			}
		}	
		//TCDECREL-4795 : End

	}
	//5353-start
	public void RemoveItemTable()
	{
		Set<String> targetItemRevs = targetTable.itemRevCRMap.keySet();
				
		if(targetItemRevs.size()!=0)
		{
			 TCComponent[] partFamilyComponents;
			
			//removing the targets if any CR has been removed
					
					Object[] addedCrItemObjs = targetItemRevs.toArray();
					if(addedCrItemObjs.length!=0)
					{
					for (int j = 0; j < addedCrItemObjs.length; j++)
				 		{
				 			Vector<TCComponent> compVector = targetTable.itemRevCRMap.get(addedCrItemObjs[j]);
				 			if(compVector !=null)
				 			{
					 			Vector<TCComponent> vectItemToAdd = new Vector<TCComponent>() ;
					 			vectItemToAdd.add(compVector.get(0));
					 			partFamilyComponents= getValidPartFamilyComponents(vectItemToAdd);
								if(partFamilyComponents.length!=0)
								{
									for(int iCount=0;iCount<partFamilyComponents.length;iCount++)
									{
										targetTable.removeTarget((TCComponentItemRevision) partFamilyComponents[iCount]);
									}
								}
				 			}			 			
							// remove the target
				 			if(compVector != null)
				 			{
								targetTable.removeTarget((TCComponentItemRevision)compVector.get(0), (TCComponentForm)compVector.get(1));
								updateTableUI();
								
				 			}
						}
					}
		}
						
}
	//5353-end
	//TCDECREL-4795 : Start
	//7804-added one more input argument revisionType to the below function call
	public Map<String, Vector<TCComponent>> updateItemRevCRMap(Map<String, Vector<TCComponent>> itemRevFormMap,String sRevisionType)
    {
		sRevType=sRevisionType;//7804-assigning revisiontype value
		Vector<TCComponent> partFamilyVect = new Vector<TCComponent>();
	    Set<String> crsItemRevs = itemRevFormMap.keySet();
	   
	    if( crsItemRevs.size()<=0 )
	    	return itemRevFormMap;
	    
        String[] crItemObjs = crsItemRevs.toArray(new String[crsItemRevs.size()]);
        
	    try
        {
	    	for (int i = 0; i < crItemObjs.length; i++)
	    	{
	    		String strECR = null;
	    		Vector<TCComponent> vectItemToAdd = new Vector<TCComponent>() ;

	    		Vector<TCComponent> compVec = itemRevFormMap.get(crItemObjs[i]);
	    		vectItemToAdd.add(compVec.get(0));
	    		TCComponent formObject = compVec.get(1);

	    		strECR = formObject.getTCProperty("object_name").getStringValue();
	    		
	    		if(partFamilyVect.contains(compVec.get(0)) == false)
	    		{
	    			//7804-added one more input argument revisionType to the below function
	    			TCComponent[] validCRItemsFamily = getValidPartFamilyComponents(vectItemToAdd);
                    if(validCRItemsFamily.length == 0)//7804-remove the item from the map if validation fails
                    {
                    	itemRevFormMap.remove(crItemObjs[i]);
                    }
	    			Collections.addAll(partFamilyVect,validCRItemsFamily);

	    			for(int inx=0;inx<validCRItemsFamily.length;inx++)
	    			{
	    				TCComponent revObject = validCRItemsFamily[inx];
	    				String crStr = null;
	    				if(compVec.get(0) != revObject)
	    				{
	    					String sRevObj = revObject.getTCProperty("object_string").getStringValue();		    			
	    					crStr = sRevObj+"::"+ strECR;
	    					if((!isAlreadyAdded(crItemObjs,sRevObj)))
	    					{
	    						Vector<TCComponent> tarCompVec = new Vector<TCComponent>();
	    						tarCompVec.add(0, (TCComponent) revObject);
	    						tarCompVec.add(1, null);
	    						itemRevFormMap.put(crStr, tarCompVec);
	    						
	    					}
	    				}
	    			}
	    		}

	    	}
        }
	    catch (TCException e1)
	    {
	    	// TODO Auto-generated catch block
	    	e1.printStackTrace();
	    }

	    return itemRevFormMap;
	    
    }
	//TCDECREL-4795 : End
	private boolean isAlreadyAdded(String[] crItemObjs, String sValue)
	{
		boolean isAdded = false;
		for(int i=0;i<crItemObjs.length;i++)
		{
			if(crItemObjs[i].contains(sValue))
			{
				isAdded = true;
				break;
			}
		}
		return isAdded;
	}
	   
    public void componentHidden(ComponentEvent e){}

	public void componentMoved(ComponentEvent e) {}

	public void componentResized(ComponentEvent e)
	{
		Dimension dim = NOVCRItemSelectionPanel.this.getSize();
		int ht = dim.height;
		int wt = dim.width;
		trgPane.setPreferredSize(new Dimension(wt,ht));
		trgPane.updateUI();
		trgPane.revalidate();
		trgPane.repaint();
		this.updateUI();
		this.revalidate();
		this.repaint();
	}

	public void componentShown(ComponentEvent e) {}

	public void updateTableUI()
	{
		try
        {
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
					targetTable.updateUI();
				}
            });
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvocationTargetException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	public TCComponent[] getValidPartFamilyComponents(Vector<TCComponent> vectSelCom)
	{
		Map<String, String> mapECOProperty = new HashMap<String, String>();
        TCComponent[] validatedItems = null;
        
        String changeType = crSelectpanel.typeofChange;
        mapECOProperty.put("changetype", changeType);
                        
        TCComponent[] selectedComps = vectSelCom.toArray(new TCComponent[vectSelCom.size()]);
                    
        NOVCreateECOHelper helperObj = new NOVCreateECOHelper();
        validatedItems = helperObj.validateTargetsToECO(selectedComps, mapECOProperty,sRevType);//7804-added null argument-need to add the minor/major rev type instead of null
        
        return validatedItems;
	}
}
