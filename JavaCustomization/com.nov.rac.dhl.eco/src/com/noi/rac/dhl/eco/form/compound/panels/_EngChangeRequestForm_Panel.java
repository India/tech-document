package com.noi.rac.dhl.eco.form.compound.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import com.noi.rac.dhl.eco.form.compound.data._EngChangeRequestForm_DC;
import com.noi.rac.dhl.eco.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.dhl.eco.util.components.ECRTargetsTable;
import com.noi.util.components.NOIJLabel;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.iTextField;

public class _EngChangeRequestForm_Panel extends PrintablePanel
{
    private static final long       serialVersionUID     = 1L;
    public Registry                 reg;
    public JPanel                   partDocInfoPanel;
    public TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
    ECRTargetsTable partDocInfoTable;
    public _EngChangeRequestForm_Panel()
    {
        super();
    }

    public void initialize()
    {
    }

    public void createUI(INOVCustomFormProperties formProps )
    {
        reg = Registry.getRegistry(this);
        novFormProperties = formProps;
        partDocInfoPanel = new JPanel();
        partDocInfoPanel.setBorder(new TitledBorder("Teamcenter Part/Document Information"));
        partDocInfoTable = new ECRTargetsTable(tcSession);        
        partDocInfoTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        JScrollPane partDocInfoTablePane = new JScrollPane(partDocInfoTable);
        partDocInfoTablePane.setBackground(Color.WHITE);
        partDocInfoTablePane.setPreferredSize(new Dimension(600 , 100));
        partDocInfoPanel.add(partDocInfoTablePane);
        partDocInfoPanel.setPreferredSize(new Dimension(620 , 140));
        loadECRTargets();
        
        JPanel crInfoPanel = getInitPanel();
        JPanel closurePanel = null; //Rakesh - Closure Panel
        
        crInfoPanel.setBorder(new TitledBorder("Change Request Information"));
        //crInfoPanel.setPreferredSize(new Dimension(620 , 480));//soma
        crInfoPanel.setPreferredSize(new Dimension(620 ,620));//soma
        
        SuportingFilesPanel files = new SuportingFilesPanel(novFormProperties, false, this);
        files.theTable.setEnabled(false);
        files.attachmentsBoxMinus.setEnabled(false);
        files.attachmentsBoxPlus.setEnabled(false);
        files.viewAttachment.setEnabled(false);
        files.tablePane.setPreferredSize(new Dimension(600 , 100));
        files.setPreferredSize(new Dimension(620,180));
        TitledBorder tb = new javax.swing.border.TitledBorder("Attachments");
        tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        tb.setTitleColor(Color.BLACK);
        files.setBorder(tb);
        //files.getBorder().
               
        this.setLayout(new PropertyLayout(0 , 0 , 0 , 0 , 0 , 0));
        add("1.1.left.center", partDocInfoPanel);
        add("2.1.left.center", crInfoPanel);
        add("3.1.left.center", files);
        
        //Rakesh - DHL-ECO - Create Closure Comments panel if ECR is closed
        if(((iTextField)novFormProperties.getProperty("ecr_status")).getText().equalsIgnoreCase("Stopped") ||
        		((iTextField)novFormProperties.getProperty("ecr_status")).getText().equalsIgnoreCase("Closed"))
        {
        	closurePanel = getClosurePanel();
        	TitledBorder tbClosurePanel = new javax.swing.border.TitledBorder("Closure Comments");
            tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
            tb.setTitleColor(Color.BLACK);
            closurePanel.setBorder(tbClosurePanel);
        	add("4.1.left.center", closurePanel);
        	this.setPreferredSize(new Dimension(680 , 1140));
        }
        else
        {
	       // this.setPreferredSize(new Dimension(680 , 800));//soma
	        this.setPreferredSize(new Dimension(680 , 1020));//soma
        }
    }

    private void loadECRTargets()
    {
        try
        {
            String[] type = new String[]{"Non-Engineering Revision","Nov4Part Revision","Documents Revision"};
            String[] relation =new String[] {"RelatedECN"};
            System.out.println(novFormProperties.getForm());
            AIFComponentContext[] comps =  novFormProperties.getForm().whereReferencedByTypeRelation(type,relation );
            for ( int i = 0; i < comps.length; i++ )
            {
                InterfaceAIFComponent infcomp = comps[i].getComponent();
                
                if ( infcomp instanceof TCComponentItemRevision )
                {
                    partDocInfoTable.addTarget((TCComponentItemRevision)infcomp);
                }
            }
        }
        catch ( TCException e )
        {
            e.printStackTrace();
        }
        
    }

    private JPanel getInitPanel()
    {
        JPanel iniPanel = new JPanel(new PropertyLayout());
        NOIJLabel ecrNoLbl = new NOIJLabel("ECR Number:");
        ecrNoLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel ecrStatLbl = new NOIJLabel("ECR Status:");
        ecrStatLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel requesterLbl = new NOIJLabel("Requestor:");
        requesterLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel reqDateLbl = new NOIJLabel("Request Date:");
        reqDateLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel productLine = new NOIJLabel("Product Line:");
        productLine.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel typOfChangLbl = new NOIJLabel("Type of Change:");
        typOfChangLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel reason4ChangeLbl = new NOIJLabel("Reason For Change:");
        reason4ChangeLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel chngDescLbl = new NOIJLabel("Change Description:");
        chngDescLbl.setPreferredSize(new Dimension(200 , 60));
        NOIJLabel apprGrpLbl = new NOIJLabel("Approval group:");
        apprGrpLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel apprLbl = new NOIJLabel("Approvers:");
        apprLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel apprDateLbl = new NOIJLabel("Approve Date:");
        apprDateLbl.setPreferredSize(new Dimension(200 , 21));
        NOIJLabel emailDistrLbl = new NOIJLabel("Email Distribution:");
        emailDistrLbl.setPreferredSize(new Dimension(200 , 40));
        NOIJLabel commentsLbl = new NOIJLabel("Comments:");
        commentsLbl.setPreferredSize(new Dimension(200 , 60));
        
        
        
        iTextField ecrNoTextField1 = ((iTextField)novFormProperties.getProperty("object_name"));
        ecrNoTextField1.setPreferredSize(new Dimension(150 , 21));
        ecrNoTextField1.setEditable(false);
        iTextField ecrStatTextField1 = ((iTextField)novFormProperties.getProperty("ecr_status"));
        ecrStatTextField1.setPreferredSize(new Dimension(250 , 21));
        ecrStatTextField1.setEditable(true);//soma
        iTextField requesterTextField1 = ((iTextField)novFormProperties.getProperty("requested_by"));
        requesterTextField1.setPreferredSize(new Dimension(250 , 21));
        requesterTextField1.setEditable(false);
     
        iTextField reqDateTextField1 = ((iTextField)novFormProperties.getProperty("requested_date"));
        reqDateTextField1.setPreferredSize(new Dimension(250 , 21));
        reqDateTextField1.setEditable(false);
        iTextField productLIneTextField1 = ((iTextField)novFormProperties.getProperty("nov4_product_line"));
        productLIneTextField1.setPreferredSize(new Dimension(250 , 21));
        productLIneTextField1.setEditable(false);
        iTextField typOfChangTextField1 = ((iTextField)novFormProperties.getProperty("change_type"));
        typOfChangTextField1.setPreferredSize(new Dimension(250 , 21));
        typOfChangTextField1.setEditable(true);//soma
        iTextField reason4ChangeTextField1 = ((iTextField)novFormProperties.getProperty("reason_for_change"));
        reason4ChangeTextField1.setPreferredSize(new Dimension(250 , 21));
        reason4ChangeTextField1.setEditable(false);
        iTextArea chngDescTextArea = ((iTextArea)novFormProperties.getProperty("change_desc"));
        chngDescTextArea.setEditable(false);
        chngDescTextArea.setLineWrap(true);//TCDECREL - 3411
        chngDescTextArea.setWrapStyleWord(true);//TCDECREL - 3411
        JScrollPane chngDescAreaPane = new JScrollPane(chngDescTextArea);
        //chngDescAreaPane.setPreferredSize(new Dimension(350 , 60));//soma
        chngDescAreaPane.setPreferredSize(new Dimension(350 , 100));//soma
        //chngDescAreaPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        iTextField apprGrpTextField1 = ((iTextField)novFormProperties.getProperty("group"));
        apprGrpTextField1.setEditable(false);
        apprGrpTextField1.setPreferredSize(new Dimension(250 , 21)); //TCDECREL - 3411 changed textfield length from 150 to 250
        iTextField apprTextField1 = ((iTextField)novFormProperties.getProperty("disposition_by"));
        apprTextField1.setEditable(false);
        apprTextField1.setPreferredSize(new Dimension(250 , 21));
        apprTextField1.setToolTipText(apprTextField1.getText());
        iTextField apprDateTextField1 = (iTextField)novFormProperties.getProperty("decision_date");
        apprDateTextField1.setEditable(false);
        apprDateTextField1.setPreferredSize(new Dimension(250 , 21));
        JScrollPane emailDistrListPane = new JScrollPane(((JList)novFormProperties.getProperty("distribution")));
        emailDistrListPane.setPreferredSize(new Dimension(200 , 80));
        iTextArea commentsTextArea = ((iTextArea)novFormProperties.getProperty("comments"));
        commentsTextArea.setEditable(true);//soma
        commentsTextArea.setLineWrap(true);//TCDECREL - 3411
        commentsTextArea.setWrapStyleWord(true);//TCDECREL - 3411
        JScrollPane commentsAreaPane = new JScrollPane(commentsTextArea);
       // commentsAreaPane.setPreferredSize(new Dimension(350 , 60));//soma
        commentsAreaPane.setPreferredSize(new Dimension(350 , 120));
        
        JButton viewPdf = new JButton("View PDF");
        viewPdf.addActionListener(new EngChangePanelListener(novFormProperties));
        /*viewPdf.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent acEvt) 
            {
                try 
                {
                    AIFComponentContext[] comps = novFormProperties.getForm().getSecondary();
                    boolean isPDFAttached = false;
                    if (comps!=null && comps.length>0) 
                    {
                        for (int i = 0; i < comps.length; i++) 
                        {
                            InterfaceAIFComponent compToOpen = comps[i].getComponent();
                            if( compToOpen instanceof TCComponentDataset)
                            {
                                //System.out.println(((TCComponentDataset)compToOpen).getType());
                                if(((TCComponentDataset)compToOpen).getType().equals("PDF"))
                                {
                                    AbstractAIFCommand abstractaifcommand = novFormProperties.getForm().getSession()
                                    .getOpenCommand(new Object[]{ AIFUtility.getActiveDesktop(), compToOpen });
                                    abstractaifcommand.executeModeless();
                                    isPDFAttached = true;
                                }
                            }
                        }
                    }
                    if (!isPDFAttached) 
                    {
                        MessageBox.post("No PDF Attachecd to "+novFormProperties.getForm().toString(), "Info", MessageBox.INFORMATION);    
                    }
                }
                catch (Exception e) 
                {
                    e.printStackTrace();
                }
            }
        });  */      
        
        iniPanel.setPreferredSize(new Dimension(600 , 425));
        iniPanel.add("1.1.left.center", ecrNoLbl);
        iniPanel.add("1.2.left.center", ecrNoTextField1);
        iniPanel.add("1.3.left.center", viewPdf);
        iniPanel.add("2.1.left.center", ecrStatLbl);
        iniPanel.add("2.2.left.center", ecrStatTextField1);
        iniPanel.add("3.1.left.center", requesterLbl);
        iniPanel.add("3.2.left.center", requesterTextField1);
        iniPanel.add("4.1.left.center", reqDateLbl);
        iniPanel.add("4.2.left.center", reqDateTextField1);
        iniPanel.add("5.1.left.center", productLine);
        iniPanel.add("5.2.left.center", productLIneTextField1);
        iniPanel.add("6.1.left.center", typOfChangLbl);
        iniPanel.add("6.2.left.center", typOfChangTextField1);
        iniPanel.add("7.1.left.center", reason4ChangeLbl);
        iniPanel.add("7.2.left.center", reason4ChangeTextField1);
        iniPanel.add("8.1.left.center", chngDescLbl);
        iniPanel.add("8.2.left.center", chngDescAreaPane);
        iniPanel.add("9.1.left.center", apprGrpLbl);
        iniPanel.add("9.2.left.center", apprGrpTextField1);
        iniPanel.add("10.1.left.center", apprLbl);
        iniPanel.add("10.2.left.center", apprTextField1);
        iniPanel.add("11.1.left.center", apprDateLbl);
        iniPanel.add("11.2.left.center", apprDateTextField1);
        iniPanel.add("12.1.left.center", emailDistrLbl);
        iniPanel.add("12.2.left.center", emailDistrListPane);
        iniPanel.add("13.1.left.center", commentsLbl);
        iniPanel.add("13.2.left.center", commentsAreaPane);
        return iniPanel;
    }
    
    //Rakesh - DHL-ECO - ECR Closure Panel - Starts here
    private JPanel getClosurePanel()
    {
        JPanel closurePanel = new JPanel(new PropertyLayout());
        
        //Create Closed By: Field
        NOIJLabel closedByLbl = new NOIJLabel("ECR Closed by:");
        closedByLbl.setPreferredSize(new Dimension(200 , 21));
        iTextField closedByTextField = ((iTextField)novFormProperties.getProperty("nov4_ecrclosed_by"));
        closedByTextField.setPreferredSize(new Dimension(250 , 21));
        closedByTextField.setEditable(false);
        
        //Create Closure Comments: Field
        NOIJLabel commentsLbl = new NOIJLabel("Closure Comments:");
        commentsLbl.setPreferredSize(new Dimension(200 , 21));        
        iTextArea commentsTextArea = ((iTextArea)novFormProperties.getProperty("nov4_closure_comments"));
        commentsTextArea.setEditable(false);
        JScrollPane commentsAreaPane = new JScrollPane(commentsTextArea);
        commentsAreaPane.setPreferredSize(new Dimension(350 , 120));
        commentsAreaPane.setEnabled(false);
        
        //Add the fields to the Panel
        closurePanel.setPreferredSize(new Dimension(620 , 180));
        closurePanel.add("1.1.left.center", closedByLbl);
        closurePanel.add("1.2.left.center", closedByTextField);
        closurePanel.add("2.1.left.center", commentsLbl);
        closurePanel.add("2.2.left.center", commentsAreaPane);  
        
        return closurePanel;    
    }
    //Rakesh - DHL-ECO - ECR Closure Panel - Ends here

    public void saveForm()
    {
        ((_EngChangeRequestForm_DC) novFormProperties).saveFormData();
    }


}
