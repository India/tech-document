package com.noi.rac.dhl.eco.masschange.dialogs;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.eclipse.swt.widgets.Display;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.util.components.MassBOMChangeDialog;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.VerticalLayout;

/**
 * @author kabades
 * 
 */
public class MassChangeDialog extends AbstractAIFDialog implements ActionListener
{
    
    private static final long serialVersionUID = 1L;
    private _EngChangeObjectForm_Panel m_ecoFormPanel;
    private ButtonGroup m_btnGroup;
    private JRadioButton m_massBOMRadioButton;
    private JRadioButton m_massRDRadioButton;
    private JButton m_nextBtn;
    private JButton m_cancelBtn;
    private final int DIALOG_WIDTH = 270;
    private final int DIALOG_HEIGHT = 150;
    private final int X_AXIS_DIALOG_SCREEN_VALUE = 250;
    private final int Y_AXIS_DIALOG_SCREEN_VALUE = 250;
    
    public MassChangeDialog(_EngChangeObjectForm_Panel ecoFormPanel)
    {
        super(AIFUtility.getActiveDesktop());
        m_ecoFormPanel = ecoFormPanel;
        setModal(true);
        setMinimumSize(new Dimension(DIALOG_WIDTH, DIALOG_HEIGHT));
        setTitle("Mass Change");
        createUI();
        addListeners();
    }
    
    /**
	 */
    private void createUI()
    {
        final int fontSize = 12;
        Font labelFont = new Font("Dialog", 1, fontSize);
        JPanel radioPanel = new JPanel(new PropertyLayout(5, 0, 5, 5, 5, 5));
        m_massBOMRadioButton = new JRadioButton("Mass BOM Edit");
        m_massBOMRadioButton.setActionCommand("MassBOMEdit");
        m_massBOMRadioButton.setFont(labelFont);
        m_massBOMRadioButton.setSelected(true);
        m_massRDRadioButton = new JRadioButton("Mass Related Document Edit");
        m_massRDRadioButton.setActionCommand("MassRDEdit");
        m_massRDRadioButton.setFont(labelFont);
        
        m_btnGroup = new ButtonGroup();
        m_btnGroup.add(m_massBOMRadioButton);
        m_btnGroup.add(m_massRDRadioButton);
        
        radioPanel.add("1.1.left.center", m_massBOMRadioButton);
        radioPanel.add("2.1.left.center", m_massRDRadioButton);
        
        JPanel btnPanel = new JPanel(new PropertyLayout(5, 5, 5, 5, 5, 5));
        m_nextBtn = new JButton("Next");
        m_nextBtn.setFont(labelFont);
        m_cancelBtn = new JButton("Cancel");
        m_cancelBtn.setFont(labelFont);
        
        btnPanel.add("1.1.right.center", m_nextBtn);
        btnPanel.add("1.2.right.center", m_cancelBtn);
        setLayout(new VerticalLayout(5, 5, 5, 5, 5));
        add("top.nobind.left.top", radioPanel);
        add("top.nobind.right.top", btnPanel);
    }
    
    private void addListeners()
    {
        m_nextBtn.addActionListener(this);
        m_cancelBtn.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource().equals(m_nextBtn))
        {
            if (getSelection(m_btnGroup).getActionCommand().toString().compareTo("MassBOMEdit") == 0)
            {
                MassBOMChangeDialog MBC = new MassBOMChangeDialog(m_ecoFormPanel.getECOTargetPanel());
                int x = m_nextBtn.getLocationOnScreen().x - X_AXIS_DIALOG_SCREEN_VALUE;
                int y = m_nextBtn.getLocationOnScreen().y - Y_AXIS_DIALOG_SCREEN_VALUE;
                MBC.setLocation(x, y);
                this.dispose();
                MBC.setVisible(true);
            }
            else
            {
                this.dispose();
                Display.getDefault().asyncExec(new Runnable()
                {
                    public void run()
                    {
                        MassRDChangeComposite massChDlg = new MassRDChangeComposite(AIFUtility.getCurrentApplication()
                                .getDesktop().getShell(), m_ecoFormPanel);
                        massChDlg.open();
                    }
                });
                // this.setVisible(false);
                // this.dispose();
            }
        }
        else if (e.getSource().equals(m_cancelBtn))
        {
            this.dispose();
        }
    }
    
    /**
     * @param group
     * @return
     */
    private JRadioButton getSelection(ButtonGroup group)
    {
        for (Enumeration<AbstractButton> e = group.getElements(); e.hasMoreElements();)
        {
            JRadioButton b = (JRadioButton) e.nextElement();
            if (b.getModel() == group.getSelection())
            {
                return b;
            }
        }
        return null;
    }
}
