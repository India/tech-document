package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.form.compound.util.CheckBoxHeader;
import com.noi.rac.dhl.eco.form.compound.util.CheckBoxHeaderListener;
import com.noi.util.components.NOIJLabel;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.quicksearch.services.utils.Utils;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class MassBOMChangeDialog extends AbstractAIFDialog implements INOVSearchProvider
{
	private static final long 					serialVersionUID = 1L;
	
	/* Default Width and Height of the Dialog */
	private  	final int 						m_defaultHeight = 450;
	private  	final int 						m_defaultWidth = 600;	
	
	private 	JButton 						m_itemChSrBtn;
	private 	JButton 						m_repItemSrBtn;
	private 	JButton 						m_okBtn;
	private 	JButton 						m_applyBtn;
	private 	JButton 						m_clearBtn;
	private 	JButton 						m_cancelBtn;
	private 	JButton							m_filterBtn;
	private 	JRadioButton 					m_replItemStrcBtn;
	private 	JRadioButton 					m_delItemStrcBtn;
	private 	JRadioButton 					m_addItemECOBtn;
	private     JComboBox                       m_mmrComboBox;
	private 	ButtonGroup 					m_btnGroup;	
	private 	NOIJLabel 						m_lblWhrUsed;
	
	private 	AIFTableModel 					m_whrUsedtblmodel;
	private 	JTable 							m_whrUsedTable;
	private		TableRowSorter<AIFTableModel> 	m_whrUsedTableSorter;
	private 	TCComponent[] 					m_whereUsedItems; 
	private 	Map<String, String> 			m_whrUsedItemsMap;	
	private 	Set<String> 					m_whrUsdItemSelected;
	private 	List<String> 					m_nonEditableItemList;
	private		Map<String, String>				m_nonEditableItemMap;
	
	private 	TCSession 						m_session;
	private 	Registry 						m_registry;
	private 	TCComponentItemRevision 		m_itemRevWhereUsed;	
	private		boolean							m_itemRevWhereUsedChanged;
	
	private 	String							m_bomUpdateFailedItems;
	private 	boolean 						m_isValidRevAdded;
	private		boolean 						m_operationComplete;
	private 	TCComponentGroup 				m_userGroup;
	private		NOVWhereUsedFilterCriteria		m_filterCriteria;
	private		int[]							m_rowSelection;	
	
	public 		JTextField 						m_txtItemChange;
	public 		JTextField 						m_txtRepItem;
	public 		TCComponentItemRevision 		m_itemToChange;
	public 		TCComponentItemRevision 		m_replacementItem;	
	public 		_EngChangeObjectForm_Panel 		m_ecoFormPanel;	
	
	private		Vector<String> 					m_whereUsedItemsRowData;
	private		int								m_whereUsedItemsRowCount = 0;
	private 	int								m_whereUsedItemsColCount = 0;	
			
	public MassBOMChangeDialog(NOVECOTargetItemsPanel targetItemSelectionPanel) 
	{
		super(AIFUtility.getActiveDesktop());
		m_registry = Registry.getRegistry(this);
		m_ecoFormPanel = targetItemSelectionPanel.ecoFormPanel;
		m_session = m_ecoFormPanel.getSession();
		m_userGroup = m_session.getCurrentGroup();
		
		m_nonEditableItemList 	= new ArrayList<String>();
		m_nonEditableItemMap	= new HashMap<String, String>();
		m_whrUsedItemsMap 		= new HashMap<String, String>();
		m_whrUsdItemSelected 	= new HashSet<String>();
		m_filterCriteria 		= new NOVWhereUsedFilterCriteria();
		
		createUI();		
	}	
	
	private void createUI() 
	{
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		/* Create and Add WhereUsed Search panel */
		JPanel itemSrPanel = createWhrUsedTablePanel();	
		mainPanel.add(itemSrPanel);	
		
		/* Create and Add Radio Buttons panel */		
		JPanel rbPanel = createRadioButtonPanel();	
		mainPanel.add(rbPanel);
		
		/* Create and Add OK/Cancel/Apply button panel */		
		JPanel btnPanel = createButtonPanel();
		addButtonListeners();
		mainPanel.add(btnPanel);
		
		JScrollPane mainPanelScrollPane = new JScrollPane(mainPanel);
		getContentPane().add(mainPanelScrollPane);
		setTitle("Mass BOM Edit");
		//Set minimum dimensions for the dialog
		setMinimumSize(new Dimension(m_defaultWidth, m_defaultHeight));
		pack();		
		setModal(true);		
		
		enableButtons();
	}
	
	private JPanel createWhrUsedTablePanel()
	{
		ImageIcon searchIcon = m_registry.getImageIcon("search.ICON");
		NOIJLabel lblItemChange = new NOIJLabel("Component Item :");
		final	int txtItemChangeSize = 30;
		m_txtItemChange = new JTextField(txtItemChangeSize);
		m_txtItemChange.setEditable(false);
		
		m_itemChSrBtn = new JButton();
		final	int itemChSrBtnSize = 20;
		m_itemChSrBtn.setPreferredSize(new Dimension(itemChSrBtnSize,itemChSrBtnSize));
		m_itemChSrBtn.setIcon(searchIcon);
		
		m_filterBtn = new JButton();
		m_filterBtn.setText("Filter");
		m_filterBtn.setEnabled(false);
		
		JPanel compPanel = new JPanel();
		compPanel.add(lblItemChange);
		compPanel.add(m_txtItemChange);
		compPanel.add(m_itemChSrBtn);
		compPanel.add(m_filterBtn);
		
		m_lblWhrUsed = new NOIJLabel("Where Used Report :");
		m_lblWhrUsed.setAlignmentY(CENTER_ALIGNMENT);
		
		/* Creating WhereUsed Table and Model */
		createWhereUsedTable();
		createWhereUsedTableSorter();
		assignColumnRenderer();		
		setColumnWidths();

		/* Adding WhereUsed table to WhereUsed pane */
		final int whrUsedPaneWidth = 400;
		final int whrUsedPaneHeight = 226;
		JScrollPane whrUsedPane = new JScrollPane(m_whrUsedTable);
		whrUsedPane.setPreferredSize(new Dimension(whrUsedPaneWidth, whrUsedPaneHeight));
		whrUsedPane.setBorder(BorderFactory.createBevelBorder(1));
		
		JPanel itemSrPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;		
		itemSrPanel.add(compPanel, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;		
		itemSrPanel.add(m_lblWhrUsed, c);
				
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.BOTH;		
		itemSrPanel.add(whrUsedPane, c);
		
		return itemSrPanel;		
	}
	
	private JPanel createRadioButtonPanel()
	{
		final int fontSize = 12;
		Font labelFont = new Font("Dialog", 1, fontSize);
		ImageIcon searchIcon = m_registry.getImageIcon("search.ICON");
		
		m_replItemStrcBtn = new JRadioButton();
		m_replItemStrcBtn.setActionCommand("ReplaceItem");
		m_replItemStrcBtn.setText("Replace Item in BOM with ");
		m_replItemStrcBtn.setFont(labelFont);
		
		final int txtRepItemSize = 20;
		m_txtRepItem = new JTextField(txtRepItemSize);
		m_txtRepItem.setEditable(false);
		
		m_repItemSrBtn = new JButton();
		final	int repItemSrBtnSize = 20;
		m_repItemSrBtn.setPreferredSize(new Dimension(repItemSrBtnSize,repItemSrBtnSize));
		m_repItemSrBtn.setIcon(searchIcon);
		m_repItemSrBtn.setEnabled(false);
		
		m_delItemStrcBtn = new JRadioButton();
		m_delItemStrcBtn.setActionCommand("DeleteItem");		
		m_delItemStrcBtn.setText("Remove Item from BOM");
		m_delItemStrcBtn.setFont(labelFont);
		
		m_addItemECOBtn = new JRadioButton();
		m_addItemECOBtn.setActionCommand("AddItem");		
		m_addItemECOBtn.setText("Add Parent to ECO (No BOM Change)");
		m_addItemECOBtn.setFont(labelFont);
		
		//TCDECREL-7805: Start
		m_mmrComboBox = new JComboBox();
		m_mmrComboBox.addItem("Create Major Revision");
		m_mmrComboBox.addItem("Create Minor Revision");
		m_mmrComboBox.setFont(labelFont);
		//TCDECREL-7805: End
		
		final int topMargin = 5;
		JPanel rdBtnPanel = new JPanel(new PropertyLayout(0,0,0,0,topMargin,0));
		rdBtnPanel.add("1.1.left.center",m_replItemStrcBtn);
		rdBtnPanel.add("1.2.left.center",m_txtRepItem);
		rdBtnPanel.add("1.3.left.center",m_repItemSrBtn);
		rdBtnPanel.add("2.1.left.center",m_delItemStrcBtn);
		rdBtnPanel.add("3.1.left.center",m_addItemECOBtn);
		rdBtnPanel.add("3.2.left.center",m_mmrComboBox); //7805
		
		/* ************** Add Action commands radio buttons to button group ************** */
		m_btnGroup = new ButtonGroup();
		m_btnGroup.add(m_replItemStrcBtn);
		m_btnGroup.add(m_delItemStrcBtn);
		m_btnGroup.add(m_addItemECOBtn);
		/* ************** Add Action commands radio buttons to button group ************** */
		
		JPanel rbPanel = new JPanel();
		rbPanel.add(rdBtnPanel);
		
		return rdBtnPanel;
	}
	
	private JPanel createButtonPanel()
	{
		JPanel btnPanel = new JPanel();
		
		m_okBtn = new JButton("OK");		
		m_applyBtn = new JButton("Apply");		
		m_clearBtn = new JButton("Clear");		
		m_cancelBtn = new JButton("Cancel");		
		
		btnPanel.add(m_okBtn);
		btnPanel.add(m_applyBtn);
		//btnPanel.add(clearBtn);
		btnPanel.add(m_cancelBtn);
		
		return btnPanel;
	}
	
	private void enableButtons()
	{
		if (m_ecoFormPanel.typeofChange.equals("Lifecycle")) 
		{
			m_replItemStrcBtn.setEnabled(false);
			m_repItemSrBtn.setEnabled(false);
			m_delItemStrcBtn.setEnabled(false);
			m_addItemECOBtn.setSelected(false);//4352-changed default selection as false 
			m_mmrComboBox.setVisible(false); //7805: Hiding MMR Combobox for LifeCycle change type
		}
	}

	private void setSelection(int[] rows, Object cellVal) 
	{
		// TODO Auto-generated method stub
		/* Clear selection of all rows in case Check All is selected */
		/*for(int i = 0, j = whrUsedTable.getRowCount(); i<j; i++)
		{
			whrUsedTable.setValueAt(false, i, 0);
		}*/
		
		/*  Set selected rows */
		int selectedRowCount = rows.length;
		System.out.println("Rows selected " +  selectedRowCount);
		m_whrUsedTable.getSelectionModel().clearSelection();
		for(int i = 0; i<selectedRowCount; i++)
		{
			if(m_whrUsedTable.isCellEditable(rows[i], 0) )
			{
				m_whrUsedTable.setValueAt(cellVal, rows[i], 0);
				m_whrUsedTable.getSelectionModel().addSelectionInterval(rows[i], rows[i]);
			}			
		}
		m_whrUsedTable.repaint();
	}

	private void executeWhereUsedSearch()
	{
	    //Mohan : All NOVLogger statements are commented out because it is failing in CITRIX
        
		if(m_itemRevWhereUsed == null)
		{
			return;
		}
		
		//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Before execute");
		//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Searching for " + m_itemRevWhereUsed);
		INOVSearchProvider searchService = this;
		INOVSearchResult searchResult = null;
		try
		{
			searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}				
					
		//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("After execute");	
		
		if(searchResult != null && searchResult.getResponse().nRows > 0)
		{
			INOVResultSet theResSet = searchResult.getResultSet();
			m_whereUsedItemsRowData = theResSet.getRowData();
			m_whereUsedItemsRowCount = theResSet.getRows();
			m_whereUsedItemsColCount = theResSet.getCols();
		}
		else
		{
			m_whereUsedItemsRowData = null;
			m_whereUsedItemsRowCount = 0;
			m_whereUsedItemsColCount = 0;
		}
		populateWhereUsedTable();
	}
	
	
	public void populateWhereUsedTable()
	{	
		try 
		{
			m_whrUsedTable.setRowSorter(null);
			m_whrUsedtblmodel.removeAllRows();	
			m_nonEditableItemList.clear();
			
			resetHeaderCheckbox();
			
			if(m_whereUsedItemsRowData == null)
			{
				return;
			}
			
			for (int i = 0; i < m_whereUsedItemsRowCount; i++) 
			{	
				int rowIndex = m_whereUsedItemsColCount * i;
				String itemId = m_whereUsedItemsRowData.get(rowIndex + 1); 
				String itemName = m_whereUsedItemsRowData.get(rowIndex + 2);
				String itemDesc = m_whereUsedItemsRowData.get(rowIndex + 3);
				String owningGroup = m_whereUsedItemsRowData.get(rowIndex + 4);
				String lifeCycle = m_whereUsedItemsRowData.get(rowIndex + 5);
				String itemRevId = m_whereUsedItemsRowData.get(rowIndex + 6);
				String releaseStatus = m_whereUsedItemsRowData.get(rowIndex + 7);
				String sites = m_whereUsedItemsRowData.get(rowIndex + 8);
				String pendingECO = m_whereUsedItemsRowData.get(rowIndex + 9);
				String sRevPuid = m_whereUsedItemsRowData.get(rowIndex + 10);
				
				String pendingECOName = pendingECO.split(" ")[0];
								
				Vector<Object> rowData = new Vector<Object>();
				rowData.add(new Boolean(false));
				rowData.add(itemId); 	
				rowData.add(lifeCycle); 	
				rowData.add(pendingECOName);					
				rowData.add(itemDesc); 	
				rowData.add(sites); 	
				rowData.add(itemRevId); 	
				
				m_whrUsedtblmodel.addRow(rowData);
				m_whrUsedItemsMap.put(itemId,sRevPuid);
				
				/* Disable selection of Items that are In Process */
				boolean isRevInProcess = !pendingECO.isEmpty();
				if (isRevInProcess) 
				{
					String sRevInProcess = m_registry.getString("ItemRevInProcess.MSG");
					m_nonEditableItemList.add(itemId);
					m_nonEditableItemMap.put(itemId, sRevInProcess);					
				}
				
				/* Disable selection of Items that are not released or are the first revision */
				if(!isRevInProcess && releaseStatus.isEmpty())
				{
					String sRevNotReleased = m_registry.getString("ItemRevNotReleased.MSG");
					m_nonEditableItemList.add(itemId);
					m_nonEditableItemMap.put(itemId, sRevNotReleased);
				}
				
				/* Disable selection of Items from other Groups */
				if(!m_userGroup.toString().contains(owningGroup))
				{
					String sNotInCurrentGrp = m_registry.getString("ItemNotBelongToCurrentGrp.MSG");
					m_nonEditableItemList.add(itemId);
					m_nonEditableItemMap.put(itemId, sNotInCurrentGrp);
				}
			}
			
			if(m_whereUsedItemsRowCount >= 1 && m_whrUsedTable.getRowSorter() == null)
			{
				m_whrUsedTable.setRowSorter(m_whrUsedTableSorter);
				m_whrUsedTableSorter.setSortable(0, false);
				m_whrUsedTableSorter.sort();
				m_filterBtn.setEnabled(true);
			}			
			else
			{
				m_filterBtn.setEnabled(false);
			}
			
			/* Update row count status */
			m_lblWhrUsed.setText("Where Used Report : " + m_whrUsedTable.getRowCount() + " Items found.");
			
			/* No rows are enabled; so disable Select All option */
			if(m_whereUsedItemsRowCount != m_nonEditableItemList.size())
			{
				enableSelectAll(true);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}
	
	public void _populateWhereUsedTable()
	{	
		try 
		{
			/* Moved the Where Used search and Table population to separate thread to implement Progress bar */
			/*TCComponentRevisionRuleType revRuleType = (TCComponentRevisionRuleType) itemRev.getSession().getTypeComponent("RevisionRule");
			TCComponent[] revRules = revRuleType.extent();
			TCComponentRevisionRule revRule = null;
			TCComponentRevisionRule defaultRevRule = null;
			String confRevRule = registry.getString("Latest Working DSE");
			String defRevRule = registry.getString("Latest Released DSE");
			for (int i = 0; i < revRules.length; i++) 
			{
				if(revRules[i].getProperty("object_name").equalsIgnoreCase(confRevRule))
				{
					revRule =  new TCComponentRevisionRule();
					revRule = (TCComponentRevisionRule) revRules[i];
				}
				else if(revRules[i].getProperty("object_name").equalsIgnoreCase(defRevRule))
				{
					defaultRevRule =  new TCComponentRevisionRule();
					defaultRevRule = (TCComponentRevisionRule) revRules[i];
				}
			}
			if (revRule == null) 
			{
				revRule = defaultRevRule;
			}
			
			whereUsedItems = itemRev.whereUsed(TCComponentItemRevision.WHERE_USED_CONFIGURED,revRule);*/
				
			//m_filterCriteria = null;
			//m_whrUsedTableSorter.setRowFilter(null);
			m_whrUsedTable.setRowSorter(null);
			m_whrUsedtblmodel.removeAllRows();	
			m_nonEditableItemList.clear();
			/*TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
			((JCheckBox)tabCol.getHeaderRenderer()).setSelected(false);
			m_whrUsedTable.getTableHeader().repaint();*/
			resetHeaderCheckbox();
			
			//m_lblWhrUsed.setText("Where Used Report : " + m_whereUsedItems.length + " Items found.");
			if(m_whereUsedItems == null || m_whereUsedItems.length == 0)
			{
				return;
			}
			
			String[] itemRevProps = {"item_id", "Lifecycle", "object_desc", "Sites", "item_revision_id"};
			for (int i = 0; i < m_whereUsedItems.length; i++) 
			{
				/*String itemId = m_whereUsedItems[i].getProperty("item_id");
				//String lifecycle = whereUsedItems[i].getProperty("release_status_list");//soma
				String lifecycle = m_whereUsedItems[i].getProperty("Lifecycle");
				String site = m_whereUsedItems[i].getProperty("Sites");				
				String desc = m_whereUsedItems[i].getProperty("object_desc");//soma*/				
				
				TCComponentItemRevision latestRev = ((TCComponentItemRevision)m_whereUsedItems[i]);
				String[] revpropValues = latestRev.getProperties(itemRevProps);
				
				String pendingECO = null;
				/*TCComponent[] releaseStats = null;
				releaseStats = latestRev.getTCProperty("release_status_list").getReferenceValueArray();
				if (releaseStats.length == 0)
				{
					pendingECO = NOVECOHelper.getRefECO(m_whereUsedItems[i]);
				}  else {
					pendingECO = "";
				}*/				
				if (!NOVECOHelper.isReleasedRevision(latestRev) || NOVECOHelper.isItemRevInProcess(latestRev))
				{
					pendingECO = NOVECOHelper.getRefECO(m_whereUsedItems[i]);
				}  else 
				{
					pendingECO = "";
				}
				
				String itemId = revpropValues[0];
				Vector<Object> rowData = new Vector<Object>();
				rowData.add(new Boolean(false));
				rowData.add(revpropValues[0]); 	//Item ID
				rowData.add(revpropValues[1]); 	//Lifecycle
				rowData.add(pendingECO);		//Pending ECO			
				rowData.add(revpropValues[2]); 	//Description
				rowData.add(revpropValues[3]); 	//Site
				rowData.add(revpropValues[4]); 	//Revision ID
				
				m_whrUsedtblmodel.addRow(rowData);
				
				//m_whrUsedItemsMap.put(itemId, m_whereUsedItems[i]);  //Storing puid from search results instead of component
				
				/* Disable selection of Items that are In Process */
				boolean isInProcess = NOVECOHelper.isItemRevInProcess(latestRev);
				if (isInProcess) 
				{
					TCComponentProcess ECOProc = m_ecoFormPanel.getProcess();
					TCComponentProcess itemRevProc = latestRev.getCurrentJob();
					
					if(!ECOProc.equals(itemRevProc))
					{
						//nonEditableRowList.add(whrUsedTable.getRowCount()-1);	
						m_nonEditableItemList.add(itemId);
						m_nonEditableItemMap.put(itemId, "Item Revision is in process.");
					}							
				}
				
				/* Disable selection of Items that are not released or are the first revision */
				if(!isInProcess && !NOVECOHelper.isReleasedRevision(latestRev))
				{
					//nonEditableRowList.add(whrUsedTable.getRowCount()-1);	
					m_nonEditableItemList.add(itemId);
					m_nonEditableItemMap.put(itemId, "Item Revision is not Released.");
				}
				
				/* Disable selection of Items from other Groups */
				TCComponentGroup owningGroup = (TCComponentGroup) latestRev.getTCProperty("owning_group").getReferenceValue();				
				if(owningGroup != m_userGroup)
				{
					//nonEditableRowList.add(whrUsedTable.getRowCount()-1);	
					m_nonEditableItemList.add(itemId);
					m_nonEditableItemMap.put(itemId, "Item does not belong to current user group.");
				}
			}
			
			if(m_whereUsedItems.length >= 1 && m_whrUsedTable.getRowSorter() == null)
			{
				m_whrUsedTable.setRowSorter(m_whrUsedTableSorter);
				m_whrUsedTableSorter.setSortable(0, false);
				m_whrUsedTableSorter.sort();
				m_filterBtn.setEnabled(true);
			}			
			else
			{
				m_filterBtn.setEnabled(false);
			}
			
			/* Update row count status */
			m_lblWhrUsed.setText("Where Used Report : " + m_whrUsedTable.getRowCount() + " Items found.");
			
			/* No rows are enabled; so disable Select All option */
			if(m_whereUsedItems.length != m_nonEditableItemList.size())
			{
				enableSelectAll(true);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
		
	// This method returns the selected radio button in a button group
	private JRadioButton getSelection(ButtonGroup group) 
	{
	    for (Enumeration<AbstractButton> e=group.getElements(); e.hasMoreElements(); ) 
	    {
	        JRadioButton b = (JRadioButton)e.nextElement();
	        if (b.getModel() == group.getSelection()) 
	        {
	            return b;
	        }
	    }
	    return null;
	}
		
	public void populateWhereUsedSelectedItems()
	{
		//Swamy -> Populate the selected rows here
		System.out.println( "rowcount : " + m_whrUsedTable.getRowCount());
		int rowcount = m_whrUsedTable.getModel().getRowCount();
		m_whrUsdItemSelected.clear();
		for ( int rc=0;rc<rowcount;rc++)
		{
			Object cellVal = m_whrUsedTable.getModel().getValueAt(rc,0);
			String itemID = (String)m_whrUsedTable.getModel().getValueAt(rc, 1);
			if (cellVal instanceof Boolean) 
			{
				if(((Boolean)cellVal).booleanValue())
				{
					m_whrUsdItemSelected.add(itemID);
				}
			}
				
		}
		System.out.println( "Total selected Items : " + m_whrUsdItemSelected.size());
	}
	
	public TCComponent[] getWhereUsedItems()
	{
	
		//Vector<TCComponent> whrUsedItemsVec = new Vector<TCComponent>();
		
		int iSelectedWhrUsdItmsSize = m_whrUsdItemSelected.size();
		TCComponent[] comps = new TCComponent[iSelectedWhrUsdItmsSize];
		String[] sPuids = new String[iSelectedWhrUsdItmsSize];
		Object[] whrObs = m_whrUsdItemSelected.toArray();
		for (int i = 0; i < iSelectedWhrUsdItmsSize; i++) 
		{
			//whrUsedItemsVec.add(m_whrUsedItemsMap.get(whrObs[i]));
			
			sPuids[i] = m_whrUsedItemsMap.get(whrObs[i]);
		}
		try 
		{
			comps = m_session.stringToComponent(sPuids);
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return comps;
	}	
	
	/* Returns Component array of specified size and starting from specific index */
	public TCComponent[] getWhereUsedItems(int startIndex, int batchSize)
	{
		int index = 0;
		TCComponent[] comps = new TCComponent[batchSize];
		String[] sPuids = new String[batchSize];
		Object[] whrObs = m_whrUsdItemSelected.toArray();
		for (int i = startIndex; i < startIndex + batchSize; i++) 
		{
			sPuids[index++] = m_whrUsedItemsMap.get(whrObs[i]);
			//comps[index++] = m_whrUsedItemsMap.get(whrObs[i]);
		}
		try 
		{
			comps = m_session.stringToComponent(sPuids);
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return comps;
	}
	
	//7805 
	public String getSelectedRevision()
	{
	    return m_mmrComboBox.getSelectedItem().toString();
	}
	
	private void addButtonListeners()
	{
		m_itemChSrBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				ItemSearchDialog ISD = null;
				try 
				{
					ISD = new ItemSearchDialog(MassBOMChangeDialog.this,true);
				} catch (TCException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				final int hDisp = 280;
				final int vDisp = 25;
				int x = m_itemChSrBtn.getLocationOnScreen().x-hDisp;
	            int y = m_itemChSrBtn.getLocationOnScreen().y+vDisp;
	            ISD.setLocation(x, y);
				ISD.setVisible(true);
				
				/* If ISD is cancelled, no need to run Where Used search; m_itemRevWhereUsedChanged will not be set to true */
				if(m_itemRevWhereUsedChanged)
				{
					m_itemRevWhereUsedChanged = false;
					//TCDECREL-3495:Start
					if(m_filterCriteria != null)
	                {
	                    m_filterCriteria.clearSearchCriteria();
	                    filterWhrUsedList();
	                }
					//TCDECREL-3495:End
					executeWhereUsedSearch();
				}
			}
		});

		m_filterBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				NOVWhereUsedFilterDialog filterDlg = new NOVWhereUsedFilterDialog(m_filterCriteria);
				final int hDisp = 280;
				final int vDisp = 25;
				int x = m_filterBtn.getLocationOnScreen().x-hDisp;
	            int y = m_filterBtn.getLocationOnScreen().y+vDisp;
	            Object obj = filterDlg.showDialog(x, y);
	            NOVWhereUsedFilterCriteria filterCriteria = (NOVWhereUsedFilterCriteria) obj; 
	            if(filterCriteria != m_filterCriteria)
	            {
	            	m_filterCriteria = filterCriteria;
	                filterWhrUsedList();
	                m_lblWhrUsed.setText("Where Used Report : " + m_whrUsedTable.getRowCount() + " Items found.");
	            }	
			}
		});
		
		m_repItemSrBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				ItemSearchDialog ISD = null;
				try {
					ISD = new ItemSearchDialog(MassBOMChangeDialog.this,false);
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				final int hDisp = 280;
				final int vDisp = 25;
				int x = m_itemChSrBtn.getLocationOnScreen().x-hDisp;
	            int y = m_itemChSrBtn.getLocationOnScreen().y+vDisp;
	            ISD.setLocation(x, y);
				ISD.setVisible(true);
			}
		});
		
		m_replItemStrcBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				if (!m_repItemSrBtn.isEnabled()) 
				{
					m_repItemSrBtn.setEnabled(true);
				}
				m_mmrComboBox.setSelectedIndex(0);  //7805
				m_mmrComboBox.setEnabled(false);    //7805
			}
		});
		
		m_delItemStrcBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				m_repItemSrBtn.setEnabled(false);
				m_txtRepItem.setText("");
				m_replacementItem = null;
				m_mmrComboBox.setSelectedIndex(0);  //7805
				m_mmrComboBox.setEnabled(false);    //7805
			}
		});
		
		m_addItemECOBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				m_repItemSrBtn.setEnabled(false);
				m_txtRepItem.setText("");
				m_replacementItem = null;
				//7805: Start
				if (!m_ecoFormPanel.typeofChange.equals("Lifecycle"))
				{
				    m_mmrComboBox.setEnabled(true);
				}
				//7805: End
			}
		});
		
		m_okBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				populateWhereUsedSelectedItems();
				boolean isValid = validateInput();
				TCComponentProcess process = m_ecoFormPanel.getProcess();
				if (process==null) 
				{
					JOptionPane.showMessageDialog(MassBOMChangeDialog.this, "Selected ECO is not in process.", "Warning", JOptionPane.WARNING_MESSAGE);
				}
				if ((isValid) && (process!=null)&&(m_session!=null)) 
				{
					int retStatus = 0;
					retStatus = executeMassBomUpdate();					
					
					if(retStatus == 0)
					{
						MassBOMChangeDialog.this.dispose();
					}
				}			
			}
		});
		
		m_applyBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				populateWhereUsedSelectedItems();
				boolean isValid = validateInput();
				TCComponentProcess process = m_ecoFormPanel.getProcess();
				if (process==null) 
				{
					JOptionPane.showMessageDialog(MassBOMChangeDialog.this, "Selected ECO is not in process.", "Warning", JOptionPane.WARNING_MESSAGE);
				}
				if ((isValid) && (process!=null)&&(m_session!=null)) 
				{
					int retStatus = 0;
					retStatus = executeMassBomUpdate();								
					
					//If operation successful, Refresh WhereUsed list
					if(retStatus == 0)
					{
						executeWhereUsedSearch();
					}
				}
			}
		});
		
		m_clearBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				m_txtItemChange.setText("");
				m_txtRepItem.setText("");
				if (m_itemToChange!=null) 
				{
					m_itemToChange =null;	
				}
				if (m_replacementItem!=null) 
				{
					m_replacementItem = null;
				}
				if (m_whrUsedTable.getRowCount()>0) 
				{
					m_whrUsedTable.removeAll();
				}
			}
		});
		
		m_cancelBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				// TODO Auto-generated method stub
				MassBOMChangeDialog.this.dispose();
			}
		});	
	}	
	
	private void filterWhrUsedList() 
	{
		/*if(m_sorter == null)
			return;*/
				
		if(m_filterCriteria != null)
		{			
			/* 1. Remove Row sorter from Where Used table */
			m_whrUsedTable.setRowSorter(null);	
			
			/* 2. Construct Filter criteria and Add to sorter */			
			Vector<String> siteFilter = m_filterCriteria.getSiteFilterValue();
			Vector<String> lcFilter = m_filterCriteria.getLifecycleFilterValue();
			Vector<RowFilter<AIFTableModel,Object>> mainFilterVector = new Vector<RowFilter<AIFTableModel,Object>>(2);
						
			if(!siteFilter.isEmpty())
			{
				//Construct Filters for Sites	
				int colIndex = m_whrUsedtblmodel.findColumn("Sites");
				Vector<RowFilter<AIFTableModel,Object>> tempFilter = new Vector<RowFilter<AIFTableModel,Object>>(siteFilter.size());
				for(int i = 0;i<siteFilter.size(); i++)
				{
					try
					{
						RowFilter<AIFTableModel, Object> rf = RowFilter.regexFilter(siteFilter.get(i), colIndex);
						tempFilter.add(rf);
					}
					catch (java.util.regex.PatternSyntaxException e) 
					{
						return;
					}
				}
				RowFilter<AIFTableModel,Object> mainSitesFilter = RowFilter.orFilter(tempFilter);
				mainFilterVector.add(mainSitesFilter);
			}
			if(!lcFilter.isEmpty())
			{
				//Construct Filters for LIfecycle
				int colIndex = m_whrUsedtblmodel.findColumn("Lifecycle");
				Vector<RowFilter<AIFTableModel,Object>> tempFilter = new Vector<RowFilter<AIFTableModel,Object>>(lcFilter.size());
				for(int i = 0;i<lcFilter.size(); i++)
				{
					try
					{
						RowFilter<AIFTableModel, Object> rf = RowFilter.regexFilter("^"+lcFilter.get(i), colIndex); //4350
						tempFilter.add(rf);
					}
					catch (java.util.regex.PatternSyntaxException e) 
					{
						return;
					}
				}
				RowFilter<AIFTableModel,Object> mainLifecycleFilter = RowFilter.orFilter(tempFilter);
				mainFilterVector.add(mainLifecycleFilter);
			}
			
			if(!mainFilterVector.isEmpty())
			{
				RowFilter<AIFTableModel,Object> mainFilter = RowFilter.andFilter(mainFilterVector);
				m_whrUsedTableSorter.setRowFilter(mainFilter);
			}
			else
			{
				m_whrUsedTableSorter.setRowFilter(null);
			}
			
			/* 3. Apply sorter to Where Used table and sort by Item ID */
			m_whrUsedTable.setRowSorter(m_whrUsedTableSorter);
			m_whrUsedTableSorter.setSortable(0, false);
			m_whrUsedTableSorter.sort();			
		}
		
		/* After filtering enable/disable Select All depending on if there are any editable rows */
		enableSelectAll();
	}

	private void _executeWhereUsedSearch()
	{
		if(m_itemRevWhereUsed == null)
		{
			return;
		}
		
		final Display display = PlatformUI.getWorkbench().getDisplay();
        if(display != null)
        {
        	display.syncExec(new Runnable() 
        	{
                public void run()
                {
                    try
                    {
                    	new ProgressMonitorDialog(display.getActiveShell()).run(true, true,
  					          new SearchWhrUsedItemsOperation( MassBOMChangeDialog.this));
                    }
                    catch(InvocationTargetException e)
                    {
                        e.printStackTrace();
                    }
                    catch(InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
    		});
        }
	}
	
	private int executeMassBomUpdate()
	{	
		/* Initialize status values */
		if(getSelection(m_btnGroup).getActionCommand().toString().compareTo("AddItem") == 0)
		{
			m_isValidRevAdded = false;
		}
		else
		{
			m_bomUpdateFailedItems = "";
		}
		
		/*  Perform the operation */
		int retStatus = 0;
		try 
		{			
			setOperationStatus(false);
			
			final Display display = PlatformUI.getWorkbench().getDisplay();
	        if(display != null)
	        {
	        	display.syncExec(new Runnable() {
	                    public void run()
	                    {
	                        try
	                        {
	                        	new ProgressMonitorDialog(display.getActiveShell()).run(true, true,
	      					          new MassBOMChangeOperation( MassBOMChangeDialog.this, getSelection(m_btnGroup).getActionCommand().toString()));
	                        }
	                        catch(InvocationTargetException e)
	                        {
	                            e.printStackTrace();
	                        }
	                        catch(InterruptedException e)
	                        {
	                            e.printStackTrace();
	                        }
	                    }
	        		}
	        	);
	        }
	        
		    /* After completion of operation, check status and show appropriate message */  
	        if(getSelection(m_btnGroup).getActionCommand().toString().compareTo("AddItem") == 0)
	        {		        	
		        if(m_isValidRevAdded)
	        	{
					JOptionPane.showMessageDialog(this, "Selected Items are added to ECO.", "Added Item", JOptionPane.INFORMATION_MESSAGE);
					m_ecoFormPanel.ecoForm.refresh();
					retStatus = 0;
				}
	        }
	        else
	        {
	        	if (m_bomUpdateFailedItems.length()>0) 
				{
					JOptionPane.showMessageDialog(this, "Cannot update BOM details of Item(s): "+m_bomUpdateFailedItems, "Error", JOptionPane.ERROR_MESSAGE);
					retStatus = -1;
				}
				else
				{
					JOptionPane.showMessageDialog(this, "BOM update successful.", "Info", JOptionPane.INFORMATION_MESSAGE);
					try {
						m_ecoFormPanel.ecoForm.refresh();
					} catch (TCException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					retStatus = 0;				
				}
	        }
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		return retStatus;	
	}
	
	public Vector<TCComponent> getComponentsToAdd()
	{
		Vector<TCComponent> vecComp = new Vector<TCComponent>();
		TCComponent[] comp = getWhereUsedItems();
		for(int i=0;i<comp.length; i++)
		{
			if(!m_ecoFormPanel.ecoTargets.contains(comp[i]))
			{
				vecComp.add(comp[i]);
			}
		}
		return vecComp;
	}
	
	private boolean validateInput() 
	{
		String actionCmd =null;
		JRadioButton selRdBtn = getSelection(m_btnGroup);
		if (selRdBtn!=null) 
		{
			actionCmd = selRdBtn.getActionCommand();
		}
		else
		{
			JOptionPane.showMessageDialog(this, "Select action to perform.", "Warning", JOptionPane.WARNING_MESSAGE);
		}
		
		if ((actionCmd!=null)&&(actionCmd.equalsIgnoreCase("ReplaceItem")||actionCmd.equalsIgnoreCase("AddItem")||actionCmd.equalsIgnoreCase("DeleteItem"))) 
		{
			if (m_itemToChange == null) 
			{
				JOptionPane.showMessageDialog(this, "Select Item to change.", "Warning", JOptionPane.WARNING_MESSAGE);
			}
			else if (!(m_replacementItem!=null)&& (actionCmd.equalsIgnoreCase("ReplaceItem"))) 
			{
				JOptionPane.showMessageDialog(this, "Select Replacement Item.", "Warning", JOptionPane.WARNING_MESSAGE);
			}			
			else if(/*(whrUsedTable.getRowCount()>0) &&*/ !(m_whrUsdItemSelected.size()>0)/*&& need to analyse the scenarios*/)
			{
				JOptionPane.showMessageDialog(this, "Select at least one where used items.", "Warning", JOptionPane.WARNING_MESSAGE);
			}
			else if(actionCmd.equalsIgnoreCase("AddItem"))
			{	
				return validateAddTarget();
			}
			else
			{
				return true;
			}
		}				
		return false;
	}
	
	private boolean validateAddTarget()
	{
		boolean retStatus = true;
		Vector<TCComponent> vecComp = getComponentsToAdd();
		final int vecCompSize = vecComp.size();
		
		if(vecCompSize == 0)
		{
			JOptionPane.showMessageDialog(this, "Items already exists in the target list.", "Items exists", JOptionPane.INFORMATION_MESSAGE);
			retStatus = false;
		}
		
		return retStatus;
	}
	
	public void setAddItemToTargetStatus(boolean status)
	{
		m_isValidRevAdded = status;
	}
	
	public void setBomUpdateStatus(String str)
	{
		m_bomUpdateFailedItems = str;		
	}
	
	public void setOperationStatus(boolean m_operationComplete) 
	{
		this.m_operationComplete = m_operationComplete;
	}

	public boolean getOperationStatus() 
	{
		return m_operationComplete;
	}
	
	public TCComponent getItemToChange() 
	{
		return m_itemToChange;
	}
	
	public TCComponent getReplacementItem() 
	{
		return m_replacementItem;
	}
	
	public _EngChangeObjectForm_Panel getEcoFormPanel()
	{
		return m_ecoFormPanel;
	}
	
	public TCComponentItemRevision getWhrUsedItemRev()
	{
		return m_itemRevWhereUsed;
	}
	
	public void setWhrUsedItemRev(TCComponentItemRevision itemRev)
	{
		 m_itemRevWhereUsed = itemRev;
		 m_itemRevWhereUsedChanged = true;
	}
	
	
	public String getConfRevRule()
	{
		return m_registry.getString("Latest Working DSE");
	}
	
	public String getDefRevRule()
	{
		return m_registry.getString("Latest Released DSE");
	}
	
	public void setWhereUsedResult(TCComponent[] comp)
	{
		m_whereUsedItems = comp;
	}
	
	/**
	 * Purpose: Create WhereUsed table model and table
	 */
	private void createWhereUsedTable()
	{
		String[] attrNames = {" ","Item ID","Lifecycle","Pending ECO","Description","Sites"};
		m_whrUsedtblmodel = new AIFTableModel(attrNames);			
		m_whrUsedTable = new JTable(m_whrUsedtblmodel)
		{
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) 
			{
				/* 19.04.2012:Rakesh - TCDECREL-2327 - Sorting enabled; so Item ID should be used for comparisons instead of row num */
				if (column == 0 && (!m_nonEditableItemList.contains(getValueAt(row, 1))))
				{					
					return true;
				}
				else
				{
					return false;
				}
			}
			
			@Override
            public String getToolTipText(MouseEvent e) 
            {
                String tip = null;
                Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                
                if(rowIndex != -1)
                {
                String itemID = getValueAt(rowIndex, 1).toString();
                
                if(m_nonEditableItemList.contains(itemID))
                {
                	tip = m_nonEditableItemMap.get(itemID);
                }                
                }
               
                return tip;
            }
            
            @SuppressWarnings({ "unchecked" })
			public Class getColumnClass(int column)
            {
               return getValueAt(0, column).getClass();
            } 
		};
		
		addWhereUsedTableListener();
		m_whrUsedTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);	
		m_whrUsedTable.getTableHeader().setReorderingAllowed(false);		
		m_whrUsedTable.doLayout();
	}
	
	private void createWhereUsedTableSorter()
	{
		m_whrUsedTableSorter = new TableRowSorter<AIFTableModel>(m_whrUsedtblmodel)
		{
			Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();

		    public void toggleSortOrder(int column)
		    {
		    	if(column == 0)
		    	{
		    		return;
		    	}
		    	
		    	SortKey key = m_keysMap.get(column);
		        SortOrder order = null;
		        // Get last sort order.
		        if (key != null) 
		        {
		            if (key.getSortOrder() == SortOrder.DESCENDING)
		            {
		                order = SortOrder.ASCENDING;
		            }
		            else 
		            {
		                order = SortOrder.DESCENDING;
		            }
		        }
		        else 
		        {
		            order = SortOrder.DESCENDING;
		        }

		        m_keysMap.put(column, new SortKey(column, order));
		        List<SortKey> keys = new ArrayList<SortKey>();
				SortKey sortKey = new SortKey(column, order);
				keys.add(sortKey);
				
		        this.setSortKeys(keys);
		        m_whrUsedTable.setRowSorter(this);
		        this.sort();
		        
		        System.out.println("Running custom Sorting: Column " + column + order);
		        
		    }
		};
				
		List<SortKey> keys = new ArrayList<SortKey>();
		SortKey sortKey = new SortKey(m_whrUsedtblmodel.findColumn("Item ID"), SortOrder.ASCENDING);
		keys.add(sortKey);
		m_whrUsedTableSorter.setSortKeys(keys);		
		m_whrUsedTable.setRowSorter(m_whrUsedTableSorter);
		m_whrUsedTableSorter.setSortable(0, false);
	}
	
	/**
	 * Purpose: Add Listeners to Where Used Table
	 */
	private void addWhereUsedTableListener()
	{
		m_whrUsedTable.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent msEvt) 
			{				
				setHeaderCheckbox();
				
				/* Not a multi-row selection then treat it as default single row selection */
				if(m_rowSelection == null || !(m_rowSelection.length>1))
				{
					return;
				}
				
				JTable table = (JTable)msEvt.getSource();
				if(table.getSelectedColumn()==0)
				{	
					/* if checked row NOT in multi-rows selected list-> treat it as default single row selection */
					int row = table.getSelectedRow();
					boolean rowInSelection = false;
					for(int i =0; i < m_rowSelection.length; i++ )
					{
						if(m_rowSelection[i] == row)
						{
							rowInSelection = true;
						}
					}
					if(!rowInSelection) 
					{
						m_rowSelection = null;
						return;
					}
					
					/* Checked row in multi-row selected list */
					Object cellVal = table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
					if (cellVal instanceof Boolean) 
					{
						//CheckBox selected	
						if(m_rowSelection != null)
						{
							for(int i = 0;i<m_rowSelection.length; i++)
							{
								m_whrUsedTable.getSelectionModel().addSelectionInterval(m_rowSelection[i], m_rowSelection[i]);							
							}
							setSelection(m_rowSelection, cellVal);
						}
					}
				}				
			}
			
			@Override
			public void mousePressed(MouseEvent mouseEvt)
			{
				final JTable table = (JTable)mouseEvt.getSource();
				if(table.getSelectedColumn()!=0)
				{
					System.out.println("inside listner : Rows selected " +  table.getSelectedRowCount());
					m_rowSelection = table.getSelectedRows();
				}		
			}
		});
	}
	
	private void resetHeaderCheckbox() 
	{
		TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
		((JCheckBox)tabCol.getHeaderRenderer()).setSelected(false);
		((JCheckBox)tabCol.getHeaderRenderer()).setEnabled(false);
		m_whrUsedTable.getTableHeader().repaint();
	}
	
	private void setHeaderCheckbox() 
	{		
		boolean val = true;
		
		int rowCount = m_whrUsedTable.getRowCount();
		for(int i=0; i<rowCount; i++)
		{
			boolean v = (Boolean) m_whrUsedTable.getValueAt(i, 0);
			if(!v)
			{
				val = false;
				break;
			}
		}		
		setHeaderCheckbox(val);
	}

	private void setHeaderCheckbox(boolean val) 
	{
		TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
		((JCheckBox)tabCol.getHeaderRenderer()).setSelected(val);
		m_whrUsedTable.getTableHeader().repaint();
	}
	
	private void enableSelectAll() 
	{		
		boolean setEnabled = false;
		
		int rowCount = m_whrUsedTable.getRowCount();
		for(int i=0; i<rowCount; i++)
		{
			if(!m_nonEditableItemList.contains(m_whrUsedTable.getValueAt(i, 1)))
			{
				setEnabled = true;
				break;
			}
		}
		enableSelectAll(setEnabled);
	}
	
	private void enableSelectAll(boolean val) 
	{		
		TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
		((JCheckBox)tabCol.getHeaderRenderer()).setEnabled(val);
		m_whrUsedTable.getTableHeader().repaint();
	}

	/**
	 * Purpose: Assign Column renderers and Column header renderers to the WhereUsed table
	 */
	private void assignColumnRenderer()
	{
		/* Set Table Header Check box renderer */
		final int checkBoxColWidth = 10;
		TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
	    tabCol.setResizable(false);
	    tabCol.setPreferredWidth(checkBoxColWidth);
        tabCol.setCellEditor(m_whrUsedTable.getDefaultEditor(Boolean.class));
		tabCol.setCellRenderer(m_whrUsedTable.getDefaultRenderer(Boolean.class));
		CheckBoxHeaderListener headerListener = new CheckBoxHeaderListener(m_whrUsedTable);
		CheckBoxHeader chkBoxHeader = new CheckBoxHeader(headerListener);
		tabCol.setHeaderRenderer(chkBoxHeader); 
				
		/* Highlight Disabled rows with Grey color */
		for (int i = 0; i < m_whrUsedTable.getColumnCount(); i++) 
		{
			m_whrUsedTable.getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer()
			{
				private static final long serialVersionUID = 1L;
				public Component getTableCellRendererComponent(JTable table ,
			            Object value , boolean isSelected , boolean hasFocus , int row ,
			            int column )
			    {
					if (m_nonEditableItemList.contains(table.getValueAt(row, 1))) 
			    	{
						setBackground(Color.lightGray);
						//setToolTipText("Item is in Process or does not belong to this group");
						return super.getTableCellRendererComponent(table, value, isSelected,
				                hasFocus, row, column);
					}					    	
					setBackground(table.getBackground());
					return super.getTableCellRendererComponent(table, value, isSelected,
				                hasFocus, row, column);				    
			    }
			});
		}
		
		/* Set Check box renderer for the first Column */
		m_whrUsedTable.getColumnModel().getColumn(0).setCellRenderer( new TCCellRenderer() 
		{
            private static final long serialVersionUID = 1L;
            
            public Component getTableCellRendererComponent(JTable jtable, Object value, 
            		boolean isSelected, boolean isFocused, int row, int col) 
            {
			      if(value instanceof Boolean)
			      {
			    	 Boolean marked = (Boolean) value;		              
		             JCheckBox rendererComponent = new JCheckBox();
		             rendererComponent.setBackground(Color.white);
		             rendererComponent.setLayout(new FlowLayout( FlowLayout.CENTER));
		             if (marked.booleanValue())
		             {
		            	 rendererComponent.setSelected(true);
		             }
		             rendererComponent.setHorizontalAlignment(SwingConstants.CENTER);
		             /* 19.04.2012:Rakesh - TCDECREL-2327 - Sorting enabled; so Item ID should be used for comparisons instead of row num */
		 			 if (m_nonEditableItemList.contains(jtable.getValueAt(row, 1)))
		             {
		            	 rendererComponent.setEnabled(false);
					 }
		             return rendererComponent;
			      }
			      return null;		
             }
        });
	}
	
	/**
	 * Purpose: Set Column Widths of the WhereUsed table
	 */
	public void setColumnWidths()
	{
		DefaultTableColumnModel defaulttablecolumnmodel = (DefaultTableColumnModel)m_whrUsedTable.getColumnModel();
		final int charWidth = (int) 11.5;
		final int factor = 20;
		int i = defaulttablecolumnmodel.getColumnCount();
		for ( int j = 0; j < i; j++ )
		{
			TableColumn tablecolumn = defaulttablecolumnmodel.getColumn(j);
			try
			{
				int integer = tablecolumn.getHeaderValue().toString().length();
				tablecolumn.setPreferredWidth((integer * charWidth) + factor);
				/* Removing restriction of max width for last 2 columns (Description and Sites) */
				if(j<i-2)
				{
					tablecolumn.setMaxWidth((integer * charWidth) + factor);
				}
			}
			catch ( Exception exception )
			{				
			}
		}		
	}

	@Override
	public QuickBindVariable[] getBindVariables() 
	{
		String strItemRevPUID = m_itemRevWhereUsed.getUid();
		
		QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[3];
		
		bindVars[0] = new QuickSearchService.QuickBindVariable();
		bindVars[0].nVarType = POM_typed_reference;
		bindVars[0].nVarSize = 1;
		bindVars[0].strList = new String[]{strItemRevPUID};
						
		bindVars[1] = new QuickSearchService.QuickBindVariable();
		bindVars[1].nVarType = POM_string;
		bindVars[1].nVarSize = 1;
		bindVars[1].strList = new String[]{"Nov4Part"};
		
		bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[]{"Non-Engineering"};
		
		return bindVars;
	}

	@Override
	public String getBusinessObject() 
	{
		return "Nov4Part";
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo() 
	{
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[5];
		
		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-where-used-items";
		allHandlerInfo[0].listBindIndex = new int[] {1};
		allHandlerInfo[0].listReqdColumnIndex = new int[]{1,2,3,4}; 
		allHandlerInfo[0].listInsertAtIndex = new int[] {1,2,3,4}; 
		
		allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[1].handlerName = "NOVSRCH-get-release-status";
		allHandlerInfo[1].listBindIndex = new int[0];
		allHandlerInfo[1].listReqdColumnIndex = new int[] {2};
		allHandlerInfo[1].listInsertAtIndex = new int[] {5};
		
		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[2].handlerName = "NOVSRCH-get-nov4part-latest-revs";
		allHandlerInfo[2].listBindIndex = new int[0];
		allHandlerInfo[2].listReqdColumnIndex = new int[] {1,2,5,6};
		allHandlerInfo[2].listInsertAtIndex = new int[] {6, 7 /*Release Status*/,9/*Pending ECO*/,10 /*Revision puid*/};
		
		allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-DH-sites-props";
        allHandlerInfo[3].listBindIndex = new int[]{2};
        allHandlerInfo[3].listReqdColumnIndex = new int[] {1};
        allHandlerInfo[3].listInsertAtIndex = new int[] {8};
        
        allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-DH-sites-props";
        allHandlerInfo[4].listBindIndex = new int[]{3};
        allHandlerInfo[4].listReqdColumnIndex = new int[] {1};
        allHandlerInfo[4].listInsertAtIndex = new int[] {8};
		
		/*allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[4].handlerName = "NOVSRCH-get-pending-ECO";
		allHandlerInfo[4].listBindIndex = new int[0];
		allHandlerInfo[4].listReqdColumnIndex = new int[] {1};
		allHandlerInfo[4].listInsertAtIndex = new int[] {9};*/
		
		return allHandlerInfo;
	}
}



