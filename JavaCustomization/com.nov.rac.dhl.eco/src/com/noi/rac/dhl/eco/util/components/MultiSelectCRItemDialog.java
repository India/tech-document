package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.noi.util.components.NOIJLabel;
import com.nov.rac.dhl.eco.handlers.NOVECOMoveItemHandler;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

public class MultiSelectCRItemDialog extends AbstractAIFDialog implements MouseListener
{
	private static final long serialVersionUID = 1L;
	private 	Vector<TCComponent> 	m_ecrVector ;
	ECOWizardItemTable crItemTable;
	Map<String,TCComponentForm> crMap ;
	TCComponentItemRevision selItem;
	TCTable table;
	NOVCRItemSelectionPanel itemSelPanel;
	NOVECOTargetItemsPanel formItemSelPanel;
	private boolean isItemMove =false;
	private TCComponentForm targetECO = null;
	private String changeType="";
	public MultiSelectCRItemDialog(NOVCRItemSelectionPanel itemSelectionPanel,TCComponentItemRevision selectedItem,Vector<TCComponent> ecrForms) 
	{
		super(AIFUtility.getActiveDesktop());
		try 
		{
			itemSelPanel = itemSelectionPanel;
			crItemTable = itemSelPanel.targetTable;
			crMap = new HashMap<String, TCComponentForm>();
			selItem = selectedItem;
			m_ecrVector = ecrForms;
			createUI();
			/*AIFComponentContext[] compConxt = selItem.getRelated("RelatedECN");*/
			loadTableData(/*compConxt*/);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public MultiSelectCRItemDialog(NOVECOTargetItemsPanel targetItemSelPanel,TCComponentItemRevision selcomp,Vector<TCComponent> ecrForms) 
	{
		super(AIFUtility.getActiveDesktop());
		try 
		{
			formItemSelPanel = targetItemSelPanel;
			crMap = new HashMap<String, TCComponentForm>();
			selItem = selcomp;
			m_ecrVector = ecrForms;
			createUI();
			/*AIFComponentContext[] compConxt = selItem.getRelated("RelatedECN");*/
			loadTableData(/*compConxt*/);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	//TCDECREL-3196 :Start
	/**
	 * @param typeChange 
	 * 
	 */
	public MultiSelectCRItemDialog(TCComponentItemRevision selcomp,Vector<TCComponent> ecrForms,boolean isMoveItem, TCComponentForm targetForm) 
	{
		super(AIFUtility.getActiveDesktop());
		try 
		{
			isItemMove = isMoveItem;
			targetECO = targetForm;
			if(targetECO != null)
			{
				changeType = targetECO.getTCProperty("changetype").toString();
			}
			crMap = new HashMap<String, TCComponentForm>();
			selItem = selcomp;
			m_ecrVector = ecrForms;
			createUI();
			loadTableData();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	//TCDECREL-3196 : End
	private void createUI()
	{
		NOIJLabel nLabel = new NOIJLabel();
		String text = "Selected Item Revision is having multiple Change Requests attached.\n Select one CR-Item.";
		nLabel.setText(text);
		String[] columnNames = {"ECR","Item Revision"};
		
		table = new TCTable(columnNames);
		table.addMouseListener(this);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scPane = new JScrollPane(table);
		scPane.setPreferredSize(new Dimension(150,100));
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(nLabel,BorderLayout.NORTH);
		mainPanel.add(scPane,BorderLayout.CENTER);	
				
		this.getContentPane().add(mainPanel);
		this.pack();
		this.setModal(true);
		centerToScreen();
	}
	
	private void loadTableData(/*AIFComponentContext[] compConxt*/)
	{
		try 
		{
			/*if (compConxt.length>0) 
			{*/								
				Vector<String> rowData = new Vector<String>();
				if(m_ecrVector !=null && m_ecrVector.size()>0)
				{
					TCComponentForm []ecrArray =(TCComponentForm[]) m_ecrVector.toArray(new TCComponentForm[m_ecrVector.size()]);
					for (int i = 0; i < ecrArray.length; i++) 
					{
						rowData.clear();
						String object_name =ecrArray[i].getProperty("object_name");
						rowData.add(object_name); 
						rowData.add(selItem.getProperty("object_string"));
						crMap.put(object_name, ecrArray[i]);
						table.addRow(rowData);
					}					
				}
				else
				{
					this.dispose();
				}
						
			//}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		
	}
	public void mouseClicked(MouseEvent evt) 
	{
		if (evt.getClickCount()==2) 
		{
			try 
			{
				int selRow = table.getSelectedRow();
				Object crNum = table.getValueAt(selRow, 0);
				if (itemSelPanel!=null) 
				{
					if (itemSelPanel.crSelectpanel.typeofChange.trim().length()==0) 
					{
						MessageBox.post("Select Type of change.", "Wanring", MessageBox.WARNING);
						return;
					}
					if(!NOVECOHelper.isValidECR(itemSelPanel.crSelectpanel.typeofChange,crMap.get(crNum)))
					{
						//Messagebox shown in NOVECOHelper class
						return;
					}
					boolean retval = crItemTable.addTarget(selItem, crMap.get(crNum));
					itemSelPanel.notifyObserver();
					if (retval) 
					{
						MessageBox.post("The Item "+ selItem.getProperty("item_id")+ " is added to targets.",
								"Added Item",MessageBox.WARNING);
					}
					else
					{
						MessageBox.post("RelatedDefiningDocument relation contains other than Documents.", "Cannot add item", MessageBox.WARNING);
					}
				}
				if (formItemSelPanel!=null) 
				{
					if(!NOVECOHelper.isValidECR(formItemSelPanel.ecoFormPanel.typeofChange,crMap.get(crNum)))
					{
						//Messagebox shown in NOVECOHelper class
						return;
					}
					boolean isValidRevAdded = formItemSelPanel.ecoFormPanel.addValidRevisionToTarget(selItem);
					if (isValidRevAdded) 
					{
						formItemSelPanel.ecoFormPanel.updateCRsList(crMap.get(crNum));	
						MessageBox.post("The Item "+ selItem.getProperty("item_id")+ " is added to targets.",
									"Added Item",MessageBox.WARNING);
					}
					formItemSelPanel.ecoFormPanel.saveDatatoForm();
				}
				//TCDECREL-3196 : Start
				if(isItemMove)
				{
					if(!NOVECOHelper.isValidECR(changeType,crMap.get(crNum)))
					{
						return;
					}
					boolean isValidRevAdded =NOVECOHelper.addValidRevisionToTarget(selItem,targetECO);
					if (isValidRevAdded) 
					{
						targetECO.add("_ECNECR_",crMap.get(crNum));
						MessageBox.post("The Item "+ selItem.getProperty("item_id")+ " is added to targets.",
									"Added Item",MessageBox.WARNING);
					}
				}
				//TCDECREL-3196 : End
			}
			catch (TCException e)
			{
				e.printStackTrace();
			}
			MultiSelectCRItemDialog.this.dispose();
		}
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	
	
}
