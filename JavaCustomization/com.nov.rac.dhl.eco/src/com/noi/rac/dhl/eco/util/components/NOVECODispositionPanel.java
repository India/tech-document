package com.noi.rac.dhl.eco.util.components;


import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class NOVECODispositionPanel extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private _EngChangeObjectForm_Panel ecoPanel;
	private Vector<TCComponent> targets;		
	public TCComponent[] referncedispdata;	
	public HashMap<TCComponent, TCComponent> targetdispMap=new HashMap<TCComponent, TCComponent>();
	public HashMap<TCComponent, TCComponent> disptargetMap=new HashMap<TCComponent, TCComponent>();
	public HashMap<TCComponent, String> targetStatusMap=new HashMap<TCComponent, String>();
	public Set<TCComponent> orphanCompSet = new HashSet<TCComponent>();
	public NOVECOTargetsPanel ecoTargetsPanel;

	public NOVECODispositionPanel(TCComponent[] referncedispdata)
	{		
		this.referncedispdata=referncedispdata;
	}

	public void createUI( _EngChangeObjectForm_Panel _engChngObjFormPanel )
	{	
		try
		{			
			ecoTargetsPanel = new NOVECOTargetsPanel( _engChngObjFormPanel, referncedispdata );
			String ECOTergets = "Disposition Objects";
			ecoTargetsPanel.createUI();
			ecoPanel.ecautomatonTabbePane.addTab( ECOTergets, ecoTargetsPanel );
			
			/* Rakesh - TCDECREL-2788 - Add FOcus change listener to save disposition data - Starts here */
			ecoPanel.ecautomatonTabbePane.addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent changeevent) 
				{
					// TODO Auto-generated method stub
					System.out.println("TAB changed " + ((JTabbedPane)changeevent.getSource()).getSelectedIndex());
					int index = ((JTabbedPane)changeevent.getSource()).getSelectedIndex();
					if(index != 1)
					{
						ecoTargetsPanel.dispPanel.saveData();
					}
				}
			});
			/* Rakesh - TCDECREL-2788 - Add FOcus change listener to save disposition data - Ends here */
			
			//createDispTabPanels(referncedispdata);
			//add( ecoPanel.ecautomatonTabbePane );
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	public void setDispData( _EngChangeObjectForm_Panel enPanel )
	{
		this.ecoPanel=enPanel;
		targets=enPanel.ecoTargets;
		//disposition data initially
		try
		{
			if((referncedispdata.length>0)&&(targets.size()>0)) //Create the Disposition instances for the targets
			{
				Vector<TCComponent> tempReference = new Vector<TCComponent>(Arrays.asList(referncedispdata));				
				for(int k=0;k<targets.size();k++)
				{
					for(int j=0;j<tempReference.size();j++)
					{
						String targetId=(tempReference.get(j).getTCProperty("targetitemid")).getStringValue();
						String itemId=((TCComponentItemRevision)targets.get(k)).getProperty("item_id");
						if(targetId.equalsIgnoreCase(itemId))
						{
							targetdispMap.put(tempReference.get(j),targets.get(k));
							disptargetMap.put(targets.get(k), tempReference.get(j));
							tempReference.remove(j);							
						}
					}
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
	}
	public TCComponent[] getDispData()
	{
		return referncedispdata;
	}
	public void setDispositionData(TCComponent[] referncedispdata)
	{		
		this.referncedispdata=referncedispdata;
	}
	/*public void createDispTabPanels(TCComponent[] dispcomps)
	{
		NOVENDispositionPanelData dispPanelData = new NOVENDispositionPanelData( dispcomps );
		try
		{
			for(int k=0;k<dispcomps.length;k++)
			{
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("creating Disp panel Start : " + k);
				NOVECODispositionTabPanel dispPanel= new NOVECODispositionTabPanel(ecoPanel,ecoPanel.getSession(), dispcomps[k], dispPanelData );
				dispPanel.createBOMPanel(targetdispMap.get(dispcomps[k]));
				dispPanel.createUI();
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("creating Disp panel End");
				//dispPanel.setTargetItemRev(targetdispMap.get(dispcomps[k]));
				String itemId=(dispcomps[k].getTCProperty("targetitemid")).getStringValue();
				ImageIcon typeicon=TCTypeRenderer.getIcon(targetdispMap.get(dispcomps[k]));
				ecoPanel.ecautomatonTabbePane.addTab(itemId,typeicon ,dispPanel);
			}
		}catch(TCException tc)
		{
			System.out.println(tc);

		}
	}*/

	public void deleteOrphanedDispInstances()
	{
		Object[] orphObjs = orphanCompSet.toArray();
		if (orphObjs!=null && orphObjs.length>0) 
		{
			TCComponent[] orphComps = orphanCompSet.toArray(new TCComponent[orphObjs.length]);
			 
			for (int i = 0; i < orphComps.length; i++) 
			{
				try 
				{
					AIFComponentContext[] instanceRef = orphComps[i].whereReferenced();
					if (instanceRef.length==0) 
					{
						orphComps[i].lock();
						orphComps[i].delete();	
					}
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}	
			}
			orphanCompSet.clear();
		}	
	}
	
	public void adddispTabs(TCComponent[] newdispData)
	{
		//update disp data		
		try
		{		
			//get the Target Item IDs
			/*String itemIds[];	
			String targetRevIds[];
			Vector<String> vtargetIds=new Vector<String>();
			Vector<String> vtargetRevIds=new Vector<String>();
			Vector<TCComponent>newTargets=new Vector<TCComponent>();
			for(int i=0;i<newdispData.length;i++)
			{		
				//if(!(newdispData[i].getProperty("object_type").equalsIgnoreCase("Documents Revision")))
				//{
				vtargetIds.add(((TCComponentItemRevision)newdispData[i]).getProperty("item_id"));
				vtargetRevIds.add(((TCComponentItemRevision)newdispData[i]).getProperty("current_revision_id"));
				newTargets.add(newdispData[i]);
				//}
			}
			//Create the disp instances for the new Targets
			itemIds=new String[vtargetIds.size()];
			vtargetIds.copyInto(itemIds);
			targetRevIds=new String[vtargetRevIds.size()];
			vtargetRevIds.copyInto(targetRevIds);
			TCComponent[] newdispInstances=createDispInstances(itemIds, targetRevIds);
			for(int k=0;k<newdispInstances.length;k++)
			{
				targetdispMap.put(newdispInstances[k],newTargets.get(k));
				disptargetMap.put(newTargets.get(k), newdispInstances[k]);
			}*/
			//Update Disp Info
			TCComponent[] updateddispData=new TCComponent[referncedispdata.length+newdispData.length];		
			int k=0, i=0;
			for(k=0;k<referncedispdata.length;k++)
			{
				updateddispData[k]=referncedispdata[k];
			}
			for(;k<(referncedispdata.length+newdispData.length);k++)
			{
				updateddispData[k]=newdispData[i++];
			}
			referncedispdata=updateddispData;
			//save form
			//ecoPanel.saveDatatoForm();
			//Create new Disp Tabs
			//createDispTabPanels(newdispData);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	public void removeDispTab(TCComponent tobRemoved)
	{
		TCComponent dispComptoBremoved=null;
		try 
		{
			if(!(tobRemoved.getProperty("object_type").equalsIgnoreCase("Documents Revision")))
			{
				/*String item_id=tobRemoved.getProperty("item_id");
				//remove the tab panel also;
				for(int k=0;k<ecoPanel.ecautomatonTabbePane.getTabCount();k++)
				{
					String title=ecoPanel.ecautomatonTabbePane.getTitleAt(k);
					if(title.equalsIgnoreCase(item_id))
					{
						NOVECODispositionTabPanel dispPanel=(NOVECODispositionTabPanel)ecoPanel.ecautomatonTabbePane.getComponentAt(k);
						dispComptoBremoved=dispPanel.dispcomp;
						ecoPanel.ecautomatonTabbePane.remove(k);
						break;
					}
				}*/
				if (disptargetMap.get(tobRemoved)!=null) 
				{
					int i=0;		
					TCComponent[] updateddispData=new TCComponent[referncedispdata.length];
					for(int k=0;k<referncedispdata.length;k++)
					{
						if(referncedispdata[k]!=dispComptoBremoved)
							updateddispData[i++]=referncedispdata[k];
					}
					//dispComptoBremoved.delete();
					//orphanCompSet.add(dispComptoBremoved);
					referncedispdata=updateddispData;
					
					referncedispdata = ecoPanel.getValidDisps();
					
				}
				//save form
				ecoPanel.saveDatatoForm();
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	public void saveDispData()
	{
		ecoTargetsPanel.dispPanel.saveData();//5084
		
		/*for(int k=0;k<ecoPanel.ecautomatonTabbePane.getTabCount();k++)
		{
			if(ecoPanel.ecautomatonTabbePane.getComponentAt(k) instanceof NOVECODispositionTabPanel)
			{
				NOVECODispositionTabPanel dispPanel=(NOVECODispositionTabPanel)ecoPanel.ecautomatonTabbePane.getComponentAt(k);
				dispPanel.saveData();
			}
		}*/
	}
}
