package com.noi.rac.dhl.eco.masschange.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.wizard.NOVCreateECOHelper;
import com.nov.rac.mmr.revisionstrategy.MajorDefaultRevisionStrategy;
import com.nov.rac.utilities.services.deleteHelper.CreateRelationObjectHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.soa.client.model.ErrorStack;

/**
 * @author tripathim
 * 
 */
public class NOVRDChangeHelper
{
    
    public static String getNextMajorRevisionId(Vector<TCComponent> partFamilyComps)
    {
        MajorDefaultRevisionStrategy majorObj = new MajorDefaultRevisionStrategy();
        return majorObj.getNextRevision(partFamilyComps);
    }
    
    public static String getNextMinorRevisionId(String sCurrentRevId)
    {
        NOVCreateECOHelper ecoHelperObj = new NOVCreateECOHelper();
        return ecoHelperObj.getNextMinorRevisionId(sCurrentRevId);
    }
    
    public static String getSuggestedNextRevID(TCComponent tcComponent, String mmrType) throws TCException
    {
        String nextRevID = null;
        if (mmrType.equalsIgnoreCase("Create Major Revision"))
        {
            Vector<TCComponent> partFamily = NOVECOHelper.getAllPartFamilyMembers(tcComponent);
            nextRevID = getNextMajorRevisionId(partFamily);
        }
        else
        {
            nextRevID = getNextMinorRevisionId(tcComponent.getProperty("item_revision_id").toString());
        }
        return nextRevID;
    }
    
    public static Map<TCComponent, String> fillAllComponentsWithRevid(TCComponent[] selectedComps, String mmrType)
            throws TCException
    {
        Map<TCComponent, String> compWithRevIdsMap = new HashMap<TCComponent, String>();
        for (int i = 0; i < selectedComps.length; i++)
        {
            String nextRevID = null;
            if (mmrType.equalsIgnoreCase("Create Major Revision"))
            {
                Vector<TCComponent> partFamily = NOVECOHelper.getAllPartFamilyMembers(selectedComps[i]);
                if (partFamily.size() == 0)
                {
                    partFamily.add(selectedComps[i]);
                }
                Vector<TCComponent> filteredPartFamily = filterPartFamiliy(partFamily);
                nextRevID = NOVRDChangeHelper.getNextMajorRevisionId(filteredPartFamily);
                for (int j = 0; j < filteredPartFamily.size(); j++)
                {
                    compWithRevIdsMap.put(filteredPartFamily.get(j), nextRevID);
                }
            }
            else
            {
                nextRevID = NOVRDChangeHelper.getNextMinorRevisionId(selectedComps[i].getProperty("item_revision_id")
                        .toString());
                compWithRevIdsMap.put(selectedComps[i], nextRevID);
            }
        }
        
        return compWithRevIdsMap;
    }
    
    private static Vector<TCComponent> filterPartFamiliy(Vector<TCComponent> partFamily) throws TCException
    {
        Set<TCComponent> filteredPartFamily = new HashSet<TCComponent>();
        for (int i = 0; i < partFamily.size(); i++)
        {
            TCComponentItemRevision itemrev = (TCComponentItemRevision) partFamily.get(i);
            filteredPartFamily.add(itemrev.getItem().getLatestItemRevision());
            
        }
        return new Vector<TCComponent>(filteredPartFamily);
    }
    
    public static List<ErrorStack> reviseObject(Map<TCComponent, String> compsWithRevIdMap) throws TCException
    {
        DataManagement.ReviseInfo[] reviseInfo = new DataManagement.ReviseInfo[compsWithRevIdMap.size()];
        
        Iterator iter = compsWithRevIdMap.keySet().iterator();
        int index = 0;
        while (iter.hasNext() && index < compsWithRevIdMap.size())
        {
            TCComponent compKey = (TCComponent) iter.next();
            String revIdValue = (String) compsWithRevIdMap.get(compKey);
            reviseInfo[index] = new ReviseInfo();
            reviseInfo[index].clientId = compKey.getProperty("item_id").toString();
            reviseInfo[index].baseItemRevision = (TCComponentItemRevision) compKey;
            reviseInfo[index].description = compKey.getProperty("object_desc").toString();
            reviseInfo[index].name = compKey.getProperty("object_name").toString();
            reviseInfo[index].newRevId = revIdValue;
            index++;
        }
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        DataManagementService dmService = DataManagementService.getService(tcSession);
        
        DataManagement.ReviseResponse2 serviceResponse = dmService.revise2(reviseInfo);
        ServiceData serviceData = serviceResponse.serviceData;
        return addErrors(serviceData);
    }
    
    private static List<ErrorStack> addErrors(ServiceData serviceData)
    {
        ArrayList<ErrorStack> errors = new ArrayList<ErrorStack>();
        int totalErrors = serviceData.sizeOfPartialErrors();
        if (totalErrors > 0)
        {
            for (int i = 0; i < totalErrors; i++)
            {
                errors.add(serviceData.getPartialError(i));
            }
            
        }
        return errors;
    }
    
    public static CreateRelationObjectHelper[] getCreateRDRelationObjects(TCComponent[] selectedComps,
            TCComponent rdComponent)
    {
        CreateRelationObjectHelper[] creRelationObjectHelperArr = new CreateRelationObjectHelper[selectedComps.length];
        for (int i = 0; i < selectedComps.length; i++)
        {
            try
            {
                TCComponentItemRevision newItemRev = ((TCComponentItemRevision) selectedComps[i]).getItem()
                        .getLatestItemRevision();
                {
                    creRelationObjectHelperArr[i] = new CreateRelationObjectHelper(
                            CreateRelationObjectHelper.OPERATION_CREATE_RELATION);
                    creRelationObjectHelperArr[i].setPrimaryObject(newItemRev);
                    creRelationObjectHelperArr[i].setSecondaryObject(rdComponent);
                    creRelationObjectHelperArr[i].setRelationName("RelatedDocuments");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
        }
        return creRelationObjectHelperArr;
        
    }
    
    public static DeleteObjectHelper[] getRemoveRDRelationObjects(TCComponent[] selectedComps, TCComponent rdComponent)
    {
        DeleteObjectHelper[] removeRelationObjHelperArr = new DeleteObjectHelper[selectedComps.length];
        for (int i = 0; i < selectedComps.length; i++)
        {
            try
            {
                TCComponentItemRevision newItemRev = ((TCComponentItemRevision) selectedComps[i]).getItem()
                        .getLatestItemRevision();
                {
                    removeRelationObjHelperArr[i] = new DeleteObjectHelper(DeleteObjectHelper.OPERATION_DELETE_RELATION);
                    removeRelationObjHelperArr[i].setPrimaryObject(newItemRev);
                    removeRelationObjHelperArr[i].setSecondaryObject(rdComponent);
                    removeRelationObjHelperArr[i].setRelationName("RelatedDocuments");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            
        }
        return removeRelationObjHelperArr;
        
    }
    
    public static TCComponent[] getLatestRevCompArr(TCComponent[] tcCompArr)
    {
        TCComponent[] tcLatestRevCompArr = new TCComponent[tcCompArr.length];
        for (int i = 0; i < tcCompArr.length; i++)
        {
            try
            {
                TCComponentItem item = ((TCComponentItemRevision) tcCompArr[i]).getItem();
                tcLatestRevCompArr[i] = item.getLatestItemRevision();
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        return tcLatestRevCompArr;
    }
    
}
