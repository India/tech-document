package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.noi.NationalOilwell;
import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.util.components.ItemSearchResultTable.SearchTreeTableNode;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.quicksearch.services.utils.Utils;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.validations.additem.NOVECRSelectionDialog;
import com.nov.rac.dhl.eco.validations.additem.NOVECRTable;
import com.nov.rac.dhl.eco.validations.additem.NOVECRTableLine;
import com.nov.rac.dhl.eco.wizard.NOVCreateECOHelper;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.lov.LOVHierarchyPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.UIUtilities;
import com.teamcenter.rac.util.combobox.iComboBox;

public class ItemSearchDialog extends AbstractAIFDialog implements INOVSearchProvider
{
    private static final long serialVersionUID = 1L;

    private JTextField m_itemIDtxt;
    private JTextField m_itemNametxt;
    private JTextField m_partSubTypetxt;
    private iComboBox m_itemTypeCmb;
    private iComboBox m_lifeCycleCmb;
    private JButton m_searchBtn;

    private JToggleButton m_classPopupButton = null;
    private JDialog m_classpopup = null;
    private LOVHierarchyPanel m_hierarchyPanel = null;
    private JLabel m_searchStatusLbl;

    private JButton m_addTargetbtn;
    private JButton m_cancelbtn;

    private String m_sItemId = null;
    private String m_sItemName = null;
    private String m_sItemType = null;
    private String m_sPartSubType = null;
    private String m_sLifeCycle = null;

    private TCSession m_session;
    private Registry m_registry = Registry.getRegistry(this);

    private NOVCRItemSelectionPanel m_itemSelPanel;
    private NOVECOTargetItemsPanel m_targetItemSelPanel;
    private MassBOMChangeDialog m_massBOMChDlg;
    private MassRDChangeComposite m_massRDChangeDlg; //5733
    private TCComponentItemRevision m_itemRevtoSet;

    private String[] m_itemTypeList = null;
    private String m_defaultItemType = null;
    private boolean m_isItem2Change;
    private ItemSearchResultTable m_searchResultTable;
    private int m_selectionMode;
    private int m_selectedCount;
    private Vector<TCComponent> m_ecrVector = new Vector<TCComponent>();
	private     String                  m_sValidationMSG      = null;
	private     Map<TCComponent,Vector<TCComponent>> m_itemRevECRMap = new HashMap<TCComponent,Vector<TCComponent>>();
	private INOVSearchResult m_searchResult = null;
    private boolean fromECOSearchDialog     =false;//7804
    protected iComboBox m_mmrCmb;//7804
    protected String sChangeType=null;//7804
    protected Map<String, String> mapItemwithRevType = new HashMap<String, String>();  //7804
    public ItemSearchDialog(Frame parent,NOVCRItemSelectionPanel itemSelectionPanel) throws TCException, IOException
    {
        super(parent);
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_itemSelPanel = itemSelectionPanel;
        m_selectionMode = ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
        initItemTypeList();
        fromECOSearchDialog=true;//7804
        sChangeType=m_itemSelPanel.crSelectpanel.typeofChange;//7804
        createUI(m_itemSelPanel);
        this.centerToScreen();
    }

    public ItemSearchDialog(NOVECOTargetItemsPanel targetItemSelectionPanel) throws TCException, IOException
    {
        super(AIFUtility.getActiveDesktop());
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_targetItemSelPanel = targetItemSelectionPanel;
        m_selectionMode = ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
        initItemTypeList();
        fromECOSearchDialog=true;//7804
        sChangeType=m_targetItemSelPanel.ecoFormPanel.typeofChange;//7804
        createUI(m_targetItemSelPanel);
        this.centerToScreen();
    }

    public ItemSearchDialog(MassBOMChangeDialog massBOMChangeDlg, boolean isItemToChange) throws TCException,
            IOException
    {
        super(massBOMChangeDlg);
        m_massBOMChDlg = massBOMChangeDlg;
        m_isItem2Change = isItemToChange;
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_selectionMode = ListSelectionModel.SINGLE_SELECTION;
        initItemTypeList();
        createUI(null);
        m_addTargetbtn.setText("Ok");
    }
    
    public ItemSearchDialog(MassRDChangeComposite massRDChangeDlg) throws TCException,  //5733
        IOException
    {
        super(AIFUtility.getActiveDesktop());
        m_massRDChangeDlg = massRDChangeDlg;
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_selectionMode = ListSelectionModel.SINGLE_SELECTION;
        initItemTypeList();
        createUI(null);
        m_addTargetbtn.setText("Ok");
    }

    /*
     * 18.04.2012:Rakesh - TCDECREL-2327 - Mass BOM changes - Set Item Type list
     * depending on context - Starts here
     */
    private void initItemTypeList()
    {
        if (m_targetItemSelPanel != null || m_itemSelPanel != null)
        {
            if (m_session.getCurrentGroup().toString().substring(0, 2).equalsIgnoreCase("9A")
                    || m_session.getCurrentGroup().toString().substring(0, 2).equalsIgnoreCase("1B"))
            {
                m_itemTypeList = new String[] { "Documents", "Engineering" };
            }
            else
            {
                m_itemTypeList = new String[] { "Documents", "Engineering", "Non-Engineering" };
            }
            m_defaultItemType = "Engineering";
        }

        if (m_massBOMChDlg != null)
        {
            m_itemTypeList = new String[] { "Engineering", "Non-Engineering" };
            m_defaultItemType = "Engineering";
        }
        if (m_massRDChangeDlg != null) //5733
        {
            m_itemTypeList = new String[] { "Documents" };
            m_defaultItemType = "Documents";
        }
    }

    /*
     * 18.04.2012:Rakesh - TCDECREL-2327 - Mass BOM changes - Set Item Type list
     * depending on context - Ends here
     */

    private void createUI(final JPanel panel) throws TCException, IOException
    {
        // Mohan : All NOVLogger statements are commented out because it is
        // failing in CITRIX

        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("create UI");

        /* Build Search Criteria Panel */
        JPanel searchCriPanel = buildSearchCriteriaPanel();

        /* Build Search Result Panel */
        JPanel searchResultPanel = buildSearchResultPanel(panel);

        /* Build Button panel */
        JPanel btnPanel = buildButtonPanel(panel);

        /* Add All panels to the main panel */
        JPanel contentPanel = new JPanel();
        contentPanel.add(searchCriPanel, BorderLayout.NORTH);
        contentPanel.add(searchResultPanel, BorderLayout.CENTER);
        contentPanel.add(btnPanel, BorderLayout.SOUTH);

        this.setPreferredSize(new Dimension(700, 509));
        this.getContentPane().add(contentPanel);
        this.setResizable(false);
        this.setTitle("Item Search");
        this.pack();
        this.setModal(true);
    }

    private JPanel buildSearchCriteriaPanel()
    {

        JPanel searchCriPanel = new JPanel();

        GridBagLayout gbl_searchCriPanel = new GridBagLayout();
        gbl_searchCriPanel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_searchCriPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
        gbl_searchCriPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0,
                0.0, Double.MIN_VALUE };
        gbl_searchCriPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        searchCriPanel.setLayout(gbl_searchCriPanel);

        /*
         *  ****************************** Item ID
         * *******************************
         */
        JLabel itemIdLbl = new JLabel("Item ID");
        GridBagConstraints gbc_itemIdLbl = new GridBagConstraints();
        gbc_itemIdLbl.anchor = GridBagConstraints.WEST;
        gbc_itemIdLbl.insets = new Insets(0, 0, 5, 5);
        gbc_itemIdLbl.gridx = 0;
        gbc_itemIdLbl.gridy = 0;
        searchCriPanel.add(itemIdLbl, gbc_itemIdLbl);

        m_itemIDtxt = new JTextField(14);
        m_itemIDtxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    search_PopulateItems();
                }
            }
        });
        GridBagConstraints gbc_itemIDtxt = new GridBagConstraints();
        gbc_itemIDtxt.insets = new Insets(0, 0, 5, 5);
        gbc_itemIDtxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_itemIDtxt.gridx = 1;
        gbc_itemIDtxt.gridy = 0;
        searchCriPanel.add(m_itemIDtxt, gbc_itemIDtxt);
        m_itemIDtxt.setColumns(10);
        /* ********************************************************************* */

        /* ***************************** Item Name ***************************** */
        JLabel itemNameLbl = new JLabel("Item Name");
        GridBagConstraints gbc_itemNameLbl = new GridBagConstraints();
        gbc_itemNameLbl.anchor = GridBagConstraints.WEST;
        gbc_itemNameLbl.insets = new Insets(0, 0, 5, 5);
        gbc_itemNameLbl.gridx = 5;
        gbc_itemNameLbl.gridy = 0;
        searchCriPanel.add(itemNameLbl, gbc_itemNameLbl);

        m_itemNametxt = new JTextField(14);
        m_itemNametxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    search_PopulateItems();
                }
            }
        });
        GridBagConstraints gbc_itemNametxt = new GridBagConstraints();
        gbc_itemNametxt.insets = new Insets(0, 0, 5, 5);
        gbc_itemNametxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_itemNametxt.gridx = 6;
        gbc_itemNametxt.gridy = 0;
        searchCriPanel.add(m_itemNametxt, gbc_itemNametxt);
        m_itemNametxt.setColumns(10);
        /* ******************************************************************** */

        /* ****************************** Search Button *********************** */
        m_searchBtn = new JButton();
        ImageIcon searchIcon = m_registry.getImageIcon("search.ICON");
        m_searchBtn.setIcon(searchIcon);
        m_searchBtn.setPreferredSize(new Dimension(20, 20));
        m_searchBtn.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent mouseevent)
            {
                // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("search populate");
                search_PopulateItems();
            }
        });

        GridBagConstraints gbc_searchBtn = new GridBagConstraints();
        gbc_searchBtn.insets = new Insets(0, 0, 5, 0);
        gbc_searchBtn.gridx = 12;
        gbc_searchBtn.gridy = 0;
        searchCriPanel.add(m_searchBtn, gbc_searchBtn);
        /* ***************************************************************** */

        /* ****************************** Item Type ************************ */
        JLabel itemTypeLbl = new JLabel("Item type");
        GridBagConstraints gbc_itemTypeLbl = new GridBagConstraints();
        gbc_itemTypeLbl.anchor = GridBagConstraints.WEST;
        gbc_itemTypeLbl.insets = new Insets(0, 0, 5, 5);
        gbc_itemTypeLbl.gridx = 0;
        gbc_itemTypeLbl.gridy = 1;
        searchCriPanel.add(itemTypeLbl, gbc_itemTypeLbl);

        m_itemTypeCmb = new iComboBox(m_itemTypeList);
        m_itemTypeCmb.setSelectedItem(m_defaultItemType);
        m_itemTypeCmb.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                String itemType = m_itemTypeCmb.getSelectedItem().toString();
                if (itemType.equals("Documents"))
                {
                    /*
                     * Restrict PartSubType selection; Reset PartSubType field
                     * and Disable for selection
                     */
                    m_partSubTypetxt.setText("");
                    m_partSubTypetxt.setEnabled(false);
                    m_classPopupButton.setEnabled(false);
                }
                else
                {
                    m_partSubTypetxt.setEnabled(true);
                    m_classPopupButton.setEnabled(true);
                }
            }
        });
        GridBagConstraints gbc_itemTypeCmb = new GridBagConstraints();
        gbc_itemTypeCmb.insets = new Insets(0, 0, 5, 5);
        gbc_itemTypeCmb.fill = GridBagConstraints.HORIZONTAL;
        gbc_itemTypeCmb.gridx = 1;
        gbc_itemTypeCmb.gridy = 1;
        searchCriPanel.add(m_itemTypeCmb, gbc_itemTypeCmb);
        /* ***************************************************************** */

        /* ************************** Part Sub Type ************************ */
        JLabel partSubTypeLbl = new JLabel("Part Sub type");
        GridBagConstraints gbc_partSubTypeLbl = new GridBagConstraints();
        gbc_partSubTypeLbl.insets = new Insets(0, 0, 5, 5);
        gbc_partSubTypeLbl.gridx = 5;
        gbc_partSubTypeLbl.gridy = 1;
        searchCriPanel.add(partSubTypeLbl, gbc_partSubTypeLbl);

        m_partSubTypetxt = new JTextField(14);
        m_partSubTypetxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    search_PopulateItems();
                }
            }
        });
        GridBagConstraints gbc_partSubTypetxt = new GridBagConstraints();
        gbc_partSubTypetxt.insets = new Insets(0, 0, 5, 5);
        gbc_partSubTypetxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_partSubTypetxt.gridx = 6;
        gbc_partSubTypetxt.gridy = 1;
        searchCriPanel.add(m_partSubTypetxt, gbc_partSubTypetxt);
        /* ***************************************************************** */

        /* ************************ Class Popup button ********************* */
        m_classPopupButton = new JToggleButton();
        // Registry registry =
        // Registry.getRegistry("com.teamcenter.common.rac.lov.lov");
        // m_classPopupButton.setIcon(registry.getImageIcon("hierarchy.ICON"));
        m_classPopupButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    m_classpopup = new JDialog(ItemSearchDialog.this, true);
                    m_classpopup.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
                    m_classpopup.setTitle("Classification Part Sub Type");

                    /*
                     * TCComponentListOfValues typeLov =
                     * TCComponentListOfValuesType .findLOVByName((TCSession)
                     * AIFUtility.getDefaultSession(),
                     * NationalOilwell.DHITEMTYPELOV);
                     */
                    String lovName = NationalOilwell.DHITEMTYPELOV
                            + m_session.getCurrentGroup().toString().subSequence(0, 2);
                    TCComponentListOfValues typeLov = TCComponentListOfValuesType.findLOVByName(m_session, lovName);

                    m_hierarchyPanel = new LOVHierarchyPanel(typeLov.getSession(), true);
                    m_hierarchyPanel.setLovComponent(typeLov);
                    m_hierarchyPanel.setPreferredSize(UIUtilities.getSizeRelatedToScreen(0.25D, 0.25D));
                    m_hierarchyPanel.setValidationPanelVisible(false);

                    m_hierarchyPanel.addMouseListenerToTree(new MouseAdapter()
                    {
                        public void mouseClicked(MouseEvent mouseevent)
                        {
                            if (mouseevent.getClickCount() == 2)
                            {
                                m_partSubTypetxt.setText(m_hierarchyPanel.getSelectedName());
                                m_classpopup.dispose();
                            }
                        }
                    });

                    m_classpopup.add(m_hierarchyPanel);
                    m_classpopup.setSize(250, 250);
                    int x = m_classPopupButton.getLocationOnScreen().x - m_classpopup.getWidth();
                    int y = m_classPopupButton.getLocationOnScreen().y - m_classpopup.getHeight();
                    m_classpopup.setLocation(x, y);
                    m_classpopup.repaint();
                    m_classpopup.validate();
                    m_classpopup.setModal(true);
                    m_classpopup.setVisible(true);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });
        m_classPopupButton.setBackground(this.getBackground());
        m_classPopupButton.setPreferredSize(new Dimension(21, 21));
        GridBagConstraints gbc_classPopupButton = new GridBagConstraints();
        gbc_classPopupButton.insets = new Insets(0, 0, 5, 0);
        gbc_classPopupButton.gridx = 12;
        gbc_classPopupButton.gridy = 1;
        searchCriPanel.add(m_classPopupButton, gbc_classPopupButton);
        /* ***************************************************************** */

        /* ************************* Item Status *************************** */
        JLabel lifeCycleLbl = new JLabel("Item Status");
        GridBagConstraints gbc_lifeCycleLbl = new GridBagConstraints();
        gbc_lifeCycleLbl.anchor = GridBagConstraints.WEST;
        gbc_lifeCycleLbl.insets = new Insets(0, 0, 5, 5);
        gbc_lifeCycleLbl.gridx = 0;
        gbc_lifeCycleLbl.gridy = 2;
        searchCriPanel.add(lifeCycleLbl, gbc_lifeCycleLbl);

        TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName(m_session, "DH_Lifecycle");
        String[] lovS = null;
        try
        {
            lovS = lovValues.getListOfValues().getStringListOfValues();
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Vector<String> lifecycleVals = new Vector<String>();
        lifecycleVals.add("");
        for (int i = 0; i < lovS.length; i++)
        {
            lifecycleVals.add(lovS[i]);
        }
        String[] lifecycleValueArr = lifecycleVals.toArray(new String[lifecycleVals.size()]);
        m_lifeCycleCmb = new iComboBox(lifecycleValueArr);

        GridBagConstraints gbc_lifeCycleCmb = new GridBagConstraints();
        gbc_lifeCycleCmb.insets = new Insets(0, 0, 5, 5);
        gbc_lifeCycleCmb.fill = GridBagConstraints.HORIZONTAL;
        gbc_lifeCycleCmb.gridx = 1;
        gbc_lifeCycleCmb.gridy = 2;
        searchCriPanel.add(m_lifeCycleCmb, gbc_lifeCycleCmb);
        //7804-start
        if(fromECOSearchDialog && sChangeType.equalsIgnoreCase("General"))
        {
        	 JLabel mmrLbl = new JLabel("");
             GridBagConstraints gbc_mmrLbl = new GridBagConstraints();
             gbc_mmrLbl.anchor = GridBagConstraints.WEST;
             gbc_mmrLbl.insets = new Insets(0, 0, 5, 5);
             gbc_mmrLbl.gridx = 5;
             gbc_mmrLbl.gridy = 2;
             searchCriPanel.add(mmrLbl, gbc_mmrLbl);
             
             String[] mmrValues={"Create Major Revision", "Create Minor Revision"};
             String   defaultRevision="Create Major Revision";
             m_mmrCmb=new iComboBox(mmrValues);
             m_mmrCmb.setSelectedItem(defaultRevision);
             m_mmrCmb.setEditable(false);
             GridBagConstraints gbc_mmrCmb = new GridBagConstraints();
             gbc_mmrCmb.insets = new Insets(0, 0, 5, 5);
             gbc_mmrCmb.fill = GridBagConstraints.HORIZONTAL;
             gbc_mmrCmb.gridx = 6;
             gbc_mmrCmb.gridy = 2;
             searchCriPanel.add(m_mmrCmb, gbc_mmrCmb);
             
             
        }
        //7804-end
        /* ***************************************************************** */

        return searchCriPanel;
    }

    private JPanel buildSearchResultPanel(final JPanel panel)
    {
        /* Build Search Tree Table */
        String[] columnNames = { "Item ID", "Name", "PartSubType", "Lifecycle" };
        m_searchResultTable = new ItemSearchResultTable(m_session, m_selectionMode, columnNames);

        /*
         * if (panel instanceof NOVECOTargetItemsPanel) { m_searchResultTable =
         * new ItemSearchResultTable(m_session,
         * ListSelectionModel.MULTIPLE_INTERVAL_SELECTION, columnNames); } else
         * { m_searchResultTable = new ItemSearchResultTable(m_session,
         * ListSelectionModel.SINGLE_SELECTION, columnNames); }
         */

        /* Add Search tree table Listener */
        addSearchResultTableListener(panel);

        /* Add Scroll pane */
        JScrollPane treePane = new JScrollPane(m_searchResultTable);
        treePane.setPreferredSize(new Dimension(650, 310));
        treePane.getViewport().setBackground(Color.white);

        /* Prepare Search Status in RED BOLD */
        m_searchStatusLbl = new JLabel("");
        m_searchStatusLbl.setFont(new Font(m_searchStatusLbl.getFont().getFamily(), Font.BOLD, m_searchStatusLbl
                .getFont().getSize()));
        m_searchStatusLbl.setForeground(Color.RED);
        m_searchStatusLbl.setAlignmentX(CENTER_ALIGNMENT);
        m_searchStatusLbl.setAlignmentY(CENTER_ALIGNMENT);

        /* Add Search Status field to the screen */
        JPanel statusPanel = new JPanel();
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.Y_AXIS));
        statusPanel.add(treePane);
        statusPanel.add(m_searchStatusLbl);

        return statusPanel;
    }

    private JPanel buildButtonPanel(final JPanel panel)
    {
        JPanel btnPanel = new JPanel();
        m_addTargetbtn = new JButton("Add To Targets");
        m_addTargetbtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent arg0)
            {
                TCComponent[] selcomp = null;
                m_itemRevECRMap.clear();
                TreeTableNode[] selNodes = m_searchResultTable.getSelectedNodes();
                if (selNodes == null || selNodes.length == 0)
                {
                    JOptionPane.showMessageDialog(ItemSearchDialog.this, m_registry.getString("NoTargetItemSelection.MSG"), "Warning...",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                }
                else
                {
                    m_addTargetbtn.setEnabled(false); //TCDECREL-4794
                	boolean isValidObjectSelected = true;

                    String[] strPUID = new String[selNodes.length];
                    for (int i = 0; i < selNodes.length; i++)
                    {
                    	if(selNodes[i] instanceof SearchTreeTableNode)
                    	{
                    		strPUID[i] = ((SearchTreeTableNode) selNodes[i]).getUID(); // Construct
                    		// PUID
                    		// String
                    		// array
                    	}
                    	else
                    	{
                    		isValidObjectSelected = false;
                    	}
                    }
                    if(isValidObjectSelected == false)
                	{
                		JOptionPane.showMessageDialog(ItemSearchDialog.this, m_registry.getString("InvalidTargetSelection.MSG"), "Warning...",
                                JOptionPane.WARNING_MESSAGE);
                		m_addTargetbtn.setEnabled(true); //TCDECREL-4794
                		return;
                	}

                    try
                    {
                        selcomp = m_session.stringToComponent(strPUID); // Obtain
                                                                        // TCComponent
                                                                        // array
                                                                        // from
                                                                        // the
                                                                        // given
                                                                        // PUID
                                                                        // array
                    }
                    catch (TCException e)
                    {
                        e.printStackTrace();
                    }

                    ItemSearchDialog.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    if (panel != null)
                    {
                    	/*
                    	 * Rakesh - Check for Preference value to call SOA service
                    	 * or the existing java code
                    	 */
                    	TCPreferenceService prefService = m_session.getPreferenceService();
                    	String addComponentToTargetSOA = prefService.getString(TCPreferenceService.TC_preference_site,
                    	"addComponentToTargetSOA");

                    	if (addComponentToTargetSOA.compareToIgnoreCase("Y") == 0
                    			&& panel instanceof NOVECOTargetItemsPanel)
                    	{
                    		try
                    		{
                    			addItemsToTargetSOA(panel, selcomp);
                    		}
                    		catch (TCException e)
                    		{
                    			e.printStackTrace();
                    		}
                    	}
                    	else
                    	{
							//addItemsToTarget(panel, selcomp);
							if(panel instanceof NOVCRItemSelectionPanel)
							{
						    	Vector<TCComponent> selectedComps = new Vector<TCComponent>(Arrays.asList(selcomp));
						    	addItemsToTargetECO(selectedComps);
							}
							else
							{
                    			addItemsToTarget(panel, selcomp);
                    		}
                    	}
                	}
                    else if (m_massBOMChDlg != null)
                	{
                		if (selcomp[0] instanceof TCComponentItemRevision || selcomp[0] instanceof TCComponentItem)
                		{
                			if (selcomp[0] instanceof TCComponentItem)
                			{
                				try
                				{
                					m_itemRevtoSet = ((TCComponentItem) selcomp[0]).getLatestItemRevision();
                				}
                				catch (TCException e1)
                				{
                					e1.printStackTrace();
                				}
                			}
                			else
                			{
                				m_itemRevtoSet = (TCComponentItemRevision) selcomp[0];
                			}
                			if (m_isItem2Change)
                			{
                				// massBOMChDlg.whrUsedTable.removeAllRows();
                				m_massBOMChDlg.m_txtItemChange.setText(m_itemRevtoSet.toString());
                				m_massBOMChDlg.m_txtItemChange.setCaretPosition(0); // 18.04.2012:Rakesh
                				// -
                				// TCDECREL-2327
                				// -
                				// View
                				// Item
                				// Name
                				// from
                				// beginning
                				m_massBOMChDlg.m_itemToChange = m_itemRevtoSet;

                				try
                				{
                					// massBOMChDlg.populateWhereUsedTable(((TCComponentItem)selcomp[0]).getLatestItemRevision());
                					m_massBOMChDlg.setWhrUsedItemRev(((TCComponentItem) selcomp[0])
                							.getLatestItemRevision());
                				}
                				catch (TCException e)
                				{
                					// TODO Auto-generated catch block
                					e.printStackTrace();
                				}
                			}
                			else
                			{
                				m_massBOMChDlg.m_txtRepItem.setText(m_itemRevtoSet.toString());
                				m_massBOMChDlg.m_txtRepItem.setCaretPosition(0); // 18.04.2012:Rakesh
                				// -
                				// TCDECREL-2327
                				// -
                				// View
                				// Item
                				// Name
                				// from
                				// beginning
                				m_massBOMChDlg.m_replacementItem = m_itemRevtoSet;
                			}
                			ItemSearchDialog.this.dispose();
                		}
                    }
                    else if(m_massRDChangeDlg != null) //5733
                    {
                        if (selcomp[0] instanceof  TCComponentItem)
                        {
                            final TCComponentItem selectedComp = (TCComponentItem)selcomp[0];
                            ItemSearchDialog.this.dispose();
                            Display.getDefault().asyncExec(new Runnable()
                            {
                               public void run()
                               {
                                   m_massRDChangeDlg.setSearchedDocument(selectedComp);
                                   
                               }
                            });
                            
                        }
                    }
                    ItemSearchDialog.this.setCursor(Cursor.getDefaultCursor());
                    m_addTargetbtn.setEnabled(true); //TCDECREL-4794
                }
            }
        });
        m_cancelbtn = new JButton("Cancel");
        m_cancelbtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                ItemSearchDialog.this.dispose();
            }
        });
        btnPanel.add(m_addTargetbtn, BorderLayout.EAST);
        btnPanel.add(m_cancelbtn, BorderLayout.WEST);

        return btnPanel;
    }

    private void addSearchResultTableListener(final JPanel panel)
    {
        m_searchResultTable.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                	m_itemRevECRMap.clear();
                    int row = ((ItemSearchResultTable) e.getSource()).getSelectedRow();
                    if (!(row > 0))
                    {
                        return;
                    }

                    String puid = ((SearchTreeTableNode) ((ItemSearchResultTable) e.getSource()).getNodeForRow(row))
                            .getUID();
                    TCComponent selComp = null;
                    try
                    {
                        selComp = m_session.stringToComponent(puid);
                    }
                    catch (TCException ex)
                    {
                        // TODO Auto-generated catch block
                        ex.printStackTrace();
                    }

                    if (selComp == null)
                    {
                        return;
                    }
                    ItemSearchDialog.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    if (panel != null)
                    {
                        TCPreferenceService prefService = m_session.getPreferenceService();
                        String addComponentToTargetSOA = prefService.getString(TCPreferenceService.TC_preference_site,
                                "addComponentToTargetSOA");

                        if (addComponentToTargetSOA.compareToIgnoreCase("Y") == 0
                                && panel instanceof NOVECOTargetItemsPanel)
                        {
                            try
                            {
								//addItemToTargetSOA(panel, selComp);
							    addItemsToTargetSOA(panel, new TCComponent[]{selComp});
							} catch (TCException e1)
                            {
                                e1.printStackTrace();
                            }
                        }
                        else
                        {
                            m_selectedCount = 1;
							if(panel instanceof NOVCRItemSelectionPanel)
							{
							    Vector<TCComponent> selectedComps = new Vector<TCComponent>();
							    selectedComps.add(selComp);
							    addItemsToTargetECO(selectedComps);
							}
							else
							{
                            	addItemToTarget(panel, selComp);
                        	}
                    	}
                    }
                    else if (m_massBOMChDlg != null)
                    {
                        if (selComp instanceof TCComponentItemRevision || selComp instanceof TCComponentItem)
                        {
                            if (selComp instanceof TCComponentItem)
                            {
                                try
                                {
                                    m_itemRevtoSet = ((TCComponentItem) selComp).getLatestItemRevision();
                                }
                                catch (TCException e1)
                                {
                                    e1.printStackTrace();
                                }
                            }
                            else
                            {
                                m_itemRevtoSet = (TCComponentItemRevision) selComp;
                            }
                            if (m_isItem2Change)
                            {
                                m_massBOMChDlg.m_txtItemChange.setText(m_itemRevtoSet.toString());
                                m_massBOMChDlg.m_txtItemChange.setCaretPosition(0); // 18.04.2012:Rakesh
                                                                                    // -
                                                                                    // TCDECREL-2327
                                                                                    // -
                                                                                    // View
                                                                                    // Item
                                                                                    // Name
                                                                                    // from
                                                                                    // beginning
                                m_massBOMChDlg.m_itemToChange = m_itemRevtoSet;
                                m_massBOMChDlg.setWhrUsedItemRev(m_itemRevtoSet);
                            }
                            else
                            {
                                m_massBOMChDlg.m_txtRepItem.setText(m_itemRevtoSet.toString());
                                m_massBOMChDlg.m_txtRepItem.setCaretPosition(0); // 18.04.2012:Rakesh
                                                                                 // -
                                                                                 // TCDECREL-2327
                                                                                 // -
                                                                                 // View
                                                                                 // Item
                                                                                 // Name
                                                                                 // from
                                                                                 // beginning
                                m_massBOMChDlg.m_replacementItem = m_itemRevtoSet;
                            }
                            ItemSearchDialog.this.dispose();
                        }
                    }
                    else if(m_massRDChangeDlg != null) //5733
                    {
                        if (selComp instanceof  TCComponentItem)
                        {
                            final TCComponentItem selectedComp = (TCComponentItem)selComp;
                            ItemSearchDialog.this.dispose();
                            Display.getDefault().asyncExec(new Runnable() 
                            {
                               public void run()
                               {
                                   m_massRDChangeDlg.setSearchedDocument(selectedComp);
                               }
                            });
                            
                        }
                    }
                    ItemSearchDialog.this.setCursor(Cursor.getDefaultCursor());
                }
            }
        });

    }

    /*
     * Rakesh - For ECO Add Targets, call SOA service implementation function to
     * add to target - starts here
     */
    private void _addItemToTargetSOA(JPanel panel, TCComponent selcomp) throws TCException
    {
        boolean isValidRevAdded = false;
        TCComponentItemRevision selcompItemRev = ((TCComponentItem) selcomp).getLatestItemRevision();
        AIFComponentContext[] compConxt = getChangeRequestContextObjects(selcompItemRev.getRelated("RelatedECN"));

        if (compConxt.length > 1)
        {

            validateECR(compConxt);
            if (m_ecrVector != null && m_ecrVector.size() > 0)
            {
                dispose();
                MultiSelectCRItemDialog crItemdlg = new MultiSelectCRItemDialog(m_targetItemSelPanel, selcompItemRev,
                        m_ecrVector);
                crItemdlg.setVisible(true);
            }
            else
            {
                /*isValidRevAdded = m_targetItemSelPanel.ecoFormPanel.addComponentToTarget("NOVECOTargetItemsPanel",
                        new TCComponent[] { selcomp });*/
            }
        }
        else
        {
            /*isValidRevAdded = m_targetItemSelPanel.ecoFormPanel.addComponentToTarget("NOVECOTargetItemsPanel",
                    new TCComponent[] { selcomp });*/
        }

        if (isValidRevAdded)
        {
            m_targetItemSelPanel.ecoFormPanel.ecoForm.save();
            String msgStr = "The Item " + ((TCComponentItem) selcomp).getProperty("item_id") + " is added to targets.";
            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item", JOptionPane.WARNING_MESSAGE);
            m_targetItemSelPanel.ecoFormPanel.ecoForm.refresh();
        }

    }

    /*
     * Rakesh - For ECO Add Targets, call SOA service implementation function to
     * add to target - ends here
     */

    /*
     * Rakesh - TCDECREL-2604 - For ECO Add Targets, call SOA service to add
     * multiple items to target - starts here
     */
    private void _addItemsToTargetSOA(JPanel panel, TCComponent[] selComps) throws TCException
    {
        TCComponent[] selCompArray = new TCComponent[selComps.length];
        int count = 0;
        boolean isValidRevAdded = false;

        for (int i = 0, l = selComps.length; i < l; i++)
        {
            TCComponentItemRevision selcompItemRev = ((TCComponentItem) selComps[i]).getLatestItemRevision();
            AIFComponentContext[] compConxt = getChangeRequestContextObjects(selcompItemRev.getRelated("RelatedECN"));

            if (compConxt.length > 1)
            {
                validateECR(compConxt);
                if (m_ecrVector != null && m_ecrVector.size() > 0)
                {
                    dispose();
                    MultiSelectCRItemDialog crItemdlg = new MultiSelectCRItemDialog(m_targetItemSelPanel,
                            selcompItemRev, m_ecrVector);
                    crItemdlg.setVisible(true);
                }
                else
                {
                    selCompArray[count] = selComps[i];
                    count++;
                }
            }
            else
            {
                selCompArray[count] = selComps[i];
                count++;
            }
        }
        if (selCompArray.length > 0 && selCompArray != null)
        {
            /*isValidRevAdded = m_targetItemSelPanel.ecoFormPanel.addComponentToTarget("NOVECOTargetItemsPanel",
                    selCompArray);*/
        }

        if (isValidRevAdded)
        {
            m_targetItemSelPanel.ecoFormPanel.ecoForm.save();
            /*
             * MessageBox.post("Selected Items are added to targets.",
             * "Added Item",MessageBox.WARNING);
             */
            String msgStr = "Selected Items are added to targets.";
            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item", JOptionPane.WARNING_MESSAGE);
            // m_targetItemSelPanel.ecoFormPanel.ecoForm.refresh();
        }

        /*
         * Refresh Target list in case some items were added and rest errored
         * out
         */
        m_targetItemSelPanel.ecoFormPanel.ecoForm.refresh();
    }

    private void validateECR(AIFComponentContext[] compConxt)
    {
        try
        {
        	m_ecrVector = new Vector<TCComponent>();
            m_ecrVector.clear();
            if (compConxt.length > 0)
            {

                for (int i = 0; i < compConxt.length; i++)
                {
                    InterfaceAIFComponent infComp = compConxt[i].getComponent();
                    if (infComp.getType().equalsIgnoreCase("_EngChangeRequest_"))
                    {
                        TCComponentForm ecrform = (TCComponentForm) infComp;
                        String statuses = ((TCComponentForm) infComp).getProperty("release_statuses");
                        if (statuses.contains("Open"))
                        {
                            m_ecrVector.add(ecrform);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /*
     * Rakesh - TCDECREL-2604 - For ECO Add Targets, call SOA service to add
     * multiple items to target - ends here
     */

    private void addItemsToTarget(JPanel panel, TCComponent[] selcomp)
    {
        int compToAddCount = selcomp.length;
        int compAddedCount = 0;
        String msgStr = "";
        String errStr = "";

        for (int i = 0; i < compToAddCount; i++)
        {
            if (addItemToTarget(panel, selcomp[i])) // Component Added
                                                    // successfully
                compAddedCount++;
            else
            {
                try
                {
                    if (errStr.isEmpty())
                        errStr = ((TCComponentItem) selcomp[i]).getProperty("object_string");
                    else
                        errStr = errStr + ", " + ((TCComponentItem) selcomp[i]).getProperty("object_string");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }

        if (compToAddCount == compAddedCount)
        {
            msgStr = "Selected Item(s) are added to target.";
        }
        else
        {
            // msgStr = "Below Item(s) could not be added to target: \n" +
            // errStr;
            msgStr = "Some Items could not be added to target.";
        }

        JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item", JOptionPane.WARNING_MESSAGE);
    }

    private boolean addItemToTarget(JPanel panel, TCComponent selcomp)
    {
        // Validate the item revision based on typeofChange
        // For general change orders: The latest released item revision will
        // always be added.
        // Newly created items (items with no released revisions) can be added;
        // only the working revision will be added to the change.
        // For lifecycle change orders, only the latest released revision can be
        // added.

        if (selcomp instanceof TCComponentItemRevision || selcomp instanceof TCComponentItem)
        {
            try
            {
                /*
                 * Rakesh - Check for Preference value to call SOA service or
                 * the existing java code
                 */
                /*
                 * TCPreferenceService prefService =
                 * session.getPreferenceService(); String
                 * addComponentToTargetSOA =
                 * prefService.getString(TCPreferenceService
                 * .TC_preference_site,"addComponentToTargetSOA");
                 * if(addComponentToTargetSOA.compareToIgnoreCase("Y") == 0 &&
                 * panel instanceof NOVECOTargetItemsPanel) {
                 * addItemToTargetSOA(panel, selcomp); } else
                 */
                {
                    if (selcomp instanceof TCComponentItem)
                    {
                        TCComponentItemRevision[] workingRevs = ((TCComponentItem) selcomp).getWorkingItemRevisions();
                        if (workingRevs.length > 1)
                        {
                            /*
                             * MessageBox.post("The Item "+
                             * ((TCComponentItem)selcomp
                             * ).getProperty("object_string")+
                             * " contains many working revisions.\n"+
                             * "Please delete the revision(s) not required and then continue the process."
                             * ,"Cannot add Item",MessageBox.WARNING);
                             */
                            if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                            {
                                String msgStr = "The Item " + ((TCComponentItem) selcomp).getProperty("object_string")
                                        + " contains many working revisions.\n"
                                        + "Please delete the revision(s) not required and then continue the process.";
                                JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                            return false;
                        }
                        selcomp = ((TCComponentItem) selcomp).getLatestItemRevision();
                    }

                    short checkedOutStatus = NOVECOHelper
                            .isItemRevSecObjectCheckedout((TCComponentItemRevision) selcomp);

                    if (checkedOutStatus == NOVECOConstants.Item_CheckedOut)
                    {
                        if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                        {
                            String msgStr = "The Item " + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                    + " is Checked-Out.\nCannot be added to Engineering Change.";
                            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                    JOptionPane.WARNING_MESSAGE);
                        }

                        return false;
                    }

                    if (checkedOutStatus == NOVECOConstants.ItemRev_CheckedOut)
                    {
                        /*
                         * MessageBox.post("The Revision "+
                         * ((TCComponentItemRevision
                         * )selcomp).getProperty("object_string")+
                         * " is Checked-Out.\nCannot be added to Engineering Change."
                         * , "Cannot add Item",MessageBox.WARNING);
                         */
                        if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                        {
                            String msgStr = "The Revision "
                                    + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                    + " is Checked-Out.\nCannot be added to Engineering Change.";
                            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                    JOptionPane.WARNING_MESSAGE);
                        }

                        return false;
                    }

                    if (checkedOutStatus == NOVECOConstants.SecObjects_CheckedOut)
                    {
                        /*
                         * MessageBox.post("Dataset under revision "+
                         * ((TCComponentItemRevision
                         * )selcomp).getProperty("object_string")+
                         * " is Checked-Out.\nCannot be added to Engineering Change."
                         * , "Cannot add Item",MessageBox.WARNING);
                         */
                        if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                        {
                            String msgStr = "Dataset under revision "
                                    + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                    + " is Checked-Out.\nCannot be added to Engineering Change.";
                            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                        return false;
                    }

                    if (checkedOutStatus == NOVECOConstants.RDD_DOCRev_CheckedOut)
                    {
                        /*
                         * MessageBox.post(
                         * "The related defining document revision under "+
                         * ((TCComponentItemRevision
                         * )selcomp).getProperty("object_string")+
                         * " is Checked-Out.\nCannot be added to Engineering Change."
                         * , "Cannot add Item",MessageBox.WARNING);
                         */
                        if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                        {
                            String msgStr = "The related defining document revision under "
                                    + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                    + " is Checked-Out.\nCannot be added to Engineering Change.";
                            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                        return false;
                    }
                    if (checkedOutStatus == NOVECOConstants.RDD_DOCRev_SecObjects_CheckedOut)
                    {
                        /*
                         * MessageBox.post(
                         * "Dataset of related defining document revision under "
                         * + ((TCComponentItemRevision)selcomp).getProperty(
                         * "object_string")+
                         * " is Checked-Out.\nCannot be added to Engineering Change."
                         * , "Cannot add Item",MessageBox.WARNING);
                         */
                        if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                        {
                            String msgStr = "Dataset of related defining document revision under "
                                    + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                    + " is Checked-Out.\nCannot be added to Engineering Change.";
                            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                        return false;
                    }
                    /*
                     * if ( selcomp
                     * !=((TCComponentItemRevision)selcomp).getItem(
                     * ).getLatestItemRevision() ) {
                     * MessageBox.post("The Revision "+
                     * ((TCComponentItemRevision
                     * )selcomp).getProperty("object_string")+
                     * " is not latest revision."
                     * ,"Cannot add Item",MessageBox.WARNING); return ; }
                     */

                    TCComponentItemRevision[] inProcessRevs = ((TCComponentItemRevision) selcomp).getItem()
                            .getInProcessItemRevisions();
                    Vector<TCComponentItemRevision> inProccessRevVec = new Vector<TCComponentItemRevision>(
                            Arrays.asList(inProcessRevs));

                    if (inProccessRevVec.contains(selcomp))
                    {
                        /*
                         * MessageBox.post("The Revision "+
                         * ((TCComponentItemRevision
                         * )selcomp).getProperty("object_string")+
                         * " is already in process."
                         * ,"Cannot add Item",MessageBox.WARNING);
                         */
                        if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                        {
                            String msgStr = "The Revision "
                                    + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                    + " is already in process.";
                            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                    JOptionPane.WARNING_MESSAGE);
                        }
                        return false;
                    }

                    // If user has selected only Document revision then the
                    // respective part revision is selected.
                    // If no part revision is defined then that revision is
                    // added as target alone
                    if (selcomp.getType().equalsIgnoreCase("Documents Revision"))
                    {
                        // @Harshada - Code added for looking RDD part revisions
                        TCComponentItem searchedItem = ((TCComponentItemRevision) selcomp).getItem();
                        String[] types = { "Nov4Part Revision", "Purchased Part Revision", "Non-Engineering Revision" };
                        String[] relations = { "RelatedDefiningDocument" };
                        AIFComponentContext[] relatedPartRevs = searchedItem.whereReferencedByTypeRelation(types,
                                relations);

                        // AIFComponentContext[] relatedPartRevs =
                        // ((TCComponentItemRevision)selcomp).getItem().whereReferenced();

                        for (int i = 0; i < relatedPartRevs.length; i++)
                        {
                            // if
                            // (relatedPartRevs[i].getContext().equals("RelatedDefiningDocument"))
                            {
                                if (relatedPartRevs[i].getComponent() instanceof TCComponentItemRevision)
                                {
                                    TCComponentItem relatePart = ((TCComponentItemRevision) relatedPartRevs[i]
                                            .getComponent()).getItem();
                                    // Swamy
                                    TCComponentItem RDDItem = NOVECOHelper.getRDDItem(relatePart
                                            .getLatestItemRevision());
                                    if (RDDItem != null)
                                    {

                                        // Swamy
                                        String revId = selcomp.getProperty("current_revision_id");
                                        TCComponentItemRevision partRev = NOVECOHelper.getItemRevision(relatePart,
                                                revId);
                                        if (partRev == null)
                                        {
                                            return false;
                                        }
                                        else
                                        {
                                            // Re Assigning selected document
                                            // revision with part revision
                                            selcomp = partRev;
                                        }
                                    }
                                    else
                                    {
                                        System.out.println("Only Doc Item is required");
                                        // If Latest Reivsion doesn't have this
                                        // Document as RDD, then no need to pull
                                        // the Part Item only
                                        // Document Item needs to be added
                                        selcomp = selcomp;

                                    }
                                }
                            }
                        }
                        /*
                         * TCComponentItemRevision partRev =
                         * NOVECOHelper.getRelatedPartRevision
                         * ((TCComponentItemRevision)selcomp); if
                         * (partRev!=null) { //Re Assigning selected document
                         * revision with part revision selcomp = partRev; }
                         */
                    }

                    AIFComponentContext[] compConxt = getChangeRequestContextObjects(((TCComponentItemRevision) selcomp)
                            .getRelated("RelatedECN"));

                    TCComponent[] releaseStats = ((TCComponentItemRevision) selcomp).getTCProperty(
                            "release_status_list").getReferenceValueArray();
                    if (m_targetItemSelPanel != null)
                    {
                        if (m_targetItemSelPanel.ecoFormPanel.typeofChange.equals("Lifecycle"))
                        {
                            if (releaseStats.length == 0)
                            {
                                // MessageBox.post("The Revision "+
                                // ((TCComponentItemRevision)selcomp).getProperty("object_string")+
                                // " is not released.","Cannot add Item",MessageBox.WARNING);
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    String msgStr = "The Revision "
                                            + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                            + " is not released.";
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                                return false;
                            }
                        }
                    }
                    else if (m_itemSelPanel != null)
                    {
                        if (m_itemSelPanel.crSelectpanel.typeofChange.equals("Lifecycle"))
                        {
                            if (releaseStats.length == 0)
                            {
                                // MessageBox.post("The Revision "+
                                // ((TCComponentItemRevision)selcomp).getProperty("object_string")+
                                // " is not released.","Cannot add Item",MessageBox.WARNING);
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    String msgStr = "The Revision "
                                            + ((TCComponentItemRevision) selcomp).getProperty("object_string")
                                            + " is not released.";
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                                return false;
                            }
                        }
                    }

                    if (panel instanceof NOVECOTargetItemsPanel)
                    {
                        if (m_targetItemSelPanel.targetTable.strTargetItemIds
                                .contains(((TCComponentItemRevision) selcomp).getProperty("item_id")))
                        {
                            /*
                             * MessageBox.post("The Item "+
                             * ((TCComponentItemRevision
                             * )selcomp).getProperty("item_id")+
                             * " is already added to targets.",
                             * "Cannot add an Item",MessageBox.WARNING);
                             */
                            if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                            {
                                String msgStr = "The Item "
                                        + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                        + " is already added to targets.";
                                JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add Item",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                            return false;
                        }
                        if (compConxt.length == 1)
                        {
                            if (!NOVECOHelper.isValidECR(m_targetItemSelPanel.ecoFormPanel.typeofChange,
                                    compConxt[0].getComponent()))
                            {
                                // Messagebox shown in NOVECOHelper class
                                return false;
                            }
                            boolean isValidRevAdded = m_targetItemSelPanel.ecoFormPanel
                                    .addValidRevisionToTarget((TCComponentItemRevision) selcomp);

                            if (isValidRevAdded)
                            {
                                m_targetItemSelPanel.ecoFormPanel.ecoForm.save();
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    String msgStr = "The Item " + ((TCComponentItem) selcomp).getProperty("item_id")
                                            + " is added to targets.";
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item",
                                            JOptionPane.WARNING_MESSAGE);

                                }
                                m_targetItemSelPanel.ecoFormPanel.ecoForm.refresh();
                                return true;
                            }
                        }
                        else if (compConxt.length > 1)
                        {

                            validateECR(compConxt);
                            if (m_ecrVector != null && m_ecrVector.size() > 0)
                            {
                                dispose();
                                MultiSelectCRItemDialog crItemdlg = new MultiSelectCRItemDialog(m_targetItemSelPanel,
                                        (TCComponentItemRevision) selcomp, m_ecrVector);
                                crItemdlg.setVisible(true);
                            }
                            else
                            {
                                boolean isValidRevAdded = m_targetItemSelPanel.ecoFormPanel
                                        .addValidRevisionToTarget((TCComponentItemRevision) selcomp);

                                if (isValidRevAdded)
                                {
                                    m_targetItemSelPanel.ecoFormPanel.saveDatatoForm();
                                    /*
                                     * MessageBox.post("The Item "+
                                     * ((TCComponentItemRevision
                                     * )selcomp).getProperty("item_id")+
                                     * " is added to targets.",
                                     * "Added Item",MessageBox.WARNING);
                                     */
                                    if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                    {
                                        String msgStr = "The Item "
                                                + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                                + " is added to targets.";
                                        JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item",
                                                JOptionPane.WARNING_MESSAGE);
                                    }
                                    return true;
                                }
                            }
                        }
                        else
                        {
                            boolean isValidRevAdded = m_targetItemSelPanel.ecoFormPanel
                                    .addValidRevisionToTarget((TCComponentItemRevision) selcomp);

                            if (isValidRevAdded)
                            {
                                m_targetItemSelPanel.ecoFormPanel.saveDatatoForm();
                                /*
                                 * MessageBox.post("The Item "+
                                 * ((TCComponentItemRevision
                                 * )selcomp).getProperty("item_id")+
                                 * " is added to targets.",
                                 * "Added Item",MessageBox.WARNING);
                                 */
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    String msgStr = "The Item "
                                            + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                            + " is added to targets.";
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                                return true;
                            }
                        }
                    }

                    if (panel instanceof NOVCRItemSelectionPanel)
                    {
                        if (m_itemSelPanel.targetTable.strTargetItemIds.contains(((TCComponentItemRevision) selcomp)
                                .getProperty("item_id")))
                        {
                            /*
                             * MessageBox.post("The Item "+
                             * ((TCComponentItemRevision
                             * )selcomp).getProperty("item_id")+
                             * " is already added to targets.",
                             * "Cannot add an Item",MessageBox.WARNING);
                             */
                            if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                            {
                                String msgStr = "The Item "
                                        + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                        + " is already added to targets.";
                                JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add an Item",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                            return false;
                        }
                        // addItemtoTargetsList((TCComponent)selcomp);
                        if (compConxt.length == 1)
                        {
                            if (m_itemSelPanel.crSelectpanel.typeofChange.trim().length() == 0)
                            {
                                // MessageBox.post("Select Type of change.",
                                // "Wanring", MessageBox.WARNING);
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, "Select Type of change.",
                                            "Warning", JOptionPane.WARNING_MESSAGE);
                                }
                                return false;
                            }
                            int response = JOptionPane.showConfirmDialog(null,
                                    "Item Revision :" + selcomp.getProperty("object_string") + " is associated to CR :"
                                            + compConxt[0].getComponent().getProperty("object_name")
                                            + ". Do you want to add item to the change ?", "Confirm",
                                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (response == JOptionPane.YES_OPTION)
                            {
                                if (!NOVECOHelper.isValidECR(m_itemSelPanel.crSelectpanel.typeofChange,
                                        compConxt[0].getComponent()))
                                {
                                    // Messagebox shown in NOVECOHelper class
                                    return false;
                                }
                                boolean retval = m_itemSelPanel.targetTable.addTarget(
                                        (TCComponentItemRevision) selcomp,
                                        (TCComponentForm) compConxt[0].getComponent());
                                m_itemSelPanel.notifyObserver();
                                if (retval)
                                {
                                    /*
                                     * MessageBox.post("The Item "+
                                     * ((TCComponentItemRevision
                                     * )selcomp).getProperty("item_id")+
                                     * " is added to targets.",
                                     * "Added Item",MessageBox.WARNING);
                                     */
                                    if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                    {
                                        String msgStr = "The Item "
                                                + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                                + " is added to targets.";
                                        JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item",
                                                JOptionPane.WARNING_MESSAGE);
                                    }
                                    return true;
                                }
                                else
                                {
                                    // MessageBox.post("RelatedDefiningDocument relation contains other than Documents.",
                                    // "Cannot add item", MessageBox.WARNING);
                                    if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                    {
                                        String msgStr = "RelatedDefiningDocument relation contains other than Documents.";
                                        JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add item",
                                                JOptionPane.WARNING_MESSAGE);
                                    }
                                    return false;
                                }
                            }
                        }
                        else if (compConxt.length > 1)
                        {
                            // dispose();
                            validateECR(compConxt);
                            if (m_ecrVector != null && m_ecrVector.size() > 0)
                            {
                                dispose();
                                MultiSelectCRItemDialog crItemdlg = new MultiSelectCRItemDialog(m_itemSelPanel,
                                        (TCComponentItemRevision) selcomp, m_ecrVector);
                                crItemdlg.setVisible(true);
                            }
                            else
                            {
                                boolean retval = m_itemSelPanel.targetTable.addTarget(
                                        (TCComponentItemRevision) selcomp, null);
                                if (retval)
                                {
                                    /*
                                     * MessageBox.post("The Item "+
                                     * ((TCComponentItemRevision
                                     * )selcomp).getProperty("item_id")+
                                     * " is added to targets.",
                                     * "Added Item",MessageBox.WARNING);
                                     */
                                    if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                    {
                                        String msgStr = "The Item "
                                                + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                                + " is added to targets.";
                                        JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item",
                                                JOptionPane.WARNING_MESSAGE);
                                    }
                                    return true;
                                }

                            }

                        }
                        else
                        {
                            boolean retval = m_itemSelPanel.targetTable.addTarget((TCComponentItemRevision) selcomp,
                                    null);
                            if (retval)
                            {
                                /*
                                 * MessageBox.post("The Item "+
                                 * ((TCComponentItemRevision
                                 * )selcomp).getProperty("item_id")+
                                 * " is added to targets.",
                                 * "Added Item",MessageBox.WARNING);
                                 */
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    String msgStr = "The Item "
                                            + ((TCComponentItemRevision) selcomp).getProperty("item_id")
                                            + " is added to targets.";
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                                return true;
                            }
                            else
                            {
                                /*
                                 * MessageBox.post(
                                 * "RelatedDefiningDocument relation contains other than Documents."
                                 * , "Cannot add item", MessageBox.WARNING);
                                 */
                                if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION || m_selectedCount == 1)
                                {
                                    String msgStr = "RelatedDefiningDocument relation contains other than Documents.";
                                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Cannot add item",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
            }
        }
        else
        {
            // MessageBox.post("Select only Item or Item Revision","Info",MessageBox.INFORMATION);
            if (m_selectionMode == ListSelectionModel.SINGLE_SELECTION)
            {
                String msgStr = "Select only Item or Item Revision";
                JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Info", JOptionPane.WARNING_MESSAGE);
            }
            return false;
        }

        return true;
    }

    private AIFComponentContext[] getChangeRequestContextObjects(AIFComponentContext[] compConxt)
    {
        Vector<AIFComponentContext> compCxtArr = new Vector<AIFComponentContext>();
        if (compConxt.length > 0)
        {
            for (int i = 0; i < compConxt.length; i++)
            {
                InterfaceAIFComponent infComp = compConxt[i].getComponent();
                if (infComp.getType().equalsIgnoreCase("_EngChangeRequest_"))
                {
                    compCxtArr.add(compConxt[i]);
                }
            }
        }
        return compCxtArr.toArray(new AIFComponentContext[compCxtArr.size()]);
    }

    private void search_PopulateItems()
    {
        try
        {
            if (m_searchResultTable.getChildCount() > 0)
            {
                m_searchResultTable.removeAllChildren();
            }

            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            m_searchStatusLbl.setText("");

            String itemId = m_itemIDtxt.getText();
            String itemName = m_itemNametxt.getText();
            String itemType = (String) m_itemTypeCmb.getSelectedItem();
            String group = m_session.getGroup().toString();

            if (itemType.trim().length() == 0)
            {
                itemType = "*";
            }
            // Rakesh - 08.09.2011 - Code starts here
            // DHL-166:Send NOV4part for Item type Engineering in ECO item
            // addition form
            else
            {
                if (itemType.trim().equalsIgnoreCase("Engineering"))
                {
                    itemType = "Nov4Part";
                }
            }
            // Rakesh - 08.09.2011 - Code ends here

            if (itemName.trim().length() == 0)
            {
                itemName = "*";
            }
            if (itemId.trim().length() == 0)
            {
                itemId = "*";
            }

            m_searchResultTable.setItemType(itemType);

            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Before execute");
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(itemId + "," +
            // itemName + "," +itemType + "," +group );
            final INOVSearchProvider searchService = this;

            m_searchResult = null;
            final Display display = Display.getDefault();
            display.syncExec(new Runnable()
            {
				@Override
				public void run()
				{
					Shell shell = null;
					if(ItemSearchDialog.this.isDisplayable())
					{
						Canvas canvas = new Canvas();
						ItemSearchDialog.this.add(canvas);
						ItemSearchDialog.this.addNotify();
						shell = SWT_AWT.new_Shell(display, canvas);
					}
					else
					{
						shell = AIFUtility.getActiveDesktop().getShell();
					}
            		try
            		{
		                m_searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL,shell);
            		}
            		catch (Exception e)
            		{
               		 	e.printStackTrace();
            		}
				}
			});

            this.toFront();

            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("After execute");

            if (m_searchResult != null && m_searchResult.getResponse().nRows > 0)
            {
                INOVResultSet theResSet = m_searchResult.getResultSet();
                Vector<String> rowData = theResSet.getRowData();
                final int colCount = theResSet.getCols();

                if (itemType.equals("Documents")) // Documents search
                {
                    for (int i = 0; i < m_searchResult.getResponse().nRows; i++)
                    {
                        Vector<String> vRowData = new Vector<String>();

                        vRowData.add(rowData.elementAt(colCount * i)); // PUID
                        vRowData.add(rowData.elementAt(colCount * i + 1)); // ITEM
                                                                           // ID
                        vRowData.add(rowData.elementAt(colCount * i + 2)); // ITEM
                                                                           // NAME
                        vRowData.add(""); // PART SUB TYPE
                        vRowData.add(rowData.elementAt(colCount * i + 3)); // LIFECYCLE

                        m_searchResultTable.addNode(vRowData);
                    }
                }
                else
                // Engineering and Non-Engineering search
                {
                    for (int i = 0; i < m_searchResult.getResponse().nRows; i++)
                    {
                        Vector<String> vRowData = new Vector<String>();

                        vRowData.add(rowData.elementAt(colCount * i)); // PUID
                        vRowData.add(rowData.elementAt(colCount * i + 1)); // ITEM
                                                                           // ID
                        vRowData.add(rowData.elementAt(colCount * i + 2)); // ITEM
                                                                           // NAME
                        vRowData.add(rowData.elementAt(colCount * i + 4)); // PART
                                                                           // SUB
                                                                           // TYPE
                        vRowData.add(rowData.elementAt(colCount * i + 3)); // LIFECYCLE

                        m_searchResultTable.addNode(vRowData);
                    }
                }

                m_searchStatusLbl.setForeground(Color.BLACK);
                m_searchStatusLbl.setText(m_searchResult.getResponse().nRows + " Item(s) found.");
            }
            else
            {
                m_searchStatusLbl.setForeground(Color.RED);
                m_searchStatusLbl.setText("No Item(s) found. Change your search criteria");
            }

            m_searchStatusLbl.setVisible(true);
            m_searchResultTable.updateUI();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        return;
    }

    @SuppressWarnings("serial")
    private static class CustomTreeCellRenderer extends DefaultTreeCellRenderer
    {
        ImageIcon m_rendererIcon;

        public void setRendererIcon(ImageIcon myIcon)
        {
            this.m_rendererIcon = myIcon;
        };

        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                boolean leaf, int row, boolean hasFocus)
        {

            Component ret = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

            JLabel label = (JLabel) ret;

            label.setIcon(m_rendererIcon);

            return ret;
        }
    }

    public QuickSearchService.QuickBindVariable[] getBindVariables()
    {
        // Prepare input fields - substitute * for empty values
        m_sItemId = m_itemIDtxt.getText();
        if (m_sItemId == null || m_sItemId.isEmpty())
        {
            m_sItemId = "*";
        }

        m_sItemName = m_itemNametxt.getText();
        if (m_sItemName == null || m_sItemName.isEmpty())
        {
            m_sItemName = "*";
        }

        m_sItemType = (String) m_itemTypeCmb.getSelectedItem();
        if (m_sItemType == null || m_sItemType.isEmpty() || m_sItemType.equalsIgnoreCase("Engineering"))
        {
            m_sItemType = "Nov4Part";
        }

        m_sPartSubType = (String) m_partSubTypetxt.getText();
        if (m_sPartSubType == null || m_sPartSubType.isEmpty())
        {
            m_sPartSubType = "*";
        }

        m_sLifeCycle = (String) m_lifeCycleCmb.getSelectedItem();
        if (m_sLifeCycle == null || m_sLifeCycle.isEmpty())
        {
            m_sLifeCycle = "*";
        }

        String strGroupPUID = Utils.getCurrentGroupPUID();
        ;

        // Construct Bind Variables
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[6];

        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[] { m_sItemId };

        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[] { m_sItemName };

        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[] { m_sItemType };

        bindVars[3] = new QuickSearchService.QuickBindVariable();
        bindVars[3].nVarType = POM_string;
        bindVars[3].nVarSize = 1;
        bindVars[3].strList = new String[] { m_sPartSubType };

        bindVars[4] = new QuickSearchService.QuickBindVariable();
        bindVars[4].nVarType = POM_string;
        bindVars[4].nVarSize = 1;
        bindVars[4].strList = new String[] { m_sLifeCycle };

        bindVars[5] = new QuickSearchService.QuickBindVariable();
        bindVars[5].nVarType = POM_typed_reference;
        bindVars[5].nVarSize = 1;
        bindVars[5].strList = new String[] { strGroupPUID };

        return bindVars;
    }

    public QuickSearchService.QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];

        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-DHItems-for-add-target";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2 }; // object_name
                                                                    // , item_id
        allHandlerInfo[0].listInsertAtIndex = new int[] { 2, 1 }; // Item Id,
                                                                  // Item Name

        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-release-status";
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 2 }; // Lifecycle
        allHandlerInfo[1].listInsertAtIndex = new int[] { 3 }; // Lifecycle

        if (!m_itemTypeCmb.getSelectedItem().toString().equals("Documents"))
        {
            QuickSearchService.QuickHandlerInfo[] handlerInfo = new QuickSearchService.QuickHandlerInfo[3];
            handlerInfo[0] = allHandlerInfo[0];
            handlerInfo[1] = allHandlerInfo[1];

            handlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
            handlerInfo[2].handlerName = "NOVSRCH-get-DH-master-props";
            handlerInfo[2].listBindIndex = new int[] { 3 }; // Item Type
            handlerInfo[2].listReqdColumnIndex = new int[] { 1 }; // PartSubType
            handlerInfo[2].listInsertAtIndex = new int[] { 4 }; // PartSubType

            allHandlerInfo = handlerInfo;
        }

        return allHandlerInfo;
    }

    public String getBusinessObject()
    {
        return m_sItemType;
    }
    //7804-start
   
    public Map<String, String> getItemWithRevTypeMap()
    {
    	return m_itemSelPanel.targetTable.getItemWithRevTypeMap();
    }
    //7804-end
	private void addItemsToTargetECO(Vector<TCComponent> selectedComps) //KLOC - 131
	{
	    try
	    {
	        TCComponent selComp = null;
	         //TCDECREL-4398 : Start
	        Map<String, String> mapECOProperty = new HashMap<String, String>();
	        TCComponent[] validatedItems = null;
	        TCComponent[] selComponent = selectedComps.toArray(new TCComponent[selectedComps.size()]);
	        String changeType = m_itemSelPanel.crSelectpanel.typeofChange;
	        mapECOProperty.put("changetype", changeType);
	        Vector<TCComponent> vectSelCom = new Vector<TCComponent>();
	        
	        vectSelCom = validateExistingItems(selComponent, true);
	        
	        if(vectSelCom.size()<=0)
	        	return;
	        
	        TCComponent[] caNotExistSelectedComps = vectSelCom.toArray(new TCComponent[vectSelCom.size()]);
                        
	        NOVCreateECOHelper helperObj = new NOVCreateECOHelper();
	        //7804-revType should be in General ECO only-added if-else condition
	        Vector<TCComponent> vectSelComAfterValidate = new Vector<TCComponent>();
	        if(changeType.equalsIgnoreCase("General"))
	        {
	            TCComponent[] validComps = validateCompForMinorRev(caNotExistSelectedComps); //8334
	        	validatedItems = helperObj.validateTargetsToECO(validComps, mapECOProperty,(String) m_mmrCmb.getSelectedItem());//7804-added one more input argument to this function call
	        	vectSelComAfterValidate = validatePartFamilyItems(validatedItems);
	        }
	        else
	        {
	        	validatedItems = helperObj.validateTargetsToECO(caNotExistSelectedComps, mapECOProperty,null);	
	        	vectSelComAfterValidate = validateExistingItems(validatedItems, false);
	        }
	        
	        if(vectSelComAfterValidate.size()<=0)
	        	return;
	        
	        validatedItems = vectSelComAfterValidate.toArray(new TCComponent[vectSelComAfterValidate.size()]);
	        //setItemWithRevType(validatedItems);//7804
	         //TCDECREL-4398 : End
	         //*********************
	        for(int inx=0; inx<validatedItems.length; inx++)
	        {
	            m_sValidationMSG = "";
	            //bValid = true;
	            //selComp = selectedComps.get(inx);
	            selComp = validatedItems[inx];

                AIFComponentContext[] compConxt =  getChangeRequestContextObjects(((TCComponentItemRevision)selComp).getRelated("RelatedECN"));
                if(compConxt == null || compConxt.length == 0)
                {
                	if(changeType.equalsIgnoreCase("Lifecycle"))
                	{
                		m_itemSelPanel.targetTable.addTarget((TCComponentItemRevision)selComp , null);
                	}
                   
                    //7804-added below function call and added below function call for general eco
                	else
                	{
                		 m_itemSelPanel.targetTable.addTargetsToItemsTable((TCComponentItemRevision)selComp , null,(String) m_mmrCmb.getSelectedItem());
                	}
                   
                }
                else
                {
                    validateECR(compConxt);
                    if(m_ecrVector!=null && m_ecrVector.size()>0)
                    {
                        m_itemRevECRMap.put(selComp, m_ecrVector);
                    }
                    else
                    {
                        m_itemSelPanel.targetTable.addTarget((TCComponentItemRevision)selComp , null);
                    }
                }
                /*if(bValid)
                {
                    m_sValidationMSG = "The Item "+ ((TCComponentItemRevision)selComp).getProperty("item_id")+ " is added to targets.";
                }
                else
                {
                    m_sValidationMSG = "RelatedDefiningDocument relation contains other than Documents.";
                }
	            
	            if(!bValid)
	            {
	                bIsAllItemsAdded = false;
	            }*/
	        }
	        if(m_itemRevECRMap.values().size()>0)
	        {
	            NOVECRSelectionDialogWorker launchECRSelectDlg = new NOVECRSelectionDialogWorker();
	            launchECRSelectDlg.execute();
	        }
//	        else
//	        {
//	            displayMessage(bIsAllItemsAdded,"");
//	        }
	        else if(helperObj.isAllValidateSuccess())
	        {
	        	displayMessage(true,"");
	        }
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
	}
	private Vector<TCComponent> validateExistingItems(TCComponent[] selComponent, boolean bThrowMessage) 
	{
		String sErrorIds = "Following Items already added.";
        boolean bIsItemExists = false;

        Vector<TCComponent> vectSelCom = new Vector<TCComponent>();

        for (int i = 0; i < selComponent.length; i++)
        {
	        try {
				if(! m_itemSelPanel.targetTable.strTargetItemIds.contains(selComponent[i].getProperty("item_id")) )
				{
					vectSelCom.add(selComponent[i]);
				}
				else
				{
					bIsItemExists = true;
					sErrorIds = sErrorIds + "\n" + selComponent[i].toString();
				}
			} catch (TCException e) {
				e.printStackTrace();
			}
		}
        if(bIsItemExists && bThrowMessage)
        	com.nov.rac.utilities.utils.MessageBox.post(ItemSearchDialog.this,"Item already added",
        			sErrorIds,
        			com.nov.rac.utilities.utils.MessageBox.INFORMATION);
        return vectSelCom;
	}
	
	private TCComponent[] validateCompForMinorRev(TCComponent[] selComponent) throws TCException //8334
    {
        Vector<TCComponent> validComp = new Vector<TCComponent>();
        Vector<TCComponent> invalidComp = new Vector<TCComponent>();
        String sRevType = getSelectedRevType();
        for(int i=0; i<selComponent.length;i++)
        {
            TCComponentItemRevision itemRev =  ((TCComponentItem)selComponent[i]).getLatestItemRevision();
            if(NOVECOHelper.isValidCompForMinorRev(itemRev, sRevType))
            {
                validComp.add(selComponent[i]);
            }
            else
            {
                invalidComp.add(selComponent[i]);
            }
        }
        
        if(invalidComp.size()>0)
        {
            StringBuilder msgStr = new StringBuilder();
            msgStr.append("Below selected item(s) may have latest working revision. Cannot add as minor revision.\n");
            for(int k=0;k<invalidComp.size();k++)
            {
                msgStr.append(invalidComp.get(k));
                msgStr.append("\n");
            }
            JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Added Item", JOptionPane.WARNING_MESSAGE);
        }
        TCComponent[] comp = validComp.toArray(new TCComponent[validComp.size()]);
        return comp;
    }
	
	private Vector<TCComponent> validatePartFamilyItems(TCComponent[] selComponent) throws TCException // KLOV -130
    {
        String sErrorIds = "Cannot add the target(s) as a major revision, because the following part family Member(s) already added as a minor revision";
        boolean bIsItemExists = false;
        Vector<TCComponent> vecComp = new Vector<TCComponent>();
        Vector<TCComponent> vectSelCom = new Vector<TCComponent>();
        
        for (int i = 0; i < selComponent.length; i++)
        {
            if (!m_itemSelPanel.targetTable.strTargetItemIds.contains(selComponent[i].getProperty("item_id")))
            {
                vectSelCom.add(selComponent[i]);
            }
            else
            {
                bIsItemExists = true;
                sErrorIds = sErrorIds + "\n" + selComponent[i].toString();
                Vector<TCComponent> partFamily = NOVECOHelper.getAllPartFamilyMembers(selComponent[i]);
                if (!vecComp.contains(partFamily))
                    vecComp.addAll(partFamily);
                
            }
        }
        if (bIsItemExists)
        {
            com.nov.rac.utilities.utils.MessageBox.post(ItemSearchDialog.this, "Item already added", sErrorIds,
                    com.nov.rac.utilities.utils.MessageBox.INFORMATION);
            vectSelCom.removeAll(vecComp);
            
        }
        return vectSelCom;
    }

	private TCComponent getPartFromSelectedDoc(TCComponent selcomp)
	{
	    //If user has selected only Document revision then the respective part revision is selected.
	    //If no part revision is defined then that revision is added as target alone
	    try
	    {
	        if (selcomp.getType().equalsIgnoreCase("Documents Revision"))
	        {
	            //@Harshada - Code added for looking RDD part revisions
	            TCComponentItem searchedItem = ((TCComponentItemRevision)selcomp).getItem();
	            String[] types = { "Nov4Part Revision", "Purchased Part Revision",  "Non-Engineering Revision" };
	            String[] relations = { "RelatedDefiningDocument" };
	            AIFComponentContext[] relatedPartRevs = searchedItem.whereReferencedByTypeRelation(types, relations);

	            for (int i = 0; i < relatedPartRevs.length; i++)
	            {
	                if (relatedPartRevs[i].getComponent() instanceof TCComponentItemRevision)
	                {
	                    TCComponentItem relatePart = ((TCComponentItemRevision)relatedPartRevs[i].getComponent()).getItem();
	                    //Swamy
	                    TCComponentItem RDDItem = NOVECOHelper.getRDDItem(relatePart.getLatestItemRevision());
	                    if(RDDItem != null )
	                    {
	                        //Swamy
	                        String revId = selcomp.getProperty("current_revision_id");
	                        TCComponentItemRevision partRev = NOVECOHelper.getItemRevision(relatePart,revId);

	                        selcomp = partRev;
	                        /*if (partRev==null)
	                         {
	                            return false;
	                        }
	                        else
	                        {
	                            //Re Assigning selected document revision with part revision
	                            selcomp = partRev;
	                        }*/
	                    }
	                    else
	                    {
	                        System.out.println("Only Doc Item is required");
	                        // If Latest Reivsion doesn't have this Document as RDD, then no need to pull the Part Item only
	                        // Document Item needs to be added
	                        selcomp=selcomp;
	                    }
	                }
	            }
	        }
	    }
	    catch (Exception e1)
	    {
	        e1.printStackTrace();
	    }
	    return selcomp;
	}
	private boolean validateTargetItem(TCComponent selcomp)
	{
	    try
	    {
	        if (selcomp instanceof TCComponentItem )
            {
                TCComponentItemRevision[] workingRevs = ((TCComponentItem)selcomp).getWorkingItemRevisions();
                if (workingRevs.length>1)
                {
                    m_sValidationMSG = "The Item "+ ((TCComponentItem)selcomp).getProperty("object_string")+ " contains many working revisions.\n"+
                                       "Please delete the revision(s) not required and then continue the process.";
                    return false;
                }
            }
	        short checkedOutStatus = NOVECOHelper.isItemRevSecObjectCheckedout((TCComponentItemRevision)selcomp);

	        if (checkedOutStatus == NOVECOConstants.Item_CheckedOut)
	        {
	            m_sValidationMSG = "The Item "+ ((TCComponentItemRevision)selcomp).getProperty("item_id")+ " is Checked-Out.\nCannot be added to Engineering Change.";
	            return false;
	        }
	        else if (checkedOutStatus == NOVECOConstants.ItemRev_CheckedOut)
	        {
	            m_sValidationMSG = "The Revision "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is Checked-Out.\nCannot be added to Engineering Change.";
	            return false;
	        }
	        else if (checkedOutStatus == NOVECOConstants.SecObjects_CheckedOut)
	        {
	            m_sValidationMSG = "Dataset under revision "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is Checked-Out.\nCannot be added to Engineering Change.";
	            return false;
	        }
	        else if (checkedOutStatus == NOVECOConstants.RDD_DOCRev_CheckedOut)
	        {
	            m_sValidationMSG = "The related defining document revision under "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is Checked-Out.\nCannot be added to Engineering Change.";
	            return false;
	        }
	        else if (checkedOutStatus == NOVECOConstants.RDD_DOCRev_SecObjects_CheckedOut)
	        {
	            m_sValidationMSG = "Dataset of related defining document revision under "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is Checked-Out.\nCannot be added to Engineering Change.";
	            return false;
	        }
	        else if(NOVECOHelper.isItemRevInProcess((TCComponentItemRevision)selcomp))
	        {
	            m_sValidationMSG = "The Revision "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is already in process.";
	            return false;
	        }

	        TCComponent[] releaseStats = ((TCComponentItemRevision)selcomp).getTCProperty("release_status_list").getReferenceValueArray();
	        if ((m_targetItemSelPanel!=null && m_targetItemSelPanel.ecoFormPanel.typeofChange.equals("Lifecycle"))
	                &&releaseStats.length==0 )
	        {
	            m_sValidationMSG = "The Revision "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is not released.";
	            return false;
	        }
	        else if(m_itemSelPanel!=null)
	        {
	            if(m_itemSelPanel.crSelectpanel.typeofChange.equals("Lifecycle")&& releaseStats.length==0)
	            {
	                m_sValidationMSG = "The Revision "+ ((TCComponentItemRevision)selcomp).getProperty("object_string")+ " is not released.";
	                return false;
	            }
	            if (m_itemSelPanel.targetTable.strTargetItemIds.contains(((TCComponentItemRevision)selcomp).getProperty("item_id")))
	            {
	                m_sValidationMSG = "The Item "+ ((TCComponentItemRevision)selcomp).getProperty("item_id")+ " is already added to targets.";
	                return false;
	            }
	        }
	    }
	    catch (Exception e1)
	    {
	        e1.printStackTrace();
	    }
	    return true;
	}
	//7804-start
	public String getSelectedRevType()
	{
	    String revType = "";
	    if(m_mmrCmb != null)
	    {
	        revType = m_mmrCmb.getSelectedItem().toString();
	    }
		return revType;
	}
	//7804-end
	//7804-added argument mmrType to the function below
	private void addItemsToTargetSOA(JPanel panel, TCComponent[] selComps) throws TCException
    {
		TCComponentProcess currentProcess = NOVECOHelper.getCurrentProcess((TCComponentForm)m_targetItemSelPanel.ecoFormPanel.ecoForm);
		if(currentProcess != null)
		{
			Vector<TCComponent> selCompVect = new Vector<TCComponent>();
			String sErrorMsg = null;
			boolean isValidRevAdded = false;
			String sRevType = getSelectedRevType();
            Vector<TCComponent> invalidComp = new Vector<TCComponent>();
			for(int i=0, l=selComps.length; i<l; i++)
			{
				TCComponentItemRevision selcompItemRev = ((TCComponentItem)selComps[i]).getLatestItemRevision();
				if(!NOVECOHelper.isValidCompForMinorRev(selcompItemRev, sRevType))//8334
                {
                    invalidComp.add(selComps[i]);
                    continue;
                }
				AIFComponentContext[] compConxt =  getChangeRequestContextObjects(selcompItemRev.getRelated("RelatedECN"));

				if (compConxt != null && compConxt.length>0)
				{
					validateECR(compConxt);
					if(m_ecrVector!=null && m_ecrVector.size()>0)
					{
						m_itemRevECRMap.put(selcompItemRev,m_ecrVector);
					}
					else
					{
						selCompVect.add(selComps[i]);
					}
				}
				else
				{
					selCompVect.add(selComps[i]);
				}
			}
			if(selCompVect!=null && selCompVect.size()>0)
			{
				Vector<TCComponent> selECRs = new Vector<TCComponent>();
				if(sChangeType.equalsIgnoreCase("Lifecycle"))
				{
					sErrorMsg = NOVECOHelper.addTargetsToECO("NOVECOTargetItemsPanel", m_targetItemSelPanel.ecoFormPanel.ecoForm,
							selCompVect.toArray(new TCComponent[selCompVect.size()]),
							selECRs.toArray(new TCComponent[selECRs.size()]));
				}
				//7804-added below function call only for General ECO
				else
				{
					
					sErrorMsg = NOVECOHelper.addTargetsandCreateRevision("NOVECOTargetItemsPanel", m_targetItemSelPanel.ecoFormPanel.ecoForm,
							selCompVect.toArray(new TCComponent[selCompVect.size()]),
							selECRs.toArray(new TCComponent[selECRs.size()]),(String) m_mmrCmb.getSelectedItem());
					//7804-end
				}
				
				
				//7804-end
				if (sErrorMsg != null && sErrorMsg.trim().length()>0)
				{
					isValidRevAdded = false;
				}
				else
				{
					isValidRevAdded = true;
					m_targetItemSelPanel.ecoFormPanel.ecoForm.save();
				}
			}
			if(m_itemRevECRMap.values().size()>0)
			{
				NOVECRSelectionDialogWorker ecrSelectionDlg = new NOVECRSelectionDialogWorker();
				ecrSelectionDlg.execute();
			}
			else
			{
				/* Refresh Target list in case some items were added and rest errored out */
			    if(invalidComp.size() > 0)//8334
                {
                    StringBuilder msgStr = new StringBuilder();
                    msgStr.append("Below selected item(s) may have latest working revision. Cannot add as minor revision.\n");
                    for(int k=0;k<invalidComp.size();k++)
                    {
                        msgStr.append(invalidComp.get(k));
                        msgStr.append("\n");
                    }
                    JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item", JOptionPane.WARNING_MESSAGE);
                }
                else if (isValidRevAdded)
				{
					String msgStr = "Selected Items are added to targets.";
					JOptionPane.showMessageDialog(ItemSearchDialog.this, msgStr, "Add Item", JOptionPane.WARNING_MESSAGE);
				}
				else
				{
					JOptionPane.showMessageDialog(ItemSearchDialog.this, sErrorMsg, "Add Item", JOptionPane.WARNING_MESSAGE);
				}
				m_targetItemSelPanel.ecoFormPanel.ecoForm.refresh();
			}
		}
		else
		{
			String sMsg = m_targetItemSelPanel.ecoFormPanel.ecoForm.toString() +" "+ m_registry.getString("InvalidECO.MSG");
			JOptionPane.showMessageDialog(ItemSearchDialog.this, sMsg, "Add Item", JOptionPane.WARNING_MESSAGE);
		}
    }
	public void displayMessage(boolean bSuccess,String sErrorMsg)
    {
        if(bSuccess == false)
        {
            if(sErrorMsg != null && sErrorMsg.trim().length()>0)
            {
            	m_sValidationMSG = sErrorMsg;
            }
            else
            {
            	m_sValidationMSG = "Some Items could not be added to target.";
            }
        }
        else
        {
            if(m_selectionMode != ListSelectionModel.SINGLE_SELECTION || m_selectedCount != 1)
            {
                m_sValidationMSG = "Selected Item(s) are added to target.";
            }
        }
        if(this.isVisible())
        {
        	SwingUtilities.invokeLater(new Runnable() 
        	{				
				@Override
				public void run() 
				{
					com.nov.rac.utilities.utils.MessageBox.post(ItemSearchDialog.this,"Add Item",m_sValidationMSG,com.nov.rac.utilities.utils.MessageBox.INFORMATION);
				}
			});
        }
        else
        {

        	final Display display = Display.getDefault();
        	display.asyncExec(new Runnable()
        	{
				@Override
				public void run()
				{
					Shell shell = display.getActiveShell();
					MessageBox.post(shell, m_sValidationMSG,"Add Item", MessageBox.INFORMATION);
				}
			});

        }
    }
	class NOVECRSelectionDialogWorker extends SwingWorker<Integer, Integer>
    {
        private int m_iRetCode = -1;
        private int m_iNoOfRows = 0;
        private NOVECRTable m_ecrSelectionTable = null;
        private NOVECRSelectionDialog m_ecrSelectionDlg = null;
        private boolean m_bAllItemsAdded = true;
        private String m_sChangeType = "";
        private Vector<TCComponent> m_addedRevs = new Vector<TCComponent>();
        private Vector<TCComponent> m_addedECRs = new Vector<TCComponent>();
        private Vector<TCComponent> m_crForms = new Vector<TCComponent>();
        public NOVECRSelectionDialogWorker()
        {
            if (m_targetItemSelPanel != null)
            {
                m_sChangeType = m_targetItemSelPanel.ecoFormPanel.typeofChange;
                ListModel model = m_targetItemSelPanel.ecoFormPanel.targetCRsPanel.crsList.getModel();
                int iSize = model.getSize();
                for (int i = 0; i < iSize; i++)
                {
                	m_crForms.add((TCComponent) model.getElementAt(i));
                }
            }
            else if(m_itemSelPanel != null)
            {
                m_sChangeType = m_itemSelPanel.crSelectpanel.typeofChange;
            }

        }
        protected Integer doInBackground() throws Exception
        {
            // Do a time-consuming task.
        	ItemSearchDialog.this.dispose();
            final Display display = Display.getDefault();
            display.syncExec(new Runnable()
            {
                public void run()
                {
                    if(m_itemRevECRMap.size() > 0)
                    {
                    	Shell shell = null;
                    	if(m_itemSelPanel != null && ItemSearchDialog.this.isDisplayable())
                    	{
                    		Canvas canvas = new Canvas();
                    		ItemSearchDialog.this.add(canvas);
                    		ItemSearchDialog.this.addNotify();

                    		shell = SWT_AWT.new_Shell(display, canvas);
                    		shell.setLayout(new GridLayout());
                    	}
                    	else if(m_targetItemSelPanel != null && m_targetItemSelPanel.ecoFormPanel != null)
                    	{
                    		shell = display.getActiveShell();
                    		if(shell == null && m_targetItemSelPanel.ecoFormPanel.isDisplayable())
                    		{
                    			Canvas canvas = new Canvas();
                    			m_targetItemSelPanel.ecoFormPanel.add(canvas);
                    			m_targetItemSelPanel.ecoFormPanel.addNotify();

                    			shell = SWT_AWT.new_Shell(display, canvas);
                    			shell.setLayout(new GridLayout());
                    		}

                    	}

                        m_ecrSelectionDlg = new NOVECRSelectionDialog(shell);
                        m_ecrSelectionDlg.create();

                        NOVECOHelper.centerToScreen(m_ecrSelectionDlg.getShell());

                        m_ecrSelectionDlg.populateECRTable(m_sChangeType,m_itemRevECRMap);

                        m_ecrSelectionTable = m_ecrSelectionDlg.getECRSelectionTable();
                        m_iNoOfRows = m_ecrSelectionTable.getRowCount();

                        m_iRetCode = m_ecrSelectionDlg.open();
                    }
                }
            });
            return m_iRetCode;
        }
        protected void done()
        {
        	try 
        	{
				if(get() == Dialog.OK)
				{
					openDialog(false);
					
					SwingUtilities.invokeLater(new Runnable() 
					{					
						@Override
						public void run() 
						{
							processSelectedItems();
						}
					});
				}
				else
				{
					openDialog(true);
				}
			} 
        	catch (InterruptedException e) 
        	{
				e.printStackTrace();
			} 
        	catch (ExecutionException e) 
        	{
				e.printStackTrace();
			}
        }
        private void processSelectedItems()
        {
        	try
        	{
        		getSelectedItems();

        		if(m_targetItemSelPanel != null)
        		{
        		    String sErrorMsg;
        		    //7700:only for General
        		   if(sChangeType.equalsIgnoreCase("General"))
        		   {
        		       sErrorMsg = NOVECOHelper.addTargetsandCreateRevision("NOVECOTargetItemsPanel", m_targetItemSelPanel.ecoFormPanel.ecoForm,
                               m_addedRevs.toArray(new TCComponent[m_addedRevs.size()]),
                               m_addedECRs.toArray(new TCComponent[m_addedECRs.size()]),(String) m_mmrCmb.getSelectedItem());
        		   }
        		   else
        		   {
        		       sErrorMsg = NOVECOHelper.addTargetsToECO("NOVECOTargetItemsPanel", m_targetItemSelPanel.ecoFormPanel.ecoForm,
                       m_addedRevs.toArray(new TCComponent[m_addedRevs.size()]),
                       m_addedECRs.toArray(new TCComponent[m_addedECRs.size()]));
        		   }
        		    
        			

        			if(sErrorMsg!=null && sErrorMsg.trim().length()>0)
        			{
        				m_bAllItemsAdded = false;
        			}
        			else
        			{
        				m_bAllItemsAdded = true;
        			}
        			/* Refresh Target list in case some items were added and rest errored out */
        			m_targetItemSelPanel.ecoFormPanel.ecoForm.refresh();

        			displayMessage(m_bAllItemsAdded,sErrorMsg);
        		}
        		else
        		{
        			displayMessage(m_bAllItemsAdded,"");
        		}
        		m_addedRevs.clear();
        		m_addedECRs.clear();
        	}
        	catch (Exception e)
        	{
        		e.printStackTrace();
        	}
        }
        private void getSelectedItems()
        {
        	for(int indx=0;indx<m_iNoOfRows;indx++)
    		{
    			boolean bChkBoxValue = (Boolean)m_ecrSelectionTable.getValueAt(indx, 0);
    			if(bChkBoxValue)
    			{
    				AIFTableLine tableLine = m_ecrSelectionTable.getRowLine(indx);
    				NOVECRTableLine ecrTableLine = (NOVECRTableLine)tableLine;
    				TCComponent formObject = ecrTableLine.getTableLineFormObject();
    				TCComponent revObject = ecrTableLine.getTableLineRevObject();

    				if(m_itemSelPanel != null)
    				{
    					addRevAndECRToTarget((TCComponentItemRevision) revObject,(TCComponentForm) formObject);
    				}
    				else if(m_targetItemSelPanel != null)
    				{
    					if(!m_addedRevs.contains(revObject))
    					{
    						m_addedRevs.add(revObject);
    					}
    					if(!m_crForms.contains(formObject) && !m_addedECRs.contains(formObject))
						{
    						m_addedECRs.add(formObject);
						}
    				}
    				m_itemRevECRMap.remove(revObject);
    			}
    		}
    		TCComponent[] revComps = m_itemRevECRMap.keySet().toArray(new TCComponent[m_itemRevECRMap.keySet().size()]);
    		int iNoOfRevs = revComps.length;
    		for(int i=0;i<iNoOfRevs;i++)
    		{
    			if(m_itemSelPanel != null)
				{
    				addRevAndECRToTarget((TCComponentItemRevision) revComps[i],null);
				}
				else if(m_targetItemSelPanel != null)
				{
					if(!m_addedRevs.contains(revComps[i]))
					{
						m_addedRevs.add(revComps[i]);
					}
				}
    		}
        }
        private void addRevAndECRToTarget(TCComponentItemRevision revObject,TCComponentForm formObject)
        {
        	//7784-added new function call for general eco
        	boolean retval;
        	if(sChangeType.equalsIgnoreCase("General"))
        	{
        		retval = m_itemSelPanel.targetTable.addTargetsToItemsTable(revObject,formObject,(String) m_mmrCmb.getSelectedItem());
        	}
        	else
        	{
        		retval = m_itemSelPanel.targetTable.addTarget(revObject,formObject);
        	}
        			
        	
			m_itemSelPanel.notifyObserver();
			if(retval == false)
			{
				m_bAllItemsAdded = false;
			}
        }
        private void openDialog(boolean bIsModel)
        {
        	if(ItemSearchDialog.this.isVisible() == false)
			{
				ItemSearchDialog.this.toFront();
				ItemSearchDialog.this.setModal(bIsModel);
				ItemSearchDialog.this.pack();
				ItemSearchDialog.this.setVisible(true);
			}
        }
    }
}
