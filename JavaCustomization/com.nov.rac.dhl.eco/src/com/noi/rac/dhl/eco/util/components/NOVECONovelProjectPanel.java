package com.noi.rac.dhl.eco.util.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.form.compound.util.INOVCustomFormProperties;
import com.noi.util.components.NOIJLabel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVECONovelProjectPanel extends JPanel
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Registry m_registry;
    _EngChangeObjectForm_Panel m_ECOFormPanel;
    INOVCustomFormProperties m_formProps;
    private JButton m_editBtn;
    public JList m_novelPrjctList;
    private JScrollPane m_novelPrjctPane;
    
    private Vector<TCComponentForm> m_listedFormVec;
    private Map<String,TCComponentForm> m_formMap = new HashMap<String, TCComponentForm>();
    
    
    public NOVECONovelProjectPanel(_EngChangeObjectForm_Panel engChangeObjectFormPanel, INOVCustomFormProperties formProps)
    {
        m_ECOFormPanel = engChangeObjectFormPanel;
        m_formProps = formProps;
        m_registry = Registry.getRegistry(this);
        initUI();
        loadNovelPrjcts();
    }

    private void initUI()
    {  
        this.setLayout(new PropertyLayout(0, 2, 0, 10, 0, 0));
        
        NOIJLabel npLbl = new NOIJLabel(m_registry.getString("novelProject.Lbl"));
        m_editBtn = new JButton(m_registry.getString("edit.Lbl"));
        ImageIcon plusIcon = m_registry.getImageIcon("edit.ICON");
        m_editBtn.setIcon(plusIcon);
        m_editBtn.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent aevt) 
            {
                NOVNovelProjectSearchDialog dlg = new NOVNovelProjectSearchDialog(NOVECONovelProjectPanel.this);
                dlg.setVisible(true);
            }
        });
        
        DefaultListModel listModel = new DefaultListModel();
        m_novelPrjctList = new JList(listModel);
        m_novelPrjctPane = new JScrollPane(m_novelPrjctList);
        m_novelPrjctPane.setPreferredSize(new Dimension(500,100));
        m_novelPrjctList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        m_novelPrjctList.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvt) 
            {
                openBrowser(mouseEvt);
            }
        });
        
        JPanel npBtnLblPanel = new JPanel(new PropertyLayout(5, 0, 0, 0, 0, 5));
        npBtnLblPanel.add("1.1.left.center",npLbl);
        npBtnLblPanel.add("1.2.right.center",m_editBtn);
        npBtnLblPanel.setPreferredSize(new Dimension(300,25));

        this.add("1.1.left.center",npBtnLblPanel);
        this.add("2.1.left.center",m_novelPrjctPane);
    }
    
    private void loadNovelPrjcts()
    {
        try 
        {
            if (m_ECOFormPanel!=null) 
            {
                AIFComponentContext[] novelFormRefs = m_ECOFormPanel.ecoForm.getSecondary();
                disaplyNovelProject(novelFormRefs);
            }
        } 
        catch (TCException e) 
        {
            e.printStackTrace();
        }
    }
    
    private void disaplyNovelProject(AIFComponentContext[] novelFormRefs)//KlockWork -260
    {
    	DefaultListModel listModel = (DefaultListModel)m_novelPrjctList.getModel();
        m_listedFormVec = new Vector<TCComponentForm>();

        for (int j = 0; j < novelFormRefs.length; j++) 
        {
            if (novelFormRefs[j].getContext().equals("Nov4_DHL_EC_NovelProject")) 
            {
                InterfaceAIFComponent comp =  novelFormRefs[j].getComponent();
                try 
                {
	                if (comp instanceof TCComponentForm) 
	                {
	                    m_listedFormVec.add((TCComponentForm)comp);
	                    TCComponentForm form  = (TCComponentForm)comp;
	                    String key = form.getTCProperty("nov4_project_number").toString()+" "+form.getTCProperty("nov4_project_name").toString();
						
	                    m_formMap.put(key,form);
	                    listModel.addElement(key);
	                }
                } catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
    }
    
    

	public void enableNovelProjectPanel(boolean isEnabled)
    {
        m_editBtn.setEnabled(isEnabled);
    }
    
    private void openBrowser(MouseEvent mouseEvt)
    {
        Object source = mouseEvt.getSource();
        if (SwingUtilities.isRightMouseButton(mouseEvt))
        {
              JPopupMenu jPopupMenu = new JPopupMenu();
              JMenuItem jMenuOpen = new JMenuItem();
              String sMenuLabel = "<html><b><font=8 color=red>Open</font></b>";
              jMenuOpen.setText(sMenuLabel);
              jMenuOpen.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) 
                    {
                    	String selectedComponent=(String) m_novelPrjctList.getSelectedValue();
                          if(selectedComponent != null)
                          {
                           //http://srvhoudhtlb01/dhtweb/Stage3/Summary.aspx?projid=sgProjectID
                              try
                              {
                            	  TCComponentForm formComp = m_formMap.get(selectedComponent);
                                  String projectID = formComp.getTCProperty("nov4_project_id").toString();
                                  //String stageID = formComp.getTCProperty("nov4_current_stage_id").toString();
                                  StringBuffer prjctIDURL = new StringBuffer(); 
                                  prjctIDURL.append(m_registry.getString("novelProjectID.URL"));
                                  prjctIDURL.append("=");
                                  prjctIDURL.append(projectID);
                                  Runtime.getRuntime().exec("cmd /c start "+ prjctIDURL);
                              }
                              catch ( Exception ex )
                              {
                                  ex.printStackTrace();
                              } 
                          }
                    }
              });
              jPopupMenu.add(jMenuOpen);
              jPopupMenu.setVisible(true);
              jPopupMenu.show((Component) source,mouseEvt.getX(), mouseEvt.getY());
        }
    }
    
    public Vector<TCComponentForm> getNovelPrjctForms()
    {
        return m_listedFormVec;
    }
    
}
