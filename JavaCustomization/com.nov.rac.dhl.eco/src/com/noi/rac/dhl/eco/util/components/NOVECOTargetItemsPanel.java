package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.masschange.dialogs.MassChangeDialog;
import com.noi.util.components.NOIJLabel;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVECOTargetItemsPanel extends JPanel implements IObserver2
{
	private static final long serialVersionUID = 1L;
	Registry registry = Registry.getRegistry(this);
	public NOVECOTargetItemsTable targetTable;
	public NOVECODispositionPanel dispositionPanel;
	public Map<String, TCComponent> dispMap ;
	public _EngChangeObjectForm_Panel ecoFormPanel;
	private NOVECOSetStatusDialog statusDialog ;
	
	final JButton validateBtn;
	final JButton editTrgBtn;
	final JButton massBOMBtn;
	final JButton viewReportBtn;
	final JButton setStatusBtn;
	
	private JScrollPane trgPane;
	private JPanel editpLeft;
	private JPanel editpRight;
	private JPanel trgValPanel;
	
	public NOVECOTargetItemsPanel(_EngChangeObjectForm_Panel engChangeObjectFormPanel) 
	{
		ecoFormPanel = engChangeObjectFormPanel;
		dispositionPanel = engChangeObjectFormPanel.dispPanel;
		dispMap = new HashMap<String, TCComponent>();
		NOIJLabel targetsLbl = new NOIJLabel("Targets:");
		validateBtn = new JButton("Validate");
		setStatusBtn = new JButton("Set Status");
		
		if(NOVECOHelper.isEcoReleased(ecoFormPanel.ecoForm))
		{
			validateBtn.setEnabled(false);
			setStatusBtn.setEnabled(false);
		}
		validateBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				TCUserService userService = ecoFormPanel.ecoForm.getSession().getUserService();
				Object[] inputObjs = new Object[2];
				inputObjs[0] = ecoFormPanel.ecoForm;
				Vector<TCComponent> targets = ecoFormPanel.getECOTargets();
				//targets.add(ecoFormPanel.ecoForm);
				inputObjs[1] = targets.toArray(new TCComponent[targets.size()]);
				try 
				{
					/* saving new life cycle attribute before validate report is displayed*/
					ecoFormPanel.saveNewLifecycle();
					Object retObj = userService.call("NOV_ECO_Validate",inputObjs );
					NOVECOTargetsValidationDialog dlg = new NOVECOTargetsValidationDialog(ecoFormPanel.ecoForm,targets,retObj);
					int x = validateBtn.getLocationOnScreen().x;
	                int y = validateBtn.getLocationOnScreen().y;
	                dlg.setLocation(x, y);
	                dlg.pack();
					dlg.setVisible(true);					
				} 
				catch (TCException e1) 
				{
					e1.printStackTrace();
				}				
			}
		});
		
		viewReportBtn = new JButton("View Report");
		viewReportBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				NOVECODetailsReport dlg = new NOVECODetailsReport(ecoFormPanel);
				int x = viewReportBtn.getLocationOnScreen().x;
				int y = viewReportBtn.getLocationOnScreen().y;
				dlg.setLocation(x, y);
				dlg.pack();
				dlg.setVisible(true);				
			}
		});
		
//		viewReportBtn.setEnabled(false);
//		viewReportBtn.setVisible(false);

		/*start-TCDECREL-2564-Function to set lifecycle status of all targets on a lifecycle ECO*/	
		setStatusBtn.addActionListener(new ActionListener() 
		{			
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
				 String grpName = ecoFormPanel.ecoForm.getProperty("owning_group").toString().substring(0, 2);
				 statusDialog = new NOVECOSetStatusDialog(grpName);
				 statusDialog.registerObserver(NOVECOTargetItemsPanel.this);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				int x = setStatusBtn.getLocationOnScreen().x-280;
                int y = setStatusBtn.getLocationOnScreen().y+25;
                statusDialog.setLocation(x, y);
                statusDialog.pack();
                statusDialog.setVisible(true);				
			}
		});
		/*End-TCDECREL-2564-Function to set lifecycle status of all targets on a lifecycle ECO*/
		
		editTrgBtn = new JButton("Add Item");
		ImageIcon plusIcon = registry.getImageIcon("edit.ICON");
		editTrgBtn.setIcon(plusIcon);
		
		massBOMBtn = new JButton("Mass Change");//5733
		/* 26.04.2012:Rakesh - Mass BOM EDit should be enabled only for General ECO */
		//if ( ! ecoFormPanel.typeofChange.equalsIgnoreCase("Lifecycle"))
		//4474-commented out to enable the button for both life cycle and General porcess
		/*if ( ! ecoFormPanel.typeofChange.equalsIgnoreCase("General"))
			massBOMBtn.setEnabled(false);

		if(ecoFormPanel.typeofChange.equalsIgnoreCase("Lifecycle"))//4352-added to enable the MassBomEdit  button for Lifecyle type of change
		{
			massBOMBtn.setEnabled(true);//4352
		}*/
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		String[] columnNames = {"Item Number","Name",/*"Type",*/"Old Rev","New Rev", "Old Lifecycle","New Lifecycle"};
		targetTable = new NOVECOTargetItemsTable(ecoFormPanel,tcSession, columnNames,false);
		trgPane = new JScrollPane(targetTable);
		trgPane.setPreferredSize(new Dimension(662, 150));		
		trgPane.getViewport().setBackground(Color.white);
		
		editpLeft = new JPanel(new FlowLayout(FlowLayout.LEFT));
		editpLeft.add(targetsLbl);
		editpLeft.add(validateBtn);
		editpLeft.add(viewReportBtn);
		//editpLeft.add(setStatusBtn);
		
		editpRight = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		editpRight.add(massBOMBtn);
		editpRight.add(editTrgBtn);
		editpRight.add(setStatusBtn);//aligned the button to right in ecopanel
		
		trgValPanel = new JPanel(new BorderLayout());
		trgValPanel.setPreferredSize(new Dimension(662,28));	
		trgValPanel.add(editpLeft, BorderLayout.LINE_START);
		trgValPanel.add(editpRight, BorderLayout.LINE_END);
				
		editTrgBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				ItemSearchDialog ISD = null;
				try {
					ISD = new ItemSearchDialog(NOVECOTargetItemsPanel.this);
				} catch (TCException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/*int x = editTrgBtn.getLocationOnScreen().x-280;
                int y = editTrgBtn.getLocationOnScreen().y+25;
                ISD.setLocation(x, y);*/
				ISD.setVisible(true);	
			}
		});
		
		massBOMBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				//MassBOMChangeDialog MBC = new MassBOMChangeDialog(NOVECOTargetItemsPanel.this);
			    MassChangeDialog MBC = new MassChangeDialog(ecoFormPanel);//5733
			    int x = massBOMBtn.getLocationOnScreen().x-280;
                int y = massBOMBtn.getLocationOnScreen().y+25;
                MBC.setLocation(x, y);
                MBC.pack();
				MBC.setVisible(true);
			    /*Display.getDefault().asyncExec(new Runnable()  //5733
	            {
	               public void run()
	               {
	                   MassChangeComposite massChDlg = new MassChangeComposite(AIFUtility.getCurrentApplication().getDesktop().getShell(), NOVECOTargetItemsPanel.this);
	                   massChDlg.open();
	               }
	            });*/
			}
		});
		//TCDECREL-2798 : Start : Akhilesh
		/*this.setLayout(new PropertyLayout());
		add("1.1.left.center",trgValPanel);
		add("2.1.left.center",trgPane);*/
		this.setLayout(new BorderLayout());
		add(trgValPanel,BorderLayout.NORTH);
		add(trgPane,BorderLayout.CENTER);
		//TCDECREL-2798 : End
		setBorder(new TitledBorder(""));
	}
	
	public void resizePanel(Dimension dPanel, Dimension dTgtPane, Dimension dTgtValPane)
	{
		if(dPanel != null)
			setPreferredSize(dPanel);
		
		if(dTgtPane != null)
			trgPane.setPreferredSize(dTgtPane);
				
		if(dTgtValPane != null)
			trgValPanel.setPreferredSize(dTgtValPane);	
	}
	
	public void setEnabled(boolean enabled) 
	{
		editTrgBtn.setEnabled(enabled);
		/*DHL-202 enable validate button irrespective whether the form is chekedin/out*/
		//validateBtn.setEnabled(enabled);
		targetTable.setEnabled(enabled);
		setStatusBtn.setEnabled(enabled);//TCDECREL-2564 disable when eco form is checkedin
	}
	
	public void loadTargetItems( TCComponent[] targetItemRevs,TCComponent[] dispComps )
	{
		buildDispMap( dispComps );

		String[] reqdProperties = { "item_id" };
		String[][] retriviedProperties = null;
		try
		{
		    List<TCComponent> targetItemRevList = Arrays.asList(targetItemRevs);//TC10.1 Upgrade
			//retriviedProperties = TCComponentType.getPropertiesSet( targetItemRevs, reqdProperties );
			retriviedProperties = TCComponentType.getPropertiesSet( targetItemRevList, reqdProperties );//TC10.1 Upgrade
		}
		catch( TCException e1 )
		{
			e1.printStackTrace();
		}		
		
		for( int index = 0; index < targetItemRevs.length; index++ ) 
		{
			if (dispMap.containsKey(retriviedProperties[index][0])) 
			{
				targetTable.addTarget( targetItemRevs[index], dispMap.get( retriviedProperties[index][0] ) );	
			}
			else
			{
				targetTable.addTarget( targetItemRevs[index], null );
			}
		}
		
		/*buildDispMap(dispComps);
		for (int i = 0; i < targetItemRevs.length; i++) 
		{
			try 
			{
				targetTable.addTarget(targetItemRevs[i],dispMap.get(targetItemRevs[i].getProperty("item_id")));
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}	
		}*/
	}
	
	public void loadLifeCycleInstances( TCComponent[] lifecycleInstances ) 
	{
		String[] reqdProperties = { "nov4_targetitemid" };
		String[][] retriviedProperties = null;
		try
		{
		    List<TCComponent> lifecycleInstanceList = Arrays.asList(lifecycleInstances);//TC10.1 Upgrade
			//retriviedProperties = TCComponentType.getPropertiesSet( lifecycleInstances, reqdProperties );
			retriviedProperties = TCComponentType.getPropertiesSet( lifecycleInstanceList, reqdProperties );//TC10.1 Upgrade
		}
		catch( TCException e1 )
		{
			e1.printStackTrace();
		}		

		for( int index = 0; index < lifecycleInstances.length; index++ ) 
		{
			targetTable.lifecycleMap.put( retriviedProperties[index][0], lifecycleInstances[index] );
		}
	}
	
	private void buildDispMap( TCComponent[] dispComps )
	{
		String[] reqdProperties = { "targetitemid" };
		String[][] retriviedProperties = null;
		try
		{
		    List<TCComponent> dispCompList = Arrays.asList(dispComps);//TC10.1 Upgrade
			//retriviedProperties = TCComponentType.getPropertiesSet( dispComps, reqdProperties );
			retriviedProperties = TCComponentType.getPropertiesSet( dispCompList, reqdProperties );//TC10.1 Upgrade
		}
		catch( TCException e1 )   
		{
			e1.printStackTrace();
		}				
		
		for( int index = 0;index < dispComps.length; index++ ) 
		{
			try 
			{
				dispComps[index].refresh();
				dispMap.put( retriviedProperties[index][0], dispComps[index] );	
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}						
		}
	}

	public boolean addToTargetList(TCComponentItemRevision selcomp)
	{
		Vector<TCComponent> revs = new Vector<TCComponent>(); 		
		boolean isReleatedObjsAdded = NOVECOHelper.getRddRevsofDocument(revs, selcomp);
		if (isReleatedObjsAdded) 
		{
			revs.add(selcomp) ;
			
			TCComponentProcess currentProcess = ecoFormPanel.getProcess();
			if (currentProcess!=null) 
			{
				try 
				{
					Vector<TCComponent> targetCompTobeAdded = new Vector<TCComponent>();
					TCComponent[] temptargetComp = revs.toArray(new TCComponent[revs.size()]);
					//If same dataset is referenced in two revisions we need to filter for maintaining one object, so skip adding duplicate 					
					TCComponent[] targets = currentProcess.getRootTask().getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
					Vector<TCComponent> ecoTargetsVec = new Vector<TCComponent>(Arrays.asList(targets));
					for (int i = 0; i < temptargetComp.length; i++) 
					{
						if (!ecoTargetsVec.contains(temptargetComp[i])) 
						{
							targetCompTobeAdded.add(temptargetComp[i]);
						}
					}
					
					TCComponent[] targetComp = targetCompTobeAdded.toArray(new TCComponent[targetCompTobeAdded.size()]);
					int attachtypes[]= new int[targetComp.length];
					for (int i = 0; i < attachtypes.length; i++) 
					{
						attachtypes[i]= TCAttachmentType.TARGET;	
					}
					currentProcess.getRootTask().refresh();
					currentProcess.getRootTask().addAttachments(TCAttachmentScope.GLOBAL, targetComp, attachtypes);
					AIFComponentContext[] cxtObjs =  currentProcess.getRootTask().getChildren();
					for (int i = 0; i < cxtObjs.length; i++) 
					{
						if (cxtObjs[i].getContext().equals("pseudo_folder")) 
						{
							((TCComponent)cxtObjs[i].getComponent()).refresh();	
						}
					}
					currentProcess.getRootTask().refresh();
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}	
			}		
		}
		else
		{
			MessageBox.post("RelatedDefiningDocument relation contains other than Documents.", "Cannot add item", MessageBox.WARNING);
		}
		
		return isReleatedObjsAdded;
	}
		
	public TCComponentItemRevision getItemRevisionofRevID(TCComponentItem rddItem, String RevId)
	{
		TCComponentItemRevision itemRev=null;
		try
		{
			AIFComponentContext[] revItems=rddItem.getChildren("revision_list");
			for(int k=0;k<revItems.length;k++)
			{
				String revId=((TCComponentItemRevision)revItems[k].getComponent()).getProperty("current_revision_id");
				if(revId.compareTo(RevId)==0)
				{
					itemRev=(TCComponentItemRevision)revItems[k].getComponent();
					break;
				}
			}			
		}catch(TCException e)
		{
			System.out.println(e);
		}
		return itemRev;
	}
	
	public void enableButtons(boolean val)
	{
		massBOMBtn.setEnabled(massBOMBtn.isEnabled() && val);
	}
	
	@Override
	public void update(ISubject2 obj, Object arg) 
	{
		if(arg!=null && arg instanceof String[])
		{
			String [] dialogData = (String[])arg;
			
			if(dialogData.length>0 && dialogData !=null)
			{
				targetTable.setStatusToTargets(dialogData);
			}
			
			
		}
		
	}
}
