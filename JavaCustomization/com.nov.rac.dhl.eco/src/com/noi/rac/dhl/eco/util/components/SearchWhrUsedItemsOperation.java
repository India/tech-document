package com.noi.rac.dhl.eco.util.components;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentRevisionRule;
import com.teamcenter.rac.kernel.TCComponentRevisionRuleType;

public class SearchWhrUsedItemsOperation implements IRunnableWithProgress 
{
	private		String 						m_confRevRule;
	private		String 						m_defRevRule;
	private		TCComponentItemRevision		m_itemRev;
	private		TCComponent[]				m_whereUsedItems;
	private 	MassBOMChangeDialog 		m_mbc;
	
	final 		int							m_taskSleepTime = 50;
	
	public SearchWhrUsedItemsOperation(MassBOMChangeDialog mbc)
	{
		m_mbc = mbc;
		m_itemRev = m_mbc.getWhrUsedItemRev();
		m_confRevRule = m_mbc.getConfRevRule();
		m_defRevRule = m_mbc.getDefRevRule();
	}
	
	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException 
	{
		monitor.beginTask("Executing Where Used Search", IProgressMonitor.UNKNOWN);
						
		final IProgressMonitor finalMonitor = monitor;
		Runnable theRunnable = new Runnable() 
		{			
			@Override
			public void run()
			{			
				try 
				{
					TCComponentRevisionRuleType revRuleType = (TCComponentRevisionRuleType) m_itemRev.getSession().getTypeComponent("RevisionRule");
					TCComponent[] revRules = revRuleType.extent();
					TCComponentRevisionRule revRule = null;
					TCComponentRevisionRule defaultRevRule = null;
					for (int i = 0; i < revRules.length; i++) 
					{
						if(revRules[i].getProperty("object_name").equalsIgnoreCase(m_confRevRule))
						{
							revRule =  new TCComponentRevisionRule();
							revRule = (TCComponentRevisionRule) revRules[i];
						}
						else if(revRules[i].getProperty("object_name").equalsIgnoreCase(m_defRevRule))
						{
							defaultRevRule =  new TCComponentRevisionRule();
							defaultRevRule = (TCComponentRevisionRule) revRules[i];
						}
					}
					if (revRule == null) 
					{
						revRule = defaultRevRule;
					}
					
					m_whereUsedItems = m_itemRev.whereUsed(TCComponentItemRevision.WHERE_USED_CONFIGURED,revRule);
					m_mbc.setWhereUsedResult(m_whereUsedItems);
					m_mbc.populateWhereUsedTable();
					
					System.out.println("Searching Where Used items Operation complete");
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
					m_whereUsedItems = null;
					finalMonitor.setCanceled(true);
					finalMonitor.done();
				}			
			} 
		};
		
		new Thread(theRunnable).start();
		
		while(!monitor.isCanceled() && m_whereUsedItems == null)
		{				
		}
		
		monitor.done();		
		
		Thread.sleep(m_taskSleepTime);
		
		if ( !monitor.isCanceled() )
	    {
			//Update status to client			
	    }
	    else
	    {
	    	//User clicked Cancelled - Scenario not defined 
	    }
	}
}
