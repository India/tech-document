package com.noi.rac.dhl.eco.util.components;

public interface IObserver 
{
	public void update();	

}
