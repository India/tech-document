package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.util.components.NOVLOVPopupButton;
import com.nov.object.policies.NOVDataManagementServiceHelper;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
//import com.teamcenter.rac.aif.ApplicationDef;//TC10.1 Upgrade
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.kernel.TCClassificationService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentBOMViewRevision;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;

public class NOVECODispositionTabPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private TCComponent targetItemRev;
	public iTextArea txtChangeDesc;
	public NOVLOVPopupButton m_dispositionFieldlov=new NOVLOVPopupButton();//4474
	//commented out for the ticket 4474
	/*public NOVLOVPopupButton inProcesslov = new NOVLOVPopupButton();
	public NOVLOVPopupButton inInventorylov = new NOVLOVPopupButton();
	public NOVLOVPopupButton assembledlov = new NOVLOVPopupButton();
	public NOVLOVPopupButton inFieldlov = new NOVLOVPopupButton();*/
	private JPanel classifPanel;
	String className="";
	public Registry registry = Registry.getRegistry(this);	

	public JTextArea txtDispInstrux;
	public TCComponent dispcomp;
	NOVENDispositionPanelData paneldata;
	_EngChangeObjectForm_Panel ecoPanel = null;
	NOVECOTargetsPanel ecoTargetsPanel = null;
	private boolean isClassified = false;
	private TCSession session;
	boolean hasBOM=false;
	public JButton compareBOMBtn;
	public JButton viewBOMBtn;
	public JButton refreshBOMBtn;
	public JButton classifyBtn;
	public JButton viewClassificationBtn;
	public JButton copyDispBtn;
	public TCClassificationService classificationService;
	private JLabel lblClassified;
	private JLabel lblIntendedBOM;
	private Boolean	m_isPreviewMode = false;	
	private TCComponent[]	m_arrDispComp;
	public JButton m_saveDispBtn;//5084
	
	/*public NOVECODispositionTabPanel(_EngChangeObjectForm_Panel argENPanel,NOVECOTargetsPanel ecoTargetsPanel,
			TCComponent dispcomp, NOVENDispositionPanelData panelData, boolean previewMode )
	{
		this( argENPanel, argENPanel.getSession(), dispcomp,  panelData );
		isPreviewMode = previewMode;
		this.ecoTargetsPanel = ecoTargetsPanel;
	}*/
	
	public NOVECODispositionTabPanel(_EngChangeObjectForm_Panel argENPanel,TCSession session,
			TCComponent dispcomp, NOVENDispositionPanelData panelData )
	{
		this.dispcomp=dispcomp;
		this.session=session;
		classificationService = session.getClassificationService();
		this.paneldata = panelData;
		ecoPanel = argENPanel;
	}
	
	public void setPreviewMode(boolean previewMode)
	{
		m_isPreviewMode = previewMode;
	}
	
	public void setDispositionComps(TCComponent[]	dispComp)
	{
		m_arrDispComp = dispComp;
	}
	
	public void createUI()
	{
		//create the top panel based on newRelease
		JPanel mainPanel = new JPanel();
		JPanel middlePanel =new JPanel();
		JPanel changePanel =new JPanel();
		JPanel bomPanel = new JPanel();
		JPanel classificationPanel = new JPanel(new PropertyLayout());		
		
		/* ------------ Change Description panel starts here --------- */
		javax.swing.JLabel lblChageDesc = new JLabel();
		lblChageDesc.setText("Change Description");
		lblChageDesc.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		
		txtChangeDesc = new iTextArea(30,30);
		JScrollPane scChangeDesc = new JScrollPane(txtChangeDesc);
		scChangeDesc.setPreferredSize(new Dimension(550,100));
		
		changePanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));	
		
		changePanel.add("1.1.left.top",lblChageDesc);
		changePanel.add("2.1.left.top",scChangeDesc);
		/* -------------- Change Description panel ends here --------- */
		
		
		/* ----------------- Middle panel starts here ---------------- */
		JPanel disposition = new JPanel(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
		JLabel lblDisposition = new JLabel("Disposition");
		lblDisposition.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		//4474-start
		JLabel m_lblDispositionField=new JLabel("Disposition");
		m_lblDispositionField.setFont(new Font("Dialog",java.awt.Font.BOLD,12));//4474-end
		//commented out for the ticket 4474
		/**JLabel lblInProcess = new JLabel("In Process");
		lblInProcess.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		JLabel lblInventry = new JLabel("In Inventory");
		lblInventry.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		JLabel lblAssembled = new JLabel("Assembled");
		lblAssembled.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		JLabel lblInField = new JLabel("In Field");
		lblInField.setFont(new Font("Dialog",java.awt.Font.BOLD,12));*/		
		
		//disposition.add("1.1.left.top", lblDisposition);
		disposition.add("1.1.left.top", m_lblDispositionField);//444
		disposition.add("1.2.left.top",m_dispositionFieldlov );//4474
		//commented out for the ticket 4474
		/**disposition.add("1.1.left.top", lblInProcess);
		disposition.add("1.2.left.top", inProcesslov);
		disposition.add("3.1.left.top", lblInventry);
		disposition.add("3.2.left.top", inInventorylov);
		disposition.add("4.1.left.top", lblAssembled);
		disposition.add("4.2.left.top", assembledlov);
		disposition.add("5.1.left.top", lblInField);
		disposition.add("5.2.left.top", inFieldlov);*/

		JPanel dispInstructions = new JPanel();
		dispInstructions.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));
		JLabel lblDispInstrux = new JLabel("Disposition Instruction");
		lblDispInstrux.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		txtDispInstrux = new JTextArea(30,30);
		JScrollPane scDispInstrux = new JScrollPane(txtDispInstrux);
		scDispInstrux.setPreferredSize(new Dimension(378,110));
		dispInstructions.add("1.1.left.top",lblDispInstrux);
		dispInstructions.add("2.1.left.top",scDispInstrux);
		middlePanel.add(disposition);
		middlePanel.add(dispInstructions);		
		/* ----------------- Middle panel ends here ------------------ */
		
		/* ---------------- Copy Disposition starts here ------------- */
		JPanel copyDispPanel = new JPanel();
		//5084-starts
		
		//m_saveDispBtn = new JButton("Save Disposition");
		m_saveDispBtn = new JButton("Save Disposition");
        m_saveDispBtn.setEnabled(false);
        invokeListeenerOfSaveDispBtn();
        copyDispPanel.add(m_saveDispBtn);
        //5084-end
		copyDispBtn = new JButton("Copy Disposition to all parts");
		copyDispBtn.setEnabled(false);
		copyDispBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				saveData();
				copyDispositionData();
			}
		});
		copyDispPanel.add(copyDispBtn);
		

		/* ---------------- Copy Disposition ends here --------------- */
		
				
		/* -------- Classification panel creation starts here -------- */
		lblClassified = new JLabel("Classification Details");
		lblClassified.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		
		viewClassificationBtn = new JButton("View Classification");
		viewClassificationBtn.setPreferredSize(new Dimension(120, 23));
		viewClassificationBtn.setEnabled(false);
		viewClassificationBtn.addActionListener(new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				NOVECODispositionTabPanel.this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));		
				NOVViewClassificationDialog classDialog = new NOVViewClassificationDialog(targetItemRev);
				classDialog.setMinimumSize(new Dimension(320, 380));
				NOVECODispositionTabPanel.this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				
				if(classDialog.isClassAttribDefined())
				{
					int x = viewClassificationBtn.getLocationOnScreen().x-classDialog.getWidth();
	                int y = viewClassificationBtn.getLocationOnScreen().y-classDialog.getHeight();
	                classDialog.setLocation(x, y);
	                classDialog.pack();
	                classDialog.setVisible(true);
				}
				else
				{
					JOptionPane.showMessageDialog(NOVECODispositionTabPanel.this, "Classification Attributes are not defined.", "Warning", JOptionPane.WARNING_MESSAGE);
					classDialog.dispose();
					return ;
				}					
			}
		});
		
		classifyBtn = new JButton("Classify");
		classifyBtn.setPreferredSize(new Dimension(120, 23));
		classifyBtn.setEnabled(false);
		classifyBtn.addActionListener(new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				ecoPanel.saveForm();
				/*/ApplicationDef appDef =  AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey("com.teamcenter.rac.classification.icm.ICMApplication");
				appDef.openApplication(new InterfaceAIFComponent[]{targetItemRev});*/
				
				 IOpenService openPerspective = PerspectiveDefHelper.getOpenService("com.teamcenter.rac.classification.icm.ICMApplication");//TC10.1 Upgrade
	             openPerspective.open(new InterfaceAIFComponent[]{targetItemRev});//TC10.1 Upgrade
			}
		});
		
		JPanel classifPanelLeft = new JPanel(new FlowLayout(FlowLayout.LEFT));
		classifPanelLeft.add(lblClassified);
				
		JPanel classifPanelRight = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		classifPanelRight.add(viewClassificationBtn);
		classifPanelRight.add(classifyBtn);
		
		classifPanel = new JPanel(new BorderLayout());
		classifPanel.setPreferredSize(new Dimension(620,30));
		classifPanel.add(classifPanelLeft, BorderLayout.LINE_START);
		classifPanel.add(classifPanelRight, BorderLayout.LINE_END);	
		
		classificationPanel.add("1.1.left.top", classifPanel);
		classificationPanel.setBorder(new TitledBorder("Classification"));
		/* --------- Classification panel creation ends here ---------- */
		
		
		/* ------------ View BOM panel creation starts here ----------- */
		compareBOMBtn =  new JButton("Compare BOM");
		compareBOMBtn.setPreferredSize(new Dimension(120, 23));
		compareBOMBtn.setEnabled(false);
		compareBOMBtn.addActionListener(new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				//Open BOM compare dialog
				NOVViewBOMCompareDialog bcDialog = new NOVViewBOMCompareDialog(targetItemRev);
				int x = compareBOMBtn.getLocationOnScreen().x-bcDialog.getWidth()+210;
                int y = compareBOMBtn.getLocationOnScreen().y-bcDialog.getHeight();
                bcDialog.setLocation(x, y);
                bcDialog.setVisible(true);
			}
		});			
		
		viewBOMBtn =  new JButton("View BOM");
		viewBOMBtn.setPreferredSize(new Dimension(120, 23));
		viewBOMBtn.setEnabled(false);
		viewBOMBtn.addActionListener(new ActionAdapter()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				ecoPanel.saveForm();
				/*ApplicationDef appDef =  AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey("com.teamcenter.rac.pse.PSEApplication");
				appDef.openApplication(new InterfaceAIFComponent[]{targetItemRev});*/
				
				IOpenService openPerspective = PerspectiveDefHelper.getOpenService("com.teamcenter.rac.pse.PSEApplication");//TC10.1 Upgrade
                openPerspective.open(new InterfaceAIFComponent[]{targetItemRev});//TC10.1 Upgrade
			}
		});	
		
		JPanel btnPnl = new JPanel();
		btnPnl.add(compareBOMBtn);
		btnPnl.add(viewBOMBtn);
		
		JPanel bomBtnPanel = new JPanel(new BorderLayout());
		
		lblIntendedBOM = new JLabel("Bill of Materials");
		lblIntendedBOM.setFont(new Font("Dialog",java.awt.Font.BOLD,12));
		bomBtnPanel.setPreferredSize(new Dimension(620,30));
		bomBtnPanel.add(BorderLayout.WEST,lblIntendedBOM);			
		bomBtnPanel.add(BorderLayout.EAST,btnPnl);
		
		//bomPanel.setLayout(new PropertyLayout(5 , 5 , 5 , 5 , 5 , 5));			
		bomPanel.setLayout(new PropertyLayout());
		bomPanel.add("1.1.left.top",bomBtnPanel);
		bomPanel.setBorder(new TitledBorder("Bill of Materials"));
		
		/* ------------ View BOM panel creation ends -here ----------- */
				

		mainPanel.setLayout(new PropertyLayout(5 , 10 , 5 , 5 , 5 , 5));		
		/*mainPanel.add("1.1.left.top", changePanel);
		mainPanel.add("2.1.left.top", middlePanel);
		mainPanel.add("3.1.left.top", classificationPanel);
		mainPanel.add("4.1.left.top", bomPanel);*/
		
		mainPanel.add("1.1.left.top", changePanel);
		mainPanel.add("2.1.left.top", middlePanel);
		
		/* Copy Disposition panel should be added only for Preview mode */
		if(m_isPreviewMode)
		{			
			mainPanel.add("3.1.left.top", copyDispPanel);
			mainPanel.add("4.1.left.top", classificationPanel);
			mainPanel.add("5.1.left.top", bomPanel);
		}
		else
		{
			mainPanel.add("3.1.left.top", classificationPanel);
			mainPanel.add("4.1.left.top", bomPanel);
		}		
		
		JScrollPane mainPSc = new JScrollPane(mainPanel);
		mainPSc.setBorder(new TitledBorder("Disposition Component Preview"));
		this.add(mainPSc);		
	}
	
	//5084-start
	public void invokeListeenerOfSaveDispBtn()
	{
		m_saveDispBtn.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent actionevent) 
            {
                  saveData();
            }
      });
	}
	//5084-end
	public void loadData(TCComponent dispComp, TCComponent targetRev)
	{
		this.dispcomp = dispComp;
		this.targetItemRev = targetRev;
		copyDispBtn.setEnabled(true);
		m_saveDispBtn.setEnabled(true);//5084
		
		TCComponent[] refDispItemArr = { dispComp };
		this.paneldata = new NOVENDispositionPanelData( refDispItemArr );
        
		try 
		{
			isClassified = classificationService.isObjectClassifiable(targetRev);
			hasBOM = hasBOM(targetRev);
		} catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		loadData();
	}
	
	public void loadData()
	{	
		//commented out for the ticket 4474
		//loadGeneralData();
		loadDispositionData();
		loadClassificationData();
		loadBOMData();
	}

	public void saveData()
	{
		if(paneldata == null || dispcomp == null)
			return;
		
		try
		{	
			String chnageReason = txtChangeDesc.getText();
			String dispInstruction = txtDispInstrux.getText();
			String dispositionValue = m_dispositionFieldlov.getText();//4474
			//commented out for the ticket 4474
			/*String inProcess = inProcesslov.getText();
			String infield = inFieldlov .getText();
			String inInventory = inInventorylov.getText();
			String assembled = assembledlov.getText();*/
	
			/**if( !( paneldata.changedesc.equalsIgnoreCase( chnageReason ) ) || !( paneldata.dispinstruction.equalsIgnoreCase( dispInstruction ) ) || 
					!( paneldata.inprocess.equalsIgnoreCase( inProcess ) ) || !( paneldata.infield.equalsIgnoreCase( infield ) ) ||
					!( paneldata.ininventory.equalsIgnoreCase( inInventory ) ) || !( paneldata.assembled.equalsIgnoreCase( assembled ) )|| ! (paneldata.m_disposition.equalsIgnoreCase( dispositionValue ) ) )*/
			//4474-commented out the existing condition and added the below condition	
			if( !( paneldata.changedesc.equalsIgnoreCase( chnageReason ) ) || !( paneldata.dispinstruction.equalsIgnoreCase( dispInstruction ) ) || 
					!( paneldata.m_disposition.equalsIgnoreCase( dispositionValue ) )  )
			{	
				System.out.println("Saving Disposition Table data");
				//commented out for the ticket 4474
				/**paneldata.setDispClassData( chnageReason, dispInstruction, inProcess,  infield,
						inInventory, assembled, dispositionValue,this.dispcomp );*/
				//4474
				paneldata.setDispositionClassData( chnageReason, dispInstruction,dispositionValue ,this.dispcomp );
			}
		}
		catch(Exception te)
		{
			System.out.println(te);
		}
	}
	//commented out for the ticket 4474
	
	/*private void loadGeneralData()
	{
		txtChangeDesc.setText( paneldata.changedesc );
		txtDispInstrux.setText( paneldata.dispinstruction );

		inProcesslov.setLOVComponentByName( session,NOVECOConstants.ECNAvailability_LOV );	
		
		String curInProcess = paneldata.inprocess;
		if( curInProcess == null || curInProcess.trim().length() == 0 )
		{
			curInProcess ="N/A";
			
		}
		inProcesslov.setText( curInProcess );
        
		
	inFieldlov.setLOVComponentByName( session,NOVECOConstants.ECNAvailability_LOV );			
		String curInField = paneldata.infield;
		if( curInField == null || curInField.trim().length() == 0 )
		{
			curInField ="N/A";
		}
		inFieldlov.setText( curInField );

		inInventorylov.setLOVComponentByName( session,NOVECOConstants.ECNAvailability_LOV );		
		String curInInventory = paneldata.ininventory;
		if( curInInventory == null || curInInventory.trim().length() == 0 )
		{
			curInInventory ="N/A";
		}
		inInventorylov.setText( curInInventory );

		assembledlov.setLOVComponentByName( session,NOVECOConstants.ECNAvailability_LOV );			
		String curpurchased = paneldata.assembled;
		if( curpurchased == null || curpurchased .trim().length() == 0 )
		{
			curpurchased ="N/A";
		}
		assembledlov.setText( curpurchased );
	}*/
	//4474-start
	private void loadDispositionData()
	{
		txtChangeDesc.setText( paneldata.changedesc );
		txtDispInstrux.setText( paneldata.dispinstruction );
		m_dispositionFieldlov.setLOVComponentByName( session,NOVECOConstants.ECODISPOSITION_LOV );
		String curDisposition=paneldata.m_disposition;
		m_dispositionFieldlov.setText( curDisposition );
	}
   //4474-end
	
	private void loadClassificationData()
	{
		if (isClassified) 
		{
			String classificationTxt ="";
			String objectString = "";
			if ((targetItemRev != null) && (targetItemRev instanceof TCComponentItemRevision)) 
			{
				try 
				{
					className = targetItemRev.getProperty("ics_subclass_name");
					objectString = targetItemRev.getProperty("object_string");
					if (targetItemRev.getProperty("ics_subclass_name").toString().length()>0) 
					{
						classificationTxt = objectString + " is classified in " +  className;
						viewClassificationBtn.setEnabled(true);
					}
					else
					{
						classificationTxt = objectString + " is  not classified. ";	
						viewClassificationBtn.setEnabled(false);
					}					
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				classificationTxt = "Classification Details:";	
			}
			
			lblClassified.setText(classificationTxt);	
		}
		else
		{
			String objctName="";;
			try 
			{
				if (targetItemRev!=null) 
				{
					objctName = targetItemRev.getProperty("object_name");	
				}
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
			lblClassified.setText(objctName+" is Not Classified");
			viewClassificationBtn.setEnabled(false);
		}
		if(targetItemRev != null)
			classifyBtn.setEnabled(true);
		else
			classifyBtn.setEnabled(false);
	}
	
	private void loadBOMData()
	{
		if (hasBOM) 
		{
			lblIntendedBOM.setText("BVR available");
			viewBOMBtn.setEnabled(true);
			compareBOMBtn.setEnabled(true);
		}
		else
		{
			lblIntendedBOM.setText("No BVRs available");
			viewBOMBtn.setEnabled(false);
			compareBOMBtn.setEnabled(false);
		}
	}
	
	public boolean hasBOM(TCComponent revcomp)
	{
		AIFComponentContext[] itemRevisionComponents;
		try 
		{
			itemRevisionComponents = ((TCComponentItemRevision)revcomp).getRelated();
			for (int i=0;i<itemRevisionComponents.length;i++)
			{
				if (itemRevisionComponents[i].getComponent() instanceof TCComponentBOMViewRevision) 
				{					
					return true;
				}
			}
		} catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return false;
	}
	
	private String copyDispositionData()
	{		
		final int argSize 			= 2;
		final int indexSrcDispComp	= 0;
		final int indexTgtDispComp	= 1;
		
		String retStr = "";
		TCUserService userService = session.getUserService();
		Object[] args = new Object[argSize];
		
		args[indexSrcDispComp] = dispcomp;
		args[indexTgtDispComp] = m_arrDispComp;//ecoTargetsPanel.refDispItemsArr;		
				

		try 
		{
			/*Object obj = userService.call("NATOIL_copyDisposition", args);
			if (obj instanceof String) 
			{
				retStr = (String)obj;				
			}
			*/
			
			TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
			String[] stringlist = {"changedesc","dispinstruction","nov4_ecodisposition"};
			TCProperty[] tcProperty = dispcomp.getTCProperties(stringlist);
			
	    	NOVDataManagementServiceHelper nOVDataManagementServiceHelper = new NOVDataManagementServiceHelper(tcSession);
	    	
	    	String[] attrList = {"changedesc","dispinstruction","nov4_ecodisposition"};
	    	
	    	
	    	nOVDataManagementServiceHelper.getObjectProperties(dispcomp, attrList, null);		

			CreateInObjectHelper createInObjectHelper = new CreateInObjectHelper(m_arrDispComp[0].getType(), CreateInObjectHelper.OPERATION_UPDATE);
			
			createInObjectHelper.setString("changedesc", dispcomp.getStringProperty("changedesc"));
			createInObjectHelper.setString("dispinstruction", dispcomp.getStringProperty("dispinstruction"));
			createInObjectHelper.setString("nov4_ecodisposition", dispcomp.getStringProperty("nov4_ecodisposition"));
			
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
			
		/* Refresh the objects */
		try
		{
			TCComponentType.refreshThese(m_arrDispComp);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return retStr;
	}
	
	public void clearFields()
	{
		this.dispcomp = null;
		this.targetItemRev = null;		
		
		txtChangeDesc.setText( "" );
		txtDispInstrux.setText( "" );
		//4474-commented out for this ticket
		/*inProcesslov.setText( "" );
		inInventorylov.setText( "" );
		assembledlov.setText( "" );
		inFieldlov.setText( "" );*/
		m_dispositionFieldlov.setText( "" );//4474
		
		lblClassified.setText("Classification Details");
		viewClassificationBtn.setEnabled(false);
		classifyBtn.setEnabled(false);
		
		copyDispBtn.setEnabled(false);
		m_saveDispBtn.setEnabled(false);//5084
		
		lblIntendedBOM.setText("Bill of Materials");
		viewBOMBtn.setEnabled(false);
		compareBOMBtn.setEnabled(false);
	}
	
	/* Disable fields that modify data */
	public void setEditable(boolean val)
	{
		txtChangeDesc.setEnabled(val);
		txtDispInstrux.setEnabled(val);
		//4474-commented out for this ticket
		/*inProcesslov.setEnabled(val);
		inInventorylov.setEnabled(val);
		assembledlov.setEnabled(val);
		inFieldlov.setEnabled(val);*/
		
		classifyBtn.setEnabled(val);
		copyDispBtn.setEnabled(val);
		m_saveDispBtn.setEnabled(val);//5084
		
	}
	//4474-start
	public void setDispositionEditable(boolean value)
	{
		m_dispositionFieldlov.setEnabled(value);
		m_saveDispBtn.setEnabled(value);//5084
		//4907-start
		txtChangeDesc.setEnabled(value);
		txtDispInstrux.setEnabled(value);
		classifyBtn.setEnabled(value);
		compareBOMBtn.setEnabled(value);
		copyDispBtn.setEnabled(value);
		//4907-end
		
	}
	//4474-end
	
	/*public void createBOMPanel(TCComponent revcomp)
	{
		if(revcomp!=null)
		{
			try
			{
				isClassified = classificationService.isObjectClassifiable(revcomp);
				targetItemRev = revcomp;
				AIFComponentContext[] itemRevisionComponents = ((TCComponentItemRevision)revcomp).getRelated();
				for (int i=0;i<itemRevisionComponents.length;i++)
				{
					if (itemRevisionComponents[i].getComponent() instanceof TCComponentBOMViewRevision) 
					{					
						hasBOM=true;
						break;
					}
				}	
				if(hasBOM)
				{
					//get the previous Revision;
					AIFComponentContext[] revItems=((TCComponentItemRevision)revcomp).getItem().getChildren("revision_list");
					TCComponentItemRevision prevrev=null;
					prevrev=getPreiviousRev(revItems, (TCComponentItemRevision)revcomp);
					bomInfoPanel = new NOVDHLBomInfoPanel(prevrev, (TCComponentItemRevision)revcomp);
					//bomInfoPanel.compareBOM(true);
					bomInfoPanel.setVisible(false);
					bomTable=bomInfoPanel.bomInfoTable;					
				}
			}
			catch(TCException tc)
			{
				System.out.println(tc);
			}
		}
	}*/
}





