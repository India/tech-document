/**
 * 
 */
package com.noi.rac.dhl.eco.masschange.factory;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.masschange.operations.AbstractMassRDChangeOperation;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author kabades
 * 
 */

public class MassChangeFactory
{
    private Registry m_registry = null;
    private String m_operationName = null;
    
    /**
     * Get singleton instance of MassChangeFactory Factory.
     * 
     * @return
     */
    
    public static MassChangeFactory newInstance()
    {
        return (new MassChangeFactory());
    }
    
    /**
     * set the Registry, the Registry will be used by Factory for creating UI
     * components.
     * 
     * @param theRegistry
     */
    public void setRegistry(Registry theRegistry)
    {
        m_registry = theRegistry;
    }
    
    /**
     * 
     * @return Current Registry being used by Factory.
     */
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setOperationName(String operationName)
    {
        this.m_operationName = operationName;
    }
    
    private String getOperationName()
    {
        String strOperationName = null;
        // 1.0 Try to get the custom UI for "<actionCommand>.OPERATION"
        if (m_operationName != null)
        {
            // Create "<actionCommand>.OPERATION"
            strOperationName = m_operationName + ".OPERATION";
        }
        return strOperationName;
    }
    
    public AbstractMassRDChangeOperation createOperation(MassRDChangeComposite massRDChangeComposite,
            TCComponent[] selectedComps, String mmrRevType)
    {
        final int ARRAY_LENGTH = 3;
        AbstractMassRDChangeOperation theOperation = null;
        String operationName = getOperationName();// strOperationName+
                                                  // ".OPERATION";
        Object[] params = new Object[ARRAY_LENGTH];
        params[0] = massRDChangeComposite;
        params[1] = selectedComps;
        params[2] = mmrRevType;
        theOperation = (AbstractMassRDChangeOperation) getRegistry().newInstanceFor(operationName, params);
        
        return theOperation;
    }
}
