package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;


public class NOVECOTargetsValidationDialog extends AbstractAIFDialog 
{

	private static final long serialVersionUID = 1L;
	private Vector<TCComponent> wfTargets;
	private Vector<String> clsFailItemIds;
	private Vector<String> altIDFailItemIds;
	private Vector<String> altIdMatchFailItemIds;
	private Vector<String> lifeCylFailItemIds;
	private Map<String,String> cldRelFailItemIds;
	private Map<String,String> qtycldFailItemIds;
	private Map<String,String> findcldFailItemIds;
	
	public NOVECOTargetsValidationDialog(TCComponentForm ecoForm,Vector<TCComponent> targets,Object serCallRet)
	{
		super(AIFUtility.getActiveDesktop());
		wfTargets = targets;		
		cldRelFailItemIds = new HashMap<String, String>();
		qtycldFailItemIds = new HashMap<String, String>();
		findcldFailItemIds = new HashMap<String, String>();
		//Parsing the return string to check the BOM approval, alternate id, classification and life cycle
		parseRetStringArray(serCallRet);
		JEditorPane editorPane = new JEditorPane();
		String ecoform = "";
		try 
		{
			ecoform = ecoForm.getProperty("object_name");
		}
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		final String htmlText = " <FONT SIZE=\"6\" COLOR=#330033><center>"+ecoform+" Change Process Validation Report</center></FONT>"+getValidationReportString();
		editorPane.setContentType("text/html");
		editorPane.setText(htmlText);
		editorPane.setEditable(false);
		JScrollPane scPane = new JScrollPane(editorPane);
		scPane.setPreferredSize(new Dimension(750, 350));
		this.setTitle("Validation Report");
		this.setModal(true);
		//this.centerToScreen();
		
		JPanel panel = new JPanel();
		JButton saveBtn = new JButton("Save Report");
		
		saveBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JFileChooser fileChooser = new JFileChooser();
				int returnVal = fileChooser.showSaveDialog(NOVECOTargetsValidationDialog.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile(); //This grabs the File you typed
					String nameOfFile = "";   
		            nameOfFile = file.getPath();
		            nameOfFile =nameOfFile+".html";
		            try 
					{   
						BufferedWriter out = new BufferedWriter(new FileWriter(nameOfFile));   
						out.write(htmlText);   
						out.close();   
					} 
					catch (IOException ex1) 
					{   
						MessageBox.post(ex1);
					}					
				}
			}
		});
		
		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				NOVECOTargetsValidationDialog.this.dispose();
			}
		});
		panel.add(saveBtn);
		panel.add(cancelBtn);
		getContentPane().add(scPane,BorderLayout.CENTER);		//TCDECREL-2798 : Akhilesh
		getContentPane().add(panel,BorderLayout.SOUTH);			//TCDECREL-2798 : Akhilesh
		setModal(true);
	}
	
	private void parseRetStringArray(Object serCallRet) 
	{
		if (serCallRet instanceof String[]) 
		{
			String[] ckStr = (String[])serCallRet;
		    if (ckStr.length == 7) 
		    {
		    	String[] clsStrArr = ckStr[0].split(",");
		    	clsFailItemIds = new Vector<String>(Arrays.asList(clsStrArr));
		    	String[] altidStrArr = ckStr[1].split(",");
		    	altIDFailItemIds = new Vector<String>(Arrays.asList(altidStrArr));
		    	String[] itemIdStrArr = ckStr[2].split(",");//4761
                altIdMatchFailItemIds = new Vector<String>(Arrays.asList(itemIdStrArr));//4761
		    	String[] lifecycStrArr = ckStr[3].split(",");
		    	lifeCylFailItemIds = new Vector<String>(Arrays.asList(lifecycStrArr));
		    	String[] BOMcldStrArr = ckStr[4].split("~");//4500: fix
		    	for (int i = 0; i < BOMcldStrArr.length; i++) 
		    	{
		    		String[] oneCldck = BOMcldStrArr[i].split(",");
		    		if (oneCldck.length>=2) 
		    		{
		    			//String errorStr = "Failed: Not Approved Item(s) "+oneCldck[1];
		    			String errorStr = "Failed: Child is either Unreleaed Or Obsolete/Cancelled Or ZTBD Subtype Item(s) "+oneCldck[1];
		    			if(oneCldck.length>2)
		    				for(int idx=2; idx<oneCldck.length; idx++)
		    					errorStr = errorStr+" "+oneCldck[idx];
		    			cldRelFailItemIds.put(oneCldck[0],errorStr );
					}
				}
		    	String[] BOMqtyStrArr = ckStr[5].split("~");//4500: fix
		    	for (int i = 0; i < BOMqtyStrArr.length; i++) 
		    	{
		    		String[] oneCldck = BOMqtyStrArr[i].split(",");
		    		if (oneCldck.length>=2) 
		    		{
		    			String errorStr = "Failed: Qty not defined for Item(s) "+oneCldck[1];
		    			if(oneCldck.length>2)
		    				for(int idx=2; idx<oneCldck.length; idx++)
		    					errorStr = errorStr+" "+oneCldck[idx];
		    			qtycldFailItemIds.put(oneCldck[0],errorStr);
		    		}
				}
		    	String[] BOMfindnoStrArr = ckStr[6].split("~");//4500: fix
		    	for (int i = 0; i < BOMfindnoStrArr.length; i++) 
		    	{
		    		String[] oneCldck = BOMfindnoStrArr[i].split(",");
		    		if (oneCldck.length>=2) 
		    		{
		    			String errorStr = "Failed: Find No not defined for Item(s) "+oneCldck[1];
		    			if(oneCldck.length>2)
		    				for(int idx=2; idx<oneCldck.length; idx++)
		    					errorStr = errorStr+" "+oneCldck[idx];
		    			findcldFailItemIds.put(oneCldck[0],errorStr);
		    		}
				}		    	
		    }	
		}		
	}

	private String getValidationReportString()
	{
		String reportStr = "";
		if (wfTargets!=null) 
		{
			for (int i = 0; i < wfTargets.size(); i++) 
			{
				try 
				{
					String targetid = wfTargets.get(i).getProperty("item_id");
					String bomckStr;
					String clsStr ;
					String altidStr ;
					String qtyCkStr ;
					String findNumStr ;
					String lfcyclStr ;
					String itemIdStr;//4761
					String relatedDocvalidation;//7762
					
					if (!wfTargets.get(i).getType().startsWith("Documents Revision")) 
					{
						if(((TCComponentItemRevision)wfTargets.get(i)).getTCProperty("structure_revisions").getReferenceValueArray().length == 0)
						{
							bomckStr ="N/A";
							qtyCkStr ="N/A";
							findNumStr ="N/A";
						}
						else
						{
							bomckStr = cldRelFailItemIds.get(targetid);
							if (bomckStr==null) 
							{
								bomckStr = "Passed";
							}
							
							qtyCkStr = qtycldFailItemIds.get(targetid);
							if (qtyCkStr==null) 
							{
								qtyCkStr = "Passed";
							}
							
							findNumStr = findcldFailItemIds.get(targetid);
							if (findNumStr==null) 
							{
								findNumStr = "Passed";
							}
							else
							{
								findNumStr = ""+findNumStr;
							}	
						}
							
						boolean itemClassiFailed = clsFailItemIds.contains(targetid);
						if (itemClassiFailed) 
						{
							clsStr = "Failed: Not Classified";
						}
						else
						{
							clsStr ="Passed";	
						}
						boolean altIdFailed = altIDFailItemIds.contains(targetid);
						if (altIdFailed) 
						{
							altidStr = "Failed: JDE Context ID not defined";
						}
						else
						{
							altidStr ="Passed";	
						}
						//7762-start
						relatedDocvalidation=NOVECOHelper.checkRDValidation((TCComponentItemRevision) wfTargets.get(i),wfTargets);
						
						//7762-end
						//4761 start
                        boolean itemIdFailed = altIdMatchFailItemIds.contains(targetid);
                        if (itemIdFailed)
                        {
                            itemIdStr = "Failed: Mismatched ItemID and AlternateID ";
                        }
                        else
                        {
                            itemIdStr ="Passed";
                        }
                        //4761 end
                       
                        
					}
					else
					{
						bomckStr ="N/A";
						clsStr ="N/A";
						altidStr ="N/A";
						qtyCkStr ="N/A";
						findNumStr ="N/A";
						itemIdStr = "N/A";//4761
						relatedDocvalidation="N/A";//7762
						
					}
					
					boolean lifeCyclenotFilled = lifeCylFailItemIds.contains(targetid);
					if (lifeCyclenotFilled) 
					{
						lfcyclStr = "Failed: No Lifecycle defined";
					}
					else
					{
						lfcyclStr ="Passed";	
					}
					reportStr = reportStr + "<TR><TD>"+targetid+"</TD>";
					reportStr = reportStr + (( bomckStr.startsWith("Passed") || bomckStr.startsWith("N/A"))?
						 "<TD><FONT COLOR=#347C17>"+bomckStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+bomckStr+"</TD>");
					
					reportStr = reportStr + ( (qtyCkStr.startsWith("Passed") || qtyCkStr.startsWith("N/A"))?
							 "<TD><FONT COLOR=#347C17>"+qtyCkStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+qtyCkStr+"</TD>");
					
					reportStr = reportStr + ( (findNumStr.startsWith("Passed") || findNumStr.startsWith("N/A"))?
							 "<TD><FONT COLOR=#347C17>"+findNumStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+findNumStr+"</TD>");
					
					reportStr = reportStr + ( (clsStr.startsWith("Passed") || clsStr.startsWith("N/A")) ?
							 "<TD><FONT COLOR=#347C17>"+clsStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+clsStr+"</TD>");
					
					reportStr = reportStr + ( (altidStr.startsWith("Passed") || altidStr.startsWith("N/A")) ?
							 "<TD><FONT COLOR=#347C17>"+altidStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+altidStr+"</TD>");
					
					//TCDECREL -4761 START
                    reportStr = reportStr + ( (itemIdStr.startsWith("Passed") || itemIdStr.startsWith("N/A")) ?
                            "<TD><FONT COLOR=#347C17>"+itemIdStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+itemIdStr+"</TD>");
                    //TCDECREL -4761 END
					
					reportStr = reportStr + ( (lfcyclStr.startsWith("Passed") || lfcyclStr.startsWith("N/A"))?
							 "<TD><FONT COLOR=#347C17>"+lfcyclStr+"</TD>":"<TD><FONT COLOR=#FF3300>"+lfcyclStr+"</TD>");
					//7762-start
					reportStr = reportStr + ( (relatedDocvalidation.startsWith("Passed") || relatedDocvalidation.startsWith("N/A"))?
                            "<TD><FONT COLOR=#347C17>"+relatedDocvalidation+"</TD>":"<TD><FONT COLOR=#FF3300>"+relatedDocvalidation+"</TD>");
					//7762-end
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}
			}	
		}	
		//7762-added new Column
	    String str = "<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Targets</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>BOM Child Check</B></TD>"+
	    "<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Quantity Check</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Find No Check</B></TD>"+
		"<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Classification Check</B></TD><TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>Alternate ID Check</B></TD>" +
		"<TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>ItemID AlternateID Mismatch Check</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Lifecycle Check</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Related Document Validation</B></TD></TR>"+reportStr+"</table>";
		return str;
	}
}
