package com.noi.rac.dhl.eco.util.components;
/*
 *@author mishalt
 *Created On: July 2013
 */
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;

import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVNovelProjectSearchDialog extends AbstractAIFDialog implements ComponentListener
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private NOVECONovelProjectPanel m_novelPrjctPanel;
    private NOVNovelProjectSearchPanel m_searchPanel;
    private JList m_novelPrjctList;
    private JButton m_addToECOBtn;
    private JButton m_cancelBtn;
    Vector<String> m_listedForms;
    Vector<TCComponentForm> m_listedFormVec;
    
    public NOVNovelProjectSearchDialog(NOVECONovelProjectPanel novelPrjctPanel)
    {
        super(AIFUtility.getActiveDesktop());
        m_novelPrjctPanel = novelPrjctPanel;
        m_novelPrjctList = novelPrjctPanel.m_novelPrjctList;
        initUI();
    }
    
    private void initUI()
    {
        m_searchPanel = new NOVNovelProjectSearchPanel(m_novelPrjctPanel);
        m_listedFormVec = m_novelPrjctPanel.getNovelPrjctForms();
        Object[] formObjs = new Object[m_novelPrjctList.getModel().getSize()];
        //String[] prjctIDObjs = new String[m_novelPrjctList.getModel().getSize()];
        
        for (int i = 0; i < m_novelPrjctList.getModel().getSize(); i++)
        {
            formObjs[i] = m_novelPrjctList.getModel().getElementAt(i);
            //int inx = ((StringBuffer) formObjs[i]).indexOf(" ");
            //prjctIDObjs[i] = ((StringBuffer) formObjs[i]).substring(0, inx).toString();
        }
        
        if (m_listedFormVec != null && m_listedFormVec.size() > 0)
        {
            m_searchPanel.populateFormOnTable(m_listedFormVec);
        }
        
        //m_listedForms = new Vector<String>(Arrays.asList(prjctIDObjs));
        
        JPanel btnPanel = createButtons();
        
        setLayout(new VerticalLayout(5, 5, 5, 10, 0));
        
        JPanel topPanel = new JPanel(new VerticalLayout());
        topPanel.add("top.bind.center.center", m_searchPanel);
        topPanel.addComponentListener(this);
        add("top.bind.center.center", topPanel);
        add("bottom.bind.center.center", btnPanel);
        setMinimumSize(new Dimension(500, 600));
        setTitle("Novel Project Search");
        pack();
        setResizable(true);
        setModal(true);
        centerToScreen();
    }
    
    private JPanel createButtons()
    {
        JPanel btnPanel = new JPanel();
        
        m_addToECOBtn = new JButton("Update");
        m_addToECOBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent actEvt)
            {
                m_searchPanel.synchronizeNovelFormList(m_listedFormVec);
                NOVNovelProjectSearchDialog.this.dispose();
            }
        });
        
        m_cancelBtn = new JButton("Cancel");
        m_cancelBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                NOVNovelProjectSearchDialog.this.dispose();
            }
        });
        btnPanel.add(m_addToECOBtn);
        btnPanel.add(m_cancelBtn);
        return btnPanel;
    }
    
    @Override
    public void componentResized(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void componentMoved(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void componentShown(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void componentHidden(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        
    }
    
}
