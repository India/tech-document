package com.noi.rac.dhl.eco.util.components;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;


public class NOVViewBOMCompareDialog extends AbstractAIFDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel m_contentPanel = new JPanel();
	private	TCComponentItemRevision m_itemRev;
	private TCTable m_bomTable = new TCTable(); 
	JScrollPane m_sctblIntendedBOM;
	private NOVDHLBomInfoPanel m_bomInfoPanel;
	
	/* Create the dialog */
	public NOVViewBOMCompareDialog(TCComponent targetItemRev) 
	{
		super(AIFUtility.getActiveDesktop(), true);	
		m_itemRev = (TCComponentItemRevision)targetItemRev;
		createUI();
	}
	
	public void createUI()
	{
		createContentPanel();
		
		getContentPane().setLayout(new BorderLayout());		
		getContentPane().add(m_contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) 
					{
						NOVViewBOMCompareDialog.this.dispose();						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		pack();
		setTitle("BOM Compare report for " + m_itemRev.toString());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		final int width = 300;
		final int height = 200;
		setPreferredSize(new Dimension(width, height));
		setResizable(false);		
		setModal(true);
	}
	
	public void createContentPanel()
	{
		createBOMPanel();
		
		m_contentPanel.setLayout(new BoxLayout(m_contentPanel, BoxLayout.Y_AXIS));
		m_contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));	
		m_contentPanel.add(m_sctblIntendedBOM);			
	}
	
	public void createBOMPanel()
	{		
		try
		{
			AIFComponentContext[] revItems=((TCComponentItemRevision)m_itemRev).getItem().getChildren("revision_list");
			TCComponentItemRevision prevrev=null;
			prevrev = getPreviousRev(revItems, (TCComponentItemRevision)m_itemRev);
			m_bomInfoPanel = new NOVDHLBomInfoPanel(prevrev, (TCComponentItemRevision)m_itemRev);
			m_bomInfoPanel.compareBOM(true);
			m_bomTable=m_bomInfoPanel.bomInfoTable;	
			m_bomTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			m_sctblIntendedBOM = new JScrollPane(m_bomTable);
			final int tblWidth = 550;
			final int tblHeight = 250;
			m_sctblIntendedBOM.setPreferredSize(new Dimension(tblWidth,tblHeight));
		} catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TCComponentItemRevision getPreviousRev(AIFComponentContext[] allrevs, TCComponentItemRevision rev)
	{
		TCComponentItemRevision prevRev=null;
		try
		{
			Date curRevDate=rev.getDateProperty("creation_date");
			//Date currentdate=DateFormat.
			prevRev=(TCComponentItemRevision)allrevs[0].getComponent();	
			Date prevDate=prevRev.getDateProperty("creation_date");
			for(int k=1;k<allrevs.length;k++)
			{
				Date compareDate=((TCComponentItemRevision)allrevs[k].getComponent()).getDateProperty("creation_date");
				if((compareDate.before(curRevDate))&&(compareDate.after(prevDate)))
				{
					prevDate=compareDate;
					prevRev=(TCComponentItemRevision)allrevs[k].getComponent();
				}
			}
		}
		catch(TCException tc)
		{
			System.out.println(tc);
		}
		return prevRev;
	}
}
