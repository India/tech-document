package com.noi.rac.dhl.eco.form.compound.panels;

import java.io.IOException;
import java.net.URLEncoder;
import javax.swing.JPanel;

import com.noi.rac.dhl.eco.form.compound.util.INOVCustomFormProperties;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;

/**
 * @author niuy
 * @Date:  May 30, 2007
 * @usage: The class is used to 
 */
public class PrintablePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 protected  INOVCustomFormProperties novFormProperties; 
	 private String encodingType = "UTF-8";

	public PrintablePanel() {
		super();
	}


	/* (non-Javadoc)
	 */
	public void print(String servletParameter) throws TCException, IOException {

        TCPreferenceService prefServ = novFormProperties.getForm().getSession().getPreferenceService();
        String server = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGRSServices_server" );
        String port = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGRSServices_port" );
        String context = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGRSServices_context" );
        String formName = novFormProperties.getForm().getTCProperty("object_name").getStringValue();
        String encFormName = URLEncoder.encode(formName,encodingType);
        String cmd = "cmd /c start http://"+server+":"+port+context+servletParameter+encFormName;
        //test url is localhost:8080
        //cmd = "cmd /c start http://10.146.10.161:8080/cetgrs/"+servletParameter+encFormName;
        Runtime.getRuntime().exec(cmd);
     

	}
	public void alcprint(String uri,String ecnname,String db) throws TCException, IOException {
		
        TCPreferenceService prefServ = novFormProperties.getForm().getSession().getPreferenceService();
        String server = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGALCServices_server" );
        String port = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGALCServices_port" );
        String context = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGALCServices_context" );
        //String encFormName = URLEncoder.encode(ecnname,encodingType);
        //String endDbName = URLEncoder.encode(db,encodingType);
        //String cmd = "cmd /c start http://"+server+":"+port+context+uri+"?ecn="+encFormName+"&db="+db;
        //test url is localhost:8080
        //cmd = "cmd /c start http://10.146.10.161:8080/cetgrs/"+servletParameter+encFormName;
        String cmd = "http://"+server+":"+port+context+uri+"?ecn="+ecnname+"&db="+db;
        Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+cmd);
     

	}
	public void getENhelp() throws TCException, IOException {

        TCPreferenceService prefServ = novFormProperties.getForm().getSession().getPreferenceService();
        String server = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGWSServices_server" );
        String port = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGWSServices_port" );
        String context = prefServ.getString( TCPreferenceService.TC_preference_all, "CETGWSServices_context" );
        String cmd = "cmd /c start http://"+server+":"+port+context+"D321000057-MAN-001.pdf";
        //test url is localhost:8080
        //cmd = "cmd /c start http://10.146.10.161:8080/cetgrs/"+servletParameter+encFormName;
        Runtime.getRuntime().exec(cmd);
     

	}

}
