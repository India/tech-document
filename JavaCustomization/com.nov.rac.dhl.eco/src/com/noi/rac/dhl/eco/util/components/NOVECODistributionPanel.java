package com.noi.rac.dhl.eco.util.components;

/*
 * @Author: Mishalt
 */
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.noi.rac.en.util.components.NOIJLabel;
import com.noi.rac.form.compound.component.AddressBookComponent;
import com.noi.util.components.AbstractReferenceLOVList;
import com.nov.rac.utilities.utils.NOVCustomTextField;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVECODistributionPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Dimension m_listSize;
	private NOVCustomTextField m_emailAddressField;
	private JButton m_emailIDSearchButton;
	private JButton m_globalAddressBtn;
	private JButton m_removeTarget;
	private JList m_sourceList;
	private JScrollPane m_sourceListScroll;
	private AbstractReferenceLOVList m_sourceData = null;
	private DefaultListModel m_sourceListModel;

	public NOVECODistributionPanel() {
		super();
		m_sourceList = new JList();
		m_sourceListScroll = new JScrollPane(m_sourceList);
	}

	public void initUI() {
		Registry reg = Registry.getRegistry(this);
		this.setLayout(new PropertyLayout(0, 2, 0, 10, 0, 0));
		m_sourceList = new JList();

		m_sourceListModel = new DefaultListModel();
		m_sourceList.setModel(m_sourceListModel);
		JScrollPane sourceListScroll = new JScrollPane(m_sourceList);
		sourceListScroll.setPreferredSize(new Dimension(330, 100));
		
		NOIJLabel srcLbl = new NOIJLabel(reg.getString("distribution.NAME"));
		srcLbl.setPreferredSize(new Dimension(200, 25));

		ImageIcon minusIcon = reg.getImageIcon("remove.ICON");
		m_removeTarget = new JButton();
		m_removeTarget.setPreferredSize(new Dimension(25, 25));
		m_removeTarget.setIcon(minusIcon);
		m_removeTarget.addActionListener(this);
				
		JPanel scrollPanel = new JPanel(new PropertyLayout(5, 0, 0, 5, 0, 5));

		scrollPanel.add("1.1.left.center", sourceListScroll);
		scrollPanel.add("1.2.right.right", m_removeTarget);

		m_emailAddressField = new NOVCustomTextField(
				reg.getString("emailAddress.NAME"), 254);
		m_emailAddressField.setColumns(25);
		ImageIcon plusIcon = reg.getImageIcon("add.ICON");
		m_emailIDSearchButton = new JButton(plusIcon);
		m_emailIDSearchButton.setPreferredSize(new Dimension(20, 20));
		m_emailIDSearchButton.addActionListener(this);
		m_globalAddressBtn = new JButton();
		ImageIcon addrIcon = reg.getImageIcon("enaddr.ICON");
		m_globalAddressBtn.setIcon(addrIcon);
		m_globalAddressBtn.addActionListener(this);
		m_globalAddressBtn.setPreferredSize(new Dimension(20, 20));
		JPanel searchPanel = new JPanel(new PropertyLayout(5, 5, 0, 5, 5, 5));

		searchPanel.add("1.1.left.center", m_emailAddressField);
		searchPanel.add("1.2.right.right", m_emailIDSearchButton);
		searchPanel.add("1.3.right.right", m_globalAddressBtn);

		this.add("1.1.left.center", srcLbl);
		this.add("2.1.left.top", scrollPanel);
		this.add("3.1.left.center", searchPanel);

		populateSourceList();
	}

	public void actionPerformed(ActionEvent actEvt) {
		Registry reg = Registry
				.getRegistry("com.noi.rac.en.form.compound.component.component");
		if (actEvt.getSource() == m_globalAddressBtn) {
			AddressBookComponent comp = new AddressBookComponent(AIFUtility
					.getCurrentApplication().getDesktop(), m_sourceData,
					m_sourceList, m_listSize.width);
			comp.searchField.setText(m_emailAddressField.getText());
			comp.setVisible(true);
		}
		if (actEvt.getSource() == m_emailIDSearchButton) {
			emailIdSearchListener(reg);
		}

		if (actEvt.getSource() == m_removeTarget) {
			removeTargetListener();
		}
	}
	
	private void emailIdSearchListener(Registry reg)
	{
		if (isValidEmail(m_emailAddressField.getText())) {
			int objInd = m_sourceListModel.indexOf(m_emailAddressField
					.getText());
			if (objInd >= 0) {
				MessageBox
						.post(reg.getString("hasMailIds.MSG") + " : "
								+ m_emailAddressField.getText(),
								reg.getString("info.TITLE"),
								MessageBox.INFORMATION);
			} else {
				m_sourceListModel.addElement(m_emailAddressField.getText());
				populateSourceList();
			}
		} else {
			MessageBox.post(reg.getString("emailIdFormat.MSG"),
					reg.getString("info.TITLE"), MessageBox.INFORMATION);
		}
	}
	
	private void removeTargetListener()
	{
		Object[] vals = m_sourceList.getSelectedValues();
		for (int i = 0; i < vals.length; i++) {
			String str = (String) vals[i];
			Object c = m_sourceData.getReferenceFromString(str);
			if (c != null) {
				m_sourceData.removeItem(c, str);
				m_sourceListModel.removeElement(str);
				populateSourceList();
			} else {
				for (i = 0; i < vals.length; i++) {
					m_sourceData.removeItem(vals[i], (String) vals[i]);
					m_sourceListModel.removeElement(vals[i]);
				}
				populateSourceList();
			}
			this.repaint();
		}
	}

	public void enableDistributionPanel(boolean isEnabled) {
		m_removeTarget.setEnabled(isEnabled);
		m_emailIDSearchButton.setEnabled(isEnabled);
		m_globalAddressBtn.setEnabled(isEnabled);
		m_emailAddressField.setEnabled(isEnabled);
	}

	public void init(AbstractReferenceLOVList source, Dimension size) {
		m_sourceData = source;
		m_listSize = size;
		initUI();
	}

	private void populateSourceList() {
		String[] listValues = m_sourceData.getList();
		String[] targetGlobalAddList = getTargetListObjects();

		Vector<String> vTargetList = new Vector<String>();

		for (int i = 0; targetGlobalAddList != null
				&& i < targetGlobalAddList.length; i++) {
			vTargetList.addElement(targetGlobalAddList[i]);
		}

		for (int j = 0; listValues != null && j < listValues.length; j++) {
			vTargetList.addElement(listValues[j]);
		}
		String[] targetListStr = vTargetList.toArray(new String[vTargetList
				.size()]);

		m_sourceList.setListData(targetListStr);
		m_sourceList.setPreferredSize(new Dimension(m_listSize.width,
				17 * targetListStr.length));
	}

	private boolean isValidEmail(String mailID) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*"
				+ "@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern p = Pattern.compile(EMAIL_PATTERN);
		Matcher m = p.matcher(mailID);

		if (!m.find()) {
			return false;
		}

		return true;
	}

	public void populateSourceList(Object[] sourceObjs) {
		if (sourceObjs != null) {
			if (m_sourceListModel.getSize() > 0) {
				m_sourceListModel.clear();
			}
			for (int i = 0; i < sourceObjs.length; i++) {
				if (!m_sourceListModel.contains(sourceObjs[i].toString())) {
					m_sourceListModel.addElement(sourceObjs[i].toString());
				}
			}
		}
	}

	public String[] getAllTargetListObjects() {
		String[] listValues = m_sourceData.getList();
		String[] targetGlobalAddList = getTargetListObjects();
		Vector<String> vTargetList = new Vector<String>();

		for (int i = 0; targetGlobalAddList != null
				&& i < targetGlobalAddList.length; i++) {
			vTargetList.addElement(targetGlobalAddList[i]);
		}

		for (int j = 0; listValues != null && j < listValues.length; j++) {
			vTargetList.addElement(listValues[j]);
		}
		String[] targetListStr = vTargetList.toArray(new String[vTargetList
				.size()]);

		return targetListStr;
	}

	private String[] getTargetListObjects() {
		String[] retObjs = new String[m_sourceListModel.getSize()];
		for (int i = 0; i < m_sourceListModel.getSize(); i++) {
			retObjs[i] = m_sourceListModel.getElementAt(i).toString();
		}
		return retObjs;
	}

	public void setNewDimention(int width, int height) {
		this.setPreferredSize(new Dimension(width, height));
		m_sourceListScroll.setPreferredSize(new Dimension((int) (width / 2.3),
				(int) (height / 1.6)));
	}
}
