package com.noi.rac.dhl.eco.util.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.noi.util.components.NOIJLabel;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVCRSelectionPanel extends JPanel implements IObserver, ISubject, ComponentListener,
        ListSelectionListener
{
    private static final long serialVersionUID = 1L;
    TCTable pendingCRtbl;
    TCTable apprCRtbl;
    TCTable ptargetCRs;
    TCTable atargetCRs;
    AIFTableModel selecedCRtblmodel;
    AIFTableModel pendingCRtblmodel;
    AIFTableModel closedCRtblmodel;
    TCSession session;
    private JScrollPane scpane;
    private JScrollPane scpane1;
    private JScrollPane scpane2;
    private JScrollPane scpane3;
    public Map<String, TCComponentForm> formCompMap;
    private Map<String, Vector<TCComponent>> itemRevFormMap;
    public Vector<TCComponentForm> addedCRsVec;
    public Set<TCComponentForm> removedCrs;
    TCUserService usrService;
    String typeofChange;
    // String crTypeAttr = "change_type";
    JTabbedPane tabbedPane;
    private ArrayList<IObserver> subList;
    
    private String[] typeStr = { "_DHL_EngChangeForm_" };
    private String[] relation = { "_ECNECR_" };
    private NOVCRDescriptionPanel pendingCRDescPanel = null;
    private NOVCRDescriptionPanel approvedCRDescPanel = null;
    private TableRowSorter<AIFTableModel> m_pendingCRTableSorter;
    private TableRowSorter<AIFTableModel> m_approvedCRTableSorter;
    private Registry m_registry; // TCDECREL - 4961
    
    public NOVCRSelectionPanel()
    {
        session = (TCSession) AIFUtility.getDefaultSession();
        formCompMap = new HashMap<String, TCComponentForm>();
        itemRevFormMap = new HashMap<String, Vector<TCComponent>>();
        addedCRsVec = new Vector<TCComponentForm>();
        removedCrs = new HashSet<TCComponentForm>();
        subList = new ArrayList<IObserver>();
        
        initTableUI();
        addComponentListener(this);
        usrService = session.getUserService();
        m_registry = Registry.getRegistry(this);// TCDECREL - 4961
        // populateCRTable("Open");
        // populateCRTable("Closed");
    }
    
    private void populateCRTable(String statusType)
    {
        try
        {
            if (typeofChange != null)
            {
                Object[] objs = new Object[2];
                objs[0] = statusType;
                objs[1] = typeofChange;
                Object retObj = usrService.call("NATOIL_CRsQuery", objs);
                
                if (retObj != null)
                {
                    removeRowsForSorting();// TCDECREL - 3382
                    
                    TCComponent[] crObjs = (TCComponent[]) retObj;
                    
                    for (int i = 0; i < crObjs.length; i++)
                    {
                        TCComponentForm form = (TCComponentForm) crObjs[i];
                        Vector<Object> rowData = getRowData(form);
                        
                        if (statusType.equals("Open"))
                        {
                            pendingCRtbl.addRow(rowData);
                        }
                        else if (statusType.equals("Closed"))
                        {
                            apprCRtbl.addRow(rowData);
                        }
                    }
                    sortCRTable();// TCDECREL - 3382
                }
            }
            
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    public void populateSelectedCRTable(TCComponentForm[] crforms)
    {
        if (crforms != null)
        {
            
            for (int i = 0; i < crforms.length; i++)
            {
                Vector<Object> rowData = getRowData(crforms[i]);
                selecedCRtblmodel.addRow(rowData);
                if (!addedCRsVec.contains(crforms[i]))
                {
                    addedCRsVec.add(crforms[i]);
                }
            }
        }
        apprCRtbl.updateUI();
    }
    
    public void setRowData(TCComponentForm crForm)
    {
        Vector<Object> rowData = getRowData(crForm);
        atargetCRs.addRow(rowData);
        addedCRsVec.add(crForm);
        atargetCRs.updateUI();
    }
    
    public void removeRow(String crNumber)
    {
        for (int i = 0; i < atargetCRs.getRowCount(); i++)
        {
            String tCrNum = (String) atargetCRs.getValueAt(i, 1);
            
            if (tCrNum.equals(crNumber))
            {
                atargetCRs.removeRow(i);
                break;
            }
        }
    }
    
    public Vector<Object> getRowData(TCComponentForm form)
    {
        java.util.Vector<Object> rowData = new java.util.Vector<Object>();
        String[] sProperties = {"object_name","change_type","group","requested_by","creation_date"};
        TCProperty[] sRetrievedProp = null;
        try
        {
            sRetrievedProp = form.getTCProperties(sProperties);
            rowData.add("");
//            String crName = form.getTCProperty("object_name").getStringValue();
//            rowData.add(crName);
//            rowData.add(form.getTCProperty("change_type").getStringValue());
//            rowData.add(form.getTCProperty("group").getStringValue());
//            rowData.add(form.getTCProperty("requested_by"));
//            rowData.add(form.getTCProperty("creation_date").getDateValue());
            String crName = sRetrievedProp[0].getStringValue();
            rowData.add(crName);
            rowData.add(sRetrievedProp[1].getStringValue());
            rowData.add(sRetrievedProp[2].getStringValue());
            rowData.add(sRetrievedProp[3].getStringValue());
            rowData.add(sRetrievedProp[4].getDateValue());
            rowData.add(getRefCOs(form));
            formCompMap.put(crName, form);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return rowData;
    }
    
    public Object[] getSelectedCRs()
    {
        Object[] forms = null;
        
        if (addedCRsVec.size() > 0)
        {
            forms = addedCRsVec.toArray();
        }
        return (Object[]) (forms == null ? new Object[0] : forms);
    }
    
    //TCDECREL-4398 - Method added for getting selected ECR while creating ECO
    public TCComponent[] getSelectedCRComponents()
    {
        TCComponent[] forms = null;
        
        if (addedCRsVec.size() > 0)
        {
            forms = addedCRsVec.toArray(new TCComponent[0]);
        }
        return (TCComponent[]) (forms == null ? new TCComponent[0] : forms);
    }
    
    public void clearSelectedCRs()
    {
        selecedCRtblmodel.removeAllRows();
        formCompMap.clear();
        addedCRsVec.removeAllElements();
        removedCrs.clear();
    }
    //5353-start
    public TCComponent[] getRemovedCRs()
    {
        TCComponent[] forms = null;
        
        if (removedCrs.size() > 0)
        {
            forms = removedCrs.toArray(new TCComponent[0]);
        }
        return (TCComponent[]) (forms == null ? new TCComponent[0] : forms);
    	
    	
    }
    //5353-end
    public Map<String, Vector<TCComponent>> buildCRItemRevMap()
    {
        Object[] forms = getSelectedCRs();
        if (forms != null)
        {
            itemRevFormMap.clear();
            for (int i = 0; i < forms.length; i++)
            {
                TCComponentForm crForm = (TCComponentForm) forms[i];
                try
                {
                    AIFComponentContext[] compConxt = crForm.whereReferenced();
                    for (int j = 0; j < compConxt.length; j++)
                    {
                        InterfaceAIFComponent intAifComp = compConxt[j].getComponent();
                        if (intAifComp instanceof TCComponentItemRevision)
                        {
                            TCComponentItemRevision itemRevAttached = (TCComponentItemRevision) intAifComp;
                            TCComponentItemRevision latestRev = itemRevAttached.getItem().getLatestItemRevision();
                            boolean isInProcess = NOVECOHelper.isItemRevInProcess(latestRev);
                            if (!isInProcess)
                            {
                                String crStr = latestRev.getProperty("object_string") + "::"
                                        + crForm.getProperty("object_name");
                                Vector<TCComponent> tarCompVec = new Vector<TCComponent>();
                                tarCompVec.add(0, (TCComponent) latestRev);
                                tarCompVec.add(1, crForm);
                                itemRevFormMap.put(crStr, tarCompVec);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            itemRevFormMap.clear();
        }
        return itemRevFormMap;
    }
    
    private String getRefCOs(TCComponentForm crForm)
    {
        String retStr = "";
        if (crForm != null)
        {
            try
            {
                AIFComponentContext[] compCxt = crForm.whereReferencedByTypeRelation(typeStr, relation);
                for (int i = 0; i < compCxt.length; i++)
                {
                    retStr = retStr + compCxt[i].getComponent().getProperty("object_name") + ",";
                }
                if (compCxt.length > 0)
                {
                    if (retStr.substring(retStr.length() - 1, retStr.length()).equals(","))
                    {
                        retStr = retStr.substring(0, retStr.length() - 1);
                    }
                }
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return retStr;
    }
    
    private void initTableUI()
    {
        String[] columnNames = { "", "CR", "Type", "Group", "Initiator", "Date Entered", "Ref.CO's" };
        
        selecedCRtblmodel = new AIFTableModel(columnNames);
        
        pendingCRtblmodel = new AIFTableModel(columnNames);
        closedCRtblmodel = new AIFTableModel(columnNames);
        pendingCRtbl = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        pendingCRtbl.setModel(pendingCRtblmodel);
        
        // Rakesh - Add Row selection listener
        pendingCRtbl.getSelectionModel().addListSelectionListener(this);
        pendingCRtbl.getColumnModel().getSelectionModel().addListSelectionListener(this);
        
        // pendingCRtbl.setColumnNames(columnNames);
        //pendingCRtbl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        pendingCRtbl.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        pendingCRtbl.getTableHeader().setReorderingAllowed(false);
        pendingCRtbl.getColumnModel().getColumn(0).setResizable(false);
        pendingCRtbl.getColumnModel().getColumn(0).setMaxWidth(25);
        pendingCRtbl.setSortEnabled(false);// TCDECREL - 3382
        createPendingCRTableSorter();// TCDECREL - 3382
        scpane = new JScrollPane(pendingCRtbl);
        scpane.setPreferredSize(new Dimension(520, 150));
        
        apprCRtbl = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        apprCRtbl.setModel(closedCRtblmodel);
        
        // Rakesh - Add Row selection listener
        apprCRtbl.getSelectionModel().addListSelectionListener(this);
        apprCRtbl.getColumnModel().getSelectionModel().addListSelectionListener(this);
        
        //apprCRtbl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        apprCRtbl.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        apprCRtbl.getTableHeader().setReorderingAllowed(false);
        apprCRtbl.getColumnModel().getColumn(0).setResizable(false);
        apprCRtbl.getColumnModel().getColumn(0).setMaxWidth(25);
        apprCRtbl.setSortEnabled(false);// TCDECREL - 3382
        createApprovedCRTableSorter();// TCDECREL - 3382
        scpane1 = new JScrollPane(apprCRtbl);
        scpane1.setPreferredSize(new Dimension(520, 150));
        
        ptargetCRs = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        ptargetCRs.setModel(selecedCRtblmodel);
        //ptargetCRs.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ptargetCRs.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        ptargetCRs.getTableHeader().setReorderingAllowed(false);
        ptargetCRs.getColumnModel().getColumn(0).setResizable(false);
        ptargetCRs.getColumnModel().getColumn(0).setMaxWidth(25);
        ptargetCRs.getSelectionModel().addListSelectionListener(this);
        ptargetCRs.getColumnModel().getSelectionModel().addListSelectionListener(this);
        ptargetCRs.setSortEnabled(false);
        
        scpane2 = new JScrollPane(ptargetCRs);
        scpane2.setPreferredSize(new Dimension(520, 150));// 7786
        
        atargetCRs = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        atargetCRs.setModel(selecedCRtblmodel);
        //atargetCRs.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        atargetCRs.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        atargetCRs.getTableHeader().setReorderingAllowed(false);
        atargetCRs.getColumnModel().getColumn(0).setResizable(false);
        atargetCRs.getColumnModel().getColumn(0).setMaxWidth(25);
        atargetCRs.getSelectionModel().addListSelectionListener(this);
        atargetCRs.getColumnModel().getSelectionModel().addListSelectionListener(this);
        atargetCRs.setSortEnabled(false);
        
        scpane3 = new JScrollPane(atargetCRs);
        scpane3.setPreferredSize(new Dimension(520, 150));// 7786
        
        pendingCRtbl.getColumnModel().getColumn(0)
                .setCellEditor(new TableButtonEditor(NOVCRSelectionPanel.this, pendingCRtbl, ptargetCRs));
        apprCRtbl.getColumnModel().getColumn(0)
                .setCellEditor(new TableButtonEditor(NOVCRSelectionPanel.this, apprCRtbl, atargetCRs));
        ptargetCRs.getColumnModel().getColumn(0)
                .setCellEditor(new TableButtonEditor(NOVCRSelectionPanel.this, null, ptargetCRs));
        atargetCRs.getColumnModel().getColumn(0)
                .setCellEditor(new TableButtonEditor(NOVCRSelectionPanel.this, null, atargetCRs));
        
        pendingCRtbl.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(true));
        apprCRtbl.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(true));
        ptargetCRs.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(false));
        atargetCRs.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(false));
        
        pendingCRtbl.getColumnModel().getColumn(5).setCellRenderer(new DateCellRenderer());
        apprCRtbl.getColumnModel().getColumn(5).setCellRenderer(new DateCellRenderer());
        ptargetCRs.getColumnModel().getColumn(5).setCellRenderer(new DateCellRenderer());
        atargetCRs.getColumnModel().getColumn(5).setCellRenderer(new DateCellRenderer());
        
        pendingCRtbl.getColumnModel().getColumn(6).setCellRenderer(new TextWrapRenderer());
        apprCRtbl.getColumnModel().getColumn(6).setCellRenderer(new TextWrapRenderer());
        ptargetCRs.getColumnModel().getColumn(6).setCellRenderer(new TextWrapRenderer());
        atargetCRs.getColumnModel().getColumn(6).setCellRenderer(new TextWrapRenderer());
        
        pendingCRtbl.getColumnModel().getColumn(0).setPreferredWidth(18);
        apprCRtbl.getColumnModel().getColumn(0).setPreferredWidth(18);
        ptargetCRs.getColumnModel().getColumn(0).setPreferredWidth(18);
        atargetCRs.getColumnModel().getColumn(0).setPreferredWidth(18);
        
        pendingCRtbl.getColumnModel().getColumn(5).setPreferredWidth(105);
        apprCRtbl.getColumnModel().getColumn(5).setPreferredWidth(105);
        ptargetCRs.getColumnModel().getColumn(5).setPreferredWidth(105);
        atargetCRs.getColumnModel().getColumn(5).setPreferredWidth(105);
        
        DefaultTableCellRenderer topRenderer = new DefaultTableCellRenderer();
        topRenderer.setVerticalAlignment(JLabel.TOP);
        
        pendingCRtbl.getColumnModel().getColumn(1).setCellRenderer(topRenderer);
        pendingCRtbl.getColumnModel().getColumn(2).setCellRenderer(topRenderer);
        pendingCRtbl.getColumnModel().getColumn(3).setCellRenderer(topRenderer);
        pendingCRtbl.getColumnModel().getColumn(4).setCellRenderer(topRenderer);
        
        apprCRtbl.getColumnModel().getColumn(1).setCellRenderer(topRenderer);
        apprCRtbl.getColumnModel().getColumn(2).setCellRenderer(topRenderer);
        apprCRtbl.getColumnModel().getColumn(3).setCellRenderer(topRenderer);
        apprCRtbl.getColumnModel().getColumn(4).setCellRenderer(topRenderer);
        
        
        ptargetCRs.getColumnModel().getColumn(1).setCellRenderer(topRenderer);
        ptargetCRs.getColumnModel().getColumn(2).setCellRenderer(topRenderer);
        ptargetCRs.getColumnModel().getColumn(3).setCellRenderer(topRenderer);
        ptargetCRs.getColumnModel().getColumn(4).setCellRenderer(topRenderer);
        
        atargetCRs.getColumnModel().getColumn(1).setCellRenderer(topRenderer);
        atargetCRs.getColumnModel().getColumn(2).setCellRenderer(topRenderer);
        atargetCRs.getColumnModel().getColumn(3).setCellRenderer(topRenderer);
        atargetCRs.getColumnModel().getColumn(4).setCellRenderer(topRenderer);

        // Rakesh - ECO description panel starts here
        pendingCRDescPanel = new NOVCRDescriptionPanel();
        pendingCRDescPanel.showCRDescriptionPanel(false);
        approvedCRDescPanel = new NOVCRDescriptionPanel();
        approvedCRDescPanel.showCRDescriptionPanel(false);
        // Rakesh - ECO description panel ends here
        
        JLabel label1 = new JLabel("Added Change Requests");
        label1.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        
        JLabel label2 = new JLabel("Added Change Requests");
        label2.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        
        JPanel pendingCrPanel = new JPanel(new VerticalLayout());
        pendingCrPanel.add("top.bind.center.center", scpane);
        pendingCrPanel.add("top.bind.center.center", pendingCRDescPanel);
        pendingCrPanel.add("top.bind.center.center", label1);
        pendingCrPanel.add("top.bind.center.center", scpane2);
        
        JPanel approvedCrPanel = new JPanel(new VerticalLayout());
        approvedCrPanel.add("top.bind.center.center", scpane1);
        approvedCrPanel.add("top.bind.center.center", approvedCRDescPanel);
        approvedCrPanel.add("top.bind.center.center", label2);
        approvedCrPanel.add("top.bind.center.center", scpane3);
        
        tabbedPane = new JTabbedPane();
        tabbedPane.setPreferredSize(new Dimension(550, 550));// 7786
        tabbedPane.addTab("OpenCRs" + " ( " + 0 +" )", pendingCrPanel);
        tabbedPane.addTab("ClosedCRs"+ " ( " + 0 +" )", approvedCrPanel);
        
        JPanel filerPanel = new JPanel(new PropertyLayout());
        final JComboBox grpcmb = new JComboBox();
        
        /* Rakesh:060312 - Group list is driven by preference - Starts here */
        // grpcmb.addItem("");
        // NOVECOHelper.retriveLOVValues(session,NOVECOConstants.DHL_ECR_Groups,
        // grpcmb);
        String groupPrefName = NOVECOConstants.ECR_Groups_base + session.getCurrentGroup().toString().substring(0, 2);
        NOVECOHelper.retriveLOVValues(session, groupPrefName, grpcmb);
        
        // TCDECREL-3519 -> Commented out setting default value. Because ECR
        // values are not getting
        // populated when user selects the value from the combo
        /*
         * if(grpcmb.getItemCount() == 2) grpcmb.setSelectedIndex(1);
         */// Default to Group if there is only 1 group present
        /* Rakesh:060312 - Group list is driven by preference - Ends here */

        grpcmb.setPreferredSize(new Dimension(150, 20));
        
        grpcmb.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                if (itemEvt.getStateChange() == ItemEvent.SELECTED && ((String)itemEvt.getItem()).length()!=0) //Added AND logical condition to avoid listener to call when user hit enter on txtfield and it clears last selected combo value
                {
                    String groupName = (String) itemEvt.getItem();
                    searchCR(null, groupName, false,m_registry.getString("ShowGroupSearch.MSG"));//7784
                }
            }
        });
        
        final JTextField tiltertxt = new JTextField(15);
        tiltertxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    grpcmb.setSelectedItem("");
                    String crNumber = tiltertxt.getText();
                    searchCR(crNumber, null, false,m_registry.getString("ShowECRSearch.MSG"));//7784
                }
            }
        });
        
        JButton showFilerBtn = new JButton("Show All");
        showFilerBtn.addActionListener(new ActionAdapter()
        {
            public void actionPerformed(ActionEvent ae)
            {
                grpcmb.setSelectedItem("");
                tiltertxt.setText("");
                searchCR(null, null, true,m_registry.getString("ShowAllSearch.MSG"));//7784                
            }
        });
        
       
        JButton findBtn = new JButton("Find CR");
        findBtn.addActionListener(new ActionAdapter()
        {
            public void actionPerformed(ActionEvent ae)
            {
                grpcmb.setSelectedItem("");
                String crNumber = tiltertxt.getText();
                searchCR(crNumber, null, false,m_registry.getString("ShowECRSearch.MSG"));//7784
            }
        });
        filerPanel.add("1.1.center.center",new NOIJLabel("Group Filter"));
        filerPanel.add("1.2.center.center.resizable.preferred",grpcmb);
        filerPanel.add("1.3.center.center.resizable.preferred",tiltertxt);
        filerPanel.add("1.4.center.center",findBtn);
        filerPanel.add("1.5.center.center",showFilerBtn);
        
        this.setLayout(new VerticalLayout(0));
        this.add("top.bind.center.center", filerPanel);
        this.add("top.bind.center.center", tabbedPane);
    }
    
    /*
     * public String getCRCommentString()//soma start { Object[] forms =
     * addedCRsVec.toArray(); String comments=""; for (int i = 0; i <
     * forms.length; i++) { TCComponentForm crForm = (TCComponentForm)forms[i];
     * try { String crName =
     * crForm.getTCProperty("object_name").getStringValue(); String
     * crComments=crForm.getTCProperty("comments").getStringValue(); if
     * (comments.length()>0) { comments = comments + "CR:"+crName +"\n"; } else
     * { comments = "CR:"+crName +"\n"; } if(crComments!=null) { comments
     * =comments + "CR Comments:" + crComments + "\n"; } comments = comments +
     * "---------------------------------------"+"\n"; } catch (TCException e) {
     * e.printStackTrace(); } } return comments; }//soma end
     */

    public String getReason4ChangeString()
    {
        Object[] forms = addedCRsVec.toArray();
        String reason4Change = "";
        for (int i = 0; i < forms.length; i++)
        {
            TCComponentForm crForm = (TCComponentForm) forms[i];
            
            try
            {
                String crName = crForm.getTCProperty("object_name").getStringValue();
                String crReason4Chn = crForm.getTCProperty("reason_for_change").getStringValue();
                String crDetails = crForm.getTCProperty("change_desc").getStringValue();
                String crComments = crForm.getTCProperty("comments").getStringValue();// soma
                
                if (reason4Change.length() > 0)
                {
                    reason4Change = reason4Change + "CR:" + crName + "\n";
                }
                else
                {
                    reason4Change = "CR:" + crName + "\n";
                }
                if (crReason4Chn != null)
                {
                    reason4Change = reason4Change + "Reason for change:" + crReason4Chn + "\n";
                }
                if (crDetails != null)
                {
                    reason4Change = reason4Change + "CR Detail:" + crDetails + "\n";
                }
                if (crComments != null)
                {
                    reason4Change = reason4Change + "CR Comments:" + crComments + "\n";
                }
                
                reason4Change = reason4Change + "---------------------------------------" + "\n";
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        return reason4Change;
    }
    //7784-Start
    public void executeECRSearch(String formName, String groupName, boolean isShowAll)
    {
        NOVSearchECRFormOperation searchOp = new NOVSearchECRFormOperation();
        searchOp.setChangeType(typeofChange);
        searchOp.setFormName(formName);
        searchOp.setGroupName(groupName);
        searchOp.setShowAll(isShowAll);
        removeClsAndOpenRowsForSorting();
        
        HashMap<String, Vector<Object>> mapSrchOpenECRForm = new HashMap<String, Vector<Object>>();
        searchOp.setStatus("Open");
        mapSrchOpenECRForm = searchOp.executeSearch();
        addCRsToTable(pendingCRtblmodel, mapSrchOpenECRForm);
        
        HashMap<String, Vector<Object>> mapSrchClosedECRForm = new HashMap<String, Vector<Object>>();
        searchOp.setStatus("Closed");
        mapSrchClosedECRForm = searchOp.executeSearch();
        addCRsToTable(closedCRtblmodel, mapSrchClosedECRForm);
        
        tabbedPane.setTitleAt(0, " OpenCRs ( "+ mapSrchOpenECRForm.size() + " ) ");
        tabbedPane.setTitleAt(1, " ClosedCRS ( "+ mapSrchClosedECRForm.size() + " ) ");
        sortOpenAndClsCRTable();
    }
    
    private void addCRsToTable(AIFTableModel tableModel, Map<String, Vector<Object>> ecrMap)
    {
    	if(ecrMap != null && ecrMap.size() > 0)
        {
        	String[] strForms =  ecrMap.keySet().toArray(new String[ecrMap.size()]);
            for (int i = 0; i < strForms.length;i++)
            {
                Vector<String> rowData = new Vector<String>();
                Vector<Object> vectStrRow = new Vector<Object>();
                vectStrRow = ecrMap.get(strForms[i]);
                rowData.add(strForms[i]);
                rowData.add((String) vectStrRow.elementAt(0));
                rowData.add((String) vectStrRow.elementAt(1));
                rowData.add((String) vectStrRow.elementAt(2));
                rowData.add((String) vectStrRow.elementAt(3));
                rowData.add((String) vectStrRow.elementAt(4));
                rowData.add((String) vectStrRow.elementAt(5));
                TCComponentForm component= null;
				try {
					component = (TCComponentForm)session.stringToComponent(strForms[i]);
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                formCompMap.put((String) vectStrRow.elementAt(0), component);
                tableModel.addRow(rowData);
            }
        }
    }
    //7784-End
    
    //6144-start
    public void fireUserServiceCall(String functionName, Object[] args)
    {
    	if(args.length == 2 && functionName.equalsIgnoreCase("NATOIL_CRsQuery"))
    	{
    		updateTableForShowAll(functionName,args);
    	}
    	else
    	{
    		Object[] closedInput=new Object[3];
            Object[] openInput=new Object[3];
            for(int i=0;i<args.length;i++)
            {
            	if(i==args.length-2)
            	{
            		if( args[i].equals("Open"))
            		{
            			openInput[args.length-2]=args[i];
            			closedInput[args.length-2]="Closed";
            		}
            		else
            		{
            			closedInput[args.length-2]=args[i];
            			openInput[args.length-2]="Open";
            		}
            	}
            	else
            	{
            		openInput[i]=args[i];
            		closedInput[i]=args[i];
            	}
            }
            try
            {
            	Object openRetObj = usrService.call(functionName, openInput);
                Object closedRetObj = usrService.call(functionName, closedInput);
                updateCRsTable(openRetObj, closedRetObj);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
    	}
    	
    }
   private void updateCRsTable(Object openRetObj, Object closedRetObj)
   {
	   if (openRetObj != null && closedRetObj!= null)
       {
		   //removeRowsForSorting();// TCDECREL - 3382
       	   removeClsAndOpenRowsForSorting();
           TCComponent[] crOpenObjs = (TCComponent[]) openRetObj;
           TCComponent[] crClosedObjs = (TCComponent[]) closedRetObj;
           int cntOpen = crOpenObjs.length;
           int cntClosed = crClosedObjs.length;
           tabbedPane.setTitleAt(0, " OpenCRs ( "+ cntOpen + " ) ");
           tabbedPane.setTitleAt(1, " ClosedCRS ( "+ cntClosed + " ) ");
           addOpenCRsToTable(crOpenObjs);
           addClosedRsToTable(crClosedObjs);
           //sortCRTable();// TCDECREL - 3382
           sortOpenAndClsCRTable();
       }
	   
   }
    private void updateTableForShowAll (String functionName, Object[] args)
    {
    	Object[] closedInput=new Object[2];
        Object[] openInput=new Object[2];
        for(int i=0;i<args.length;i++)
        {
        	if(args[0].equals("Open"))
        	{
        		openInput[i]=args[i];
        		openInput[i+1]=args[i+1];
        		closedInput[i]="Closed";
        		closedInput[i+1]=args[i+1];
        		break;
        	}
        	else
        	{
        		openInput[i]="Open";
        		openInput[i+1]=args[i+1];
        		closedInput[i]=args[i];
        		closedInput[i+1]=args[i+1];
        		break;
        	}
        }
        try
        {
        	Object openRetObj = usrService.call(functionName, openInput);
            Object closedRetObj = usrService.call(functionName, closedInput);
            updateCRsTable(openRetObj, closedRetObj);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
 
   
    private void addOpenCRsToTable(TCComponent[] crOpenObjs )
    {
    	for (int i = 0; i < crOpenObjs.length; i++)
        {
        	TCComponentForm form = (TCComponentForm) crOpenObjs[i];
            java.util.Vector<Object> rowData = getRowData(form);
            pendingCRtbl.addRow(rowData);
        }
    }
    private void addClosedRsToTable(TCComponent[] crClosedObjs )
    {
    	 for (int i = 0; i < crClosedObjs.length; i++)
         {
             TCComponentForm form = (TCComponentForm) crClosedObjs[i];
             java.util.Vector<Object> rowData = getRowData(form);
             apprCRtbl.addRow(rowData);
         }
    }
    //6144-end
    
    public class DateCellRenderer extends DefaultTableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            super.setVerticalAlignment(JLabel.TOP);
            if (value instanceof Date)
            {
                String strDate = new SimpleDateFormat("dd/MM/yy").format((Date) value);
                this.setText(strDate);
            }
            return this;
        }
    }
    
    public class TextWrapRenderer extends JTextArea implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public TextWrapRenderer()
        {
            setLineWrap(true);
            setWrapStyleWord(true);
        }
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            // Component comp = super.getTableCellRendererComponent( table,
            // value, isSelected, hasFocus, row, column );
            /*
             * if ( comp instanceof JComponent ) { String toolTipText =
             * table.getValueAt(row, column).toString();
             * ((JComponent)comp).setToolTipText(toolTipText); }
             */
            if (isSelected)
            {
                setBackground(table.getSelectionBackground());
                setForeground(table.getForeground());
            }
            else
            {
                setBackground(table.getBackground());
                setForeground(table.getForeground());
            }
            setText(value.toString());
            setSize(table.getColumnModel().getColumn(column).getWidth(), getPreferredSize().height);
           
            if (table.getRowHeight(row) != getPreferredSize().height)
            {
                table.setRowHeight(row, getPreferredSize().height);
            }
            
            return this;
            
        }
    }
    
    public void setTypeofChange(String typeOfChg)
    {
        if (!typeOfChg.equals(typeofChange))
        {
            typeofChange = typeOfChg;
            //TCDECREL-4998:Start Desc: Resetting table content
            //pendingCRtbl.removeAllRows();
            //apprCRtbl.removeAllRows();
            removePendingCRTableRows();
            removeApprovedCRTableRows();
            tabbedPane.setTitleAt(0, "OpenCRs" + " ( " + 0 +" )");
            tabbedPane.setTitleAt(1, " ClosedCRS ( "+ 0 + " ) ");
            //TCDECREL-4998:End
            if (typeofChange.equals("Lifecycle"))
            {
                // populateCRTable("Open");
            }
            else if (typeofChange.equals("General"))
            {
                // populateCRTable("Open");
            }
        }
        
    }
    
    public void update()
    {
        // System.out.println("CR Selection page is getting updated....");
        NOVCRItemSelectionPanel itemSelectionPanel = null;
        for (int i = 0; i < subList.size(); i++)
        {
            IObserver obrPanel = subList.get(i);
            if (obrPanel instanceof NOVCRItemSelectionPanel)
            {
                itemSelectionPanel = (NOVCRItemSelectionPanel) obrPanel;
                break;
            }
        }
        
        if (itemSelectionPanel != null)
        {
            Vector<TCComponent> targetCRs = itemSelectionPanel.targetTable.targetCRs;
            Vector<TCComponent> removedCRs = itemSelectionPanel.targetTable.removedCRs;
            
            for (int i = 0; i < targetCRs.size(); i++)
            {
                if (!addedCRsVec.contains(targetCRs.get(i)))
                {
                    setRowData((TCComponentForm) targetCRs.get(i));
                }
            }
            
            for (int i = 0; i < removedCRs.size(); i++)
            {
                TCComponent crform = removedCRs.get(i);
                try
                {
                    String crNumber = crform.getProperty("object_name");
                    removeRow(crNumber);
                    addedCRsVec.remove(crform);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    public void notifyObserver()
    {
        for (int i = 0; i < subList.size(); i++)
        {
            subList.get(i).update();
        }
    }
    
    public void registerObserver(IObserver subs)
    {
        subList.add(subs);
    }
    
    public void removeObserver(IObserver subs)
    {
        
    }
    
    public void componentHidden(ComponentEvent e)
    {
    }
    
    public void componentMoved(ComponentEvent e)
    {
    }
    
    public void componentResized(ComponentEvent e)
    {
        /*
         * Dimension dim = NOVCRSelectionPanel.this.getSize(); int ht =
         * dim.height; int wt = dim.width; tabbedPane.setPreferredSize(new
         * Dimension(wt,ht)); scpane.setPreferredSize(new Dimension(wt,ht/2));
         * scpane1.setPreferredSize(new Dimension(wt,ht/2));
         * scpane2.setPreferredSize(new Dimension(wt,(int)(ht/3)));
         * scpane3.setPreferredSize(new Dimension(wt,(int)(ht/3)));
         * pendingCRtbl.
         * getColumnModel().getColumn(3).setPreferredWidth((int)(wt/5));
         * apprCRtbl
         * .getColumnModel().getColumn(3).setPreferredWidth((int)(wt/5));
         * ptargetCRs
         * .getColumnModel().getColumn(3).setPreferredWidth((int)(wt/5));
         * atargetCRs
         * .getColumnModel().getColumn(3).setPreferredWidth((int)(wt/5));
         * pendingCRtbl
         * .getColumnModel().getColumn(4).setPreferredWidth((int)(wt/5));
         * apprCRtbl
         * .getColumnModel().getColumn(4).setPreferredWidth((int)(wt/5));
         * ptargetCRs
         * .getColumnModel().getColumn(4).setPreferredWidth((int)(wt/5));
         * atargetCRs
         * .getColumnModel().getColumn(4).setPreferredWidth((int)(wt/5));
         * pendingCRtbl
         * .getColumnModel().getColumn(6).setPreferredWidth((int)(wt/3.3));
         * apprCRtbl
         * .getColumnModel().getColumn(6).setPreferredWidth((int)(wt/3.3));
         * ptargetCRs
         * .getColumnModel().getColumn(6).setPreferredWidth((int)(wt/3.3));
         * atargetCRs
         * .getColumnModel().getColumn(6).setPreferredWidth((int)(wt/3.3));
         * scpane.updateUI(); scpane1.updateUI(); scpane2.updateUI();
         * scpane3.updateUI(); tabbedPane.updateUI(); this.updateUI();
         * this.revalidate(); this.repaint();
         */
    }
    
    public void componentShown(ComponentEvent e)
    {
    }
    
    // Rakesh - Implement Row Selection for CR list panel
    public void valueChanged(ListSelectionEvent e)
    {
        String crName = null;
        if (e.getSource() == pendingCRtbl.getSelectionModel() && pendingCRtbl.getRowSelectionAllowed())
        {
            //TCDECREL-4998 desc: Put condition to prevent from Exception while selecting or unselecting table row
            if(pendingCRtbl.getSelectedRow() != -1)
            {
                crName = (String) pendingCRtbl.getValueAt(pendingCRtbl.getSelectedRow(), 1);
            }
            if (crName != null)
            {
                try
                {
                    TCComponentForm form = formCompMap.get(crName);
                    pendingCRDescPanel.displayCRDescription(form);
                }
                catch (TCException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        if (e.getSource() == apprCRtbl.getSelectionModel() && apprCRtbl.getRowSelectionAllowed())
        {
            crName = (String) apprCRtbl.getValueAt(apprCRtbl.getSelectedRow(), 1);
            if (crName != null)
            {
                try
                {
                    TCComponentForm form = formCompMap.get(crName);
                    approvedCRDescPanel.displayCRDescription(form);
                }
                catch (TCException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        
    }
    
    private void createPendingCRTableSorter()
    {
        
        m_pendingCRTableSorter = new TableRowSorter<AIFTableModel>(pendingCRtblmodel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                pendingCRtbl.setRowSorter(this);
                this.sort();
                
                System.out.println("Running custom Sorting: Column " + column + order);
                
            }
        };
        
        try
        {
            List<SortKey> keys = new ArrayList<SortKey>();
            SortKey sortKey = new SortKey(pendingCRtblmodel.findColumn("CR"), SortOrder.ASCENDING);
            keys.add(sortKey);
            m_pendingCRTableSorter.setSortKeys(keys);
            pendingCRtbl.setRowSorter(m_pendingCRTableSorter);
            m_pendingCRTableSorter.setSortable(1, false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
    }
    
    private void createApprovedCRTableSorter()
    {
        m_approvedCRTableSorter = new TableRowSorter<AIFTableModel>(closedCRtblmodel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                apprCRtbl.setRowSorter(this);
                this.sort();
                
                System.out.println("Running custom Sorting: Column " + column + order);
                
            }
        };
        
        try
        {
            List<SortKey> keys = new ArrayList<SortKey>();
            SortKey sortKey = new SortKey(closedCRtblmodel.findColumn("CR"), SortOrder.ASCENDING);
            keys.add(sortKey);
            m_approvedCRTableSorter.setSortKeys(keys);
            apprCRtbl.setRowSorter(m_approvedCRTableSorter);
            m_approvedCRTableSorter.setSortable(1, false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    private void sortCRTable()
    {
        if (pendingCRtbl.getRowCount() >= 1 && pendingCRtbl.getRowSorter() == null)
        {
            sortPendingCRTable();
        }
        else if (apprCRtbl.getRowCount() >= 1 && apprCRtbl.getRowSorter() == null)
        {
            sortApprovedCRTable();
        }
    }
    
    private void sortPendingCRTable()
    {
        pendingCRtbl.setRowSorter(m_pendingCRTableSorter);
        m_pendingCRTableSorter.setSortable(1, false);
        m_pendingCRTableSorter.sort();
    }
  //6144-start
    private void sortOpenAndClsCRTable()
    {
    	sortPendingCRTable();
        sortApprovedCRTable();
    }
    private void removeClsAndOpenRowsForSorting()
    {
    	removePendingCRTableRows();
    	removeApprovedCRTableRows();
    }
    //6144-end
    
    private void sortApprovedCRTable()
    {
        apprCRtbl.setRowSorter(m_approvedCRTableSorter);
        m_approvedCRTableSorter.setSortable(1, false);
        m_approvedCRTableSorter.sort();
    }
    
    private void removeRowsForSorting()
    {
        if (tabbedPane.getSelectedIndex() == 0)
        {
            removePendingCRTableRows();
        }
        else if (tabbedPane.getSelectedIndex() == 1)
        {
            removeApprovedCRTableRows();
        }
    }
    
    private void removePendingCRTableRows()
    {
        pendingCRtbl.setRowSorter(null);
        pendingCRtblmodel.removeAllRows();
    }
    
    private void removeApprovedCRTableRows()
    {
        apprCRtbl.setRowSorter(null);
        closedCRtblmodel.removeAllRows();
    }
    
  
    private void searchCR(final String formName, final String groupName, final boolean isShowAll, final String message) //7784
    {
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if(display != null)
        {
            display.syncExec(new Runnable() 
            {
                public void run()
                {
                    try
                    {
                        new ProgressMonitorDialog(display.getActiveShell()).run(true, true,
                                new NOVWizardSearchECROperation(NOVCRSelectionPanel.this, formName, groupName, message));
                    }
                    catch(InvocationTargetException e)
                    {
                        e.printStackTrace();
                    }
                    catch(InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    
    
}
