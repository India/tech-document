package com.noi.rac.dhl.eco.form.compound.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import com.noi.rac.dhl.eco.form.compound.util.NOVCustomFormProperties;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.rac.util.iTextField;

public class _EngChangeRequestForm_DC extends NOVCustomFormProperties 
{
	public iTextField ecrNoTextField;
	public iTextField ecrStatTextField;
	public iTextField requesterTextField;
	public iTextField reqDateTextField;
	public iTextField productLIneTextField;
	public iTextField typOfChangTextField;
	public iTextField reason4ChangeTextField;
	public iTextArea chngDescTextArea;
	public iTextField apprGrpTextField;
	public iTextField apprTextField;
	public iTextField apprDateTextField;
	public JList emailDistrList;
	public iTextArea commentsTextArea;
	public iTextField closedByTextField;
	public iTextArea closureCommentsTextArea;
	public DefaultListModel listModel;

    String[]          propertyNames = new String[]
                                    { "object_name","ecr_status",
            "requested_by", "requested_date", "nov4_product_line", "change_type", "reason_for_change",
            "change_desc", "group", "disposition_by", "decision_date", "distribution", "comments", "nov4_ecrclosed_by", "nov4_closure_comments" };

    public _EngChangeRequestForm_DC()
    {
        ecrNoTextField = new iTextField();
        ecrStatTextField = new iTextField();
        requesterTextField = new iTextField();
        reqDateTextField = new iTextField();
        productLIneTextField = new iTextField();
        typOfChangTextField = new iTextField();
        reason4ChangeTextField = new iTextField();
        chngDescTextArea = new iTextArea();
        apprGrpTextField = new iTextField();
        apprTextField = new iTextField();
        apprDateTextField = new iTextField();
        listModel = new DefaultListModel();
        emailDistrList = new JList(listModel);
        commentsTextArea = new iTextArea();
        closedByTextField = new iTextField();
        closureCommentsTextArea = new iTextArea();
        

        
        addProperty("object_name", ecrNoTextField);
        addProperty("ecr_status", ecrStatTextField);
        addProperty("requested_by", requesterTextField);
        addProperty("requested_date", reqDateTextField); 
        addProperty("nov4_product_line", productLIneTextField);
        addProperty("change_type", typOfChangTextField);
        addProperty("reason_for_change", reason4ChangeTextField);
        addProperty("change_desc", chngDescTextArea);
        addProperty("group", apprGrpTextField);
        addProperty("disposition_by", apprTextField);
        addProperty("decision_date", apprDateTextField);
        addProperty("distribution", emailDistrList);
        addProperty("comments", commentsTextArea);
        addProperty("nov4_ecrclosed_by", closedByTextField);
        addProperty("nov4_closure_comments", closureCommentsTextArea);
    }
 
	public String[] getPropertyNames()
	{
		return propertyNames; 
	}

	public void setForm(TCComponent form) 
	{
		if (form!=null) 
		{
			super.setForm(form);	            
			
			try 
			{
			    super.getFormProperties(propertyNames);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
	}

	public void clearForm() 
	{}

	public void populateFormComponents() 
	{
		if(getForm() == null) return;

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		
		ecrNoTextField.setText(((TCProperty)imanProperties.get(ecrNoTextField)).getStringValue());
		ecrStatTextField.setText(((TCProperty)imanProperties.get(ecrStatTextField)).getStringValue());
		requesterTextField.setText(((TCProperty)imanProperties.get(requesterTextField)).getStringValue());
		Date requestorDate =((TCProperty)imanProperties.get(reqDateTextField)).getDateValue();
		if(((TCProperty)imanProperties.get(reqDateTextField)).getDateValue() != null)
		    reqDateTextField.setText(sdf.format(requestorDate));
		productLIneTextField.setText(((TCProperty)imanProperties.get(productLIneTextField)).getStringValue());
		typOfChangTextField.setText(((TCProperty)imanProperties.get(typOfChangTextField)).getStringValue());
	    reason4ChangeTextField.setText(((TCProperty)imanProperties.get(reason4ChangeTextField)).getStringValue());
	    chngDescTextArea.setText(((TCProperty)imanProperties.get(chngDescTextArea)).getStringValue());
	    apprGrpTextField.setText(((TCProperty)imanProperties.get(apprGrpTextField)).getStringValue());
	    // for disposition by
	    String[] dispStrArr =((TCProperty)imanProperties.get(apprTextField)).getStringValueArray();
	    String appendStr ="";
	    for ( int i = 0; i < dispStrArr.length; i++ )
	    {
	      if(i == 0)
	    	  appendStr = appendStr+ dispStrArr[i];
	      else
	    	  appendStr = appendStr+","+ dispStrArr[i];
	    } 
	    apprTextField.setText(appendStr);
	    Date approvedDate = ((TCProperty)imanProperties.get(apprDateTextField)).getDateValue();
	    if(((TCProperty)imanProperties.get(apprDateTextField)).getDateValue() != null)
	       apprDateTextField.setText(sdf.format(approvedDate));
	     // for distribution list
	    String[] distrStrArry =((TCProperty)imanProperties.get(emailDistrList)).getStringValueArray();
	    for ( int i = 0; i < distrStrArry.length; i++ )
	    {
	      ((DefaultListModel)emailDistrList.getModel()).addElement(distrStrArry[i]);
	    }  
	    commentsTextArea.setText(((TCProperty)imanProperties.get(commentsTextArea)).getStringValue());
		
	    closedByTextField.setText(((TCProperty)imanProperties.get(closedByTextField)).getStringValue());
	    closureCommentsTextArea.setText(((TCProperty)imanProperties.get(closureCommentsTextArea)).getStringValue());
	}

	public void saveFormData() 
	{
		try 
		{
			 /*((TCProperty)imanProperties.get(ecrNoTextField)).setStringValueData(ecrNoTextField.getText());
			 ((TCProperty)imanProperties.get(ecrStatTextField)).setStringValueData(ecrStatTextField.getText());
			 ((TCProperty)imanProperties.get(requesterTextField)).setStringValueData(requesterTextField.getText());
			 ((TCProperty)imanProperties.get(reqDateTextField)).setStringValueData(reqDateTextField.getText());
			 ((TCProperty)imanProperties.get(typOfChangTextField)).setStringValueData(typOfChangTextField.getText());
			 ((TCProperty)imanProperties.get(reason4ChangeTextField)).setStringValueData(reason4ChangeTextField.getText());
			 ((TCProperty)imanProperties.get(chngDescTextArea)).setStringValueData(chngDescTextArea.getText());
			 //((TCProperty)imanProperties.get(apprGrpTextField)).setStringValueData(apprGrpTextField.getText());
			 ((TCProperty)imanProperties.get(apprTextField)).setStringValueData(apprTextField.getText());
			 ((TCProperty)imanProperties.get(apprDateTextField)).setStringValueData(apprDateTextField.getText());
			 
			 // Save distribution list
			 int listCount = ((DefaultListModel)emailDistrList.getModel()).getSize();
			 String[] strArr= new String[listCount];
			 for ( int i = 0; i < listCount; i++ )
		     {
				strArr[i]= ((DefaultListModel)emailDistrList.getModel()).getElementAt(i).toString();
		     }
			 ((TCProperty)imanProperties.get(emailDistrList)).setStringValueArrayData(strArr);
			 
			 ((TCProperty)imanProperties.get(commentsTextArea)).setStringValueData(commentsTextArea.getText());*/
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public boolean isFormSavable()
	{
		return true;
	}


}
