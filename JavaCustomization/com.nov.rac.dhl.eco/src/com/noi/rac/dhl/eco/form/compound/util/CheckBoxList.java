package com.noi.rac.dhl.eco.form.compound.util;

import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCSession;

/**
 * Constructs a selection list with Checkboxes
 */
public class CheckBoxList extends JPanel 
{
    private static final long serialVersionUID = 1L;
	private Map<JCheckBox, String> m_valueMap;
	private Map<String, JCheckBox> m_checkboxMap;
    private JPanel m_internalPanel;
    private	String	m_title;
    private	int	m_colCount;
   
    /**
     * Create the CheckBoxList UI.
     */
    public CheckBoxList() 
    {
    	
    }
    
    public CheckBoxList(String title, int colCount) 
    {    	
    	m_title = title;
    	m_colCount = colCount;
    	
    	m_valueMap = new HashMap<JCheckBox, String>();
    	m_checkboxMap = new HashMap<String, JCheckBox>();
    	TitledBorder tb = new TitledBorder(m_title);
    	m_internalPanel = new JPanel();
    	m_internalPanel.setBorder(tb);
    	m_internalPanel.setLayout( new GridLayout(0,m_colCount));
        add(m_internalPanel);
    }

    /**
     * Creates a panel with list of checkboxes with Display values as specified by the input parameter.
     *
     * @param box   the checkbox.
     * @param value the value bound to the checkbox.
     */
    public CheckBoxList(String[] strValues, String title, int colCount)
    {
    	this(title, colCount);    	
    	if(strValues != null && strValues.length>0)
    	{
    		for(int i = 0; i<strValues.length; i++)
    		{
    			JCheckBox box = new JCheckBox(strValues[i]);
    			addCheckBox(box, strValues[i]);
    		}
    	}
    }
    
    public CheckBoxList(TCSession session, String lovName)
    {
    	this(session, lovName, "", 1);
    }
    
   /* Creates a panel with list of checkboxes with Display values as specified by the input parameter.
    *
    * @param box   the checkbox.
    * @param value the value bound to the checkbox.
    */
    public CheckBoxList(TCSession session, String lovName, String title, int colCount)
    {
    	this(title, colCount);
    	TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName(session, lovName);
		if(lovValues != null)
		{
			ListOfValuesInfo lovInfo;
			try
			{
			    lovInfo = lovValues.getListOfValues();
			    String[] strValues = lovInfo.getStringListOfValues();
			    
			    if(strValues != null && strValues.length>0)
			   	{
			   		for(int i = 0; i<strValues.length; i++)
			   		{
			   			JCheckBox box = new JCheckBox(strValues[i]);
			   			addCheckBox(box, strValues[i]);
			   		}
			   	}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
   }
    
    /**
     * Add a checkbox with an associated value.
     *
     * @param box   the checkbox.
     * @param value the value bound to the checkbox.
     */
    public void addCheckBox(JCheckBox box, String value) 
    {
        m_internalPanel.add(box);
        m_valueMap.put(box, value);
        m_checkboxMap.put(value, box);
    }

    /**
     * Returns a list of selected checkbox values.
     *
     * @return list of selected checkbox values.
     */
    public Vector<String> getSelectedValues() 
    {
        Vector<String> vec = new Vector<String>();
        Iterator<JCheckBox> iter = m_valueMap.keySet().iterator();
        while (iter.hasNext()) {
            JCheckBox checkbox = (JCheckBox)iter.next();
            if (checkbox.isSelected()) {
                String value = (String)m_valueMap.get(checkbox);
                vec.add(value);
            }
        }
        return vec;
    }
    
    /**
     * Sets all the checkbox values to the specified value.
     *
     * @return None.
     */
    public void setSelectedValues(boolean val) 
    {
        Iterator<JCheckBox> iter = m_valueMap.keySet().iterator();
        while (iter.hasNext()) 
        {
            JCheckBox checkbox = (JCheckBox)iter.next();
            checkbox.setSelected(val);
        }
    }
    
    /**
     * Sets Selection to the checkbox values.
     *
     * @return None.
     */
    public void setSelection(Vector<String> lstSelection)
    {
    	Iterator<String> iter = lstSelection.iterator();
        while (iter.hasNext()) 
        {
            JCheckBox checkbox = m_checkboxMap.get(iter.next());            
            checkbox.setSelected(true);
        }
    }
    
    /**
     * Resets all the checkbox values.
     *
     * @return None.
     */
    public void reset() 
    {
    	setSelectedValues(false); 
    }
    
}