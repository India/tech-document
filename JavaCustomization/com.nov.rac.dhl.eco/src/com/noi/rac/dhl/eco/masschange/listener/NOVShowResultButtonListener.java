package com.noi.rac.dhl.eco.masschange.listener;

import java.io.IOException;

import javax.swing.SwingUtilities;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.util.components.ItemSearchDialog;
import com.teamcenter.rac.kernel.TCException;

/**
 * @author kabades
 * 
 */
public class NOVShowResultButtonListener implements SelectionListener
{
    
    private MassRDChangeComposite m_massRDChangeListener;
    
    public NOVShowResultButtonListener(MassRDChangeComposite massRDChangeComposite)
    {
        m_massRDChangeListener = massRDChangeComposite;
    }
    
    @Override
    public void widgetDefaultSelected(SelectionEvent selectionevent)
    {
        
    }
    
    @Override
    public void widgetSelected(SelectionEvent selectionevent)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                invokeDocSearchDialog(false);
            }
        });
        
    }
    
    private void invokeDocSearchDialog(boolean isRDSearch)
    {
        ItemSearchDialog searchDialog = null;
        try
        {
            m_massRDChangeListener.setValueForSelectedButton(true);
            searchDialog = new ItemSearchDialog(m_massRDChangeListener);
            searchDialog.setVisible(true);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
}
