package com.noi.rac.dhl.eco.util.components;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.validations.addecr.NOVCustomTable;
import com.nov.rac.dhl.eco.validations.addecr.NOVCustomTableLine;
import com.nov.rac.dhl.eco.validations.addecr.NOVItemSelectionDialog;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVCRSelectionDialog extends AbstractAIFDialog implements ComponentListener
{
    private static final long serialVersionUID = 1L;
    NOVCRSelectionPanel selectionPanel;
    // private Registry registry;
    private JButton addBtn;
    private JButton cancelBtn;
    private JList cRsList;
    private TCComponentForm[] crforms;
    private NOVECOTargetCRsListPanel ecoCRlistpanel;
    // private String[] typeStr = {"_DHL_EngChangeForm_"};
    // private String[] relation = {"_ECNECR_"};
    private JPanel crPanel;
	private NOVItemSelectionDialog m_itemSelectionDlg = null; 
	private Vector<TCComponent> m_targetECRs = null;
    public NOVCRSelectionDialog(NOVECOTargetCRsListPanel crlistpanel)
    {
        super(AIFUtility.getActiveDesktop());
        // registry = Registry.getRegistry(this);
        ecoCRlistpanel = crlistpanel;
        cRsList = crlistpanel.crsList;
        selectionPanel = new NOVCRSelectionPanel();
        selectionPanel.typeofChange = crlistpanel.ecoFormPanel.typeofChange;
        Object[] formObjs = new Object[cRsList.getModel().getSize()];
        
        for (int i = 0; i < cRsList.getModel().getSize(); i++)
        {
            formObjs[i] = cRsList.getModel().getElementAt(i);
        }
        
        crforms = Arrays.asList(formObjs).toArray(new TCComponentForm[formObjs.length]);
        
        if (crforms != null)
        {
            selectionPanel.populateSelectedCRTable(crforms);
        }
        
        // if (selectionPanel.typeofChange!=null)
        // {
        // Object[] objs = new Object[3];
        // objs[0] = "";
        // objs[1] = "Open";
        // objs[2] = selectionPanel.typeofChange;
        // selectionPanel.fireUserServiceCall("NATOIL_CRsNameQuery", objs);
        // }
        
        addBtn = new JButton("Update");
        addBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent actEvt)
            {
                sycnchronizeCRsItemList();
                ecoCRlistpanel.ecoFormPanel.reasonforChange.setText(selectionPanel.getReason4ChangeString());
                // ecoCRlistpanel.ecoFormPanel.additionalComments.setText(selectionPanel.getCRCommentString());//soma
                
                ecoCRlistpanel.ecoFormPanel.saveDatatoForm();
                // ecoCRlistpanel.ecoFormPanel.saveForm();//soma
				//NOVCRSelectionDialog.this.dispose();
            }
        });
        
        cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                NOVCRSelectionDialog.this.dispose();
            }
        });
        
        setLayout(new VerticalLayout(5,5,5,10,0));
        JPanel btnPanel = new JPanel();
        btnPanel.add(addBtn);
        btnPanel.add(cancelBtn);
        crPanel = new JPanel(new VerticalLayout());
        //crPanel.setPreferredSize(new Dimension(750, 600));// TCDECREL - 3382
        crPanel.add("top.bind.center.center",selectionPanel);
        crPanel.addComponentListener(this);
       /*  add("1.1.left.center", crPanel);
        add("2.1.center.center", btnPanel);*/
        add("top.bind.center.center", crPanel);
        add("bottom.bind.center.center", btnPanel);
		setMinimumSize(new Dimension(500, 600));
        setTitle("Add/Remove Change Request");
        pack();
        setResizable(true);// TCDECREL - 3382
        setModal(true);
        centerToScreen();
    }
    
    protected void sycnchronizeCRsItemList()
    {
        Object[] selCrs = selectionPanel.getSelectedCRs();
		
        Vector<TCComponentForm> initialCRs = new Vector<TCComponentForm>(Arrays.asList(crforms));
        try
        {
			TCComponent[] ecrComps = ecoCRlistpanel.ecoFormPanel.ecoForm.getTCProperty("_ECNECR_").getReferenceValueArray();
			m_targetECRs = new Vector<TCComponent>(Arrays.asList(ecrComps));
            Vector<TCComponentForm> newAddedCRs = new Vector<TCComponentForm>();
			//Compare initial crs and selected crs, get the difference (added crs) and fire the update 
            if ((selCrs != null) && (selCrs.length > 0))
            {
                Vector<TCComponentForm> selectedCRs = new Vector(Arrays.asList(selCrs));
                
                for (int i = 0; i < selectedCRs.size(); i++)
                {
                    if (!initialCRs.contains(selectedCRs.get(i)))
                    {
                        newAddedCRs.add(selectedCRs.get(i));
                    }
                }
                if(newAddedCRs.size()>0)
                {
                	NOVItemSelectionDialogWorker itemSelectionDLgWorker = new NOVItemSelectionDialogWorker(newAddedCRs);	
                	itemSelectionDLgWorker.execute();
                }
                else
                {
                	this.dispose();
                }
                // Updating to CRslist
                DefaultListModel listModel = (DefaultListModel) cRsList.getModel();
                listModel.clear();
                for (int i = 0; i < selCrs.length; i++)
                {
                    listModel.addElement(selCrs[i]);
                }
            }
            else
            {
                DefaultListModel listModel = (DefaultListModel) cRsList.getModel();
                listModel.clear();
				NOVCRSelectionDialog.this.dispose();
            }
            removeECRs();
            ecoCRlistpanel.ecoFormPanel.ecoForm.refresh();
            
			
      	}
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
	public void componentHidden(ComponentEvent e){}
    
	public void componentMoved(ComponentEvent e) {}
    
    public void componentResized(ComponentEvent e)
    {
        Dimension crPanelDim = crPanel.getSize();
        selectionPanel.setPreferredSize(crPanelDim);
    }
    
	public void componentShown(ComponentEvent e) {}
	
	public void addECRs(Vector<TCComponentForm> newAddedCRs)
    {
	    for (int i = 0; i < newAddedCRs.size(); i++) 
        {
            if (!m_targetECRs.contains(newAddedCRs.get(i))) 
            {
                try
                {
                 	ecoCRlistpanel.ecoFormPanel.ecoForm.add("_ECNECR_", newAddedCRs.get(i));
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                m_targetECRs.add(newAddedCRs.get(i));
             }
         }
     }
            
	public void removeECRs()
	{
    	// removed cr with items should remove from target list
        // selectionPanel.removedCrs
        Object[] removedCrs = selectionPanel.removedCrs.toArray();
        if (removedCrs != null && removedCrs.length > 0)
        {
        	for (int i = 0; i < removedCrs.length; i++)
            {
            	TCComponentForm crform = (TCComponentForm) removedCrs[i];
                AIFComponentContext[] comps = null;
                try
                {
                    comps = crform.whereReferenced();
                    
                    for (int j = 0; j < comps.length; j++)
                    {
                        if (comps[j].getContext().equals("RelatedECN"))
                        {
                            InterfaceAIFComponent infComp = comps[j].getComponent();
                            if (infComp instanceof TCComponentItemRevision)
                            {
                            	removePartFamily((TCComponentItemRevision)infComp);
                                ecoCRlistpanel.ecoFormPanel.removeRevisionsFromTarget((TCComponentItemRevision)infComp,null);
                            }
                        }
                        else if(comps[j].getContext().equals("_ECNECR_") && (comps[j].getComponent()==ecoCRlistpanel.ecoFormPanel.ecoForm))
                        {
                            if (m_targetECRs.contains(crform)) 
                            {
                                ecoCRlistpanel.ecoFormPanel.ecoForm.remove("_ECNECR_", crform);
                            }
                        }
                    }
                }
        		catch (TCException e)
        		{
                    // TODO Auto-generated catch block
            		e.printStackTrace();
        		}
    		}
        }
	}
	private void removePartFamily(TCComponentItemRevision comp)
	{
		Vector<TCComponentItemRevision> partFamily = ecoCRlistpanel.ecoFormPanel.targetItemsPanel.targetTable.getPartFamily(comp);
		for(int i=0;i<partFamily.size();i++)
		{
			ecoCRlistpanel.ecoFormPanel.removeRevisionsFromTarget(partFamily.get(i),null);
		}
	}
    
	class NOVItemSelectionDialogWorker extends SwingWorker<Integer, Integer> 
    {
	    private Vector<TCComponentForm> m_selectedECRs;
	    private int m_iRetCode = -1;
	    private int m_iNoOfRows = 0;
	    private NOVCustomTable m_itemSelectionTable = null;
	    private Vector<TCComponent> m_addedRevs = new Vector<TCComponent>();
	    public NOVItemSelectionDialogWorker(Vector<TCComponentForm> ECRs)
	    {
	        m_selectedECRs = ECRs;
    	}
		protected Integer doInBackground() throws Exception 
		{ 
			// Do a time-consuming task. 
	        
	    	NOVCRSelectionDialog.this.dispose();
    
	    	final Display display = Display.getDefault();
        	display.syncExec(new Runnable() 
    		{
        		public void run() 
            	{   
        			//7804-added typeofChange to NOVItemSelectionDialog constructor
            		m_itemSelectionDlg = new NOVItemSelectionDialog(display.getActiveShell(),ecoCRlistpanel.ecoFormPanel.typeofChange);
                    
                	m_itemSelectionDlg.create();              
                    
                	m_itemSelectionDlg.populateItemTable(m_selectedECRs.toArray());
                    
                	m_itemSelectionTable = m_itemSelectionDlg.getItemSelectionTable();
                	m_iNoOfRows = m_itemSelectionTable.getRowCount();
                    
                	if(m_iNoOfRows>0)
                	{
                		m_iRetCode = m_itemSelectionDlg.open();
                	}
             	}
          	});
	      	return m_iRetCode; 
    	}
    
	    protected void done() 
    	{
	        try 
	        {
                if(get() == Dialog.OK)
                {
                    for(int indx=0;indx<m_iNoOfRows;indx++)
                    {
                        boolean bChkBoxValue = (Boolean)m_itemSelectionTable.getValueAt(indx, 0);
                        if(bChkBoxValue)
                        {
                            AIFTableLine tableLine = m_itemSelectionTable.getRowLine(indx);
                            NOVCustomTableLine itemTableLine = (NOVCustomTableLine)tableLine;
                            TCComponent revObject = itemTableLine.getTableLineRevObject();
                           
                            if(!m_addedRevs.contains(revObject))
                            {
                            	m_addedRevs.add(revObject);
                            }
    					}
                    }
                                       
                    processSelectedObjs();
                }
                else if(get() == -1)
                {
                	if(m_selectedECRs.size()>0)
                	{                		
                		processSelectedObjs();
                	}
                }
                else //Cancel
                {
                	NOVCRSelectionDialog.this.setVisible(true);
                }
    
	        } 
	        catch (Exception e) 
    		{
	            e.printStackTrace(); 
	        } 
    	}
	    private void processSelectedObjs()
	    {
	    	final Display display = Display.getDefault();
	    	display.asyncExec(new Runnable() 
	    	{				
				@Override
				public void run() 
				{
					final AddOperation addOperation = new AddOperation(m_addedRevs,m_selectedECRs);
					final ProgressMonitorDialog addProgressDialog=new ProgressMonitorDialog(display.getActiveShell());

					try 
					{
						addProgressDialog.run(true, false, addOperation);
					} 
					catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
	    }
	} 
	private class AddOperation implements IRunnableWithProgress
	{
		Vector<TCComponentForm> m_selectedECRs;
		Vector<TCComponent> m_selectedRevs;
		public AddOperation(Vector<TCComponent> selectedRevs,Vector<TCComponentForm> selectedECRs) 
		{
			m_selectedRevs = selectedRevs;
			m_selectedECRs = selectedECRs;
		}

		@Override
		public void run(IProgressMonitor progressMonitor)
				throws InvocationTargetException, InterruptedException 
		{
			progressMonitor.beginTask("Editing ECO...", IProgressMonitor.UNKNOWN);
			String sErrorMsg=null;
			final IProgressMonitor finalprogressMonitor = progressMonitor;
			
			try 
			{
				//7804-added new function (addTargetsandCreateRevision) call for general type of ECO
				if(ecoCRlistpanel.ecoFormPanel.typeofChange.equalsIgnoreCase("General"))
				{
				    Vector<TCComponent> validComp = validateCompForMinorRev(m_selectedRevs); //8334
				    sErrorMsg = NOVECOHelper.addTargetsandCreateRevision("NOVECOTargetItemsPanel", ecoCRlistpanel.ecoFormPanel.ecoForm,
				                validComp.toArray(new TCComponent[validComp.size()]),
								m_selectedECRs.toArray(new TCComponent[m_selectedECRs.size()]),m_itemSelectionDlg.getRevType());
				}
				else
				{
					 sErrorMsg = NOVECOHelper.addTargetsToECO("NOVECOTargetItemsPanel", ecoCRlistpanel.ecoFormPanel.ecoForm,
						m_selectedRevs.toArray(new TCComponent[m_selectedRevs.size()]),
					    m_selectedECRs.toArray(new TCComponent[m_selectedECRs.size()]));
				}
				//7804-display error message if any validation fails
				if(sErrorMsg != null && sErrorMsg.trim().length()>0)
				{
					JOptionPane.showMessageDialog(NOVCRSelectionDialog.this, sErrorMsg, "Add Item", JOptionPane.WARNING_MESSAGE);
				}
				 ecoCRlistpanel.ecoFormPanel.ecoForm.refresh();
				finalprogressMonitor.done();
			} catch (Exception e) {
				e.printStackTrace();
				finalprogressMonitor.setCanceled(true);
				finalprogressMonitor.done();
			}
		}
		
		private Vector<TCComponent> validateCompForMinorRev(Vector<TCComponent> selComponent) throws TCException //8334
	    {
	        Vector<TCComponent> validComp = new Vector<TCComponent>();
	        Vector<TCComponent> invalidComp = new Vector<TCComponent>();
	        String sRevType = m_itemSelectionDlg.getRevType();
	        for(int i=0; i<selComponent.size();i++)
	        {
	            TCComponentItemRevision itemRev =  (TCComponentItemRevision)selComponent.get(i);
	            if(NOVECOHelper.isValidCompForMinorRev(itemRev, sRevType))
	            {
	                validComp.add(selComponent.get(i));
	            }
	            else
	            {
	                invalidComp.add(selComponent.get(i));
	            }
	        }
	        
	        if(invalidComp.size()>0)
	        {
	            StringBuilder msgStr = new StringBuilder();
	            msgStr.append("Below selected target(s) may have latest working revision. Cannot add as minor revision.\n");
	            for(int k=0;k<invalidComp.size();k++)
	            {
	                msgStr.append(invalidComp.get(k));
	                msgStr.append("\n");
	            }
	            JOptionPane.showMessageDialog(NOVCRSelectionDialog.this, msgStr, "Add Item", JOptionPane.WARNING_MESSAGE);
	        }
	        return validComp;
	    }
	}

}
