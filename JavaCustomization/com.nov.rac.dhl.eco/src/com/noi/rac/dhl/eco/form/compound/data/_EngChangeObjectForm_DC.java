package com.noi.rac.dhl.eco.form.compound.data;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.noi.rac.dhl.eco.form.compound.util.NOVCustomFormProperties;
import com.noi.rac.dhl.eco.util.components.NOVECODispositionPanel;
import com.noi.rac.dhl.eco.util.components.NOVECODistributionPanel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetsPanel;
import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel;
import com.noi.util.components.AbstractReferenceLOVList;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.iTextArea;

public class _EngChangeObjectForm_DC extends NOVCustomFormProperties
{
    public JTextField typeofChange;
    public JTextField workflowType;
    public JComboBox changeCat;
    public JComboBox groupCmb;
    public JComboBox reasonCode;
    public iTextArea reasonforChange;
    public iTextArea additionalComments;
    public JComboBox productLine;
    private JComboBox m_formDCECOClassification;// TCDECREL-4471
    public NOVECODispositionPanel dispPanel;
    //public NOVEnDistributionPanel distributionPanel;
    public NOVECODistributionPanel m_distributionPanel; //TCDECREL-6163
    public AbstractReferenceLOVList distSource;
    public AbstractReferenceLOVList distTarget;
    private TCPreferenceService prefService;
    private TCSession tcSession;
    
    String[] propertyNames = new String[] { "changetype", "wfname", "changecat", "group", "reasoncode", "distribution",
            "reasonforchange", "nov4_product_line", "nov4_ecoclassification", "additionalcomments",
            "ectargetdisposition" };
    
    public _EngChangeObjectForm_DC()
    {
        typeofChange = new JTextField(14);
        workflowType = new JTextField(14);
        changeCat = new JComboBox();
        groupCmb = new JComboBox();
        reasonCode = new JComboBox();
        reasonforChange = new iTextArea();
        additionalComments = new iTextArea();
        productLine = new JComboBox();
        m_formDCECOClassification = new JComboBox();// TCDECREL-4471
        
        addProperty("changetype", typeofChange);
        addProperty("wfname", workflowType);
        addProperty("additionalcomments", additionalComments);
        addProperty("reasonforchange", reasonforChange);
        addProperty("nov4_product_line", productLine);
        addProperty("nov4_ecoclassification", m_formDCECOClassification);// TCDECREL-4471
        
    }
    
    public String[] getPropertyNames()
    {
        return propertyNames;
    }
    
    public void setForm(TCComponent form)
    {
        if (form != null)
        {
            super.setForm(form);
            
            tcSession = form.getSession();
            prefService = tcSession.getPreferenceService();
            
            try
            {
                TCProperty targetdispProperty = form.getTCProperty("ectargetdisposition");
                
                dispPanel = new NOVECODispositionPanel(targetdispProperty.getReferenceValueArray());
                // NOVECOHelper.retriveLOVValues(form.getSession(),NOVECOConstants.ChangeCatagory_LOV,
                // changeCat);
                
                // NOVECOHelper.retriveLOVValues(form.getSession(),NOVECOConstants.ReasonCodeGenReq_LOV,
                // reasonCode);
                // NOVECOHelper.retriveLOVValues(form.getSession(),NOVECOConstants.DHL_ECR_Groups,
                // groupCmb);
                TCComponentGroup owningGroupComp = (TCComponentGroup) form.getTCProperty("owning_group")
                        .getReferenceValue();
                String owningGroup = owningGroupComp.toString();
                // String owningGroup =
                // form.getTCProperty("owning_group").getReferenceValue().toString();
                String groupLovName = NOVECOConstants.ECR_Groups_base + owningGroup.substring(0, 2);
                NOVECOHelper.retriveLOVValues(form.getSession(), groupLovName, groupCmb);
                
                //distributionPanel = new NOVEnDistributionPanel();
                m_distributionPanel = new NOVECODistributionPanel(); //6163
                // Code added for Distribution List -------
                String currentGroup = "";
                distSource = new AbstractReferenceLOVList();
                TCComponentGroup group = form.getSession().getCurrentGroup();
                // String groupname=group.getFullName();
                String groupname = owningGroupComp.getFullName();
                if (groupname.indexOf('.') >= 0)
                    currentGroup = groupname.substring(0, groupname.indexOf('.'));
                else
                    currentGroup = groupname;
                currentGroup = currentGroup + ":";
                
                distSource.initClassECNERO(form.getSession(), "ImanAliasList", "alName", currentGroup);
                distTarget = new AbstractReferenceLOVList();
                // Code added for Distribution List -------
                addProperty("ectargetdisposition", dispPanel);
                addProperty("changecat", changeCat);
                addProperty("group", groupCmb);
                addProperty("reasoncode", reasonCode);
                addProperty("distribution", m_distributionPanel); //6163
                
                super.getFormProperties(propertyNames);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    }
    
    public void clearForm()
    {
    }
    
    public void populateFormComponents()
    {
        if (getForm() == null)
            return;
        
        typeofChange.setText(((TCProperty) imanProperties.get(typeofChange)).getStringValue());
        workflowType.setText(((TCProperty) imanProperties.get(workflowType)).getStringValue());
        reasonforChange.setText(((TCProperty) imanProperties.get(reasonforChange)).getStringValue());
        additionalComments.setText(((TCProperty) imanProperties.get(additionalComments)).getStringValue());
        
        NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ProductLine_LOV, productLine);
        productLine.setSelectedItem(((TCProperty) imanProperties.get(productLine)).getStringValue());
        // TCDECREL-4471-start
        NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ECOCLASSIFICATION_LOV,
                m_formDCECOClassification);
        m_formDCECOClassification.setSelectedItem(((TCProperty) imanProperties.get(m_formDCECOClassification))
                .getStringValue());// TCDECREL-4471-end
        
        String changeCatStr = ((TCProperty) imanProperties.get(changeCat)).getStringValue();
        
        /* Rakesh:130212 - Populate Change Category from LOVs - Start here */
        /*
         * if (changeCatStr.equalsIgnoreCase("Lifecycle Change")) {
         * changeCat.removeAllItems(); changeCat.addItem(changeCatStr); } else {
         * changeCat.setSelectedItem(changeCatStr); }
         */
        loadChangeCategory();
        changeCat.setSelectedItem(changeCatStr);
        /* Rakesh:130212 - Populate Change Category from LOVs - Ends here */

        groupCmb.setSelectedItem(((TCProperty) imanProperties.get(groupCmb)).getStringValue());
        loadReasonCode();
        reasonCode.setSelectedItem(((TCProperty) imanProperties.get(reasonCode)).getStringValue());
        
        String[] arr1 = ((TCProperty) imanProperties.get(m_distributionPanel)).getStringValueArray();//6163
        for (int i = 0; i < arr1.length; i++)
        {
            distTarget.addItem(arr1[i], arr1[i]);
            distSource.addExclusion(arr1[i]);
        }
        //distributionPanel.init(distSource, distTarget, new java.awt.Dimension(250, 100));
        m_distributionPanel.init(distTarget, new java.awt.Dimension(250, 100)); //TCDECREL-6163
        
    }
    
    /* Rakesh:130212 - Populate Change Category from LOVs - Start here */
    private void loadChangeCategory()
    {
        try
        {
            String owningGroup = getForm().getTCProperty("owning_group").getReferenceValue().toString().substring(0, 2);
            String typeOfChange = ((TCProperty) imanProperties.get(typeofChange)).getStringValue();
            String wf = workflowType.getText();
            
            String changeCatPref = "ECO_Workflows" + "." + owningGroup + "." + typeOfChange + "."
                    + wf.substring(wf.indexOf(" ") + 1) + "." + "category";
            String[] changeCatList = prefService.getStringArray(prefService.TC_preference_all, changeCatPref);
            changeCat.removeAllItems();
            changeCat.addItem("");
            for (int i = 0; i < changeCatList.length; i++)
            {
                changeCat.addItem(changeCatList[i]);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    /* Rakesh:130212 - Populate Change Category from LOVs - Ends here */

    public void loadReasonCode()
    {
        String selChgCat = (String) changeCat.getSelectedItem();
        reasonCode.removeAllItems();
        if (selChgCat.equalsIgnoreCase("Documentation"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeDoc_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("General Request"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeGenReq_LOV, reasonCode);
        }
        /*
         * else if(selChgCat.equalsIgnoreCase("Lane - Bitzip")) {
         * NOVECOHelper.retriveLOVValues
         * (getForm().getSession(),NOVECOConstants.ReasonCodeBitZip_LOV,
         * reasonCode); }
         */
        else if (selChgCat.equalsIgnoreCase("Lane - Bitzip")
                && workflowType.getText().indexOf("RH Type 1 Release") != -1)
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane1_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Lane - Bitzip")
                && workflowType.getText().indexOf("RH Type 2 Release") != -1)
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane2_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Lane - Bitzip")
                && workflowType.getText().indexOf("RH Type 3 Release") != -1)
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane3_LOV, reasonCode);
        }
        // @swamy else if(selChgCat.equalsIgnoreCase("Lane 1 Request"))
        else if (selChgCat.equalsIgnoreCase("Lane 1 Request/SBR"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane1_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Lane 2 Request"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane2_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Lane 3 Request"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane3_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Lane 4 Request"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLane4_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Lifecycle Change"))
        {
            reasonCode.removeAllItems();
            reasonCode.addItem("Lifecycle Change");
        }
        /* Rakesh:130212 - Load Reason code for new Groups 1B/9A - Starts here */
        else if (selChgCat.equalsIgnoreCase("Lifecycle"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeLifecycle_LOV, reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Item Revision"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeItemRevision_LOV,
                    reasonCode);
        }
        else if (selChgCat.equalsIgnoreCase("Item Creation"))
        {
            NOVECOHelper.retriveLOVValues(getForm().getSession(), NOVECOConstants.ReasonCodeItemCreation_LOV,
                    reasonCode);
        }
        /* Rakesh:130212 - Load Reason code for new Groups 1B/9A - Ends here */
    }
    
    public void saveOnlyDispFormData()
    {
        try
        {
            TCProperty[] arr = null;
            
            // disposition data
            TCComponent[] dispcomps = dispPanel.getDispData();
            if (dispcomps != null && dispPanel.isEnabled())
            {
                ((TCProperty) imanProperties.get(dispPanel)).setReferenceValueArrayData(dispcomps);
            }
            
            ((TCProperty) imanProperties.get(reasonforChange)).setStringValueData(reasonforChange.getText());
            ((TCProperty) imanProperties.get(additionalComments)).setStringValueData(additionalComments.getText());// soma
            ((TCProperty) imanProperties.get(productLine)).setStringValueData(productLine.getSelectedItem().toString());
            ((TCProperty) imanProperties.get(m_formDCECOClassification)).setStringValueData(m_formDCECOClassification
                    .getSelectedItem().toString());// TCDECREL-4471
            
            arr = enabledProperty(arr, reasonforChange);
            arr = enabledProperty(arr, dispPanel);
            arr = enabledProperty(arr, additionalComments);
            arr = enabledProperty(arr, productLine);
            arr = enabledProperty(arr, m_formDCECOClassification);// TCDECREL-4471
            
            getForm().setTCProperties(arr);
            getForm().save();
            dispPanel.deleteOrphanedDispInstances();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public void saveFormData()
    {
        try
        {
            // ((TCProperty)imanProperties.get(reasonforChange)).setStringValueData(reasonforChange.getText());
            ((TCProperty) imanProperties.get(additionalComments)).setStringValueData(additionalComments.getText());
            
            ((TCProperty) imanProperties.get(productLine)).setStringValueData(productLine.getSelectedItem().toString());
            ((TCProperty) imanProperties.get(m_formDCECOClassification)).setStringValueData(m_formDCECOClassification
                    .getSelectedItem().toString());// TCDECREL-4471
            
            /*
             * Object changeCatObj = changeCat.getSelectedItem(); if
             * (changeCatObj!=null) {
             * ((TCProperty)imanProperties.get(changeCat))
             * .setStringValueData(changeCatObj.toString()); }
             */
            Object groupObj = groupCmb.getSelectedItem();
            if (groupObj != null)
            {
                ((TCProperty) imanProperties.get(groupCmb)).setStringValueData(groupObj.toString());
            }
            
            Object reasonCodeObj = reasonCode.getSelectedItem();
            if (reasonCodeObj != null)
            {
                ((TCProperty) imanProperties.get(reasonCode)).setStringValueData(reasonCodeObj.toString());
            }
            
            TCComponent[] dispcomps = dispPanel.getDispData();
            dispPanel.saveDispData();
            if (dispcomps != null && dispPanel.isEnabled())
            {
                ((TCProperty) imanProperties.get(dispPanel)).setReferenceValueArrayData(dispcomps);
            }
            ((TCProperty) imanProperties.get(m_distributionPanel)).setStringValueArrayData(m_distributionPanel  //6163
                    .getAllTargetListObjects());
            
            TCProperty[] arr = null;
            // arr = enabledProperty(arr,typeofChange);
            // arr = enabledProperty(arr,workflowType);
            arr = enabledProperty(arr, reasonforChange);
            arr = enabledProperty(arr, additionalComments);
            arr = enabledProperty(arr, productLine);
            arr = enabledProperty(arr, m_formDCECOClassification);// TCDECREL-4471
            // arr = enabledProperty(arr,changeCat);
            arr = enabledProperty(arr, groupCmb);
            arr = enabledProperty(arr, m_distributionPanel);//6163
            arr = enabledProperty(arr, reasonCode);
            arr = enabledProperty(arr, dispPanel);
            getForm().setTCProperties(arr);
            getForm().save();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public boolean isFormSavable()
    {
        return true;
    }
    
}
