package com.noi.rac.dhl.eco.util.components;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentICO;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.ics.ICSKeyLov;
import com.teamcenter.rac.kernel.ics.ICSProperty;
import com.teamcenter.rac.kernel.ics.ICSPropertyDescription;
import com.teamcenter.rac.util.PropertyLayout;


public class NOVViewClassificationDialog extends AbstractAIFDialog 
{
	private static final long serialVersionUID = 1L;
	private final JPanel m_contentPanel = new JPanel();
	private	TCComponentItemRevision m_itemRev;
	private	boolean	m_classAttribDefined = false;
	
	/**
	 * Create the dialog.
	 */
	public NOVViewClassificationDialog(TCComponent targetItemRev) 
	{
		super(AIFUtility.getActiveDesktop(), true);	
		m_itemRev = (TCComponentItemRevision)targetItemRev;
		createUI();
	}
	
	public void createUI()
	{
		JPanel classPanel = createClassifyPanel();
		m_contentPanel.add(classPanel);
		
		getContentPane().setLayout(new BorderLayout());		
		getContentPane().add(m_contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) 
					{
						NOVViewClassificationDialog.this.dispose();						
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		pack();
		setTitle("Classification Info");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(true);		
		setModal(true);
	}
	
	public JPanel createClassifyPanel()
	{
		final int txtWidth = 100;
		final int txtHeight = 20;
		JPanel classPanel = new JPanel(new PropertyLayout(0,2,5,5,5,5));
		classPanel.setBorder(BorderFactory.createBevelBorder(1));	
		try 
		{
			Map<String, String> classAttrs = new HashMap<String,String>();
			TCComponentICO[] icoObjs = ( ( TCComponentItemRevision ) m_itemRev ).getClassificationObjects();
			if (icoObjs == null || icoObjs.length<1)
			{
				return null;
			}
			TCComponentICO icoObj = icoObjs[0];
			ICSProperty[] icsProperties = icoObj.getICSProperties(true);
			Map<String, String> attrIDToValueMap = new HashMap<String,String>();
			
			for (ICSProperty icsProperty: icsProperties)
			{
				attrIDToValueMap.put(icsProperty.getId()+"",icsProperty.getValue());
			
				// get attribute values
				ICSPropertyDescription[] cp = icoObj.getICSPropertyDescriptors(); 

				for (int i=0;i<cp.length;i++)
				{
					try
					{
						String attrDatatype = cp[i].getFormat().getDisplayString();
						//boolean isMandatory = cp[i].isMandatory();
						String attrDefaultAnnotation = cp[i].getAnnotation();
						ICSKeyLov lov = cp[i].getFormat().getKeyLov();
						String attrName = cp[i].getName();
						String attrID = cp[i].getId()+"";
						String key = (String)attrIDToValueMap.get(attrID);
						String attrValue = "";
						if (attrName.equalsIgnoreCase("SHORT NAME"))
						{
							continue;
						}
						if (lov != null) 
						{
							attrValue = lov.getValueOfKey(key);
						}

						else 
						{
							attrValue = (String)attrIDToValueMap.get(attrID);
							if (attrDatatype.contains("INTEGER") ||attrDatatype.contains("REAL") || attrDatatype.contains("DOUBLE"))
							{
								while (attrValue != null && attrValue.endsWith("0"))
								{
									attrValue = attrValue.substring(0,attrValue.length()-1);
								}

								if (attrValue != null && (attrValue.length() > 0) && attrValue.charAt(attrValue.length()-1) == '.')
								{
									attrValue = attrValue.substring(0,attrValue.length()-1);
								}

								while (attrValue != null && attrValue.startsWith("0", 0) && attrValue.length() > 1)
								{
									attrValue = attrValue.substring(1);
								}
							}
						}

						if (attrValue == null)
						{
							attrValue = "";
						}
						if (attrDefaultAnnotation != null && !attrDefaultAnnotation.equals("") && attrValue != null && attrValue.length() > 0 )
						{
							attrValue = attrDefaultAnnotation+" "+attrValue;
						}
						
						System.out.println("attrName="+attrName);
						System.out.println("attrValue="+attrValue);
						classAttrs.put(attrName, attrValue);								
					}
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			}
			Set<String> keySet = classAttrs.keySet();
			Object[] keyObjs =  keySet.toArray();
			if(keyObjs != null && keyObjs.length > 0)
			{
				m_classAttribDefined = true;
			}
			else
			{
				m_classAttribDefined = false;
			}
			
			for (int i = 0; i < keyObjs.length; i++) 
			{
				Object valObj = classAttrs.get(keyObjs[i]);	
				JLabel lblKey = new JLabel(keyObjs[i].toString());
				JTextField lblTxt = new JTextField();
				lblTxt.setPreferredSize(new Dimension(txtWidth, txtHeight));
				lblTxt.setEnabled(false);
				lblTxt.setText(valObj.toString());
				
				String row = Integer.toString(i+1);
				classPanel.add(row+".1.left.center",lblKey);
				classPanel.add(row+".2.left.center",lblTxt);
			}
		}
			
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		return classPanel;
	}
	
	public boolean isClassAttribDefined()
	{
		return m_classAttribDefined;
	}
}
