package com.noi.rac.dhl.eco.masschange.operations;

import java.util.List;
import java.util.Map;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.masschange.utils.NOVRDChangeHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.soa.client.model.ErrorStack;

/**
 * @author tripathim
 * 
 */
public class NOVRemoveRDOperation extends AbstractMassRDChangeOperation
{
    
    private TCComponent m_removeRDComp;
    
    public NOVRemoveRDOperation(MassRDChangeComposite massRDChangeComposite, TCComponent[] selectedComps,
            String mmrRevType)
    {
        super(massRDChangeComposite, selectedComps, mmrRevType);
        m_removeRDComp = massRDChangeComposite.getSelectedRD();
    }
    
    @Override
    public void performRDEditOperation()
    {
        try
        {
            Map<TCComponent, String> compsWithRevIdMap = NOVRDChangeHelper.fillAllComponentsWithRevid(m_selectedComps,
                    m_mmrRevType);
            List<ErrorStack> errorsStack = NOVRDChangeHelper.reviseObject(compsWithRevIdMap);
            if (!(errorsStack.size() > 0))
            {
                removeRD();
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    private void removeRD() throws TCException
    {
        DeleteObjectHelper[] removeRelationObjHelperArr = NOVRDChangeHelper.getRemoveRDRelationObjects(m_selectedComps,
                m_removeRDComp);
        CreateObjectsSOAHelper.deleteRelationObjects(removeRelationObjHelperArr);
    }
    
}
