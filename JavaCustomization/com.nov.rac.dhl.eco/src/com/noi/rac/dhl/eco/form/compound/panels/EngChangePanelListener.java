package com.noi.rac.dhl.eco.form.compound.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.noi.rac.dhl.eco.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.en.customize.util.MessageBox;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentDataset;

public class EngChangePanelListener implements ActionListener
{
	
     /* Class Level variables*/
	
	private INOVCustomFormProperties m_novFormProperties;	
	
	public EngChangePanelListener( INOVCustomFormProperties novFormProperties )
	{
		super();
		m_novFormProperties = novFormProperties;	
		
	}

	
	public void actionPerformed(ActionEvent e) 
	{
            try 
            {
            	AIFComponentContext[] comps = null;
//            	if(m_novFormProperties.getForm().getType().equalsIgnoreCase("_DHL_EngChangeForm_"))
//            		
//            		comps = m_novFormProperties.getForm().getPrimary();
//            	
//            	else
            		 //comps = m_novFormProperties.getForm().getSecondary();
            		 comps =  m_novFormProperties.getForm().getRelated("_generatedattachment_");
            		 
            		
            		 System.out.println("No of secondary " + comps.length);
                boolean isPDFAttached = false;
                if (comps!=null && comps.length>0) 
                {
                    for (int i = 0; i < comps.length; i++) 
                    {
                        InterfaceAIFComponent compToOpen = comps[i].getComponent();
                        System.out.println("secondary Type " + comps[i].getComponent().toString());
                        if( compToOpen instanceof TCComponentDataset)
                        {
                            System.out.println(((TCComponentDataset)compToOpen).getType());
                            if(((TCComponentDataset)compToOpen).getType().equals("PDF"))
                            {
                                AbstractAIFCommand abstractaifcommand = m_novFormProperties.getForm().getSession()
                                .getOpenCommand(new Object[]{ AIFUtility.getActiveDesktop(), compToOpen });
                                abstractaifcommand.executeModeless();
                                isPDFAttached = true;
                            }
                        }
                    }
                }
                if (!isPDFAttached) 
                {
                    MessageBox.post(null, "info", "No PDF Attached to "+m_novFormProperties.getForm().toString(), MessageBox.INFORMATION);    
                }
            }
            catch (Exception ex) 
            {
                ex.printStackTrace();
            }
        
		
		
	}
	
	
	
	

}
