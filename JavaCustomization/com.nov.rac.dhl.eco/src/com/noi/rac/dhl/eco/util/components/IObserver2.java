/**
 * 
 */
package com.noi.rac.dhl.eco.util.components;

/**
 * @author deys
 *
 */
public interface IObserver2 
{
	public void update(ISubject2 obj, Object arg);
}
