package com.noi.rac.dhl.eco.masschange.utils;

import java.util.Arrays;
import java.util.Vector;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

/**
 * @author kabades
 * 
 */
public class NOVMassRDEditValidator
{
    private MassRDChangeComposite m_massRDComposite;
    private String m_errorMessage;
    
    public NOVMassRDEditValidator(MassRDChangeComposite massRDComposite)
    {
        this.m_massRDComposite = massRDComposite;
    }
    
    public boolean validateUI()
    {
        if (m_massRDComposite.getSelectedRD() == null)
        {
            m_errorMessage = NOVRDMessages.getString("SelectRD.MESSAGE");
            displayErrorMessage();
            return false;
        }
        if (m_massRDComposite.getSelectedWhereUsedItem().length == 0)
        {
            m_errorMessage = NOVRDMessages.getString("SelectWhereUsedItems.MESSAGE");
            displayErrorMessage();
            return false;
        }
        if (m_massRDComposite.getActionCammand() == null || m_massRDComposite.getActionCammand().isEmpty())
        {
            m_errorMessage = NOVRDMessages.getString("SelectRadioButton.MESSAGE");
            displayErrorMessage();
            return false;
        }
        if ((m_massRDComposite.getActionCammand().equalsIgnoreCase("ReplaceRD") || m_massRDComposite.getActionCammand()
                .equalsIgnoreCase("AddRD")) && m_massRDComposite.getReplacementRD() == null)
        {
            m_errorMessage = NOVRDMessages.getString("SelectReplaceOrADDRadioButton.MESSAGE");
            displayErrorMessage();
            return false;
        }
        return true;
    }
    
    private void displayErrorMessage()
    {
        MessageBox.post(m_massRDComposite.getShell(), m_errorMessage, "Warning", MessageBox.WARNING);
    }
}
