package com.noi.rac.dhl.eco.util.components;

public interface ISubject {
	public void notifyObserver();
	public void registerObserver(IObserver subs);
	public void removeObserver(IObserver subs);

}
