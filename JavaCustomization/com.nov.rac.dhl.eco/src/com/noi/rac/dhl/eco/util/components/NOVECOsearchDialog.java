package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Component;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.quicksearch.services.utils.Utils;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.validations.additem.NOVECRSelectionDialog;
import com.nov.rac.dhl.eco.validations.additem.NOVECRTable;
import com.nov.rac.dhl.eco.validations.additem.NOVECRTableLine;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

/**
 * Implementation for Dialog to search ECO
 * @author Akhilesh
 *
 */
public class NOVECOsearchDialog extends AbstractSWTDialog implements INOVSearchProvider{

	protected Shell shell;
	NOVECOMoveTargetDialog novecoMoveTargetDialog;
	protected 	String  m_sPrdtLine  = null;
	protected 	String  m_sGroup     	= null;
	protected 	String  m_sChangeType     	= null;
	protected 	String  m_sWrkFlow		= null;
	protected 	String  m_sChngCat		= null;
	protected 	String  m_sReasonCode		= null;
	protected 	String 	m_sCreatedAfter= null;
	protected 	String 	m_sCreatedBefore= null;
	protected 	String 	m_sReleasedAfter= null;
	protected 	String 	m_sReleaseStatus= null;
	protected 	String 	m_sReleasedBefore= null;
	protected 	String  m_sFormName=null;
	protected 	String  m_sOwningUser=null;
	protected 	String  m_sOwningGroup=null;
	protected  String  m_sRevisionType=null;

	private Combo m_chngTypCmb;
	private Combo m_grpCmb;
	private Text m_txtECONumber;
	private Combo m_prdtLineCmb;
	private Combo m_revCmb;

	private TCSession m_tcSession;
	private TCPreferenceService m_prefService;
	private String m_sChngType;
	private String m_sTargetItem=null;

	private TCTable searchTbl;

	String[] objTYPE= null;
	Map<String , String>  mapIndex= new HashMap<String , String>();

	protected Registry reg = Registry.getRegistry(this);
	public TCComponent targetForm;
	protected TCComponent m_currentECO;
	private TCComponent[] m_compItem = null;
	private Vector<TCComponent> 	m_ecrVector  = new Vector<TCComponent>();
	private boolean m_bValidECO = false;
	private Vector<TCComponent> 	m_selectedRevs  = new Vector<TCComponent>();
	private Vector<TCComponent> 	m_selectedECRs  = new Vector<TCComponent>();
	private Map<TCComponent,Vector<TCComponent>> m_itemRevCRMap = new HashMap<TCComponent,Vector<TCComponent>>();

	/**
	 * @param parentShell - Parent Shell
	 * @throws TCException
	 * @throws IOException
	 * @wbp.parser.constructor
	 */
	public NOVECOsearchDialog(Shell parentShell)
						throws TCException, IOException {
		super(parentShell);
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE
				| SWT.APPLICATION_MODAL);
		m_tcSession = (TCSession) AIFUtility.getDefaultSession();
		m_prefService = m_tcSession.getPreferenceService();
		m_currentECO =null;
	}

	public void setCompItem(TCComponent[] compItem)
	{
		this.m_compItem = compItem;
	}

	/**
	 * @param moveDlg - NOVECOMoveTargetDialog object
	 * @param changeType - Change Type of ECO.
	 * @throws TCException
	 * @throws IOException
	 */
	public NOVECOsearchDialog(NOVECOMoveTargetDialog moveDlg, String changeType)
						throws TCException, IOException
	{
		super(moveDlg.getShell());
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE
				| SWT.APPLICATION_MODAL);
		m_tcSession = (TCSession) AIFUtility.getDefaultSession();
		m_prefService = m_tcSession.getPreferenceService();
		m_sChngType = changeType;
		novecoMoveTargetDialog = moveDlg;
		m_currentECO = novecoMoveTargetDialog.ecoFormPnl.ecoForm;
	}

	@Override
	protected Control createContents(Composite parent)
	{
		parent.getShell().setText("Search ECO");
		parent.setLayout(new GridLayout(1, false));
		GridData gd_parent = new GridData(SWT.FILL , SWT.FILL, true, true, 1, 1);
		gd_parent.widthHint = 550;
		gd_parent.heightHint = 600;
		parent.setLayoutData(gd_parent);

		//composite to set criterion for ECO search
		Composite searchECOComp = new Composite(parent, SWT.NONE);
		GridData gdSearchComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gdSearchComposite.heightHint = 80;//120
		gdSearchComposite.widthHint = 500;
		searchECOComp.setLayoutData(gdSearchComposite);
		GridLayout glSearchLayout = new GridLayout(4, false);
		glSearchLayout.horizontalSpacing = 20;
		glSearchLayout.verticalSpacing = 20;
		glSearchLayout.marginBottom = 10;
		searchECOComp.setLayout(glSearchLayout);


		Label lblECONumber = new Label(searchECOComp,SWT.NONE);
		GridData gdLblECO = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblECO.widthHint = 70;
		gdLblECO.heightHint = 15;
		lblECONumber.setLayoutData(gdLblECO);
		lblECONumber.setText("ECO Number");

		m_txtECONumber = new Text(searchECOComp, SWT.BORDER);
		GridData gdTxtECO = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdTxtECO.widthHint = 100;
		gdTxtECO.heightHint = 15;
		m_txtECONumber.setLayoutData(gdTxtECO);
		m_txtECONumber.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent event)
			{
				if (event.character == '\r')
				{
					searchPopulate();
				}
			}
		});

		Label lblProductLine = new Label(searchECOComp, SWT.NONE);
		GridData gdLblProductLine = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblProductLine.heightHint = 15;
		gdLblProductLine.widthHint = 80;
		lblProductLine.setLayoutData(gdLblProductLine);
		lblProductLine.setText("Product Line");

		m_prdtLineCmb = new Combo(searchECOComp, SWT.NONE);
		GridData gdPrdtLineCmb = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdPrdtLineCmb.heightHint = 15;
		gdPrdtLineCmb.widthHint = 90;
		m_prdtLineCmb.setLayoutData(gdPrdtLineCmb);

		Label lblGroup = new Label(searchECOComp, SWT.NONE);
		GridData gdLblGroup = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblGroup.widthHint = 80;
		lblGroup.setLayoutData(gdLblGroup);
		lblGroup.setText("Group");

		m_grpCmb = new Combo(searchECOComp, SWT.NONE);
		GridData gdGrpCmb = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gdGrpCmb.widthHint = 90;
		m_grpCmb.setLayoutData(gdGrpCmb);

		Label lblChangeType = new Label(searchECOComp, SWT.NONE);
		GridData gdLblChangeType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdLblChangeType.widthHint = 80;
		lblChangeType.setLayoutData(gdLblChangeType);
		lblChangeType.setText("Change Type");

		m_chngTypCmb = new Combo(searchECOComp, SWT.NONE);
		GridData gdChngTypCmb = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gdChngTypCmb.widthHint = 90;
		m_chngTypCmb.setLayoutData(gdChngTypCmb);

		//7805:Start
		Composite btnContainer = new Composite(parent, SWT.NONE);
		GridData gd_btnContainer = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
        gd_btnContainer.heightHint = 40;
        gd_btnContainer.widthHint = 400;
        gd_btnContainer.horizontalIndent = 60;
        btnContainer.setLayoutData(gd_btnContainer);
        GridLayout btnLayout = new GridLayout(3, false);
        btnLayout.horizontalSpacing = 20;
        btnLayout.verticalSpacing = 10;
        btnLayout.marginBottom = 10;
        btnContainer.setLayout(btnLayout);
        
        if(m_currentECO == null)
        {
    		m_revCmb = new Combo(btnContainer, SWT.READ_ONLY); 
    		m_revCmb.add("Create Major Revision");
    		m_revCmb.add("Create Minor Revision");
    		m_revCmb.select(0);
            GridData gdRevCmb = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
            gdRevCmb.widthHint = 115;
            m_revCmb.setLayoutData(gdRevCmb);
        }
        
		Button btnReset = new Button(btnContainer, SWT.NONE);
		GridData gdReset = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gdReset.widthHint = 55;
		gdReset.horizontalIndent = 80;
		btnReset.setLayoutData(gdReset);
		btnReset.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				m_txtECONumber.setText("");
				m_prdtLineCmb.deselectAll();
				m_grpCmb.deselectAll();
				if(m_revCmb != null)//7805
				    m_revCmb.select(0);
				if (m_currentECO == null)
				{
					m_chngTypCmb.deselectAll();
				}
				searchTbl.removeAllRows();

			}
		});
		//btnReset.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnReset.setText("Reset");

		Image searchIcon = reg.getImage("search.ICON");
		Button btnSearch = new Button(btnContainer, SWT.NONE);
		btnSearch.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		btnSearch.setText("Search");
		btnSearch.setImage(searchIcon);

		btnSearch.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				searchPopulate();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
			}
		});

		//retrieve Product Line data
		retriveLOVValues(m_tcSession,NOVECOConstants.ProductLine_LOV, m_prdtLineCmb);

		//retrieve Group data
		String groupPrefName = NOVECOConstants.ECR_Groups_base + m_tcSession.getCurrentGroup().toString().substring(0, 2);
		retriveLOVValues(m_tcSession,groupPrefName, m_grpCmb);

		//retrieve Type of Change for ECO and set it to combo field
		String typeOfChangePrefName = "ECO_Workflows" + "." + m_tcSession.getCurrentGroup().toString().substring(0, 2);
		String[] typeOfChangeList = m_prefService.getStringArray(TCPreferenceService.TC_preference_all, typeOfChangePrefName );
		//TCDECREL-3196 : Start : Akhilesh
		if (m_sChngType == null)
		{
			m_sChngType="";
			m_chngTypCmb.add("");
			for(int i=0;i<typeOfChangeList.length;i++)
			{
				m_chngTypCmb.add(typeOfChangeList[i]);
			}
		}
		else
		{
			for(int i=0;i<typeOfChangeList.length;i++)
			{
				if(typeOfChangeList[i].equalsIgnoreCase(m_sChngType))
				{
					m_sChngType=typeOfChangeList[i];
				}
			}
			m_chngTypCmb.setText(m_sChngType);
			m_chngTypCmb.setEnabled(false);
		}
		//TCDECREL-3196 : End

		//composite for ECO search Table
		Composite seachResContainer = new Composite(parent, SWT.NONE);
		seachResContainer.setLayout(new FillLayout(SWT.HORIZONTAL));
		GridData gd_seachResContainer = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_seachResContainer.heightHint = 100;
		gd_seachResContainer.widthHint = 450;
		seachResContainer.setLayoutData(gd_seachResContainer);

		String[] attrNames = {"ECO Name","Product Line","Group","Change Category"};
		AIFTableModel serachTableMdl = new AIFTableModel(attrNames);

		searchTbl = new TCTable();
		searchTbl.setModel(serachTableMdl);
		searchTbl.setSession(m_tcSession);

		searchTbl.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		searchTbl.getTableHeader().setReorderingAllowed(false);
		searchTbl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		if(m_currentECO!=null)
		searchTbl.getColumnModel().getColumn(0).setCellRenderer(new CustomTableCellRenderer());
		JTableHeader head = searchTbl.getTableHeader();
		TableCellRenderer renderer = head.getDefaultRenderer();
		JLabel label = (JLabel) renderer;
		label.setHorizontalAlignment(JLabel.CENTER);
		//TCDECREL-7807: Implemented listner for showing and hiding mmr combo box on selection type
		searchTbl.addSelectionChangedListener(new ISelectionChangedListener()
        {
            @Override
            public void selectionChanged(SelectionChangedEvent arg0)
            {
                TCTable table = (TCTable)arg0.getSource();
                String changeType = (String ) (table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 3));
                if( m_revCmb != null)
                {
                    if(changeType.equals("Lifecycle Change")){
                        m_revCmb.setVisible(false);
                    }
                    else{
                        m_revCmb.setVisible(true);
                    }
                }
            }
        });
		//TCDECREL-7807:End
		JScrollPane searchResPane = new JScrollPane(searchTbl);

		SWTUIUtilities.embed(seachResContainer, searchResPane,true);

		//composite for Buttons
		Composite compositeBttn = new Composite(parent, SWT.EMBEDDED);
		GridLayout glCompositebtn = new GridLayout(2, false);
		glCompositebtn.marginRight = 5;
		glCompositebtn.marginTop = 5;
		glCompositebtn.marginBottom = 10;
		compositeBttn.setLayout(glCompositebtn);
		GridData gdCompositeBtn = new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1);
		gdCompositeBtn.heightHint = 40;
		compositeBttn.setLayoutData(gdCompositeBtn);

		Button btnOk = new Button(compositeBttn, SWT.NONE);
		GridData gd_btnOk = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnOk.widthHint = 55;
		btnOk.setLayoutData(gd_btnOk);
		btnOk.setText(reg.getString("OK"));

		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				//validate selected ECO and set it as Target ECO
				validateSelectedECO();
				if(m_bValidECO)
				{
					if (m_currentECO != null)
					{
						novecoMoveTargetDialog.targetECO =  (TCComponentForm) targetForm;
						getShell().dispose();
					}
					else
					{
						addItemsToECO();
						//8922-start
						try
                        {
                            targetForm.refresh();
                        }
                        catch (TCException e)
                        {
                            
                            e.printStackTrace();
                        }
                        //8922-end
					}
					//getShell().dispose();
				}

			}

		});

		Button btnCancel = new Button(compositeBttn, SWT.NONE);
		GridData gdCancelBtn = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdCancelBtn.widthHint = 55;
		btnCancel.setLayoutData(gdCancelBtn);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				getShell().dispose();
			}
		});
		btnCancel.setText(reg.getString("Cancel"));
		getShell().setMinimumSize(550, 500);
		return parent;
	}

	/**
	 * Validate Selected ECO and set it as Target ECO in MoveECO dialog
	 */
	protected void validateSelectedECO()
	{
		TCComponent selcomp = null;
		searchTbl.getUpdateSelectionOnSort();
		int selIndex = searchTbl.getSelectedRow();

		if (selIndex != -1)
		{
			String itemSelected = (String) searchTbl.getValueAt(selIndex, 0);
			String itemPUID = mapIndex.get(itemSelected);
			try
			{
				selcomp = m_tcSession.stringToComponent(itemPUID);
			} catch (TCException e)
			{
				e.printStackTrace();
			}

			if (selcomp instanceof TCComponentForm)
			{
				targetForm = selcomp;
				TCReservationService reserserv = selcomp.getSession().getReservationService();

				try
				{
					TCComponentProcess currentProcess = NOVECOHelper.getCurrentProcess((TCComponentForm)selcomp);
					if(currentProcess != null)
					{
						//checking check-out status of target ECO
						if (reserserv.isReserved(selcomp))
						{
							//TCDECREL-3196 : Start
							String userName = null;
							String owningUser = targetForm.getTCProperty("checked_out_user").toString();
							if (m_currentECO!=null)
							{
								userName = m_currentECO.getTCProperty("checked_out_user").toString();
								if (owningUser.equals(userName))
								{
									m_bValidECO=true;
								}
							}
							else
							{
								userName = m_tcSession.getUserName();
								if (owningUser.contains(userName))
								{
									m_bValidECO=true;
								}
							}
							if (!m_bValidECO)
							{
								displayMsgBox(getShell(), SWT.ICON_INFORMATION, targetForm.toString()+" is already checked out by other user");
							}
						}
						else
						{
							m_bValidECO=true;
						}
					}
					else
					{
						m_bValidECO = false;
						String sMsg = targetForm.toString() +" "+ reg.getString("InvalidECO.MSG");
						displayMsgBox(getShell(), SWT.ICON_INFORMATION, sMsg);
					}
				}
				catch (TCException e)
				{
					e.printStackTrace();
				}
			}
		}


	}

	/**
	 *
	 * Validate UI Input field Data
	 */
	protected boolean validateInput()
	{
		m_sPrdtLine = "";
		m_sOwningGroup = "";
		m_sChangeType = "";
		m_sWrkFlow = "";
		m_sChngCat = "";
		m_sReasonCode = "";
		m_sFormName = "";

		m_sFormName = m_txtECONumber.getText();
		m_sPrdtLine = m_prdtLineCmb.getText();
		m_sOwningGroup = m_grpCmb.getText();
		m_sChangeType = m_chngTypCmb.getText();

		return true;
	}

	/**
	 * Populate the search result
	 */
	protected void searchPopulate()
	{
		boolean isValidated = false;
		//validate search input values
		isValidated = validateInput();

		if (isValidated)
		{
			INOVSearchProvider searchService = NOVECOsearchDialog.this;
			INOVSearchResult searchResult = null;
			INOVResultSet theResSet = null;
			try
			{
				searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL);
				if (searchResult != null)
				{
					theResSet = (INOVResultSet) searchResult.getResultSet();
				}
			}
			catch (Exception e)
			  {
			     e.printStackTrace();
			  }
			searchTbl.removeAllRows();

			if (searchResult != null && searchResult.getResponse().nRows > 0)
			{
				System.out.println("Found"+ searchResult.getResponse().nRows + "objects");
				for (int i = 0; i < searchResult.getResponse().nRows; i++)
				{
					Vector<String> rowData = new Vector<String>();

					String objPUID = theResSet.getRowData().elementAt(6 * i + 0);
					String objNAME = theResSet.getRowData().elementAt(6 * i + 1);
					String rev = theResSet.getRowData().elementAt(6 * i + 2);
					String chngCat = theResSet.getRowData().elementAt(6 * i + 3);
					String group = theResSet.getRowData().elementAt(6 * i + 4);

					rowData.add(objNAME);
					rowData.add(rev);
					rowData.add(group);
					rowData.add(chngCat);

					searchTbl.addRow(rowData);
					mapIndex.put(objNAME,objPUID);
				}
			}
		}
	}

	/**
	 * Retrieve LOV value for properties
	 * @param tcSession - default session
	 * @param lovName - Name of LOV
	 * @param comboBox - Combo box to be fill with LOV values
	 */
	public static void retriveLOVValues(TCSession tcSession, String lovName, Combo comboBox)
	{
		try
        {
            TCComponentListOfValues lovValues = TCComponentListOfValuesType.findLOVByName(tcSession, lovName);
            ListOfValuesInfo lovInfo = lovValues.getListOfValues();
            Object[] lovS = lovInfo.getListOfValues();
            if (lovS.length > 0 )
            {
            	comboBox.add("");
                for ( int i = 0; i < lovS.length; i++ )
                {
                	comboBox.add(lovS[i].toString());
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
	}

	@Override
	public QuickBindVariable[] getBindVariables()
	{
		if(m_sFormName == null || m_sFormName.isEmpty())
			m_sFormName = "*";

		if (m_sPrdtLine == null || m_sPrdtLine.isEmpty())
			m_sPrdtLine = "*";

		if (m_sGroup == null || m_sGroup.isEmpty())
			m_sGroup = "*";

		if (m_sChangeType == null || m_sChangeType.isEmpty())
			m_sChangeType = "*";

		if (m_sWrkFlow == null || m_sWrkFlow.isEmpty())
			m_sWrkFlow = "*";

		if (m_sChngCat == null || m_sChngCat.isEmpty())
			m_sChngCat = "*";

		if (m_sReasonCode == null || m_sReasonCode.isEmpty())
			m_sReasonCode = "*";

		if (m_sCreatedAfter == null && m_sCreatedBefore == null )
		{
			m_sCreatedAfter = "*";
			m_sCreatedBefore = "*";
		}

		if (m_sReleasedAfter == null && m_sReleasedBefore == null )
		{
			m_sReleasedAfter = "*";
			m_sReleasedBefore = "*";
		}

		if (m_sReleaseStatus == null || m_sReleaseStatus.isEmpty() )
		{
			m_sReleaseStatus = "";			//TCDECREL-3592
		}
		if(m_sOwningUser == null)
			m_sOwningUser = "*";

		if (m_sOwningUser != null &&
				(m_sOwningUser.isEmpty()|| m_sOwningUser.trim().equalsIgnoreCase(""))){
			m_sOwningUser = "*";
		}
		if (m_sTargetItem == null || m_sTargetItem.isEmpty() )
		{
			m_sTargetItem = "*";
		}

		if (m_sOwningGroup != null &&
				(m_sOwningGroup.isEmpty()|| m_sOwningGroup.trim().equalsIgnoreCase(""))){
			m_sOwningGroup = "*";
		}

		String strGroupPUID = Utils.getCurrentGroupPUID();

		QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[12];

		bindVars[0] = new QuickSearchService.QuickBindVariable();
		bindVars[0].nVarType = POM_string;
		bindVars[0].nVarSize = 1;
		bindVars[0].strList = new String[]{m_sFormName};

		bindVars[1] = new QuickSearchService.QuickBindVariable();
		bindVars[1].nVarType = POM_string;
		bindVars[1].nVarSize = 1;
		bindVars[1].strList = new String[]{m_sChangeType};

		bindVars[2] = new QuickSearchService.QuickBindVariable();
		bindVars[2].nVarType = POM_string;
		bindVars[2].nVarSize = 1;
		bindVars[2].strList = new String[]{m_sChngCat};

		bindVars[3] = new QuickSearchService.QuickBindVariable();
		bindVars[3].nVarType = POM_string;
		bindVars[3].nVarSize = 1;
		bindVars[3].strList = new String[]{m_sReasonCode};

		bindVars[4] = new QuickSearchService.QuickBindVariable();
		bindVars[4].nVarType = POM_date;
		bindVars[4].nVarSize = 2;
		bindVars[4].strList = new String[]{m_sCreatedAfter,m_sCreatedBefore};

		bindVars[5] = new QuickSearchService.QuickBindVariable();
		bindVars[5].nVarType = POM_date;
		bindVars[5].nVarSize = 2;
		bindVars[5].strList = new String[]{m_sReleasedAfter,m_sReleasedBefore};

		bindVars[6] = new QuickSearchService.QuickBindVariable();
		bindVars[6].nVarType = POM_string;
		bindVars[6].nVarSize = 1;
		bindVars[6].strList = new String[]{m_sOwningUser};

		bindVars[7] = new QuickSearchService.QuickBindVariable();
		bindVars[7].nVarType = POM_string;
		bindVars[7].nVarSize = 1;
		bindVars[7].strList = new String[]{m_sTargetItem};

		bindVars[8] = new QuickSearchService.QuickBindVariable();
		bindVars[8].nVarType = POM_string;
		StringTokenizer stringTokenizer = new StringTokenizer(m_sOwningGroup, ",");
		Vector<String> vec = new Vector<String>();
		while(stringTokenizer.hasMoreTokens())
		{
			vec.add(stringTokenizer.nextToken());
		}
		bindVars[8].nVarSize = vec.size();
		String[] s_arr = new String[1];
		s_arr = vec.toArray(s_arr);
		bindVars[8].strList = s_arr;

		bindVars[9] = new QuickSearchService.QuickBindVariable();
		bindVars[9].nVarType = POM_string;
		bindVars[9].nVarSize = 1;
		bindVars[9].strList = new String[]{m_sReleaseStatus};

		bindVars[10] = new QuickSearchService.QuickBindVariable();
		bindVars[10].nVarType = POM_string;
		bindVars[10].nVarSize = 1;
		bindVars[10].strList = new String[]{m_sPrdtLine};

		bindVars[11] = new QuickSearchService.QuickBindVariable();
		bindVars[11].nVarType = POM_typed_reference;
		bindVars[11].nVarSize = 1;
		bindVars[11].strList = new String[]{strGroupPUID};

		return bindVars;
	}

	@Override
	public String getBusinessObject()
	{
		return "_DHL_EngChangeForm_";
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo() {
		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];

		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-dnholechangeforms";
		allHandlerInfo[0].listBindIndex = new int[] { 1,2,3,4,5,6,7,8,9,10,11,12};
		allHandlerInfo[0].listReqdColumnIndex = new int[] { 1,13,8,10,14};
		allHandlerInfo[0].listInsertAtIndex = new int[] { 1,2, 3,4,5};

		return allHandlerInfo;
	}

	/**
	 * Function to display Message box with given message and style
	 * @param shellObj - parent shell of Message Box
	 * @param style -
	 * @param errmessage - Message to be displayed
	 */
	public void displayMsgBox(Shell shellObj,int style,String errmessage)
	{
		MessageBox messageBox = new MessageBox(shellObj, style);
		messageBox.setText(reg.getString("Search ECO"));
		messageBox.setMessage(errmessage);
		messageBox.open();
	}

	class CustomTableCellRenderer extends JLabel implements TableCellRenderer
	{
		private static final long serialVersionUID = 1L;


	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
	            boolean hasFocus, int row, int column)
	   {
		  //this.setIcon(TCTypeRenderer.getTypeIcon(objTYPE[row], "Item"));
		  this.setText(value.toString());
		  if(value.toString().equalsIgnoreCase(m_currentECO.toString()))
		  {
			  setBackground(Color.GRAY);
			  this.setBackground(null);
			  this.setForeground(Color.GRAY);
		  }else if(isSelected)
		  {
			  setOpaque(true);
			  this.setBackground(table.getSelectionBackground());
		      this.setForeground(table.getSelectionForeground());
		  }
		  else
		  {
			  this.setBackground(null);
			  this.setForeground(null);
		  }
	      return this;
	   }
	}

	//TCDECREL-3196 : Start
	/**
	 * Add seleted items/item revisions to searched ECO.
	 */
	private void addItemsToECO()
	{
		boolean isValidRevAdded = false;
		TCComponentForm targetECO = null;

		TCPreferenceService prefService = m_tcSession.getPreferenceService();
		String addComponentToTargetSOA = prefService.getString(TCPreferenceService.TC_preference_site,
											NOVECOHelper.m_sPrefAddCompToTargetSOA);
		m_itemRevCRMap.clear();
		m_selectedRevs.clear();
		m_selectedECRs.clear();
        m_sRevisionType = m_revCmb.getText(); //7807
        Vector<TCComponent> invalidComp = new Vector<TCComponent>();
		if (addComponentToTargetSOA.compareToIgnoreCase("Y") == 0 )
		{
			try
			{
				for(int i=0, l=m_compItem.length; i<l; i++)
				{
					TCComponentItemRevision selcompItemRev = null;
					if (m_compItem[i] instanceof TCComponentItem)
					{
						selcompItemRev = ((TCComponentItem)m_compItem[i]).getLatestItemRevision();
					}
					else
					{
						selcompItemRev = (TCComponentItemRevision)m_compItem[i];
					}
					if(!NOVECOHelper.isValidCompForMinorRev(selcompItemRev, m_sRevisionType))//8334
                    {
                        invalidComp.add(m_compItem[i]);
                        continue;
                    }
					AIFComponentContext[] compConxt =  getChangeRequestContextObjects(selcompItemRev.getRelated("RelatedECN"));

			        /*if (compConxt.length>1)
					{
						if (m_ecrVector != null && m_ecrVector.size() > 0)
						{
			                //MultiSelectCRItemDialog crItemdlg = new MultiSelectCRItemDialog( selcompItemRev,m_ecrVector,true,targetECO);
							//crItemdlg.setVisible(true);

			                m_itemRevCRMap.put(selcompItemRev,m_ecrVector);
						}
						else
						{
			                selCompVect.add(m_compItem[i]);
						}
					}
					else
					{
			            selCompVect.add(m_compItem[i]);
			        }*/
			        if (m_ecrVector != null && m_ecrVector.size() > 0)
		            {
			        	m_itemRevCRMap.put(selcompItemRev,m_ecrVector);
					}
			        else
			        {
			        	m_selectedRevs.add(selcompItemRev);
			        }
				}
			}
			catch (TCException e1)
			{
				e1.printStackTrace();
			}
			if(invalidComp.size() > 0)//8334
            {
                StringBuilder msgStr = new StringBuilder();
                msgStr.append("Below selected item(s) may have latest working revision. Cannot add as minor revision.\n");
                for(int k=0;k<invalidComp.size();k++)
                {
                    msgStr.append(invalidComp.get(k));
                    msgStr.append("\n");
                }
                com.teamcenter.rac.util.MessageBox.post(this.getShell(), msgStr.toString(), "Add Item...",
                                                            com.teamcenter.rac.util.MessageBox.WARNING);
            }
			if(targetForm!=null)
			{
				targetECO = (TCComponentForm) targetForm;

				if(m_itemRevCRMap.values().size() > 0)
                {
				    //close();
				    launchECRSelectionDlg(targetECO);
                }
				else
				{
					close();
				}
				int iNoOfSelectedRevs = m_selectedRevs.size();
				if(iNoOfSelectedRevs>0)
				{
					final AddOperation addOperation = new AddOperation(targetECO);
					final Display display = PlatformUI.getWorkbench().getDisplay();
					final ProgressMonitorDialog addProgressDialog=new ProgressMonitorDialog(display.getActiveShell());

					try
					{
						addProgressDialog.run(true, false, addOperation);
					}
					catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				//isValidRevAdded = NOVECOHelper.addComponentToTarget("NOVECOTargetItemsPanel", targetECO, selCompArray);
			}
		}
	}
	private void launchECRSelectionDlg(TCComponentForm targetECO)
	{
		boolean bIsECRSelected = false;
	    NOVECRSelectionDialog ecrSelectionDlg = new NOVECRSelectionDialog(this.getShell());
        ecrSelectionDlg.create();

        String sChangeType = "";
        try
        {
            sChangeType = targetECO.getTCProperty("changetype").getStringValue();
        }
        catch (TCException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        ecrSelectionDlg.populateECRTable(sChangeType,m_itemRevCRMap);

        NOVECRTable ecrSelectionTable = ecrSelectionDlg.getECRSelectionTable();
        int iNoOfRows = ecrSelectionTable.getRowCount();

        int iRetCode = ecrSelectionDlg.open();

        if(iRetCode == Dialog.OK)
        {
        	close();
            for(int indx=0;indx<iNoOfRows;indx++)
            {
                boolean bChkBoxValue = (Boolean)ecrSelectionTable.getValueAt(indx, 0);
                if(bChkBoxValue)
                {
                	bIsECRSelected = true;
                    AIFTableLine tableLine = ecrSelectionTable.getRowLine(indx);
                    NOVECRTableLine ecrTableLine = (NOVECRTableLine)tableLine;
                    TCComponent formObject = ecrTableLine.getTableLineFormObject();
                    TCComponent revObject = ecrTableLine.getTableLineRevObject();
                    if(!m_selectedRevs.contains(revObject))
                    {
                    	m_selectedRevs.add(revObject);
                    }
                    if(!m_selectedECRs.contains(formObject))
                    {
                    	m_selectedECRs.add(formObject);
                    }

                }
            }
            if(bIsECRSelected == false)
            {
            	m_selectedRevs.addAll(m_itemRevCRMap.keySet());
            }
        }
	}

	/**
	 * Get CR for item revision
	 * @param compConxt
	 * @return
	 */
	private AIFComponentContext[] getChangeRequestContextObjects(AIFComponentContext[] compConxt)
	{
		Vector<AIFComponentContext> compCxtArr = new Vector<AIFComponentContext>();
		m_ecrVector = new Vector<TCComponent>();
		if (compConxt.length>0)
		{
			for (int i = 0; i < compConxt.length; i++)
			{
				InterfaceAIFComponent infComp = compConxt[i].getComponent();
				if (infComp.getType().equalsIgnoreCase("_EngChangeRequest_"))
				{
					compCxtArr.add(compConxt[i]);
					TCComponentForm ecrform = (TCComponentForm) infComp;
					String statuses;
					try
					{
						statuses = ((TCComponentForm)infComp).getProperty("release_statuses");
						if (statuses.contains("Open"))
						{
							m_ecrVector.add(ecrform);
						}
					} catch (TCException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return compCxtArr.toArray(new AIFComponentContext[compCxtArr.size()]);
	}

	/**
	 * Purpose - This class provide implementation for progress monitor dialog.
	 * @author GuptaA2
	 *
	 */
	private class AddOperation implements IRunnableWithProgress
	{
		private TCComponentForm m_TargetForm =null;

		public AddOperation(TCComponentForm targetECO)
		{
			m_TargetForm = targetECO;
		}

		@Override
		public void run(IProgressMonitor progressMonitor)
				throws InvocationTargetException, InterruptedException
		{
			progressMonitor.beginTask("Adding Item(s) to Target..", IProgressMonitor.UNKNOWN);
			final IProgressMonitor finalprogressMonitor = progressMonitor;

			try
			{
				final String sMsg = NOVECOHelper.addTargetsandCreateRevision("NOVECOTargetItemsPanel", m_TargetForm,
																m_selectedRevs.toArray(new TCComponent[m_selectedRevs.size()]),
																m_selectedECRs.toArray(new TCComponent[m_selectedECRs.size()]),
																m_sRevisionType); //7805


				finalprogressMonitor.done();
				if(sMsg!=null && sMsg.trim().length()>0)
				{
					final Display display = Display.getDefault();
					display.asyncExec(new Runnable()
					{
						@Override
						public void run()
						{
							com.teamcenter.rac.util.MessageBox.post(display.getActiveShell(),sMsg,"Add Item...",com.teamcenter.rac.util.MessageBox.INFORMATION);
						}
					});

				}
			} catch (Exception e) {
				e.printStackTrace();
				finalprogressMonitor.setCanceled(true);
				finalprogressMonitor.done();
			}
		}
	}
	//TCDECREL-3196 : End
}
