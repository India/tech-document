package com.noi.rac.dhl.eco.util.components;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;


public class ECOWizardItemTable extends JTreeTable {

	private static final long serialVersionUID = 1L;
	private NOVECOTreeTableModel treeTableModel;
	public Set<TCComponent> targetItems ;
	public Set<String> strTargetItemIds ;
	public Vector<TCComponent> targetCRs ;
	public Map<String, String> itemwithRevTypeMap;
	public Vector<TCComponent> removedCRs ;
	public Map<String,Vector<TCComponent>> itemRevCRMap;
	
	public ECOWizardItemTable(TCSession session) 
	{
		super(session);
		targetItems = new HashSet<TCComponent>();
		strTargetItemIds = new HashSet<String>();
		targetCRs = new Vector<TCComponent>();
		removedCRs = new Vector<TCComponent>();
		itemwithRevTypeMap=new HashMap<String, String>();//7804
		itemRevCRMap =  new HashMap<String,Vector<TCComponent>>();
		String[] columnNames = {"Item Number","ECR","Type","Rev", "Lifecycle","Name","Description"};
		treeTableModel = new NOVECOTreeTableModel(columnNames);
		setModel(treeTableModel);
		
		for(int i = 0; i < columnNames.length; i++)
		{
			TableColumn col = new TableColumn(i);
			addColumn(col);
		}
		
		getTree().setCellRenderer(new TargetIdRenderer(this));
		getColumnModel().getColumn(1).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(2).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(3).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(4).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(5).setCellRenderer(new RootCellRenderer());
		getColumnModel().getColumn(6).setCellRenderer(new RootCellRenderer());
		
	}
	
	public void removeTarget(TCComponentItemRevision nItemRev,TCComponentForm nCrForm)
	{
		int childcnt = ((TreeTableNode)treeTableModel.getRoot()).getChildCount();
		
		for (int i = 0; i < childcnt; i++) 
		{
			NovTreeTableNode treenode = (NovTreeTableNode)((TreeTableNode)treeTableModel.getRoot()).getChildAt(i);
			
			if ((treenode.itemRev == nItemRev )&&(treenode.crForm == nCrForm)) 
			{
				removeTarget(treenode);
				break;
			}
		}
	}
	
	public void removeTarget(TCComponentItemRevision itemRev)
	{
		int childcnt = ((TreeTableNode)treeTableModel.getRoot()).getChildCount();
		
		for (int i = 0; i < childcnt; i++) 
		{
			NovTreeTableNode treenode = (NovTreeTableNode)((TreeTableNode)treeTableModel.getRoot()).getChildAt(i);
			
			if (treenode.itemRev == itemRev ) 
			{
				removeTarget(treenode);
				break;
			}
		}
	}
	
	public void removeAllTargets()
	{
		int childcnt = ((TreeTableNode)treeTableModel.getRoot()).getChildCount();
		
		for (int i = 0; i < childcnt; i++) 
		{
			NovTreeTableNode treenode = (NovTreeTableNode)((TreeTableNode)treeTableModel.getRoot()).getChildAt(0);
			removeTarget(treenode);
		}
		
		if (childcnt>0) 
		{
			updateUI();
			itemRevCRMap.clear();
			removedCRs.removeAllElements();
			strTargetItemIds.clear();
			targetCRs.removeAllElements();
			targetItems.clear();	
		}
		
	}
	
	public void removeTarget(NovTreeTableNode nodeToRemove)
	{
		TCComponentItemRevision nItemRev = nodeToRemove.itemRev;
		TCComponentForm nCrForm = nodeToRemove.crForm;
		
		try 
		{
			if (nCrForm!=null) 
			{
				String crStr =  nItemRev.getProperty("object_string")+"::"+nCrForm.getProperty("object_name");
				removedCRs.add(nCrForm);
				itemRevCRMap.remove(crStr);
				
				removeTargetItems(nItemRev);
				
				targetCRs.remove(nCrForm);
			}
			else
			{
				removeTargetItems(nItemRev);
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		final NovTreeTableNode node = nodeToRemove;
		if(SwingUtilities.isEventDispatchThread())
		{
			((TreeTableNode)treeTableModel.getRoot()).remove(node);
		}
		else
		{
			SwingUtilities.invokeLater(new Runnable() 
			{			
				@Override
				public void run() 
				{
					((TreeTableNode)treeTableModel.getRoot()).remove(node);
				}
			});
		}
	}
	private void removeTargetItems(TCComponentItemRevision nItemRev) throws TCException  //KLOC -127
	{
		String itemId = nItemRev.getProperty("item_id");
		targetItems.remove(nItemRev);				
		strTargetItemIds.remove(itemId);
		AIFComponentContext contextRDD[] = nItemRev.getRelated("RelatedDefiningDocument");
		//7804-added a condition not to allow it for minor revisions
		if(!nItemRev.getProperty("current_revision_id").contains("."))
		{
			for(int i = 0; i < contextRDD.length; i++)
			{
				TCComponentItem rddcomp=(TCComponentItem)contextRDD[i].getComponent();
				if (isRDDRefsinOtherPart(rddcomp,itemId)) 
				{
					TCComponentItemRevision rddRev = NOVECOHelper.getItemRevisionofRevID(rddcomp, nItemRev.getProperty("current_revision_id"));
					targetItems.remove(rddRev);
					strTargetItemIds.remove(rddRev.getProperty("item_id"));	
				}
			}
		}
	}
	
	
	private boolean isRDDRefsinOtherPart(TCComponentItem rddcomp, String itemId) 
	{
		try 
		{
			AIFComponentContext rddParts[] = rddcomp.whereReferenced();
			
			for (int i = 0; i < rddParts.length; i++) 
			{
				InterfaceAIFComponent infComp =  rddParts[i].getComponent();
				
				if (infComp instanceof TCComponentItemRevision) 
				{
					String infItemId = infComp.getProperty("item_id");
					if (infItemId.equals(itemId)) 
					{
						continue;
					}
					else if(strTargetItemIds.contains(infItemId)) 
					{
						return true;
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	//7804-start
	
    public Map<String, String> getItemWithRevTypeMap()
    {
    	return itemwithRevTypeMap;
    }
	public boolean addTargetsToItemsTable(TCComponentItemRevision itemRev,TCComponentForm crForm,String revType)
	{
		try
		{
			//String itemtype=((TCComponentItemRevision)itemRev).getTCProperty("object_type").toString();
			if(itemRev!=null)
			{
				String crStr = addCR(itemRev, crForm);
				NovTreeTableNode node1 = new NovTreeTableNode((TCComponentItemRevision)itemRev,crForm);
				String itemId=itemRev.getProperty("item_id");//7804
				itemwithRevTypeMap.put(itemId, revType);//7804
				AIFComponentContext contextRDD[] = itemRev.getRelated("RelatedDefiningDocument");
				//7804-added a check to verify the revision type
				if(revType.equalsIgnoreCase("Create Major Revision"))
				{
					for(int i = 0; i < contextRDD.length; i++)
					{
						InterfaceAIFComponent infComp = contextRDD[i].getComponent();
						if (infComp.getType().equals("Documents")) 
						{
							TCComponentItem rddcomp=(TCComponentItem)contextRDD[i].getComponent();
							String rddRevId=rddcomp.getLatestItemRevision().getProperty("current_revision_id");//7804-test
							TCComponentItemRevision rddRev = NOVECOHelper.getItemRevision(rddcomp, /*RevId*/rddRevId );//7804-commented passing item revision id , instead, given rdd item id
							//if(enformPanel.Entargets.contains(rddRev))
							//7161-start
							//String rddRevision=rddRev.getProperty("current_revision_id");
							String rddReleaseStatus=rddRev.getProperty("release_status_list");
							String itemReleaseStatus=itemRev.getProperty("release_status_list");
							String rddItemId=rddRev.getProperty("item_id");
							//7804-commented revisions comparison as its not needed for mmr
							if(!(/*RevId.equalsIgnoreCase(rddRevision) &&*/ rddReleaseStatus.contains("Released") && !itemReleaseStatus.contains("Released")) )
						    {
								//7161-added if condition
								if(((rddRev.getProperty("process_stage_list")== null) || (rddRev.getProperty("process_stage_list" ).equalsIgnoreCase(""))))
								{
								NovTreeTableNode nodeRDD = new NovTreeTableNode(rddRev,crForm);
								node1.add(nodeRDD);
								itemwithRevTypeMap.put(rddItemId, revType);//7804
								}
							}	
						}
						else
						{
							if (crForm!=null)
							{
								if (itemRevCRMap.containsKey(crStr)) 
								{
									itemRevCRMap.remove(crStr);
								}
								if (targetCRs.contains(crForm)) 
								{
									targetCRs.remove(crForm);
								}
							}
							return false;
						}
					}
				}
				targetItems.add(itemRev);
				strTargetItemIds.add(itemRev.getProperty("item_id"));
				treeTableModel.addRoot(node1);		
				updateTableUI();
				return true;
			}			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	private String addCR(TCComponentItemRevision itemRev, TCComponentForm crForm) throws TCException //KLOC -126 & 128
	{
		String crStr = null;
		if (crForm!=null) 
		{
			String ecrStatus = crForm.getProperty("ecr_status");	
			if(!(ecrStatus.equalsIgnoreCase("Closed")))
			{
				crStr = itemRev.getProperty("object_string")+"::"+crForm.getProperty("object_name");
				Vector<TCComponent> tarCompVec = new Vector<TCComponent>();
				tarCompVec.add(0, itemRev);
				tarCompVec.add(1, crForm);
				itemRevCRMap.put(crStr, tarCompVec);
				targetCRs.add(crForm);
			}
		}
		return crStr;
	}
	
	private void updateTableUI() throws InterruptedException, InvocationTargetException //KLOC -126 & 128
	{
		if (SwingUtilities.isEventDispatchThread()) 
		{
			updateUI();
		}
		else
		{
		    SwingUtilities.invokeAndWait(new Runnable()
		    {                    
		        @Override
		        public void run()
		        {
		            updateUI();
		        }
		    });
		}
	}
	//7804-end

	public boolean addTarget(TCComponentItemRevision itemRev,TCComponentForm crForm)
	{
		try
		{
			//String itemtype=((TCComponentItemRevision)itemRev).getTCProperty("object_type").toString();
			if(itemRev!=null)
			{
				String crStr = null;
				if (crForm!=null) 
				{
					/*TCDECREL-2436 - check if the ECR status is closed, then dont add those ECRS to eco*/
					String ecrStatus ;
					ecrStatus = crForm.getProperty("ecr_status");	
					if(!(ecrStatus.equalsIgnoreCase("Closed")))
					{
						crStr = itemRev.getProperty("object_string")+"::"+crForm.getProperty("object_name");
						Vector<TCComponent> tarCompVec = new Vector<TCComponent>();
						tarCompVec.add(0, itemRev);
						tarCompVec.add(1, crForm);
						itemRevCRMap.put(crStr, tarCompVec);
						targetCRs.add(crForm);
					}
				}
				NovTreeTableNode node1 = new NovTreeTableNode((TCComponentItemRevision)itemRev,crForm);
				String RevId=itemRev.getProperty("current_revision_id");
				AIFComponentContext contextRDD[] = itemRev.getRelated("RelatedDefiningDocument");
				for(int i = 0; i < contextRDD.length; i++)
				{
					InterfaceAIFComponent infComp = contextRDD[i].getComponent();
					if (infComp.getType().equals("Documents")) 
					{
						TCComponentItem rddcomp=(TCComponentItem)contextRDD[i].getComponent();
						String rddRevId=rddcomp.getLatestItemRevision().getProperty("current_revision_id");//7804
						TCComponentItemRevision rddRev = NOVECOHelper.getItemRevision(rddcomp, /*RevId*/rddRevId );//7804-commented passing item revision id , instead, given rdd item id
						
						//if(enformPanel.Entargets.contains(rddRev))
						//7161-start
						String rddRevision=rddRev.getProperty("current_revision_id");
						String rddReleaseStatus=rddRev.getProperty("release_status_list");
						String itemReleaseStatus=itemRev.getProperty("release_status_list");
						
						if(!(RevId.equalsIgnoreCase(rddRevision) && rddReleaseStatus.contains("Released") && !itemReleaseStatus.contains("Released")) )
					    {
							//7161-added if condition
							if((rddRev.getProperty("process_stage_list")== null) || (rddRev.getProperty("process_stage_list" ).equalsIgnoreCase("")))
							{
							NovTreeTableNode nodeRDD = new NovTreeTableNode(rddRev,crForm);
							node1.add(nodeRDD);
							}
								//Nataraj : Commented to avoid revisioning of item twice 
								//targetItems.add(rddRev);
								//strTargetItemIds.add(rddRev.getProperty("item_id"));
							//}
							
						}	
					}
					else
					{
						if (crForm!=null)
						{
							if (itemRevCRMap.containsKey(crStr)) 
							{
								itemRevCRMap.remove(crStr);
							}
							if (targetCRs.contains(crForm)) 
							{
								targetCRs.remove(crForm);
							}
						}
						return false;
					}
				}
				targetItems.add(itemRev);
				strTargetItemIds.add(itemRev.getProperty("item_id"));
				treeTableModel.addRoot(node1);		
				updateTableUI();
				
				return true;
			}			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}
	
	
	public class NOVECOTreeTableModel extends TreeTableModel
	{
		private static final long serialVersionUID = 1L;
		private Vector<String> columnNamesVec;

		public NOVECOTreeTableModel(String columnNames[])
		{
			super();
			columnNamesVec = new Vector<String>();
			TreeTableNode top = new TreeTableNode();
			setRoot(top);
			top.setModel(this);
			for(int i = 0; i < columnNames.length; i++)
			{
				columnNamesVec.addElement(columnNames[i]);
				modelIndexToProperty.add(columnNames[i]);
			}

		}
		public void addRoot(TreeTableNode root)
		{
			TreeTableNode top = (TreeTableNode)getRoot();
			top.add(root);
		}

		public TreeTableNode getRoot(int index)
		{
			if(((TreeTableNode)getRoot()).getChildCount() > 0)
				return (TreeTableNode)((TreeTableNode)getRoot()).getChildAt(index);
			else
				return null;
		}

		public int getColumnCount()
		{
			return columnNamesVec.size();
		}

		public String getColumnName(int column)
		{
			return (String)columnNamesVec.elementAt(column);
		}
		public void removeNode(int row)
		{
			//getNodeForRow(row).remove(arg0);
			((TreeTableNode)getRoot()).remove(getNodeForRow(row));
		}
	}

	public class NovTreeTableNode extends TreeTableNode
	{

		private static final long serialVersionUID = 1L;
		public TCComponentItemRevision itemRev;
		public TCComponentForm crForm;
		public NovTreeTableNode(TCComponentItemRevision itemRev2,TCComponentForm crForm2)
		{
			itemRev = itemRev2;
			crForm = crForm2;
		}
		@Override
		public String getProperty(String name)
		{
			try 
			{	String retStr="";
				if(name.equalsIgnoreCase("ECR"))
				{	
					if (crForm !=null) 
					{
						return crForm.getProperty("object_name");
					}
					return "";
					//return context.getProperty("item_id") == null ? "" : context.getProperty("item_id").toString();
				}
				else if(name.equalsIgnoreCase("Item Number"))
				{
					return itemRev.getProperty("item_id") == null ? "" : itemRev.getProperty("item_id").toString();
				}
				else if(name.equalsIgnoreCase("Type"))
				{
					return itemRev.getProperty("object_type") == null ? "" : itemRev.getProperty("object_type").toString();
				}
				else if(name.equalsIgnoreCase("Rev"))
				{
					return itemRev.getProperty("current_revision_id") == null ? "" : itemRev.getProperty("current_revision_id").toString();
				}
				else if(name.equalsIgnoreCase("Lifecycle"))
				{
					AIFComponentContext[] compConxt = ((TCComponentItemRevision)itemRev).getRelated("IMAN_master_form_rev");
					if (compConxt.length>0) 
					{
						return compConxt[0].getComponent().getProperty("Lifecycle");
					}
					return retStr;
				}
				else if(name.equalsIgnoreCase("Name"))
				{
					return itemRev.getProperty("object_name") == null ? "" : itemRev.getProperty("object_name").toString();
				}
				else if(name.equalsIgnoreCase("Description"))
				{
					return itemRev.getProperty("object_desc") == null ? "" : itemRev.getProperty("object_desc").toString();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			return "";
		}

		public String getRelation()
		{			
			return " ";
		}

		public String getType()
		{
			return itemRev.getType();
		}
	}
	class TargetIdRenderer extends TreeTableTreeCellRenderer
	{   		
		private static final long serialVersionUID = 1L;

		public TargetIdRenderer(JTreeTable arg0) {
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{
			// Select the current value
			try
			{
				super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
						row, hasFocus);

				if(row==0)
				{
					//setIcon(TCTypeRenderer.getTypeIcon(tcSession.getTypeComponent("Job"), "Job"));
					setText("Target Items");
				}
				else
				{
				    NovTreeTableNode treeNode = (NovTreeTableNode)getNodeForRow(row);
				    if(treeNode != null)
				    {
				        TCComponentItemRevision itemRev = treeNode.itemRev;
				        setText(itemRev!=null?itemRev.getProperty("item_id"):"");
				        setIcon(TCTypeRenderer.getIcon(itemRev));
				    }
				}				
			}
			catch(Exception e)
			{
				System.out.println(e);

			}
			return this;
		}
	}
	class RootCellRenderer extends JLabel implements TableCellRenderer
	{    

		private static final long serialVersionUID = 1L;

		public RootCellRenderer()
		{			
			super();
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
			{
				setText("");
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			else
			{
				setText(value.toString());
				if(isSelected)
				{
					setOpaque(true);
					setBackground(table.getSelectionBackground());
					setForeground(table.getForeground());
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
			}
			return this;
		}
	}
}
