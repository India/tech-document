package com.noi.rac.dhl.eco.util.components;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.combobox.iComboBox;

public class NOVECOSetStatusDialog extends AbstractAIFDialog implements ISubject2
{
    private static final long serialVersionUID = 1L;
    private TCSession m_session;
    private String m_grpName;
    private JPanel m_setStatusPanel;
    private JPanel m_compPanel;
    private JPanel m_buttonPanel;
    private JLabel m_itemTypeLbl;
    private JLabel m_statusLbl;
    private iComboBox m_itemTypeCmb;
    private iComboBox m_statusCmb;
    private JButton m_okButton;
    private JButton m_cancelButton;
    private JButton m_applyButton;
    private List<IObserver2> m_observers;
    private String[] m_itemTypeList = null;
    private String m_itemTypeStr = "";
    private String m_statusStr = "";
    private String[] m_newlifecycStrArr;
    private Registry m_registry = Registry.getRegistry(this);
    private String[] m_newlifecycStrArr_Doc = { "", "Standard", "Obsolete" };
    private String[] m_newlifecycStrArr_nonEng = { "", "Engineering", "Standard", "Phase Out", "Obsolete" };
    
    public NOVECOSetStatusDialog(String grpName)
    {
        super(AIFUtility.getActiveDesktop(), true);
        m_observers = new ArrayList<IObserver2>();
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_grpName = grpName;
        setTitle();
        initItemTypeList();
        createUI();
        addListeners();
    }
    
    private void addListeners()
    {
        
        m_itemTypeCmb.addActionListener(new ActionListener()
        {
            
            public void actionPerformed(ActionEvent e)
            {
                clearDialogData();
                m_itemTypeStr = m_itemTypeCmb.getSelectedItem().toString();
                if (m_itemTypeStr != null)
                {
                    if (m_itemTypeStr.trim().equalsIgnoreCase("Engineering"))
                    {
                        m_itemTypeStr = "Nov4Part";
                    }
                    loadStatusCombo(m_itemTypeStr);
                }
            }
        });
        
        m_statusCmb.addActionListener(new ActionListener()
        {
            
            public void actionPerformed(ActionEvent e)
            {
                m_statusStr = m_statusCmb.getSelectedItem().toString();
            }
        });
        
        m_okButton.addActionListener(new ActionListener()
        {
            
            public void actionPerformed(ActionEvent e)
            {
                boolean validateSelectn = validateSelectnForTrg();
                if (validateSelectn)
                {
                    notifyObserver(getDialogData());
                    NOVECOSetStatusDialog.this.dispose();
                    clearDialogData();
                }
                else
                    return;
            }
        });
        m_applyButton.addActionListener(new ActionListener()
        {
            
            public void actionPerformed(ActionEvent e)
            {
                boolean validateSelectn = validateSelectnForTrg();
                if (validateSelectn)
                {
                    notifyObserver(getDialogData());
                }
                else
                    return;
            }
        });
        m_cancelButton.addActionListener(new ActionListener()
        {
            
            public void actionPerformed(ActionEvent e)
            {
                NOVECOSetStatusDialog.this.dispose();
                clearDialogData();
            }
        });
        
    }
    
    private void clearDialogData()
    {
        m_itemTypeStr = "";
        m_statusStr = "";
        
    }
    
    private void loadValues(iComboBox cmbBox, String[] newlifecycStrArr)
    {
        
        if (newlifecycStrArr != null)
        {
            cmbBox.removeAllItems();
            for (int i = 0; i < newlifecycStrArr.length; i++)
            {
                cmbBox.addItem(newlifecycStrArr[i]);
            }
        }
    }
    
    private void initItemTypeList()
    {
        m_itemTypeList = new String[] { "Documents", "Engineering", "Non-Engineering" };
        
    }
    
    private void setTitle()
    {
        this.setTitle("Set Status");
    }
    
    private void createUI()
    {
        m_setStatusPanel = new JPanel(new GridLayout(2, 1));
        
        JPanel statusComponentsPanel = new JPanel();
        
        GridBagLayout gbl_setStatusPanel = new GridBagLayout();
        statusComponentsPanel.setLayout(gbl_setStatusPanel);
        
        m_itemTypeLbl = new JLabel("Item Type");
        GridBagConstraints gbc_itemTypeLbl = new GridBagConstraints();
        gbc_itemTypeLbl.anchor = GridBagConstraints.CENTER;
        gbc_itemTypeLbl.insets = new Insets(0, 0, 15, 5);
        gbc_itemTypeLbl.gridx = 0;
        gbc_itemTypeLbl.gridy = 0;
        statusComponentsPanel.add(m_itemTypeLbl, gbc_itemTypeLbl);
        
        m_itemTypeCmb = new iComboBox(m_itemTypeList);
        m_itemTypeCmb.setEditable(false);
        GridBagConstraints gbc_itemTypeCmb = new GridBagConstraints();
        gbc_itemTypeCmb.insets = new Insets(0, 0, 15, 5);
        gbc_itemTypeCmb.fill = GridBagConstraints.HORIZONTAL;
        gbc_itemTypeCmb.gridx = 1;
        gbc_itemTypeCmb.gridy = 0;
        statusComponentsPanel.add(m_itemTypeCmb, gbc_itemTypeCmb);
        
        m_statusLbl = new JLabel("Status");
        GridBagConstraints gbc_statusLbl = new GridBagConstraints();
        gbc_statusLbl.anchor = GridBagConstraints.WEST;
        gbc_statusLbl.insets = new Insets(0, 0, 5, 5);
        gbc_statusLbl.gridx = 0;
        gbc_statusLbl.gridy = 1;
        statusComponentsPanel.add(m_statusLbl, gbc_statusLbl);
        
        m_statusCmb = new iComboBox();
        m_statusCmb.setEditable(false);
        GridBagConstraints gbc_statusCmb = new GridBagConstraints();
        gbc_statusCmb.insets = new Insets(0, 0, 5, 5);
        gbc_statusCmb.fill = GridBagConstraints.HORIZONTAL;
        gbc_statusCmb.gridx = 1;
        gbc_statusCmb.gridy = 1;
        statusComponentsPanel.add(m_statusCmb, gbc_statusCmb);
        
        /* creates panel containg ok/cancel/apply buttons */
        createButtonBar();
        m_setStatusPanel.add(statusComponentsPanel);
        m_setStatusPanel.add(m_buttonPanel);
        this.getContentPane().add(m_setStatusPanel);
        this.pack();
        this.centerToScreen();
        this.setPreferredSize(new Dimension(350, 250));
        
    }
    
    private void createButtonBar()
    {
        
        m_buttonPanel = new JPanel();
        m_okButton = new JButton("OK");
        m_applyButton = new JButton("Apply");
        m_cancelButton = new JButton("Cancel");
        
        JPanel stattusBtnsPanel = new JPanel();
        
        GridBagConstraints gbc_okButton = new GridBagConstraints();
        gbc_okButton.anchor = GridBagConstraints.WEST;
        gbc_okButton.insets = new Insets(0, 0, 5, 5);
        gbc_okButton.gridx = 0;
        gbc_okButton.gridy = 0;
        stattusBtnsPanel.add(m_okButton, gbc_okButton);
        
        GridBagConstraints gbc_applyButton = new GridBagConstraints();
        gbc_applyButton.anchor = GridBagConstraints.CENTER;
        gbc_applyButton.insets = new Insets(0, 0, 5, 5);
        gbc_applyButton.gridx = 0;
        gbc_applyButton.gridy = 0;
        stattusBtnsPanel.add(m_applyButton, gbc_applyButton);
        
        GridBagConstraints gbc_cancelButton = new GridBagConstraints();
        gbc_cancelButton.anchor = GridBagConstraints.EAST;
        gbc_cancelButton.insets = new Insets(0, 0, 5, 5);
        gbc_cancelButton.gridx = 0;
        gbc_cancelButton.gridy = 0;
        stattusBtnsPanel.add(m_cancelButton, gbc_cancelButton);
        
        m_buttonPanel.add(m_okButton);
        m_buttonPanel.add(m_applyButton);
        m_buttonPanel.add(m_cancelButton);
    }
    
    public String[] getDialogData()
    {
        String[] getDialogDataArr = { m_itemTypeStr, m_statusStr };
        return getDialogDataArr;
    }
    
    /* load status combo based on item type */

    private void loadStatusCombo(String itemType)
    {
        try
        {
            
            if (itemType.equals("Documents"))
            {
                loadValues(m_statusCmb, m_newlifecycStrArr_Doc);
            }
            else if (itemType.equals("Nov4Part"))
            {
                TCComponentListOfValues lovValues = TCComponentListOfValuesType
                        .findLOVByName(m_session, "DH_Lifecycle");
                String filterStr = m_grpName;
                String[] lovS = NOVECOHelper.getLovList(lovValues, filterStr);
                Vector<String> newLifecycleVals = new Vector<String>();
                newLifecycleVals.add("");
                for (int i = 0; i < lovS.length; i++)
                {
                    newLifecycleVals.add(lovS[i]);
                }
                m_newlifecycStrArr = newLifecycleVals.toArray(new String[newLifecycleVals.size()]);
                if (m_newlifecycStrArr != null)
                {
                    loadValues(m_statusCmb, m_newlifecycStrArr);
                }
                
            }
            else if (itemType.equals("Non-Engineering"))
            {
                loadValues(m_statusCmb, m_newlifecycStrArr_nonEng);
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public boolean validateSelectnForTrg()
    {
        
        if (m_itemTypeStr.isEmpty())
        {
            if (m_statusStr.isEmpty())
            {
                JOptionPane.showMessageDialog(this, m_registry.getString("No_Itemtype_Status_Selected.TITLE"),
                        m_registry.getString("ERROR.TITLE"), JOptionPane.ERROR_MESSAGE);
                return false;
                
            }
            else
            {
                JOptionPane.showMessageDialog(this, m_registry.getString("No_ItemType_Selected.TITLE"),
                        m_registry.getString("ERROR.TITLE"), JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        else if (m_statusStr.isEmpty())
        {
            JOptionPane.showMessageDialog(this, m_registry.getString("No_Itemtype_Status_Selected.TITLE"),
                    m_registry.getString("ERROR.TITLE"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
        
    }
    
    public void registerObserver(IObserver2 ob)
    {
        m_observers.add(ob);
    }
    
    public void removeObserver(IObserver2 subs)
    {
        
    }
    
    public void notifyObserver(Object arg)
    {
        Iterator<IObserver2> i = m_observers.iterator();
        while (i.hasNext())
        {
            IObserver2 theObserver = (IObserver2) i.next();
            theObserver.update(this, arg);
        }
        
    }
    
}