/**
 * 
 */
package com.noi.rac.dhl.eco.util.components;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

/**
 * @author kabades
 *
 */
public class NOVWizardSearchECROperation implements IRunnableWithProgress
{
    private NOVCRSelectionPanel m_novCRSelectionPanel;
    private String m_formName;
    private String m_groupName;
    private boolean m_showAll; 
    private String m_progressBarMsg;

    NOVWizardSearchECROperation(NOVCRSelectionPanel novCRSelectionPanel,String formName,String groupName, String message)
    {
        
        this.m_novCRSelectionPanel = novCRSelectionPanel;
        this.m_formName = formName;
        this.m_groupName = groupName;
        this.m_progressBarMsg = message;
   }

    @Override
    public void run(IProgressMonitor progressMonitor)
            throws InvocationTargetException, InterruptedException
    {
        progressMonitor.beginTask(m_progressBarMsg,IProgressMonitor.UNKNOWN );
        
        final IProgressMonitor finalprogressMonitor = progressMonitor;

        try
        {
        	m_novCRSelectionPanel.executeECRSearch(m_formName, m_groupName, m_showAll); //7784
            finalprogressMonitor.done();
        } catch (Exception e) {
            e.printStackTrace();
            finalprogressMonitor.setCanceled(true);
            finalprogressMonitor.done();
        }
    }
}
