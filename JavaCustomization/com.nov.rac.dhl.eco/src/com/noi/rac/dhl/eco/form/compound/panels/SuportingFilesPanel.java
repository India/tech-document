package com.noi.rac.dhl.eco.form.compound.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.FileChooserUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import com.noi.NationalOilwell;
import com.noi.rac.commands.newdataset.NewDatasetDialog;
import com.noi.rac.dhl.eco.form.compound.util.INOVCustomFormProperties;
import com.noi.util.components.ExternalAttachmentsTableModel;
import com.nov.rac.utilities.services.deleteHelper.DeleteRelationHelper;
import com.teamcenter.rac.aif.AbstractAIFApplication;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class SuportingFilesPanel extends JPanel implements ActionListener,ListSelectionListener {

	private static final long serialVersionUID = 1L;
	private JPanel parentPanel;
	JTable theTable = new JTable();
	private String m_defFilePath = null;
		
	
	JScrollPane tablePane;
	JButton attachmentsBoxPlus;
	JButton attachmentsBoxMinus;
	JButton viewAttachment;
	
	ArrayList<InterfaceAIFComponent> tableData = new ArrayList<InterfaceAIFComponent>();
	Vector<InterfaceAIFComponent>    theData = new Vector<InterfaceAIFComponent>();
	AbstractAIFApplication app=null;

	Vector<InterfaceAIFComponent> attachments = new Vector<InterfaceAIFComponent>();
	
	ExternalAttachmentsTableModel theModel=null;
	int selectedItemRow=-1;
    INOVCustomFormProperties fp; 
    boolean isFormLocked=false;
	
	public SuportingFilesPanel(INOVCustomFormProperties fp,boolean locked, JPanel parentPanel) {
		super();
		this.parentPanel = parentPanel;
		isFormLocked = locked;
		this.fp = fp;
		try 
		{
			//TCComponent[] comps=fp.getForm().getRelatedComponents("_CustomSubForm_");
			String[] relation={"_CustomSubForm_"};
	        AIFComponentContext[] comps = fp.getForm().whereReferencedByTypeRelation(new String[0], relation);
	        for (int i=0;i<comps.length;i++) 
	        {
	            if ((comps[i].getComponent()) instanceof TCComponentDataset) 
	            {
	                attachments.add(comps[i].getComponent());
	            }
	        }
	    	theModel = new ExternalAttachmentsTableModel(attachments);
	    	addSaveDialogListener();
	    	
	    	
				
		}
		catch (Exception e) {
			
		}
		setInitialData(new ArrayList());
		createUI();	
	}

	/*TCDECREL-3198-Retain directory file path for the attachments on ECO Form*/
	private void addSaveDialogListener() {
		
    	theTable.addMouseListener(new java.awt.event.MouseAdapter() 
    	{
			public void mousePressed(java.awt.event.MouseEvent mouseEvt)
			{
				try 
				{					
					Object source = mouseEvt.getSource();
					if(SwingUtilities.isRightMouseButton(mouseEvt) && (mouseEvt.getClickCount() == 1)&& selectedItemRow>=0 )
					{
						JPopupMenu popupMenu = new JPopupMenu();
						JMenuItem item = new JMenuItem();
						String initialText1 = "<html><b><font=8 color=red>Save As</font></b>";
						item.setText(initialText1);
						item.addActionListener(new ActionListener() 
						{	
							
							public void actionPerformed(ActionEvent e)
							{
								showSaveDialog();
							}
						});									
						popupMenu.add(item);
						popupMenu.show((java.awt.Component)source, mouseEvt.getX(), mouseEvt.getY());
					}					
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}});		
	}
	
	/*TCDECREL-3198-Retain directory path for saving the Attachments on ECO form*/
	private void showSaveDialog()
	{
		try
		{
			String fetchFileType = "";
			
			/*TCComponentDataset ds =((ExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);*///fetching dataset
			TCComponentDataset ds = theModel.getAttachment(selectedItemRow);
           fetchFileType = getFileRefType(ds);//getting ds type
               
           if(null!=fetchFileType && fetchFileType.length()>0)
           { 
        	   
        	   String srcPath = null;
        	   String destPath = null;
        	   System.out.println("fetchFileType ***** "+fetchFileType);        	  
        	   File[] files =ds.getFiles(fetchFileType);			            	   
        	   if(files!=null)
        	   {
        		   if(m_defFilePath == null)
        			   m_defFilePath = files[0].getAbsolutePath();//absolute path.....portal//temp
        		   
					srcPath = m_defFilePath.replace('\\', '/');
					String fileName = files[0].getName();	
					
					JFileChooser saveDialog = new JFileChooser(m_defFilePath);
					/*saveDialog.getSelectedFile( );*/
					
					//setting the save dialog file name with the selected Attachment name
					FileChooserUI fcUi = saveDialog.getUI();      
					Class<? extends FileChooserUI> fcClass = fcUi.getClass();         
					Method setFileName = fcClass.getMethod("setFileName", String.class);         
					setFileName.invoke(fcUi, fileName);
					//saveDialog.setSelectedFile(files[0]);
					
					int saveChoice = saveDialog.showSaveDialog(theTable);						
					if (saveChoice == JFileChooser.APPROVE_OPTION)
			        {
						 File saveFile = saveDialog.getSelectedFile();
						 destPath = saveFile.getPath().replace('\\', '/');
						 if( srcPath.length()>0 && destPath.length()>0)							
						 {
							 m_defFilePath = saveFile.getPath();    //retaining the directory path
							 //copyFile(srcPath, destPath);
							 renameSrcToDestPath(srcPath, destPath); 
						 }
						 
			        }
					
        	   }
        	   
           }
           
		}
		catch (Exception ex)
		{
		    ex.printStackTrace();
		} 
		
	}
	private void renameSrcToDestPath(String srcPath, String destPath) throws FileNotFoundException, IOException
	{
		
		File srcFile =new File(srcPath);
		if(new File(destPath).exists())
		{
			JOptionPane.showMessageDialog(theTable,"File already exists");
			return;
		}
		else
		{		 
	 	   if(srcFile.renameTo(new File(destPath)))
	 	   {
	 		System.out.println("File is moved successful!");
	 	   }
	 	   else
	 	   {
	 		System.out.println("File is failed to move!");
	 	   }
		}
		
	}
	
	
	public void setEnabled(boolean enabled) 
	{
		attachmentsBoxPlus.setEnabled(enabled);
		attachmentsBoxMinus.setEnabled(enabled);
		viewAttachment.setEnabled(enabled);
	}

	public TableModel getTableModel() { return theTable.getModel(); }
	
	
    public void setInitialData(ArrayList al) {
    	
    	if (attachments.size() > 0) return;
    	
    	tableData = al;
    	for (int i=0;i<tableData.size();i++)
    		theData.add(tableData.get(i));
    	theModel = new ExternalAttachmentsTableModel(theData);
    }
	
    public void createUI() {
    	
    	Registry registry = Registry.getRegistry(this);
    	
    	//setLayout(new GridBagLayout());
    	//setBackground(new Color(225,225,225));
    	setPreferredSize(new Dimension(680,180));		
    	TitledBorder tb = new javax.swing.border.TitledBorder(registry.getString("SuportingFilesPanel.Title"));
    	tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
    	tb.setTitleColor(Color.BLACK);
    	setBorder(tb);
        ListSelectionModel rowSM = theTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        theTable.setModel(theModel);
        theTable.setPreferredScrollableViewportSize(new Dimension(675,200));		//TCDECREL-2798 : Akhilesh
        TableColumn col = null;
        
        col = theTable.getColumnModel().getColumn(0);
        int newwidth = 125;
        col.setPreferredWidth(newwidth);
        
        col = theTable.getColumnModel().getColumn(1);
        col.setCellEditor(null);
        newwidth = 250;
        col.setPreferredWidth(newwidth);
    	
        JTableHeader head = theTable.getTableHeader();
        head.setBackground(NationalOilwell.NOVPanelContrast);
        
        tablePane = new JScrollPane(theTable);
        
        ImageIcon plusIcon=null;
        try {
        	//Registry.getImageIconFromPath
        	plusIcon = registry.getImageIcon("add.ICON");
        }
        catch (Exception ie) {
        	System.out.println(ie);
        }
		ImageIcon minusIcon =registry.getImageIcon("remove.ICON");
        
		attachmentsBoxPlus = new JButton(plusIcon);
		attachmentsBoxMinus = new JButton(minusIcon);
		viewAttachment = new JButton(registry.getString("SuportingFilesPanel.ViewSelectedDocument"));
		try
		{
			isFormLocked=fp.getForm().getLogicalProperty("locked");
		}
		catch(Exception e)
		{
			
		}
		
		viewAttachment.setVisible(false);
		attachmentsBoxPlus.addActionListener(this);
		attachmentsBoxMinus.addActionListener(this);
		viewAttachment.addActionListener(this);
		
		//setLayout();
		tablePane.setToolTipText("Additional information for reference. Will not be printed with the EN");
		tablePane.setPreferredSize(new Dimension(800,100));			//TCDECREL-2798 : Akhilesh
        this.add(tablePane);
        
        JPanel bpanel = new JPanel();
        //bpanel.setBackground(Color.yellow);
        //bpanel.setPreferredSize(new Dimension(250,25));
        attachmentsBoxPlus.setToolTipText(registry.getString("SuportingFilesPanel.AddButtonToolTip"));
        attachmentsBoxMinus.setToolTipText(registry.getString("SuportingFilesPanel.RemoveButtonToolTip"));
        bpanel.add(attachmentsBoxPlus);
        bpanel.add(attachmentsBoxMinus);
        bpanel.add(viewAttachment);
        this.add(bpanel);
        //resizePanel();
    
    }

    public void resizePanel(Dimension dPanel)
    {
    	if(dPanel != null)
    		setPreferredSize(dPanel);
    }
    public void actionPerformed(ActionEvent event) 
    {
    	if (event.getSource() == attachmentsBoxPlus && !isFormLocked) 
    	{
    		NewDatasetDialog dlg = new NewDatasetDialog(null, AIFUtility.getCurrentApplication(),
    				                fp.getForm(),false);
    		//dlg.setModal(true);
    		dlg.setLocationRelativeTo(this);
    		if(Toolkit.getDefaultToolkit().getScreenResolution()>96)
    		{
    		dlg.setSize(600, 220);
    		}
    		else
    		{
    			dlg.setSize(500, 220);
    		}
    		dlg.setDsName("Attachment");
    		dlg.setVisible(true);
    		if (dlg.getDataset() != null) {
    		    Vector<TCComponent> v = new Vector<TCComponent>();
    		    v.add(dlg.getDataset());
    		    ((ExternalAttachmentsTableModel)theTable.getModel()).addRows(v);
    		}
    		fp.saveFormData();
    	}
    	else if (event.getSource() == attachmentsBoxMinus && !isFormLocked)
    	{
    		if(theTable.getModel().getRowCount()>0)
    		{
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
    			{
	    			TCComponentDataset ds = 
	    				((ExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
	    			TCComponentForm form = (TCComponentForm)fp.getForm();
	    			try 
	    			{
	                    DeleteRelationHelper delete = new DeleteRelationHelper(ds,
	                            form,"_CustomSubForm_");
	                    delete.deleteRelation();
	    			}
	    			catch (Exception e) {
	    				System.out.println("Could not remove attachment: "+e);
	    			}
	    			
	    			((ExternalAttachmentsTableModel)theTable.getModel()).removeRow(selectedItemRow);
    			}
    		}		
    	}
    	else if (event.getSource() == viewAttachment)
    	{
    		if(theTable.getModel().getRowCount()>0){
    			if (selectedItemRow >= 0 && selectedItemRow < theTable.getModel().getRowCount())
	    			{
		    			TCComponentDataset ds =((ExternalAttachmentsTableModel)theTable.getModel()).getAttachment(selectedItemRow);
		    			try 
		    			{
			             
			               String   cmd="";
			               String fetchFileType = "";
			               fetchFileType = getFileRefType(ds);
			               /*
			               else if (dstype.equalsIgnoreCase("acaddwg"))
			               {
			            	   fname = ds.getFileNames("DWG");
			            	   fetchFileType ="DWG";
			               }
			              */
			               if(null!=fetchFileType && fetchFileType.length()>0)
			               {   
			            	  /* if (dstype.equalsIgnoreCase("pdf"))
				               {
				            	   fetchFileType ="PDF_Reference";
				               }*/
			            	   
			            	   System.out.println("fetchFileType ***** "+fetchFileType);
			            	   String fileName = null;
			            	   File[] fl =ds.getFiles(fetchFileType);
			            	   if(fl!=null)
			            	   {
									fileName = fl[0].getAbsolutePath();
									
									if (fileName!=null && fileName.length() > 0) 
									{
										System.out.println("File name : "+ fl[0].getAbsolutePath());
										cmd = "rundll32 shell32.dll,OpenAs_RunDLL "+ fileName;// windPath+"\\temp\\"+newName;
										Runtime.getRuntime().exec(cmd);
									}
			            	   }
			               }
			               else
			               {
			            	   MessageBox.post("Attachment do not have any reference to view","Error viewing document",MessageBox.INFORMATION);
			               }
	    			}
	    			catch (Exception e) {
	    				e.printStackTrace();
	    				MessageBox.post("Could not execute required function:",e.toString(),"Error viewing document",MessageBox.INFORMATION);
	    			}
	    		} 
    		}
    	}
		//resizePanel();
    	theTable.updateUI();
    }
 
    
    
    public String getFileRefType(TCComponentDataset dataset)
    {
    	String strRefType=null;
    	try 
    	{
    		strRefType = dataset.getProperty("ref_names");
    		
    		if( strRefType!=null && strRefType.indexOf(",")!=-1 )
    			strRefType = strRefType.split(",")[0];
    		
    		return strRefType;
			
		} 
    	catch (TCException e) 
    	{
			e.printStackTrace();
		}
		return strRefType;
    }
    
	public void valueChanged(ListSelectionEvent e) {
		
        if (e.getValueIsAdjusting()) return;

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
        	;
        } else {
            selectedItemRow = lsm.getMinSelectionIndex();
            viewAttachment.setVisible(true);
            viewAttachment.setEnabled(true);
        }
		
	}
    public void resizePanel() {
    	
        TableModel tm = theTable.getModel();
        int tableHeight = (tm.getRowCount()+1)*18;
        if (tm.getRowCount() <= 0){
        	tablePane.setVisible(false); 
            viewAttachment.setVisible(false);
            viewAttachment.setEnabled(false);

        }
        else {
        	tablePane.setVisible(true);
        	if(selectedItemRow >= 0 && selectedItemRow < tm.getRowCount()){
        		viewAttachment.setVisible(true);
        		viewAttachment.setEnabled(true);
        	}
        	else{
                viewAttachment.setVisible(false);
                viewAttachment.setEnabled(false);
        	}
        }
        if (tableHeight > 100) tableHeight=100;
        if (tableHeight <= 54) tableHeight+=4;
        
        tablePane.setPreferredSize(new Dimension(670,tableHeight));
        this.setPreferredSize(new Dimension(680,tableHeight+70));
       this.setMinimumSize(new Dimension(680,tableHeight+70));
       
       //parentPanel.setPreferredSize(new Dimension(680,parentPanel.getPreferredSize().height));
	   parentPanel.revalidate();
       parentPanel.repaint();
    	
    }    

}
