/**
 * 
 */
package com.noi.rac.dhl.eco.util.components;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
/**
 * @author kabades
 *
 */
public class NOVSearchECROperation implements IRunnableWithProgress
{
    private Object m_objs[] =null;
    private NOVCRManagementPanel m_novCRManagementPanel;
    private String m_userServiceName;
    private String m_progressBarMsg;
    
    NOVSearchECROperation(Object []objs,NOVCRManagementPanel novCRManagementPanel,String userServiceName,String message)
    {
        this.m_objs = objs;
        this.m_novCRManagementPanel =novCRManagementPanel;
        this.m_userServiceName = userServiceName;
        this.m_progressBarMsg = message;
    }

    @Override
    public void run(IProgressMonitor progressMonitor)
            throws InvocationTargetException, InterruptedException
    {
        progressMonitor.beginTask(m_progressBarMsg,IProgressMonitor.UNKNOWN );
        
        final IProgressMonitor finalprogressMonitor = progressMonitor;

        try
        {
            m_novCRManagementPanel.invokeUserService(m_userServiceName, m_objs,finalprogressMonitor);
            finalprogressMonitor.done();
        } catch (Exception e) {
            e.printStackTrace();
            finalprogressMonitor.setCanceled(true);
            finalprogressMonitor.done();
        }
    }
}
