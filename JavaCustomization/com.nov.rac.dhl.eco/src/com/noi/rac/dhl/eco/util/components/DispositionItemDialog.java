package com.noi.rac.dhl.eco.util.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.util.PropertyLayout;

public class DispositionItemDialog extends AbstractAIFDialog
{
	private static final long serialVersionUID = 1L;
	
	_EngChangeObjectForm_Panel engChangeObjFormPanel;	
	TCComponent refDispItem;
	TCComponent targetRev;
	NOVENDispositionPanelData dispPanelData;
	NOVECODispositionTabPanel dispPanel ;//4474-remove this variable as final
 
	public DispositionItemDialog( NOVECOTargetsPanel ecoTargetsPanel,
			_EngChangeObjectForm_Panel _engChangeObjFormPanel, TCComponent _refDispItems, TCComponent _targetRev )
	{
		super( AIFUtility.getActiveDesktop() );
		engChangeObjFormPanel = _engChangeObjFormPanel;
		refDispItem = _refDispItems;
		targetRev = _targetRev;
		createUI();
	}

	public void createUI()
	{
        JPanel dispItemPanel = new JPanel( new PropertyLayout() );
        //4474
        TCReservationService      reserserv;
		boolean flag;
		reserserv = engChangeObjFormPanel.ecoForm.getSession().getReservationService();
        TCComponent[] refDispItemArr = { refDispItem };
        dispPanelData = new NOVENDispositionPanelData( refDispItemArr );
        
		 dispPanel = new NOVECODispositionTabPanel( engChangeObjFormPanel,
				engChangeObjFormPanel.getSession(), refDispItem, dispPanelData );

		//dispPanel.createBOMPanel( targetRev );
		dispPanel.createUI();
		//enableDispositionField();//4474
		
		dispPanel.loadData(refDispItem, targetRev);
		setDispositionFieldsEnable();//4907
		//disableTargetDispositionFields();//3328
		//dispItemPanel.setPreferredSize( new Dimension( 630, 820 ) );
		dispItemPanel.add( "1.1.center.center", dispPanel );
		
		JPanel saveCancelBtnpanel = new JPanel();
		JButton saveBtn = new JButton( "Save" );
		
		saveBtn.addActionListener( new ActionListener() 
		{
			public void actionPerformed( ActionEvent arg0 ) 
			{
				dispPanel.saveData();
			}
		} );
		
		JButton cancelBtn = new JButton( "Cancel" );
		cancelBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				DispositionItemDialog.this.dispose();
			}
		});		
		
		saveCancelBtnpanel.add( saveBtn );
		saveCancelBtnpanel.add( cancelBtn );
		
		
		JPanel contentPanel = new JPanel( new PropertyLayout() );
		contentPanel.add( "1.1.center.center", dispItemPanel );
		contentPanel.add( "2.1.right.center", saveCancelBtnpanel );
		
		contentPanel.setLocation( 20, 20 );
		JScrollPane contentScrollPane = new JScrollPane( contentPanel );
		
		//this.setPreferredSize( new Dimension( 640, 900 ) );
		this.getContentPane().add( contentScrollPane );
		this.setResizable( true );
		this.setTitle( "ECO Target Item Properties" );
		this.pack();
		this.centerToScreen();
		this.setModal( true );
	}
	//4474-start
	private void enableDispositionField()
	{
		TCReservationService      reserserv;
		reserserv = engChangeObjFormPanel.ecoForm.getSession().getReservationService();
		try 
		{
			if(reserserv.isReserved(engChangeObjFormPanel.ecoForm))
			{
				dispPanel.setDispositionEditable(true);
			}
			else
			{
				dispPanel.setDispositionEditable(false);
			}
		} catch (TCException e)
		{
			e.printStackTrace();
		}
	}
	//4474-end
	//3328-start
	private void disableTargetDispositionFields()
	{
		if(engChangeObjFormPanel.isEcoReleased)
		{
			dispPanel.setEditable(false);
		}
	}
	//3328-end
	//4907-start
	private void setDispositionFieldsEnable()
	{
		TCReservationService      reserserv;
		reserserv = engChangeObjFormPanel.ecoForm.getSession().getReservationService();
		try 
		{
			if(! (engChangeObjFormPanel.ecoForm.okToModify()) || engChangeObjFormPanel.isEcoReleased )
			{
				dispPanel.setDispositionEditable(false);
			}
			else if(reserserv.isReserved(engChangeObjFormPanel.ecoForm) )
			{
				dispPanel.setDispositionEditable(true);
			}
			else
			{
				dispPanel.setDispositionEditable(false);
			}
			}catch (TCException e)
			{
				e.printStackTrace();
			}
			
			}
	//4907-end
}
