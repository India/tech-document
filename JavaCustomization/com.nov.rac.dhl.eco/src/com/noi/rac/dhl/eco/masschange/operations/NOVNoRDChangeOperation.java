package com.noi.rac.dhl.eco.masschange.operations;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.masschange.utils.NOVRDMessages;
import com.teamcenter.rac.kernel.TCComponent;

/**
 * @author tripathim
 * 
 */
public class NOVNoRDChangeOperation extends AbstractMassRDChangeOperation
{
    public NOVNoRDChangeOperation(MassRDChangeComposite massRDChangePanel, TCComponent[] selectedComps,
            String mmrRevType)
    {
        super(massRDChangePanel, selectedComps, mmrRevType);
        m_successMsg = NOVRDMessages.getString("ItemsAddedSuccess.MESSAGE");
    }
    
    @Override
    public void performRDEditOperation()
    {
        
    }
}
