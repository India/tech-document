package com.noi.rac.dhl.eco.util.components;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.noi.NationalOilwell;
import com.noi.rac.dhl.eco.form.compound.util.CheckBoxList;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;

public class NOVWhereUsedFilterDialog extends AbstractAIFDialog//JDialog 
{
	private static final long serialVersionUID = 1L;
	
	private	JPanel							m_mainPanel;
	private JPanel							m_btnPanel;
	
	private	CheckBoxList					m_sitesPanel;
	private	CheckBoxList					m_lifecyclePanel;
	private	TCSession						m_session;	
	
	private	NOVWhereUsedFilterCriteria		m_selection;
	
	final	int								m_columns = 4;
	final	int								m_lifeCyclePanelColumns = 3;
	
	public NOVWhereUsedFilterDialog(NOVWhereUsedFilterCriteria selection)
	{
		super(AIFUtility.getActiveDesktop());		
		m_session =  (TCSession) AIFUtility.getDefaultSession();
		m_selection = selection;
		createUI();
		showSelection();
	} 	
	
	public NOVWhereUsedFilterDialog(Frame parent, NOVWhereUsedFilterCriteria selection)
	{
		super(parent);		
		m_session =  (TCSession) AIFUtility.getDefaultSession();
		m_selection = selection;
		createUI();
		showSelection();
	}

	public Object showDialog(int x, int y)
	{
		setLocation(x, y);
		setVisible(true); 
		return m_selection;  
	} 
	
	public void createUI()
	{
		m_mainPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		m_sitesPanel = new CheckBoxList(m_session, NationalOilwell.DHSITELOV, "Sites", m_columns);
		m_lifecyclePanel = new CheckBoxList(m_session, "DH_Lifecycle", "Lifecycle", m_lifeCyclePanelColumns);
				
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;		
		m_mainPanel.add(m_sitesPanel, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;	
		m_mainPanel.add(m_lifecyclePanel, c);
				
		c.gridx = 0;
		c.gridy = 2;
		c.fill = GridBagConstraints.HORIZONTAL;	
		m_btnPanel = createButtonPanel();
		m_mainPanel.add(m_btnPanel, c);
		
		getContentPane().add(m_mainPanel);	
		
		this.setTitle("Where Used Filter criteria");
		this.setResizable(false);
		this.pack();
		this.setModal(true);
	}
	
	private JPanel createButtonPanel()
	{
		JButton 			okBtn;
		JButton 			clearBtn;
		JButton 			cancelBtn;
		JPanel				btnPanel;
		
		btnPanel = new JPanel();
		okBtn = new JButton("OK");
		okBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				m_selection = new NOVWhereUsedFilterCriteria();
				m_selection.setSiteFilterValue(m_sitesPanel.getSelectedValues());
				m_selection.setLifecycleFilterValue(m_lifecyclePanel.getSelectedValues());
				//4344-start-set the flag if selection is empty 
				if(m_selection.getSiteFilterValue().size()==0 && m_selection.getLifecycleFilterValue().size()==0)
				{				
					m_selection.setSelectionEmpty(true);
				}//4344-end
				setVisible(false);
				NOVWhereUsedFilterDialog.this.dispose();
			}
		});
		
		clearBtn = new JButton("Reset");
		clearBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				/* Clears the selection of checkboxes */
				//4344-start-changed to check all the check boxes to be check 
				m_sitesPanel.setSelectedValues(true);
				m_lifecyclePanel.setSelectedValues(true);//4344-end
			}
		});
		
		cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(new ActionListener() 
		{			
			@Override
			public void actionPerformed(ActionEvent actionevent) 
			{
				setVisible(false);
				NOVWhereUsedFilterDialog.this.dispose();				
			}
		});
		
		btnPanel.add(clearBtn);
		btnPanel.add(okBtn);
		btnPanel.add(cancelBtn);
		
		return btnPanel;
	}
	
	private void showSelection() 
	{
		if(m_selection != null)
		{
			if(m_selection.getSiteFilterValue().size()>0)
			{
				m_sitesPanel.setSelection(m_selection.getSiteFilterValue());
			}
			if(m_selection.getLifecycleFilterValue().size()>0)
			{
				m_lifecyclePanel.setSelection(m_selection.getLifecycleFilterValue());
			}
			showSiteAndLifecycleSelection();//4344
		}				
	}	
	//4344-start
	private void showSiteAndLifecycleSelection()
	{
		if(m_selection.getLifecycleFilterValue().size()==0 && m_selection.getSiteFilterValue().size()==0 )
		{
			if(m_selection.isSelectionEmpty())
			{
				m_lifecyclePanel.setSelectedValues(false);
				m_sitesPanel.setSelectedValues(false);
				//m_selectionFlag=false;
			}
			else
			{
			m_sitesPanel.setSelectedValues(true);
			m_lifecyclePanel.setSelectedValues(true);
			}			
		}
	}//4344-end
}
