package com.noi.rac.dhl.eco.util.components;

/**
 * @author mishalt
 * Created On: July 2013
 */
import java.util.HashMap;
import java.util.Vector;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;

public class NOVSearchECRFormOperation implements INOVSearchProvider
{
    private HashMap<String, Vector<Object>> m_mapECRForms;
    private Vector<String> m_ecrFormRowData = null;
    private int m_ecrFormRowCount = 0;
    private int m_ecrFormColCount = 0;
    
    private INOVSearchResult m_searchResult = null;
    private String m_sFormName = null;
    private String m_sChangeType = null;
    private String m_sOwningGroup = null;
    private String m_sECRstatus = null;
    
    public NOVSearchECRFormOperation()
    {
        super();
        m_mapECRForms = new HashMap<String, Vector<Object>>();
    }
    
    public HashMap<String, Vector<Object>> executeSearch()
    {
        final INOVSearchProvider searchService = this;
        
        m_searchResult = null;
        final Display display = Display.getDefault();
        display.syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                Shell shell = AIFUtility.getActiveDesktop().getShell();
                
                try
                {
                    m_searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL,
                            shell);
                    if (m_searchResult != null && m_searchResult.getResponse().nRows > 0)
                    {
                        INOVResultSet theResSet = m_searchResult.getResultSet();
                        m_ecrFormRowData = theResSet.getRowData();
                        m_ecrFormRowCount = theResSet.getRows();
                        m_ecrFormColCount = theResSet.getCols();
                        m_mapECRForms = getECRFormTableMap();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        return m_mapECRForms;
    }
    
    public void setFormName(String formName)
    {
        m_sFormName = formName;
    }
    
    public void setGroupName(String groupName)
    {
        m_sOwningGroup = groupName;
    }
    
    public void setStatus(String status)
    {
        m_sECRstatus = status;
    }
    
    public void setChangeType(String changeType)
    {
        m_sChangeType = changeType;
    }
    
    public void setShowAll(boolean isShowAll)
    {
        if (isShowAll)
        {
            m_sOwningGroup = "";
            m_sFormName = "";
        }
    }
    
    public HashMap<String, Vector<Object>> getECRFormTableMap()
    {
        HashMap<String, Vector<Object>> mapECRFromTable = new HashMap<String, Vector<Object>>();
        try
        {
            if (m_ecrFormRowData != null)
            {
                for (int i = 0; i < m_ecrFormRowCount; i++)
                {
                    int rowIndex = m_ecrFormColCount * i;
                    String puid = m_ecrFormRowData.get(rowIndex + 0);
                    String ecrName = m_ecrFormRowData.get(rowIndex + 1);
                    String ecrType = m_ecrFormRowData.get(rowIndex + 6);
                    String group = m_ecrFormRowData.get(rowIndex + 7);
                    String initiator = m_ecrFormRowData.get(rowIndex + 13);
                    String dateEntered = m_ecrFormRowData.get(rowIndex + 2);
                    String refECOs = m_ecrFormRowData.get(rowIndex + 14);
                    
                    Vector<Object> rowData = new Vector<Object>();
                    rowData.add(ecrName);
                    rowData.add(ecrType);
                    rowData.add(group);
                    rowData.add(initiator);
                    rowData.add(dateEntered);
                    rowData.add(refECOs);
                    
                    mapECRFromTable.put(puid, rowData);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return mapECRFromTable;
    }
    
    @Override
    public String getBusinessObject()
    {
        return "Form";
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        if (m_sFormName == null || m_sFormName.trim().equalsIgnoreCase(""))
        {
            m_sFormName = "*";
        }
        if (m_sChangeType == null || m_sChangeType.equals("General"))
        {
            m_sChangeType = "*";
        }
        if (m_sOwningGroup == null || m_sOwningGroup.trim().equalsIgnoreCase(""))
        {
            m_sOwningGroup = "*";
        }
        if (m_sECRstatus == null)
        {
            m_sECRstatus = "*";
        }
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[12];
        
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_string;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { m_sFormName };
        
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { m_sChangeType };
        
        allBindVars[2] = new QuickSearchService.QuickBindVariable();
        allBindVars[2].nVarType = POM_string;
        allBindVars[2].nVarSize = 1;
        allBindVars[2].strList = new String[] { "*" };
        
        allBindVars[3] = new QuickSearchService.QuickBindVariable();
        allBindVars[3].nVarType = POM_date;
        allBindVars[3].nVarSize = 2;
        allBindVars[3].strList = new String[] { "*", "*" };
        
        allBindVars[4] = new QuickSearchService.QuickBindVariable();
        allBindVars[4].nVarType = POM_string;
        allBindVars[4].nVarSize = 1;
        allBindVars[4].strList = new String[] { "*" };
        
        allBindVars[5] = new QuickSearchService.QuickBindVariable();
        allBindVars[5].nVarType = POM_string;
        allBindVars[5].nVarSize = 1;
        allBindVars[5].strList = new String[] { "*" };
        
        allBindVars[6] = new QuickSearchService.QuickBindVariable();
        allBindVars[6].nVarType = POM_string;
        allBindVars[6].nVarSize = 1;
        allBindVars[6].strList = new String[] { m_sOwningGroup };
        
        allBindVars[7] = new QuickSearchService.QuickBindVariable();
        allBindVars[7].nVarType = POM_string;
        allBindVars[7].nVarSize = 1;
        allBindVars[7].strList = new String[] { m_sECRstatus };
        
        allBindVars[8] = new QuickSearchService.QuickBindVariable();
        allBindVars[8].nVarType = POM_date;
        allBindVars[8].nVarSize = 2;
        allBindVars[8].strList = new String[] { "*", "*" };
        
        allBindVars[9] = new QuickSearchService.QuickBindVariable();
        allBindVars[9].nVarType = POM_string;
        allBindVars[9].nVarSize = 1;
        allBindVars[9].strList = new String[] { "Approved" };
        
        allBindVars[10] = new QuickSearchService.QuickBindVariable();
        allBindVars[10].nVarType = POM_string;
        allBindVars[10].nVarSize = 1;
        allBindVars[10].strList = new String[] { "*" };
        
        allBindVars[11] = new QuickSearchService.QuickBindVariable();
        allBindVars[11].nVarType = POM_string;
        allBindVars[11].nVarSize = 1;
        allBindVars[11].strList = new String[] { "_ECNECR_" };

        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-dnholeecrforms";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-form-by-relationtype";
        allHandlerInfo[1].listBindIndex = new int[] { 12 };
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 15 };
        
        return allHandlerInfo;
    }
    
}
