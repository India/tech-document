package com.noi.rac.dhl.eco.form.compound.masters;


import javax.swing.ImageIcon;
import javax.swing.JComponent;
import com.noi.rac.dhl.eco.form.compound.data._EngChangeObjectForm_DC;
import com.noi.rac.dhl.eco.form.compound.data._EngChangeObjectForm_Data;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.form.compound.masters.BaseMasterForm;
import com.teamcenter.rac.kernel.TCAccessControlService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;

public class _EngChangeObjectForm_ extends BaseMasterForm
{

	public _EngChangeObjectForm_DC enDC;
	public _EngChangeObjectForm_Data enData;
	_EngChangeObjectForm_Panel enPanel;
	String masterFormName="";
	static boolean once=false;
	public _EngChangeObjectForm_(TCComponentForm master)
	{
		super();
		masterForm = master;

		try 
		{
		    //Mohan : All NOVLogger statements are commented out because it is failing in CITRIX
	        
			//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("_EngChangeObjectForm_");
			
			masterFormName = masterForm.getProperty("object_name");
			enDC = new _EngChangeObjectForm_DC();
			enPanel = new _EngChangeObjectForm_Panel();
			TCComponent[] releasedStats = masterForm.getTCProperty("release_status_list").getReferenceValueArray();
			TCAccessControlService accContrlSer = masterForm.getSession().getTCAccessControlService();
			if (accContrlSer.checkPrivilege(masterForm, "WRITE")&& (releasedStats.length==0)) 
			{
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("_EngChangeObjectForm_:Before Sync");
				enPanel.synchronizeWFTargets(masterForm);
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("_EngChangeObjectForm_:After Sync");
			}
			enDC.setForm(masterForm);
			enDC.populateFormComponents();
			//Populate the components and then create UI so as to get the disposition panel
			enPanel.createUI(enDC); 
			enData = new _EngChangeObjectForm_Data(enPanel);
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	public JComponent getPanel() 
	{
		return enPanel;
	}

	public ImageIcon getTabIcon() 
	{
		return null;
	}

	public String getTip() 
	{
		return null;
	}

	public void lockForm(boolean lock) 
	{

	}

	public void saveForm() 
	{
		enPanel.saveNewLifecycle();
		enDC.saveFormData();
	}

	public boolean isformSavable()
	{
		/*try 
		{
			saveNewLifecycle();
			String errString ="";
			Vector<TCComponent> strarrRevs = new Vector<TCComponent>();
			String[] lfValues = newLifecycleProp.getStringValueArray();
			
			for (int i = 0; i < lfValues.length; i++) 
			{
				String[] splVals = lfValues[i].split("::");
				strarrRevs.add(masterForm.getSession().stringToComponent(splVals[0]));
			}
			
			Vector<TCComponent> ecoTargetsVec = enPanel.getECOTargets();
			
			for (int i = 0; i < ecoTargetsVec.size(); i++) 
			{
				if (!strarrRevs.contains(ecoTargetsVec.get(i))) 
				{
					errString = errString +"\n"+ecoTargetsVec.get(i).toString();
				}
			}
			
			if (errString.trim().length()>1) 
			{
				MessageBox.post( "Select New Lifecycle value(s) for revision(s).."+errString,"Select New Lifecycle",MessageBox.WARNING);				
			}
			else
			{
				return true;
			}
			
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}*/
		
	    return true;
	}
	
	public void setDisplayProperties()
	{

	}
}