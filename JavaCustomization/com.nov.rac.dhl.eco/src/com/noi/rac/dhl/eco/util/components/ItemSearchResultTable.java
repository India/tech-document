package com.noi.rac.dhl.eco.util.components;

import java.awt.Component;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.JTreeTable;
import com.teamcenter.rac.treetable.TreeTableModel;
import com.teamcenter.rac.treetable.TreeTableNode;

public class ItemSearchResultTable extends JTreeTable 
{
	private static final long serialVersionUID = 1L;
	private ItemSearchTreeTableModel m_treeTableModel;	
	private	String[] m_columnNames = {"Item ID","Name"};
	private TCSession m_session;
	private	String m_sItemType;
	
	public ItemSearchResultTable(TCSession tcSession, int selectionMode)
	{
		this(tcSession,selectionMode, null);
	}
	
	public ItemSearchResultTable(TCSession tcSession, int selectionMode, String[] columnNames) 
	{
		super(tcSession);
		this.m_session = tcSession;
		
		/* Initialize Display column list */
		if(columnNames != null && columnNames.length > 0)
		{
			m_columnNames = columnNames;
		}
		
		/* Default Item Type decides the initial renderer */
		m_sItemType = "Nov4Part";
		
		/* Create Tree Table Model and set Table model */
		m_treeTableModel = new ItemSearchTreeTableModel(m_columnNames);
		setModel(m_treeTableModel);
		
		/* Add Columns to the Table view */
		for(int i = 0; i < m_columnNames.length; i++)
		{
			TableColumn col = new TableColumn(i);
			addColumn(col);
		}
		
		setCellRenderer();	
		
		setAutoResizeMode(JTreeTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);	
		setSelectionMode(selectionMode);
	}
	
	public void setItemType(String itemType)
	{
		m_sItemType = itemType;
	}
	
	public void setCellRenderer()
	{
		getTree().setCellRenderer(new ItemIdRenderer(this));
			
		if(m_columnNames.length >= 2)
		{
			for(int i = 1, l=m_columnNames.length; i<l; i++ )
			{
				getColumnModel().getColumn(i).setCellRenderer(new RootCellRenderer());
			}
		}
	}
	
	public int getChildCount()
	{
		return ((TreeTableNode)m_treeTableModel.getRoot()).getChildCount();
	}
	
	public void removeAllChildren()
	{
		((TreeTableNode)m_treeTableModel.getRoot()).removeAllChildren();
	}
	
	public void addNode(Vector<String> vRowData)
	{
		SearchTreeTableNode node = new SearchTreeTableNode(vRowData);
		m_treeTableModel.addRoot(node);
	}
	
	public class SearchTreeTableNode extends TreeTableNode
	{
		private static final long serialVersionUID = 1L;
		private	String		m_puid;
		private	String[] 	m_columnVals;		
		
		public SearchTreeTableNode(Vector<String> rowData)
		{
			m_puid = rowData.elementAt(0);
			m_columnVals = new String[rowData.size()-1];
			for(int i=0, l=rowData.size()-1; i<l; i++)
				m_columnVals[i] = rowData.elementAt(i+1);
		}
		
		@Override
		public String getProperty(String name)
		{
			String retStr = "";
			for(int i=0, l=m_columnNames.length; i<l; i++)
				if(m_columnNames[i].equals(name)) 
					return m_columnVals[i];
						
			return retStr;
		}
		
		public String getUID()
		{
			return m_puid;
		}	
	}

	@SuppressWarnings("unchecked")
	public class ItemSearchTreeTableModel extends TreeTableModel
	{
		private static final long serialVersionUID = 1L;
		private Vector<String> m_columnNamesVec;

		public ItemSearchTreeTableModel(String columnNames[])
		{
			super();
			m_columnNamesVec = new Vector<String>();
			TreeTableNode top = new TreeTableNode();
			setRoot(top);
			top.setModel(this);
			for(int i = 0; i < columnNames.length; i++)
			{
				m_columnNamesVec.addElement(columnNames[i]);
				modelIndexToProperty.add(columnNames[i]);
			}

		}
		
		public void addRoot(SearchTreeTableNode root)
		{
			TreeTableNode top = (TreeTableNode)getRoot();
			top.add(root);
		}

		public SearchTreeTableNode getRoot(int index)
		{
			if(((SearchTreeTableNode)getRoot()).getChildCount() > 0)
			{
				return (SearchTreeTableNode)((SearchTreeTableNode)getRoot()).getChildAt(index);
			}
			else
			{
				return null;
			}
		}

		public int getColumnCount()
		{
			return m_columnNamesVec.size();
		}

		public String getColumnName(int column)
		{
			return (String)m_columnNamesVec.elementAt(column);
		}
		public void removeNode(int row)
		{
			((SearchTreeTableNode)getRoot()).remove(getNodeForRow(row));
		}
	}	
	
	class ItemIdRenderer extends TreeTableTreeCellRenderer
	{   		
		private static final long serialVersionUID = 1L;

		public ItemIdRenderer(JTreeTable arg0) 
		{
			super(arg0);
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean sel, boolean expanded, boolean leaf, int row,
				boolean hasFocus) 
		{
			// Select the current value
			try
			{
				super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);	
				setIcon(TCTypeRenderer.getTypeIcon(m_sItemType, null));
				if(row==0)
				{
					setText("Search Result");
				}								
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			return this;
		}
	}
	
	class RootCellRenderer extends JLabel implements TableCellRenderer
	{    
		private static final long serialVersionUID = 1L;

		public RootCellRenderer()
		{			
			super();
		}

		public Component getTableCellRendererComponent(JTable table ,
				Object value , boolean isSelected , boolean hasFocus , int row ,
				int column )
		{
			if(row==0)
			{
				setText("");
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			else
			{
				setText(value.toString());
				if(isSelected)
				{
					setOpaque(true);
					setBackground(table.getSelectionBackground());
					setForeground(table.getForeground());
				}
				else
				{
					setBackground(table.getBackground());
					setForeground(table.getForeground());
				}
			}
			return this;
		}
	}
}

