/*================================================================================
                             Copyright (c) 2010 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVEnBomInfoPanel
 Package Name: com.noi.rac.en.form.compound.component
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         2010/01/02    NatarajS      File added in SVN   
 ================================================================================*/

package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentBOMLineType;
import com.teamcenter.rac.kernel.TCComponentBOMWindow;
import com.teamcenter.rac.kernel.TCComponentBOMWindowType;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCComponentBOMLineType.BOMCompareReportObject;
import com.teamcenter.rac.pse.dialogs.BOMCompareDialog;
import com.teamcenter.rac.util.MessageBox;

public class NOVDHLBomInfoPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private Vector<Vector<Object>> rowDataVectBL;
	private Vector<Vector<Object>> rowDataVect;
	private Map<String, Vector<Object>> targetItemIDVectMap;
	private Map<String, Vector<Object>> lstbaselineItemIDVectMap;
	public Set<String> qtyChangeSet;
	public Set<String> itemAddedSet;
	public Set<String> itemRemovedSet;
	public Set<String> revChangeSet;
	public TCTable bomInfoTable;
	private TCComponentItemRevision latestBaselineRev;
	private TCComponentItemRevision targetRev;
	
	public NOVDHLBomInfoPanel(TCComponentItemRevision prevRevision, TCComponentItemRevision LatestRevision)
	{
		targetRev = LatestRevision;
		latestBaselineRev = prevRevision;
		
		rowDataVect = new Vector<Vector<Object>>();
		rowDataVectBL = new Vector<Vector<Object>>();
		targetItemIDVectMap = new HashMap<String, Vector<Object>>();
		lstbaselineItemIDVectMap = new HashMap<String, Vector<Object>>();
		qtyChangeSet = new HashSet<String>();
		itemAddedSet = new HashSet<String>();
		itemRemovedSet = new HashSet<String>();
		revChangeSet = new HashSet<String>();
		
		String[] colNames = {"Seq No","Part ID","Part Name","Rev","Item Status","Qty"};
		bomInfoTable = new TCTable(colNames);
		bomInfoTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane tablePane = new JScrollPane(bomInfoTable);
		tablePane.setPreferredSize(new Dimension(700,300));
		tablePane.getViewport().setBackground(Color.WHITE);
			
		for (int i = 0; i < bomInfoTable.getColumnCount(); i++) 
		{
			bomInfoTable.getColumnModel().getColumn(i).setCellRenderer(new NOVDHLBomInfoTableRenderer(this));
		}
		
		bomInfoTable.getColumnModel().getColumn(5).setCellRenderer(new DefaultTableCellRenderer()
		{
			private static final long serialVersionUID = 5461562363756260101L;
			NOVDHLBomInfoTableRenderer.StrikeThroughRenderer strikeThrRen = new NOVDHLBomInfoTableRenderer(NOVDHLBomInfoPanel.this).strikeThrouRenderer;
			
			public Component getTableCellRendererComponent(JTable table ,
		            Object value , boolean isSelected , boolean hasFocus , int row ,
		            int column )
		    {
				Object itemIdObj = table.getValueAt(row, 1);
				String slNoObj = (String)table.getValueAt(row, 0);
				if (qtyChangeSet.contains(itemIdObj)) 
				{
					return strikeThrRen.getTableCellRendererComponent(table, value, isSelected, 
							hasFocus, row, column);
				}
				else if (itemAddedSet.contains(itemIdObj)) 
		    	{
					setBackground(Color.GREEN);
					return super.getTableCellRendererComponent(table, value, isSelected,
			                hasFocus, row, column);
				}
		    	else if ((itemRemovedSet.contains(itemIdObj)||revChangeSet.contains(itemIdObj))&& (slNoObj.trim().length()>0))
		    	{
		    		return strikeThrRen.getTableCellRendererComponent(table, value, isSelected,
			                hasFocus, row, column);
		    	}
		    	else if (revChangeSet.contains(itemIdObj)&& (slNoObj.trim().length()==0))
		    	{
		    		setBackground(Color.YELLOW);
					return super.getTableCellRendererComponent(table, value, isSelected,
			                hasFocus, row, column);
		    	}
				setBackground(table.getBackground());
				return super.getTableCellRendererComponent(table, value, isSelected,
			                hasFocus, row, column);
		    }
		});
		
		add(tablePane);
		this.setBackground(Color.WHITE);
	}
	
	public void compareBOM(boolean isEN)
	{
		if (rowDataVect.size() > 0){rowDataVect.clear();}
		if (rowDataVectBL.size() > 0){rowDataVectBL.clear();}
		if (qtyChangeSet.size() > 0){qtyChangeSet.clear();}
		if (itemAddedSet.size() > 0){itemAddedSet.clear();}
		if (itemRemovedSet.size() > 0){itemRemovedSet.clear();}
		if (revChangeSet.size() > 0){revChangeSet.clear();}
		
		if (targetRev == null  && latestBaselineRev == null) 
		{
			return ;
		}
		try 
		{
			TCComponentBOMWindowType bomWindowType = (TCComponentBOMWindowType) targetRev.getSession().getTypeComponent("BOMWindow");
			TCComponentBOMWindow baselineBomWindow =  bomWindowType.create(null);
			TCComponentBOMLine targetRevTopBomLine =  baselineBomWindow.setWindowTopLine(targetRev.getItem(), targetRev, null, null);
			AIFComponentContext[] baselineAifComps =  targetRevTopBomLine.getChildren(); 
			for (int i = 0; i < baselineAifComps.length; i++) 
			{
				Vector<Object> rowDataVec =  new Vector<Object>();
					TCComponentBOMLine bomLine = (TCComponentBOMLine)baselineAifComps[i].getComponent();
					rowDataVec.add(bomLine.getTCProperty("bl_sequence_no").getStringValue());
					rowDataVec.add(bomLine.getTCProperty("bl_item_item_id").getStringValue());
					rowDataVec.add(bomLine.getTCProperty("bl_item_object_name").getStringValue());
					rowDataVec.add(bomLine.getItemRevision().getTCProperty("current_revision_id"));
					rowDataVec.add(bomLine.getItem().getTCProperty("release_statuses"));
					String qty = bomLine.getTCProperty("bl_quantity").getStringValue();
					if (!(qty.trim().length()>0)) 
					{
						qty = "0";
					}
					rowDataVec.add(qty);
					rowDataVect.add(i, rowDataVec);
					targetItemIDVectMap.put(bomLine.getTCProperty("bl_item_item_id").getStringValue(), rowDataVec);
			}
			
			if (latestBaselineRev != null) 
			{
				//we need to pass precise conf rule
				TCComponentBOMWindow bomWindow =  bomWindowType.create(null);
				TCComponentBOMLine baselinetopBomLine =  bomWindow.setWindowTopLine(targetRev.getItem(), latestBaselineRev, null, null);
				AIFComponentContext[] aifComps =  baselinetopBomLine.getChildren(); 
				for (int i = 0; i < aifComps.length; i++) 
				{
					Vector<Object> browDataVec =  new Vector<Object>();
					TCComponentBOMLine bomLine = (TCComponentBOMLine)aifComps[i].getComponent();
					browDataVec.add(bomLine.getTCProperty("bl_sequence_no").getStringValue());
					browDataVec.add(bomLine.getTCProperty("bl_item_item_id").getStringValue());
					browDataVec.add(bomLine.getTCProperty("bl_item_object_name").getStringValue());
					String revStr = bomLine.getItemRevision().getTCProperty("current_revision_id").toString();
					//String rev = revStr.substring(0,revStr.indexOf("."));
					browDataVec.add(revStr);
					browDataVec.add(bomLine.getItem().getTCProperty("release_statuses"));
					String qty = bomLine.getTCProperty("bl_quantity").getStringValue();
					if (!(qty.trim().length()>0)) 
					{
						qty = "0";
					}
					browDataVec.add(qty);
					
					/* Rakesh:16.03.2012 - DHL-153 - Set Find No. from the current Bom Line - Starts here*/
					Vector<Object> currentBL = targetItemIDVectMap.get(browDataVec.elementAt(1));
					if(currentBL != null)
					{
						browDataVec.set(0, currentBL.get(0));
					}
					/* Rakesh:16.03.2012 - DHL-153 - Set Find No. from the current Bom Line - Ends here*/
					
					rowDataVectBL.add(i, browDataVec);
					lstbaselineItemIDVectMap.put(bomLine.getTCProperty("bl_item_item_id").getStringValue(), browDataVec);
				}			
				
				
				/*TCComponentBOMLine[] bomLines =  new TCComponentBOMLine[2];
				TCComponentBOMWindow bomWindow1 =  bomWindowType.create(null);
				TCComponentBOMWindow bomWindow2 =  bomWindowType.create(null);
				bomLines[0] = bomWindow1.setWindowTopLine(targetRev.getItem(), targetRev, null, null);
				bomLines[1] = bomWindow2.setWindowTopLine(targetRev.getItem(), latestBaselineRev, null, null);*/
				
				TCComponentBOMLineType tccomponentbomlinetype = (TCComponentBOMLineType)baselinetopBomLine.getTypeComponent();
				TCComponentBOMLineType.BOMCompareMode genericModes[] = tccomponentbomlinetype.bomCompareModes();
				
				baselinetopBomLine.bomCompareExecute(targetRevTopBomLine,genericModes[1].name,BOMCompareDialog.BOM_compare_output_report);
				
				try 
				{
					BOMCompareReportObject bomCompReportObj =  baselinetopBomLine.bomCompareReport();
					
					//String [] compareItems = bomCompReportObj.compareItems;
					String [] reportLines = bomCompReportObj.reportLines;
						
					populateBomCompareList(reportLines);
					//populating Table with qty change by adding empty row 
					for (int i = 0; i < rowDataVectBL.size(); i++) 
					{
						
						bomInfoTable.dataModel.addRow(rowDataVectBL.elementAt(i));
					}		
					
					//populating removed item list to table 
					Object[] objs = itemAddedSet.toArray();
					for (int i = 0; i < objs.length; i++) 
					{
						Vector browDataVect = (Vector)targetItemIDVectMap.get(objs[i]);
						bomInfoTable.dataModel.addRow(browDataVect);
					}
				} 
				catch (Exception e) 
				{
					//System.out.println(e);
					if(!isEN)
					MessageBox.post("No difference was found between previous Baseline and target...","Warning",MessageBox.WARNING);
					loadWorkingRevBOM();
				}
			}
			else
			{
				if(!isEN)
				MessageBox.post("No Previous Baseline found for selected Item...","No EPR",MessageBox.INFORMATION);
				loadWorkingRevBOM();
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void loadWorkingRevBOM()
	{
		for (int i = 0; i < rowDataVect.size(); i++) 
		{
			bomInfoTable.dataModel.addRow(rowDataVect.elementAt(i));
		}
	}
	
	private void populateBomCompareList(String[] reportLines) 
	{
	    //Mohan : All NOVLogger statements are commented out because it is failing in CITRIX
        
		String columnNames = reportLines[0];
		
		int itemIDIdx = columnNames.indexOf("Item Id");
		if(itemIDIdx==-1)
			itemIDIdx=columnNames.indexOf("bl_item_item_id");
		int itemNameIdx = columnNames.indexOf("Item Name");
		if(itemNameIdx==-1)
			itemNameIdx=columnNames.indexOf("bl_item_object_name");
		int itemRevIdx = columnNames.indexOf("Rev");
		int itemQtyIdx = columnNames.indexOf("Qty");
		
		System.out.println("itemIDIdx :"+itemIDIdx +"\nitemNameIdx:"+itemNameIdx+"\nitemRevIdx :"+itemRevIdx+"\nitemQtyIdx:"+itemQtyIdx);
		
		for (int i = 1; i < reportLines.length; i++) 
		{
			//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("ReportLine: " + i + "=> "+ reportLines[i]);
			
			String rowData = reportLines[i];
			String itemID = rowData.substring(itemIDIdx, itemNameIdx).trim();
			String itemName = rowData.substring(itemNameIdx, itemRevIdx).trim();
			String itemRevComp = rowData.substring(itemRevIdx, itemQtyIdx).trim();
			boolean isRevChanged = isRevisionChanged(itemID,itemRevComp);
			String itemQtyComp = rowData.substring(itemQtyIdx, rowData.length()).trim();
			
			//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("itemQtyComp: " + itemQtyComp);
			boolean isQtyChanged = isQtyChanged(itemID,itemQtyComp);
			
			//@swamy:RedLine if (isQtyChanged && !isRevChanged )
			//if (isQtyChanged && !isRevChanged && !itemRemovedSet.contains(itemID) )
			if (isQtyChanged && !isRevChanged )
			{
				Object vecObj =  lstbaselineItemIDVectMap.get(itemID);
				if (vecObj instanceof Vector) 
				{
					int vecIndex = rowDataVectBL.indexOf(vecObj);
					Vector<Object> addNewRow = new Vector<Object>();
					addNewRow.add(" ");
					addNewRow.add(" ");
					addNewRow.add(" ");
					addNewRow.add(" ");
					addNewRow.add(" ");
					addNewRow.add(itemQtyComp.split("->")[1]);
					rowDataVectBL.add(vecIndex+1, addNewRow);
				}
			}
			if (isRevChanged ) 
			{
				Object vecObj =  lstbaselineItemIDVectMap.get(itemID);
				if (vecObj instanceof Vector) 
				{
					int vecIndex = rowDataVectBL.indexOf(vecObj);
					Vector<Object> addNewRow = new Vector<Object>();
					addNewRow.add(" ");
					addNewRow.add(itemID);
					addNewRow.add(itemName);
					String rev ="";
					if (itemRevComp.contains("->")) 
					{
						rev = itemRevComp.split("->")[1];
					}
					else
					{
						rev = itemRevComp;
					}
					String qty ="";
					if (itemQtyComp.contains("->")) 
					{
						qty = itemQtyComp.split("->")[1];
					}
					else
					{
						qty = itemQtyComp;
					}
					addNewRow.add(rev);
					addNewRow.add(" ");
					addNewRow.add(qty);
					rowDataVectBL.add(vecIndex+1, addNewRow);
				}
			}
		}
	}
	
	private boolean isQtyChanged(String itemID,String itemQtyCompare) 
	{
		String[] qtys = itemQtyCompare.split("->");
		if (qtys.length==2) 
		{
			//@Swamy: REDBOMLIne
			/*if (qtys[0].equals("0") || qtys[1].equals("0")) 
			{
				com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Qty 0->0: " );
				return false;
			}*/
			//if (Integer.parseInt(qtys[0].trim()) == Integer.parseInt(qtys[1].trim())) 
			if (qtys[0].trim().equalsIgnoreCase(qtys[1].trim()))
			{
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Qty 0: "  + Integer.parseInt(qtys[0].trim()) + " ->" + Integer.parseInt(qtys[1].trim()) );
				return false;
			}
			else
			{
				//populate the qty change list
				if (!revChangeSet.contains(itemID) && !itemAddedSet.contains(itemID)&& !itemRemovedSet.contains(itemID)) 
				{
					//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Qty Changed: " + itemID );
					qtyChangeSet.add(itemID);
					return true;
				}
				
			}
		}
		return false;
	}

	private boolean isRevisionChanged(String itemID, String itemRevCompare) 
	{
		String[] revCompare = itemRevCompare.split("->");
		if (revCompare.length==2) 
		{
			if (revCompare[1].equals("()")) 
			{
				//populate removed Revision list
				//********get value from baseline
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Item removed: " + itemID );
				itemRemovedSet.add(itemID);
				return false;
			}
			else if(revCompare[0].equals("()"))
			{
				//com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Item Added: " + itemID );
				//populate added Revision list
				itemAddedSet.add(itemID);
				return false;
			}
			else if (revCompare[0].contains(revCompare[1]))
			{
				return false;
			}
			else
			{
				//revision changed
				revChangeSet.add(itemID);
				return true;
			}
		}
		return false;
	}	
}