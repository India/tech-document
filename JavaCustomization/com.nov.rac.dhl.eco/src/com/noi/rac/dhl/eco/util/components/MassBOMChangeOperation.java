package com.noi.rac.dhl.eco.util.components;

import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.utilities.common.NOVChangeManagementServiceInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;

public class MassBOMChangeOperation implements IRunnableWithProgress 
{
	private 	MassBOMChangeDialog 	m_mbc;
	private		String					m_actionCmd;
	private 	int 					m_itemsToProcessCount;
	private 	int 					m_mbcBatchSize;
	private 	boolean 				m_isValidRevAdded;
	private 	String	 				m_bomUpdateFailedItems;
	private		TCComponent[]			m_comps;
	private		TCSession				m_session;			
	private		int						m_taskLength;
	
	final 		int						m_defaultBatchSize = 10;
	final 		int						m_taskSleepTime = 50;
	final 		int						m_taskInitialProgress = 1;
	
	public MassBOMChangeOperation(MassBOMChangeDialog mbc, String actionCmd)
	{
		m_mbc = mbc;	
		m_actionCmd = actionCmd;
		m_session = (TCSession) AIFUtility.getDefaultSession();
		
		/* Initialize component list to be processed */
		/* Need to filter items for Add target to validate if items already exist in the target list */
		if(m_actionCmd.compareTo("AddItem")==0)
		{
			Vector<TCComponent> vecComp = m_mbc.getComponentsToAdd();
			m_comps = vecComp.toArray(new TCComponent[vecComp.size()]);
		}
		else
		{
			m_comps = m_mbc.getWhereUsedItems();
		}
		
		m_taskLength = m_comps.length;
		
		/* Add Item to target batch wise as determined from the Preference variable */
		TCPreferenceService prefService = ((TCSession)AIFUtility.getDefaultSession()).getPreferenceService();				
		String mbeBatchSizePrefVal = prefService.getString(TCPreferenceService.TC_preference_site, "NOV_DH_MBE_batch_process" );
		
		/* Initialise Batch Size; If no preference defined or empty then default to 10 */
		if(mbeBatchSizePrefVal == null || mbeBatchSizePrefVal.isEmpty())
		{
			m_mbcBatchSize = m_defaultBatchSize;
		}
		else
		{
			m_mbcBatchSize = Integer.parseInt(mbeBatchSizePrefVal);	
		}
		//m_mbcBatchSize = 3; //TEST
	}
	
	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException 
	{
		monitor.beginTask("Processing Mass BOM update",	m_taskLength);
		monitor.worked(m_taskInitialProgress);
				
		final IProgressMonitor finalMonitor = monitor;
		Runnable theRunnable = new Runnable() 
		{			
			@Override
			public void run()
			{			
				try 
				{					
					/* Initialize parameters for batch processing */
					int startIndex = 0;
					int itemsProcessed = 0;
					m_itemsToProcessCount = m_comps.length;
										
					/* Start processing of items in batches */
					while(m_itemsToProcessCount > 0)
					{
						int batchCount = m_itemsToProcessCount>m_mbcBatchSize?m_mbcBatchSize:m_itemsToProcessCount;
												
						TCComponent[] itemsToAdd = getBatchOfItems(startIndex, batchCount);
						if(m_actionCmd.compareTo("AddItem")==0)
						{
							//m_isValidRevAdded = m_mbc.m_ecoFormPanel.addComponentToTarget("NOVECOTargetItemsPanel", itemsToAdd);
							
							String sErrorMsg = NOVECOHelper.addTargetItemRevsToECO("NOVECOTargetItemsPanel",m_mbc.m_ecoFormPanel.ecoForm, itemsToAdd, m_mbc.getSelectedRevision());//7805
							if(sErrorMsg != null && sErrorMsg.trim().length()>0)
							{
								m_isValidRevAdded = false;
								JOptionPane.showMessageDialog(m_mbc.m_ecoFormPanel, sErrorMsg, "Cannot add Item to target", JOptionPane.WARNING_MESSAGE);
							}
							else
							{
								m_isValidRevAdded = true;
							}
						}
						else
						{	
							// Implemented As Is.Failed item list from the SOA output will be handled by UI team.
							TCComponentItemRevision[] retStr = callSOAService(getBatchOfItems(startIndex, batchCount));
							//String retStr = callUserService(getBatchOfItems(startIndex, batchCount));
							if(m_bomUpdateFailedItems == null || m_bomUpdateFailedItems.isEmpty())
							{
								//m_bomUpdateFailedItems = retStr
								m_bomUpdateFailedItems = retStr[0].toString();
							}
							else
							{
								//m_bomUpdateFailedItems = m_bomUpdateFailedItems + "," + retStr;
								m_bomUpdateFailedItems = m_bomUpdateFailedItems + "," + retStr[0].toString();
							}
						}
						m_itemsToProcessCount = m_itemsToProcessCount - batchCount;
						startIndex = startIndex + batchCount;
						itemsProcessed = itemsProcessed + batchCount;
						finalMonitor.worked(itemsProcessed);
						finalMonitor.setTaskName(Integer.toString(itemsProcessed) + " of " + m_taskLength + " Items Processed.");
						try 
						{
							m_mbc.m_ecoFormPanel.ecoForm.refresh();
						} 
						catch (TCException e) 
						{
							e.printStackTrace();
						}						
						Thread.sleep(m_taskSleepTime);
					}
					
					/* Set operation completion status */
					m_mbc.setOperationStatus(true);					
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
					finalMonitor.setCanceled(true);
					finalMonitor.done();
				}			
			} 
		};
		
		new Thread(theRunnable).start();
		
		while(!monitor.isCanceled() && !m_mbc.getOperationStatus())
		{				
		}
		
		monitor.done();		
		
		Thread.sleep(m_taskSleepTime);
		
		if ( !monitor.isCanceled() )
	    {
			if(m_actionCmd.compareTo("AddItem")==0)
			{
				m_mbc.setAddItemToTargetStatus(m_isValidRevAdded);
			}
			else
			{
				m_mbc.setBomUpdateStatus(m_bomUpdateFailedItems);
			}
	    }
	    else
	    {
	    	//User clicked Cancelled - Scenario not defined 
	    }
	}
	
	private TCComponent[] getBatchOfItems(int startIndex, int batchSize)
	{
		int index = 0;
		TCComponent[] comps = new TCComponent[batchSize];
		
		for (int i = startIndex; i < startIndex + batchSize; i++) 
		{
			comps[index++] = m_comps[i];
		}
		return comps;
	}
	
	//private String callsUserService(TCComponent[] comps) 
	private TCComponentItemRevision[] callSOAService(TCComponent[] comps) 
	{
		/*final int argSize 			= 6;
		final int indexItemToChange = 0;
		final int indexRepItem 		= 1;
		final int indexCompArr 		= 2;
		final int indexActionCmd 	= 3;
		final int indexEcoProcess 	= 4;
		final int indexEcoForm 		= 5;*/
		
		NOVChangeManagementServiceInfo novMassBOMEditService = new NOVChangeManagementServiceInfo();		
		novMassBOMEditService.setSelectedItemRevs((TCComponentItemRevision[]) comps);
		novMassBOMEditService.setComponentItem((TCComponentItemRevision) m_mbc.getItemToChange());
		
		novMassBOMEditService.setEcoForm(m_mbc.getEcoFormPanel().ecoForm);
		
		if(m_actionCmd.equals("DeleteItem"))
		{
			novMassBOMEditService.setiAction(2);
		}
		else
			if(m_actionCmd.equals("ReplaceItem"))
			{
				novMassBOMEditService.setReplacementItem( (TCComponentItemRevision) m_mbc.getReplacementItem());
				novMassBOMEditService.setiAction(1);
			}
		
		//1- replace 2-delete
		TCComponentItemRevision[] failedItemIds = novMassBOMEditService.ecoMassBOMEdit();	     
	     
		return failedItemIds;
		/*String retStr = "";
		TCUserService userService = m_session.getUserService();
		Object[] args = new Object[argSize];
		args[indexItemToChange] = m_mbc.getItemToChange();
		if (m_mbc.getReplacementItem() == null) 
		{
			args[indexRepItem] = new TCComponent();
		}
		else
		{
			args[indexRepItem] = m_mbc.getReplacementItem();	
		}
		args[indexCompArr] = comps;
		args[indexActionCmd] = m_actionCmd;
		args[indexEcoProcess] = m_mbc.getEcoFormPanel().getProcess();
		args[indexEcoForm] = m_mbc.getEcoFormPanel().ecoForm;
				
		 try 
		 {		
			Object obj = userService.call("NOV_ECO_MassBOMChange", args);
			if (obj instanceof String) 
			{
				retStr = (String)obj;				
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
			
		return retStr;
		*/
	}
	
}
