package com.noi.rac.dhl.eco.util.components;
/**
 * @author mishalt
 * Created On: July 2013
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableRowSorter;

import com.noi.util.components.NOVJComboBox;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVNovelProjectSearchPanel extends JPanel implements IObserver, ISubject, ComponentListener, ListSelectionListener
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public JTextField m_ProjectNumTxt;
    public JTextField m_projectTypeTxt;
    public NOVJComboBox m_prjctStatusCmb;
    public NOVJComboBox m_productLineCmb;
    public JTextField m_projectNameTxt;
    public JTextArea m_descTextArea;
    private JButton m_searchBtn;
    private JScrollPane m_searchedTblPane;
    private JScrollPane m_addedTblPane;
    
    private TCTable m_searchedNPTbl;
    private TCTable m_addedNPTbl;
    private AIFTableModel m_addedNPTblModel;
    private AIFTableModel m_searchedNPTblmodel;
    private TCSession m_session;
    
    public Map<String, TCComponentForm> m_formCompMap;
    public Vector<String> m_addedNPVec;
    public Set<String> m_removedNPSet;
    public String m_typeofChange;

    private ArrayList<IObserver> m_subList;
    private String[] m_productLineList = null;
    private String[] m_projectStatusList = null;
    private Registry m_registry;
    private JLabel m_searchStatusLbl;
    
    NOVECONovelProjectPanel m_novelPrjctPanel;
    
    HashMap<String, Vector<Object>> m_mapSrchdNovelForm = new HashMap<String, Vector<Object>>();
    private TableRowSorter<AIFTableModel> m_searchedResultTableSorter;
    
    
    
    public NOVNovelProjectSearchPanel()
    {
    	 
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_registry = Registry.getRegistry(this);
        m_formCompMap = new HashMap<String, TCComponentForm>();
        m_addedNPVec = new Vector<String>();
        m_removedNPSet = new HashSet<String>();
        m_subList = new ArrayList<IObserver>();
        loadListOfValues();
        JPanel searchCriCompPanel = buildSearchCriteria();
        JPanel resultTablePanel = initTableUI();
        
        this.setLayout(new VerticalLayout(0));
        this.add("top.bind.center.center", searchCriCompPanel);
        this.add("top.bind.center.center", resultTablePanel);
        addComponentListener(this);
    	
    }
    
    public NOVNovelProjectSearchPanel(NOVECONovelProjectPanel novelPrjctPanel)
    {
        m_novelPrjctPanel = novelPrjctPanel; 
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_registry = Registry.getRegistry(this);
        m_formCompMap = new HashMap<String, TCComponentForm>();
        m_addedNPVec = new Vector<String>();
        m_removedNPSet = new HashSet<String>();
        m_subList = new ArrayList<IObserver>();
        loadListOfValues();
        JPanel searchCriCompPanel = buildSearchCriteria();
        JPanel resultTablePanel = initTableUI();
        
        this.setLayout(new VerticalLayout(0));
        this.add("top.bind.center.center", searchCriCompPanel);
        this.add("top.bind.center.center", resultTablePanel);
        addComponentListener(this);
    }
    /*
     * Method buildSearchCriteria(): Building seach panel
     */
    
    private JPanel buildSearchCriteria()
    {
        JPanel searchCriPanel = new JPanel();

        GridBagLayout gbl_searchCriPanel = new GridBagLayout();
        gbl_searchCriPanel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_searchCriPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
        gbl_searchCriPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0,
                0.0, Double.MIN_VALUE };
        gbl_searchCriPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        searchCriPanel.setLayout(gbl_searchCriPanel);

        buildInitialRowSearchCriPanel(searchCriPanel);
        buildSecondRowSearchCriPanel(searchCriPanel);
        buildLastRowSearchCriPanel(searchCriPanel);
        initializeListeners();
        
        return searchCriPanel;
    }
    
    private void initializeListeners()
    {
    	m_ProjectNumTxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    populateSearchResults();
                }
            }
        });
    	
    	m_projectTypeTxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    populateSearchResults();
                }
            }
        });
    	
    	m_searchBtn.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent mouseevent)
            {
                populateSearchResults();
            }
        });
    	
    	 m_productLineCmb.addKeyListener(new KeyAdapter()
         {
             public void keyReleased(KeyEvent ke)
             {
                 int keyCode = ke.getKeyCode();
                 if (keyCode == KeyEvent.VK_ENTER)
                 {
                     populateSearchResults();
                 }
             }
         });
    	
    	 m_prjctStatusCmb.addKeyListener(new KeyAdapter()
         {
             public void keyReleased(KeyEvent ke)
             {
                 int keyCode = ke.getKeyCode();
                 if (keyCode == KeyEvent.VK_ENTER)
                 {
                     populateSearchResults();
                 }
             }
         });
    	 
    	 m_projectNameTxt.addKeyListener(new KeyAdapter()
         {
             public void keyReleased(KeyEvent ke)
             {
                 int keyCode = ke.getKeyCode();
                 if (keyCode == KeyEvent.VK_ENTER)
                 {
                     populateSearchResults();
                 }
             }
         });
    }
    private void buildInitialRowSearchCriPanel(JPanel searchCriPanel)
    {
    	JLabel projectLbl = new JLabel(m_registry.getString("prjctNum.LBL"));
        GridBagConstraints gbc_prjctNoLbl = new GridBagConstraints();
        gbc_prjctNoLbl.anchor = GridBagConstraints.WEST;
        gbc_prjctNoLbl.insets = new Insets(0, 5, 5, 5);
        gbc_prjctNoLbl.gridx = 0;
        gbc_prjctNoLbl.gridy = 0;
        searchCriPanel.add(projectLbl, gbc_prjctNoLbl);

        m_ProjectNumTxt = new JTextField(14);
        
        GridBagConstraints gbc_prjctNumTxt = new GridBagConstraints();
        gbc_prjctNumTxt.insets = new Insets(0, 0, 5, 5);
        gbc_prjctNumTxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_prjctNumTxt.gridx = 1;
        gbc_prjctNumTxt.gridy = 0;
        searchCriPanel.add(m_ProjectNumTxt, gbc_prjctNumTxt);
        m_ProjectNumTxt.setColumns(10);

        JLabel prjctTypeLbl = new JLabel(m_registry.getString("prjctType.LBL"));
        GridBagConstraints gbc_prjctTypeLbl = new GridBagConstraints();
        gbc_prjctTypeLbl.anchor = GridBagConstraints.WEST;
        gbc_prjctTypeLbl.insets = new Insets(0, 10, 5, 5);
        gbc_prjctTypeLbl.gridx = 5;
        gbc_prjctTypeLbl.gridy = 0;
        searchCriPanel.add(prjctTypeLbl, gbc_prjctTypeLbl);

        m_projectTypeTxt = new JTextField(14);
        
        GridBagConstraints gbc_prjctTypeTxt = new GridBagConstraints();
        gbc_prjctTypeTxt.insets = new Insets(0, 0, 5, 5);
        gbc_prjctTypeTxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_prjctTypeTxt.gridx = 6;
        gbc_prjctTypeTxt.gridy = 0;
        searchCriPanel.add(m_projectTypeTxt, gbc_prjctTypeTxt);
        m_projectTypeTxt.setColumns(10);

        /* ****************************** Search Button *********************** */
        m_searchBtn = new JButton();
        ImageIcon searchIcon = m_registry.getImageIcon("search.ICON");
        m_searchBtn.setIcon(searchIcon);
        m_searchBtn.setPreferredSize(new Dimension(20, 20));
        
        GridBagConstraints gbc_searchBtn = new GridBagConstraints();
        gbc_searchBtn.insets = new Insets(0, 0, 5, 0);
        gbc_searchBtn.anchor = GridBagConstraints.WEST;
        gbc_searchBtn.gridx = 7;
        gbc_searchBtn.gridy = 0;
        searchCriPanel.add(m_searchBtn, gbc_searchBtn);
    }
    private void buildSecondRowSearchCriPanel(JPanel searchCriPanel)
    {
    	JLabel productLineLbl = new JLabel(m_registry.getString("productLine.LBL"));
        GridBagConstraints gbc_productLineLbl = new GridBagConstraints();
        gbc_productLineLbl.anchor = GridBagConstraints.WEST;
        gbc_productLineLbl.insets = new Insets(0, 5, 5, 5);
        gbc_productLineLbl.gridx = 0;
        gbc_productLineLbl.gridy = 1;
        searchCriPanel.add(productLineLbl, gbc_productLineLbl);

        m_productLineCmb = new NOVJComboBox(m_productLineList);
        m_productLineCmb.setSelectedIndex(-1);
        GridBagConstraints gbc_productLineCmb = new GridBagConstraints();
        gbc_productLineCmb.insets = new Insets(0, 0, 5, 5);
        gbc_productLineCmb.fill = GridBagConstraints.HORIZONTAL;
        gbc_productLineCmb.gridx = 1;
        gbc_productLineCmb.gridy = 1;
        searchCriPanel.add(m_productLineCmb, gbc_productLineCmb);
        
        JLabel projectStatusLbl = new JLabel(m_registry.getString("prjctStatus.LBL"));
        GridBagConstraints gbc_prjctStatusLbl = new GridBagConstraints();
        gbc_prjctStatusLbl.insets = new Insets(0, 10, 5, 5);
        gbc_prjctStatusLbl.gridx = 5;
        gbc_prjctStatusLbl.gridy = 1;
        searchCriPanel.add(projectStatusLbl, gbc_prjctStatusLbl);
        m_prjctStatusCmb = new NOVJComboBox(m_projectStatusList);
        m_prjctStatusCmb.setSelectedIndex(1);
        
        GridBagConstraints gbc_prjctStatusTxt = new GridBagConstraints();
        gbc_prjctStatusTxt.insets = new Insets(0, 0, 5, 5);
        gbc_prjctStatusTxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_prjctStatusTxt.gridx = 6;
        gbc_prjctStatusTxt.gridy = 1;
        searchCriPanel.add(m_prjctStatusCmb, gbc_prjctStatusTxt);

    }
    private void buildLastRowSearchCriPanel(JPanel searchCriPanel)
    {
    	JLabel projectNameLbl = new JLabel(m_registry.getString("prjctName.LBL"));
        GridBagConstraints gbc_prjctNameLbl = new GridBagConstraints();
        gbc_prjctNameLbl.anchor = GridBagConstraints.WEST;
        gbc_prjctNameLbl.insets = new Insets(0, 5, 5, 5);
        gbc_prjctNameLbl.gridx = 0;
        gbc_prjctNameLbl.gridy = 2;
        searchCriPanel.add(projectNameLbl, gbc_prjctNameLbl);

        m_projectNameTxt = new JTextField();
 
        GridBagConstraints gbc_prjctNameTxt = new GridBagConstraints();
        gbc_prjctNameTxt.insets = new Insets(0, 0, 5, 5);
        gbc_prjctNameTxt.fill = GridBagConstraints.HORIZONTAL;
        gbc_prjctNameTxt.gridx = 1;
        gbc_prjctNameTxt.gridy = 2;
        searchCriPanel.add(m_projectNameTxt, gbc_prjctNameTxt);
    	
    }
    private void buildSearchedNPTable()
    {
    	String[] columnNames =  m_registry.getStringArray("seachTableColumns.Name");
        m_searchedNPTblmodel = new AIFTableModel(columnNames);
    	
        m_searchedNPTbl = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        m_searchedNPTbl.setModel(m_searchedNPTblmodel);
        
        m_searchedNPTbl.getSelectionModel().addListSelectionListener(this);
        m_searchedNPTbl.getColumnModel().getSelectionModel().addListSelectionListener(this);
        m_searchedNPTbl.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        m_searchedNPTbl.getTableHeader().setReorderingAllowed(false);
        m_searchedNPTbl.getColumnModel().getColumn(0).setResizable(false);
        m_searchedNPTbl.getColumnModel().getColumn(0).setMaxWidth(25);
        m_searchedNPTbl.setSortEnabled(false);
        m_searchedNPTbl.setRowHeight(25);
        
        m_searchedTblPane = new JScrollPane(m_searchedNPTbl);
        m_searchedTblPane.setPreferredSize(new Dimension(500, 170));
       
        
    }
    
    private void createSearchResultNPTblSorter()//7367
    {
    	m_searchedResultTableSorter =  new TableRowSorter<AIFTableModel>(m_searchedNPTblmodel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                m_searchedNPTbl.setRowSorter(this);
                this.sort();
            }
        };
        try
        {
            List<SortKey> keys = new ArrayList<SortKey>();
            SortKey sortKey = new SortKey(m_searchedNPTblmodel.findColumn("Project#"), SortOrder.ASCENDING);
            keys.add(sortKey);
            m_searchedResultTableSorter.setSortKeys(keys);
            m_searchedNPTbl.setRowSorter(m_searchedResultTableSorter);
            m_searchedResultTableSorter.setSortable(1, false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    	
    }
    
    private void buildAddedNPTable()
    {
    	String[] columnNames =  m_registry.getStringArray("seachTableColumns.Name");
    	m_addedNPTblModel = new AIFTableModel(columnNames);
        m_addedNPTbl = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        m_addedNPTbl.setModel(m_addedNPTblModel);
        
        m_addedNPTbl.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        m_addedNPTbl.getTableHeader().setReorderingAllowed(false);
        m_addedNPTbl.getColumnModel().getColumn(0).setResizable(false);
        m_addedNPTbl.getColumnModel().getColumn(0).setMaxWidth(25);
        m_addedNPTbl.getSelectionModel().addListSelectionListener(this);
        m_addedNPTbl.getColumnModel().getSelectionModel().addListSelectionListener(this);
        m_addedNPTbl.setSortEnabled(false);
        m_addedNPTbl.setRowHeight(25);
        m_addedTblPane = new JScrollPane(m_addedNPTbl);
        m_addedTblPane.setPreferredSize(new Dimension(500, 145));
        
        
    }
    
    
    
    private void createCellEditorAndRenderer()
    {
    	m_searchedNPTbl.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(true));
        m_searchedNPTbl.getColumnModel().getColumn(0).setCellEditor(new NOVNovelFormTableButtonEditor(NOVNovelProjectSearchPanel.this, m_searchedNPTbl, m_addedNPTbl));
        m_searchedNPTbl.getColumnModel().getColumn(0).setPreferredWidth(18);
        
        DefaultTableCellRenderer tblRenderer = new DefaultTableCellRenderer();
        tblRenderer.setVerticalAlignment(JLabel.TOP);
        
        m_searchedNPTbl.getColumnModel().getColumn(1).setCellRenderer(tblRenderer);
        m_searchedNPTbl.getColumnModel().getColumn(2).setCellRenderer(tblRenderer);
        m_searchedNPTbl.getColumnModel().getColumn(3).setCellRenderer(tblRenderer);
        m_searchedNPTbl.getColumnModel().getColumn(4).setCellRenderer(tblRenderer);
        
        m_addedNPTbl.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(false));
        m_addedNPTbl.getColumnModel().getColumn(0) .setCellEditor(new NOVNovelFormTableButtonEditor(NOVNovelProjectSearchPanel.this, null, m_addedNPTbl));
        m_addedNPTbl.getColumnModel().getColumn(0).setPreferredWidth(18);
        
        
        
        m_addedNPTbl.getColumnModel().getColumn(1).setCellRenderer(tblRenderer);
        m_addedNPTbl.getColumnModel().getColumn(2).setCellRenderer(tblRenderer);
        m_addedNPTbl.getColumnModel().getColumn(3).setCellRenderer(tblRenderer);
        m_addedNPTbl.getColumnModel().getColumn(4).setCellRenderer(tblRenderer);
    }
    /*
     * Method: initTableUI() Initialising Table UI
     */
    private JPanel initTableUI()
    {
    	buildSearchedNPTable();
    	buildAddedNPTable();
    	createCellEditorAndRenderer();

        JPanel srchdTablePanel = new JPanel(new VerticalLayout());
        srchdTablePanel.add("top.bind.center.center", m_searchedTblPane);
        
        JPanel addedTablePanel = new JPanel(new VerticalLayout());
        addedTablePanel.add("top.bind.center.center", m_addedTblPane);
        
        TitledBorder tb_srchNP = new javax.swing.border.TitledBorder(m_registry.getString("searchNovelFrmTbl.LBL"));
        tb_srchNP.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        tb_srchNP.setTitleColor(Color.BLACK);
        srchdTablePanel.setBorder(tb_srchNP);
        
        JPanel novelDescPanel = createNovelDescUI(); //7786
        
        TitledBorder tb_addedNP = new javax.swing.border.TitledBorder(m_registry.getString("addedNovelFrmTbl.LBL"));
        tb_addedNP.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        tb_addedNP.setTitleColor(Color.BLACK);
        addedTablePanel.setBorder(tb_addedNP);
        //start
        JPanel m_searchResultStatusPanel = new JPanel(new VerticalLayout());
        m_searchStatusLbl = new JLabel("");
        m_searchStatusLbl.setFont(new Font(m_searchStatusLbl.getFont().getFamily(), Font.BOLD, m_searchStatusLbl
                .getFont().getSize()));
        m_searchStatusLbl.setForeground(Color.RED);
        m_searchStatusLbl.setAlignmentX(CENTER_ALIGNMENT);
        m_searchStatusLbl.setAlignmentY(CENTER_ALIGNMENT);
        m_searchResultStatusPanel.add("top.bind.center.center", m_searchStatusLbl);

        JPanel tablePanel = new JPanel(new VerticalLayout());
        tablePanel.add(srchdTablePanel, BorderLayout.CENTER);
        tablePanel.add(novelDescPanel, BorderLayout.CENTER);//7786
        tablePanel.add(addedTablePanel, BorderLayout.CENTER);
        tablePanel.add(m_searchResultStatusPanel, BorderLayout.CENTER);

        return tablePanel;
    }
    
    private JPanel createNovelDescUI()
    {
        JPanel novelDescPanel = new JPanel(new VerticalLayout());

        m_descTextArea = new JTextArea();
        m_descTextArea.setLineWrap(true);
        m_descTextArea.setWrapStyleWord(true);
        m_descTextArea.setEditable(false);
        JScrollPane novelDescPane = new JScrollPane(m_descTextArea);
        novelDescPane.setPreferredSize(new Dimension(500, 70)); 
        
        TitledBorder tb_descNP = new javax.swing.border.TitledBorder("Novel Engineering Project Description");
        tb_descNP.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        tb_descNP.setTitleColor(Color.BLACK);
        novelDescPanel.setBorder(tb_descNP);  
        novelDescPanel.add("top.bind.center.center", novelDescPane);
        
        return novelDescPanel;
    }
    
    /*
     * Method: loadListOfValues() Loads LOVs using preferences
     */
    private void loadListOfValues()
    {
        TCPreferenceService prefServ = ((TCSession)m_session).getPreferenceService();
        m_productLineList = prefServ.getStringArray(TCPreferenceService.TC_preference_site, "NOV_DHL_NovelProject_ProductLine");
        m_projectStatusList = prefServ.getStringArray(TCPreferenceService.TC_preference_site, "NOV_DHL_NovelProject_ProjectStatus");
    }
    
    /*
     * Method: populateSearchResults() Populate search results based on i/p
     */
    private void populateSearchResults()
    {
        NOVSearchNovelFormOperation searchOp = new NOVSearchNovelFormOperation(NOVNovelProjectSearchPanel.this);
        m_mapSrchdNovelForm = searchOp.executeSearch();
        
        clearSearchFormTable();
        m_searchStatusLbl.setText("");
        String[] strForms = null; 
        if(m_mapSrchdNovelForm != null && m_mapSrchdNovelForm.size() > 0)
        {
            strForms =  m_mapSrchdNovelForm.keySet().toArray(new String[m_mapSrchdNovelForm.size()]);
            for (int i = 0; i < strForms.length;i++)
            {
                Vector<String> rowData = new Vector<String>();
                Vector<Object> vectStrRow = new Vector<Object>();
                vectStrRow = m_mapSrchdNovelForm.get(strForms[i]);
                rowData.add(strForms[i]);
                rowData.add((String) vectStrRow.elementAt(0));
                rowData.add((String) vectStrRow.elementAt(1));
                rowData.add((String) vectStrRow.elementAt(2));
                rowData.add((String) vectStrRow.elementAt(3));
                m_searchedNPTblmodel.addRow(rowData);
            }
            m_searchStatusLbl.setForeground(Color.BLACK);
            m_searchStatusLbl.setText(m_mapSrchdNovelForm.size() +" "+ m_registry.getString("FormFound.MSG"));//TCDECREL-7464
        }
        else
        {
            //JOptionPane.showMessageDialog(this, m_registry.getString("NoObjectFound.MSG"), m_registry.getString("searchResults.MSG"), JOptionPane.INFORMATION_MESSAGE);
        	m_searchStatusLbl.setForeground(Color.RED);
        	m_searchStatusLbl.setText( m_registry.getString("FormSeachResult.MSG"));
        }
        createSearchResultNPTblSorter();//7367
        
    }
    /*
     * Method: getSelectedForms() Fetch selected forms in Vector<TCComponentForm> format
     */
    public Vector<TCComponentForm> getSelectedForms()
    {
    	fillFormComponentMap();
        Vector<TCComponentForm> selectedForms = new Vector<TCComponentForm>();
        for(int i = 0; i< m_addedNPVec.size(); i++)
        {
            if(m_formCompMap.containsKey(m_addedNPVec.get(i)))
            {
                selectedForms.add(m_formCompMap.get(m_addedNPVec.get(i)));
            }
        }
        return selectedForms;
    }
    
    /*
     * Method: getSelectedFormsArray() Fetch selected forms in TCComponent Array format
     */
    public TCComponent[] getSelectedFormsArray()
    {
    	TCComponent[] selectedFormsArray = null;
    	Vector<TCComponentForm> selectedForms = getSelectedForms();
    	if(selectedForms.size() >0 )
    	{
    		selectedFormsArray = selectedForms.toArray(new TCComponent[0]);
    	}
    	return (TCComponent[]) (selectedFormsArray == null ? new TCComponent[0] : selectedFormsArray);
    }
    
    
    /*
     * Method: populateFormOnTable() Responsible to populate forms on table based on i/p  Vector<TCComponentForm>
     */
    public void populateFormOnTable(Vector<TCComponentForm> compForms)
    {
        if (compForms != null)
        {
            for (int i = 0; i < compForms.size(); i++)
            {
                Vector<Object> rowData = getRowData(compForms.get(i));
                m_addedNPTblModel.addRow(rowData);
                try
                {
                    String strPrjctNum = compForms.get(i).getTCProperty("nov4_project_number").toString();
                    if (!m_addedNPVec.contains(strPrjctNum))
                    {
                        m_addedNPVec.add(strPrjctNum);
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
        m_addedNPTbl.updateUI();
    }
    /*
     * Method getRowData(): Responsible to fetch Row data.
     * I/P: TCComponentForm form
     * O/P: Vector<Object>
     */
    public Vector<Object> getRowData(TCComponentForm form)
    {
        Vector<Object> rowData = new Vector<Object>();
        String[] sProperties = m_registry.getStringArray("NovelFormProperties.Name");
        
        TCProperty[] sRetrievedProp = null;
        try
        {
            sRetrievedProp = form.getTCProperties(sProperties);
            String strPuid = form.getUid();
            rowData.add(strPuid);
            rowData.add(sRetrievedProp[0].getStringValue());
            rowData.add(sRetrievedProp[1].getStringValue());
            rowData.add(sRetrievedProp[2].getStringValue());
            rowData.add(sRetrievedProp[3].getStringValue());
            m_formCompMap.put(strPuid, form);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return rowData;
    }
    
    /*
     * Method: synchronizeNovelFormList: Synchs NovelForm List on given input Vector<TCComponentForm>
     */
    protected void synchronizeNovelFormList(Vector<TCComponentForm> novelFormVec)
    {
        Vector<TCComponentForm> selforms = getSelectedForms();        
        try
        {
        	addFormToPanel(selforms,novelFormVec);
        	removeFromPanel(novelFormVec);
            m_novelPrjctPanel.m_ECOFormPanel.ecoForm.refresh();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void addFormToPanel(Vector<TCComponentForm> selforms,Vector<TCComponentForm> novelFormVec)
    {
    	if ((selforms != null) && (selforms.size() > 0))
        {
            for (int i = 0; i < selforms.size(); i++)
            {
                if (!novelFormVec.contains(selforms.get(i)))
                {
                    try {
						m_novelPrjctPanel.m_ECOFormPanel.ecoForm.add("Nov4_DHL_EC_NovelProject", selforms.get(i));
					} catch (TCException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
            }
        }
    }
    
    private void removeFromPanel(Vector<TCComponentForm> novelFormVec)
    {
    	try
    	{
	    	for(int i=0;i<novelFormVec.size();i++)
	        {
	            TCComponentForm compForm = novelFormVec.get(i);
	            String strForm = compForm.getTCProperty("nov4_project_number").toString();
	            if(m_removedNPSet.contains(strForm))
	            {
	                m_novelPrjctPanel.m_ECOFormPanel.ecoForm.remove("Nov4_DHL_EC_NovelProject", compForm);
	            }
	        }
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    }
    
    /*
     * Method: clearSearchFormTable() Clears Search table
     */
    public void clearSearchFormTable()
    {
        m_searchedNPTbl.setRowSorter(null);
        m_searchedNPTblmodel.removeAllRows();
    }
    
    /*
     * Method: clearAddedFormTable() Clears Add Table
     */
    public void clearAddedFormTable()
    {
        m_addedNPTbl.setRowSorter(null);
        m_addedNPTblModel.removeAllRows();
    }
    
    @Override
    public void valueChanged(ListSelectionEvent event) //7786
    {
        String sFormPuid = null;
        if (event.getSource() == m_searchedNPTbl.getSelectionModel() && m_searchedNPTbl.getRowSelectionAllowed())
        {
            if(m_searchedNPTbl.getSelectedRow() != -1)
            {
                sFormPuid = (String) m_searchedNPTbl.getValueAt(m_searchedNPTbl.getSelectedRow(), 0);
            }
            if (sFormPuid != null)
            {
                try
                {
                    TCComponent comp = m_session.stringToComponent(sFormPuid);
                    AIFComponentContext[] context =comp.whereReferenced();
                    TCComponentForm formComp = (TCComponentForm) context[0].getComponent();
                    displayNovelDescription(formComp);
                }
                catch (TCException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void componentResized(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        System.out.println(">>>>>>>>>>>>>>>>componentResized<<<<<<<<<<");
    }

    @Override
    public void componentMoved(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        System.out.println(">>>>>>>>>>>>>>>>componentMoved<<<<<<<<<<");
    }

    @Override
    public void componentShown(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        System.out.println(">>>>>>>>>>>>>>>>componentShown<<<<<<<<<<");
    }

    @Override
    public void componentHidden(ComponentEvent componentevent)
    {
        // TODO Auto-generated method stub
        System.out.println(">>>>>>>>>>>>>>>>componentHidden<<<<<<<<<<");
    }

    @Override
    public void notifyObserver()
    {
        for (int i = 0; i < m_subList.size(); i++)
        {
            m_subList.get(i).update();
        }
    }

    @Override
    public void registerObserver(IObserver subs)
    {
        m_subList.add(subs);
    }

    @Override
    public void removeObserver(IObserver subs)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void update()
    {
        // TODO Auto-generated method stub
        
    }
    private void fillFormComponentMap()
    {
        	String[] strForms = null; 
            if(m_mapSrchdNovelForm != null && m_mapSrchdNovelForm.size() > 0)
            {
                strForms =  m_mapSrchdNovelForm.keySet().toArray(new String[m_mapSrchdNovelForm.size()]);
                TCComponent comp[] = null;
    	        try
    	        {
    	            comp = m_session.stringToComponent(strForms);
    	            for (int i = 0; i < strForms.length; i++) {
    	            		Vector<Object> vectStrRow = new Vector<Object>();
    	            		vectStrRow = m_mapSrchdNovelForm.get(strForms[i]);
    	                    AIFComponentContext[] context =comp[i].whereReferenced();
    	                    TCComponentForm formComp = (TCComponentForm) context[0].getComponent();
    	                    m_formCompMap.put((String) vectStrRow.elementAt(0),formComp);
    					
    				}
    	        }
    	        catch (TCException e1)
    	        {
    	            e1.printStackTrace();
    	        }	
            }
    }
    
    private void displayNovelDescription(TCComponentForm form) throws TCException
    {
        if (form != null)
        {
            m_descTextArea.setText(form.getTCProperty("object_desc").getStringValue());
        }
        else
        {
            m_descTextArea.setText("");
        }
    }
}

