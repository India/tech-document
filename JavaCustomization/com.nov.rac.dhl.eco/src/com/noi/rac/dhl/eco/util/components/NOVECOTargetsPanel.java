package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetItemsTable.NovTreeTableNode;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTableModel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.PropertyLayout;

public class NOVECOTargetsPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	public TCComponent[] refDispItemsArr;
	public _EngChangeObjectForm_Panel engChangeObjFormPanel;
	NOVECOTargetItemsTable targetItemsTable;
	TCTableModel targetItemsTableModel;
	//public Map<String, TCComponent> dispMap;
	DispositionItemDialog dispItemDialog;
	NOVECODispositionTabPanel dispPanel;
	
	public NOVECOTargetsPanel( _EngChangeObjectForm_Panel _engChangeObjFormPanel, TCComponent[] _refDispItemsArr )
	{		
		if( this.engChangeObjFormPanel != _engChangeObjFormPanel )
			engChangeObjFormPanel = _engChangeObjFormPanel;
		if( this.refDispItemsArr != _refDispItemsArr )
			this.refDispItemsArr = _refDispItemsArr;
			
		//dispMap = new HashMap<String, TCComponent>();
	}

	public void createUI()
	{
		String[] columnNames = { "Item Number", "Name",/*"Type",*/"Old Rev", "New Rev", "Old Lifecycle", "New Lifecycle" };

		final TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		targetItemsTable = new NOVECOTargetItemsTable( engChangeObjFormPanel, tcSession, columnNames,true );
		
		addTargetItems();
		targetItemsTable.addMouseListener( new MouseAdapter() 
		{
			public void mouseClicked( MouseEvent mouseEvent ) 
			{
				JTable table = (JTable)mouseEvent.getSource();
				int row = table.getSelectedRow();
				if(row < 0)
				{
					return;
				}
				
				dispPanel.saveData();
				previewTargetItemProperties();
				
				if( mouseEvent.getClickCount() == 2 )
				{
					displayTargetItemPropertiesDialog();
					previewTargetItemProperties();//4760
				}
			}						
		} );		
		
		
		addPopupMenu();
		
		JScrollPane trgPane = new JScrollPane( targetItemsTable );
		trgPane.setLocation( 300, 200 );
		
		//trgPane.setPreferredSize( new Dimension( 662, 1150 ) );
		trgPane.setPreferredSize( new Dimension( 662, 350 ) );
		
		trgPane.getViewport().setBackground( Color.white );
		
		JPanel targetItemsTablePanel = new JPanel( new PropertyLayout() );
		targetItemsTablePanel.add( "1.1.left.center", trgPane );

		//add( "1.1.left.center", targetItemsTablePanel );
		
		dispPanel = new NOVECODispositionTabPanel( engChangeObjFormPanel, engChangeObjFormPanel.ecoForm.getSession(), null, null);
		dispPanel.setPreviewMode(true);
		dispPanel.setDispositionComps(refDispItemsArr);
		dispPanel.createUI();		
		dispPanel.setEditable(false);
		dispPanel.setDispositionEditable(false);//4474
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add( targetItemsTablePanel );
		add( dispPanel );
		
		
		//TableColumn tableColumn = targetItemsTable.getColumnModel().getColumn( 6 );
		TableColumn tableColumn = targetItemsTable.getColumnModel().getColumn( 5 );
		targetItemsTable.removeColumn( tableColumn );
	}
	
	public void previewTargetItemProperties()
	{
		int row = targetItemsTable.getSelectedRow();
		if(row == 0)
		{
			dispPanel.clearFields();
			dispPanel.setEditable(false);
			dispPanel.setDispositionEditable(false);//4474
		}
		else
		{		
			TCComponent dispComponent = ( ( NovTreeTableNode ) targetItemsTable.getNodeForRow( row ) ).dispComp;
			TCComponent targetRev = ( ( NovTreeTableNode ) targetItemsTable.getNodeForRow( row ) ).targetRev;	
			
			dispPanel.loadData(dispComponent, targetRev);
			if(engChangeObjFormPanel.isEcoReleased)
			{
				dispPanel.setEditable(false);
				dispPanel.setDispositionEditable(false);//4474
			}
			else
			{
				enableDispositionDataFileds();//4474
				
			}			
		}
	}
	//4474-start
	private void enableDispositionDataFileds()
	{
		TCReservationService      reserserv;
		reserserv = engChangeObjFormPanel.ecoForm.getSession().getReservationService();	
		
		try {
			/*if(reserserv.isReserved(engChangeObjFormPanel.ecoForm) ) 
			{
				dispPanel.setEditable(true);
				dispPanel.setDispositionEditable(true);
			
			}
			else//if the form is check in
			{
				dispPanel.setEditable(true);
				dispPanel.setDispositionEditable(false);
				
			}*/
			//4907-start
			if(! engChangeObjFormPanel.ecoForm.okToModify())
			{
				dispPanel.setDispositionEditable(false);
			}
			else if(reserserv.isReserved(engChangeObjFormPanel.ecoForm))
			{
				dispPanel.setDispositionEditable(true);
			}
			else
			{
				dispPanel.setDispositionEditable(false);
			}
			//4907-end
		} catch (TCException e) {
			
			e.printStackTrace();
		}
		
	}
	//4474-end
	private void addTargetItems()
	{
		Vector<TCComponent> ecoTargetItemsVec = engChangeObjFormPanel.getECOTargets();
		TCComponent[] ecoTargetItemsArr = ( ecoTargetItemsVec.toArray( new TCComponent[ecoTargetItemsVec.size()] ) );
		
		String[] reqdDispProperties = { "targetitemid" };
		String[][] retrivedDispProperties = null;
		
		String[] reqdRevProperties = { "item_id" };
		String[][] retrivedRevProperties = null;
		
		try
		{

		    List<TCComponent> refDispItemsList = Arrays.asList(refDispItemsArr);//TC10.1 Upgrade
			//retrivedDispProperties = TCComponentType.getPropertiesSet( refDispItemsArr, reqdDispProperties );
			retrivedDispProperties = TCComponentType.getPropertiesSet( refDispItemsList, reqdDispProperties );//TC10.1 Upgrade
			List<TCComponent> ecoTargetItemsList = Arrays.asList(ecoTargetItemsArr);//TC10.1 Upgrade
			//retrivedRevProperties = TCComponentType.getPropertiesSet( ecoTargetItemsArr, reqdRevProperties );
			retrivedRevProperties = TCComponentType.getPropertiesSet( ecoTargetItemsList, reqdRevProperties );
		}
		catch( TCException e1 )
		{
			e1.printStackTrace();
		}

		TCComponent[] tempECOTargetItemsArr = new TCComponent[refDispItemsArr.length];
		for( int index = 0; index < refDispItemsArr.length; ++index )
		{
			for( int indexTarget = 0; indexTarget < ecoTargetItemsVec.size(); ++indexTarget )
			{
				if( retrivedDispProperties[index][0].equals( retrivedRevProperties[indexTarget][0] ) )
					tempECOTargetItemsArr[index] = ecoTargetItemsArr[indexTarget];
			}
			if(tempECOTargetItemsArr[index] != null)
			{
				targetItemsTable.addTarget( tempECOTargetItemsArr[index], refDispItemsArr[index] );
			}
		}
	}
	
	private void addPopupMenu()
	{
		targetItemsTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed( java.awt.event.MouseEvent mouseEvt )
			{
				Object source = mouseEvt.getSource();
				TreeTableNode[] treeNode = ( ( NOVECOTargetItemsTable )source ).getSelectedNodes();
				if( treeNode.length == 1 ) 
				{
					if( treeNode[0] instanceof NovTreeTableNode )
					{
						if( !( ( TreeTableNode )treeNode[0].getParent() ).isRoot() ) 
						{
							return;
						}
					}
				}
				int selRow = ( ( NOVECOTargetItemsTable )source ).getSelectedRow();
				
				try 
				{
					TCReservationService resService = engChangeObjFormPanel.ecoForm.getSession().getReservationService();
					boolean isReserved = resService.isReserved( engChangeObjFormPanel.ecoForm );
					if( SwingUtilities.isRightMouseButton( mouseEvt ) && ( mouseEvt.getClickCount() == 1 )&& selRow >0 &&( isReserved ) )
					{
						JPopupMenu popupMenu = new JPopupMenu();
						JMenuItem item = new JMenuItem();
						String initialText = "<html><b><font=8 color=red>Open Disposition Form</font></b>";
						item.setText(initialText);
						item.addActionListener(new ActionListener() 
						{
							public void actionPerformed( ActionEvent e )
							{				
								//Confirmation dialog
								int row = targetItemsTable.getSelectedRow();
								if( row!=-1 )
								{
									displayTargetItemPropertiesDialog();
								}
							}
						} );
						popupMenu.add( item );
						popupMenu.show( ( java.awt.Component )source, mouseEvt.getX(), mouseEvt.getY() );
					}
				} 
				catch( TCException e1 ) 
				{
					e1.printStackTrace();
				}
			}			 
		} );
	}
	
	private void displayTargetItemPropertiesDialog()
	{
		int row = targetItemsTable.getSelectedRow();
		TCComponent dispComponent = ( ( NovTreeTableNode ) targetItemsTable.getNodeForRow( row ) ).dispComp;
		TCComponent targetRev = ( ( NovTreeTableNode ) targetItemsTable.getNodeForRow( row ) ).targetRev;

		dispItemDialog = new DispositionItemDialog( NOVECOTargetsPanel.this, engChangeObjFormPanel, dispComponent, targetRev );
		dispItemDialog.setLocation( 300, 50 );
		dispItemDialog.setVisible( true );
	}
	
}
