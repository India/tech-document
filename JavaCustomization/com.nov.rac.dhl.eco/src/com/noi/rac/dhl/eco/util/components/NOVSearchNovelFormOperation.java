package com.noi.rac.dhl.eco.util.components;
/**
 * @author mishalt
 * Created On: July 2013
 */
import java.util.HashMap;
import java.util.Vector;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;

public class NOVSearchNovelFormOperation implements INOVSearchProvider
{
    private HashMap<String, Vector<Object>> m_mapNovelForms;
    private Vector<String> m_novelFormRowData = null;
    private int m_novelFormRowCount            = 0;
    private int m_novelFormColCount            = 0;
    private NOVNovelProjectSearchPanel m_novelProjectSearchPanel;
    private INOVSearchResult m_searchResult = null;
    private String m_projectNum;
    private String m_projectName;
    private String m_projectType;
    private String m_projectStatus;
    private String m_productLine;
    
    public NOVSearchNovelFormOperation(NOVNovelProjectSearchPanel novelProjectSearchPanel)
    {
        super();
        m_novelProjectSearchPanel = novelProjectSearchPanel;
        m_mapNovelForms = new HashMap<String, Vector<Object>>();
    }

    public HashMap<String, Vector<Object>> executeSearch()
    {
        final INOVSearchProvider searchService = this;

        m_searchResult = null;
        final Display display = Display.getDefault();
        display.syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                Shell shell = AIFUtility.getActiveDesktop().getShell();
                
                try
                {
                    m_searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL,shell);
                    if(m_searchResult != null && m_searchResult.getResponse().nRows > 0)
                    {
                        INOVResultSet theResSet = m_searchResult.getResultSet();
                        m_novelFormRowData = theResSet.getRowData();
                        m_novelFormRowCount = theResSet.getRows();
                        m_novelFormColCount = theResSet.getCols();
                        m_mapNovelForms = getNovelFormTableMap();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                } 
            }
        });      
        return m_mapNovelForms;
    }


    public HashMap<String, Vector<Object>> getNovelFormTableMap()
    { 
        HashMap<String, Vector<Object>> mapNovelFromTable = new HashMap<String, Vector<Object>>();
        try 
        {
            if(m_novelFormRowData != null)
            {
                for (int i = 0; i < m_novelFormRowCount; i++) 
                {   
                    int rowIndex = m_novelFormColCount * i;
                    String puid = m_novelFormRowData.get(rowIndex + 0); 
                    String projectNum = m_novelFormRowData.get(rowIndex + 1); 
                    String projectName = m_novelFormRowData.get(rowIndex + 2);
                    String projectType = m_novelFormRowData.get(rowIndex + 3);
                    String productLine= m_novelFormRowData.get(rowIndex + 4);
                                    
                    Vector<Object> rowData = new Vector<Object>();
                    rowData.add(projectNum); 
                    rowData.add(projectName);
                    rowData.add(projectType);                    
                    rowData.add(productLine); 
                    
                    mapNovelFromTable.put(puid,rowData);
                } 
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        } 
        return mapNovelFromTable;
    }
    
    @Override
    public String getBusinessObject()
    {
        return "Nov4NovelProjectForm";
    }
    
    private void collectInput()//klockwork - KLOC265
    {
    	m_projectNum = "*";
        m_projectName = "*";
        m_projectType   = "*";
        m_projectStatus  = "*";
        m_productLine = "*";
    	System.out.println("klockwork - KLOC265 fixed");
    	if(m_novelProjectSearchPanel.m_ProjectNumTxt.getText() != null && m_novelProjectSearchPanel.m_ProjectNumTxt.getText().length()>0)
        {
            m_projectNum = m_novelProjectSearchPanel.m_ProjectNumTxt.getText();
        }
        if(m_novelProjectSearchPanel.m_projectNameTxt.getText() != null && m_novelProjectSearchPanel.m_projectNameTxt.getText().length()>0)
        {
            m_projectName = m_novelProjectSearchPanel.m_projectNameTxt.getText();
        }
        if(m_novelProjectSearchPanel.m_projectTypeTxt.getText() != null && m_novelProjectSearchPanel.m_projectTypeTxt.getText().length()>0)
        {
            m_projectType = m_novelProjectSearchPanel.m_projectTypeTxt.getText();
        }
        if(m_novelProjectSearchPanel.m_prjctStatusCmb.getSelectedItem() != null 
                && m_novelProjectSearchPanel.m_prjctStatusCmb.getSelectedIndex() != -1
                && m_novelProjectSearchPanel.m_prjctStatusCmb.getSelectedIndex() != 0)
        {
            m_projectStatus = m_novelProjectSearchPanel.m_prjctStatusCmb.getSelectedItem().toString();
        }
        if(m_novelProjectSearchPanel.m_productLineCmb.getSelectedItem() != null 
                && m_novelProjectSearchPanel.m_productLineCmb.getSelectedIndex() != -1
                && m_novelProjectSearchPanel.m_productLineCmb.getSelectedIndex() != 0)
        {
            m_productLine = m_novelProjectSearchPanel.m_productLineCmb.getSelectedItem().toString();
        }
    }
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        
        collectInput();
        
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[5];
        
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[]{m_projectNum};
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[]{m_projectName};
        
        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[]{m_projectType};
        
        bindVars[3] = new QuickSearchService.QuickBindVariable();
        bindVars[3].nVarType = POM_string;
        bindVars[3].nVarSize = 1;
        bindVars[3].strList = new String[]{m_projectStatus};
        
        bindVars[4] = new QuickSearchService.QuickBindVariable();
        bindVars[4].nVarType = POM_string;
        bindVars[4].nVarSize = 1;
        bindVars[4].strList = new String[]{m_productLine};
        
        return bindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-DH-novelprojectform"; 
        allHandlerInfo[0].listBindIndex = new int[] {1,2,3,4,5};
        allHandlerInfo[0].listReqdColumnIndex = new int[]{1,2,3,5}; 
        allHandlerInfo[0].listInsertAtIndex = new int[] {1,2,3,4}; 
          
        return allHandlerInfo ;
    }
    
}
