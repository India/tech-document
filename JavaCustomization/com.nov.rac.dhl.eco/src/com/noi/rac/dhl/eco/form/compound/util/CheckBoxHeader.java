package com.noi.rac.dhl.eco.form.compound.util;

import java.awt.Component;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class CheckBoxHeader extends JCheckBox implements TableCellRenderer, MouseListener 
{  
	private static final long serialVersionUID = 1L;
	private CheckBoxHeader rendererComponent;  
	protected int column;  
	protected boolean mousePressed = false;  
	
	public void setRendererComponent(CheckBoxHeader rendererComponent) 
	{
		this.rendererComponent = rendererComponent;
	}
	
	public CheckBoxHeader getRendererComponent() 
	{
		return rendererComponent;
	}
	
	public CheckBoxHeader(ItemListener itemListener) 
	{  
		setRendererComponent(this);  
		getRendererComponent().addItemListener(itemListener);  
	}  
	
	public Component getTableCellRendererComponent(JTable table, Object value,  
			boolean isSelected, boolean hasFocus, int row, int column) 
	{  
		if (table != null) 
		{  
			JTableHeader header = table.getTableHeader();  
			if (header != null) 
			{  
				getRendererComponent().setForeground(header.getForeground());  
				getRendererComponent().setBackground(header.getBackground());  
				getRendererComponent().setFont(header.getFont()); 	        
				header.addMouseListener(getRendererComponent());  
			}  
		}  
		getRendererComponent().setHorizontalAlignment(SwingConstants.CENTER);
		setColumn(column);  
		//rendererComponent.setText("Check All");  
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));  
		return getRendererComponent();  
	}  
	
	protected void setColumn(int column) 
	{  
		this.column = column;  
	} 
	
	public int getColumn() 
	{  
		return column;  
	}  
	
	protected void handleClickEvent(MouseEvent e) 
	{  
		if (mousePressed) 
		{  
			mousePressed=false;  
			JTableHeader header = (JTableHeader)(e.getSource());  
			JTable tableView = header.getTable();  
			TableColumnModel columnModel = tableView.getColumnModel();  
			int viewColumn = columnModel.getColumnIndexAtX(e.getX());  
			int column = tableView.convertColumnIndexToModel(viewColumn);  

			if (viewColumn == 0 && e.getClickCount() == 1 && column != -1) 
			{     
				doClick();  
			}  
		}  
	}  
	
	public void mouseClicked(MouseEvent e) 
	{  
		handleClickEvent(e);  
		((JTableHeader)e.getSource()).repaint();  
	}  
	public void mousePressed(MouseEvent e) 
	{  
		mousePressed = true;  
	}  
	
	public void mouseReleased(MouseEvent e) {}  
	public void mouseEntered(MouseEvent e) {}  
	public void mouseExited(MouseEvent e) {}  
} 
