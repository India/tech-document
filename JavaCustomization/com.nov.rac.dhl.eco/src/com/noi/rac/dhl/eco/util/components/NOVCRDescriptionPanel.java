package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVCRDescriptionPanel extends JPanel
{
    private static final long serialVersionUID = 1L;
    private JLabel statusValue = null;
    private JLabel reqDateValue = null;
    private JLabel reasonValue = null;
    private JTextArea changeDescTextArea = null;
    private JScrollPane changeDescPane = null;
    private JLabel approversValue = null;
    
    private JPanel ecoStatusPanel = null;
    private JPanel changeDescPanel = null;
    private JPanel approversPanel = null;
    
    NOVCRDescriptionPanel()
    {
        
        Border raisedEtched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
        this.setLayout(new VerticalLayout());
        this.setPreferredSize(new Dimension(520, 180));// TCDECREL - 3382
        
        JLabel label = new JLabel("ECR Description");
        label.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        
        this.add("top.bind.center.center", label);
        
        JPanel pendingCRDescBorderPanel = new JPanel(new VerticalLayout());
        pendingCRDescBorderPanel.setPreferredSize(new Dimension(520, 150));// TCDECREL
                                                                           // -
                                                                           // 3382
        pendingCRDescBorderPanel.setBorder(raisedEtched);
        /********************** Status - Request Date - Reason for Change *********************/
        // Main panel ecoStatusPanel consists of 2 panels statusPanel and
        // reasonPanel
        // statusPanel consists of Status and Request date fields
        // reasonPanel consists of Reason for Change fields
        // JPanel ecoStatusPanel = new JPanel(new GridLayout(0,2));
        ecoStatusPanel = new JPanel(new GridLayout(0, 1));
        ecoStatusPanel.setPreferredSize(new Dimension(510, 25));
        
        // Construct Status and Request Date panel
        JPanel statusPanel = new JPanel(new GridBagLayout());
        GridBagConstraints spaceConstraints = new GridBagConstraints();
        spaceConstraints.insets = new Insets(5, 5, 5, 5);
        
        // statusPanel.setPreferredSize(new Dimension(150, 30));
        JLabel statusLabel = new JLabel("Status:");
        statusLabel.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        statusValue = new JLabel("");
        
        JLabel reqDateLabel = new JLabel("Request Date:");
        reqDateLabel.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        reqDateValue = new JLabel("");
        
        spaceConstraints.insets = new Insets(5, 0, 5, 5);
        statusPanel.add(statusLabel, spaceConstraints);
        spaceConstraints.insets = new Insets(5, 5, 5, 5);
        statusPanel.add(statusValue, spaceConstraints);
        statusPanel.add(reqDateLabel, spaceConstraints);
        statusPanel.add(reqDateValue, spaceConstraints);
        
        // Construct Reason for Change Panel
       // JPanel reasonPanel = new JPanel(new GridLayout(0, 2));
        // reasonPanel.setPreferredSize(new Dimension(200, 30));
        JLabel reasonLabel = new JLabel("Reason for Change:");
        reasonLabel.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        reasonValue = new JLabel("");
        /*reasonPanel.add(reasonLabel);
        reasonPanel.add(reasonValue);*/
        statusPanel.add(reasonLabel, spaceConstraints);
        statusPanel.add(reasonValue, spaceConstraints);
        // Add the above panels to the "ecoStatusPanel" panel
        ecoStatusPanel.add(statusPanel);
        //ecoStatusPanel.add(reasonPanel);
        /********************** Status - Request Date - Reason for Change *********************/
        
        /********************************* Change Description *********************************/
        
        changeDescTextArea = new JTextArea(); // TCDECREL - 3382
        changeDescTextArea.setLineWrap(true);
        changeDescTextArea.setWrapStyleWord(true);
        changeDescPane = new JScrollPane(changeDescTextArea);
        changeDescPane.setPreferredSize(new Dimension(500, 60));
        changeDescPanel = new JPanel(new BorderLayout(0, 1));
        TitledBorder changeDescrBorder = new javax.swing.border.TitledBorder("Change Description:");
        changeDescrBorder.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        changeDescrBorder.setTitleColor(Color.BLACK);
        
        changeDescPanel.setBorder(changeDescrBorder);
        changeDescPanel.add(changeDescPane, BorderLayout.CENTER);
        
        /********************************* Change Description *********************************/
        
        /************************************ Approvers Field *********************************/
        // Approvers: Field
        approversPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        approversPanel.setPreferredSize(new Dimension(510, 25));
        JLabel approversLabel = new JLabel("Approvers:");
        approversLabel.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        approversValue = new JLabel("");
        
        approversPanel.add(approversLabel);
        approversPanel.add(approversValue);
        /************************************ Approvers Field *********************************/
        
        pendingCRDescBorderPanel.add("top.bind.center.center", ecoStatusPanel);
        pendingCRDescBorderPanel.add("top.bind.center.center", changeDescPanel);
        pendingCRDescBorderPanel.add("top.bind.center.center", approversPanel);
        
        this.add("top.bind.center.center", pendingCRDescBorderPanel);
    }
    
    public void showCRDescriptionPanel(boolean val)
    {
        ecoStatusPanel.setVisible(val);
        changeDescPanel.setVisible(val);
        approversPanel.setVisible(val);
    }
    
    public void displayCRDescription(TCComponentForm form) throws TCException
    {
        if (form != null)
        {
            showCRDescriptionPanel(true);
            statusValue.setText(form.getTCProperty("ecr_status").getStringValue());
            Date reqDate = form.getTCProperty("requested_date").getDateValue();
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            reqDateValue.setText(formatter.format(reqDate));
            reasonValue.setText(form.getTCProperty("reason_for_change").getStringValue());
            changeDescTextArea.setText(form.getTCProperty("change_desc").getStringValue());
            String[] approvers = form.getTCProperty("disposition_by").getStringValueArray();
            String approversVal = new String();
            for (int i = 0; approvers != null && i < approvers.length; i++)
            {
                approversVal = approversVal + approvers[i];
                if (i < approvers.length - 1)
                    approversVal = approversVal + ",";
            }
            
            approversValue.setText(approversVal);
            changeDescTextArea.setCaretPosition(0);
        }
        else
        {
            showCRDescriptionPanel(false);
            statusValue.setText("");
            reqDateValue.setText("");
            reasonValue.setText("");
            changeDescTextArea.setText("");
            approversValue.setText("");
        }
    }
}
