package com.noi.rac.dhl.eco.util.components;

import java.util.Arrays;
import java.util.List;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;

public class NOVENDispositionPanelData
{
	String targetitemid = "targetitemid";
	String changedesc = "changedesc";
	String dispinstruction = "dispinstruction";
	//commented out for the ticket 4474
	/*String inprocess = "inprocess";
	String infield = "infield";
	String ininventory = "ininventory";
	String assembled = "assembled";*/
	String m_disposition= "nov4_ecodisposition";//4474
	
	public NOVENDispositionPanelData( TCComponent[] dispComp ) 
	{
		createPanelData( dispComp );
	}

	private void createPanelData( TCComponent[] dispcomp )
	{
		try
		{
			//TCComponent[] dispCompArr = { dispcomp };
			//commented out for the ticket 4474
			/**String[] reqdProperties = { targetitemid, changedesc, dispinstruction, inprocess, 
			infield, ininventory, assembled,m_disposition };*/
			//4474-comment out the above existing  declaration added below
			String[] reqdProperties = { targetitemid, changedesc, dispinstruction,m_disposition  };			
			String[][] retriviedProperties = null;

			List<TCComponent> dispcompList = Arrays.asList(dispcomp);//TC10.1 Upgrade
			//retriviedProperties = TCComponentType.getPropertiesSet( dispcomp, reqdProperties );
			retriviedProperties = TCComponentType.getPropertiesSet( dispcompList, reqdProperties );//TC10.1 Upgrade
			
			for( int index = 0; index < dispcomp.length; ++index )
			{
				for( int indexOne = 0; indexOne < retriviedProperties[index].length; ++indexOne )
				{
					switch( indexOne )
					{
						case 0:
						{
							targetitemid = retriviedProperties[index][indexOne];
						}
						case 1:
						{
							changedesc = retriviedProperties[index][indexOne];
						}
						case 2:
						{
							dispinstruction = retriviedProperties[index][indexOne];
						}
						//commented out for the ticket 4474
						/**case 3:
						{
							inprocess = retriviedProperties[index][indexOne];
						}
						case 4:
						{
							infield = retriviedProperties[index][indexOne];
						}
						case 5:
						{
							ininventory = retriviedProperties[index][indexOne];
						}
						case 6:
						{
							assembled = retriviedProperties[index][indexOne];
						}*/
						//4474-comment out the existing lines and added below
						case 3:
						{
							m_disposition = retriviedProperties[index][indexOne];
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setDispClassData( String strchnageReason, String strdispInstruction, String strinProcess,
			 String strinfield, String strinInventory, String strassembled, String dipositionValue, TCComponent dispcomp ) 
	{
		try
		{
			String[] strPropToSet = { "changedesc", "dispinstruction", "inprocess", "infield", "ininventory", "assembled","nov4_ecodisposition" };
			String[] strNewProp = { strchnageReason, strdispInstruction, strinProcess, strinfield, strinInventory, strassembled,dipositionValue  };
			
			dispcomp.setProperties( strPropToSet,  strNewProp );
			dispcomp.lock();
			dispcomp.save();
			dispcomp.unlock();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	//4474-start
	public void setDispositionClassData( String strChangeReason, String strDispInstruction, String strDispositionField,TCComponent dispComp)
			 
	{
		try
		{
			String[] strPropToSet = { "changedesc", "dispinstruction", "nov4_ecodisposition" };
			String[] strNewProp = { strChangeReason, strDispInstruction, strDispositionField };
			
			dispComp.setProperties( strPropToSet,  strNewProp );
			dispComp.lock();
			dispComp.save();
			dispComp.unlock();
			/*refresh() on disposition object resolves lag time/session time issue with the 
			* ECO, when user 1 passes ECO to user 2 for approval  
			*/
			dispComp.refresh();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}//4474-end
	
}

