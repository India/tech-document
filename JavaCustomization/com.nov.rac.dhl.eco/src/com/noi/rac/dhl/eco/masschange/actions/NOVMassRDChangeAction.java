package com.noi.rac.dhl.eco.masschange.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.masschange.factory.MassChangeFactory;
import com.noi.rac.dhl.eco.masschange.operations.AbstractMassRDChangeOperation;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author tripathim
 * 
 */
public class NOVMassRDChangeAction implements IRunnableWithProgress
{
    private MassRDChangeComposite m_massRDChangeComposite;
    private AbstractMassRDChangeOperation m_operation;
    private String m_actionCmd;
    private String m_mmrRevType;
    private TCComponent[] m_comps;
    private int m_taskLength;
    private Registry m_reg = null;
    private final int m_taskInitialProgress = 1;
    
    /**
     * @param composite
     * @param actionCmd
     * @param mmrRevType
     */
    public NOVMassRDChangeAction(MassRDChangeComposite composite, String actionCmd, String mmrRevType)
    {
        m_massRDChangeComposite = composite;
        m_actionCmd = actionCmd;
        m_comps = m_massRDChangeComposite.getSelectedWhereUsedItem();
        m_mmrRevType = mmrRevType;
        m_taskLength = m_comps.length;
        init();
    }
    
    private void init()
    {
    	System.out.println("<<<<<<<< Code testing >>>>>>>>>>>");
        m_reg = Registry.getRegistry("com.noi.rac.dhl.eco.masschange.operations.operations");
        MassChangeFactory theFactory = MassChangeFactory.newInstance();
        theFactory.setRegistry(m_reg);
        theFactory.setOperationName(m_actionCmd);
        m_operation = theFactory.createOperation(m_massRDChangeComposite, m_comps, m_mmrRevType);
        
    }
    
    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.operation.IRunnableWithProgress#run(org.eclipse.core
     * .runtime.IProgressMonitor)
     */
    @Override
    public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
    {
        monitor.beginTask("Processing Mass RD Update", m_taskLength);
        monitor.worked(m_taskInitialProgress);
        m_operation.performRDEditOperation();
        m_operation.addTargetsToECO();
    }
    
}
