package com.noi.rac.dhl.eco.util.components;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetItemsTable.NovTreeTableNode;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.validations.additem.NOVECRSelectionDialog;
import com.nov.rac.dhl.eco.validations.additem.NOVECRTable;
import com.nov.rac.dhl.eco.validations.additem.NOVECRTableLine;
import com.teamcenter.rac.aif.common.AIFTableLine;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

/**
 * Implementation for Dialog to move attached target item to other ECO
 * @author Akhilesh 
 *
 */
public class NOVECOMoveTargetDialog extends AbstractSWTDialog{

	protected Object result;
	TCComponentForm ecoForm;
	protected static Shell shell;
	private Text txtTargetItem;
	private Text txtMoveECO;
	private Text txtOnSelect;
	private TCSession session;
	private TCUserService service;
	private String changeType;
	NOVECOsearchDialog searchECODlg;
	public NOVECOTargetItemsTable targetItemsTbl;
	public _EngChangeObjectForm_Panel ecoFormPnl;
	TCComponent targetItem;
	NOVECOMoveTargetDialog moveDlg;
	public TCComponentForm targetECO;
	HashMap<TCComponent, String> mapLifecycle = new HashMap<TCComponent, String>();
	private HashMap<TCComponent, String> newLifecycleMap;
	private String newLifeValue;
	
	private String targetitemid = "";
	private String changedesc = "";
	private String dispinstruction = "";
	private String inprocess = "";
	private String infield = "";
	private String ininventory = "";
	private String assembled = "";
	private Vector<TCComponent> m_selectedRevs = new Vector<TCComponent>();
	private Vector<TCComponent> m_selectedECRs = new Vector<TCComponent>();
	private boolean m_bIsOkClicked = false;
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 * @wbp.parser.constructor
	 */
	public NOVECOMoveTargetDialog(Shell parent) {
		super(AIFUtility.getActiveDesktop().getShell());
		setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.SYSTEM_MODAL);
		session = (TCSession) AIFUtility.getDefaultSession();
		moveDlg=this;
		
	}

	
	/**
	 * Open the dialog.
	 * @param parent - parent shell
	 * @param remTarget - target item
	 * @param chngType - Type of Change of current ECO form
	 * @return ecoFormPanel - ECO form Panel
	 */
	public NOVECOMoveTargetDialog(Shell parent, TCComponent remTarget,
			String chngType, _EngChangeObjectForm_Panel ecoFormPanel) {
		super(AIFUtility.getActiveDesktop().getShell());
		setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.APPLICATION_MODAL);
		session = (TCSession) AIFUtility.getDefaultSession();
		moveDlg=this;
		ecoFormPnl = ecoFormPanel;
		targetItemsTbl =ecoFormPanel.targetItemsPanel.targetTable;
		targetItem = remTarget;
		changeType=chngType;
		
		// TODO Auto-generated constructor stub
	}
	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	protected Control createContents(Composite  parent) {
	
		parent.setLayout(new GridLayout(1, false));
		GridData gdParent = new GridData(SWT.FILL , SWT.FILL, true, true, 1, 1);
		gdParent.widthHint = 300;
		gdParent.heightHint = 220;
		parent.setLayoutData(gdParent);
		parent.getShell().setText("Move To Other ECO");
		
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		Label lblTargetItem = new Label(composite, SWT.NONE);
		lblTargetItem.setText("Target Item");
		
		txtTargetItem = new Text(composite, SWT.BORDER);
		txtTargetItem.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		txtTargetItem.setEditable(false);
		GridData gdTextTargetItem = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gdTextTargetItem.widthHint = 100;
		txtTargetItem.setLayoutData(gdTextTargetItem);
		txtTargetItem.setEnabled(false);
		try 
		{
			txtTargetItem.setText(targetItem.getProperty("item_id") + "/"+targetItem.getProperty("current_revision_id"));
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Label(composite, SWT.NONE);
		
		Label lblMoveToOther = new Label(composite, SWT.NONE);
		lblMoveToOther.setText("Target ECO");
		
		txtMoveECO = new Text(composite, SWT.BORDER);
		GridData gdTextMoveECO = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gdTextMoveECO.widthHint = 100;
		txtMoveECO.setLayoutData(gdTextMoveECO);
		//txtMoveECO.setEditable(false);
		txtMoveECO.setEnabled(false);
		
		Button btnSelect = new Button(composite, SWT.NONE);
		GridData gdButtonSelect = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdButtonSelect.widthHint = 30;
		btnSelect.setLayoutData(gdButtonSelect);
		btnSelect.setText("...");
		
		btnSelect.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				try {
					searchECODlg = new NOVECOsearchDialog(moveDlg, changeType);
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//ecoForm = searchECODlg.open();
				searchECODlg.open();
				try {
					if (!targetECO.toString().isEmpty()) {
						txtMoveECO.setText(targetECO.toString());
						ecoForm = targetECO;
					}
				} catch (NullPointerException e) {
					// TODO: handle exception
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		
		txtOnSelect = new Text(composite, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.MULTI);
		GridData gdTextSelect = new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1);
		gdTextSelect.widthHint = 232;
		txtOnSelect.setLayoutData(gdTextSelect);
		txtOnSelect.setText("On selecting OK, Target item will be removed from current ECO and will be moved to selected ECO Target list.");
		txtOnSelect.setEditable(false);
		txtOnSelect.setEnabled(false);
		
		Composite compositeBtn = new Composite(parent, SWT.NONE);
		compositeBtn.setLayout(new GridLayout(2, true));
		GridData gd_compositeBtn = new GridData(SWT.CENTER , SWT.FILL, true, true, 1, 1);
		gd_compositeBtn.widthHint = 100;
		gd_compositeBtn.heightHint = 40;
		compositeBtn.setLayoutData(gd_compositeBtn);
		
		
		Button btnOk = new Button(compositeBtn, SWT.CENTER);
		GridData gdButtonOk = new GridData(SWT.CENTER, SWT.FILL, true, false, 1, 1);
		gdButtonOk.widthHint = 50;
		btnOk.setLayoutData(gdButtonOk);
		btnOk.setText("OK");
		btnOk.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				if (targetItem !=null && ecoForm!=null)
				{
					//boolean isRemoved = false;
					if (targetItemsTbl.newLifecycleMap.containsKey(targetItem))
					{
						newLifeValue = targetItemsTbl.newLifecycleMap.get(targetItem);
						mapLifecycle.put(targetItem,targetItemsTbl.newLifecycleMap.get(targetItem));
					}
					getDispositionData();
					
					//isRemoved = removeTarget();
					
					getShell().dispose();
					
					m_bIsOkClicked = false;
					ecoFormPnl.moveToECOOp(true);
					launchECRSelectionDlg(ecoForm);
					
					if(m_bIsOkClicked == true)
					{
						final AddOperation addOperation = new AddOperation();
						final Display display = PlatformUI.getWorkbench().getDisplay();
						final ProgressMonitorDialog addProgressDialog=new ProgressMonitorDialog(display.getActiveShell());

						try 
						{
							addProgressDialog.run(true, false, addOperation);
						} 
						catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
						catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Button btnCancel = new Button(compositeBtn, SWT.CENTER);
		GridData gdButtonCancel = new GridData(SWT.LEFT, SWT.FILL, true, false, 1, 1);
		gdButtonCancel.widthHint = 55;
		btnCancel.setLayoutData(gdButtonCancel);
		btnCancel.setText("Cancel");
		btnCancel.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				getShell().dispose();
				
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		return parent;
	}
	public void moveItemtoECO()
	{
		int iRow = targetItemsTbl.getSelectedRow();
		TCComponent remTarget=((NovTreeTableNode)targetItemsTbl.getNodeForRow(iRow)).targetRev;
		
		Vector<TCComponentItemRevision> partFamily = new Vector<TCComponentItemRevision>();
		if(!isMinorRevSelected(remTarget)) //TCDECREL-8344: For minor rev selected, should not remove other part family member.
		{
		    partFamily = targetItemsTbl.getPartFamily(remTarget);
		}
		if(!partFamily.contains(remTarget))
		{
			partFamily.add((TCComponentItemRevision)remTarget);
		}
		for(int i=0;i<partFamily.size();i++)
		{
			ecoFormPnl.moveToECOOp(true);
			ecoFormPnl.removeTargetRevs(partFamily.get(i));
		}
				
		ecoFormPnl.removeECRs(m_selectedECRs);

		TCReservationService reserserv = ecoForm.getSession().getReservationService();
		try 
		{
			if (! reserserv.isReserved(ecoForm) )
			{
				//check-out the selected ECO
				reserserv.reserve(ecoForm);
			}

			if(reserserv.isReserved(ecoForm) )
			{
				String sMsg = NOVECOHelper.addTargetsToECO("NOVECOTargetItemsPanel", ecoForm, 
															m_selectedRevs.toArray(new TCComponent[m_selectedRevs.size()]),
															m_selectedECRs.toArray(new TCComponent[m_selectedECRs.size()]));
				
				if (sMsg == null || sMsg.trim().length() == 0)
				{
					saveLifeCycleValue(ecoForm);
					saveDispositionData(ecoForm);
				}
			}

			if(reserserv.isReserved(ecoForm))
			{
				reserserv.unreserve(ecoForm);
				ecoFormPnl.moveToECOOp(false);
			}

		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private boolean isMinorRevSelected(TCComponent comp)
	{
	    boolean isMinor = false;
	    String revId = "";
	    try
        {
            revId = comp.getProperty("item_revision_id").toString();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
	    if(revId.contains("."))
	    {
	        isMinor = true;
	    }
	    return isMinor;
	}
	
	/**
	 * Function : to save Disposition data to Target ECO form
	 * @param ecoTargetForm - Target ECO Form
	 */
	protected void saveDispositionData(TCComponentForm ecoTargetForm)
	{
		TCComponent dispcomp =null;
		try
		{
			TCComponent[] dispo = ecoTargetForm.getTCProperty("ectargetdisposition").getReferenceValueArray();
			for( int index = 0; index < dispo.length; ++index )
			{
				String targetItemID = dispo[index].getTCProperty("targetitemid").toString();
				String itemTargetId = targetItem.getTCProperty("item_id").toString();
				if (targetItemID.equals(itemTargetId))
				{
					dispcomp = dispo[index];
				}
			}
			
			String[] strPropToSet = { "changedesc", "dispinstruction", "inprocess", "infield", "ininventory", "assembled" };
			String[] strNewProp = { changedesc, dispinstruction, inprocess, infield, ininventory, assembled  };
			if (dispcomp !=null)
			{
				dispcomp.setProperties( strPropToSet,  strNewProp );
				dispcomp.lock();
				dispcomp.save();
				dispcomp.unlock();
			}
			
		} catch (TCException e) 
		{
			e.printStackTrace();
		}
	}


	/**
	 * To get the Disposition data of Target item from Current ECO Form
	 */
	protected void getDispositionData() 
	{
		try 
		{
			TCComponent[] dispo = ecoFormPnl.ecoForm.getTCProperty("ectargetdisposition").getReferenceValueArray();
			
			 targetitemid = "targetitemid";
			 changedesc = "changedesc";
			 dispinstruction = "dispinstruction";
			 inprocess = "inprocess";
			 infield = "infield";
			 ininventory = "ininventory";
			 assembled = "assembled";
			 
			 String[] reqdProperties = { targetitemid, changedesc, dispinstruction, inprocess, 
										infield, ininventory, assembled };
						
			String[][] retriviedProperties = null;
			List<TCComponent> dispoList = Arrays.asList(dispo);//TC10.1 Upgrade
			//retriviedProperties = TCComponentType.getPropertiesSet( dispo, reqdProperties );
			retriviedProperties = TCComponentType.getPropertiesSet( dispoList, reqdProperties );//TC10.1 Upgrade
			String itemTargetId = targetItem.getTCProperty("item_id").toString();
			
			for( int index = 0; index < dispo.length; ++index )
			{
				if (retriviedProperties[index][0].equals(itemTargetId))
				{
					for( int indexOne = 0; indexOne < retriviedProperties[index].length; ++indexOne )
					{
						switch( indexOne )
						{
							case 0:
							{
								targetitemid = retriviedProperties[index][indexOne];
							}
							case 1:
							{
								changedesc = retriviedProperties[index][indexOne];
							}
							case 2:
							{
								dispinstruction = retriviedProperties[index][indexOne];
							}
							case 3:
							{
								inprocess = retriviedProperties[index][indexOne];
							}
							case 4:
							{
								infield = retriviedProperties[index][indexOne];
							}
							case 5:
							{
								ininventory = retriviedProperties[index][indexOne];
							}
							case 6:
							{
								assembled = retriviedProperties[index][indexOne];
							}
						}
					}
				}
			}
		} catch (TCException e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Function - Save LifeCycle value for target item in target ECO form.
	 * @param ecoTargetForm - Target ECO
	 */
	protected void saveLifeCycleValue(TCComponentForm ecoTargetForm) 
	{
		Vector<String> nLifecycleVLAVec = new Vector<String>();
		try 
		{
			String[] uidLifecycleArr = ecoTargetForm.getTCProperty("targetsnewlifecycle").getStringValueArray();
			for (int i = 0; i < uidLifecycleArr.length; i++) 
			{
				String[] uidLifeCycle = uidLifecycleArr[i].split("::");
				if (uidLifeCycle.length == 2) 
				{
					TCComponent itemRev = ecoTargetForm.getSession().stringToComponent(uidLifeCycle[0]);					
					System.out.println( " New Llifecycle for " + itemRev.getProperty("item_id") + " is : " +uidLifeCycle[1] );
					mapLifecycle.put(itemRev, uidLifeCycle[1]);					
				}
			}
			
			TCComponent[] itemRevComps = mapLifecycle.keySet().toArray(new TCComponent[mapLifecycle.keySet().size()]);
			
			for (int i = 0; i < itemRevComps.length; i++) 
			{
					nLifecycleVLAVec.add(itemRevComps[i].getUid()+"::"+mapLifecycle.get(itemRevComps[i]));	
				
			}
			String[] newLifecycleVLA = nLifecycleVLAVec.toArray(new String[nLifecycleVLAVec.size()]);
			TCProperty newLifecycleProp = ecoTargetForm.getTCProperty("targetsnewlifecycle");
			
			if (newLifecycleVLA.length>0) 
			{
				newLifecycleProp.setStringValueArray(newLifecycleVLA);
			}
			else
			{
				newLifecycleProp.setStringValueArray(new String[0]);
			}
			
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
	}


	/**
	 * This will remove target from current ECO
	 * @return
	 */

	protected boolean removeTarget() 
	{
		boolean isRemoveTarget =false;
		try 
		{
			for (int i = 0; i < targetItemsTbl.getRowCount(); i++) 
			{
				TreeTableNode treeTblNode =  targetItemsTbl.getNodeForRow(i);
				
				if (treeTblNode instanceof NovTreeTableNode) 
				{
					TCComponent trgRev = ((NovTreeTableNode)treeTblNode).targetRev;
					
					if ( trgRev != null)
					{
						if (targetItem.getProperty("item_id").equals(((TCComponentItemRevision)trgRev).getProperty("item_id")) ) 
						{
							//it will remove target from workflow and eco form both.
							targetItemsTbl.removeTargetsandCleanup(i);
							isRemoveTarget =  true;
						}
					}
				}
			}
		}
		catch (TCException e) 
		{
			MessageBox.post("Cannot delete Revision. "+e.getError(),"Error",MessageBox.ERROR);
			e.printStackTrace();
			isRemoveTarget =  false;
		}
		return isRemoveTarget;
	}
	
	protected Map<TCComponent,Vector<TCComponent>> getTargetRevsECRInfo()
	{
		Map<TCComponent,Vector<TCComponent>> itemRevECRMap = new HashMap<TCComponent,Vector<TCComponent>>();
		int iSelectedRow = targetItemsTbl.getSelectedRow();
		
		TreeTableNode treeTblNode =  targetItemsTbl.getNodeForRow(iSelectedRow);
		if (treeTblNode instanceof NovTreeTableNode) 
		{
			TCComponent trgRev = ((NovTreeTableNode)treeTblNode).targetRev;

			TCComponent targetDisp = ((NovTreeTableNode)treeTblNode).dispComp;
			
			Vector<TCComponent> ecrs = targetItemsTbl.getECRsList(trgRev,targetDisp);

			if(ecrs.size() > 0)
			{
				itemRevECRMap.put(trgRev,ecrs);
			}
			else
			{
				m_selectedRevs.add(trgRev);
			}
		}
		return itemRevECRMap;
	}
	
	private void launchECRSelectionDlg(final TCComponentForm targetECO)
	{
		final Display display = Display.getDefault();
        display.syncExec(new Runnable() 
        {
            public void run() 
            {
            	int iRetCode = 0;
            	int iNoOfRows = 0;
            	boolean bIsECRSelected = false;
            	NOVECRTable ecrSelectionTable = null;
            	Map<TCComponent,Vector<TCComponent>> itemRevCRMap = getTargetRevsECRInfo();
            	if(itemRevCRMap.values().size()>0)
            	{
            		Shell shell = null;
					shell = display.getActiveShell();
									
            		NOVECRSelectionDialog ecrSelectionDlg = new NOVECRSelectionDialog(shell);
            		ecrSelectionDlg.create();        
            		
            		//NOVECOHelper.centerToScreen(ecrSelectionDlg.getShell());

            		String sChangeType = "";
            		try
            		{
            			sChangeType = targetECO.getTCProperty("changetype").getStringValue();
            		}
            		catch (TCException e1)
            		{
            			// TODO Auto-generated catch block
            			e1.printStackTrace();
            		}
            		ecrSelectionDlg.populateECRTable(sChangeType,itemRevCRMap);

            		ecrSelectionTable = ecrSelectionDlg.getECRSelectionTable();
            		iNoOfRows = ecrSelectionTable.getRowCount();

            		iRetCode = ecrSelectionDlg.open();
            	}
            	if(iRetCode == Dialog.OK)
            	{
            		m_bIsOkClicked = true;
            		for(int indx=0;indx<iNoOfRows;indx++)
            		{
            			boolean bChkBoxValue = (Boolean)ecrSelectionTable.getValueAt(indx, 0);
            			if(bChkBoxValue)
            			{
            				bIsECRSelected = true;
            				AIFTableLine tableLine = ecrSelectionTable.getRowLine(indx);
            				NOVECRTableLine ecrTableLine = (NOVECRTableLine)tableLine;
            				TCComponent formObject = ecrTableLine.getTableLineFormObject();
            				TCComponent revObject = ecrTableLine.getTableLineRevObject();

            				if(!m_selectedRevs.contains(revObject))
            				{
            					m_selectedRevs.add(revObject);
            				}
            				if(!m_selectedECRs.contains(formObject))
            				{
            					m_selectedECRs.add(formObject);
            				}

            			}
            		}
            		if(bIsECRSelected == false)
            		{
            			m_selectedRevs.addAll(itemRevCRMap.keySet());
            		}
            	}
            }
        });
	}
	private class AddOperation implements IRunnableWithProgress
	{
		public AddOperation() 
		{
		}

		@Override
		public void run(IProgressMonitor progressMonitor)
				throws InvocationTargetException, InterruptedException 
		{
			progressMonitor.beginTask("Moving Item to ECO...", IProgressMonitor.UNKNOWN);
			final IProgressMonitor finalprogressMonitor = progressMonitor;
			
			try 
			{
				moveItemtoECO();
				
				finalprogressMonitor.done();
			} catch (Exception e) {
				e.printStackTrace();
				finalprogressMonitor.setCanceled(true);
				finalprogressMonitor.done();
			}
		}
	}
}
