/**
 * 
 */
package com.noi.rac.dhl.eco.masschange.operations;

import javax.swing.JOptionPane;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.noi.rac.dhl.eco.masschange.utils.NOVRDChangeHelper;
import com.noi.rac.dhl.eco.masschange.utils.NOVRDMessages;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;

/**
 * @author tripathim
 * 
 */
public abstract class AbstractMassRDChangeOperation
{
    protected TCComponent[] m_selectedComps;
    protected MassRDChangeComposite m_massRDChangePanel;
    protected String m_mmrRevType;
    protected String m_successMsg;
    private TCComponentForm m_ecoForm;
    
    public AbstractMassRDChangeOperation(MassRDChangeComposite massRDChangeComposite, TCComponent[] selectedComps,
            String mmrRevType)
    {
        m_massRDChangePanel = massRDChangeComposite;
        m_selectedComps = selectedComps;
        m_mmrRevType = mmrRevType;
        m_successMsg = NOVRDMessages.getString("RDUpdateSuccess.MESSAGE");
    }
    
    abstract public void performRDEditOperation();
    
    public void addTargetsToECO()
    {
        boolean isSuccess = false;
        m_ecoForm = m_massRDChangePanel.getECOFormPanel().getECOForm();
        TCComponent[] latestRevCompArr = NOVRDChangeHelper.getLatestRevCompArr(m_selectedComps);
        String sErrorMsg = NOVECOHelper.addTargetItemRevsToECO("NOVECOTargetItemsPanel", m_ecoForm, latestRevCompArr,
                m_mmrRevType);
        if (sErrorMsg != null && sErrorMsg.trim().length() > 0)
        {
            isSuccess = false;
            JOptionPane.showMessageDialog(m_massRDChangePanel.getECOFormPanel(), sErrorMsg,
                    "Cannot add Item to target", JOptionPane.WARNING_MESSAGE);
        }
        else
        {
            isSuccess = true;
        }
        try
        {
            m_ecoForm.refresh();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        m_massRDChangePanel.setSuccessStatus(isSuccess, m_successMsg);
    }
}
