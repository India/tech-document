package com.noi.rac.dhl.eco.form.compound.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import schema.v1_3.webservices.tce.nov.com.CredentialsInfo;

import com.noi.rac.dhl.eco.form.compound.data._EngChangeObjectForm_DC;
import com.noi.rac.dhl.eco.form.compound.util.INOVCustomFormProperties;
import com.noi.rac.dhl.eco.util.components.NOVECODispositionPanel;
import com.noi.rac.dhl.eco.util.components.NOVECODistributionPanel;
import com.noi.rac.dhl.eco.util.components.NOVECONovelProjectPanel;
import com.noi.rac.dhl.eco.util.components.NOVECOSyncTargetsDialog;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetCRsListPanel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetItemsPanel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetItemsTable.NovTreeTableNode;
import com.noi.rac.en.form.compound.component.NOVEnDistributionPanel;
import com.noi.util.components.NOIJLabel;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInCompoundHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.ecohelper.DeleteECOHelper;
import com.nov.rac.utilities.services.revisehelper.ReviseObjectsSOAHelper;
import com.nov.www.cet.webservices.V1_3.CETGServicesProxy;
import com.nov.www.cet.webservices.V1_3.CETWSFault;
import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.open.OpenFormDialog;
import com.teamcenter.rac.common.tcviewer.FormViewer;
import com.teamcenter.rac.kernel.TCAccessControlService;
import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentProcess;
import com.teamcenter.rac.kernel.TCComponentSite;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCReservationService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCSiteInfo;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.iTextArea;
import com.teamcenter.soa.client.model.ErrorStack;

public class _EngChangeObjectForm_Panel extends PrintablePanel
{
    private static final long serialVersionUID = 1L;
    public Registry m_registry;
    public TCComponentForm ecoForm;
    public JTabbedPane ecautomatonTabbePane = new JTabbedPane();
    public Vector<TCComponent> ecoTargets = new Vector<TCComponent>();
    public Vector<TCComponent> targetsToBeRemoved = new Vector<TCComponent>();
    Map<String, TCComponent> dispTargetsMap = new HashMap<String, TCComponent>();
    public Map<String, TCComponent> ecoTargetsMap = new HashMap<String, TCComponent>();
    Set<TCComponent> dispCompTobeDeleted = new HashSet<TCComponent>();
    public TCComponent[] wftargets;
    private TCComponentProcess currentProcess = null;
    public String typeofChange;
    public NOVECOTargetItemsPanel targetItemsPanel;
    public NOVECOTargetCRsListPanel targetCRsPanel;
    public NOVECODispositionPanel dispPanel;
    public iTextArea reasonforChange;
    SuportingFilesPanel extPanel;
    JComboBox changeCat;
    JComboBox groupCmb;
    JComboBox reasonCode;
    JComboBox productLine;
    JComboBox m_formPanelECOClassification;// TCDECREL-4471
    public iTextArea additionalComments;
    JTextField workflowType;
    JButton cancelProcessBtn;
      String syncMsgStr = "";
    TCAccessControlService accessCtrlSer;
    protected JScrollPane addtnCommentsPane;
    public boolean isEcoReleased = false;
    private static boolean m_bMoveToECO = false;
    private TCComponent[] m_dispComps = null;
    JButton m_ECOHelp; //TCDECREL-5889
	NOVECODistributionPanel distributionPanel; //TCDECREL-6163
    NOVECONovelProjectPanel m_novelPrjctPanel;  //TCDECREL-6163
	
	private Vector<TCComponent> m_TargetsWithDisp = new Vector<TCComponent>();
    public static boolean m_isEcoDeleted = false;
    Vector<String> targetId =new Vector<String>();//7398
    
    public _EngChangeObjectForm_Panel()
    {
        super();
        m_registry = Registry.getRegistry(this);
    }
    
    public void initialize()
    {
        
    }
    
    public TCSession getSession()
    {
        return ecoForm.getSession();
    }
    
    public void saveDatatoForm()
    {
        ((_EngChangeObjectForm_DC) novFormProperties).saveOnlyDispFormData();
    }
    
    public void removeDispositionData(TCComponent tobremoved)
    {
        NOVECODispositionPanel dispnale = (NOVECODispositionPanel) novFormProperties.getProperty("ectargetdisposition");
        dispnale.removeDispTab(tobremoved);
    }
    
    public TCComponentProcess getProcess()
    {
        return currentProcess;
    }
    
    private String getWarningText()
    {
        // Updating the target list before validating
        // loadEnTargets(ecoForm);
        String returnStr = "This will perform the following actions.\n";
        
        try
        {
            returnStr = returnStr + "Delete the Change object :" + ecoForm.getProperty("object_name") + ". \n"
                    + "Delete the workflow :" + workflowType.getText();
            
            String revStr = "";
            TCComponent[] targetComps = wftargets;
            int count = 0;
            if (targetComps != null)
            {
                for (int i = 0; i < targetComps.length; i++)
                {
                    String objectType = targetComps[i].getProperty("object_type");
                    boolean isValidType = false;
                    String[] type = new String[] { "Non-Engineering Revision", "Nov4Part Revision",
                            "Documents Revision" };
                    for (int index = 0; index < type.length; index++)
                    {
                        if (objectType.equalsIgnoreCase(type[index]))
                        {
                            isValidType = true;
                            break;
                        }
                    }
                    if (isValidType)
                    {
                        if (count > 3)
                        {
                            count = 0;
                            revStr = revStr + "\n";
                        }
                        revStr = revStr + targetComps[i].getProperty("object_string") + ",";
                        count++;
                    }
                }
                if (revStr.length() > 0)
                {
                    if (revStr.charAt(revStr.length() - 1) == ',')
                    {
                        revStr = revStr.substring(0, revStr.length() - 1);
                    }
                    returnStr = returnStr + "\nDelete the item revision(s) and Related Defining Document(s) :" + revStr;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return returnStr;
    }
    
    public void createUI(INOVCustomFormProperties formProps)
    {
        // Mohan : All NOVLogger statements are commented out because it is
        // failing in CITRIX
        
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Create UI Start");
        
        novFormProperties = formProps;
        ecoForm = (TCComponentForm) formProps.getForm();
        isEcoReleased = NOVECOHelper.isEcoReleased(ecoForm);
        
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Load EN start");
        loadEnTargets(ecoForm);
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Load EN start");
        
        JPanel chNamePanel = new JPanel();
        try
        {
            
            JLabel formName = new JLabel("Change #:" + ecoForm.getProperty("object_name"));
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info(formName.getText());
            formName.setFont(new Font("", java.awt.Font.BOLD, 16));
            chNamePanel.add(formName);
            cancelProcessBtn = new JButton("Delete Change Object");
            cancelProcessBtn.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent arg0)
                {
                    int response = JOptionPane.showConfirmDialog(null,
                            "Do you want to delete the Enginering change object?\n" + getWarningText(), "Confirm",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (response == JOptionPane.YES_OPTION)
                    {
                    	// TCDECREL- 7066 Replace userservices NATOIL_callOnECOCancel and NATOIL_callOnECOCancel2 to SOA
                    	
                        try 
                        { 
                        	Vector<TCComponent> targets = getECOTargets();
                        	
                        	DeleteECOHelper ecoHelper = new DeleteECOHelper();
                        
                        	ecoHelper.deleteChangeObject(ecoForm, targets.toArray(new TCComponent[targets.size()]));
						} 
                        catch (TCException e) 
                        {
							e.printStackTrace();
						}
                                                  
                        deleteJob();
                        
                        //instanceof TCComponentItemRevision
                        //7154-commented if condition
                        //if(dispTargetsMap.size()>=1)
                        //{
                        	deleteECO();
                        //}
                    }
                }
            });
            
            chNamePanel.add(cancelProcessBtn);
            /*
             * DHL-298 add "View PDF" for only released ecos start
             */
            if (NOVECOHelper.isEcoReleased(ecoForm))
            {
                JButton viewPdf = new JButton("View PDF");
                viewPdf.addActionListener(new EngChangePanelListener(novFormProperties));
                chNamePanel.add(viewPdf);
            }
            /*
             * DHL-298 add "View PDF" for only released ecos end
             */

            JPanel iniPanel = getInitPanel();
            iniPanel.setBorder(new TitledBorder(""));
            
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("tarrget items start");
            targetItemsPanel = new NOVECOTargetItemsPanel(this);
            targetItemsPanel.resizePanel(null, new Dimension(825, 150), new Dimension(825, 28)); // TCDECREL-2798
                                                                                                 // :
                                                                                                 // Akhilesh
            
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("target items end,lifecycle map Start");
            buildNewLifeCycleMap();
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("lifecycle map end");
            // @swamy
            // TCComponent[] lifecycleInstances =
            // ecoForm.getTCProperty("ecotargetlifecycledata").getReferenceValueArray();
            TCComponent[] lifecycleInstances = ecoForm.getTCProperty("nov4_ecotgtlifecycledata")
                    .getReferenceValueArray();
            
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Load target item start");
            targetItemsPanel.loadLifeCycleInstances(lifecycleInstances);
            // Loading Targets panel
            TCComponent[] dispInstances = novFormProperties.getImanProperty(
                    ((_EngChangeObjectForm_DC) novFormProperties).dispPanel).getReferenceValueArray();
            targetItemsPanel.loadTargetItems(ecoTargets.toArray(new TCComponent[ecoTargets.size()]), dispInstances);
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Load target item End");
            
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("create disposition start");
            ((NOVECODispositionPanel) novFormProperties.getProperty("ectargetdisposition")).setDispData(this);
            dispPanel = ((NOVECODispositionPanel) novFormProperties.getProperty("ectargetdisposition"));
            dispPanel.createUI(this);
            // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("create disposition End");
            
            targetCRsPanel = new NOVECOTargetCRsListPanel(this);
            targetCRsPanel.resizePanel(null, new Dimension(250, 120)); // TCDECREL-2798
                                                                       // :
                                                                       // Akhilesh
            
            reasonforChange = (iTextArea) novFormProperties.getProperty("reasonforchange");
            reasonforChange.setLineWrap(true);
            reasonforChange.setWrapStyleWord(true);
            reasonforChange.setEditable(false);
            JScrollPane reasonchagePane = new JScrollPane(reasonforChange);
            // reasonchagePane.setPreferredSize(new Dimension(455,138));
            reasonchagePane.setPreferredSize(new Dimension(555, 138)); // TCDECREL-2798
                                                                       // :
                                                                       // Akhilesh
            JPanel reasonChangePanel = new JPanel(new PropertyLayout());
            TitledBorder tb = new javax.swing.border.TitledBorder("Reason for Change from ECR");
            tb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
            tb.setTitleColor(Color.BLACK);
            reasonChangePanel.setBorder(tb);
            reasonChangePanel.add("1.1.left.center", reasonchagePane);
            
            additionalComments = (iTextArea) novFormProperties.getProperty("additionalcomments");
            additionalComments.setLineWrap(true);
            additionalComments.setWrapStyleWord(true);
            JScrollPane addtnCommentsPane = new JScrollPane(additionalComments);
            // addtnCommentsPane.setPreferredSize(new Dimension(665,138));
            // TCDECREL-2798 : Start : Akhilesh
            addtnCommentsPane.setPreferredSize(new Dimension(825, 138));
            // JPanel addtnCmentsPanel = new JPanel(new PropertyLayout());
            JPanel addtnCmentsPanel = new JPanel(new BorderLayout(0, 1));
            TitledBorder adntb = new javax.swing.border.TitledBorder("Reason for Change");//4824
            adntb.setTitleFont(new Font("Dialog", java.awt.Font.BOLD, 12));
            adntb.setTitleColor(Color.BLACK);
            addtnCmentsPanel.setBorder(adntb);
            // addtnCmentsPanel.add("1.1.left.center.resizable.preferred",addtnCommentsPane);
            addtnCmentsPanel.add(addtnCommentsPane, BorderLayout.CENTER);
            
            JPanel p3Panel = new JPanel(new BorderLayout(0, 1));
            p3Panel.add(targetCRsPanel, BorderLayout.CENTER);
            p3Panel.add(reasonChangePanel, BorderLayout.LINE_END);
            // p3Panel.add(addtnCmentsPanel);
            // TCDECREL-2798 : End
            
            //TCDECREL-6163:Start
            JPanel novelPrjctDistributionPanel = new JPanel(new BorderLayout(0, 1));
            m_novelPrjctPanel = new NOVECONovelProjectPanel(this, formProps);
            
            distributionPanel = (NOVECODistributionPanel) formProps.getProperty("distribution");
            //distributionPanel.setTitleBorderText("Select Distribution");
            //((TitledBorder) distributionPanel.getBorder()).setTitleColor(Color.BLACK);
            //distributionPanel.setNewDimention(860, 250); 
            novelPrjctDistributionPanel.add(m_novelPrjctPanel, BorderLayout.LINE_START);
            novelPrjctDistributionPanel.add(distributionPanel, BorderLayout.LINE_END);
            novelPrjctDistributionPanel.setBorder( new javax.swing.border.TitledBorder(""));
            //TCDECREL-6163:End
            
            extPanel = new SuportingFilesPanel(formProps, false, this);
            extPanel.resizePanel(new Dimension(840, 200)); // TCDECREL-2798 :
                                                           // Akhilesh
            
            this.setLayout(new PropertyLayout(0, 0, 0, 0, 0, 0));
            setDistributionPanelEnable();//4907
            
            add("1.1.left.center", chNamePanel);
            // TCDECREL-2798 : Start : Akhilesh
            add("2.1.left.center.resizable.preferred", iniPanel);
            add("3.1.left.center.resizable.preferred", targetItemsPanel);
            add("4.1.left.center.resizable.preferred", p3Panel);
            add("5.1.left.center.resizable.preferred", addtnCmentsPanel);
            add("6.1.left.center.resizable.preferred", novelPrjctDistributionPanel);
            //add("7.1.left.center.resizable.preferred", distributionPanel);
            add("7.1.left.center.resizable.preferred", extPanel);
            // TCDECREL-2798 : End
            
            // this.setPreferredSize(new Dimension(680,1150));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        // com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("Create UI End");
    }
  //4907-start
    public void setDistributionPanelEnable()
    {
    	TCReservationService reservService = ecoForm.getSession().getReservationService();
    	try
    	{
    	if(! (ecoForm.okToModify())|| isEcoReleased)
		{
    		distributionPanel.enableDistributionPanel(false);
		}
		else if(reservService.isReserved(ecoForm) )
		{
			distributionPanel.enableDistributionPanel(true);
		}
		else
		{
			distributionPanel.enableDistributionPanel(false);
		}
		}catch (TCException e)
		{
			e.printStackTrace();
		}
		
    	
    }
    //4907-end 
    public OpenFormDialog initECODialog()
    {
        Component parent = _EngChangeObjectForm_Panel.this.getParent();
        do
        {
            if (parent instanceof OpenFormDialog)
            {
                return ((OpenFormDialog) parent);
                
            }
            if (parent instanceof FormViewer)
                break;
            parent = parent.getParent();
        }
        while (parent != null);
        return null;
    }
    
    public void deleteJob()
    {
        try
        {
            // String ecoName = ecoForm.getProperty("object_name");
            String jobName = ecoForm.getCurrentJob().getProperty("object_name");
            
            // String ecoPuid = ecoForm.getUid();
            String jobPuid = ecoForm.getCurrentJob().getUid();
            
            TCReservationService reserv = ecoForm.getSession().getReservationService();
            if (reserv.isReserved(ecoForm))
            {
                reserv.unreserve(ecoForm);
            }
            
            TCPreferenceService prefService = ecoForm.getSession().getPreferenceService();
            String userpwd = prefService.getString(TCPreferenceService.TC_preference_site, "CETGServicesV1.3_PWD");
            if (userpwd != null && userpwd.length() > 0)
            {
                
                // This URL points to Weblogic Test Server
                TCComponentSite site = ecoForm.getSession().getCurrentSite();
                TCSiteInfo info = site.getSiteInfo();
                String database = info.getSiteName();
                String wsUrl = null;
                
                if (database.indexOf("Test") != -1)
                    wsUrl = "http://srvhouplmt01:7081/cetgws/V1.3/CETGServices?WSDL";
                else
                    wsUrl = "http://cetws.shp.nov.com:7081/cetgws/V1.3/CETGServices?WSDL";
                CETGServicesProxy proxy = new CETGServicesProxy();
                proxy.setEndpoint(wsUrl);
                
                CredentialsInfo credentials = proxy.getCredentials("webservices", userpwd);//
                System.out.println("cred=" + credentials.getUsername());
                credentials.setLiveResultsOnly(true);
                
                if (jobPuid != null)
                {
                    
                    boolean ret = proxy.deleteObject(credentials, jobPuid);
                    if (ret)
                    {
                        // ecoForm.refresh();
                        System.out.println("The Workflow Job Object : " + jobName + " is Deleted");
                    }
                    
                    // if (ecoPuid != null) {
                    // ret = proxy.deleteObject(credentials, ecoPuid);
                    // }
                    // if (ret) {
                    // // ecoForm.refresh();
                    // System.out.println("The ECOForm Object : " + ecoName
                    // + "is Deleted");
                    // }
                    
                }
            }
        }
        catch (CETWSFault fault)
        {
            System.out.println("Fault Message: " + fault.getCETWSFault());
        }
        catch (Exception e)
        {
            // System.out.println("Fault Message: ECO is not in JOB");
            e.printStackTrace();
        }
        
    }
    
    public void deleteECO()
    {
        try
        {
            String ecoName = ecoForm.getProperty("object_name");
            // String jobName =
            // ecoForm.getCurrentJob().getProperty("object_name");
            String ecoPuid = ecoForm.getUid();
            // String jobPuid = ecoForm.getCurrentJob().getUid();
            TCReservationService reserv = ecoForm.getSession().getReservationService();
            if (reserv.isReserved(ecoForm))
            {
                reserv.unreserve(ecoForm);
            }
            TCPreferenceService prefService = ecoForm.getSession().getPreferenceService();
            String userpwd = prefService.getString(TCPreferenceService.TC_preference_site, "CETGServicesV1.3_PWD");
            if (userpwd != null && userpwd.length() > 0)
            {
                // This URL points to Weblogic Test Server
                TCComponentSite site = ecoForm.getSession().getCurrentSite();
                TCSiteInfo info = site.getSiteInfo();
                String database = info.getSiteName();
                String wsUrl = null;
                if (database.indexOf("Test") != -1)
                    wsUrl = "http://srvhouplmt01:7081/cetgws/V1.3/CETGServices?WSDL";
                else
                    wsUrl = "http://cetws.shp.nov.com:7081/cetgws/V1.3/CETGServices?WSDL";
                CETGServicesProxy proxy = new CETGServicesProxy();
                proxy.setEndpoint(wsUrl);
                CredentialsInfo credentials = proxy.getCredentials("webservices", userpwd);//
                System.out.println("cred=" + credentials.getUsername());
                credentials.setLiveResultsOnly(true);
                // if (jobPuid != null) {
                //
                // boolean ret = proxy.deleteObject(credentials, jobPuid);
                // if (ret) {
                // // ecoForm.refresh();
                // System.out.println("The Workflow Job Object : "
                // + jobName + " is Deleted");
                // }
                
                if (ecoPuid != null)
                {
                    boolean ret = proxy.deleteObject(credentials, ecoPuid);
                    if (ret)
                    {
                        // ecoForm.refresh();
                        System.out.println("The ECOForm Object : " + ecoName + "is Deleted");
                        dispTargetsMap.clear();
                        m_isEcoDeleted = true;
                    }
                }
                
                // }
            }
        }
        catch (CETWSFault fault)
        {
            System.out.println("Fault Message: " + fault.getCETWSFault());
        }
        catch (Exception e)
        {
            // System.out.println("Fault Message: ECO is not in JOB");
            e.printStackTrace();
        }
        
    }
    
    private void buildNewLifeCycleMap()
    {
        try
        {
            String[] uidLifecycleArr = ecoForm.getTCProperty("targetsnewlifecycle").getStringValueArray();
            for (int i = 0; i < uidLifecycleArr.length; i++)
            {
                String[] uidLifeCycle = uidLifecycleArr[i].split("::");
                if (uidLifeCycle.length == 2)
                {
                    TCComponent itemRev = ecoForm.getSession().stringToComponent(uidLifeCycle[0]);
                    System.out.println(" New Llifecycle for " + itemRev.getProperty("item_id") + " is : "
                            + uidLifeCycle[1]);
                    targetItemsPanel.targetTable.newLifecycleMap.put(itemRev, uidLifeCycle[1]);
                } 
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    public void saveNewLifecycle()
    {
        Vector<TCComponent> targetsVec = getECOTargets();
        Map<TCComponent, String> newLifecycleMap = targetItemsPanel.targetTable.newLifecycleMap;
        TCComponent[] itemRevComps = newLifecycleMap.keySet().toArray(new TCComponent[newLifecycleMap.keySet().size()]);
        Vector<String> nLifecycleVLAVec = new Vector<String>();
        // String[] newLifecycleVLA = new String[itemRevComps.length];
        for (int i = 0; i < itemRevComps.length; i++)
        {
            // Synchronizing with targets
            if (targetsVec.contains(itemRevComps[i]))
            {
                nLifecycleVLAVec.add(itemRevComps[i].getUid() + "::" + newLifecycleMap.get(itemRevComps[i]));
            }
        }
        
        try
        {
            String[] newLifecycleVLA = nLifecycleVLAVec.toArray(new String[nLifecycleVLAVec.size()]);
            TCProperty newLifecycleProp = ecoForm.getTCProperty("targetsnewlifecycle");
            
            if (newLifecycleVLA.length > 0)
            {
                newLifecycleProp.setStringValueArray(newLifecycleVLA);
            }
            else
            {
                newLifecycleProp.setStringValueArray(new String[0]);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public String[] getNewLifecycleValues()
    {
        Vector<TCComponent> targetsVec = getECOTargets();
        Map<TCComponent, String> newLifecycleMap = targetItemsPanel.targetTable.newLifecycleMap;
        TCComponent[] itemRevComps = newLifecycleMap.keySet().toArray(new TCComponent[newLifecycleMap.keySet().size()]);
        Vector<String> nLifecycleVLAVec = new Vector<String>();
        // String[] newLifecycleVLA = new String[itemRevComps.length];
        for (int i = 0; i < itemRevComps.length; i++)
        {
            // Synchronizing with targets
            if (targetsVec.contains(itemRevComps[i]))
            {
                nLifecycleVLAVec.add(itemRevComps[i].getUid() + "::" + newLifecycleMap.get(itemRevComps[i]));
            }
        }
        
        return nLifecycleVLAVec.toArray(new String[nLifecycleVLAVec.size()]);
    }
    
    private JPanel getInitPanel()
    {
        
        JPanel iniPanel = new JPanel(new PropertyLayout());
        NOIJLabel typelbl = new NOIJLabel("Type of Change:");
        NOIJLabel chcatlbl = new NOIJLabel("Change Category:");
        NOIJLabel grplbl = new NOIJLabel("Group:");
        NOIJLabel wflbl = new NOIJLabel("Workflow:");
        NOIJLabel reasoncodelbl = new NOIJLabel("Reason Code:");
        NOIJLabel productLinelbl = new NOIJLabel("Product Line:");
        
        NOIJLabel ecoClassificationlbl = new NOIJLabel(m_registry.getString("ECOClassificationLable.Name"));// TCDECREL-4471
        JTextField typeofChangelbl = (JTextField) novFormProperties.getProperty("changetype");
        typeofChangelbl.setEditable(false);
        typeofChangelbl.setColumns(16);
        // typeofChangelbl.setPreferredSize(new Dimension(120,20));
        typeofChange = ((JTextField) novFormProperties.getProperty("changetype")).getText();
        typeofChangelbl.setPreferredSize(new Dimension(120, 20));
        changeCat = (JComboBox) novFormProperties.getProperty("changecat");
        changeCat.setEnabled(false);
        changeCat.setPreferredSize(new Dimension(125, 20));
        groupCmb = (JComboBox) novFormProperties.getProperty("group");
        groupCmb.setPreferredSize(new Dimension(188, 20));
        workflowType = (JTextField) novFormProperties.getProperty("wfname");
        workflowType.setEditable(false);
        workflowType.setToolTipText(workflowType.getText());
        workflowType.setPreferredSize(new Dimension(160, 20));
        // workflowType.setSize(50, 20);
        workflowType.setColumns(25);
        reasonCode = (JComboBox) novFormProperties.getProperty("reasoncode");
        reasonCode.setPreferredSize(new Dimension(147, 20));
        
        // Rakesh - 27.12.2011 - DHL ECO 2.0
        productLine = (JComboBox) novFormProperties.getProperty("nov4_product_line");
        productLine.setPreferredSize(new Dimension(206, 20));
        // TCDECREL-4471-start
        m_formPanelECOClassification = (JComboBox) novFormProperties.getProperty("nov4_ecoclassification");
        m_formPanelECOClassification.setPreferredSize(new Dimension(206, 20));// TCDECREL-4471-end
        //TCDECREL-5889:Start
        m_ECOHelp = new JButton("ECO Help"); 
        m_ECOHelp.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                TCPreferenceService prefService = getSession().getPreferenceService();
                String ECOHelp = prefService.getString(TCPreferenceService.TC_preference_site,"NOV_DHL_ECOHelp");
                try
                {
                    Runtime.getRuntime().exec("cmd /c start "+ ECOHelp);
                }
                catch ( Exception ex )
                {
                    ex.printStackTrace();
                }              
            }
        });
        //TCDECREL-5889:End
        // Rakesh - 27.12.2011 - DHL ECO 2.0
        
        /*
         * iniPanel.add("1.1.left.center",typelbl);
         * iniPanel.add("1.2.left.center",typeofChangelbl);
         * iniPanel.add("1.3.left.center",chcatlbl);
         * iniPanel.add("1.4.left.center",changeCat);
         * iniPanel.add("1.5.left.center",grplbl);
         * iniPanel.add("1.6.left.center",groupCmb);
         * iniPanel.add("2.1.left.center",wflbl);
         * iniPanel.add("2.2.left.center",workflowType);
         * iniPanel.add("2.3.right.center",reasoncodelbl);
         * iniPanel.add("2.4.left.center",reasonCode);
         * iniPanel.add("2.5.right.center",productLinelbl);
         * iniPanel.add("2.6.left.center",productLine);
         */

        iniPanel.add("1.1.left.center", productLinelbl);
        iniPanel.add("1.2.left.center", productLine);
        iniPanel.add("1.3.left.center", grplbl);
        iniPanel.add("1.4.left.center", groupCmb);
        iniPanel.add("1.5.left.center", typelbl);
        iniPanel.add("1.6.left.center", typeofChangelbl);
        iniPanel.add("2.1.left.center", wflbl);
        iniPanel.add("2.2.left.center", workflowType);
        iniPanel.add("2.3.left.center", chcatlbl);
        iniPanel.add("2.4.left.center", changeCat);
        iniPanel.add("2.5.right.center", reasoncodelbl);
        iniPanel.add("2.6.left.center", reasonCode);
        // TCDECREL-4471-start
        iniPanel.add("3.1.right", ecoClassificationlbl);
        iniPanel.add("3.2.left.center", m_formPanelECOClassification);// TCDECREL-4471-end
        iniPanel.add("3.3.left.center", m_ECOHelp); //TCDECREL-5889
        return iniPanel;
    }
    
    public void setFormModifiableProperty(boolean isModifiable)
    {
        targetItemsPanel.setEnabled(isModifiable);
        targetItemsPanel.enableButtons(isModifiable);
        cancelProcessBtn.setEnabled(isModifiable);
        //distributionPanel.setEnabled(isModifiable);
        distributionPanel.enableDistributionPanel(isModifiable); //TCDECREL-6163
        m_novelPrjctPanel.enableNovelProjectPanel(isModifiable); //TCDECREL-6163
        targetCRsPanel.setEnabled(isModifiable);
        extPanel.setEnabled(isModifiable);
        reasonCode.setEnabled(isModifiable);
        groupCmb.setEnabled(isModifiable);
        additionalComments.setEditable(isModifiable);
        productLine.setEnabled(isModifiable);
        m_formPanelECOClassification.setEnabled(isModifiable);// TCDECREL-4471
        //distributionPanel.disableButtons();
    }
    
    public Vector<TCComponent> getECOTargets()
    {
        ecoTargets.removeAllElements();
        m_TargetsWithDisp.removeAllElements();
        loadEnTargets(ecoForm);
        return ecoTargets;
    }
    
    public void saveForm()
    {
        ((_EngChangeObjectForm_DC) novFormProperties).saveFormData();
    }
    
    public void updateCRsList(TCComponentForm crForm)
    {
        if (crForm != null)
        {
            ((DefaultListModel) targetCRsPanel.crsList.getModel()).addElement(crForm);
        }
        targetCRsPanel.crsList.updateUI();
        try
        {
            ecoForm.add("_ECNECR_", crForm);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        Vector<TCComponentForm> crForms = new Vector<TCComponentForm>();
        for (int i = 0; i < targetCRsPanel.crsList.getModel().getSize(); i++)
        {
            crForms.add((TCComponentForm) targetCRsPanel.crsList.getModel().getElementAt(i));
        }
        TCComponentForm[] crforms = crForms.toArray(new TCComponentForm[crForms.size()]);
        String reasonforChangestr = NOVECOHelper.getReason4ChangeString(crforms);
        reasonforChange.setText(reasonforChangestr);
    }
    
    public void removeRevisionsFromTarget(TCComponentItemRevision removecomp)
    {
        try
        {
            for (int i = 0; i < targetItemsPanel.targetTable.getRowCount(); i++)
            {
                TreeTableNode treeTblNode = targetItemsPanel.targetTable.getNodeForRow(i);
                
                if (treeTblNode instanceof NovTreeTableNode)
                {
                    TCComponent trgRev = ((NovTreeTableNode) treeTblNode).targetRev;
                    // ((NovTreeTableNode) treeTblNode).getProperty(name)
                    // TCComponent dispComp =
                    // ((NovTreeTableNode)treeTblNode).dispComp;
                    if (trgRev != null && removecomp != null)
                    {
                        if (removecomp.getProperty("item_id").equals(
                                ((TCComponentItemRevision) trgRev).getProperty("item_id")))
                        {
                            /*
                             * it will remove target from workflow and eco form
                             * both.
                             */
                            targetItemsPanel.targetTable.removeTargetsandCleanup(i);
                            
                            /*
                             * TCDECREL-2330: newly created revision will get
                             * deleted , created as a result of only General
                             * Type of change
                             */
                            if (ecoForm.getProperty("changetype").equalsIgnoreCase("General"))
                            {
                                if (((NovTreeTableNode) treeTblNode).getProperty("Old Rev").trim().length() > 0)
                                {
                                    TCComponentItemRevision rddDocRev = null;
                                    String trgRevId = trgRev.getProperty("item_revision_id");
                                    AIFComponentContext[] comps = trgRev.getSecondary();
                                    
                                    for (int j = 0; j < comps.length; j++)
                                    {
                                        if (comps[j].getContext().equals("RelatedDefiningDocument"))
                                        {
                                            InterfaceAIFComponent infDocItem = comps[j].getComponent();
                                            if (infDocItem instanceof TCComponentItem
                                                    && infDocItem.getType().equalsIgnoreCase("Documents"))
                                            {
                                                rddDocRev = ((TCComponentItem) infDocItem).getLatestItemRevision();
                                                String rddDocRevId = rddDocRev.getProperty("item_revision_id");
                                                if (!rddDocRevId.equals(trgRevId))
                                                {
                                                    rddDocRev = null;
                                                }
                                                
                                                break;
                                            }
                                        }
                                    }
                                    // System.out.println("Before SWIM delete");
                                    /*
                                     * deleting swimrelation b/w cad part
                                     * revision and RDD drwing dataset
                                     */
                                    if (rddDocRev != null && trgRev != null)
                                        deleteSwimRelation(rddDocRev, (TCComponentItemRevision) trgRev);
                                    // System.out.println("Before delete dataset");
                                    /* Deleting the datasets and item rev */
                                    deleteDatasets((TCComponentItemRevision) trgRev);
                                    // System.out.println("Before delete rev");
                                    trgRev.delete();
                                    // Deleting the datasets and documents rev
                                    // System.out.println("Before delete docrev");
                                    if (rddDocRev != null)
                                    {
                                        deleteDatasets(rddDocRev);
                                        rddDocRev.delete();
                                    }
                                    break;
                                }
                            }
                        }
                        
                    }
                    ((NovTreeTableNode) treeTblNode).targetRev = null;
                    
                }
            }
        }
        catch (TCException e)
        {
            MessageBox.post("Cannot delete Revision. " + e.getError(), "Error", MessageBox.ERROR);
            e.printStackTrace();
        }
        
    }
    
    private void deleteSwimRelation(TCComponentItemRevision docRev, TCComponentItemRevision itemRev)
    {
        try
        {
            AIFComponentContext[] aifCompsLst = docRev.getSecondary();
            
            for (int i = 0; i < aifCompsLst.length; i++)
            {
                if (aifCompsLst[i].getComponent() instanceof TCComponentDataset)
                {
                    try
                    
                    {
                        /* deleting swimdependency */

                        if (aifCompsLst[i].getComponent().getType().equalsIgnoreCase("SWDrw"))
                        {
                            System.out.println("drawing datase exists");
                            ((TCComponentDataset) aifCompsLst[i].getComponent()).remove("SWIM_dependency", itemRev);
                        }
                        
                    }
                    catch (Exception e)
                    {
                        continue;
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void deleteDatasets(TCComponentItemRevision itemRev)
    {
        try
        {
            AIFComponentContext[] aifCompsLst = itemRev.getSecondary();
            
            for (int i = 0; i < aifCompsLst.length; i++)
            {
                if (aifCompsLst[i].getComponent() instanceof TCComponentDataset)
                {
                    try
                    {
                        ((TCComponentDataset) aifCompsLst[i].getComponent()).delete();
                        
                    }
                    catch (Exception e)
                    {
                        continue;
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    /*********************** SOA service call implementation *************************/
    public boolean _addComponentToTarget(String context, TCComponent[] selcomp)
    {
        
        NOV4ChangeManagement.ECOAddTargetsResponse theResponse = null;
        
        TCSession session = (TCSession) AIFUtility.getActiveDesktop().getCurrentApplication().getSession();
        
        NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
        NOV4ChangeManagement.ECOAddTargetInput input = new NOV4ChangeManagement.ECOAddTargetInput();
        
        input.context = context;
        input.ecoForm = ecoForm;
        input.targetItems = selcomp;
        
        theResponse = service.addTargetsToECO(input);
        
        if (theResponse.serviceData.sizeOfPartialErrors() > 0) // Check for
                                                               // partial errors
        {
            ErrorStack errorStack = theResponse.serviceData.getPartialError(0);
            String errorMsg = (errorStack.getMessages())[0];
            // MessageBox.post(errorMsg, "Cannot add Item to target",
            // MessageBox.WARNING);
            JOptionPane.showMessageDialog(this, errorMsg, "Cannot add Item to target", JOptionPane.WARNING_MESSAGE);
            
            return false;
        }
        
        return true;
    }
    
    /**********************************************************************************/
    
    public boolean addValidRevisionToTarget(TCComponentItemRevision selcomp)
    {
        try
        {
            TCComponent[] releaseStatus = selcomp.getTCProperty("release_status_list").getReferenceValueArray();
            if (typeofChange.equals("General"))
            {
                if (releaseStatus.length > 0)
                {
                    TCComponentItemRevision newRevision = getNewRevision(selcomp);
                    if (newRevision != null)
                    {
                        TCComponent dipsComp = null;
                        if (!newRevision.getType().equalsIgnoreCase("Documents Revision"))
                        {
                            dipsComp = createDispositionComp(selcomp);
                            TCComponent[] dispComps = new TCComponent[1];
                            dispComps[0] = dipsComp;
                            dipsComp.setStringProperty("oldrev", selcomp.getProperty("current_revision_id"));
                            String relStatus = newRevision.getItem().getProperty("release_statuses");
                            dipsComp.setStringProperty("oldlifecycle", relStatus);
                            dispPanel.disptargetMap.put(newRevision, dipsComp);
                            dispPanel.targetdispMap.put(dipsComp, newRevision);
                            dispPanel.adddispTabs(dispComps);
                        }
                        
                        boolean isItemAdded = targetItemsPanel.addToTargetList(newRevision);
                        
                        if (isItemAdded)
                        {
                            targetItemsPanel.targetTable.addTarget(newRevision, dipsComp);
                        }
                        // saveDatatoForm();
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    TCComponent[] revisioncnt = selcomp.getItem().getTCProperty("revision_list")
                            .getReferenceValueArray();
                    if ((revisioncnt.length > 1) && (!selcomp.getType().equalsIgnoreCase("Documents Revision")))
                    {
                        boolean isItemAdded = targetItemsPanel.addToTargetList(selcomp);
                        
                        if (isItemAdded)
                        {
                            TCComponent dipsComp = createDispositionComp(selcomp);
                            TCComponent[] dispComps = new TCComponent[1];
                            dispComps[0] = dipsComp;
                            String relStatus = selcomp.getItem().getProperty("release_statuses");
                            dipsComp.setStringProperty("oldlifecycle", relStatus);
                            // dipsComp.setStringProperty("oldrev",
                            // selcomp.getProperty("current_revision_id"));
                            dispPanel.disptargetMap.put(selcomp, dipsComp);
                            dispPanel.targetdispMap.put(dipsComp, selcomp);
                            dispPanel.adddispTabs(dispComps);
                            targetItemsPanel.targetTable.addTarget(selcomp, dipsComp);
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        boolean isItemAdded = targetItemsPanel.addToTargetList(selcomp);
                        
                        if (isItemAdded)
                        {
                            targetItemsPanel.targetTable.addTarget(selcomp, null);
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // saveDatatoForm();
                    return true;
                }
            }
            else if (typeofChange.equals("Lifecycle"))
            {
                if (releaseStatus.length > 0)
                {
                    TCComponentItemRevision[] releasedRevs = selcomp.getItem().getReleasedItemRevisions();
                    if (selcomp.equals(releasedRevs[0]))
                    {
                        boolean isItemAdded = targetItemsPanel.addToTargetList(selcomp);
                        if (isItemAdded)
                        {
                            targetItemsPanel.targetTable.addTarget(selcomp, null);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.post("Select Latest Released Revision.", "Info", MessageBox.INFORMATION);
                    }
                }
                else
                {
                    MessageBox.post("Select only Latest Released Revision for LifeCycle Change.", "Info",
                            MessageBox.INFORMATION);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public TCComponent createDispositionComp(TCComponentItemRevision selcomp)
    {
      /*  try
        {
            TCUserService userService = ecoForm.getSession().getUserService();
            Object[] args = new Object[1];
            args[0] = new String[] { selcomp.getProperty("item_id") };
            Object comp = userService.call("NATOIL_createEcoDispositionData", args);
            if (comp != null)
            {
                TCComponent[] comps = (TCComponent[]) comp;
                return comps[0];
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return null;
        */
        
        TCComponent comps = null;
        try
        {
           /* TCUserService userService = targetECO.getSession().getUserService();
            Object[] args = new Object[1];
            args[0] = new String[] { selcomp.getProperty("item_id") };
            Object comp = userService.call("NATOIL_createEcoDispositionData", args);
            if (comp != null)
            {
                TCComponent[] tcComp = (TCComponent[])comp;
				comps = tcComp[0];
            }
            */
        	
			CreateInObjectHelper createInObjectHelper = new CreateInObjectHelper(ecoForm.getType(), CreateInObjectHelper.OPERATION_CREATE);
			
			createInObjectHelper.setString("targetitemid", selcomp.getProperty("item_id"));
			
			//other properties are set as empty in user service.
			//Need to check if these empty properties are need to set as manually or default getting set to empty
			
			CreateObjectsSOAHelper.createUpdateObjects(createInObjectHelper);
			
			TCComponent[] createdObejcts = CreateObjectsSOAHelper.getCreatedOrUpdatedObjects(createInObjectHelper);
        
            if(createdObejcts != null && createdObejcts.length >0)
            {
               TCComponent partRevision = createdObejcts[0];
               if(partRevision instanceof TCComponentItemRevision)
               {
            	   comps = (TCComponentItemRevision) partRevision;
               }
            }			
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return comps;        
    }
    
    private TCComponentItemRevision getNewRevision(TCComponentItemRevision selcomp)
    {
    	TCComponentItemRevision createdItemRev = null;
        try
        {
           /* TCUserService userService = ((TCSession) AIFUtility.getDefaultSession()).getUserService();
            Object[] args = new Object[1];
            args[0] = new TCComponent[] { selcomp };
            Object comp = userService.call("NATOIL_deepcopyRev", args);
            if (comp != null)
            {
                TCComponent[] comps = (TCComponent[]) comp;
                return (TCComponentItemRevision) comps[0];
            }
            /*
             * TCComponentItemRevision newRev =
             * selcomp.getItem().revise(selcomp.getItem().getNewRev(),
             * selcomp.getProperty("object_name"), ""); return newRev;
             */
 
			IPropertyMap reviseProperMap = new SimplePropertyMap();

			TCComponent revMasterForm = selcomp
					.getRelatedComponent("IMAN_master_form_rev");

			reviseProperMap.setComponent(selcomp);

			CreateInCompoundHelper revCompoundHelper = new CreateInCompoundHelper(
					reviseProperMap);
			revCompoundHelper.addCompound("ItemRevision",
					"IMAN_master_form_rev");

			IPropertyMap revMasterFormPropMap = reviseProperMap
					.getCompound("IMAN_master_form_rev");

			PropertyMapHelper.componentToMap(selcomp, new String[] { "item_id",
					"object_name", "object_desc" }, reviseProperMap);

			TCProperty[] tcPropertyObject = revMasterForm.getAllTCProperties();
			PropertyMapHelper.addPropertiesToMap(tcPropertyObject,
					revMasterFormPropMap);

			reviseProperMap.setCompound("IMAN_master_form_rev",
					revMasterFormPropMap);
			ReviseObjectsSOAHelper reviseHelper = new ReviseObjectsSOAHelper();
			reviseHelper.reviseObjects(new IPropertyMap[] { reviseProperMap });
        }
        catch (TCException e)
        {
            System.out.println("New revision can not be created");
            MessageBox.post("Item Revision " + selcomp.toString()
                    + " cannot be revised. Please check the access. Save ECO form to refresh.", "Error",
                    MessageBox.ERROR);
            // e.printStackTrace();
        }
        return createdItemRev;
    }
    
    /**
     * \brief This method takes the _DHL_EngChangeForm_ and gets the process
     * attached to it. From this process it checks for the attachments and if it
     * has the ItemRevisions attached then creates a map of the item_id ( of
     * ItemRevision ) and ItemRevision also creates the vector of ItemRevisions.
     * \param[in] engChangeForm A TCComponentForm whic is _DHL_EngChangeForm_.
     */
    public void loadEnTargets(TCComponentForm engChangeForm)
    {
        try
        {
            // ECO Targets
            AIFComponentContext[] contexts = engChangeForm.whereReferenced();
            if (contexts != null)
            {
                for (int j = 0; j < contexts.length; j++)
                {
                    if ((TCComponent) contexts[j].getComponent() instanceof TCComponentProcess)
                    {
                        currentProcess = (TCComponentProcess) contexts[j].getComponent();
                        TCComponentTask rootTask = ((TCComponentProcess) currentProcess).getRootTask();
                        rootTask.refresh();
                        wftargets = rootTask.getAttachments(TCAttachmentScope.LOCAL, TCAttachmentType.TARGET);
                        ecoTargetsMap.clear();
                        m_TargetsWithDisp.clear();
                        for (int i = 0; i < wftargets.length; i++)
                        {
                            if (wftargets[i] instanceof TCComponentItemRevision)
                            {
                                ecoTargetsMap.put(wftargets[i].getProperty("item_id"), wftargets[i]);
                                ecoTargets.add(wftargets[i]);
                                
                                TCComponentItem item = ((TCComponentItemRevision)wftargets[i]).getItem();
                                TCComponent[] releasedRevs = item.getReleasedItemRevisions();
                                if ((!item.getType().equalsIgnoreCase("Documents")) && (releasedRevs.length > 0) && (NOVECOHelper.isItemArchived(item) == false))
                                {
                                	m_TargetsWithDisp.add(wftargets[i]);
                                	targetId.add(wftargets[i].getProperty("item_id"));//7398
                                }
                            }
                        }
                        break;
                    }
                }
            }
            if (currentProcess == null)
            {
                for (int inx = 0; inx < contexts.length; inx++)
                {
                	TCComponent component = (TCComponent) contexts[inx].getComponent();
                    if (contexts[inx].getContext().equals("ReleatedECN")
                            && (component instanceof TCComponentItemRevision))
                    {
                        ecoTargets.add(component);
                        
                        TCComponentItem item = ((TCComponentItemRevision)component).getItem();
                        TCComponent[] releasedRevs = item.getReleasedItemRevisions();
                        if ((!item.getType().equalsIgnoreCase("Documents")) && (releasedRevs.length > 0))
                        {
                        	m_TargetsWithDisp.add(component);
                        	targetId.add(component.getProperty("item_id"));//7398
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Exception is:" + e);
            
        }
        
    }
    
    public void synchronizeWFTargets(TCComponentForm engChangeForm)
    {
        try
        {
        	if(m_isEcoDeleted == false) //To fix console exceptions on deleted eco
        	{
        		
        		// Getting targetslist
        		ecoTargets.removeAllElements();
        		m_TargetsWithDisp.removeAllElements();
        		loadEnTargets(engChangeForm);
        		// ecoTargets
        		ecoForm = engChangeForm;

        		// Getting Disposition instances

        		TCProperty targetdispProperty = ecoForm.getTCProperty("ectargetdisposition");
        		TCComponent[] dispComps = targetdispProperty.getReferenceValueArray();
        		String changeType = engChangeForm.getProperty("changetype");
        		if((changeType.equalsIgnoreCase("General") == true) && (m_TargetsWithDisp.size() != dispComps.length))
        		{
        			//Fix for session lag time issue
        			engChangeForm.refresh();
        			dispComps = ecoForm.getTCProperty("ectargetdisposition").getReferenceValueArray();
        		}
        		//7398-start
        		else if(m_TargetsWithDisp.size() == dispComps.length && changeType.equalsIgnoreCase("General"))
                {
                    for(int i=0;i<dispComps.length;i++)
                    {
                        String dispid=dispComps[i].getProperty("targetitemid");
                        if(!targetId.contains(dispid))
                        {
                            engChangeForm.refresh();
                            dispComps = ecoForm.getTCProperty("ectargetdisposition").getReferenceValueArray();
                            break;
                        }
                       
                    }
                }
                
                //7398-end
        		getTargetDisposition(dispComps);

        		// Getting Newlifecycle list
        		String[] targetnewlfVals = engChangeForm.getTCProperty("targetsnewlifecycle").getStringValueArray();

        		// Getting ECRs list
        		// String[] relation = {"_ECNECR_"};
        		// String[] relObjsType = {"_EngChangeRequest_"};
        		AIFComponentContext[] formComps = engChangeForm.getRelated("_ECNECR_");
        		Vector<TCComponentForm> formsVec = new Vector<TCComponentForm>();
        		for (int i = 0; i < formComps.length; i++)
        		{
        			InterfaceAIFComponent infComp = formComps[i].getComponent();
        			if (infComp.getType().equalsIgnoreCase("_EngChangeRequest_"))
        			{
        				formsVec.add((TCComponentForm) infComp);
        			}
        		}

        		// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("validateECR");
        		// Validate ECR item exist if it is referred to any of the items
        		// then check at least one item exist in the targets list

        		//validateECR(engChangeForm, formsVec);   //TCDECREL-3412 : Donnie has confirmed ECO can contain only ECRs with out any of its associated items

        		// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("validateTargets");
        		// Validate targets for checking latest revisions is added to
        		// workflow and it doesn't contain multiple working revision
        		// Validation for checked out object for both part and RDD, if
        		// target is attached to ECR, check it is referred to ECO form
        		validateTargets(engChangeForm, formsVec);

        		// validateRelatedObjectsAddedToWF();

        		// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("ValidateTargetsNewLifecycle");
        		ValidateTargetsNewLifecycle(engChangeForm, targetnewlfVals);

        		// Reset the targets List
        		// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("deleteDispositions");
        		// Deleting unwanted dispositions
        		deleteDispositions();

        		// com.nov.rac.dhl.eco.helper.NOVLogger.NOVLog.info("removeTargets");
        		removeTargets();

        		TCComponent[] ecrComps = engChangeForm.getTCProperty("_ECNECR_").getReferenceValueArray();
        		for (int i = 0; i < ecrComps.length; i++)
        		{
        			if (!(ecrComps[i] instanceof TCComponentForm))
        			{
        				syncMsgStr = syncMsgStr + (ecrComps[i]).getProperty("object_name") + "::"
        				+ (ecrComps[i]).getProperty("object_type") + "::"
        				+ " This is invalid object for relation _ECNECR_ with ECO. Remove it from list." + "!!";
        			}
        		}

        		// Checking the new Reason for change after sync with the value
        		// saved in ECO form, if there is a difference then
        		// the value is saved. This check is added to fix refresh issue when
        		// we open the ECO form.
        		String reasonforchange = NOVECOHelper.getReason4ChangeString(ecrComps);
        		TCProperty reasonProp = engChangeForm.getTCProperty("reasonforchange");
        		String reasonChangeFormValue = reasonProp.getStringValue();
        		
        		int maxLength = reasonProp.getDescriptor().getMaxStringLength();
    			if (reasonforchange.length() > maxLength)
    			{
    				reasonforchange = reasonforchange.substring(0, maxLength);
    			}
        		if (!reasonforchange.equalsIgnoreCase(reasonChangeFormValue))
        		{
        			reasonProp.setStringValue(reasonforchange);
        		}
        		// show sync report
        		if (syncMsgStr.trim().length() > 0)
        		{
        			NOVECOSyncTargetsDialog synctargetsDlg = new NOVECOSyncTargetsDialog(syncMsgStr);
        			synctargetsDlg.pack();
        			synctargetsDlg.setModal(true);
        			synctargetsDlg.setVisible(true);
        			syncMsgStr = "";
        		}
        		// remove all targets from vector as this is again populated in the
        		// create UI method
        		ecoTargets.removeAllElements();
        		m_TargetsWithDisp.removeAllElements();
        	}
        	m_isEcoDeleted = false;
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    private void getTargetDisposition(TCComponent[] dispComps)
    {
    	String[] reqdProperties = { "targetitemid" };
		String[][] retriviedProperties = null;
		try
		{
			retriviedProperties = TCComponentType.getPropertiesSet(Arrays.asList(dispComps), reqdProperties);
		}
		catch (TCException e1)
		{
			e1.printStackTrace();
		}

		for (int index = 0; index < dispComps.length; index++)
		{
			if(!dispTargetsMap.containsKey(retriviedProperties[index][0]))
			{
				dispTargetsMap.put(retriviedProperties[index][0], dispComps[index]);
			}
		}
    }
    
    private void removeTargets()
    {
        try
        {
            if (targetsToBeRemoved.size() > 0)
            {
                if (wftargets != null)
                {
                    Vector<TCComponent> remTargetVec = new Vector<TCComponent>();
                    Vector<TCComponent> wftargetVec = new Vector<TCComponent>(Arrays.asList(wftargets));
                    for (int i = 0; i < targetsToBeRemoved.size(); i++)
                    {
                        TCComponent tobeRem = targetsToBeRemoved.get(i);
                        if (wftargetVec.contains(tobeRem))
                        {
                            remTargetVec.add(tobeRem);
                        }
                    }
                    TCComponent[] compsTobeRemoved = remTargetVec.toArray(new TCComponent[remTargetVec.size()]);
                    // if(accessCtrlSer.checkPrivilege(ecoForm, "WRITE"))
                    {
                        currentProcess.getRootTask().removeAttachments(compsTobeRemoved);
                    }
                    AIFComponentContext[] cxtObjs = currentProcess.getRootTask().getChildren();
                    for (int i = 0; i < cxtObjs.length; i++)
                    {
                        if (cxtObjs[i].getContext().equals("pseudo_folder"))
                        {
                            ((TCComponent) cxtObjs[i].getComponent()).refresh();
                        }
                    }
                    currentProcess.getRootTask().refresh();
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void deleteDispositions()
    {
        Object[] dispObjs = dispCompTobeDeleted.toArray();
        if (dispObjs != null && dispObjs.length > 0)
        {
            TCComponent[] dispComps = dispCompTobeDeleted.toArray(new TCComponent[dispCompTobeDeleted.size()]);
            for (int i = 0; i < dispComps.length; i++)
            {
                try
                {
                	dispComps[i].refresh();
                    dispComps[i].lock();
                    dispComps[i].delete();
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private void ValidateTargetsNewLifecycle(TCComponentForm engChangeForm, String[] targetnewlfVals)
    {
        try
        {
            Vector<String> lifecycleValues = new Vector<String>();
            boolean isSomeTargetRemoved = false;
            if (targetnewlfVals != null)
            {
                for (int i = 0; i < targetnewlfVals.length; i++)
                {
                    String[] uidLifeCycle = targetnewlfVals[i].split("::");
                    if (uidLifeCycle.length == 2)
                    {
                        TCComponent itemRev = engChangeForm.getSession().stringToComponent(uidLifeCycle[0]);
                        if (ecoTargets.contains(itemRev))
                        {
                            lifecycleValues.add(targetnewlfVals[i]);
                        }
                        else
                        {
                            isSomeTargetRemoved = true;
                        }
                    }
                }
            }
            if (isSomeTargetRemoved)
            {
                String[] uidLifeCycle = lifecycleValues.toArray(new String[lifecycleValues.size()]);
                // if(accessCtrlSer.checkPrivilege(ecoForm, "WRITE"))
                {
                    engChangeForm.getTCProperty("targetsnewlifecycle").setStringValueArray(uidLifeCycle);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
    private void validateTargets(TCComponentForm engChangeForm, Vector<TCComponentForm> formsVec)
    {
        TCComponentItemRevision[] itemRevisionArr = ecoTargets.toArray(new TCComponentItemRevision[ecoTargets.size()]);
        String[] reqdProperties = { "item_id", "object_type", "object_string" };
        
        try
        {
            List<TCComponentItemRevision> itemRevisionList = Arrays.asList(itemRevisionArr);//TC10.1 Upgrade
            //String[][] itemRevisionProperties = TCComponentType.getPropertiesSet(itemRevisionArr, reqdProperties);
            String[][] itemRevisionProperties = TCComponentType.getPropertiesSet(itemRevisionList, reqdProperties);//TC10.1 Upgrade
            
            String changeType = engChangeForm.getProperty("changetype");
            for (int i = 0; i < ecoTargets.size(); i++)
            {
                TCComponentItemRevision itemRevision = (TCComponentItemRevision) ecoTargets.get(i);
                // String itemId = itemRevision.getProperty("item_id");
                String itemId = itemRevisionProperties[i][0];
                String objectType = itemRevisionProperties[i][1];
                String objectString = itemRevisionProperties[i][2];
                
                if (changeType.equalsIgnoreCase("General"))
                {
                    // Checking if target is released revision
                    if (NOVECOHelper.isReleasedRevision(itemRevision))
                    {
                        // syncMsgStr =
                        // syncMsgStr+itemId+"::"+(itemRevision).getProperty("object_type")+"::"+"The Revision "+
                        // (itemRevision).getProperty("object_string")+
                        // " is released revision.\nCannot be added to Engineering Change."
                        // + "!!";
                        syncMsgStr = syncMsgStr + itemId + "::" + objectType + "::" + "The Revision " + objectString
                                + " is released revision.\nCannot be added to Engineering Change." + "!!";
                        removeFromTargetList(itemRevision, itemId);
                        continue;
                    }
                    // Multiple Working revision validation
                    if (!itemRevision.isTypeOf("Documents Revision"))
                    {
                        /*TCComponent[] allRevs = itemRevision.getItem().getTCProperty("revision_list")
                                .getReferenceValueArray();
                        TCComponent[] relasedRevs = itemRevision.getItem().getReleasedItemRevisions();
                        if ((allRevs.length - relasedRevs.length) > 1)*/
                        TCComponentItemRevision[] workingRevs = itemRevision.getItem().getWorkingItemRevisions();
                        if (workingRevs.length>1)
                        {
                            // syncMsgStr =
                            // syncMsgStr+itemId+"::"+(itemRevision).getProperty("object_type")+"::"+"The Item "+
                            // (itemRevision).getProperty("item_id")+
                            // " contain many working revisions.Please delete the revision(s) not required.\nCannot be added to Engineering Change."+
                            // "!!";
                            syncMsgStr = syncMsgStr
                                    + itemId
                                    + "::"
                                    + objectType
                                    + "::"
                                    + "The Item "
                                    + itemId
                                    + " contain many working revisions.Please delete the revision(s) not required.\nCannot be added to Engineering Change."
                                    + "!!";
                            // If working revisions are more than one then
                            // remove the item from target
                            removeFromTargetList(itemRevision, itemId);
                            continue;
                        }
                    }
                }
                else if (changeType.equalsIgnoreCase("Lifecycle"))
                {
                    if (!NOVECOHelper.isReleasedRevision(itemRevision))
                    {
                        // syncMsgStr = syncMsgStr+
                        // itemId+"::"+(itemRevision).getProperty("object_type")+"::"+"The Revision "+
                        // (itemRevision).getProperty("object_string")+
                        // " is not released revision.\nCannot be added to Engineering Change."+
                        // "!!";
                        syncMsgStr = syncMsgStr + itemId + "::" + objectType + "::" + "The Revision " + objectString
                                + " is not released revision.\nCannot be added to Engineering Change." + "!!";
                        removeFromTargetList(itemRevision, itemId);
                        continue;
                    }
                }
                /*
                 * Vector<TCComponent> workingRevVec = new
                 * Vector<TCComponent>(Arrays.asList(workingRevs)); if
                 * (!workingRevVec.contains(itemRevision)) { //not a valid
                 * working revision removeFromTargetList(itemRevision,itemId);
                 * continue; }
                 */

                // validation for Check out
                /*
                 * if (NOVECOHelper.isItemRevSecObjectCheckedout(itemRevision)==
                 * NOVECOConstants.ItemRev_CheckedOut) { syncMsgStr =
                 * syncMsgStr+
                 * itemId+"::"+(itemRevision).getProperty("object_type"
                 * )+"::"+"The Revision "+
                 * (itemRevision).getProperty("object_string")+
                 * " is Checked-Out.\nCannot be added to Engineering Change."+
                 * "!!"; removeFromTargetList(itemRevision,itemId); continue; }
                 * if (NOVECOHelper.isItemRevSecObjectCheckedout(itemRevision)==
                 * NOVECOConstants.SecObjects_CheckedOut) { syncMsgStr =
                 * syncMsgStr+
                 * itemId+"::"+(itemRevision).getProperty("object_type"
                 * )+"::"+"Dataset under revision "+
                 * (itemRevision).getProperty("object_string")+
                 * " is Checked-Out.\nCannot be added to Engineering Change."+
                 * "!!"; removeFromTargetList(itemRevision,itemId); continue; }
                 * if (NOVECOHelper.isItemRevSecObjectCheckedout(itemRevision)==
                 * NOVECOConstants.RDD_DOCRev_CheckedOut) { syncMsgStr =
                 * syncMsgStr
                 * +itemId+"::"+(itemRevision).getProperty("object_type"
                 * )+"::"+"The related defining document revision under "+
                 * (itemRevision).getProperty("object_string")+
                 * " is Checked-Out.\nCannot be added to Engineering Change."+
                 * "!!"; removeFromTargetList(itemRevision,itemId); continue; }
                 * if (NOVECOHelper.isItemRevSecObjectCheckedout(itemRevision)==
                 * NOVECOConstants.RDD_DOCRev_SecObjects_CheckedOut) {
                 * syncMsgStr = syncMsgStr+
                 * itemId+"::"+(itemRevision).getProperty("object_type")+"::"+
                 * "Dataset of related defining document revision under "+
                 * (itemRevision).getProperty("object_string")+
                 * " is Checked-Out.\nCannot be added to Engineering Change."+
                 * "!!"; removeFromTargetList(itemRevision,itemId); continue; }
                 */

                // ECR
                
                //As part of TCDECREL-3412 -> ECO can contain only items with out having its ecrs
                
                /*AIFComponentContext[] ecrForms = itemRevision.getRelated("RelatedECN");
                Vector<TCComponentForm> ecrFormsVec = new Vector<TCComponentForm>();
                for (int inx = 0; inx < ecrForms.length; inx++)
                {
                    InterfaceAIFComponent infComp = ecrForms[inx].getComponent();
                    if (infComp.getType().equalsIgnoreCase("_EngChangeRequest_"))
                    {
                        ecrFormsVec.add((TCComponentForm) infComp);
                    }
                }
                if (ecrFormsVec.size() == 1)
                {
                    // if item is related to only one ECR and if it does not
                    // added to eco form then attach
                    InterfaceAIFComponent infComp = ecrFormsVec.get(0);
                    if (!formsVec.contains(infComp)
                                                    * &&(accessCtrlSer.
                                                    * checkPrivilege(ecoForm,
                                                    * "WRITE"))
                                                    )
                    {
                        // Adding ECR to ECO
                        String statuses = ((TCComponentForm) infComp).getProperty("release_statuses");
                        if (statuses.contains("Open"))
                        {
                            engChangeForm.add("_ECNECR_", (TCComponentForm) infComp);
                            formsVec.add((TCComponentForm) infComp);
                        }
                    }
                }
                else if (ecrFormsVec.size() > 1)
                {
                    // If Item is related to multiple ECRs then remove that item
                    // saying select a CR-Item Combination
                    boolean isItemContainsECR = false;
                    for (int j = 0; j < ecrFormsVec.size(); j++)
                    {
                        InterfaceAIFComponent infComp = ecrFormsVec.get(j);
                        if (formsVec.contains(infComp))
                        {
                            isItemContainsECR = true;
                        }
                    }
                    if (!isItemContainsECR)
                    {
                        // syncMsgStr =
                        // syncMsgStr+itemId+"::"+(itemRevision).getProperty("object_type")+"::"+"The Item "+
                        // (itemRevision).getProperty("item_id")+
                        // " has ECR(s). Select one ECR.\nCannot be added to Engineering Change."+
                        // "!!";
                        syncMsgStr = syncMsgStr + itemId + "::" + objectType + "::" + "The Item " + itemId
                                + " has ECR(s). Select one ECR.\nCannot be added to Engineering Change." + "!!";
                        removeFromTargetList(itemRevision, itemId);
                    }
                    continue;
                }*/
                
                if (changeType.equalsIgnoreCase("General"))
                {
                	
                	TCComponentItem item = itemRevision.getItem();
                    TCComponent[] releasedRevs = item.getReleasedItemRevisions();
                    // TCComponent[] revisioncnt =
                    // itemRevision.getItem().getTCProperty("revision_list").getReferenceValueArray();
                    if (!itemRevision.getType().equalsIgnoreCase("Documents Revision"))
                    {
                        if (releasedRevs.length > 0 /* && revisioncnt.length>0 */&& !NOVECOHelper.isItemArchived(item))//TCDECREL-7472
                        {
                            if (!dispTargetsMap.containsKey(itemId))
                            {
                                TCComponent dipsComp = createDispositionComp(itemRevision);
                                // Since we are not revising, not saving oldrev
                                // value
                                
                                String relStatus = item.getProperty("release_statuses");
                                dipsComp.setStringProperty("oldlifecycle", relStatus);
                                
                                //getReleasedItemRevisions() method is returning released revisions in descending order
                                if(releasedRevs[0] != itemRevision)
                                {
                                	dipsComp.setStringProperty("oldrev", releasedRevs[0].getProperty("current_revision_id"));
                                }                               
                                
                                dispTargetsMap.put(itemId, dipsComp);
                            }
                        }
                    }
                }
                // Need to validate related objects are added or not
                // NOVECOHelper.getRddRevsofDocument(revs, selcomp)
            }
            //7413-added for lifecycle process as well
            if (changeType.equalsIgnoreCase("General") || changeType.equalsIgnoreCase("Lifecycle") )
            {
                // need to set the disposition attribute
                if (dispTargetsMap.values().size() > 0)
                {                    
                    m_dispComps = getValidDisps();
                    // if(accessCtrlSer.checkPrivilege(ecoForm, "WRITE"))
                    //6283
                    /*if(m_dispComps.length>0)
                    {*/
                        TCProperty targetdispProperty = engChangeForm.getTCProperty("ectargetdisposition");
                        targetdispProperty.setReferenceValueArray(m_dispComps);
                        if(dispPanel != null)
                        {
                        	dispPanel.setDispositionData(m_dispComps);
                        }
                    //}
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private void removeFromTargetList(TCComponentItemRevision itemRevision, String itemId)
    {
        targetsToBeRemoved.add(itemRevision);
        Vector<TCComponent> compsremVec = new Vector<TCComponent>();
        NOVECOHelper.getRddRevsofDocument(compsremVec, itemRevision);
        for (int i = 0; i < compsremVec.size(); i++)
        {
            targetsToBeRemoved.add(compsremVec.get(i));
        }
        
        if (dispTargetsMap.containsKey(itemId))
        {
            dispCompTobeDeleted.add(dispTargetsMap.get(itemId));
            dispTargetsMap.remove(itemId);
        }
    }
    
    /**
     * \brief This method takes the _DHL_EngChangeForm_ and _EngChangeRequest_
     * gets the process attached to it. From this process it checks for the
     * attachments and if it has the ItemRevisions attached then creates a map
     * of the item_id ( of ItemRevision ) and ItemRevision also creates the
     * vector of ItemRevisions. \param[in] engChangeForm A TCComponentForm whic
     * is _DHL_EngChangeForm_.
     */
    private void validateECR(TCComponentForm engChangeForm, Vector<TCComponentForm> formsVec)
    {
        String[] type = new String[] { "Non-Engineering Revision", "Nov4Part Revision", "Documents Revision" };
        String[] relation = new String[] { "RelatedECN" };
        
        try
        {
            for (int i = 0; i < formsVec.size(); i++)
            {
                TCComponentForm ecrForm = formsVec.get(i);
                AIFComponentContext[] comps = ecrForm.whereReferencedByTypeRelation(type, relation);
                // If the ECR is related to item, checking whether it is added
                // to target if not remove the relation
                // This validation required if user adds the ECR from selection
                // and with that its respective items will also come
                // If user removes item manually from job's target then we also
                // need to remove the ECR
                
                // if ECR is not related to any of the items then it will not be
                // removed
                if (comps.length > 0)
                {
                    boolean isTargetInList = false;
                    for (int j = 0; j < comps.length; j++)
                    {
                        InterfaceAIFComponent infComp = comps[j].getComponent();
                        if (infComp instanceof TCComponentItemRevision)
                        {
                            String targetItemId = infComp.getProperty("item_id");
                            if (ecoTargetsMap.containsKey(targetItemId))
                            {
                                isTargetInList = true;
                            }
                        }
                    }
                    if (!isTargetInList)
                    {
                        // if(accessCtrlSer.checkPrivilege(engChangeForm,
                        // "WRITE"))
                        {
                            syncMsgStr = syncMsgStr
                                    + (ecrForm).getProperty("object_name")
                                    + "::"
                                    + (ecrForm).getProperty("object_type")
                                    + "::"
                                    + "Associated Item to this ECR is not there in the target.Cannot be added to Engineering Change."
                                    + "\n Add this ECR again from Edit button." + "!!";
                            engChangeForm.remove("_ECNECR_", ecrForm);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
	public void removeRevisionsFromTarget(TCComponent removecomp, Vector<TCComponent> ecrs)
	{
		try
		{
			for (int i = 0; i < targetItemsPanel.targetTable.getRowCount(); i++)
			{
				TreeTableNode treeTblNode = targetItemsPanel.targetTable.getNodeForRow(i);

				if (treeTblNode instanceof NovTreeTableNode)
				{
					TCComponent trgRev = ((NovTreeTableNode)treeTblNode).targetRev;
					//((NovTreeTableNode) treeTblNode).getProperty(name)
					//TCComponent dispComp = ((NovTreeTableNode)treeTblNode).dispComp;
					if (trgRev != null && removecomp != null)
					{
						if (removecomp.getProperty("item_id").equals(((TCComponentItemRevision)trgRev).getProperty("item_id")))
						{
							/*it will remove target from workflow and eco form both.*/
							TCComponent ecrComps[] = null;
							if(ecrs != null)
							{
								ecrComps = ecrs.toArray(new TCComponent[ecrs.size()]);
							}
							targetItemsPanel.targetTable.removeTargetsandCleanup(i,ecrComps);

							/* TCDECREL-2330: newly created revision will get deleted , created as a result of only General Type of change */
							if (ecoForm.getProperty("changetype").equalsIgnoreCase("General"))
							{
								if (treeTblNode instanceof NovTreeTableNode)
								{
									String sOldRev = ((NovTreeTableNode)treeTblNode).getProperty("Old Rev");
									if (sOldRev.trim().length() > 0)
									{
										TCComponentItemRevision rddDocRev = null;
										String trgRevId = trgRev.getProperty("item_revision_id");
										AIFComponentContext[] comps = trgRev.getSecondary();
										for (int j = 0; j < comps.length; j++)
										{
											if (comps[j].getContext().equals("RelatedDefiningDocument"))
											{
												InterfaceAIFComponent infDocItem = comps[j].getComponent();
												if (infDocItem instanceof TCComponentItem && infDocItem.getType().equalsIgnoreCase("Documents"))
												{
													rddDocRev = ((TCComponentItem)infDocItem).getLatestItemRevision();
													String rddDocRevId = rddDocRev.getProperty("item_revision_id");
													if (!rddDocRevId.equals(trgRevId))
													{
														rddDocRev = null;
													}

													break;
												}
											}
										}
										/*deleting swimrelation b/w cad part revision and RDD drwing dataset*/
										if (rddDocRev != null && removecomp != null)
										{
											deleteSwimRelation(rddDocRev, (TCComponentItemRevision)trgRev);
										}
										/*Deleting the datasets and item rev*/
										deleteDatasets((TCComponentItemRevision)trgRev);
										//System.out.println("Before delete rev");
										trgRev.delete();
										//Deleting the datasets and documents rev
										//System.out.println("Before delete docrev");
										if (rddDocRev != null && !trgRevId.contains("."))//8471:  RDD should not be deleted for Minor rev type
										{
											deleteDatasets(rddDocRev);
											rddDocRev.delete();
										}
									}
								}
							}
							break;
						}
					}
				}
			}
		}
		catch (TCException e)
		{
			MessageBox.post("Cannot delete Revision. " + e.getError(), "Error", MessageBox.ERROR);
			e.printStackTrace();
		}
	}
	public void removeECRs(Vector<TCComponent> ecrs)
	{
		if(ecrs != null)
		{
			int iECRCount = ecrs.size();
			for (int i = 0; i < iECRCount; i++)
			{
				Vector<TCComponent> targetPanelCRs = targetCRsPanel.getCRForms();
				TCComponent ecrComp = (TCComponent) ecrs.get(i);
				if (targetPanelCRs.contains(ecrComp))
				{
					DefaultListModel model = ((DefaultListModel)targetCRsPanel.crsList.getModel());
					model.removeElement(ecrComp);
					try 
					{
						ecoForm.remove("_ECNECR_", ecrComp);
					} 
					catch (TCException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		Vector<TCComponentForm> crForms = new Vector<TCComponentForm>();
		int itargetCRsCount = targetCRsPanel.crsList.getModel().getSize();
		for (int i = 0; i < itargetCRsCount; i++)
		{
			crForms.add((TCComponentForm)targetCRsPanel.crsList.getModel().getElementAt(i));
		}
		TCComponentForm[] crforms = crForms.toArray(new TCComponentForm[crForms.size()]);
		String reasonChange = NOVECOHelper.getReason4ChangeString(crforms);
		reasonforChange.setText(reasonChange);
	}
	public void removeTargetRevs(TCComponent removecomp)
	{
		try
		{
			for (int i = 0; i < targetItemsPanel.targetTable.getRowCount(); i++)
			{
				TreeTableNode treeTblNode = targetItemsPanel.targetTable.getNodeForRow(i);

				if (treeTblNode instanceof NovTreeTableNode)
				{
					TCComponent trgRev = ((NovTreeTableNode)treeTblNode).targetRev;
					if (trgRev != null)
					{
						if (removecomp.getProperty("item_id").equals(((TCComponentItemRevision)trgRev).getProperty("item_id")))
						{							
							targetItemsPanel.targetTable.removeTargets(trgRev);							
						}
					}
				}
			}
		}
		catch (TCException e)
		{
			MessageBox.post(e.getError(), "Error", MessageBox.ERROR);
			e.printStackTrace();
		}
	}
	public void moveToECOOp(boolean bMoveToECO)
	{
		m_bMoveToECO = bMoveToECO;
	}
	public boolean isMoveToECOOp()
	{
		return m_bMoveToECO;
	}
	public TCComponent[] getValidDisps()
	{
		Vector<TCComponent> validdispComp = new Vector<TCComponent>();
        String[] dispitemIDs = dispTargetsMap.keySet().toArray(new String[dispTargetsMap.entrySet().size()]);
        for (int i = 0; i < dispitemIDs.length; i++)
        {
            if (ecoTargetsMap.containsKey(dispitemIDs[i]))
            {
                validdispComp.add(dispTargetsMap.get(dispitemIDs[i]));
            }
            else
            {
                dispCompTobeDeleted.add(dispTargetsMap.get(dispitemIDs[i]));
            }
        }
		return validdispComp.toArray(new TCComponent[validdispComp.size()]);
	}
	public NOVECOTargetItemsPanel getECOTargetPanel()
	{
		return targetItemsPanel; 
	}
	
	public String getTypeofChange()
	{
		return typeofChange;
	}
	public TCComponentForm getECOForm()
	{
		return ecoForm;
	}
}
