package com.noi.rac.dhl.eco.form.compound.masters;


import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import com.noi.rac.dhl.eco.form.compound.data._EngChangeRequestForm_DC;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeRequestForm_Panel;
import com.noi.rac.form.compound.masters.BaseMasterForm;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.MessageBox;

public class _EngChangeRequestForm_ extends BaseMasterForm
{

	public _EngChangeRequestForm_DC enDC;
	_EngChangeRequestForm_Panel enPanel;
	String masterFormName="";
	static boolean once=false;
	private TCProperty newLifecycleProp ;
	public _EngChangeRequestForm_(TCComponentForm master)
	{
		super();
		masterForm = master;

		try 
		{
			masterFormName = masterForm.getProperty("object_name");
			enDC = new _EngChangeRequestForm_DC();
			enPanel = new _EngChangeRequestForm_Panel();
			enDC.setForm(masterForm);
			enDC.populateFormComponents();
			//Populate the components and then create UI so as to get the disposition panel
			enPanel.createUI(enDC); 			
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	public JComponent getPanel() 
	{
		return enPanel;
	}

	public ImageIcon getTabIcon() 
	{
		return null;
	}

	public String getTip() 
	{
		return null;
	}

	public void lockForm(boolean lock) 
	{

	}

	public void saveForm() 
	{
		//Save New Life cycle value to String VLA
		enDC.saveFormData();
	}

	public boolean isformSavable()
	{
		try 
		{
			String errString ="";
			Vector<TCComponent> strarrRevs = new Vector<TCComponent>();
			String[] lfValues = newLifecycleProp.getStringValueArray();
			
			for (int i = 0; i < lfValues.length; i++) 
			{
				String[] splVals = lfValues[i].split("::");
				strarrRevs.add(masterForm.getSession().stringToComponent(splVals[0]));
			}
			
			if (errString.trim().length()>1) 
			{
				MessageBox.post( "Select New Lifecycle value(s) for revision(s).."+errString,"Select New Lifecycle",MessageBox.WARNING);				
			}
			else
			{
				return true;
			}
			
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
	    return false;
	}
	
public void setDisplayProperties()
	{

	}

}