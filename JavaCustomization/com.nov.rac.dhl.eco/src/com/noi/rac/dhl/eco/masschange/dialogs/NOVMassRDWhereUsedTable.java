package com.noi.rac.dhl.eco.masschange.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import com.noi.rac.dhl.eco.form.compound.util.CheckBoxHeader;
import com.noi.rac.dhl.eco.form.compound.util.CheckBoxHeaderListener;
import com.noi.rac.dhl.eco.util.components.NOVWhereUsedFilterCriteria;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.common.TCCellRenderer;

/**
 * @author tripathim
 * 
 */
public class NOVMassRDWhereUsedTable
{
    protected AIFTableModel m_whrUsedtblmodel;
    protected JTable m_whrUsedTable;
    protected TableRowSorter<AIFTableModel> m_whrUsedTableSorter;
    private List<String> m_nonEditableItemList;
    private Map<String, String> m_nonEditableItemMap;
    private JPanel m_mainTablePanel;
    private int[] m_rowSelection;
    private NOVWhereUsedFilterCriteria m_filterCriteria;
    
    public NOVMassRDWhereUsedTable()
    {
        m_nonEditableItemList = new ArrayList<String>();
        m_nonEditableItemMap = new HashMap<String, String>();
        m_filterCriteria = new NOVWhereUsedFilterCriteria();
    }
    
    public JPanel createTableUI()
    {
        m_mainTablePanel = new JPanel();
        createWhereUsedTable();
        createWhereUsedTableSorter();
        assignColumnRenderer();
        setColumnWidths();
        
        /* Adding WhereUsed table to WhereUsed pane */
        final int whrUsedPaneWidth = 480;
        final int whrUsedPaneHeight = 160;
        JScrollPane whrUsedPane = new JScrollPane(m_whrUsedTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        whrUsedPane.setPreferredSize(new Dimension(whrUsedPaneWidth, whrUsedPaneHeight));
        whrUsedPane.setBorder(BorderFactory.createBevelBorder(1));
        
        m_mainTablePanel.add(whrUsedPane);
        
        return m_mainTablePanel;
    }
    
    /**
     * Purpose: Create WhereUsed table model and table
     */
    private void createWhereUsedTable()
    {
        String[] attrNames = { " ", "Item ID", "Lifecycle", "Pending ECO", "Description", "Sites" };
        m_whrUsedtblmodel = new AIFTableModel(attrNames);
        m_whrUsedTable = new JTable(m_whrUsedtblmodel)
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int column)
            {
                /*
                 * Sorting enabled; so Item ID should be used for comparisons
                 * instead of row num
                 */
                if (column == 0 && (!m_nonEditableItemList.contains(getValueAt(row, 1))))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
            @Override
            public String getToolTipText(MouseEvent e)
            {
                String tip = null;
                Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                
                if (rowIndex != -1)
                {
                    String itemID = getValueAt(rowIndex, 1).toString();
                    
                    if (m_nonEditableItemList.contains(itemID))
                    {
                        tip = m_nonEditableItemMap.get(itemID);
                    }
                }
                
                return tip;
            }
            
            @SuppressWarnings({ "unchecked" })
            public Class getColumnClass(int column)
            {
                return getValueAt(0, column).getClass();
            }
        };
        
        addWhereUsedTableListener();
        m_whrUsedTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        m_whrUsedTable.getTableHeader().setReorderingAllowed(false);
        m_whrUsedTable.doLayout();
    }
    
    private void createWhereUsedTableSorter()
    {
        m_whrUsedTableSorter = new TableRowSorter<AIFTableModel>(m_whrUsedtblmodel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                if (column == 0)
                {
                    return;
                }
                
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                m_whrUsedTable.setRowSorter(this);
                this.sort();
                
            }
        };
        
        List<SortKey> keys = new ArrayList<SortKey>();
        SortKey sortKey = new SortKey(m_whrUsedtblmodel.findColumn("Item ID"), SortOrder.ASCENDING);
        keys.add(sortKey);
        m_whrUsedTableSorter.setSortKeys(keys);
        m_whrUsedTable.setRowSorter(m_whrUsedTableSorter);
        m_whrUsedTableSorter.setSortable(0, false);
    }
    
    /**
     * Purpose: Add Listeners to Where Used Table
     */
    private void addWhereUsedTableListener()
    {
        m_whrUsedTable.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent msEvt)
            {
                setHeaderCheckbox();
                
                /*
                 * Not a multi-row selection then treat it as default single row
                 * selection
                 */
                if (m_rowSelection == null || !(m_rowSelection.length > 1))
                {
                    return;
                }
                
                JTable table = (JTable) msEvt.getSource();
                if (table.getSelectedColumn() == 0)
                {
                    /*
                     * if checked row NOT in multi-rows selected list-> treat it
                     * as default single row selection
                     */
                    int row = table.getSelectedRow();
                    boolean rowInSelection = false;
                    for (int i = 0; i < m_rowSelection.length; i++)
                    {
                        if (m_rowSelection[i] == row)
                        {
                            rowInSelection = true;
                        }
                    }
                    if (!rowInSelection)
                    {
                        m_rowSelection = null;
                        return;
                    }
                    
                    /* Checked row in multi-row selected list */
                    Object cellVal = table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
                    if (cellVal instanceof Boolean)
                    {
                        // CheckBox selected
                        if (m_rowSelection != null)
                        {
                            for (int i = 0; i < m_rowSelection.length; i++)
                            {
                                m_whrUsedTable.getSelectionModel().addSelectionInterval(m_rowSelection[i],
                                        m_rowSelection[i]);
                            }
                            setSelection(m_rowSelection, cellVal);
                        }
                    }
                }
            }
            
            @Override
            public void mousePressed(MouseEvent mouseEvt)
            {
                final JTable table = (JTable) mouseEvt.getSource();
                if (table.getSelectedColumn() != 0)
                {
                    m_rowSelection = table.getSelectedRows();
                }
            }
        });
    }
    
    /**
     * Purpose: Assign Column renderers and Column header renderers to the
     * WhereUsed table
     */
    private void assignColumnRenderer()
    {
        /* Set Table Header Check box renderer */
        final int checkBoxColWidth = 10;
        TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
        tabCol.setResizable(false);
        tabCol.setPreferredWidth(checkBoxColWidth);
        tabCol.setCellEditor(m_whrUsedTable.getDefaultEditor(Boolean.class));
        tabCol.setCellRenderer(m_whrUsedTable.getDefaultRenderer(Boolean.class));
        CheckBoxHeaderListener headerListener = new CheckBoxHeaderListener(m_whrUsedTable);
        CheckBoxHeader chkBoxHeader = new CheckBoxHeader(headerListener);
        tabCol.setHeaderRenderer(chkBoxHeader);
        
        /* Highlight Disabled rows with Grey color */
        for (int i = 0; i < m_whrUsedTable.getColumnCount(); i++)
        {
            m_whrUsedTable.getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer()
            {
                private static final long serialVersionUID = 1L;
                
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                        boolean hasFocus, int row, int column)
                {
                    if (m_nonEditableItemList.contains(table.getValueAt(row, 1)))
                    {
                        setBackground(Color.lightGray);
                        // setToolTipText("Item is in Process or does not belong to this group");
                        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    }
                    setBackground(table.getBackground());
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        }
        
        /* Set Check box renderer for the first Column */
        m_whrUsedTable.getColumnModel().getColumn(0).setCellRenderer(new TCCellRenderer()
        {
            private static final long serialVersionUID = 1L;
            
            public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected,
                    boolean isFocused, int row, int col)
            {
                if (value instanceof Boolean)
                {
                    Boolean marked = (Boolean) value;
                    JCheckBox rendererComponent = new JCheckBox();
                    rendererComponent.setBackground(Color.white);
                    rendererComponent.setLayout(new FlowLayout(FlowLayout.CENTER));
                    if (marked.booleanValue())
                    {
                        rendererComponent.setSelected(true);
                    }
                    rendererComponent.setHorizontalAlignment(SwingConstants.CENTER);
                    /*
                     * Sorting enabled; so Item ID should be used for
                     * comparisons instead of row num
                     */
                    if (m_nonEditableItemList.contains(jtable.getValueAt(row, 1)))
                    {
                        rendererComponent.setEnabled(false);
                    }
                    return rendererComponent;
                }
                return null;
            }
        });
    }
    
    /**
     * Purpose: Set Column Widths of the WhereUsed table
     */
    public void setColumnWidths()
    {
        DefaultTableColumnModel defaulttablecolumnmodel = (DefaultTableColumnModel) m_whrUsedTable.getColumnModel();
        final int charWidth = (int) 11.5;
        final int factor = 20;
        int i = defaulttablecolumnmodel.getColumnCount();
        for (int j = 0; j < i; j++)
        {
            TableColumn tablecolumn = defaulttablecolumnmodel.getColumn(j);
            try
            {
                int integer = tablecolumn.getHeaderValue().toString().length();
                tablecolumn.setPreferredWidth((integer * charWidth) + factor);
                /*
                 * Removing restriction of max width for last 2 columns
                 * (Description and Sites)
                 */
                if (j < i - 2)
                {
                    tablecolumn.setMaxWidth((integer * charWidth) + factor);
                }
            }
            catch (Exception exception)
            {
            }
        }
    }
    
    private void setHeaderCheckbox()
    {
        boolean val = true;
        
        int rowCount = m_whrUsedTable.getRowCount();
        for (int i = 0; i < rowCount; i++)
        {
            boolean v = (Boolean) m_whrUsedTable.getValueAt(i, 0);
            if (!v)
            {
                val = false;
                break;
            }
        }
        setHeaderCheckbox(val);
    }
    
    private void setHeaderCheckbox(boolean val)
    {
        TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
        ((JCheckBox) tabCol.getHeaderRenderer()).setSelected(val);
        m_whrUsedTable.getTableHeader().repaint();
    }
    
    public void resetHeaderCheckbox()
    {
        boolean isRowEnabled = false;
        m_whrUsedtblmodel.fireTableDataChanged();
        TableColumn tabCol = m_whrUsedTable.getColumnModel().getColumn(0);
        ((JCheckBox) tabCol.getHeaderRenderer()).setSelected(false);
        int rowcount = m_whrUsedtblmodel.getRowCount();
        for (int rc = 0; rc < rowcount; rc++)
        {
            boolean isCellEnable = m_whrUsedTable.isCellEditable(rc, 0);
            if(isCellEnable)
            {
                isRowEnabled = true;
                break;
            }
        }
        if(isRowEnabled)
        {
            ((JCheckBox) tabCol.getHeaderRenderer()).setEnabled(true);
        }
        else
        {
            ((JCheckBox) tabCol.getHeaderRenderer()).setEnabled(false);
        }
        m_whrUsedTable.getTableHeader().repaint();
    }
    
    private void setSelection(int[] rows, Object cellVal)
    {
        int selectedRowCount = rows.length;
        m_whrUsedTable.getSelectionModel().clearSelection();
        for (int i = 0; i < selectedRowCount; i++)
        {
            if (m_whrUsedTable.isCellEditable(rows[i], 0))
            {
                m_whrUsedTable.setValueAt(cellVal, rows[i], 0);
                m_whrUsedTable.getSelectionModel().addSelectionInterval(rows[i], rows[i]);
            }
        }
        m_whrUsedTable.repaint();
    }
    
    public void setNonEditableList(List<String> nonEditableItemList)
    {
        m_nonEditableItemList = nonEditableItemList;
    }
    
    public void setNonEditableMap(Map<String, String> nonEditableItemMap)
    {
        m_nonEditableItemMap = nonEditableItemMap;
    }
    
    public List<String> getNonEditableList()
    {
        return m_nonEditableItemList;
    }
    
    public Map<String, String> getNonEditableMap()
    {
        return m_nonEditableItemMap;
    }
    
    public void filterWhrUsedList()
    {
        /*
         * if(m_sorter == null) return;
         */

        if (m_filterCriteria != null)
        {
            /* 1. Remove Row sorter from Where Used table */
            m_whrUsedTable.setRowSorter(null);
            
            /* 2. Construct Filter criteria and Add to sorter */
            Vector<String> siteFilter = m_filterCriteria.getSiteFilterValue();
            Vector<String> lcFilter = m_filterCriteria.getLifecycleFilterValue();
            Vector<RowFilter<AIFTableModel, Object>> mainFilterVector = new Vector<RowFilter<AIFTableModel, Object>>(2);
            
            if (!siteFilter.isEmpty())
            {
                // Construct Filters for Sites
                int colIndex = m_whrUsedtblmodel.findColumn("Sites");
                Vector<RowFilter<AIFTableModel, Object>> tempFilter = new Vector<RowFilter<AIFTableModel, Object>>(
                        siteFilter.size());
                for (int i = 0; i < siteFilter.size(); i++)
                {
                    try
                    {
                        RowFilter<AIFTableModel, Object> rf = RowFilter.regexFilter(siteFilter.get(i), colIndex);
                        tempFilter.add(rf);
                    }
                    catch (java.util.regex.PatternSyntaxException e)
                    {
                        return;
                    }
                }
                RowFilter<AIFTableModel, Object> mainSitesFilter = RowFilter.orFilter(tempFilter);
                mainFilterVector.add(mainSitesFilter);
            }
            if (!lcFilter.isEmpty())
            {
                // Construct Filters for LIfecycle
                int colIndex = m_whrUsedtblmodel.findColumn("Lifecycle");
                Vector<RowFilter<AIFTableModel, Object>> tempFilter = new Vector<RowFilter<AIFTableModel, Object>>(
                        lcFilter.size());
                for (int i = 0; i < lcFilter.size(); i++)
                {
                    try
                    {
                        RowFilter<AIFTableModel, Object> rf = RowFilter.regexFilter("^" + lcFilter.get(i), colIndex); // 4350
                        tempFilter.add(rf);
                    }
                    catch (java.util.regex.PatternSyntaxException e)
                    {
                        return;
                    }
                }
                RowFilter<AIFTableModel, Object> mainLifecycleFilter = RowFilter.orFilter(tempFilter);
                mainFilterVector.add(mainLifecycleFilter);
            }
            
            if (!mainFilterVector.isEmpty())
            {
                RowFilter<AIFTableModel, Object> mainFilter = RowFilter.andFilter(mainFilterVector);
                m_whrUsedTableSorter.setRowFilter(mainFilter);
            }
            else
            {
                m_whrUsedTableSorter.setRowFilter(null);
            }
            
            /* 3. Apply sorter to Where Used table and sort by Item ID */
            m_whrUsedTable.setRowSorter(m_whrUsedTableSorter);
            m_whrUsedTableSorter.setSortable(0, false);
            m_whrUsedTableSorter.sort();
        }
        
        /*
         * After filtering enable/disable Select All depending on if there are
         * any editable rows
         */
        // enableSelectAll();
    }
    
    public NOVWhereUsedFilterCriteria getWhereUsedFilterCriteria()
    {
        return m_filterCriteria;
    }
    
    /**
     * @return the m_whrUsedTable
     */
    public JTable getWhrUsedTable()
    {
        return m_whrUsedTable;
    }
    
    /**
     * @param m_filterCriteria
     *            the m_filterCriteria to set
     */
    public void setWhereUsedFilterCriteria(NOVWhereUsedFilterCriteria m_filterCriteria)
    {
        this.m_filterCriteria = m_filterCriteria;
    }
    
    /**
     * @return the m_whrUsedTableSorter
     */
    public TableRowSorter<AIFTableModel> getWhrUsedTableSorter()
    {
        return m_whrUsedTableSorter;
    }
    
}
