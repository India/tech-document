package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileFilter;

import com.noi.rac.dhl.eco.form.compound.data._EngChangeObjectForm_Data;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.util.components.NOVECOTargetItemsTable.NovTreeTableNode;
import com.nov.rac.utilities.services.NOVCompareBomReportHelper;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.treetable.TreeTableNode;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVECODetailsReport extends AbstractAIFDialog
{
    
    private static final long serialVersionUID = 1L;
    private _EngChangeObjectForm_Data ecoFormData;
    private _EngChangeObjectForm_Panel ecoFormPanel;
    private File m_targetFile;
    private String m_ecoFormName;
    private FileFilter m_fileFilter;
    private Registry m_registry;
    private JFileChooser m_fileChooser;
    private String m_htmlText;
    
    Vector<String> vectDisp = new Vector<String>();
    HashMap<TCComponent, String> dispMap = new HashMap<TCComponent, String>();
    
    public NOVECODetailsReport(_EngChangeObjectForm_Panel ecoFormPanel)
    {
        super(AIFUtility.getActiveDesktop());
        
        this.ecoFormPanel = ecoFormPanel;
        ecoFormData = new _EngChangeObjectForm_Data(ecoFormPanel);
        m_registry = Registry.getRegistry(this);
        JEditorPane editorPane = new JEditorPane();
        
        String ecoFormName = "";
        try
        {
            ecoFormName = ecoFormPanel.ecoForm.getProperty("object_name");
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        m_htmlText = " <FONT SIZE=\"6\" COLOR=#330033><center>" + ecoFormName + " ECO Report</center></FONT>"
                + getReportString();
        editorPane.setContentType("text/html");
        editorPane.setText(m_htmlText);
        editorPane.setEditable(false);
        JScrollPane scPane = new JScrollPane(editorPane);
        scPane.setPreferredSize(new Dimension(750, 500));
        this.setTitle("ECO Report");
        this.setModal(true);
        
        JPanel panel = new JPanel();
        JButton saveBtn = new JButton("Save Report");
        m_ecoFormName = ecoFormName;
        
        saveBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent arg0)
            {
                m_targetFile = createTargetFile();// Sachin: creating File
                                                  // object
                // using Factory Design Pattern
                m_fileChooser = new JFileChooser();
                m_fileChooser.setSelectedFile(m_targetFile);
                m_fileFilter = createCustomFileFilter();// Sachin: creating
                                                        // FileFilter object
                                                        // using Factory Design
                                                        // Pattern
                m_fileChooser.setFileFilter(m_fileFilter);
                
                int returnVal = m_fileChooser.showSaveDialog(NOVECODetailsReport.this);
                
                if (returnVal == JFileChooser.APPROVE_OPTION)
                {
                    
                    String nameOfFile = m_fileChooser.getSelectedFile().getPath()
                            + m_registry.getString("htmlTypeExtension.Name");
                    validateFileName(nameOfFile);
                    File fileName = createSelectedFile();
                    if (fileName != null)
                    {
                        isFileExist(m_fileChooser, returnVal, fileName);
                    }
                    
                }
                
            }
        });
        
        JButton cancelBtn = new JButton("Close");
        cancelBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent arg0)
            {
                NOVECODetailsReport.this.dispose();
            }
        });
        
        panel.add(saveBtn);
        panel.add(cancelBtn);
        getContentPane().add(scPane, BorderLayout.CENTER);
        getContentPane().add(panel, BorderLayout.SOUTH);
        setModal(true);
    }
    
    /*
     * Function : getReportString() Return Type : StringBuilder Parameters :
     * None Purpose : Get the ECO report string in HTML format
     */
    private StringBuilder getReportString()
    {
        StringBuilder str = new StringBuilder();
        str.append(getEcoDetailsReport());
        str.append(getDispositionsReport());
        str.append(getTargetsReport());
        
        TCComponentItemRevision[] itemRev = new TCComponentItemRevision[vectDisp.size()];
        for (int i = 0; i < vectDisp.size(); i++)
        {
            itemRev[i] = (TCComponentItemRevision) ecoFormPanel.ecoTargetsMap.get(vectDisp.elementAt(i));
        }
        str.append(getBomCompareReport(itemRev));
        
        str.append(getReportRDD());			//TCDECREL-3589
        
        return str;
    }
    
	//TCDECREL-3589 : Start : 06-DEC-2012 : Akhilesh
	/** Function	: getReportRDD()
	 * Return Type	: StringBuilder
	 * Parameters	: StringBuilder
	 * Purpose		: Gets part and RDD report in HTML format to the input String parameter
	 */
	private StringBuilder getReportRDD() 
	{
		StringBuilder str = new StringBuilder();
		StringBuilder reportStr = new StringBuilder();
		Vector<TCComponent> vectItem = new Vector<TCComponent>();
		
		for (int i = 0; i < ecoFormPanel.targetItemsPanel.targetTable.getRowCount(); i++) 
		{
			TreeTableNode treeTblNode =  ecoFormPanel.targetItemsPanel.targetTable.getNodeForRow(i);
			
			if (treeTblNode instanceof NovTreeTableNode) 
			{
				String objType ="";
				TCComponent itemPart = ((NovTreeTableNode)treeTblNode).targetRev;
				try 
				{
					objType = itemPart.getTCProperty("object_type").getStringValue();
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(objType.equalsIgnoreCase("Nov4Part Revision") || objType.equalsIgnoreCase("Non-Engineering Revision") )
				{
					vectItem.add(itemPart);
				}
				
			}
		}
		TCComponent[] itemTarget = vectItem.toArray(new TCComponent[vectItem.size()]);
		String[] reqdPropertiesRDDandRDs= { "RelatedDefiningDocument","RelatedDocuments"  };
		String[][] retriviedPropertiesRDDandRDs = null;
		

		try
		{
		    List<TCComponent> itemTargetList = Arrays.asList(itemTarget);//TC10.1 Upgrade
			//retriviedPropertiesRDDandRDs = TCComponentType.getPropertiesSet( itemTarget, reqdPropertiesRDDandRDs );
			retriviedPropertiesRDDandRDs = TCComponentType.getPropertiesSet( itemTargetList, reqdPropertiesRDDandRDs );//TC10.1 Upgrade
		}
		catch( TCException e1 )
		{
			e1.printStackTrace();
		}
		reportStr.append(getInfoRDDandRDs(itemTarget, retriviedPropertiesRDDandRDs));
			
		reportStr.append("</table>");
		
		str.append("<FONT COLOR=#347C17><B><center>RDD & RD Details</FONT>");
		str.append("<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Targets Part ID</B></TD><TD WIDTH=\"200\">");
		str.append("<FONT COLOR=#153E7E><B><center>Related Defining Document</B></TD><TD WIDTH=\"250\"><FONT COLOR=#153E7E><B><center>Related Documents</B></TD></TR>");
		str.append(reportStr);
		str.append("</table>");
		
		return str;
	}
	
	/** Function	: getInfoRDDandRDs()
	 * Return Type	: StringBuilder
	 * Parameters	: StringBuilder
	 * Purpose		: Appends part and RDD info details in HTML format to the input String parameter
	 */
	private StringBuilder getInfoRDDandRDs(TCComponent[] partItem,
			String[][] propertyRDDandRDs) {
		StringBuilder strInfo = new StringBuilder();
		for(int i=0;i<partItem.length;i++)
		{
				strInfo.append("<TR>");
				strInfo.append("<TD><FONT COLOR=#153E7E>");
				try 
				{
					strInfo.append(partItem[i].getProperty("current_id").toString());
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}
				strInfo.append("</TD><TD><FONT COLOR=#153E7E>");
				strInfo.append(propertyRDDandRDs[i][0]);
				strInfo.append("</TD><TD>");
				String[] strRD = propertyRDDandRDs[i][1].split(",");
				strInfo.append("<FONT COLOR=#153E7E>");
				strInfo.append(strRD[0]);
				if(strRD.length>1)
				{
					for (int j=1;j<strRD.length;j++)
					{
						strInfo.append(",<br><FONT COLOR=#153E7E>");
						strInfo.append(strRD[j]);
					}
				}
				strInfo.append("</TD>");
				strInfo.append("</TR>");
			}
		return strInfo;
	}
	//TCDECREL-3589 : End

    /*
     * Function : getEcoDetailsReport() Return Type : StringBuilder Parameters :
     * StringBuilder Purpose : Appends the ECO form details in HTML format to
     * the input String parameter
     */
    private StringBuilder getEcoDetailsReport()
    {
        StringBuilder str = new StringBuilder();
        String ChangeTypeStr = "";
        String wfNameStr = "";
        String changeCatStr = "";
        String groupStr = "";
        String reasoncodeStr = "";
        String nov4ProductLineStr = "";
        
        String rowEndHTML = "</TD></TR></table></TD></TR></table>";
        
        ChangeTypeStr = ecoFormData.getProperty("changetype").toString();
        wfNameStr = ecoFormData.getProperty("wfname").toString();
        changeCatStr = ecoFormData.getProperty("changecat").toString();
        groupStr = ecoFormData.getProperty("group").toString();
        reasoncodeStr = ecoFormData.getProperty("reasoncode").toString();
        nov4ProductLineStr = ecoFormData.getProperty("nov4_product_line").toString();
        str.append("<FONT COLOR=#347C17><B><center>ECO DETAILS</FONT>");
        
        str.append("<table border=\"0\"><TR><TD WIDTH=\"150\"><FONT COLOR=#153E7E>Change Type</TD><TD><table border=\"1\"><TR><TD WIDTH=\"300\"><FONT COLOR=#153E7E>");
        str.append(ChangeTypeStr);
        str.append(rowEndHTML);
        str.append("<table border=\"0\"><TR><TD WIDTH=\"150\"><FONT COLOR=#153E7E>WorkFlow Name</TD><TD><table border=\"1\"><TR><TD WIDTH=\"300\"><FONT COLOR=#153E7E>");
        str.append(wfNameStr);
        str.append(rowEndHTML);
        str.append("<table border=\"0\"><TR><TD WIDTH=\"150\"><FONT COLOR=#153E7E>Change Category</TD><TD><table border=\"1\"><TR><TD WIDTH=\"300\"><FONT COLOR=#153E7E>");
        str.append(changeCatStr);
        str.append(rowEndHTML);
        str.append("<table border=\"0\"><TR><TD WIDTH=\"150\"><FONT COLOR=#153E7E>Group</TD><TD><table border=\"1\"><TR><TD WIDTH=\"300\"><FONT COLOR=#153E7E>");
        str.append(groupStr);
        str.append(rowEndHTML);
        str.append("<table border=\"0\"><TR><TD WIDTH=\"150\"><FONT COLOR=#153E7E>Reason Code</TD><TD><table border=\"1\"><TR><TD WIDTH=\"300\"><FONT COLOR=#153E7E>");
        str.append(reasoncodeStr);
        str.append(rowEndHTML);
        str.append("<table border=\"0\"><TR><TD WIDTH=\"150\"><FONT COLOR=#153E7E>Product Line</TD><TD><table border=\"1\"><TR><TD WIDTH=\"300\"><FONT COLOR=#153E7E>");
        str.append(nov4ProductLineStr);
        str.append(rowEndHTML);
        
        return str;
    }
    
    /*
     * Function : getDispositionsReport() Return Type : StringBuilder Parameters
     * : StringBuilder Purpose : Appends the ECO disposition details in HTML
     * format to the input String parameter
     */
    private StringBuilder getDispositionsReport()
    {
        StringBuilder str = new StringBuilder();
        TCComponent[] dispComp = (TCComponent[]) ecoFormData.getProperty("ectargetdisposition");
        
        for (int i = 0; i < dispComp.length; i++)
        {
            try
            {
                String targetItemId = dispComp[i].getTCProperty("targetitemid").toString();
                vectDisp.add(targetItemId);
                dispMap.put(dispComp[i], targetItemId);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        // HashMap<String,Vector<String>> mapTarget = (HashMap<String,
        // Vector<String>>)ecoFormData.getProperty("targetsnewlifecycle");
        // TCDECREL-2331 for .8 patch :Start
        /*
         * str.append("<FONT COLOR=#347C17><B><center>DISPOSITION OBJECTS</FONT>"
         * ); str.append(
         * "<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Disposition</B></TD><TD WIDTH=\"200\">"
         * ); str.append(
         * "<FONT COLOR=#153E7E><B><center>Item Name</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Old Rev</B></TD>"
         * ); str.append(
         * "<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Old LifeCycle</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>"
         * ); str.append(
         * "New Rev</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>New Lifecycle</B></TD></TR>"
         * ); for (int i = 0; i <
         * ecoFormPanel.targetItemsPanel.targetTable.getRowCount(); i++) {
         * TreeTableNode treeTblNode =
         * ecoFormPanel.targetItemsPanel.targetTable.getNodeForRow(i); if
         * (treeTblNode instanceof NovTreeTableNode) {
         * if(vectDisp.contains(treeTblNode.getProperty("Item Number"))) {
         * str.append(getTargetsString(treeTblNode)); } } }
         * str.append("</table>");
         */
        // TCDECREL-2331 for .8 patch :End
        return str;
    }
    
    /*
     * Function : getTargetsReport() Return Type : StringBuilder Parameters :
     * StringBuilder Purpose : Appends the ECO Targets details in HTML format to
     * the input String parameter
     */
    private StringBuilder getTargetsReport()
    {
        StringBuilder str = new StringBuilder();
        StringBuilder reportStr = new StringBuilder();
        for (int i = 0; i < ecoFormPanel.targetItemsPanel.targetTable.getRowCount(); i++)
        {
            TreeTableNode treeTblNode = ecoFormPanel.targetItemsPanel.targetTable.getNodeForRow(i);
            
            if (treeTblNode instanceof NovTreeTableNode)
            {
                reportStr.append(getTargetsString(treeTblNode));
                
                if (treeTblNode.areChildrenLoaded() && !treeTblNode.isNodeExpanded())
                {
                    for (int j = 0; j < treeTblNode.getChildCount(); j++)
                    {
                        TreeTableNode treeTblNode1 = (TreeTableNode) treeTblNode.getChildAt(j);
                        
                        reportStr.append(getTargetsString(treeTblNode1));
                    }
                }
            }
        }
        
        reportStr.append("</table>");
        
        str.append("<FONT COLOR=#347C17><B><center>ECO TARGET ITEM</FONT>");
        str.append("<table border=\"1\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Targets</B></TD><TD WIDTH=\"200\">");
        str.append("<FONT COLOR=#153E7E><B><center>Item Name</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Old Rev</B></TD>");
        str.append("<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Old LifeCycle</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B>");
        str.append("<center>New Rev</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>New Lifecycle</B></TD></TR>");
        str.append(reportStr);
        str.append("</table>");
        
        return str;
    }
    
    /*
     * Function : getTargetsString() Return Type : StringBuilder Parameters :
     * TreeTableNode Purpose : Appends the ECO Targets details in HTML format
     */
    private StringBuilder getTargetsString(TreeTableNode node)
    {
        StringBuilder strTrgt = new StringBuilder();
        strTrgt.append("<TR>");
        strTrgt.append("<TD><FONT COLOR=#153E7E>");
        strTrgt.append(node.getProperty("Item Number"));
        strTrgt.append("</TD><TD><FONT COLOR=#153E7E>");
        strTrgt.append(node.getProperty("Name"));
        strTrgt.append("</TD><TD><FONT COLOR=#153E7E>");
        strTrgt.append(node.getProperty("Old Rev"));
        strTrgt.append("</TD><TD><FONT COLOR=#153E7E>");
        strTrgt.append(node.getProperty("Old Lifecycle"));
        strTrgt.append("</TD><TD><FONT COLOR=#153E7E>");
        strTrgt.append(node.getProperty("New Rev"));
        strTrgt.append("</TD><TD><FONT COLOR=#153E7E>");
        strTrgt.append(node.getProperty("New Lifecycle"));
        strTrgt.append("</TD>");
        strTrgt.append("</TR>");
        return strTrgt;
    }
    
    /*
     * Function : getBomCompareReport() Return Type : StringBuilder Parameters :
     * StringBuilder Purpose : Appends the BOM compare report details for each
     * Disposition object in HTML format to the input String parameter
     */
    private StringBuilder getBomCompareReport(TCComponentItemRevision[] itemrev)
    {
        StringBuilder str = new StringBuilder();
        /*
         * Call SOA service to generate BOM compare report for all Disposition
         * items
         */
        NOVCompareBomReportHelper.generateCompareBomReport(itemrev, NOVCompareBomReportHelper.BOM_compare_singlelevel,
                NOVCompareBomReportHelper.BOM_compare_output_report);
        
        /* Get Disposition array for which there is BOM compare results */
        TCComponent[] comp = NOVCompareBomReportHelper.dispCompArr;
        
        if (comp.length > 0)
        {
            str = str.append("<FONT COLOR=#347C17><B><center>BOM Compare Report</FONT>");
            for (int i = 0; i < comp.length; i++)
            {
                str.append("<br>");
                str.append("<FONT COLOR=#347C17><B><center>BOM Line compare for Disposition component ");
                // str.append(dispMap.get(comp[i]));
                str.append(comp[i].toString());
                str.append("</FONT>");
                str.append(NOVCompareBomReportHelper.getCompareBomReportStr(comp[i]));
            }
        }
        
        return str;
    }
    
    private File createTargetFile()
    {
        return createTargetFileImpl();
    }
    
    public File createTargetFileImpl()
    {
        m_targetFile = new File(m_ecoFormName);
        return m_targetFile;
    }
    
    private void validateFileName(String nameOfFile)
    {
        if (!isValidFileName(nameOfFile))
        {
            MessageBox.post(m_registry.getString("validFileName.Title"), "Warning", MessageBox.WARNING);
            return;
        }
    }
    
    private boolean isValidFileName(String fileName)
    {
        boolean isValidName = false;
        if ((fileName.endsWith(m_registry.getString("htmlTypeExtension.Name")) || fileName.endsWith(m_registry
                .getString("htmTypeExtension.Name"))))
        {
            isValidName = true;
        }
        return isValidName;
    }
    
    private FileFilter createCustomFileFilter()
    {
        return new CustomFileFilter();
    }
    
    private void isFileExist(JFileChooser fileChooser, int returnVal, File fileName)
    {
        if (fileName.exists())
        {
            
            String fileNamePath = fileChooser.getSelectedFile().getPath()
                    + m_registry.getString("htmlTypeExtension.Name") + " ";
            String msgForDialog = fileNamePath + m_registry.getString("alreadyExist.MSG");
            returnVal = JOptionPane.showConfirmDialog(NOVECODetailsReport.this, msgForDialog,
                    m_registry.getString("save.Title"), 1, JOptionPane.WARNING_MESSAGE);
            if (returnVal == JOptionPane.NO_OPTION)
            {
                fileChooser.showSaveDialog(NOVECODetailsReport.this);
                fileName = createSelectedFile();
                isFileExist(fileChooser, returnVal, fileName);
            }
            
            else if (returnVal == JOptionPane.YES_OPTION)
            {
                saveToFile(fileName);
            }
            return;
        }
        saveToFile(fileName);
    }
    
    private File createSelectedFile()
    {
        return createSelectedFileImpl();
        
    }
    
    private File createSelectedFileImpl()
    {
        m_targetFile = new File(m_fileChooser.getSelectedFile().getPath()
                + m_registry.getString("htmlTypeExtension.Name"));
        return m_targetFile;
    }
    
    private void saveToFile(File fileName)
    {
        try
        {
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
            out.write(m_htmlText);
            out.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
}

class CustomFileFilter extends FileFilter
{
    private Registry m_reg = Registry.getRegistry(this);
    
    public boolean accept(File file)
    {
        String strAbsolutePath = file.getAbsolutePath();
        return file.isDirectory() || strAbsolutePath.endsWith(m_reg.getString("htmlTypeExtension.Name"))
                || strAbsolutePath.endsWith(m_reg.getString("htmTypeExtension.Name"));
    }
    
    public String getDescription()
    {
        return m_reg.getString("fileTypeExtension.Name");
    }
}