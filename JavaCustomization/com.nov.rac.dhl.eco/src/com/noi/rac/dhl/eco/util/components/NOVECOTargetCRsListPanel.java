package com.noi.rac.dhl.eco.util.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.util.components.NOIJLabel;
import com.nov.rac.ui.perspective.PerspectiveDefHelper;
//import com.teamcenter.rac.aif.ApplicationDef;TC10.1 Upgrade
import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Registry;

public class NOVECOTargetCRsListPanel extends JPanel 
{
	private static final long serialVersionUID = 1L;
	public JList crsList;
	Registry registry = Registry.getRegistry(this);
	public _EngChangeObjectForm_Panel ecoFormPanel;
	JButton editTrgBtn;
	JScrollPane crPane;
	public NOVECOTargetCRsListPanel(_EngChangeObjectForm_Panel engChangeObjectFormPanel)
	{
		ecoFormPanel = engChangeObjectFormPanel;
		DefaultListModel listModel = new DefaultListModel();
		crsList = new JList(listModel);
		crPane = new JScrollPane(crsList);
		crPane.setPreferredSize(new Dimension(185,120));		
		
		loadCRFroms() ;
		
		//5077-start
		crsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		crsList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent mouseEvt) {
            	
            	sendECRsToMyTC(mouseEvt);
                    }
         });
       //5077-end
		NOIJLabel crLbl = new NOIJLabel("Change Requests:");
		editTrgBtn = new JButton("Edit");
		ImageIcon plusIcon = registry.getImageIcon("edit.ICON");
		editTrgBtn.setIcon(plusIcon);
		
		editTrgBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent aevt) 
			{
				NOVCRSelectionDialog crSelDlg = new NOVCRSelectionDialog(NOVECOTargetCRsListPanel.this);
				crSelDlg.setVisible(true);
			}
		});
		
		JPanel chbtnPanel = new JPanel(new PropertyLayout());
		chbtnPanel.add("1.1.left.center",crLbl);
		chbtnPanel.add("1.2.left.center",editTrgBtn);
		chbtnPanel.setPreferredSize(new Dimension(180,28));
		//chbtnPanel.setBackground(Color.YELLOW);
		//TDECREL-2798 : Start : Akhilesh
		/*this.setLayout(new PropertyLayout());
		add("1.1.left.center",chbtnPanel);
		add("2.1.left.center",crPane);*/
		this.setLayout(new BorderLayout(0,1));
		add(chbtnPanel,BorderLayout.NORTH);
		add(crPane,BorderLayout.CENTER);
		//TCDECREL-2798 : End
		setBorder(new TitledBorder(""));
	}
	//5077-start
	public void sendECRsToMyTC(java.awt.event.MouseEvent mouseEvt)
	{
		Object source = mouseEvt.getSource();
        if (SwingUtilities.isRightMouseButton(mouseEvt)
                     )
        {
              JPopupMenu jPopupMenu = new JPopupMenu();
              JMenuItem jMenuItem = new JMenuItem();
              String sMenuLabel = "<html><b><font=8 color=red>Send to My Teamcenter</font></b>";
              jMenuItem.setText(sMenuLabel);
              jMenuItem.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                          
                          InterfaceAIFComponent selectedComponent=(InterfaceAIFComponent) crsList.getSelectedValue();
                          if(selectedComponent != null)
                          {
                          
                          try {
                             //ApplicationDef navigatorApp = AIFUtility.getAIFApplicationDefMgr().getApplicationDefByKey("com.teamcenter.rac.ui.perspectives.navigatorPerspective");//It will send to MyTeamcenter application
                             IPerspectiveDef perspectiveDef = PerspectiveDefHelper.getPerspectiveDefByLegacyAppID("com.teamcenter.rac.ui.perspectives.navigatorPerspective");//10.1 Upgrade
                             InterfaceAIFComponent components[] = new InterfaceAIFComponent[1];
                             components[0]=selectedComponent;
                             //navigatorApp.openApplication(components);
                             perspectiveDef.openPerspective(components);//TC10.1 Upgrade
                          } catch (Exception e1) {
                                
                                e1.printStackTrace();
                          }
                         }
                    }
              });
              jPopupMenu.add(jMenuItem);
              jPopupMenu.setVisible(true);
              jPopupMenu.show((java.awt.Component) source,mouseEvt.getX(), mouseEvt.getY());
        }
	}
	//5077-end
	public void resizePanel(Dimension dPanel, Dimension dCrPane)
	{
		if(dPanel != null)
			setPreferredSize(dPanel);
		
		if(dCrPane != null)
			crPane.setPreferredSize(dCrPane);
	}
	
	public void setEnabled(boolean enabled) 
	{
		editTrgBtn.setEnabled(enabled);		
	}
	
	private void loadCRFroms() 
	{
		try 
		{
			if (ecoFormPanel!=null) 
			{
				AIFComponentContext[] ecrFormRefs = ecoFormPanel.ecoForm.getSecondary();
				DefaultListModel listModel = (DefaultListModel)crsList.getModel();
					
				for (int j = 0; j < ecrFormRefs.length; j++) 
				{
					if (ecrFormRefs[j].getContext().equals("_ECNECR_")) 
					{
						InterfaceAIFComponent comp =  ecrFormRefs[j].getComponent();
						if (comp instanceof TCComponentForm) 
						{
							listModel.addElement(comp);
						}		
					}
				}
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}		
	}
	
	public Vector<TCComponent> getCRForms()
	{
		Vector<TCComponent> crForms = new Vector<TCComponent>();
		DefaultListModel listModel = (DefaultListModel)crsList.getModel();
		int crNums = crsList.getModel().getSize();
		
		for (int i = 0; i < crNums; i++) 
		{
			TCComponentForm crForm =  (TCComponentForm)listModel.get(i);
			crForms.add(crForm);
		}
		return crForms;
	}
}
