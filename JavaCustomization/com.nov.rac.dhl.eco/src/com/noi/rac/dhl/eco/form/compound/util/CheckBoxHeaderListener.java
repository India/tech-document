package com.noi.rac.dhl.eco.form.compound.util;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.JTable;

public class CheckBoxHeaderListener implements ItemListener  
{  
	private JTable m_table;
	
	public CheckBoxHeaderListener(JTable table)
	{
		m_table = table;
	}
	
	@Override
	public void itemStateChanged(ItemEvent e) 
	{
		Object source = e.getSource();  
		if (source instanceof AbstractButton == false) return;  
		boolean checked = e.getStateChange() == ItemEvent.SELECTED;  
		
		for(int x = 0, y = m_table.getRowCount(); x < y; x++)  
		{  
			if(m_table.isCellEditable(x, 0))
				m_table.setValueAt(new Boolean(checked),x,0);			
		}  
		m_table.updateUI();
	 }  
}	
