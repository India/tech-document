package com.noi.rac.dhl.eco.form.compound.util;

import java.util.LinkedList;

import com.teamcenter.rac.kernel.TCComponentForm;

public class ItemRevisionHistory {

	public String revisionId;
	public String creationDate;
	public String dateReleased;
	public String name;
	public String description;
	TCComponentForm revMasterForm=null;
	LinkedList        subForms = new LinkedList();
	public ItemRevisionHistory() { }
	public ItemRevisionHistory(TCComponentForm master) { revMasterForm = master; }
	public void addSubForm(TCComponentForm sub) { subForms.add(sub); }
	public TCComponentForm getForm() { return revMasterForm; }
}
