package com.noi.rac.dhl.eco.masschange.operations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.table.TableColumn;

import com.noi.rac.dhl.eco.masschange.dialogs.MassRDChangeComposite;
import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

/**
 * @author tripathim
 * 
 */
public class NOVRDSearchOperation implements INOVSearchProvider
{
    private String m_itempuid;
    private Vector<String> m_whereUsedItemsRowData;
    private int m_whereUsedItemsRowCount = 0;
    private int m_whereUsedItemsColCount = 0;
    private TCComponentGroup m_userGroup;
    private TCSession m_session;
    private MassRDChangeComposite m_massRDComposite;
    private Map<String, String> m_whrUsedItemsMap;
    private List<String> m_nonEditableItemList;
    private Map<String, String> m_nonEditableItemMap;
    
    private Registry m_registry = Registry.getRegistry("com.noi.rac.dhl.eco.util.components.components");
    
    public NOVRDSearchOperation(MassRDChangeComposite composite, TCSession session, String itempuid)
    {
        m_massRDComposite = composite;
        m_session = session;
        m_itempuid = itempuid;
        m_userGroup = m_session.getCurrentGroup();
    }
    
    public void executeWhereUsedSearch()
    {
        INOVSearchProvider searchService = this;
        INOVSearchResult searchResult = null;
        try
        {
            searchResult = NOVSearchExecuteHelperUtils.execute(searchService, INOVSearchProvider.LOAD_ALL);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (searchResult != null && searchResult.getResponse().nRows > 0)
        {
            INOVResultSet theResSet = searchResult.getResultSet();
            m_whereUsedItemsRowData = theResSet.getRowData();
            m_whereUsedItemsRowCount = theResSet.getRows();
            m_whereUsedItemsColCount = theResSet.getCols();
        }
        else
        {
            m_whereUsedItemsRowData = null;
            m_whereUsedItemsRowCount = 0;
            m_whereUsedItemsColCount = 0;
        }
    }
    
    public void populateWhereUsedTable()
    {
        try
        {
            m_massRDComposite.getWhereUsedTable().setRowSorter(null);
            m_massRDComposite.getWhereUsedTableModel().removeAllRows();
            m_nonEditableItemList = new ArrayList<String>();
            m_nonEditableItemMap = new HashMap<String, String>();
            m_whrUsedItemsMap = new HashMap<String, String>();
            
            if (m_whereUsedItemsRowData == null)
            {
                return;
            }
            
            for (int i = 0; i < m_whereUsedItemsRowCount; i++)
            {
                int rowIndex = m_whereUsedItemsColCount * i;
                String itemId = m_whereUsedItemsRowData.get(rowIndex + 1);
                // String itemName = m_whereUsedItemsRowData.get(rowIndex + 2);
                String itemDesc = m_whereUsedItemsRowData.get(rowIndex + 3);
                String owningGroup = m_whereUsedItemsRowData.get(rowIndex + 4);
                String lifeCycle = m_whereUsedItemsRowData.get(rowIndex + 5);
                String itemRevId = m_whereUsedItemsRowData.get(rowIndex + 6);
                String releaseStatus = m_whereUsedItemsRowData.get(rowIndex + 7);
                String sites = m_whereUsedItemsRowData.get(rowIndex + 8);
                String pendingECO = m_whereUsedItemsRowData.get(rowIndex + 9);
                String sRevPuid = m_whereUsedItemsRowData.get(rowIndex + 10);
                
                String pendingECOName = pendingECO.split(" ")[0];
                
                Vector<Object> rowData = new Vector<Object>();
                rowData.add(new Boolean(false));
                rowData.add(itemId);
                rowData.add(lifeCycle);
                rowData.add(pendingECOName);
                rowData.add(itemDesc);
                rowData.add(sites);
                rowData.add(itemRevId);
                
                m_massRDComposite.getWhereUsedTableModel().addRow(rowData);   
                m_whrUsedItemsMap.put(itemId, sRevPuid);
                validateSearchResults(pendingECO, releaseStatus, itemId, owningGroup);
            }
            sortWhereUsedTable();
            /* No rows are enabled; so disable Select All option */
            if (m_whereUsedItemsRowCount != m_nonEditableItemList.size())
            {
                enableSelectAll(true);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private void validateSearchResults(String pendingECO, String releaseStatus, String itemId, String owningGroup)
    {
        /* Disable selection of Items that are In Process */
        boolean isRevInProcess = !pendingECO.isEmpty();
        if (isRevInProcess)
        {
            String sRevInProcess = m_registry.getString("ItemRevInProcess.MSG");
            m_nonEditableItemList.add(itemId);
            m_nonEditableItemMap.put(itemId, sRevInProcess);
        }
        
        /*
         * Disable selection of Items that are not released or are the
         * first revision
         */
        if (!isRevInProcess && releaseStatus.isEmpty())
        {
            String sRevNotReleased = m_registry.getString("ItemRevNotReleased.MSG");
            m_nonEditableItemList.add(itemId);
            m_nonEditableItemMap.put(itemId, sRevNotReleased);
        }
        
        /* Disable selection of Items from other Groups */
        if (!m_userGroup.toString().contains(owningGroup))
        {
            String sNotInCurrentGrp = m_registry.getString("ItemNotBelongToCurrentGrp.MSG");
            m_nonEditableItemList.add(itemId);
            m_nonEditableItemMap.put(itemId, sNotInCurrentGrp);
        }
        
        /* Disable selection of Items if already added */
        checkExistingTargetAndUpdateList(itemId);
        
        m_massRDComposite.getMassRDWhereUsedTable().setNonEditableList(m_nonEditableItemList);
        m_massRDComposite.getMassRDWhereUsedTable().setNonEditableMap(m_nonEditableItemMap);
        m_massRDComposite.m_whereUsedLabel.setText("Where Used Report : " + m_whereUsedItemsRowCount + " Items found.");
    }
    
    private void checkExistingTargetAndUpdateList(String itemId)
    {
        Vector<TCComponent> targetComps = m_massRDComposite.getECOFormPanel().ecoTargets;
        for (int j = 0; j < targetComps.size(); j++)
        {
            if(targetComps.get(j).toString().startsWith(itemId))
            {
                String sNotInCurrentGrp = m_registry.getString("AlreadyAddedToTargetList.MSG");
                m_nonEditableItemList.add(itemId);
                m_nonEditableItemMap.put(itemId, sNotInCurrentGrp);
            }
        }
    }
    
    private void sortWhereUsedTable()
    {
        if (m_whereUsedItemsRowCount >= 1
                && m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTable().getRowSorter() == null)
        {
            m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTable()
                    .setRowSorter(m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTableSorter());
            m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTableSorter().setSortable(0, false);
            m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTableSorter().sort();
            m_massRDComposite.getFilterButton().setEnabled(true);
            
        }
        else
        {
            m_massRDComposite.getFilterButton().setEnabled(false);
            
        }
    }
    
    private void enableSelectAll(boolean val)
    {
        TableColumn tabCol = m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTable().getColumnModel()
                .getColumn(0);
        ((JCheckBox) tabCol.getHeaderRenderer()).setEnabled(val);
        m_massRDComposite.getMassRDWhereUsedTable().getWhrUsedTable().getTableHeader().repaint();
    }
    
    public Map<String, String> getWhereUsedItemsMap()
    {
        return m_whrUsedItemsMap;
    }
    
    @Override
    public String getBusinessObject()
    {
        return "Item";
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[4];
        
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_typed_reference;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[] { m_itempuid };
        
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[] { "RelatedDocuments" };
        
        bindVars[2] = new QuickSearchService.QuickBindVariable();
        bindVars[2].nVarType = POM_string;
        bindVars[2].nVarSize = 1;
        bindVars[2].strList = new String[] { "Nov4Part" };
        
        bindVars[3] = new QuickSearchService.QuickBindVariable();
        bindVars[3].nVarType = POM_string;
        bindVars[3].nVarSize = 1;
        bindVars[3].strList = new String[] { "Non-Engineering" };
        
        return bindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[5];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-primary-item-by-relationtype";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2, 3, 4 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-release-status";
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 2 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 5 };
        
        allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-nov4part-latest-revs";
        allHandlerInfo[2].listBindIndex = new int[0];
        allHandlerInfo[2].listReqdColumnIndex = new int[] { 1, 2, 5, 6 };
        allHandlerInfo[2].listInsertAtIndex = new int[] { 6, 7, 9, 10 };
        /* 7->Release Status 9->Pending ECO 10->Revision puid */

        allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-DH-sites-props";
        allHandlerInfo[3].listBindIndex = new int[] { 3 };
        allHandlerInfo[3].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[3].listInsertAtIndex = new int[] { 8 };
        
        allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-DH-sites-props";
        allHandlerInfo[4].listBindIndex = new int[] { 4 };
        allHandlerInfo[4].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[4].listInsertAtIndex = new int[] { 8 };
        
        return allHandlerInfo;
    }
    
}
