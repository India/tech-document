package com.noi.rac.dhl.eco.util.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.util.Registry;

public class NOVNovelFormTableButtonEditor extends AbstractCellEditor implements TableCellEditor, ActionListener
{
    private static final long serialVersionUID = 1L;
    private JButton m_tblButton;
    private TCTable m_srcTable ;
    private TCTable m_trgTable ;
    private Registry m_registry ;
    private Vector<String> m_addedFormVector;
    private NOVNovelProjectSearchPanel m_npSelectionPanel;
    
    public NOVNovelFormTableButtonEditor(NOVNovelProjectSearchPanel npPanel,TCTable sourcetable,TCTable targettable) 
    {
        m_registry = Registry.getRegistry(this);
        m_npSelectionPanel = npPanel;
        m_addedFormVector = npPanel.m_addedNPVec;       
        m_srcTable = sourcetable;
        m_trgTable = targettable;
        m_tblButton = new JButton();
        m_tblButton.setMinimumSize(new Dimension(18 , 20));
        m_tblButton.setPreferredSize(new Dimension(18 , 20));
        m_tblButton.setMaximumSize(new Dimension(18 , 20));
        m_tblButton.setBackground(Color.WHITE);
        m_tblButton.addActionListener(this);
        if (sourcetable != null) 
        {
            ImageIcon plusIcon = m_registry.getImageIcon("plus.ICON");
            m_tblButton.setIcon(plusIcon);
            m_tblButton.setActionCommand("addRow");   
        }
        else
        {
            ImageIcon minusIcon = m_registry.getImageIcon("minus.ICON");
            m_tblButton.setIcon(minusIcon);
            m_tblButton.setActionCommand("removeRow");
        }
    }   
    
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) 
    {
        return m_tblButton;
    }

    public Object getCellEditorValue() 
    {
        return null;
    }

    public void actionPerformed(ActionEvent e) 
    {
        String actCmd = e.getActionCommand();
        if (actCmd.equals("addRow")) 
        {
        	addRowListener();
        }
        else if (actCmd.equals("removeRow")) 
        {
        	removeRowListener();
        }
        
    }
    
    //KlockWork - 261 start
    private void addRowListener()
    {
    	if (m_srcTable != null  && m_trgTable!=null) 
        {
            int selRow = m_srcTable.getSelectedRow();
            String formName = (String)m_srcTable.getValueAt(selRow, 1);
            if (isValidForm(formName)) 
            {
                Object[] rowdata = m_srcTable.getRowData(selRow);
                m_npSelectionPanel.m_addedNPVec.add(formName);
                m_trgTable.addRow(rowdata);
                m_trgTable.repaint();
                if(m_npSelectionPanel != null)
                {
                    if (m_npSelectionPanel.m_removedNPSet.contains(formName)) 
                    {
                        m_npSelectionPanel.m_removedNPSet.remove(formName);
                    }
                    m_npSelectionPanel.notifyObserver();  
                }
            }
        }
    }
    
    private void removeRowListener()
    {
    	int selRow = m_trgTable.getSelectedRow();
        if (m_trgTable.getValueAt(selRow, 1)!=null)
        {
            if (m_addedFormVector.contains(m_trgTable.getValueAt(selRow, 1)))
            {
                if(m_npSelectionPanel != null) 
                {
                    m_npSelectionPanel.m_addedNPVec.remove(m_trgTable.getValueAt(selRow, 1));
                    m_npSelectionPanel.m_removedNPSet.add(m_trgTable.getValueAt(selRow, 1).toString());
                    m_trgTable.removeRow(selRow);
                    m_npSelectionPanel.notifyObserver();
                }
            }
        }
    	
    }
    //KlockWork - 261 end

    private boolean isValidForm(String formName) 
    {
        if (m_addedFormVector.contains(formName)) 
        {
            //MessageBox.post("The "+formName+" is already added to table.","Cannot add Form",MessageBox.WARNING);
            JOptionPane.showMessageDialog(m_npSelectionPanel, "The "+formName+" is already added to table.", "Cannot add Form", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }   
}
