package com.noi.rac.dhl.eco.masschange.dialogs;

import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.SWTResourceManager;

import com.noi.rac.dhl.eco.form.compound.panels._EngChangeObjectForm_Panel;
import com.noi.rac.dhl.eco.masschange.actions.NOVMassRDChangeAction;
import com.noi.rac.dhl.eco.masschange.listener.NOVRDSearchListener;
import com.noi.rac.dhl.eco.masschange.listener.NOVShowResultButtonListener;
import com.noi.rac.dhl.eco.masschange.operations.NOVRDSearchOperation;
import com.noi.rac.dhl.eco.masschange.utils.NOVMassRDEditValidator;
import com.noi.rac.dhl.eco.masschange.utils.NOVRDMessages;
import com.noi.rac.dhl.eco.util.components.NOVWhereUsedFilterCriteria;
import com.noi.rac.dhl.eco.util.components.NOVWhereUsedFilterDialog;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

/**
 * @author tripathim
 * 
 */

public class MassRDChangeComposite extends AbstractSWTDialog
{
    private _EngChangeObjectForm_Panel m_ecoFormPanel;
    protected NOVMassRDWhereUsedTable m_wherUsedTblObj;
    private TCSession m_session;
    private Map<String, String> m_whereUsedItemsMap;
    private String m_sDefaultRevType = "Create Major Revision";
    private TCComponent m_replacementRD;
    private TCComponent m_selectedRD;
    private Combo m_MMRCombo;
    private Button m_okButton;
    private Button m_applyButton;
    private Button m_cancelButton;
    private Text m_RDIDText;
    private Button m_RDSearchButton;
    private Button m_filterButton;
    private Button m_replRDRadio;
    private Button m_addRDRadio;
    private Button m_removeRDRadio;
    private Button m_addToECORadio;
    private Text m_docIDText;
    private Button m_docSearchButton;
    private Label m_documentLabel;
    public Label m_whereUsedLabel;
    private boolean m_isRDSearch;
    private boolean m_isSuccess;
    private String m_successMsg;
    private String m_actionCmdKey;
    private NOVMassRDEditValidator m_validateObj;
    private NOVShowResultButtonListener m_showResultButtonListener;
    private NOVRDSearchListener m_rdSearchListener;
    private Image m_searchIcon;
    private final int TABLE_WIDTH = 500;
    private final int TABLE_HEIGHT = 200;
    private final int COLUMN_NUMBERS = 4;
    private final int RADIO_BUTTON_COLUMNS = 3;
    private final int RADIO_BUTTON_WIDTH_HINT = 500;
    private final int DOCID_WIDTH_HINT = 25;
    private final int SEARCHBUTTON_WIDTH_HINT = 3;
    private final int OKBUTTON_WIDTH_HINT = 6;
    private final int APPLYBUTTON_WIDTH_HINT = 6;
    private final int CANCELBUTTON_WIDTH_HINT = 6;
    private final int REVRULELABEL_WIDTH_HINT = 13;
    private final int MMRCOMBO_WIDTH_HINT = 13;
    private final int DOCLABEL_WIDTH_HINT = 10;
    private final int RDIDTXT_WIDTH_HINT = 25;
    private final int RDSRCHBUTTON_WIDTH_HINT = 3;
    private final int FILTERBUTTON_WIDTH_HINT = 7;
    
    public MassRDChangeComposite(Shell parentFrame, _EngChangeObjectForm_Panel ecoFormPanel)
    {
        super(parentFrame);
        m_ecoFormPanel = ecoFormPanel;
        m_session = ecoFormPanel.getSession();
    }
    
    @Override
    protected void configureShell(Shell shell)
    {
        super.configureShell(shell);
        shell.setText(NOVRDMessages.getString("Dialog.TITLE"));
    }
    
    @Override
    protected Control createDialogArea(Composite composite)
    {
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        createSearchCriteria(composite);
        createTableUI(composite);
        createRadioButtonUI(composite);
        setFont();
        m_validateObj = new NOVMassRDEditValidator(MassRDChangeComposite.this);
        addListeners();
        return composite;
    }
    
    private void createSearchCriteria(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(COLUMN_NUMBERS, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        
        m_documentLabel = new Label(composite, SWT.NONE);
        GridData gd_m_documentLabel = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd_m_documentLabel.widthHint = ((m_documentLabel.getFont().getFontData())[0].getHeight()) * DOCLABEL_WIDTH_HINT;
        m_documentLabel.setLayoutData(gd_m_documentLabel);
        m_documentLabel.setText(NOVRDMessages.getString("Document.LABEL"));
        
        m_RDIDText = new Text(composite, SWT.READ_ONLY | SWT.BORDER | SWT.FocusOut);
        GridData gd_m_docIDText = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd_m_docIDText.widthHint = ((m_RDIDText.getFont().getFontData())[0].getHeight()) * RDIDTXT_WIDTH_HINT;
        m_RDIDText.setLayoutData(gd_m_docIDText);
        
        m_RDSearchButton = new Button(composite, SWT.NONE);
        GridData gd_m_docSearchButton = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd_m_docSearchButton.widthHint = ((m_RDSearchButton.getFont().getFontData())[0].getHeight())
                * RDSRCHBUTTON_WIDTH_HINT;
        m_RDSearchButton.setLayoutData(gd_m_docSearchButton);
        m_searchIcon = NOVRDMessages.getRegistry().getImage("search.ICON");
        m_RDSearchButton.setImage(m_searchIcon);
        m_RDSearchButton.setFocus();
        
        m_filterButton = new Button(composite, SWT.NONE);
        GridData gd_m_filterButton = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd_m_filterButton.widthHint = ((m_filterButton.getFont().getFontData())[0].getHeight())
                * FILTERBUTTON_WIDTH_HINT;
        m_filterButton.setLayoutData(gd_m_filterButton);
        m_filterButton.setText(NOVRDMessages.getString("Filter.LABEL"));
        m_filterButton.setEnabled(false);
        invokeFilterButtonListener();
        
    }
    
    private void addListeners()
    {
        m_showResultButtonListener = new NOVShowResultButtonListener(MassRDChangeComposite.this);
        m_RDSearchButton.addSelectionListener(m_showResultButtonListener);
        m_rdSearchListener = new NOVRDSearchListener(MassRDChangeComposite.this);
        m_docSearchButton.addSelectionListener(m_rdSearchListener);
    }
    
    private void createTableUI(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        
        m_whereUsedLabel = new Label(composite, SWT.NONE);
        GridData gd_m_documentLabel = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd_m_documentLabel.widthHint = ((m_whereUsedLabel.getFont().getFontData())[0].getHeight()) * DOCLABEL_WIDTH_HINT;
        m_whereUsedLabel.setLayoutData(gd_m_documentLabel);
        m_whereUsedLabel.setText(NOVRDMessages.getString("WhereUsedReport.LABEL"));
        
        Composite composite_1 = new Composite(composite, SWT.EMBEDDED);
        GridData gd_whereUsedTable = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gd_whereUsedTable.widthHint = TABLE_WIDTH;
        gd_whereUsedTable.heightHint = TABLE_HEIGHT;
        composite_1.setLayoutData(gd_whereUsedTable);
        
        Frame frame = SWT_AWT.new_Frame(composite_1);
        
        m_wherUsedTblObj = new NOVMassRDWhereUsedTable();
        JPanel whrUsedRDTable = m_wherUsedTblObj.createTableUI();
        
        JPanel whereUsedTablePanel = new JPanel(new VerticalLayout());
        whereUsedTablePanel.add("top.bind.center.center", whrUsedRDTable);
        
        frame.add(whereUsedTablePanel);
        
    }
    
    private void invokeFilterButtonListener()
    {
        m_filterButton.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                final int hDisp = 280;
                final int vDisp = 25;
                final int x = m_filterButton.getLocation().x + hDisp;
                final int y = m_filterButton.getLocation().y + vDisp;
                SwingUtilities.invokeLater(new Runnable()
                {
                    public void run()
                    {
                        NOVWhereUsedFilterDialog filterDlg = new NOVWhereUsedFilterDialog(m_wherUsedTblObj
                                .getWhereUsedFilterCriteria());
                        Object obj = filterDlg.showDialog(x, y);
                        NOVWhereUsedFilterCriteria filterCriteria = (NOVWhereUsedFilterCriteria) obj;
                        if (filterCriteria != m_wherUsedTblObj.getWhereUsedFilterCriteria())
                        {
                            m_wherUsedTblObj.setWhereUsedFilterCriteria(filterCriteria);
                            m_wherUsedTblObj.filterWhrUsedList();
                        }
                    }
                });
            }
        });
    }
    
    private void invokeRepalceRDRadioButtonListener()
    {
        
        m_replRDRadio.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                if (!m_docSearchButton.isEnabled())
                {
                    m_docSearchButton.setEnabled(true);
                }
                m_actionCmdKey = "ReplaceRD";
            }
        });
    }
    
    private void invokeAddRDRadioButtonListener()
    {
        m_addRDRadio.addListener(SWT.Selection, new Listener()
        {
            
            @Override
            public void handleEvent(Event arg0)
            {
                if (!m_docSearchButton.isEnabled())
                {
                    m_docSearchButton.setEnabled(true);
                }
                m_actionCmdKey = "AddRD";
            }
        });
    }
    
    private void invokeRemoveRDRadioButtonListener()
    {
        m_removeRDRadio.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                m_docSearchButton.setEnabled(false);
                m_docIDText.setText("");
                m_replacementRD = null;
                m_actionCmdKey = "RemoveRD";
            }
        });
    }
    
    private void invokeNoRDChangeRadioButtonListener()
    {
        m_addToECORadio.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                m_docSearchButton.setEnabled(false);
                m_docIDText.setText("");
                m_replacementRD = null;
                m_actionCmdKey = "NoRDChange";
            }
        });
    }
    
    private void createRadioButtonUI(Composite parent)
    {
        
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(RADIO_BUTTON_COLUMNS, false));
        GridData gd_composite = new GridData(SWT.CENTER, SWT.FILL, true, true);
        gd_composite.widthHint = RADIO_BUTTON_WIDTH_HINT;
        composite.setLayoutData(gd_composite);
        m_replRDRadio = new Button(composite, SWT.RADIO);
        m_replRDRadio.setText(NOVRDMessages.getString("ReplaceRD.LABEL"));
        invokeRepalceRDRadioButtonListener();
        
        m_docIDText = new Text(composite, SWT.READ_ONLY | SWT.BORDER);
        GridData gd_m_docIDText = new GridData(SWT.FILL, SWT.CENTER, false, false);
        gd_m_docIDText.widthHint = ((m_docIDText.getFont().getFontData())[0].getHeight()) * DOCID_WIDTH_HINT;
        m_docIDText.setLayoutData(gd_m_docIDText);
        
        m_docSearchButton = new Button(composite, SWT.NONE);
        GridData gd_m_docSearchButton = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gd_m_docSearchButton.widthHint = ((m_docSearchButton.getFont().getFontData())[0].getHeight())
                * SEARCHBUTTON_WIDTH_HINT;
        m_docSearchButton.setLayoutData(gd_m_docSearchButton);
        m_docSearchButton.setImage(m_searchIcon);
        m_docSearchButton.setEnabled(false);
        
        m_addRDRadio = new Button(composite, SWT.RADIO);
        m_addRDRadio.setText(NOVRDMessages.getString("AddRD.LABEL"));
        
        invokeAddRDRadioButtonListener();
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        
        m_removeRDRadio = new Button(composite, SWT.RADIO);
        m_removeRDRadio.setText(NOVRDMessages.getString("RemoveRD.LABEL"));
        invokeRemoveRDRadioButtonListener();
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        
        m_addToECORadio = new Button(composite, SWT.RADIO);
        m_addToECORadio.setText(NOVRDMessages.getString("AddToECO.LABEL"));
        invokeNoRDChangeRadioButtonListener();
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
    }
    
    protected Control createButtonBar(final Composite parent)
    {
        Composite composite = new Composite(parent, SWT.CENTER);
        composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        composite.setLayout(new GridLayout(1, false));
        
        displayOperationsBasedOnType(composite);
        
        Composite btn_composite = new Composite(composite, SWT.NONE);
        btn_composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        btn_composite.setLayout(new GridLayout(3, false));
        
        m_okButton = new Button(btn_composite, SWT.NONE);
        GridData gd_m_okButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_m_okButton.widthHint = ((m_okButton.getFont().getFontData())[0].getHeight()) * OKBUTTON_WIDTH_HINT;
        m_okButton.setLayoutData(gd_m_okButton);
        m_okButton.setText(NOVRDMessages.getString("OK.LABEL"));
        invokeOkButtonListener();
        
        m_applyButton = new Button(btn_composite, SWT.NONE);
        GridData gd_m_applyButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_m_applyButton.widthHint = ((m_applyButton.getFont().getFontData())[0].getHeight()) * APPLYBUTTON_WIDTH_HINT;
        m_applyButton.setLayoutData(gd_m_applyButton);
        m_applyButton.setText(NOVRDMessages.getString("Apply.LABEL"));
        invokeApplyButtonListener();
        
        m_cancelButton = new Button(btn_composite, SWT.NONE);
        GridData gd_m_cancelButton = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
        gd_m_cancelButton.widthHint = ((m_cancelButton.getFont().getFontData())[0].getHeight())
                * CANCELBUTTON_WIDTH_HINT;
        m_cancelButton.setLayoutData(gd_m_cancelButton);
        m_cancelButton.setText(NOVRDMessages.getString("Cancel.LABEL"));
        invokeCancelButtonListener();
        
        return composite;
    }
    
    private void invokeOkButtonListener()
    {
        m_okButton.addListener(SWT.Selection, new Listener()
        {
            
            @Override
            public void handleEvent(Event arg0)
            {
                if (m_validateObj.validateUI())
                {
                    executeMassRDUpdate();
                    MassRDChangeComposite.this.getShell().dispose();
                }
                
            }
        });
    }
    
    private void invokeApplyButtonListener()
    {
        m_applyButton.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                if (m_validateObj.validateUI())
                {
                    executeMassRDUpdate();
                }
            }
        });
    }
    
    private void invokeCancelButtonListener()
    {
        m_cancelButton.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                MassRDChangeComposite.this.getShell().dispose();
            }
        });
    }
    
    private void createMMRUI(Composite parent)
    {
        Composite mmr_composite = new Composite(parent, SWT.NONE);
        mmr_composite.setLayout(new GridLayout(2, false));
        mmr_composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        Label m_setRevisionRuleLabel = new Label(mmr_composite, SWT.NONE);
        GridData gd_m_setRevisionRuleLabel = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
        gd_m_setRevisionRuleLabel.widthHint = ((m_setRevisionRuleLabel.getFont().getFontData())[0].getHeight())
                * REVRULELABEL_WIDTH_HINT;
        m_setRevisionRuleLabel.setLayoutData(gd_m_setRevisionRuleLabel);
        m_setRevisionRuleLabel.setText(NOVRDMessages.getString("SetRevisionRule.LABEL"));
        org.eclipse.swt.graphics.Font font = SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD);
        m_setRevisionRuleLabel.setFont(font);
        
        m_MMRCombo = new Combo(mmr_composite, SWT.DROP_DOWN | SWT.READ_ONLY);
        m_MMRCombo.setItems(new String[] { "Create Major Revision", "Create Minor Revision" });
        m_MMRCombo.setText(m_sDefaultRevType);
        
        GridData gd_m_MMRCombo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_m_MMRCombo.widthHint = ((m_MMRCombo.getFont().getFontData())[0].getHeight()) * MMRCOMBO_WIDTH_HINT;
        m_MMRCombo.setLayoutData(gd_m_MMRCombo);
    }
    
    private void setFont()
    {
        org.eclipse.swt.graphics.Font font = SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD);
        m_documentLabel.setFont(font);
        m_removeRDRadio.setFont(font);
        m_addToECORadio.setFont(font);
        m_addRDRadio.setFont(font);
        m_replRDRadio.setFont(font);
        m_whereUsedLabel.setFont(font);
    }
    
    private void displayOperationsBasedOnType(Composite composite)
    {
        if (!m_ecoFormPanel.getTypeofChange().equals("Lifecycle"))
        {
            createMMRUI(composite);
        }
        else
        {
            setEnabled(false);
        }
    }
    
    private void setEnabled(boolean enabled)
    {
        m_removeRDRadio.setEnabled(enabled);
        m_docSearchButton.setEnabled(enabled);
        m_addRDRadio.setEnabled(enabled);
        m_replRDRadio.setEnabled(enabled);
    }
    
    private void executeMassRDUpdate()
    {
        final String mmrRevType = getSelectedRevType();
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if (display != null)
        {
            display.syncExec(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        new ProgressMonitorDialog(display.getActiveShell()).run(true, false, new NOVMassRDChangeAction(
                                MassRDChangeComposite.this, m_actionCmdKey, mmrRevType));
                    }
                    catch (InvocationTargetException e)
                    {
                        e.printStackTrace();
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
        updateTableAfterOperation();
        if(m_isSuccess)
        {
            MessageBox.post(getShell(), m_successMsg, NOVRDMessages.getString("Info.INFO"), MessageBox.INFORMATION);
        }
    }
    
    private void updateTableAfterOperation()
    {
        Vector<TCComponent> targetComps = m_ecoFormPanel.getECOTargets();
        List<String> nonEditableItemList = m_wherUsedTblObj.getNonEditableList();
        Map<String, String> nonEditableItemMap = m_wherUsedTblObj.getNonEditableMap();
        
        int rowcount = m_wherUsedTblObj.m_whrUsedtblmodel.getRowCount();
        for (int rc = 0; rc < rowcount; rc++)
        {
            Object cellVal = m_wherUsedTblObj.m_whrUsedtblmodel.getValueAt(rc, 0);
            String itemID = (String) m_wherUsedTblObj.m_whrUsedTable.getModel().getValueAt(rc, 1);
            for (int i = 0; i < targetComps.size(); i++)
            {
                if(targetComps.get(i).toString().startsWith(itemID))
                {
                    System.out.println("selected item id "+itemID);
                    nonEditableItemList.add(itemID);
                    nonEditableItemMap.put(itemID, NOVRDMessages.getString("AlreadyAddedToTargetList.MESSAGE"));
                }
                if (cellVal instanceof Boolean)
                {
                    m_wherUsedTblObj.m_whrUsedtblmodel.setValueAt(false, rc, 0);
                }
            }
        }
        m_wherUsedTblObj.resetHeaderCheckbox();
        m_wherUsedTblObj.m_whrUsedtblmodel.fireTableDataChanged();
    }
    
    public void setSuccessStatus(boolean isSuccess, String successMsg)
    {
        m_isSuccess = isSuccess;
        m_successMsg = successMsg;
    }
    
    public void setValueForSelectedButton(boolean isSelected)
    {
        m_isRDSearch = isSelected;
    }
    
    public void setSearchedDocument(TCComponentItem selectedDoc)
    {
        
        if (m_isRDSearch)
        {
            m_RDIDText.setText(selectedDoc.toString());
            m_RDIDText.setToolTipText(selectedDoc.toString());
            m_selectedRD = selectedDoc;
            executeRDSearch();
            m_wherUsedTblObj.resetHeaderCheckbox();
        }
        else
        {
            m_docIDText.setText(selectedDoc.toString());
            m_docIDText.setToolTipText(selectedDoc.toString());
            m_replacementRD = selectedDoc;
        }
    }
    
    private void executeRDSearch()
    {
        NOVRDSearchOperation searchOp = new NOVRDSearchOperation(this, m_ecoFormPanel.getSession(),
                m_selectedRD.getUid());
        searchOp.executeWhereUsedSearch();
        searchOp.populateWhereUsedTable();
        m_whereUsedItemsMap = searchOp.getWhereUsedItemsMap();
    }
    
    public TCComponent[] getSelectedWhereUsedItem()
    {
        int rowcount = m_wherUsedTblObj.m_whrUsedtblmodel.getRowCount();
        Vector<String> puidVec = new Vector<String>();
        for (int rc = 0; rc < rowcount; rc++)
        {
            Object cellVal = m_wherUsedTblObj.m_whrUsedtblmodel.getValueAt(rc, 0);
            String itemID = (String) m_wherUsedTblObj.m_whrUsedTable.getModel().getValueAt(rc, 1);
            if (cellVal instanceof Boolean)
            {
                if (((Boolean) cellVal).booleanValue())
                {
                    puidVec.add(m_whereUsedItemsMap.get(itemID));
                }
            }
        }
        return convertVectorInToComponentArray(puidVec);
    }
    
    private TCComponent[] convertVectorInToComponentArray(Vector<String> puidVec)
    {
        TCComponent[] comps = new TCComponent[puidVec.size()];
        try
        {
            comps = m_session.stringToComponent(puidVec.toArray(new String[puidVec.size()]));
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return comps;
    }
    
    private String getSelectedRevType()
    {
        if (m_MMRCombo != null)
        {
            return m_MMRCombo.getText().toString();
        }
        else
        {
            return m_sDefaultRevType;
        }
    }
    
    public TCComponent getSelectedRD()
    {
        return m_selectedRD;
    }
    
    public TCComponent getReplacementRD()
    {
        return m_replacementRD;
    }
    
    public JTable getWhereUsedTable()
    {
        return m_wherUsedTblObj.m_whrUsedTable;
    }
    
    public AIFTableModel getWhereUsedTableModel()
    {
        return m_wherUsedTblObj.m_whrUsedtblmodel;
    }
    
    public _EngChangeObjectForm_Panel getECOFormPanel()
    {
        return m_ecoFormPanel;
    }
    
    /**
     * @return the m_wherUsedTblObj
     */
    public NOVMassRDWhereUsedTable getMassRDWhereUsedTable()
    {
        return m_wherUsedTblObj;
    }
    
    /**
     * @return the m_filterButton
     */
    public Button getFilterButton()
    {
        return m_filterButton;
    }
    
    /**
     * @return the m_actionCmdKey
     */
    public String getActionCammand()
    {
        return m_actionCmdKey;
    }
}
