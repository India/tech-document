package com.noi.rac.dhl.eco.util.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.rac.util.iTextArea;

@SuppressWarnings("unused")
public class NOVECOReasonPanel extends JPanel implements ComponentListener, ISubject 
{
	private static final long serialVersionUID = 1L;
	protected JPanel requiredPanel,supportFilesPanel;
	boolean required = true;
	private TCSession tcSession;
	public Map<String, TCComponentForm> formCompMap;	
	private TCPreferenceService prefService;
	private Registry registry;
	private JPanel mainPanel;
	public boolean isChangeTypeAltered = false;
	private List<IObserver> observers = new ArrayList<IObserver>();
	public iTextArea reasonforChange;
	public iTextArea addtnlCommentsText;
	JScrollPane addtnlCommentsTextScroll;
	JScrollPane reasonforChangeTextScroll;
	
	public NOVECOReasonPanel() 
	{
		tcSession = (TCSession) AIFUtility.getDefaultSession();
		prefService = tcSession.getPreferenceService();
		registry = Registry.getRegistry(this);
		addComponentListener(this);
		formCompMap = new HashMap<String, TCComponentForm>();
		initUI();
	}
	
	private void initUI() 
	{		
		mainPanel = new JPanel(new VerticalLayout());
		
		JPanel panel1 = new JPanel(new VerticalLayout());
		JLabel lblAddReasonForChange = new JLabel("Reason for Change:");//4824
		lblAddReasonForChange.setFont(new Font("Arial",1,12));			
		addtnlCommentsText = new iTextArea();
		addtnlCommentsText.setLineWrap(true);
		addtnlCommentsText.setWrapStyleWord(true);
		addtnlCommentsText.setRequired(true);
		
		addtnlCommentsText.addKeyListener(new KeyListener() {
			
			
			public void keyTyped(KeyEvent keyevent) {
				// TODO Auto-generated method stub
				
			}
		
			public void keyReleased(KeyEvent keyevent) 
			{
				// TODO Auto-generated method stub
				NOVECOReasonPanel.this.notifyObserver();
			}
			
			
			public void keyPressed(KeyEvent keyevent) {
				// TODO Auto-generated method stub
				
			}
		});
		addtnlCommentsTextScroll = new JScrollPane(addtnlCommentsText);
		addtnlCommentsTextScroll.setPreferredSize(new Dimension(530, 225));
		panel1.add("top.nobind.left.center",lblAddReasonForChange);
		panel1.add("top.nobind.left.center",addtnlCommentsTextScroll);	
		
		JPanel panel2 = new JPanel(new VerticalLayout());
		JLabel lblReasonForChange = new JLabel("Reason for Change from ECR:");
		lblReasonForChange.setFont(new Font("Arial",1,12));			
		reasonforChange = new iTextArea();
		reasonforChange.setLineWrap(true);
		reasonforChange.setWrapStyleWord(true);
		reasonforChange.setRequired(false);
		reasonforChange.setEditable(false);
		reasonforChangeTextScroll = new JScrollPane(reasonforChange);
		reasonforChangeTextScroll.setPreferredSize(new Dimension(530, 225));
		panel2.add("top.nobind.left.center",lblReasonForChange);
		panel2.add("top.nobind.left.center",reasonforChangeTextScroll);		
		
		mainPanel.add("top.nobind.center.center",panel1);
		mainPanel.add("top.nobind.center.center",new JLabel(" "));
		mainPanel.add("top.nobind.center.center",panel2);
		
		this.add(mainPanel);
	}

	public void componentHidden(ComponentEvent e){}

	public void componentMoved(ComponentEvent e) {}

	public void componentResized(ComponentEvent e)
	{System.out.println("\nheight " + NOVECOReasonPanel.this.getSize().height + " width " + NOVECOReasonPanel.this.getSize().width + "\n");
	
		Dimension dim = NOVECOReasonPanel.this.getSize();
		int height = dim.height;
		int width = dim.width;
		
		addtnlCommentsTextScroll.setPreferredSize(new Dimension(width - 20, 225 + (height - 525)/2));
		reasonforChangeTextScroll.setPreferredSize(new Dimension(width - 20, 225 + (height - 525)/2));
		addtnlCommentsTextScroll.updateUI();
		addtnlCommentsTextScroll.revalidate();
		addtnlCommentsTextScroll.repaint();
		
		reasonforChangeTextScroll.updateUI();
		reasonforChangeTextScroll.revalidate();
		reasonforChangeTextScroll.repaint();
		
		mainPanel.updateUI();
		mainPanel.revalidate();
		mainPanel.repaint();
		
		this.updateUI();
		this.revalidate();
		this.repaint();
		//mainPanel.setLocation(width/2-150, height/2-150);
	}

	public void componentShown(ComponentEvent e) {}	
	
	public String getAddReasonForChange()
	{
		return addtnlCommentsText == null ? "" : addtnlCommentsText.getText().trim();		
	}
	
	public boolean isMandatoryValuesFilled() 
	{
		if ((!(getAddReasonForChange().length()>0)))
		{
			return false;
		}
		return true;
	}
	
	
	public void notifyObserver() 
	{
		Iterator<IObserver> i = observers.iterator();
		while (i.hasNext())
		{
			IObserver o = (IObserver) i.next();
			o.update();
		}
	}

	
	public void registerObserver(IObserver ob) 
	{
		observers.add(ob);		
	}

	
	public void removeObserver(IObserver ob) 
	{
		observers.remove(ob);				
	}
}
