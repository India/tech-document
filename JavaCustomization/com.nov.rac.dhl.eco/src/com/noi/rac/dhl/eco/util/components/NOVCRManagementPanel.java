package com.noi.rac.dhl.eco.util.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.noi.util.components.NOIJLabel;
import com.nov.rac.dhl.eco.helper.NOVECOConstants;
import com.nov.rac.dhl.eco.helper.NOVECOHelper;
import com.nov.rac.dhl.eco.wizard.NOVCRManagementDialog;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.ActionAdapter;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.VerticalLayout;

public class NOVCRManagementPanel extends JPanel implements IObserver, ISubject, ComponentListener,
        ListSelectionListener
{
    private static final long serialVersionUID = 1L;
    TCTable pendingCRtbl;
    TCTable ptargetCRs;
    AIFTableModel selecedCRtblmodel;
    AIFTableModel pendingCRtblmodel;
    TCSession session;
    private JScrollPane scpane;
    private JScrollPane scpane2;
    public Map<String, TCComponentForm> formCompMap;
    private Map<String, Vector<TCComponent>> itemRevFormMap;
    public Vector<TCComponentForm> addedCRsVec;
    public Set<TCComponentForm> removedCrs;
    TCUserService usrService;
    String typeofChange;
    
    private ArrayList<IObserver> subList;
    
    private String[] typeStr = { "_DHL_EngChangeForm_" };
    private String[] relation = { "_ECNECR_" };
    private NOVCRDescriptionPanel pendingCRDescPanel = null;
    private static boolean callService = true;
    private NOVCRManagementDialog dlg = null;
    private TableRowSorter<AIFTableModel> m_mPanelPendingCRTableSorter;
    public boolean groupNotFound = false;
    private Registry m_registry; // TCDECREL - 4961

    
    public NOVCRManagementPanel(NOVCRManagementDialog dlg)
    {
        this.dlg = dlg;
        session = (TCSession) AIFUtility.getDefaultSession();
        formCompMap = new HashMap<String, TCComponentForm>();
        itemRevFormMap = new HashMap<String, Vector<TCComponent>>();
        addedCRsVec = new Vector<TCComponentForm>();
        removedCrs = new HashSet<TCComponentForm>();
        subList = new ArrayList<IObserver>();
        
        initTableUI();
        addComponentListener(this);
        usrService = session.getUserService();
        m_registry = Registry.getRegistry(this);// TCDECREL - 4961
    }
    
    public TCSession getSession()
    {
        return session;
    }
    
    private void populateCRTable(String statusType)
    {
        try
        {
            Object[] objs = new Object[2];
            objs[0] = statusType;
            objs[1] = "General";
            Object retObj = usrService.call("NATOIL_CRsQuery", objs);
            if (retObj != null)
            {
                removeMPanelPendingCRTableRows();// TCDECREL - 3382
                TCComponent[] crObjs = (TCComponent[]) retObj;
                
                for (int i = 0; i < crObjs.length; i++)
                {
                    TCComponentForm form = (TCComponentForm) crObjs[i];
                    Vector<Object> rowData = getRowData(form);
                    
                    // if(form.getTCProperty("ecr_status").getStringValue().equalsIgnoreCase("Open")
                    // && !(rowData.get(6).toString().trim().length()>0))
                    if (form.getTCProperty("ecr_status").getStringValue().equalsIgnoreCase("Open"))
                    {
                        pendingCRtbl.addRow(rowData);
                    }
                }
                if (pendingCRtbl.getRowCount() == 0)
                {
                    MessageBox.post("No Open CR(s) found for the given criteria.", "Info", MessageBox.INFORMATION);
                    makeDialogActive();
                    return;
                }
                sortMPanelCRTable();// TCDECREL - 3382
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    /*
     * Purpose : Refresh the data in pendingCRtbl and ptargetCRs table; Called
     * after clicking 'Set Status to Closed' Input : None Output : None
     */
    public void refreshCRTable()
    {
        // Remove Closed CRs from Pending CRs list
        for (int i = 0; i < addedCRsVec.size(); i++)
        {
            try
            {
                String ecr = addedCRsVec.get(i).getStringProperty("object_name");
                System.out.println("Removing ECR: " + ecr + "\n");
                removeRow(ecr);
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        // Clear Selected CRs list
        clearSelectedCRs();
    }
    
    public void removeRow(String ecrName)
    {
        for (int row = 0; row < pendingCRtblmodel.getRowCount(); row++)
        {
            if (pendingCRtblmodel.getValueAt(row, 1).toString().equals(ecrName))
            {
                pendingCRtbl.removeRow(row);
                //pendingCRtbl.updateUI(); //TCDECREL-5016
                break;
            }
        }
    }
    
    /*************************************************************************************************************/
    
    public void populateSelectedCRTable(TCComponentForm[] crforms)
    {
        if (crforms != null)
        {
            for (int i = 0; i < crforms.length; i++)
            {
                Vector<Object> rowData = getRowData(crforms[i]);
                selecedCRtblmodel.addRow(rowData);
                if (!addedCRsVec.contains(crforms[i]))
                {
                    addedCRsVec.add(crforms[i]);
                }
            }
        }
    }
    
    public void setRowData(TCComponentForm crForm)
    {
        addedCRsVec.add(crForm);
    }
    
    public Vector<Object> getRowData(TCComponentForm form)
    {
        java.util.Vector<Object> rowData = new java.util.Vector<Object>();
        
        try
        {
            rowData.add("");
            String crName = form.getTCProperty("object_name").getStringValue();
            rowData.add(crName);
            rowData.add(form.getTCProperty("change_type").getStringValue());
            rowData.add(form.getTCProperty("group").getStringValue());
            rowData.add(form.getTCProperty("requested_by"));
            rowData.add(form.getTCProperty("creation_date").getDateValue());
            rowData.add(getRefCOs(form));
            formCompMap.put(crName, form);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return rowData;
    }
    
    public Object[] getSelectedCRs()
    {
        Object[] forms = null;
        
        if (addedCRsVec.size() > 0)
        {
            forms = addedCRsVec.toArray();
        }
        return (Object[]) (forms == null ? new Object[0] : forms);
    }
    
    public void clearSelectedCRs()
    {
        selecedCRtblmodel.removeAllRows();
        // formCompMap.clear();
        addedCRsVec.removeAllElements();
        removedCrs.clear();
    }
    
    public Map<String, Vector<TCComponent>> buildCRItemRevMap()
    {
        Object[] forms = getSelectedCRs();
        if (forms != null)
        {
            itemRevFormMap.clear();
            for (int i = 0; i < forms.length; i++)
            {
                TCComponentForm crForm = (TCComponentForm) forms[i];
                try
                {
                    AIFComponentContext[] compConxt = crForm.whereReferenced();
                    for (int j = 0; j < compConxt.length; j++)
                    {
                        InterfaceAIFComponent intAifComp = compConxt[j].getComponent();
                        if (intAifComp instanceof TCComponentItemRevision)
                        {
                            TCComponentItemRevision itemRevAttached = (TCComponentItemRevision) intAifComp;
                            TCComponentItemRevision latestRev = itemRevAttached.getItem().getLatestItemRevision();
                            boolean isInProcess = NOVECOHelper.isItemRevInProcess(latestRev);
                            if (!isInProcess)
                            {
                                String crStr = latestRev.getProperty("object_string") + "::"
                                        + crForm.getProperty("object_name");
                                Vector<TCComponent> tarCompVec = new Vector<TCComponent>();
                                tarCompVec.add(0, (TCComponent) latestRev);
                                tarCompVec.add(1, crForm);
                                itemRevFormMap.put(crStr, tarCompVec);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            itemRevFormMap.clear();
        }
        return itemRevFormMap;
    }
    
    private String getRefCOs(TCComponentForm crForm)
    {
        String retStr = "";
        if (crForm != null)
        {
            try
            {
                AIFComponentContext[] compCxt = crForm.whereReferencedByTypeRelation(typeStr, relation);
                for (int i = 0; i < compCxt.length; i++)
                {
                    retStr = retStr + compCxt[i].getComponent().getProperty("object_name") + ",";
                }
                if (compCxt.length > 0)
                {
                    if (retStr.substring(retStr.length() - 1, retStr.length()).equals(","))
                    {
                        retStr = retStr.substring(0, retStr.length() - 1);
                    }
                }
                
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return retStr;
    }
    
    private void initTableUI()
    {
        String[] columnNames = { "", "CR", "Type", "Group", "Initiator", "Date Entered", "Ref.CO's" };
        
        selecedCRtblmodel = new AIFTableModel(columnNames);
        
        pendingCRtblmodel = new AIFTableModel(columnNames);
        pendingCRtbl = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        pendingCRtbl.setModel(pendingCRtblmodel);
        pendingCRtbl.setSession(session);
        
        pendingCRtbl.getSelectionModel().addListSelectionListener(this);
        pendingCRtbl.getColumnModel().getSelectionModel().addListSelectionListener(this);
        
        pendingCRtbl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        pendingCRtbl.getTableHeader().setReorderingAllowed(false);
        pendingCRtbl.getColumnModel().getColumn(0).setResizable(false);
        //pendingCRtbl.setSortEnabled(false);
        //createECRTableSorter();// TCDECREL - 3382
        scpane = new JScrollPane(pendingCRtbl);
        scpane.setPreferredSize(new Dimension(520, 170));
        
        ptargetCRs = new TCTable()
        {
            private static final long serialVersionUID = 1L;
            
            public boolean isCellEditable(int row, int col)
            {
                if (col == 0)
                {
                    return true;
                }
                else
                    return false;
            }
        };
        ptargetCRs.setModel(selecedCRtblmodel);
        ptargetCRs.setSession(session);
        ptargetCRs.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ptargetCRs.getTableHeader().setReorderingAllowed(false);
        ptargetCRs.getColumnModel().getColumn(0).setResizable(false);
        ptargetCRs.getColumnModel().getSelectionModel().addListSelectionListener(this);
        ptargetCRs.getSelectionModel().addListSelectionListener(this);
        ptargetCRs.setSortEnabled(false);
        
        scpane2 = new JScrollPane(ptargetCRs);
        scpane2.setPreferredSize(new Dimension(520, 120));
        
        pendingCRtbl.getColumnModel().getColumn(0)
                .setCellEditor(new TableButtonEditor(NOVCRManagementPanel.this, pendingCRtbl, ptargetCRs));
        ptargetCRs.getColumnModel().getColumn(0)
                .setCellEditor(new TableButtonEditor(NOVCRManagementPanel.this, null, ptargetCRs));
        
        pendingCRtbl.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(true));
        ptargetCRs.getColumnModel().getColumn(0).setCellRenderer(new TableButtonRenderer(false));
        
        pendingCRtbl.getColumnModel().getColumn(5).setCellRenderer(new DateCellRenderer());
        ptargetCRs.getColumnModel().getColumn(5).setCellRenderer(new DateCellRenderer());
        
        pendingCRtbl.getColumnModel().getColumn(6).setCellRenderer(new TextWrapRenderer());
        ptargetCRs.getColumnModel().getColumn(6).setCellRenderer(new TextWrapRenderer());
        
        pendingCRtbl.getColumnModel().getColumn(0).setPreferredWidth(18);
        ptargetCRs.getColumnModel().getColumn(0).setPreferredWidth(18);
        
        pendingCRtbl.getColumnModel().getColumn(5).setPreferredWidth(105);
        ptargetCRs.getColumnModel().getColumn(5).setPreferredWidth(105);
        
        pendingCRDescPanel = new NOVCRDescriptionPanel();
        pendingCRDescPanel.showCRDescriptionPanel(false);
        
        JLabel label1 = new JLabel("Change Requests to Close");
        label1.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        
        JPanel pendingCrPanel = new JPanel(new VerticalLayout());
        pendingCrPanel.add("top.nobind.center.center", scpane);
        pendingCrPanel.add("top.nobind.center.center", pendingCRDescPanel);
        pendingCrPanel.add("top.nobind.center.center", label1);
        pendingCrPanel.add("top.nobind.center.center", scpane2);
        
        JPanel filerPanel = new JPanel();
        final JTextField tiltertxt = new JTextField(15);
        final JComboBox grpcmb = new JComboBox();
        
        /* Rakesh:060312 - Group list is driven by preference - Starts here */
        // grpcmb.addItem("");
        // NOVECOHelper.retriveLOVValues(session,NOVECOConstants.DHL_ECR_Groups,
        // grpcmb);
        String groupPrefName = NOVECOConstants.ECR_Groups_base + session.getCurrentGroup().toString().substring(0, 2);
        NOVECOHelper.retriveLOVValues(session, groupPrefName, grpcmb);
        if (grpcmb.getItemCount() == 2)
            grpcmb.setSelectedIndex(1); // Default to Group if there is only 1
                                        // group present
        /* Rakesh:060312 - Group list is driven by preference - Ends here */

        grpcmb.setPreferredSize(new Dimension(150, 20));
        grpcmb.addItemListener(new ItemListener()
        {
            public void itemStateChanged(ItemEvent itemEvt)
            {
                if (itemEvt.getStateChange() == ItemEvent.SELECTED && ((String)itemEvt.getItem()).length()!=0) //Added AND logical condition to avoid listener to call when user hit enter on txtfield and it clears last selected combo value
                {
                    if (NOVCRManagementPanel.callService)
                    {
                        tiltertxt.setText("");
                        String groupName = (String) itemEvt.getItem();
                        Object[] objs = new Object[3];
                        if (groupName.trim().length() == 0)
                        {
                            groupName = "*";
                        }
                        objs[0] = groupName;
                        
                        objs[1] = "Open";
                        
                        objs[2] = "General";
                     // Rakesh - ECR search by Group - service call - starts here   
                        searchECR(objs,"NATOIL_CRsGroupQuery",m_registry.getString("ShowGroupSearch.MSG"));//TCDECREL - 4961
                        showMessageForEmptyResult();
                     // Rakesh - ECR search by Group - service call - ends here
                    }
                    else
                    {
                        NOVCRManagementPanel.callService = true;
                    }
                }
            }
        });
        
        // final JTextField tiltertxt = new JTextField(15);
        tiltertxt.addKeyListener(new KeyAdapter()
        {
            public void keyReleased(KeyEvent ke)
            {
                
                int keyCode = ke.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER)
                {
                    NOVCRManagementPanel.callService = false;
                    grpcmb.setSelectedItem("");
                    String crNumber = tiltertxt.getText();
                    Object[] objs = new Object[3];
                    objs[0] = crNumber;
                    
                    objs[1] = "Open";
                    
                    objs[2] = "General";
                    
                    // Rakesh - ECR search by Name - service call - starts here
                    searchECR(objs,"NATOIL_CRsNameQuery",m_registry.getString("ShowECRSearch.MSG"));//TCDECREL - 4961
                    showMessageForEmptyResult();
                    // Rakesh - ECR search by Name - service call - ends here
                    
                }
            }
        });
        
        JButton showFilerBtn = new JButton("Show All");
        showFilerBtn.addActionListener(new ActionAdapter()
        {
            public void actionPerformed(ActionEvent ae)
            {
                grpcmb.setSelectedItem("");
                tiltertxt.setText("");
                //populateCRTable("Open");
                // ECR search ALL - service call - starts here
                Object[] objs = new Object[2];
                objs[0] = "Open";
                objs[1] = "General";
                //searchAllECR("Open","NATOIL_CRsQuery",);//TCDECREL - 4961
                searchECR(objs,"NATOIL_CRsQuery",m_registry.getString("ShowAllSearch.MSG"));//TCDECREL - 4961
                
                showMessageForEmptyResult();
                // ECR search ALL - service call - end here
            }
        });
        
        JButton findBtn = new JButton("Find CR");
        findBtn.addActionListener(new ActionAdapter()
        {
            public void actionPerformed(ActionEvent ae)
            {
                grpcmb.setSelectedItem("");
                String crNumber = tiltertxt.getText();
                Object[] objs = new Object[3];
                objs[0] = crNumber;
                
                objs[1] = "Open";
                // pendingCRtbl.removeAllRows();
                
                objs[2] = "General";
                
                // Rakesh - ECR search by Name - service call - starts here
                searchECR(objs,"NATOIL_CRsNameQuery",m_registry.getString("ShowECRSearch.MSG"));//TCDECREL - 4961
                showMessageForEmptyResult();
                // Rakesh - ECR search by Name - service call - ends here
                
            }
        });
        
        filerPanel.add(new NOIJLabel("Group Filter"));
        filerPanel.add(grpcmb);
        filerPanel.add(tiltertxt);
        filerPanel.add(findBtn);
        filerPanel.add(showFilerBtn);
        
        this.setLayout(new VerticalLayout(5));
        this.add("top.nobind.center.center", filerPanel);
        this.add(new JSeparator(), "growx, wrap");
        JLabel labelHeader = new JLabel("Open Change Requests");
        labelHeader.setFont(new Font("Dialog", java.awt.Font.BOLD, 12));
        this.add("top.nobind.center.center", labelHeader);
        this.add("top.nobind.center.center", pendingCrPanel);
        this.add("top.nobind.center.center", new JLabel(" "));
        this.add(new JSeparator(), "growx, wrap");
    }
    
    public String getCRCommentString()
    {
        Object[] forms = addedCRsVec.toArray();
        String comments = "";
        for (int i = 0; i < forms.length; i++)
        {
            TCComponentForm crForm = (TCComponentForm) forms[i];
            try
            {
                String crComments = crForm.getTCProperty("comments").getStringValue();
                if (crComments != null)
                {
                    comments = "CR Comments:" + crComments + "\n";
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
            
        }
        return comments;
    }
    
    public String getReason4ChangeString()
    {
        Object[] forms = addedCRsVec.toArray();
        String reason4Change = "";
        for (int i = 0; i < forms.length; i++)
        {
            TCComponentForm crForm = (TCComponentForm) forms[i];
            
            try
            {
                String crName = crForm.getTCProperty("object_name").getStringValue();
                String crReason4Chn = crForm.getTCProperty("reason_for_change").getStringValue();
                String crDetails = crForm.getTCProperty("change_desc").getStringValue();
                
                if (reason4Change.length() > 0)
                {
                    reason4Change = reason4Change + "ECR: " + crName + "\n";
                }
                else
                {
                    reason4Change = "ECR: " + crName + "\n";
                }
                if (crReason4Chn != null)
                {
                    reason4Change = reason4Change + "Reason for change:" + crReason4Chn + "\n";
                }
                if (crDetails != null)
                {
                    reason4Change = reason4Change + "ECR Detail: " + crDetails + "\n";
                }
                
                reason4Change = reason4Change + "---------------------------------------" + "\n";
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        return reason4Change;
    }
    
    public TCComponent[] getCRList()
    {
        TCComponent[] comp = new TCComponent[addedCRsVec.size()];
        for (int i = 0; i < addedCRsVec.size(); i++)
            comp[i] = (TCComponent) addedCRsVec.elementAt(i);
        return comp;
    }
    
    public String getCRListString()
    {
        Object[] forms = addedCRsVec.toArray();
        String crList = "";
        for (int i = 0; i < forms.length; i++)
        {
            TCComponentForm crForm = (TCComponentForm) forms[i];
            
            try
            {
                String crName = crForm.getTCProperty("object_name").getStringValue();
                
                crList = crList + crName + "\n";
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        return crList;
    }
    
    public void fireUserServiceCall(String functionName, Object[] args)
    {
        try
        {
            Object retObj = usrService.call(functionName, args);
            if (retObj != null)
            {
                TCComponent[] crObjs = (TCComponent[]) retObj;
                for (int i = 0; i < crObjs.length; i++)
                {
                    TCComponentForm form = (TCComponentForm) crObjs[i];
                    java.util.Vector<Object> rowData = getRowData(form);
                    pendingCRtbl.addRow(rowData);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public class DateCellRenderer extends DefaultTableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (value instanceof Date)
            {
                String strDate = new SimpleDateFormat("dd/MM/yy").format((Date) value);
                this.setText(strDate);
            }
            return this;
        }
    }
    
    public class TextWrapRenderer extends JTextArea implements TableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        
        public TextWrapRenderer()
        {
            setLineWrap(true);
            setWrapStyleWord(true);
        }
        
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column)
        {
            if (isSelected)
            {
                setBackground(table.getSelectionBackground());
                setForeground(table.getForeground());
            }
            else
            {
                setBackground(table.getBackground());
                setForeground(table.getForeground());
            }
            setText(value.toString());
            setSize(table.getColumnModel().getColumn(column).getWidth(), getPreferredSize().height);
            
            if (table.getRowHeight(row) != getPreferredSize().height)
            {
                table.setRowHeight(row, getPreferredSize().height);
            }
            
            return this;
        }
    }
    
    public void setTypeofChange(String typeOfChg)
    {
        if (!typeOfChg.equals(typeofChange))
        {
            typeofChange = typeOfChg;
            pendingCRtbl.removeAllRows();
        }
        
    }
    
    public void update()
    {
        NOVCRItemSelectionPanel itemSelectionPanel = null;
        for (int i = 0; i < subList.size(); i++)
        {
            IObserver obrPanel = subList.get(i);
            if (obrPanel instanceof NOVCRItemSelectionPanel)
            {
                itemSelectionPanel = (NOVCRItemSelectionPanel) obrPanel;
                break;
            }
        }
        
        if (itemSelectionPanel != null)
        {
            Vector<TCComponent> targetCRs = itemSelectionPanel.targetTable.targetCRs;
            Vector<TCComponent> removedCRs = itemSelectionPanel.targetTable.removedCRs;
            
            for (int i = 0; i < targetCRs.size(); i++)
            {
                if (!addedCRsVec.contains(targetCRs.get(i)))
                {
                    setRowData((TCComponentForm) targetCRs.get(i));
                }
            }
            
            for (int i = 0; i < removedCRs.size(); i++)
            {
                TCComponent crform = removedCRs.get(i);
                addedCRsVec.remove(crform);
            }
        }
        
    }
    
    public void notifyObserver()
    {
        for (int i = 0; i < subList.size(); i++)
        {
            subList.get(i).update();
        }
    }
    
    public void registerObserver(IObserver subs)
    {
        subList.add(subs);
    }
    
    public void removeObserver(IObserver subs)
    {
        
    }
    
    public void componentHidden(ComponentEvent e)
    {
    }
    
    public void componentMoved(ComponentEvent e)
    {
    }
    
    public void componentShown(ComponentEvent e)
    {
    }
    
    public void valueChanged(ListSelectionEvent e)
    {
        String crName = null;
        if (e.getSource() == pendingCRtbl.getSelectionModel() && pendingCRtbl.getRowSelectionAllowed())
        {
            crName = (String) pendingCRtbl.getValueAt(pendingCRtbl.getSelectedRow(), 1);
            if (crName != null)
            {
                try
                {
                    TCComponentForm form = formCompMap.get(crName);
                    pendingCRDescPanel.displayCRDescription(form);
                }
                catch (TCException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        
    }
    
    public void componentResized(ComponentEvent arg0)
    {
        // TODO Auto-generated method stub
    }
    
    private void createECRTableSorter()
    {
        
        m_mPanelPendingCRTableSorter = new TableRowSorter<AIFTableModel>(pendingCRtblmodel)
        {
            Map<Integer, SortKey> m_keysMap = new HashMap<Integer, SortKey>();
            
            public void toggleSortOrder(int column)
            {
                SortKey key = m_keysMap.get(column);
                SortOrder order = null;
                // Get last sort order.
                if (key != null)
                {
                    if (key.getSortOrder() == SortOrder.DESCENDING)
                    {
                        order = SortOrder.ASCENDING;
                    }
                    else
                    {
                        order = SortOrder.DESCENDING;
                    }
                }
                else
                {
                    order = SortOrder.DESCENDING;
                }
                
                m_keysMap.put(column, new SortKey(column, order));
                List<SortKey> keys = new ArrayList<SortKey>();
                SortKey sortKey = new SortKey(column, order);
                keys.add(sortKey);
                
                this.setSortKeys(keys);
                pendingCRtbl.setRowSorter(this);
                this.sort();
                
                System.out.println("Running custom Sorting: Column " + column + order);
                
            }
        };
        
        try
        {
            List<SortKey> keys = new ArrayList<SortKey>();
            SortKey sortKey = new SortKey(pendingCRtblmodel.findColumn("CR"), SortOrder.ASCENDING);
            keys.add(sortKey);
            m_mPanelPendingCRTableSorter.setSortKeys(keys);
            pendingCRtbl.setRowSorter(m_mPanelPendingCRTableSorter);
            m_mPanelPendingCRTableSorter.setSortable(1, false);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
    }
    
    private void sortMPanelCRTable()
    {
        if (pendingCRtbl.getRowCount() >= 1 /*&& pendingCRtbl.getRowSorter() == null*/)
        {
            sortMPanelPendingCRTable();
        }
    }
    
    private void sortMPanelPendingCRTable()
    {
        /*pendingCRtbl.setRowSorter(m_mPanelPendingCRTableSorter);
        m_mPanelPendingCRTableSorter.setSortable(1, false);
        m_mPanelPendingCRTableSorter.sort();*/
    	
    	NOVECOHelper.sortTableRowsInAscendingOrder(pendingCRtbl, "CR");
    }
    
    private void removeMPanelPendingCRTableRows()
    {
        //pendingCRtbl.setRowSorter(null);
        pendingCRtblmodel.removeAllRows();
    }
    
    private void makeDialogActive()
    {
        Display.getDefault().asyncExec(new Runnable()
        {
            public void run()
            {
                dlg.getShell().forceActive();
            }
        });
    }
    
    private void searchECR(final Object []objs,final String userServiceName,final String message)
    {
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if(display != null)
        {
            display.syncExec(new Runnable() 
            {
                public void run()
                {
                    try
                    {
                        new ProgressMonitorDialog(display.getActiveShell()).run(true, true,
                                new NOVSearchECROperation(objs,NOVCRManagementPanel.this,userServiceName,message));
                    }
                    catch(InvocationTargetException e)
                    {
                        e.printStackTrace();
                    }
                    catch(InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    
    protected void invokeUserService (String userServiceName, Object [] objs,IProgressMonitor finalprogressMonitor )
    {
        
        Object retObj = null;
        try
        {
            retObj = usrService.call(userServiceName, objs);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        if (retObj != null)
        {
            removeMPanelPendingCRTableRows();// TCDECREL - 3382
            TCComponent[] crObjs = (TCComponent[]) retObj;
            for (int i = 0; i < crObjs.length; i++)
            {
                TCComponentForm form = (TCComponentForm) crObjs[i];
                Vector<Object> rowData = getRowData(form);
                try
                {
                    // if(form.getTCProperty("ecr_status").getStringValue().equalsIgnoreCase("Open")
                    // &&
                    // !(rowData.get(6).toString().trim().length()>0))
                    if (form.getTCProperty("ecr_status").getStringValue().equalsIgnoreCase("Open"))
                    {
                        pendingCRtbl.addRow(rowData);
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
            checkIsResultEmpty();
            sortMPanelCRTable();// TCDECREL - 3382
        }
    }
    
    private void showMessageForEmptyResult()
    {
        if (isResultEmpty())
        {
            MessageBox.post("No Open CR(s) found for the given criteria.", "Info",
                    MessageBox.INFORMATION);
            return;
        }
    }
    
    private void checkIsResultEmpty()
    {
        if (isResultEmpty())
        {
            return;
        }
        
    }
    
    private boolean isResultEmpty()
    {
        boolean isEmpty = false;
        if (pendingCRtbl.getRowCount() == 0)
        {
            isEmpty = true;
        }
        
        return isEmpty;
            
    }
}