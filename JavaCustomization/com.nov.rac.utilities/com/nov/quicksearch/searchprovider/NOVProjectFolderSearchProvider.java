package com.nov.quicksearch.searchprovider;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NOVProjectFolderSearchProvider implements INOVSearchProvider {

private String m_objectName;
	
	public NOVProjectFolderSearchProvider(String objectName)
	{
		super();
		m_objectName = objectName;
	}
	
	@Override
	public String getBusinessObject() 
	{
		return null;
	}

	@Override
	public QuickBindVariable[] getBindVariables() 
	{
		if(m_objectName == null || m_objectName.length() == 0)
		{
			m_objectName = "*";
		}
		
		QuickSearchService.QuickBindVariable[] bindVars = new QuickSearchService.QuickBindVariable[2];
        
        // Name of project folder
        bindVars[0] = new QuickSearchService.QuickBindVariable();
        bindVars[0].nVarType = POM_string;
        bindVars[0].nVarSize = 1;
        bindVars[0].strList = new String[]{m_objectName};
        
        // Type of object - Project
        bindVars[1] = new QuickSearchService.QuickBindVariable();
        bindVars[1].nVarType = POM_string;
        bindVars[1].nVarSize = 1;
        bindVars[1].strList = new String[]{"Project"};
        
        return bindVars;
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo() 
	{
		 QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];
	        
	        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
	        allHandlerInfo[0].handlerName = "NOVSRCH-get-wsobject";
	        allHandlerInfo[0].listBindIndex = new int[] {1,2};
	        allHandlerInfo[0].listReqdColumnIndex = new int[]{1,2}; 
	        allHandlerInfo[0].listInsertAtIndex = new int[]{1,2} ; 
	        
	        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
	        allHandlerInfo[1].handlerName = "NOVSRCH-get-release-status";
	        allHandlerInfo[1].listBindIndex = new int[0];
	        allHandlerInfo[1].listReqdColumnIndex = new int[]{2}; 
	        allHandlerInfo[1].listInsertAtIndex = new int[] {3};
	        
	        return allHandlerInfo;
	}
	
	public TCComponent[] execute(int initLoadAll) throws Exception
    {
        INOVSearchProvider searchService = this;
        INOVSearchResult searchResult = null;
        TCComponent[] projectFolders = new TCComponent[0];

        searchResult = NOVSearchExecuteHelperUtils.execute(searchService, initLoadAll);
        if(searchResult != null && searchResult.getResponse().nRows > 0)
        {
            INOVResultSet theResSet = searchResult.getResultSet();
            projectFolders = getNonInactiveProjects(theResSet);
            System.out.println();
        }
        
        return projectFolders;
        
    }

	TCComponent[] getNonInactiveProjects(INOVResultSet theResSet) 
	{
		Vector<String> rowData = theResSet.getRowData();
		Vector<String> objectUids = new Vector<String>();
		TCSession session = (TCSession) AIFUtility.getDefaultSession();
        String sReleaseStatus = null;        
        final int colCount = theResSet.getCols();
        TCComponent[] projectFolders = new TCComponent[0];
        
        for (int i = 0; i < theResSet.getRows(); i++)
        {
            sReleaseStatus = rowData.elementAt(colCount*(i + 1) - 1);
            if(sReleaseStatus != null && !sReleaseStatus.equalsIgnoreCase("INACTIVE") )
            {
            	objectUids.add(rowData.elementAt(colCount*i));          	
            }
        }
        try 
        {
			projectFolders = session.stringToComponent(objectUids.toArray(new String[objectUids.size()]));
		} 
        catch (TCException e) 
        {
			e.printStackTrace();
		}
        
        return projectFolders;
	}
	
}

