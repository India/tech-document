package com.nov.quicksearch.export;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.eclipse.swt.widgets.Display;

import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.util.MessageBox;

public class ExportTableData 
{
	private AIFTable m_Table;
	public static final int FORMAT_CSV = 1;
	public static final int FORMAT_EXCEL = 2;
	
	private String m_sHeader[] = null;
	private String m_sData[][] = null;
	private int m_iTableColWidth[] = null;
	
	private HSSFWorkbook m_workBook = null;
	
	public ExportTableData(AIFTable aTable)
	{
		m_Table = aTable;
		
		initialize(aTable);
	}
	
	public ExportTableData(String[] sHeader, String[][] sData)
	{
		m_sHeader = sHeader;
		m_sData = sData;
		
		int iHeaderCount = m_sHeader.length;
		m_iTableColWidth = new int[iHeaderCount];
		for(int iHeaderIndex = 0; iHeaderIndex<iHeaderCount;iHeaderIndex++)
		{
			m_iTableColWidth[iHeaderIndex] = m_sHeader[iHeaderIndex].length();
		}
	}
	private void initialize(AIFTable aTable)
	{
		//Get Table Header Names
		int iColCount = aTable.getColumnCount();
		int iRowCount = aTable.getRowCount();
		
		m_sHeader = new String[iColCount];
		m_iTableColWidth = new int[iColCount];
		for(int iColIndex=0; iColIndex<iColCount; iColIndex++)
		{
			m_sHeader[iColIndex] = aTable.getColumnName(iColIndex);
			m_iTableColWidth[iColIndex] = aTable.getColumnWidthInt(iColIndex);
		}
		
		//Get Table Data
		m_sData = new String[iRowCount][iColCount];
		for(int rowNum=0; rowNum<iRowCount; rowNum++)
		{
			for(int colNum=0; colNum<iColCount; colNum++)
			{
				Object colValue = aTable.getValueAt(rowNum, colNum);
				if(colValue != null)
				{
					m_sData[rowNum][colNum] = colValue.toString();
				}
				else
				{
					m_sData[rowNum][colNum] = "";
				}
			}
		}
	}
	
	public void exportData(File aFile, int aFormat) throws IOException
    {	
		//System.out.println("Exporting to file " + aFile.toString() + " started");
    	switch(aFormat)
    	{
    		case FORMAT_CSV: //Export to CSV
    			exportDataToCSV(m_Table, aFile);
    			break;
    			
    		case FORMAT_EXCEL: //Export to Excel
    			exportTableDataToExcel(m_Table, aFile);
    			break;    			
    	}
    	//System.out.println("Exporting to file " + aFile.toString() + " finished");
    }
    
	public void exportDataToCSV(AIFTable aTable, File aFile) throws IOException
	{
		exportDataToCSV(aFile);
	}	
	
	public void exportDataToCSV(File aFile) throws IOException
	{
    	FileWriter out = new FileWriter(aFile);
    	
		//Write Column names
    	int iHeaderLength = m_sHeader.length;
		for(int iColIndex=0; iColIndex<iHeaderLength; iColIndex++)
		{
			out.write(m_sHeader[iColIndex] + "\t");
		}
		out.write(System.getProperty("line.separator"));
		
		int iRowLength = m_sData.length;		
		for(int iRowIndex=0; iRowIndex<iRowLength; ++iRowIndex)
		{
			int iColLength = m_sData[iRowIndex].length;
			for(int iColIndex=1; iColIndex<iColLength; ++iColIndex)
			{
				out.write(m_sData[iRowIndex][iColIndex] + "\t");
			}
			out.write(System.getProperty("line.separator"));
		}	
		out.close();
	}
	
	public HSSFCellStyle cellStyle(HSSFWorkbook wb, String fontName, short fontSize, short fontBold, short foregroundColor, short fillPattern, short alignment, boolean wrapText)
	{
		HSSFCellStyle cs = wb.createCellStyle();
		HSSFFont font = wb.createFont();
		if(fontName != null)
			font.setFontName(fontName);
		
		if(fontSize != 0)
			font.setFontHeightInPoints(fontSize);
		
		if(fontBold != 0)
			font.setBoldweight(fontBold);
		
		cs.setFont(font);
		
		if(fillPattern != 0)
			cs.setFillPattern(fillPattern );
		if(foregroundColor != 0)
			cs.setFillForegroundColor(foregroundColor);
		
		if(alignment != 0)
			cs.setAlignment(alignment);
		
		cs.setWrapText(wrapText);
		
		return cs;
	}
	
	public void createHeader(HSSFSheet sheet)
	{
		//Create Header cell style
		HSSFCellStyle csHeader = cellStyle(sheet.getWorkbook(), null, (short) 0, HSSFFont.BOLDWEIGHT_BOLD, new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_CENTER, false);
		
		//Create Header Row

		//HSSFRow row = sheet.createRow(0); //Mohan : TCDECREL-1987

		int iHeaderRow = sheet.getPhysicalNumberOfRows(); //Mohan : TCDECREL-1987		
		HSSFRow row = sheet.createRow(iHeaderRow); //Mohan : TCDECREL-1987
				
		int iHeaderLength = m_sHeader.length;
		for(int iColIndex=0; iColIndex<iHeaderLength; iColIndex++)
		{
			HSSFCell cell = row.createCell(iColIndex);
			cell.setCellStyle(csHeader);
			cell.setCellValue(m_sHeader[iColIndex]);			
			sheet.setColumnWidth(iColIndex, m_iTableColWidth[iColIndex] * 500);
			
			setThinBorderStyle(cell,csHeader); //Mohan : TCDECREL-1987
		}
	}
		
	public void createBody(HSSFSheet sheet)
	{
		//Create Body cell styles
		HSSFCellStyle csBody = cellStyle(sheet.getWorkbook(), null, (short) 0, HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.WHITE().getIndex(), HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);
		HSSFCellStyle csBodyAlt = cellStyle(sheet.getWorkbook(), null, (short) 0, HSSFFont.BOLDWEIGHT_NORMAL, new HSSFColor.LIGHT_CORNFLOWER_BLUE().getIndex(), HSSFCellStyle.SOLID_FOREGROUND, HSSFCellStyle.ALIGN_LEFT, false);
		
		// populating from Table view: displayable result set is exported to excel
			
		int iRowToInsertData = sheet.getPhysicalNumberOfRows();  //Mohan : TCDECREL-1987
		
		int iRowLength = m_sData.length;
		for(int rowNum=0; rowNum<iRowLength; rowNum++)
		{
			HSSFRow row = sheet.createRow(iRowToInsertData + rowNum /*+ 1*/);
			HSSFCellStyle cs;
			if ((rowNum % 2) == 0)
			{
		        cs = csBody;
			}
			else
			{
				cs = csBodyAlt;
			}
			
			int iColLength = m_sData[rowNum].length;
			for(int colNum=0; colNum<iColLength; colNum++)
			{
				HSSFCell cell = row.createCell(colNum);
				cell.setCellStyle(cs);
				cell.setCellValue(m_sData[rowNum][colNum]);		
				
				setThinBorderStyle(cell,cs); //Mohan : TCDECREL-1987
			}
		}
	}

	//Mohan : TCDECREL-1987
	//Creates a border line to excel cell
	private void setThinBorderStyle(Cell cell, HSSFCellStyle style) 
	{ 
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN); 
		style.setBorderRight(HSSFCellStyle.BORDER_THIN); 
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cell.setCellStyle(style); 
	} 
	//Mohan : TCDECREL-1987
	
	public void exportTableDataToExcel(AIFTable aTable, File aFile) throws IOException
	{
		exportTableDataToExcel(aFile);
	}
	
	public void exportTableDataToExcel(File aFile) throws IOException
	{
		/* populating from Table view: displayable result set is exported to excel */		
		FileOutputStream out = new FileOutputStream(aFile);
    	
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();
		wb.setSheetName(0, "Search result");	
				
		createHeader(sheet);
		createBody(sheet);		
		
		wb.write(out);
		out.close();
		
		MessageBox.post(Display.getDefault().getActiveShell(),"Data successfully exported to " + aFile.toString(), "Information", MessageBox.INFORMATION);
	
	}	
	
	//Mohan : TCDECREL-1987
	public void exportTableDataToExcel(File aFile,HSSFWorkbook workBook) throws IOException
	{
		/* populating from Table view: displayable result set is exported to excel */		
		FileOutputStream out = new FileOutputStream(aFile);
		
		int iActiveSheetIndex = workBook.getActiveSheetIndex();
		HSSFSheet sheet = workBook.getSheetAt(iActiveSheetIndex);
						
		createHeader(sheet);
		createBody(sheet);		
		
		workBook.write(out);
		out.close();
		
		MessageBox.post("Data successfully exported to " + aFile.toString(), "Information", MessageBox.INFORMATION);
	
	}
	//Mohan : TCDECREL-1987
}
