package com.nov.quicksearch.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.util.MessageBox;

public class NOVExportSearchResultUtils 
{	
	public static void exportSearchResultToExcel(AIFTable theTable)
	{
		//Display disp = new Display();  //8680
	    Display disp = PlatformUI.getWorkbench().getDisplay();
		FileDialog fd = new FileDialog(new Shell(disp), SWT.SAVE);
        fd.setText("Save");
        fd.setFilterPath(System.getProperty("user.home"));
        String[] filterExt = { "*.xls" };
        fd.setFilterExtensions(filterExt);
        String fileName = fd.open();
        
        if( fileName != null && !fileName.isEmpty() )
        {
        	java.io.File file = new File(fileName);
        	try 
        	{	
        		ExportTableData exp = new ExportTableData(theTable);
        		exp.exportData( file, ExportTableData.FORMAT_EXCEL );			        		
			} 
        	catch (FileNotFoundException e)
            {
        	    MessageBox.post("The file " + file.toString()+" is probably open in another window. " +
                        "Please close the file and try again or save it to a new file. ", 
                         "Information", MessageBox.INFORMATION);
            } 
        	catch (IOException e) 
			{
			    
				e.printStackTrace();
			}
        }
	}
	
	public static void exportSearchResultToExcel( Composite mainComposite, AIFTable theTable )
	{
		FileDialog fd = new FileDialog( mainComposite.getShell(), SWT.SAVE );
        fd.setText("Save");
        fd.setFilterPath(System.getProperty("user.home"));
        String[] filterExt = { "*.xls" };
        fd.setFilterExtensions(filterExt);
        String fileName = fd.open();
        
        if( fileName != null && !fileName.isEmpty())
        {
        	java.io.File file = new File(fileName);
        	try 
        	{	
        		ExportTableData exp = new ExportTableData( theTable );
        		exp.exportData( file, ExportTableData.FORMAT_EXCEL );			        		
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
        }
	}
	
	//Mohan : TCDECREL-1987
	
	public static void exportData(AIFTable table ,String [] sHeader, String sFileName)
	{
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();
		HSSFCellStyle cs = wb.createCellStyle();
		
		wb.setSheetName(wb.getActiveSheetIndex(), "Print Template");
		
		int iTotalCols = table.getColumnCount();
		
		int iHeaderLength = sHeader.length + 1 ;
		for(int rowIndex=0;rowIndex<iHeaderLength;rowIndex++)
		{
			HSSFRow row = sheet.createRow(rowIndex);
			
			for(int colIndex=0;colIndex<iTotalCols;colIndex++)
			{
				HSSFCell cell = row.createCell(colIndex);
				setThinBorderStyle(cell,cs);
			}			
			sheet.addMergedRegion(new CellRangeAddress(rowIndex,rowIndex,0,iTotalCols-1));
		}
		
		HSSFRow row = sheet.getRow(0);
		HSSFCell cell = row.getCell(0);
		cell.setCellValue(sFileName);
		
		exportSearchResultToExcel(table, wb,sFileName);
	}
	
	public static void setThinBorderStyle(Cell cell, HSSFCellStyle style) 
	{ 
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN); 
		style.setBorderRight(HSSFCellStyle.BORDER_THIN); 
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cell.setCellStyle(style); 
	}
	
	public static void exportSearchResultToExcel(final AIFTable theTable, final HSSFWorkbook wb, final String sFileName )
	{
		final Display disp = Display.getCurrent();
		disp.syncExec(new Runnable() 
		{
			@Override
			public void run()
			{
				Shell shell = new Shell(disp);
				FileDialog fd = new FileDialog(shell , SWT.SAVE|SWT.APPLICATION_MODAL|SWT.CENTER);
				fd.setText("Save");
				fd.setFilterPath(System.getProperty("user.home"));
				String[] filterExt = { "*.xls" };
				fd.setFilterExtensions(filterExt);
				fd.setFileName(sFileName);
				String fileName = fd.open();

				if( fileName != null && !fileName.isEmpty())
				{
					java.io.File file = new File(fileName);
					try 
					{	
						ExportTableData exp = new ExportTableData( theTable );	
						exp.exportTableDataToExcel(file,wb);			        		
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					}
				}			

			}
		});
	}
	//Mohan : TCDECREL-1987

}
