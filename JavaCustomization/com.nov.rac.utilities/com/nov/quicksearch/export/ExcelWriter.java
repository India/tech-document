package com.nov.quicksearch.export;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddress;

public class ExcelWriter
{
    final static int REPORT_COLUMN_STRING = 1;
    final static int REPORT_COLUMN_DATE = 2;
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function createHeader()
     * @purpose Writes the header row to the excel sheet
     * @input HSSFSheet sheet - Sheet to write into HSSFCellStyle cellStyle -
     *        Style/Format to write String[] headerTitle - Header Row Titles
     * @output None
     */
    
    public static void createHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle)
    {
        int[] headerColWidth = getColumnWidth(headerTitle);
        createHeader(sheet, cellStyle, headerTitle, headerColWidth);
    }
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function createHeader()
     * @purpose Writes the header row to the excel sheet with specified column
     *          width
     * @input HSSFSheet sheet - Sheet to write into HSSFCellStyle cellStyle -
     *        Style/Format to write String[] headerTitle - Header Row Titles
     *        int[] colWidth - Column Width
     * @output None
     */
    
    public static void createHeader(HSSFSheet sheet, HSSFCellStyle cellStyle, String[] headerTitle, int[] colWidth)
    {
        int iHeaderRow = sheet.getPhysicalNumberOfRows();
        HSSFRow row = sheet.createRow(iHeaderRow);
        int iHeaderLength = headerTitle.length;
        for (int iColIndex = 0; iColIndex < iHeaderLength; iColIndex++)
        {
            HSSFCell cell = row.createCell(iColIndex);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(headerTitle[iColIndex]);
            sheet.setColumnWidth(iColIndex, colWidth[iColIndex]);
            setThinBorderStyle(cell, cellStyle);
        }
    }
    
        
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function createBody()
     * @purpose Writes the Matrix data to the excel sheet
     * @input HSSFSheet sheet - Sheet to write into HSSFCellStyle cellStyle -
     *        Style/Format to write HSSFCellStyle cellAltStyle - Style/Format
     *        for alternate rows of the body String[][] data - Matrix data to
     *        write to excel
     * @output None
     */
    
    public static void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle, String[][] data)
    {
        int iRowToInsertData = sheet.getPhysicalNumberOfRows();
        
        int iRowLength = data.length;
        for (int rowNum = 0; rowNum < iRowLength; rowNum++)
        {
            HSSFRow row = sheet.createRow(iRowToInsertData + rowNum);
            HSSFCellStyle cs;
            if ((rowNum % 2) == 0)
            {
                cs = cellStyle;
            }
            else
            {
                cs = cellAltStyle;
            }
            
            int iColLength = data[rowNum].length;
            for (int colNum = 0; colNum < iColLength; colNum++)
            {
                HSSFCell cell = row.createCell(colNum);
                cell.setCellStyle(cs);
                cell.setCellValue(data[rowNum][colNum]);
                
                setThinBorderStyle(cell, cs);
            }
        }
    }
    
    public static void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle,
            String[][] data, int[] colTypes)
    {
        int nStartRow = 0;
        int nEndRow = data.length;
        int nStartColumn = 0;
        int nEndColumn = data[0].length;
        
        createBody(sheet, cellStyle, cellAltStyle, data, nStartRow, nEndRow, nStartColumn, nEndColumn, colTypes);
    }
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function createBody()
     * @purpose Writes the Matrix data to the excel sheet with column formatting
     * @input HSSFSheet sheet - Sheet to write into HSSFCellStyle cellStyle -
     *        Style/Format to write HSSFCellStyle cellAltStyle - Style/Format
     *        for alternate rows of the body String[][] data - Matrix data to
     *        write to excel
     * @output None
     */
    
    public static void createBody(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle,
            String[][] data, int nStartRow, int nEndRow, int nStartColumn, int nEndColumn, int[] colTypes)
    {
        HSSFCellStyle[][] cellStyles = getFormatters(sheet, cellStyle, cellAltStyle);
        DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        
        int iRowToInsertData = sheet.getPhysicalNumberOfRows();
        for (int rowNum = 0; rowNum < nEndRow; rowNum++)
        {
            HSSFRow row = sheet.createRow(iRowToInsertData + rowNum);
            
            for (int colNum = nStartColumn; colNum < nEndColumn; colNum++)
            {
                HSSFCell cell = row.createCell(colNum);
                HSSFCellStyle cs = cellStyles[rowNum % 2][colTypes[colNum] - 1];
                
                try
                {
                    if (!data[nStartRow][colNum].isEmpty())
                    {
                        switch (colTypes[colNum])
                        {
                            case REPORT_COLUMN_STRING:
                                cell.setCellValue(data[nStartRow][colNum]);
                                cell.setCellStyle(cs);
                                break;
                            
                            case REPORT_COLUMN_DATE:
                                Date dt = (Date) dateFormatter.parse(data[nStartRow][colNum]);
                                cell.setCellValue(dt);
                                cell.setCellStyle(cs);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                
                setThinBorderStyle(cell, cs);                
            }
            nStartRow++;
        }
    }
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function cellStyle()
     * @purpose Get Style/Format for the cells
     * @input HSSFWorkbook wb - Workbook to write into String fontName - Font
     *        Name short fontSize - Font size short fontBold - Font weight short
     *        foregroundColor - Foreground color short fillPattern - Fill
     *        Pattern short alignment - Horizontal Alignment boolean wrapText -
     *        Wrap text
     * @output HSSFCellStyle cellStyle - CellStyle
     */
    
    public static HSSFCellStyle cellStyle(HSSFWorkbook wb, String fontName, short fontSize, short fontBold,
            short foregroundColor, short fillPattern, short alignment, boolean wrapText)
    {
        HSSFCellStyle cs = wb.createCellStyle();
        HSSFFont font = wb.createFont();
        if (fontName != null)
            font.setFontName(fontName);
        
        if (fontSize != 0)
            font.setFontHeightInPoints(fontSize);
        
        if (fontBold != 0)
            font.setBoldweight(fontBold);
        
        cs.setFont(font);
        
        if (fillPattern != 0)
            cs.setFillPattern(fillPattern);
        if (foregroundColor != 0)
            cs.setFillForegroundColor(foregroundColor);
        
        if (alignment != 0)
            cs.setAlignment(alignment);
        
        cs.setWrapText(wrapText);
        
        return cs;
    }
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function setThinBorderStyle()
     * @purpose Sets the Border Style of the cell
     * @input Cell cell - Cell to write into HSSFCellStyle style - Cell
     *        Style/Format
     * @output None
     */
    
    public static void setThinBorderStyle(Cell cell, HSSFCellStyle style)
    {
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cell.setCellStyle(style);
    }
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function getColumnWidth()
     * @purpose Returns the Column widths based on the Header Titles
     * @input String[] headerTitle - Header Titles
     * @output int[] colWidth - Column Widths
     */
    
    private static int[] getColumnWidth(String[] headerTitle)
    {
        int colCount = headerTitle.length;
        int[] colWidth = new int[colCount];
        for (int iHeaderIndex = 0; iHeaderIndex < colCount; iHeaderIndex++)
        {
            colWidth[iHeaderIndex] = headerTitle[iHeaderIndex].length() * 500;
        }
        
        return colWidth;
    }
    
    /**
     * @author rakeshmk
     * @date 09.10.2012
     * @function getFormatters()
     * @purpose Get String/Date formatters for the given columns cell styles
     * @input HSSFSheet sheet - Sheet to write into HSSFCellStyle cellStyle -
     *        Style/Format to write HSSFCellStyle cellAltStyle - Style/Format
     *        for alternate rows of the body
     * @output HSSFCellStyle[][]
     */
    
    public static HSSFCellStyle[][] getFormatters(HSSFSheet sheet, HSSFCellStyle cellStyle, HSSFCellStyle cellAltStyle)
    {
        HSSFCellStyle cs;
        HSSFCellStyle csAlt;
        DataFormat format = sheet.getWorkbook().createDataFormat();
        
        HSSFCellStyle[][] cellStyles = new HSSFCellStyle[2][2];
        
        // String Format
        cs = cellStyle;
        csAlt = cellAltStyle;
        cellStyles[0][0] = cs;
        cellStyles[1][0] = csAlt;
        
        // DATE Format
        cs = cellStyle;
        cs.setDataFormat(format.getFormat("dd-mmm-yyyy"));
        csAlt = cellAltStyle;
        csAlt.setDataFormat(format.getFormat("dd-mmm-yyyy"));
        cellStyles[0][1] = cs;
        cellStyles[1][1] = csAlt;
        
        return cellStyles;
    }
}
