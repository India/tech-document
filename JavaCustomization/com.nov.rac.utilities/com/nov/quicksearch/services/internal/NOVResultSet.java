package com.nov.quicksearch.services.internal;

import java.util.Collections;
import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;

public class NOVResultSet implements INOVResultSet
{
	private Vector<String> m_RowData;
	private int            m_nRows;
	private int            m_nCols;
	
	public NOVResultSet( )
	{
		m_RowData = new Vector<String>();	
		m_nRows = 0;
		m_nCols = 0;
	}
	
	public NOVResultSet( String strRows[], int nRows, int nCols)
	{
		// Add strRows to m_RowData.
		m_RowData = new Vector<String>();
		
		Collections.addAll(m_RowData, strRows);
		
		m_nRows = nRows;
		m_nCols = nCols;
	}
	public int getCols()
	{
		return m_nCols;
	}
	
	public int getRows()
	{
		return m_nRows;
	}
	
	public Vector<String> getRowData()
	{
		return m_RowData;
	}

	public void add(String strRows[], int nRows, int nCols)
	{
		Collections.addAll(m_RowData, strRows);
		m_nRows += nRows;
		m_nCols = nCols;
	}
	
	public void add(String strRows[], int nRows)
	{
		Collections.addAll(m_RowData, strRows);
		m_nRows += nRows;
	}
	
	public void clear()
	{
		m_nRows = 0;
		m_nCols = 0;
		m_RowData.clear();
	}

}

