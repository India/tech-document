package com.nov.quicksearch.services.internal;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchResult;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickSearchResponse;

public class NOVSearchDefaultSearchResult implements INOVSearchResult {

	private QuickSearchService.QuickSearchResponse m_response;
	private INOVResultSet m_resultset;
	
	@Override
	public QuickSearchResponse getResponse() 
	{
		return m_response;
	}

	@Override
	public INOVResultSet getResultSet() 
	{
		return m_resultset;
	}

	@Override
	public void setResponse(QuickSearchService.QuickSearchResponse response) 
	{
		m_response = response;
	}
	
	public void setResultSet(INOVResultSet resultSet) 
	{
		m_resultset = resultSet;
	}

}
