package com.nov.quicksearch.services;

import java.util.Vector;

public interface INOVResultSet 
{
	public int getCols();
	
	public int getRows();
	
	public Vector<String> getRowData();

	public void add(String strRows[], int nRows, int nCols);
	
	public void add(String strRows[], int nRows);
	
	public void clear();
}
