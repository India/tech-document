package com.nov.quicksearch.services;

import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;

public interface INOVSearchResult 
{
	public void setResponse(QuickSearchService.QuickSearchResponse response);
	
	public QuickSearchService.QuickSearchResponse getResponse();
	
	public void setResultSet( INOVResultSet resultSet);
	
	public INOVResultSet getResultSet();
}