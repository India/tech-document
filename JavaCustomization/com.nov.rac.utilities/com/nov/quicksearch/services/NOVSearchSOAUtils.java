package com.nov.quicksearch.services;

import java.text.DateFormat;
import java.util.Date;

import com.novquicksearch.services.rac.quicksearch.QuickSearchServiceService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;

public class NOVSearchSOAUtils 
{
	public static QuickSearchService.QuickSearchResponse performSOASearch2( INOVSearchProvider searchService,
			                                                                int nLoadAll) throws TCException
	{			
		QuickSearchService.QuickSearchResponse theRespo = null;
		
		TCSession session = (TCSession)AIFUtility.getActiveDesktop().getCurrentApplication().getSession();	
		
		QuickSearchServiceService  service = QuickSearchServiceService.getService(session);
		
		QuickSearchService.QuickSearchInput input = new QuickSearchService.QuickSearchInput();
		
		input.allBindVars = searchService.getBindVariables();
		
		input.allHandlerInfo = searchService.getHandlerInfo();
		
		input.theBusinessObjectType = searchService.getBusinessObject();
		
		input.nLoadAll = nLoadAll;			
			
	    Date now1 = new Date ( );
	    System.out.println ("before query :"+
	    		DateFormat.getDateTimeInstance ( DateFormat.LONG, DateFormat.LONG ).format(now1) );
		
		theRespo = service.novQuickSearch( input );				
			
		if( theRespo.serviceData.sizeOfPartialErrors() > 0 )
		{
			ErrorStack errorStack = theRespo.serviceData.getPartialError(0);
			
			TCException tcExp = new TCException(errorStack.getLevels(), errorStack.getCodes(), errorStack.getMessages());
			
			throw tcExp;
		}
		
	    Date now2 = new Date ( );
	    System.out.println ("after query :"+
	    		DateFormat.getDateTimeInstance ( DateFormat.LONG, DateFormat.LONG ).format(now2) );		
			
		return theRespo;
	}
}
