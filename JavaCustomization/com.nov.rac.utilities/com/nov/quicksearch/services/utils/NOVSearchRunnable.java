package com.nov.quicksearch.services.utils;

/* *************************************************
 * This class is a wrapper around java.lang.Runnable
 * interface.
 *  
 * The Runnable.run() method can not throw any exception
 * so the wrapper class would store the exception and 
 * pass the exception to other thread.
 ************************************************* */

public class NOVSearchRunnable implements Runnable 
{
    public String str = null;
    public int    nInt = 0;
    public Exception theException = null;
    
    public NOVSearchRunnable()
    {
    	super();
    	theException = null;
    }
    
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
	}

}
