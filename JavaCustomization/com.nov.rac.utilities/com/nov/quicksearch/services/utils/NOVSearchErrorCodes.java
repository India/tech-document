package com.nov.quicksearch.services.utils;

public enum NOVSearchErrorCodes 
{ 
	
  // START -- Define all error code here.
	
  TOO_MANY_OBJECTS_FOUND  (919601);
  
  // END -- Define all error code here.

  private int errorCode;
  
  NOVSearchErrorCodes(int theErrorCode)
  {
	  errorCode = theErrorCode;
  }
  
  public int getErrorCode()
  {
	  return errorCode;
  }
  
}

