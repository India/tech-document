package com.nov.quicksearch.services.utils;

import java.util.Vector;

import com.nov.quicksearch.services.INOVSearchResult;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentGroup;
import com.teamcenter.rac.kernel.TCComponentGroupType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class Utils 
{
	
	/* *************************************** */
	/* Get PUID of current logged in group     */
	/* *************************************** */
	public static String getCurrentGroupPUID()
	{
		String currGroupPUID = "";
		
		TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
		TCComponentGroup theGroup = theSession.getCurrentGroup();
		
		currGroupPUID = theGroup.getUid();	

		return currGroupPUID;
		
	}
	
	public static String getGroupPUID(TCComponentGroup grpName)
	{
		String groupPUID = "";
		groupPUID = grpName.getUid();
		
		
		return groupPUID;
		
	}
	
	public static String getGroupPUID(String strGroupFullName) 
	{
		String groupPUID = null;
		
		if(strGroupFullName != null)
		{
			TCSession session =(TCSession) AIFUtility.getDefaultSession();	
			
			try 
			{
				TCComponentGroupType groupTypeComponent;
				groupTypeComponent = (TCComponentGroupType)session.getTypeComponent("Group");
				TCComponentGroup theGroupComponent = groupTypeComponent.find(strGroupFullName);
				
				groupPUID = theGroupComponent.getUid();
			} 
			catch (TCException e) 
			{
				return null;				
			}
		}
				
		return groupPUID;
	}
	
	/**
	 * @author 		rakeshmk
	 * @date		09.10.2012
	 * @purpose 	Convert the QuickSearch result set into 2-D String array
	 * @input		
	 * 				INOVSearchResult searchResult 		- Search result
	 * @output		
	 * 				String[][]
	 */
	
	public static String[][] getSearchResultMatrix(INOVSearchResult searchResult) throws TCException 
	{
		int rowCount = searchResult.getResultSet().getRows();
		int colCount = searchResult.getResultSet().getCols();
		Vector<String> rowData = searchResult.getResultSet().getRowData();
		
		String[][] strColValues = new String[rowCount][colCount-1];
		
		for(int i = 0; i<rowCount; i++)
		{
			for(int j = 1; j<colCount; j++)
			{
				strColValues[i][j-1] = rowData.get(i*colCount + j);
			}
		}
		
		return strColValues;
	}

}
