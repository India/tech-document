package com.nov.quicksearch.services;

import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;

public interface INOVSearchProvider
{
	// Available Data types for Bind Variables.
	public static final int POM_date = 2;
	public static final int POM_double = 3;
	public static final int POM_int = 4;
	public static final int POM_logical = 6;	
	public static final int POM_string = 8;
	public static final int POM_typed_reference = 9;
	
	public static final int LOAD_ALL = -2;
	public static final int INIT_LOAD_ALL = -1;
	
		
	// return item type
	public String getBusinessObject();
	
	// return input values, return * for null values
	public QuickSearchService.QuickBindVariable[] getBindVariables();
	
	// return handler names
	public QuickSearchService.QuickHandlerInfo[] getHandlerInfo();		
}
