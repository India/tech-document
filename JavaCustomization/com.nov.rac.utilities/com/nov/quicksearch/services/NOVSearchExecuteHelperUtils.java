package com.nov.quicksearch.services;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.util.Date;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.nov.quicksearch.services.internal.NOVResultSet;
import com.nov.quicksearch.services.internal.NOVSearchDefaultSearchResult;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.teamcenter.rac.kernel.TCException;

public class NOVSearchExecuteHelperUtils
{
/******************************************************************************
*
******************************************************************************/
	public static INOVSearchResult execute(INOVSearchProvider searchService, int nLoadAll, Shell theShell) throws Exception
	{
		//Shell activeShell = AIFUtility.getPortal().getDisplay().getActiveShell();
		NOVSearchExecuteOperaion executeSearch = new NOVSearchExecuteOperaion(searchService, true, nLoadAll);
		
		// execute Search
		try
		{			
			new ProgressMonitorDialog(theShell).run(true, true, executeSearch);			
		}
		catch(InvocationTargetException invTarExp)
		{
             throw (Exception)(invTarExp.getCause());	
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return executeSearch.getSearchResult();  
	}
	
	public static INOVSearchResult execute(INOVSearchProvider searchService, int nLoadAll) throws Exception
	{
		final NOVSearchExecuteOperaion executeSearch = new NOVSearchExecuteOperaion(searchService, true, nLoadAll);
		
		final Display display = PlatformUI.getWorkbench().getDisplay();
		if(display != null)
		{
			display.syncExec((new Runnable(){			
				@Override
				public void run() {
					try 
					{
						new ProgressMonitorDialog(display.getActiveShell()).run(true, true, executeSearch);
					} catch (InvocationTargetException e) 
					{
						e.printStackTrace();
					} catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
					
				}}));
		}		
		return executeSearch.getSearchResult();   
		
		//return NOVSearchExecuteHelperUtils.searchSOACallServer(searchService, nLoadAll);	
	}

/******************************************************************************
*
******************************************************************************/
	public static void searchSOACallServer(INOVSearchProvider searchService) throws Exception
	{
		searchSOACallServer(searchService, 0);
	}

/******************************************************************************
*
******************************************************************************/
	public static INOVSearchResult searchSOACallServer(INOVSearchProvider searchService, int nLoadAll) throws TCException
	{
		NOVSearchDefaultSearchResult searchResult = new NOVSearchDefaultSearchResult();
		NOVResultSet resultSet = new NOVResultSet();
		QuickSearchService.QuickSearchResponse response;
	    Date now1 = new Date ( );
	    System.out.println ("before Server call:"+
	    		DateFormat.getDateTimeInstance ( DateFormat.LONG, DateFormat.LONG ).format(now1) );		
		
	    response = NOVSearchSOAUtils.performSOASearch2( searchService, nLoadAll);
		
	    resultSet.add( response.rows, 
	    		response.nRows, 
	    		response.nCols);

		Date now2 = new Date ( );
	    System.out.println ("after Server call: "+
	    		DateFormat.getDateTimeInstance ( DateFormat.LONG, DateFormat.LONG ).format(now2) );
	    
	    searchResult.setResponse(response);
	    searchResult.setResultSet(resultSet);
	    
	    return searchResult;
	}
/******************************************************************************
*
******************************************************************************/	
}
