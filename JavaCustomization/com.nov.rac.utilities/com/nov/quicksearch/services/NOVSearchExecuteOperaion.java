package com.nov.quicksearch.services;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.nov.quicksearch.services.utils.NOVSearchErrorCodes;
import com.nov.quicksearch.services.utils.NOVSearchRunnable;
import com.teamcenter.rac.kernel.TCException;

/******************************************************************************
* class SearchExecuteOperaion
******************************************************************************/
public class NOVSearchExecuteOperaion implements IRunnableWithProgress
{
	private boolean indeterminate;
	private int m_nLoadAll = 0;
	private INOVSearchProvider m_searchService;
	private INOVSearchResult m_searchResult;
	
	public NOVSearchExecuteOperaion(INOVSearchProvider searchService, boolean indeterminate, int nLoadAll)
	{
		this.indeterminate = indeterminate;
		m_nLoadAll = nLoadAll;
		m_searchService = searchService;
		m_searchResult = null;
	}
		
	public INOVSearchResult getSearchResult()
	{
		return m_searchResult;
	}
	
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
	{
		monitor.beginTask("Executing NOV Search", indeterminate ? IProgressMonitor.UNKNOWN : 10000);
				
		final IProgressMonitor finalMonitor = monitor;
		
		NOVSearchRunnable theRunnable = new NOVSearchRunnable() 
		{			
			@Override
			public void run()
			{			
				try
				{
					m_searchResult = NOVSearchExecuteHelperUtils.searchSOACallServer(m_searchService, m_nLoadAll);	
				}
				catch( TCException Exp)
				{					
					if(Exp.getErrorCode() == NOVSearchErrorCodes.TOO_MANY_OBJECTS_FOUND.getErrorCode() )
					{				
						  try 
						  {
							  m_searchResult = NOVSearchExecuteHelperUtils.execute(m_searchService, -2);
						  } 
						  catch (Exception e1) 
						  {
						     e1.printStackTrace();
						  }				
					}
					else
					{					
						Exp.printStackTrace();
						m_searchResult = null;
						this.str = Exp.getMessage();
						this.nInt = Exp.getErrorCode();	
						this.theException = Exp;
						finalMonitor.setCanceled(true);
						finalMonitor.done();
					}
				}				
					
			} 
		};

		new Thread(theRunnable).start();
		
		while(!monitor.isCanceled() && m_searchResult == null)
		{				
		}

	    monitor.done();
	    
	    if(theRunnable.nInt > 0 && theRunnable.str != null)
	    {
	    	throw (new InvocationTargetException( theRunnable.theException));
	    }
	    	    
	    Thread.sleep(50);
	    
	    if ( !monitor.isCanceled() )
	    {
	    	if( m_searchResult != null && m_searchResult.getResponse() != null && m_searchResult.getResponse().nRows > 0 )
	    	{
	    		//ExecuteHelperUtils.searchSOAUpdateClient(m_searchService,m_nLoadAll);
	    	}
	    }
	    else
	    {
	    	//m_searchService.m_bSearchCancelledByUsr = true;
	    }
	}
}
