package com.nov.quicksearch.table;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;

import com.teamcenter.rac.common.table.AbstractTCTableCellRenderer;
import com.teamcenter.rac.kernel.TCComponent;

public abstract class AbstractNOVTableCellRenderer extends AbstractTCTableCellRenderer 
{
	private static final long serialVersionUID = 1L;

	public AbstractNOVTableCellRenderer()
	{
		
	}
	
	@Override
	protected Icon getDisplayIcon(TCComponent tccomponent, Object obj) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void initiateIcons() {
		// TODO Auto-generated method stub

	}
	
	protected abstract String getCellValue(JTable jtable,Object obj , int iRow, int iCol); 
	
	
	 public Component getTableCellRendererComponent(JTable jtable, Object obj, boolean flag, boolean flag1, int i, int j)
	    {
	        String rowType = null;
	        if(jtable instanceof NOVSearchTable)
	        {
	        	
	        	//rowType = (String)((NOVSearchTable)jtable).getRowType(i);
	        	rowType = getCellValue(jtable,obj,i,j);
	        }
	        if(flag)
	        {
	            super.setForeground(jtable.getSelectionForeground());
	            super.setBackground(jtable.getSelectionBackground());
	        } else
	        {
	            super.setForeground(jtable.getForeground());
	            super.setBackground(i % 2 != 1 ? jtable.getBackground() : alternateBackground);
	        }
	        if(flag1)
	        {
	            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
	            if(jtable.isCellEditable(i, j))
	            {
	                super.setForeground(UIManager.getColor("Table.focusCellForeground"));
	                super.setBackground(UIManager.getColor("Table.focusCellBackground"));
	            }
	        } else
	        {
	            setBorder(noFocusBorder);
	        }
	        Icon icon = getDisplayIcon(rowType, obj);
	        String s = getDisplayText(obj);
//	        if (s == null)
//	        {
//	        	s = rowType;
//	        }
	        setText(s != null ? s : "");
	        setIcon(icon);
	        int k = getFontMetrics(getFont()).stringWidth(getText());
	        int l = jtable.getCellRect(i, j, false).width;
	        if(k + (icon != null ? icon.getIconWidth() : 0) > l - 8)
	            setToolTipText(s);
	        else
	            setToolTipText(null);
	       
	        return this;
	    }
	 
	 protected abstract Icon getDisplayIcon(String rowType, Object obj);
	 //protected abstract Icon getDisplayIcon(String releaseType);


}
