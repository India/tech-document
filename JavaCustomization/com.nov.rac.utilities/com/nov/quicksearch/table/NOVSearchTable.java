package com.nov.quicksearch.table;

import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCTextService;
import com.teamcenter.rac.util.MessageBox;

public class NOVSearchTable extends TCTable 
{
	private static final long serialVersionUID = 1L;

	public NOVSearchTable()
    {
        super();
        setModel(new NOVSearchTableModel());
    }
	
	public NOVSearchTable(String as[], String as1[])
	{
	    super( as,as1);
	    setModel(new NOVSearchTableModel(as));
	}
	 
	public NOVSearchTable(String as[])
	{
	   super(as);
	   setModel(new NOVSearchTableModel(as));
		  
	   //super.setModel(new NOVSearchTableModel(as));
       //addMouseListenerToHeaderInTable();
	   assignColumnRenderer();
	}	
	
	public NOVSearchTable(TCSession tcsession,String as[])
	{
	   super(tcsession,as);
	   
	   if(tcsession != null)
       {
           String as1[][] = getDisplayableHeaderValue(tcsession, as, null);
           dataModel = new NOVSearchTableModel(as1);
       } else
       {
           dataModel = new NOVSearchTableModel(as);
       }
	   
	   setModel(dataModel);
		  
	   //super.setModel(new NOVSearchTableModel(as));
       //addMouseListenerToHeaderInTable();
	   assignColumnRenderer();
	}	

//TODO :NITIN Remove this	
//	public String getRowType(int i)
//	{
//		String itemType = "Item";
//		
//		Object[] rowContent = getRowData(i);
//		String temp = (String)rowContent[4];
//		if(temp != null && !temp.isEmpty())
//			itemType = temp;
//		
//		return itemType;
//		//return "Item";
//		
//	}
//
//	public String getRevRelStatus(int i)
//	{
//		String revRelStatus = "Engineering";
//		
//		Object[] rowContent = getRowData(i);
//		String temp = (String)rowContent[11];
//		if(temp != null && !temp.isEmpty())
//			revRelStatus = temp;
//		
//		return revRelStatus;
//		//return "Item";
//		
//	}
	
	public String getValueAtDataModel(int iRow, int iCol)
	{
		String retValue = null;
		
		retValue = (String ) (getModel().getValueAt(convertRowIndexToModel(iRow), iCol));
		
		return retValue;
	}
	
	public int getColumnIndexFromDataModel( String theColName )
	{
	    int nColIndex = -1;
	    
	    int nColCount = dataModel.getColumnCount();
	    
	    for( int inx = 0; inx< nColCount; inx++ )
	    {
	        if( theColName.equals(dataModel.getColumnIdentifier(inx)) )
	        {
	        	nColIndex = inx;
	        	break;
	        }
	    }
	    
	    return nColIndex;
	}
	
	protected String[][] getDisplayableHeaderValue(TCSession tcsession, String as[], String s)
    {
        if(as == null)
            return (String[][])null;
        
        //Rakesh K - Read Column Display preference values
        //Code starts here
        String as_display_names[] = null;
		if(s != null )
		{
			//TCPreferenceService prefServ = ((TCSession)session).getPreferenceService();
			TCPreferenceService prefServ = getSession().getPreferenceService();//TC10.1 Upgrade
			String as_display_pref = "NOV4_" + s + "_ColumnDisplayPreferences";
			as_display_names = prefServ.getStringArray(TCPreferenceService.TC_preference_site, as_display_pref);
		}
		
		//Code ends here
		
        int i = as.length;
        for(int j = 0; j < i; j++)
        {
            if(s != null)
                as[j] = (new StringBuilder()).append(s).append(".").append(as[j]).toString();         
        }

        String as1[] = new String[i];
        //boolean flag = false;
        for(int l = 0; l < i; l++)
        {
            int k = as[l].indexOf(".");
            as1[l] = as[l].substring(k + 1);
        }

        String as2[] = null;
        if(as_display_names == null )
        {
	        try
	        {
	            TCTextService tctextservice = tcsession.getTextService();
	            as2 = tctextservice.getTextValues(as);
	        }
	        catch(TCException tcexception)
	        {
	            //logger.error(tcexception.getClass().getName(), tcexception);
	            as2 = as1;
	            return (String[][])null;
	        }
        }
        else
        {
        	as2 = as_display_names;
        }
        String as3[][] = new String[i][3];
        for(int i1 = 0; i1 < i; i1++)
        {
        	if(as2[i1] == null || as2[i1].length() <= 0)
                as3[i1][0] = as1[i1];
            else
                as3[i1][0] = as2[i1];
        	
            as3[i1][1] = as1[i1];
            as3[i1][2] = as[i1];
        }

        return as3;
    }
	
	
    public void createDefaultColumnsFromModel()
    {
        TableModel tablemodel = getModel();
        
        if( tablemodel != null && tablemodel instanceof NOVSearchTableModel )
        {
        	NOVSearchTableModel theTableModel = (NOVSearchTableModel)tablemodel;
          
            for(TableColumnModel tablecolumnmodel = getColumnModel(); tablecolumnmodel.getColumnCount() > 0; tablecolumnmodel.removeColumn(tablecolumnmodel.getColumn(0)));
            for(int i = 0; i < theTableModel.getColumnCount(); i++)
            {
                TableColumn tablecolumn = new TableColumn(i);
                //AIFIdentifier theIdent = theTableModel.getColumnIdentifier(i);
                tablecolumn.setIdentifier( theTableModel.getColumnIdentifier(i) );
                tablecolumn.setHeaderValue( theTableModel.getColumnName(i) );
                addColumn(tablecolumn);
            }
        	
        }
    }

	
	public static String[][] getPropertyTableDisplayKey(String searchKey)
    {
		String values[][] = null;
		String as[] = new String[2];
		searchKey = searchKey + "_";
		as[0] = "NOV4_" + searchKey + "ColumnPreferences";
		as[1] = "NOV4_" + searchKey + "ColumnWidthPreferences";

    	TCSession session= (TCSession)AIFUtility.getDefaultSession();
		TCPreferenceService prefServ = ((TCSession)session).getPreferenceService();
		String[] columnNames = prefServ.getStringArray(
				TCPreferenceService.TC_preference_site, as[0]);

		String[] columnWidth = prefServ.getStringArray(
				TCPreferenceService.TC_preference_site, as[1]);

		values = new String[2][columnNames.length];
		for (int i = 0; null != columnNames && i < columnNames.length; i++)
			values[0][i] = columnNames[i].toString();

		for (int i = 0; null != columnWidth && i < columnWidth.length; i++)
			values[1][i] = columnWidth[i].toString();
		
		if(columnNames.length == 0 || columnWidth.length == 0)
		{
			MessageBox.post("Please Define ColumnPreferences and ColumnWidthPreferences for selected " +
					"BusinessObjectType \n eg: "+as[0]+"=\n puid \n object_name \n", "ERROR", MessageBox.ERROR);
		}

		return values;
		
	} //getPropertyTableDisplayKey(String searchKey)
	
    public void hideColumns(String strBusinessObjectType )
    {
	    String as = "NOV_" + strBusinessObjectType + "_hidden_columns";

        // Get the list of hidden columns.
        //TCPreferenceService prefServ = ((TCSession)session).getPreferenceService();
	    TCPreferenceService prefServ = getSession().getPreferenceService();//TC10.1 Upgrade
        String[] columnNames = prefServ.getStringArray(
		TCPreferenceService.TC_preference_site, as);

        // hide columns.
		NOVSearchTable theTable = (NOVSearchTable) this;
		
		for(int inx=0; inx<columnNames.length; inx++)
		{
			try
			{
			    TableColumn theColm = theTable.getColumn( columnNames[inx] );
			
			    theTable.removeColumn(theColm);
			}
			catch( Exception ex)
			{
				ex.printStackTrace();
			}
		}
		  
    } // hideColumns(String strBusinessObjectType )
    
    
    public void showColumns(String[] columnNamesShown )
    {
	   if (columnNamesShown ==null)
		   return;
        
        // hide columns.
		NOVSearchTable theTable = (NOVSearchTable) this;
		
		for(int inx=0; inx<columnNamesShown.length; inx++)
		{
			try
			{
				NOVSearchTableModel theTableModel = (NOVSearchTableModel)theTable.getModel();
		          
				int i = theTableModel.findColumnByID( columnNamesShown[inx] );
					            
                TableColumn tablecolumn = new TableColumn(i);                
                tablecolumn.setIdentifier( theTableModel.getColumnIdentifier(i) );
                tablecolumn.setHeaderValue( theTableModel.getColumnName(i) );
                theTable.addColumn(tablecolumn);
			}
			catch( Exception ex)
			{
				ex.printStackTrace();
			}
		}
		  
    } // hideColumns(String strBusinessObjectType )
}
