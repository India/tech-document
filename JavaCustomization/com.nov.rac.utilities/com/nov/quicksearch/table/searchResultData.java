package com.nov.quicksearch.table;

public class searchResultData
{
	private int m_nStartRowIndex = 0;
	private int m_nEndRowIndex = 0;
	private int m_iCols = 0;
	private String[] m_result = null;

	private searchResultData()
	{
	}
	
	public searchResultData(String[] result, int nStartRowIndex,int nEndRowIndex, int iCols)
	{
		m_result = result;		
		m_nStartRowIndex = nStartRowIndex;
		m_nEndRowIndex = nEndRowIndex;
		m_iCols = iCols;
	}

	public String [] getSearchData()
	{
		return m_result;
	}
	public int getNumRows()
	{
		return m_nEndRowIndex - m_nStartRowIndex;
	}
	public int getNumCols()
	{
		return m_iCols;
	}
	public int getStartRowIndex()
	{
		return m_nStartRowIndex;
	}
	public int getEndRowIndex()
	{
		return m_nEndRowIndex;
	}
}
