package com.nov.quicksearch.table;

import javax.swing.Icon;
import javax.swing.JTable;

import com.nov.quicksearch.table.AbstractNOVTableCellRenderer;
import com.nov.quicksearch.table.NOVSearchTable;
import com.teamcenter.rac.common.TCTypeRenderer;

public class NOVNameRendererEx extends AbstractNOVTableCellRenderer 
{
	private static final long serialVersionUID = 1L;
	private int iObject_Type_Col_Index = -1;
	private String m_strObjectTypeColumnName = "object_type";

    public NOVNameRendererEx()
    {
    	
    }
    
    public void setObjectTypeColumnName(String colName)
    {
    	m_strObjectTypeColumnName = colName;
    }
    
	@Override
	protected String getCellValue(JTable jtable, Object obj, int iRow, int iCol) 
	{
		if(iObject_Type_Col_Index == -1)
		{
			if(jtable instanceof NOVSearchTable)
			{
				int iViewColIndex = -1;
				
				iViewColIndex = ((NOVSearchTable)jtable).getColumnIndexFromDataModel( m_strObjectTypeColumnName );
				
				iObject_Type_Col_Index = iViewColIndex; //jtable.convertColumnIndexToModel(iViewColIndex);
	        }
			
		}
		
		String rowType = null;
		
		if(iObject_Type_Col_Index != -1)
		{
			
			if(jtable instanceof NOVSearchTable)
			{	        	  
	        	rowType = (String)((NOVSearchTable)jtable).getValueAtDataModel(iRow, iObject_Type_Col_Index );
	        }
		}
		
		return rowType;

	}

	@Override
	protected Icon getDisplayIcon(String rowType, Object obj) 
	{
		if(rowType == null)
			return null;		
		
		javax.swing.ImageIcon imageicon = (javax.swing.ImageIcon) getValueIcon(rowType);
        if(imageicon == null)
        {
            imageicon = TCTypeRenderer.getTypeIcon(rowType, null);
            
            if(imageicon != null)
            {
            	setValueIcon(rowType, imageicon);	
            }
            
        }
        return imageicon;

	} // getDisplayIcon

}
