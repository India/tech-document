package com.nov.quicksearch.table;

import java.util.Hashtable;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.common.TCTableLine;
import com.teamcenter.rac.kernel.TCComponent;

public class NOVSearchTableLine extends TCTableLine 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NOVSearchTableLine(Hashtable<String, String> hashtable)
	{
		super(hashtable);
	}
	
	public Object getProperty(String s)
	{
        Object obj = null;
        
        if(s != null)
        {
           obj = propertyValues.get(s);
        }
        
//        if(s != null && s.equals("relation"))
//        {
//            AIFComponentContext aifcomponentcontext = getContextComponent();
//            if(aifcomponentcontext != null)
//                obj = aifcomponentcontext.getContextDisplayName();
//        } else
//        {
//            InterfaceAIFComponent interfaceaifcomponent = getComponent();
//            try
//            {
//                if(interfaceaifcomponent != null)
//                    obj = interfaceaifcomponent.getProperty(s);
//                else
//                    obj = propertyValues.get(s);
//            }
//            catch(Exception exception) { }
//        }
        
        return obj;
    }

	public void setProperty(String s, Object obj)
    {
        InterfaceAIFComponent interfaceaifcomponent = getComponent();
        if((interfaceaifcomponent == null || s.indexOf('.') >= 0) && s != null)
            if(obj != null)
                propertyValues.put(s, obj);
            else
                propertyValues.remove(s);
    }
	
	/* ********************************************************** 
	 * 
	 * ********************************************************** 
	 * */
	 public TCComponent getTCComponent()
    {
		 TCComponent theComponent = null;
	
		 // get the UID for the component from this line.
		 
		 //covnert the UID to TCComponent.
		  
        return theComponent;
    }

}
