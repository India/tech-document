package com.nov.quicksearch.table;

import java.util.Hashtable;

import com.nov.quicksearch.table.searchResultData;
import com.teamcenter.rac.common.TCTableModel;

public class NOVSearchTableModel extends TCTableModel 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NOVSearchTableModel()
	{
	    super();
	}

	public NOVSearchTableModel(String as[][])
	{
	   super(as);	    
	}

	public NOVSearchTableModel(String as[])
	{
	   super(as);	       
	}

	public NOVSearchTableLine createLine(Object obj)
    {
		NOVSearchTableLine aiftableline = null;
		String [] strObj = null;
		
		if(obj instanceof String[])
		{
			strObj = (String []) ((Object[])(obj));	
			
	        if(strObj != null)
	        {
	            int i = strObj.length;
	            int j = getColumnCount();
	            Hashtable<String, String> hashtable = new Hashtable<String, String>();
	            for(int k = 0; k < j && k < i; k++)
	            {
	               if(strObj[k] != null)
	               {
	            	   hashtable.put(getColumnIdentifier(k), strObj[k]);
	               }
	            }
	             

	            aiftableline = new NOVSearchTableLine(hashtable);
	        }
		}
		else 
		{
			aiftableline = (NOVSearchTableLine) super.createLine(obj);
		}
        
		return aiftableline;
    } // public NOVSearchTableLine createLine(Object obj)

    protected NOVSearchTableLine[] createLines(Object obj)
    {
    	NOVSearchTableLine aaiftableline[] = null;

    	if( obj instanceof searchResultData )
        {
    		searchResultData result = (searchResultData) obj;
    		    		
            int i = result.getNumRows();
            int nStartInx = result.getStartRowIndex();
            int nEndInx = result.getEndRowIndex();
                        
            aaiftableline = new NOVSearchTableLine[i];
            int icols = result.getNumCols();
        	String[] resultRows = result.getSearchData();
        	int nRowInxToVectorInx = 0;
            
            for(int j = nStartInx; j < nEndInx; j++)
            {
            	String[] row = new String[icols];
            	
    			//row[0] = Integer.toString(j);
            	nRowInxToVectorInx = (j * icols);
            	
            	for(int colInx=0; colInx < icols; colInx++ )
    			{
    				row[colInx] = (resultRows[ nRowInxToVectorInx + colInx]);
    			}
            		
            	NOVSearchTableLine newLine = createLine( row );
                aaiftableline[j - nStartInx] = newLine;
            }

        }
    	else if( obj instanceof String [][] )
        {
    		String [][] strArrArr = (String [][])obj;
    		
            int i = strArrArr.length;
            aaiftableline = new NOVSearchTableLine[i];
            for(int j = 0; j < i; j++)
            {
            	NOVSearchTableLine newLine = createLine( strArrArr[j] );
                aaiftableline[j] = newLine;
            }

        } 
    	else
    	{
    		aaiftableline = (NOVSearchTableLine[]) super.createLines(obj);
    	}
    	
    	return aaiftableline;
    }
    
    public int findColumnByID(String s)
    {
    	String colName;
        for(int i = 0; i < getColumnCount(); i++)
        {
        	colName = getColumnIdentifier(i);
            if(s.equals(colName))
            {
                return i;
            }
        }

        return -1;
    }

}
