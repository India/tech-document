package com.nov.rac.interfaces;

import java.util.List;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public interface ILoadDelegate
{
    void loadComponent(Object inputObject,String context) throws TCException;
    List<IUIPanel> getInputProviders();
    void setRegistry(Registry registry);
    void setComponent(TCComponent tcComponent);
}
