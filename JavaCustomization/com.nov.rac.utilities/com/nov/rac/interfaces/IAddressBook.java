package com.nov.rac.interfaces;

public interface IAddressBook
{
    public void setVisible(boolean isVisible);
    public String getSelectedText();
}
