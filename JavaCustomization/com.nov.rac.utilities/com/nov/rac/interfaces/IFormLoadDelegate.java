package com.nov.rac.interfaces;

import java.util.List;

import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public interface IFormLoadDelegate
{
    public List<IUIPanel> getInputProviders();
    public void registerOperationInputProvider(List<IUIPanel> inputProviders);
    public void loadComponent(Object inputObject) throws TCException ;
}
