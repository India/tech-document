package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVSearchProvider2;

public class FindButtonListener extends ExecutorImpl implements SelectionListener
{
    public FindButtonListener(INOVSearchProvider2 searchProvider)
    {
        super(searchProvider);
    }
    
    public void widgetDefaultSelected(SelectionEvent arg0)
    {
    }
    
    public void widgetSelected(SelectionEvent arg0)
    {
        execute();
        publishData();
    }
}
