package com.nov.rac.item.itemsearch;

import com.nov.quicksearch.services.INOVSearchResult;

public class ItemSearchDataModel
{
    public static ItemSearchDataModel getInstance()
    {
        if(m_itemSearchDataModel == null)
            m_itemSearchDataModel = new ItemSearchDataModel();
        
        return m_itemSearchDataModel;
    }
    
    private ItemSearchDataModel()
    {
    }
    
    public void setSearchResult(INOVSearchResult searchResult)
    {
        m_searchResult = searchResult;
    }

    public INOVSearchResult getSearchResult()
    {
        return m_searchResult;
    }

    public void setResponseIndex(int responseIndex)
    {
        m_responseIndex = responseIndex;
    }

    public int getResponseIndex()
    {
        return m_responseIndex;
    }
    
    private INOVSearchResult m_searchResult;
    
    private static ItemSearchDataModel m_itemSearchDataModel;
    
    private int m_responseIndex = 0;
}
