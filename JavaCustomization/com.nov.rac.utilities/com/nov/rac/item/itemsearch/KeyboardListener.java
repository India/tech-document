package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

import com.nov.quicksearch.services.INOVSearchProvider2;

public class KeyboardListener extends ExecutorImpl implements KeyListener
{
    public KeyboardListener(INOVSearchProvider2 searchProvider)
    {
        super(searchProvider);
    }
    
    public void keyPressed(KeyEvent keyevent)
    {
        if( keyevent.character == '\r' )
        {
            execute();
            publishData();
        }
    }

    public void keyReleased(KeyEvent keyevent)
    {
    }
}
