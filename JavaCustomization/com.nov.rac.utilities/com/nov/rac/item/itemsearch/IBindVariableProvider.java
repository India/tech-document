package com.nov.rac.item.itemsearch;

import com.nov.rac.propertymap.IPropertyMap;

public interface IBindVariableProvider
{
    public IPropertyMap readBindVariables();
}
