package com.nov.rac.item.itemsearch;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;

public class ExecutorImpl implements IExecutor, IPublisher
{
    /*private  IController controller = null;
    
    public IController getController()
    {
        return controller;
    }

    public void setController(IController controller)
    {
        this.controller = controller;
    }
    */
    public ExecutorImpl(INOVSearchProvider2 searchProvider)
    {
        m_searchProvider = searchProvider;
    }
    
    public void execute()
    {
        try
        {
            if (m_searchProvider != null)
            {
                if (m_searchProvider.validateInput() == false)
                {
                    return;
                }
                m_searchResult = NOVSearchExecuteHelperUtils.execute(m_searchProvider, 0);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        if (m_searchResult == null)
            return;
    }
    
    public void publishData()
    {
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        dataModel.setSearchResult(m_searchResult);
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
//       if(null != controller)
//       {
           PublishEvent publishEvent = new PublishEvent(getPublisher(), "ItemSearch", "ResultsAvailable", "");
           controller.publish(publishEvent);
       //}
    }

    public IPropertyMap getData()
    {
        return null;
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    IPublisher getPublisher()
    {
        return this;
    }

    private INOVSearchProvider2 m_searchProvider;
    
    private INOVSearchResult m_searchResult = null;
}
