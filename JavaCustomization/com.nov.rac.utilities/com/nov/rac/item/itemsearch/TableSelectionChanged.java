package com.nov.rac.item.itemsearch;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;

import com.nov.quicksearch.table.NOVSearchTable;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class TableSelectionChanged implements ISelectionChangedListener, IPublisher
{
    /*private  IController controller = null;
    
    public IController getController()
    {
        return controller;
    }

    public void setController(IController controller)
    {
        this.controller = controller;
    }*/
    
    public void selectionChanged(SelectionChangedEvent selectionchangedevent)
    {
        NOVSearchTable searchTable = (NOVSearchTable) selectionchangedevent.getSource();
        publishData(searchTable);
    }
    
    /*private void publishData(NOVSearchTable searchTable)
    {
        int iSelectdRow = searchTable.getSelectedRow();
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        
        String sPUID = searchTable.getValueAtDataModel(iSelectdRow, 0);
        
        TCComponent theSelComponent = null;
        try
        {
            theSelComponent = session.stringToComponent(sPUID);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
        //        if(null != controller)
        //        {
            PublishEvent publishEvent = new PublishEvent(getPublisher(), "SelectedItem", theSelComponent, "");
            controller.publish(publishEvent);
       // }
    }*/
    
    //8677 commented and added this
    private void publishData(NOVSearchTable searchTable)
    {
        int[] iSelectdRows = searchTable.getSelectedRows();
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        
        TCComponent[] selectedComponents = new TCComponent[iSelectdRows.length];
        
        for(int inx=0; inx < selectedComponents.length; inx++)
        {
            String sPUID = searchTable.getValueAtDataModel(iSelectdRows[inx], 0);
            TCComponent theSelComponent = null;
            try
            {
                theSelComponent = session.stringToComponent(sPUID);
                selectedComponents[inx] = theSelComponent;
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
        //        if(null != controller)
        //        {
            PublishEvent publishEvent = new PublishEvent(getPublisher(), "SelectedItem", selectedComponents, "");
            controller.publish(publishEvent);
       // }
    }
    //8677 end
    
    public IPropertyMap getData()
    {
        return null;
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    IPublisher getPublisher()
    {
        return this;
    }
}
