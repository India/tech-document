package com.nov.rac.item.itemsearch;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ItemSearchTitleComposite  extends AbstractUIPanel
{
    public ItemSearchTitleComposite(Composite parent, int style)
    {
        super(parent, style);
        
        m_appReg = getRegistry();
    }

    Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.itemsearch.itemsearch");
    }

    public boolean createUI() throws TCException
    {
        final Composite parent = getComposite();
        
        Color compColor = new Color( null, 160, 160, 160 );
        
        parent.setLayout(new GridLayout(3, false));
        GridData gdParent = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
        parent.setLayoutData(gdParent);
        parent.setBackground(compColor);
        
        m_pinnedButton = new Button(parent, SWT.NONE);
        int width = m_pinnedButton.getMonitor().getBounds().width;
        
        GridData gdPinnedButton = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
        gdPinnedButton.widthHint = width / 60;
        gdPinnedButton.heightHint = width / 60;
        m_pinnedButton.setLayoutData(gdPinnedButton);
        m_pinnedButton.setBackground(compColor);
        
        Image pinnedButtonImage = new Image(parent.getDisplay(),
                m_appReg.getImage("ItemSearch.Pinned"), SWT.NONE);
        m_pinnedButton.setImage(pinnedButtonImage);
        
        m_titleBarLabel = new Label(parent, SWT.NONE);
        GridData gdTitleBarLabel = new GridData(SWT.LEFT, SWT.TOP,  false, false, 1, 1);
        gdTitleBarLabel.widthHint = 240;
        m_titleBarLabel.setLayoutData(gdTitleBarLabel);
        m_titleBarLabel.setText("Open by Name");
        m_titleBarLabel.setBackground(compColor);
        
        m_closeButton = new Button(parent, SWT.NONE);
        GridData gdCloseButton = new GridData(SWT.RIGHT, SWT.TOP, true, false, 1, 1);
        gdCloseButton.widthHint = width / 60;
        gdCloseButton.heightHint = width / 60;
        m_closeButton.setLayoutData(gdCloseButton);
        m_closeButton.setBackground(compColor);
        
        Image closeButtonImage = new Image(parent.getDisplay(),
                m_appReg.getImage("ItemSearch.Close"), SWT.NONE);
        m_closeButton.setImage(closeButtonImage);
        
        m_closeButton.addSelectionListener( new SelectionListener()
        {
            public void widgetSelected(SelectionEvent selectionevent)
            {
                closeDialog(parent);
            }
            
            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        });
        
        return true;
    }

    public boolean reload() throws TCException
    {
        return false;
    }

    private void closeDialog(Composite parent)
    {
        /// Get the dialog and dispose it.
        parent.getShell().close();
    }
    
    private Label m_titleBarLabel;
    
    private Button m_pinnedButton;
    
    private Button m_closeButton;
    
    private Registry m_appReg = null;
}
