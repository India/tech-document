package com.nov.rac.item.itemsearch;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.propertymap.IPropertyMap;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ItemIDSearchProvider implements INOVSearchProvider2
{
    private String[] m_objectTypesArr;
    
    private String m_itemId = null;
    private String m_objectName = null;
    
    private Registry m_registry;
    
    private IBindVariableProvider m_bindVarProvider;
    
    public ItemIDSearchProvider(IBindVariableProvider bindVarProvider)
    {
        // TODO Auto-generated constructor stub
        m_bindVarProvider = bindVarProvider;
        m_registry = getRegistry();
    }
    
    Registry getRegistry()
    {
        return Registry.getRegistry("com.nov.rac.item.itemsearch.itemsearch");
    }
    
    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return "Item";
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        // TODO Auto-generated method stub
        if (m_itemId == null || m_itemId.isEmpty())
        {
            m_itemId = "*";
        }
        
        if (m_objectName == null || m_objectName.isEmpty())
        {
            m_objectName = "*";
        }
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[12];
        
        // Item.item_id
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_string;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { m_itemId };
        
        // Item.object_name
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { m_objectName };
        
        // Item.object_type
        allBindVars[2] = new QuickSearchService.QuickBindVariable();
        allBindVars[2].nVarType = POM_string;
        allBindVars[2].nVarSize = 1;
        allBindVars[2].strList = m_objectTypesArr;
        
        // m_sCreatedAfter,m_sCreatedBefore
        allBindVars[3] = new QuickSearchService.QuickBindVariable();
        allBindVars[3].nVarType = POM_date;
        allBindVars[3].nVarSize = 2;
        allBindVars[3].strList = new String[] { "*", "*" };
        
        // m_sItemDesc
        allBindVars[4] = new QuickSearchService.QuickBindVariable();
        allBindVars[4].nVarType = POM_string;
        allBindVars[4].nVarSize = 1;
        allBindVars[4].strList = new String[] { "*" };
        
        // m_sReleaseStatus
        allBindVars[5] = new QuickSearchService.QuickBindVariable();
        allBindVars[5].nVarType = POM_string;
        allBindVars[5].nVarSize = 1;
        allBindVars[5].strList = new String[] { "*" };
        
        // m_sOwningUser
        allBindVars[6] = new QuickSearchService.QuickBindVariable();
        allBindVars[6].nVarType = POM_string;
        allBindVars[6].nVarSize = 1;
        allBindVars[6].strList = new String[] { "*" };
        
        // m_grpPuid
        allBindVars[7] = new QuickSearchService.QuickBindVariable();
        allBindVars[7].nVarType = POM_typed_reference;
        allBindVars[7].nVarSize = 1;
        allBindVars[7].strList = new String[] { "" };
        
        // RSOne 4 alternate ID
        allBindVars[8] = new QuickSearchService.QuickBindVariable();
        allBindVars[8].nVarType = POM_string;
        allBindVars[8].nVarSize = 1;
        allBindVars[8].strList = new String[] { "*" };
        
        // m_sLastOwningUser
        allBindVars[9] = new QuickSearchService.QuickBindVariable();
        allBindVars[9].nVarType = POM_string;
        allBindVars[9].nVarSize = 1;
        allBindVars[9].strList = new String[] { "*" };
        
        // Send Alternate ID selection
        allBindVars[10] = new QuickSearchService.QuickBindVariable();
        allBindVars[10].nVarType = POM_string;
        allBindVars[10].nVarSize = 1;
        allBindVars[10].strList = new String[] { "*" };
        
        // TCDECREL-4832 Added additional bind variable for release status
        allBindVars[11] = new QuickSearchService.QuickBindVariable();
        allBindVars[11].nVarType = POM_string;
        allBindVars[11].nVarSize = 1;
        allBindVars[11].strList = new String[] { "*" };
        
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        // TODO Auto-generated method stub
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[2];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 6, 7 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 2, 1, 3, 4, 5 };
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-nov4part-master-props";
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[1].listInsertAtIndex = new int[] { 6 };
        
        return allHandlerInfo;
    }
    
    @Override
    public boolean validateInput()
    {
        // TODO Auto-generated method stub
        boolean retValue = true;
        
        IPropertyMap bindVariablesMap = m_bindVarProvider.readBindVariables();
        
        m_itemId = bindVariablesMap.getString("item_id");
        m_objectName = bindVariablesMap.getString("object_name");
        
        m_itemId = m_itemId.trim();
        m_objectName = m_objectName.trim();
        
        m_objectTypesArr = bindVariablesMap.getStringArray("object_type");
        
        if (m_itemId.isEmpty() && m_objectName.isEmpty())
        {
            MessageBox.post(m_registry.getString("ItemSearch.NO_INPUT_CRITERIA_FOUND"),
                    m_registry.getString("ItemSearch.TITLE"), 1);
            
            retValue = false;
        }
        
        return retValue;
    }
    
}
