package com.nov.rac.item.itemsearch;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.util.Registry;

public class SearchButtonComponent extends Composite
{
    public SearchButtonComponent(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        createUI(parent, style);
    }
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    private void createUI(final Composite parent, final int style)
    {
        setLayout(new FillLayout());
        m_searchButton = new Button(this, style);
        
       // Registry reg = Registry.getRegistry(this);
        m_searchButton.setText(getRegistry().getString("ItemSearch.SearchButton"));
        
        m_searchButton.addSelectionListener( new SelectionListener()
        {
            public void widgetSelected(SelectionEvent arg0)
            {
                showPopupDialog();
            }
            
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
            }
        });
    }
    
    /*public void setSearchItemType(String[] serachItemTypeArr)
    {
        int size = serachItemTypeArr.length;
        m_serachItemTypeArr = new String[size];
        
        for (int index = 0; index < size; ++index)
        {
            m_serachItemTypeArr[index] = serachItemTypeArr[index];
        }
    }*/
    
    private void showPopupDialog()
    {
        m_itemSearchDialog = new ItemSearchDialog(m_searchButton.getShell(), SWT.BORDER | SWT.RESIZE);
        //m_itemSearchDialog.setSearchItemType(m_serachItemTypeArr);
        setItemSearchProperties();
        Point btnLocation = m_searchButton.toDisplay(m_searchButton.getLocation());
        Point btnSize = m_searchButton.getSize();
        m_itemSearchDialog.setLocation(btnLocation, btnSize);
        
        m_itemSearchDialog.open();
    }
    
    private void setItemSearchProperties()
    {
        ISearchPropertiesProvider itemSearchDialog = m_itemSearchDialog;
        Iterator <Map.Entry<String, Object>> iterator = m_propertyMap.entrySet().iterator();
        while(iterator.hasNext())
        {
            Map.Entry<String, Object> entrySet = iterator.next();
            itemSearchDialog.setProperty(entrySet.getKey(), entrySet.getValue());
        }
    }

    public Object getProperty(String propName)
    {
        /*ISearchPropertiesProvider itemSearchDialog = m_itemSearchDialog;
        return itemSearchDialog.getProperty(propName);*/
        return m_propertyMap.get(propName);
    }

    public void setProperty(String propName, Object propValue)
    {
        m_propertyMap.put(propName, propValue);
        /*ISearchPropertiesProvider itemSearchDialog = m_itemSearchDialog;
        itemSearchDialog.setProperty(propName, propValue);*/
    }

    public void setImage(Image image)
    {
        m_searchButton.setImage(image);
    }
    
    public void setText(String text)
    {
        m_searchButton.setText(text);
    }
    
    public void setEnabled(boolean enabled)
    {
        m_searchButton.setEnabled(enabled);
    }
    
    private Button m_searchButton;
    private ItemSearchDialog m_itemSearchDialog;
    //private String[] m_serachItemTypeArr;
    private Registry m_registry;
    private Map<String,Object> m_propertyMap = new HashMap<String, Object>();
	
	
	
}
