package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;

public class OKButtonListener extends ButtonListener
{
    
    @Override
    public void widgetSelected(SelectionEvent event)
    {
        publishData();
        
        Button okBtn = (Button)(event.getSource());
        okBtn.getShell().dispose();
    }
}
