package com.nov.rac.item.itemsearch;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class ItemSerachToolBarComposite extends AbstractUIPanel implements ISubscriber
{

    public ItemSerachToolBarComposite(Composite parent, int style)
    {
        super(parent, style);
        
        m_appReg = getRegistry();
        
        registerSubscriber();
    }
    
    private Registry getRegistry()
    {
        return Registry.getRegistry(this);
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
//        IController theController = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        theController.registerSubscriber("ItemSearch", this);
        
        theController.registerSubscriber("NextResults", this);
        
        theController.registerSubscriber("PreviousResults", this);
        
        theController.registerSubscriber("LoadAllItems", this);
        
        theController.registerSubscriber("SelectedItem", this);
    }
    
    public boolean createUI() throws TCException
    {
        Composite parent = getComposite();
        parent.setLayout(new GridLayout(1, false));
        GridData gdParent = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        parent.setLayoutData(gdParent);
        
        Composite toolbarComposite = createNewComposite(parent,5);
        //parent.setLayout(new GridLayout(5, false));
        //GridData gdParent = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
        //parent.setLayoutData(gdParent);
        m_itemSearchStatus = new Text(toolbarComposite, SWT.BOLD | SWT.BORDER | SWT.SHADOW_ETCHED_IN);
        GridData gd_m_itemSearchStatus = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
       // gd_m_itemSearchStatus.widthHint = 180;
        m_itemSearchStatus.setLayoutData(gd_m_itemSearchStatus);
        m_itemSearchStatus.setText(m_appReg.getString("ItemSearch.STATUS"));
        //TCDECREL-6780 starts
        m_itemSearchStatus.setEditable(false);
        m_itemSearchStatus.setEnabled(false);
        //TCDECREL-6780 ends
        
//        int width = m_itemSearchStatus.getMonitor().getBounds().width;
//        int buttonWidths = width / 60;
        
        m_previousButton = new Button(toolbarComposite, SWT.NONE);
        GridData gdPrevious = new GridData(SWT.NONE, SWT.CENTER, false, false, 1, 1);
       // gdPrevious.widthHint = buttonWidths;
       // gdPrevious.heightHint = buttonWidths;
        m_previousButton.setLayoutData(gdPrevious);
        
        Image previousButtonImage = new Image(toolbarComposite.getDisplay(), m_appReg.getImage("ItemSearch.Previous"), SWT.NONE);
        
        m_previousButton.setImage(previousButtonImage);
        
        m_nextButton = new Button(toolbarComposite, SWT.NONE);
        GridData gdNext = new GridData(SWT.NONE, SWT.CENTER, false, false, 1, 1);
       // gdNext.widthHint = buttonWidths;
       // gdNext.heightHint =  buttonWidths;
        m_nextButton.setLayoutData(gdNext);
        
        Image nextButtonImage = new Image(toolbarComposite.getDisplay(), m_appReg.getImage("ItemSearch.Next"), SWT.NONE);
        
        m_nextButton.setImage(nextButtonImage);
        
        m_loadAllButton = new Button(toolbarComposite, SWT.NONE);
        GridData gdLoadAll = new GridData(SWT.NONE, SWT.CENTER, false, false, 1, 1);
      //  gdLoadAll.widthHint = buttonWidths;
      //  gdLoadAll.heightHint = buttonWidths;
        m_loadAllButton.setLayoutData(gdLoadAll);
        
        Image loadAllButtonImage = new Image(toolbarComposite.getDisplay(), m_appReg.getImage("ItemSearch.LoadAll"), SWT.NONE);
        
        m_loadAllButton.setImage(loadAllButtonImage);
        
        m_copyToClipboardButton = new Button(toolbarComposite, SWT.NONE);
        GridData gdcopyToClipboard = new GridData(SWT.NONE, SWT.CENTER, false, false, 1, 1);
     //   gdcopyToClipboard.widthHint = buttonWidths;
     //   gdcopyToClipboard.heightHint = buttonWidths;
        m_copyToClipboardButton.setLayoutData(gdcopyToClipboard);
        
        Image copyItemButtonImage = new Image(toolbarComposite.getDisplay(), m_appReg.getImage("ItemSearch.CopyToClipBoard"),
                SWT.NONE);
        
        m_copyToClipboardButton.setImage(copyItemButtonImage);
        
        addOkAppyCancelButtons(parent);
        
        //TCDECREL-9076
        m_previousButton.setToolTipText(m_appReg.getString("PreviousBtn.TOOLTIP"));
        m_nextButton.setToolTipText(m_appReg.getString("NextBtn.TOOLTIP"));
        m_loadAllButton.setToolTipText(m_appReg.getString("LoadAllBtn.TOOLTIP"));
        m_copyToClipboardButton.setToolTipText(m_appReg.getString("CopyToClipBoardBtn.TOOLTIP"));    
        
        /*//8677 add ok button
        Composite okButtonComposite = createNewComposite(parent,2);
        addOkButton(okButtonComposite);
        addCancelButton(okButtonComposite);*/
        
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        dataModel.setResponseIndex(0);
        
        updateControlState("");
        
        return true;
    }

    private void addOkAppyCancelButtons(Composite parent)
    {

        if(!getSearchContext().equals("NID")) // This condition is introduced to keep NID functionality as it is
        {
            Composite buttonComposite = createNewComposite(parent,5);
            addEmptyLabel(buttonComposite);
            addOkButton(buttonComposite);
            addApplyButton(buttonComposite);
            addCancelButton(buttonComposite);
            addEmptyLabel(buttonComposite);
        }
        
    }
    
    private void addEmptyLabel(Composite buttonComposite)
    {
        Label label = new Label(buttonComposite,SWT.NONE);
        GridData gdLabel = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
        label.setLayoutData(gdLabel);
    }
    
    private void addCancelButton(Composite buttonComposite)
    {
        Button cancelButton = new Button(buttonComposite, SWT.NONE);
        GridData gdCancelButton = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
        cancelButton.setLayoutData(gdCancelButton);
        cancelButton.setText(" Cancel ");
        addCancelButtonListener(cancelButton);
    }

    private void addCancelButtonListener(final Button cancelButton)
    {
        cancelButton.addSelectionListener(new SelectionListener()
        {
            
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                cancelButton.getShell().dispose();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0)
            {
                
            }
        });
    }

    private void addApplyButton(Composite buttonComposite)
    {
        Button applyButton = new Button(buttonComposite, SWT.NONE);
        GridData gdApplyButton = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
        applyButton.setLayoutData(gdApplyButton);
        applyButton.setText(" Apply ");
        addApplyButtonListener(applyButton);
    }

    private void addApplyButtonListener(Button applyButton)
    {
        m_buttonApplyListener = new ButtonListener();
        m_buttonApplyListener.setSource(getSource());
        applyButton.addSelectionListener(m_buttonApplyListener);
    }

    private Composite createNewComposite(Composite parent,int noOfColumns)
    {
        Composite composite = new Composite(parent,SWT.NONE);
        composite.setLayout(new GridLayout(noOfColumns, false));
        GridData gdParent = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        composite.setLayoutData(gdParent);
        return composite;
    }
    private void addOkButton(Composite buttonComposite)
    {
        m_okButton = new Button(buttonComposite, SWT.NONE);
        GridData gdOkButton = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
        m_okButton.setLayoutData(gdOkButton);
        m_okButton.setText("   OK   ");
        addOKButtonListener(m_okButton);
    }
    
    private void addOKButtonListener(Button button)
    {
        m_buttonOkListener = new OKButtonListener();
        m_buttonOkListener.setSource(getSource());
        button.addSelectionListener(m_buttonOkListener);
    }

    public boolean reload() throws TCException
    {
        return false;
    }
    
    public void setSearchProvider(INOVSearchProvider2 searchProvider)
    {
        m_searchProvider = searchProvider;
        
        addSelectionListeners();
    }
    
    private void addSelectionListeners()
    {
        //IController theController = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        
        NextButtonListener nextButtonListener = new NextButtonListener(m_searchProvider);
        //nextButtonListener.setController(theController);
        m_nextButton.addSelectionListener(nextButtonListener);
        
        PreviousButtonListener prevButtonListener = new PreviousButtonListener();
        //prevButtonListener.setController(theController);
        m_previousButton.addSelectionListener(prevButtonListener);
        
        LoadAllButtonListener loadAllButtonListener = new LoadAllButtonListener(m_searchProvider);
        //loadAllButtonListener.setController(theController);
        m_loadAllButton.addSelectionListener(loadAllButtonListener);
    }
    
    public void update(PublishEvent event)
    {
        String propertyName = event.getPropertyName();
        if (propertyName.equals("SelectedItem"))
        {
            /*InterfaceAIFComponent aifComponent = (InterfaceAIFComponent) event.getNewValue();
            
            CopyToClipboardListener copytoClipboardButtonListener = new CopyToClipboardListener(
                    new InterfaceAIFComponent[] { aifComponent });
            m_copyToClipboardButton.addSelectionListener(copytoClipboardButtonListener);*/
            
            InterfaceAIFComponent[] aifComponent = (InterfaceAIFComponent[]) event.getNewValue();
            
            CopyToClipboardListener copytoClipboardButtonListener = new CopyToClipboardListener(
                    aifComponent);
            m_copyToClipboardButton.addSelectionListener(copytoClipboardButtonListener);
            
            //TCDECREL-9076
            if(aifComponent.length > 0)
            {
                m_okButton.setEnabled(true);
            }
            else
            {
                m_okButton.setEnabled(false);
            }
            //TCDECREL-9076
        }
        else
        {
            setSearchResultInfo(propertyName);
            updateControlState(propertyName);
        }
    }
    
    private void setSearchResultInfo(String propertyName)
    {
    	ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        int responseIndex = dataModel.getResponseIndex();
      
        INOVSearchResult result = dataModel.getSearchResult();
        if(null != result)
        {
        	 int total = result.getResultSet().getRows();
             
             int pageSize = 5;
         	
     		if(propertyName.equals("ItemSearch"))
     		{
     			if(total == 0)
     			{
     				setSearchResultInfo(-1, responseIndex, total);
     			}
     			else
     				setSearchResultInfo(0, responseIndex, total);
     		}
     		
     		if(propertyName.equals("LoadAllItems"))
     		{
     			setSearchResultInfo( 0, total, total );
     		}
     	
     		if(propertyName.equals("NextResults"))
     		{
     			int start = 0;
     			
     			if(responseIndex % pageSize != 0)
     			{
     				start = (responseIndex / pageSize) * pageSize;
     			}
     			else
     			{
     				start = responseIndex - pageSize;
     			}
     	
     			setSearchResultInfo(start, responseIndex, total);
     		}

     		if(propertyName.equals("PreviousResults"))
     		{
     			int start = 0;
     			
     			if( responseIndex % pageSize != 0)
     			{
     				start = (responseIndex / pageSize) * pageSize;
     				responseIndex = start + pageSize;
     			}
     			else
     			{
     				start = responseIndex - pageSize;
     			}
     	
     			setSearchResultInfo( start, responseIndex, total);
     		}
        }
       
    }
    	
    
	private void setSearchResultInfo( int startIndex, int respIndex, int totalCount )
	{
		m_itemSearchStatus.setText( "( " + Integer.toString( startIndex + 1 ) +
				" ~ " + Integer.toString( respIndex ) +
				" / " + Integer.toString( totalCount ) + " )" );
	}
    
    private void updateControlState(String propertyName)
    {
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        int responseIndex = dataModel.getResponseIndex();
        
		int pageSize = 5;
		
		if( responseIndex > 0 )
		{
			m_copyToClipboardButton.setEnabled( true );
		}
		else
		{
			m_copyToClipboardButton.setEnabled( false );
		}
		
		if( responseIndex >= pageSize )
		{
			m_nextButton.setEnabled( true );
			m_loadAllButton.setEnabled( true );
		}
		else
		{
			m_nextButton.setEnabled( false );
			m_loadAllButton.setEnabled( false );
			m_previousButton.setEnabled( false );
		}
		
		if( responseIndex > pageSize )
			m_previousButton.setEnabled( true );
		else
			m_previousButton.setEnabled( false );
		
		if( responseIndex % pageSize > 0  )
		{
			m_nextButton.setEnabled( false );
		
			//TCDECREL-9076
			m_loadAllButton.setEnabled( false );
            m_previousButton.setEnabled(false);
		}

		if(propertyName.equals("PreviousResults"))
			m_nextButton.setEnabled(true);
    }
    
    public void dispose()
    {
        ///Unregister the Listeners
        IController theController = ControllerFactory.getInstance().getDefaultController();
        //IController theController = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        theController.unregisterSubscriber("ItemSearch", this);
        
        theController.unregisterSubscriber("NextResults", this);
        
        theController.unregisterSubscriber("PreviousResults", this);
        
        theController.unregisterSubscriber("LoadAllItems", this);
        
        theController.unregisterSubscriber("SelectedItem", this);
        
        if(m_buttonOkListener != null)
        {
            m_buttonOkListener.unregisterSubscriber();
        }
        
        if(m_buttonApplyListener != null)
        {
            m_buttonApplyListener.unregisterSubscriber();
        }
    }
    
    public Object getSource()
    {
        return m_source;
    }

    public void setSource(Object source)
    {
        this.m_source = source;
    }
    
    public void setSearchContext(String context)
    {
        m_searchContext = context;
    }
    
    public String getSearchContext()
    {
        return m_searchContext;
    }
    
    private Text m_itemSearchStatus;
    private Button m_previousButton;
    private Button m_nextButton;
    private Button m_loadAllButton;
    private Button m_copyToClipboardButton;
    private Object m_source;
    private String m_searchContext;
    private Button m_okButton;
    private ButtonListener m_buttonOkListener;
    private ButtonListener m_buttonApplyListener;
    
    private Registry m_appReg = null;
    
    private INOVSearchProvider2 m_searchProvider;
}
