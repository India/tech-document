package com.nov.rac.item.itemsearch;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class IdentifierSearchProvider implements INOVSearchProvider
{
    private String m_altId = null;
    
    public IdentifierSearchProvider(String altId)
    {
        super();
        this.m_altId = altId;
    }
    
    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        // TODO Auto-generated method stub
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[1];
        
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_string;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { m_altId };
        
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        // TODO Auto-generated method stub
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-identifier";
        allHandlerInfo[0].listBindIndex = new int[] { 1 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1 };
        
        return allHandlerInfo;
    }
    
    public INOVResultSet execute(int initLoadAll) throws Exception
    {
        
        INOVSearchProvider searchService = this;
        INOVSearchResult searchResult = null;
        
        INOVResultSet theResSet = null;
        searchResult = NOVSearchExecuteHelperUtils.searchSOACallServer(searchService, initLoadAll);
        
        if (searchResult != null && searchResult.getResultSet().getRows() > 0)
        {
            theResSet = searchResult.getResultSet();
        }
        
        return theResSet;
    }
    
}
