package com.nov.rac.item.itemsearch;

public interface DialogActionListener
{
    void closeDialog();
}
