package com.nov.rac.item.itemsearch;

import java.awt.event.MouseAdapter;

import javax.swing.border.SoftBevelBorder;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.table.NOVSearchTable;
import com.nov.quicksearch.table.searchResultData;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.renderer.IDRenderer;
import com.nov.rac.utilities.renderer.NOVItemRevRelStatusRenderer;
import com.nov.rac.ui.AbstractUIPanel;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class ItemSearchResultComposite extends AbstractUIPanel implements IPublisher, ISubscriber
{
	private String m_searchContext;
	private Registry m_registry;
	
    public ItemSearchResultComposite(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        
        registerSubscriber();
    }
    
    public boolean createUI() throws TCException
    {
        Composite parent = getComposite();
        
        parent.setLayout(new GridLayout(1, false));
        
        GridData gdParent = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        parent.setLayoutData(gdParent);
        
        Composite tablePanel = new Composite(parent, SWT.EMBEDDED);
        
        GridData gdTablePanel = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 20);
        
        tablePanel.setLayoutData(gdTablePanel);
        
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        
        createTable(session);
        
        TableSelectionChanged tblSel = new TableSelectionChanged();
        //tblSel.setController(ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode()));
        m_itemSearchTable.addSelectionChangedListener(tblSel);
        m_itemSearchTable.setSelectionMode(getSelectionMode());
        //8677 resize column width
//        m_itemSearchTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        
        ScrollPagePane tablePane = new ScrollPagePane(m_itemSearchTable);
        
        SoftBevelBorder softBorder = new SoftBevelBorder(SoftBevelBorder.RAISED);
        java.awt.Color highlightInnerColor = softBorder.getHighlightInnerColor();
        java.awt.Color shadowOuterColor = softBorder.getShadowOuterColor();
        java.awt.Color shadowInnerColor = softBorder.getShadowInnerColor();
        SoftBevelBorder borderToSetToComp = new SoftBevelBorder(SoftBevelBorder.RAISED, new java.awt.Color(233, 233,
                218), highlightInnerColor, shadowOuterColor, shadowInnerColor);
        tablePane.setBorder(borderToSetToComp);
        tablePane.getViewport().setBackground(java.awt.Color.WHITE);
        
        SWTUIUtilities.embed(tablePanel, tablePane, false);
        
        // TCDECREL-6759
        addMouseListenerOnTable(tablePanel);
        
        
        // assignColumnRenderer();
        
        return true;
    }
    
    /*private void createTable(TCSession session)
    {
        String strCol[][] = NOVSearchTable.getPropertyTableDisplayKey(m_searchContext);
        m_itemSearchTable = new NOVSearchTable(session, strCol[0]);
        
        m_itemSearchTable.setColumnNames(strCol[0],m_searchContext);
        
        m_itemSearchTable.hideColumns(m_searchContext);
        IDRenderer id_renderer = new IDRenderer();
        int id_column = m_itemSearchTable.getColumnModel().getColumnIndex("item_id");
        m_itemSearchTable.getColumnModel().getColumn(id_column).setCellRenderer(id_renderer);
    }*/
    
    private void createTable(TCSession session)
    {
        String strCol[][] = NOVSearchTable.getPropertyTableDisplayKey(getSearchContext());
        m_itemSearchTable = new NOVSearchTable(session, strCol[0]);
        
        m_itemSearchTable.setColumnNames(strCol[0], getSearchContext());
        
        m_itemSearchTable.hideColumns(getSearchContext());
        IDRenderer id_renderer = new IDRenderer();
        NOVItemRevRelStatusRenderer rev_renderer = new NOVItemRevRelStatusRenderer();
        //Tushar
        int id_column = m_itemSearchTable.getColumnModel().getColumnIndex("item_id");
        m_itemSearchTable.getColumnModel().getColumn(id_column).setCellRenderer(id_renderer);
        
        if(m_itemSearchTable.getColumnIndex("latest_item_rev") >= 0)
        {
            int rev_column = m_itemSearchTable.getColumnModel().getColumnIndex("latest_item_rev");
            m_itemSearchTable.getColumnModel().getColumn(rev_column).setCellRenderer(rev_renderer);
        }
    }
    
    private void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
//        IController theController = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        theController.registerSubscriber("ItemSearch", this);
        
        theController.registerSubscriber("NextResults", this);
        
        theController.registerSubscriber("PreviousResults", this);
        
        theController.registerSubscriber("LoadAllItems", this);
    }
    
    public boolean reload() throws TCException
    {
        return false;
    }
    
    public void update(PublishEvent event)
    {
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        INOVSearchResult searchResult = dataModel.getSearchResult();
        if(null != searchResult)
        {
        	INOVResultSet resultSet = searchResult.getResultSet();
        	int responseIndex = dataModel.getResponseIndex();
        	
        	//8677
        	if(resultSet.getRows() == 0)
        	{
        		m_itemSearchTable.removeAllRows();
        		MessageBox.post(m_registry.getString("No_OBJECTS_FOUND.WARNING"), "Warning", SWT.ICON_INFORMATION);
        	}
        	
        	String propertyName = event.getPropertyName();
        	if (propertyName.equals("ItemSearch"))
        	{
        		responseIndex = addRowsToTable(resultSet, 0, 5);
        		dataModel.setResponseIndex(responseIndex);
        	}
        	else if (propertyName.equals("NextResults"))
        	{
        		responseIndex = addRowsToTable(resultSet, responseIndex, 5, 0);
        		dataModel.setResponseIndex(responseIndex);
        	}
        	else if (propertyName.equals("PreviousResults"))
        	{
        		int respIndexPrev = responseIndex - (2 * 5);
        		
        		if (respIndexPrev < 0)
        			respIndexPrev = 0;
        		
        		if (resultSet.getRows() > 0)
        		{
        			responseIndex = addRowsToTable(resultSet, respIndexPrev, 5, 0);
        			dataModel.setResponseIndex(responseIndex);
        		}
        	}
        	else if (propertyName.equals("LoadAllItems"))
        	{
        		dataModel.setResponseIndex(0);
        		responseIndex = addRowsToTable(resultSet, responseIndex, 5, -2);
        		dataModel.setResponseIndex(responseIndex);
        	}
        	
        }
    }
    
    public int addRowsToTable(INOVResultSet theResSet, int currIndex, int pageMaxSize)
    {
        int maxRows30 = pageMaxSize;
        int nStartIndex = 0;
        int nEndIndex = 0;
        
        if (maxRows30 > (theResSet.getRows() - currIndex))
        {
            maxRows30 = (theResSet.getRows() - currIndex);
        }
        
        // start Index
        nStartIndex = currIndex;
        nEndIndex = currIndex + maxRows30;
        
        // int nRowInxToVectorInx = 0;
        String[] strRowsTemp = new String[1];
        String[] strRows = (String[]) (theResSet.getRowData().toArray(strRowsTemp));
        
        // remove all the rows..
        m_itemSearchTable.removeAllRows();
        searchResultData result = new searchResultData(strRows, nStartIndex, nEndIndex, theResSet.getCols());
        m_itemSearchTable.addRow(result);
        
        return nEndIndex;
    }
    
    public int addRowsToTable(INOVResultSet theResSet, int currIndex, int pageMaxSize, int m_loadall)
    {
        assert (theResSet != null);
        
        int maxRows30 = pageMaxSize;
        int nStartIndex = 0;
        int nEndIndex = 0;
        
        int nTotalCount = theResSet.getRows();
        if (m_loadall >= 0)
        {
            if (maxRows30 > (theResSet.getRows() - currIndex))
            {
                maxRows30 = (theResSet.getRows() - currIndex);
            }
            
            // start Index
            nStartIndex = currIndex;
            nEndIndex = currIndex + maxRows30;
        }
        
        String[] strRowsTemp = new String[1];
        String[] strRows = (String[]) (theResSet.getRowData().toArray(strRowsTemp));
        // remove all the rows..
        m_itemSearchTable.removeAllRows();
        
        if (m_loadall < 0)
        {
            nStartIndex = 0;
            nEndIndex = 0;
            maxRows30 = pageMaxSize;
            while (nEndIndex < nTotalCount)
            {
                
                if (maxRows30 > (nTotalCount - currIndex))
                {
                    maxRows30 = (nTotalCount - currIndex);
                }
                
                // start Index
                nStartIndex = currIndex;
                nEndIndex = currIndex + maxRows30;
                currIndex = nEndIndex;
                
                searchResultData result = new searchResultData(strRows, nStartIndex, nEndIndex, theResSet.getCols());
                
                m_itemSearchTable.addRow(result);
            }
        }
        else
        {
            searchResultData result = new searchResultData(strRows, nStartIndex, nEndIndex, theResSet.getCols());
            m_itemSearchTable.addRow(result);
        }
        
        return nEndIndex;
    }
    
    public void dispose()
    {
        ///Unregister the Listeners
        IController theController = ControllerFactory.getInstance().getDefaultController();
//        IController theController = ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode());
        theController.unregisterSubscriber("ItemSearch", this);
        
        theController.unregisterSubscriber("NextResults", this);
        
        theController.unregisterSubscriber("PreviousResults", this);
        
        theController.unregisterSubscriber("LoadAllItems", this);
        super.dispose();
    }
    
    private void addMouseListenerOnTable(final Composite tablePanel) 
    {
    	m_itemSearchTable.addMouseListener(new MouseAdapter()
    	{
    		@Override
			public void mouseClicked(java.awt.event.MouseEvent mouseevent)
			{
				if(mouseevent.getClickCount() == 2)
				{
					 Display.getDefault().asyncExec(new Runnable()
			            {
			                public void run()
			                {
			                	tablePanel.getShell().dispose();
			                	//Tushar Start
								publishItemSelectionEvent();
								//Tushar End
			                }
			            });
				}
			}
		});
    	
	}
    
    public String getSearchContext()
    {
        return m_searchContext;
    }

    public void setSearchContext(String searchContext)
    {
        this.m_searchContext = searchContext;
    }
    
    public int getSelectionMode()
    {
        return m_selection;
    }

    public void setSelectionMode(int selection)
    {
        this.m_selection = selection;
    }
    
    NOVSearchTable m_itemSearchTable;

    //8677 start
    private void publishItemSelectionEvent()
    {
    	int[] iSelectdRows = m_itemSearchTable.getSelectedRows();
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        
        TCComponent[] selectedComponents = new TCComponent[iSelectdRows.length];
        
        for(int inx=0; inx < selectedComponents.length; inx++)
        {
            String sPUID = m_itemSearchTable.getValueAtDataModel(iSelectdRows[inx], 0);
            TCComponent theSelComponent = null;
            try
            {
                theSelComponent = session.stringToComponent(sPUID);
                selectedComponents[inx] = theSelComponent;
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent publishEvent = new PublishEvent(this, "DoubleClickedItem", selectedComponents, "");
        controller.publish(publishEvent);
	}
	//public void setSearchContext(String searchContext) 
	//{
	//	m_searchContext = searchContext;
	//}
	
	@Override
	public IPropertyMap getData()
	{
		// TODO Auto-generated method stub
		return null;
	}
	   //8677 end

	
    private int m_selection;
}
