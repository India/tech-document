package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;

public class PreviousButtonListener  implements SelectionListener, IPublisher
{
    /*private  IController controller = null;
    public IController getController()
    {
        return controller;
    }

    public void setController(IController controller)
    {
        this.controller = controller;
    }
*/
    public PreviousButtonListener()
    {
        super();
    }
  
    public IPropertyMap getData()
    {
        return null;
    }

    public void widgetSelected(SelectionEvent selectionevent)
    {
        publishData();
    }

    public void widgetDefaultSelected(SelectionEvent selectionevent)
    {
    }
    
    private void publishData()
    {
         IController controller = ControllerFactory.getInstance().getDefaultController();
//        if(null != controller)
//        {
            PublishEvent publishEvent = new PublishEvent(getPublisher(), "PreviousResults", "ResultsAvailable", "");
            
            controller.publish(publishEvent);
            
       // }
       
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    IPublisher getPublisher()
    {
        return this;
        
    }
}
