package com.nov.rac.item.itemsearch;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ListSelectionModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class ItemSearchDialog extends AbstractSWTDialog implements ISearchPropertiesProvider
{
    
    public ItemSearchDialog(final Shell shell, int style)
    {
        super(shell, style);
        m_registry = getRegistry();
    }

    private Registry getRegistry()
    {
        return Registry.getRegistry(this);
    }

    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.BOLD | SWT.BORDER);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
        composite.addDisposeListener( new DisposeListener()
        {
            @Override
            public void widgetDisposed(DisposeEvent disposeevent)
            {
                dispose();
            }
        });
        
       // m_titleComposite = new ItemSearchTitleComposite(composite, SWT.NONE);
        
        m_inputComposite = new ItemSearchInputComposite(composite, SWT.NONE);
        setItemSearchInputProperties();
        //m_inputComposite.setSearchItemType((String[])m_propertyMap.get(ISearchPropertiesProvider.ITEM_TYPES));
       // m_inputComposite.setOperationName((String)m_propertyMap.get(ISearchPropertiesProvider.OPERATION_CONTEXT));
        
        m_outputComposite = new ItemSearchResultComposite(composite, SWT.NONE);
        setItemSearchResultInputProperties();
        //m_outputComposite.setSearchContext((String)m_propertyMap.get(ISearchPropertiesProvider.SEARCH_CONTEXT));
        //m_outputComposite.setSelectionMode((Integer)m_propertyMap.get(ISearchPropertiesProvider.SELECTION_MODE));
        
        m_toolbarComposite = new ItemSerachToolBarComposite(composite, SWT.NONE);
        setItemSearchToolBarProperties();
        //m_toolbarComposite.setSelectionMode((Integer)m_propertyMap.get(ISearchPropertiesProvider.SELECTION_MODE));
        //m_toolbarComposite.setSearchContext((String)m_propertyMap.get(ISearchPropertiesProvider.SEARCH_CONTEXT));
        
        //8677
        //m_toolbarComposite.setSearchContext(m_searchContext);
        
        try
        {
           // m_titleComposite.createUI();
            m_inputComposite.createUI();
            m_outputComposite.createUI();
            m_toolbarComposite.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        m_toolbarComposite.setSearchProvider(m_itemSearchProvider);
        
        return parent;
    }
    
    private void setItemSearchInputProperties()
    {
        String[] itemSearchTypes = new String[]{} ;
        String operationName = "";
        
        if(m_propertyMap.get(ISearchPropertiesProvider.ITEM_TYPES) != null)
        {
            itemSearchTypes = (String[]) m_propertyMap.get(ISearchPropertiesProvider.ITEM_TYPES);
        }
        
        if(m_propertyMap.get(ISearchPropertiesProvider.OPERATION_CONTEXT) != null)
        {
            operationName = (String) m_propertyMap.get(ISearchPropertiesProvider.OPERATION_CONTEXT);
        }
        m_inputComposite.setSearchItemType(itemSearchTypes);
        m_inputComposite.setOperationName(operationName);
    }

    private void setItemSearchResultInputProperties()
    {
       String searchContext = "";
       int selectionMode = ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
       
       if(m_propertyMap.get(ISearchPropertiesProvider.SEARCH_CONTEXT) != null)
       {
           searchContext = (String) m_propertyMap.get(ISearchPropertiesProvider.SEARCH_CONTEXT);
       }
        
       if(m_propertyMap.get(ISearchPropertiesProvider.SELECTION_MODE) != null)
       {
           selectionMode = (Integer) m_propertyMap.get(ISearchPropertiesProvider.SELECTION_MODE);
       }
       
       m_outputComposite.setSearchContext(searchContext);
       m_outputComposite.setSelectionMode(selectionMode);
    }
    
    private void setItemSearchToolBarProperties()
    {
        String searchContext = "";
        Object source = null;
        
        if(m_propertyMap.get(ISearchPropertiesProvider.SEARCH_CONTEXT) != null)
        {
            searchContext = (String) m_propertyMap.get(ISearchPropertiesProvider.SEARCH_CONTEXT);
        }
        
        if(m_propertyMap.get(ISearchPropertiesProvider.BUTTON_SOURCE) != null)
        {
            source = m_propertyMap.get(ISearchPropertiesProvider.BUTTON_SOURCE);
        }
        m_toolbarComposite.setSearchContext(searchContext);
        m_toolbarComposite.setSource(source);
    }
    
    protected Control createButtonBar(Composite parent)
    {
        return null;
    }
    
    /*public void setSearchItemType(String[] serachItemTypeArr)
    {
        m_serachItemTypeArr = serachItemTypeArr;
    }*/
    //8677
    public void setSearchContext(String searchContext) 
    {
    	m_searchContext = searchContext;
    
	}
    
    public void setLocation(Point location, Point size)
    {
    	m_dialogLocation = location;
    	
    	m_searchBtnSize = size;
    }
    
    protected Point getInitialLocation(Point point)
    {
    	Point size = getInitialSize();
    	
    	Point location = new Point(m_dialogLocation.x, m_dialogLocation.y - size.y);
    	
    	if(location.y < 0)
    	{
    		location = new Point(m_dialogLocation.x, m_dialogLocation.y + m_searchBtnSize.y);
    	}
    	
    	return location;
    }
    
    //TCDECREL-9076 Configure title for item search dialog
    @Override
    protected void configureShell(Shell newShell) 
    {
      super.configureShell(newShell);
      newShell.setText(m_registry.getString("ItemSearch.TITLE"));
    }
    
    public void dispose()
    {
        m_propertyMap.clear();
        m_outputComposite.dispose();
        m_toolbarComposite.dispose();
    }
    
    @Override
    public Object getProperty(String propName)
    {
        return m_propertyMap.get(propName);
    }

    @Override
    public void setProperty(String propName, Object propValue)
    {
        m_propertyMap.put(propName, propValue);
    }
    
    //private ItemSearchTitleComposite m_titleComposite;
    private ItemSearchInputComposite m_inputComposite;
    private ItemSearchResultComposite m_outputComposite;
    private ItemSerachToolBarComposite m_toolbarComposite;
    private ItemIDSearchProvider m_itemSearchProvider;
    
    private String m_searchContext;
    String[] m_serachItemTypeArr;
    
    Point m_dialogLocation;
    Point m_searchBtnSize;
    private Map<String,Object> m_propertyMap = new HashMap<String, Object>();
    private Registry m_registry = null;

}
