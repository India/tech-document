package com.nov.rac.item.itemsearch;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.quicksearch.services.utils.NOVSearchErrorCodes;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class LoadAllButtonListener implements SelectionListener, IPublisher
{
    /*private  IController controller = null;
    public IController getController()
    {
        return controller;
    }

    public void setController(IController controller)
    {
        this.controller = controller;
    }
*/
    public LoadAllButtonListener(INOVSearchProvider2 searchProvider)
    {
        super();
        m_searchProvider = searchProvider;
    }
   
    public IPropertyMap getData()
    {
        return null;
    }

    public void widgetSelected(SelectionEvent selectionevent)
    {
        execute();

        publishData();
    }

    public void widgetDefaultSelected(SelectionEvent selectionevent)
    {
    }
    
    protected void execute()
    {
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        
        dataModel.setResponseIndex(0);
        INOVResultSet resultSet = dataModel.getSearchResult().getResultSet();

        if(null!=resultSet)
        {
            resultSet.clear();
        }

        try
        {
            if( m_searchProvider != null )
            {
                if( m_searchProvider.validateInput() == false )
                {
                    return;
                }
                m_searchResult = NOVSearchExecuteHelperUtils.execute(m_searchProvider, INOVSearchProvider.INIT_LOAD_ALL);
            }
        }
        catch( TCException tcExp )
        {
            if( tcExp.getErrorCode() == NOVSearchErrorCodes.TOO_MANY_OBJECTS_FOUND.getErrorCode() )
            {
                String str = tcExp.getMessage();
                Registry theReg = Registry.getRegistry( this );
                String strErrorText = theReg.getString( "TOO_MANY_OBJECTS_FOUND.ERROR" );
                
                str = str + ", " + strErrorText; 

                boolean bResult = MessageDialog.openConfirm( AIFUtility.getActiveDesktop().getShell(),
                        theReg.getString( "ItemSearch.TITLE" ), str ); 
                if(bResult)
                {
                  try 
                  {
                      m_searchResult = NOVSearchExecuteHelperUtils.execute( m_searchProvider, INOVSearchProvider.LOAD_ALL );
                  } 
                  catch (Exception e1) 
                  {
                     e1.printStackTrace();
                  } 
                }               
            }
        }
        catch(Exception e) 
        {
            e.printStackTrace();
        }
    }

    private void publishData()
    {
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        dataModel.setSearchResult(m_searchResult);
        IController controller = ControllerFactory.getInstance().getDefaultController();
//        if(null != controller)
//        {
            PublishEvent publishEvent = new PublishEvent(getPublisher(), "LoadAllItems", "ResultsAvailable", "");
            controller.publish(publishEvent);
        //}
       
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    IPublisher getPublisher()
    {
        return this;
    }

    private INOVSearchProvider2 m_searchProvider;
    private INOVSearchResult m_searchResult = null;
}
