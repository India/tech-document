package com.nov.rac.item.itemsearch;


public class SearchResultBean
{
    private Object m_tcCompoObject = null;
    private Object m_source = null;
    
    public Object getSearchResult()
    {
        return m_tcCompoObject;
    }
    public void setSearchResult(Object resultObject)
    {
        m_tcCompoObject = resultObject;
    }
    
    public Object getSource()
    {
        return m_source;
    }
    public void setSource(Object source)
    {
        this.m_source = source;
    }
}
