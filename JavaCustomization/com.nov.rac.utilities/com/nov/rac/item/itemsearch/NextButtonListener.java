package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;

public class NextButtonListener implements SelectionListener, IPublisher
{
    /*private  IController controller = null;
    public IController getController()
    {
        return controller;
    }

    public void setController(IController controller)
    {
        this.controller = controller;
    }*/
    public NextButtonListener(INOVSearchProvider2 searchProvider)
    {
        super();
        
        m_searchProvider = searchProvider;
    }
    
    public IPropertyMap getData()
    {
        return null;
    }

    public void widgetSelected(SelectionEvent selectionevent)
    {
        execute();

        publishData();
    }

    public void widgetDefaultSelected(SelectionEvent selectionevent)
    {
    }
    
    protected void execute()
    {
        ItemSearchDataModel dataModel = ItemSearchDataModel.getInstance();
        
        int responseIndex = dataModel.getResponseIndex();
        m_searchResult = dataModel.getSearchResult();
        
        // KLOC214
        if(null != m_searchResult && ( responseIndex + 1 ) >= m_searchResult.getResultSet().getRows() )
        {
            try
            {
                if( m_searchProvider != null )
                {
                    INOVSearchResult searchResult = NOVSearchExecuteHelperUtils.execute(m_searchProvider, responseIndex);
                    INOVResultSet resultSet = searchResult.getResultSet();
                    String[] rowData = resultSet.getRowData().toArray( new String[resultSet.getRowData().size()] );
                    m_searchResult.getResultSet().add(rowData, resultSet.getRows());
                    dataModel.setSearchResult(m_searchResult);
                }
            }
            catch( Exception exception ) 
            {
                exception.printStackTrace();
            }
        }
    }
    
    private void publishData()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
//        if(null != controller)
//        {
            
            PublishEvent publishEvent = new PublishEvent(getPublisher(), "NextResults", "ResultsAvailable", "");
            
            controller.publish(publishEvent);
        //}
    }
    
    /**
     * Gets the publisher.
     * 
     * @return IPublisher
     */
    IPublisher getPublisher()
    {
        return this;
    }
    
    private INOVSearchProvider2 m_searchProvider;
    
    private INOVSearchResult m_searchResult = null;
}
