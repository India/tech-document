package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCComponent;

public class ButtonListener implements SelectionListener, IPublisher,ISubscriber
{

    private TCComponent[] m_selectedItems = null;
    private Object m_source = null;
    
    public ButtonListener ()
    {
        registerSubscriber();
    }
    
    public void registerSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.registerSubscriber("SelectedItem", this);
    }
    
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }

    @Override
    public void widgetDefaultSelected(SelectionEvent arg0)
    {
        
    }

    @Override
    public void widgetSelected(SelectionEvent arg0)
    {
        publishData();
    }

    @Override
    public void update(PublishEvent event)
    {
        m_selectedItems = (TCComponent[]) event.getNewValue();
    }
    
    public void unregisterSubscriber()
    {
        IController theController = ControllerFactory.getInstance().getDefaultController();
        theController.unregisterSubscriber("SelectedItem", this);
    }
    
    protected void publishData()
    {
        SearchResultBean searchResult = new SearchResultBean();
        searchResult.setSearchResult(m_selectedItems);
        searchResult.setSource(m_source);
        IController controller = ControllerFactory.getInstance().getDefaultController();
        PublishEvent publishEvent = new PublishEvent(this,"SEARCH_RESULT",searchResult ,null);
            controller.publish(publishEvent);
    }

    public void setSource(Object source)
    {
        m_source = source;
    }
}
