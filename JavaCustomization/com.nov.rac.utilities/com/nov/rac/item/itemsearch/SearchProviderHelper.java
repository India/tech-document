package com.nov.rac.item.itemsearch;

import com.nov.rac.utilities.common.GroupUtils;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.util.Registry;

public class SearchProviderHelper
{
    /*public static String getSearchProvider()
    {
        String searchProviderClass = null;
        Registry itemSrchRegistry = Registry.getRegistry("com.nov.rac.item.itemsearch.itemsearch");
        
        String operationName =  NewItemCreationHelper.getSelectedOperation();
        String businessGroup =  NewItemCreationHelper.getBusinessGroup();
        
        String[] configParams = RegistryUtils.getConfiguration(businessGroup, operationName
                + "." + "ITEM_SEARCH_PROVIDER", itemSrchRegistry);
        if (configParams != null && configParams.length > 0)
        {
            searchProviderClass = configParams[0];
        }
        else if (configParams == null || configParams.length == 0)
        {
            configParams = RegistryUtils.getConfiguration(businessGroup, "ITEM_SEARCH_PROVIDER", itemSrchRegistry);
            searchProviderClass = configParams[0];
        }
        return searchProviderClass;
    }*/
    
    public static String getSearchProvider(String operationName)
    {
        String searchProviderClass = null;
        String[] configParams = null;
        Registry itemSrchRegistry = Registry.getRegistry("com.nov.rac.item.itemsearch.itemsearch");
        
        String businessGroup =  GroupUtils.getBusinessGroupName();
        
        if(operationName != null && !operationName.equals(""))
        {
            configParams = RegistryUtils.getConfiguration(businessGroup, operationName
                    + "." + "ITEM_SEARCH_PROVIDER", itemSrchRegistry);
        }
        
        if (configParams != null && configParams.length > 0)
        {
            searchProviderClass = configParams[0];
        }
        else if (configParams == null || configParams.length == 0)
        {
            configParams = RegistryUtils.getConfiguration(businessGroup, "ITEM_SEARCH_PROVIDER", itemSrchRegistry);
            searchProviderClass = configParams[0];
        }
        return searchProviderClass;
    }
}
