package com.nov.rac.item.itemsearch;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;


public class CopyToClipboardListener implements SelectionListener
{   
	//8677
//    public CopyToClipboardListener()
	public CopyToClipboardListener(InterfaceAIFComponent ainterfaceaifcomponent[])
	{
        super();
        
        //8677
        m_ainterfaceaifcomponent = ainterfaceaifcomponent;
    }
    
    public void widgetDefaultSelected(SelectionEvent arg0)
    {
    }
    
    public void widgetSelected(SelectionEvent arg0)
    {
        execute();
    }
    
    protected void execute()
    {
        try
        {
            Registry registry = Registry.getRegistry("com.teamcenter.rac.common.actions.actions"); 
            AbstractAIFCommand abstractaifcommand = (AbstractAIFCommand) registry.newInstanceFor("copyCommand",
                    new Object[] { m_ainterfaceaifcomponent });
            abstractaifcommand.executeModal();
        }
        catch (Exception exception)
        {
            MessageBox.post(exception);
        }
    }
    
    InterfaceAIFComponent m_ainterfaceaifcomponent[];
}