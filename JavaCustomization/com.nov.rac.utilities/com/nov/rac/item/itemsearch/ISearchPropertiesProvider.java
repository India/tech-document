package com.nov.rac.item.itemsearch;


public interface ISearchPropertiesProvider
{
    String OPERATION_CONTEXT = "operation-context";
    String ITEM_TYPES = "item_types";
    String SEARCH_CONTEXT = "search_context";
    String REGISTRY = "registry";
    String SELECTION_MODE = "selection_mode";
    String BUTTON_SOURCE = "source";
    
    Object getProperty(String propName);
    void setProperty(String propName,Object propValue);
    
    //remove all below functions
   // void setRegistry(Registry registry);
    //void setSearchItemType(String[] serachItemTypeArr);
    //void setSearchContext(String searchContext);
   // void setOperationContext(String operationContext);
   // void setSelectionMode(int selection);
    
    //constants
    // OPERATION_CONTEXT
    //ITEM_TYPES
    //SEARCH_CONTEXT
    //REGISTRY
    //SELECTIONMODE
    
    //Object getProperty(String propName)
    // void setProperty(String propName,Object propValue)
}
