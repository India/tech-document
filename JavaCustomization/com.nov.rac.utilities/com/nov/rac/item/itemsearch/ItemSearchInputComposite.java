package com.nov.rac.item.itemsearch;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVSearchProvider2;
import com.nov.rac.ui.AbstractUIPanel;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class ItemSearchInputComposite extends AbstractUIPanel implements INOVSearchProvider2
{
    public ItemSearchInputComposite(Composite parent, int style)
    {
        super(parent, style);
        
        m_registry = getRegistry();
    }
    
    Registry getRegistry()
    {
        return Registry.getRegistry(this);
    }
    
    public boolean createUI() throws TCException
    {
        Composite parent = getComposite();
        
        parent.setLayout(new GridLayout(3, false));
        GridData gdParent = new GridData(SWT.NONE, SWT.TOP, false, false, 1, 1);
        parent.setLayoutData(gdParent);
        
        m_itemIDLabel = new Label(parent, SWT.NONE);
        m_itemIDLabel.setText(m_registry.getString("ItemSearch.ItemID"));
        
        m_itemIDText = new Text(parent, SWT.BORDER);
        int width = m_itemIDText.getMonitor().getBounds().width / 10;
        GridData gdItemID = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gdItemID.widthHint = width;
        m_itemIDText.setLayoutData(gdItemID);
        new Label(parent, SWT.NONE);
        
        m_itemNameLabel = new Label(parent, SWT.NONE);
        m_itemNameLabel.setText(m_registry.getString("ItemSearch.ItemName"));
        
        m_itemNameText = new Text(parent, SWT.BORDER);
        GridData gdItemName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gdItemName.widthHint = width;
        m_itemNameText.setLayoutData(gdItemName);
        
        m_findButton = new Button(parent, SWT.NONE);
        GridData gdFindButton = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gdFindButton.widthHint = 61;
        m_findButton.setLayoutData(gdFindButton);
        m_findButton.setText(m_registry.getString("ItemSearch.FindBtnName"));
        
        FindButtonListener findButtonListener = new FindButtonListener(this);
        //findButtonListener.setController(ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode()));
        m_findButton.addSelectionListener(findButtonListener);
        
        IExecutor keyboardListener = new KeyboardListener(this);
        //keyboardListener.setController(ControllerFactory.getInstance().getController(""+getComposite().getShell().hashCode()));
        m_itemIDText.addKeyListener((KeyboardListener) keyboardListener);
        m_itemNameText.addKeyListener((KeyboardListener) keyboardListener);
        
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    public String getBusinessObject()
    {
        return "Item";
    }
    
    public void setSearchItemType(String[] serachItemTypeArr)
    {
        int size = serachItemTypeArr.length;
        m_serachItemTypeArr = new String[size];
        
        for (int index = 0; index < size; ++index)
        {
            m_serachItemTypeArr[index] = serachItemTypeArr[index];
        }
    }
    
    public QuickBindVariable[] getBindVariables()
    {
        if (m_strItemID == null || m_strItemID.isEmpty())
        {
            m_strItemID = "*";
        }
        
        if (m_strItemName == null || m_strItemName.isEmpty())
        {
            m_strItemName = "*";
        }
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[12];
        
        // Item.item_id
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_string;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { m_strItemID };
        
        // Item.object_name
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { m_strItemName };
        
        // Item.object_type
        allBindVars[2] = new QuickSearchService.QuickBindVariable();
        allBindVars[2].nVarType = POM_string;
        allBindVars[2].nVarSize = 1;
        allBindVars[2].strList = m_serachItemTypeArr;
        
        //m_sCreatedAfter,m_sCreatedBefore              
        allBindVars[3] = new QuickSearchService.QuickBindVariable();
        allBindVars[3].nVarType = POM_date;
        allBindVars[3].nVarSize = 2;
        allBindVars[3].strList = new String[] {"*","*"};
        
        //m_sItemDesc
        allBindVars[4] = new QuickSearchService.QuickBindVariable();
        allBindVars[4].nVarType = POM_string;
        allBindVars[4].nVarSize = 1;
        allBindVars[4].strList = new String[] {"*"};
        
        //m_sReleaseStatus
        allBindVars[5] = new QuickSearchService.QuickBindVariable();
        allBindVars[5].nVarType = POM_string;
        allBindVars[5].nVarSize = 1;
        allBindVars[5].strList = new String[] {"*"};
        
        //m_sOwningUser
        allBindVars[6] = new QuickSearchService.QuickBindVariable();
        allBindVars[6].nVarType = POM_string;
        allBindVars[6].nVarSize = 1;
        allBindVars[6].strList = new String[] {"*"};
    
        //m_grpPuid
        allBindVars[7] = new QuickSearchService.QuickBindVariable();
        allBindVars[7].nVarType = POM_typed_reference;
        allBindVars[7].nVarSize = 1;
        allBindVars[7].strList = new String[] { "" };
        
        //RSOne 4 alternate ID
        allBindVars[8] = new QuickSearchService.QuickBindVariable();
        allBindVars[8].nVarType = POM_string;
        allBindVars[8].nVarSize = 2;
        allBindVars[8].strList = new String[] { "RSOne" , "JDE"};
        
        //m_sLastOwningUser
        allBindVars[9] = new QuickSearchService.QuickBindVariable();
        allBindVars[9].nVarType = POM_string;
        allBindVars[9].nVarSize = 1;
        allBindVars[9].strList = new String[] {"*"};
        
        //Send Alternate ID selection
        allBindVars[10] = new QuickSearchService.QuickBindVariable();
        allBindVars[10].nVarType = POM_string;
        allBindVars[10].nVarSize = 1;
        allBindVars[10].strList = new String[] {"*"};
        
     // TCDECREL-4832 Added additional bind variable for release status
		allBindVars[11] = new QuickSearchService.QuickBindVariable();
        allBindVars[11].nVarType = POM_string;
        allBindVars[11].nVarSize = 1;
        allBindVars[11].strList = new String[] {"*"};

        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo() 
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[5];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-partdoc-items";
        allHandlerInfo[0].listBindIndex = new int[] {1,2,3,4,5,6,7,8,10,11,12};       
        //TCDECREL-9077
        allHandlerInfo[0].listReqdColumnIndex = new int[]{1, 2, 3,5, 7};
        allHandlerInfo[0].listInsertAtIndex = new int[] {5,1,3,7,8};
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[1].handlerName = "NOVSRCH-get-nov4part-master-props";
		allHandlerInfo[1].listBindIndex = new int[0];			
		allHandlerInfo[1].listReqdColumnIndex = new int[]{1};		
		//TCDECREL-9077
		allHandlerInfo[1].listInsertAtIndex = new int[]{6};
		
		allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-nov4part-latest-revs";
        allHandlerInfo[2].listBindIndex = new int[0];
        allHandlerInfo[2].listReqdColumnIndex = new int[] { 1 };    
        //TCDECREL-9077 
        allHandlerInfo[2].listInsertAtIndex = new int[]{4};
        
        allHandlerInfo[3] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[3].handlerName = "NOVSRCH-get-alternate-ids";
        allHandlerInfo[3].listBindIndex = new int[] { 9 };
        allHandlerInfo[3].listReqdColumnIndex = new int[] { 1 };
        allHandlerInfo[3].listInsertAtIndex = new int[] { 2 };
        
        allHandlerInfo[4] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[4].handlerName = "NOVSRCH-get-release-status";
        allHandlerInfo[4].listBindIndex = new int[0];
        allHandlerInfo[4].listReqdColumnIndex = new int[] { 2 };
        //TCDECREL-9077
        allHandlerInfo[4].listInsertAtIndex = new int[] { 9 };
            
        return allHandlerInfo;
    }
    
    // TCDECREL-7419 Set search criteria to minimum 3 characters
    public boolean validateInput()
    {
        boolean retValue = true;
        //if wild card exists in search string, remove the wild cards and verify string length.
        //it should be min. 3 characters long. Else throw error.
        String inputId = "";
        String inputName = "";
        
        m_strItemID = m_itemIDText.getText().trim();
        m_strItemName = m_itemNameText.getText().trim();
        
        char[] itemIdArr = m_itemIDText.getText().trim().toCharArray();
        char[] itemNameArr = m_itemNameText.getText().trim().toCharArray();
        
        for( int i = 0; i < itemIdArr.length; i++)
        {
                if(itemIdArr[i] != '*')
                {
                        inputId = inputId + itemIdArr[i];
                }
        }
        
        for( int i = 0; i < itemNameArr.length; i++)
        {
                if(itemNameArr[i] != '*')
                {
                        inputName = inputName + itemNameArr[i];
                }
        }
              
        if(inputId.trim().length() < 3 && inputName.trim().length() < 3)
        {
            MessageBox.post( "Please enter minimum 3 characters for search criteria", 
                    "Error", MessageBox.ERROR);
            retValue = false;
        } 
            
        return retValue;
    }
    // TCDECREL-7419 end
    
	public String getOperationName()
    {
        return m_operationName;
    }

    public void setOperationName(String operationName)
    {
        this.m_operationName = operationName;
    }
	
    Label m_itemIDLabel;
    Label m_itemNameLabel;
    
    Text m_itemIDText;
    Text m_itemNameText;
    
    String m_strItemID;
    String m_strItemName;
    
    String[] m_serachItemTypeArr;
    
    Button m_findButton;
    
    Registry m_registry = null;
	private String m_operationName;
}
