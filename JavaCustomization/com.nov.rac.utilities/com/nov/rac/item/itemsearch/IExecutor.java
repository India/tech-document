package com.nov.rac.item.itemsearch;


public interface IExecutor
{
    public void execute();
    
    public void publishData();
    
//    public IController getController();
//
//    public void setController(IController controller);
}
