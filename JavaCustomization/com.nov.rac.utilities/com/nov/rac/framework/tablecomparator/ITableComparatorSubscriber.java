package com.nov.rac.framework.tablecomparator;

import com.nov.rac.framework.communication.PublishEvent;

public interface ITableComparatorSubscriber
{
    public void updateTable(PublishEvent event);
}
