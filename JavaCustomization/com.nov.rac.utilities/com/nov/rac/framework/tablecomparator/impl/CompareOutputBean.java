package com.nov.rac.framework.tablecomparator.impl;

import java.util.ArrayList;
import java.util.List;

public class CompareOutputBean
{
    private List<Integer> addedRows = new ArrayList<Integer>();
    private List<Integer> removedRows = new ArrayList<Integer>();
    private List<Integer> updatedRows = new ArrayList<Integer>();
    private List<Integer> updatedColumns = new ArrayList<Integer>();

    private Object object = null;
    
    public CompareOutputBean()
    {
        
    }
    
    public Object getObject()
    {
        return object;
    }

    public void setObject(Object object)
    {
        this.object = object;
    }
    
    public List<Integer> getAddedRows()
    {
        return addedRows;
    }

    public void setAddedRows(List<Integer> addedRows)
    {
        this.addedRows = addedRows;
    }

    public List<Integer> getRemovedRows()
    {
        return removedRows;
    }

    public void setRemovedRows(List<Integer> removedRows)
    {
        this.removedRows = removedRows;
    }

    public List<Integer> getUpdatedRows()
    {
        return updatedRows;
    }

    public void setUpdatedRows(List<Integer> updatedRows)
    {
        this.updatedRows = updatedRows;
    }

    public List<Integer> getUpdatedColumns()
    {
        return updatedColumns;
    }

    public void setUpdatedColumns(List<Integer> updatedColumns)
    {
        this.updatedColumns = updatedColumns;
    }
}
