package com.nov.rac.framework.tablecomparator.impl;

import java.util.Comparator;

import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.teamcenter.rac.common.TCTable;

public interface IDataModel
{
    /*
     * IDataModel createContext(String context, String primaryKeyColumn,
     * String[] tableColumns, TCTable table, Comparator comparator, Object
     * compareObject,ITableComparatorSubscriber subscriber);
     */
    
    String getContext();
    
    TCTable getTable();
    
    String[] getTableColumns();
    
    Object getCompareObject();
    
    Comparator<Object> getObjectComparator();
    
    String getPrimaryKeyColumn();
    
    ITableComparatorSubscriber getSubscriber();
    
    void setPrimaryKeyColumn(String primaryKeyColumn);
    
    void setComparator(Comparator<Object> comparator);
    
    void setCompareObject(Object object);
    
    void setTableColumns(String[] tableColumns);
    
    void setSubscriber(ITableComparatorSubscriber subscriber);
    
    void setTable(TCTable table);
    
    // void clear();
}
