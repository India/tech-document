package com.nov.rac.framework.tablecomparator.impl;

import java.util.Comparator;

import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.teamcenter.rac.common.TCTable;

public class CompareInfo
{
    
    private TCTable m_firstTable = null;
    private TCTable m_secondTable = null;
    
    private Comparator<Object> m_comparator = null;
    private Object m_firstObject = null;
    private Object m_secondObject = null;
    
    private String[] m_columnNames = null;
    private String m_primaryKey = null;
    private ITableComparatorSubscriber m_firstSubscriber = null;
    private ITableComparatorSubscriber m_secondSubscriber = null;
    
    public CompareInfo()
    {
        
    }
    
    public TCTable getFirstTable()
    {
        return m_firstTable;
    }
    
    public void setFirstTable(TCTable m_firstTable)
    {
        this.m_firstTable = m_firstTable;
    }
    
    public TCTable getSecondTable()
    {
        return m_secondTable;
    }
    
    public void setSecondTable(TCTable m_secondTable)
    {
        this.m_secondTable = m_secondTable;
    }
    
    public Comparator<Object> getComparator()
    {
        return m_comparator;
    }
    
    public void setComparator(Comparator<Object> m_comparator)
    {
        this.m_comparator = m_comparator;
    }
    
    public Object getFirstObject()
    {
        return m_firstObject;
    }
    
    public void setFirstObject(Object m_firstObject)
    {
        this.m_firstObject = m_firstObject;
    }
    
    public Object getSecondObject()
    {
        return m_secondObject;
    }
    
    public void setSecondObject(Object m_secondObject)
    {
        this.m_secondObject = m_secondObject;
    }
    
    public String[] getColumnNames()
    {
        return m_columnNames;
    }
    
    public void setColumnNames(String[] m_columnNames)
    {
        this.m_columnNames = m_columnNames;
    }
    
    public String getPrimaryKey()
    {
        return m_primaryKey;
    }
    
    public void setPrimaryKey(String m_primaryKey)
    {
        this.m_primaryKey = m_primaryKey;
    }
    
    public ITableComparatorSubscriber getFirstSubscriber()
    {
        return m_firstSubscriber;
    }
    
    public void setFirstSubscriber(ITableComparatorSubscriber m_firstSubscriber)
    {
        this.m_firstSubscriber = m_firstSubscriber;
    }
    
    public ITableComparatorSubscriber getSecondSubscriber()
    {
        return m_secondSubscriber;
    }
    
    public void setSecondSubscriber(
            ITableComparatorSubscriber m_secondSubscriber)
    {
        this.m_secondSubscriber = m_secondSubscriber;
    }
    
    public boolean isComparable()
    {
        boolean isComparable = false;
        
        if (m_firstTable != null && m_secondTable != null
                && m_firstSubscriber != null && m_secondSubscriber != null
                && m_primaryKey != null && m_columnNames != null)
        {
            
            isComparable = true;
            
        }
        return isComparable;
    }
    
    public void add(IDataModel dataModel)
    {
        // 1.0 First case : Add for first table.
        TCTable modelTable = dataModel.getTable();
        if (getFirstTable() == null
                || (modelTable != null && modelTable.equals(getFirstTable())))
        {
            addToFirst(dataModel);
        }
        // 2.0 Second Case : Add to second table.
        else if (getSecondTable() == null
                || (modelTable != null && modelTable.equals(getSecondTable())))
        {
            addToSecond(dataModel);
        }
    }
    
    public void reset()
    {
        setFirstTable(null);
        setFirstObject(null);
        setFirstSubscriber(null);
        setComparator(null);
        setPrimaryKey(null);
        setColumnNames(null);
    }
    
    private void addToSecond(IDataModel dataModel)
    {
        setSecondTable(dataModel.getTable());
        setSecondObject(dataModel.getCompareObject());
        setSecondSubscriber(dataModel.getSubscriber());
    }
    
    private void addToFirst(IDataModel dataModel)
    {
        setFirstTable(dataModel.getTable());
        setFirstObject(dataModel.getCompareObject());
        setFirstSubscriber(dataModel.getSubscriber());
        setComparator(dataModel.getObjectComparator());
        setPrimaryKey(dataModel.getPrimaryKeyColumn());
        setColumnNames(dataModel.getTableColumns());
    }
    
    
}
