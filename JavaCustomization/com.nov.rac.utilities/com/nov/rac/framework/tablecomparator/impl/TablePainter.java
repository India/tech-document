package com.nov.rac.framework.tablecomparator.impl;

import java.util.List;

import com.teamcenter.rac.common.TCTable;

public class TablePainter
{
    private CompareOutputBean m_resultSet = null;
    private static final String ADD = "add";
    private static final String REMOVE = "remove";
    private static final String MODIFY = "modify";
    private static final String BLANK = "";
    private boolean m_shouldReset = true;
    
    public TablePainter(CompareOutputBean resultSet)
    {
        this.m_resultSet = resultSet;
    }
    
    public void paintTable(TCTable table)
    {
        int statusColumnIndex = table.getColumnIndex("status");
        if (m_resultSet != null)
        {
            add(table, statusColumnIndex);
            remove(table, statusColumnIndex);
            update(table, statusColumnIndex);
        }
        
        reset(table, statusColumnIndex);
        table.repaint();
    }
    
    private void add(TCTable table, int statusColIdx)
    {
        for (Object row : m_resultSet.getAddedRows())
        {
            table.getModel().setValueAt(ADD, (Integer) row, statusColIdx);
            m_shouldReset = false;
        }
    }
    
    private void remove(TCTable table, int statusColIdx)
    {
        for (Object row : m_resultSet.getRemovedRows())
        {
            table.getModel().setValueAt(REMOVE, (Integer) row, statusColIdx);
            m_shouldReset = false;
        }
    }
    
    private void update(TCTable table, int statusColIdx)
    {
        /*List<Integer> updatedRows = m_resultSet.getUpdatedRows();
        List<Integer> updatedColumns = m_resultSet.getUpdatedColumns();
        
        for(int inx = 0; inx < updatedRows.size(); inx++)
        {
            table.getModel().setValueAt(MODIFY+"-"+updatedColumns.get(inx), updatedRows.get(inx), statusColIdx);
            m_shouldReset = false;
        }*/
        for (Object row : m_resultSet.getUpdatedRows())
        {
            table.getModel().setValueAt(MODIFY, (Integer) row, statusColIdx);
            m_shouldReset = false;
        }
    }
    
    private void reset(TCTable table, int statusColumnIndex)
    {
        if(m_shouldReset)
        {
            int rowCount = table.getRowCount();
            
            for (int inx = 0; inx < rowCount; inx++)
            {
                table.getModel().setValueAt(BLANK, inx, statusColumnIndex);
            }
        }
    }
}
