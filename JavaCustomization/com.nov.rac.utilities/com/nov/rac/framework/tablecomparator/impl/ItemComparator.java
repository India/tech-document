package com.nov.rac.framework.tablecomparator.impl;

import java.util.Comparator;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class ItemComparator implements Comparator
{

    @Override
    public int compare(Object object1, Object object2)
    {
        int index1 = 0;
        int index2 = 0;
        
        if(object1 instanceof TCComponent && object2 instanceof TCComponent)
        {
            TCComponent tcComponent1 = (TCComponent)object1;
            TCComponent tcComponent2 = (TCComponent)object2;
            TCComponent[] revisionArray = getItemRevisions(tcComponent1);
            
            for(int inx=0; inx < revisionArray.length; inx++)
            {
                TCComponent tcComponent3 = revisionArray[inx];
                if(tcComponent3.equals(tcComponent1))
                {
                    index1 = inx;
                }
                
                if(tcComponent3.equals(tcComponent2))
                {
                    index2 = inx;
                }
            }
            
        }
        return (new Integer(index1).compareTo(new Integer(index2)));
    }
    
    private TCComponent[] getItemRevisions(TCComponent tcComponent) 
    {
        TCComponent[] revisionList = null;
        try
        {
            TCComponentItemRevision tcComponentItemRevision = (TCComponentItemRevision)tcComponent;
            TCComponentItem tcComponentItem = tcComponentItemRevision.getItem();
            revisionList = tcComponentItem
                    .getReferenceListProperty("revision_list");
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return revisionList;
    }
    
}
