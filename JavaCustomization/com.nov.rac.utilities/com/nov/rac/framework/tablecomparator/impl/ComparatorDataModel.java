package com.nov.rac.framework.tablecomparator.impl;

import java.util.Comparator;

import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.teamcenter.rac.common.TCTable;

public class ComparatorDataModel implements IDataModel
{
    private ITableComparatorSubscriber m_subscriber;
    
    private TCTable m_table;
    
    private String[] m_tableColumns;
    
    private Object m_object;
    
    private Comparator<Object> m_comparator;

    private String m_primaryKeyColumn = null;
    
    private String m_context = null;
    
    public ComparatorDataModel()
    {
        
    }
    
    /*
     * public IDataModel getDefaultDataModel(String context) { if(m_container ==
     * null) { m_container = new ConcurrentHashMap<String, IDataModel>(); } if
     * (m_container.containsKey(context)) { m_dataModel =
     * m_container.get(context); } else { m_dataModel = new
     * ComparatorDataModel(context); m_container.put(context, m_dataModel); }
     * return m_dataModel; }
     */
    
    /*
     * private void initializeCollection() { m_context = new
     * ArrayList<String>(); m_subscribers = new ConcurrentHashMap<String,
     * List<ITableComparatorSubscriber>>(); m_tables = new
     * ConcurrentHashMap<String, List<TCTable>>(); m_tableColumns = new
     * ConcurrentHashMap<String, String[]>(); m_objects = new
     * ConcurrentHashMap<String, List<Object>>(); m_revisionComparator = new
     * ConcurrentHashMap<String, Comparator<Object>>(); m_primaryKeyColumn = new
     * ConcurrentHashMap<String, String>(); }
     */
    
    @Override
    public ITableComparatorSubscriber getSubscriber()
    {
        return m_subscriber;
    }
    
    @Override
    public String getContext()
    {
        return m_context;
    }
    
    @Override
    public TCTable getTable()
    {
        return m_table;
    }
    
    @Override
    public String[] getTableColumns()
    {
        return m_tableColumns;
    }
    
    @Override
    public Object getCompareObject()
    {
        return m_object;
    }
    
    @Override
    public Comparator<Object> getObjectComparator()
    {
        return m_comparator;
    }
    
    @Override
    public String getPrimaryKeyColumn()
    {
        return m_primaryKeyColumn;
    }
    
    /*
     * @Override public IDataModel createContext(String context, String
     * primaryKeyColumn, String[] tableColumns, TCTable table, Comparator
     * comparator,Object compareObject, ITableComparatorSubscriber subscriber) {
     * // setContext(context); setPrimaryKeyColumn(context, primaryKeyColumn);
     * setTableColumns(context, tableColumns); setTables(context, table);
     * setComparator(context, comparator); setCompareItems(context,comparator);
     * setSubscriber(context, subscriber); return m_dataModel; }
     */
    
    public void setContext(String context)
    {
        this.m_context = context;
    }
    
    public void setPrimaryKeyColumn(String primaryKeyColumn)
    {
        this.m_primaryKeyColumn = primaryKeyColumn;
    }
    
    public void setComparator(Comparator<Object> comparator)
    {
        this.m_comparator = comparator;
    }
    
    public void setCompareObject(Object object)
    {
        this.m_object = object;
    }
    
    public void setTableColumns(String[] tableColumns)
    {
        this.m_tableColumns = tableColumns;
    }
    
    public void setSubscriber(ITableComparatorSubscriber subscriber)
    {
        this.m_subscriber = subscriber;
    }
    
    public void setTable(TCTable table)
    {
        this.m_table = table;
    }
    
    /*
     * public void clear() { m_context.clear(); m_container.clear();
     * m_subscribers.clear(); m_tables.clear(); m_tableColumns.clear();
     * m_objects.clear(); m_revisionComparator.clear();
     * m_primaryKeyColumn.clear(); }
     */
}
