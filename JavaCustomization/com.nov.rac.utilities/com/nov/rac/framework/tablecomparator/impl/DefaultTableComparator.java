package com.nov.rac.framework.tablecomparator.impl;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.framework.tablecomparator.ITableComparator;
import com.nov.rac.framework.tablecomparator.ITableComparatorSubscriber;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.common.TCTable;

public class DefaultTableComparator implements ITableComparator, IPublisher
{
    private Map<String, CompareInfo> m_container = new ConcurrentHashMap<String, CompareInfo>();
    
    private static ITableComparator controller = null;
    
    public DefaultTableComparator()
    {
        
    }
    
    public static ITableComparator getController()
    {
        if (controller == null)
        {
            controller = new DefaultTableComparator();
        }
        return controller;
    }
    
    public void setContext(String context, IDataModel inputModel)
    {
        if (!m_container.containsKey(context))
        {
            CompareInfo compareInfo = new CompareInfo();
            m_container.put(context, compareInfo);
        }
        
        CompareInfo compareInfo = m_container.get(context);
        compareInfo.add(inputModel);
    }
    
    @Override
    public void unregisterToComparator(String context)
    {
        if(!context.equals(""))
        {
            CompareInfo compareInfo = m_container.get(context);
            if(compareInfo != null)
            {
                compareInfo.reset();
                m_container.remove(context);
            }
        }
        else
        {
            m_container.clear();
        }
       
    }
    
    @Override
    public void compareTable(String context)
    {
        CompareInfo compareInfo = m_container.get(context);
        
        if(compareInfo != null && compareInfo.isComparable())
        {
            Object firstObject = compareInfo.getFirstObject();
            Object secondObject = compareInfo.getSecondObject();
            
            TCTable firstTable = compareInfo.getFirstTable();
            TCTable secondTable = compareInfo.getSecondTable();
            
            Comparator<Object> comparator = compareInfo.getComparator();
            
            String primaryKeyColumn = compareInfo.getPrimaryKey();
            String[] tableColumnNames = compareInfo.getColumnNames();
            
            int result = 0;
            
            if(comparator != null)
            {
                result = comparator.compare(firstObject, secondObject);
            }
            
            Boolean isTable1Latest = null; // means both comparing items are same.
            if (result > 0)
            {
                isTable1Latest = true;
            }
            else if (result < 0)
            {
                isTable1Latest = false;
            }
            
            CompareOutputBean resultSet = null;
            ITableComparatorSubscriber firstSubscriber = compareInfo
                    .getFirstSubscriber();
            ITableComparatorSubscriber secondSubscriber = compareInfo
                    .getSecondSubscriber();
            
            if (isTable1Latest != null)
            {
                TCTableComparator tcomp = new TCTableComparator();
                resultSet = tcomp.compareView(firstTable,
                        secondTable, primaryKeyColumn, tableColumnNames,
                        isTable1Latest);
                
                if (isTable1Latest)
                {
                    setPublishSubscriberData(context, firstSubscriber,
                            secondSubscriber, resultSet);
                }
                else
                {
                    setPublishSubscriberData(context, secondSubscriber,
                            firstSubscriber, resultSet);
                }
            }
            else
            {
                //need to update table if there is no comparison 
                firstSubscriber.updateTable(null);
                secondSubscriber.updateTable(null);
            }
        }
    }
    
    private void setPublishSubscriberData(String context,
            ITableComparatorSubscriber latestRevisionSubscriber,
            ITableComparatorSubscriber oldRevisionSubscriber,
            CompareOutputBean resultSet)
    {
        CompareOutputBean subscriberData1 = new CompareOutputBean();
        CompareOutputBean subscriberData2 = new CompareOutputBean();
        
        subscriberData1.setObject(latestRevisionSubscriber);
        subscriberData1.setAddedRows(resultSet.getAddedRows());
        subscriberData1.setUpdatedRows(resultSet.getUpdatedRows());
        subscriberData1.setUpdatedColumns(resultSet.getUpdatedColumns());
        
        publish(context, subscriberData1);
        
        subscriberData2.setObject(oldRevisionSubscriber);
        subscriberData2.setRemovedRows(resultSet.getRemovedRows());
        publish(context, subscriberData2);
    }
    
    @Override
    public void publish(String context, CompareOutputBean subscriberData)
    {
        PublishEvent event = new PublishEvent(this, context, subscriberData,
                null);
        
        ITableComparatorSubscriber subscriber = (ITableComparatorSubscriber) subscriberData
                .getObject();
        
        subscriber.updateTable(event);
    }
    
    @Override
    public IPropertyMap getData()
    {
        return null;
    }
    
}
