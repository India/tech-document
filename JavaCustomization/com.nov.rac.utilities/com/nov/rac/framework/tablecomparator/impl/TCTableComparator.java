package com.nov.rac.framework.tablecomparator.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.teamcenter.rac.common.TCTable;

public class TCTableComparator
{
    private List<Integer> m_addedRows = new ArrayList<Integer>();
    private List<Integer> m_removedRows = new ArrayList<Integer>();
    private List<Integer> m_updatedRows = new ArrayList<Integer>();
   // private List<Integer> m_updatedColumns = new ArrayList<Integer>();
    
    public TCTableComparator()
    {
        
    }
    
    private void compare(TCTable firstTable, TCTable secondTable,
            String primaryKeyColumn, String[] tableColumnNames,
            boolean isTable1Latest)
    {
        int primaryColumnIndex = firstTable.getColumnIndex(primaryKeyColumn);
        Map<Object, Integer> firstObjectIndexMap = new ConcurrentHashMap<Object, Integer>();
        Map<Object, Integer> secondObjectIndexMap = new ConcurrentHashMap<Object, Integer>();
        
        // firstTableItemList is the list of primaryKeyColumn objects exist in
        // table1
        List<Object> firstTableItemList = new CopyOnWriteArrayList<Object>();
        
        firstTable.getColumnObjects(primaryColumnIndex, firstTableItemList);
        fillMap(firstObjectIndexMap, firstTableItemList);
        
        // secondTableItemList is the list of primaryKeyColumn objects exist in
        // table2
        List<Object> secondTableItemList = new CopyOnWriteArrayList<Object>();
        
        secondTable.getColumnObjects(primaryColumnIndex, secondTableItemList);
        fillMap(secondObjectIndexMap, secondTableItemList);
        
        // firstSet:Set of first table primary column objects
        Set<Object> firstSet = createObjectSet(firstObjectIndexMap);
        
        // secondSet:Set of second table primary column objects
        Set<Object> secondSet = createObjectSet(secondObjectIndexMap);
        
        // inFirstSetOnly:Set of objects present in first table only
        Set<Object> inFirstSetOnly = new HashSet<Object>(firstSet);
        inFirstSetOnly.removeAll(secondSet);
        
        // inSecondSetOnly:Set of objects present in second table only
        Set<Object> inSecondSetOnly = new HashSet<Object>(secondSet);
        inSecondSetOnly.removeAll(firstSet);
        
        // common:Set of common objects present in both first and second table
        Set<Object> common = new HashSet<Object>(firstSet);
        common.retainAll(secondSet);
        
        if (isTable1Latest)
        {
            createOutputMap(firstTable, secondTable, tableColumnNames,
                    inFirstSetOnly, inSecondSetOnly, firstObjectIndexMap,
                    secondObjectIndexMap, common);
        }
        else
        {
            createOutputMap(secondTable, firstTable, tableColumnNames,
                    inSecondSetOnly, inFirstSetOnly, secondObjectIndexMap,
                    firstObjectIndexMap, common);
        }
    }
    
    private void createOutputMap(TCTable latestTable, TCTable olderTable,
            String[] tableColumnNames, Set<Object> latestObjectSet,
            Set<Object> olderObjectSet,
            Map<Object, Integer> latestObjectIndexMap,
            Map<Object, Integer> olderObjectIndexMap, Set<Object> common)
    {
        addedRows(latestObjectSet, latestObjectIndexMap);
        
        removedRows(olderObjectSet, olderObjectIndexMap);
        
        modifiedRows(latestTable, olderTable, tableColumnNames,
                latestObjectIndexMap, olderObjectIndexMap, common);
    }
    
    private void modifiedRows(TCTable latestTable, TCTable olderTable,
            String[] tableColumnNames,
            Map<Object, Integer> latestObjectIndexMap,
            Map<Object, Integer> olderObjectIndexMap, Set<Object> common)
    {
        if (common.size() > 0)
        {
            // Get column indices for table column names.
            int[] latestColumnIndex = new int[tableColumnNames.length];
            int[] oldColumnIndex = new int[tableColumnNames.length];
            
            for (int inx = 0; inx < tableColumnNames.length; inx++)
            {
                latestColumnIndex[inx] = latestTable
                        .getColumnIndex(tableColumnNames[inx]);
                oldColumnIndex[inx] = olderTable
                        .getColumnIndex(tableColumnNames[inx]);
            }
            
            // Iterate for each common row.
            for (Object object : common)
            {
                int latestRowIndex = latestObjectIndexMap.get(object);
                int olderRowIndex = olderObjectIndexMap.get(object);
                
                // Compare all column values for a common row.
                for (int colInx = 0; colInx < latestColumnIndex.length; colInx++)
                {
                    
                    Object latestValue = latestTable.getValueAt(latestRowIndex,
                            latestColumnIndex[colInx]);
                    Object olderValue = olderTable.getValueAt(olderRowIndex,
                            oldColumnIndex[colInx]);
                    
                    // If column values differ, it means the row is modified in
                    // latest table.
                    if (!latestValue.equals(olderValue))
                    {
                        m_updatedRows.add(latestRowIndex);
                       // m_updatedColumns.add(latestColumnIndex[colInx]);
                        break;
                    }
                }
            }
        }
    }
    
    private void removedRows(Set<Object> olderObjectSet,
            Map<Object, Integer> olderObjectIndexMap)
    {
        for (Object object : olderObjectSet)
        {
            m_removedRows.add(olderObjectIndexMap.get(object));
        }
    }
    
    private void addedRows(Set<Object> latestObjectSet,
            Map<Object, Integer> latestObjectIndexMap)
    {
        for (Object object : latestObjectSet)
        {
            m_addedRows.add(latestObjectIndexMap.get(object));
        }
    }
    
    private Set<Object> createObjectSet(Map<Object, Integer> objectIndexMap)
    {
        Set<Object> set = objectIndexMap.keySet();
        return set;
    }
    
    private void fillMap(Map<Object, Integer> ObjectIndexMap,
            List<Object> tableItemList)
    {
        int index = 0;
        for (Object object : tableItemList)
        {
            ObjectIndexMap.put(object, index);
            index++;
        }
    }
    
    public CompareOutputBean compareView(TCTable table1, TCTable table2,
            String primaryKeyColumn, String[] tableColumnNames,
            boolean isTable1RevisionLatest)
    {
        CompareOutputBean resultSet = new CompareOutputBean();
        
        compare(table1, table2, primaryKeyColumn, tableColumnNames,
                isTable1RevisionLatest);
        resultSet.setAddedRows(m_addedRows);
        resultSet.setRemovedRows(m_removedRows);
        resultSet.setUpdatedRows(m_updatedRows);
       // resultSet.setUpdatedColumns(m_updatedColumns);
        return resultSet;
    }
}
