package com.nov.rac.framework.tablecomparator;

import com.nov.rac.framework.tablecomparator.impl.CompareOutputBean;
import com.nov.rac.framework.tablecomparator.impl.IDataModel;


public interface ITableComparator
{
    
    void setContext(String context,IDataModel inputModel);
    
    void unregisterToComparator(String context);
    
    void publish(String context, CompareOutputBean subscriberDat);
    
    void compareTable(String context);
    
}
