package com.nov.rac.framework.communication;

import com.nov.rac.propertymap.IPropertyMap;

/**
 * The Interface IPublisher defines methods to retrieve data from publisher.
 */
public interface IPublisher
{
    
    /**
     * Gets the data.
     * 
     * @return the data
     */
    IPropertyMap getData();
}
