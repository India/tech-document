package com.nov.rac.framework.communication.impl;

import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;

public class Subcriber implements ISubscriber
{
    
    private static int subscriberCount = 1;
    private int subscriberNumber;
    
    public Subcriber()
    {
	subscriberNumber = subscriberCount;
	subscriberCount++;
    }
    
    @Override
    public void update(PublishEvent event)
    {
	System.out.println("notified Subcriber :: " + subscriberNumber);
	System.out.println(event);
    }
    
}
