package com.nov.rac.framework.communication.impl;

import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;

public class Main
{
    
    public static PublishEvent createEvent(String prop, Object prevValue, Object newValue, IPublisher publisher)
    {
        PublishEvent event = new PublishEvent(publisher, prop, newValue, prevValue);
        return event;
    }
    
    public static void main(String[] args)
    {
        IPublisher publisher = new Publisher();
        ISubscriber subscriber1 = new Subcriber();
        ISubscriber subscriber2 = new Subcriber();
        ISubscriber subscriber3 = new Subcriber();
        
        IController controller = new DefaultController();
        controller.registerSubscriber("testProp", subscriber1);
        controller.registerSubscriber("testProp", subscriber2);
        
        controller.registerSubscriber("testPropNew", subscriber2);
        controller.registerSubscriber("testPropNew", subscriber3);
        
        controller.publish(createEvent("testProp", "Past", "Future", publisher));
        
        controller.publish(createEvent("testPropNew", "Practice", "Perfect", publisher));
    }
}
