package com.nov.rac.framework.communication.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.ui.IUIPanel;

/**
 * The Class DefaultController has the default implementation of IController.
 */
public class DefaultController implements IController
{
    private static IController controller = null;
    
    public static IController getController()
    {
        if (controller == null)
        {
            
            controller = new DefaultController();
        }
        return controller;
    }
    
    
    /** The registered subscribers. */
    private Map<String, List<ISubscriber>> m_subscribers;
    
    /**
     * Instantiates a new default controller.
     */
    public DefaultController()
    {
        /*
         * ConcurrentHashMap is used to handle concurrency issues. 
         */
        //m_subscribers = new HashMap<String, List<ISubscriber>>();
        m_subscribers = new ConcurrentHashMap<String, List<ISubscriber>>();
    }
    
    /**
     * Register subscriber.
     * 
     * @param prop
     *            the prop
     * @param subscriber
     *            the subscriber
     */
    @Override
    public void registerSubscriber(String prop, ISubscriber subscriber)
    {
        // We are creating a list of subscribers because many subscribers may
        // want to get notified for a single event
        List<ISubscriber> subscribers = null;
        if (m_subscribers.containsKey(prop))
        {
            subscribers = m_subscribers.get(prop);
        }
        else
        {
            subscribers = new Vector<ISubscriber>();
            m_subscribers.put(prop, subscribers);
        }
        // The following condition is just added to avoid duplicacy
        // if the subscriber has already registered
        if (!subscribers.contains(subscriber))
            subscribers.add(subscriber);
    }
    
    /**
     * Unregister subscriber.
     * 
     * @param prop
     *            the prop
     * @param subscriber
     *            the subscriber
     */
    @Override
    public void unregisterSubscriber(String prop, ISubscriber subscriber)
    {
        if (m_subscribers.containsKey(prop))
        {
            List<ISubscriber> subscribers = m_subscribers.get(prop);
            subscribers.remove(subscriber);
        }
    }
    
    /**
     * Publishes the event.
     * 
     * @param event
     *            the event
     */
    @Override
    public void publish(PublishEvent event)
    {
        String prop = event.getPropertyName();
        if (m_subscribers.containsKey(prop))
        {
            List<ISubscriber> subscriberss = m_subscribers.get(prop);
            
            /*
             * CopyOnWriteArrayList is used to handle concurrency issues. 
             */
            CopyOnWriteArrayList<ISubscriber> subscribers = new CopyOnWriteArrayList<ISubscriber>();
            subscribers.addAll(subscriberss);
            
            for (ISubscriber subscriber : subscribers)
            {
                try
                {
//                    if(subscriber instanceof IUIPanel)
//                    {
//                       // Composite composite = (Composite)subscriber;
//                        IUIPanel panel = (IUIPanel)subscriber;
//                        Composite composite = (Composite)panel.getUIPanel();
//                        if(!composite.isDisposed())
//                            subscriber.update(event);
//                    }
//                    else
//                    {
                        subscriber.update(event);
                 //   }
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
