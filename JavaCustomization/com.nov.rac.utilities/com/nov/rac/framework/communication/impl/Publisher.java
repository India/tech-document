package com.nov.rac.framework.communication.impl;

import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;

public class Publisher implements IPublisher
{
    IPropertyMap m_PropertyMap;
    
    public Publisher()
    {
	m_PropertyMap = new SimplePropertyMap();
	m_PropertyMap.setString("Hello", "Hello Abhijeet");
    }
    
    @Override
    public IPropertyMap getData()
    {
	return m_PropertyMap;
    }
    
}
