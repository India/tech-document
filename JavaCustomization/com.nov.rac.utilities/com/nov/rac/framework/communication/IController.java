package com.nov.rac.framework.communication;

/**
 * The Interface IController.
 */
public interface IController
{
    
    /**
     * Register subscriber.
     * 
     * @param prop
     *            the prop
     * @param subscriber
     *            the subscriber
     */
    void registerSubscriber(String prop, ISubscriber subscriber);
    
    /**
     * Unregister subscriber.
     * 
     * @param prop
     *            the prop
     * @param subscriber
     *            the subscriber
     */
    void unregisterSubscriber(String prop, ISubscriber subscriber);
    
    /**
     * Publishes the event.
     * 
     * @param event
     *            the event
     */
    void publish(PublishEvent event);
    
}
