package com.nov.rac.framework.communication;

import java.util.HashMap;
import java.util.Map;

import com.nov.rac.framework.communication.impl.DefaultController;

/**
 * A factory for creating Controller objects.
 */
public class ControllerFactory
{
    
    /** The m_ instance. */
    private static ControllerFactory m_Instance = null;
    
    // Need the following object to synchronize a block
    /** The sync object_. */
    private static Object syncObject_=new Object();
    
    /** The controller map. */
    private Map<String, IController> controllerMap;
    
    /**
     * Instantiates a new controller factory.
     */
    private ControllerFactory()
    {
        controllerMap = new HashMap<String, IController>();
        controllerMap.put(null, new DefaultController());
    }
    
    /**
     * Gets the single instance of ControllerFactory.
     *
     * @return single instance of ControllerFactory
     */
    public static ControllerFactory getInstance()
    {
        // we are ensuring that we have singleton object in thread safe way;
        // with double checking
        // (note : same if condition is checked twice )
        if (m_Instance == null)
        {
            synchronized (syncObject_)
            {
                if (m_Instance == null)
                {
                    m_Instance = new ControllerFactory();
                }
            }
        }
        return m_Instance;
    }
    
    /**
     * Gets the default controller.
     *
     * @return the default controller
     */
    public IController getDefaultController()
    {
        return controllerMap.get(null);
    }
    
    /**
     * Gets the controller associated with given key ,
     * if not exists creates new
     *
     * @param key the key
     * @return the controller
     */
    public IController getController(String key)
    {
        IController controller = null;
        if (controllerMap.containsKey(key))
            controller = controllerMap.get(key);
        else
        {
            controller = new DefaultController();
            controllerMap.put(key, controller);
        }
        return controller;
    }
    
}
