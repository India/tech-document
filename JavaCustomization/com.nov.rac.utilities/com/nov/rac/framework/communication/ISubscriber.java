package com.nov.rac.framework.communication;

/**
 * The Interface ISubscriber defines methods for updating Subscriber when some
 * Event of their Interest has happened.
 */
public interface ISubscriber
{
    
    /**
     * Updates about the event.
     * 
     * @param event
     *            the event
     */
    void update(PublishEvent event);
}
