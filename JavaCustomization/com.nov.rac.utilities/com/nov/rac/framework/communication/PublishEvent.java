package com.nov.rac.framework.communication;

/**
 * The Class PublishEvent contains information about event.
 */
public class PublishEvent
{
    
    /** The property name. */
    private String m_propertyName;
    
    /** The source. */
    private IPublisher m_source;
    
    /** The old value. */
    private Object m_oldValue;
    
    /** The new value. */
    private Object m_newValue;
    
    /**
     * Instantiates a new publish event.
     *
     * @param publisher the publisher
     * @param m_propertyName the m_property name
     * @param new_Value the new value
     * @param old_Value the old value
     */
    public PublishEvent(IPublisher publisher, String m_propertyName, Object new_Value, Object old_Value)
    {
        if (publisher == null)
        {
            throw new IllegalArgumentException("null source");
        }
        if (m_propertyName == null)
        {
            throw new IllegalArgumentException("null property name");
        }
        this.m_propertyName = m_propertyName;
        this.m_source = publisher;
        this.m_newValue = new_Value;
        this.m_oldValue = old_Value;
    }
    
    /**
     * Gets the old value.
     *
     * @return the old value
     */
    public Object getOldValue()
    {
        return m_oldValue;
    }
    
    /**
     * Gets the property name.
     * 
     * @return the property name
     */
    public String getPropertyName()
    {
        return m_propertyName;
    }
    
    /**
     * Sets the property name.
     * 
     * @param propertyName
     *            the new property name
     */
    public void setPropertyName(String propertyName)
    {
        this.m_propertyName = propertyName;
    }
    
    /**
     * Gets the new value.
     * 
     * @return the new value
     */
    public Object getNewValue()
    {
        return m_newValue;
    }
    
    /**
     * Sets the new value.
     * 
     * @param newValue
     *            the new new value
     */
    public void setNewValue(Object newValue)
    {
        this.m_newValue = newValue;
    }
    
    /**
     * Gets the source.
     * 
     * @return the source
     */
    public IPublisher getSource()
    {
        return m_source;
    }
    
    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(IPublisher source)
    {
        this.m_source = source;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder(getClass().getName());
        stringBuilder.append("[");
        stringBuilder.append("source=").append(m_source).append("\t");
        stringBuilder.append("property name=").append(m_propertyName).append("\t");
        stringBuilder.append("Old Value=").append(m_oldValue).append("\t");
        stringBuilder.append("New Value=").append(m_newValue);
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
    
}
