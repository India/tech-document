package com.nov.rac.utilities.certification.delegates;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;


import com.nov.rac.operations.IOperationDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.UserSessionInfo;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.nov.rac.utilities.utils.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.InterfaceExceptionStack;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractOperationDelegate implements IOperationDelegate
{
    protected ExceptionStack stack = new ExceptionStack();
    
    public AbstractOperationDelegate(Shell theShell)
    {
        m_shell = theShell;
    }
    
    @Override
    abstract public boolean preCondition() throws TCException;
    
    @Override
    abstract public boolean preAction() throws TCException;
    
    @Override
    abstract public boolean baseAction() throws TCException;
    
    @Override
    public boolean postAction() throws TCException
    {
        // final TCComponent container = NewItemCreationHelper.getContainer();
        // if (null != container)
        // {
        // Display.getDefault().asyncExec(new Runnable()
        // {
        //
        // @Override
        // public void run()
        // {
        // try
        // {
        // container.refresh();
        // }
        // catch (TCException e)
        // {
        // e.printStackTrace();
        // }
        //
        // }
        // });
        // }
        return true;
    }
    
    private void runWithProgress(final IProgressMonitor monitor)
    {
        final Registry registry = Registry.getRegistry(this);      
        monitor.beginTask(registry.getString("Operation.MESSAGE"), 10000);
        Display.getDefault().syncExec(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    monitor.subTask(registry.getString("Precondition.MESSAGE"));
                    success = preCondition();
                    if (success)
                    {
                        monitor.subTask(registry.getString("CreateObject.MESSAGE"));
                        success = preAction();
                        
                        if (success)
                        {
                            // monitor.subTask("Executing baseAction");
                            success = baseAction();
                            if (success)
                            {
                                // monitor.subTask("Executing postAction");
                                success = postAction();
                            }
                        }
                    }
                    monitor.done();
                }
                catch (TCException exception)
                {
                    monitor.setCanceled(true);
                    monitor.done();
                    
                    success = false;
                    int[] errorSeverities;
                    try
                    {
                        errorSeverities = stack.getErrorSeverities();
                    }
                    catch (NullPointerException e)
                    {
                        errorSeverities = null;
                    }
                    if (errorSeverities != null && errorSeverities.length > 0
                            && errorSeverities[0] != InterfaceExceptionStack.SEVERITY_ERROR)
                    {
                        success = true;
                        TCException excep = new TCException(stack.getErrorCodes(), stack.getErrorSeverities(), stack
                                .getErrorStack());
                        MessageBox.post(null, excep, "Information", MessageBox.INFORMATION);
                    }
                    else
                    {
                       // MessageBox.post(exception); TCDECREL-6750
                    	 MessageBox.post(null, exception, registry.getString("Error.TITLE_MSG"), MessageBox.ERROR);
                    }
                }
               
            }
        });
    }
    
    @Override
    public boolean executeOperation()
    {
        stack.clear(); // To clear the error stack captured in previous
                       // executeOperation() call
        
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if (display != null)
        {
            display.syncExec((new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(m_shell);
                        progressMonitorDialog.run(true, false, new IRunnableWithProgress()
                        {
                            @Override
                            public void run(final IProgressMonitor monitor) throws InvocationTargetException,
                                    InterruptedException
                            {
                                runWithProgress(monitor);
                            }
                        });
                    }
                    catch (InvocationTargetException exp)
                    {
                        Throwable theCause = exp.getCause();
                        MessageBox.post(theCause);
                    }
                    catch (InterruptedException exp)
                    {
                        Throwable theCause = exp.getCause();
                        MessageBox.post(theCause);
                    }
                }
            }));
        }
        
        return success;
    }
    
    /**
     * Gets the filtered panels.
     * 
     * @param itemType
     *            the item type - by which given panels will be filtered
     * @param panels
     *            the panels
     * @return the filtered panels
     */
    protected final List<IUIPanel> getFilteredPanels(String theGroup,
             String contextInfo, List<IUIPanel> panels)
    {
        StringBuffer contextString = new StringBuffer();
        contextString.append(".").append(contextInfo)
                .append(".").append("DATA_PANELS_LIST");
        
        Registry registry = getRegistry();
        
        // String[] panelNames =
        // registry.getStringArray(contextString.toString(),
        // null);
        String[] panelNames = RegistryUtils.getConfiguration(theGroup,
                contextString.toString(), registry);
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        return panelList;
    }
    
    protected Registry getRegistry() {
		// TODO Auto-generated method stub
		return Registry.getRegistry(this);
	}

	protected final List<IUIPanel> getFilteredPanels( String contextInfo, List<IUIPanel> panels)
    {
        String currentGroup = UserSessionInfo.getCurrentGroup();
        
        return getFilteredPanels(currentGroup, contextInfo,panels);
        
    }

    
    /**
     * Fill panels data into given property map.
     * 
     * @param itemType
     *            the item type
     * @param panels
     *            the panels
     * @param propertyMap
     *            the property map
     */
    protected final void fillPanelsData(String contextInfo, List<IUIPanel> panels, IPropertyMap propertyMap)
    {
    	
    	
        List<IUIPanel> filteredPanels = panels;/*getFilteredPanels( contextInfo, panels);*/
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof ILoadSave)
            {
                ILoadSave saveablepanel = (ILoadSave) panel;
                try
                {
                    saveablepanel.save(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
   
    @Override
    public void registerOperationInputProvider(List<IUIPanel> inputProviders)
    {
        m_OperationInputProviders = inputProviders;
    }
    
    /**
     * Gets the operation input providers.
     * 
     * @return the operation input providers
     */
    protected final List<IUIPanel> getOperationInputProviders()
    {
        return m_OperationInputProviders;
    }
    
    
  
    
    protected List<IUIPanel> m_OperationInputProviders = null;
    private Shell m_shell;
    private boolean success = false;
}
