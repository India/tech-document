package com.nov.rac.utilities.certification.delegates;

import org.eclipse.swt.widgets.Shell;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.utilities.certification.report.NOVExcelReportGenerator;
import com.teamcenter.rac.kernel.TCException;

public class CertificationWizardOperationDelegate extends AbstractOperationDelegate
{
    IPropertyMap propMap = new SimplePropertyMap();
    
    public CertificationWizardOperationDelegate(Shell theShell)
    {
        super(theShell);
    }
    
    @Override
    public boolean preCondition() throws TCException
    {
        fillPanelsData("Certification", getOperationInputProviders(), propMap);
        
        return true;
    }
    
    @Override
    public boolean preAction() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean baseAction() throws TCException
    {
        boolean isFileCreated = false;
        
        // Excel report generation
        NOVExcelReportGenerator excelCreator = new NOVExcelReportGenerator();
        isFileCreated = excelCreator.generateExcelReport(propMap);
        return isFileCreated;
    }
}
