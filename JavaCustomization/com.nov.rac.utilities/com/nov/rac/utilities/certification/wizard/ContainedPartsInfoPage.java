package com.nov.rac.utilities.certification.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class ContainedPartsInfoPage extends WizardPage implements IUIPanel, ILoadSave
{
	 List<IUIPanel> containedPartPanels = new ArrayList<IUIPanel>();
	
    private IUIPanel uiPanelDelegate;
    
    public ContainedPartsInfoPage()
    {
        super("Contained Parts");
        setTitle("Contained Parts Search");
        setDescription("Search for Assembly part to get Certified components results");
    }
 
    @Override
    public void dispose()
    {
    	for(int i = 0; i < containedPartPanels.size(); i++)
    	{
    		containedPartPanels.get(i).dispose();
    	}
    	super.dispose();
    }


    @Override
    public void createControl(Composite parent)
    {
        Composite newComposite = new Composite(parent, SWT.NONE);
        FillLayout layout = new FillLayout();
        newComposite.setLayout(layout);
        
        uiPanelDelegate = new ContainedPartsPanel(newComposite, SWT.NONE);
        
        //Add panels to List<IUIPanel>
        containedPartPanels.add(uiPanelDelegate);
        
        try
        {
            uiPanelDelegate.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        setControl((Control) newComposite);
        setPageComplete(false);
    }

    public IPropertyMap getPropMap(IPropertyMap propMapForExcel)
    {
        try
        {
            ((ILoadSave) uiPanelDelegate).save(propMapForExcel);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return propMapForExcel;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        if (uiPanelDelegate instanceof ILoadSave)
        {
            return ((ILoadSave) uiPanelDelegate).load(propMap);
        }
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        if (uiPanelDelegate instanceof ILoadSave)
        {
            return ((ILoadSave) uiPanelDelegate).save(propMap);
        }
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public Object getUIPanel()
    {
        return null;
    }
    
    @Override
    public boolean createUIPost() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean reset()
    {
        return false;
    }
}
