package com.nov.rac.utilities.certification.wizard;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.window.Window;

import com.teamcenter.rac.aifrcp.AIFUtility;

public class NOVCertificationHandler extends AbstractHandler
{

    @Override
    public Object execute(ExecutionEvent arg0) throws ExecutionException
    {

        NOVCertificationWizardDialog wizardDialog = new NOVCertificationWizardDialog(AIFUtility.getActiveDesktop().getShell(), new NOVCertificationDocIndexWizard());
                    
        if (wizardDialog.open() == Window.OK) 
        {
            System.out.println("Ok pressed");                  
        }
        else
        {
            System.out.println("Cancel pressed");
        }       
        
        return null;
    }
    
}
