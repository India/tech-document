package com.nov.rac.utilities.certification.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;

import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.teamcenter.rac.kernel.TCException;

public class HeaderInfoInputPage extends WizardPage implements IUIPanel, ILoadSave, ISubscriber
{
    IPropertyMap firstMap = new SimplePropertyMap();
    IUIPanel uiPanelDelegate;
    
    List<IUIPanel> headerInfoPanels = new ArrayList<IUIPanel>();
    
    
    @Override
    public boolean canFlipToNextPage()
    {
        return super.canFlipToNextPage();
    }
    
    
    
    @Override
    public void dispose()
    {
    	unregisterSubscriber();
    	for(int i = 0; i < headerInfoPanels.size(); i++)
    	{
    		headerInfoPanels.get(i).dispose();
    	}
    	super.dispose();
    }
    
    
    
    private void unregisterSubscriber() 
    {
    	IController lcontroller = ControllerFactory.getInstance().getDefaultController();
    	lcontroller.unregisterSubscriber("salesOrderValueChanged", getSubscriber());
	}


	protected ISubscriber getSubscriber()
    {
		return this;
	}


	@Override
    public boolean isPageComplete()
    {
        boolean isPageComplete = true;
        fillPanelData();
        Link linkContainedPart = ((NOVCertificationDocIndexWizard) getWizard()).getlink_contained();
        Link linkHeaderInfo = ((NOVCertificationDocIndexWizard) getWizard()).getlink_header();
        if (firstMap.getString("Sales Order").trim().isEmpty())
        {
            isPageComplete = false;
            linkHeaderInfo.setEnabled(true);
            linkContainedPart.setEnabled(false);
        }
        else
        {
            linkContainedPart.setEnabled(true);
            linkHeaderInfo.setEnabled(true);
        }
        return isPageComplete;
    }
    
    public void fillPanelData()
    {
        try
        {
            ((ILoadSave) uiPanelDelegate).save(firstMap);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    public HeaderInfoInputPage()
    {
        super("Header Information");
        setTitle("Enter Sales Order information");
        setDescription("These fields will be used in the header of the Certification Documentation Index");
        
        registerSubscriber();
    }
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber("salesOrderValueChanged", this);
    }
    
    @Override
    public void createControl(Composite parent)
    {
        Composite newComposite = new Composite(parent, SWT.NONE);     
        FillLayout layout = new FillLayout();
        newComposite.setLayout(layout);
      
        uiPanelDelegate = new HeaderPropertiesPanel(newComposite, SWT.NONE);
        
        //Add panels to List<IUIPanel>
        headerInfoPanels.add(uiPanelDelegate);
        
        try
        {
            uiPanelDelegate.createUI();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        setControl((Control) newComposite /* uiPanelDelegate.getUIPanel() */);
        setPageComplete(false);
    }
    
    public IPropertyMap getPropMap(IPropertyMap propMapForExcel)
    {
        try
        {
            ((ILoadSave) uiPanelDelegate).save(propMapForExcel);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return propMapForExcel;
    }
    
    @Override
    public void update(PublishEvent event)
    {
        
        if (event.getPropertyName().equals("salesOrderValueChanged"))
        {
            if (event.getNewValue().toString().isEmpty())
            {
                setPageComplete(false);
            }
            else
            {
                setPageComplete(true);
            }
        }
    }
    
    @Override
    public boolean createUI() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public Object getUIPanel()
    {
        return uiPanelDelegate.getUIPanel();
    }
    
    @Override
    public boolean createUIPost() throws TCException
    {
        return true;
    }
    
    @Override
    public boolean reset()
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        if (uiPanelDelegate instanceof ILoadSave)
        {
            return ((ILoadSave) uiPanelDelegate).load(propMap);
        }
        return false;
    }
    
    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        if (uiPanelDelegate instanceof ILoadSave)
        {
            return ((ILoadSave) uiPanelDelegate).save(propMap);
        }
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }
}