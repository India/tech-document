package com.nov.rac.utilities.certification.wizard;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

public class NOVCertificationWizardDialog extends WizardDialog
{
    public NOVCertificationWizardDialog(Shell parentShell, IWizard newWizard)
    {
        super(parentShell, newWizard);
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent)
    {
        super.createButtonsForButtonBar(parent);
        getButton(IDialogConstants.BACK_ID).setVisible(false);
        getButton(IDialogConstants.FINISH_ID).setText("Create Index");
    }
}
