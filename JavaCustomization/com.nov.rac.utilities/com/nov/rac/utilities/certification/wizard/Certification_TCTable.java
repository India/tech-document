package com.nov.rac.utilities.certification.wizard;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.TableUtils;
import com.nov.rac.utilities.table.CheckboxCellEditor;
import com.nov.rac.utilities.table.CheckboxCellRenderer;
import com.nov.rac.utilities.table.CheckboxHeaderRenderer;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class Certification_TCTable extends TCTable implements IPublisher, CertificationTableUpdater
{
    private static final long serialVersionUID = -8954422881453190651L;
    private CertificationTableUpdater m_tableUpdater;
    private int m_includeColumnIndex;
    private TableColumn m_includeColumn;
    Registry m_registry = Registry.getRegistry(this);
    
    public Certification_TCTable(TCSession tcsession, String[] columnNames)
    {        super(tcsession, columnNames);
        configureTable();
    }
    
    public Certification_TCTable(String[] columnIds, String[] columnNames)
    {
        super(columnIds, columnNames);
        configureTable();
    }
    
    @Override
    public boolean isCellEditable(int row, int column)
    {
        boolean isCellEditable = false;
        int includeColumn = m_includeColumnIndex;

        if( column == includeColumn )
        {
        	isCellEditable = true;
        }
        return isCellEditable;
    }
    
    //Added method to get the selected row index for minor revision table
	public int[] getSelectedRowsIndices()
	{
		int[] rowIndices = null;
		Vector<Integer> rowsSelect = new Vector<Integer>();
		
		for(int i = 0; i < this.getRowCount(); i++)
		{
			if(this.getValueAt(i, m_includeColumnIndex).equals("true"))
			{	
				rowsSelect.add(i);
			}
		}
		rowIndices = new int[rowsSelect.size()];
		
		for(int i=0; i< rowsSelect.size(); i++)
		{
			rowIndices[i] = rowsSelect.get(i);
		}
		return rowIndices;	
	}
	
	//Configure table properties
	public void configureTable()
	{
		m_includeColumn = this.getColumn(m_registry.getString("includeRev.LABEL"));
		m_includeColumnIndex = TableUtils.getColumnIndex(m_registry.getString("includeRev.LABEL"), this);
		
		//this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
//		String[] widths = {"20","15","15","15","15","20"};
//		this.setColumnWidths(widths);
//		Enumeration<TableColumn> tblCols = this.getColumnModel().getColumns();
		
		
		this.getTableHeader().setReorderingAllowed(false);
		
		this.setName("Certified Components Table");
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
			
		//Update the table
		this.setTableUpdater(this);
		
		//Show checkbox in header
		CheckboxCellRenderer editor = new CheckboxCellRenderer();
		this.getColumnModel().getColumn(m_includeColumnIndex).setCellRenderer(editor);
		this.getColumnModel().getColumn(m_includeColumnIndex).setCellEditor(new CheckboxCellEditor());
		
		CheckboxHeaderRenderer tableHeaderRenderer = new CheckboxHeaderRenderer(m_registry.getString("includeRev.LABEL"), false, true);
		m_includeColumn.setHeaderRenderer(tableHeaderRenderer);
		tableHeaderRenderer.addActionListener(new SelectAllActionListener());
		this.getTableHeader().setBorder(UIManager.getBorder("TableHeader.cellBorder"));
	}   
	
	//Select or De-select all minor revisions if Include Rev header is checked or unchecked
	class SelectAllActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            JCheckBox checkbox = (JCheckBox) e.getSource();
            selectAllRevisions(checkbox.isSelected());
        }       
    }
    
    public void selectAllRevisions(boolean bSelectAll)
    {
        String isSelected = String.valueOf(bSelectAll);
        for (int iCnt = 0; iCnt < this.getModel().getRowCount(); iCnt++)
        {
            if (this.isCellEditable(iCnt,m_includeColumnIndex))
            {
            	this.getModel().setValueAt(isSelected, iCnt, m_includeColumnIndex);
            }
        }
    }

    protected void setColumnRenderer(TableColumn tablecolumn)
    {
        
        if (tablecolumn.getHeaderRenderer() instanceof CheckboxHeaderRenderer)
        {
            /*
             * This method gets called from TCTable behaviour on header click if
             * table header contains Jcheckbox then its obvious that user wants
             * selection operation on click of header, therefore previous header
             * is to be persisted; of course,user has no intension of sorting in
             * this case
             */
        }
        else
        {
            super.setColumnRenderer(tablecolumn);
            /*
             * While sorting column; table headers are changing from bold to
             * normal font, so to set font bold , we have done below changes
             */
            // TCDECREL-6700 Commented to set column header font to normal
            // JComponent component = (JComponent) tablecolumn.getHeaderValue();
            // UIHelper.setFontStyle((JComponent) component, Font.BOLD);           
        }
    }
  
    @Override
    public IPropertyMap getData()
    {
        return null;
    }

    public void setCheckboxHeaderState()
    {
    	boolean isEnabled = true;
    	int columnIndex = m_includeColumnIndex;
    	
    	JCheckBox headerCheckbox = (JCheckBox) this.getColumnModel().getColumn(columnIndex)
         .getHeaderRenderer()
         .getTableCellRendererComponent(this, null, false, false, 0, 0);
        
    	if (this.getRowCount() == 0)
        { 
        	isEnabled = false;
        }
        headerCheckbox.setEnabled(isEnabled);
    }
    
    public void clearCheckboxHeader()
    {
    	int columnIndex = m_includeColumnIndex;
    	
    	JCheckBox headerCheckbox = (JCheckBox) this.getColumnModel().getColumn(columnIndex)
    	.getHeaderRenderer()
    	.getTableCellRendererComponent(this, null, false, false, 0, 0);
        
    	if (this.getSelectedRowCount() == 0)
        { 
            headerCheckbox.setSelected(false);
        }
    }
    
    @Override
    public void setValueAt(Object value, int row, int col)
    {
        super.setValueAt(value, row, col);
        CertificationTableUpdater updater = getTableUpdater();
        if (null != updater)
        {
            updater.update(this, value, row, col);           
            this.getTableHeader().repaint();
        }
    }
    
    public CertificationTableUpdater getTableUpdater()
    {
        return m_tableUpdater;
    }
    
    public void setTableUpdater(CertificationTableUpdater tableUpdater)
    {
        this.m_tableUpdater = this;
    }

	@Override
	public void update(JTable table, Object value, int row, int col) 
	{
		if (col == m_includeColumnIndex)
        {
            updateHeaderCheckbox(table, row, col);
        }
	}
	
	private void updateHeaderCheckbox(JTable table, int row, int col) 
	{
		int includeColumnIndex = m_includeColumnIndex;
		TableColumnModel tableColumnModel = table.getColumnModel();
		TableCellRenderer headerRenderer = tableColumnModel.getColumn(includeColumnIndex).getHeaderRenderer();
		Component rendererComponent = headerRenderer.getTableCellRendererComponent(table, null, false, false, row, includeColumnIndex);
		
		if (rendererComponent instanceof JCheckBox)
		{
		    int checkedCount = 0;
		    JCheckBox headerCheckBox = (JCheckBox) rendererComponent;		    		    
		    boolean[] flags = new boolean[table.getRowCount()];
		   
		    for (int i = 0; i < table.getRowCount(); i++)
		    {
		        if (isCellEditable(i, includeColumnIndex))
		        {
		            flags[i] = (new Boolean(table.getValueAt(i, includeColumnIndex).toString()));
		            if (flags[i])
		            {
		                checkedCount++;
		            }
		        }
		    }
		    int rowCountEnabled = table.getRowCount();
		    if (rowCountEnabled > 0 && checkedCount == rowCountEnabled)
		    {
		        headerCheckBox.setSelected(true);
		    }
		    else
		    {
		        headerCheckBox.setSelected(false);
		    }
		}
	}

	@Override
	public boolean isRowEditable(int row) 
	{
		return false;
	}
}
