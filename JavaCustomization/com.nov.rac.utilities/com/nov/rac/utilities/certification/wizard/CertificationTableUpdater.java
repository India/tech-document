package com.nov.rac.utilities.certification.wizard;

import javax.swing.JTable;

public interface CertificationTableUpdater
{
    public void update(JTable table, Object value, int row, int col);
    
    public boolean isCellEditable(int row, int col);

    public boolean isRowEditable(int row);
    

    
}
