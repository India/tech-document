package com.nov.rac.utilities.certification.wizard;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.ISubscriber;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.utilities.certification.report.CertificationBOMSearchOperation;
import com.nov.rac.item.itemsearch.ISearchPropertiesProvider;
import com.nov.rac.item.itemsearch.SearchButtonComponent;
import com.nov.rac.item.itemsearch.SearchResultBean;
import com.nov.rac.utilities.utils.SWTUIHelper;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.SWTUIUtilities;
import com.teamcenter.rac.util.scrollpage.ScrollPagePane;

public class ContainedPartsPanel extends AbstractUIPanel implements ILoadSave, ISubscriber
{
    private Composite m_container;
    private Text m_searchText;
    private Certification_TCTable m_table;
    private TCSession m_session = (TCSession) AIFUtility.getDefaultSession();
	private TCComponent[] selectedItems;
	private Registry m_registry = null;
    
    public ContainedPartsPanel(Composite parent, int style)
    {
        super(parent, style);
        m_registry = Registry.getRegistry(this);
        registerSubscriber();
    }
    
    
    protected ISubscriber getSubscriber()
    {
		return this;
	}
    
    private void registerSubscriber()
    {
        IController controller = ControllerFactory.getInstance().getDefaultController();
        controller.registerSubscriber("SEARCH_RESULT", getSubscriber());
        
        //Tushar
        controller.registerSubscriber("DoubleClickedItem", getSubscriber());
    }
    
    @Override
    public void dispose()
    {
    	unregisterSubscriber();
    	super.dispose();
    }
    
    
    private void unregisterSubscriber() 
    {
    	IController lcontroller = ControllerFactory.getInstance().getDefaultController();
    	lcontroller.unregisterSubscriber("SEARCH_RESULT", getSubscriber());
    	lcontroller.unregisterSubscriber("DoubleClickedItem", getSubscriber());
	}

	@Override
    public boolean createUI() throws TCException
    {
        m_container = getComposite();
        
        GridLayout layout = new GridLayout(3, false);
        m_container.setLayout(layout);
        
        Label searchLabel = new Label(m_container, SWT.NONE);
        searchLabel.setText("Search for Assembly Part Number :");
        searchLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
        
        m_searchText = new Text(m_container, SWT.BORDER);
        m_searchText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        m_searchText.setEnabled(false);
        
       
//      SearchButtonComponent searchButton = new SearchButtonComponent(m_container, SWT.PUSH);
//      
//      GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
//      searchBtnGridData.horizontalIndent = SWTUIHelper.convertHorizontalDLUsToPixels(searchButton, 0);
//      
//      searchButton.setLayoutData(searchBtnGridData);
//      searchButton.setText("");
//      Image searchImage = Registry.getRegistry(this).getImage("search.ICON");
//      searchButton.setImage(searchImage);
      
//      searchButton.setProperty(ISearchPropertiesProvider.ITEM_TYPES, searchItemTypes);
//      searchButton.setProperty(ISearchPropertiesProvider.SEARCH_CONTEXT, "CertificationWizard");
//      // searchButton.setProperty(ISearchPropertiesProvider.OPERATION_CONTEXT,operationContext);
//      searchButton.setProperty(ISearchPropertiesProvider.SELECTION_MODE, ListSelectionModel.SINGLE_SELECTION);
      
      
      SearchButtonComponent searchButton = new SearchButtonComponent(m_container, SWT.PUSH);
      GridData searchBtnGridData = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
      searchBtnGridData.horizontalIndent=SWTUIHelper.convertHorizontalDLUsToPixels(searchButton,6/*NationalOilwell.HORIZONTAL_INDENT_FOR_BUTTON*/ );
      searchButton.setLayoutData(searchBtnGridData);
      //searchButton.setSearchItemType(new String[] {"Nov4Part"});
      
      //TCDECREL-9077
     // searchButton.setSearchContext("CertificationDocIndex");
      
      searchButton.setProperty(ISearchPropertiesProvider.ITEM_TYPES, new String[]{"Nov4Part"});
      searchButton.setProperty(ISearchPropertiesProvider.SEARCH_CONTEXT, "CertificationDocIndex");
      searchButton.setProperty(ISearchPropertiesProvider.BUTTON_SOURCE,m_container);
      searchButton.setProperty(ISearchPropertiesProvider.SELECTION_MODE,ListSelectionModel.SINGLE_SELECTION);
      
      Image searchImage = Registry.getRegistry(this).getImage("search.ICON");
      searchButton.setImage(searchImage);
      
      searchButton.setText("");
      searchButton.setImage(searchImage);
      
      
      Label searchMessageLabel = new Label(m_container, SWT.NONE);
      searchMessageLabel.setText(m_registry.getString("SearchTableLabel.MSG"));
      searchMessageLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
    
      String[] columnNames = { "Contained Part#", "Part Name", "MFG Name", "MFG Part#", "Certificate#",
            "Certificate Name", "Include" };
    
      m_table = new Certification_TCTable(m_session, columnNames);
    
      m_table.setCheckboxHeaderState();
    
      //8677
      m_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

      JScrollPane scPane = new JScrollPane(m_table);
      scPane.setPreferredSize(new Dimension(150, 100));
    
      Composite comp = new Composite(m_container, SWT.EMBEDDED);
      GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
      comp.setLayoutData(gridData);
    
      JPanel tablePanel = new JPanel();
      tablePanel.setLayout(new BorderLayout());
    
      ScrollPagePane scrollPane = new ScrollPagePane(m_table);
      tablePanel.add(scrollPane);
    
      SWTUIUtilities.embed(comp, tablePanel, false);
      return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        List<IPropertyMap> tableRowPropMap = new ArrayList<IPropertyMap>();
        int selectedRowIndex[] = m_table.getSelectedRowsIndices();
        
        for (int row : selectedRowIndex)
        {
            IPropertyMap rowMap = new SimplePropertyMap();
            rowMap.setString("Assembly Part No", m_searchText.getText());
            for (int col = 0; col < m_table.getColumnCount() - 1; col++)
            {
                rowMap.setString(m_table.getColumnName(col), (String) m_table.getModel().getValueAt(row, col));
            }
            tableRowPropMap.add(rowMap);
        }
        propMap.setCompoundArray("TableRows", tableRowPropMap.toArray(new IPropertyMap[tableRowPropMap.size()]));
        return true;
    }
    
    @Override
    public boolean reload() throws TCException
    {
        return false;
    }
    
    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        return false;
    }
    
    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    //Tushar To get selected component
    @Override
    public void update(PublishEvent event)
    {
    	try 
		{
	        if (event.getPropertyName().equals("SEARCH_RESULT"))
	        {
	        	SearchResultBean searchBean = (SearchResultBean)(event.getNewValue());
	        	
	        	selectedItems = (TCComponent[]) (searchBean.getSearchResult());
	        	
	        	String selectedItemId = null;
				if (selectedItems!= null)
				{
					selectedItemId = selectedItems[0].getTCProperty("item_id").toString();
					
					String selectedItemPuid = selectedItems[0].getUid();
		            
		            searchExport();
		            m_searchText.setText(selectedItemId);
				}
					
	            
	            System.out.println("Item Id");
	        } 
			
	        if(event.getPropertyName().equalsIgnoreCase("DoubleClickedItem"))
	        {
	        	TCComponent[] selectedComp = (TCComponent[]) event.getNewValue();
	        	selectedItems = selectedComp;
	        	TCComponentItem selectedItem = (TCComponentItem) selectedComp[0];
	        	String itemID = selectedItem.getProperty("item_id");
	        	m_searchText.setText(itemID);
	        	
	        	searchExport();
	        }
		}
    	catch (TCException e) 
    	{
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
    
    public void searchExport()
    {
    	m_table.removeAllRows();
    	INOVSearchResult searchResult = null;
    	INOVSearchProvider certificationSearchOperation = new CertificationBOMSearchOperation(selectedItems[0]);
    	try
    	{
    		searchResult = NOVSearchExecuteHelperUtils.execute(certificationSearchOperation, INOVSearchProvider.LOAD_ALL);
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    	Vector<String> m_resultRowData = null;
		int m_resultRowCount = 0;
		int m_resultColCount = 0;
		if (searchResult != null && searchResult.getResponse().nRows > 0)
    	{
    		INOVResultSet theResSet = searchResult.getResultSet();
    		m_resultRowData = theResSet.getRowData();
    		m_resultRowCount = theResSet.getRows();
    		m_resultColCount = theResSet.getCols();
    	}
		
    	Vector<String> rowData = new Vector<String>();
    	System.out.println("bom search");
    	
    	for (int i = 0; i < m_resultRowCount; i++)
    	{
    		int rowIndex = m_resultColCount * i;
    		String partName = m_resultRowData.get(rowIndex + 1);
    		String partID = m_resultRowData.get(rowIndex + 2);
    		String mfgName = m_resultRowData.get(rowIndex + 3);
    		String mfgNum = m_resultRowData.get(rowIndex + 4);
    		String certID = m_resultRowData.get(rowIndex + 5);
    		String certName = m_resultRowData.get(rowIndex + 6);

    		if(certID.isEmpty() || certName.isEmpty())
    			continue;

    		String strID[] = certID.split(",");
    		String strName[] = certName.split(",");
    		if(strID.length > 1 || strName.length > 1)
    		{
    			for(int j=0; j<strID.length; j++)
    			{
    				rowData.clear();
    				rowData.add(partName);
    				rowData.add(partID);
    				rowData.add(mfgName);
    				rowData.add(mfgNum);
    				rowData.add(strID[j]);
    				rowData.add(strName[j]);
    				m_table.addRow(rowData);
    			}
    		}
    		else
    		{
    			rowData.clear();
    			rowData.add(partName);
    			rowData.add(partID);
    			rowData.add(mfgName);
    			rowData.add(mfgNum);
    			rowData.add(certID);
    			rowData.add(certName);
    			m_table.addRow(rowData);
    		}
    	}
    	if(m_table.getRowCount() == 0)
    	{
    	    MessageBox.post(m_registry.getString("NoCertifiedComponents.MSG"), "Information", SWT.ICON_INFORMATION);
    	}
    	m_table.setCheckboxHeaderState();
    }
}

