package com.nov.rac.utilities.certification.wizard;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.framework.communication.ControllerFactory;
import com.nov.rac.framework.communication.IController;
import com.nov.rac.framework.communication.IPublisher;
import com.nov.rac.framework.communication.PublishEvent;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.AbstractUIPanel;
import com.nov.rac.ui.ILoadSave;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public class HeaderPropertiesPanel extends AbstractUIPanel implements ILoadSave, IPublisher
{
    private Composite m_container;
    
    private Text m_salesOrder;
    public Text getSalesOrder() {
		return m_salesOrder;
	}

	private Text m_projectNameText;
    private Text m_blockDrawingText;
    private Text m_customerNameText;
    private Text m_pointDrawingText;
    private Text m_quoteNumberText;
    private Text m_revIdText;
    private Text m_customerPOText;
    private Text m_preparedByText;
    private Text m_approvedByText;
    
    public HeaderPropertiesPanel(Composite parent, int style)
    {
        super(parent, style);
    }

    @Override
    public boolean createUI() throws TCException
    {
        m_container = getComposite();
        
        GridLayout layout = new GridLayout();
        m_container.setLayout(layout);
        layout.numColumns = 4;

        createLabel(m_container, "Project Name");
        m_projectNameText = createText(m_container);
        createLabel(m_container, "Block Drawing");
        m_blockDrawingText = createText(m_container);
        createLabel(m_container, "Customer Name");
        m_customerNameText = createText(m_container);
        createLabel(m_container, "Point Drawing");
        m_pointDrawingText = createText(m_container);
        createLabel(m_container, "Quote Number");
        m_quoteNumberText = createText(m_container);
        createLabel(m_container, "Revision");
        m_revIdText = createText(m_container);
        createLabel(m_container, "Customer PO");
        m_customerPOText = createText(m_container);
        createLabel(m_container, "Prepared By");
        m_preparedByText = createText(m_container);
        createLabel(m_container, "Sales Order");
        m_salesOrder = createText(m_container);   
        createLabel(m_container, "Approved By");
        m_approvedByText = createText(m_container);
        
        //TCDECREL-9076 Start
//        createLabel(m_container, "* Required Field");
        GridData reqdGrid = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
        Image image = Registry.getRegistry(this).getImage("asterik.IMAGE");
        Composite reqField = new Composite(m_container, SWT.NONE);
        reqField.setLayout(new GridLayout(2, false));
        reqField.setLayoutData(reqdGrid);
      
        Label requiredLabel = new Label(reqField, SWT.NONE);
        requiredLabel.setImage(image);
        createLabel(reqField, "Required Field");
        //TCDECREL-9076 End
        
        addListerners();
        
        return true;
    }
    
    private void addListerners()
    {
        m_salesOrder.addModifyListener(new ModifyListener()
        {
            @Override
            public void modifyText(ModifyEvent modifyevent)
            {
                IController controller = ControllerFactory.getInstance().getDefaultController();
                controller.publish(new PublishEvent(HeaderPropertiesPanel.this, "salesOrderValueChanged", m_salesOrder.getText().trim(), null));
            }
        });
    }

    public Text createText(Composite composite)
    {
        Text textBox = new Text(composite, SWT.BORDER | SWT.SINGLE);
        textBox.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        return textBox;
    }
    
    public void createLabel(Composite composite, String name)
    {
        Label label = new Label(composite, SWT.NONE);
        label.setText(name);

        if(name.equalsIgnoreCase("Sales Order"))
        {
           ControlDecoration redAsterisk = new ControlDecoration(label, SWT.TOP | SWT.RIGHT);
           redAsterisk.setDescriptionText("This field is mandatory");
           Image image = Registry.getRegistry(this).getImage("asterik.IMAGE");
           redAsterisk.setImage(image);    
        }
    }

    @Override
    public boolean reload() throws TCException
    {
        return false;
    }

    @Override
    public boolean load(IPropertyMap propMap) throws TCException
    {
        return true;
    }

    @Override
    public boolean save(IPropertyMap propMap) throws TCException
    {
        propMap.setString("Project Name", m_projectNameText.getText());
        propMap.setString("Block Drawing", m_blockDrawingText.getText());
        propMap.setString("Customer Name", m_customerNameText.getText());
        propMap.setString("Point Drawing", m_pointDrawingText.getText());
        propMap.setString("Quote Number", m_quoteNumberText.getText());
        propMap.setString("Revision", m_revIdText.getText());
        propMap.setString("Customer PO", m_customerPOText.getText());
        propMap.setString("Prepared By", m_preparedByText.getText());
        propMap.setString("Sales Order", m_salesOrder.getText());
        propMap.setString("Approved By", m_approvedByText.getText());
        
        return true;
    }

    @Override
    public boolean validate(IPropertyMap propMap) throws TCException
    {
        return false;
    }

    @Override
    public IPropertyMap getData()
    {
        return null;
    }
}
