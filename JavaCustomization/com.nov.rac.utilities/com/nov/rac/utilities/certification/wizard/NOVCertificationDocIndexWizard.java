package com.nov.rac.utilities.certification.wizard;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;

import com.nov.rac.operations.IOperationDelegate;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.certification.delegates.CertificationWizardOperationDelegate;

public class NOVCertificationDocIndexWizard extends Wizard
{
    String m_salesValue = "";
    
    protected IWizardPage one;
    protected IWizardPage two;
    private Link m_link_header;
    private Composite m_contentComposite;
    
    public Link getlink_header()
    {
        return m_link_header;
    }
    
    public Link getlink_contained()
    {
        return m_link_contained;
    }
    
    private Link m_link_contained;
    
    public NOVCertificationDocIndexWizard()
    {
        super();
        setWindowTitle("Certification Document Index");
        setNeedsProgressMonitor(true);
    }
    
    @Override
    public boolean canFinish()
    {
        boolean enableFinishButton = false;
        IWizardPage page = this.getContainer().getCurrentPage();
        IWizardPage nextPage = this.getNextPage(page);
        
        if (nextPage == null)
        {
            enableFinishButton = true;
        }
        return enableFinishButton;
    }
    
    @Override
    public void addPages()
    {
        one = new HeaderInfoInputPage();
        two = new ContainedPartsInfoPage();
        addPage(one);
        addPage(two);
    }
    
    @Override
    public void createPageControls(Composite parent)
    {
        createUI(parent);
        super.createPageControls(m_contentComposite);
        getShell().setMinimumSize(750, 450);
    }
    
    private void createUI(Composite parent)
    {
        Composite intermediateComposite = new Composite(parent, SWT.NONE);
        Layout parentLayout = parent.getLayout();
        intermediateComposite.setLayout(new GridLayout(4, true));
        
        Composite listComposite = new Composite(intermediateComposite, SWT.BORDER);
        
        m_link_header = new Link(listComposite, SWT.SINGLE | SWT.V_SCROLL | SWT.FILL);
        m_link_header.setText("<A>Header Information</A>");
        m_link_header.setEnabled(false);
        addListenerOnLink(m_link_header);
        
        m_link_contained = new Link(listComposite, SWT.SINGLE | SWT.V_SCROLL | SWT.FILL);
        m_link_contained.setText("<A>Contained Parts</A>");
        m_link_contained.setEnabled(false);
        addListenerOnLink(m_link_contained);
        
        GridData gData = new GridData(GridData.FILL_BOTH);
        gData.grabExcessVerticalSpace = true;
        gData.grabExcessHorizontalSpace = false;
        listComposite.setLayoutData(gData);
        listComposite.setLayout(new GridLayout(1, true));
        
        m_contentComposite = new Composite(intermediateComposite, SWT.BORDER | SWT.FILL);
        m_contentComposite.setLayout(parentLayout);
        m_contentComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
    }
    
    private void addListenerOnLink(Link link)
    {
        link.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event arg0)
            {
                String selectedVal = arg0.text;
                getContainer().showPage(getPage(selectedVal));
            }
        });
    }
    
    @Override
    public boolean performFinish()
    {
        boolean isFinish = false;
        try
        {
            List<IUIPanel> inputProviders = getInputProviders();
            
            IOperationDelegate certificationWizardDelegate = (IOperationDelegate) new CertificationWizardOperationDelegate(
                    this.getShell());
            certificationWizardDelegate.registerOperationInputProvider(inputProviders);
            isFinish = certificationWizardDelegate.executeOperation();
            if(isFinish)
            {
                disposePanels(inputProviders);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return isFinish;
    }
    
    private void disposePanels(List<IUIPanel> inputProviders) 
    {
		for(int i = 0 ; i < inputProviders.size(); i++)
		{
			inputProviders.get(i).dispose();
		}
	}

	private List<IUIPanel> getInputProviders()
    {
        List<IUIPanel> inputProviders = new ArrayList<IUIPanel>();
        
        IWizardPage[] allPages = getPages();
        
        for (IWizardPage eachWizardPage : allPages)
        {
            if (eachWizardPage instanceof IUIPanel)
            {
                inputProviders.add((IUIPanel) eachWizardPage);
            }
        }
        return inputProviders;
    }
}
