/**
 * 
 */
package com.nov.rac.utilities.certification.search.utils;

import com.nov.rac.utilities.certification.search.report.operation.AbstractCertificationSearchOperation;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author tripathim
 * 
 */
public class CertificationSearchOpeartionFactory
{
    private Registry m_registry = null;
    private String m_commandID = null;
    
    /**
     * Get singleton instance of MassChangeFactory Factory.
     * 
     * @return
     */
    
    public static CertificationSearchOpeartionFactory newInstance()
    {
        return (new CertificationSearchOpeartionFactory());
    }
    
    /**
     * set the Registry, the Registry will be used by Factory for creating UI
     * components.
     * 
     * @param theRegistry
     */
    public void setRegistry(Registry theRegistry)
    {
        m_registry = theRegistry;
    }
    
    /**
     * 
     * @return Current Registry being used by Factory.
     */
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setOperationName(String operationName)
    {
        this.m_commandID = operationName;
    }
    
    private String getOperationName()
    {
        String strOperationName = null;
        if (m_commandID != null)
        {
            int beginIndex = m_commandID.lastIndexOf(".") + 1;
            int endIndex = m_commandID.length();
            strOperationName = m_commandID.substring(beginIndex, endIndex) + ".OPERATION";
            System.out.println("strRegEntry   " + strOperationName);
        }
        return strOperationName;
    }
    
    public AbstractCertificationSearchOperation createOperation(TCComponent selectedComponent)
    {
        System.out.println("");
        AbstractCertificationSearchOperation theOperation = null;
        String operationName = getOperationName();
        Object[] params = new Object[1];
        params[0] = selectedComponent;
        theOperation = (AbstractCertificationSearchOperation) getRegistry().newInstanceFor(operationName, params);
        System.out.println("Operation -> " + theOperation);
        return theOperation;
    }
}
