package com.nov.rac.utilities.certification.search.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.nov.quicksearch.export.ExportTableData;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.util.MessageBox;

public class NOVCertificationReportExporter extends ExportTableData
{
    private String m_reportType;
    private String m_selectedObjectID;
    private static int ZERO_ROW = 0;
    private static int FIRST_ROW = 1;
    private static int SECOND_ROW = 2;
    private static int ZERO_CELL = 0;
    private static int FIRST_CELL = 1;
    
    public NOVCertificationReportExporter(AIFTable aTable)
    {
        super(aTable);
    }
    
    public void exportSearchResultToExcel()
    {
        Display disp = PlatformUI.getWorkbench().getDisplay();
        FileDialog fd = new FileDialog(new Shell(disp), SWT.SAVE);
        fd.setText("Save");
        fd.setFilterPath(System.getProperty("user.home"));
        String[] filterExt = { "*.xls" };
        fd.setFilterExtensions(filterExt);
        String fileName = fd.open();
        
        if (fileName != null && !fileName.isEmpty())
        {
            java.io.File file = new File(fileName);
            try
            {
                exportTableDataToExcel(file);
            }
            catch (FileNotFoundException e)
            {
                MessageBox.post("The file " + file.toString() + " is probably open in another window. "
                        + "Please close the file and try again or save it to a new file. ", "Information",
                        MessageBox.INFORMATION);
            }
            catch (IOException e)
            {
                
                e.printStackTrace();
            }
        }
    }
    
    public void exportTableDataToExcel(File aFile) throws IOException
    {
        FileOutputStream out = new FileOutputStream(aFile);
        
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();
        wb.setSheetName(0, "Search result");
        
        createReportTypeHeader(sheet);
        createHeader(sheet);
        createBody(sheet);
        
        wb.write(out);
        out.close();
        
        MessageBox.post(Display.getDefault().getActiveShell(), "Data successfully exported to " + aFile.toString(),
                "Information", MessageBox.INFORMATION);
    }
    
    public void createReportTypeHeader(HSSFSheet sheet)
    {
        HSSFCellStyle csHeader = cellStyle(sheet.getWorkbook(), null, (short) 0, HSSFFont.BOLDWEIGHT_BOLD,
                new HSSFColor.GREY_40_PERCENT().getIndex(), HSSFCellStyle.SOLID_FOREGROUND,
                HSSFCellStyle.ALIGN_GENERAL, false);
        
        HSSFRow row1 = sheet.createRow(ZERO_ROW);
        HSSFCell cell11 = row1.createCell(ZERO_CELL);
        cell11.setCellValue(m_reportType);
        setThinBorderStyle(cell11, csHeader);
        
        HSSFCell cell12 = row1.createCell(FIRST_CELL);
        setThinBorderStyle(cell12, csHeader);
        
        HSSFRow row2 = sheet.createRow(FIRST_ROW);
        HSSFCell cell21 = row2.createCell(ZERO_CELL);
        cell21.setCellValue("Report for:");
        setThinBorderStyle(cell21, csHeader);
        
        HSSFCell cell22 = row2.createCell(FIRST_CELL);
        cell22.setCellValue(m_selectedObjectID);
        
        // Blank Row
        sheet.createRow(SECOND_ROW);
    }
    
    private void setThinBorderStyle(Cell cell, HSSFCellStyle style)
    {
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cell.setCellStyle(style);
    }
    
    public void setSelectedObject(String selectedObjID)
    {
        m_selectedObjectID = selectedObjID;
    }
    
    public void setReportType(String reportType)
    {
        m_reportType = reportType;
    }
    
}
