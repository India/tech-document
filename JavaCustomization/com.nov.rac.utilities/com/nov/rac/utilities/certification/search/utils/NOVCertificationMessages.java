package com.nov.rac.utilities.certification.search.utils;

import java.util.ResourceBundle;

public class NOVCertificationMessages
{
    
    public NOVCertificationMessages()
    {
    }
    
    public static String getString(String s)
    {
        return RESOURCE_BUNDLE.getString(s);
    }
    
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle("com.nov.rac.utilities.certification.search.utils.utils_locale");
    
}
