package com.nov.rac.utilities.certification.search.utils;

import java.io.File;

import com.nov.rac.utilities.wirelist.utils.NOVDatasetHelper;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;

public class NOVCertificationTemplate
{
    private TCSession m_session = (TCSession) AIFUtility.getDefaultSession();
    private File m_templatFile;
    
    public String getTemplateFilePath()
    {
        String sTemplateFilePath = null;
        try
        {
            TCComponent templateFolder = getTemplateParentFolder();
            TCComponent certificationDoc = getCertificationTemplateDoc(templateFolder);
            m_templatFile = getTemplateFile(certificationDoc);
            if (m_templatFile != null)
            {
                sTemplateFilePath = m_templatFile.getAbsolutePath();
            }
            else
            {
                MessageBox.post(NOVCertificationMessages.getString("NoTemplateFound.ERROR"),
                        NOVCertificationMessages.getString("Error.TITLE"), MessageBox.ERROR);
            }
        }
        catch (TCException e)
        {
            MessageBox.post(e.getMessage(),
                    NOVCertificationMessages.getString("Error.TITLE"), MessageBox.ERROR);
            e.printStackTrace();
        }
        return sTemplateFilePath;
    }
    
    private TCComponent getTemplateParentFolder() throws TCException
    {
        
        TCComponent parentFolder = null;
        TCComponentQueryType tc = (TCComponentQueryType) m_session.getTypeComponent(NOVCertificationMessages
                .getString("queryType.NAME"));
        TCComponent[] queries = new TCComponent[1];
        queries[0] = tc.find(NOVCertificationMessages.getString("generalQuery.NAME"));
        if (queries[0] == null)
        {
            MessageBox.post(NOVCertificationMessages.getString("NoQueryFound.ERROR"),
                    NOVCertificationMessages.getString("Error.TITLE"), MessageBox.ERROR);
        }
        TCComponentQuery qry = (TCComponentQuery) queries[0];
        
        String[] sCriteriaName = new String[3];
        String[] sCriteriaVals = new String[3];
        sCriteriaName[0] = NOVCertificationMessages.getString("QueryCriteriaName1.NAME");
        sCriteriaName[1] = NOVCertificationMessages.getString("QueryCriteriaName2.NAME");
        sCriteriaName[2] = NOVCertificationMessages.getString("QueryCriteriaName3.NAME");
        
        sCriteriaVals[0] = NOVCertificationMessages.getString("QueryCriteriaVal1.NAME");
        sCriteriaVals[1] = NOVCertificationMessages.getString("QueryCriteriaVal2.NAME");
        sCriteriaVals[2] = NOVCertificationMessages.getString("QueryCriteriaVal3.NAME");
        
        if (qry != null)
        {
            AIFComponentContext[] qryResult = qry.getExecuteResults(sCriteriaName, sCriteriaVals);
            if(qryResult.length > 0)
            {
                parentFolder = (TCComponent) qryResult[0].getComponent();
            }
            else
            {
                throw new TCException(NOVCertificationMessages.getString("NoTemplateFound.ERROR"));
            }
        }
        return parentFolder;
    }
    
    private TCComponent getCertificationTemplateDoc(TCComponent folder) throws TCException
    {
        AIFComponentContext[] childrenComp = folder.getChildren();
        TCComponentItem certTemplateDoc = null;
        for (int inx = 0; inx < childrenComp.length; inx++)
        {
            if (childrenComp[inx].getComponent() instanceof TCComponentItem)
            {
                certTemplateDoc = (TCComponentItem) childrenComp[inx].getComponent();
                break;
            }
        }
        if(certTemplateDoc == null)
        {
            throw new TCException(NOVCertificationMessages.getString("NoTemplateFound.ERROR"));
        }
        return certTemplateDoc;
    }
    
    private File getTemplateFile(TCComponent certTemplateDoc) throws TCException
    {
        TCComponentItemRevision certTemplateDocRev = ((TCComponentItem) certTemplateDoc).getLatestItemRevision();
        String[] namedReferences = new String[1];
        NOVDatasetHelper dsHelper = new NOVDatasetHelper(m_session);
        namedReferences = dsHelper.getNamedRefOfDataSet(NOVCertificationMessages.getString("excelDataset.TYPE"));
        
        TCComponent[] datasets = certTemplateDocRev.getRelatedComponents("IMAN_specification");
        TCComponentDataset dataset = null;
        for (int index = 0; datasets != null && index < datasets.length; index++)
        {
            if (datasets[index] instanceof TCComponentDataset)
            {
                dataset = (TCComponentDataset) datasets[index];
                if (dataset.getType().equalsIgnoreCase(namedReferences[0]))
                {
                    break;
                }
            }
        }
        File m_templateWordFile = importDatasetFile(dataset, namedReferences[0]);
        return m_templateWordFile;
    }
    
    private File importDatasetFile(TCComponentDataset dataset, String datasetType) throws TCException
    {
        File[] imanFile = dataset.getFiles(datasetType, getTempFolderLoc());
        return (imanFile != null) ? imanFile[0] : null;
    }
    
    private String getTempFolderLoc()
    {
        TCPreferenceService prefServ = m_session.getPreferenceService();
        String sTCTempDir = prefServ.getString(TCPreferenceService.TC_preference_site,
                NOVCertificationMessages.getString("tempDirPrefrence.NAME"));
        return sTCTempDir;
    }
    
    public void deleteGeneratedFiles()
    {
        if (m_templatFile != null && m_templatFile.isFile())
        {
            m_templatFile.delete();
        }
    }
}
