package com.nov.rac.utilities.certification.search.report.operation;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.nov.rac.utilities.certification.search.utils.NOVCertificationMessages;
import com.nov.rac.utilities.certification.search.utils.NOVCertificationReportExporter;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;

/**
 * @author tripathim
 * 
 */
public abstract class AbstractCertificationSearchOperation implements INOVSearchProvider
{
    public TCComponent m_selectedComponent;
    public String[] m_columnNames;
    public int m_resultRowCount;
    private Vector<String> m_resultRowData;
    private int m_resultColCount;
    public String m_reportType;
    
    public AbstractCertificationSearchOperation(TCComponent selectedComponent)
    {
        this.m_selectedComponent = selectedComponent;
    }
    
    public abstract void init();
    
    public void search()
    {
        INOVSearchResult searchResult = null;
        try
        {
            searchResult = NOVSearchExecuteHelperUtils.execute(this, INOVSearchProvider.LOAD_ALL);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (searchResult != null && searchResult.getResponse().nRows > 0)
        {
            INOVResultSet theResSet = searchResult.getResultSet();
            m_resultRowData = theResSet.getRowData();
            m_resultRowCount = theResSet.getRows();
            m_resultColCount = theResSet.getCols();
        }
        else
        {
            MessageBox.post(NOVCertificationMessages.getString("NoResultFound.INFO"),
                    NOVCertificationMessages.getString("Information.TITLE"), MessageBox.INFORMATION);
        }
    }
    
    public void export()
    {
        final TCTable table = new TCTable(m_columnNames);
        Vector<String> rowData = new Vector<String>();
        for (int i = 0; i < m_resultRowCount; i++)
        {
            rowData.clear();
            int rowIndex = m_resultColCount * i;
            for (int j = 1; j <= m_columnNames.length; j++)
            {
                rowData.add(m_resultRowData.get(rowIndex + j));
            }
            table.addRow(rowData);
        }
        NOVCertificationReportExporter exporter = new NOVCertificationReportExporter(table);
        exporter.setReportType(m_reportType);
        exporter.setSelectedObject(m_selectedComponent.toDisplayString());
        exporter.exportSearchResultToExcel();
    }
    
}
