package com.nov.rac.utilities.certification.search.utils;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;

/**
 * @author tripathim
 * 
 */
public class NOVCertificationSearchValidator
{
    private InterfaceAIFComponent[] m_selectedComps;
    private TCComponent m_component;
    
    public NOVCertificationSearchValidator(InterfaceAIFComponent[] selectedComps)
    {
        this.m_selectedComps = selectedComps;
    }
    
    public boolean validate()
    {
        if (m_selectedComps.length <= 0)
        {
            displayErrorMessage(NOVCertificationMessages.getString("NoObjectSelected.ERROR"));
            return false;
        }
        else if (m_selectedComps.length > 1)
        {
            displayErrorMessage(NOVCertificationMessages.getString("MoreObjectsSelected.ERROR"));
            return false;
        }
        if (!validateSelectedComponent())
        {
            return false;
        }
        return true;
    }
    
    public boolean validateSelectedComponent()
    {
        boolean isValid = false;
        try
        {
            if (m_selectedComps[0] instanceof TCComponentItem)
            {
                m_component = (TCComponent) m_selectedComps[0];
                System.out.println("Component Type: " + m_component.getType());
                isValid = validateDocumentType();
            }
            else if (m_selectedComps[0] instanceof TCComponentItemRevision)
            {
                m_component = ((TCComponentItemRevision) m_selectedComps[0]).getItem();
                isValid = validateDocumentType();
            }
            else if (m_selectedComps[0] instanceof TCComponentForm)
            {
                m_component = (TCComponent) m_selectedComps[0];
                isValid = validateFormType();
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isValid;
    }
    
    private boolean validateDocumentType()
    {
        boolean isValid = true;
        String docTypeCat = null;
        try
        {
            docTypeCat = m_component.getTCProperty("DocumentsCAT").toString();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        if (!docTypeCat.equalsIgnoreCase("CER"))
        {
            displayErrorMessage(NOVCertificationMessages.getString("InvalidDocSelected.ERROR"));
            isValid = false;
        }
        return isValid;
    }
    
    private boolean validateFormType()
    {
        boolean isValid = true;
        if (!m_component.getType().equalsIgnoreCase("Nov4_Standard_Form"))
        {
            displayErrorMessage(NOVCertificationMessages.getString("InvalidFormSelected.ERROR"));
            isValid = false;
        }
        return isValid;
    }
    
    public TCComponent getSelectedComponent()
    {
        return m_component;
    }
    
    private void displayErrorMessage(String errorMessage)
    {
        MessageBox.post(errorMessage, NOVCertificationMessages.getString("Error.TITLE"), MessageBox.ERROR);
    }
}
