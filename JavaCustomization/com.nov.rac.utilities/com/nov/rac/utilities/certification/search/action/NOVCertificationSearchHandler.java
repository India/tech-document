package com.nov.rac.utilities.certification.search.action;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import com.nov.rac.utilities.certification.search.report.operation.AbstractCertificationSearchOperation;
import com.nov.rac.utilities.certification.search.utils.CertificationSearchOpeartionFactory;
import com.nov.rac.utilities.certification.search.utils.NOVCertificationSearchValidator;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;

/**
 * @author tripathim
 * 
 */
public class NOVCertificationSearchHandler extends AbstractHandler
{
    
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        InterfaceAIFComponent[] selectedComps = AIFUtility.getTargetComponents();
        NOVCertificationSearchValidator validator = new NOVCertificationSearchValidator(selectedComps);
        boolean isValid = validator.validate();
        if (!isValid)
        {
            return null;
        }
        TCComponent selectedComponent = validator.getSelectedComponent();
        String commandId = event.getCommand().getId();
        AbstractCertificationSearchOperation m_operation = getOperation(selectedComponent, commandId);
        m_operation.init();
        m_operation.search();
        if (m_operation.m_resultRowCount > 0)
        {
            m_operation.export();
        }
        return null;
    }
    
    private AbstractCertificationSearchOperation getOperation(TCComponent selectedComponent, String commandId)
    {
        Registry m_reg = Registry.getRegistry("com.nov.rac.utilities.certification.search.report.operation.operations");
        CertificationSearchOpeartionFactory theFactory = CertificationSearchOpeartionFactory.newInstance();
        theFactory.setRegistry(m_reg);
        theFactory.setOperationName(commandId);
        AbstractCertificationSearchOperation operation = theFactory.createOperation(selectedComponent);
        return operation;
    }
    
}
