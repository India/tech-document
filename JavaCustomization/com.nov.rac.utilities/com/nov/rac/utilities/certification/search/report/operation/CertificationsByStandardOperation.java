package com.nov.rac.utilities.certification.search.report.operation;

import com.nov.rac.utilities.certification.search.utils.NOVCertificationMessages;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.kernel.TCComponent;

/**
 * @author tripathim
 * 
 */
public class CertificationsByStandardOperation extends AbstractCertificationSearchOperation
{
    
    public CertificationsByStandardOperation(TCComponent selectedComponent)
    {
        super(selectedComponent);
    }
    
    @Override
    public void init()
    {
        m_columnNames = NOVCertificationMessages.getString("doctSearchCol.LIST").split(",");
        m_reportType = NOVCertificationMessages.getString("CertDocByStandard.NAME");
    }
    
    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        String compPuid = m_selectedComponent.getUid();
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_typed_reference;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { compPuid };
        
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { "Nov4_Standard" };
        
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-certification-part-by-relationtype";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2, 3, 4, 5 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 3, 4, 5, 2 };
        
        return allHandlerInfo;
    }
}
