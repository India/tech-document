package com.nov.rac.utilities.certification.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;

import com.nov.quicksearch.export.NOVExportSearchResultUtils;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.utilities.certification.search.utils.NOVCertificationTemplate;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.newobjectlink.GetRegistryInfo;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVExcelReportGenerator
{
	private HSSFCellStyle m_cellStyle;
	private HSSFFont m_fontStyle;
	
    public NOVExcelReportGenerator()
    {
		m_registry = Registry.getRegistry(this);
	}

	public boolean generateExcelReport(IPropertyMap mPropMapForExcel)
	{
		//8677
		boolean isFileCreated = false;
		
		try 
		{
			//POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream("C:\\Users\\rakat1\\Desktop\\template.xls"));
		    NOVCertificationTemplate templateObj = new NOVCertificationTemplate(); 
		    String sTemplateFilePath = templateObj.getTemplateFilePath();
		    if(sTemplateFilePath == null){
		        return true;
		    }
		    POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(sTemplateFilePath));
			HSSFWorkbook wb = new  HSSFWorkbook(fs, false);
			HSSFSheet baseSheet = wb.getSheet(m_registry.getString("Template.SHEET_NAME"));
			
			//Table cellstyle formatting
			HSSFCellStyle tableCellStyle = wb.createCellStyle();
			HSSFFont tableFont = wb.createFont();
			tableFont.setFontHeightInPoints((short)8);
			tableCellStyle.setFont(tableFont);
			tableCellStyle.setWrapText(true);
			tableCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER );	
			tableCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);	
			
			
			int lastRow = baseSheet.getPhysicalNumberOfRows();		
			
			
			//Remove later Cell content formatting
			setCellFormatting(wb);
			
			//String[] colsToBePopulatedInExcel = m_registry.getString("ColumnsToBePopulatedInExcel.Arr").split(",");
			
			//Values obtained from first page of wizard
			String[] excelHeaderNames = {"Project Name","Block Drawing","Customer Name","Point Drawing","Quote Number",
			                            "Revision","Customer PO","Prepared By","Sales Order","Approved By"};
			
			for(int i = 0; i < excelHeaderNames.length; i++)
            {
                updateField(baseSheet, excelHeaderNames[i], mPropMapForExcel.getString(excelHeaderNames[i]));
            }
			
			Map<String, Integer> excelColumnNameIndexMap = new HashMap<String, Integer>();
			excelColumnNameIndexMap.put(m_registry.getString("AssemblyPartNo.NAME"), 2);
			excelColumnNameIndexMap.put(m_registry.getString("Description.NAME"),3);
			excelColumnNameIndexMap.put(m_registry.getString("ContainedPartNo.NAME"), 4);
			excelColumnNameIndexMap.put(m_registry.getString("Manufacturer.NAME"), 5);
			excelColumnNameIndexMap.put(m_registry.getString("ManufacturerPN.NAME"), 6);
			excelColumnNameIndexMap.put(m_registry.getString("CertificateNo.NAME"), 8);
			
			String[][] excelRowVals = populateStringData(mPropMapForExcel.getCompoundArray("TableRows"), m_registry.getString("DocIndexTable.COLUMN_NAMES"));
			//Populate excel rows
		
			for( int row = 0; row < excelRowVals.length; row++ )
			{
				int colCount = 0;
//				HSSFCellStyle tableCellStyle = m_cellStyle;
				
				HSSFRow newRow = baseSheet.createRow( lastRow + row );
				for( int col = 0; col < excelHeaderNames.length; col++ )
				{
					//8677
					Cell newCell = newRow.createCell(col);
					newCell.setCellStyle(tableCellStyle);
					//Populate value inside cell
					if(excelColumnNameIndexMap.values().contains(col))
					{
						newCell.setCellValue(excelRowVals[row][colCount++]);
					}
					NOVExportSearchResultUtils.setThinBorderStyle(newCell, tableCellStyle);
					
				}
			}		
			
			isFileCreated =  generateReportSaveDialog(wb);
	        templateObj.deleteGeneratedFiles();

		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}	
		return isFileCreated;
	}
	
	private void setCellFormatting(HSSFWorkbook wb) 
	{
		m_cellStyle = wb.createCellStyle();
		m_fontStyle = wb.createFont();
		m_fontStyle.setFontHeightInPoints((short)8);
		m_cellStyle.setFont(m_fontStyle);
		m_cellStyle.setWrapText(true);
		m_cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER );	
		m_cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);	
	}

	private void updateField(HSSFSheet sheet, String propName, String value)
	{
		for (Row row : sheet) 
	    {
	        for (Cell cell : row)
	        {
	            if (cell.getCellType() == Cell.CELL_TYPE_STRING) 
	            {
	                if (cell.getRichStringCellValue().getString().trim().equals(propName))
	                {
	                    row.getCell(cell.getColumnIndex()+1).setCellValue(value);
	                    
	                    //Font adjustments
	                    row.getCell(cell.getColumnIndex()+1).setCellStyle(m_cellStyle);
	                   
	                }
	            }
	        }
	    }    
	}
	
	private String[][] populateStringData(IPropertyMap[] propMaps, String columnHeaderString)
    {
        String[][] propData = new String[propMaps.length][6];
        String[] propNames = columnHeaderString.split(",");
        String strPropValue = null;
        // Process Each propertyMap
        for(int inx=0;inx<propMaps.length; inx++)
        {
            // Read each property.
            for(int propIndex=0; propIndex<propNames.length;propIndex++)
            {
                strPropValue = PropertyMapHelper.handleNull( propMaps[inx].getString( propNames[propIndex] ) );
                propData[inx][propIndex] = strPropValue;
            }
        }
        return propData;
    }
	
	public boolean generateReportSaveDialog(HSSFWorkbook wb) throws IOException
	{
		File excelFile = null;
		boolean isFileCreated = false;
		try 
		{
			FileDialog fd = new FileDialog(AIFUtility.getActiveDesktop().getShell(), SWT.SAVE);
		    fd.setText("Save");
		    fd.setFilterPath(System.getProperty("user.home"));
		    String[] filterExt = { "*.xls" };
		    fd.setFilterExtensions(filterExt);
		    String fileName = fd.open();
		    
		    if( fileName != null && !fileName.isEmpty() )
		    {
		    	excelFile = new File(fileName);
		    	FileOutputStream fileOut = new FileOutputStream(excelFile); 
				wb.write(fileOut);
				fileOut.close();
				isFileCreated = true;
		    }
		}
	    catch (FileNotFoundException e)
		{
	    	MessageBox.post("The file " + excelFile.toString()+" is probably open in another window. " +
                    "Please close the file and try again or save it to a new file. ", 
                     "Information", MessageBox.INFORMATION);
	    	isFileCreated = generateReportSaveDialog(wb);
		} 
	    catch (IOException e) 
		{
			e.printStackTrace();
		}
		return isFileCreated;
	}
	
	private Registry m_registry;
}
