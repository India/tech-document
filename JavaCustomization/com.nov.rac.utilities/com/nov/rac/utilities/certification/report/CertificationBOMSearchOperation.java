package com.nov.rac.utilities.certification.report;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.MessageBox;

public class CertificationBOMSearchOperation implements INOVSearchProvider

{
	private TCComponent m_selectedComponent;
    public CertificationBOMSearchOperation(TCComponent selectedComponent)
    {
    	m_selectedComponent = selectedComponent;
        
    }

    @Override
    public String getBusinessObject()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public QuickBindVariable[] getBindVariables()
    {
        String compPuid = m_selectedComponent.getUid();
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[3];
        
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_typed_reference;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { compPuid };
        
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { "view" };
        
        allBindVars[2] = new QuickSearchService.QuickBindVariable();
        allBindVars[2].nVarType = POM_string;
        allBindVars[2].nVarSize = 1;
        allBindVars[2].strList = new String[] { "Nov4_Certification" };
        
        return allBindVars;
    }

    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[3];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-all-level-child-BOMLine";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 4/*Contained Part#*/, 5/*Part name*/};
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2 }; 
        
        allHandlerInfo[1] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[1].handlerName = "NOVSRCH-get-nov4part-latest-revs";
        allHandlerInfo[1].listBindIndex = new int[0];
        allHandlerInfo[1].listReqdColumnIndex = new int[] { 1, 3, 4 };//
        allHandlerInfo[1].listInsertAtIndex = new int[] { 7, 3, 4 }; 
        
        allHandlerInfo[2] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[2].handlerName = "NOVSRCH-get-secondary-item-by-relationtype";
        allHandlerInfo[2].listBindIndex = new int[] { 3 };
        allHandlerInfo[2].listReqdColumnIndex = new int[] {1, 2};
        allHandlerInfo[2].listInsertAtIndex = new int[] {5, 6 }; 
        
        return allHandlerInfo;
    }
    
    
}  
   