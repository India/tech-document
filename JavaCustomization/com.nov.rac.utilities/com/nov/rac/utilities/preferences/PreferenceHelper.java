package com.nov.rac.utilities.preferences;

import java.util.Arrays;
import java.util.List;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.PreferenceObject;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCPreferenceService.TCPreferenceLocation;
import com.teamcenter.rac.kernel.TCSession;

public class PreferenceHelper 
{

	
	public static String[] getStringValues(String preferenceName, int preferenceLevel)
	{
		String[] stringValues = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility
				.getDefaultSession()).getPreferenceService();

		stringValues = prefServ.getStringArray(preferenceLevel, preferenceName);
		
		// Abhijit:- Not Able to get Registry:: Need to Revisit for Error Msg.
		
//		Registry registry = Registry.getRegistry("com.nov.rac.utilities.preferences.preferences");
//		if(stringValues.length==0)
//		{
//			MessageBox.post(registry.getString("NO_PREFERENCE.Error")+" " +preferenceName,registry.getString("Error.TITLE"), MessageBox.ERROR);
//		}

		return stringValues;
		
	}
	public static String getStringValue(String preferenceName, int preferenceLevel)
	{
		String stringValue = null;

		TCPreferenceService prefServ = ((TCSession) AIFUtility
				.getDefaultSession()).getPreferenceService();

		stringValue = prefServ.getStringValue(preferenceName);	

		return stringValue;
		
	}
	
	public static int getIntegerValue(String preferenceName, int preferenceLevel)
	{
		int intValue = 0;

		TCPreferenceService prefServ = ((TCSession) AIFUtility
				.getDefaultSession()).getPreferenceService();

		intValue = prefServ.getInt(preferenceLevel, preferenceName, -2);
	

		return intValue;
		
	}
	
	public boolean isGroupRequireSerialization()
	{
		return isPreferenceExist("NOV_rsone_serializable_classes", TCPreferenceService.TC_preference_group);
	}
	
	public boolean isPreferenceExist(String preferenceName, int preferenceLevel)
	{
		TCPreferenceService prefServ = ((TCSession) AIFUtility
				.getDefaultSession()).getPreferenceService();
		PreferenceObject prefObj = prefServ.getPreferenceUsage(preferenceName, preferenceLevel);
		boolean prefExist = prefObj != null;
		return prefExist;
	}
	
	public boolean isSerializedClass(Object className)
	{
		boolean isSerialized = false;
		if(null != className)
		{
			initializeArr();
			isSerialized = this.listOfSerializableClasses.contains(className);
		}
		return isSerialized;
	}
	
	private void initializeArr()
	{
		if(null == this.listOfSerializableClasses)
		{
			String prefName = "NOV_rsone_serializable_classes";
			
			String[] serializableClasses = getStringValues(prefName, TCPreferenceService.TC_preference_group);
			
			this.listOfSerializableClasses = Arrays.asList(serializableClasses);
		}
	}
	
	public static int getParsedPrefValueForGroup(String preferenceName, int preferenceLevel, String loginGroupName, String delimiter)	{
		int value = 32;
		
		String[] prefValues = getStringValues(preferenceName, preferenceLevel );
		
		for( int index = 0; index <  prefValues.length; ++index )
		{
			int delimiterPos = prefValues[index].indexOf( delimiter );
			if(delimiterPos >=0)  //TCDECREL-8088
			{
				String tempGroup = prefValues[index].substring( 0, delimiterPos	);
				
				if( tempGroup != null && tempGroup.length() > 0 && tempGroup.equalsIgnoreCase( loginGroupName ) )
				{
					String tempLength = prefValues[index].substring( delimiterPos + 1, prefValues[index].length() );
					value = Integer.parseInt( tempLength );
					break;
				}
			}
		}
		
		return value;
	}
	
	/**
     * To check whether specified group is present in the specified preference or not
     * return true if specified group is present in the specified preference 
     * else returns false
     * @param group
     * @param preference
     * @param prefLocation - Scope of a preference
     * @return
     */
    public static boolean isGroupExistInPref(String group, String preference, TCPreferenceLocation prefLocation)
    {
       boolean isGroupMatched = false;
          
       TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
       
       TCPreferenceService prefServ = session.getPreferenceService();
       
       String[] preferenceValues = prefServ.getStringValuesAtLocation(preference, prefLocation);
       
       for (int i=0;i<preferenceValues.length;i++) 
       {
          if (group.contains(preferenceValues[i]))
          {
               isGroupMatched =  true;
               break;
          }
       }
       
       return isGroupMatched;
    }
    
    /**
     * To check whether specified string is present in the specified preference or not
     * return true if specified string is present in the specified preference 
     * else returns false
     * @param string
     * @param preference
     * @param prefLocation - Scope of a preference
     * @return
     */
    public static boolean isStringExistInPref(String string, String preference, TCPreferenceLocation prefLocation)
    {
       boolean isStringMatched = false;
          
       TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
       
       TCPreferenceService prefServ = session.getPreferenceService();
       
       String[] preferenceValues = prefServ.getStringValuesAtLocation(preference, prefLocation);
       
       for (int i=0;i<preferenceValues.length;i++) 
       {
          if (string.contains(preferenceValues[i]))
          {
        	  isStringMatched =  true;
               break;
          }
       }
       
       return isStringMatched;
    }

	private List<String> listOfSerializableClasses;
	
}
