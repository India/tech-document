package com.nov.rac.utilities.databinding;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;

public class DataBindingModel implements PropertyChangeListener
{

	private Map<String, Object> 	m_propNameValeMap = null;
	private PropertyChangeSupport 	m_propertyChangeSupport = new PropertyChangeSupport(this);
	
	private boolean 				m_SaveRequired = false;

	public DataBindingModel()
	{
		m_propNameValeMap = new HashMap<String, Object>();
	}

	@Override
	public void propertyChange(PropertyChangeEvent propertychangeevent) 
	{
		Object newPropValue = propertychangeevent.getNewValue();
		Object oldPropValue = propertychangeevent.getOldValue();
		String propName = propertychangeevent.getPropertyName();
		m_propertyChangeSupport.firePropertyChange( propName, oldPropValue, newPropValue);
	}

	public void setProperty(String propName, Object newPropValue)
	{
		// Get prop old value.
		Object oldPropValue = getProperty(propName);

		// Set new value to property.
		setPropValue(propName,newPropValue);
		
		if(newPropValue != oldPropValue){
			m_SaveRequired = true;
		}
		
//		if(!newPropValue.equals(oldPropValue)){
//			m_SaveRequired = true;
//		}

		m_propertyChangeSupport.firePropertyChange( propName, oldPropValue, newPropValue);
	}

	public Object getProperty( String propName )
	{
		Object objValue = m_propNameValeMap.get(propName);

		return objValue;
	}

	private void setPropValue(String propName, Object propValue)
	{
		m_propNameValeMap.put(propName, propValue);
	}
	
	public void addPropertyChangeListener(String propertyName,
			PropertyChangeListener listener) {
		m_propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		m_propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	public void clear(){
		m_propNameValeMap.clear();
		m_SaveRequired = false;
	}
	
	public boolean isDirty(){
		return m_SaveRequired;
	}
	
	public void setDirty(boolean flag){
		m_SaveRequired = flag;
	}
}
