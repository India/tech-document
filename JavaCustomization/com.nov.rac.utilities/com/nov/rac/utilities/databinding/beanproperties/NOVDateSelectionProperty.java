package com.nov.rac.utilities.databinding.beanproperties;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.IProperty;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;


import com.teamcenter.rac.util.DateButton;

public class NOVDateSelectionProperty extends SimpleValueProperty{

	public NOVDateSelectionProperty() {
		super();
	}

	@Override
	public Object getValueType() {
		return null;
	}

	public INativePropertyListener adaptListener(ISimplePropertyListener listener)
	{
		return new DateButtonListener(this, listener);
	}

	private class DateButtonListener extends NativePropertyListener
	implements PropertyChangeListener//,ActionListener
	{

		protected void doAddTo(Object source)
		{
			DateButton dateButton = (DateButton)source;

//			dateButton.addActionListener(this);
			dateButton.addPropertyChangeListener(this);
		}

		protected void doRemoveFrom(Object source)
		{
		}

		protected DateButtonListener(IProperty property, ISimplePropertyListener listener)
		{
			super(property, listener);
		}

//		@Override
//		public void actionPerformed(ActionEvent actionevent) {
//			fireChange(actionevent.getSource(), null);	
//		}

		@Override
		public void propertyChange(PropertyChangeEvent propertychangeevent) {
			
			if(propertychangeevent.getPropertyName().compareTo(DateButton.TEXT_CHANGED_PROPERTY) == 0){
				fireChange(propertychangeevent.getSource(), null);
			}
						
		}
	}

	@Override
	protected Object doGetValue(Object source) {
		
		return ((DateButton)source).getDate();
	}

	@Override
	protected void doSetValue(Object source, Object val) {
		
		((DateButton)source).setDate((Date) val);
	}
	
}
