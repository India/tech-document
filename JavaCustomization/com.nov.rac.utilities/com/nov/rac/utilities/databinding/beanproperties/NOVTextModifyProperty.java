package com.nov.rac.utilities.databinding.beanproperties;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JTextField;

import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.IProperty;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;


import com.teamcenter.rac.util.DateButton;

public class NOVTextModifyProperty extends SimpleValueProperty{

	public NOVTextModifyProperty() {
		super();
	}

	@Override
	public Object getValueType() {
		return null;
	}

	public INativePropertyListener adaptListener(ISimplePropertyListener listener)
	{
		return new TextListener(this, listener);
	}

	private class TextListener extends NativePropertyListener
	implements PropertyChangeListener
	{

		protected void doAddTo(Object source)
		{
			JTextField txtField = (JTextField)source;

			txtField.addActionListener((ActionListener)this);
			
		}
		protected void doRemoveFrom(Object source)
		{
		}

		protected TextListener(IProperty property, ISimplePropertyListener listener)
		{
			super(property, listener);
		}

//		@Override
//		public void actionPerformed(ActionEvent actionevent) {
//			fireChange(actionevent.getSource(), null);	
//		}
		@Override
		public void propertyChange(PropertyChangeEvent propertychangeevent) {
			fireChange(propertychangeevent.getSource(), null);
			
		}
	}

	@Override
	protected Object doGetValue(Object source) {
		
		return ((JTextField)source).getText();
	}

	@Override
	protected void doSetValue(Object source, Object val) {
		
		((JTextField)source).setText((String) val);
	}
	
}
