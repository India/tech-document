package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyDescriptor;
import org.eclipse.core.databinding.beans.*;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.masterdetail.IObservableFactory;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.property.list.IListProperty;
import org.eclipse.core.databinding.property.list.ListProperty;

// Referenced classes of package org.eclipse.core.internal.databinding.beans:
//            BeanObservableListDecorator

public class NOVBeanListPropertyDecorator extends ListProperty
    implements IBeanListProperty
{

    public NOVBeanListPropertyDecorator(IListProperty delegate, PropertyDescriptor propertyDescriptor)
    {
        _flddelegate = delegate;
        this.propertyDescriptor = propertyDescriptor;
    }

    public Object getElementType()
    {
        return _flddelegate.getElementType();
    }

    public IBeanListProperty values(String propertyName)
    {
        return values(propertyName, null);
    }

    public IBeanListProperty values(String propertyName, Class valueType)
    {
        Class beanClass = (Class)_flddelegate.getElementType();
        return values(NOVBeanPropertyHelper.value(beanClass, propertyName, valueType));
    }

    public IBeanListProperty values(IBeanValueProperty property)
    {
        return new NOVBeanListPropertyDecorator(super.values(property), property.getPropertyDescriptor());
    }

    public PropertyDescriptor getPropertyDescriptor()
    {
        return propertyDescriptor;
    }

    public IObservableList observe(Object source)
    {
    	return super.observe(source);
//        return new NOVBeanObservableListDecorator(_flddelegate.observe(source), propertyDescriptor);
    }

    public IObservableList observe(Realm realm, Object source)
    {
        return new NOVBeanObservableListDecorator(_flddelegate.observe(realm, source), propertyDescriptor);
//    	return new NOVBeanObservableValueDecorator(m_delegate.observe(realm, source), m_propertyDescriptor);
    }

    public IObservableFactory listFactory()
    {
        return _flddelegate.listFactory();
    }

    public IObservableFactory listFactory(Realm realm)
    {
        return _flddelegate.listFactory(realm);
    }

    public IObservableList observeDetail(IObservableValue master)
    {
        return new NOVBeanObservableListDecorator(_flddelegate.observeDetail(master), propertyDescriptor);
    }

    public String toString()
    {
        return _flddelegate.toString();
    }

    private final IListProperty _flddelegate;
    private final PropertyDescriptor propertyDescriptor;
}
