package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.IDiff;
import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;



public class NOVBeanValueProperty extends SimpleValueProperty{

	private PropertyDescriptor m_propertyDescriptor = null;
	private PropertyChangeListener m_listener = null;    
    private final Class m_valueType;
	
	public NOVBeanValueProperty(PropertyDescriptor propertyDescriptor,
			Class valueType) {		
		
		m_propertyDescriptor = propertyDescriptor;
		m_valueType = valueType;
	}

	@Override
	protected Object doGetValue(Object source) {
		Object obj = NOVBeanPropertyHelper.readProperty(source, m_propertyDescriptor);
		
		m_listener = (PropertyChangeListener) source;
		
		return obj;
	}
	
    protected void doSetValue(Object source, Object value)
    {
    	Object obj = NOVBeanPropertyHelper.writeProperty(source, m_propertyDescriptor,value);
    }

	@Override
	public INativePropertyListener adaptListener(ISimplePropertyListener listener) {
		
		return new NOVBeanPropertyListener(this, m_propertyDescriptor, listener) {

            protected IDiff computeDiff(Object oldValue, Object newValue)
            {
                return Diffs.createValueDiff(oldValue, newValue);
            }

        };
	}

	@Override
	public Object getValueType() {
		
		return m_valueType;
	}

	
}
