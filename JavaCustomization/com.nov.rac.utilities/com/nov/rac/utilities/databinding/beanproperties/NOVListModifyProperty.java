package com.nov.rac.utilities.databinding.beanproperties;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.IProperty;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;


import com.teamcenter.rac.util.DateButton;

public class NOVListModifyProperty extends SimpleValueProperty{

	public NOVListModifyProperty() {
		super();
	}

	@Override
	public Object getValueType() {
		return null;
	}

	public INativePropertyListener adaptListener(ISimplePropertyListener listener)
	{
		return new ListListener(this, listener);
	}

	private class ListListener extends NativePropertyListener
	implements Listener
	{

		protected void doAddTo(Object source)
		{
			List list = (List)source;

//			dateButton.addActionListener(this);
			list.addListener(SWT.Modify,this);
		}

		protected void doRemoveFrom(Object source)
		{
		}

		protected ListListener(IProperty property, ISimplePropertyListener listener)
		{
			super(property, listener);
		}

//		@Override
//		public void actionPerformed(ActionEvent actionevent) {
//			fireChange(actionevent.getSource(), null);	
//		}

//		@Override
//		public void propertyChange(PropertyChangeEvent propertychangeevent) {
//			
//			if(propertychangeevent.getPropertyName().compareTo(DateButton.TEXT_CHANGED_PROPERTY) == 0){
//				fireChange(propertychangeevent.getSource(), null);
//			}
//						
//		}

		@Override
		public void handleEvent(Event event) {
			
			fireChange(event.data, null);
		}
	}

	@Override
	protected Object doGetValue(Object source) {
		
		if(source == null)
			return null;
		else{
			int index = ((List)source).getSelectionIndex();
			if (index != -1){
				return ((List)source).getItem(index);
			}
			else
				return null;
		}
	}

	@Override
	protected void doSetValue(Object source, Object val) {
		
		if(val != null)
			((List)source).setItems((String[]) val);
	}
	
}
