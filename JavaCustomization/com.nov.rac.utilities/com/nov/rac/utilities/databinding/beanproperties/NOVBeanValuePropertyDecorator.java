package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.IBeanListProperty;
import org.eclipse.core.databinding.beans.IBeanMapProperty;
import org.eclipse.core.databinding.beans.IBeanSetProperty;
import org.eclipse.core.databinding.beans.IBeanValueProperty;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.masterdetail.IObservableFactory;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.core.databinding.property.value.ValueProperty;


public class NOVBeanValuePropertyDecorator extends ValueProperty implements IBeanValueProperty
{

	private IValueProperty m_delegate = null;
	private PropertyDescriptor m_propertyDescriptor = null;
	
	public NOVBeanValuePropertyDecorator(IValueProperty delegate,
			PropertyDescriptor propertyDescriptor) {
		
		m_propertyDescriptor = propertyDescriptor;
		
		m_delegate = delegate;
	}

	@Override
	public IObservableValue observe(Object source) {
		// TODO Auto-generated method stub
		return super.observe(source);
	}

	@Override
	public IObservableValue observe(Realm realm, Object source) {
		// TODO Auto-generated method stub
		
		return new NOVBeanObservableValueDecorator(m_delegate.observe(realm, source), m_propertyDescriptor);
	}
	
    public IBeanValueProperty value(String propertyName, Class valueType)
    {
        Class beanClass = (Class)m_delegate.getValueType();
        return value(NOVBeanPropertyHelper.value(beanClass, propertyName, valueType));
    }

    public IBeanValueProperty value(IBeanValueProperty property)
    {
        return new NOVBeanValuePropertyDecorator(super.value(property), property.getPropertyDescriptor());
    }
    
    
    //===============================================================================
    
    public PropertyDescriptor getPropertyDescriptor()
    {
        return m_propertyDescriptor;
    }

    public Object getValueType()
    {
        return m_delegate.getValueType();
    }

    public IBeanValueProperty value(String propertyName)
    {
        return value(propertyName, null);
    }

  

    public IBeanListProperty list(String propertyName)
    {
        return list(propertyName, null);
    }


    public IBeanSetProperty set(String propertyName)
    {
        return set(propertyName, null);
    }

    public IBeanMapProperty map(String propertyName)
    {
        return map(propertyName, null, null);
    }

    public IBeanMapProperty map(String propertyName, Class keyType, Class valueType)
    {
        Class beanClass = (Class)m_delegate.getValueType();
        return (IBeanMapProperty) map(BeanProperties.map(beanClass, propertyName, keyType, valueType));
    }

 
    public IObservableFactory valueFactory()
    {
        return m_delegate.valueFactory();
    }

    public IObservableFactory valueFactory(Realm realm)
    {
        return m_delegate.valueFactory(realm);
    }

   
    public String toString()
    {
        return m_delegate.toString();
    }

	@Override
	public IObservableList observeDetail(IObservableList iobservablelist) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IObservableMap observeDetail(IObservableSet iobservableset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IObservableMap observeDetail(IObservableMap iobservablemap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IBeanListProperty list(IBeanListProperty ibeanlistproperty) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IBeanListProperty list(String s, Class class1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IBeanMapProperty map(IBeanMapProperty ibeanmapproperty) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IBeanSetProperty set(IBeanSetProperty ibeansetproperty) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IBeanSetProperty set(String s, Class class1) {
		// TODO Auto-generated method stub
		return null;
	}

}
