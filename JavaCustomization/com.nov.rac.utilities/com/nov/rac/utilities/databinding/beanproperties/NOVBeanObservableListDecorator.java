package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyDescriptor;
import java.util.Collection;

import org.eclipse.core.databinding.beans.IBeanObservable;
import org.eclipse.core.databinding.observable.IObserving;
import org.eclipse.core.databinding.observable.list.DecoratingObservableList;
import org.eclipse.core.databinding.observable.list.IListChangeListener;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.ListChangeEvent;
import org.eclipse.core.databinding.observable.list.ListDiff;
import org.eclipse.jface.databinding.swt.ISWTObservableList;
import org.eclipse.swt.widgets.Widget;

public class NOVBeanObservableListDecorator extends DecoratingObservableList
    implements IBeanObservable
{

    public NOVBeanObservableListDecorator(IObservableList decorated, PropertyDescriptor propertyDescriptor)
    {
        super(decorated, true);
        this.propertyDescriptor = propertyDescriptor;
    }

    public synchronized void dispose()
    {
        propertyDescriptor = null;
        super.dispose();
    }

    public Object getObserved()
    {
        org.eclipse.core.databinding.observable.IObservable decorated = getDecorated();
        if(decorated instanceof IObserving)
            return ((IObserving)decorated).getObserved();
        else
            return null;
    }

    public PropertyDescriptor getPropertyDescriptor()
    {
        return propertyDescriptor;
    }

    
    @Override
	public synchronized void addListChangeListener(IListChangeListener listener) {
		// TODO Auto-generated method stub
		super.addListChangeListener(listener);
	}

	@Override
	protected void fireListChange(ListDiff diff) {
		// TODO Auto-generated method stub
		super.fireListChange(diff);
	}

	@Override
	protected void fireChange() {
		// TODO Auto-generated method stub
		super.fireChange();
	}

	@Override
	protected void firstListenerAdded() {
		// TODO Auto-generated method stub
		super.firstListenerAdded();
	}

	@Override
	protected void handleListChange(ListChangeEvent event) {
		// TODO Auto-generated method stub
		super.handleListChange(event);
	}

	@Override
	public void add(int index, Object o) {
		// TODO Auto-generated method stub
		super.add(index, o);
	}

	@Override
	public boolean addAll(int index, Collection c) {
		// TODO Auto-generated method stub
		return super.addAll(index, c);
	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		return super.get(index);
	}

	@Override
	public Object set(int index, Object element) {
		// TODO Auto-generated method stub
		return super.set(index, element);
	}

	private PropertyDescriptor propertyDescriptor;

}
