package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.beans.IBeanObservable;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.IDisposeListener;
import org.eclipse.core.databinding.observable.IObserving;
import org.eclipse.core.databinding.observable.IStaleListener;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.DecoratingObservableValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;

public class NOVBeanObservableValueDecorator extends DecoratingObservableValue implements IBeanObservable 
{

	  public NOVBeanObservableValueDecorator(IObservableValue decorated, PropertyDescriptor propertyDescriptor)
	    {
	        super(decorated, true);
	        this.propertyDescriptor = propertyDescriptor;
	    }

	    public synchronized void dispose()
	    {
	        propertyDescriptor = null;
	        super.dispose();
	    }

	    public Object getObserved()
	    {
	        org.eclipse.core.databinding.observable.IObservable decorated = getDecorated();
	        if(decorated instanceof IObserving)
	            return ((IObserving)decorated).getObserved();
	        else
	            return null;
	    }

	    public PropertyDescriptor getPropertyDescriptor()
	    {
	        return propertyDescriptor;
	    }

	    private PropertyDescriptor propertyDescriptor;

}
