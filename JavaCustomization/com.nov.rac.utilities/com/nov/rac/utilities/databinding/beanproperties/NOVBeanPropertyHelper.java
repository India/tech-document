package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.core.databinding.beans.IBeanListProperty;
import org.eclipse.core.databinding.beans.IBeanValueProperty;
import org.eclipse.core.databinding.property.list.IListProperty;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.core.databinding.util.Policy;
import org.eclipse.core.internal.databinding.property.ListPropertyDetailValuesList;
import org.eclipse.core.runtime.Status;

public class NOVBeanPropertyHelper {

	public static IBeanValueProperty value(Class beanClass, String propertyName, Class valueType)
	{
		NOVPropertyDescriptor propertyDescriptor = null;
		IValueProperty property;
		try {
			propertyDescriptor = new NOVPropertyDescriptor(propertyName, beanClass, "getProperty","setProperty");
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		
		IBeanValueProperty beanProperty = null;
		
		if(propertyDescriptor != null){
			property = new NOVBeanValueProperty(propertyDescriptor, valueType);
			beanProperty = new NOVBeanValuePropertyDecorator(property, propertyDescriptor);
		}
		

		return beanProperty;
	}

	public static IBeanValueProperty value(Class beanClass, String propertyName)
	{
		return value(beanClass, propertyName, null);
	}
	
	public static IBeanListProperty values(Class beanClass, String propertyName)
	{
		return values(beanClass, propertyName, null);
	}
	
	
    public static Object readProperty(Object source, PropertyDescriptor propertyDescriptor)
    {
        Method readMethod;
        readMethod = propertyDescriptor.getReadMethod();
        if(readMethod == null)
            throw new IllegalArgumentException(propertyDescriptor.getName() + " property does not have a read method.");
        if(!readMethod.isAccessible())
            readMethod.setAccessible(true);
        
        Object propertyName = propertyDescriptor.getName();
        
        try {
			return readMethod.invoke(source, new Object[]{propertyName});
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
    }

	public static Object writeProperty(Object source,PropertyDescriptor propertyDescriptor, Object value) {
		
		try
        {
            Method writeMethod = propertyDescriptor.getWriteMethod();
            if(!writeMethod.isAccessible())
                writeMethod.setAccessible(true);
            
            Object propertyName = propertyDescriptor.getName();
            
            writeMethod.invoke(source, new Object[] {propertyName, value });
        }
        catch(InvocationTargetException e)
        {
            throw new RuntimeException(e.getCause());
        }
        catch(Exception e)
        {
            Policy.getLog().log(new Status(2, "org.eclipse.core.databinding", 0, "Could not change value of " + source + "." + propertyDescriptor.getName(), e));
        }

		return null;
	}
	
    public static IBeanListProperty list(Class beanClass, String propertyName, Class elementType)
    {
    	NOVPropertyDescriptor propertyDescriptor = null;
		IListProperty property;

		try {
			propertyDescriptor = new NOVPropertyDescriptor(propertyName, beanClass, "getProperty","setProperty");
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		
		IBeanListProperty beanProperty = null;
		
		if(propertyDescriptor != null){
			property = new NOVBeanListProperty(propertyDescriptor, elementType);
			beanProperty = new NOVBeanListPropertyDecorator(property, propertyDescriptor);
		}

		return beanProperty;
    }
	
	public static IBeanListProperty values(Class beanClass, String propertyName, Class valueType)
	{
		NOVPropertyDescriptor propertyDescriptor = null;
		IListProperty property;

		try {
			propertyDescriptor = new NOVPropertyDescriptor(propertyName, beanClass, "getProperty","setProperty");
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		
		IBeanListProperty beanProperty = null;
		
		if(propertyDescriptor != null){
			property = new NOVBeanListProperty(propertyDescriptor, valueType);
			beanProperty = new NOVBeanListPropertyDecorator(property, propertyDescriptor);
		}

		return beanProperty;
	}

}
