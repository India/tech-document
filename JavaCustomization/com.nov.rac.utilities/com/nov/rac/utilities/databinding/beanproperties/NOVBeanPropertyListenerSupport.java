
package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import org.eclipse.core.databinding.util.Policy;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.Status;

public class NOVBeanPropertyListenerSupport
{

    public NOVBeanPropertyListenerSupport()
    {
    }

    public static void hookListener(Object bean, String propertyName, PropertyChangeListener listener)
    {
        Assert.isNotNull(bean, "Bean cannot be null");
        Assert.isNotNull(listener, "Listener cannot be null");
        Assert.isNotNull(propertyName, "Property name cannot be null");
        processListener(bean, propertyName, listener, "addPropertyChangeListener", "Could not attach listener to ");
    }

    public static void unhookListener(Object bean, String propertyName, PropertyChangeListener listener)
    {
        Assert.isNotNull(bean, "Bean cannot be null");
        Assert.isNotNull(listener, "Listener cannot be null");
        Assert.isNotNull(propertyName, "Property name cannot be null");
        processListener(bean, propertyName, listener, "removePropertyChangeListener", "Cound not remove listener from ");
    }

    private static boolean processListener(Object bean, String propertyName, PropertyChangeListener listener, String methodName, String message)
    {
        Method method;
        Object parameters[];
        IllegalArgumentException e;
        method = null;
        parameters = (Object[])null;
        try
        {
            try
            {
                method = bean.getClass().getMethod(methodName, new Class[] {
                    java.lang.String.class, java.beans.PropertyChangeListener.class
                });
                parameters = (new Object[] {
                    propertyName, listener
                });
            }
            catch(NoSuchMethodException _ex)
            {
                try {
					method = bean.getClass().getMethod(methodName, new Class[] {
					    java.beans.PropertyChangeListener.class
					});
				} catch (NoSuchMethodException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
                parameters = (new Object[] {
                    listener
                });
            }
        }
        catch(SecurityException _ex) { }
        return false;
    }

    private static void log(int severity, String message, Throwable throwable)
    {
        Policy.getLog().log(new Status(severity, "org.eclipse.core.databinding", 0, message, throwable));
    }
}



