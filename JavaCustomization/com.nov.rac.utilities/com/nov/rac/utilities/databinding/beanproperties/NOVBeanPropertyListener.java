package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import org.eclipse.core.databinding.observable.IDiff;
import org.eclipse.core.databinding.property.IProperty;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;

public abstract class NOVBeanPropertyListener extends NativePropertyListener implements PropertyChangeListener{

	protected NOVBeanPropertyListener(IProperty property, PropertyDescriptor propertyDescriptor, ISimplePropertyListener listener)
    {
        super(property, listener);
        this.propertyDescriptor = propertyDescriptor;
    }

    public void propertyChange(PropertyChangeEvent evt)
    {
        if(evt.getPropertyName() == null || propertyDescriptor.getName().equals(evt.getPropertyName()))
        {
            Object oldValue = evt.getOldValue();
            Object newValue = evt.getNewValue();
            IDiff diff;
//            if(evt.getPropertyName() == null || oldValue == null || newValue == null)
//                diff = null;
//            else
                diff = computeDiff(oldValue, newValue);
            fireChange(evt.getSource(), diff);
        }
    }

    protected abstract IDiff computeDiff(Object obj, Object obj1);

    protected void doAddTo(Object source)
    {
        NOVBeanPropertyListenerSupport.hookListener(source, propertyDescriptor.getName(), this);
    }

    protected void doRemoveFrom(Object source)
    {
        NOVBeanPropertyListenerSupport.unhookListener(source, propertyDescriptor.getName(), this);
    }

    private final PropertyDescriptor propertyDescriptor;
	
}
