package com.nov.rac.utilities.databinding.beanproperties;

import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.jface.databinding.swt.WidgetProperties;

public class NOVWidgetProperties extends WidgetProperties{
	
	public static IValueProperty valueChanged()
    {
        return new NOVSwingValueProperty();
    }

}
