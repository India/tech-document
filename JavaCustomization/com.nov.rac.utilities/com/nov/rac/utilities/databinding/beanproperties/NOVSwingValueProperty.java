package com.nov.rac.utilities.databinding.beanproperties;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.eclipse.core.databinding.property.value.DelegatingValueProperty;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.swt.widgets.List;

import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.util.DateButton;

public class NOVSwingValueProperty extends DelegatingValueProperty implements IValueProperty 
{

	@Override
	protected IValueProperty doGetDelegate(Object obj) {
		
		if(obj instanceof DateButton){
			if(swingCompValue == null){
				swingCompValue = new NOVDateSelectionProperty();
			}
		}
		else if(obj instanceof JTable){
			if(swingCompValue == null){
				swingCompValue = new NOVTableChangeProperty();
			}
		}
		else if(obj instanceof AIFTableModel){
			if(swingCompValue == null){
				swingCompValue = new NOVTableModelChangeProperty();
			}
		}
		else if(obj instanceof JTextField){
			if(swingCompValue == null){
				swingCompValue = new NOVTextModifyProperty();
			}
		}
		else if(obj instanceof List){
			if(swingCompValue == null){
				swingCompValue = new NOVListModifyProperty();
			}
		}
		
		return swingCompValue;
	}

	private IValueProperty swingCompValue;

}
