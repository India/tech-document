package com.nov.rac.utilities.databinding.beanproperties;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.eclipse.core.databinding.observable.IDiff;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.IProperty;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;

import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.util.DateButton;


public class NOVTableChangeProperty extends SimpleValueProperty{
	
	public NOVTableChangeProperty() {
		super();
	}

	@Override
	public Object getValueType() {
		return null;
	}

	public INativePropertyListener adaptListener(ISimplePropertyListener listener)
	{
		return new TableListener(this, listener);
	}

	private class TableListener extends NativePropertyListener
	implements PropertyChangeListener
	{

		protected void doAddTo(Object source)
		{
			JTable tableComponent = (JTable)source;
			
			tableComponent.addPropertyChangeListener(this);
		}

		protected void doRemoveFrom(Object source)
		{
		}

		protected TableListener(IProperty property, ISimplePropertyListener listener)
		{
			super(property, listener);
		}

		@Override
		public void propertyChange(PropertyChangeEvent propertychangeevent) {
			fireChange(propertychangeevent.getSource(), null);
		}
	}
	

	@Override
	protected Object doGetValue(Object source) {
		
		Object value = null;
		
		if(source != null){
		
			int cols = ((JTable)source).getSelectedColumn();
			
			int rows = ((JTable)source).getSelectedRow();
			
			if(cols != -1 && rows != -1){
				value = ((JTable)source).getValueAt(rows, cols);
			}			
		}
		
		return value;
	}

	@Override
	protected void doSetValue(Object source, Object val) {

//		if(val != null){
//			((JTable)source).setModel((TableModel) val);
//		}

	}
	
}
