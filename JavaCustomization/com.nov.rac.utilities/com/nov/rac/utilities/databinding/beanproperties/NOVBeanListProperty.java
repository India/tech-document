package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.util.*;
import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.IDiff;
import org.eclipse.core.databinding.observable.list.ListDiff;
import org.eclipse.core.databinding.property.*;
import org.eclipse.core.databinding.property.list.SimpleListProperty;

// Referenced classes of package org.eclipse.core.internal.databinding.beans:
//            BeanPropertyHelper, BeanPropertyListener

public class NOVBeanListProperty extends SimpleListProperty
{
	private PropertyChangeListener m_listener = null;    
    private PropertyDescriptor m_PropertyDescriptor;
    private final Class elementType;

    public NOVBeanListProperty(PropertyDescriptor propertyDescriptor, Class elementType)
    {
    	m_PropertyDescriptor = propertyDescriptor;
    	
        this.elementType = java.lang.Object.class; //!= null ? elementType : NOVBeanPropertyHelper.getCollectionPropertyElementType(propertyDescriptor);
    }

    public Object getElementType()
    {
        return elementType;
    }

    protected List doGetList(Object source)
    {
    	m_listener = (PropertyChangeListener) source;
    	
        return asList(NOVBeanPropertyHelper.readProperty(source, m_PropertyDescriptor));
    }

    private List asList(Object propertyValue)
    {
        if(propertyValue == null)
            return new ArrayList();
        if(m_PropertyDescriptor.getPropertyType().isArray())
            return new ArrayList(Arrays.asList((Object[])propertyValue));
        else
            return (List)propertyValue;
    }

    protected void doSetList(Object source, List list, ListDiff diff)
    {
        NOVBeanPropertyHelper.writeProperty(source, m_PropertyDescriptor, convertListToBeanPropertyType(list));
    }

    private Object convertListToBeanPropertyType(List list)
    {
        Object propertyValue = list;
        if(m_PropertyDescriptor.getPropertyType().isArray())
        {
            Class componentType = m_PropertyDescriptor.getPropertyType().getComponentType();
            Object array[] = (Object[])Array.newInstance(componentType, list.size());
            list.toArray(array);
            propertyValue = ((Object) (array));
        }
        return propertyValue;
    }

    public INativePropertyListener adaptListener(ISimplePropertyListener listener)
    {
        return new NOVBeanPropertyListener(this, m_PropertyDescriptor, listener) {

            protected IDiff computeDiff(Object oldValue, Object newValue)
            {
                return Diffs.computeListDiff(asList(oldValue), asList(newValue));
            }

        }
;
    }

}
