package com.nov.rac.utilities.databinding.beanproperties;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import com.nov.rac.utilities.databinding.DataBindingModel;

public class NOVPropertyDescriptor extends PropertyDescriptor{

	public NOVPropertyDescriptor(String s, Class class1, String s1,
			String s2) throws IntrospectionException {
		super(s, class1, s1, s2);
	}

	@Override
	public synchronized Method getReadMethod() {
		
		Class	m_beanClass = DataBindingModel.class;
		String	m_getMethodName = "getProperty";
		String	m_setMethodName = "setProperty";

		if(!m_beanClass.isInterface())
		{
			BeanInfo beanInfo;
			try
			{
				beanInfo = Introspector.getBeanInfo(m_beanClass);
			}
			catch(IntrospectionException _ex)
			{
				return null;
			}


			MethodDescriptor[] methodDescriptors = beanInfo.getMethodDescriptors();
			for(int i = 0; i < methodDescriptors.length; i++)
			{
				MethodDescriptor descriptor = methodDescriptors[i];
				if(descriptor.getName().equals(m_getMethodName))
					return descriptor.getMethod();
			}
		}
		return null;
	}

	@Override
	public synchronized Method getWriteMethod() {
		
		Class	m_beanClass = DataBindingModel.class;
		String	m_getMethodName = "getProperty";
		String	m_setMethodName = "setProperty";
		
		if(!m_beanClass.isInterface())
		{
			BeanInfo beanInfo;
			try
			{
				beanInfo = Introspector.getBeanInfo(m_beanClass);
			}
			catch(IntrospectionException _ex)
			{
				return null;
			}


			MethodDescriptor[] methodDescriptors = beanInfo.getMethodDescriptors();
			for(int i = 0; i < methodDescriptors.length; i++)
			{
				MethodDescriptor descriptor = methodDescriptors[i];
				if(descriptor.getName().equals(m_setMethodName))
					return descriptor.getMethod();
			}
		}
		return null;
	}

	@Override
	public synchronized void setReadMethod(Method method)
			throws IntrospectionException {
		// TODO Auto-generated method stub
		super.setReadMethod(method);
	}

	@Override
	public synchronized void setWriteMethod(Method method)
			throws IntrospectionException {
		// TODO Auto-generated method stub
		super.setWriteMethod(method);
	}

	
}
