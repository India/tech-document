package com.nov.rac.utilities.databinding;

import java.util.HashMap;
import java.util.Map;

public class DataBindingRegistry {
	
	private Map<String, Object> m_dataBindingModels = null;
	
	private static DataBindingRegistry m_registryInstance= null;
	
	private DataBindingRegistry(){
		m_dataBindingModels = new HashMap<String, Object>();
	}

	public static DataBindingRegistry getRegistry(){
		if(m_registryInstance == null){
			m_registryInstance = new DataBindingRegistry();
		}
		return m_registryInstance;		
	}
	
	public void registerDataModel(String dataModelName, Object dataModel){
		m_dataBindingModels.put(dataModelName, dataModel);
	}
	
	public Object getDataModel(String dataModelName){
		return m_dataBindingModels.get(dataModelName);
	}
	
	public void unregisterDataModel(String dataModelName){
		m_dataBindingModels.remove(dataModelName);
	}
}
