package com.nov.rac.utilities.integrationpoint;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import schema.v1_3.webservices.tce.nov.com.CredentialsInfo;
import schema.v1_3.webservices.tce.nov.com.GeneralOutput;
import schema.v1_3.webservices.tce.nov.com.ItemIntegrationPoint;

import com.nov.www.cet.webservices.V1_3.CETGServicesProxy;
import com.nov.www.cet.webservices.V1_3.CETWSFault;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVIntegrationPointsHandler extends AbstractHandler
{
    @Override
    public Object execute(ExecutionEvent arg0) throws ExecutionException
    {
        TCSession m_session = (TCSession) AIFUtility.getDefaultSession();
        Registry m_registry = Registry.getRegistry(this);
        TCPreferenceService m_prefServ = m_session.getPreferenceService();
        
        String userpwd = m_prefServ.getString(TCPreferenceService.TC_preference_site, "CETGServicesV1.3_PWD");
        String server = m_prefServ.getString(TCPreferenceService.TC_preference_site, "CETGRSServices_server");
        String wsUrl = "http://" + server + ":7081/cetgws/V1.3/CETGServices?WSDL";
        
        CETGServicesProxy proxy = new CETGServicesProxy();
        proxy.setEndpoint(wsUrl);
        
        CredentialsInfo credentials = null;
        
        TCComponent m_targetObj = (TCComponent) AIFUtility.getTargetComponent();
        List<TCComponent> tcCompsList = new ArrayList<TCComponent>();
        tcCompsList.add(m_targetObj);
        String errMsg = "";

        try
        {
           
            String itemId = m_targetObj.getProperty("item_id");
            TCComponent[] itemIntPointObj = m_targetObj.getTCProperty("nov4_integration_point_info")
                    .getReferenceValueArray();
            
            for (int index = 0; index < itemIntPointObj.length; index++)
            {
                ItemIntegrationPoint itemIntPointRef = new ItemIntegrationPoint();
                itemIntPointRef.setIntegrationPointName("RSOne");
                itemIntPointRef.setDataModified(Calendar.getInstance());
                itemIntPointRef.setDateCreated(null);
                itemIntPointRef.setIsActive(false);
                
                credentials = proxy.getCredentials("webservices", userpwd);
                credentials.setLiveResultsOnly(true);
                
                GeneralOutput generalOutputRef = proxy.createOrUpdateItemIntegrationPoint(credentials, itemId,
                        itemIntPointRef);
                
                String strStatus = generalOutputRef.getStatus();
                if(strStatus.equalsIgnoreCase("FAILED")) {
                	errMsg = errMsg + generalOutputRef.getMessage();
                }
            }
            
            if(errMsg.length() > 0) {
                MessageBox.post(errMsg, "Error", MessageBox.ERROR);
            } else 
            {
                MessageBox.post(m_registry.getString("ActiveInactive.Msg"), "Info",MessageBox.INFORMATION);
            }
            
            m_targetObj.refresh();
        }
        catch (CETWSFault e)
        {
            e.printStackTrace();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        return null;
    }
}
