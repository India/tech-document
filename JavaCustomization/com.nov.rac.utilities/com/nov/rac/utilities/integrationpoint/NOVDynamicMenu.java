package com.nov.rac.utilities.integrationpoint;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import schema.v1_3.webservices.tce.nov.com.CredentialsInfo;
import schema.v1_3.webservices.tce.nov.com.GeneralOutput;
import schema.v1_3.webservices.tce.nov.com.ItemIntegrationPoint;
import schema.v1_3.webservices.tce.nov.com.ItemRevIntegrationPointAudit;

import com.nov.www.cet.webservices.V1_3.CETGServicesProxy;
import com.nov.www.cet.webservices.V1_3.CETWSFault;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVDynamicMenu extends ContributionItem
{
    TCSession m_session = (TCSession) AIFUtility.getDefaultSession();
    Registry m_registry = Registry.getRegistry(this);
    TCPreferenceService m_prefServ = m_session.getPreferenceService();
    TCComponent m_targetObj = null;
    private String m_objectType = null;
    private String m_owningGroup = null;
    private String m_selection = null;
    
    public NOVDynamicMenu()
    {
        
    }
    
    public NOVDynamicMenu(String id)
    {
        super(id);
    }
    
    @Override
    public void fill(Menu menu, int index)
    {
        // Root level menu
        MenuItem publishMenuItem = new MenuItem(menu, SWT.CASCADE);
        publishMenuItem.setText(m_registry.getString("Root_Menu.Name"));
        Menu publishMenu = new Menu(publishMenuItem);
        
        // Get erp system name values from site preference
        String integrationSystemNames[] = m_prefServ.getStringArray(TCPreferenceService.TC_preference_site,
                m_registry.getString("User_Select_Integration_Preference.Name"));
        
        for (int i = 0; i < integrationSystemNames.length; i++)
        {
            // Construct first level menu
            constructMenuOptions(publishMenu, integrationSystemNames[i]);
        }
        publishMenuItem.setMenu(publishMenu);
    }
    
    private void constructMenuOptions(Menu publishMenu, String sysERPName)
    {
        MenuItem erpMenuItem = new MenuItem(publishMenu, SWT.PUSH);
        erpMenuItem.setText(sysERPName);
        erpMenuItem.addSelectionListener(m_selectionListener);
    }
    
    // Selection listener on the sub-menu selection
    SelectionListener m_selectionListener = new SelectionAdapter()
    {
        @Override
        public void widgetSelected(SelectionEvent selectionevent)
        {
            m_selection = ((MenuItem) selectionevent.widget).getText();
            List<TCComponent> tcCompsList = new ArrayList<TCComponent>();
            m_targetObj = (TCComponent) AIFUtility.getTargetComponent();
            
            try
            {
                m_objectType = m_targetObj.getProperty("object_type");
                
                // Check if valid object type
                if (!(m_targetObj instanceof TCComponentItem))
                {
                    MessageBox.post(m_registry.getString("Invalid_Target.Error"), "Error", MessageBox.ERROR);
                    return;
                }
                
                TCComponentItem tcItem = (TCComponentItem) m_targetObj;
                TCComponentItemRevision tcItemRev = tcItem.getLatestItemRevision();
                tcCompsList.add(tcItem);
                tcCompsList.add(tcItemRev);
                
                m_owningGroup = m_targetObj.getProperty("owning_group");
                
                // Check access on target object
                if (!m_owningGroup.equalsIgnoreCase(m_session.getCurrentGroup().toString()))
                {
                    MessageBox.post(m_registry.getString("No_Access.Error"), "Error", MessageBox.ERROR);
                    return;
                }
                
                if (isValidSystemSelected(m_selection))
                {
                    createOrUpdateIntegrationPointInfo(m_selection, tcItem, tcItemRev);
                }
                else
                {
                    MessageBox.post(m_registry.getString("Invalid_Selection.Error"), "Error", MessageBox.ERROR);
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
    };
    
    private void createOrUpdateIntegrationPointInfo(String selectedOption, TCComponentItem tcItem,
            TCComponentItemRevision tcItemRev)
    {
        String userpwd = m_prefServ.getString(TCPreferenceService.TC_preference_site, "CETGServicesV1.3_PWD");
        String server = m_prefServ.getString(TCPreferenceService.TC_preference_site, "CETGRSServices_server");
        String wsUrl = "http://" + server + ":7081/cetgws/V1.3/CETGServices?WSDL";
        
        CETGServicesProxy proxy = new CETGServicesProxy();
        proxy.setEndpoint(wsUrl);
        
        CredentialsInfo credentials = null;
        
        try
        {
            String itemId = tcItem.getProperty("item_id");
            String revId = tcItemRev.getProperty("item_revision_id");
            String revPuid = tcItemRev.getUid();
            
            credentials = proxy.getCredentials("webservices", userpwd);
            credentials.setLiveResultsOnly(true);
            
            TCComponent[] itemIntPointObj = m_targetObj.getTCProperty("nov4_integration_point_info")
                    .getReferenceValueArray();
            boolean bUpdateIntPoint = true;
            for (int k = 0; k < itemIntPointObj.length; k++)
            {
                TCComponent sysItemDefObj = (itemIntPointObj[k].getTCProperty("nov4_integration_system"))
                        .getReferenceValue();
                if (sysItemDefObj != null
                        && (sysItemDefObj.getTCProperty("nov4_integration_sys_name").toString())
                                .equalsIgnoreCase(m_selection))
                {
                    bUpdateIntPoint = false;
                    break;
                }
            }
            
            String errMsg = "";
            String strStatus = "";
            
            if (bUpdateIntPoint)
            {
                ItemIntegrationPoint itemIntPointRef = new ItemIntegrationPoint();
                itemIntPointRef.setIntegrationPointName(selectedOption);
                itemIntPointRef.setDataModified(Calendar.getInstance());
                itemIntPointRef.setDateCreated(Calendar.getInstance());
                itemIntPointRef.setIsActive(true);
                
                GeneralOutput generalOutputRef = proxy.createOrUpdateItemIntegrationPoint(credentials, itemId,
                        itemIntPointRef);
                
                strStatus = generalOutputRef.getStatus();
                
                if (strStatus.equalsIgnoreCase("FAILED"))
                {
                    errMsg = errMsg + generalOutputRef.getMessage();
                }
                
                tcItem.refresh();
            }
            
            String groupPref = m_objectType + "_IntegrationSystemNames";
            
            String groupERPSystems[] = m_prefServ.getStringArray(TCPreferenceService.TC_preference_group,
                    m_registry.getString(groupPref));
            
            // Check if integration system selected is a menu integration system
            // and update audit object accordingly
            if (Arrays.asList(groupERPSystems).contains(selectedOption))
            {
                
                TCComponent[] itemRevAuditObj = tcItemRev.getTCProperty("nov4_integration_point_info")
                        .getReferenceValueArray();
                boolean bItemRevAudit = true;
                for (int k = 0; k < itemRevAuditObj.length; k++)
                {
                    TCComponent itemIntPointObj1 = (itemRevAuditObj[k].getTCProperty("nov4_integration_point"))
                            .getReferenceValue();
                    if (itemIntPointObj1 != null)
                    {
                        TCComponent sysItemDefObj = (itemIntPointObj1.getTCProperty("nov4_integration_system"))
                                .getReferenceValue();
                        if (sysItemDefObj != null
                                && (sysItemDefObj.getTCProperty("nov4_integration_sys_name").toString())
                                        .equalsIgnoreCase(m_selection))
                        {
                            bItemRevAudit = false;
                            break;
                        }
                    }
                }
                if (bItemRevAudit)
                {
                	TCUserService userService = m_session.getUserService();
                	Object[] inputToUserServ = new Object[2];
                	
                    ItemRevIntegrationPointAudit revAuditObj = new ItemRevIntegrationPointAudit();
                    revAuditObj.setIntegrationPointName(selectedOption);
                    revAuditObj.setLogDate(Calendar.getInstance());
                    revAuditObj.setMessage(m_registry.getString("Submitted.Msg"));
                    revAuditObj.setStatus(m_registry.getString("N/A.Str"));
                    
                    GeneralOutput generalOutputRefAudit = proxy.addItemIntegrationPointAudit(credentials, itemId,
                            revId, revPuid, revAuditObj);
                    
                    strStatus = generalOutputRefAudit.getStatus();
                    if (strStatus.equalsIgnoreCase("FAILED"))
                    {
                        errMsg = errMsg + generalOutputRefAudit.getMessage();
                    }
                    
                    if (errMsg.length() > 0)
                    {
                        MessageBox.post(errMsg, "Error", MessageBox.ERROR);
                    }
                    else
                    {
                        MessageBox.post(m_registry.getString("Success.Msg"), "Info", MessageBox.INFORMATION);
                    }
                    
                    tcItemRev.refresh();
                    
                    //Submit To CET Director
                    inputToUserServ[0] = tcItemRev;
                    inputToUserServ[1] = "create";
                    userService.call("NATOIL_integration_points_submitToCETDirector", inputToUserServ);
                }
                else
                {
                    MessageBox.post(m_registry.getString("AlreadyPublished.Msg"), "Error", MessageBox.ERROR);
                }
            }
        }
        catch (CETWSFault e)
        {
            e.printStackTrace();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private boolean isValidSystemSelected(String selectedOption)
    {
        boolean isValidSelection = false;
        
        // Get values from group preference and compare
        TCPreferenceService prefServ = m_session.getPreferenceService();
        String groupPrefIntSys[] = prefServ.getStringArray(TCPreferenceService.TC_preference_group, m_objectType
                + "_IntegrationSystemNames");
        
        if (Arrays.asList(groupPrefIntSys).contains(selectedOption))
        {
            isValidSelection = true;
        }
        return isValidSelection;
    }
}
