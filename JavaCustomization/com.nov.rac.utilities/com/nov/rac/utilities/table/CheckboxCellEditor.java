package com.nov.rac.utilities.table;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;

public class CheckboxCellEditor extends DefaultCellEditor
{
    
    private static final long serialVersionUID = 1L;
    
    private JCheckBox m_checkBox = null;
    
    public CheckboxCellEditor()
    {
        super(new JCheckBox());
        
        m_checkBox = (JCheckBox) getComponent();
        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
    }
    
    public Object getCellEditorValue()
    {
        return Boolean.valueOf(m_checkBox.isSelected()).toString();
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable jtable, Object obj, boolean flag, int i, int j)
    {
        // TODO Auto-generated method stub
        return m_checkBox;
    }
    
}
