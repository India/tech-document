package com.nov.rac.utilities.table;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CheckboxCellRenderer extends DefaultTableCellRenderer
{
    
    private static final long serialVersionUID = 1L;
    private JCheckBox m_checkBox;
    
    public CheckboxCellRenderer()
    {
        
        m_checkBox = new JCheckBox();
        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
    }
    
//    public CheckboxCellRenderer(JCheckBox checkbox)
//    {
//        
//        m_checkBox = checkbox;
//        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
//    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int col)
    {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
        /*if (!value.equals(m_checkBox.isSelected()))
        {
            boolean checkFlag = new Boolean(value.toString());
            m_checkBox.setSelected(checkFlag);
        }*/
        return m_checkBox;
    }
    
}
