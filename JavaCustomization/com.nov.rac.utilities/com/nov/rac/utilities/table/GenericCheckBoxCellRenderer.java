package com.nov.rac.utilities.table;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class GenericCheckBoxCellRenderer extends DefaultTableCellRenderer
{
    private static final long serialVersionUID = 1L;
    private JCheckBox m_checkBox;
    
    public GenericCheckBoxCellRenderer()
    {
        
        m_checkBox = new JCheckBox();
        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int col)
    {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
        
        if (value instanceof Boolean) 
        {  
            m_checkBox.setSelected(((Boolean)value).booleanValue());   
        }
        return m_checkBox;
    }
}
