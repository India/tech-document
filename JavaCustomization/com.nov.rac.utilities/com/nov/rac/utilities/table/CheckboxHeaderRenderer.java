package com.nov.rac.utilities.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.nov.rac.utilities.utils.UIHelper;

public class CheckboxHeaderRenderer implements TableCellRenderer, MouseListener 
{
    private static final long serialVersionUID = 1L;
    private JCheckBox m_checkBox;
    private boolean m_mousePressed = false;
    private int m_column = -1;
    
    public CheckboxHeaderRenderer(String name, boolean isSelected, boolean isEnabled)
    {
        m_checkBox = new JCheckBox();
        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
        m_checkBox.setText(name);
        m_checkBox.setSelected(isSelected);
        m_checkBox.setEnabled(isEnabled);
        m_checkBox.setForeground(Color.BLACK);
        //TCDECREL-5689 Start
        m_checkBox.setBorderPaintedFlat(true);
        m_checkBox.setBorderPainted(true);
        //TCDECREL-5689 Stop
        
        //8677
        UIHelper.setFontStyle(m_checkBox, Font.PLAIN);
    }
    
    public CheckboxHeaderRenderer(JCheckBox checkBox)
    {
        m_checkBox = checkBox;
        m_checkBox.setHorizontalAlignment(JCheckBox.CENTER);
    }
    
    public CheckboxHeaderRenderer(ActionListener actionListener, String name, boolean isSelected, boolean isEnabled)
    {
        new CheckboxHeaderRenderer(name, isSelected, isEnabled);
        m_checkBox.addActionListener(actionListener);
    }
    
    public void addActionListener(ActionListener actionListener)
    {
        m_checkBox.addActionListener(actionListener);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column)
    {
        JTableHeader header = table.getTableHeader();
        header.setLayout(new BorderLayout());
        header.addMouseListener(this);
        m_column = column;
        return m_checkBox;
    }
    
    protected void handleClickEvent(MouseEvent mouseevent)
    {
        if (m_mousePressed)
        {
            m_mousePressed = false;
            JTableHeader header = (JTableHeader) (mouseevent.getSource());
            JTable tableView = header.getTable();
            TableColumnModel columnModel = tableView.getColumnModel();
            
            int viewColumn = columnModel.getColumnIndexAtX(mouseevent.getX());
            
            int column = tableView.convertColumnIndexToModel(viewColumn);
            
            if (viewColumn == this.m_column && mouseevent.getClickCount() == 1 && column != -1)
            {
                m_checkBox.doClick();
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent mouseevent)
    {
        handleClickEvent(mouseevent);
        ((JTableHeader) mouseevent.getSource()).repaint();
    }
    
    @Override
    public void mouseEntered(MouseEvent mouseevent)
    {
    }
    
    @Override
    public void mouseExited(MouseEvent mouseevent)
    {
    }
    
    @Override
    public void mousePressed(MouseEvent mouseevent)
    {
        m_mousePressed = true;
    }
    
    @Override
    public void mouseReleased(MouseEvent mouseevent)
    {
    }
    
}
