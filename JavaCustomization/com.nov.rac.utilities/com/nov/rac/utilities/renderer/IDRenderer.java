package com.nov.rac.utilities.renderer;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.common.table.DefaultRenderer;
import com.teamcenter.rac.kernel.TCComponent;

public class IDRenderer extends DefaultRenderer implements TableCellRenderer
{
    private static final long serialVersionUID = 1L;
    private int iObject_Type_Col_Index = -1;
	private  int iRsOne_ItemType_Index = -1;
    
    public IDRenderer()
    {
    }
    
    protected Icon getDisplayIcon(String rowType, Object obj)
    {
        if (rowType == null)
            return null;
        
        javax.swing.ImageIcon imageicon = (javax.swing.ImageIcon) getValueIcon(rowType);
        if (imageicon == null)
        {
            imageicon = TCTypeRenderer.getTypeIcon(rowType, null);
            
            if (imageicon != null)
            {
                setValueIcon(rowType, imageicon);
            }
            
        }
        return imageicon;
    }
    
    @Override
    protected Icon getDisplayIcon(TCComponent arg0, Object arg1)
    {
        return null;
    }
    
    @Override
    protected void initiateIcons()
    {
        
    }
    
    protected String getCellValue(TCTable jtable, Object obj, int iRow, int iCol)
    {
        if (iObject_Type_Col_Index == -1)
        {
            // if(jtable instanceof NOVSearchTable)
            {
                int iViewColIndex = -1;
                
                iViewColIndex = getColumnIndex("object_type", jtable);
                
                iObject_Type_Col_Index = iViewColIndex; // jtable.convertColumnIndexToModel(iViewColIndex);
            }
            
        }

        if(iRsOne_ItemType_Index == -1)
		{
			//if(jtable instanceof NOVSearchTable)
			{
				int iViewColIndex = -1;
				
				iViewColIndex = getColumnIndex("rsone_itemtype", jtable);
				
				iRsOne_ItemType_Index = iViewColIndex; //jtable.convertColumnIndexToModel(iViewColIndex);
	        }			
		}
        
        String rowType = null;
   
        //check if rsone_itemtype column is available.
		// if the column is available, check if its value is "Purchased (Inventory Item)"
		// then set the Icon as Purchased Part
		String rsone_itemtype_value = null;
		
		if( iRsOne_ItemType_Index != -1)
		{
			rsone_itemtype_value = (String) (jtable).getModel().getValueAt(iRow,  iRsOne_ItemType_Index);
		}
		
		if( (rsone_itemtype_value != null) && (rsone_itemtype_value.compareTo( "Purchased (Inventory Item)") == 0) )
		{
			rowType = "PurchasedInventory Item";//TCDECREL-6263
		}
		else if (iObject_Type_Col_Index != -1)
        {
            rowType = (String) (jtable).getModel().getValueAt(iRow, iObject_Type_Col_Index);
        }
        
        return rowType;
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean flag, boolean flag1, int iRow,
            int iCol)
    {
        if(flag)
        {
            super.setForeground(jTable.getSelectionForeground());
            super.setBackground(jTable.getSelectionBackground());
        } else
        {
            super.setForeground(jTable.getForeground());
            super.setBackground(iRow % 2 != 1 ? jTable.getBackground() : alternateBackground);
        }
        if(flag1)
        {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if(jTable.isCellEditable(iRow, iCol))
            {
                super.setForeground(UIManager.getColor("Table.focusCellForeground"));
                super.setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } else
        {
            setBorder(noFocusBorder);
        }
        if (jTable instanceof TCTable)
        {
           TCTable tcTable =  (TCTable)jTable;
           String objectType = getCellValue(tcTable, obj, iRow, iCol);
           
           Icon icon = getDisplayIcon(objectType, obj);
           String s = getDisplayText(obj);
           setText(s != null ? s : "");
           setIcon(icon);
        }
        
        
        return this;
    }
    
    private int getColumnIndex(String columnName, TCTable table)
    {
        TableModel model = table.getModel();
        model.getColumnCount();
        for (int columnNo = 0; columnNo < table.dataModel.getColumnCount(); columnNo++)
        {
            String columnNameFromModel = table.dataModel.getColumnIdentifier(columnNo);
            
            if (columnName.equals(columnNameFromModel))
            {
                return columnNo;
            }
        }
        return -1;
    }
}