package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class CheckButtonPropertyComposite extends AbstractAttributeComposite
{
	private CLabel     m_Label         ;
	private Button     m_checkButton   ;
	
	public CheckButtonPropertyComposite(Composite parent, int style,
			                                    TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		boolean propValue = false;
		try 
		{
			propValue = getProperty().getLogicalValue();
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}

		m_checkButton.setSelection(propValue);
		
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_Label.setText(propDisplayName);

		if( !getProperty().isModifiable() )
		{
			m_checkButton.setEnabled(false);
		}
		
	}

	@Override
	protected void savePropertyValue() 
	{
		try 
		{
			getProperty().setLogicalValue(m_checkButton.getSelection());
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	protected void createUI()
	{
		setLayout(new GridLayout(2, false));
		
		m_checkButton = new Button(this, SWT.CHECK);
		
		m_Label = new CLabel(this, SWT.NONE);
		
		//m_checkButton = new Button(this, SWT.CHECK);

	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}
	
}
