package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCProperty;

public class PropertyArrayComposite extends AbstractAttributeComposite 
{
	private CLabel   				 m_Label;
	private ArrayPropertyComponent   m_text;
	
	public PropertyArrayComposite(Composite parent, int style,
			                                        TCProperty theProp)
	{
		super(parent, SWT.NONE, theProp);
	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);
		setLayout(gridLayout);
		
		m_Label = new CLabel(this, SWT.NONE);
		m_Label.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		
		m_text  = new ArrayPropertyComponent(this, SWT.NONE);
		m_text.setLayoutData(new GridData(GridData.FILL, SWT.FILL, true, true, 1, 18));

	}

	@Override
	public void load()
	{ 
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_Label.setText(propDisplayName);	
		
		try
		{
			m_text.load( getProperty() );
		} 
		catch (Exception e) 
		{		
			e.printStackTrace();
		}
		
		if( !getProperty().isModifiable() )
		{
			m_text.setEnabled(false);
		}
	}

	@Override
	protected void savePropertyValue() 
	{	
		try 
		{
			m_text.save( getProperty());
		} 
		catch (Exception e) 
		{		
			e.printStackTrace();
		}
	}

	@Override
	public void setLabel(String theLabel) {
		// TODO Auto-generated method stub
		
	}

}
