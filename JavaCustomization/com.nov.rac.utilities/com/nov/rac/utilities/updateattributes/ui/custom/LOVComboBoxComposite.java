package com.nov.rac.utilities.updateattributes.ui.custom;

import java.util.Collections;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.nov.rac.utilities.utils.LOVUtils;
import com.nov.rac.utilities.utils.UIHelper;
import com.nov.rac.utilities.utils.ValidateHelper;
import com.richclientgui.toolbox.validation.ValidatingField;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;

public class LOVComboBoxComposite extends AbstractAttributeComposite  //  TCDECREL-2340
{
	private CLabel   m_label ;//= null;
	private Combo   m_combo ;//= null;
	private Registry m_registry = null;
	private StringValidationToolkit m_strValToolkit = null;
	private boolean m_valid = false;

	public LOVComboBoxComposite( Composite parent, 
							   			  int style, 
							   			  TCProperty theProp) 
	{
		super(parent, style, theProp);
	}
	
	private boolean validateString(String name) 
	{
		boolean valid = false;
		
			if( ValidateHelper.searchInArrayInsensitive(name, m_combo.getItems()))
			{
				valid = true;
			}
		
		return valid;	
	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,false);	
		setLayout(gridLayout);
		
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_label.widthHint = 105;
		m_label = new CLabel(this, SWT.None);
		
		m_label.setLayoutData(gd_label);
		
		m_combo = new Combo(this, SWT.DROP_DOWN | SWT.BORDER);
		GridData gd_combo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);

		final int size = 5;
		int width = m_combo.getMonitor().getBounds().width / size ;
		int height = m_combo.getTextHeight();
		
		gd_combo.widthHint = width;
		gd_combo.heightHint = height;

		m_combo.setLayoutData(gd_combo);
		
		m_strValToolkit = new StringValidationToolkit(SWT.NONE,2, false);    

        final IFieldValidator createComboValidator = new IFieldValidator()
        {
			@Override
			public String getErrorMessage() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getWarningMessage() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isValid(Object arg0) {
				// TODO Auto-generated method stub
				m_valid = validateString(arg0.toString());
				
                return m_valid;
			}

			@Override
			public boolean warningExist(Object arg0) {
				// TODO Auto-generated method stub
				return false;
			}
        };
		
		final ValidatingField<String> comboField = m_strValToolkit.createField(m_combo, createComboValidator, true, "");
		
		m_combo.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub				
				if(m_valid == false )
				{
					m_combo.setText("");
				}
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}
		});

		m_combo.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
	
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				setCursorPosition( 0, 0 );
			}
		});
	
		m_combo.addSelectionListener(new SelectionListener() 
		{
			@Override
			public void widgetSelected(SelectionEvent selectionevent)
			{
				setCursorPosition( 0, 0 );
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent selectionevent)
			{
				
			}
		});
		
		m_combo.addControlListener(new ControlListener() {
			
			@Override
			public void controlResized(ControlEvent arg0) {
				// TODO Auto-generated method stub
				setCursorPosition( 0, 0 );
			}
			
			@Override
			public void controlMoved(ControlEvent arg0) {
				// TODO Auto-generated method stub
			}
		});

	}

	@Override
	public void load()
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		m_label.setText( propDisplayName );
		
		LOVObject [] arrLOVObjs = null;
		String propName = null;
		m_registry = Registry.getRegistry(this);
		
		propName = getProperty().getPropertyName();
		
		propName = propName + ".LOV";
		
		arrLOVObjs = LOVUtils.getLOVObjectsForLOV(m_registry.getString(propName));
		
		if(arrLOVObjs != null && arrLOVObjs.length > 0)
		{		
			String [] strArrLOVValues = new String[arrLOVObjs.length];
			
			for(int inx=0; inx< arrLOVObjs.length; inx++)
			{
				strArrLOVValues[inx] = arrLOVObjs[inx].getTextValue();
			}

			m_combo.setItems(strArrLOVValues);
			UIHelper.setAutoComboArray(m_combo, strArrLOVValues);
		}

		String strPropValue = getProperty().getStringValue();
		
		if(strPropValue != null)
		{
			int nIndex = m_combo.indexOf(strPropValue);
			
			if(nIndex > -1)
			{
				m_combo.select(nIndex);
			}
		}
		
		if(!getProperty().isModifiable())
		{
			m_combo.setEnabled(false);
		}
		
	}
		
	private void setCursorPosition(int x, int y)
	{
		m_combo.setSelection (new Point (x, y));
	}

	@Override
	protected void savePropertyValue()
	{
		String comboTxt = m_combo.getText();
		Vector vecCombo = new Vector();
		Collections.addAll(vecCombo, m_combo.getItems());
		
		//if(m_combo.getSelectionIndex() > -1)
		try 
    	{
			if ( vecCombo.indexOf(comboTxt) > -1)
			{
				String strCurrSelectedValue = m_combo.getItem( vecCombo.indexOf(comboTxt) );
			    
			    if(strCurrSelectedValue != null)
			    {
					getProperty().setStringValue( strCurrSelectedValue );
			    }
			}
			else if(comboTxt.isEmpty())
			{
				getProperty().setStringValue( "" );
			}
    	} 
    	catch (TCException e) 
    	{			
			e.printStackTrace();
		}
	}

	@Override
	public void setLabel(String theLabel)
	{
		m_label.setText(theLabel);
	}

}
