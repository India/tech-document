package com.nov.rac.utilities.updateattributes.ui;

import java.util.Vector;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCProperty;

public class AbstractAttributeContainer extends AbstractAttributeComposite 
{
    private Vector<AbstractAttributeComposite> m_list = new Vector<AbstractAttributeComposite>();
    
	public AbstractAttributeContainer(Composite parent, int style,
			TCProperty theProp) 
	{
		super(parent, style, theProp);	
	}

	@Override
	protected void createUI() 
	{
		
	}

	@Override
	public void load() 
	{
		if(m_list != null)
		{
            for(int inx=0; inx < m_list.size(); inx++)
            {
        	    m_list.get(inx).load();
            }
		}
	}

	@Override
	protected void savePropertyValue() 
	{
        for(int inx=0; inx < m_list.size(); inx++)
        {
        	m_list.get(inx).save();
        }
	}
	
	public void addComponent(AbstractAttributeComposite theComponent)
	{
		m_list.add(theComponent);
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		return;
	}

}
