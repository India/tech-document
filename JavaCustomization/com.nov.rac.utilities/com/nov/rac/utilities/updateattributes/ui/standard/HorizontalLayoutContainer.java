package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeContainer;
import com.teamcenter.rac.kernel.TCProperty;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;

public class HorizontalLayoutContainer extends AbstractAttributeContainer 
{

	public HorizontalLayoutContainer( Composite parent, 
			                          int style,
			                          TCProperty theProp) 
	{
		super(parent, style, theProp);
		setLayout(new FillLayout(SWT.HORIZONTAL));
	}
	
	@Override
	protected void createUI() 
	{
	}


}
