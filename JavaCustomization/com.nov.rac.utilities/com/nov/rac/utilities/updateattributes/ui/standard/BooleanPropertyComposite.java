package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import org.eclipse.swt.layout.RowLayout;

public class BooleanPropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_Label       ;//= null;
	private Button     m_btnTrue   ;//= null;
	private Button 	   m_btnFalse  ;//= null;
	private Group      m_grpRadButns    ;//=  null;
	
	public BooleanPropertyComposite(Composite parent, int style,
												TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_Label.setText(propDisplayName);
		
		boolean propValue = false;
		try 
		{
			propValue = getProperty().getLogicalValue();
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}	
		
		if(propValue == true)
		{
			m_btnTrue.setSelection(true);
		}
		else
		{
			m_btnFalse.setSelection(true);
		}

		if( !getProperty().isModifiable() )
		{
			m_btnFalse.setEnabled(false);
			m_btnTrue.setEnabled(false);
			
		}
		
	}

	@Override
	protected void savePropertyValue()
	{	
		Boolean selected = null;
		try 
        {
			selected = getProperty().getLogicalValue();
			if(selected == true)
			{
				m_btnTrue.setSelection(true);
			}
			else
			{
				m_btnFalse.setSelection(true);
			}
		 } 
        catch (TCException e)
        {
			e.printStackTrace();
		 }
		
	}
	
	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
		
		m_Label = new CLabel(this, SWT.RIGHT);
		m_Label.setLayoutData( new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1) );
		
		GridData gd_grpBttns = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
/*		gd_grpBttns.widthHint = 120;
		gd_grpBttns.heightHint = 18;*/
		
		m_grpRadButns = new Group(this, SWT.NONE);
		m_grpRadButns.setLayout(new RowLayout(SWT.HORIZONTAL));
		m_grpRadButns.setLayoutData(gd_grpBttns);
		
		m_btnTrue = new Button(m_grpRadButns, SWT.RADIO);
		m_btnTrue.setText("True");
		
		m_btnFalse = new Button(m_grpRadButns, SWT.RADIO);
		m_btnFalse.setText("False");
		
		m_grpRadButns.pack();
		
		
		
	}

	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}
}
