package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;

public class TypedReferencePropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_typedRef_Label  ;//= null;
	private Link       m_Link            ;//= null;

	public TypedReferencePropertyComposite(Composite parent, int style,
														TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	protected void createUI() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
			
		m_typedRef_Label = new CLabel(this, SWT.NONE);
		m_typedRef_Label.setText(propDisplayName);
		m_typedRef_Label.setLayoutData( new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1) );
		
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		//gd_text.widthHint = 151;

		m_Link = new Link(this, SWT.NONE);
		m_Link.setLayoutData(gd_text);

		/*m_Link.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				Composite c1 = new Composite(getParent(), SWT.NONE);
				Menu menu = new Menu(c1);
			    MenuItem itemCut = new MenuItem(menu, SWT.POP_UP);
			    itemCut.setText("Cut");
			    MenuItem itemPaste = new MenuItem(menu, SWT.POP_UP);
			    itemPaste.setText("Paste");
			    itemCut.addSelectionListener(new SelectionAdapter()
			    {
			    	@Override
			    	public void widgetDefaultSelected(SelectionEvent arg0) 
			    	{
						
			    	}
		});*/
	}

	@Override
	public void load()
	{
		String strName = "";
		TCComponent value = getProperty().getReferenceValue();
		strName = value.toDisplayString();
		
		if(!strName.isEmpty())
		{
			strName = "<a> " + strName + " </a>";	
		}
		
		m_Link.setText(strName);
		
		if(!getProperty().isModifiable())
		{
			m_Link.setEnabled(false);
		}
		
	}

	@Override
	protected void savePropertyValue() 
	{
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_typedRef_Label.setText(theLabel);
	}

}
