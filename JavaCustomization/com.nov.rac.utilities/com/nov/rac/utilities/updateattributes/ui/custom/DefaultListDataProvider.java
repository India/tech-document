package com.nov.rac.utilities.updateattributes.ui.custom;

import java.util.HashMap;
import java.util.Map;

import com.nov.rac.utilities.updateattributes.ui.IListDataProvider;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class DefaultListDataProvider implements IListDataProvider 
{

	public DefaultListDataProvider()
	{
		
	}
	
	@Override
	public Map<String, Object> load(TCProperty tcProp) throws TCException 
	{
		Map<String, Object> theDataMap = new HashMap<String, Object>();
		
		tcProp.getPropertyValue();
		
		return null;
	}

	@Override
	public void save(TCProperty tcProp, String keyName, Object keyData)
			throws Exception 
	{
        if(keyData != null)
        {
        	tcProp.setPropertyData(keyData);
        }
        else
        {
        	tcProp.setStringValue(keyName);
        }
	}
	
	private void getDataFromLOV()
	{
		
	}

	private void getDataFromPreference()
	{
		
	}

}
