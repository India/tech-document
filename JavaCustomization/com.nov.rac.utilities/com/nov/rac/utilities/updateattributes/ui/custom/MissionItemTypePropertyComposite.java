package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class MissionItemTypePropertyComposite extends AbstractAttributeComposite 
{
	private final String MAKE_ITEM = "Make Item (Proprietary)";
	private final String PURCHASE_ITEM = "Purchased (Inventory Item)";
	
	private Group      m_Group      		 ;//= null;
	private Button     m_makeItemButton      ;//= null;
	private Button     m_PurchaseItemButton  ;//= null;
	
	public MissionItemTypePropertyComposite(Composite parent, int style,
			                                           TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		//String propDisplayName = getProperty().getPropertyDisplayName();		
		//m_Group.setText(propDisplayName);
		
		String propValue = null;
		propValue = getProperty().getStringValue();
		
		if(propValue.equals(MAKE_ITEM))
		{
			m_makeItemButton.setSelection(true);
		}
		else if (propValue.equals(PURCHASE_ITEM))
		{
			m_PurchaseItemButton.setSelection(true);
		}
		
		if( !getProperty().isModifiable() )
		{
			m_makeItemButton.setEnabled(false);
			m_PurchaseItemButton.setEnabled(false);
		}	
		
	}

	@Override
	protected void savePropertyValue()
	{
		String strSelectedButton = null;		
		
		if(m_makeItemButton.getSelection())
		{
			strSelectedButton = m_makeItemButton.getText(); 
		}			
		else if(m_PurchaseItemButton.getSelection())
		{
			strSelectedButton = m_PurchaseItemButton.getText(); 
		}
		
		if(strSelectedButton != null)
		{
			try 
			{
				getProperty().setStringValue(strSelectedButton);
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void createUI()
	{
		setLayout(new GridLayout(1, true));

		m_Group = new Group(this, SWT.CENTER );
		m_Group.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));
		m_Group.setLayout( new GridLayout(2, true));
		
		m_makeItemButton = new Button(m_Group, SWT.RADIO);
		m_makeItemButton.setText(MAKE_ITEM);
		
		m_PurchaseItemButton = new Button(m_Group, SWT.RADIO);
		m_PurchaseItemButton.setText(PURCHASE_ITEM);
		m_PurchaseItemButton.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		
	}		

	@Override
	public void setLabel(String theLabel) 
	{
		m_Group.setText(theLabel);
	}
}
