package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCProperty;

public class UntypedReferencePropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_untypedRef_Label       ;//= null;
	private Link       m_Link                   ;//= null;
	
	public UntypedReferencePropertyComposite(Composite parent, int style,
												TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		String strName = "";
		TCComponent value = getProperty().getReferenceValue();
		strName = value.toDisplayString();
		
		if(!strName.isEmpty())
		{
			strName = "<a> " + strName + " </a>";	
		}
		
		m_Link.setText(strName);
		
		if(!getProperty().isModifiable())
		{
			m_Link.setEnabled(false);
		}
		
	}

	@Override
	protected void savePropertyValue()
	{
	}
	
	@Override
	protected void createUI() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
			
		m_untypedRef_Label = new CLabel(this, SWT.NONE);
		m_untypedRef_Label.setText(propDisplayName);
		m_untypedRef_Label.setLayoutData( new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1) );
		
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_text.widthHint = 151;

		m_Link = new Link(this, SWT.NONE);
		m_Link.setLayoutData(gd_text);
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_untypedRef_Label.setText(theLabel);
	}


}
