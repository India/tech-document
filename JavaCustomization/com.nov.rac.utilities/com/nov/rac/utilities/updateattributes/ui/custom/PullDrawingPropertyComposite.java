package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class PullDrawingPropertyComposite extends AbstractAttributeComposite 
{
	private CLabel  m_Label ;
	private CCombo  m_combo ;
	
	public PullDrawingPropertyComposite(Composite parent, int style,
			                                        TCProperty theProp) 
	{
		super(parent, style, theProp);
	}

	@Override
	protected void createUI()
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_Label = new CLabel(this, SWT.NONE);
		m_Label.setLayoutData(gd_label);
		
		GridData gd_combo = new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1);
		
		m_combo = new CCombo(this, SWT.DROP_DOWN | SWT.BORDER);
		m_combo.setLayoutData(gd_combo);
		
		String[] ITEMS = { "No", "Yes" };
		m_combo.setItems(ITEMS);
	}

	@Override
	public void load() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		m_Label.setText(propDisplayName);

        boolean propValues = false;
        int i;
		
		try 
		{
			propValues = getProperty().getLogicalValue();
		}
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
		if(propValues == false)
		{
			i = m_combo.indexOf("No");
			if(i > -1)
			{
				m_combo.select(i);
			}
		}
		else
		{
			i = m_combo.indexOf("Yes");
			if(i > -1)
			{
				m_combo.select(i);
			}
		}
		
		if(!getProperty().isModifiable())
		{
			m_combo.setEnabled(false);
		}
	}

	@Override
	protected void savePropertyValue() 
	{
		String selected = m_combo.getItem(m_combo.getSelectionIndex());
		
		try
		{
				if(selected.equals("No"))
				{
					getProperty().setLogicalValue(false);
				}
				else if(selected.equals("Yes"))
				{
					getProperty().setLogicalValue(true);
				}
		}
		catch(TCException e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}

}
