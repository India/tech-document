package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.standard.StringPropertyComposite;
import com.teamcenter.rac.kernel.TCProperty;

public class StringLimitPropertyComposite extends StringPropertyComposite
{

	@Override
	protected void createUI() 
	{
		super.createUI();
		
		m_text.setTextLimit(30);
		
		m_text.addVerifyListener(new VerifyListener()
	       {
	            
	           @Override
	           public void verifyText(VerifyEvent e)
	           {
	               e.text = e.text.toUpperCase();
	           }
	       });
	}

	public StringLimitPropertyComposite(Composite parent, int style,
			                                      TCProperty mTheProperty)
	{
		super(parent, style, mTheProperty);
	}

}
