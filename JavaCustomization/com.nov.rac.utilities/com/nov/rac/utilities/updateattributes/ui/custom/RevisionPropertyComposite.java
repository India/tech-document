package com.nov.rac.utilities.updateattributes.ui.custom;



import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.common.PreferenceUtils;
import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class RevisionPropertyComposite extends AbstractAttributeComposite 
{
	private CLabel    m_Label      ;// = null;
	private Text      m_text       ;// = null;
	private TCComponent m_latestRev = null;
	private TCComponent[] m_propValue = null;
	Listener m_listener;//5276
	private static String[] m_allowedSpecialChars = null;

	public RevisionPropertyComposite( Composite parent, 
			                          int style,
			                          TCProperty theProp) 
	{
		super(parent, style, theProp);	
	}

	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,false);	
		setLayout(gridLayout);
		
		m_Label = new CLabel(this, SWT.None);
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_label.widthHint = 105;
		m_Label.setLayoutData(gd_label);
		
		GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		
		m_text  = new Text(this, SWT.BORDER | SWT.SCROLL_LINE);
		int width = m_text.getBorderWidth();
		int height = m_text.getLineHeight();
		
		gridData.widthHint = 15 * width ;//5276-changed 10 to 15
		gridData.heightHint = height;

		m_text.setLayoutData(gridData);
		
//		PixelConverter pixConv = new PixelConverter(m_text);
//		int nWidth = pixConv.convertWidthInCharsToPixels(2);			
//		
//		Point ptSize = m_text.getSize();
//		ptSize.x = nWidth;
//		m_text.setSize(ptSize);
		
		m_text.setTextLimit(2);	
		
		//5276-start
		 m_listener = new Listener()
		 {
			 @Override
				public void handleEvent(Event e)
				{
					keyTypedAction(e);					
				}
         };
			
	    m_text.addListener(SWT.Verify, m_listener);
		//5276-end
	}

	public void load() 
	{
//		String propDisplayName = getProperty().getPropertyDisplayName();		
//		m_Label.setText(propDisplayName);
		
		try 
		{			
		
			m_propValue = getProperty().getReferenceValueArray();
	
			if(m_propValue != null && m_propValue.length > 0 )
			{
				m_latestRev = m_propValue[ m_propValue.length -1 ];
				
				String strItemRevID = m_latestRev.getTCProperty("item_revision_id").getStringValue();	
				
				String propDisplayName = m_latestRev.getTCProperty("item_revision_id").getPropertyDisplayName();
				m_Label.setText(propDisplayName);
				
				m_text.setEditable(true);
//				if ( m_latestRev instanceof TCComponentItemRevision ) 
//				{
//					TCComponent[] releaseStats = null;
//					 
//					releaseStats = m_latestRev.getTCProperty("release_status_list").getReferenceValueArray();
//					
//					if (releaseStats.length == 0)
//					{
//						m_text.setEditable(true);
//					}  
//					else
//					{
//						m_text.setEditable(false);
//					}
//	
//				}
				
				setRevisionField(strItemRevID);//5276
					
			} // if(m_propValue != null && m_propValue.length > 0 )		
		
			TCProperty itemRevIdProp = m_latestRev.getTCProperty("item_revision_id");
			
			if( (itemRevIdProp != null) && (!itemRevIdProp.isModifiable()) )
			{
				m_text.setEditable(false);
			}

		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void save()
	{		
		savePropertyValue();
	}
	//5276-start
	private void setRevisionField(String strItemRevID)
	{
		if(strItemRevID != null && strItemRevID.contains("."))
		{
			m_text.setEditable(false);
			m_text.removeListener(SWT.Verify, m_listener);
			m_text.setTextLimit(5);
			m_text.setText(strItemRevID);
		}
		else 
		{
			if(strItemRevID != null)
			{
			  m_text.setText(strItemRevID);
			}
		}
	}
	private void keyTypedAction(Event event)
	{		        		
        if((isBypassSpecialCharsCheck(event.text) == false) && (isValueHasSpecialChars(event.text) == true))
        {
        	event.doit = false;
        	return;
        }
        
	}
	private boolean isBypassSpecialCharsCheck(String sValue)
	{
		boolean isBypassCheck = false;
		if(m_allowedSpecialChars == null)
		{
			Registry reg = Registry.getRegistry(this);
			String sPrefName = reg.getString("NOV_UPADTEATTRIBUTE_REVID_BYPASS_SPECIAL_CHARS_PREF.NAME");
			int theScope = TCPreferenceService.TC_preference_group;
			m_allowedSpecialChars = PreferenceUtils.getStringValues(theScope, sPrefName);
		}
        
        if(m_allowedSpecialChars != null)
        {
        	for(int indx=0;indx<m_allowedSpecialChars.length;indx++)
        	{
        		if(m_allowedSpecialChars[indx].contains(sValue))
        		{
        			isBypassCheck = true;
        		}
        	}
        }
        
        return isBypassCheck;
	}
	private boolean isValueHasSpecialChars(String sValue)
	{
		boolean isSpecialChars = false;
		
		char[] chars = new char[sValue.length()];
		sValue.getChars(0, chars.length, chars, 0);
		for (int i = 0; i < chars.length; i++) 
    	{ 	
    		if( !(('0' <= chars[i] && chars[i] <= '9') || ( 97<=(int)chars[i] && (int)chars[i] <= 122) || (65 <=(int)chars[i] && (int)chars[i]<=90)))
    		{
    			isSpecialChars = true;
    		}       	
    	}
		
		return isSpecialChars;
	}
	//5276-end

	@Override
	protected void savePropertyValue() 
	{
		m_propValue = getProperty().getReferenceValueArray();

		if(m_propValue != null && m_propValue.length > 0 )
		{
			m_latestRev = m_propValue[ m_propValue.length -1 ];
			
			boolean isMod = false;
			
			try 
			{
				isMod = m_latestRev.isModifiable("item_revision_id");
				
				if(! isMod)
				{
					return;
				}		

				
				// Update the item revision id	
				m_latestRev.getTCProperty("item_revision_id").setStringValue(m_text.getText());
				
				// Update the alternate id's of item revision
				
				TCProperty property = m_latestRev.getTCProperty("altid_list");
				TCComponent[] altid_propValues = property.getReferenceValueArray();
				for(int i=0; i<altid_propValues.length; i++)
				{
					TCProperty idfr_propertyid = altid_propValues[i].getTCProperty("idfr_id");
					idfr_propertyid.setStringValue(m_text.getText());
		
				}			
				
				TCComponent nameform = m_latestRev.getTCProperty("item_master_tag").getReferenceValue();
				
				String itemIdName = m_latestRev.getTCProperty("item_id").getDisplayableValue() + "/" + m_text.getText();      
				nameform.getTCProperty("object_name").setStringValue(itemIdName);
				
			} 			
			catch (TCException e)
			{
				e.printStackTrace();
				MessageBox.post(e);         // If one of the item revision has already the same value.
				
			}
		}
	
	}

	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);		
	}

}
