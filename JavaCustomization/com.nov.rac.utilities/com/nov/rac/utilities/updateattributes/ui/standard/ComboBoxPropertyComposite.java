package com.nov.rac.utilities.updateattributes.ui.standard;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.nov.rac.utilities.updateattributes.ui.IListDataProvider;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

public class ComboBoxPropertyComposite extends AbstractAttributeComposite
{
	private CLabel  m_Label ;//= null;
	private CCombo  m_combo ;//= null;
	
	private IListDataProvider m_dataProvider;
	
	public ComboBoxPropertyComposite(Composite parent, int style,
			                                                  TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_Label = new CLabel(this, SWT.NONE);
		m_Label.setLayoutData(gd_label);
		
		GridData gd_combo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		
		m_combo = new CCombo(this, SWT.DROP_DOWN | SWT.BORDER);
		m_combo.setLayoutData(gd_combo);
	}

	@Override
	public void load() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		m_Label.setText(propDisplayName);
		
		Map<String, Object> comboData = null;;
		
		try 
		{
			comboData = m_dataProvider.load( getProperty() );
		} 
		catch (TCException e) 
		{		
			e.printStackTrace();
		}
		
		if(comboData != null)
		{
			addProviderDataToCombo(m_combo, comboData);	
		}		

		if(!getProperty().isModifiable())
		{
			m_combo.setEnabled(false);
		}
	}

	@Override
	protected void savePropertyValue()
	{
		int nSelIndex = m_combo.getSelectionIndex();
		String theKey = m_combo.getItem(nSelIndex);
		
		Object theValue = m_combo.getData(theKey);
		
		try 
		{
			m_dataProvider.save( getProperty(), theKey, theValue);
		} 
		catch (TCException e) 
		{		
			e.printStackTrace();
		} 
		catch (Exception e) 
		{			
			e.printStackTrace();
		}
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}
	
	private void addProviderDataToCombo(CCombo theCombo, Map<String, Object> theComboData)
	{
		Set< Entry<String, Object> > theEntrySet = theComboData.entrySet();
		Iterator< Entry<String, Object> > theItr = theEntrySet.iterator();
		
	    while( theItr.hasNext() )
	    {
	    	Entry<String, Object> theEntry = theItr.next();
	    	
	    	String theKey = theEntry.getKey();
	    	Object theValue = theEntry.getValue();
	    	
	    	theCombo.add(theKey);
	    	
	    	if(theValue != null)
	    	{
	    		theCombo.setData(theKey, theValue);
	    	}
	    	
	    }
	}
}
