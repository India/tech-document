package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class DocumentsCATPropertyComposite extends AbstractAttributeComposite
{
	private CLabel   m_Label ;
	private Text     m_text  ;
	
	public DocumentsCATPropertyComposite(Composite parent, int style,
			                                          TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);	
		setLayout(gridLayout);
		
		m_Label = new CLabel(this, SWT.NONE);		
		m_Label.setLayoutData( new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1) );
		
		m_text = new Text( this, SWT.BORDER );
		m_text.setLayoutData(new GridData( SWT.LEFT, SWT.FILL, true, false, 1, 1 ));
	}

	@Override
	public void load()
	{
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_Label.setText(propDisplayName);
		
		String propValue = getProperty().getStringValue();		
		m_text.setText(propValue);
		
		m_text.setEnabled(false);
	}

	@Override
	protected void savePropertyValue()
	{
		 try 
         {
        	 getProperty().setStringValue( m_text.getText());
		 } 
         catch (TCException e)
         {
			e.printStackTrace();
		 }
		
	}

	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}

}
