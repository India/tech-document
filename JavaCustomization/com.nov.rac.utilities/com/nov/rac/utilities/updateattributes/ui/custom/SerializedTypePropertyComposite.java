package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class SerializedTypePropertyComposite extends AbstractAttributeComposite 
{
	private final String NONE           = "None"         ;
	private final String SERIALIZED     = "Serialized"   ;
	private final String LOTCONTROL     = "Lot Control"  ;

	private Group      m_Group      ;
	private Button     m_None       ;
	private Button     m_Serialized ;
	private Button     m_LotControl ;
	
	public SerializedTypePropertyComposite(Composite parent, int style,
		                                             	TCProperty theProp) 
	{
		super(parent, style, theProp);
	}

	@Override
	protected void createUI()
	{
		
		setLayout(new GridLayout(1, true));
		m_Group = new Group(this,GridData.CENTER);
		m_Group.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));
		m_Group.setLayout( new GridLayout(3, true));
		
		m_None = new Button(m_Group, SWT.RADIO);
		m_None.setText(NONE);
		m_None.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		
		
		m_Serialized = new Button(m_Group, SWT.RADIO);
		m_Serialized.setText(SERIALIZED);
		m_Serialized.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		
		
		m_LotControl = new Button(m_Group, SWT.RADIO);
		m_LotControl.setText(LOTCONTROL);
		m_LotControl.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		
	}

	@Override
	public void load()
	{
		//String propDisplayName = getProperty().getPropertyDisplayName();		
		//m_Group.setText(propDisplayName);
		
		String propValue = null;
		
		propValue = getProperty().getStringValue();
		
//		if(propValue.equals(""))
//		{
//			m_None.setSelection(true);
//		}
//		else 
		if(propValue.equals(NONE))
		{
			m_None.setSelection(true);
		}
		else if (propValue.equals(SERIALIZED))
		{
			m_Serialized.setSelection(true);
		}
		else if(propValue.equals(LOTCONTROL))
		{
			m_LotControl.setSelection(true);
		}
		
		if( !getProperty().isModifiable() )
		{
			m_None.setEnabled(false);
			m_Serialized.setEnabled(false);
			m_LotControl.setEnabled(false);
		}
		
		
	}

	@Override
	protected void savePropertyValue() 
	{
		String strSelectedButton = null;
		
		if(m_None.getSelection())
		{
			strSelectedButton = m_None.getText(); 
		}			
		else if(m_Serialized.getSelection())
		{
			strSelectedButton = m_Serialized.getText(); 
		}
		else if(m_LotControl.getSelection())
		{
			strSelectedButton = m_LotControl.getText(); 
		}
		
		if(strSelectedButton != null)
		{
			try 
			{
				getProperty().setStringValue(strSelectedButton);
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void setLabel(String theLabel) {
		// TODO Auto-generated method stub
		
	}

}
