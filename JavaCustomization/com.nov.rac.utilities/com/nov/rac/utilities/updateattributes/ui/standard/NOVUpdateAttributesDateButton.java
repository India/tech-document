package com.nov.rac.utilities.updateattributes.ui.standard;



import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.util.DateButton;
import com.teamcenter.rac.util.SWTUIUtilities;

public class NOVUpdateAttributesDateButton extends Composite 
{
      private DateButton m_dateButton ;//= null;
      
      public NOVUpdateAttributesDateButton(Composite parent, int style)
      {
           super(parent,style | SWT.EMBEDDED);
           m_dateButton = new DateButton();
           
           SWTUIUtilities.embed( this, m_dateButton, false);
           
           initDateControl();
      }
      
      public String getDateString()
      {
            return m_dateButton.getDateString();
      }

      public Date getDate()
      {
            return m_dateButton.getDate();
      }

      public void setDateString(String date)
      {
            m_dateButton.setDate(date);
      }

      public void setDate (Date date)
      {
            m_dateButton.setDate(date);
      }

      
      public void initDateControl()
      {
            m_dateButton.setDate("");
      }
}
