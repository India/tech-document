package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class StringPropertyComposite extends AbstractAttributeComposite 
{
	protected CLabel    m_Label      ;// = null;
	protected Text      m_text       ;// = null;
	
	public StringPropertyComposite( Composite parent, 
			                        int style,
			                        TCProperty mTheProperty) 
	{	    	
		super(parent, style, mTheProperty);
	}

	@Override
	public void load() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		m_Label.setText(propDisplayName);
		
		String propValue = getProperty().getStringValue();		
		m_text.setText(propValue);
		
		if( !getProperty().isModifiable() )
		{
			m_text.setEditable(false);
		}
	}

	@Override
	protected void savePropertyValue() 
	{
         try 
         {
        	 getProperty().setStringValue( m_text.getText());
		 } 
         catch (TCException e)
         {
			e.printStackTrace();
		 }
	}

	@Override
	protected void createUI() 
	{
		int LINE_SIZE = 80;
			
		GridLayout gridLayout = new GridLayout(2,false);	
		setLayout(gridLayout);
		
				
		m_Label = new CLabel(this, SWT.None);	
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_label.widthHint = 105;
		m_Label.setLayoutData(gd_label);

		int nStrLength = getProperty().getDescriptor().getMaxStringLength();
		
		int nLines = nStrLength / LINE_SIZE;
		

		if(nLines > 1)
		{
			int numLines = (2 * nLines) + 1;
			
			GridData gd_text1 = new GridData(GridData.FILL, GridData.FILL, false, true,1,numLines);
			m_text  = new Text(this, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
			int width = m_text.getBorderWidth();
			int height = m_text.getLineHeight();

			gd_text1.widthHint = LINE_SIZE * width +100;
			gd_text1.heightHint = height;
			
			 
			m_text.setLayoutData(gd_text1);
			
		}
		else
		{
			GridData gd_text = new GridData( GridData.FILL, GridData.FILL, true, true,1,1);
			m_text = new Text( this, SWT.SINGLE | SWT.BORDER | SWT.SCROLL_LINE );
			int width = m_text.getBorderWidth();
			gd_text.widthHint = width;
			m_text.setLayoutData(gd_text);
		}
		
		m_text.setTextLimit( nStrLength );		

	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
		
	}

}
