package com.nov.rac.utilities.updateattributes.ui;

import java.util.Map;

import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public interface IListDataProvider 
{
    public Map<String, Object> load(TCProperty tcProp) throws TCException;
    
    public void save(TCProperty tcProp, String keyName, Object keyData)throws Exception;
    
}
