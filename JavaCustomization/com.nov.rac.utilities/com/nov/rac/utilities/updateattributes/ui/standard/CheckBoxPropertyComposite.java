package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class CheckBoxPropertyComposite extends AbstractAttributeComposite 
{
	private final String PUBLISH_TO_PP = "Publish to PP";
	private final String SECURE = "Secure (Not viewable in web-portal)";
	private Group      m_Group                ;//= null;
	private Button     m_chkPublishPPBttn     ;//= null;
	private Button     m_chkSecureBttn        ;//= null;
	
	//private Button     m_checkBttn;
	
	public CheckBoxPropertyComposite(Composite parent, int style,
		                                              	TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load()
	{
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_Group.setText(propDisplayName);
		
		String propValue = null;
		propValue = getProperty().getStringValue();
		
		if(propValue.equals(PUBLISH_TO_PP))
		{
			m_chkPublishPPBttn.setSelection(true);
		}
		
		if(propValue.equals(SECURE))
		{
			m_chkSecureBttn.setSelection(true);
		}
		
		if( !getProperty().isModifiable() )
		{
			m_chkPublishPPBttn.setEnabled(false);
			m_chkSecureBttn.setEnabled(false);
		}
		
		/*
		String propDisplayName = getProperty().getPropertyDisplayName();
		m_checkBttn.setText(propDisplayName);
		
		Boolean propValue = false;
		try 
		{
			propValue = getProperty().getLogicalValue();
		}
		catch (TCException e) 
		{
			e.printStackTrace();
		}
		
		if(propValue.equals(true))
		{
			m_checkBttn.setSelection(true);
		}
		else
		{
			m_checkBttn.setSelection(false);
		}
		
		if( !getProperty().isModifiable() )
		{
			m_checkBttn.setEnabled(false);
		}
		*/
	}

	@Override
	protected void savePropertyValue()
	{
		String selectedBttn = null;
		
		if(m_chkPublishPPBttn.getSelection())
		{
			selectedBttn = m_chkPublishPPBttn.getText();
		}
		
		if(m_chkSecureBttn.getSelection())
		{
			selectedBttn = m_chkSecureBttn.getText();
		}
		
		if(selectedBttn != null)
		{
			try 
			{
				getProperty().setStringValue(selectedBttn);
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
		}
		
		/*try 
		{
			getProperty().setLogicalValue(m_checkBttn.getSelection());
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}*/
		
	}
	
	@Override
	protected void createUI()
	{
		RowLayout rowLayout = new RowLayout(SWT.HORIZONTAL);
		rowLayout.center = true;
		rowLayout.justify = true;
		setLayout(rowLayout);	
		
		GridLayout gd = new GridLayout();
		gd.numColumns = 2;
		//gd.makeColumnsEqualWidth = true;
		
		m_Group = new Group(this, SWT.CENTER);
		m_Group.setLayout(gd);
		
		m_chkPublishPPBttn = new Button(m_Group, SWT.CHECK | SWT.CENTER);
		m_chkPublishPPBttn.setText(PUBLISH_TO_PP);
		m_chkPublishPPBttn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		
		m_chkSecureBttn  = new Button(m_Group, SWT.CHECK);
		m_chkSecureBttn.setAlignment(SWT.CENTER);
		m_chkSecureBttn.setText(SECURE);
		m_chkSecureBttn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		/*m_checkBttn = new Button(this, SWT.CHECK | SWT.CENTER);
		m_checkBttn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));*/
		
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_Group.setText(theLabel);
	}

}
