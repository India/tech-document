package com.nov.rac.utilities.updateattributes.ui.standard;



import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class FloatPropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_float_Label       ;//= null;
	private Text       m_float_text        ;//= null;
	
	public FloatPropertyComposite(Composite parent, int style,
			                              TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		
		float Value = getProperty().getFloatValue();
		
		String propValue = Float.toString(Value);
		m_float_text.setText(propValue);
		
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_float_Label.setText(propDisplayName);

		if( !getProperty().isModifiable() )
		{
			m_float_text.setEditable(false);
		}
		
	}

	@Override
	protected void savePropertyValue() 
	{
		try
		{
			getProperty().setFloatValue(Float.parseFloat(m_float_text.getText()));
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);

		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_float_Label = new CLabel(this, SWT.NONE);
		m_float_Label.setLayoutData( gd_label );
		
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		//gd_text.widthHint = 151;

		m_float_text  = new Text(this, SWT.BORDER);
		m_float_text.setLayoutData(gd_text);
		m_float_text.addFocusListener(new FocusListener() 
		{
			@Override
			public void focusLost(FocusEvent focusevent)
			{
				String textValue = m_float_text.getText();
				try 
				{
					getProperty().setFloatValue(Float.parseFloat(textValue));
					m_float_text.setFocus();
				} 
				catch (NumberFormatException e) 
				{
					e.printStackTrace();
				}
				catch (TCException e) 
				{
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void focusGained(FocusEvent focusevent) 
			{
				
			}
		});
		
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_float_Label.setText(theLabel);
	}

}
