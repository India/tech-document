
package com.nov.rac.utilities.updateattributes.ui;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCProperty;

public abstract class AbstractAttributeComposite extends Composite 
{
	protected TCProperty m_theProperty = null;

	public AbstractAttributeComposite( Composite parent, 
                                         int style, 
                                         TCProperty theProp) 
	{
		super(parent, style);
		m_theProperty = theProp;
		
		createUI();
		load();
	}
	
	public abstract void load();
	
	protected abstract void savePropertyValue();
	
	protected abstract void createUI();
	
	public void save()
	{
		if( !m_theProperty.isModifiable() )
		{
			return;
		}
		
		savePropertyValue();
	}
	
	protected TCProperty getProperty()
	{
		return m_theProperty;
	}
	
	protected void setProperty(TCProperty theProp)
	{
		m_theProperty = theProp;
	}
	
	public abstract void setLabel(String theLabel);
//	{
//	    return;	
//	}
	
}