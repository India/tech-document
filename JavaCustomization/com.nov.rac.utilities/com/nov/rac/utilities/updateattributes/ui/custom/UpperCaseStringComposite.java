/**
 * 
 */
package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.standard.StringPropertyComposite;
import com.teamcenter.rac.kernel.TCProperty;

/**
 * @author viveksm
 * 
 */
public class UpperCaseStringComposite extends StringPropertyComposite
{
    
    /**
     * @param parent
     * @param style
     * @param mTheProperty
     */
    public UpperCaseStringComposite(Composite parent, int style, TCProperty mTheProperty)
    {
        super(parent, style, mTheProperty);
    }
    
    @Override
    protected void createUI()
    {
        super.createUI();
        
        m_text.addVerifyListener(new VerifyListener()
        {
            
            @Override
            public void verifyText(VerifyEvent e)
            {
                e.text = e.text.toUpperCase();
            }
        });
    }
    
}
