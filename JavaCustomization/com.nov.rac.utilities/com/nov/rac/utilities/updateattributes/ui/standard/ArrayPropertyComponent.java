package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FillLayout;

import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.stylesheet.PropertyArray;
import com.teamcenter.rac.util.SWTUIUtilities;


public class ArrayPropertyComponent extends Composite
{
	private PropertyArray  m_textControl ;

	public ArrayPropertyComponent(Composite parent, int style) 
	{
		super(parent, style | SWT.EMBEDDED);
		
		m_textControl = new PropertyArray();
		
		SWTUIUtilities.embed(this, m_textControl, true);		
		setLayout(new FillLayout(SWT.HORIZONTAL));
		
	}
	
	public void load(TCProperty theProp) throws Exception
	{
		m_textControl.load(theProp);
	}

	public void save(TCProperty theProp) throws Exception
	{
		m_textControl.save(theProp);
	}

	/*public String[] getString()
	{
		return m_textControl.getValues();
	}*/

    public void setEnabled(boolean isEnabled)
    {
    	super.setEnabled(isEnabled);    	
    	
    	m_textControl.setModifiable(isEnabled);
    	
    }

}
