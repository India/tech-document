package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class IntegerPropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_int_Label       ;//= null;
	private Text       m_int_text        ;//= null;

	public IntegerPropertyComposite(Composite parent, int style,
			                                      TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		int Value = getProperty().getIntValue();
		
		String propValue = Integer.toString(Value);
		m_int_text.setText(propValue);
		 
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_int_Label.setText(propDisplayName);

		if( !getProperty().isModifiable() )
		{
			m_int_text.setEditable(false);
		}
		
	}

	@Override
	protected void savePropertyValue()
	{
		try 
		{
			getProperty().setIntValue(Integer.parseInt(m_int_text.getText()));
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	protected void createUI()
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);

		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_int_Label = new CLabel(this, SWT.NONE);
		m_int_Label.setLayoutData( gd_label );
		
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		//gd_text.widthHint = 151;

		m_int_text  = new Text(this, SWT.BORDER);
		m_int_text.setLayoutData(gd_text);
		
		m_int_text.addFocusListener(new FocusListener()
		{
			@Override
			public void focusLost(FocusEvent focusevent) 
			{
				String textValue = m_int_text.getText();
				try 
				{
					getProperty().setIntValue(Integer.parseInt(textValue));
					m_int_text.setFocus();
				} 
				catch (NumberFormatException e)
				{
					e.printStackTrace();
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}	
			}
			
			@Override
			public void focusGained(FocusEvent focusevent) 
			{
				
			}
		});
		
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_int_Label.setText(theLabel);
	}

}
