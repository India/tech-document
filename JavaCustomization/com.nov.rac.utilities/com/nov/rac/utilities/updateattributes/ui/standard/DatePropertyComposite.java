package com.nov.rac.utilities.updateattributes.ui.standard;

import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class DatePropertyComposite  extends AbstractAttributeComposite
{

	private CLabel     m_Label       ;//= null;
	private NOVUpdateAttributesDateButton m_dateControl ;//= null;
	
	public DatePropertyComposite(Composite parent, int style, TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load() 
	{
		Date propValue = getProperty().getDateValue();
		m_dateControl.setDate(propValue);
		
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_Label.setText(propDisplayName);
		
		if( !getProperty().isModifiable() )
		{
			m_dateControl.setEnabled(false);
		}
		
	}

	@Override
	protected void savePropertyValue()
	{		
		Date date1 = m_dateControl.getDate();
		try 
		{
			getProperty().setDateValue(date1);
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	protected void createUI() 
	{		
		GridLayout gridLayout = new GridLayout(2, true);
		setLayout(gridLayout);
		
		m_Label = new CLabel(this, SWT.RIGHT);
		m_Label.setLayoutData( new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1) );
		
		GridData gd_date = new GridData(SWT.LEFT, SWT.TOP, true, false, 1, 1);
		gd_date.heightHint = 24;
		gd_date.widthHint = 105;
		
		m_dateControl = new NOVUpdateAttributesDateButton(this, SWT.NONE);
		m_dateControl.setLayoutData(gd_date);
	
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}

}
