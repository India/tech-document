package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class NotePropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_note_Label       ;//= null;
	private Text       m_note_text        ;//= null;
	
	public NotePropertyComposite(Composite parent, int style, TCProperty theProp)
	{
		super(parent, style, theProp);
	}

	@Override
	public void load()
	{
		String propValue = getProperty().getNoteValue();
		m_note_text.setText(propValue);
		
		String propDisplayName = getProperty().getDisplayableValue();
		m_note_Label.setText(propDisplayName);
		
		if(!getProperty().isModifiable())
		{
			m_note_text.setEnabled(false);
		}
	}

	@Override
	protected void savePropertyValue() 
	{
		try
		{
			getProperty().setNoteValue(m_note_text.getText());
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_note_Label = new CLabel(this, SWT.NONE);
		m_note_Label.setLayoutData( gd_label );
		
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		//gd_text.widthHint = 151;

		m_note_text = new Text(this, SWT.BORDER);
		m_note_text.setLayoutData(gd_text);
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		m_note_Label.setText(theLabel);
	}

}
