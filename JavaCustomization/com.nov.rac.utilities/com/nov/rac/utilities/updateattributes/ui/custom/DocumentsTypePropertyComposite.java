package com.nov.rac.utilities.updateattributes.ui.custom;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.nov.rac.utilities.utils.LOVUtils;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class DocumentsTypePropertyComposite extends AbstractAttributeComposite 
{
	private CLabel  m_Label ;//= null;
	private CCombo  m_combo ;//= null;

	public DocumentsTypePropertyComposite( Composite parent, 
			                               int style,
			                               TCProperty theProp) 
	{
		super(parent, style, theProp);

	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_Label = new CLabel(this, SWT.NONE);
		m_Label.setLayoutData(gd_label);
		
		GridData gd_combo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		
		Color color = new Color(null, 255, 255, 255);
		
		m_combo = new CCombo(this, SWT.DROP_DOWN | SWT.BORDER);
		m_combo.setBackground(color);
		
		m_combo.setLayoutData(gd_combo);
		m_combo.setEditable(false);

	}

	@Override
	public void load() 
	{
		String propDisplayName = getProperty().getPropertyDisplayName();
		m_Label.setText(propDisplayName);
		
		// 1.0 get TCComponent for the DocumentsType property.
		TCComponent theComp = getProperty().getTCComponent();
		
		try
		{
			// 2.0 get get DocumentsCAT property and its value.
			TCProperty theDocCATProp  = null;
			String     strDocCATValue = null;
			
			if(theComp != null)
			{
				theDocCATProp = theComp.getTCProperty("DocumentsCAT");
				
				strDocCATValue = theDocCATProp.getStringValue();
			
			}
			
			LOVObject [] arrLOVObjs = null;
			
			if(strDocCATValue != null)
			{
				// 3.0 use the prop value, and get values for DocumentsType values.
				String strDocTypeLovName = strDocCATValue + "_Subtype_LOV";
				
				arrLOVObjs = LOVUtils.getLOVObjectsForLOV(strDocTypeLovName);
				
			}
			
			if(arrLOVObjs != null && arrLOVObjs.length > 0)
			{
				String [] strArrLOVValues = new String[arrLOVObjs.length];
				
				for(int inx=0; inx< arrLOVObjs.length; inx++)
				{
					//arrLOVObjs[inx].toString();
					strArrLOVValues[inx] = arrLOVObjs[inx].getTextValue();
				}
				
				if(strArrLOVValues != null)
				{
					m_combo.setItems(strArrLOVValues);
				}
			}
			
			// 3.0 Now get the value of property of DocumentsType, and select that from the drop down.
			String strPropValue = getProperty().getStringValue();
			
			if(strPropValue != null)
			{
				int nIndex = m_combo.indexOf(strPropValue);
				
				if(nIndex > -1)
				{
					m_combo.select(nIndex);
				}
			}
		
		}
		catch( TCException exp)
		{
			exp.printStackTrace();
		}
		
		if(!getProperty().isModifiable())
		{
			m_combo.setEnabled(false);
		}

	}

	@Override
	protected void savePropertyValue() 
	{
	    String strCurrSelectedValue = m_combo.getItem( m_combo.getSelectionIndex() );
	    
	    if(strCurrSelectedValue != null)
	    {
	    	try 
	    	{
				getProperty().setStringValue( strCurrSelectedValue );
			} 
	    	catch (TCException e) 
	    	{			
				e.printStackTrace();
			}
	    }
	}

	@Override
	public void setLabel(String theLabel) 
	{
		m_Label.setText(theLabel);
	}

}
