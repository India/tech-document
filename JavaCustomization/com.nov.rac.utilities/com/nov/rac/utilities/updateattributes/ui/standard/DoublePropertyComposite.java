package com.nov.rac.utilities.updateattributes.ui.standard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class DoublePropertyComposite extends AbstractAttributeComposite 
{
	private CLabel     m_double_Label       ;//= null;
	private Text       m_double_text        ;//= null;
	
	public DoublePropertyComposite(Composite parent, int style,
		                                  	TCProperty theProp) 
	{
		super(parent, style, theProp);
	}

	@Override
	public void load()
	{
		double Value = getProperty().getDoubleValue();
		
		String propValue = Double.toString(Value);
		m_double_text.setText(propValue);
		
		String propDisplayName = getProperty().getPropertyDisplayName();		
		m_double_Label.setText(propDisplayName);

		if( !getProperty().isModifiable() )
		{
			m_double_text.setEditable(false);
		}
	}

	@Override
	protected void savePropertyValue()
	{
		try 
		{
			getProperty().setDoubleValue(Float.parseFloat(m_double_text.getText()));
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	protected void createUI() 
	{
		GridLayout gridLayout = new GridLayout(2,true);		
		setLayout(gridLayout);
		
		GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		
		m_double_Label = new CLabel(this, SWT.NONE);
		m_double_Label.setLayoutData( gd_label );
		
		GridData gd_text = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		//gd_text.widthHint = 151;

		m_double_text = new Text(this, SWT.BORDER);
		m_double_text.setLayoutData(gd_text);
		
		m_double_text.addFocusListener(new FocusListener()
		{
			@Override
			public void focusLost(FocusEvent focusevent)
			{
				String textValue = m_double_text.getText();
				try 
				{
					getProperty().setDoubleValue(Double.parseDouble(textValue));
					m_double_text.setFocus();
				} 
				catch (NumberFormatException e)
				{
					e.printStackTrace();
				} 
				catch (TCException e)
				{
					e.printStackTrace();
				}
			}
			
			@Override
			public void focusGained(FocusEvent focusevent)
			{
				
			}
		});
		
	}
	
	@Override
	public void setLabel(String theLabel) 
	{
		 m_double_Label.setText(theLabel);
	}

}
