package com.nov.rac.utilities.updateattributes.dialog;

import java.util.Collections;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.utilities.updateattributes.factory.AttributeFactory;
import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class UpdateAttributesDialog extends AbstractSWTDialog 
{

	/**
	 * Create the dialog.
	 * @param parentShell
	 * @param theProp 
	 * @param none 
	 */
	
	protected String []   m_strArrAttributes = null;
	protected String m_businessGroup = null;
	protected TCComponent m_selectedObject = null;
	protected Registry m_reg = null;
	protected Registry reg = Registry.getRegistry(this);
	protected String m_objType = null;
	protected String strName2 = null;
	protected Vector<AbstractAttributeComposite> m_VectorComponent = new Vector<AbstractAttributeComposite>();
	
	public UpdateAttributesDialog(Shell parentShell, String objType) 
	{
		super(parentShell);	
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE | SWT.APPLICATION_MODAL);
		m_objType = objType;
	}
	
	public void setAttributes(String [] arrAttributes)
	{
		m_strArrAttributes = arrAttributes;
	}
	
	public void setSelectedItem(TCComponent item)
	{
		m_selectedObject = item;
		//setTitle();
	}
	

	public void setBusinessGroup(String businessGroup) 
	{
		m_businessGroup = businessGroup;
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	
	
	
	@Override
	protected Control createDialogArea(final Composite parent) 
	{
		setTitle();
		Composite composite = new Composite(parent, SWT.NONE);
		
		GridLayout gd = new GridLayout(1, false);
		composite.setLayout(gd);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		//addHeader Image
		
		m_reg = Registry.getRegistry(this);
		setHeader();
		
		Label lblItemRevision = new Label(composite, SWT.CENTER);
		String lblname= m_reg.getString("ItemRevision.Name")+ strName2;
		lblItemRevision.setText(lblname);
		lblItemRevision.setFont(new Font(composite.getDisplay(), "", 9, 1));
		lblItemRevision.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 7));
		
		final ScrolledComposite sc = new ScrolledComposite(parent, SWT.BORDER
		        | SWT.H_SCROLL | SWT.V_SCROLL);
		
		    sc.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, true, 1, 1));
		    
		    final Composite attrComposite = new Composite(sc, SWT.LEFT);

		    RowLayout rowLayout = new RowLayout();
		    rowLayout.center = true;
		    rowLayout.fill = true;
		    rowLayout.justify = true;
		    rowLayout.type = SWT.VERTICAL;
		    attrComposite.setLayout(rowLayout);
		    
		    m_reg = Registry.getRegistry("com.nov.rac.utilities.updateattributes.ui.ui");
			//AttributeFactory theFactory = AttributeFactory.getInstance();
			AttributeFactory theFactory = AttributeFactory.newInstance();
			theFactory.setRegistry(m_reg);
			theFactory.setBusinessGroupName(m_businessGroup);
			theFactory.setAttributes(m_strArrAttributes);
			theFactory.setParentControl( attrComposite );
			theFactory.setSelectedObject( m_selectedObject );
			
			try 
			{
				AbstractAttributeComposite [] uiComps  = theFactory.createUI();
				
				Collections.addAll( m_VectorComponent, uiComps);
				
			}
			catch (TCException e) 
			{
				e.printStackTrace();
			}
				
			//attrComposite.pack();
			sc.setContent(attrComposite);
			sc.setExpandHorizontal(true);
			sc.setExpandVertical(true);
			sc.setMinSize(attrComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		Composite compositeBttn = new Composite(parent, SWT.CENTER);
		GridLayout gd1 = new GridLayout(2, true);
		compositeBttn.setLayout(gd1);
		compositeBttn.setLayoutData(new GridData(SWT.CENTER, SWT.BOTTOM, false, false, 1, 1));
		
		Button btnUpdate = new Button(compositeBttn, SWT.NONE);
		btnUpdate.setText("Update");
		//btnUpdate.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, true, true, 1, 1));
		btnUpdate.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				for(int i=0; i<m_VectorComponent.size(); i++)
				{
					m_VectorComponent.get(i).save();
				}
				
				okPressed();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent event)
			{
				
			}
		});
		
		Button btnCancel = new Button(compositeBttn, SWT.NONE);
		btnCancel.setText("Cancel");
		//btnCancel.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, true, true, 1, 1));

		Listener listener = new Listener()
		{
			@Override
			public void handleEvent(Event event) 
			{
				parent.getShell().dispose();
				
			}
		    };
		    
		    btnCancel.addListener(SWT.Selection, listener);
		    
		return parent;
		
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent)
	{
		/*createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);*/
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() 
	{
		Monitor monitor = this.getShell().getMonitor();

        Rectangle size = monitor.getBounds();
        Rectangle rect = this.getShell().getBounds();
        
        int rectwidth = rect.width;

        int width = ((size.width ) / 2) - (rectwidth / 6);
        int height = ((size.height ) / 2);
        
		return new Point(width, height);

	}

	public void setTitle()
	{
		String strType = m_selectedObject.getType();
		
		strType = strType + ".DIALOG_TITLE";
		
		String strDialogTitle = reg.getString(strType, null);
		
		if(strDialogTitle == null)
		{
			strType = reg.getString("Default.DIALOG_TITLE");
			
			strDialogTitle = reg.getString(strType, null);		
		}
		
		if(strDialogTitle != null)
		{
			this.getShell().setText(strDialogTitle);
		}						
	}
	
	public void setHeader()
	{
		TCComponentItemRevision lastRev = null ;
		
		TCComponentItem selectedItem = (TCComponentItem ) m_selectedObject;
		try 
		{
			lastRev =  selectedItem.getLatestItemRevision();
		} 
		catch (TCException e1)
		{
			e1.printStackTrace();
		}
		
		strName2 = lastRev.toDisplayString();
		
	}
}
