/**
 * 
 */
package com.nov.rac.utilities.updateattributes.factory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.nov.rac.utilities.updateattributes.ui.AbstractAttributeComposite;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;

/**
 * @author kishora
 * 
 * usage :
 * 
 * String groupName = "Mission";
 * Registry theReg = Registry.getRegistry(this);
 * 
 * AttributeFactory theFactory = AttributeFactory.getInstance();
 * 
 * theFactory.setRegistry(theReg);
 * theFactory.setBusinessGroupName(groupName);
 * theFactory.setAttributes(arrayOfAttributes);
 * theFactory.setParentControl( attributeComposite );
 * 
 * AbstractAttributeComposite theComponent = 
 *              theFactory.createUIComponent( parent_composite,
 *                                            style // (can be SWT.None)
 *                                            object_type, // "Nov4Part", 
 *                                            attr_name,   // "object_name"
 *                                            theProp // The TCProperty 
 *                                          );
 *                                          
 */
public class AttributeFactory 
{
	 private final int PROP_array = 1000;
	 
	 private final String ATTRIBUTE_SPLIT_CHAR = ".";
	 
     private static AttributeFactory m_factory = null;
     private Registry m_registry               = null;
     private String m_businessGroupName        = null;
     private TCComponent  m_selectedObject     = null;
     private TCProperty[] m_arrayTopLevelProps = null; 
     private TCProperty[] m_arrayTCProps       = null;   
     private String []   m_strArrAttributes    = null;
     private Control     m_parentControl       = null;
     
     // Integer = Index of attribute in m_strArrAttributes
     // String = Name of the attribute post DOT (eg Attribute => item_revision.item_rev_id, then String => item_rev_id )
     private Map<Integer, String> m_chainedProperty = new HashMap<Integer, String> ();
     
     private Map<Integer, String> propNameMap = null; // new HashMap<Integer, String>();
     
     // String = Container Instance Name
     // AbstractAttributeComposite = Container Object
     private Map<String, AbstractAttributeComposite> m_mapContainers = null; // new HashMap<Integer, String>();
     private boolean m_bContainersLoaded = false;
     
	/**
	 * 
	 */
     
	private AttributeFactory() 
	{
		init();
	}
	
	private void init()
	{
		if(propNameMap == null)
		{
			propNameMap = new HashMap<Integer, String>();
			
			propNameMap.put( new Integer(TCProperty.PROP_char), "PROP_char" );
			propNameMap.put( new Integer(TCProperty.PROP_date), "PROP_date" );
			propNameMap.put( new Integer(TCProperty.PROP_double), "PROP_double" );
			
			propNameMap.put( new Integer(TCProperty.PROP_float), "PROP_float" );
			propNameMap.put( new Integer(TCProperty.PROP_int), "PROP_int" );
			propNameMap.put( new Integer(TCProperty.PROP_logical), "PROP_logical" );
			
			propNameMap.put( new Integer(TCProperty.PROP_long_string), "PROP_long_string" );
			propNameMap.put( new Integer(TCProperty.PROP_note), "PROP_note" );
			propNameMap.put( new Integer(TCProperty.PROP_short), "PROP_short" );
			
			propNameMap.put( new Integer(TCProperty.PROP_string), "PROP_string" );
			
			propNameMap.put( new Integer(TCProperty.PROP_external_reference), "PROP_external_reference" );
			propNameMap.put( new Integer(TCProperty.PROP_typed_reference), "PROP_typed_reference" );
			propNameMap.put( new Integer(TCProperty.PROP_untyped_reference), "PROP_untyped_reference" );		
			
			
			propNameMap.put( new Integer(PROP_array), "PROP_array" );
			
			//propNameMap.put( new Integer(TCProperty.PROP_typed_reference), "PROP_typed_reference" );
		}
		
		// Load Containers
		
	}
	
	/**
	 * Get singleton instance of Attribute Factory.
	 * @return
	 */
	
	public static AttributeFactory getInstance()
	{
		if(m_factory == null)
		{
			m_factory = new AttributeFactory();
		}
		
		return m_factory;
	}
	
	/**
	 * Get singleton instance of Attribute Factory.
	 * @return
	 */
	
	public static AttributeFactory newInstance()
	{
		return (new AttributeFactory());
	}

	
	/**
	 * set the Registry, the Registry will be used by 
	 * Factory for creating UI components.
	 * @param theRegistry
	 */
	public void setRegistry(Registry theRegistry)
	{
		m_registry = theRegistry;
	}

    /**
     * 
     * @return Current Registry being used by Factory.
     */
	public Registry getRegistry()
	{
		return m_registry;
	}
	
	public void setBusinessGroupName(String theName)
	{
		m_businessGroupName = theName;
	}
	
	public void setAttributes(String [] arrAttributes)
	{
		m_strArrAttributes = arrAttributes;
	}
	
	public void setParentControl(Control theParent)
	{
		m_parentControl = theParent;
	}
	
	public void setSelectedObject(TCComponent theSelectedObject)
	{
		m_selectedObject = theSelectedObject;
	}

	/**
	 * @throws TCException 
	 * 
	 */

	public AbstractAttributeComposite[] createUI() throws TCException
	{
		Vector<AbstractAttributeComposite> attrList = new Vector<AbstractAttributeComposite> ();
		
		// 1.0 Get properties for all attributes at one go.
		
		//1.1 Get top level properties (and store chained props in the map)
		String [] strTopLevelProps = getTopLevelProperties( m_strArrAttributes );
		
		// 1.2 Get properties for selected object.
		if(m_selectedObject != null)
		{
//			try 
//			{
			    m_arrayTopLevelProps = m_selectedObject.getTCProperties( strTopLevelProps );
//			} 
//			catch (TCException e) 
//			{			
//				e.printStackTrace();
//			}
		}
		
		// 2.0 for chained properties (eg : item_revision.item_rev_id) get the final TCProperty
		m_arrayTCProps = processChainedProperties( m_chainedProperty, m_arrayTopLevelProps);		
		
		// 3.0 for each TCProperty, create the AttributeComposite.
		attrList = createUIObjects(m_arrayTopLevelProps,m_arrayTCProps);
		
		// 4.0 return the list of Composites to Dialog.		
		AbstractAttributeComposite [] arrayAttrList = new AbstractAttributeComposite[1];
		arrayAttrList = attrList.toArray(arrayAttrList);
		
		return arrayAttrList;
	}
	
	/**
	 * 
	 */
	public AbstractAttributeComposite createUIComponent( Composite parent,
			                                             int style,
			                                             String strObjType, 
			                                             String strAttrName,
			                                             TCProperty theProp) 
	{
		Composite theParent = parent;
		AbstractAttributeComposite theComposite = null;
		
		//1.0 Check if we have a customized container for the attribute.
		String strCustomContainerName = getCustomContainerName( strObjType, strAttrName);
		
		if(strCustomContainerName != null)
		{
			theParent = getCustomContainer( parent, style , strCustomContainerName );
		}
		
		// 1.0 Check if we have customization for a particular attribute.
		String strCustomUIName = getCustomUIName(strObjType, strAttrName);
		
		// 2.0 if we have customization for the attribute, then create the customized UI
		if(strCustomUIName != null)
		{
			theComposite = createCustomUI(  theParent,
                                            style,
                                            strCustomUIName,
                                            theProp
                                          );
		}
		// 3.0 else create the Standard UI
		else
		{
			theComposite = createStandardUI( theParent,
                                             style,
                                             theProp
                                           );
		}
		
		return theComposite;
		
	}
	
	private String getCustomUIName(String strObjType, String strAttrName)
	{
	    String theCustomUI = null;
	    
	    String strRegEntry = null;
	    
	    // 1.0 Try to get the custom UI for "Mission.Nov4Part.object_name"
	    if( m_businessGroupName != null)
	    {
	    	// Create Mission.Nov4Part.object_string
	    	strRegEntry = m_businessGroupName + "." 
	    	              + strObjType + "." 
	    	              + strAttrName
	    	              + ".PROP_COMPOSITE";
	    	
	    	
	    	theCustomUI = getRegistry().getString(strRegEntry, null);
	    	
	    }
	    
	    // 2.0 if we have not got custom UI, then check for "Nov4Part.object_name"
	    if( theCustomUI == null )
	    {
	    	strRegEntry = strObjType + "." + strAttrName + ".PROP_COMPOSITE";
	    	
	    	theCustomUI =  getRegistry().getString(strRegEntry, null);
	    } 
	    
	    // If Custom UI is NOT found for the given Attribute.
	    if(theCustomUI == null)
	    {
	    	strRegEntry = null;
	    }
	    
	    //return theCustomUI;
	    return strRegEntry;
	}
	
	private String getCustomContainerName(String strObjType, String strAttrName)
	{
	    String theCustomUI = null;
	    
	    String strRegEntry = null;
	    
	    // 1.0 Try to get the custom UI for "Mission.Nov4Part.object_name"
	    if( m_businessGroupName != null)
	    {
	    	// Create Mission.Nov4Part.object_string
	    	strRegEntry = m_businessGroupName + "." 
	    	              + strObjType + "." 
	    	              + strAttrName
	    	              + ".PROP_CONTAINER";
	    	
	    	
	    	theCustomUI = getRegistry().getString(strRegEntry, null);
	    	
	    }
	    
	    // 2.0 if we have not got custom UI, then check for "Nov4Part.object_name"
	    if( theCustomUI == null )
	    {
	    	strRegEntry = strObjType + "." + strAttrName + ".PROP_CONTAINER";
	    	
	    	theCustomUI =  getRegistry().getString(strRegEntry, null);
	    } 
	    
	    // If Custom UI is NOT found for the given Attribute.
	    if(theCustomUI == null)
	    {
	    	strRegEntry = null;
	    }
	    
	    //return theCustomUI;
	    return strRegEntry;
	}

	
	/**
	 * Create Customized UI Component
	 * @param strCustomUIName
	 * @return
	 */
	
	private AbstractAttributeComposite createCustomUI(  Composite parent,
                                                        int style,
                                                        String strCustomUIName,
                                                        TCProperty theProp
                                                      )
	{
		AbstractAttributeComposite theUI = null;
		
		Object [] params = new Object[3];
		params[0] = parent;
		params[1] = style;
		params[2] = theProp;
		
		theUI = (AbstractAttributeComposite) getRegistry().newInstanceFor(strCustomUIName, params);
	    
		return theUI;
	}

	/**
	 * Create Standard UI Component
	 * @param strCustomUIName
	 * @return
	 */
	
	private AbstractAttributeComposite createStandardUI( Composite parent,
                                                         int style,
                                                         TCProperty theProp
                                                        )
	{
		AbstractAttributeComposite theUI = null;
		
		// Get TCProperty.getType()
		int propType = TCProperty.PROP_unknown;
		
		if( theProp.getPropertyDescriptor().isArray() )
		{
			propType = PROP_array;
		}
		else
		{		
		    propType = theProp.getPropertyType();
		}
		
		String propName = getPropertyTypeName(propType);
		
		String strStandardUIName = null;
		
		if( propName != null )
		{
			strStandardUIName = propName + ".PROP_TYPE";
		}		
		
		if( strStandardUIName != null )
		{
			Object [] params = new Object[3];
			params[0] = parent;
			params[1] = style;
			params[2] = theProp;

			theUI = (AbstractAttributeComposite) getRegistry().newInstanceFor( strStandardUIName, 
					                                                           params
					                                                         );	
		}
		
		return theUI;
	}
	
	/**
	 * convert prop type int value to String format.
	 * @param propType
	 * @return
	 */
	
	private String getPropertyTypeName( int propType )
	{
	    String propName = null;
	    	    
	    propName = propNameMap.get(new Integer(propType));

	    return propName;
	}
	
	/**
	 * Remove "DOT" from property names and return names of top level props only.
	 *  Also, add chained property into to the Map.
	 */
	private String [] getTopLevelProperties( String [] arrayRawAttrNames )
	{
		String [] arrayTopLevelProperties = new String[arrayRawAttrNames.length];
		
		for(int inx=0; inx < arrayRawAttrNames.length; inx++)
		{
			
			arrayTopLevelProperties[inx] = arrayRawAttrNames[inx];
			
			// the "2" should split the attribute name in at max 2 parts.
			//strSplits =  Pattern.compile(ATTRIBUTE_SPLIT_CHAR).split(arrayRawAttrNames[inx]); //arrayRawAttrNames[inx].split( ATTRIBUTE_SPLIT_CHAR , -1);
			int indexOfDot = arrayRawAttrNames[inx].indexOf(ATTRIBUTE_SPLIT_CHAR);
			
			String firstPart = arrayRawAttrNames[inx];
			String secondPart = "";
			
			if(indexOfDot != -1)
			{
				firstPart = arrayRawAttrNames[inx].substring(0, indexOfDot);
				secondPart = arrayRawAttrNames[inx].substring(indexOfDot + 1);
			}
			
			if( ! firstPart.isEmpty() )//if(strSplits != null && strSplits.length > 0)
			{
				// If we got the first part.
				//if(strSplits[0] != null)
				{
					arrayTopLevelProperties[inx] = firstPart; //strSplits[0];
				}
				
				// for chained properties, we will have strSplits[1] as not null
				if( ! secondPart.isEmpty() )//if(strSplits.length > 1 && strSplits[1] != null)
				{
					// Add the Index and remaining chain of properties into a map.
					m_chainedProperty.put( (new Integer(inx)), secondPart /*strSplits[1]*/);
				}
			}
		}
		
		//
		
		return arrayTopLevelProperties;
	}
	
	/**
	 * @throws TCException 
	 * 
	 * 
	 */
	private TCProperty [] processChainedProperties( Map<Integer, String> mapChainedProperties, TCProperty [] arrayTopLevelProps) throws TCException
	{		
		
		TCProperty currProperty = null;
		TCProperty [] arrLastProps = new TCProperty[1];
		Vector<TCProperty> localVectorTcProps = new Vector<TCProperty>();
		
		Set<Integer> theKeySet = mapChainedProperties.keySet();
		
		// Make a copy of actual property list.
		Collections.addAll(localVectorTcProps, arrayTopLevelProps);
		
		//arrLastProps = arrayTCProps;
		
		// 1.0 go thru the Map, for each entry in map, pick the property and process it till the end node.
		for(Iterator<Integer> setItr = theKeySet.iterator(); setItr.hasNext(); )
		{
			// pick the property and process it till the end node.
			Integer mapKey = setItr.next();
			
			String theChain = mapChainedProperties.get(mapKey);
			
			if( theChain != null )
			{
				currProperty = arrayTopLevelProps[ mapKey.intValue() ];
				
				currProperty = processChain(currProperty, theChain);
			}
			
			// 2.0 get the final property and add it to the list.
			if(currProperty != null)
			{
				localVectorTcProps.set( mapKey.intValue(), currProperty );
				//arrLastProps[ mapKey.intValue() ] = currProperty;
			}
			
		} // for
		
		arrLastProps = localVectorTcProps.toArray(arrLastProps);
		
		return arrLastProps;
	}
	
	/**
	 * @throws TCException 
	 * 
	 * 
	 */
	private TCProperty processChain(TCProperty theProp, String theChain) throws TCException
	{
        TCProperty theLastProp = null;
        TCProperty theTempProp = null;
        
        String [] theSplits = Pattern.compile(ATTRIBUTE_SPLIT_CHAR, Pattern.LITERAL).split(theChain); //theChain.split(ATTRIBUTE_SPLIT_CHAR);
        
        theTempProp = theProp;
        
        // Iterate thru the entire chain.
        for(int inx = 0; inx< theSplits.length; inx++)
        {
        	// If the property type is "Reference" then go ahead and get the child property.
        	if( theTempProp.isReferenceType() )
        	{
        		// get the TCComponent for current property
        		TCComponent theComponent = theTempProp.getReferenceValue();
        		
        		if(theComponent != null)
        		{
            		// get property from child component
            		TCProperty theChainProp = theComponent.getTCProperty( theSplits[inx] );
            		
            		if(theChainProp != null)
            		{
            			theTempProp = theChainProp;
            		}        			
        		}
        	}
        	else
        	{
        		break;
        	}        
        }
        
        theLastProp = theTempProp;
        return theLastProp;
	}

   /**
    * 
    * 
    */
	private Vector<AbstractAttributeComposite> createUIObjects(TCProperty [] arrayTopLevelProps,TCProperty [] arrayTCProps)
	{
		Vector<AbstractAttributeComposite> attrList = new Vector<AbstractAttributeComposite>();
		
		Composite parentControl = (Composite) m_parentControl;
		
		String strObjectType = m_selectedObject.getType(); 
		
		for( int inx= 0; inx < m_strArrAttributes.length; inx++)
		{
			AbstractAttributeComposite theComponent = createUIComponent( parentControl,
                                                                         SWT.None,
                                                                         strObjectType, 
                                                                         m_strArrAttributes[inx],
                                                                         arrayTCProps[inx]
                                                                       );
			
			if(theComponent != null)
			{
				//String theLabel = arrayTopLevelProps[inx].getPropertyDisplayName();
				//theComponent.setLabel( theLabel );
				
				attrList.add(theComponent);	
			}
		}
		 
                
		return attrList;
	}
	
	 private Composite getCustomContainer( Composite parent, int style , String strCustomContainerName )
	 {
        Composite theParent = parent;
		 
        // 1.0 get the container instance for the Container Name.
        String strContainerInstance = getRegistry().getString(strCustomContainerName);
        
        if(strContainerInstance != null)
        {
            // 2.0 Check if container instance exists. if not then create one.
        	// TODO : Move this code to an appropriate place...
        	if( m_bContainersLoaded == false)
        	{
        		loadContainers();
        		m_bContainersLoaded = true;
        	}
        	
        	theParent = createOrFindCustomContainer( parent, style , strContainerInstance );
        	
        }      
        
		return theParent;
	 }
	 
	 /*
	  * 
	  * Load all available container instance names from the Registry.
	  */
	 private void loadContainers()
	 {
          String [] strArrAvlContainers = getRegistry().getStringArray("AvailablePropContainers");
          
          if(strArrAvlContainers != null)
          {	      
        	  m_mapContainers = new HashMap<String, AbstractAttributeComposite>();
        	  
	          for(int inx=0; inx< strArrAvlContainers.length; inx++)
	          {
	        	  m_mapContainers.put( strArrAvlContainers[inx], null);
	          }
          }
	 }

	 private Composite createOrFindCustomContainer( Composite parent, int style , String strCustomContainerName )
	 {
		 Composite theParent =  parent;
		 
		 // 1.0 find if strCustomContainerName exists in the map.
		 if( m_mapContainers.containsKey(strCustomContainerName) )
		 {
			// 2.0 if found, check if container instance is available.
			Composite theContainer = m_mapContainers.get(strCustomContainerName);
			
			if(theContainer != null && !(theContainer.isDisposed()) )
			{
				// got the container, send it as return value.
				theParent = theContainer;
			}
			else
			{
				// 3.0 if instance not available create one.
				Object [] params = new Object[3];
				params[0] = parent;
				params[1] = style;
				params[2] = null;
				
				AbstractAttributeComposite theContainerUI = 
					(AbstractAttributeComposite) getRegistry().newInstanceFor(strCustomContainerName, params);
				
				if(theContainerUI != null)
				{
					m_mapContainers.put(strCustomContainerName, theContainerUI);
					theParent = theContainerUI;
				}
				
			}
		 } 		 
		 
		 return theParent;
	 }

}
