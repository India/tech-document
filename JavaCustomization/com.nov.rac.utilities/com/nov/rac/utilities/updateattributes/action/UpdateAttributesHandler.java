/* ============================================================
  
  File description: 

  Filename: UpdateAttributesHandler.java 
  Module  : /com/nov/rac/utilities/updateattributes/action

  UpdateAttributesHandler is invoked when user selects 'UpdateAttributes' 
  option from custom 'NOVTools' menu 

  Date         Developers    Description
  02-08-2011   Vivek   	  Initial Creation

  $HISTORY
  ============================================================ */

package com.nov.rac.utilities.updateattributes.action;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.utilities.updateattributes.dialog.UpdateAttributesDialog;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdentifier;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class UpdateAttributesHandler extends AbstractHandler
{
    private TCSession m_session = (TCSession) AIFUtility.getDefaultSession();
    private InterfaceAIFComponent[] m_comps = null; // AIFUtility.getTargetComponents();
    private String m_prefName = null;
    private String[] m_arrAttributeNames = null;
    private String m_businessGroup = null;
    private String m_objType = null;
    private Registry m_reg = Registry.getRegistry(this);
    
    public UpdateAttributesHandler()
    {
        super();
    }
    
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        
        m_comps = AIFUtility.getTargetComponents();
        
        if (m_comps.length <= 0)
        {
            MessageBox.post(m_reg.getString("NO_Object_Selected.ERROR"), m_reg.getString("Error.TITLE"),
                    MessageBox.ERROR);
            
            return null;
        }
        else if (m_comps.length > 1)
        {
            MessageBox.post(m_reg.getString("More_Than_One_Object_Selected.ERROR"), m_reg.getString("Error.TITLE"),
                    MessageBox.ERROR);
            
            return null;
        }
        
        boolean isValid = validateComponents(m_comps, m_session);
        
        if (!isValid)
        {
            MessageBox.post(m_reg.getString("Owning_Object.ERROR"), m_reg.getString("Error.TITLE"), MessageBox.ERROR);
            return null;
        }
        
        getAttributeNames();
        TCComponent item = (TCComponent) m_comps[0];
        
        boolean isMod = false;
        try
        {
            isMod = item.okToModify();
            
            if (!isMod || (m_arrAttributeNames == null || m_arrAttributeNames.length == 0))
            {
                MessageBox.post(m_reg.getString("Access_Denied.ERROR"), m_reg.getString("Error.TITLE"),
                        MessageBox.ERROR);
            }
            // 6.0 if preference returns any value then return true. else return
            // false.
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        if ((m_arrAttributeNames != null) && (m_arrAttributeNames.length > 0) && (isMod))
        {
            boolean isModifiable = false;
            
            try
            {
            	isModifiable = isRevisionModifiable(item);
            }
            catch (TCException e)
            {
                MessageBox.post(e.getError(), m_reg.getString("Error.TITLE"), MessageBox.ERROR);
                e.printStackTrace();
            }
            
            if (isModifiable)
            {
                UpdateAttributesDialog updateAttributes = new UpdateAttributesDialog(window.getShell(), m_objType);
                updateAttributes.setAttributes(m_arrAttributeNames);
                updateAttributes.setSelectedItem(item);
                updateAttributes.setBusinessGroup(m_businessGroup);
                updateAttributes.open();
            }
        }
        return null;
    }
    
    public String[] getAttributeNames()
    {
        m_arrAttributeNames = null;
        
        // 1.0 Get current logged in group from TCSession
        
        String currUserGroup = null;
        try
        {
            m_session = (TCSession) AIFUtility.getDefaultSession();
            currUserGroup = m_session.getCurrentGroup().getFullName();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        // 2.0 Check if the group exists in Mission preference
        // "_NOV_Mission_item_creation_groups_"
        
        // TCPreferenceService prefServ = m_session.getPreferenceService();
        //
        // String[] grpStrArray = prefServ.getStringArray(
        // TCPreferenceService.TC_preference_site,
        // "_NOV_Mission_item_creation_groups_test");
        //
        // for(int i = 0; grpStrArray != null && i< grpStrArray.length; i++ )
        // {
        // if(currUserGroup.equals(grpStrArray[i]))
        // {
        // m_businessGroup = "Mission";
        // break;
        // }
        // else
        // {
        // m_businessGroup = "RSOne";
        // }
        // }
        m_businessGroup = findBusinessGroup(currUserGroup);
        
        // 3.0 get the Selected Object, and get its object_type
        
        m_objType = m_comps[0].getType();
        
        // 4.0 Generate the preference name using
        // "NOV_<Group Name>_<Object Type>_update_attributes"
        if (m_objType != null && m_businessGroup != null)
        {
            m_prefName = "NOV_" + m_businessGroup + "_" + m_objType + "_update_attributes";
        }
        
        // here the Group Name refers to RSOne or DH or Mission
        
        // 5.0 Read the values for the preference.( generated preference )
        
        if (m_prefName != null)
        {
            TCPreferenceService prefServ = m_session.getPreferenceService();
            
            m_arrAttributeNames = prefServ.getStringArray(TCPreferenceService.TC_preference_site, m_prefName);
        }
        
        return m_arrAttributeNames;
        
    }// getAttributeNames
    
    public boolean validateComponents(InterfaceAIFComponent[] items, TCSession tcSession)
    {
        boolean isValid = false;
        
        String currentUsrGrp = tcSession.getCurrentGroup().toString();
        String owning_group = "";
        
        try
        {
            owning_group = ((TCComponent) items[0]).getTCProperty("owning_group").toString();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
        String currUserBusinessGrp = findBusinessGroup(currentUsrGrp);
        String owningUserBusinessGrp = findBusinessGroup(owning_group);
        
        if (currUserBusinessGrp.equalsIgnoreCase(owningUserBusinessGrp))
        {
            isValid = true;
            // MessageBox.post(m_reg.getString("Owning_Object.ERROR"),
            // m_reg.getString("Error.TITLE"), MessageBox.ERROR);
        }
        
        return isValid;
    }
    
    /**
     * Given the current logged in group, get the Business Group name.
     * 
     * @param strCurrentGroup
     * @return Business Group Name
     */
    private String findBusinessGroup(String strCurrentGroup)
    {
        String strCurrentBusinessGroup = "RSOne";
        
        // 1.0 get all preference names from Registry.
        
        String[] strArrAllPrefs = m_reg.getStringArray("All_Item_Creation_Group_Names", null);
        
        if (strArrAllPrefs != null && strArrAllPrefs.length > 0)
        {
            TCPreferenceService prefServ = m_session.getPreferenceService();
            
            // 2.0 Iterate over available Item Creation Group Names.
            for (int inx = 0; inx < strArrAllPrefs.length; inx++)
            {
                // 3.0 Get the preference name for Item Creation Group
                String strItemCreationGroupPrefName = m_reg.getString(strArrAllPrefs[inx] + ".PREF", null);
                
                if (strItemCreationGroupPrefName == null)
                {
                    continue;
                }
                
                String[] grpStrArray = prefServ.getStringArray(TCPreferenceService.TC_preference_site,
                        strItemCreationGroupPrefName);
                
                for (int i = 0; grpStrArray != null && i < grpStrArray.length; i++)
                {
                    if (strCurrentGroup.equals(grpStrArray[i]))
                    {
                        strCurrentBusinessGroup = strArrAllPrefs[inx];
                        
                        return strCurrentBusinessGroup;
                        // break;
                    }
                }
            }
        }
        
        return strCurrentBusinessGroup;
        
    }
    
    private boolean isLatestRevisionReleased(TCComponent item)
    {
        boolean isReleased = false;
        if (item instanceof TCComponentItem)
        {
            try
            {
                TCComponentItemRevision latestRev = ((TCComponentItem) item).getLatestItemRevision();
                TCComponent[] relStatusList;
                relStatusList = (latestRev.getTCProperty("release_status_list").getReferenceValueArray());
                for (int index = 0; index < relStatusList.length; ++index)
                {
                    if (relStatusList[index].getProperty("name").equalsIgnoreCase("Released"))
                    {
                        isReleased = true;
                        break;
                    }
                }
            }
            catch (TCException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return isReleased;
        
    }

    /**
     * Given the current Item, validates if the latest revision and the JDE ID revision are modifiable.
     * and returns the result.
     * 
     * @param strCurrentGroup
     * @return Business Group Name
     */
    private boolean isRevisionModifiable(TCComponent item) throws TCException
    {
    	boolean isModifiableReturn = false;
        TCComponentItemRevision latestRev = ((TCComponentItem) item).getLatestItemRevision();
        String isModifiable_1 = latestRev.getProperty("is_modifiable");
        if (isModifiable_1.length() > 0)
        {
            TCComponentIdentifier altid_tag = (TCComponentIdentifier) latestRev
                    .getRelatedComponent("altid_list");
            if(altid_tag == null)
            {
            	isModifiableReturn = true;
            }
            else
            {
                String isModifiable_2 = altid_tag.getProperty("is_modifiable");
                if (isModifiable_2.length() > 0)
                {
                	isModifiableReturn = true;
                }
                else
                {
                    MessageBox.post(
                            "No access on the JDE ID of the latest revision. Please initiate Mission Change Workflow "
                                    + "and then use Update Attribute", m_reg.getString("Error.TITLE"),
                            MessageBox.ERROR);
                }
            }
        }
        else
        {
            MessageBox.post("Latest Revision is Released. Please initiate Mission Change Workflow "
                    + "and then use Update Attribute", m_reg.getString("Error.TITLE"), MessageBox.ERROR);
        }
        
        return isModifiableReturn;
    }
    
}
