package com.nov.rac.utilities.tester;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.expressions.PropertyTester;

import com.nov.rac.utilities.utils.UpdateAttributesUtils;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.AdapterUtil;

public class NOVUpdatePropVisibilityTester extends PropertyTester {

	
	private TCSession m_session;
	private Map<String, Boolean> m_preferencesCache= new HashMap<String, Boolean>();
	private String m_userGroup=null;
	private String m_bussinessGroup=null;

	public NOVUpdatePropVisibilityTester()
	{
		m_session = (TCSession) AIFUtility.getDefaultSession();
	}
	
	@Override
	public boolean test(Object obj, String property, Object[] args,Object expectedValue) {
		boolean flag = false;
		TCComponent tccomponent = (TCComponent) AdapterUtil.getAdapter(obj, com.teamcenter.rac.kernel.TCComponent.class);
		
		if (tccomponent != null && "isUpdatePropVisible".equals(property))
		{
			
			flag = isObjectTypeSupported( tccomponent);
		}
		return flag;
		
	}

	private boolean isObjectTypeSupported( TCComponent tccomponent)
	{
		boolean flag=false;
		
		String currentUserGroup = null, currentBusnsGroup=null, prefName=null;
		String componentType = tccomponent.getType();
		try 
		{
			currentUserGroup = m_session.getCurrentGroup().getFullName();
		} 
		catch (TCException e) 
		{		
			e.printStackTrace();
		}
		
		if(!currentUserGroup.equalsIgnoreCase(m_userGroup))
		{
			currentBusnsGroup=UpdateAttributesUtils.findBusinessGroup( currentUserGroup, m_session );
			m_bussinessGroup=currentBusnsGroup;
			m_userGroup=currentUserGroup;
		}
		 
		if(componentType != null && m_bussinessGroup != null)
		{
			prefName = "NOV_" + m_bussinessGroup + "_" + componentType + "_update_attributes";         	
		}
		
		if( prefName != null )
		{
			//check if prefName is cached
			if(m_preferencesCache.get(prefName)!=null)
			{
				flag = (Boolean) m_preferencesCache.get(prefName);
				return flag;
			}
			else //check from preference
			{
				TCPreferenceService prefServ = m_session.getPreferenceService();
			
				String[] arrAttributeNames = prefServ.getStringArray(  TCPreferenceService.TC_preference_site,
						                                      prefName 
						                                   );
				if(arrAttributeNames.length>0)
				{
					flag=true;
				}
				else
				{
					flag=false;
				}
				m_preferencesCache.put(prefName,flag); //cache the value
			}
		}
		return flag;
	}

}


