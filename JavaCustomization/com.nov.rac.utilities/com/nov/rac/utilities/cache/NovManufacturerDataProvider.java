package com.nov.rac.utilities.cache;

import java.rmi.RemoteException;
import java.util.Vector;

import com.nov.www.esb.GetManufacturersResponse.Manufacturers;
import com.nov.www.esb.GetManufacturersResponse.ManufacturersManufacturer;
import com.oracle.xmlns.GetManufacturers.GetManufacturersProxy;
import com.teamcenter.rac.util.MessageBox;

public class NovManufacturerDataProvider {
	private static NovManufacturerDataProvider m_novManufacturerDataProvider;
	private Vector<String> m_mfgNameVec = new Vector<String>();
	
 	public static synchronized NovManufacturerDataProvider getInstance(String instance)
 	{
 		if( m_novManufacturerDataProvider == null )
 		{
 			m_novManufacturerDataProvider = new NovManufacturerDataProvider(instance);
 		}	
 		
 		return m_novManufacturerDataProvider;
 	}
 	
 	public NovManufacturerDataProvider(String instance)
 	{
 		createManufacturerNames(instance);
 	}
 	
 	private void createManufacturerNames(String instance) {
	    
 		if(m_mfgNameVec.isEmpty() && instance != null && instance.length() > 0) {
 			
			GetManufacturersProxy proxy = new GetManufacturersProxy();
			
			try {
				Manufacturers mfgs = proxy.process(instance);
			    for (int i=0;mfgs.getManufacturer() != null && i < mfgs.getManufacturer().length;i++) 
			    {
			    	ManufacturersManufacturer mfg = mfgs.getManufacturer(i);
			    	m_mfgNameVec.add(mfg.getName());
			    }				
		    
				m_mfgNameVec.add("UNSPECIFIED");

				System.out.println("MFG names are loaded into cache...");			    
			} catch (RemoteException e) {
				e.printStackTrace();
				MessageBox.post(e, "GetManufacturersProxy failed due to: " + e.getMessage());
			}
 		}
 	}
 	
 	public Vector<String> getManufacturerNames() {
		return m_mfgNameVec;
 	}
}
