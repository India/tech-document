package com.nov.rac.utilities.thread.utils;

public class NOVCustomRunnable implements Runnable 
{
    protected Exception theException = null;
    
    public NOVCustomRunnable()
    {
        super();
        theException = null;
    }
    
    @Override
    public void run() 
    {
        // TODO Auto-generated method stub
    }
    
    public Exception getException()
    {
        return theException;
    }
    
    public void setException(Exception exception)
    {
        theException = exception;
    }
}
