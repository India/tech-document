package com.nov.rac.utilities.userservices;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;

public abstract class AbstractUserServiceHelper
{
    protected int ARG_NUMBER = 0; /*
                                   * Number of arguments required for user
                                   * service, if any new arguments are added in
                                   * future, just update this number here�
                                   */
    private int m_argCount = 0;
    protected Object[] m_inputArgs = null;
    
    protected TCUserService m_Service = null;
    
    public AbstractUserServiceHelper()
    {
    }
    
    public void initServiceHelper()
    {
        // Allocate Objects array for required arguments.
        m_inputArgs = new Object[ARG_NUMBER];
        
        if (m_inputArgs != null && m_inputArgs.length > 0)
        {
            for (int inx = 0; inx < ARG_NUMBER; inx++)
            {
                m_inputArgs[inx] = "";
            }
        }
        
        // Get Service reference.
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        m_Service = session.getUserService();
    }
    
    public abstract Object invokeUserService() throws TCException;
    
    /*
     * For each argument to the service, the RAC client needs to call
     * addArgument function on helper.
     */
    public void addArgument(Object objArgument)
    {
        if (m_argCount < ARG_NUMBER)
        {
            m_inputArgs[m_argCount] = objArgument;
            m_argCount = m_argCount + 1;
        }
    }
    
}
