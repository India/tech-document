package com.nov.rac.utilities.userservices;

import com.teamcenter.rac.kernel.TCException;

public class NOV4CopyNamedReferences extends AbstractUserServiceHelper
{
    
    public NOV4CopyNamedReferences()
    {
        ARG_NUMBER = 2; // Number of arguments need to be initialized
        initServiceHelper();
    }
    
    @Override
    public Object invokeUserService() throws TCException
    {
        return m_Service.call("NATOIL_CopyNamedReferences", m_inputArgs);
    }
    
}
