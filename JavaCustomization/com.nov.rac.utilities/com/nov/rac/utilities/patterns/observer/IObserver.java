package com.nov.rac.utilities.patterns.observer;

public interface IObserver 
{
	public void update();	

}
