package com.nov.rac.utilities.patterns.observer;

public interface ISubject {
	public void notifyObserver();
	public void registerObserver(IObserver subs);
	public void removeObserver(IObserver subs);

}
