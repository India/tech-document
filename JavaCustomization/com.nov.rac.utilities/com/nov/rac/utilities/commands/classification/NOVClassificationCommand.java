package com.nov.rac.utilities.commands.classification;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.utilities.commands.classification.patternCreator.ClassificationPatternCreator;
import com.nov.rac.utilities.commands.classification.patternCreator.FrenchClassfnPatternCreator;
import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.teamcenter.rac.classification.common.AbstractG4MContext;
import com.teamcenter.rac.classification.common.commands.G4MSaveCommand;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.ics.ICSApplicationObject;
import com.teamcenter.rac.kernel.ics.ICSView;
import com.teamcenter.rac.util.Registry;

public class NOVClassificationCommand extends G4MSaveCommand
{
    
    private AbstractG4MContext m_abstractG4MCtx = null;
    private Registry m_registry;
    private TCSession m_session;
    private TCPreferenceService m_prefService = null;
    private static String m_classIdArray[] = null;
    private boolean m_proceedSave = false;
    private PreferenceHelper m_prefHlpr = null;
    private List<String> m_classIDList = new ArrayList<String>();
    private List<String> m_classIDAttributeIDList = new ArrayList<String>();
    private static String[] m_attributeIDClassIDArr = null;
    public String m_attributeID = null;
    
    public NOVClassificationCommand(AbstractG4MContext arg0, String arg1, Boolean arg2)
    {
        super(arg0, arg1, arg2);
        m_prefHlpr = new PreferenceHelper();
    }
    
    public NOVClassificationCommand(AbstractG4MContext context, String arg1)
    {
        super(context, arg1);
        m_abstractG4MCtx = context;
        m_registry = Registry.getRegistry(this);
        m_session = m_abstractG4MCtx.getSession();
        m_prefService = m_session.getPreferenceService();
        m_prefHlpr = new PreferenceHelper();
    }
    
    @Override
    public void executeCommand()
    {
        super.executeCommand();
        
    }
    
    @Override
    public void executeSaveCommand() throws Exception
    {
        boolean isChangeToSerialized = checkForSerailizationClass(this.m_context.getICSApplicationObject());
        
        if (!isChangeToSerialized)
        {
            executeG4MSave();
        }
        else
        {
            Display.getDefault().syncExec(new Runnable()
            {
                public void run()
                {
                    
                    Shell shell = new Shell(Display.getDefault(), SWT.APPLICATION_MODAL | SWT.NO_TRIM);
                    MessageBox msgBox = new MessageBox(shell, SWT.OK | SWT.CANCEL);
                    msgBox.setText("Warning");
                    String warningMsg = m_registry.getString("serialize_warning.msg");
                    msgBox.setMessage(warningMsg);
                    
                    int selectedOption = msgBox.open();
                    
                    if (selectedOption == SWT.OK)
                    {
                        m_proceedSave = true;
                    }
                    
                    shell.dispose();
                }
            });
            
            if (m_proceedSave)
            {
                executeG4MSave();
            }
            
            return;
        }
    }
    
    private void executeG4MSave() throws Exception
    {
        try
        {
            super.executeSaveCommand();
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    
    private boolean checkForSerailizationClass(ICSApplicationObject icsApplicationObj)
    {
        boolean serialized = false;
        String currentClassId = icsApplicationObj.getClassId();
        if (null != currentClassId && m_prefHlpr.isGroupRequireSerialization())
        {
            boolean currentStat = m_prefHlpr.isSerializedClass(currentClassId);
            if (!currentStat && icsApplicationObj != null)
            {
                ICSView icsView = icsApplicationObj.getICSBaseObject().getView();
                if (null != icsView)
                {
                    String classId = icsView.getClassID();
                    serialized = m_prefHlpr.isSerializedClass(classId);
                }
            }
        }
        return serialized;
    }
    
    @Override
    protected void invokeCustomPostHook()
    {
        super.invokeCustomPostHook();
        
        String classIDValue = null;
        ICSApplicationObject icsApplicationObj = this.m_context.getICSApplicationObject();
        if (m_prefHlpr.isGroupRequireSerialization())
        {
            updateRequireSerializationOption(icsApplicationObj);
        }
        if (m_classIdArray == null)
        {
            m_classIdArray = m_prefService.getStringArray(TCPreferenceService.TC_preference_site,
                    m_registry.getString("NOV4_AUTOCOMPUTE_CLASSID_PREF"));
        }
        fetchClassIDAttributeID(m_classIdArray);
        for (String classId : m_classIdArray)
        {
            if (classId.equalsIgnoreCase(icsApplicationObj.getClassId()))
            {
                classIDValue = classId;
                break;
            }
        }
        // Below logic validates class name with selected classified object
        validateClassAndAutoCompute(classIDValue, icsApplicationObj);
        
    }
    
    /*
     * validateClassAndAutoCompute(String classIDValue, ICSApplicationObject
     * icsApplicationObj); This method validated ClassId value and based on
     * Class ID corresponding classification attributes are fetched the same
     * will be send UserService
     */
    private void validateClassAndAutoCompute(String classIDValue, ICSApplicationObject icsApplicationObj)// KLOC2121
    {
        String attributevalue = null;
        if (classIDValue != null && icsApplicationObj != null)
        {
            TCComponent icoObj = null;
            try
            {
                icoObj = icsApplicationObj.getClassifiedComponent();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            attributevalue = matchClassIdWithInput(classIDValue, m_attributeIDClassIDArr);
            m_attributeID = attributevalue;
            if (m_attributeID == "defaultID")
            {
                // Invoke NonFrench class
                ClassificationPatternCreator classfnPatternCreatorOcj = new ClassificationPatternCreator(this,
                        classIDValue, icsApplicationObj, icoObj);
                classfnPatternCreatorOcj.buildClassfnPattern();
            }
            else
            {
                // //Invoke French class
                FrenchClassfnPatternCreator frPatternCreatorOcj = new FrenchClassfnPatternCreator(this, classIDValue,
                        icsApplicationObj, icoObj);
                frPatternCreatorOcj.buildFRClassfnPattern();
            }
        }
    }
    
    /*
     * fetchClassIDAttributeID(String classAttributeArr[]) FRAP classification
     * objects: This method fills ClassIDArray and AttributeIDClassIDArray
     * HOUSTON classification objects: This method fills ClassIDArray
     */
    private void fetchClassIDAttributeID(String classAttributeArr[])
    {
        for (String idValue : classAttributeArr)
        {
            if (idValue.indexOf(NOVClassificationConstants.TildeDelimiter) < 0)
            {
                m_classIDList.add(idValue);
            }
            else
            {
                StringTokenizer st = new StringTokenizer(idValue, NOVClassificationConstants.TildeDelimiter);
                if (st.hasMoreTokens())
                {
                    m_classIDList.add(st.nextToken());
                    m_classIDAttributeIDList.add(idValue);
                }
            }
        }
        convertListToArray(m_classIDList, m_classIDAttributeIDList);// KLOC2122
    }
    
    /*
     * convertListToArray( List<String> m_classIDList, List<String>
     * m_classIDAttributeIDList); This method converts List to Array
     */
    private void convertListToArray(List<String> m_classIDList, List<String> m_classIDAttributeIDList)
    {
        if (m_classIDList.size() > 0)
        {
            m_classIdArray = m_classIDList.toArray(new String[m_classIDList.size()]);
        }
        if (m_classIDAttributeIDList.size() > 0)
        {
            m_attributeIDClassIDArr = m_classIDAttributeIDList.toArray(new String[m_classIDAttributeIDList.size()]);
        }
    }
    
    public String[] getClassIDArray()
    {
        return m_classIdArray;
    }
    
    public String[] getAttributeIDArray()
    {
        return m_attributeIDClassIDArr;
    }
    
    public TCPreferenceService getPreferenceService()
    {
        return m_prefService;
    }
    
    public TCSession getSession()
    {
        return m_session;
    }
    
    /*
     * matchClassIdWithInput(String classIDValue, String classAttributeArr[]):
     * For valid classAttributeArr this methos returns the Atribute ID to which
     * autcomputed value should be stored. IF classAttributeArr is null,
     * returning "NA" string for validation.
     */
    private String matchClassIdWithInput(String classIDValue, String classAttributeArr[])
    {
        
        String attributeId = "defaultID";
        if (classAttributeArr != null)
        {
            for (String classAttributeValue : classAttributeArr)
            {
                if (classAttributeValue.contains(classIDValue))
                {
                    attributeId = getAttribute(classAttributeValue);
                    
                }
                
            }
        }
        return attributeId;
    }
    
    /*
     * getAttribute(String classAttributeValue): This method filters ClassID
     * returns attribute Id only.
     */
    private String getAttribute(String classAttributeValue)
    {
        String attributeId = null;
        attributeId = classAttributeValue.substring(
                classAttributeValue.indexOf(NOVClassificationConstants.TildeDelimiter, 0) + 1,
                classAttributeValue.length());
        
        return attributeId;
    }
    
    private void updateRequireSerializationOption(ICSApplicationObject icsApplicationObj)
    {
        if (icsApplicationObj != null)
        {
            ICSView icsView = icsApplicationObj.getICSBaseObject().getView();
            if (null != icsView)
            {
                String classId = icsView.getClassID();
                boolean serializeReq = m_prefHlpr.isSerializedClass(classId);
                
                try
                {
                    TCComponent itemRev = icsApplicationObj.getClassifiedComponent();
                    if (serializeReq)
                    {
                        updateItemMaster((TCComponentItemRevision) itemRev, serializeReq);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    private void updateItemMaster(TCComponentItemRevision latestRev, boolean serializeReq)
    {
        try
        {
            TCComponentItem item = latestRev.getItem();
            String itemType = item.getType();
            if (itemType.equalsIgnoreCase("Nov4Part") || itemType.equalsIgnoreCase("Non-Engineering"))
            {
                TCComponentForm itemMaster = (TCComponentForm) item.getRelatedComponent("item_master_tag");
                if (null != itemMaster)
                {
                    String serilizationReq = serializeReq ? "Y" : "";
                    itemMaster.setProperty("rsone_serialize", serilizationReq);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
    }
    
}
