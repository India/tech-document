package com.nov.rac.utilities.commands.classification.patternCreator;

import com.nov.rac.utilities.commands.classification.NOVClassificationCommand;
import com.nov.rac.utilities.commands.classification.NOVClassificationConstants;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.kernel.ics.ICSApplicationObject;
import com.teamcenter.rac.util.Registry;

/**
 * @author kabades created on Jan 2014
 */

public class FrenchClassfnPatternCreator
{
    private NOVClassificationCommand m_frClassfinCommand = null;
    private static String m_frAutoComputePrefValues[] = null;
    private Registry m_frRegistry;
    private String m_frClassIDValue = null;
    private ICSApplicationObject m_frICSpplicationObj = null;
    private TCComponent m_frICOObj = null;
    
    public FrenchClassfnPatternCreator(NOVClassificationCommand classfinCommand, String classIDValue,
            ICSApplicationObject icsApplicationObj, TCComponent icoObj)
    {
        this.m_frClassfinCommand = classfinCommand;
        this.m_frClassIDValue = classIDValue;
        this.m_frICSpplicationObj = icsApplicationObj;
        this.m_frICOObj = icoObj;
        m_frRegistry = Registry.getRegistry("com.nov.rac.utilities.commands.classification.classification");
    }
    
    /**
     * buildFRClassfnPattern() Method builds pattern for French class
     */
    public void buildFRClassfnPattern()
    {
        String[] patternArray = null;
        String[] attrIdArr = null;
        String attributevalue = m_frClassfinCommand.m_attributeID;
        String[] classfnAttributes = null;
        
        classfnAttributes = fetchClassificationAttrFromPref(m_frClassIDValue, m_frICSpplicationObj);
        if (classfnAttributes != null)
        {
            patternArray = PatternCreatorHelper.getPatterns(classfnAttributes);
            attrIdArr = PatternCreatorHelper.getAttributeIds(classfnAttributes);
            invokeUserService(m_frClassIDValue, attrIdArr, patternArray, m_frICOObj, attributevalue);
        }
    }
    
    /**
     * @param classIDValue
     * @param icsApplicationObj
     * @return: classification attributes array for given i/p classIDValue &
     *          icsApplicationObj
     */
    private String[] fetchClassificationAttrFromPref(String classIDValue, ICSApplicationObject icsApplicationObj)
    {
        String[] attributes = null;
        String attributeValue = PatternCreatorHelper.fetchAttributeValueofStrType(icsApplicationObj,
                NOVClassificationConstants.PITCH_ATTRIBUTE_ID);
        attributes = getClassificationAttributes(classIDValue, attributeValue,
                NOVClassificationConstants.PITCH_ATTRIBUTE_ID);
        return attributes;
    }
    
    /**
     * @param classIDValue
     * @param attributeValue
     * @param attributeId
     * @return: attributeArray belong to the given ClassIDvalue
     */
    private String[] getClassificationAttributes(String classIDValue, String attributeValue, int attributeId)
    {
        String attributeArray[] = null;
        String stringArray[] = new String[] { NOVClassificationConstants.DashCharacter,
                NOVClassificationConstants.ISO_STANDARD };
        boolean isStringMataches = PatternCreatorHelper.isStringMatches(attributeValue, stringArray);
        String prefValue = fetchPrefValue(classIDValue);
        String preStringArray[] = PatternCreatorHelper.fetchTokenArray(prefValue);
        String strAttributeId = String.valueOf(attributeId);
        attributeArray = fetchAttrArrayBasedOnInput(preStringArray, strAttributeId, isStringMataches);
        return attributeArray;
    }
    
    /**
     * @param classId
     * @return preference value for the i/p
     */
    private String fetchPrefValue(String classId)
    {
        String matchingClassIDExprssion = null;
        if (m_frAutoComputePrefValues == null)
        {
            m_frAutoComputePrefValues = m_frClassfinCommand.getPreferenceService().getStringArray(
                    TCPreferenceService.TC_preference_site, m_frRegistry.getString("NOV4_AUTOCOMPUTE_PREF"));
        }
        
        for (String autoComputePrefValue : m_frAutoComputePrefValues)
        {
            if (PatternCreatorHelper.isClassIDMatchesWithPrefValue(autoComputePrefValue, classId))
            {
                matchingClassIDExprssion = autoComputePrefValue;
                break;
            }
        }
        return matchingClassIDExprssion;
        
    }
    
    /**
     * @param preStringArray
     * @param attributeId
     * @param isStringMatched
     * @return attribute Array for given i/p attributeId and if Pitch attribute
     *         has Dash{"-" or "ISO standard"}value
     */
    private String[] fetchAttrArrayBasedOnInput(String[] preStringArray, String attributeId, boolean isStringMatched)
    {
        String[] attributeArray = null;
        for (int i = 0; i < preStringArray.length; i++)
        {
            if (preStringArray[i].contains(attributeId) || isStringMatched)
            {
                attributeArray = PatternCreatorHelper.getSelectedClassId(preStringArray[i]);
                break;
            }
        }
        return attributeArray;
    }
    
    /**
     * @param classIDValue
     * @param selectedClassfnAttributes
     * @param patternAttributeArray
     * @param icoObj
     * @param attributeID
     */
    private void invokeUserService(String classIDValue, String[] selectedClassfnAttributes,
            String[] patternAttributeArray, TCComponent icoObj, String attributeID)
    {
        TCUserService userService = m_frClassfinCommand.getSession().getUserService();
        Object[] inputObjs = new Object[5];
        inputObjs[0] = classIDValue;
        inputObjs[1] = selectedClassfnAttributes;
        inputObjs[2] = patternAttributeArray;
        inputObjs[3] = icoObj;
        inputObjs[4] = attributeID;
        try
        {
            userService.call("NOV_buildAutoComputePattern", inputObjs);
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
        
    }
}
