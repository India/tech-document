package com.nov.rac.utilities.commands.classification.patternCreator;

import java.util.StringTokenizer;

import com.nov.rac.utilities.commands.classification.NOVClassificationConstants;
import com.teamcenter.rac.kernel.ics.ICSApplicationObject;
import com.teamcenter.rac.kernel.ics.ICSProperty;

/**
 * @author kabades Created On: Jan 2014
 */

public class PatternCreatorHelper
{
    /**
     * @param classfnAttributes
     * @return pattern array for the give attributes array I/P: inputeString
     *         ={"8001643$ x ","8001642$ ","8001437$-","8001438"} O/P:
     *         outputString[] = {" x "," ","-"}
     */
    public static String[] getPatterns(String[] classfnAttributes)
    {
        
        String[] patternArray = new String[classfnAttributes.length - 1];
        for (int j = 0; j < classfnAttributes.length - 1; j++)
        {
            String str = classfnAttributes[j];
            StringTokenizer strToken = new StringTokenizer(str, NOVClassificationConstants.DollarDelimiter);
            while (strToken.hasMoreElements())
            {
                patternArray[j] = strToken.nextToken().toString();
            }
        }
        return patternArray;
    }
    
    /**
     * @param classfnAttributes
     * @return Attribute Ids
     */
    public static String[] getAttributeIds(String classfnAttributes[])
    {
        String[] attributeIDArray = new String[classfnAttributes.length];
        for (int i = 0; i < classfnAttributes.length; i++)
        {
            StringTokenizer token = new StringTokenizer(classfnAttributes[i],
                    NOVClassificationConstants.DollarDelimiter);
            if (token != null)
            {
                attributeIDArray[i] = token.nextToken();
            }
        }
        
        return attributeIDArray;
    }
    
    /**
     * @param prefValue
     * @param classID
     * @return true if match found Returns false if match not found
     */
    public static boolean isClassIDMatchesWithPrefValue(String prefValue, String classID)
    {
        boolean isMatch = false;
        
        String classIdFromPref = prefValue.substring(0, prefValue.indexOf(NOVClassificationConstants.TildeDelimiter));
        if (classIdFromPref.equalsIgnoreCase(classID))
        {
            isMatch = true;
        }
        return isMatch;
    }
    
    /**
     * @param prefValue
     * @return string Array from input prefValue.
     */
    public static String[] fetchTokenArray(String prefValue)// TCDECREL-7976
    {
        
        String[] tokenArray = null;
        if (prefValue != null)
        {
            StringTokenizer strToken = new StringTokenizer(prefValue, NOVClassificationConstants.HashDelimiter);
            tokenArray = new String[strToken.countTokens()];
            int counter = 0;
            while (strToken.hasMoreTokens())
            {
                tokenArray[counter] = strToken.nextToken();
                counter++;
            }
        }
        
        return tokenArray;
        
    }
    
    /**
     * @param mactchedPrefRow
     * @return Attribute values associated with pattern and filtering ClassID
     *         from the preference value I/P: inputeString =
     *         "DHT06024002~8001643$ x ~8001642$ ~8001437$-~8001438" 
     *         O/P: outputString[] =
     *         {"8001643$ x ","8001642$ ","8001437$-","8001438"}
     */
    public static String[] getSelectedClassId(String mactchedPrefRow)
    {
        
        mactchedPrefRow = mactchedPrefRow.substring(
                mactchedPrefRow.indexOf(NOVClassificationConstants.TildeDelimiter, 0) + 1, mactchedPrefRow.length());
        
        StringTokenizer stringTokenizer = new StringTokenizer(mactchedPrefRow,
                NOVClassificationConstants.TildeDelimiter);
        String[] tokenArray = new String[stringTokenizer.countTokens()];
        
        int counter = 0;
        while (stringTokenizer.hasMoreTokens())
        {
            tokenArray[counter] = stringTokenizer.nextToken();
            counter++;
        }
        return tokenArray;
    }
    
    /**
     * @param icsApplicationObj
     * @param attributeId
     * @return AttributeId value
     */
    public static String fetchAttributeValueofStrType(ICSApplicationObject icsApplicationObj, int attributeId)
    {
        
        ICSProperty[] props = icsApplicationObj.getProperties();
        String keyString = null;
        for (int i = 0; i < props.length; i++)
        {
            
            if (props[i].getId() == attributeId)
            {
                
                String[] strKey = props[i].getValues();
                if (strKey != null)
                {
                    keyString = strKey[0];
                }
                break;
            }
        }
        return keyString;
    }
    
    /**
     * @param type
     * @param tokenArray
     * @return fetches the tokenString based on i/p type that matches from the
     *         given tokenArray
     */
    public static String findTokenString(String type, String[] tokenArray)// TCDECREL-7976
    {
        
        String tokenString = null;
        String filteredTokenString = null;
        for (int i = 0; i < tokenArray.length; i++)
        {
            if (tokenArray[i].contains(type))
            {
                tokenString = tokenArray[i];
                break;
            }
            
        }
        int index = tokenString.length() - type.length() - 1;
        filteredTokenString = tokenString.substring(0, index);
        return filteredTokenString;
    }
    
    public static boolean isStringMatches(String attributeValue, String[] specialCharStringArray)
    {
        boolean isMatched = false;
        for (String specialCharString : specialCharStringArray)
        {
            if (attributeValue.equalsIgnoreCase(specialCharString))
            {
                isMatched = true;
                break;
            }
        }
        return isMatched;
        
    }
}
