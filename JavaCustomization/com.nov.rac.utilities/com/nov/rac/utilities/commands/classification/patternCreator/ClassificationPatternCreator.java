package com.nov.rac.utilities.commands.classification.patternCreator;

import com.nov.rac.utilities.commands.classification.NOVClassificationCommand;
import com.nov.rac.utilities.commands.classification.NOVClassificationConstants;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.kernel.ics.ICSApplicationObject;
import com.teamcenter.rac.kernel.ics.ICSKeyLov;
import com.teamcenter.rac.kernel.ics.ICSKeyLov.KeyLov;
import com.teamcenter.rac.kernel.ics.ICSPropertyDescription;
import com.teamcenter.rac.kernel.ics.ICSView;
import com.teamcenter.rac.util.Registry;

/**
 * @author kabades created on Jan 2014
 */

public class ClassificationPatternCreator
{
    private NOVClassificationCommand m_classfinCommand = null;
    private static String m_autoComputePrefValues[] = null;
    private Registry m_registry;
    private String m_classIDValue = null;
    private ICSApplicationObject m_icsApplicationObj = null;
    private TCComponent m_icoObj = null;
    
    public ClassificationPatternCreator(NOVClassificationCommand classfinCommand, String classIDValue,
            ICSApplicationObject icsApplicationObj, TCComponent icoObj)
    {
        this.m_classfinCommand = classfinCommand;
        this.m_classIDValue = classIDValue;
        this.m_icsApplicationObj = icsApplicationObj;
        this.m_icoObj = icoObj;
        m_registry = Registry.getRegistry("com.nov.rac.utilities.commands.classification.classification");
    }
    
    public void buildClassfnPattern()
    {
        String[] selectedClassfnAttributes = null;
        String[] patternArray = null;
        String[] attrIdArr = null;
        String attributevalue = m_classfinCommand.m_attributeID;
        selectedClassfnAttributes = getClassificationAttributes();
        
        if (selectedClassfnAttributes != null)
        {
            patternArray = PatternCreatorHelper.getPatterns(selectedClassfnAttributes);
            attrIdArr = PatternCreatorHelper.getAttributeIds(selectedClassfnAttributes);
            invokeUserService(m_classIDValue, attrIdArr, patternArray, m_icoObj, attributevalue);
        }
    }
    
    private String[] getClassificationAttributes()
    {
        String[] classfnAttributes = null;
        if (m_classIDValue.equalsIgnoreCase(NOVClassificationConstants.HOLEOPENER_CLASS))
        {
            classfnAttributes = getExpressionForHoleOpener(m_icsApplicationObj, m_classIDValue);
        }
        else
        {
            classfnAttributes = fetchClassificationAttrFromPref(m_classIDValue);
        }
        return classfnAttributes;
    }
    
    /**
     * @param classIDValue
     * @param selectedClassfnAttributes
     * @param patternAttributeArray
     * @param icoObj
     * @param attributeID
     */
    private void invokeUserService(String classIDValue, String[] selectedClassfnAttributes,
            String[] patternAttributeArray, TCComponent icoObj, String attributeID)
    {
        
        TCUserService userService = m_classfinCommand.getSession().getUserService();
        Object[] inputObjs = new Object[5];
        inputObjs[0] = classIDValue;
        inputObjs[1] = selectedClassfnAttributes;
        inputObjs[2] = patternAttributeArray;
        inputObjs[3] = icoObj;
        inputObjs[4] = attributeID;
        try
        {
            userService.call("NOV_buildAutoComputePattern", inputObjs);
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
        
    }
    
    /**
     * @param classid
     * @return fetches classification attributes for the i/p classID
     */
    private String[] fetchClassificationAttrFromPref(String classid)
    {
        String[] selectedClassIDRow = null;
        selectedClassIDRow = PatternCreatorHelper.getSelectedClassId(fetchPrefValue(classid));
        return selectedClassIDRow;
    }
    
    /**
     * @param icsApplicationObj
     * @param classIDValue
     * @return Gives expression for specific classID
     */
    private String[] getExpressionForHoleOpener(ICSApplicationObject icsApplicationObj, String classIDValue)
    {
        String classType = null;
        String[] attributeIdArray = null;
        classType = fetchHoleOpenerClassType(icsApplicationObj);// KLOC2123
        if (classType != null)
        {
            attributeIdArray = fetchAttributeArray(classType, classIDValue);
        }
        
        return attributeIdArray;
    }
    
    /**
     * @param icsApplicationObj
     * @return Type(Holeopener) is fetched from the given icsApplicationObj,
     */
    private String fetchHoleOpenerClassType(ICSApplicationObject icsApplicationObj)// KLOC2123
    {
        String keyString = null;
        String classType = null;
        keyString = PatternCreatorHelper.fetchAttributeValueofStrType(icsApplicationObj,
                NOVClassificationConstants.HOLEOPNERTYPE_ID);
        ICSView icsView = icsApplicationObj.getView();
        ICSPropertyDescription icsPropDesc[] = icsView.getPropertyDescriptions();
        for (int i = 0; i < icsPropDesc.length; i++)
        {
            if (icsPropDesc[i].getId() == NOVClassificationConstants.HOLEOPNERTYPE_ID)
            {
                ICSKeyLov icsKeyLOV = icsPropDesc[i].getNonMetricFormat().getKeyLov();
                if (icsKeyLOV != null)
                {
                    KeyLov kelyLov = icsKeyLOV.getKeyLovOfKey(keyString);
                    if (kelyLov != null)
                    {
                        classType = kelyLov.getDisplayValue();
                    }
                    
                }
                break;
            }
        }
        return classType;
        
    }
    
    /**
     * @param classType
     * @param classIDValue
     * @return Building expression based on selected type
     */
    private String buildHoleOpenerExpressionType(String classType, String classIDValue)
    {
        String prefValue = fetchPrefValue(classIDValue);
        String[] tokenArrValue = PatternCreatorHelper.fetchTokenArray(prefValue);
        String tokenString = PatternCreatorHelper.findTokenString(classType, tokenArrValue);
        
        return tokenString;
    }
    
    /**
     * @param classId
     * @return preference value for the i/p classId
     */
    private String fetchPrefValue(String classId)
    {
        if (m_autoComputePrefValues == null)
        {
            m_autoComputePrefValues = m_classfinCommand.getPreferenceService().getStringArray(
                    TCPreferenceService.TC_preference_site, m_registry.getString("NOV4_AUTOCOMPUTE_PREF"));
        }
        String matchingClassIDExprssion = null;
        
        for (String autoComputePrefValue : m_autoComputePrefValues)
        {
            if (PatternCreatorHelper.isClassIDMatchesWithPrefValue(autoComputePrefValue, classId))
            {
                matchingClassIDExprssion = autoComputePrefValue;
                break;
            }
            
        }
        return matchingClassIDExprssion;
        
    }
    
    /**
     * @param classType
     * @param classIDValue
     * @return fetches matchedPrefValue based on classType, classIDValue exps
     */
    private String[] fetchAttributeArray(String classType, String classIDValue)
    {
        String[] classIDsAttributes = null;
        String expressionString = buildHoleOpenerExpressionType(classType, classIDValue);
        classIDsAttributes = PatternCreatorHelper.getSelectedClassId(expressionString);
        
        return classIDsAttributes;
        
    }
    
}
