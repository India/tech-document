package com.nov.rac.utilities.commands.classification;

public class NOVClassificationConstants
{
    public static final String TildeDelimiter = "~";
    public static final String DollarDelimiter = "$";
    public static final String HashDelimiter = "#";
    public static final Integer HOLEOPNERTYPE_ID = 8001651;
    public static final String HOLEOPENER_CLASS = "DHT06024001";
    public static final Integer PITCH_ATTRIBUTE_ID =  7000403;
    public static final String DashCharacter = "-";
    public static final String ISO_STANDARD = "ISO standard";
    
}
