package com.nov.rac.utilities.utils;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class GeneralItemUtils
{
    private static final String DASH_SYMBOL = "-";
    
    public static String removeDashnumberFromItemId(String itemId)
    {
        String modifiedId = null;
        if(itemId!=null && itemId.contains(DASH_SYMBOL))
        {
            modifiedId = itemId.split(DASH_SYMBOL)[0];
        }
        
        return modifiedId;
    }
    
    public static TCComponentItem getItemFromTCComponent(TCComponent component)
    {
        TCComponentItem targetItem = null;
        try
        {
            if (component instanceof TCComponentItem)
            {
                targetItem = (TCComponentItem) component;
            }
            else if (component instanceof TCComponentItemRevision)
            {
                targetItem = ((TCComponentItemRevision) component).getItem();
            }
        }
        catch(TCException ex)
        {
            ex.printStackTrace();
        }

        return targetItem;
    }
    
    
}
