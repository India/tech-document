package com.nov.rac.utilities.utils;

import java.awt.Frame;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

public class MessageBox								//    Vivek
{
    public static final int INFORMATION = MessageBoxOptions.ENUM_INFORMATION.getValue(); //JOptionPane.INFORMATION_MESSAGE;
    public static final int WARNING = MessageBoxOptions.ENUM_WARNING.getValue(); //JOptionPane.WARNING_MESSAGE;
    public static final int ERROR = MessageBoxOptions.ENUM_ERROR.getValue(); //JOptionPane.ERROR_MESSAGE;
    public static final int CONFIRM = MessageBoxOptions.ENUM_CONFIRM.getValue();//JOptionPane.YES_NO_OPTION;
    public static final int CONFIRM_YES_NO = MessageBoxOptions.ENUM_CONFIRM_YES_NO.getValue(); //JOptionPane.YES_NO_OPTION;
    public static final int CONFIRM_OK_CANCEL = MessageBoxOptions.ENUM_CONFIRM_OK_CANCEL.getValue(); //JOptionPane.OK_CANCEL_OPTION;
    
    private static Map<Integer, Integer> m_msgTypeToJOptionType = new HashMap<Integer, Integer> ();
    
    // Map all Msg Types to corresponding JOptions Dialog type.
    static {
    	
    	m_msgTypeToJOptionType.put( new Integer(INFORMATION), new Integer(JOptionPane.INFORMATION_MESSAGE));
    	m_msgTypeToJOptionType.put( new Integer(WARNING), new Integer(JOptionPane.WARNING_MESSAGE));
    	m_msgTypeToJOptionType.put( new Integer(ERROR), new Integer(JOptionPane.ERROR_MESSAGE));
    	m_msgTypeToJOptionType.put( new Integer(CONFIRM), new Integer(JOptionPane.YES_NO_OPTION));
    	m_msgTypeToJOptionType.put( new Integer(CONFIRM_YES_NO), new Integer(JOptionPane.YES_NO_OPTION));
    	m_msgTypeToJOptionType.put( new Integer(CONFIRM_OK_CANCEL), new Integer(JOptionPane.OK_CANCEL_OPTION));
    	
    }
    
    // Result Values
    public static final int DEFAULT_OPTION = -1;
    public static final int YES_OPTION = 0;
    public static final int NO_OPTION = 1;
    public static final int CANCEL_OPTION = 2;
    public static final int OK_OPTION = 0;
    public static final int CLOSED_OPTION = -1;
    
    private int m_result =-1;
    
    public MessageBox( Frame parent, String msgBoxTitle, Object message, int type )
    {
		createMessageBox( parent, msgBoxTitle, message, type );
    }
    
    public MessageBox( Window parent, String msgBoxTitle, Object message, int type )
    {
		createMessageBox( parent, msgBoxTitle, message, type );
    }
    
   
    private void createMessageBox(Frame parent, String msgBoxTitle,
            Object message, int type) 
    {
    	//JOptionPane.showMessageDialog(parent, message, msgBoxTitle, type);
    	createMessageBox( parent, msgBoxTitle, message, type );
    }

	public static MessageBox post( Frame window, String msgBoxTitle, Object message, int i )
    {
    	MessageBox messagebox = new MessageBox( window, msgBoxTitle,  message, i );
        return messagebox;
    }
    
    public static MessageBox post( Window window, String msgBoxTitle, Object message, int i )
    {
    	MessageBox messagebox = new MessageBox( window, msgBoxTitle,  message, i );
        return messagebox;
    }
    
    
    private void createMessageBox( Window parent, String msgBoxTitle, Object message, int type )
    {
    	// Get the JOptionType from Map for given type
    	int jOptionsType = m_msgTypeToJOptionType.get(type);
    	 	
    	if(type == INFORMATION || type == WARNING || type == ERROR)
    	{
    		
    	    JOptionPane.showMessageDialog(parent, message, msgBoxTitle, jOptionsType);
    	}
    	else if (type == CONFIRM || type == CONFIRM_YES_NO || type == CONFIRM_OK_CANCEL )
    	{
    		m_result = JOptionPane.showConfirmDialog(parent, message, msgBoxTitle, jOptionsType);
    	}
    }  
    
    public int getValue()
    {
    	return m_result;
    }

	private enum MessageBoxOptions
    {
		ENUM_ERROR(0),
		ENUM_INFORMATION(1),
		ENUM_WARNING(2),
		ENUM_CONFIRM(3),
		ENUM_CONFIRM_YES_NO(3),
		ENUM_CONFIRM_OK_CANCEL(4);
		
          private int m_value = 0;
          
          MessageBoxOptions(int option)
          {
        	  m_value = option;
          }
          
          public int getValue()
          {
          	return m_value;
          }
          
    };

}
