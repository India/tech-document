package com.nov.rac.utilities.utils;
import java.util.Vector;

import com.teamcenter.rac.util.InterfaceExceptionStack;

public class ExceptionStack implements InterfaceExceptionStack 
{
	public ExceptionStack()
	{
		errorCodeCount = 0;
		errorSeverities = new Vector<Integer>();
		errorCodes = new Vector<Integer> ();
		errorStrings = new Vector<String> ();
	}
	
	public ExceptionStack(int[] theErrorCodes, int [] theErrorSeverities, String [] theErrorStrings  )
	{
		this();
		addErrors( theErrorCodes, theErrorSeverities, theErrorStrings);
	}
	
	public ExceptionStack(InterfaceExceptionStack theStack)
	{
		this();
		addErrors(theStack);
		//this(theStack.getErrorCodes(), theStack.getErrorSeverities(), theStack.getErrorStack());	
	}
	

	public int[] getErrorCodes()
	{
		Integer [] intArr = new Integer[1];
		intArr = errorCodes.toArray(intArr);
		
		int [] inArray = new int [intArr.length];
		
		for(int inx=0; inx <intArr.length; inx++)
		{
			inArray[inx] = intArr[inx].intValue();
		}
		
		return inArray;
	}

	public int[] getErrorSeverities()
	{
		Integer [] intArr = new Integer[1];
		intArr = errorSeverities.toArray(intArr);
		
		int [] inArray = new int [intArr.length];
		
		for(int inx=0; inx <intArr.length; inx++)
		{
			inArray[inx] = intArr[inx].intValue();
		}
		
		return inArray;	
	}

	public String[] getErrorStack()
	{
		String [] strArr = new String[1];
		
		strArr = errorStrings.toArray(strArr);
		
		return strArr;
	}
	
	public int getErrorCount()
	{
		return errorCodeCount;
	}
	
	public void addErrors(InterfaceExceptionStack theStack)
	{
		int[] theErrorCodes = theStack.getErrorCodes(); 
		int [] theErrorSeverities = theStack.getErrorSeverities(); 
		String [] theErrorStrings = theStack.getErrorStack();
		
		addErrors( theErrorCodes, theErrorSeverities, theErrorStrings);
	}
	
	public void addErrors(int[] theErrorCodes, int [] theErrorSeverities, String [] theErrorStrings)
	{
		for(int inx=0; inx<theErrorCodes.length; inx++)
		{
		    errorSeverities.add( new Integer(theErrorSeverities[inx]) ); 
		    errorCodes.add(new Integer(theErrorCodes[inx]));
		    errorStrings.add(theErrorStrings[inx]);
		}
		
		errorCodeCount = errorCodes.size();
	}
	
	public void clear()
	{
	    errorCodeCount = 0;
        errorSeverities.clear();
        errorCodes.clear();
        errorStrings.clear();
	}
	
	/****************************************************/
	private int errorCodeCount;
	//private int errorSeverities[]; 
	private Vector<Integer> errorSeverities;
	private Vector<Integer> errorCodes;
	private Vector<String> errorStrings;

}