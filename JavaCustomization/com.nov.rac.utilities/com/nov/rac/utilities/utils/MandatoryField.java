package com.nov.rac.utilities.utils;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Control;

import com.teamcenter.rac.util.Registry;

public class MandatoryField {
	private static final MandatoryField instance = new MandatoryField();

	private MandatoryField() {
		Registry registry = Registry
				.getRegistry("com.nov.rac.utilities.utils.utils");
		Image reqImage = registry.getImage("requiredFieldImage.ICON");
		FieldDecorationRegistry.getDefault().registerFieldDecoration(
				"NOV_DEC_REQUIRED", "Field is mandatory", reqImage);
	}

	public static MandatoryField getInstance() {
		return instance;
	}

	public static void markMandatoryField(Control control) {
		MandatoryField mandatoryField = getInstance();
		FieldDecoration reqFieldIndicator = FieldDecorationRegistry
				.getDefault().getFieldDecoration("NOV_DEC_REQUIRED");

		ControlDecoration decIDText = new ControlDecoration(control, SWT.TOP
				| SWT.RIGHT | SWT.EMBEDDED);

		decIDText.setImage(reqFieldIndicator.getImage());
		decIDText.setDescriptionText(reqFieldIndicator.getDescription());
	}

}





