package com.nov.rac.utilities.utils;

import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.IControlContentAdapter2;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Button;

public class ButtonContentAdapter implements IControlContentAdapter, IControlContentAdapter2 {

	@Override
	public String getControlContents(Control control) 
	{
		String contents = ((Button)control).getText();
		
		return contents;
	}
	
	@Override
	public void setControlContents(Control control, String text, int cursorPosition) 
	{
		((Button)control).setText(text);
	}

	@Override
	public int getCursorPosition(Control control) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Rectangle getInsertionBounds(Control control) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertControlContents(Control control, String text, int cursorPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCursorPosition(Control control, int cursorPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public Point getSelection(Control control) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSelection(Control control, Point point) {
		// TODO Auto-generated method stub
		
	}
}
