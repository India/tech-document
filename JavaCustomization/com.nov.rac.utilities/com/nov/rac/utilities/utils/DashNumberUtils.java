package com.nov.rac.utilities.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

public  class DashNumberUtils {
	
	
	public static Vector<String> sortDashNumbers(Vector<String> dashNos) {
		
		Map<Integer,String> dashNoMap = new TreeMap<Integer, String>();
    	for (int inx = 0; inx< dashNos.size();inx++)
    	{
       		dashNoMap.put(Integer.parseInt((String) dashNos.elementAt(inx)),(String) dashNos.elementAt(inx));
    	}
    	
        Vector<String> dashNosSorted = new Vector<String>(dashNoMap.values());
		// TODO Auto-generated method stub
		return dashNosSorted;

	}
	
	public static Object[] sortDashNumbers(Object[] dashNos) {
		
		Map<Integer,String> dashNoMap = new TreeMap<Integer, String>();
    	for (int inx = 0; inx< dashNos.length;inx++)
    	{
       		dashNoMap.put(Integer.parseInt((String) dashNos[inx]),(String) dashNos[inx]);
    	}
    	Object[] dashNosSorted =dashNoMap.values().toArray();
        //Vector<String> dashNosSorted = new Vector<String>(dashNoMap.values());
		// TODO Auto-generated method stub
		return dashNosSorted;

	}

}
