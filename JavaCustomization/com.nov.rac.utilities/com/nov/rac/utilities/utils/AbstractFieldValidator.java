package com.nov.rac.utilities.utils;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;

/**
 * The Class AbstractFieldValidator.
 *
 * @param <T> the generic type
 */
public abstract class AbstractFieldValidator<T> implements IFieldValidator<T>
{
    
    /** The error message. */
    private final String errorMessage;
    
    /** The warning message. */
    private final String warningMessage;
    
    /**
     * Instantiates a new abstract field validator.
     *
     * @param errorMessage the error message
     * @param warningMessage the warning message
     */
    public AbstractFieldValidator(String errorMessage, String warningMessage)
    {
        this.errorMessage = errorMessage;
        this.warningMessage = warningMessage;
    }
    

    /* (non-Javadoc)
     * @see com.richclientgui.toolbox.validation.validator.IFieldValidator#getErrorMessage()
     */
    @Override
    public String getErrorMessage()
    {
        return errorMessage;
    }

    /* (non-Javadoc)
     * @see com.richclientgui.toolbox.validation.validator.IFieldValidator#getWarningMessage()
     */
    @Override
    public String getWarningMessage()
    {
        return warningMessage;
    }
}
