/**
 * @author sachinkab
 * Created On: Dec 2013
 */
package com.nov.rac.utilities.utils;

import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class NOVFindObjectHelper
{
    
    public static TCComponentItemRevision findRDDforItem(TCComponent itemRev)
    {
        TCComponentItemRevision rddDocRev = null;
        AIFComponentContext[] comps;
        try
        {
            comps = itemRev.getSecondary();
            
            for (int j = 0; j < comps.length; j++)
            {
                if (comps[j].getContext().equals("RelatedDefiningDocument"))
                {
                    InterfaceAIFComponent infDocItem = comps[j].getComponent();
                    if (infDocItem instanceof TCComponentItem && infDocItem.getType().equalsIgnoreCase("Documents"))
                    {
                        rddDocRev = ((TCComponentItem) infDocItem).getLatestItemRevision();
                        break;
                    }
                }
            }
            
        }
        catch (TCException e)
        {
            
            e.printStackTrace();
        }
        return rddDocRev;
    }
    
}
