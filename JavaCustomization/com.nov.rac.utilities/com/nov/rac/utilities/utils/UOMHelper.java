package com.nov.rac.utilities.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class UOMHelper
{
    private static final UOMHelper instance = new UOMHelper();
    
    private UOMHelper()
    {
        //String[] uomValue;
        UOMMap = new HashMap<String, TCComponent>();
        m_unitToUOM = new HashMap<String, TCComponent>();
        
        TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
        TCComponentType UOMType;
        try
        {
            UOMType = session.getTypeService().getTypeComponent("UnitOfMeasure");
            TCComponent[] uomList = UOMType.extent();
            
            String[][] uomValue = UOMType.getPropertiesSet(Arrays.asList(uomList), new String[] { "symbol", "unit"});            
            
            for (int index = 0; index <uomList.length; index++)
            {                
                UOMMap.put(uomValue[index][0], uomList[index]);
                m_unitToUOM.put(uomValue[index][1], uomList[index]);
            }
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
    public static UOMHelper getInstance()
    {
        return instance;
    }
    
    public static TCComponent getUOMObject(String uomSymbol) throws TCException
    {
        UOMHelper loadUOMMap = getInstance();
        return loadUOMMap.UOMMap.get(uomSymbol);
        
    }
    
    public static TCComponent getUOMObjectForUnit(String uomUnit) throws TCException
    {
        UOMHelper loadUOMMap = getInstance();
        return loadUOMMap.m_unitToUOM.get(uomUnit);
        
    }
    
    private Map<String, TCComponent> UOMMap ;
    private Map<String, TCComponent> m_unitToUOM ;
    
}
