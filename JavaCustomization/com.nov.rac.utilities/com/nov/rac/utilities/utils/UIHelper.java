package com.nov.rac.utilities.utils;

import java.awt.Font;
import java.util.Collection;
import java.util.Map;
import java.util.Vector;
import java.util.WeakHashMap;

import javax.swing.JComponent;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.teamcenter.rac.util.MessageBox;

public class UIHelper
{
    /** The Constant DEC_REQUIRED_NOV_MANDATORY. */
    public static final String DEC_REQUIRED_NOV_MANDATORY = "NOV_DEC_REQUIRED";
    
    /** The Constant DEC_REQUIRED_NOV_MANDATORY_MESSAGE. */
    public static final String DEC_REQUIRED_NOV_MANDATORY_MESSAGE = "Field is mandatory";
    
    /** The Constant DECORATOR_POSITION. */
    private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT | SWT.EMBEDDED;
    
    /** The Constant DECORATOR_MARGIN_WIDTH. */
    private static final int DECORATOR_MARGIN_WIDTH = 1;
    
    private static final CustomValidationToolkit CUSTOM_VALIDATION_TOOLKIT = new CustomValidationToolkit(
            DECORATOR_POSITION, DECORATOR_MARGIN_WIDTH, true);
    
    private static final int KEY_EVENT_CTRL_A = 97;
    
    private static Map<Control, CustomValidatingField> m_validatingFieldMap = new WeakHashMap<Control, CustomValidatingField>();
    
    public static void setAutoComboArray(Combo m_Combo, String[] values)
    {
        if (m_Combo != null && values.length > 0)
        {
            m_Combo.setItems(values);
            
            new NovAutoCompleteField(m_Combo, values);
        }
    }
        
    public static void setFontStyle(JComponent component, int fontStyle)
    {
        Font newLabelFont = new Font(component.getFont().getName(), fontStyle, component.getFont().getSize());
        component.setFont(newLabelFont);
    }
    
    public static void setAutoComboArray(Combo m_Combo, Vector<String> templateNames)
    {
        String[] values = templateNames.toArray(new String[templateNames.size()]);
        if (m_Combo != null && values.length > 0)
        {
            m_Combo.setItems(values);
            
            new NovAutoCompleteField(m_Combo, values);
        }
    }
    
    public static void convertToUpperCase(Text text)
    {
        text.addVerifyListener(new VerifyListener()
        {
            public void verifyText(VerifyEvent e)
            {
                e.text = e.text.toUpperCase();
            }
        });
    }
    
    public static boolean getBoolean(String yesNo)
    {
        boolean flag = false;
        if (yesNo.equalsIgnoreCase("yes"))
        {
            flag = true;
        }
        return flag;
    }
    
    public static void setAutoComboCollection(Combo cmb, Collection vector)
    {
        if (cmb != null && vector != null && vector.size() > 0)
        {
            int vecSize = vector.size();
            String[] strArrLOVValues = new String[vecSize];
            int inx = 0;
            for (Object object : vector)
            {
                strArrLOVValues[inx] = (String) object;
                inx++;
            }
            if (strArrLOVValues != null)
            {
                cmb.setItems(strArrLOVValues);
                new NovAutoCompleteField(cmb, strArrLOVValues);
            }
        }
    }
    
    public static CustomValidatingField makeMandatory(Control field, IFieldValidator fieldValidator, 
                                                      boolean isRequired, Object intialValue)
    {
        CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(field))
        {
            customValidatingField = m_validatingFieldMap.get(field);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(field, fieldValidator, isRequired, intialValue);
            
            addFieldToValidationMap(field, customValidatingField);
        }
        
        return customValidatingField;
    }
    
    /**
     * Make mandatory - Validates based on fieldValidator and Decorates label
     * with default control decoration
     * 
     * @param label
     *            the label
     * @return the custom validating field
     */
    
    public static CustomValidatingField makeMandatory(Label label, IFieldValidator fieldValidator)
    {
/*        CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(label))
        {
            customValidatingField = m_validatingFieldMap.get(label);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(label, fieldValidator, true, "");
            
            addFieldToValidationMap(label, customValidatingField);
        }
        
        return customValidatingField;*/
        
        return makeMandatory(label, fieldValidator, true, "");
    }
    
    /**
     * Make mandatory - Validates the text contents and Decorates Text with
     * default control decoration
     * 
     * @param textField
     *            the text field
     * @return the custom validating field
     */
    
    public static CustomValidatingField makeMandatory(Text textField)
    {
/*        CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(textField))
        {
            customValidatingField = m_validatingFieldMap.get(textField);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(textField,
                    new MandatoryStringFieldValidator(), true, "");
            
            addFieldToValidationMap(textField, customValidatingField);
        }
        
        return customValidatingField;*/
        
        return makeMandatory(textField, new MandatoryStringFieldValidator(), true, "");
    }
    
    /**
     * Make mandatory - Validates the combo contents and Decorates combo with
     * default control decoration
     * 
     * @param combo
     *            the combo
     * 
     * @return the custom validating field
     */
    
    public static CustomValidatingField makeMandatory(Combo combo)
    {
  /*      CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(combo))
        {
            customValidatingField = m_validatingFieldMap.get(combo);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(combo, new TextComboFieldValidator(),
                    true, "");
            
            addFieldToValidationMap(combo, customValidatingField);
        }
        return customValidatingField;*/
        
        return makeMandatory(combo, new TextComboFieldValidator(), true, "");
    }
    
    /**
     * Make mandatory - Validates the combo contents and Decorates combo with
     * default control decoration
     * 
     * @param combo
     *            the combo
     * @param boolean isSelectionMandatory - true if combo is non-editable(i.e.
     *        only selection is possible in combo)
     * 
     * @return the custom validating field
     */
    
    public static CustomValidatingField makeMandatory(Combo combo, boolean isSelectionMandatory)
    {
/*        CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(combo))
        {
            customValidatingField = m_validatingFieldMap.get(combo);
            customValidatingField.validate();
        }
        
        else if (isSelectionMandatory)
        {
            customValidatingField = makeMandatory(combo);
        }
        
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(combo, new TextComboFieldValidator(),
                    true, "");
            
            addFieldToValidationMap(combo, customValidatingField);
        }
        
        return customValidatingField;*/
        
        CustomValidatingField customValidatingField = null;
        
        if (isSelectionMandatory)
        {
            customValidatingField = makeMandatory(combo);
        }
        else
        {
            customValidatingField = makeMandatory(combo, new TextComboFieldValidator(), true, "");
        }
        
        return customValidatingField;
    }
    
    /**
     * Make mandatory - Validates the cCombo contents and Decorates CCombo with
     * default control decoration
     * 
     * @param cCombo
     *            the CCombo
     * @return the custom validating field
     */
    public static CustomValidatingField makeMandatory(CCombo cCombo)
    {
/*        CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(cCombo))
        {
            customValidatingField = m_validatingFieldMap.get(cCombo);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(cCombo, new CComboFieldValidator(cCombo),
                    true, "");
            
            addFieldToValidationMap(cCombo, customValidatingField);
        }
        
        return customValidatingField;*/
        
        return makeMandatory(cCombo, new CComboFieldValidator(cCombo), true, "");
    }
    
    /**
     * Make mandatory - Validates the button text and Decorates button with
     * default control decoration
     * 
     * @param button
     *            the button
     * @return the custom validating field
     */
    
    public static CustomValidatingField makeMandatory(final Button button)
    {
 /*       CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(button))
        {
            customValidatingField = m_validatingFieldMap.get(button);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(button, new MandatoryStringFieldValidator(),
                    true, "");
            
            addFieldToValidationMap(button, customValidatingField);
        }
        
        return customValidatingField;*/
        
        return makeMandatory(button, new MandatoryStringFieldValidator(), true, "");
    }
    
    /**
     * Make mandatory - Validates the list contents and Decorates list with
     * default control decoration
     * 
     * @param list
     *            the list
     * @return the custom validating field
     */
    
    public static CustomValidatingField makeMandatory(final List list)
    {
/*        CustomValidatingField customValidatingField = null;
        
        if (m_validatingFieldMap.containsKey(list))
        {
            customValidatingField = m_validatingFieldMap.get(list);
            customValidatingField.validate();
        }
        else
        {
            customValidatingField = CUSTOM_VALIDATION_TOOLKIT.createField(list, new MandatoryStringFieldValidator(),
                    true, "");
            
            addFieldToValidationMap(list, customValidatingField);
        }
        
        return customValidatingField;*/
        
        return makeMandatory(list,new MandatoryStringFieldValidator(), true, "");
    }
    
    /**
     * Make mandatory - Decorates Composite with control decoration
     * 
     * @param composite
     *            the composite
     * @return
     */
    
    public static void makeMandatory(final Composite composite)
    {
        final ControlDecoration controlDecoration = new ControlDecoration(composite, DECORATOR_POSITION);
        
        controlDecoration.setDescriptionText(DEC_REQUIRED_NOV_MANDATORY_MESSAGE);
        
        addErrorDecoration(controlDecoration);
    }
    
    /**
     * Make optional - Hides control decoration of control in the argument
     * 
     * @param field
     *            the field
     * @return
     */
    
    public static void makeOptional(Control field)
    {
        if (m_validatingFieldMap.containsKey(field))
        {
            CustomValidatingField customValidatingField = m_validatingFieldMap.get(field);
            customValidatingField.dispose();
            
            m_validatingFieldMap.remove(field);
        }
    }
    
    private static void addFieldToValidationMap(Control control, CustomValidatingField customValidatingField)
    {
        if (customValidatingField != null)
        {
            m_validatingFieldMap.put(control, customValidatingField);
        }
    }
    
    private static void addErrorDecoration(ControlDecoration controlDecoration)
    {
        
        FieldDecoration fieldDecoration = FieldDecorationRegistry.getDefault().getFieldDecoration(
                FieldDecorationRegistry.DEC_ERROR);
        controlDecoration.setImage(fieldDecoration.getImage());
    }
    
    /**
     * limitCharCountInTextWithPopUpMsg - Limits character count in text to
     * count given in an argument display pop up message if message string is
     * provided in argument
     * 
     * @param text
     *            the text
     * 
     * @param noOfCharsAllowed
     *            the count
     * 
     * @param tedisplayMsg
     *            the message string
     * 
     * @return
     */
    
    public static void limitCharCountInTextWithPopUpMsg(final Text theText, final int noOfCharsAllowed,
            final String displayMsg)
    {
        theText.addVerifyListener(new VerifyListener()
        {
            @Override
            public void verifyText(VerifyEvent event)
            {
                if ((theText.getCharCount() == noOfCharsAllowed) && theText.getSelectionText().isEmpty())
                {
                    event.doit = false;
                    if (null != displayMsg && !displayMsg.isEmpty())
                    {
                        MessageBox.post(displayMsg, "Error!", MessageBox.WARNING);
                    }
                }
                else
                {
                    event.doit = true;
                }
            }
        });
    }
    
    /**
     * enableSelectAll - Allows select all functionality in text provided in an
     * argument
     * 
     * @param text
     *            the text
     * 
     * @return
     */
    
    public static void enableSelectAll(final Text text)
    {
        text.addKeyListener(new KeyAdapter()
        {
            public void keyPressed(KeyEvent keyEvent)
            {
                // TODO Auto-generated method stub
                // detect Ctrl+A event (select all)
                if (keyEvent.stateMask == SWT.CTRL && keyEvent.keyCode == KEY_EVENT_CTRL_A)
                {
                    text.selectAll();
                }
            }
        });
    }
}
