package com.nov.rac.utilities.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2007_06.DataManagement.RelationAndTypesFilter;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsData2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsOutput2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsPref2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsResponse2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationship;
import com.teamcenter.soa.client.model.ErrorStack;


public class NOVGRMRelationsHelper 
{
	public static final int EXPAND_FOR_PRIMARY = 1;
	public static final int EXPAND_FOR_SECONDARY = 2;
	
	private Map<String, List<TCComponent>> m_relationNameToRelationObjectMap = new HashMap<String, List<TCComponent>>();
	private Map<String, List<TCComponent>> m_relationNameToRelatedObjectsMap = new HashMap<String, List<TCComponent>>();
	private TCSession session = (TCSession) AIFUtility.getDefaultSession();
	
	
	/**
	 * This operation finds the other side objects and Relation objects for input object when relation names are given.
	 * The other side objects can be related primary or secondary which is governed by expandOption.
	 * 
	 * @param inputObjects Input object for which we want to find other side objects
	 * @param relationNames Names of relation to traverse
	 * @param expandOption Option to expand for primary or secondary.
	 * @throws TCException with errors in serviceData
	 */
	
	public void expandGRMRelations(
            TCComponent inputObjects, String[] relationNames, int expandOption) throws TCException
    {
        ExpandGRMRelationsResponse2 response = null;
        
        DataManagementService dmService = DataManagementService
                .getService(session);
        
        if(expandOption == EXPAND_FOR_PRIMARY)
        {
        	response = dmService.expandGRMRelationsForPrimary (new TCComponent[]{inputObjects},
                		getExpandGRMRelationsInput(relationNames));
        }
        else if(expandOption == EXPAND_FOR_SECONDARY)
        {
        	response = dmService.expandGRMRelationsForSecondary (new TCComponent[]{inputObjects},
                		getExpandGRMRelationsInput(relationNames));
        }
        
        if(response != null)
        {
        	handleErrors(response.serviceData);
        	
        	getRelationAndOtherSideObjects(response);
        }
               
    }

	private void getRelationAndOtherSideObjects(ExpandGRMRelationsResponse2 response) 
	{
		ExpandGRMRelationsOutput2[] output = response.output;
        
        ExpandGRMRelationsData2[] relationData = output[0].relationshipData;
        
        for (ExpandGRMRelationsData2 expandRelationsData : relationData) 
        {
        	ExpandGRMRelationship[] relationshipObjects = expandRelationsData.relationshipObjects;
        	
        	List<TCComponent> tcComponentRelationObjects = new ArrayList<TCComponent>();
        	List<TCComponent> otherSideObjects = new ArrayList<TCComponent>();
            
            for (ExpandGRMRelationship relationshipObject : relationshipObjects)
            {
                tcComponentRelationObjects.add(relationshipObject.relation);
                otherSideObjects.add(relationshipObject.otherSideObject);
            }
            
            m_relationNameToRelationObjectMap.put(expandRelationsData.relationName, tcComponentRelationObjects);
            m_relationNameToRelatedObjectsMap.put(expandRelationsData.relationName, otherSideObjects);
		}
	}

	private ExpandGRMRelationsPref2 getExpandGRMRelationsInput(
			String[] relationNames) 
	{
		ExpandGRMRelationsPref2 expandGRMRelationsPref2 = new ExpandGRMRelationsPref2();
        expandGRMRelationsPref2.expItemRev = true;
        expandGRMRelationsPref2.returnRelations = true;
        expandGRMRelationsPref2.info = getRelationAndTypesFilter(relationNames);
        
		return expandGRMRelationsPref2;
	}

	private RelationAndTypesFilter[] getRelationAndTypesFilter(
			String[] relationNames) 
	{
		RelationAndTypesFilter[] relationAndTypesFilter = null;
		
		if(relationNames != null && relationNames.length > 0)
        {
			relationAndTypesFilter = new RelationAndTypesFilter[relationNames.length];
			
			for(int inx = 0; inx < relationNames.length; inx++)
			{
				 relationAndTypesFilter[inx] = new RelationAndTypesFilter();
			     relationAndTypesFilter[inx].relationTypeName = relationNames[inx];
			}
        }
		else
		{
			relationAndTypesFilter = new RelationAndTypesFilter[]{new RelationAndTypesFilter()};
		}
		
		
		return relationAndTypesFilter;
	}
	
	private void handleErrors(ServiceData serviceData) throws TCException 
	{
		if(serviceData.sizeOfPartialErrors() > 0)
        {
        	ErrorStack errorStack = serviceData.getPartialError(0);
			
			TCException tcExp = new TCException(errorStack.getLevels(), errorStack.getCodes(), errorStack.getMessages());
			
			throw tcExp;
        }
	}

	/**
	 * 
	 * @return The map of input Relation name and corresponding list of ImanRelation objects
	 */
	
	public Map<String, List<TCComponent>> getRelationObjectsMap() 
	{
		return m_relationNameToRelationObjectMap;
	}

	/**
	 * 
	 * @return The map of input Relation name and corresponding list of other side objects
	 */
	
	public Map<String, List<TCComponent>> getRelatedObjectsMap() 
	{
		return m_relationNameToRelatedObjectsMap;
	}
	

}
