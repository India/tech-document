package com.nov.rac.utilities.utils;

import org.eclipse.swt.widgets.Combo;

public class TextComboFieldValidator extends AbstractFieldValidator<String>
{

    /** The Constant DEFAULT_ERROR_MESSAGE. */
    private static final String DEFAULT_ERROR_MESSAGE = "Select allowed value";
    
    /** The Constant DEFAULT_WARNING_MESSAGE. */
    private static final String DEFAULT_WARNING_MESSAGE = "";
    
    /** The field. */
    
    /**
     * Instantiates a new combo field validator.
     * 
     * @param field
     *            the field
     */
    public TextComboFieldValidator()
    {
        super(DEFAULT_ERROR_MESSAGE, DEFAULT_WARNING_MESSAGE);
    }
    
    /**
     * Instantiates a new combo field validator.
     * 
     * @param errorMessage
     *            the error message
     * @param warningMessage
     *            the warning message
     * @param field
     *            the field
     */
    public TextComboFieldValidator(String errorMessage, String warningMessage)
    {
        super(errorMessage, warningMessage);
    }

    @Override
    public boolean isValid(String contents)
    {       
        if (!contents.isEmpty())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean warningExist(String contents)
    {
        // TODO Auto-generated method stub
        return false;
    }
    
}
