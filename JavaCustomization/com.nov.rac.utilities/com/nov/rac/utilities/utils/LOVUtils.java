package com.nov.rac.utilities.utils;

import java.util.Arrays;
import java.util.Vector;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.common.lov.LOVObject;
import com.teamcenter.rac.kernel.ListOfValuesInfo;
import com.teamcenter.rac.kernel.TCComponentListOfValues;
import com.teamcenter.rac.kernel.TCComponentListOfValuesType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class LOVUtils 
{
	public static LOVObject [] getLOVObjectsForLOV(String strLovName)
	{
		LOVObject [] lovObjects = null;
		
		try
		{
			// 1.0 Get TCComponentListOfValues for given lov name.
			TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
			TCComponentListOfValues firstLOVComponent = null;
			
	        TCComponentListOfValuesType tccomponentlistofvaluestype = (TCComponentListOfValuesType)theSession.getTypeComponent("ListOfValues");
	        
	        TCComponentListOfValues atccomponentlistofvalues[] = tccomponentlistofvaluestype.find( strLovName );
	        
	        if(atccomponentlistofvalues != null && atccomponentlistofvalues.length > 0)
	        {
	        	firstLOVComponent = atccomponentlistofvalues[0];
	        }
	        else
	        {
	        	firstLOVComponent = null;
	        }
	        
	        // 2.0 get lov objects for given TCComponentListOfValues
	        if(firstLOVComponent != null)
	        {
	        	ListOfValuesInfo lovInfo = firstLOVComponent.getListOfValues();
	        	
	            String [] strArrFullNames = null;
	            String [] strArrDispValues = null;
	            Object [] objArrValues = lovInfo.getListOfValues();
	            
	            if(firstLOVComponent.isDescriptionShown())
	            {
	            	strArrFullNames = lovInfo.getValuesFullNames();
	            	strArrDispValues = lovInfo.getLOVDisplayValues();
	            } else
	            {
	            	strArrFullNames = lovInfo.getLOVDisplayValues();
	            }
	            
	            lovObjects = buildLOVObjectArray(strArrFullNames, strArrDispValues, objArrValues);
	        }
		}
		catch(TCException exp)
		{
			
		}
        

		return lovObjects;
	}
	
	/*
	 * 
	 * 
	 * 
	 */
	private static LOVObject[] buildLOVObjectArray(String strArrFullNames[], String strArrDispValues[], Object objArrValues[])
	{
		if(strArrFullNames == null)
		{
		    return null;
		}
		if(objArrValues == null)
		{
		    return null;
		}
		if(strArrFullNames.length != objArrValues.length)
		{
			strArrFullNames = new String[objArrValues.length];
		    for(int i = 0; i < objArrValues.length; i++)
		    {
		    	strArrFullNames[i] = objArrValues[i].toString();
		    }
		
		}
		int j = strArrFullNames.length;
		//boolean flag = false;
		LOVObject alovobject[] = new LOVObject[j];
		int l = 0;
		for(int k = 0; k < j; k++)
		{
			alovobject[l] = new LOVObject(objArrValues[k], strArrFullNames[k], strArrDispValues == null || strArrDispValues.length <= k ? null : strArrDispValues[k]);
//			if(Debug.isOn("LOV"))
//			Debug.println((new StringBuilder()).append("LOV realValue[").append(l).append("] = [").append(aobj[k]).append("], dispValue = [").append(strArrFullNames[k]).append("]").toString());
			l++;
		}
		
		return alovobject;
	}
	
	//  TCDECREL-2743
	public static TCComponentListOfValues getLOVComponent(String lovName, TCSession tcSession)
	{
		TCComponentListOfValues firstLOVComponent = null;
		
		TCComponentListOfValuesType tccomponentlistofvaluestype;
		TCComponentListOfValues atccomponentlistofvalues[] = null;
		
		try 
		{
			tccomponentlistofvaluestype = (TCComponentListOfValuesType)tcSession.getTypeComponent("ListOfValues");
			
			atccomponentlistofvalues = tccomponentlistofvaluestype.find( lovName );
        
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}
        
        if(atccomponentlistofvalues != null && atccomponentlistofvalues.length > 0)
        {
        	firstLOVComponent = atccomponentlistofvalues[0];
        }
        else
        {
        	firstLOVComponent = null;
        }
		return firstLOVComponent;
	}
	
	public static LOVObject [] getLOVObjectsForLOVDHL(String strLovName, String filterText)
	{
		LOVObject [] lovObjects = null;
		
		try{
			// 1.0 Get TCComponentListOfValues for given lov name.
			TCSession theSession = (TCSession) AIFUtility.getDefaultSession();
			TCComponentListOfValues firstLOVComponent = null;
			
			firstLOVComponent = getLOVComponent(strLovName, theSession);
	        
	        // 2.0 get lov objects for given TCComponentListOfValues
	        if(firstLOVComponent != null)
	        {
	        	ListOfValuesInfo lovInfo = firstLOVComponent.getListOfValues();
	        	
	            String [] strArrFullNames = null;
	            String [] strArrDispValues = null;
	            Object [] objArrValues = lovInfo.getListOfValues();
	            
	            if(firstLOVComponent.isDescriptionShown())
	            {
	            	strArrFullNames = lovInfo.getValuesFullNames();
	            	strArrDispValues = lovInfo.getLOVDisplayValues();
	            } 
	            else
	            {
	            	strArrFullNames = lovInfo.getLOVDisplayValues();
	            }
	            
	            
	            /* Need to filter out the values from strArrFullNames, strArrDispValues and strArrFullNames here */
				if(!filterText.isEmpty())
				{
					Vector<String> asVector = new Vector<String> ();
					Vector<String> as1Vector = new Vector<String> ();
					Vector<Object> aobjVector = new Vector<Object> ();
					
					String[] sDescriptions = lovInfo.getDescriptions();
					if(!(sDescriptions == null || sDescriptions.length == 0))
					{
						for(int i = 0; i<objArrValues.length; i++)
						{
							if(sDescriptions == null || sDescriptions.length == 0 || sDescriptions[i].trim().isEmpty() || sDescriptions[i].contains(filterText))
							{
								asVector.add(strArrFullNames[i]);
								as1Vector.add(strArrDispValues[i]);
								aobjVector.add(objArrValues[i]);
							}
						}
						strArrFullNames = (String[]) asVector.toArray(new String[asVector.size()]);
						strArrDispValues = (String[]) as1Vector.toArray(new String[as1Vector.size()]);
						objArrValues = aobjVector.toArray();
					}
				}
	            
	            lovObjects = buildLOVObjectArray(strArrFullNames, strArrDispValues, objArrValues);
	        }
		}
		catch(TCException exp)
		{
			
		}
        
		return lovObjects;
	}
	//  TCDECREL-2743
	
	public static Vector<Object> getListOfValuesforLOV(String lovName)
    {
        Vector<Object> lovVector = new Vector<Object>();
        TCSession session =  (TCSession) AIFUtility.getDefaultSession();
    	TCComponentListOfValues lovs = TCComponentListOfValuesType.findLOVByName(session, lovName);
    	if (lovs != null)
        {
        	ListOfValuesInfo lovtypeInfo = null;
        	try 
        	{
        		lovtypeInfo = lovs.getListOfValues();
        	}
        	catch (TCException e)
        	{
        		e.printStackTrace();
        	}
        	Object[] typeLovVal = lovtypeInfo.getListOfValues();

        	lovVector.clear();
        	lovVector.add("");
    		lovVector.addAll(Arrays.asList(typeLovVal));

        }
    	
		return lovVector;
    	
    }
	
	public static String[] getLovStringValues(TCComponentListOfValues tcComponentListOfvalues) throws TCException
    {
        String[] m_lovValues = null;
        TCComponentListOfValues lovValues = tcComponentListOfvalues;
        if (lovValues != null)
        {
            ListOfValuesInfo lovInfo = lovValues.getListOfValues();
            Object[] lov = lovInfo.getListOfValues();
            m_lovValues = new String[lov.length];
            if (lov.length > 0)
            {
                for (int index = 0; index < lov.length; ++index)
                {
                    m_lovValues[index] = (String) lov[index];
                }
            }
        }
        
        return m_lovValues;
    }
	//TCDECREL-6375
	public static Vector<Object> getLovValuesForRSONE(String lovName)
	{
		Vector<Object> uomvector = new Vector<Object>();
    	TCSession session = ((TCSession)AIFUtility.getDefaultSession());
		if ( session != null )
		{
			try
			{
				uomvector.clear();
				
				Object uomObjs[] = null;
				
				TCComponentListOfValuesType lovType = (TCComponentListOfValuesType) session
				.getTypeComponent("ListOfValues");
				TCComponentListOfValues uomLOVs[] = lovType.find(lovName);
				if ( uomLOVs != null && uomLOVs.length > 0 )
				{
					uomObjs = uomLOVs[0].getListOfValues().getListOfValues();
					
					uomvector.add(0, "");
					
                    
                    String desc[] = uomLOVs[0].getListOfValues().getDescriptions();
                    if(desc != null && desc.length>0 )
                    {
                    	int noOfUOms = desc.length;

                    	for(int index=0,cnt=0; index<noOfUOms ; index++)
                    	{
                    		{
                    			if ( (desc[index].indexOf("RSONE") != -1) || (desc[index].trim().length()==0) )
                    			{
                    				cnt++;
                    				uomvector.add(cnt,uomObjs[index].toString());

                    			}

                    		}
                    	}
                    }
					
				}
				else
					throw(new TCException("Please contact Administrator for: \r\n Units Of Measure LOVs are missing."));

				
			}
			catch ( TCException ex )
			{
				ex.printStackTrace();
			}
		}
		if ( uomvector.isEmpty() )
		{
			uomvector.add(0, "");
		}
		return uomvector;
	
		
	}

}
