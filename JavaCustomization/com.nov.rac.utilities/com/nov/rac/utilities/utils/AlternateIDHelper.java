package com.nov.rac.utilities.utils;

import java.util.ArrayList;

import org.eclipse.core.runtime.Assert;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class AlternateIDHelper 
{
	public static String[] getAlternateIds(TCComponent component) throws TCException
	{
		String[] actualAlternateIds = null;
		if(null != component)
		{
			TCProperty altList = component.getTCProperty("altid_list");
			actualAlternateIds = getAlternateIds(altList, null);
		}
		return actualAlternateIds;
	}
	
	public static String[] getAlternateIds(TCComponent component, String identifierContext) throws TCException
	{
		String[] actualAlternateIds = null;
		if(null != component)
		{
			TCProperty altList = component.getTCProperty("altid_list");
			actualAlternateIds = getAlternateIds(altList, identifierContext);
		}
		return actualAlternateIds;
	}
	
	public static String[] getAlternateIds(TCProperty altList, String identifierContext) throws TCException
	{
		String[] actualAlternateIds = null;
		if(altList.getDescriptor().isArray())
		{
			TCComponent[] components = altList.getReferenceValueArray();
			actualAlternateIds = getComponentNames(components);
			if(null != identifierContext)
			{
				actualAlternateIds = getAlternateIds(actualAlternateIds, identifierContext);
			}
		}
		else
		{
			Assert.isTrue(false, "Alternate Id's not of type array.");
		}
		
		return actualAlternateIds;
	}
	
	
	public static String[] getAlternateIds(String[] alternateIDsWithContext, String identifierContext) throws TCException
	{
		ArrayList<String> actualAlternateIds = new ArrayList<String>();
		if(null != alternateIDsWithContext && null != identifierContext)
		{
			int numAltIds = alternateIDsWithContext.length;
			
			for(int i = 0; i < numAltIds; ++i)
			{
				String altId = getActualAlternateID(identifierContext, alternateIDsWithContext[i]);
				if(altId != null)
				{
					actualAlternateIds.add(altId);
				}
			}
		}
		String idAsStringArray[] = new String[actualAlternateIds.size()];
		return actualAlternateIds.toArray(idAsStringArray);
	}
	
	public static String getActualAlternateID(String identifierContext, String alternateID) 
	{
		String comma = ",";
		String actualAlternateId = null;
		if(null != identifierContext && null != alternateID)
		{
			if(identifierContext.charAt(0) != '@')
			{
				identifierContext = '@' + identifierContext;
			}
			if(alternateID.indexOf(identifierContext) != -1)
			{
				//Rahul TODO : Check if we still need this logic of parsing with comma.
				if (alternateID.indexOf(comma) != -1) 
				{
					String[] sAlternateIdTokens = alternateID.split(comma);
					int limit = sAlternateIdTokens.length;
					for (int index = 0; index < limit; index++)
					{
						if (sAlternateIdTokens[index].indexOf(identifierContext) != -1) 
						{
							actualAlternateId = sAlternateIdTokens[index].split(identifierContext)[0].trim();
							break;
						}
					}
				}
				else
				{
					//Should this be replaced with substring?
					actualAlternateId = alternateID.split(identifierContext)[0];
				}
			}
		}

		return actualAlternateId;
	}
	
	private static String[] getComponentNames(TCComponent[] components)
	{
		String[] names = null;
		if(null != components)
		{
			int numOfComps = components.length;
			names = new String[numOfComps];
			for(int i = 0; i < numOfComps; ++i)
			{
				names[i] = components[i].toDisplayString();
			}
		}
		return names;
	}
	
	public static String getSingleString(String[] stringArr, String saperator)
	{
		String singleString = null;
		
		if(null != stringArr && stringArr.length > 0)
		{
			StringBuilder str = new StringBuilder();
			int length = stringArr.length;
			for(int i = 0; i < length - 1; ++i)
			{
				str.append(stringArr[i]);
				str.append(saperator);
			}
			str.append(stringArr[length-1]);
			
			singleString = str.toString();
		}
		
		return singleString;
	}

}
