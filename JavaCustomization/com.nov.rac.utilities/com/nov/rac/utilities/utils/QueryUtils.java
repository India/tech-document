package com.nov.rac.utilities.utils;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.query.SavedQueryService;
import com.teamcenter.services.rac.query._2007_06.SavedQuery.SavedQueryInput;
import com.teamcenter.services.rac.query._2006_03.SavedQuery.GetSavedQueriesResponse;
import com.teamcenter.services.rac.query._2006_03.SavedQuery.SavedQueryObject;
import com.teamcenter.services.rac.query._2007_06.SavedQuery.ExecuteSavedQueriesResponse;

public class QueryUtils {
	private SavedQueryService qryServ;
	
	static public TCComponentQuery getTcComponentQuery(String queryName) {
		TCComponentQuery qry = null;
		SavedQueryService qryServ = SavedQueryService.getService
				    ((TCSession) AIFUtility.getDefaultSession());
		try {
			GetSavedQueriesResponse resp = qryServ.getSavedQueries();
			SavedQueryObject[] qryObjs = resp.queries;
			for (int i = 0; i < qryObjs.length; i++) {
				if (qryObjs[i].name.equals(queryName)) {
					qry = qryObjs[i].query;
					return qry;
				}
			}
		} catch (ServiceException se) {
			se.printStackTrace();
		}
		return qry;
	}

	static public TCComponent[] executeSOAQuery(String[] ipEntries,
			String[] ipValues, String query, int resultsType) {
		SavedQueryService qryServ = SavedQueryService.getService
			    ((TCSession) AIFUtility.getDefaultSession());
		SavedQueryInput queryIp = new SavedQueryInput();
		queryIp.entries = ipEntries;
		queryIp.values = ipValues;
		queryIp.query = getTcComponentQuery(query);
		queryIp.resultsType = 0;

		SavedQueryInput[] queryIps = { queryIp };

		ExecuteSavedQueriesResponse resp = qryServ
				.executeSavedQueries(queryIps);

		return resp.arrayOfResults[0].objects;
	}

}
