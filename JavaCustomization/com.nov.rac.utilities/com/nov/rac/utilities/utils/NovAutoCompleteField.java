package com.nov.rac.utilities.utils;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

public class NovAutoCompleteField extends org.eclipse.jface.fieldassist.AutoCompleteField
{
    private CommonContentAdapter contentAdapter;
    
    protected NovAutoCompleteField(Control control,CommonContentAdapter controlContentAdapter)
    {
        super(control, controlContentAdapter, controlContentAdapter.getProposals());
        this.contentAdapter = controlContentAdapter;
    }
    
    public NovAutoCompleteField(Text text ,String[] proposals)
    {
        this(text,new CommonContentAdapter(text, proposals));
    }
    
    public NovAutoCompleteField(final Combo combo ,String[] proposals)
    {
        this(combo,new CommonContentAdapter(combo, proposals));
        combo.setItems(proposals);
        
        combo.addFocusListener( new FocusAdapter()
        {            
            @Override
            public void focusLost(FocusEvent theFocusEvent)
            {
                Widget theWidget = theFocusEvent.widget;
                handleFocus(theWidget);
            }            
        });        
        
    }

    
    public String[] getProposals()
    {
        return contentAdapter.getProposals();
    }
    
    
    @Override
    public void setProposals(String[] proposals)
    {
        super.setProposals(proposals);
        contentAdapter.setProposals(proposals);
    }   
    
    private void handleFocus(Widget widget) 
    {
    	if(widget instanceof Combo)
    	{
    		String proposalsForCombo[] = getProposals();
    		Combo combo =(Combo)widget;
    		String comboValue = ((Combo)widget).getText();
    		if(!Arrays.asList(proposalsForCombo).contains(comboValue)) 
    		{
    			if(comboValue.length()>0)
    			{
    				String[] filteredProposals = filterProposals(proposalsForCombo,comboValue);
    				combo.setText(filteredProposals[0]);
    			}
    		}
    	}
    	
	}

	private String[] filterProposals(String proposals[], String filterString)
    {
        if(filterString.length() == 0)
            return proposals;
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < proposals.length; i++)
        {
            String string = proposals[i];
            if(string.length() >= filterString.length() && string.substring(0, filterString.length()).equalsIgnoreCase(filterString))
                list.add(proposals[i]);
        }

        return (String[])list.toArray(new String[list.size()]);
    }
    
}
