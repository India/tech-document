 /* ============================================================
   
   File description: 

   Filename: UpdateAttributesUtils 
  
   This class will have all the utility functions being used for Update Attribute Functionality
  

   Date         Developers    Description
   09-08-2011   Namrata   	  Initial Creation

   $HISTORY
   ============================================================ */

package com.nov.rac.utilities.utils;

import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class UpdateAttributesUtils {
	
	private static Registry reg = Registry.getRegistry(UpdateAttributesUtils.class);
	
	/**
	 *  Given the current logged in group, get the Business Group name.
	 * 
	 * @param strCurrentGroup
	 * @return Business Group Name
	 */

	public static String findBusinessGroup( String strCurrentGroup , TCSession session)
	{
		String strCurrentBusinessGroup = "RSOne";
		
		// 1.0 get all preference names from Registry.
		
		String [] strArrAllPrefs = reg.getStringArray("All_Item_Creation_Group_Names", null);
		
		if(strArrAllPrefs != null && strArrAllPrefs.length > 0)
		{
			TCPreferenceService prefServ = session.getPreferenceService();
			
			// 2.0 Iterate over available Item Creation Group Names.
			for(int inx=0; inx < strArrAllPrefs.length; inx++)
			{
				// 3.0 Get the preference name for Item Createion Group
				String strItemCreationGroupPrefName = reg.getString( strArrAllPrefs[inx] + ".PREF" , null);
				
				if(strItemCreationGroupPrefName == null)
				{
					continue;
				}
				
				String[] grpStrArray = prefServ.getStringArray( TCPreferenceService.TC_preference_site,
						                                        strItemCreationGroupPrefName
						                                      );
				
				for(int i = 0; grpStrArray != null && i< grpStrArray.length; i++ )
				{
					if( strCurrentGroup.equals(grpStrArray[i]) )
					{
						strCurrentBusinessGroup = strArrAllPrefs[inx];
						
						return strCurrentBusinessGroup;
						//break;
					}
				}
			}
		}
				
		return strCurrentBusinessGroup;
	}
	
}
