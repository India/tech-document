package com.nov.rac.utilities.utils;

import com.nov.rac.utilities.services.createUpdateHelper.CreateInObjectHelper;
import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.InterfaceExceptionStack;

public class ExceptionHelper
{
    
    public static TCException formattedException(InterfaceExceptionStack stack)
    {
        String errorString = "Please Provide following mandatory fields:";
        
        return formattedException(stack, errorString);
    }
    
    public static TCException formattedException(InterfaceExceptionStack stack, String errorString)
    {
        ExceptionStack newStack = new ExceptionStack();
        TCException e = new TCException(errorString);
        newStack.addErrors(e);
        newStack.addErrors(stack);
        
        InterfaceExceptionStack intStack = newStack;
        TCException newExp = new TCException(intStack.getErrorSeverities(), intStack.getErrorCodes(),
                intStack.getErrorStack());
        return newExp;
    }
    
    public static void validateObjectCreation(CreateInObjectHelper[] input,ExceptionStack stack) throws TCException
    {
        TCComponent tcObject = CreateObjectsSOAHelper.getCreatedOrUpdatedRootObject(input[0]);
        if(tcObject != null)
        {
            String objectType = tcObject.getType();
            stack.addErrors(new int[] { 0 }, new int[] { ExceptionStack.SEVERITY_INFORMATION },
                    new String[] { objectType + " created " });
        }
        
        try
        {
            CreateObjectsSOAHelper.throwErrors(input[0]);
        }
        catch(TCException exception)
        {
            stack.addErrors(exception);
            throw exception;
        }
    }
    
    
}
