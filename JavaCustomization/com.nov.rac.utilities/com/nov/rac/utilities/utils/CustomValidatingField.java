
package com.nov.rac.utilities.utils;

import com.richclientgui.toolbox.validation.IFieldErrorMessageHandler;
import com.richclientgui.toolbox.validation.IQuickFixProvider;
import com.richclientgui.toolbox.validation.NullErrorMessageHandler;
import com.richclientgui.toolbox.validation.converter.IContentsStringConverter;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.richclientgui.toolbox.validation.validator.IInputMaskValidator;
import org.eclipse.jface.fieldassist.*;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

// Referenced classes of package com.richclientgui.toolbox.validation:
//            NullErrorMessageHandler, IFieldErrorMessageHandler, IQuickFixProvider

public class CustomValidatingField
{
    private class ValidatingModifyListener
        implements ModifyListener
    {

        public void modifyText(ModifyEvent e)
        {
            validate();
        }

        ValidatingModifyListener(ValidatingModifyListener validatingmodifylistener)
        {
        }
    }
    
    private class ValidatingControlListener
    extends ControlAdapter
    {
		@Override
		public void controlMoved(ControlEvent controlevent) 
		{
			validate();
		}
    }

    public CustomValidatingField(Control control, IFieldValidator fieldValidator, IControlContentAdapter contentAdapter, IContentsStringConverter stringConverter, boolean isRequired)
    {
        this(control, fieldValidator, null, contentAdapter, stringConverter, isRequired, ((IFieldErrorMessageHandler) (new NullErrorMessageHandler())));
    }

    public CustomValidatingField(Control control, IFieldValidator fieldValidator, ControlDecoration controlDecoration, IControlContentAdapter contentAdapter, IContentsStringConverter stringConverter, boolean isRequired)
    {
        this(control, fieldValidator, controlDecoration, contentAdapter, stringConverter, isRequired, ((IFieldErrorMessageHandler) (new NullErrorMessageHandler())));
    }

    public CustomValidatingField(Control control, IFieldValidator fieldValidator, ControlDecoration controlDecoration, IControlContentAdapter contentAdapter, IContentsStringConverter stringConverter, boolean isRequired, IFieldErrorMessageHandler errorMessageHandler)
    {
        hasContentAssist = false;
        if(control == null)
            throw new IllegalArgumentException("control may not be null");
        this.control = control;
      
        if(fieldValidator == null)
            throw new IllegalArgumentException("fieldValidator may not be null");
        this.fieldValidator = fieldValidator;
        this.controlDecoration = controlDecoration;
        this.contentAdapter = contentAdapter;
        if(stringConverter == null)
            throw new IllegalArgumentException("stringConverter may not be null");
        this.stringConverter = stringConverter;
        isRequiredField = isRequired;
        if(errorMessageHandler == null)
            throw new IllegalArgumentException("errorMessageHandler may not be null");
        this.errorMessageHandler = errorMessageHandler;
    /*    if(fieldValidator instanceof IInputMaskValidator)
            ((IInputMaskValidator)fieldValidator).install(this);*/
        addListeners();
        initQuickFixMenu();
    }

    public void setQuickFixProvider(IQuickFixProvider quickFixProvider)
    {
        this.quickFixProvider = quickFixProvider;
        quickFixMenu = null;
    }

    public void setErrorMessageHandler(IFieldErrorMessageHandler messageHandler)
    {
        if(errorMessageHandler == null)
        {
            throw new IllegalArgumentException("messageHandler may not be NULL!");
        } else
        {
            errorMessageHandler = messageHandler;
            return;
        }
    }

    protected void addListeners()
    {
    	if(control instanceof Label)
            ((Label)control).addControlListener(new ValidatingControlListener());
    	if(control instanceof List)
            ((List)control).addControlListener(new ValidatingControlListener());
    	if(control instanceof Button)
            ((Button)control).addControlListener(new ValidatingControlListener());
        if(control instanceof Text)
            ((Text)control).addModifyListener(new ValidatingModifyListener(null));
        else
        if(control instanceof Combo)
            ((Combo)control).addModifyListener(new ValidatingModifyListener(null));
        else
        if(control instanceof CCombo)
            ((CCombo)control).addModifyListener(new ValidatingModifyListener(null));
        if(!(control instanceof Text))
            control.addListener(13, new Listener() {

                public void handleEvent(Event event)
                {
                    validate();
                }
            }
);
    }

   /* private void doQuickFix()
    {
        quickFixProvider.doQuickFix(this);
    }*/

    private Menu createQuickFixMenu()
    {
        Menu newMenu = new Menu(control);
        MenuItem item = new MenuItem(newMenu, 8);
        item.setText(quickFixProvider.getQuickFixMenuText());
        item.addSelectionListener(new SelectionListener() {

            public void widgetSelected(SelectionEvent event)
            {
              //  doQuickFix();
            }

            public void widgetDefaultSelected(SelectionEvent selectionevent)
            {
            }
        }
);
        return newMenu;
    }

    private void initQuickFixMenu()
    {
        if(controlDecoration == null)
        {
            return;
        } else
        {
            controlDecoration.addMenuDetectListener(new MenuDetectListener() {

                public void menuDetected(MenuDetectEvent event)
                {
                    if(isValid() || quickFixProvider == null || !hasQuickFix())
                        return;
                    if(quickFixMenu == null)
                        quickFixMenu = createQuickFixMenu();
                    quickFixMenu.setLocation(event.x, event.y);
                    quickFixMenu.setVisible(true);
                }
            }
);
            return;
        }
    }

    public Control getControl()
    {
        return control;
    }

    public boolean isRequired()
    {
        return isRequiredField;
    }

    public boolean hasQuickFix()
    {
        return quickFixProvider != null && quickFixProvider.hasQuickFix(getContents());
    }

    public boolean hasContentAssist()
    {
        return hasContentAssist;
    }

    public void dispose()
    {
        if(fieldValidator instanceof IInputMaskValidator)
        {
            ((IInputMaskValidator)fieldValidator).uninstall();
        }
        
        if(controlDecoration != null)
        {
            controlDecoration.dispose();
            controlDecoration = null;
        }
        
        if(quickFixMenu != null)
        {
            quickFixMenu.dispose();
        }
    }

    protected FieldDecoration getErrorDecoration()
    {
        if(errorDecoration == null)
        {
            FieldDecoration standardError = null;
            if(hasQuickFix())
                standardError = FieldDecorationRegistry.getDefault().getFieldDecoration("DEC_ERRORQUICKFIX");
            else
                standardError = FieldDecorationRegistry.getDefault().getFieldDecoration("DEC_ERROR");
            if(fieldValidator.getErrorMessage() == null)
                errorDecoration = standardError;
            else
                errorDecoration = new FieldDecoration(standardError.getImage(), fieldValidator.getErrorMessage());
        }
        errorDecoration.setDescription(fieldValidator.getErrorMessage());
        return errorDecoration;
    }

    protected FieldDecoration getWarningDecoration()
    {
        if(warningDecoration == null)
        {
            FieldDecoration standardWarning = FieldDecorationRegistry.getDefault().getFieldDecoration("DEC_WARNING");
            if(fieldValidator.getWarningMessage() == null)
                warningDecoration = standardWarning;
            else
                warningDecoration = new FieldDecoration(standardWarning.getImage(), fieldValidator.getWarningMessage());
        }
        warningDecoration.setDescription(fieldValidator.getWarningMessage());
        return warningDecoration;
    }

    protected String getStringContents()
    {
        if(contentAdapter != null)
            return contentAdapter.getControlContents(control);
        if(control instanceof Text)
            return ((Text)control).getText();
        else
            return null;
    }

    protected void setStringContents(String contents)
    {
        if(contentAdapter != null)
            contentAdapter.setControlContents(control, contents, contents.length());
        else
        if(control instanceof Text)
            ((Text)control).setText(contents);
    }

    public Object getContents()
    {
        return stringConverter.convertFromString(getStringContents());
    }

    public void setContents(Object contents)
    {
        if(contents != null)
        {
            setStringContents(stringConverter.convertToString(contents));
            validate();
        }
    }

    public boolean isValid()
    {
        return fieldValidator.isValid(getContents());
    }

    public boolean isWarning()
    {
        return fieldValidator.warningExist(getContents());
    }

    public void validate()
    {
        hideDecoration();
        if(!isValid())
        {
            errorMessageHandler.handleErrorMessage(fieldValidator.getErrorMessage(), getStringContents());
            showErrorDecoration();
        } else
        if(isWarning())
        {
            errorMessageHandler.handleWarningMessage(fieldValidator.getWarningMessage(), getStringContents());
            showWarningDecoration();
        } else
        {
            errorMessageHandler.clearMessage();
            if(hasContentAssist())
                showContentAssistDecoration();
            else
            if(isRequired())
                showRequiredFieldDecoration();
        }
    }

    protected FieldDecoration getRequiredFieldDecoration()
    {
        return FieldDecorationRegistry.getDefault().getFieldDecoration("DEC_REQUIRED");
    }

    protected FieldDecoration getCueDecoration()
    {
        return FieldDecorationRegistry.getDefault().getFieldDecoration("DEC_CONTENT_PROPOSAL");
    }

    private void showDecoration(FieldDecoration dec)
    {
        if(controlDecoration != null && dec != null)
        {
            controlDecoration.setImage(dec.getImage());
            controlDecoration.setDescriptionText(dec.getDescription());
            controlDecoration.setShowOnlyOnFocus(false);
            controlDecoration.show();
        }
    }

    public void hideDecoration()
    {
        if(controlDecoration != null)
            controlDecoration.hide();
    }

    private void showErrorDecoration()
    {
        FieldDecoration dec = getErrorDecoration();
        showDecoration(dec);
    }

    private void showWarningDecoration()
    {
        FieldDecoration dec = getWarningDecoration();
        showDecoration(dec);
    }

    private void showRequiredFieldDecoration()
    {
        FieldDecoration dec = getRequiredFieldDecoration();
        showDecoration(dec);
    }

    private void showContentAssistDecoration()
    {
        FieldDecoration dec = getCueDecoration();
        showDecoration(dec);
    }

    private ControlDecoration controlDecoration;
    private final Control control;
    private final IFieldValidator fieldValidator;
    private final IControlContentAdapter contentAdapter;
    private final IContentsStringConverter stringConverter;
    private FieldDecoration errorDecoration;
    private FieldDecoration warningDecoration;
    private IFieldErrorMessageHandler errorMessageHandler;
    private IQuickFixProvider quickFixProvider;
    private Menu quickFixMenu;
    private final boolean isRequiredField;
    private boolean hasContentAssist;
}


/*
	DECOMPILATION REPORT

	Decompiled from: E:\TC10_1_Checkouts\PrivateBranchCheckouts2014\PrivateBranch17Jan_11_29\com.nov.rac.utilities\libs\rcptoolbox.1.0.10.jar
	Total time: 47 ms
	Jad reported messages/errors:
	Exit status: 0
	Caught exceptions:
*/