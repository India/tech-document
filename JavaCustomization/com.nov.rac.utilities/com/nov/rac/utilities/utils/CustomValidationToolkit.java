package com.nov.rac.utilities.utils;

import java.util.Map;
import java.util.WeakHashMap;

import com.richclientgui.toolbox.validation.CComboContentAdapter;
import com.richclientgui.toolbox.validation.IFieldErrorMessageHandler;
import com.richclientgui.toolbox.validation.IQuickFixProvider;
import com.richclientgui.toolbox.validation.converter.IContentsStringConverter;
import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import org.eclipse.jface.fieldassist.*;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.*;

// Referenced classes of package com.richclientgui.toolbox.validation:
//            ValidatingField, CComboContentAdapter, IFieldErrorMessageHandler, IQuickFixProvider

public class CustomValidationToolkit
{
	
	private static class StringConverter implements IContentsStringConverter
	{
		@Override
		public Object convertFromString(String s) 
		{
			return s;
		}

		@Override
		public String convertToString(Object obj) 
		{
			return (String) obj;
		}
		
		private StringConverter()
		{
			
		}
	}

    public CustomValidationToolkit(IContentsStringConverter stringConverter)
    {
        this(stringConverter, 16908288, 1, true);
    }

    public CustomValidationToolkit(IContentsStringConverter stringConverter, int decoratorPosition, int decoratorMarginWidth, boolean showDecorators)
    {
        this.decoratorPosition = 16908288;
        this.decoratorMarginWidth = 1;
        this.showDecorators = true;
        this.stringConverter = stringConverter;
        this.decoratorPosition = decoratorPosition;
        this.decoratorMarginWidth = decoratorMarginWidth;
        this.showDecorators = showDecorators;
    }

    public CustomValidationToolkit(int decoratorPosition,
			int decoratorMarginWidth, boolean showDecorators) 
    {
    	this(new StringConverter(), decoratorPosition, decoratorMarginWidth, showDecorators);
	}

	public void setDefaultErrorMessageHandler(IFieldErrorMessageHandler defaultErrorMessageHandler)
    {
        this.defaultErrorMessageHandler = defaultErrorMessageHandler;
    }

    public IContentsStringConverter getStringConverter()
    {
        return stringConverter;
    }
    
    private CustomValidatingField createValidatingListField(List list, IFieldValidator fieldValidator,
            ControlDecoration controlDecoration, boolean isRequired)
    {
        CustomValidatingField listField = new CustomValidatingField(list, fieldValidator, controlDecoration, new ListContentAdapter(), stringConverter, isRequired);
        
        return listField;
    }
    
    private CustomValidatingField createListField(IFieldValidator fieldValidator, boolean isRequired,
            Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider,
            List list)
    {
        ControlDecoration controlDecoration = null;
        if(showDecorators)
        {
            controlDecoration = new ControlDecoration(list, decoratorPosition);
            controlDecoration.setMarginWidth(decoratorMarginWidth);
        }
        CustomValidatingField listField = createValidatingListField(list, fieldValidator, controlDecoration, isRequired);
        
        if(errorMessageHandler != null)
        {
            listField.setErrorMessageHandler(errorMessageHandler);
        }
        else
        if(defaultErrorMessageHandler != null)
            listField.setErrorMessageHandler(defaultErrorMessageHandler);
        if(quickFixProvider != null)
            listField.setQuickFixProvider(quickFixProvider);
        listField.setContents(initialValue);
        return listField;
    }
    
    private CustomValidatingField createValidatingButtonField(Button button, IFieldValidator fieldValidator,
			ControlDecoration controlDecoration, boolean isRequired) 
    {
    	CustomValidatingField buttonField = new CustomValidatingField(button, fieldValidator, controlDecoration, new ButtonContentAdapter(), stringConverter, isRequired);
     	return buttonField;
	}
    
    private CustomValidatingField createButtonField(IFieldValidator fieldValidator, boolean isRequired,	Object initialValue, IFieldErrorMessageHandler errorMessageHandler,
			IQuickFixProvider quickFixProvider, Button button) {
    	 ControlDecoration controlDecoration = null;
         if(showDecorators)
         {
             controlDecoration = new ControlDecoration(button, decoratorPosition);
             controlDecoration.setMarginWidth(decoratorMarginWidth);
         }
         CustomValidatingField buttonField = createValidatingButtonField(button, fieldValidator, controlDecoration, isRequired);
         
         if(errorMessageHandler != null)
         {
             buttonField.setErrorMessageHandler(errorMessageHandler);
         }
         else
         if(defaultErrorMessageHandler != null)
             buttonField.setErrorMessageHandler(defaultErrorMessageHandler);
         if(quickFixProvider != null)
             buttonField.setQuickFixProvider(quickFixProvider);
         buttonField.setContents(initialValue);
         return buttonField;
	}

	private CustomValidatingField createValidatingLabelField(Label label, IFieldValidator fieldValidator,
			ControlDecoration controlDecoration, boolean isRequired) 
    {
    	CustomValidatingField labelField = new CustomValidatingField(label, fieldValidator, controlDecoration, null, stringConverter, isRequired);
     	return labelField;
	}
    
    private CustomValidatingField createLabelField(IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler,
			IQuickFixProvider quickFixProvider, Label label) 
    {
    	  ControlDecoration controlDecoration = null;
          if(showDecorators)
          {
              controlDecoration = new ControlDecoration(label, decoratorPosition);
              controlDecoration.setMarginWidth(decoratorMarginWidth);
          }
          CustomValidatingField textField = createValidatingLabelField(label, fieldValidator, controlDecoration, isRequired);

          if(errorMessageHandler != null)
              textField.setErrorMessageHandler(errorMessageHandler);
          else
          if(defaultErrorMessageHandler != null)
              textField.setErrorMessageHandler(defaultErrorMessageHandler);
          if(quickFixProvider != null)
              textField.setQuickFixProvider(quickFixProvider);
          textField.setContents(initialValue);
          return textField;
	}
    
	protected CustomValidatingField createValidatingTextField(Text text, IFieldValidator fieldValidator, ControlDecoration controlDecoration, boolean isRequired)
    {
    	CustomValidatingField textField = new CustomValidatingField(text, fieldValidator, controlDecoration, new TextContentAdapter(), stringConverter, isRequired);
        return textField;
    }

    public CustomValidatingField createTextField(Composite parent, IFieldValidator fieldValidator, boolean isRequired, Object initialValue)
    {
        return createTextField(parent, fieldValidator, isRequired, initialValue, ((IFieldErrorMessageHandler) (null)), ((IQuickFixProvider) (null)));
    }

    public CustomValidatingField createTextField(Composite parent, IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider)
    {
        Text text = new Text(parent, 2048);
        return createTextField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, text);
    }

    private CustomValidatingField createTextField(IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider, Text text)
    {
        ControlDecoration controlDecoration = null;
        if(showDecorators)
        {
            controlDecoration = new ControlDecoration(text, decoratorPosition);
            controlDecoration.setMarginWidth(decoratorMarginWidth);
        }
        CustomValidatingField textField = createValidatingTextField(text, fieldValidator, controlDecoration, isRequired);
        
        if(errorMessageHandler != null)
            textField.setErrorMessageHandler(errorMessageHandler);
        else
        if(defaultErrorMessageHandler != null)
            textField.setErrorMessageHandler(defaultErrorMessageHandler);
        if(quickFixProvider != null)
            textField.setQuickFixProvider(quickFixProvider);
        textField.setContents(initialValue);
        return textField;
    }

    protected CustomValidatingField createValidatingComboField(Combo combo, IFieldValidator fieldValidator, ControlDecoration controlDecoration, boolean isRequired)
    {
    	CustomValidatingField comboField = new CustomValidatingField(combo, fieldValidator, controlDecoration, new ComboContentAdapter(), stringConverter, isRequired);
        return comboField;
    }

    public CustomValidatingField createComboField(Composite parent, IFieldValidator fieldValidator, boolean isRequired, Object initialValue, Object items[])
    {
        return createComboField(parent, fieldValidator, isRequired, initialValue, null, null, items);
    }

    public CustomValidatingField createComboField(Composite parent, IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider, Object items[])
    {
        Combo combo = new Combo(parent, 2048);
        if(items != null)
            setComboItems(combo, items);
        return createComboField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, combo);
    }

    private void setComboItems(Combo combo, Object items[])
    {
        String strItems[] = new String[items.length];
        for(int i = 0; i < items.length; i++)
            strItems[i] = stringConverter.convertToString(items[i]);

        combo.setItems(strItems);
    }
    
    private CustomValidatingField createComboField(IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider, Combo combo)
    {
        ControlDecoration controlDecoration = null;
        if(showDecorators)
        {
            controlDecoration = new ControlDecoration(combo, decoratorPosition);
            controlDecoration.setMarginWidth(decoratorMarginWidth);
        }
        CustomValidatingField comboField = createValidatingComboField(combo, fieldValidator, controlDecoration, isRequired);
        
        if(errorMessageHandler != null)
            comboField.setErrorMessageHandler(errorMessageHandler);
        else
        if(defaultErrorMessageHandler != null)
            comboField.setErrorMessageHandler(defaultErrorMessageHandler);
        if(quickFixProvider != null)
            comboField.setQuickFixProvider(quickFixProvider);
        comboField.setContents(initialValue);
        return comboField;
    }

    protected CustomValidatingField createValidatingCComboField(CCombo combo, IFieldValidator fieldValidator, ControlDecoration controlDecoration, boolean isRequired)
    {
    	CustomValidatingField comboField = new CustomValidatingField(combo, fieldValidator, controlDecoration, new CComboContentAdapter(), stringConverter, isRequired);
        return comboField;
    }

    public CustomValidatingField createCComboField(Composite parent, IFieldValidator fieldValidator, boolean isRequired, Object initialValue, Object items[])
    {
        return createCComboField(parent, fieldValidator, isRequired, initialValue, null, null, items);
    }

    public CustomValidatingField createCComboField(Composite parent, IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider, Object items[])
    {
        CCombo combo = new CCombo(parent, 2048);
        if(items != null)
            setCComboItems(combo, items);
        return createCComboField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, combo);
    }

    private void setCComboItems(CCombo combo, Object items[])
    {
        String strItems[] = new String[items.length];
        for(int i = 0; i < items.length; i++)
            strItems[i] = stringConverter.convertToString(items[i]);

        combo.setItems(strItems);
    }
    
    private CustomValidatingField createCComboField(IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider, CCombo combo)
    {
        ControlDecoration controlDecoration = null;
        if(showDecorators)
        {
            controlDecoration = new ControlDecoration(combo, decoratorPosition);
            controlDecoration.setMarginWidth(decoratorMarginWidth);
        }
        CustomValidatingField comboField = createValidatingCComboField(combo, fieldValidator, controlDecoration, isRequired);
        
        if(errorMessageHandler != null)
            comboField.setErrorMessageHandler(errorMessageHandler);
        else
        if(defaultErrorMessageHandler != null)
            comboField.setErrorMessageHandler(defaultErrorMessageHandler);
        if(quickFixProvider != null)
            comboField.setQuickFixProvider(quickFixProvider);
        comboField.setContents(initialValue);
        return comboField;
    }
    
    public CustomValidatingField createField(Control control, IFieldValidator fieldValidator, boolean isRequired, Object initialValue)
    {
        return createField(control, fieldValidator, isRequired, initialValue, null, null);
    }

    public CustomValidatingField createField(Control control, IFieldValidator fieldValidator, boolean isRequired, Object initialValue, IFieldErrorMessageHandler errorMessageHandler, IQuickFixProvider quickFixProvider)
    {
        CustomValidatingField customValidatingField = null;
        
        if(control instanceof Text)
        {
            customValidatingField =  createTextField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, (Text)control);
        }
        else if(control instanceof Combo)
        {
            customValidatingField =  createComboField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, (Combo)control);
        }
        else if(control instanceof CCombo)
        {
            customValidatingField =  createCComboField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, (CCombo)control);
        }
        else if(control instanceof Label)
        {
            customValidatingField = createLabelField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, (Label)control);
        }
        else if(control instanceof Button)
        {
            customValidatingField = createButtonField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, (Button)control);
        }
        else if(control instanceof List)
        {
            customValidatingField = createListField(fieldValidator, isRequired, initialValue, errorMessageHandler, quickFixProvider, (List)control);
        }
        
        return customValidatingField;
    }
    
    private final IContentsStringConverter stringConverter;
    private int decoratorPosition;
    private int decoratorMarginWidth;
    private boolean showDecorators;
    private IFieldErrorMessageHandler defaultErrorMessageHandler;
}