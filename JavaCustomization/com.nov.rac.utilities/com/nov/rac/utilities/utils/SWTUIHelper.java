package com.nov.rac.utilities.utils;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Control;

public class SWTUIHelper
{
	
	
	public static int convertHorizontalDLUsToPixels(Control control, int dlus){
		

		FontMetrics fontMetrics = getFontMetrics(control);

		return Dialog.convertHorizontalDLUsToPixels(fontMetrics,dlus);
		
	}
	

	public static int convertVerticalDLUsToPixels(Control control, int dlus) {

		FontMetrics fontMetrics = getFontMetrics(control);

		return Dialog.convertVerticalDLUsToPixels(fontMetrics, dlus);

	}

	

	private static FontMetrics getFontMetrics(Control control) {
		GC gc = new GC(control);
		FontMetrics fontMetrics = gc.getFontMetrics();
		gc.dispose();
		return fontMetrics;
	}
	
	public static void setFont(Control control,int style)
	{
	    FontData fontData = control.getFont().getFontData()[0];
        Font font = new Font(control.getParent().getDisplay(), new FontData(fontData.getName(), fontData
            .getHeight(), style));
        control.setFont(font);
	}
}
