package com.nov.rac.utilities.utils;


/**
 * The Class MandatoryStringFieldValidator validates the string for its presence and
 * value against its emptiness.
 */
public class MandatoryStringFieldValidator extends AbstractFieldValidator<String>
{

    /** The Constant DEFAULT_ERROR_MESSAGE. */
    private static final String DEFAULT_ERROR_MESSAGE = "Field must not be empty";
    
    /** The Constant DEFAULT_WARNING_MESSAGE. */
    private static final String DEFAULT_WARNING_MESSAGE = "";
    
    /**
     * Instantiates a new mandatory string field validator.
     */
    public MandatoryStringFieldValidator()
    {
        super(DEFAULT_ERROR_MESSAGE, DEFAULT_WARNING_MESSAGE);
    }
    
    /**
     * Instantiates a new mandatory string field validator.
     *
     * @param errorMessage the error message
     * @param warningMessage the warning message
     */
    public MandatoryStringFieldValidator(String errorMessage, String warningMessage)
    {
        super(errorMessage, warningMessage);
    }
    
    /* (non-Javadoc)
     * @see com.richclientgui.toolbox.validation.validator.IFieldValidator#isValid(java.lang.Object)
     */
    public boolean isValid(String contents)
    {
        if (null == contents || contents.trim().isEmpty())
            return false;
        return true;
    }
    
    /* (non-Javadoc)
     * @see com.richclientgui.toolbox.validation.validator.IFieldValidator#warningExist(java.lang.Object)
     */
    public boolean warningExist(String contents)
    {
        return false;
    }
}
