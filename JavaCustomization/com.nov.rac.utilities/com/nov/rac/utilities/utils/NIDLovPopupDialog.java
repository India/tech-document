package com.nov.rac.utilities.utils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.common.controls.SWTLovPopupDialog;
import com.teamcenter.rac.kernel.TCComponentListOfValues;

/**
 * The Class NIDLovPopupDialog. This is customized SWTLovPopupDialog which
 * notifies on property change if proper listener is available
 */
public class NIDLovPopupDialog extends SWTLovPopupDialog
{
    
    /** The property change listener. */
    private PropertyChangeListener propertyChangeListener;
    
    /**
     * Gets the property change listener.
     * 
     * @return the property change listener
     */
    public PropertyChangeListener getPropertyChangeListener()
    {
        return propertyChangeListener;
    }
    
    /**
     * Sets the property change listener.
     * 
     * @param propertyChangeListener
     *            the new property change listener
     */
    public void setPropertyChangeListener(PropertyChangeListener propertyChangeListener)
    {
        this.propertyChangeListener = propertyChangeListener;
    }
    
    
    /**
     * Instantiates a new nID lov popup dialog.
     *
     * @param shell the shell
     * @param style the style
     * @param tccomponentlistofvalues the tccomponentlistofvalues
     * @param defaultLovValue the default lov value
     */
    public NIDLovPopupDialog(Shell shell, int style, TCComponentListOfValues tccomponentlistofvalues, String defaultLovValue)
    {
        super(shell, style, tccomponentlistofvalues, defaultLovValue);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.teamcenter.rac.common.controls.SWTLovPopupDialog#propertyChange(java
     * .beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent propertychangeevent)
    {
       // super.propertyChange(propertychangeevent);
        if (null != propertyChangeListener)
        {
            propertyChangeListener.propertyChange(propertychangeevent);
        }
    }
}
