/*================================================================================
                             Copyright (c) 2013 National Oilwell Varco
                             Unpublished - All rights reserved
 ================================================================================
 File Description:
 File Name   : NOVCustomTextField.java
 Package Name: com.nov.rac.utilities.utils
 ================================================================================
                                 Modification Log
 ================================================================================
 Revision        Date          Author        Description  
   1.0         2013/01/25     Mishal        Custom Text Field
 ================================================================================*/

package com.nov.rac.utilities.utils;

import java.awt.Component;
import java.awt.Window;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import com.teamcenter.rac.util.Registry;

/**
 * Class NOVStrTextField extends JTextField
 * 
 * @author mishalt
 */
public class NOVCustomTextField extends JTextField
{
    protected Registry m_reg;
    private static final long serialVersionUID = 1L;
    private Component m_comp = null;
    private int m_maxLimit = 0;
    private String m_fieldName;
    
    public NOVCustomTextField(String name, int strLimit)
    {
        m_maxLimit = strLimit;
        m_fieldName = name;
        m_reg = Registry.getRegistry(this);
    }
    
    @Override
    protected Document createDefaultModel()
    {
        return new StrTextDocument();
    }
    
    /**
     * Class StrTextDocument extends PlainDocument
     * 
     * 
     * @author mishalt
     */
    class StrTextDocument extends PlainDocument
    {
        
        private static final long serialVersionUID = 1L;
        
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
        {
            if (str == null)
            {
                return;
            }
            String oldString = getText(0, getLength());
            String newString = oldString.substring(0, offs) + str + oldString.substring(offs);
            StringBuffer errorStr = new StringBuffer();
            if (newString.length() > m_maxLimit)
            {
                m_comp = getParent().getParent();
                errorStr.append(m_fieldName);
                errorStr.append(" can have maximum ");
                errorStr.append(m_maxLimit);
                errorStr.append(" characters.");
                Window win = getWindow(m_comp);
                MessageBox.post(win, "Warning", errorStr.toString(), MessageBox.WARNING);
                return;
            }
            super.insertString(offs, str, a);
        }
    }
    
    public Window getWindow(Component con)
    {
        Window window = null;
        if (con.getParent() instanceof Window)
        {
            window = (Window) con.getParent();
        }
        else
        {
            window = getWindow(con.getParent());
        }
        return window;
    }
}
