/**
 * @author sachinkab
 * Created On: Dec 2013
 */
package com.nov.rac.utilities.utils;

import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;

public class NOVOpenObjectHelper
{
    
    /* ****************************************************** */
    /* Open selected object in its native application */
    /* ****************************************************** */
    public static void invokeObjectApplication(TCSession session, TCComponent component)
    {
        try
        {
            Object[] args = new Object[2];
            args[0] = AIFUtility.getActiveDesktop();
            args[1] = (InterfaceAIFComponent) component;
            AbstractAIFCommand openCmd = session.getOpenCommand(args);
            openCmd.executeModal();
            return;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
