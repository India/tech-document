/**
 * 
 */
package com.nov.rac.utilities.utils;

import java.awt.Component;
import java.awt.Window;

/**
 * @author viveksm
 *
 */
public class MessageBoxHelper 
{
	public static Window getWindow(Component con)
	{
		Window window = null;
		
		if(con.getParent() instanceof Window)
		{
			window = (Window) con.getParent();
		}
		else
		{
			 window = getWindow(con.getParent());
		}
		
		return window;
		
	}
}
