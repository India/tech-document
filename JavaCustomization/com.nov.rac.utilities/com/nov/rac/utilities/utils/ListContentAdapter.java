package com.nov.rac.utilities.utils;

import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Control;

public class ListContentAdapter implements IControlContentAdapter
{
    
    @Override
    public String getControlContents(Control control)
    {
        String contents = null;
        
        if(((List)control).getItemCount() > 0 )
        {
            contents = ((List)control).getItems()[0];
        }
        
        return contents;
    }
    
    @Override
    public int getCursorPosition(Control arg0)
    {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public Rectangle getInsertionBounds(Control arg0)
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void insertControlContents(Control arg0, String arg1, int arg2)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void setControlContents(Control arg0, String arg1, int arg2)
    {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void setCursorPosition(Control arg0, int arg1)
    {
        // TODO Auto-generated method stub
        
    }
    
}
