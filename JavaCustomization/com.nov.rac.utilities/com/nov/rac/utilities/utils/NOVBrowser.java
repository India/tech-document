package com.nov.rac.utilities.utils;

import java.net.URI;
import java.net.URL;

import com.teamcenter.rac.util.Registry;


public class NOVBrowser
{
    protected Registry m_registry = Registry.getRegistry(this);
    public static void open(String urlString) throws Exception
    {
        open ( new URI(urlString));
    }
    
    public static void open(URL url) throws Exception
    {
        open(url.toURI());
    }
    
    
    public static void open(URI uri) throws Exception
    {
        Registry registry = Registry.getRegistry("com.nov.rac.utilities.utils.utils");
        if (!java.awt.Desktop.isDesktopSupported())
        {
            throw new Exception(registry.getString("NOVBrowser.BrowserError"));
        }
        java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

        if (!desktop.isSupported(java.awt.Desktop.Action.BROWSE))
        {
            throw new Exception(registry.getString("NOVBrowser.BrowserActionError"));
        }
        desktop.browse(uri);
    }
    
    
    
    
//    public static void main(String[] args) throws Exception
//    {
//        String string = "C:/Users/snehac/Desktop/openURI.java";//"www.google.com";
//        open(string);
//    }
    
    
}
