package com.nov.rac.utilities.utils;

import java.util.Arrays;
import java.util.List;

public class ValidateHelper 
{

	public static boolean searchInArray(String name, String[] array)
	{
		boolean contains = false;
		if(name!=null)
		{
			List<String> stringList= Arrays.asList(array);
			/*int index= Collections.binarySearch(stringList, name);
			
			if(index <0)
			{
				contains= false;
			}*/
			//this is case insensitive contains= ((ArrayList)stringList).contains(name);
			
			for(int index=0;index<array.length;index++)
			{
				if(name.equalsIgnoreCase(array[index]))
				{
					contains=true;
					break;
				}
			}
		}
		return contains;
	}
	
	
	public static boolean searchInArrayInsensitive(String name, String[] array)
	{
		boolean contains = false;
		if(name!=null)
		{
			for(int index=0;index<array.length; ++index)
			{
				if((array[index]).regionMatches(true, 0, name, 0, name.length()))
				{
					contains=true;
					break;
				}
			}
		}
		return contains;
	}
	
	
	public static boolean startsWithInArray(String name, String[] array)
	{
		boolean contains = false;
		if(name!=null)
		{
			List<String> stringList= Arrays.asList(array);
			/*int index= Collections.binarySearch(stringList, name);
			
			if(index <0)
			{
				contains= false;
			}*/
			//this is case insensitive contains= ((ArrayList)stringList).contains(name);
			
			for(int index=0;index<array.length;index++)
			{
				if((array[index]).startsWith(name))
				{
					contains=true;
					break;
				}
			}
		}
		return contains;
	}
}
