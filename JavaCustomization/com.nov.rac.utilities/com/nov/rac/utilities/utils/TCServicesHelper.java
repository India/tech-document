package com.nov.rac.utilities.utils;

import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.common.IContextInputService;
import com.teamcenter.rac.services.IAuthenticator;
import com.teamcenter.rac.ui.RACUIPlugin;
import com.teamcenter.rac.util.OSGIUtil;

public class TCServicesHelper
{
    public static IAuthenticator getAuthenticatorService()
    {
        IAuthenticator authenticator = (IAuthenticator)OSGIUtil.getService(AifrcpPlugin.getDefault(),
                com.teamcenter.rac.services.IAuthenticator.class);
        
        return authenticator;
    }
    
    public static IContextInputService getContextInputService()
    {
        IContextInputService icontextinputservice = (IContextInputService)OSGIUtil.getService(RACUIPlugin.getDefault(),
                com.teamcenter.rac.common.IContextInputService.class);
        
        return icontextinputservice;
    }
}
