package com.nov.rac.utilities.utils;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.widgets.Combo;

/**
 * The Class validates the string for its presence in allowed values.
 */
public class ComboFieldValidator extends AbstractFieldValidator<String>
{
    
    /** The Constant DEFAULT_ERROR_MESSAGE. */
    private static final String DEFAULT_ERROR_MESSAGE = "Select allowed value";
    
    /** The Constant DEFAULT_WARNING_MESSAGE. */
    private static final String DEFAULT_WARNING_MESSAGE = "";
    
    /** The field. */
    private final Combo field;
    
    /**
     * Instantiates a new combo field validator.
     * 
     * @param field
     *            the field
     */
    public ComboFieldValidator(Combo field)
    {
        super(DEFAULT_ERROR_MESSAGE, DEFAULT_WARNING_MESSAGE);
        this.field = field;
    }
    
    /**
     * Instantiates a new combo field validator.
     * 
     * @param errorMessage
     *            the error message
     * @param warningMessage
     *            the warning message
     * @param field
     *            the field
     */
    public ComboFieldValidator(String errorMessage, String warningMessage, Combo field)
    {
        super(errorMessage, warningMessage);
        this.field = field;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.richclientgui.toolbox.validation.validator.IFieldValidator#isValid
     * (java.lang.Object)
     */
    public boolean isValid(String contents)
    {
        List<String> allowedValues = Arrays.asList(field.getItems());
        if (allowedValues.contains(contents))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.richclientgui.toolbox.validation.validator.IFieldValidator#warningExist
     * (java.lang.Object)
     */
    public boolean warningExist(String contents)
    {
        return false;
    }
}
