/**
 * @author sachinkab
 * Created On: Dec 2013
 */
package com.nov.rac.utilities.classification.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;

import com.nov.rac.utilities.utils.NOVFindObjectHelper;
import com.nov.rac.utilities.utils.NOVOpenObjectHelper;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
//import com.teamcenter.rac.aif.ApplicationDef;
//import com.teamcenter.rac.aif.IApplicationDefService;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
//import com.teamcenter.rac.classification.common.AbstractG4MContext;
//import com.teamcenter.rac.classification.icm.ClassificationService;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.services.ISelectionMediatorService;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.OSGIUtil;
import com.teamcenter.rac.util.Registry;

public class NOVViewRDDHandler extends AbstractHandler implements IExecutableExtension
{
    
    private String m_actionCommand;
    private String m_applicationID;
    private Registry m_registry = Registry.getRegistry(this);
    
    /**
     * The constructor.
     */
    public NOVViewRDDHandler()
    {
    }
    
    /**
     * the command has been executed, so extract extract the needed information
     * from the application context.
     */
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        //Commented for migration from TC8.3 to TC10.1
        /*try
        {
            String applicationId = this.m_applicationID;
            IApplicationDefService localIApplicationDefService = AifrcpPlugin.getApplicationDefService();
            if (localIApplicationDefService != null)
            {
                ApplicationDef localApplicationDef = localIApplicationDefService
                        .getApplicationDefByKey(this.m_applicationID);
                if (localApplicationDef != null)
                {
                    applicationId = localApplicationDef.getPerspectiveId();
                }
                String perspectiveId = "(perspectiveId=" + applicationId + ")";
                ClassificationService serviceObj = (ClassificationService) OSGIUtil.getService(
                        AifrcpPlugin.getDefault(), IAspectUIService.class.getName(), perspectiveId);
                AbstractG4MContext context = serviceObj.getG4MContext();
                TCComponent tcComp = context.getClassifiedComponent();
                TCComponent docRev = NOVFindObjectHelper.findRDDforItem(tcComp);
                if (docRev != null)
                {
                    NOVOpenObjectHelper.invokeObjectApplication(serviceObj.getSession(), docRev);
                }
                else
                {
                    MessageBox.post(m_registry.getString("ViewRDD.MESSAGE"), m_registry.getString("ViewRDD.TITLE"),
                            MessageBox.INFORMATION);
                    
                }
            }
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }*/
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        TCComponent tcComp = getSelectedComponent();
        TCComponent docRev = null;
        
        if(tcComp != null)
        {
            docRev = NOVFindObjectHelper.findRDDforItem(tcComp);
        }
        
        if (docRev != null)
        {
            NOVOpenObjectHelper.invokeObjectApplication(tcSession, docRev);
        }
        else
        {
            MessageBox.post(m_registry.getString("ViewRDD.MESSAGE"), m_registry.getString("ViewRDD.TITLE"),
                    MessageBox.INFORMATION);
            
        }
        
        return null;
    }
    
    private TCComponent getSelectedComponent()
    {
        TCComponent tcComponent = null;
        ISelectionMediatorService iselectionmediatorservice = (ISelectionMediatorService) OSGIUtil
                .getService(AifrcpPlugin.getDefault(),
                        ISelectionMediatorService.class);
        InterfaceAIFComponent[] interfaceAIFComponents = (InterfaceAIFComponent[]) iselectionmediatorservice
                .getTargetComponents();
        if (interfaceAIFComponents != null && interfaceAIFComponents.length > 0)
        {
            tcComponent = (TCComponent) interfaceAIFComponents[0];
        }
        
        return tcComponent;
    }
    
    public void setInitializationData(IConfigurationElement paramIConfigurationElement, String paramString,
            Object paramObject) throws CoreException
    {
        String[] arrayOfString = paramObject.toString().split(":");
        Assert.isTrue(arrayOfString.length == 2);// Check to separate
                                                 // ActionCommand and
                                                 // ApplicationID
        this.m_actionCommand = arrayOfString[0];
        this.m_applicationID = arrayOfString[1];
    }
    
}
