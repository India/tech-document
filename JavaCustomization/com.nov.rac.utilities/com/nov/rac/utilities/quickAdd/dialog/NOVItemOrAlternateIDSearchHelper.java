package com.nov.rac.utilities.quickAdd.dialog;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class NOVItemOrAlternateIDSearchHelper implements INOVSearchProvider {

	public static String m_ID=null;
	

	@Override
	public String getBusinessObject() {
		return null;
	}

	@Override
	public QuickBindVariable[] getBindVariables() {
QuickSearchService.QuickBindVariable []  allBindVars =new QuickSearchService.QuickBindVariable[1];
		
		allBindVars[0] = new QuickSearchService.QuickBindVariable();
		allBindVars[0].nVarType = POM_string;
		allBindVars[0].nVarSize = 1;
		allBindVars[0].strList = new String[]{m_ID};

		
		return allBindVars;
	}

	@Override
	public QuickHandlerInfo[] getHandlerInfo() {

		QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
		
		allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
		allHandlerInfo[0].handlerName = "NOVSRCH-get-item-or-alternateid";
		allHandlerInfo[0].listBindIndex = new int[]{1} ;
		allHandlerInfo[0].listReqdColumnIndex = new int[]{1};
		allHandlerInfo[0].listInsertAtIndex = new int[]{1};

		return allHandlerInfo;
		
	}

	
	public  String execute(int initLoadAll) throws Exception 
	{
		Vector <String> items = new Vector<String>();
		Vector <String> identifiers = new Vector<String>();
		String reqPuids = null;
		INOVSearchProvider searchService = this;
		INOVSearchResult searchResult = null;
		INOVResultSet theResSet=null;
		m_ID= getID();
		
		searchResult = NOVSearchExecuteHelperUtils.searchSOACallServer(searchService, initLoadAll);
		
		
		if(searchResult != null && searchResult.getResultSet().getRows() > 0)
		{
			theResSet = searchResult.getResultSet();
			
			for (int token=0; token<theResSet.getRowData().size();token=token+2)
			{
				if (theResSet.getRowData().get(token+1).equalsIgnoreCase("Identifier" ))
				{
					identifiers.add(theResSet.getRowData().get(token));
					
				} else 
					{
						items.add(theResSet.getRowData().get(token));
					}
				}
			
			
			if (items.size() == 1 || identifiers.size() == 1)
			{
				if (items.size()==1)
				{
					reqPuids= items.get(0);
				}else 
				{
					reqPuids= identifiers.get(0);
				}
			}
		 }
		
		return  reqPuids;
		}


	public static String getID() {
		return m_ID;
	}

	public void setID(String m_ID) {
		NOVItemOrAlternateIDSearchHelper.m_ID = m_ID;
	}

}


