package com.nov.rac.utilities.quickAdd.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.nov.quicksearch.services.INOVSearchProvider;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentBOMLine;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.kernel.TCUserService;
import com.teamcenter.rac.util.Registry;

public class AddDialog extends Dialog
{
	private String idText = null;
	private String addText = null;
	private String titleText = null;
	private Image dialogICON = null;
	private TCUserService   service   = null;
	private Text idValue=null;
	private TCComponentItem item = null;
	private Registry dlgRegistry = null;
	private Composite comp = null;
	private Listener openerListener;

	public AddDialog(Shell parent, int style) {
		//super(parent, SWT.CLOSE|SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		super(parent, style);
		initializeRegistryStuff();
		parent.setImage(this.dialogICON);
		parent.setText(this.titleText);

	}

	public void initializeRegistryStuff()
	{
		dlgRegistry = Registry.getRegistry(this);
		this.idText =dlgRegistry.getString("id.TEXT");
		this.addText =dlgRegistry.getString("add.TEXT");
		this.titleText = dlgRegistry.getString("quickAdd.TITLE");
		this.dialogICON = dlgRegistry.getImage("dailog.ICON");
	}


	public void showQuickAddDialog(final Shell shellArg,final InterfaceAIFComponent[] aifComp){
		try
		{
			final TCSession session = (TCSession)AIFUtility.getDefaultSession();
			service = session.getUserService();
			Display display = PlatformUI.getWorkbench().getDisplay();
			final Shell shellObj = new Shell(PlatformUI.getWorkbench().getDisplay(), SWT.CLOSE |SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
			shellObj.setLayout(new GridLayout());
			shellObj.setSize(250, 80);
			shellObj.setImage(this.dialogICON);
			shellObj.setText(this.titleText);
			
			//Below code Calculates coordinates of the window to place shell in the center 

			int width = display.getClientArea().width;
			int height = display.getClientArea().height;
			shellObj.setLocation(((width - shellObj.getSize().x) / 2) + display.getClientArea().x, ((height - shellObj.getSize().y) / 2) + display.getClientArea().y);

			comp = new Composite(shellObj, SWT.NONE);

			GridLayout layout = new GridLayout();
			layout.numColumns = 3;
			comp.setLayout(layout);
			GridData data = new GridData(GridData.FILL_BOTH);
			data.horizontalSpan = 2;
			comp.setLayoutData(data);

			new Label(comp, SWT.NONE).setText(this.idText);
			idValue = new Text(comp, SWT.SINGLE | SWT.BORDER);
			GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, false);
			gridData.horizontalSpan = 1;
			gridData.widthHint = 80;
			idValue.setLayoutData(gridData);
			final Button addBtn = new Button(comp,SWT.PUSH);
			GridData btngridData = new GridData(GridData.CENTER, GridData.CENTER, true, false);
			btngridData.horizontalSpan = 1;
			btngridData.widthHint = 50;
			addBtn.setText(this.addText);
			addBtn.setLayoutData(btngridData);
			shellObj.open();

			openerListener = new Listener() {
				public void handleEvent(Event arg0) {
					String itemId_or_altId = idValue.getText();
					try {
						NOVItemOrAlternateIDSearchHelper itemorAlternateIDSearchHelper = new NOVItemOrAlternateIDSearchHelper();
						itemorAlternateIDSearchHelper.setID(itemId_or_altId);
						String retObj=null;
						try {
							retObj = itemorAlternateIDSearchHelper.execute(INOVSearchProvider.INIT_LOAD_ALL);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
						item=(TCComponentItem) session.stringToComponent(retObj);
						
						//item = (TCComponentItem)service.call("NATOIL_findItemBasedOnItemorAltId",new Object[]{itemId_or_altId});
						if(item==null){
							displayMsgBox(shellArg,SWT.ICON_ERROR,dlgRegistry.getString("msgBox.ID_ALTID_DOESNOT_EXIST_MESSAGE"));
						}else
						{
							TCComponentBOMLine topBOMLine = (TCComponentBOMLine)aifComp[0];
							try {
								//System.out.println("Item Name "+item.getProperty("item_id"));
								//System.out.println("Item Name from BOMLine"+topBOMLine.getItem().getProperty("item_id"));
								if(topBOMLine.getItem().equals(item)){
									displayMsgBox(shellArg,SWT.ICON_WARNING,"Object ["+item+"] is already in the target folder as contents.");
								}
								else{
									topBOMLine.add(item, "");
									shellObj.close();
								}
							}catch (TCException e) {
								displayMsgBox(shellArg,SWT.ICON_WARNING,dlgRegistry.getString("msgBox.CIRCULAR_COPY_MESSAGE"));
							}
						}
					}catch (TCException e) {
						displayMsgBox(shellArg,SWT.ICON_WARNING,dlgRegistry.getString("msgBox.INVALID_ID_ALTID_MESSAGE"));
					}	
				}
			};

			try{
				addBtn.addListener(SWT.Selection, openerListener);
			}catch(Exception ex){
				ex.printStackTrace();
			}

			while (!shellObj.isDisposed()) {
				if (!shellObj.getDisplay().readAndDispatch())
					shellObj.getDisplay().sleep();
			}
			shellObj.dispose();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void displayMsgBox(Shell shellObj,int style,String errmessage)
	{
		MessageBox messageBox = new MessageBox(shellObj, style);
		messageBox.setText(dlgRegistry.getString("msgBox.TITLE"));
		messageBox.setMessage(errmessage);
		messageBox.open();
	}



}
