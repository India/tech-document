package com.nov.rac.utilities.quickAdd.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.utilities.quickAdd.dialog.AddDialog;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.util.Registry;

public class QuickAddHandler extends AbstractHandler  
 {

	 private Registry handlerRegistry=null;
	
	 public QuickAddHandler()
	 {
	    	super();
	    	handlerRegistry = Registry.getRegistry(this);
	}
	 
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);		
		InterfaceAIFComponent[] aifComp =  AIFUtility.getTargetComponents();
		
		if(aifComp.length>1){
			MessageBox messageBox = new MessageBox(window.getShell(), SWT.ICON_WARNING);
	        messageBox.setText(handlerRegistry.getString("msgBox.TITLE"));
	        messageBox.setMessage(handlerRegistry.getString("msgBox.MESSAGE"));
	        messageBox.open();
		}
		Shell shell= new Shell(window.getShell(),SWT.NULL);
		AddDialog obj = new AddDialog(shell,SWT.NONE);
		obj.showQuickAddDialog(shell,aifComp);
		return null;
	}
}
