package com.nov.rac.utilities.common;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

// ///////////////////////////////////////////////////
//
// This utility class should provide utility methods
// for Teamcenter Group object only.
// Please feel free to add methods required for 
// TC Groups, if not already available here.
//
// ///////////////////////////////////////////////////

public class GroupUtils 
{
	
	public static String getCurrentGroupName()
	{		
		String localcurrUserGroup = null;
		TCSession theSession = null;
		
		try 
		{
			theSession = (TCSession) AIFUtility.getDefaultSession();
			localcurrUserGroup = theSession.getCurrentGroup().getFullName();
		} 
		catch (TCException e1) 
		{		
			e1.printStackTrace();
		}
		
		return localcurrUserGroup;
	}
	
	/**
     *  Given the current logged in group, get the Business Group name.
     * 
     * @param strCurrentGroup
     * @return Business Group Name
     */
    public static String getBusinessGroupName( String strCurrentGroup )
    {
        String requiredGroupName = strCurrentGroup;
        String strCurrentBusinessGroup = null;
        
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        Registry reg = Registry.getRegistry("com.nov.rac.utilities.updateattributes.action.action");
        
        // 1.0 get all preference names from Registry.
        
        String [] strArrAllPrefs = reg.getStringArray("All_Item_Creation_Group_Names", null);
        
        if(strArrAllPrefs != null && strArrAllPrefs.length > 0)
        {
        
            TCPreferenceService prefServ = session.getPreferenceService();
            
            // 2.0 Iterate over available Item Creation Group Names.
            for(int inx=0; inx < strArrAllPrefs.length; inx++)
            {
                // RSOne preference stores short group names, Mission & DH store full group names.
                requiredGroupName = strCurrentGroup;
                if(strArrAllPrefs[inx].compareTo("RSOne") ==0 )
                {
                    requiredGroupName = getShortGroupName(strCurrentGroup);
                }
                
                // 3.0 Get the preference name for Item Creation Group
                String strItemCreationGroupPrefName = reg.getString( strArrAllPrefs[inx] + ".PREF" , null);
                
                if(strItemCreationGroupPrefName == null)
                {
                    continue;
                }
                
                String[] grpStrArray = prefServ.getStringArray( TCPreferenceService.TC_preference_site,
                                                                strItemCreationGroupPrefName
                                                              );
                
                for(int i = 0; grpStrArray != null && i< grpStrArray.length; i++ )
                {
                    if( requiredGroupName.equals(grpStrArray[i]) )
                    {
                        strCurrentBusinessGroup = strArrAllPrefs[inx];
                        
                        return strCurrentBusinessGroup;
                        //break;
                    }
                }
            }
        }
                
        return strCurrentBusinessGroup;
        
    }
    

    public static String getBusinessGroupName()
    {
        String currentUserGroup = getCurrentGroupName();
        return getBusinessGroupName(currentUserGroup);
    }
    
    private static String getShortGroupName(String groupName)
    {
        String returnValue = "";
        
        int index = groupName.indexOf(".");
        if(index > -1)
        {
            String allGroups = (String) groupName.subSequence(0, index);
            if(allGroups != null)
            {
               returnValue = allGroups;
            }
        }
       
         return returnValue;
    }

}
