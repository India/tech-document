package com.nov.rac.utilities.common;



import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
//import com.nov.rac.utilities.services.createItemHelper.GenOperationHelper;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.PerformOperationResponse;
import com.nov4.services.rac.custom.NOV4DataManagementService;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.ContainerInfo;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.GenNextValueIn;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.GenNextValueResponse;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.GenOperationInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.PropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NOVDataManagementServiceInfo {
	private String context;
	private Integer defaultCounterStartValue;
	private String counterName;
	private String prefix;
	private String postfix;
	private String[] alternateIDContext;
	
	//////////////////////////////////////////////////////////
	private String clientID;
	private PropertyMap propMap;
	private String operationMode;
	private PropertyMap deepcopyData[];	
	private ContainerInfo containterinfo[];
	
	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
		
    
	public PropertyMap getPropMap() {
		return propMap;
	}

	public void setPropMap(IPropertyMap propertyMap) {
		this.propMap = getInternalPropertyMap(propertyMap);
	}

	protected PropertyMap getInternalPropertyMap(IPropertyMap propertyMap)
    {
        SOAPropertyMap internalPropertyMap = new SOAPropertyMap(propertyMap);       
        return internalPropertyMap;
    }
	
	/*protected PropertyMap getInternalPropertyMap(SOAPropertyMap soaPropertyMap)
    {
        return soaPropertyMap;
    }*/
	
	public String getOperationMode() {
		return operationMode;
	}

	public void setOperationMode(String operationMode) {
		this.operationMode = operationMode;
	}

	public PropertyMap[] getDeepcopyData() {
		return deepcopyData;
	}

	public void setDeepcopyData(PropertyMap[] deepcopyData) {
		this.deepcopyData = deepcopyData;
	}

	public ContainerInfo[] getContainterinfo() {
		return containterinfo;
	}
	
	public void setContainterinfo(TCComponent container, String relation,boolean relate) {
		
		ContainerInfo[] containterinfo  = new ContainerInfo[2];
		
		TCComponent targetComponent = null;
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
                      
		containterinfo[0] = new ContainerInfo();
		
		containterinfo[0].targetObject = container;
		containterinfo[0].propertyName = relation;
		containterinfo[0].relate = relate ;	      
	        
		try {
			//targetComponent = tcSession.getUser().getNewStuffFolder();			
			targetComponent = tcSession.stringToComponent("y4YJJjQjoxIlPC");
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		containterinfo[1] = new ContainerInfo();
		
		containterinfo[1].targetObject = targetComponent;
		containterinfo[1].propertyName = relation;
		containterinfo[1].relate = relate ;	      
		
		this.containterinfo = containterinfo;
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public Integer getDefaultCounterStartValue() {
		return defaultCounterStartValue;
	}

	public void setDefaultCounterStartValue(Integer defaultCounterStartValue) {
		this.defaultCounterStartValue = defaultCounterStartValue;
	}

	public String getCounterName() {
		return counterName;
	}

	public void setCounterName(String counterName) {
		this.counterName = counterName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}
	public String[] getalternateIDContext() {
		return alternateIDContext;
	}

	public void setAlternateIDContext(String[] alternateIDContext) {
		this.alternateIDContext = alternateIDContext;
	}

	

	public NOV4DataManagementService getDataMgmtService() {
		
		// 1. create service stub for NOV4DataManagementService
		TCSession session = (TCSession) AIFUtility.getCurrentApplication()
				.getSession();

		NOV4DataManagementService dmService = NOV4DataManagementService
				.getService(session);

		return dmService;

	}

	public String getNextValue() {
			
		GenNextValueIn[] generateNextValueIn = new GenNextValueIn[1];
		
		generateNextValueIn[0] = new GenNextValueIn();
		
		generateNextValueIn[0].context				     = this.getContext();
		generateNextValueIn[0].defaultCounterStartValue  = this.defaultCounterStartValue;
		generateNextValueIn[0].counterName				 = this.counterName;
		generateNextValueIn[0].prefix 					 = this.getPrefix();
		generateNextValueIn[0].postfix 					 = this.getPostfix();
		generateNextValueIn[0].alternateIDContext		 = this.getalternateIDContext();	

		GenNextValueResponse response = this.getDataMgmtService().generateNextValue(generateNextValueIn);
		System.out.println("After Service call");
		
		String strNextName = response.nextGenValue[0].nextValue;
		
		System.out.println(strNextName);
		
		return response.nextGenValue[0].nextValue;
	}
	
	 public void performOp() {
		
		GenOperationInput[] OperationIn = new GenOperationInput[1];
		
		OperationIn[0] = new GenOperationInput();
				
		
		OperationIn[0].clientID		 = this.getClientID();
		OperationIn[0].operationName = this.getOperationMode();
		OperationIn[0].propertyMap   = this.getPropMap();
		//OperationIn[0].deepCopyData  = this.getDeepcopyData();
		//OperationIn[0].containerInfo = this.getContainterinfo();
				
		PerformOperationResponse response = this.getDataMgmtService().performOperation(OperationIn);
																
		System.out.println("After Service call");
		
		
	}

}
