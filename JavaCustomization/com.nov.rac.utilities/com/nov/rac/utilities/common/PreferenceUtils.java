package com.nov.rac.utilities.common;

import java.util.Arrays;
import java.util.List;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;

/////////////////////////////////////////////////////
//
// This utility class should provide utility methods
// for Teamcenter Preference object only.
// Please feel free to add methods required for 
// TC Preferences, if not already available here.
//
// ///////////////////////////////////////////////////

public class PreferenceUtils 
{

	public static String[] getStringValues(int theScope, String prefrenceName)
	{
		String[] stringValues = null;		
		TCSession theSession = null;
		
		theSession = (TCSession) AIFUtility.getDefaultSession();
	   	
		TCPreferenceService prefServ = theSession.getPreferenceService();
       	
		stringValues = prefServ.getStringArray(  theScope, //TCPreferenceService.TC_preference_site,
				                                 prefrenceName  
       			                              );
		
		return stringValues;
	}
	
	 /**
	 * Check if input value is present in the Preference  & returns a boolean
	 */
	public boolean isPrefValueExist(String strPrefInputvalue, String strPrefrenceName, int iScope) {
		
		//int theScope = TCPreferenceService.TC_preference_site;

		String[] strings = PreferenceUtils.getStringValues(iScope,
				strPrefrenceName);

		List<String> preferences = Arrays.asList(strings);
		for (String preference : preferences) {
			System.out.println(preference);
		}

		boolean bPrefValueExist = preferences.contains(strPrefInputvalue);
		System.out.println("bPrefValueExist: " + bPrefValueExist);
		return bPrefValueExist;

	}
}





