package com.nov.rac.utilities.common.actions;

import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.classification.common.AbstractG4MContext;
import com.teamcenter.rac.classification.common.actions.AbstractG4MAction;
import com.teamcenter.rac.classification.common.commands.AbstractG4MCommand;
import com.teamcenter.rac.util.MessageBox;

public class NOVClassificationAction extends AbstractG4MAction
{
    public AbstractG4MContext m_g4Context = null;
    
    public NOVClassificationAction(AbstractAIFUIApplication abstractaifuiapplication, String s)
    {
        super(abstractaifuiapplication, s);
    }
    
    public NOVClassificationAction(AbstractG4MContext abstractg4mcontext, String s, String s1)
    {
        super(abstractg4mcontext, s, s1);
        m_g4Context = abstractg4mcontext;
    }
    
    @Override
    public void run()
    {
        
        try
        {
            String commandKey = getCommandKey();
            AbstractG4MCommand abstractaifcommand = (AbstractG4MCommand) registry.newInstanceFor(commandKey,
                    new Object[] { m_g4Context, commandKey });
            abstractaifcommand.executeModal();
        }
        catch (Exception exception)
        {
            MessageBox.post(parent, exception);
        }
        
    }
    
}
