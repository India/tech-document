package com.nov.rac.utilities.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.SoaUtil;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.businessmodeler.ConstantsService;

public class ConstantHelper
{
    public  String getTypeConstant(String s, String s1)
    {
        String as[] = getTypesConstant(new String[] {
            s
        }, s1);
        return as != null && as.length == 1 ? as[0] : null;
    }

    public  String[] getTypesConstant(String as[], String s)
    {
        int i = as.length;
        String as1[] = new String[i];
        for(int j = 0; j < i; j++)
            as1[j] = s;

        return getTypesConstant(as, as1);
    }

    public  String[] getTypesConstant(String as[], String as1[])
    {
        ArrayList arraylist = new ArrayList();
        int i = as.length;
        for(int j = 0; j < i; j++)
        {
            String s = as[j];
            String s1 = as1[j];
            if(!m_typeCache.containsKey(getKey(s, s1)))
            {
                com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantKey typeconstantkey = new com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantKey();
                typeconstantkey.constantName = s1;
                typeconstantkey.typeName = s;
                arraylist.add(typeconstantkey);
            }
        }

        if(!arraylist.isEmpty())
        {
            ConstantsService constantsservice = ConstantsService.getService(m_session);
            com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantValueResponse typeconstantvalueresponse = constantsservice.getTypeConstantValues((com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantKey[])arraylist.toArray(new com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantKey[arraylist.size()]));
            com.teamcenter.rac.kernel.TCException tcexceptionpartial = SoaUtil.checkPartialErrorsNoThrow(typeconstantvalueresponse.serviceData);
            if(tcexceptionpartial != null)
                Logger.getLogger("com/teamcenter/rac/common/services/CoreConstantService").error(tcexceptionpartial.getLocalizedMessage(), tcexceptionpartial);
            com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantValue atypeconstantvalue[] = typeconstantvalueresponse.constantValues;
            int l = atypeconstantvalue.length;
            for(int i1 = 0; i1 < l; i1++)
            {
                com.teamcenter.services.rac.businessmodeler._2007_06.Constants.TypeConstantValue typeconstantvalue = atypeconstantvalue[i1];
                m_typeCache.put(getKey(typeconstantvalue.key.typeName, typeconstantvalue.key.constantName), typeconstantvalue.value);
            }

        }
        ArrayList arraylist1 = new ArrayList();
        for(int k = 0; k < i; k++)
        {
            String s2 = as[k];
            String s3 = as1[k];
            String s4 = getKey(s2, s3);
            if(m_typeCache.containsKey(s4))
            {
                arraylist1.add(m_typeCache.get(s4));
            } else
            {
                m_typeCache.put(s4, null);
                arraylist1.add(null);
            }
        }

        return (String[])arraylist1.toArray(new String[arraylist1.size()]);
    }
    
    private  String getKey(String s, String s1)
    {
        return (new StringBuilder()).append(s).append(":").append(s1).toString();
    }
    
    private TCSession m_session = (TCSession) AIFUtility.getDefaultSession();
    private final Map m_typeCache = new HashMap();

}
