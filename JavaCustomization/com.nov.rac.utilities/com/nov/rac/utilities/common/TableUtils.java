package com.nov.rac.utilities.common;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov.rac.propertymap.SimplePropertyMap;
import com.teamcenter.rac.aif.common.AIFTable;
import com.teamcenter.rac.aif.common.AIFTableModel;
import com.teamcenter.rac.common.TCTable;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

public class TableUtils
{
    public static String[] getTableColumnNames(AIFTable aifTable)
            throws TCException
    {
        Vector<String> vectorList = new Vector<String>();
        
        AIFTableModel theTableModel = (AIFTableModel) aifTable.getModel();
        int columnIndex = theTableModel.getColumnCount();
        
        for (int inx = 0; inx < columnIndex; inx++)
        {
            String columnName = theTableModel.getColumnOtherId(inx);
            vectorList.addElement(columnName);
        }
        
        String[] propertyList = new String[vectorList.size()];
        vectorList.toArray(propertyList);
        
        return propertyList;
        
    }
    
    public static String[][] getPropertiesList(AIFTable aifTable)
            throws TCException
    {
        String[] propertiesList = getTableColumnNames(aifTable);
        ArrayList<String[]> chainList = new ArrayList<String[]>();
        String[] theSplits = null;
        final String ATTRIBUTE_SPLIT_CHAR = ".";
        for (int i = 0; i < propertiesList.length; i++)
        {
            theSplits = Pattern.compile(ATTRIBUTE_SPLIT_CHAR, Pattern.LITERAL)
                    .split(propertiesList[i]);
            
            if (theSplits != null)
            {
                chainList.add(theSplits);
            }
            else
            {
                chainList.add(new String[] { propertiesList[i] });
            }
        }
        String[][] chainArray = chainList
                .toArray(new String[chainList.size()][]);
        return chainArray;
    }
    
    public static Object getChainPropertyValue(TCComponent theComponent,
            String[] chainPropertyNames) throws TCException
    {
        Object propertyValue = null;
        int iPropertyChainSize = chainPropertyNames.length;
        TCComponent theCurrentComponent = theComponent;
        
        for (int inx = 0; inx < iPropertyChainSize; inx++)
        {
            TCProperty theTempProp = theCurrentComponent
                    .getTCProperty(chainPropertyNames[inx]);
            
            if (theTempProp != null)
            {
                if (theTempProp.isReferenceType())
                {
                    if (theTempProp.isNotArray())
                    {
                        theCurrentComponent = theTempProp.getReferenceValue();
                        propertyValue = theCurrentComponent;
                    }
                    else
                    {
                        theCurrentComponent = null;
                        propertyValue = theTempProp.getReferenceValueArray();
                    }
                    
                    if (theCurrentComponent == null)
                    {
                        break;
                    }
                    
                    continue;
                }
                else
                {
                    propertyValue = theTempProp.getPropertyValue();
                    break;
                }
            }
        } // for(int inx=0; inx<iPropertyChainSize; inx++)
        
        return propertyValue;
    }
    
    public static Object getChainPropertyValue(IPropertyMap propertyMap,
            String[] chainPropertyNames) throws TCException
    {
        Object propertyValue = null;
        int iPropertyChainSize = chainPropertyNames.length;
        
        // If property is single property, then get the value from propertyMap
        if (iPropertyChainSize == 1)
        {
            // propertyValue = propertyMap.getString(chainPropertyNames[0]);
            propertyValue = PropertyMapHelper.getPropertyValue(
                    chainPropertyNames[0], propertyMap);
        }
        // else if, the property is a chain property, then get the value from
        // compound map.
        else
        {
            IPropertyMap theTempPropMap = propertyMap;
            for (int inx = 0; inx < iPropertyChainSize - 1; inx++)
            {
                theTempPropMap = theTempPropMap
                        .getCompound(chainPropertyNames[inx]);
                
                if (theTempPropMap == null)
                {
                    break;
                }
                
            } // for(int inx=0; inx<iPropertyChainSize; inx++)
            
            if (theTempPropMap != null)
            {
                // propertyValue =
                // theTempPropMap.getString(chainPropertyNames[iPropertyChainSize
                // - 1]);
                propertyValue = PropertyMapHelper.getPropertyValue(
                        chainPropertyNames[iPropertyChainSize - 1],
                        theTempPropMap);
            }
        }
        
        return propertyValue;
    }
    
    public static IPropertyMap getChainPropertyMap(IPropertyMap propMap,
            String[] chainPropertyNames) throws TCException
    {
        IPropertyMap reQPropMap = null;
        
        int iPropertyChainSize = chainPropertyNames.length;
        
        // If property is single property, then get the value from propertyMap
        if (iPropertyChainSize == 1)
        {
            reQPropMap = propMap;
        }
        // else if, the property is a chain property, then get the value from
        // compound map.
        else
        {
            // reQPropMap = propMap;
            IPropertyMap compoundPropMap = propMap;
            for (int inx = 0; inx < iPropertyChainSize - 1; inx++)
            {
                if (compoundPropMap == null)
                {
                    break;
                }
                reQPropMap = compoundPropMap
                        .getCompound(chainPropertyNames[inx]);
                
                if (reQPropMap == null)
                {
                    reQPropMap = new SOAPropertyMap();
                    compoundPropMap.setCompound(chainPropertyNames[inx],
                            reQPropMap);
                }
                
                compoundPropMap = reQPropMap;
            }
        }
        
        return reQPropMap;
    }
    
    public static void populateTable(TCComponent[] componentObjects,
            AIFTable aifTable) throws TCException
    {
        AIFTableModel theTableModel = (AIFTableModel) aifTable.getModel();
        int columnCount = theTableModel.getColumnCount();
        
        String[][] chainPropList = getPropertiesList(aifTable);
        if (componentObjects != null)
        {
            for (int iCount = 0; iCount < componentObjects.length; iCount++)
            {
                Vector<Object> propertyValues = new Vector<Object>();
                
                for (int index = 0; index < columnCount; index++)
                {
                    if (chainPropList[index] != null)
                    {
                        Object thePropertyValue = null;
                        
                        thePropertyValue = getChainPropertyValue(
                                componentObjects[iCount], chainPropList[index]);
                        
                        propertyValues.addElement(thePropertyValue);
                    }
                    
                } // for(int index=0; index < propertiesList.length;index++)
                
                aifTable.addRow(propertyValues.toArray());
                
            } // for(int iCount = 0;iCount < componentObjects.length; iCount++ )
        }
    }
    
    public static void populateTable(IPropertyMap[] propertyMapArray,
            AIFTable aifTable) throws TCException
    {
        AIFTableModel theTableModel = (AIFTableModel) aifTable.getModel();
        int columnCount = theTableModel.getColumnCount();
        
        String[][] chainPropList = getPropertiesList(aifTable);
        
        for (int iCount = 0; iCount < propertyMapArray.length; iCount++)
        {
            Vector<Object> propertyValues = new Vector<Object>();
            
            for (int index = 0; index < columnCount; index++)
            {
                if (chainPropList[index] != null)
                {
                    Object thePropertyValue = null;
                    
                    thePropertyValue = getChainPropertyValue(
                            propertyMapArray[iCount], chainPropList[index]);
                    
                    propertyValues.addElement(thePropertyValue);
                }
                
            } // for(int index=0; index < propertiesList.length;index++)
            
            aifTable.addRow(propertyValues.toArray());
            
        } // for(int iCount = 0;iCount < componentObjects.length; iCount++ )
        
    }
    
    public static IPropertyMap[] populateMap(AIFTable aifTable)
    {
        IPropertyMap[] propertyMaps = null;
        
        AIFTableModel theTableModel = (AIFTableModel) aifTable.getModel();
        int columnCount = theTableModel.getColumnCount();
        int rowCount = theTableModel.getRowCount();
        
        propertyMaps = new SimplePropertyMap[rowCount];
        
        String[] columnIdentifiers = theTableModel.getColumnIdentifiers();
        
        for (int inx = 0; inx < rowCount; inx++)
        {
            propertyMaps[inx] = new SimplePropertyMap();
            
            for (int jnx = 0; jnx < columnCount; jnx++)
            {
                Object value = theTableModel.getValueAt(inx, jnx);
                
                PropertyMapHelper.addPropertyValueToMap(columnIdentifiers[jnx],
                        value, propertyMaps[inx]);
            }
            
            propertyMaps[inx].setComponent((TCComponent) theTableModel
                    .getRowLine(inx).getClientData());
        }
        
        return propertyMaps;
    }
    
    public static IPropertyMap[] populateMap(AIFTable table,
            String[] excludeColumnIds)
    {
        IPropertyMap[] propertyMaps = null;
        
        String[][] chainPropList = null;
        try
        {
            chainPropList = getPropertiesList(table);
        }
        catch (TCException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        AIFTableModel theTableModel = (AIFTableModel) table.getModel();
        // int columnCount = theTableModel.getColumnCount();
        int rowCount = theTableModel.getRowCount();
        
        propertyMaps = new SOAPropertyMap[rowCount];
        
        String[] columnIdentifiers = theTableModel.getColumnIdentifiers();
        
        String[] reqColumnIdentifiers = excluedeColumns(columnIdentifiers,
                excludeColumnIds);
        
        int[] columnIndices = new int[reqColumnIdentifiers.length];
        
        for (int inx = 0; inx < reqColumnIdentifiers.length; inx++)
        {
            columnIndices[inx] = TableUtils.getColumnIndex(
                    reqColumnIdentifiers[inx], table);
        }
        
        for (int inx = 0; inx < rowCount; inx++)
        {
            propertyMaps[inx] = new SOAPropertyMap();
            
            for (int jnx = 0; jnx < reqColumnIdentifiers.length; jnx++)
            {
                Object value = theTableModel
                        .getValueAt(inx, columnIndices[jnx]);
                
                IPropertyMap propMap = null;
                try
                {
                    propMap = getChainPropertyMap(propertyMaps[inx],
                            chainPropList[columnIndices[jnx]]);
                }
                catch (TCException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
                if (propMap != null)
                {
                    int chainPropListLength = chainPropList[columnIndices[jnx]].length;
                    PropertyMapHelper
                            .addPropertyValueToMap(
                                    chainPropList[columnIndices[jnx]][chainPropListLength - 1],
                                    value, propMap);
                }
                
            }
            
            propertyMaps[inx].setComponent((TCComponent) theTableModel
                    .getRowLine(inx).getClientData());
        }
        
        return propertyMaps;
    }
    
    public static int getColumnIndex(String columnName, AIFTable table)
    {
        TableModel model = table.getModel();
        model.getColumnCount();
        for (int columnNo = 0; columnNo < table.dataModel.getColumnCount(); columnNo++)
        {
            String columnNameFromModel = table.dataModel
                    .getColumnIdentifier(columnNo);
            
            if (columnName.equals(columnNameFromModel))
            {
                return columnNo;
            }
        }
        return -1;
    }
    
    public static void setColumnName(TCTable table, int columnIndex,
            String columnName)
    {
        table.getColumnModel().getColumn(columnIndex)
                .setHeaderValue(columnName);
    }
    
    public static void stopTableCellEditing(JTable table)
    {
        TableCellEditor theCellEditor = table.getCellEditor();
        if (theCellEditor != null)
        {
            theCellEditor.stopCellEditing();
        }
    }
    
    /**
     * this method is used to hide column of table
     * 
     * @param table
     * @param columnNo
     */
    public static void hideTableColumn(TCTable table, int columnNo)
    {
        table.getColumnModel().getColumn(columnNo).setMaxWidth(0);
        table.getColumnModel().getColumn(columnNo).setPreferredWidth(0);
        table.getColumnModel().getColumn(columnNo).setMinWidth(0);
        table.getColumnModel().getColumn(columnNo).setPreferredWidth(0);
        table.getColumnModel().getColumn(columnNo).setResizable(false);
    }
    
    private static String[] excluedeColumns(String[] columnIds,
            String[] excludeColumnIds)
    {
        String[] requiredColumnIds = null;
        
        Vector<String> My_Vector = new Vector<String>();
        for (int inx = 0; inx < columnIds.length; inx++)
        {
            My_Vector.add(columnIds[inx]);
        }
        
        for (int inx = 0; inx < excludeColumnIds.length; inx++)
        {
            My_Vector.remove(excludeColumnIds[inx]);
        }
        requiredColumnIds = My_Vector.toArray(new String[My_Vector.size()]);
        
        return requiredColumnIds;
    }
    
    /**
     * This function will update the values passed in property map
     * in table.
     * @param aifTable
     * @param map = map of row index(in table view) to propertyMap
     * 
     * @throws TCException
     */
    
    public static void updateTable(AIFTable aifTable,
            Map<Integer, IPropertyMap> map) throws TCException
    {
        Set<Integer> rowIndices = map.keySet();
        int columnCount = aifTable.getColumnCount();
        AIFTableModel theTableModel = (AIFTableModel) aifTable.getModel();
        
        String propertyName = "";
        Integer propType = 0;
        Boolean isArray = false;
        Object thePropertyValue = null;
        
        for (Integer rowIndex : rowIndices)
        {
            String[][] chainPropList = getPropertiesList(aifTable);
            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
            {
                if (chainPropList[columnIndex] != null)
                {
                    propertyName = theTableModel.getColumnIdentifier(columnIndex);
                    
                    if ( PropertyMapHelper.findProperty(propertyName, map.get(rowIndex), propType, isArray))
                    {
                        thePropertyValue = getChainPropertyValue(map.get(rowIndex),
                                chainPropList[columnIndex]);
                        
                            aifTable.setValueAt(thePropertyValue, rowIndex, columnIndex);
                            theTableModel.fireTableRowsUpdated(rowIndex, rowIndex);
                    }
                }
            }
        }
    }
}
