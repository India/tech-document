package com.nov.rac.utilities.common;

import com.teamcenter.rac.util.Registry;

public class RegistryUtils
{
    public static String[] getConfiguration(String context, Registry registry)
    {
        String[] configParams;
        
        String currentGroup = UserSessionInfo.getCurrentGroup();
        
        configParams = getConfiguration(currentGroup, context, registry);
        
        return configParams;
    }
    
    public static String[] getConfiguration(String theGroup, String context,
            Registry registry)
    {
        String[] returnVal = new String[] {};
        String[] configParams;
        String defaultPanelContext = null;
        
        String currentGroup = theGroup;
        
        defaultPanelContext = currentGroup.equals("")?context : currentGroup + "." + context;
        
        // read default panels to be created from registry for group
        configParams = registry.getStringArray(defaultPanelContext, null, null);
        
        // if no default panels for logged in group,
        // read default panels to be created from registry for current bussiness
        // unit
        if (configParams == null)
        {
            defaultPanelContext = GroupUtils.getBusinessGroupName(currentGroup)
                    + "." + context;
            configParams = registry.getStringArray(defaultPanelContext, null,
                    null);
        }
        
        // if no default panels for current bussiness unit,
        // read default panels to be created from registry
        if (configParams == null)
        {
            defaultPanelContext = context;
            configParams = registry.getStringArray(defaultPanelContext, null,
                    null);
        }
        
        if (configParams == null)
        {
            configParams = returnVal;
        }
        
        return configParams;
    }
    
}
