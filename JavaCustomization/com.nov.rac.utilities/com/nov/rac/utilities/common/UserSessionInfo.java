package com.nov.rac.utilities.common;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.SessionChangedEvent;
import com.teamcenter.rac.kernel.SessionChangedListener;
import com.teamcenter.rac.kernel.TCSession;

public class UserSessionInfo 
{
	
	static 
	{
		m_currentGroup = GroupUtils.getCurrentGroupName();
		m_bussinessGroup = GroupUtils.getBusinessGroupName();
		
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		tcSession.addSessionChangeListener( new SessionChangedListener()
		{
            @Override
            public void sessionChanged(SessionChangedEvent arg0)
            {
                m_currentGroup = GroupUtils.getCurrentGroupName();
                m_bussinessGroup = GroupUtils.getBusinessGroupName(m_currentGroup);                
            }
		    
		} );
	}
		
	public static String getCurrentGroup() {
		return m_currentGroup;
	}
	public static String getBussinessGroup() {
		return m_bussinessGroup;
	}



	private static String m_currentGroup;
	private static String m_bussinessGroup;
}
