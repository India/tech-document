package com.nov.rac.utilities.common;


public class ItemTypeHelper
{
    
    public static String getTypeConstantValue(String type, String constantName)
    {
//        ICoreConstantService icoreconstantservice = (ICoreConstantService)OSGIUtil.getService( KernelPlugin.getDefault(), ICoreConstantService.class);
//
//        String value = icoreconstantservice.getTypeConstant( type, constantName);
        ConstantHelper constantHelper = new ConstantHelper();
        String value =  constantHelper.getTypeConstant(type, constantName);
        return value;
    }
    
}