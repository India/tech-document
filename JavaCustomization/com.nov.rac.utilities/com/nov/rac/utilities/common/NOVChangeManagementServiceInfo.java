package com.nov.rac.utilities.common;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;

import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.ENAddRemoveTargetIn;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.ENAddRemoveTargetResponse;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.MassBOMEditInputInfo;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.MassBOMEditResponse;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.ECRInput;


public class NOVChangeManagementServiceInfo {
	
	/*enAddRemoveTargets  SOA*/
	private TCComponent rootTask;
	private TCComponent[] inputTarget;
	private String operationContext;
	/*enAddRemoveTargets  SOA*/
	

	/*MassBOMChange SOA*/
	private TCComponentItemRevision[] selectedItemRevs;
	private TCComponentItemRevision componentItem;
	private TCComponentItemRevision replacementItem;
	private TCComponentForm ecoForm;
	private int iAction; 
	/*MassBOMChange SOA*/
	
	/*CloseECR SOA*/
	private TCComponent[] selectedECO;
	
	public TCComponent[] getSelectedECO() {
		return selectedECO;
	}

	public void setSelectedECO(TCComponent[] selectedECO) {
		this.selectedECO = selectedECO;
	}
		
	/*CloseECR SOA*/
		

	/*enAddRemoveTargets  SOA*/
	public TCComponent getRootTask() {
		return rootTask;
	}

	public void setRootTask(TCComponent rootTask) {
		this.rootTask = rootTask;
	}

	public TCComponent[] getInputTarget() {
		return inputTarget;
	}

	public void setInputTarget(TCComponent[] inputTarget) {
		this.inputTarget = inputTarget;
	}

	public String getOperationContext() {
		return operationContext;
	}

	public void setOperationContext(String operationContext) {
		this.operationContext = operationContext;
	}	
	/*enAddRemoveTargets  SOA*/
		
	/*MassBOMChange SOA*/
	public TCComponentItemRevision[] getSelectedItemRevs() {
		return selectedItemRevs;
	}

	public void setSelectedItemRevs(TCComponentItemRevision[] selectedItemRevs) {
		this.selectedItemRevs = selectedItemRevs;
	}

	public TCComponentItemRevision getComponentItem() {
		return componentItem;
	}

	public void setComponentItem(TCComponentItemRevision componentItem) {
		this.componentItem = componentItem;
	}

	public TCComponentItemRevision getReplacementItem() {
		return replacementItem;
	}

	public void setReplacementItem(TCComponentItemRevision replacementItem) {
		this.replacementItem = replacementItem;
	}

	public int getiAction() {
		return iAction;
	}

	public void setiAction(int iAction) {
		this.iAction = iAction;
	}	
	
	public TCComponentForm getEcoForm() {
		return ecoForm;
	}

	public void setEcoForm(TCComponentForm ecoForm) {
		this.ecoForm = ecoForm;
	}
	
	/*MassBOMChange SOA*/
	
	public NOV4ChangeManagementService getChangeMgmtService() {
		
		// 1. create service stub for NOV4ChangeManagementService
		TCSession session = (TCSession) AIFUtility.getCurrentApplication()
				.getSession();		

		NOV4ChangeManagementService cmService = NOV4ChangeManagementService
				.getService(session);
		
		return cmService;

	}

	
	public TCComponent[] enAddRemoveTargets() {
			
		ENAddRemoveTargetIn[] enAddRemoveTrgInput  = new ENAddRemoveTargetIn[1];
	
		enAddRemoveTrgInput[0] = new ENAddRemoveTargetIn();
	
		enAddRemoveTrgInput[0].rootTask					= this.getRootTask();
		enAddRemoveTrgInput[0].inputTargets				= this.getInputTarget();
		enAddRemoveTrgInput[0].targetFlag				= this.operationContext;		
		
		ENAddRemoveTargetResponse response = this.getChangeMgmtService().enAddRemoveTargets(enAddRemoveTrgInput);
		
		return response.targetOut[0].outputTargets;
	}
	
	public TCComponentItemRevision[] ecoMassBOMEdit() {
		
		 MassBOMEditInputInfo bomEditInputInfo[] = new MassBOMEditInputInfo[1];
		 bomEditInputInfo[0] = new MassBOMEditInputInfo();		 	
		
		 bomEditInputInfo[0].selectedItemRevs					= this.getSelectedItemRevs();
		 bomEditInputInfo[0].componentItem				        = this.getComponentItem();
		 bomEditInputInfo[0].replacementItem				    = this.getReplacementItem();	
		 bomEditInputInfo[0].actionCmd							= this.getiAction();
		 bomEditInputInfo[0].containerInformation.targetObject  = this.getEcoForm();
		 bomEditInputInfo[0].containerInformation.propertyName  = "RelatedECN";
		 bomEditInputInfo[0].containerInformation.relate        = true;
		
		 MassBOMEditResponse mberesponse = this.getChangeMgmtService().massBOMEdit(bomEditInputInfo);
		
		 return mberesponse.massBOMEditOutputList[0].failureItemRevs;
	}
	
	
	public ServiceData close_ECO() {
				
		ECRInput[]  ecrIn =  new ECRInput[selectedECO.length];
		
		for(int icount = 0 ;icount < selectedECO.length ;icount++)
		{			
			ecrIn[icount] = new ECRInput();
			
			ecrIn[icount].ecrList = this.getSelectedECO()[icount];
		}	
		
		ServiceData ecoRresponse = this.getChangeMgmtService().closeECROpr(ecrIn);
		
		return ecoRresponse;
	}
	
	

}
