package com.nov.rac.utilities.common;

import java.util.Arrays;
import com.teamcenter.rac.kernel.TCPreferenceService;

public class RevisionUtils 
{	
	private static String[] m_disAllowedRevisions = null;	
	private int m_revisionMaxLength;
	
	public RevisionUtils()
	{
		m_revisionMaxLength = 3;
	}
		    
    public int getRevisionMaxLength()
    {
        return m_revisionMaxLength;
    }

    public void setRevisionMaxLength(int revisionMaxLength)
    {
        this.m_revisionMaxLength = revisionMaxLength;
    }
        
    public static boolean isWithSpecialCharacters(String str)
    {
        // Pattern for checking special character presence is [^\w\s]+ but it
        // allows _
        // Thus extra condition for _ is added
        return (str.matches("\\w*[^\\w\\s]+\\w*") || str.matches("\\w*_+\\w*") || str.matches("\\w*\\s+\\w*"));
    }
    
    
    protected String getReferenceString(char c)
    {
        String valueString = null;
        if (Character.isDigit(c))
        {
            valueString = "0123456789";
        }
        else if (Character.isLowerCase(c))
        {
            valueString = "abcdefghijklmnopqrstuvwxyz";
        }
        else if (Character.isUpperCase(c))
        {
            valueString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        return valueString;
    }
    
    public static String[] getDisallowedRevisions()
    {
        if (m_disAllowedRevisions == null)
        {
            int theScope = TCPreferenceService.TC_preference_site;
            String prefrenceName = "_NOV_disallowed_Item_revision_ids";
            m_disAllowedRevisions = PreferenceUtils.getStringValues(theScope, prefrenceName);
        }
        return m_disAllowedRevisions;
    }
    
    private boolean isInDisAallowedList(String string)
    {
        return Arrays.asList(RevisionUtils.getDisallowedRevisions()).contains(string);
    }
    
    private boolean isInDisAallowedList(char c)
    {
        return isInDisAallowedList("" + c);
    }
    
    public String incrementRevision(String latestRev)
    {
        StringBuilder stringBuilder = new StringBuilder(latestRev);
        int valueLength = stringBuilder.length();
        
        if (getRevisionMaxLength() < valueLength)
            throw new RuntimeException("Can not increment revision " + latestRev);
        
        if (RevisionUtils.isWithSpecialCharacters(latestRev))
            throw new RuntimeException("Can not increment revision [Special Character] " + latestRev);
        
        boolean isCarryForward = false;
        for (int index = valueLength - 1; index >= 0; index--)
        {
            char character = stringBuilder.charAt(index);
            String valueString = getReferenceString(character);
            int lengh = valueString.length();
            int currentCharIndex = valueString.indexOf(character);
            int newCharIndex = currentCharIndex;
            isCarryForward = false;
            char nextCharacter = '\0';
            do
            {
                newCharIndex = newCharIndex + 1;
                if (newCharIndex == lengh)
                {
                    newCharIndex = 0;
                    isCarryForward = true;
                }
                nextCharacter = valueString.charAt(newCharIndex);
            }
            while (isInDisAallowedList(nextCharacter));
            
            stringBuilder.setCharAt(index, nextCharacter);
            if (!isCarryForward)
            {
                break;
            }
            
        } // for
        
        if (isCarryForward)
        {
            if (1 + valueLength > getRevisionMaxLength())
            {
                throw new RuntimeException("Can not increment revision ==> Limit exceeds" + latestRev);
            }
            else
            {
                int newCycleIndex = 0;
                char initialChar = stringBuilder.charAt(0);
                String valueString = getReferenceString(initialChar);
                if (Character.isDigit(initialChar))
                {
                    newCycleIndex = 1;
                }
                char carryForwardChar = valueString.charAt(newCycleIndex);
                stringBuilder.insert(0, carryForwardChar);
            }
        }
        return stringBuilder.toString();
    }

}
