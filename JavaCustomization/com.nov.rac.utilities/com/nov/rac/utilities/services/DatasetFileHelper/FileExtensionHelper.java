package com.nov.rac.utilities.services.DatasetFileHelper;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2007_06.DataManagement;
import com.teamcenter.services.rac.core._2007_06.DataManagement.DatasetTypeInfo;
import com.teamcenter.services.rac.core._2007_06.DataManagement.ReferenceInfo;

public class FileExtensionHelper 
{
	
	private DatasetTypeInfo[] getValidExtensionsForDatasetType(String[] datasetType)
	{
		 TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	     DataManagementService dmService = DataManagementService.getService(tcSession);
	     DataManagement.DatasetTypeInfoResponse response = dmService.getDatasetTypeInfo(datasetType);
	     return  response.infos;
	}
	
	public boolean isValidExtension(String datasetType,String extension)
	{
		return isValidExtension(new String[] {datasetType}, extension);
	}
	
	private boolean isValidExtension(String[] datasetType,String extension)
	{
		boolean isExtensionValid =false;
		DatasetTypeInfo[] validExtensions = getValidExtensionsForDatasetType(datasetType);
		for(DatasetTypeInfo extensions :validExtensions)
		{
			ReferenceInfo[] referenceInfo = extensions.refInfos;
			isExtensionValid = compareExtension(extension,	referenceInfo);
			if(isExtensionValid)
			{
				break;
			}
				
		}
		return isExtensionValid;
		
	}

	private boolean compareExtension(String extension, ReferenceInfo[] referenceInfo)
	{
		boolean isExtensionValid= false;
		for(ReferenceInfo ref:referenceInfo)
		{				
			if(ref.fileExtension.equals("*"))
			{
				isExtensionValid =true;
				break;
			}
			if(ref.fileExtension.contains(".") && ref.fileExtension.substring(ref.fileExtension.indexOf(".")).equals(extension))
			{
				isExtensionValid =true;
				break;
			}
			
		}
		return isExtensionValid;
	}
}
