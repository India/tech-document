package com.nov.rac.utilities.services.copyitemhelper;

import com.nov.rac.propertymap.DeepCopyDataHelper;
import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement.DeepCopyData;
import com.teamcenter.services.rac.core._2008_06.DataManagement.MasterFormPropertiesInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.SaveAsNewItemInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.SaveAsNewItemOutput2;
import com.teamcenter.services.rac.core._2008_06.DataManagement.SaveAsNewItemResponse2;

public class CopyItemHelper
{
	private IPropertyMap m_itemPropMap;
	private DeepCopyDataHelper[] m_secondaryObjects;
	
	
	public CopyItemHelper(IPropertyMap itemPropMap)
	{
		this(itemPropMap, null);
	}

	public CopyItemHelper(IPropertyMap itemPropMap, DeepCopyDataHelper[] secondaryObjects)
	{
		this.m_itemPropMap = itemPropMap;
		this.m_secondaryObjects = secondaryObjects;
	}
	
	public TCComponent copyItem()
	{
		String theClientID = m_itemPropMap.getString("item_id");
		
		SaveAsNewItemResponse2 response = performOperation();
		SaveAsNewItemOutput2 saveAsOutput = (SaveAsNewItemOutput2) response.saveAsOutputMap.get( theClientID );
		
		//TCDECREL-8110 Start
		/*In TC8.3 saveAsNewItem2 SOA doesn't return Item Master Form in its response
		So the property was not getting loaded and hence the icon as well the master form
		related properties were not displayed correctly.
		Master form property is explicitly loaded as a part of the fix
		This may not be required in TC 10.1*/ 
		
	//	TCComponent masterForm = saveAsOutput.newItem.getRelatedComponent("IMAN_master_form");
	//	masterForm.getTCProperty("rsone_itemtype");
		//TCDECREL-8110 End
		
		return saveAsOutput.newItem;
	}

	private SaveAsNewItemResponse2 performOperation()
	{
		SaveAsNewItemInfo[] input =new SaveAsNewItemInfo[1];
		input[0] = new SaveAsNewItemInfo();
		copyProperties(input[0]);
		
		TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
		DataManagementService dmService = DataManagementService.getService(session);
		SaveAsNewItemResponse2 response = dmService.saveAsNewItem2(input);
		
		return response;
	}

	private void copyProperties(SaveAsNewItemInfo input)
	{
		TCComponentItemRevision itemRev = (TCComponentItemRevision)m_itemPropMap.getComponent();
		input.clientId = m_itemPropMap.getString("item_id");
		
		input.baseItemRevision = itemRev;
		input.name = m_itemPropMap.getString("object_name");
		input.newItemId = m_itemPropMap.getString("item_id");
		
		input.newRevId = m_itemPropMap.getString("item_revision_id");
		input.deepCopyInfo = createDeepCopyInfo();
		input.newItemMasterProperties = new MasterFormPropertiesInfo();
		
		input.newItemRevisionMasterProperties = new MasterFormPropertiesInfo();
		
	}

	private DeepCopyData[] createDeepCopyInfo()
	{
		DeepCopyData[] deepCopyData = new DeepCopyData[0];
		
		if(m_secondaryObjects != null && m_secondaryObjects.length > 0)
		{
			deepCopyData = new DeepCopyData[m_secondaryObjects.length];
			
			int iCount = 0;
			for(DeepCopyDataHelper prop : m_secondaryObjects)
			{
				deepCopyData[iCount++] = prop.getDeepCopyData_2008_06();
			}
		}
		
		return deepCopyData;
	}
	
}
