package com.nov.rac.utilities.services;

import java.util.Arrays;

import com.nov4.services.rac.bom.CompareBOMReportService;
import com.nov4.services.rac.bom._2010_09.CompareBOMReport;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.client.model.ErrorStack;

public class NOVCompareBomReportHelper 
{
	//BOM Compare Modes
	static public int BOM_compare_singlelevel 		= 0;
	static public int BOM_compare_lowestlevel 		= 1;
	static public int BOM_compare_multilevel 		= 2;
	
	//BOM Compare Output formats
	static public int BOM_compare_output_bomline 	= 1;
	static public int BOM_compare_output_userexit 	= 2;
	static public int BOM_compare_output_report	 	= 4;
	
	static public String[] reportArr;
	static public CompareBOMReport.Position[] positionArr;
	static public TCComponent[] dispCompArr;
	
	/* Function		: generateCompareBomReport()
	 * Return Type	: Void
	 * Parameters	: selcomp			Disposition Component array
	 * 				  reportMode		Report Mode
	 * 				  reportFormat		Report Format
	 * Purpose		: Generate BOM compare report for a list of Disposition components
	 */
	static public void generateCompareBomReport( TCComponent[] selcomp, int reportMode, int reportFormat) 
	{
		CompareBOMReport.CompareBomResponse theResponse = null;
		
		TCSession session = (TCSession)AIFUtility.getActiveDesktop().getCurrentApplication().getSession();	
		
		CompareBOMReportService  service = CompareBOMReportService.getService(session);
		
		CompareBOMReport.CompareBomInput input = new CompareBOMReport.CompareBomInput();
				
		input.dispComp = selcomp;
		input.reportMode = reportMode;
		input.reportFormat = reportFormat;
		
	    theResponse = service.compareBOMReport(input );	
	    dispCompArr = theResponse.compareOutput.dispComp;
	    reportArr = theResponse.compareOutput.report;   	
	    positionArr = theResponse.compareOutput.positionSpecifier;  
	    
	    if(theResponse.serviceData.sizeOfPartialErrors()>0) //Check for partial errors
	    {
	    	ErrorStack errorStack = theResponse.serviceData.getPartialError(0);	
	    	String errorMsg = (errorStack.getMessages())[0];
	    	MessageBox.post(errorMsg, "Cannot add Item to target", MessageBox.WARNING);			
	    }
	}
	
	/* Function		: getCompareBomReportStr()
	 * Return Type	: Void
	 * Parameters	: selcomp			Disposition Component 
	 * Purpose		: Get BOM compare report in HTML format for the specified Disposition component
	 */
	static public String getCompareBomReportStr( TCComponent selcomp )
	{
		int startIndex = 0;
		int rowCount = 0;
		int colCount = 0;
		int compIndex = Arrays.asList(dispCompArr).indexOf(selcomp);
		
		String reportStr = "";
		
		for(int i = 0; i<compIndex; i++)
		{
			startIndex = startIndex + (positionArr[i].nRows * positionArr[i].nCols);
		}
		
		//startIndex = startIndex;
		rowCount = positionArr[compIndex].nRows;
		colCount = positionArr[compIndex].nCols;
		
		for(int i = 0; i<rowCount; i++)
		{	
			if(reportArr[startIndex + (i*colCount + colCount-1)].equalsIgnoreCase("Added"))
			{
				reportStr = reportStr+"<TR bgcolor=\"#2EFE2E\">";
			}
			else if(reportArr[startIndex + (i*colCount + colCount-1)].equalsIgnoreCase("Removed"))	//#E03232
			{
				reportStr = reportStr+"<TR bgcolor=\"#E03232\">";
			}
			else if(reportArr[startIndex + (i*colCount + colCount-1)].equalsIgnoreCase("Revision"))	//#E03232
			{
				reportStr = reportStr+"<TR bgcolor=\"#FFBF00\">";
			}
			else
			{
				reportStr = reportStr+"<TR>";
			}
			for(int j = 0; j<colCount-1; j++)
			{
				
				if((j == colCount-2 ) && (reportArr[startIndex + (i*colCount + colCount-1)].equalsIgnoreCase("Quantity")))
				{
					reportStr = reportStr+"<TD><FONT COLOR=#E03232><center><strike>"+reportArr[startIndex + (i*colCount + j)]+"</strike></TD>";
				}
				else
				{
					reportStr = reportStr+"<TD><FONT COLOR=#153E7E><center>"+reportArr[startIndex + (i*colCount + j)]+"</TD>";
				}
			}
			
			reportStr = reportStr+"</TR>";
			
		}
		//reportStr = reportStr+"</table>";
		
		reportStr = "<FONT COLOR=#347C17><B><center>"+"BOM RESULT"+"</FONT>"
			+ "<table border=\"1\" cellspacing=\"0\"><TR><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Find No</B></TD><TD WIDTH=\"200\"><FONT COLOR=#153E7E><B><center>Item No</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Item Name</B></TD>"+
		    "<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Rev</B></TD><TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Status</B></TD>"+
			"<TD WIDTH=\"160\"><FONT COLOR=#153E7E><B><center>Quantity</B></TD></TR>"+reportStr+"</table>";
		
		return reportStr;
	}		
}
