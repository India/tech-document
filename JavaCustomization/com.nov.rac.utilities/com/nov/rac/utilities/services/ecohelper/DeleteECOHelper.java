package com.nov.rac.utilities.services.ecohelper;

import java.util.ArrayList;

import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.DeleteECOInput;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;

public class DeleteECOHelper 
{
	private ArrayList <ErrorStack> m_errors= new ArrayList<ErrorStack>();
	
	public void deleteChangeObject(TCComponent ecoForm, TCComponent[] targets) throws TCException
	{
		TCSession session = (TCSession) AIFUtility.getDefaultSession();
		
		NOV4ChangeManagementService service = NOV4ChangeManagementService.getService(session);
        
		DeleteECOInput deleteInput = new DeleteECOInput();
        
		deleteInput.ecoForm = ecoForm;
        deleteInput.targets = targets;
        
        ServiceData serviceData = service.deleteChangeObject(deleteInput);
        
        if(serviceData.sizeOfPartialErrors() > 0)
        {
        	ErrorStack errorStack = serviceData.getPartialError(0);
			
			TCException tcExp = new TCException(errorStack.getLevels(), errorStack.getCodes(), errorStack.getMessages());
			
			throw tcExp;
        }
	}
}
