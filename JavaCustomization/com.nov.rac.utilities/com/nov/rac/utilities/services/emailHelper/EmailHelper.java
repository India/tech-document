package com.nov.rac.utilities.services.emailHelper;

import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov4.services.rac.custom.NOV4MiscellaneousService;
import com.nov4.services.rac.custom._2010_09.NOV4Miscellaneous;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;


public class EmailHelper 
{

	private String m_subject= null;
	private String m_body= null;
	private String m_senderMailID= null;
	private String[] m_recipients=null;
	private String m_defaultServerName = null;
	private String m_defaultPortNo = null;
	private TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	private String m_senderDisplayName=null;
	
	
	public EmailHelper() 
	{
		m_defaultServerName = PreferenceHelper.getStringValue( "Mail_server_name", 
				                                               TCPreferenceService.TC_preference_site);
		
		m_defaultPortNo = PreferenceHelper.getStringValue( "Mail_server_port", 
					TCPreferenceService.TC_preference_site);
	}

	
	public void setSubject(String subject)
	{
		m_subject=subject;	 
	}
	
	public void setBody(String body)
	{
		m_body=body;
	}
	
	public void  addRecipient(String []  toMailid)
	{
		 m_recipients= toMailid;
	}	
	
	public void setSenderMailID(String  fromMailid)
	{
		m_senderMailID=fromMailid;
		
	}
	
	public void setServerName(String serverName)
	{
		m_defaultServerName=serverName;
	}
	
	public void setPortName(String portNo)
	{
		m_defaultPortNo=portNo;
	}
	
	public void setsenderDisplayName (String displayName)
	{
		m_senderDisplayName= displayName;
	}
	
	public void  sendEmail() throws TCException
 	{
		
	NOV4MiscellaneousService nov4MiscellaneousService = NOV4MiscellaneousService.getService(tcSession);

	NOV4Miscellaneous.SendEmailResponse response=null;
	
	NOV4Miscellaneous.SendEmailInputs emailInputs= new NOV4Miscellaneous.SendEmailInputs();

	emailInputs.toRecipients = m_recipients;
	emailInputs.mailBody= m_body;
	emailInputs.mailSubject=m_subject;
	emailInputs.senderMailID= m_senderMailID;
	emailInputs.senderDisplayName= m_senderDisplayName;
	emailInputs.serverName= m_defaultServerName;
	emailInputs.port = m_defaultPortNo;
	
	response = nov4MiscellaneousService.sendEmailOperation(emailInputs);	
	System.out.println("sTATUS = :" +response.response);
		
 	}
 	
 }
