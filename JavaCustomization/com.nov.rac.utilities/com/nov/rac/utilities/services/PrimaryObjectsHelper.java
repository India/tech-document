package com.nov.rac.utilities.services;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core._2007_06.DataManagement.ExpandGRMRelationsData;
import com.teamcenter.services.rac.core._2007_06.DataManagement.ExpandGRMRelationsResponse;
import com.teamcenter.services.rac.core._2007_06.DataManagement.RelationAndTypesFilter;
import com.teamcenter.services.rac.core._2007_06.DataManagement.RelationAndTypesFilter2;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2007_06.DataManagement.ExpandGRMRelationsPref;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsData2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsPref2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationsResponse2;
import com.teamcenter.services.rac.core._2007_09.DataManagement.ExpandGRMRelationship;

public class PrimaryObjectsHelper
{
    
    public TCComponent[] getPrimaryObjects(TCComponent [] secondaryObjects, String relationName, String[] objectTypeNames, boolean expItemRev)
    {
        TCComponent [] primaryObjects = null;
        //1.0 Invoke DataManagement.expandGRMRelationsForSecondary( secondaryObjects, expandGRMRelationsPref);
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        DataManagementService dmService = DataManagementService.getService(tcSession);
        
        RelationAndTypesFilter[] relationAndTypesFilterarr = new RelationAndTypesFilter[1];
        RelationAndTypesFilter relationAndTypesFilter = new RelationAndTypesFilter();
        relationAndTypesFilter.relationTypeName = relationName;
        //relationAndTypesFilter.otherSideObjectTypes = objectTypeNames;
        relationAndTypesFilterarr[0] = relationAndTypesFilter;
        
        ExpandGRMRelationsPref2 expandGRMRelationsPref = new ExpandGRMRelationsPref2();
        //expandGRMRelationsPref.expItemRev = expItemRev;
        expandGRMRelationsPref.info = relationAndTypesFilterarr;
        
        ExpandGRMRelationsResponse2 expandGRMRelationsResponse = dmService.expandGRMRelationsForSecondary(secondaryObjects, expandGRMRelationsPref);
        
        
        //2.0 Verify expandGRMRelationsResponse;
        
        if(expandGRMRelationsResponse.serviceData.sizeOfPartialErrors() > 0)
        {
            for(int inx = 0; inx < expandGRMRelationsResponse.serviceData.sizeOfPartialErrors(); inx++)
            System.out.println("Error  :  "  + expandGRMRelationsResponse.serviceData.getPartialError(inx).getMessages()[0]);
        }
        
        for(int inx = 0; inx < expandGRMRelationsResponse.output.length; inx++)
        {
            TCComponent comp = expandGRMRelationsResponse.output[inx].inputObject;
            System.out.println("Secondary ObjectType  :  " + comp.getDisplayType());
            
            ExpandGRMRelationsData2[] expandGRMRelationsData= expandGRMRelationsResponse.output[inx].relationshipData;
            
            for(int index = 0; index < expandGRMRelationsData.length; index++)
            {
                System.out.println("Relation name  :  " + expandGRMRelationsData[index].relationName);
                
                ExpandGRMRelationship[] expandGRMRelationship = expandGRMRelationsData[index].relationshipObjects;
                
                primaryObjects = new TCComponent[expandGRMRelationship.length];
                
                
                for(int index2 = 0; index2 < expandGRMRelationship.length; index2++)
                {
                        System.out.println("Primary ObjectType  :  " + expandGRMRelationship[index2].otherSideObject.getDisplayType());
                        primaryObjects[index2] = expandGRMRelationship[index2].otherSideObject;
                }
                 
            }
        }
        
        return primaryObjects;
    }
}
