package com.nov.rac.utilities.services.statusHelper;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.workflow.WorkflowService;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusInput;
import com.teamcenter.services.rac.workflow._2007_06.Workflow.ReleaseStatusOption;

public class ApplyStatusSOAHelper 
{
	 public  static void applyStatus(TCComponent inputObject,String status) 
    {
    	// 1. create ReleaseStatusInput object
    	ReleaseStatusInput[] releaseStatusInput = new ReleaseStatusInput[1];
    	releaseStatusInput[0] = new ReleaseStatusInput();
    	
    	// 2. Fill required data to ReleaseStatusInput object
    	ReleaseStatusOption[] releaseStatusOption = new ReleaseStatusOption[1];
    	releaseStatusOption[0]  = new ReleaseStatusOption();
    	releaseStatusOption[0].newReleaseStatusTypeName = status;
    	releaseStatusOption[0].operation ="Append";
    	releaseStatusOption[0].existingreleaseStatusTypeName ="";
    	
    	releaseStatusInput[0].objects = new TCComponent[1];
    	releaseStatusInput[0].objects[0] = inputObject;
    	releaseStatusInput[0].operations = releaseStatusOption;
    	
    	// 3. call servise 
    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
    	WorkflowService workflowService = WorkflowService.getService(tcSession);
    	try 
    	{
			workflowService.setReleaseStatus(releaseStatusInput);
			
		}
    	catch (ServiceException e)
    	{
    		e.printStackTrace();
    	}
        
    }
	 
	public  static void replaceStatus(TCComponent inputObject, String status) 
    {
    	// create ReleaseStatusInput objects
    	ReleaseStatusInput[] releaseStatusInput = new ReleaseStatusInput[2];
    	
    	// Create ReleaseStatusOption to delete existing status
    	ReleaseStatusOption rsOptionDelete = new ReleaseStatusOption();
    	rsOptionDelete.newReleaseStatusTypeName = "";
    	rsOptionDelete.operation ="Delete";
    	
    	// Create ReleaseStatusOption to append new status
    	releaseStatusInput[0] = new ReleaseStatusInput();
    	ReleaseStatusOption rsOptionAppend = new ReleaseStatusOption();
    	rsOptionAppend.newReleaseStatusTypeName = status;
    	rsOptionAppend.operation ="Append";
    	
    	releaseStatusInput[0] = new ReleaseStatusInput();
    	releaseStatusInput[0].objects = new TCComponent[] {inputObject};
    	releaseStatusInput[0].operations = new ReleaseStatusOption[] {rsOptionDelete};
    	
    	releaseStatusInput[1] = new ReleaseStatusInput();
    	releaseStatusInput[1].objects = new TCComponent[] {inputObject};
    	releaseStatusInput[1].operations = new ReleaseStatusOption[] {rsOptionAppend};
    	
    	// 3. call servise 
    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
    	WorkflowService workflowService = WorkflowService.getService(tcSession);
    	try 
    	{
			workflowService.setReleaseStatus(releaseStatusInput);
			
		}
    	catch (ServiceException e)
    	{
    		e.printStackTrace();
    	}
        
    }
	 
}
