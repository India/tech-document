package com.nov.rac.utilities.services;

import java.security.AllPermission;
import java.util.ArrayList;
import java.util.Collections;

import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.soa.client.model.ErrorStack;

public class SOAHelper 
{
	
	public static ArrayList<ErrorStack> getErrors(ServiceData serviceData, ArrayList<ErrorStack> errors)
	{
		ErrorStack[] allErrors = getErrors(serviceData);
		
	     errors = new ArrayList<ErrorStack>();
	     
	     Collections.addAll(errors, allErrors);
	     
	     return errors;
	}
	
	public static ErrorStack[] getErrors(ServiceData serviceData)
	{		
		int totalErrors = serviceData.sizeOfPartialErrors();
		ErrorStack [] errors= new ErrorStack[totalErrors];
		
		if(totalErrors>0)
		{
			for(int inx =0 ;inx < totalErrors; inx++)
			{
				errors [inx] = (serviceData.getPartialError(inx));
			}		
		}
		
		return errors;		
	}
	
	public static void raiseTCException(ArrayList<String> errorMessages) throws TCException
	{			
		if(errorMessages.size()>0)
		{
			String[] errorStrings= new String[1];
			errorStrings= errorMessages.toArray(errorStrings);
			
			throw (new TCException(errorStrings));
		}
		
	}
	
	

}
