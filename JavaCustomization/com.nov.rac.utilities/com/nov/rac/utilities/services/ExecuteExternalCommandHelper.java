package com.nov.rac.utilities.services;

import com.nov4.services.rac.custom.NOV4MiscellaneousService;
import com.nov4.services.rac.custom._2010_09.NOV4Miscellaneous;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;

public class ExecuteExternalCommandHelper 
{	
	public static final String MODE_SYNCHRONOUS = "SYNCHRONOUS";
	public static final String MODE_ASYNCHRONOUS = "ASYNCHRONOUS";
	
	public ExecuteExternalCommandHelper()
	{
		
	}
		
	public String executeExternalCommand(String szCommandLine, String operMode) throws TCException
	{
		String processID = null;
		
		NOV4Miscellaneous.NOV4ExternalCommandResponse response = null;
		
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		
		NOV4MiscellaneousService nov4MiscellaneousService = NOV4MiscellaneousService.getService(tcSession);
		
		NOV4Miscellaneous.NOV4ExternalCommandInput input = new NOV4Miscellaneous.NOV4ExternalCommandInput();		

		input.szCommandLine = szCommandLine;

		input.operationMode = operMode;
		
		response = nov4MiscellaneousService.executeExternalCommand(new NOV4Miscellaneous.NOV4ExternalCommandInput[] {input});
		
		if(response.externalCommandOutput.length > 0 )
		{
			 processID = response.externalCommandOutput[0].processID;
		}
		
		if( response.serviceData.sizeOfPartialErrors() > 0 )
		{
			ErrorStack errorStack = response.serviceData.getPartialError(0);
			
			TCException tcExp = new TCException(errorStack.getLevels(), errorStack.getCodes(), errorStack.getMessages());
			
			throw tcExp;
		}
		
		return processID;
	}

}
