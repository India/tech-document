package com.nov.rac.utilities.services.createItemHelper;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.utils.UOMHelper;
import com.nov4.services.rac.custom.NOV4DataManagementService;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.ContainerInfo;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateItemResponse;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.OperationInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.PropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.base.ErrorStack;

public class CreateItemSOAHandler extends AbstractHandler 
{
	// should change manually for every item create
	private String item_ID = "PP000000309";
	//private String item_ID = "M000000025";
	
    // server code
	private enum object_type
	{
		part,
		document,
		dataset,
		alternateid,
		classification,
		status
	};
	
	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException 
	{
		
		System.out.println("<<<<<<<<<<<**********   Create Item Handler Called  *************>>>>>>>>>>>>");
		
		// SOA call should go here!!!
		
		performOperation();
		
		return null;
	}
	
	private void performOperation()
	{
		// 1. create service stub for NOV4DataManagementService
		TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
		NOV4DataManagementService dmService = NOV4DataManagementService.getService(session);
		
		// 2. create OperationInput array
		Map<String, OperationInput[]> createInput = createInput();
		
			
		
		// 3. call createItem operation for creating objects
		CreateItemResponse response = dmService.createItem(createInput);
		
		
		Map<String, OperationInput[]> createInputForIdentifier = createInputForIdentiferFromClient();
		
		CreateItemResponse responseIdentifier = dmService.createItem(createInputForIdentifier);
		
		/*int sizeOfPartialErrors = response.servicedata.sizeOfPartialErrors();

		for(int i = 0; i<sizeOfPartialErrors ;i++)
		{
			com.teamcenter.soa.client.model.ErrorStack tempErrorStack =  response.servicedata.getPartialError(i);
			System.out.println(tempErrorStack.getMessages().toString());
		}*/
		  
	}
	private TCComponent getHomeFolder()
	{
		try
		{
			TCSession session = (TCSession) AIFUtility.getDefaultSession();
		
			return session.getUser().getHomeFolder();
			
		} catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	private Map<String, OperationInput[]> createInputForIdentiferFromClient()
	{
		item_ID = "PP000000315";
		Map<String, OperationInput[]> createInput = new HashMap<String, OperationInput[]>();
		// 5. OperationInput for AlternateId
		OperationInput[] altIDOpInput = createOperationInputForAltID();
		createInput.put( "alternateid", altIDOpInput);
		
		return createInput;
	}
	
	private Map<String, OperationInput[]> createInput()
	{
		Map<String, OperationInput[]> createInput = new HashMap<String, OperationInput[]>();
		
		// 1. OperationInput for Part
		/*OperationInput[] partOpInput = createOperationInputArry(createPropMapForPart());
		partOpInput[0].containerInfo = new ContainerInfo();
		partOpInput[0].containerInfo.targetObject = getHomeFolder();
		partOpInput[0].containerInfo.propertyName = "contents";
		createInput.put( "part", partOpInput);           // ( object_type.part.name(), partOpInput );
		
		
		
		
		// 2. OperationInput for Document
		/*OperationInput[] docOpInput = createOperationInputArry(createPropMapForDocument());
		docOpInput[0].containerInfo = new ContainerInfo();
		docOpInput[0].containerInfo.targetObject = getHomeFolder();
		docOpInput[0].containerInfo.propertyName = "contents";		  
		createInput.put( "document", docOpInput);
		
		
		// 3. OperationInput for Dataset
		OperationInput[] datasetOpInput = createOperationInputArry(createPropMapForDataset());
		createInput.put( "dataset", datasetOpInput);
		
		// 4. OperationInput for Status
		OperationInput[] statusOpInput = createOperationInputArry(createPropMapForStatus());
		createInput.put( "status", statusOpInput);
		
		
		// 5. OperationInput for AlternateId
		OperationInput[] altIDOpInput = createOperationInputForAltID();
		createInput.put( "alternateid", altIDOpInput);
		*/
		//6. OperationInput for classification
		/*OperationInput[] classificationOpInput = createOperationInputArry(createPropMapForClassification());
		createInput.put( "classification", classificationOpInput);

		//7. operationInput for revise
		OperationInput[] partReviseOpInput = createOperationInputArry(createPropMapForRevise());
		//partReviseOpInput[0].deepCopyData = new PropertyMap[]{createPropMapDeepCopyDatas()}; 
		createInput.put( "part", partReviseOpInput);
		 
		*/
		//8. operationInput for SaveAs
		OperationInput[] saveAsOpInput = createOperationInputArrySaveAs(createPropMapSaveAs());
		saveAsOpInput[0].containerInfo = new ContainerInfo();
		saveAsOpInput[0].containerInfo.targetObject = getHomeFolder();
		saveAsOpInput[0].containerInfo.propertyName = "contents";		  		
		createInput.put( "part", saveAsOpInput);	
		

		
		
		
		return createInput;
	}
	
	
   private OperationInput[] createOperationInputForAltID()
   {
		OperationInput[] opInput = new OperationInput[2];
		opInput[0] = new OperationInput();
		opInput[1] = new OperationInput();
		
		opInput[0].operationName =  "CreateObject";
		opInput[0].propertyMap = createPropMapForIdentifier();
		
		opInput[1].operationName =  "CreateObject";
		opInput[1].propertyMap = createPropMapForIdentifierRev();
		
		return opInput;
	}

	//**********  create OperationInput array for given propMap ***************//
	private OperationInput[] createOperationInputArry(PropertyMap inPropMap)
	{
		OperationInput[] opInput = new OperationInput[1];
		opInput[0] = new OperationInput();
		
		opInput[0].operationName =  "CreateObject";
		opInput[0].propertyMap = inPropMap;
		
		return opInput;
	}

	//**********  create OperationInput array for given propMap ***************//
	private OperationInput[] createOperationInputArrySaveAs(PropertyMap inPropMap)
	{
		OperationInput[] opInput = new OperationInput[1];
		opInput[0] = new OperationInput();
		
		opInput[0].operationName =  "SaveAsObject";
		opInput[0].propertyMap = inPropMap;
		
		return opInput;
	}	

//***********    Part property map   ****************//
	
	private PropertyMap createPropMapForPart()
	{
		PropertyMap propertyMap = new PropertyMap();
		
		propertyMap.boName = "Nov4Part";
	    
		// string prop map
		//Map<String, String> strMap  = new HashMap<String, String>();
		propertyMap.stringProps.put("item_id",item_ID);  // should change every time we invoke this class
		propertyMap.stringProps.put("object_name", "CreateItemSOA-TestPart");
		propertyMap.stringProps.put("object_desc", "Description Hello World CreateItemSOA-TestPart");
		
		// tag prop map
		//Map<String, TCComponent> tagMap  = new HashMap<String, TCComponent>();
		try 
		{
			propertyMap.tagProps.put("uom_tag", UOMHelper.getUOMObject("EA"));
		} 
		catch (TCException e)
		{
			e.printStackTrace();
		}
		
		// revision prop map
		PropertyMap revPropMap = new PropertyMap();
		
		revPropMap.boName = "Nov4Part Revision";
		revPropMap.stringProps.put("item_revision_id", "01");
		propertyMap.compoundPropInput.put("revision", new PropertyMap[]{revPropMap});
		
		// master form prop map
		propertyMap.compoundPropInput.put("IMAN_master_form", new PropertyMap[]{createPropMapForMasterForm()});
		
		
		return propertyMap;
	}
	
	private PropertyMap createPropMapForMasterForm()
	{
		PropertyMap mstrFormPropMap = new PropertyMap();
		
		mstrFormPropMap.boName = "Nov4Part Master";
		
		// string prop map
		//Map<String, String> strMap  = new HashMap<String, String>();
		
		mstrFormPropMap.stringProps.put("Name2","Name 2 = CreateItemSOA-TestPart");
		mstrFormPropMap.stringProps.put("rsone_itemtype","Make Item (Proprietary)");
		mstrFormPropMap.stringProps.put("nov4_mis_type","Engineering Items");
		
		mstrFormPropMap.stringProps.put("nov4_productline","002 - M02 DRILLING EQUIPMENT");
		mstrFormPropMap.stringProps.put("nov4_productcode","1111 - HANDLING EQUIPMENT         025");
		
		//mstrFormPropMap.stringProps.put("serialiseOption","None");
		
		return mstrFormPropMap;
	}
	
//***********    Document property map   ****************//
	
	private PropertyMap createPropMapForDocument()
	{
		PropertyMap docPropertyMap = new PropertyMap();
		
		docPropertyMap.boName = "Documents";
		
		//propertyMap.stringProps = new HashMap<String, String>();  // creating hashmap object
		String docItem_ID = item_ID + "-Doc";
		
		docPropertyMap.stringProps.put("item_id", docItem_ID);
		docPropertyMap.stringProps.put("object_name", "CreateItemSOA-TestDoc");
		docPropertyMap.stringProps.put("object_desc", "Description -- Hello World !!!  CreateItemSOA-TestDoc");
		
		docPropertyMap.stringProps.put("DocumentsCAT", "DAB");
		docPropertyMap.stringProps.put("DocumentsType", "Database");
		
		// revision prop map
		PropertyMap revPropMap = new PropertyMap();
		revPropMap.boName = "Documents Revision";
		revPropMap.stringProps.put("item_revision_id", "01");
		docPropertyMap.compoundPropInput.put("revision", new PropertyMap[]{revPropMap});
		
		// master form prop map
		PropertyMap mstrFormPropMap = new PropertyMap();
		mstrFormPropMap.boName = "Documents Revision Master";
		mstrFormPropMap.boolProps.put("PullDrawing", new BigInteger("1"));
		docPropertyMap.compoundPropInput.put("IMAN_master_form", new PropertyMap[]{mstrFormPropMap});
		
		return docPropertyMap;
	}
	
//***********    Dataset property map   ****************//
	
	private PropertyMap createPropMapForDataset()
	{
		PropertyMap datasetPropertyMap = new PropertyMap();
		
		datasetPropertyMap.boName = "MSWord";
		datasetPropertyMap.stringProps.put("dataset_type", "MSWord");
		datasetPropertyMap.stringProps.put("object_name", item_ID);

		return datasetPropertyMap;
	}

//***********    Status property map   ****************//
	
	private PropertyMap createPropMapForStatus()
	{
		PropertyMap statusPropertyMap = new PropertyMap();
		
		statusPropertyMap.boName = "ReleaseStatus";
		statusPropertyMap.stringProps.put("name", "Standard");

		return statusPropertyMap;
	}

	
//***********    alternate ID property map   ****************//
	
	private PropertyMap createPropMapForIdentifier()
	{
		PropertyMap identifierPropertyMap = new PropertyMap();
		
		identifierPropertyMap.boName = "Identifier";
		
		
		 TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("JDE");
		// properties tobe set here
		identifierPropertyMap.stringProps.put("object_name", item_ID );
		identifierPropertyMap.stringProps.put("object_desc", "CreateItemSOA-AlternateID");
		identifierPropertyMap.stringProps.put("idfr_id", item_ID );
		identifierPropertyMap.tagProps.put("idcontext", idContext );
		identifierPropertyMap.tagProps.put("suppl_context", null );
		//identifierPropertyMap.boolProps.put("disp_default", new BigInteger("1") );
		//identifierPropertyMap.boolProps.put("isAlternateID", new BigInteger("0") );
		return identifierPropertyMap;
	}
	
//***********    alternate ID Rev property map   ****************//
	
	private PropertyMap createPropMapForIdentifierRev()
	{
		PropertyMap identifierRevPropertyMap = new PropertyMap();
		
		identifierRevPropertyMap.boName = "IdentifierRev";
		
        TCComponentIdContext idContext = CreateAlternateIDHelper.getIdContext("JDE");
		
        identifierRevPropertyMap.stringProps.put("object_name", item_ID );
        identifierRevPropertyMap.stringProps.put("idfr_id", "01" );
        identifierRevPropertyMap.tagProps.put("idcontext", idContext );
        //identifierRevPropertyMap.tagProps.put("suppl_context", null );
        //identifierRevPropertyMap.boolProps.put("isAlternateID", new BigInteger("0") );
		return identifierRevPropertyMap;
	}
	
	// **************classification property map ***************//
	
	private PropertyMap createPropMapForClassification()
	{
		PropertyMap classificationPropertyMap = new PropertyMap();
		
		classificationPropertyMap.stringProps.put("cid", "642401");
		classificationPropertyMap.intProps.put("AttrID",new BigInteger("-630"));
		classificationPropertyMap.stringArrayProps.put("AttrValues",new String[] {"1"} );
		
		return classificationPropertyMap;
	}
	private TCComponent getTargetObject()
	{
		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
		TCComponent[] arr = null;
		TCComponentQueryType qt;

		try {
			qt = (TCComponentQueryType)tcSession.getTypeComponent("ImanQuery");
			TCComponentQuery gen = (TCComponentQuery)qt.find("Item...");
			arr = gen.execute(new String[]{"Item ID","Type"},new String[]{item_ID,"Nov4Part"});
			//arr = gen.execute(new String[]{"Item ID","Type"},new String[]{item_ID,"Documents"});
			
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		return arr[0];
	}
	private PropertyMap createPropMapForRevise()
	{
		PropertyMap propertyMap = new PropertyMap();
		
		propertyMap.boName = "Nov4Part";
	    
		// string prop map
		//Map<String, String> strMap  = new HashMap<String, String>();
		propertyMap.stringProps.put("item_id",item_ID);  // should change every time we invoke this class
		propertyMap.targetObject = getTargetObject();		

		// revision prop map
		PropertyMap revPropMap = new PropertyMap();
		
		revPropMap.boName = "Nov4Part Revision";
		revPropMap.targetObject = getItemRevision(getTargetObject());
		revPropMap.stringProps.put("item_revision_id", "02");
		revPropMap.stringProps.put("object_name", "CreateItemSOA-TestPart New Revision");
		revPropMap.stringProps.put("object_desc", "Description New Revision");
		propertyMap.compoundPropInput.put("revision", new PropertyMap[]{revPropMap});		
	

		return propertyMap;
	}
	
	private PropertyMap createPropMapDeepCopyDatas()
	{
		PropertyMap deepCopyMap = new PropertyMap();

		TCComponent[] dataset = getItemRevisionAttchedObject(getTargetObject());
		TCComponent docdataset = dataset[0];
		deepCopyMap.tagProps.put("attachedObject", docdataset);
		deepCopyMap.stringProps.put("propertyName", "IMAN_specification");
		deepCopyMap.stringProps.put("propertyType", "Relation");
		
		deepCopyMap.stringProps.put("copyAction", "CopyAsObject");
		deepCopyMap.boolProps.put("isTargetPrimary", new BigInteger("1"));
		deepCopyMap.boolProps.put("isRequired", new BigInteger("1"));
		deepCopyMap.boolProps.put("copyRelations", new BigInteger("1"));
		
		return deepCopyMap;
	}

	private PropertyMap createPropMapSaveAs()
	{
		PropertyMap propMapSaveAs = new PropertyMap();		
		try {
			
			TCSession thesession =  (TCSession) AIFUtility.getDefaultSession();
		
		
			//TCComponentType searchItem = thesession.getTypeComponent("Item");
			TCComponentType searchItem = thesession.getTypeComponent("Nov4Part");

		    if(searchItem instanceof TCComponentItemType)
		    {
		    	//TCComponentItem[] items = ((TCComponentItemType)(searchItem)).findItems("M000000021");
		    	TCComponentItem[] items = ((TCComponentItemType)(searchItem)).findItems(item_ID);
		    	
		    	if(items != null && items.length > 0)
		    	{
		    		//propertyMap.boName = "Item";
		    		propMapSaveAs.boName = "Nov4Part";
		    		propMapSaveAs.targetObject = items[0]; 
				    
					// string prop map
					//Map<String, String> strMap  = new HashMap<String, String>();
		    		propMapSaveAs.stringProps.put("object_name","SavedAs");  // should change every time we invoke this class		
		    		propMapSaveAs.stringProps.put("item_id","PP000000315");  // should change every time we invoke this class
					
		    		propMapSaveAs.tagProps.put("parentRevTag", items[0].getLatestItemRevision());
					TCComponentItemRevision latestItemRev =  items[0].getLatestItemRevision();
					
		    	}
		    	
		    }
		    

		
		} catch (TCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return propMapSaveAs;
	}

	private TCComponent[] getItemRevisionAttchedObject(TCComponent targetObject) 
	{
		TCComponent[] dataset = null; 
		
		try 
		{
			if(targetObject instanceof TCComponentItem)
			{
				
				dataset = (((TCComponentItem)targetObject).getLatestItemRevision()).getRelatedComponents("IMAN_specification");
				
			}
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dataset;

	}
	private TCComponent getItemRevision(TCComponent targetObject) 
	{
		TCComponent rev = null;
		
		try 
		{
			if(targetObject instanceof TCComponentItem)
			{
				
					rev = ((TCComponentItem)targetObject).getLatestItemRevision();
				
			}
		} 
		catch (TCException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rev;

	}
}
