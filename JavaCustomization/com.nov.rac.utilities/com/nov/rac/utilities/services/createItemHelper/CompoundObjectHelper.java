package com.nov.rac.utilities.services.createItemHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CompoundObject;
import com.teamcenter.rac.kernel.TCComponent;

public class CompoundObjectHelper
{
    private CompoundObject m_soaCompoundObject = null;
    
    public CompoundObjectHelper(CompoundObject soaCompoundObject)
    {
        m_soaCompoundObject = soaCompoundObject;
    }
    
    public TCComponent getComponent()
    {
        return m_soaCompoundObject.theObject;
    }
    
    public Map<String, CompoundObjectHelper[]> getCompoundObjects()
    {
        return getCompoundObjects( m_soaCompoundObject.compoundObjects );
        
    } // getCompoundObjects()
    
    public static Map<String, CompoundObjectHelper[]> getCompoundObjects( Map<String, CompoundObject[]> soaCompoundObjects )
    {
        Map<String, CompoundObjectHelper[]> compoundObjectMap = new  HashMap<String, CompoundObjectHelper[]>();
        
        Set<String> theKeySet = soaCompoundObjects.keySet();
        
        for(String relationName : theKeySet)
        {
            CompoundObject[] compoundObjects = (CompoundObject[]) soaCompoundObjects.get(relationName);
            
            if(compoundObjects != null && compoundObjects.length > 0)
            {
                CompoundObjectHelper[] childCompoundObjects = new CompoundObjectHelper[compoundObjects.length];
                
                for(int inx=0; inx<compoundObjects.length; inx++)
                {
                    childCompoundObjects[inx] = new CompoundObjectHelper( compoundObjects[inx] ); 
                }
                
                compoundObjectMap.put(relationName, childCompoundObjects);
                
            } //
            
        } // for(String relationName : theKeySet)
        
        return compoundObjectMap;
        
    } // getCompoundObjects()
    
    public static CompoundObjectHelper[] getCompoundObjects( CompoundObject[] soaCompoundObjectsArray )
    {           
        CompoundObjectHelper[] childCompoundObjects = null;
        
        CompoundObject[] compoundObjects = soaCompoundObjectsArray;
            
        if(compoundObjects != null && compoundObjects.length > 0)
        {
            childCompoundObjects = new CompoundObjectHelper[compoundObjects.length];
            
            for(int inx=0; inx<compoundObjects.length; inx++)
            {
                childCompoundObjects[inx] = new CompoundObjectHelper( compoundObjects[inx] ); 
            }
                
        } // if
        
        return childCompoundObjects;
        
    } // getCompoundObjects()

 
    
    public Map<String, TCComponent[]> getChildComponents()
    {
        Map<String, TCComponent[]> childComponentMap = new  HashMap<String, TCComponent[]>();
        
        Set<String> theKeySet = m_soaCompoundObject.compoundObjects.keySet();
        
        for(String relationName : theKeySet)
        {
            CompoundObject[] compoundObjects = (CompoundObject[]) m_soaCompoundObject.compoundObjects.get(relationName);
            
            if(compoundObjects != null && compoundObjects.length > 0)
            {
                TCComponent[] childComponentObjects = new TCComponent[compoundObjects.length];
                
                for(int inx=0; inx<compoundObjects.length; inx++)
                {
                    childComponentObjects[inx] = compoundObjects[inx].theObject; 
                }
                
                childComponentMap.put(relationName, childComponentObjects);
                
            } //
            
        } // for(String relationName : theKeySet)
        
        return childComponentMap;
        
    } // getCompoundObjects()
    
}

