package com.nov.rac.utilities.services.createItemHelper;

import java.util.HashMap;
import java.util.Map;

import com.nov.rac.utilities.services.createUpdateHelper.CreateAlternateIDHelper;
import com.nov.rac.utilities.utils.UOMHelper;
import com.nov4.services.rac.custom.NOV4DataManagementService;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.ContainerInfo;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateItemResponse;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.OperationInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.PropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;

public class CreateItemSOAHelper
{
    public static final String PART = "part";
    public static final String DOCUMENT = "document";
    public static final String DATASET = "dataset";
    public static final String ALTERNATE_ID = "alternateid";
    public static final String STATUS = "status";
    public static final String CLASSIFICATION = "classification";
    public static final String PART_DOC_RELATION = "part_doc_relation";
    
    
    public static CreateItemResponseHelper createItem( Map<String, OperationInputHelper[]> operationInput)
    {
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        
        NOV4DataManagementService dmService = NOV4DataManagementService.getService(tcSession);
        
        
        CreateItemResponse theResponse = dmService.createItem(getOperationInput(operationInput));
        
        CreateItemResponseHelper createItemResponseHelper = new CreateItemResponseHelper(theResponse);
        
        try
        {
            createItemResponseHelper.throwErrors();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        
       /* int errorCount = theResponse.servicedata.sizeOfPartialErrors();
        
        if(errorCount > 0)
        {
            ErrorStack errorStack = theResponse.servicedata.getPartialError(0);
            
            //for(ErrorStack errStack : errorStack)
            {
                System.out.println("======" + errorStack.getMessages()[0] + "========\n");    
            }
            
            
        }*/
        return createItemResponseHelper;
   
    }
    
    
    private static Map<String, OperationInput[]> getOperationInput(Map<String, OperationInputHelper[]> operationInput)
    {     
        Map<String, OperationInput[]> createItemInput = new HashMap<String, OperationInput[]>();
        
        for(String itemType:  operationInput.keySet())
        {
            OperationInputHelper[] opInputHelpers = operationInput.get(itemType);
            OperationInput[] operationInputs = new OperationInput[opInputHelpers.length];
            for(int inx = 0; inx < opInputHelpers.length; inx++ )
            {
                operationInputs[inx] = opInputHelpers[inx].getOperationInput();
            }
            createItemInput.put(itemType, operationInputs);
        }
        return createItemInput;
        
    }
    
  
}
