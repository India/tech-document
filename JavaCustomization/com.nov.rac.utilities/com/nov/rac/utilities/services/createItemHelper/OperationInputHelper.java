package com.nov.rac.utilities.services.createItemHelper;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.SOAPropertyMap;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.OperationInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.PropertyMap;
import com.teamcenter.rac.kernel.TCComponent;

public class OperationInputHelper
{   
    private OperationInput m_operationInput = null;
    
    public static String OPERATION_CREATE_OBJECT = "CreateObject";    
    public static String OPERATION_UPDATE_OBJECT = "UpdateObject";
    public static String OPERATION_REVISE_OBJECT = "ReviseObject";
    public static String OPERATION_SAVEAS_OBJECT = "SaveAsObject";
    public static String OPERATION_DELETE_OBJECT = "DeleteObject";
    
    public OperationInputHelper()
    {
        m_operationInput = new OperationInput();
    }
    
    public void setOperationMode(String operationMode)
    {
        m_operationInput.operationName = operationMode;
    }
    
    public void setObjectProperties(IPropertyMap propertyMap)
    {
        m_operationInput.propertyMap = getInternalPropertyMap(propertyMap);
    }
    
    protected PropertyMap getInternalPropertyMap(SOAPropertyMap soaPropertyMap)
    {
        return soaPropertyMap;
    }
    
    protected PropertyMap getInternalPropertyMap(IPropertyMap propertyMap)
    {
        SOAPropertyMap internalPropertyMap = new SOAPropertyMap(propertyMap);       
        return internalPropertyMap;
    }
    
    public void setClientID(String clientID)
    {
        m_operationInput.clienID = clientID;
    }
    
    public void setParentObject(TCComponent container)
    {
        m_operationInput.containerInfo.targetObject = container;
    }
    
    public void setRelation(String relation)
    {
        m_operationInput.containerInfo.propertyName = relation;
    }
    
    public void setContainerInfo(TCComponent container, String relation, Boolean relate)
    {
        setContainerInfo(container, relation);
        m_operationInput.containerInfo.relate = relate;
    }
    
    public void setContainerInfo(TCComponent container, String relation)
    {
        m_operationInput.containerInfo.targetObject = container;
        m_operationInput.containerInfo.propertyName = relation;
    }
    
    public void setDeepCopyData(IPropertyMap[] propertyMaps)
    {
        if(propertyMaps != null && propertyMaps.length > 0)
        {
            PropertyMap[] objPropertyMaps = new SOAPropertyMap[propertyMaps.length];
            
            for(int inx = 0; inx < propertyMaps.length; inx++)
            {
                objPropertyMaps[inx] = getInternalPropertyMap( propertyMaps[inx] );
            }
            
            m_operationInput.deepCopyData = objPropertyMaps;            
        }
    }
    
    
    public OperationInput getOperationInput()
    {
        return m_operationInput;
    }

  
}
