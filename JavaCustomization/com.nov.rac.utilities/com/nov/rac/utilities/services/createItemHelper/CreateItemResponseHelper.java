package com.nov.rac.utilities.services.createItemHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CompoundObject;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateItemResponse;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.soa.client.model.ErrorStack;

public class CreateItemResponseHelper
{
    
    public static final String PART = "part";
    public static final String DOCUMENT = "document";
    public static final String DATASET = "dataset";
    public static final String ALTERNATE_ID = "alternateid";
    public static final String STATUS = "status";
    public static final String CLASSIFICATION = "classification";
    public static final String PART_DOC_RELATION = "part_doc_relation";
    
    ServiceData serviceData = null;
    String clientId = "";
    CompoundObject[] objects = null;
    CreateItemResponse response = null;
    
    public CreateItemResponseHelper(CreateItemResponse theResponse)
    {
        this.response = theResponse;
    }  
   
    public String getClientId()
    {
        clientId = response.clientId;
        return clientId;
    }
    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }  
    
    public CompoundObjectHelper[] getCompoundObject(String objectKey)
    {
        CompoundObjectHelper[] compoundObjects = null;
        
        Map<String, CompoundObject[]> soaCompoundObjects = response.objects;
        
        CompoundObject[] requiredCompoundObjects = soaCompoundObjects.get(objectKey);
        
        compoundObjects = CompoundObjectHelper.getCompoundObjects(requiredCompoundObjects);
        
        return compoundObjects;
    }
    
    public Map getCompoundObjects(CompoundObject compoundObject)
    {
        return compoundObject.compoundObjects;
    }

    public void throwErrors() throws TCException
    {
        ArrayList<ErrorStack> errorStacks = new ArrayList<ErrorStack>();
        
        ServiceData theServiceData = response.servicedata;        
        int totalErrors = theServiceData.sizeOfPartialErrors();
        
        if (totalErrors > 0)
        {
            for (int i = 0; i < totalErrors; i++)
            {
                errorStacks.add(theServiceData.getPartialError(i));
            }
            
        }
        
        ArrayList<String> errorMessages = new ArrayList<String>();
        ArrayList<Integer> errorCodes = new ArrayList<Integer>();
        
       
        for (ErrorStack errorStack : errorStacks)
        {
            Collections.addAll(errorMessages, errorStack.getMessages());
        }
               
        if (errorMessages.size() > 0)
        {
            String[] errorStrings = new String[1];
            errorStrings = errorMessages.toArray(errorStrings);
            
            throw (new TCException(errorStrings));
        }
        
    }
    
    
    
}
