package com.nov.rac.utilities.services.revisehelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2008_06.DataManagement;
import com.teamcenter.services.rac.core._2008_06.DataManagement.PropertyNameValueInfo;
import com.teamcenter.services.rac.core._2008_06.DataManagement.ReviseInfo;
import com.teamcenter.soa.client.model.ErrorStack;

public class ReviseObjectsSOAHelper
{
    /** The m_errors. */
    private ArrayList<ErrorStack> m_errors = new ArrayList<ErrorStack>();
    
    public void reviseObjects(List<IPropertyMap> reviseObjectList)
    {
        //1.1 Create an array of DataManagement.ReviseInfo [] 
        DataManagement.ReviseInfo [] reviseInfo = new DataManagement.ReviseInfo[reviseObjectList.size()];
     
        //1.2 For each IPropertyMap object create a ReviseInfo object.
        int i=0;
        for(IPropertyMap iPropertyMap : reviseObjectList)
        {
            reviseInfo[i] = new ReviseInfo();
            copyPropertyMapToReviseInfo(iPropertyMap,reviseInfo[i]);
            i++;
        }
        
        
        //2.0 Invoke DataManagement.revise2( reviseInfo []);
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        DataManagementService dmService = DataManagementService.getService(tcSession);
        
        Date start = new Date();
        System.out.println("reviseObjects: startTime " + start.toString());
        
        DataManagement.ReviseResponse2 serviceResponse = dmService.revise2(reviseInfo);
        
        Date end = new Date();
        System.out.println("reviseObjects: endTime " + end.toString());
        
        ServiceData serviceData = serviceResponse.serviceData;
        addErrors(serviceData);
        //serviceResponse.
    }
    
    
    public void reviseObjects(IPropertyMap [] reviseObjectArray)
    {
        reviseObjects(Arrays.asList(reviseObjectArray));
    }
        
    private  void copyPropertyMapToReviseInfo(IPropertyMap inMap,ReviseInfo outMap)
    {
        outMap.clientId = inMap.getString("item_id");
        outMap.newRevId = inMap.getString("item_revision_new_id");
        outMap.name = inMap.getString("object_name");
        outMap.description = inMap.getString("object_desc").trim();
        outMap.baseItemRevision = (TCComponentItemRevision) inMap.getComponent();
        
        IPropertyMap inCompundMap = inMap.getCompound("IMAN_master_form_rev");
        
        if(inCompundMap != null)
        {        	
        	List<PropertyNameValueInfo> propNameValueInfoList = copyPropertyMapToNameValueInfo(inCompundMap);
        	
        	outMap.newItemRevisionMasterProperties.propertyValueInfo = propNameValueInfoList.toArray(new PropertyNameValueInfo[0]); 
        }
    }
    
    private List<PropertyNameValueInfo> copyPropertyMapToNameValueInfo(IPropertyMap inCompundMap)
    {
    	List<PropertyNameValueInfo> outputPropNameValueInfoList = new ArrayList<PropertyNameValueInfo>();
    	 
    	//Copy String Properties...
    	Set<String> propNames = inCompundMap.getStringPropNames();
    	
    	if(!propNames.isEmpty())
    	{
    		for(String propName : propNames)
    		{
    			String propValue = inCompundMap.getString(propName);
    			
    			PropertyNameValueInfo propNameValueInfo = new PropertyNameValueInfo();    			
    			propNameValueInfo.propertyName = propName;
    			propNameValueInfo.propertyValues = new String[] {propValue};
    			
    			outputPropNameValueInfoList.add(propNameValueInfo);
    		}
    	} // 
    	
    	return outputPropNameValueInfoList;
	}

	public void throwErrors() throws TCException 
    {
        
        ArrayList<String> errorMessages = new ArrayList<String>();
        
//        if(m_errors.size() > 0)
//        {
//            Collections.addAll(errorMessages, "Selected items can not be revised.\n");
//        }
        for (int i = 0; i < m_errors.size(); i++)
        {
            for (ErrorStack errorStack : m_errors)
            {
            	Collections.addAll(errorMessages, errorStack.getClientId());
                Collections.addAll(errorMessages, errorStack.getMessages());
            }
        }
   
        if (errorMessages.size() > 0)
        {
            String[] errorStrings = new String[1];
            errorStrings = errorMessages.toArray(errorStrings);
            
            //MessageBox.post(new TCException(errorStrings));
            throw new TCException(errorStrings);
        }
    }
    
    public  void addErrors(ServiceData serviceData)
    {
        int totalErrors = serviceData.sizeOfPartialErrors();
        if(totalErrors>0)
        {
            for(int i =0 ;i < totalErrors; i++)
            {
                m_errors.add(serviceData.getPartialError(i));
            }
        
        }
    }
        
}
