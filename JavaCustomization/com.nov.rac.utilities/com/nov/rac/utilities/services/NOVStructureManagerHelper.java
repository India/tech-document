package com.nov.rac.utilities.services;

import java.util.Map;

import com.nov4.services.rac.bom.NOV4StructureManagerService;
import com.nov4.services.rac.bom._2010_09.NOV4StructureManager;
import com.nov4.services.rac.bom._2010_09.NOV4StructureManager.ReplaceSupercededInput;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;

public class NOVStructureManagerHelper {

	public void replaceSupercededItem() throws TCException {


		ReplaceSupercededInput replaceSupercededIn = new ReplaceSupercededInput();
		replaceSupercededIn.targetRevision = m_targetRevision;
		replaceSupercededIn.bomViewRevision = m_bvrTag;
		replaceSupercededIn.targetToSurvivorMap = m_targToSurvivor;

		TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();

		NOV4StructureManagerService nov4StructureManagerService = NOV4StructureManagerService
				.getService(tcSession);
		
		ServiceData response = nov4StructureManagerService
				.replaceSupercededItem(replaceSupercededIn);			
		handleError(response);

	}

	public void setTargetRevision(TCComponent targetObject) {
		m_targetRevision = targetObject;
	}

	public void setBvrTag(TCComponent bvrTag) {
		m_bvrTag = bvrTag;
	}

	public void setTargetToSurvivorMap(
			Map<TCComponent, TCComponent> targetToSurvivor) {
		m_targToSurvivor = targetToSurvivor;
	}

	void handleError(ServiceData response) throws TCException {
		if (response.sizeOfPartialErrors() > 0) {
			ErrorStack errorStack = response.getPartialError(0);

			int[] errorCodes = errorStack.getCodes();
			int[] errorLevels = errorStack.getLevels();
			String[] errorMessages = errorStack.getMessages();

			throw new TCException(errorCodes, errorLevels, errorMessages);
		}
	}

	TCComponent m_targetRevision;
	TCComponent m_bvrTag;
	Map<TCComponent, TCComponent> m_targToSurvivor;

};