package com.nov.rac.utilities.services.deleteHelper;

import java.util.ArrayList;

import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.soa.client.model.ErrorStack;

public class DeleteObjectHelper {

	public static int OPERATION_DELETE_RELATION=0;
	
	private int				m_operationMode = -1;	
	private TCComponent 	m_SecondaryObject = null;
	private TCComponent		m_PrimaryObject = null;
	private String			m_RelationName = null;			
	
	private ArrayList <ErrorStack> m_errors= new ArrayList<ErrorStack>();
	
	public DeleteObjectHelper(int operationMode) {
		m_operationMode = operationMode;
	}
	
	public void addErrors(ServiceData serviceData)
	{
		int totalErrors = serviceData.sizeOfPartialErrors();
		if(totalErrors>0)
		{
			for(int i =0 ;i < totalErrors; i++)
			{
				m_errors.add(serviceData.getPartialError(i));
			}
		
		}
	}
	
	public TCComponent getSecondaryObject() {
		return m_SecondaryObject;
	}

	public void setSecondaryObject(TCComponent m_SecondaryObject) {
		this.m_SecondaryObject = m_SecondaryObject;
	}

	public TCComponent getPrimaryObject() {
		return m_PrimaryObject;
	}

	public void setPrimaryObject(TCComponent m_PrimaryObject) {
		this.m_PrimaryObject = m_PrimaryObject;
	}

	public String getRelationName() {
		return m_RelationName;
	}

	public void setRelationName(String m_RelationName) {
		this.m_RelationName = m_RelationName;
	}

	public int getOperationMode() {
		return m_operationMode;
	}

	public ArrayList<ErrorStack> getErrors() 
	{
		return m_errors;
	}
	
	
}
