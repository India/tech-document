package com.nov.rac.utilities.services.deleteHelper;

import java.util.ArrayList;
import java.util.Collections;

import com.nov.rac.utilities.services.createUpdateHelper.CreateObjectsSOAHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.soa.client.model.ErrorStack;

public class DeleteObjectOperation
{
	
	private DeleteObjectHelper[] m_transferObject;

	public DeleteObjectOperation(DeleteObjectHelper transferObject)
	{
		m_transferObject = new DeleteObjectHelper[] {transferObject};
	}
	
	public void executeOperation() throws TCException 
	{
		
		CreateObjectsSOAHelper.deleteRelationObjects(m_transferObject);
		handleErrors();

	}
	
	private void handleErrors() throws TCException
	{
		ArrayList<String> errorMessages = new ArrayList<String>();
				
		for (int i=0;i<m_transferObject.length;i++) {
			for(ErrorStack errorStack:m_transferObject[i].getErrors()) {
				Collections.addAll(errorMessages, errorStack.getMessages());
			}
		}
		
		if(errorMessages.size()>0)
		{
			String[] errorStrings= new String[1];
			errorStrings= errorMessages.toArray(errorStrings);
			
			//MessageBox.post(new TCException(errorStrings));
			throw ( new TCException(errorStrings) );
		}
	}
}
