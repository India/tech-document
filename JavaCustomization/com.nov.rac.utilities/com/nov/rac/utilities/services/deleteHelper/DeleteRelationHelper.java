package com.nov.rac.utilities.services.deleteHelper;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2006_03.DataManagement.Relationship;
import com.teamcenter.soa.client.model.ErrorStack;

public class DeleteRelationHelper
{
	private TCComponent []m_primaryObjet;
    private TCComponent []m_secondaryObjet;
    private String [] m_relationType;

    public DeleteRelationHelper(TCComponent primaryObjet, TCComponent secondaryObjet, String relationType)
    {
        m_primaryObjet = new TCComponent[] {primaryObjet};
        m_secondaryObjet = new TCComponent[] {secondaryObjet};
        m_relationType = new String[] {relationType};
    }

    public DeleteRelationHelper(TCComponent[] primaryObjet, TCComponent [] secondaryObjet, String [] relationType)
    {
        m_primaryObjet = primaryObjet;
        m_secondaryObjet = secondaryObjet;
        m_relationType = relationType;
    }

    
    public void deleteRelation() throws TCException
    {
        // 1.0 create input param i.e. Relationship
    	int theSize = m_primaryObjet.length;

        Relationship[] relationship = new Relationship[theSize ];

        for(int inx=0; inx< theSize; inx++)
        {
            // 1.0 create input param i.e. Relationship 
            relationship[inx] = new Relationship();
        
            relationship[inx].primaryObject = m_primaryObjet[inx];
            relationship[inx].secondaryObject = m_secondaryObjet[inx];
            relationship[inx].relationType = m_relationType[inx];
        }
        
        // 2.0 call SOA to delete relation
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        DataManagementService dmService = DataManagementService.getService(tcSession);
        ServiceData response = dmService.deleteRelations(relationship);

        // 3.0 check for errors
        handleError(response);
    }
    
    void handleError( ServiceData response ) throws TCException
    {
        if(response.sizeOfPartialErrors() > 0)
        {
            ErrorStack errorStack = response.getPartialError(0);
            
            int[] errorCodes = errorStack.getCodes();
            int[] errorLevels = errorStack.getLevels();
            String[] errorMessages = errorStack.getMessages();
            
            throw new TCException(errorCodes, errorLevels, errorMessages);
        }
    }
    
}
