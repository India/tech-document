package com.nov.rac.utilities.services.ClassificationHelper;

import com.nov4.services.rac.classification._2010_09.NOV4Classification.AttributeValues;
import com.nov4.services.rac.classification._2010_09.NOV4Classification.GetICOPropertiesOutput;
import com.teamcenter.rac.kernel.TCComponent;

public class ICOObjectHelper
{
    TCComponent m_inputICO = null;
    AttributePropertiesHelper[] m_allAttributes = null;
    String m_classID=null;
    
    ICOObjectHelper(GetICOPropertiesOutput output)
    {
        m_inputICO = output.icoObject;
        AttributeValues[] attributeValues = output.attributesValues;
        m_classID =output.classID;
        
        if (attributeValues != null && attributeValues.length > 0)
        {
            m_allAttributes = new AttributePropertiesHelper[attributeValues.length];
            for (int i = 0; i < attributeValues.length; i++)
            {
                AttributePropertiesHelper attributePropertyHelper = new AttributePropertiesHelper();
                
                attributePropertyHelper
                        .setattributeID(attributeValues[i].attributeID);
                attributePropertyHelper
                        .settAttributeName(attributeValues[i].attributeName);
                attributePropertyHelper
                        .setShortName(attributeValues[i].shortName);
                attributePropertyHelper
                        .setAnnotation(attributeValues[i].annotation);
                attributePropertyHelper.setFormat(attributeValues[i].format);
                attributePropertyHelper
                        .setMetricUnit(attributeValues[i].metricUnit);
                attributePropertyHelper
                        .setNonMetricUnit(attributeValues[i].nonMetricUnit);
                attributePropertyHelper
                        .setMetricValues(attributeValues[i].metricValues);
                attributePropertyHelper
                        .setNonMetricValues(attributeValues[i].nonMetricValues);
                m_allAttributes[i] = attributePropertyHelper;
                
            }
        }
    }
    
    public TCComponent getICOObject()
    {
        return m_inputICO;
    }
    
    public AttributePropertiesHelper[] getAllAttributes()
    {
        return m_allAttributes;
    }
    public String getClassID()
    {
        return m_classID;
    }
}
