package com.nov.rac.utilities.services.ClassificationHelper;

import java.util.Arrays;

public class AttributePropertiesHelper
{
    public final static String PROP_NAME = "name";
    public final static String PROP_METRIC_VALUES = "metricValues";
    public final static String PROP_METRIC_UNIT = "metricUnits";
    public final static String PROP_NON_METRIC_VALUES = "nonMetricValues";
    public final static String PROP_NON_METRIC_UNIT = "nonMetricUnits";
    public final static String PROP_ATTRIBUTE_ID = "attributeId";
    public final static String PROP_SHORTNAME = "shortName";
    public final static String PROP_ANNOTATION = "annotation";
    public final static String PROP_FORMAT = "format";
    
    
    private int m_AttributeID;
    private String m_AttributeName;
    private String m_ShortName;
    private String m_Annotation;
    private int m_Format;
    private String m_MetricUnit;
    private String m_NonMetricUnit;
    private String[] m_MetricValues;
    private String[] m_NonMetricValues;
    
    public int getattributeID()
    {
        return m_AttributeID;
    }
    
    public String getAttributeName()
    {
        return m_AttributeName;
    }
    
    public String getShortName()
    {
        return m_ShortName;
    }
    
    public String getAnnotation()
    {
        return m_Annotation;
    }
    
    public int getFormat()
    {
        return m_Format;
    }
    
    public String getMetricUnit()
    {
        return m_MetricUnit;
    }
    
    public String getNonMetricUnit()
    {
        return m_NonMetricUnit;
    }
    
    public String getMetricValues()
    {
        String retStr = Arrays.toString(m_MetricValues);
        retStr = retStr.substring(1, retStr.length()-1);
        return retStr;
        
    }
    
    public String getNonMetricValues()
    {
        String retStr = Arrays.toString(m_NonMetricValues);
        retStr = retStr.substring(1, retStr.length()-1);
        return retStr;
        
    }
    
    public void setattributeID(int attributeID)
    {
        m_AttributeID = attributeID;
    }
    
    public void settAttributeName(String attributeName)
    {
        m_AttributeName = attributeName;
    }
    
    public void setShortName(String shortName)
    {
        m_ShortName = shortName;
    }
    
    public void setAnnotation(String annotation)
    {
        m_Annotation = annotation;
    }
    
    public void setFormat(int format)
    {
        m_Format = format;
    }
    
    public void setMetricUnit(String metricUnit)
    {
        m_MetricUnit = metricUnit;
    }
    
    public void setNonMetricUnit(String nonMetricUnit)
    {
        m_NonMetricUnit = nonMetricUnit;
    }
    
    public void setMetricValues(String[] metricValues)
    {
        m_MetricValues = metricValues;
        
    }
    
    public void setNonMetricValues(String[] nonMetricValues)
    {
        m_NonMetricValues = nonMetricValues;
        
    }
    
}
