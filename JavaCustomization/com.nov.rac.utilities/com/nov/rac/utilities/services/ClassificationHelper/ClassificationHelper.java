package com.nov.rac.utilities.services.ClassificationHelper;

import java.util.HashMap;
import java.util.Map;

import com.nov4.services.rac.classification.NOV4ClassificationService;
import com.nov4.services.rac.classification._2010_09.NOV4Classification.GetICOPropertiesOutput;
import com.nov4.services.rac.classification._2010_09.NOV4Classification.GetICOPropertiesResponse;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.classification.ClassificationService;
import com.teamcenter.services.rac.classification._2007_01.Classification.ClassDef;
import com.teamcenter.services.rac.classification._2007_01.Classification.GetClassDescriptionsResponse;
import com.teamcenter.services.rac.classification._2007_01.Classification.GetParentsResponse;

public class ClassificationHelper
{
    
    public static ICOObjectHelper[] getICOProperties(TCComponent[] inputICOs)
            throws TCException
    {
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        // Map iCO_AllPropertiesMap = new HashMap<TCComponent,>
        NOV4ClassificationService classification = NOV4ClassificationService
                .getService(tcSession);
        GetICOPropertiesResponse response = null;
        ICOObjectHelper[] retObj =null;
        if (inputICOs.length != 0)
        {
            response = classification.getICOProperties(inputICOs);
            
            if (response != null)
            {
                GetICOPropertiesOutput[] output = response.icoObjectProperties;
                
                if (output != null && output.length != 0)
                {
                    retObj = new ICOObjectHelper[output.length];
                    for (int i = 0; i < output.length; i++)
                    {
                        ICOObjectHelper iCOObjectHelper = new ICOObjectHelper(
                                output[i]);
                        retObj[i] = iCOObjectHelper;
                    }
                }
            }
        }
        
        return retObj;
    }
    
    public static Map getClassHierarchy(String classID[]) throws ServiceException
    {
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        ClassificationService classification =  ClassificationService.getService(tcSession) ;
        
        GetParentsResponse response = classification.getParents(classID);
        return response.parents;
        
    }
    
    public static Map getClassNames(String classID[]) throws ServiceException
    {
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        ClassificationService classification =  ClassificationService.getService(tcSession) ;
        
        GetClassDescriptionsResponse response =  classification.getClassDescriptions(classID);
        Map descMap = response.descriptions;
        HashMap<String, String> retMap = new HashMap<String,String>();
        for (String id : classID)
        {
           ClassDef classDef = (ClassDef) descMap.get(id);
           retMap.put(id, classDef.name);
        }
        
       return retMap;
        
    }
}
