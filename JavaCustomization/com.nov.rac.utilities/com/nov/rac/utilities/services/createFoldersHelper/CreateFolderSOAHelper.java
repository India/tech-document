package com.nov.rac.utilities.services.createFoldersHelper;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2006_03.DataManagement.CreateFolderInput;

public class CreateFolderSOAHelper {

	public static void createProductFolders(TCComponent part , String[] folders) throws TCException
    {
        TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
       
        CreateFolderInput[] createFolderInput = new CreateFolderInput[folders.length];
        for(int i=0; i < folders.length; i++)
        {
            createFolderInput[i] = new CreateFolderInput();
            String folderDetails[] = folders[i].split("~");
            createFolderInput[i].name = folderDetails[0];
            createFolderInput[i].desc = folderDetails[2];
        }
       
        DataManagementService dmService = DataManagementService
                .getService(session);
        com.teamcenter.services.rac.core._2006_03.DataManagement.CreateFoldersResponse createFolderResponse = dmService
                .createFolders(createFolderInput,
                        part, "IMAN_reference");
        
    }
}
