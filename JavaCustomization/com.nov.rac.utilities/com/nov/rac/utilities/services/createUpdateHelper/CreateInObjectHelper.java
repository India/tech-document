package com.nov.rac.utilities.services.createUpdateHelper;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.propertymap.PropertyMapHelper;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CompoundObject;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateIn;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateOrUpdateInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateOrUpdateResponse;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.soa.client.model.ErrorStack;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateInObjectHelper.
 */
public class CreateInObjectHelper implements IPropertyMap
{
    
    /** The OPERATIO n_ create. */
    public static OperationModes OPERATION_CREATE = OperationModes.ENUM_OPERATION_CREATE;
    
    /** The OPERATIO n_ update. */
    public static OperationModes OPERATION_UPDATE = OperationModes.ENUM_OPERATION_UPDATE;
    
    /** The SE t_ creat e_ in. */
    public static int SET_CREATE_IN = 0;
    
    /** The SE t_ property. */
    public static int SET_PROPERTY = 1;
    
    /** The m_ create in obj. */
    private CreateIn m_CreateInObj = null;
    
    /** The m_ create or update input. */
    private CreateOrUpdateInput m_CreateOrUpdateInput = null;
    
    /** The m_create response. */
    private CreateOrUpdateResponse m_createResponse = null;
    
    /** The m_create output. */
    private CompoundObject m_createOutput = null;
    
    /** The m_ target object. */
    private TCComponent m_TargetObject = null;
    
    /** The m_errors. */
    private ArrayList<ErrorStack> m_errors = new ArrayList<ErrorStack>();
    
    /**
     * ********************************************.
     */
    /* Internal enum class to encapsulate the mode */
    /***********************************************/
    // Kishor Ahuja
    public enum OperationModes
    {
        
        /** The ENU m_ operatio n_ unknown. */
        ENUM_OPERATION_UNKNOWN(0),
        /** The ENU m_ operatio n_ create. */
        ENUM_OPERATION_CREATE(1),
        /** The ENU m_ operatio n_ update. */
        ENUM_OPERATION_UPDATE(2);
        
        /** The m_mode. */
        private int m_mode = 0;
        
        /**
         * Instantiates a new operation modes.
         * 
         * @param mode
         *            the mode
         */
        OperationModes(int mode)
        {
            m_mode = mode;
        }
        
        /**
         * Gets the mode.
         * 
         * @return the mode
         */
        public int getMode()
        {
            return m_mode;
        }
    };
    
    /**
     * Instantiates a new creates the in object helper.
     * 
     * @param boTypeName
     *            the bo type name
     * @param operationMode
     *            the operation mode
     */
    public CreateInObjectHelper(String boTypeName, OperationModes operationMode)
    {
        
        m_CreateOrUpdateInput = new CreateOrUpdateInput();
        
        m_CreateOrUpdateInput.operationMode = operationMode.getMode();
        
        m_CreateInObj = new CreateIn();
        
        m_CreateInObj.data.boName = boTypeName;
        
        m_CreateOrUpdateInput.createIn = m_CreateInObj;
        
    }
    
    /**
     * Instantiates a new creates the in object helper.
     * 
     * @param boTypeName
     *            the bo type name
     * @param operationMode
     *            the operation mode
     * @param propMap
     *            the prop map
     */
    public CreateInObjectHelper(String boTypeName, OperationModes operationMode, IPropertyMap propMap)
    {
        this(boTypeName, operationMode);
        addAll(propMap);
    }
    
    /**
     * Sets the string props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setStringProps(String valName, String value, int whereToAdd)
    {
        setString(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setString(java.lang.String,
     * java.lang.String)
     */
    public void setString(String valName, String value)
    {
            m_CreateInObj.data.stringProps.put(valName, value);
    }
    
    /**
     * Sets the string array props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setStringArrayProps(String valName, String value[], int whereToAdd)
    {
        setStringArray(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#setStringArray(java.lang.String,
     * java.lang.String[])
     */
    public void setStringArray(String valName, String value[])
    {
            m_CreateInObj.data.stringArrayProps.put(valName, value);
    }
    
    /**
     * Sets the double props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setDoubleProps(String valName, Double value, int whereToAdd)
    {
        setDouble(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setDouble(java.lang.String,
     * java.lang.Double)
     */
    public void setDouble(String valName, Double value)
    {
          m_CreateInObj.data.doubleProps.put(valName, value);
    }
    
    /**
     * Sets the double array props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setDoubleArrayProps(String valName, Double value[], int whereToAdd)
    {
        setDoubleArray(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#setDoubleArray(java.lang.String,
     * java.lang.Double[])
     */
    public void setDoubleArray(String valName, Double value[])
    {
            m_CreateInObj.data.doubleArrayProps.put(valName, value);
    }
    
    /**
     * Sets the int props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setIntProps(String valName, int value, int whereToAdd)
    {
        Integer valueObj = new Integer(value);
        
        setInteger(valName, valueObj);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setInteger(java.lang.String,
     * java.lang.Integer)
     */
    public void setInteger(String valName, Integer value)
    {
            m_CreateInObj.data.intProps.put(valName, new BigInteger(value.toString()));
    }
    
    /**
     * Sets the int array props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setIntArrayProps(String valName, int value[], int whereToAdd)
    {
        Integer[] valueObj = new Integer[value.length];
        
        setIntegerArray(valName, valueObj);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#setIntegerArray(java.lang.String,
     * java.lang.Integer[])
     */
    public void setIntegerArray(String valName, Integer[] value)
    {
        BigInteger[] valueObj = new BigInteger[value.length];
        
        for (int iCount = 0; iCount < value.length; iCount++)
        {
            Integer intObject = new Integer(value[iCount]);
            valueObj[iCount] = new BigInteger(intObject.toString());
        }
        
           m_CreateInObj.data.intArrayProps.put(valName, valueObj);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setFloat(java.lang.String,
     * java.lang.Float)
     */
    public void setFloat(String propName, Float propValue)
    {
            m_CreateInObj.data.floatProps.put(propName, propValue);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setFloatArray(java.lang.String,
     * java.lang.Float[])
     */
    public void setFloatArray(String propName, Float[] propValueArr)
    {
            m_CreateInObj.data.floatArrayProps.put(propName, propValueArr);
    }
    
    /**
     * Sets the bool props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setBoolProps(String valName, Boolean value, int whereToAdd)
    {
        setLogical(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setLogical(java.lang.String,
     * java.lang.Boolean)
     */
    public void setLogical(String valName, Boolean value)
    {
            m_CreateInObj.data.boolProps.put(valName, booleanToBigInteger(value));
    }
    
    /**
     * Sets the bool array props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setBoolArrayProps(String valName, Boolean value[], int whereToAdd)
    {
        setLogicalArray(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#setLogicalArray(java.lang.String,
     * java.lang.Boolean[])
     */
    public void setLogicalArray(String valName, Boolean value[])
    {
            m_CreateInObj.data.boolArrayProps.put(valName, value);
    }
    
    /**
     * Sets the date props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setDateProps(String valName, Date value, int whereToAdd)
    {
        setDate(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setDate(java.lang.String,
     * java.util.Date)
     */
    public void setDate(String valName, Date value)
    {
        Calendar calendar = dateHelper(value);
        
            m_CreateInObj.data.dateProps.put(valName, calendar);
    }
    
    /**
     * Sets the date array props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setDateArrayProps(String valName, Date value[], int whereToAdd)
    {
        setDateArray(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setDateArray(java.lang.String,
     * java.util.Date[])
     */
    public void setDateArray(String valName, Date value[])
    {
        Calendar[] calendarArr = new Calendar[value.length];
        
        for (int iCount = 0; iCount < value.length; ++iCount)
        {
            calendarArr[iCount] = dateHelper(value[iCount]);
        }
        
       m_CreateInObj.data.dateArrayProps.put(valName, calendarArr);
        
    }
    
    /**
     * Date helper.
     * 
     * @param date
     *            the date
     * @return the calendar
     */
    private Calendar dateHelper(Date date)
    {
        Calendar calendar = null;
        if (date != null)
        {
            calendar = Calendar.getInstance();
            calendar.setTime(date);
            
        }
        
        return calendar;
    }
    
    /**
     * Sets the tag props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setTagProps(String valName, TCComponent value, int whereToAdd)
    {
        setTag(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setTag(java.lang.String,
     * com.teamcenter.rac.kernel.TCComponent)
     */
    public void setTag(String valName, TCComponent value)
    {
        m_CreateInObj.data.tagProps.put(valName, value);
    }
    
    /**
     * Sets the tag array props.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     * @param whereToAdd
     *            the where to add
     */
    public void setTagArrayProps(String valName, TCComponent value[], int whereToAdd)
    {
        setTagArray(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setTagArray(java.lang.String,
     * com.teamcenter.rac.kernel.TCComponent[])
     */
    public void setTagArray(String valName, TCComponent value[])
    {
        m_CreateInObj.data.tagArrayProps.put(valName, value);
    }
    
    /**
     * Sets the compound create input.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     */
    public void setCompoundCreateInput(String valName, CreateInObjectHelper value)
    {
        setCompound(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#setCompound(java.lang.String,
     * com.nov.rac.propertymap.IPropertyMap)
     */
    public void setCompound(String valName, IPropertyMap value)
    {
        CreateInObjectHelper createValue = getObject(value);
        
        m_CreateInObj.data.compoundCreateInput.put(valName, new CreateInput[] { createValue.m_CreateInObj.data });
    }
    
    /**
     * Sets the compound array create input.
     * 
     * @param valName
     *            the val name
     * @param value
     *            the value
     */
    public void setCompoundArrayCreateInput(String valName, CreateInObjectHelper[] value)
    {
        setCompoundArray(valName, value);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#setCompoundArray(java.lang.String,
     * com.nov.rac.propertymap.IPropertyMap[])
     */
    public void setCompoundArray(String valName, IPropertyMap[] value)
    {
        CreateInObjectHelper[] createInObject = new CreateInObjectHelper[value.length];
        CreateInput[] input = new CreateInput[value.length];
        
        for (int iCnt = 0; iCnt < value.length; iCnt++)
        {
            createInObject[iCnt] = getObject(value[iCnt]);
            input[iCnt] = createInObject[iCnt].m_CreateInObj.data;
        }
        
        m_CreateInObj.data.compoundCreateInput.put(valName, input);
    }
    
    /**
     * Adds the errors.
     * 
     * @param serviceData
     *            the service data
     */
    public void addErrors(ServiceData serviceData)
    {
        int totalErrors = serviceData.sizeOfPartialErrors();
        if (totalErrors > 0)
        {
            for (int i = 0; i < totalErrors; i++)
            {
                m_errors.add(serviceData.getPartialError(i));
            }
            
        }
    }
    
    // Intentionally Package Scope: User should not call this function directly
    /* public *//**
     * Gets the creates the or update input.
     * 
     * @return the creates the or update input
     */
    CreateOrUpdateInput getCreateOrUpdateInput()
    {
        // TODO Auto-generated method stub
        return m_CreateOrUpdateInput;
    }
    
    // Intentionally Package Scope: User should not call this function directly
    /* public *//**
     * Gets the creates the response.
     * 
     * @return the creates the response
     */
    CreateOrUpdateResponse getCreateResponse()
    {
        return m_createResponse;
    }
    
    /**
     * Sets the creates the response.
     * 
     * @param mCreateResponse
     *            the new creates the response
     */
    public void setCreateResponse(CreateOrUpdateResponse mCreateResponse)
    {
        m_createResponse = mCreateResponse;
    }
    
    /**
     * Sets the creates the output.
     * 
     * @param mCreateOutput
     *            the new creates the output
     */
    public void setCreateOutput(CompoundObject mCreateOutput)
    {
        m_createOutput = mCreateOutput;
    }
    
    /**
     * Gets the creates the output.
     * 
     * @return the creates the output
     */
    CompoundObject getCreateOutput()
    {
        return m_createOutput;
    }
    
    /**
     * Gets the objects.
     * 
     * @return the objects
     */
    public TCComponent[] getObjects()
    {
        
        return null;
    }
    
    /**
     * Gets the target object.
     * 
     * @return the target object
     */
    public TCComponent getTargetObject()
    {
        return m_CreateOrUpdateInput.targetObject;
    }
    
    /**
     * Sets the target object.
     * 
     * @param m_TargetObject
     *            the new target object
     */
    public void setTargetObject(TCComponent m_TargetObject)
    {
        m_CreateOrUpdateInput.targetObject = m_TargetObject;
    }
    
    /**
     * Gets the parent object.
     * 
     * @return the parent object
     */
    public TCComponent getParentObject()
    {
        return m_CreateOrUpdateInput.container;
    }
    
    /**
     * Sets the parent object.
     * 
     * @param m_ParentObject
     *            the new parent object
     */
    public void setParentObject(TCComponent m_ParentObject)
    {
        m_CreateOrUpdateInput.container = m_ParentObject;
    }
    
    /**
     * Gets the relation name.
     * 
     * @return the relation name
     */
    public String getRelationName()
    {
        return m_CreateOrUpdateInput.relationWithContainer;
    }
    
    /**
     * Sets the relation name.
     * 
     * @param m_RelationName
     *            the new relation name
     */
    public void setRelationName(String m_RelationName)
    {
        m_CreateOrUpdateInput.relationWithContainer = m_RelationName;
    }
    
    /**
     * Gets the operation mode.
     * 
     * @return the operation mode
     */
    public int getOperationMode()
    {
        return m_CreateOrUpdateInput.operationMode;
    }
    
    /**
     * Gets the errors.
     * 
     * @return the errors
     */
    public ArrayList<ErrorStack> getErrors()
    {
        return m_errors;
    }
    
    /**
     * Gets the string property.
     * 
     * @param attrName
     *            the attr name
     * @return the string property
     */
    public String getStringProperty(String attrName)
    {
        String attrValue = getString(attrName);
        return attrValue;
    }
    
    /**
     * Gets the date property.
     * 
     * @param attrName
     *            the attr name
     * @return the date property
     */
    public Date getDateProperty(String attrName)
    {
        Date date = getDate(attrName);
        return date;
    }
    
    //
    // public Map getNameValueMap() {
    //
    // m_CreateOrUpdateInput.createIn.data.
    //
    // return null;
    // }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getCompound(java.lang.String)
     */
    public IPropertyMap getCompound(String propName)
    {
        //IPropertyMap attrValue = (IPropertyMap) m_CreateOrUpdateInput.createIn.data.compoundCreateInput.get(propName);
        IPropertyMap attrValue = null;
        CreateInput[] inputObjects= (CreateInput[]) m_CreateOrUpdateInput.createIn.data.compoundCreateInput.get(propName);
        if(inputObjects!= null && inputObjects.length>0)
        {
            OperationModes oprMode = getOprerationMode(m_CreateOrUpdateInput.operationMode);
            CreateInObjectHelper compundObject  = new CreateInObjectHelper(inputObjects[0].boName, oprMode);
            compundObject.m_CreateInObj.data = inputObjects[0];
            attrValue = compundObject;
        }
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#getCompoundArray(java.lang.String)
     */
    public IPropertyMap[] getCompoundArray(String propName)
    {
        return null;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDate(java.lang.String)
     */
    public Date getDate(String propName)
    {
        Calendar calendar = (Calendar) m_CreateOrUpdateInput.createIn.data.dateProps.get(propName);
        
        Date attrValue = null;
        
        if (calendar != null)
        {
            attrValue = calendar.getTime();
        }
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDateArray(java.lang.String)
     */
    public Date[] getDateArray(String propName)
    {
        Calendar[] calendar = (Calendar[]) m_CreateOrUpdateInput.createIn.data.dateArrayProps.get(propName);
        
        Date[] attrValue = null;
        
        for (int index = 0; index < calendar.length; ++index)
        {
            if (calendar != null)
            {
                attrValue[index] = calendar[index].getTime();
            }
        }
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDouble(java.lang.String)
     */
    public Double getDouble(String propName)
    {
        Double attrValue = (Double) m_CreateOrUpdateInput.createIn.data.doubleProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#getDoubleArray(java.lang.String)
     */
    public Double[] getDoubleArray(String propName)
    {
        Double[] attrValue = (Double[]) m_CreateOrUpdateInput.createIn.data.doubleArrayProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getFloat(java.lang.String)
     */
    public Float getFloat(String propName)
    {
        Float attrValue = (Float) m_CreateOrUpdateInput.createIn.data.floatProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getFloatArray(java.lang.String)
     */
    public Float[] getFloatArray(String propName)
    {
        Float[] attrValue = (Float[]) m_CreateOrUpdateInput.createIn.data.floatArrayProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getInteger(java.lang.String)
     */
    public Integer getInteger(String propName)
    {
    	
        Integer attrValue = Integer.parseInt( m_CreateOrUpdateInput.createIn.data.intProps.get(propName).toString());
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#getIntegerArray(java.lang.String)
     */
    public Integer[] getIntegerArray(String propName)
    {
        Integer[] attrValue = (Integer[]) m_CreateOrUpdateInput.createIn.data.intArrayProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getLogical(java.lang.String)
     */
    public Boolean getLogical(String propName)
    {
        BigInteger bigIntegerValue = (BigInteger) m_CreateOrUpdateInput.createIn.data.boolProps.get(propName);
        return bigIntegerToBoolean(bigIntegerValue);
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#getLogicalArray(java.lang.String)
     */
    public Boolean[] getLogicalArray(String propName)
    {
        Boolean[] attrValue = (Boolean[]) m_CreateOrUpdateInput.createIn.data.boolArrayProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getString(java.lang.String)
     */
    public String getString(String propName)
    {
        String attrValue = (String) m_CreateOrUpdateInput.createIn.data.stringProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#getStringArray(java.lang.String)
     */
    public String[] getStringArray(String propName)
    {
        String[] attrValue = (String[]) m_CreateOrUpdateInput.createIn.data.stringArrayProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getTag(java.lang.String)
     */
    public TCComponent getTag(String propName)
    {
        TCComponent attrValue = (TCComponent) m_CreateOrUpdateInput.createIn.data.tagProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getTagArray(java.lang.String)
     */
    public TCComponent[] getTagArray(String propName)
    {
        TCComponent[] attrValue = (TCComponent[]) m_CreateOrUpdateInput.createIn.data.tagArrayProps.get(propName);
        
        return attrValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getCompoundArrayPropNames()
     */
    public Set<String> getCompoundArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.compoundCreateInput.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getCompoundPropNames()
     */
    public Set<String> getCompoundPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.compoundCreateInput.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDateArrayPropNames()
     */
    public Set<String> getDateArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.dateArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDatePropNames()
     */
    public Set<String> getDatePropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.dateProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDoubleArrayPropNames()
     */
    public Set<String> getDoubleArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.doubleArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getDoublePropNames()
     */
    public Set<String> getDoublePropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.doubleProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getFloatArrayPropNames()
     */
    public Set<String> getFloatArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.floatArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getFloatPropNames()
     */
    public Set<String> getFloatPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.floatProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getIntegerArrayPropNames()
     */
    public Set<String> getIntegerArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.intArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getIntegerPropNames()
     */
    public Set<String> getIntegerPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.intProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getLogicalArrayPropNames()
     */
    public Set<String> getLogicalArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.boolArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getLogicalPropNames()
     */
    public Set<String> getLogicalPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.boolProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getStringArrayPropNames()
     */
    public Set<String> getStringArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.stringArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getStringPropNames()
     */
    public Set<String> getStringPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.stringProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getTagArrayPropNames()
     */
    public Set<String> getTagArrayPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.tagArrayProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getTagPropNames()
     */
    public Set<String> getTagPropNames()
    {
        Set<String> keysSet = m_CreateOrUpdateInput.createIn.data.tagProps.keySet();
        return keysSet;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#addAll(com.nov.rac.propertymap.
     * IPropertyMap)
     */
    public void addAll(IPropertyMap propMap)
    {
        CreateInput createInObject = this.m_CreateInObj.data;
        
        Set<String> stringKeys = propMap.getStringPropNames();
        Iterator<String> itr = stringKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            String value = propMap.getString(key);
            createInObject.stringProps.put(key, value);
        }
        
        Set<String> stringArrKeys = propMap.getStringArrayPropNames();
        itr = stringArrKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            String[] value = propMap.getStringArray(key);
            createInObject.stringArrayProps.put(key, value);
        }
        
        Set<String> integerKeys = propMap.getIntegerPropNames();
        itr = integerKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            int value = propMap.getInteger(key).intValue();
            createInObject.intProps.put(key, value);
        }
        
        Set<String> integerArrayKeys = propMap.getIntegerArrayPropNames();
        itr = integerArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Integer[] value = propMap.getIntegerArray(key);
            createInObject.intArrayProps.put(key, value);
        }
        
        Set<String> doubleKeys = propMap.getDoublePropNames();
        itr = doubleKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Double value = propMap.getDouble(key);
            createInObject.doubleProps.put(key, value);
        }
        
        Set<String> doubleArrayKeys = propMap.getDoubleArrayPropNames();
        itr = doubleArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Double[] value = propMap.getDoubleArray(key);
            createInObject.doubleArrayProps.put(key, value);
        }
        
        Set<String> floatKeys = propMap.getFloatPropNames();
        itr = floatKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Float value = propMap.getFloat(key);
            createInObject.floatProps.put(key, value);
        }
        
        Set<String> floatArrayKeys = propMap.getFloatArrayPropNames();
        itr = floatArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Float[] value = propMap.getFloatArray(key);
            createInObject.floatArrayProps.put(key, value);
        }
        
        Set<String> logicalKeys = propMap.getLogicalPropNames();
        itr = logicalKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Boolean value = propMap.getLogical(key);
            createInObject.boolProps.put(key, booleanToBigInteger(value));
        }
        
        Set<String> logicalArrayKeys = propMap.getLogicalArrayPropNames();
        itr = logicalArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Boolean[] value = propMap.getLogicalArray(key);
            createInObject.boolArrayProps.put(key, value);
        }
        
        Set<String> tagKeys = propMap.getTagPropNames();
        itr = tagKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            TCComponent value = propMap.getTag(key);
            createInObject.tagProps.put(key, value);
        }
        
        Set<String> tagArrayKeys = propMap.getTagArrayPropNames();
        itr = tagArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            TCComponent[] value = propMap.getTagArray(key);
            createInObject.tagArrayProps.put(key, value);
        }
        
        Set<String> compoundKeys = propMap.getCompoundPropNames();
        itr = compoundKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            IPropertyMap value = propMap.getCompound(key);
            
            CreateInObjectHelper createValue = getObject(value);
            createInObject.compoundCreateInput.put(key, new CreateInput[] { createValue.m_CreateInObj.data });
        }
        
        // /TODO: check if its required
        Set<String> compoundArrayKeys = propMap.getCompoundArrayPropNames();
        itr = compoundArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            IPropertyMap[] value = propMap.getCompoundArray(key);
            
            if(value!=null && value.length>0)
            {
                CreateInObjectHelper[] createValue = new CreateInObjectHelper[value.length];
                CreateInput[] input = new CreateInput[value.length];                        
                
                for (int index = 0; index < value.length; ++index)
                {
                    createValue[index] = getObject(value[index]); 
                    input[index] = createValue[index].m_CreateInObj.data;
                }
                createInObject.compoundCreateInput.put(key, input);
            }
        }
    }
    
    
    /**
     * Gets the opreration mode.
     * 
     * @param operationMode
     *            the operation mode
     * @return the opreration mode
     */
    //Kishor Ahuja: Changed from private to public 
    public OperationModes getOprerationMode(int operationMode)
    {
        EnumSet<OperationModes> allOprModes = EnumSet.allOf(OperationModes.class);
        OperationModes oprMode = OperationModes.ENUM_OPERATION_UNKNOWN;
        Iterator<OperationModes> itrEnum = allOprModes.iterator();
        while (itrEnum.hasNext())
        {
            OperationModes mode = itrEnum.next();
            if (operationMode == mode.getMode())
            {
                oprMode = mode;
            }
        }
        
        return oprMode;
    }
    
    /**
     * Gets the object.
     * 
     * @param propertyMap
     *            the property map
     * @return the object
     */
    private CreateInObjectHelper getObject(IPropertyMap propertyMap)
    {
        OperationModes oprMode = getOprerationMode(m_CreateOrUpdateInput.operationMode);
        
        CreateInObjectHelper createValue = new CreateInObjectHelper(propertyMap.getType(), oprMode);
        
        createValue.addAll(propertyMap);
        
        return createValue;
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#createCompound(java.lang.Object)
     */
    @Override
    public IPropertyMap createCompound(Object args)
    {
        CreateInObjectHelper createValue = null;
        
        if (args != null && args instanceof String)
        {
            String boName = (String) args;
            OperationModes oprMode = getOprerationMode(m_CreateOrUpdateInput.operationMode);
            createValue = new CreateInObjectHelper(boName, oprMode);
        }
        
        return createValue;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getType()
     */
    @Override
    public String getType()
    {
        String type = null;
        if (m_TargetObject != null)
        {
            type = m_TargetObject.getType();
        }
        else
        {
            type = m_CreateInObj.data.boName;
        }
        
        return type;
    }
    
    /*
     * (non-Javadoc)
     * @see com.nov.rac.propertymap.IPropertyMap#getComponent()
     */
    @Override
    public TCComponent getComponent()
    {
        
        return  getTargetObject();
    }
    
    /*
     * (non-Javadoc)
     * @see
     * com.nov.rac.propertymap.IPropertyMap#setComponent(com.teamcenter.rac.
     * kernel.TCComponent)
     */
    @Override
    public void setComponent(TCComponent component)
    {
        setTargetObject(component);
    }
    
    /**
     * Convert Boolean to BigInteger.
     * 
     * @param value
     *            the value
     * @return the big integer
     */
    private BigInteger booleanToBigInteger(Boolean value)
    {
        if (value)
        {
            return BigInteger.ONE;
        }
        return BigInteger.ZERO;
    }
    
    /**
     * Convert BigInteger to Boolean.
     * 
     * @param bigInteger
     *            the big integer
     * @return the boolean
     */
    private Boolean bigIntegerToBoolean(BigInteger bigInteger)
    {
        if (BigInteger.ZERO.equals(bigInteger))
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    

    private boolean isEmptyNonCompoundMaps(Collection<Map> maps) 
    {
        return PropertyMapHelper.isEmptyMaps(maps);
    }
    
    private Collection<Map> getNonCompoundMaps(CreateInput theCreateInput)
    {
        Collection<Map> nonCompoundMaps = new ArrayList<Map>();
        
        nonCompoundMaps.add(theCreateInput.stringProps);
        nonCompoundMaps.add(theCreateInput.stringArrayProps);
        
        nonCompoundMaps.add(theCreateInput.tagProps);
        nonCompoundMaps.add(theCreateInput.tagArrayProps);
        
        nonCompoundMaps.add(theCreateInput.boolProps);
        nonCompoundMaps.add(theCreateInput.boolArrayProps);
        
        nonCompoundMaps.add(theCreateInput.intProps);
        nonCompoundMaps.add(theCreateInput.intArrayProps);
        
        nonCompoundMaps.add(theCreateInput.dateProps);
        nonCompoundMaps.add(theCreateInput.dateArrayProps);

        nonCompoundMaps.add(theCreateInput.doubleProps);
        nonCompoundMaps.add(theCreateInput.doubleArrayProps);

        nonCompoundMaps.add(theCreateInput.floatProps);
        nonCompoundMaps.add(theCreateInput.floatArrayProps);
        
        return nonCompoundMaps;

    }
    
    
    @SuppressWarnings("unchecked")
    boolean isEmptyCreateInput(CreateInput input)
    {
        boolean value = true;
        Collection<Map> nonCompoundMaps = getNonCompoundMaps(input );
        
        if (isEmptyNonCompoundMaps(nonCompoundMaps)) 
        {
            if (PropertyMapHelper.hasContent(input.compoundCreateInput)) 
            {
                
                Collection<Object> inputs = input.compoundCreateInput.values();
                for(Object object : inputs)
                {
                    CreateInput[] createInputs = (CreateInput[])object;
                    for (CreateInput createInput: createInputs) 
                    {
                        if (!isEmptyCreateInput(createInput)) {
                            value = false;
                            return value;
                        }
            
                    }
                }
            }
        } 
        else 
        {
            value = false;
        }

        return value;
    }

    @Override
    public boolean isEmpty() 
    {
        return isEmptyCreateInput(m_CreateOrUpdateInput.createIn.data);

    }

    @Override
    public void setType(String type)
    {
        m_CreateInObj.data.boName = type;
    }

    
    
}
