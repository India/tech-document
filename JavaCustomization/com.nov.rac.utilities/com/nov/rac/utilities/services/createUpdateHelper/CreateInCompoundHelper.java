package com.nov.rac.utilities.services.createUpdateHelper;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.utilities.common.ItemTypeHelper;

public class CreateInCompoundHelper
{

    public CreateInCompoundHelper( IPropertyMap itemPropertyMap)
    {
        m_propertyMap = itemPropertyMap;
    }

    public void addCompound(String typeConstant, String compoundRelation)
    {
        String itemType = m_propertyMap.getType();
        String compoundPropertyType = ItemTypeHelper.getTypeConstantValue(itemType, typeConstant);
        IPropertyMap compoundPropertyMap = m_propertyMap.createCompound(compoundPropertyType);
        m_propertyMap.setCompound(compoundRelation, compoundPropertyMap);
    }
    
    private IPropertyMap m_propertyMap;
}