package com.nov.rac.utilities.services.createUpdateHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import com.nov.rac.utilities.services.deleteHelper.CreateRelationObjectHelper;
import com.nov.rac.utilities.services.deleteHelper.DeleteObjectHelper;
import com.nov4.services.rac.custom.NOV4DataManagementService;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CompoundObject;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateOrUpdateInput;
import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.CreateOrUpdateResponse;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2006_03.DataManagement.CreateRelationsResponse;
import com.teamcenter.services.rac.core._2006_03.DataManagement.Relationship;
import com.teamcenter.soa.client.model.ErrorStack;

public class CreateObjectsSOAHelper
{
    
    static public void createUpdateObjects(CreateInObjectHelper inputObject)
    {
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        
        NOV4DataManagementService dmService = NOV4DataManagementService.getService(tcSession);
        
        CreateOrUpdateInput[] createInput = new CreateOrUpdateInput[] { inputObject.getCreateOrUpdateInput() };
        
        CreateOrUpdateResponse theResponse = dmService.createOrUpdateObjects(createInput);
        inputObject.addErrors(theResponse.serviceData);
        
        inputObject.setCreateResponse(theResponse);
        
        if (theResponse.objects != null && theResponse.objects.length > 0)
        {
            inputObject.setCreateOutput(theResponse.objects[0]);
        }
    }
    
    // Handling Multiple Objects
    static public void createUpdateObjects(CreateInObjectHelper[] inputObject)
    {
        
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        
        NOV4DataManagementService dmService = NOV4DataManagementService.getService(tcSession);
        
        CreateOrUpdateInput[] createInput = new CreateOrUpdateInput[inputObject.length];
        
        for (int iCount = 0; iCount < inputObject.length; iCount++)
        {
            
            createInput[iCount] = inputObject[iCount].getCreateOrUpdateInput();
        }
        
        CreateOrUpdateResponse theResponse = dmService.createOrUpdateObjects(createInput);
        
        inputObject[0].addErrors(theResponse.serviceData);
        
        inputObject[0].setCreateResponse(theResponse);
        
        for (int iCount = 0; iCount < theResponse.objects.length; iCount++)
        {
            
            inputObject[iCount].setCreateOutput(theResponse.objects[iCount]);
        }
        
    }
    
    public static TCComponent[] getCreatedOrUpdatedObjects(CreateInObjectHelper transferObject)
    {
        
        CompoundObject compObject = transferObject.getCreateOutput();
        
        // CompoundObject[] compObjects = theResponse.objects;
        
        Vector<TCComponent> createdorUpdatedObjs = new Vector<TCComponent>();
        
        createdorUpdatedObjs.add(compObject.theObject);
        
        getAllRelatedObjects(compObject.compoundObjects, createdorUpdatedObjs);
        
        return createdorUpdatedObjs.toArray(new TCComponent[createdorUpdatedObjs.size()]);
    }
    
    // Return top level object.
    public static TCComponent getCreatedOrUpdatedRootObject(CreateInObjectHelper transferObject)
    {
        
        CompoundObject compObject = transferObject.getCreateOutput();
        
        return compObject.theObject;
    }
    
    private static void getAllRelatedObjects(Map compoundObjects, Vector<TCComponent> createdorUpdatedObjs)
    {
        
        if (compoundObjects != null)
        {
            Collection relatedCompObjs = compoundObjects.values();
            
            Iterator iter = relatedCompObjs.iterator();
            
            while (iter.hasNext())
            {
                
                Object compObject = iter.next();
                
                if (compObject instanceof CompoundObject[])
                {
                    CompoundObject[] compObjects = (CompoundObject[]) compObject;
                    
                    if (compObjects != null)
                    {
                        for (int iCnt = 0; iCnt < compObjects.length; iCnt++)
                        {
                            createdorUpdatedObjs.add(compObjects[iCnt].theObject);
                            getAllRelatedObjects(compObjects[iCnt].compoundObjects, createdorUpdatedObjs);
                        }
                        
                    }
                }
            }
            
            // if(relatedCompObjs != null){
            // CompoundObject[] arrCompObjs = (CompoundObject[])
            // relatedCompObjs.toArray(new
            // CompoundObject[compoundObjects.size()]);
            //          
            // for(int iCount = 0 ; iCount < arrCompObjs.length; iCount++){
            // createdorUpdatedObjs.add(arrCompObjs[iCount].theObject);
            // getAllRelatedObjects(arrCompObjs[iCount].compoundObjects,createdorUpdatedObjs);
            // }
            // }
        }
    }
    
    static public void deleteRelationObjects(DeleteObjectHelper[] inputObject)
    {
        int operationMode = inputObject[0].getOperationMode();
        if (operationMode == DeleteObjectHelper.OPERATION_DELETE_RELATION)
        {
            invokeDeleteRelation(inputObject);
        }
    }
    
    static public void invokeDeleteRelation(DeleteObjectHelper[] inputObject)
    {
        int iObjLength = inputObject.length;
        
        if (iObjLength > 0)
        {
            Relationship[] inputRelations = new Relationship[iObjLength];
            
            for (int iCount = 0; iCount < iObjLength; iCount++)
            {
                Relationship relationshipData = new Relationship();
                
                relationshipData.primaryObject = inputObject[iCount].getPrimaryObject();
                relationshipData.secondaryObject = inputObject[iCount].getSecondaryObject();
                relationshipData.relationType = inputObject[iCount].getRelationName();
                inputRelations[iCount] = relationshipData;
            }
            
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            DataManagementService dmService = DataManagementService.getService(tcSession);
            ServiceData delRelServiceData = dmService.deleteRelations(inputRelations);
            for (int indx = 0; indx < iObjLength; indx++)
            {
                inputObject[indx].addErrors(delRelServiceData);
            }
        }
    }
    
    static public void createRelationObjects(CreateRelationObjectHelper[] inputObject)
    {
        int operationMode = inputObject[0].getOperationMode();
        if (operationMode == CreateRelationObjectHelper.OPERATION_CREATE_RELATION)
        {
            invokeCreateRelation(inputObject);
        }
    }
    
    static public void invokeCreateRelation(CreateRelationObjectHelper[] inputObject)
    {
        int iObjLength = inputObject.length;
        
        if (iObjLength > 0)
        {
            Relationship[] inputRelations = new Relationship[iObjLength];
            
            for (int iCount = 0; iCount < iObjLength; iCount++)
            {
                Relationship relationshipData = new Relationship();
                
                relationshipData.primaryObject = inputObject[iCount].getPrimaryObject();
                relationshipData.secondaryObject = inputObject[iCount].getSecondaryObject();
                relationshipData.relationType = inputObject[iCount].getRelationName();
                inputRelations[iCount] = relationshipData;
            }
            
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            DataManagementService dmService = DataManagementService.getService(tcSession);
            CreateRelationsResponse createRelServiceData = dmService.createRelations(inputRelations);
            for (int indx = 0; indx < iObjLength; indx++)
            {
                inputObject[indx].addErrors(createRelServiceData.serviceData);
            }
        }
    }
    
    static public void handleErrors(CreateInObjectHelper[] transferObjects)
    {
        try
        {
            throwErrors(transferObjects);
        }
        catch (TCException exception)
        {
            MessageBox.post(exception);
        }
        
    }
    
    static public void handleErrors(CreateInObjectHelper transferObjects)
    {
        handleErrors(new CreateInObjectHelper[] { transferObjects });
    }
    
    public static void throwErrors(CreateInObjectHelper[] transferObjects) throws TCException
    {
        
        ArrayList<String> errorMessages = new ArrayList<String>();
        ArrayList<Integer> errorCodes = new ArrayList<Integer>();
        
        for (int i = 0; i < transferObjects.length; i++)
        {
            for (ErrorStack errorStack : transferObjects[i].getErrors())
            {
                Collections.addAll(errorMessages, errorStack.getMessages());
            }
        }
        
        if (errorMessages.size() > 0)
        {
            String[] errorStrings = new String[1];
            errorStrings = errorMessages.toArray(errorStrings);
            
            throw (new TCException(errorStrings));
        }
    }
    
    public static void throwErrors(CreateInObjectHelper transferObjects) throws TCException
    {
        throwErrors(new CreateInObjectHelper[] { transferObjects });
    }
    
}
