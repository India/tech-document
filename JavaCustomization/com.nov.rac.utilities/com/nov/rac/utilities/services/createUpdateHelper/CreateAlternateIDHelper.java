package com.nov.rac.utilities.services.createUpdateHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentIdContext;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2007_12.DataManagement;
import com.teamcenter.services.rac.core._2007_12.DataManagement.AlternateIdentifiersInput;

public class CreateAlternateIDHelper 
{
	private IPropertyMap m_mainObject = null;
	private IPropertyMap m_revObject = null;
	
	private TCComponentIdContext m_idContext = null;

	public CreateAlternateIDHelper(IPropertyMap mainObject, IPropertyMap revObject )
	{
		m_mainObject = mainObject;
		m_revObject = revObject;		
	}
	
	public void createAlternateID()
	{
		DataManagement.AlternateIdentifiersInput[] input = new DataManagement.AlternateIdentifiersInput[1];
		input[0] = new AlternateIdentifiersInput();
		
		addIdContext(input);
		addIdentifierType(input);
		addMainObjectData(input);
		addRevObjectData(input);
		
		TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
		DataManagementService dmService = DataManagementService.getService(session);
		ServiceData serviceResponse = dmService.createAlternateIdentifiers(input);
		
		for (int i =0; i<(serviceResponse.sizeOfPartialErrors());i++)
		{
			List<String> errorString = new ArrayList<String>();
			errorString.addAll(Arrays.asList(serviceResponse.getPartialError(i).getMessages()));  	
			MessageBox.post(errorString.toString() + "\nAlternate ID may not have been created","Warning",MessageBox.WARNING);
		}
	}
	
	private void addIdContext(AlternateIdentifiersInput[] input) 
	{
		input[0].context = (TCComponentIdContext) m_mainObject.getTag("idcontext");		
	}
	
	private void addIdentifierType(AlternateIdentifiersInput[] input) 
	{
		TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
		TCComponentType identifierType = null;
		
		try
        {
			String boTypeName = m_mainObject.getType();			
			identifierType =  session.getTypeComponent(boTypeName);
	    }
        catch (TCException e) 
	    {
			e.printStackTrace();
		}
		
		input[0].identifierType = identifierType;
	}

	private void addMainObjectData(AlternateIdentifiersInput[] input) 
	{
		input[0].mainObject.alternateId = m_mainObject.getString("idfr_id");
		input[0].mainObject.identifiableObject = m_mainObject.getTag("altid_of");
		
		Map map = new HashMap<String,String[]>();
		map.put("object_name", new String[]{m_mainObject.getString("object_name")});
		input[0].mainObject.additionalProps = map;
	}
	
	private void addRevObjectData(AlternateIdentifiersInput[] input) 
	{
		input[0].revObject.alternateId = m_revObject.getString("idfr_id");
		input[0].revObject.identifiableObject = m_revObject.getTag("altid_of");
		Map map = new HashMap<String,String[]>();
		map.put("object_name", new String[]{m_revObject.getString("object_name")});
		input[0].revObject.additionalProps = map;
	}
	
	public static TCComponentIdContext getIdContext(String m_idContext) 
	{
        try 
        {
			TCSession session = (TCSession) AIFUtility.getCurrentApplication().getSession();
			TCComponentType idContext = session.getTypeComponent("IdContext");
			TCComponent[] contexts = idContext.extent();
			TCComponentIdContext IdContext = null;
			
			for( TCComponent context : contexts )
			{
				if (context.toString().equals(m_idContext))
				{
					IdContext = (TCComponentIdContext) context;
					break;
				}
			}
			return IdContext;
		} 
        catch (TCException e) 
        {
			e.printStackTrace();
			return null;
		}
	}
}
