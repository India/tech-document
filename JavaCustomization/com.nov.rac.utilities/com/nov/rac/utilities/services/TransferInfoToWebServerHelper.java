package com.nov.rac.utilities.services;

import com.nov.rac.utilities.preferences.PreferenceHelper;
import com.nov4.services.rac.custom.NOV4MiscellaneousService;
import com.nov4.services.rac.custom._2010_09.NOV4Miscellaneous;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;


public class TransferInfoToWebServerHelper
{
    public TransferInfoToWebServerHelper()
    {
        m_webserver = PreferenceHelper.getStringValue("CETGWSServices_server", TCPreferenceService.TC_preference_site);
        m_port = PreferenceHelper.getIntegerValue("CETGWSServices_port", TCPreferenceService.TC_preference_site);
        m_className = "_RSone_msg_queue_";
    }
    
    public void submitToRigDoc()
    {
        
        try
        {
            TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
            
            NOV4MiscellaneousService nov4MiscellaneousService = NOV4MiscellaneousService.getService(tcSession);
            NOV4Miscellaneous.SubmitToRigDocInput input = new NOV4Miscellaneous.SubmitToRigDocInput();
            
            input.targetObject = m_targetObject;
            input.transferInput = populateTransferInfo();
            
            ServiceData response = nov4MiscellaneousService.operationSubmitRigDoc(input);
            handleError(response);
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void transferInfoToWebService()
	{

		try
        {
	        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();

	        NOV4MiscellaneousService nov4MiscellaneousService = NOV4MiscellaneousService
	                .getService(tcSession);
	        
	  	        
	        ServiceData response = nov4MiscellaneousService.transferInfoToWebService(populateTransferInfo());		    
            handleError(response);
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
	}
    
    public ServiceData executeRSoneRequest()
    {
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();

        NOV4MiscellaneousService nov4MiscellaneousService = NOV4MiscellaneousService
                .getService(tcSession);
        
        NOV4Miscellaneous.RSOneRequestInput rsOneRequestInput = new NOV4Miscellaneous.RSOneRequestInput();
        
        rsOneRequestInput.trasferInput = populateTransferInfo();
        rsOneRequestInput.targetObject = m_targetObject;
        rsOneRequestInput.cancelSusFlag = m_cancelSusFlag;
  
        
        ServiceData response = nov4MiscellaneousService.executeRSOneRequest(rsOneRequestInput);
        
        return response;     
    }
    
    
    public void oracleNotify() throws TCException
    {
    	try
    	{
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
			NOV4MiscellaneousService nov4MiscellaneousService = NOV4MiscellaneousService
			        .getService(tcSession);
			
			String[] prefValList = PreferenceHelper.getStringValues("NOV_NonActiveEBSOrgs", TCPreferenceService.TC_preference_site);
	
			NOV4Miscellaneous.OralceNotifyInput oracleInput = new NOV4Miscellaneous.OralceNotifyInput();
			oracleInput.nonActiveEBDOrgValues = prefValList;
			oracleInput.targetObjects = m_targetRevisions;
			oracleInput.rsOneRequestInput.trasferInput = populateTransferInfo();
			ServiceData response = nov4MiscellaneousService.oracleNotify(oracleInput);
			handleError(response);
    	}
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private NOV4Miscellaneous.TransferInfo populateTransferInfo()
    {
        NOV4Miscellaneous.TransferInfo transferInfo = new NOV4Miscellaneous.TransferInfo();
        transferInfo.className = m_className;
        transferInfo.moduleName = m_moduleName;
        
        NOV4Miscellaneous.URLInput urlInput = new NOV4Miscellaneous.URLInput();
        
        urlInput.webserver = m_webserver;
        urlInput.servelet = m_serveletName;
        urlInput.context = m_context;
        urlInput.params = m_params;
        urlInput.port = m_port;
        
        transferInfo.urlData = urlInput;
        
        return transferInfo;
    }
    
    public void setTarget(TCComponent targetObject)
    {
        m_targetObject = targetObject;
    }
    
    public void setWebServer(String webserver)
    {
        m_webserver = webserver;
    }
    
    public void setPort(int port)
    {
        m_port = port;
    }
    
    public void setContext(String context)
    {
        m_context = context;
    }
    
    public void setServeletName(String serveletName)
    {
        m_serveletName = serveletName;
    }
    
    public void setParams(String params)
    {
        m_params = params;
    }
    
    public void setClassName(String className)
    {
        m_className = className;
    }
    
    public void setModuleName(String moduleName)
    {
        m_moduleName = moduleName;
    }
    
    public void setSMDLID(String smdlID)
    {
        m_smdlID = smdlID;
    }
    
    public void setRevisionID(String revisionID)
    {
        m_revID = revisionID;
    }    
    
    public void setUser(String user)
    {
        m_user = user;
    }
    
    public void setCancelSusFlag(boolean cancelSusFlag)
    {
        m_cancelSusFlag = cancelSusFlag;
    }
    
    public void setTargetRevision(TCComponent[] targetRevisions)
    {
    	m_targetRevisions = targetRevisions;
    }
    
    public void setProcessUID(String processUID)
    {
    	m_processUID = processUID;
    }
    void handleError(ServiceData response) throws TCException
    {
        if (response.sizeOfPartialErrors() > 0)
        {
            ErrorStack errorStack = response.getPartialError(0);
            
            int[] errorCodes = errorStack.getCodes();
            int[] errorLevels = errorStack.getLevels();
            String[] errorMessages = errorStack.getMessages();
            
            throw new TCException(errorCodes, errorLevels, errorMessages);
        }
    }
    
    private String m_webserver;
    private int m_port;
    private String m_context;
    private String m_serveletName;
    private String m_params;
    private String m_className;
    private String m_moduleName;
    private TCComponent m_targetObject;
    private String m_smdlID;
    private String m_revID;
    private String m_user;
    private boolean m_cancelSusFlag;
    private TCComponent[] m_targetRevisions;
    private String m_processUID;
    
}