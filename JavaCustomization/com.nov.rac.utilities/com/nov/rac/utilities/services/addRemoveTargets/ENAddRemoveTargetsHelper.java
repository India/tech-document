package com.nov.rac.utilities.services.addRemoveTargets;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.nov4.services.rac.changemanagement.NOV4ChangeManagementService;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.ENAddRemoveTargetIn;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.ENAddRemoveTargetOut;
import com.nov4.services.rac.changemanagement._2010_09.NOV4ChangeManagement.ENAddRemoveTargetResponse;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.soa.client.model.ErrorStack;


public class ENAddRemoveTargetsHelper
{
    private static final String ADD_FLAG = "ADD";
    private static final String REMOVE_FLAG = "REMOVE";
    private Map m_targetsToDispositionMap = null;
    
    public ENAddRemoveTargetsHelper()
    {
    }
    
    public Map addTargets(TCComponent[] inputTargets, TCComponentTask rootTask) throws TCException
    {
        ENAddRemoveTargetOut output = performOperation(inputTargets, rootTask, ADD_FLAG);
        return getTargetsToDispositionMap( output );
    }
    
    public TCComponent[] removeTargets(TCComponent[] inputTargets, TCComponentTask rootTask) throws TCException
    {
       ENAddRemoveTargetOut output = performOperation(inputTargets, rootTask, REMOVE_FLAG);
       return output.outputTargets;
    }
    
    public Map getM_targetsToDispositionMap()
    {
        return m_targetsToDispositionMap;
    }
    
    
    private ENAddRemoveTargetOut performOperation(TCComponent[] inputTargets, TCComponentTask rootTask, String flag) throws TCException
    {
       
        // Create input
        ENAddRemoveTargetIn[] enAddRemoveTrgInput  = new ENAddRemoveTargetIn[1];
        enAddRemoveTrgInput[0] = new ENAddRemoveTargetIn();
        enAddRemoveTrgInput[0].inputTargets = inputTargets;
        enAddRemoveTrgInput[0].rootTask = rootTask;
        enAddRemoveTrgInput[0].targetFlag = flag;
        
        // Invoke operation
        TCSession session = (TCSession) AIFUtility.getDefaultSession();
        NOV4ChangeManagementService service = NOV4ChangeManagementService
                .getService(session);
        
        ENAddRemoveTargetResponse response = service.enAddRemoveTargets(enAddRemoveTrgInput);
        handleError(response.serviceData);
        return response.targetOut[0];
    }
    
    private Map getTargetsToDispositionMap( ENAddRemoveTargetOut enAddRemoveTargetOut )
    {
        int targetsAddedCount = enAddRemoveTargetOut.outputTargets.length;
        int dispositionCount = enAddRemoveTargetOut.targetToDispositionMap.size();
        
        // Number of disposition is always equals to or less than number of targets added.
        if( targetsAddedCount != dispositionCount)
        {
            // If condition is true that means there are some targets which dont have
            // corresponding  disposition object.
            // add those targets into targetToDispositionMap and return the same map
            Set<TCComponent> outputTargetsSet = new HashSet<TCComponent>(Arrays.asList( enAddRemoveTargetOut.outputTargets ));
            Set mapKeySet = enAddRemoveTargetOut.targetToDispositionMap.keySet();
            
            outputTargetsSet.removeAll(mapKeySet);
            
            for(TCComponent eachTarget : outputTargetsSet)
            {
                enAddRemoveTargetOut.targetToDispositionMap.put(eachTarget, null);
            }
        }
        return enAddRemoveTargetOut.targetToDispositionMap;
    }

    private void handleError( ServiceData response ) throws TCException
    {
        if(response.sizeOfPartialErrors() > 0)
        {
            ErrorStack errorStack = response.getPartialError(0);
            
            int[] errorCodes = errorStack.getCodes();
            int[] errorLevels = errorStack.getLevels();
            String[] errorMessages = errorStack.getMessages();
            
            throw new TCException(errorCodes, errorLevels, errorMessages);
        }
    }
}
