package com.nov.rac.utilities.services.workflow;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.workflow.WorkflowService;

public class WorkflowSOAHelper {
	
	
	 public  static void removeAttachments(TCComponentTask Task,TCComponent [] targets) throws TCException
	    {
		 
	    	TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
	    	WorkflowService workflowService = WorkflowService.getService(tcSession);
	    	
	    	
	    	try { 
	    		
	    		workflowService.removeAttachments(Task, targets);	    	
	    	}catch (Exception e) {
	    			e.printStackTrace();
	    	}
	    }
}
