package com.nov.rac.utilities.services.validationhelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.teamcenter.services.rac.core._2013_05.DataManagement.ValidateValuesResponse;
import com.teamcenter.services.rac.core._2013_05.DataManagement.ValidationResults;
import com.teamcenter.services.rac.core._2013_05.DataManagement.ValidationStatus;

public class ValidationResultsHelper
{
   // private boolean m_uniqueId;
    private Map<String, ValueStatus> m_valueStatus;
    private boolean m_uniqueId;
    
    public ValidationResultsHelper() 
    {
    	m_valueStatus = new HashMap<String, ValueStatus>();
    }
    
    public static Map<String, ValidationResultsHelper> create(ValidateValuesResponse theValueResponse)
    {
    	Map<String, ValidationResultsHelper> validationResult = new HashMap<String, ValidationResultsHelper>();
    	
    	
    	Set<String> clientIds = theValueResponse.validationResults.keySet();
        
        for(String clientId : clientIds)
        {
        	ValidationResultsHelper validationResultsHelper = new ValidationResultsHelper();
        	
        	ValidationResults validationResults = (ValidationResults) theValueResponse.validationResults.get(clientId);
        	
        	validationResultsHelper.m_uniqueId = validationResults.uniqueID;
        	
        	for(ValidationStatus eachValidationStatus : validationResults.validationStatus)
        	{
        		String propertyName = eachValidationStatus.propName;
        		String[] modifiedValues = eachValidationStatus.modifiedValue;
        		int isValid = eachValidationStatus.valueStatus;
        		
        		validationResultsHelper.m_valueStatus.put(propertyName, new ValueStatus(isValid, modifiedValues));
        	}
        	
        	validationResult.put(clientId, validationResultsHelper);
        }
        
		return validationResult;
    	
    }
    
   
    
    public boolean isIdUnique()
    {
        return m_uniqueId;
    }
    
    public Set<String> getPropertyNames()
    {
        return m_valueStatus.keySet();
    }
    
    public int getPropertyStatus(String propertyName)
    {
    	ValueStatus valueStatus  = m_valueStatus.get(propertyName) ;  
    	
    	int isValid = 0;
    	
        if(valueStatus != null)
        {
        	isValid = valueStatus.getPropertyStatus(); 
        }
        return isValid;
    }
    
    public String[] getModifiedPropertyValues(String propertyName)
    {
    	ValueStatus valueStatus  = m_valueStatus.get(propertyName) ;  
    	
    	String [] modifiedValues = new String[0];
    	
        if(valueStatus != null)
        {
        	modifiedValues = valueStatus.getPropertyValues(); 
        }
        return modifiedValues;
    }
    
    public static class ValueStatus
    {
        private int m_propertyStatus;
        private String[] m_propertyValues;
        
        public ValueStatus(int propertyStatus, String[] propertyValues)
        {
            this.m_propertyStatus = propertyStatus;
            this.m_propertyValues = propertyValues;
        }
        
        public int getPropertyStatus()
        {
            return m_propertyStatus;
        }
        
        public String[] getPropertyValues()
        {
            return m_propertyValues;
        }
    }
}
