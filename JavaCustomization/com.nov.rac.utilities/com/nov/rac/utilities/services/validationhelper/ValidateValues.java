package com.nov.rac.utilities.services.validationhelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core._2013_05.DataManagement.ValidateInput;
import com.teamcenter.services.rac.core._2013_05.DataManagement.ValidateValuesResponse;

public class ValidateValues
{
    private String m_clientId;
    private int m_operationType;
    private String m_businessObjectName;
    
    public static final int CREATE = 0;
    public static final int REVISE = 1;
    public static final int SAVEAS = 2;
    public static final int GENERIC = 3;
    
    private IPropertyMap m_propertyValues;
    
    public ValidateValues()
    {
        
    }
    
    public String getClientId()
    {
        return m_clientId;
    }
    
    public void setClientId(String clientId)
    {
        m_clientId = clientId;
    }
    
    public int getOperationType()
    {
        return m_operationType;
    }
    
    public void setOperationType(int operationType)
    {
        m_operationType = operationType;
    }
    
    public String getType()
    {
        return m_businessObjectName;
    }
    
    public void setType(String businessObjectType)
    {
        m_businessObjectName = businessObjectType;
    }
    
    public void setPropertyValues(IPropertyMap propertyValues)
    {
        m_propertyValues = propertyValues;
    }
    
    private Map getValidateInputMap(IPropertyMap inputMap, Map outputMap)
    {
        Set<String> allPropertyNames = inputMap.getStringPropNames();        
        for (String propertyName : allPropertyNames)
        {
            outputMap.put(propertyName, new String[] { inputMap.getString(propertyName) });
        }
        
        allPropertyNames = inputMap.getStringArrayPropNames();
        for (String propertyName : allPropertyNames)
        {
            outputMap.put(propertyName, inputMap.getStringArray(propertyName));
        }
        
        return outputMap;
    }
    
    public Map<String, ValidationResultsHelper> validate()
    {
        Map<String, String[]> propertyValuesMap = new HashMap<String, String[]>();
        TCSession tcSession = (TCSession) AIFUtility.getDefaultSession();
        DataManagementService dmService = DataManagementService.getService(tcSession);
        
        ValidateInput[] validateInputs = new ValidateInput[1];
        
        ValidateInput validateInput = new ValidateInput();
        
        validateInput.clientId = getClientId();
        validateInput.operationType = getOperationType();
        validateInput.businessObjectName = getType();
        validateInput.propValuesMap = getValidateInputMap(m_propertyValues, propertyValuesMap);
        
        validateInputs[0] = validateInput;
        
        ValidateValuesResponse validateResponse = dmService.validateValues(validateInputs);
        
        Map<String, ValidationResultsHelper> resultHelper = ValidationResultsHelper.create(validateResponse);
        
        return resultHelper;
    }
    
}