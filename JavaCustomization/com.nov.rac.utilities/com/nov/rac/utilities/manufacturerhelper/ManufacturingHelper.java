package com.nov.rac.utilities.manufacturerhelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;

public class ManufacturingHelper
{
    private static ManufacturingHelper instance = null;
    private Map m_mgfIdsToMfgNames = new HashMap<String, String>();
    private Map m_mfgNamesToMgfIds = new HashMap<String, String>();
    
    private ManufacturingHelper()
    {
        NOVManufacturerHelper mfgHelper = new NOVManufacturerHelper();
        
        try
        {
            mfgHelper.setMfgStatus("*");
            mfgHelper.setMfgID("*");
            INOVResultSet theResSet = mfgHelper
                    .execute(INOVSearchProvider.INIT_LOAD_ALL);
            
            int MfgIdIndex = 2;
            int MfgNameIndex = 1;
            if (theResSet != null)
            {
                for (int inx = 0; inx < theResSet.getRows(); inx++)
                {
                    Vector<String> mfgSOARetObj= theResSet.getRowData();
                    int nCurrentRowIndex = (inx * theResSet.getCols());
                    String mfgId = mfgSOARetObj.get(nCurrentRowIndex
                            + MfgIdIndex);
                    String mfgName = mfgSOARetObj.get(nCurrentRowIndex
                            + MfgNameIndex);
                    m_mgfIdsToMfgNames.put(mfgId, mfgName);
                    m_mfgNamesToMgfIds.put(mfgName, mfgId);
                }
            }
        }
        catch (Exception e)
        {
            
        }
    }
    
    public static ManufacturingHelper getInstance()
    {
        if (instance == null)
        {
            instance = new ManufacturingHelper();
        }
        return instance;
    }
    
    // ask about event invalid ids error handling
    public String[] getMfgNames(String[] mfgIds)
    {
        String[] retObj = new String[mfgIds.length];
        
        for (int inx = 0; inx < mfgIds.length; inx++)
        {
            retObj[inx] = (String) m_mgfIdsToMfgNames.get(mfgIds[inx]);
        }
        return retObj;
    }
    
    public Map<String, String> getMFG_IdNameMapping()
    {
        return m_mgfIdsToMfgNames;
    }
    
    public Map<String, String> getMFG_NameIdMapping()
    {
        return m_mfgNamesToMgfIds;
    }
}
