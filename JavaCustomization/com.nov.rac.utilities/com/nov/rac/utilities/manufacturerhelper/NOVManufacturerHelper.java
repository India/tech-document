package com.nov.rac.utilities.manufacturerhelper;

import java.util.Vector;

import com.nov.quicksearch.services.INOVResultSet;
import com.nov.quicksearch.services.INOVSearchProvider;
import com.nov.quicksearch.services.INOVSearchResult;
import com.nov.quicksearch.services.NOVSearchExecuteHelperUtils;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickBindVariable;
import com.novquicksearch.services.rac.quicksearch._2009_10.QuickSearchService.QuickHandlerInfo;

public class NOVManufacturerHelper implements INOVSearchProvider

{
    private String m_mfgID = null;
    private String m_mfgStatus = null;
    
    public INOVResultSet execute(int initLoadAll) throws Exception
    {
        m_mfgID = getMfgID();
        m_mfgStatus = getMfgStatus();
        
        INOVSearchProvider searchService = this;
        INOVSearchResult searchResult = null;
        
        searchResult = NOVSearchExecuteHelperUtils.searchSOACallServer(
                searchService, initLoadAll);
        INOVResultSet theResSet = searchResult.getResultSet();
        return theResSet;
        
    }
    
    @Override
    public String getBusinessObject()
    {
        
        return null;
    }
    
    @Override
    public QuickBindVariable[] getBindVariables()
    {
        
        QuickSearchService.QuickBindVariable[] allBindVars = new QuickSearchService.QuickBindVariable[2];
        
        allBindVars[0] = new QuickSearchService.QuickBindVariable();
        allBindVars[0].nVarType = POM_string;
        allBindVars[0].nVarSize = 1;
        allBindVars[0].strList = new String[] { m_mfgStatus };
        
        allBindVars[1] = new QuickSearchService.QuickBindVariable();
        allBindVars[1].nVarType = POM_string;
        allBindVars[1].nVarSize = 1;
        allBindVars[1].strList = new String[] { m_mfgID };
        
        return allBindVars;
    }
    
    @Override
    public QuickHandlerInfo[] getHandlerInfo()
    {
        QuickSearchService.QuickHandlerInfo[] allHandlerInfo = new QuickSearchService.QuickHandlerInfo[1];
        
        allHandlerInfo[0] = new QuickSearchService.QuickHandlerInfo();
        allHandlerInfo[0].handlerName = "NOVSRCH-get-rsActiveMfgs";
        allHandlerInfo[0].listBindIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listReqdColumnIndex = new int[] { 1, 2 };
        allHandlerInfo[0].listInsertAtIndex = new int[] { 1, 2 };
        
        return allHandlerInfo;
    }
    
    public String getMfgID()
    {
        return m_mfgID;
    }
    
    public void setMfgID(String m_mfgID)
    {
        this.m_mfgID = m_mfgID;
    }
    
    public String getMfgStatus()
    {
        return m_mfgStatus;
        
    }
    
    public void setMfgStatus(String m_mfgStatus)
    {
        this.m_mfgStatus = m_mfgStatus;
    }
    
}
