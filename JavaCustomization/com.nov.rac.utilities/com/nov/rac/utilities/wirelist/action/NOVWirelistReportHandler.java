package com.nov.rac.utilities.wirelist.action;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.nov.rac.utilities.wirelist.dialog.NOVTemplateSelectionDialog;
import com.nov.rac.utilities.wirelist.utils.NOVWirelistReportValidator;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class NOVWirelistReportHandler extends AbstractHandler
{
    private InterfaceAIFComponent[] m_comps = null;
    
    /**
     * The constructor.
     */
    public NOVWirelistReportHandler()
    {
        super();
    }
    
    /**
     * the command has been executed, so extract extract the needed information
     * from the application context.
     */
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        m_comps = AIFUtility.getTargetComponents();
        
        NOVWirelistReportValidator validator = new NOVWirelistReportValidator(m_comps);
        boolean isValid = validator.validateSelectedItem();
        if (!isValid)
        {
            return null;
        }
        
        NOVTemplateSelectionDialog templateDialog = new NOVTemplateSelectionDialog(window.getShell(), validator);
        templateDialog.open();
        
        return null;
    }
}
