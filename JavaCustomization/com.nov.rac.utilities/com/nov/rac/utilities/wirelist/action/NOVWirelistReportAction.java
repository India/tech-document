package com.nov.rac.utilities.wirelist.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.utilities.wirelist.csvreports.NOVCSVCreator;
import com.nov.rac.utilities.wirelist.reports.NOVWirelistWordDocument;
import com.nov.rac.utilities.wirelist.utils.NOVReadXMLFile;
import com.nov.rac.utilities.wirelist.utils.NOVWirelistReportValidator;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinition;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinitionType;
import com.teamcenter.rac.kernel.TCComponentDatasetType;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVWirelistReportAction
{
    private TCSession m_session;
    private NOVWirelistReportValidator m_validator;
    private TCComponentItemRevision m_selectedItemRev;
    private TCComponentDataset m_selectedXMLDataset;
    private TCComponentItemRevision m_previousItemRev[];
    private TCComponentDataset m_previousRevXMLDataset;
    private TCComponentDataset m_selectedTemplateDataset;
    private File m_inputXMLFile;
    private File m_prevRevXMLFile;
    private File m_templateWordFile;
    private File m_genWordDocfile;
    private String[][] m_xmlVal;
    private String[][] m_xmlNumRepVal;
    private boolean m_flag;
    private String m_tempFolder;
    private Registry m_reg;
    private String m_selRevXMLFileName;
    
    public NOVWirelistReportAction(TCSession session, NOVWirelistReportValidator validator)
    {
        m_session = session;
        m_validator = validator;
        m_reg = Registry.getRegistry(this);
        m_tempFolder = getTempFolderLoc();
        m_selectedItemRev = m_validator.getSelectedItemRev();
    }
    
    public boolean doOperation(final Shell parent, final TCComponentItem selectedDocTemplate)
    {
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if (display != null)
        {
            display.syncExec((new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(parent);
                        progressMonitorDialog.run(true, false, new IRunnableWithProgress()
                        {
                            @Override
                            public void run(final IProgressMonitor monitor) throws InvocationTargetException,
                                    InterruptedException
                            {
                                if(deleteExistingDataset())
                                {
                                    return;
                                }
                                try
                                {
                                    performOperation(selectedDocTemplate);
                                }
                                catch (TCException e)
                                {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                if(m_xmlVal != null)
                                {
                                    generateCSVFiles();
                                    m_flag = true;
                                }
                            }
                        });
                    }
                    catch (InvocationTargetException exp)
                    {
                        m_flag = false;
                        Throwable theCause = exp.getCause();
                        MessageBox.post(theCause);
                    }
                    catch (InterruptedException exp)
                    {
                        m_flag = false;
                        Throwable theCause = exp.getCause();
                        MessageBox.post(theCause);
                    }
                }
            }));
        }
        return m_flag;
    }
    
    public boolean deleteExistingDataset()
    {
        boolean errorInDeletion = false;
        
        TCComponent[] datasets = null;
        TCComponentDataset dataset = null;
        try
        {
            datasets = m_selectedItemRev.getRelatedComponents("IMAN_specification");
            for (int index = 0; datasets != null && index < datasets.length; index++)
            {
                if (datasets[index] instanceof TCComponentDataset)
                {
                    dataset = (TCComponentDataset) datasets[index];
                    if (dataset.getType().equalsIgnoreCase(m_reg.getString("wordDataset.TYPE"))
                            || dataset.getType().equalsIgnoreCase(m_reg.getString("txtDataset.TYPE")))
                    {
                        String datasetName = dataset.getTCProperty("object_name").toString();
                        String generatedDatasets[] = m_reg.getString("GeneratedDataset.LIST").split(",");
                        for (int i = 0; i < generatedDatasets.length; i++)
                        {
                            if (datasetName.equalsIgnoreCase(generatedDatasets[i]))
                            {
                                m_selectedItemRev.remove("IMAN_specification", dataset);
                                try{
                                    dataset.delete();
                                }
                                catch (TCException e)
                                {
                                    m_selectedItemRev.add("IMAN_specification", dataset);
                                    MessageBox.post("Cannot generate wirelist report.", "Error",
                                            MessageBox.ERROR);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            errorInDeletion = true;
            e.printStackTrace();
        }
        return errorInDeletion;
    }
    
    public void performOperation(TCComponentItem selectedDocTemplate) throws TCException
    {
        m_previousItemRev = m_validator.getPreviousRevision(m_selectedItemRev);
        if (m_previousItemRev != null && m_previousItemRev.length > 0)
        {
            int lastPreRev = m_previousItemRev.length - 1;
            m_previousRevXMLDataset = m_validator.getDataset(m_previousItemRev[lastPreRev],
                    m_reg.getString("xmlDataset.TYPE"));
            if(m_previousRevXMLDataset != null)
            {
                m_prevRevXMLFile = importDatasetFile(m_previousRevXMLDataset, m_reg.getString("xmlDataset.TYPE"));
				
            	// Renaming the previous XML file to avoid over-riding
            	if(m_prevRevXMLFile != null) {
	   				String prevRevID = m_previousItemRev[lastPreRev].getProperty("current_revision_id");
	            	String filename = m_prevRevXMLFile.getName().substring(0, m_prevRevXMLFile.getName().indexOf("."));
	            	String newFileName = m_prevRevXMLFile.getParent() + "\\" + filename + "-" + prevRevID + ".xml";
	            	m_prevRevXMLFile.renameTo(new File(newFileName));
	            	m_prevRevXMLFile = new File(newFileName);
            	}
            }
        }
        
        /********* Getting Dataset under selected Document template ************/
        String[] namedReferences = new String[1];
        namedReferences = getNamedRefOfDataSet(m_session, m_reg.getString("wordDataset.TYPE"));
        try
        {
            TCComponentItemRevision latestRevision = selectedDocTemplate.getLatestItemRevision();
            m_selectedTemplateDataset = m_validator.getDataset(latestRevision, namedReferences[0]);
        }
        catch (TCException e1)
        {
            e1.printStackTrace();
        }
        
        /*************** Importing Template word file doc from Dataset ******************/
        String sTemplateFile = null;
        m_templateWordFile = importDatasetFile(m_selectedTemplateDataset, namedReferences[0]);
        if (m_templateWordFile != null)
        {
            sTemplateFile = m_templateWordFile.getAbsolutePath();
        }
        else{
            MessageBox.post("No Template available. Cannot generate Wirelist report.", "Error",
                    MessageBox.ERROR);
            return;
        }
        String sOutputFile = getOutputFileName(m_selectedItemRev);
        
        /*************** Importing XML Dataset ***********************/
        m_selectedXMLDataset = m_validator.getDataset(m_selectedItemRev, m_reg.getString("xmlDataset.TYPE"));
        m_inputXMLFile = importDatasetFile(m_selectedXMLDataset, m_reg.getString("xmlDataset.TYPE"));
        
        //Tushar_Start
        // Renaming the selected rev XML file to avoid over-riding
    	if(m_inputXMLFile != null) 
    	{
    		//Taking the file name in a variable, which will be used to replace placeholder for Reference
    		m_selRevXMLFileName = m_inputXMLFile.getName();
    		
			String selRevID = m_selectedItemRev.getProperty("current_revision_id");
        	String filename = m_inputXMLFile.getName().substring(0, m_inputXMLFile.getName().indexOf("."));
        	String newFileName = m_inputXMLFile.getParent() + "\\" + filename + "-" + selRevID + ".xml";
        	m_inputXMLFile.renameTo(new File(newFileName));
        	m_inputXMLFile = new File(newFileName);
    	}
        //Tushar_End
        
        /************** Creating Word Doc ***************/
        if (m_inputXMLFile != null && m_templateWordFile != null)
        {
            try
            {
                createAndGenerateDoc(sTemplateFile, sOutputFile);
            }
            catch (FileNotFoundException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (Docx4JException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        /*********** Deleting the created files from local ***************/
        deleteGeneratedFiles();
    }
    
    private void createAndGenerateDoc(String sTemplateFile, String sOutputFile) throws FileNotFoundException,
            Docx4JException
    {
        /************** Reading input XML Dataset ***************/
        NOVReadXMLFile readXMLObj = new NOVReadXMLFile(m_inputXMLFile.getAbsolutePath());
        m_xmlVal = readXMLObj.readXML();
        
        /************** Collecting XML data (previous revisions + selected revision) for Numerical report ***************/
        collectXMLDataForNumericalReport();
        
        /************** Creating Word Doc and writing ***************/
        //Tushar_Start Passing m_selRevXMLFileName instead of m_inputXMLFile.getName() bcoz it is renamed
        NOVWirelistWordDocument createDocObj = new NOVWirelistWordDocument(sTemplateFile, sOutputFile, m_selRevXMLFileName);
        try
        {
            boolean isXMLAttachedToPrevRev = false;
            
            if (m_prevRevXMLFile != null)
            {
                createDocObj.setXMLFilePaths(m_inputXMLFile, m_prevRevXMLFile);
                isXMLAttachedToPrevRev = true;
            }
            
            createDocObj.createDocument(m_xmlVal, m_xmlNumRepVal, m_selectedItemRev, m_previousItemRev, isXMLAttachedToPrevRev);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        /****** Exporting generated word doc to TC **********/
        m_genWordDocfile = new File(sOutputFile);
        exportWordDataset(m_selectedItemRev, m_genWordDocfile);
    }
    
    private void collectXMLDataForNumericalReport () {

    	String xmlType = m_reg.getString("xmlDataset.TYPE");
    	List<String[]> finalArrList = new ArrayList<String[]>();
    	
    	// collect XML data for previous revisions
        int prevRevLen = m_previousItemRev.length;
		for(int revIdx=0; revIdx<prevRevLen; revIdx++) 
		{
			TCComponentDataset xmlDataset = null;
			xmlDataset  = m_validator.getDataset(m_previousItemRev[revIdx], xmlType);
			if(xmlDataset != null) 
			{
				File inpFile = null;
				inpFile = importDatasetFile(xmlDataset, xmlType);
				if(inpFile != null) 
				{
					NOVReadXMLFile readXMLObj = new NOVReadXMLFile(inpFile.getAbsolutePath());
					String[][] tempXMLVal = readXMLObj.readXML();
					if(tempXMLVal != null)
					{
						for(int i=0;i<tempXMLVal.length;i++) 
						{
							finalArrList.add(tempXMLVal[i]); // Java class cast exception issue while converting back to array with adding directly 2D array, hence adding row by row
						}
					} 
				}				
				//Tushar_Start Deleting the file after use
				if(inpFile != null)
				{
					inpFile.delete();
				}
				//Tushar_End
			}
		}
		
		// collect XML data for selected revision also
		if(m_xmlVal != null) {
			for(int i=0;i<m_xmlVal.length;i++) {
				finalArrList.add(m_xmlVal[i]);
			}
			m_xmlNumRepVal = new String[finalArrList.size()][];
			m_xmlNumRepVal = (String[][])finalArrList.toArray(new String[finalArrList.size()][]);
		}
   }
    
    private void generateCSVFiles()
    {
    	String[] batchCodes = (m_inputXMLFile.getName().substring(0, m_inputXMLFile.getName().indexOf("."))).split("-");

        NOVCSVCreator creator = new NOVCSVCreator(m_selectedItemRev, batchCodes, m_xmlVal);
        creator.generateAllCSVFile(m_tempFolder);
    }
    
    private File importDatasetFile(TCComponentDataset dataset, String datasetType)
    {
        File[] imanFile = null;
        try
        {
            imanFile = dataset.getFiles(datasetType, m_tempFolder);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return (imanFile != null) ? imanFile[0] : null;
    }
    
    private void exportWordDataset(TCComponentItemRevision itemRev, File datasetFile)
    {
        String[] namedReferences = new String[1];
        namedReferences = getNamedRefOfDataSet(m_session, m_reg.getString("wordDataset.TYPE"));
        
        try
        {
            TCComponentDatasetType newDataset = (TCComponentDatasetType) m_session.getTypeService().getTypeComponent(
                    "Dataset");
            TCComponentDataset newDatasetInstance = newDataset.setFiles(m_reg.getString("wirelistReport.NAME"), "",
                    m_reg.getString("wordDataset.TYPE"), new String[] { datasetFile.getAbsolutePath() },
                    namedReferences);
            itemRev.add("IMAN_specification", newDatasetInstance);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private String[] getNamedRefOfDataSet(TCSession tcsession, String datasettype)
    {
        String namedRef[] = null;
        try
        {
            TCComponentDatasetDefinitionType tccompdatasetdefitype = (TCComponentDatasetDefinitionType) tcsession
                    .getTypeComponent("DatasetType");
            
            TCComponentDatasetDefinition datasetdef = tccompdatasetdefitype.find(datasettype);
            namedRef = datasetdef.getNamedReferences();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return namedRef;
    }
    
    private String getOutputFileName(TCComponentItemRevision selectedDocItemRev)
    {
        StringBuffer fileName = new StringBuffer(m_tempFolder);
        try
        {
            fileName.append("\\");
            fileName.append(m_reg.getString("wirelistReport.NAME"));
            fileName.append("-");
            fileName.append(selectedDocItemRev.getTCProperty("object_name").toString());
            fileName.append(".docx");
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return fileName.toString();
    }
    
    private void deleteGeneratedFiles()
    {
    	if(m_prevRevXMLFile != null)
    	{
	        if (m_prevRevXMLFile.isFile())
	        {
	            m_prevRevXMLFile.delete();
	        }
    	}
    	if(m_inputXMLFile != null)
    	{
	        if (m_inputXMLFile.isFile())
	        {
	            m_inputXMLFile.delete();
	        }
    	}
    	if(m_templateWordFile != null)
    	{
    		if (m_templateWordFile.isFile())
	        {
	            m_templateWordFile.delete();
	        }
    	}
    	if(m_genWordDocfile != null)
    	{
    		if (m_genWordDocfile.isFile())
            {
                m_genWordDocfile.delete();
            }
    	}      
    }
    
    private String getTempFolderLoc()
    {
        TCPreferenceService prefServ = m_session.getPreferenceService();
        String sTCTempDir = prefServ.getString(TCPreferenceService.TC_preference_site,
                m_reg.getString("tempDirPrefrence.NAME"));
        return sTCTempDir;
    }
}
