package com.nov.rac.utilities.wirelist.utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class NOVWirelistNode
{
    public boolean m_isOmitted;
    public Node m_objNode;
    public String m_revision;
    
    public NOVWirelistNode(Node tempNode)
    {
        m_isOmitted = true;
        m_objNode = tempNode; // should we use oNode.CloneNode(True) instead?
        m_revision = ((Element) tempNode).getElementsByTagName("REV").item(0).getFirstChild().getNodeValue();
    }
}
