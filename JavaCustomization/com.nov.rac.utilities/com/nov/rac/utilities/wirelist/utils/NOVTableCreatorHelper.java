package com.nov.rac.utilities.wirelist.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Vector;

import javax.xml.bind.JAXBException;

import org.docx4j.XmlUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.Tc;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.docx4j.wml.TrPr;

import com.nov.rac.utilities.wirelist.reports.NOVWirelistWordDocument;
import com.teamcenter.rac.util.Registry;

public class NOVTableCreatorHelper
{
    private Registry m_registry = Registry.getRegistry(this);
    ObjectFactory m_factory = Context.getWmlObjectFactory();
    WordprocessingMLPackage m_wordMLPackage = null;
    String[] m_columnHeaders = null;
    Vector<String> m_tableData = null;
    int m_noOfColumns = 0;
    
    public NOVTableCreatorHelper(WordprocessingMLPackage wordMLPackage, String[] columnHeaders, Vector<String> tableData)
    {
        m_wordMLPackage = wordMLPackage;
        m_columnHeaders = columnHeaders;
        m_tableData = tableData;
        m_noOfColumns = m_columnHeaders.length;
    }
    
    public void createTableForReport() throws IOException, Docx4JException, JAXBException
    {
        // Adding table headers
        Tbl table = createTblHeaders(m_columnHeaders);
        
        /************** adding row data to table *************************/
        addingRowsToTable(table, m_tableData);
        
        /************** adding table to doc *************************/
        m_wordMLPackage.getMainDocumentPart().addObject(table);
    }
    
    // Insert at a particular point before General Wiring Notes
    public void createAndInsertTableForReport() throws Exception
    {
        // Adding table headers
        Tbl table = createTblHeaders(m_columnHeaders);
        
        /************** adding row data to table *************************/
        addingRowsToTable(table, m_tableData);
        
        NOVWirelistWordDocument.insertObjectBeforeText(m_wordMLPackage,
                m_registry.getString("wiringNotesReference.Str"), table);
    }
    
    public Tbl createTblHeaders(String[] colHeaders) throws JAXBException
    {
        Tbl table = m_factory.createTbl();
        setTableLayout(table);
        Tr tableRow = m_factory.createTr();
        
        // Repeat Table Headers if table extends beyond page break
        String strTrPr = "<w:trPr xmlns:w=\"schemas.openxmlformats.org/wordprocessingml/2006/main\">"
                + "<w:tblHeader />" + "</w:trPr>";
        
        tableRow.setTrPr((TrPr) XmlUtils.unmarshalString(strTrPr, Context.jc, TrPr.class));
        BooleanDefaultTrue bdt = Context.getWmlObjectFactory().createBooleanDefaultTrue();
        TrPr trPr = tableRow.getTrPr();
        trPr.getCnfStyleOrDivIdOrGridBefore().add(Context.getWmlObjectFactory().createCTTrPrBaseTblHeader(bdt));
        
        for (int index = 0; index < m_noOfColumns; index++)
        {
            addTableCell(m_factory, tableRow, colHeaders[index], true);
        }
        
        table.getContent().add(tableRow);
        return table;
    }
    
    public void addingRowsToTable(Tbl table, Vector<String> xmlVal) throws JAXBException
    {
        int k = m_noOfColumns;
        int totalRowNum = xmlVal.size() / m_noOfColumns;
        System.out.println("Total no of rows: " + totalRowNum);
        for (int i = 0; i < totalRowNum; i++)
        {
            Tr tableRow = m_factory.createTr();
            
            // Disallow splitting of rows during page break
            String strTrPr = "<w:trPr xmlns:w=\"schemas.openxmlformats.org/wordprocessingml/2006/main\">"
                    + "<w:cantSplit/>" + "</w:trPr>";
            tableRow.setTrPr((TrPr) XmlUtils.unmarshalString(strTrPr, Context.jc, TrPr.class));
            BooleanDefaultTrue bdt = Context.getWmlObjectFactory().createBooleanDefaultTrue();
            TrPr trPr = tableRow.getTrPr();
            trPr.getCnfStyleOrDivIdOrGridBefore().add(Context.getWmlObjectFactory().createCTTrPrBaseCantSplit(bdt));
            
            for (int j = 0; j < m_noOfColumns; j++)
            {
                addTableCell(m_factory, tableRow, xmlVal.get(j + k * i), false);
            }
            table.getContent().add(tableRow);
        }
    }
    
    private void addTableCell(ObjectFactory factory, Tr tableRow, String content, Boolean bold)
    {
        Tc tableCell = factory.createTc();
        
        // Default font size for table elements, so 'null' is passed, otherwise
        // fontSize can be passed
        addStyling(tableCell, content, bold, null);
        tableRow.getContent().add(tableCell);
    }
    
    // Adding style to font
    private void addStyling(Tc tc, String content, Boolean bold, String fontSize)
    {
        P paragraph = m_factory.createP();
        
        PPr otherProperties = m_factory.createPPr();
        Jc jc = m_factory.createJc();
        jc.setVal(JcEnumeration.CENTER); // Center align the table cell contents
        otherProperties.setJc(jc);
        paragraph.setPPr(otherProperties);
        
        Text text = m_factory.createText();
        text.setValue(content);
        R run = m_factory.createR();
        run.getContent().add(text);
        
        paragraph.getContent().add(run);
        RPr runProperties = m_factory.createRPr();
        
        // Setting the font size
        if (fontSize != null)
        {
            setFontSize(runProperties, fontSize);
        }
        // Setting the font bold
        BooleanDefaultTrue b = new BooleanDefaultTrue();
        if (bold == true)
        {
            b.setVal(true);
        }
        else
        {
            b.setVal(false);
        }
        runProperties.setB(b);
        run.setRPr(runProperties);
        tc.getContent().add(paragraph);
    }
    
    private static void setFontSize(RPr runProperties, String fontSize)
    {
        HpsMeasure size = new HpsMeasure();
        size.setVal(new BigInteger(fontSize));
        runProperties.setSz(size);
        runProperties.setSzCs(size);
    }
    
    private void setTableLayout(Tbl table)
    {
        String strTblPr = "<w:tblPr " + Namespaces.W_NAMESPACE_DECLARATION + ">" + "<w:tblStyle w:val=\"TableGrid\"/>"
                + "<w:tblW w:w=\"0\" w:type=\"auto\"/>" + "<w:tblLook w:val=\"04A0\"/>" + "</w:tblPr>";
        TblPr tblPr = null;
        
        try
        {
            tblPr = (TblPr) XmlUtils.unmarshalString(strTblPr);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
        table.setTblPr(tblPr);
    }
}
