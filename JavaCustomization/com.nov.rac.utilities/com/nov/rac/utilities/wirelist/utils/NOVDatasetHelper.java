package com.nov.rac.utilities.wirelist.utils;

import com.teamcenter.rac.kernel.TCAttachmentScope;
import com.teamcenter.rac.kernel.TCAttachmentType;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinition;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinitionType;
import com.teamcenter.rac.kernel.TCComponentDatasetType;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentTask;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;

public class NOVDatasetHelper
{
    private TCComponentItemRevision m_itemRev;
    private TCComponentDataset m_existingDataset = null;
    private TCSession m_session;
    
    public NOVDatasetHelper(TCSession session)
    {
        m_session = session;
    }
    
    public NOVDatasetHelper(TCSession session, TCComponentItemRevision itemRev)
    {
        m_session = session;
        m_itemRev = itemRev;
    }
    
    public void createNewDataset(String[] filePathNames, String fileName, String sDatasetType,
            TCComponentTask itemRevTask) throws TCException
    {
        String[] namedReferences = getNamedRefOfDataSet(sDatasetType);
        TCComponentDatasetType datasetType = (TCComponentDatasetType) m_session.getTypeService().getTypeComponent(
                "Dataset");
        TCComponentDataset newDataset = datasetType
                .setFiles(fileName, "", sDatasetType, filePathNames, namedReferences);
        m_itemRev.add("IMAN_specification", newDataset);
        if (itemRevTask != null)
        {
            itemRevTask.addAttachments(TCAttachmentScope.LOCAL, new TCComponent[] { newDataset },
                    new int[] { TCAttachmentType.TARGET });
        }
    }
    
    public boolean isExistingDataset(TCComponent[] datasets, String sDatasetName, String sDatasetType)
            throws TCException
    {
        boolean isExist = false;
        for (int index = 0; datasets != null && index < datasets.length; index++)
        {
            if (datasets[index] instanceof TCComponentDataset
                    && (datasets[index].getType().equalsIgnoreCase(sDatasetType)))
            {
                String datasetName = datasets[index].getTCProperty("object_name").toString();
                if (sDatasetName.equalsIgnoreCase(datasetName))
                {
                    isExist = true;
                    m_existingDataset = (TCComponentDataset) datasets[index];
                    break;
                }
            }
        }
        return isExist;
    }
    
    public void updateDataset(TCComponentDataset newDataset, String[] filePathNames, String fileName, String datasetType)
            throws TCException
    {
        String[] namedReferences = getNamedRefOfDataSet(datasetType);
        String[] fileTypes = new String[1];
        if (newDataset.getType().equals("MSWord"))
        {
            fileTypes[0] = "word";
        }
        else
        {
            fileTypes[0] = newDataset.getType();
        }
        String[] fileSubTypes = new String[1];
        fileSubTypes[0] = newDataset.getSubType();
        
        String[] checkNamedRef = newDataset.getFileNames(fileTypes[0]);
        
        for (int ipx = 0; checkNamedRef != null && ipx < checkNamedRef.length; ipx++)
        {
            if (checkNamedRef != null && checkNamedRef[ipx].startsWith(fileName))
            {
                newDataset.removeFiles(fileTypes[0]);
            }
        }
        newDataset.setFiles(filePathNames, fileTypes, fileSubTypes, namedReferences, 20);
    }
    
    public String[] getNamedRefOfDataSet(String datasettype)
    {
        String namedRef[] = null;
        try
        {
            TCComponentDatasetDefinitionType tccompdatasetdefitype = (TCComponentDatasetDefinitionType) m_session
                    .getTypeComponent("DatasetType");
            
            TCComponentDatasetDefinition datasetdef = tccompdatasetdefitype.find(datasettype);
            namedRef = datasetdef.getNamedReferences();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return namedRef;
    }
    
    public TCComponentDataset getExistingDataset()
    {
        return m_existingDataset;
    }
    
}
