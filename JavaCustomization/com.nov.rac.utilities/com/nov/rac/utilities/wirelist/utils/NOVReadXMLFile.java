package com.nov.rac.utilities.wirelist.utils;

import java.io.File;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NOVReadXMLFile
{
    String m_fileName;
    
    public NOVReadXMLFile(String fileName)
    {
        m_fileName = fileName;
    }
    
    public String[][] readXML()
    {
        // Variables for count of valid records, null records
        int validRecordsCount = 0;
        int nullCount = 0;
        String[][] xmlValWithValidRecords = null;
        
        String xmlVal[][] = null;
        
        // Vector of required nodes
        Vector<String> xmlNodes = new Vector<String>();
        xmlNodes.add("WIRE");
        xmlNodes.add("REV");
        xmlNodes.add("LOC_F");
        xmlNodes.add("COMP_F");
        xmlNodes.add("TERM_F");
        xmlNodes.add("LOC_T");
        xmlNodes.add("COMP_T");
        xmlNodes.add("TERM_T");
        xmlNodes.add("SIZE");
        xmlNodes.add("REMARKS");
        
        try
        {
            File fXmlFile = new File(m_fileName);
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            Document doc = dBuilder.parse(fXmlFile);
            
            doc.getDocumentElement().normalize();
            
            NodeList nList = doc.getElementsByTagName("Rec");
            
            // Initializing the 2-D String array
            xmlVal = new String[nList.getLength()][(nList.item(0).getChildNodes().getLength() - 1) / 2];
            
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                Node nNode = nList.item(temp);
                
                if (nNode.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) nNode;
                    
                    // Check if wire number is blank, then skip that record
                    String wireNumber = eElement.getElementsByTagName(xmlNodes.get(0)).item(0).getTextContent();
                    if (wireNumber.isEmpty())
                    {
                        nullCount++;
                        continue;
                    }
                    
                    // Populating the 2-D string array with data under each
                    // "Rec" node
                    for (int col = 0; col < xmlNodes.size(); col++)
                    {
                        xmlVal[validRecordsCount][col] = eElement.getElementsByTagName(xmlNodes.get(col)).item(0)
                                .getTextContent();
                    }
                }
                validRecordsCount++;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        // Creating a new 2D array after removing the null rows
        xmlValWithValidRecords = new String[xmlVal.length - nullCount][];
        System.arraycopy(xmlVal, 0, xmlValWithValidRecords, 0, validRecordsCount);
        
        return xmlValWithValidRecords;
    }
}
