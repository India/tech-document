package com.nov.rac.utilities.wirelist.utils;

import java.util.List;

import org.docx4j.jaxb.Context;
import org.docx4j.model.structure.SectionWrapper;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.HeaderPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.wml.FldChar;
import org.docx4j.wml.Hdr;
import org.docx4j.wml.HdrFtrRef;
import org.docx4j.wml.HeaderReference;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.STFldCharType;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.Text;

public class NOVDocumentHeaderCreator
{
    private WordprocessingMLPackage m_wordMLPackage;
    private ObjectFactory m_factory = Context.getWmlObjectFactory();
    String m_docNumber = null;
    String m_revId = null;
    
    public NOVDocumentHeaderCreator(WordprocessingMLPackage wordMLPackage, String docNumber, String revId)
    {
        m_wordMLPackage = wordMLPackage;
        m_docNumber = docNumber;
        m_revId = revId;
    }
    
    /**
     * First we create the package and the factory. Then we create the header
     * part, which returns a relationship. This relationship is then used to
     * create a reference. Finally we add some text to the document and save it.
     */
    public void createHeaderForDoc() throws Docx4JException
    {
        Relationship relationship = createHeaderPart();
        createHeaderReference(relationship);
    }
    
    /**
     * This method creates a header part and set the package on it. Then we add
     * some text and add the header part to the package. Finally we return the
     * corresponding relationship.
     * 
     * @return
     * @throws InvalidFormatException
     */
    public Relationship createHeaderPart() throws InvalidFormatException
    {
        HeaderPart headerPart = new HeaderPart();
        headerPart.setPackage(m_wordMLPackage);
        headerPart.setJaxbElement(createHeader());
        return m_wordMLPackage.getMainDocumentPart().addTargetPart(headerPart);
    }
    
    /**
     * First we create a header, a paragraph, a run and a text. We add the given
     * given content to the text and add that to the run. The run is then added
     * to the paragraph, which is in turn added to the header. Finally we return
     * the header.
     * 
     * @param content
     * @return
     */
    private Hdr createHeader()
    {
        Hdr header = m_factory.createHdr();
        R.Tab tab = m_factory.createRTab();
        
        // Add Document No
        P paraDoumentNo = m_wordMLPackage.getMainDocumentPart().createStyledParagraphOfText("Subtitle",
                "Document Number");
        R runDocNo = m_factory.createR();
        Text docNo = new Text();
        docNo.setSpace("preserve");
        docNo.setValue(m_docNumber);
        
        runDocNo.getContent().add(tab);
        runDocNo.getContent().add(docNo);
        paraDoumentNo.getContent().add(runDocNo);
        
        // Add Revision
        P paraRevision = m_wordMLPackage.getMainDocumentPart().createStyledParagraphOfText("Subtitle", "Revision");
        R runRev = m_factory.createR();
        Text revId = new Text();
        revId.setSpace("preserve");
        revId.setValue(m_revId);
        runRev.getContent().add(tab);
        runRev.getContent().add(revId);
        paraRevision.getContent().add(runRev);
        
        // Add PageNo
        P paraPageNo = m_wordMLPackage.getMainDocumentPart().createStyledParagraphOfText("Subtitle", "Page No");
        R runParaNo = m_factory.createR();
        Text pageNumber = new Text();
        pageNumber.setValue("");
        pageNumber.setSpace("preserve");
        runParaNo.getContent().add(tab);
        
        runParaNo.getContent().add(pageNumber);
        paraPageNo.getContent().add(runParaNo);
        
        addFieldBegin(paraPageNo);
        addPageNumberField(paraPageNo);
        addFieldEnd(paraPageNo);
        
        header.getContent().add(paraDoumentNo);
        header.getContent().add(paraRevision);
        header.getContent().add(paraPageNo);
        
        return header;
    }
    
    /**
     * We use the PAGE command, which prints the number of the current page,
     * together with the MERGEFORMAT switch, which indicates that the current
     * formatting should be preserved when the field is updated.
     * 
     * @param paragraph
     */
    private void addPageNumberField(P paragraph)
    {
        R run = m_factory.createR();
        Text txt = new Text();
        txt.setSpace("preserve");
        txt.setValue(" PAGE   \\* MERGEFORMAT ");
        run.getContent().add(m_factory.createRInstrText(txt));
        paragraph.getContent().add(run);
    }
    
    /**
     * Every fields needs to be delimited by complex field characters. This
     * method adds the delimiter that precedes the actual field to the given
     * paragraph.
     * 
     * @param paragraph
     */
    private void addFieldBegin(P paragraph)
    {
        R runBegin = m_factory.createR();
        FldChar fldchar = m_factory.createFldChar();
        fldchar.setFldCharType(STFldCharType.BEGIN);
        runBegin.getContent().add(fldchar);
        paragraph.getContent().add(runBegin);
    }
    
    /**
     * Every fields needs to be delimited by complex field characters. This
     * method adds the delimiter that follows the actual field to the given
     * paragraph.
     * 
     * @param paragraph
     */
    private void addFieldEnd(P paragraph)
    {
        FldChar fieldCharEnd = m_factory.createFldChar();
        fieldCharEnd.setFldCharType(STFldCharType.END);
        R runEnd = m_factory.createR();
        runEnd.getContent().add(fieldCharEnd);
        paragraph.getContent().add(runEnd);
    }
    
    /**
     * First we retrieve the document sections from the package. As we want to
     * add a header, we get the last section and take the section properties
     * from it. The section is always present, but it might not have properties,
     * so we check if they exist to see if we should create them. If they need
     * to be created, we do and add them to the main document part and the
     * section. Then we create a reference to the header, give it the id of the
     * relationship, set the type to header reference and add it to the
     * collection of references to headers and footers in the section
     * properties.
     * 
     * @param relationship
     */
    private void createHeaderReference(Relationship relationship)
    {
        List<SectionWrapper> sections = m_wordMLPackage.getDocumentModel().getSections();
        
        SectPr sectionProperties = sections.get(sections.size() - 1).getSectPr();
        // There is always a section wrapper, but it might not contain a sectPr
        if (sectionProperties == null)
        {
            sectionProperties = m_factory.createSectPr();
            m_wordMLPackage.getMainDocumentPart().addObject(sectionProperties);
            sections.get(sections.size() - 1).setSectPr(sectionProperties);
        }
        
        HeaderReference headerReference = m_factory.createHeaderReference();
        headerReference.setId(relationship.getId());
        headerReference.setType(HdrFtrRef.DEFAULT);
        sectionProperties.getEGHdrFtrReferences().add(headerReference);
    }
}
