package com.nov.rac.utilities.wirelist.utils;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.DefaultRowSorter;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;

public class NOVDataManipulator
{
    public Vector<String> getModifiedVector(JTable jtab, String[] columnNamesJtable, String[] sortingKeys)
    {
        Vector<String> modifiedVector = new Vector<String>();
        
        // Sort as per the sorting keys provided
        sortRowData(jtab, sortingKeys);
        
        for (int i = 0; i < jtab.getRowCount(); i++)
        {
            for (int j = 0; j < jtab.getColumnCount(); j++)
            {
                modifiedVector.add((String) jtab.getValueAt(i, j));
            }
        }
        return modifiedVector;
    }
    
    private void sortRowData(JTable jtable, String[] sortKeys)
    {
        int colIndex = 0;
        jtable.setAutoCreateRowSorter(true);
        DefaultRowSorter sorter = ((DefaultRowSorter) jtable.getRowSorter());
        ArrayList<RowSorter.SortKey> list = new ArrayList<RowSorter.SortKey>();
        
        for (int index = 0; index < sortKeys.length; index++)
        {
            colIndex = jtable.getColumn(sortKeys[index]).getModelIndex();
            list.add(new RowSorter.SortKey(colIndex, SortOrder.ASCENDING));
        }
        
        sorter.setSortKeys(list);
        sorter.sort();
    }
}
