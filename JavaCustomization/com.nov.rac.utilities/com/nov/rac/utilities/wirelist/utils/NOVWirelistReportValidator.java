package com.nov.rac.utilities.wirelist.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVWirelistReportValidator
{
    private InterfaceAIFComponent[] m_selectedComps;
    private TCComponentItem m_selectedItem;
    private TCComponentItemRevision[] m_allItemRev;
    private TCComponentItemRevision m_selectedItemRev;
    private TCComponentDataset m_selectedRevDataset;
    private Registry m_reg = Registry.getRegistry(this);
    
    public NOVWirelistReportValidator(InterfaceAIFComponent[] m_comps)
    {
        m_selectedComps = m_comps;
    }
    
    public boolean validateSelectedItem()
    {
        boolean flag = true;
        if (!isValidSelection() || !isSelectedItemValid() || !isAccessOnItemRevision() || !isXMLDatasetAvailable()
                || !isValidItemRevSelected())
        {
            flag = false;
        }
        return flag;
    }
    
    private boolean isValidSelection()
    {
        boolean isValid = true;
        if (m_selectedComps.length <= 0)
        {
            MessageBox.post(m_reg.getString("NoObjectSelected.ERROR"), m_reg.getString("Error.TITLE"), 
                    MessageBox.ERROR);
            isValid = false;
        }
        else if (m_selectedComps.length > 1)
        {
            MessageBox.post(m_reg.getString("MoreObjectsSelected.ERROR"), m_reg.getString("Error.TITLE"),
                    MessageBox.ERROR);
            isValid = false;
        }
        return isValid;
    }
    
    private boolean isSelectedItemValid()
    {
        boolean flag = true;
        TCComponent item = (TCComponent) m_selectedComps[0];
        if (item instanceof TCComponentItemRevision)
        {
            m_selectedItemRev = (TCComponentItemRevision) item;
            try
            {
                m_selectedItem = m_selectedItemRev.getItem();
                m_allItemRev = m_selectedItem.getReleasedItemRevisions(); // Assuming report is run always on working revision just after released revisions.
                if (!m_selectedItemRev.getTCProperty("object_type").toString().startsWith("Document"))
                {
                    flag = false;
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        else if (item instanceof TCComponentItem)
        {
            try
            {
                m_selectedItem = (TCComponentItem) item;
                m_selectedItemRev = ((TCComponentItem) m_selectedItem).getLatestItemRevision();
                if (!m_selectedItemRev.getTCProperty("object_type").toString().startsWith("Document"))
                {
                    flag = false;
                }
            }
            catch (TCException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            flag = false;
        }
        
        if (!flag)
        {
            MessageBox.post(m_reg.getString("SelectDOC.ERROR"), m_reg.getString("Error.TITLE"), MessageBox.ERROR);
        }
        return flag;
    }
    
    private boolean isAccessOnItemRevision()
    {
        boolean isAccess = true;
        try
        {
            if (!m_selectedItemRev.okToModify())
            {
                isAccess = false;
                MessageBox.post(m_reg.getString("NoAccessOnItemRev.ERROR"), m_reg.getString("Error.TITLE"),
                        MessageBox.ERROR);
            }
            else if(isSelectedRevInProgress())
            {
                isAccess = false;
                MessageBox.post(m_reg.getString("ActiveInWorkflow.ERROR"), m_reg.getString("Error.TITLE"),
                        MessageBox.ERROR);
            }
        }
        catch (TCException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isAccess;
    }
    
    private boolean isSelectedRevInProgress()
    {
        boolean isInProgress = false;
        try
        {
            if(m_selectedItemRev.getCurrentJob()!= null)
            {
                isInProgress =true;
            }   
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return isInProgress;
    }
    
    public TCComponentItemRevision getSelectedItemRev()
    {
        return m_selectedItemRev;
    }
    
    private boolean isXMLDatasetAvailable()
    {
        m_selectedRevDataset = getDataset(m_selectedItemRev, "XML");
        if (m_selectedRevDataset == null || !(m_selectedRevDataset.getType().equalsIgnoreCase("XML")))
        {
            MessageBox.post(m_reg.getString("XMLDatasetNotAvailable.ERROR"), m_reg.getString("Error.TITLE"),
                    MessageBox.ERROR);
            return false;
        }
        return true;
    }
    
    public TCComponentDataset getDataset(TCComponentItemRevision itemRev, String dataType)
    {
        TCComponent[] datasets = null;
        TCComponentDataset dataset = null;
        try
        {
            datasets = itemRev.getRelatedComponents("IMAN_specification");
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        for (int index = 0; datasets != null && index < datasets.length; index++)
        {
            if (datasets[index] instanceof TCComponentDataset)
            {
                dataset = (TCComponentDataset) datasets[index];
                if (dataset.getType().equalsIgnoreCase(dataType))
                {
                    break;
                }
            }
        }
        return dataset;
    }
    
    private boolean isValidItemRevSelected()
    {
        if (isRevisionReleased(m_selectedItemRev))
        {
            MessageBox.post(m_reg.getString("ItemRevReleased.ERROR"), m_reg.getString("Error.TITLE"), MessageBox.ERROR);
            return false;
        }
        
        TCComponentItemRevision[] previousRev = getPreviousRevision(m_selectedItemRev);
        
        if (previousRev != null)
        {
            for (int index = 0; index < previousRev.length; index++)
            {
                if (!isRevisionReleased(previousRev[index]))
                {
                    MessageBox.post(m_reg.getString("MultipleRevExist.ERROR"), m_reg.getString("Error.TITLE"),
                            MessageBox.ERROR);
                    return false;
                }
            }
        }
        
        boolean isLatestRev = isLatestRevision(m_selectedItemRev);
        if (!isLatestRev)
        {
            List<TCComponentItemRevision> aheadRevs = getAheadRevisions(m_selectedItemRev);
            if (aheadRevs.size() > 0)
            {
                MessageBox.post(m_reg.getString("NonLatestRevSelected.WRNG"), m_reg.getString("Warning.TITLE"),
                        MessageBox.WARNING);
            }
        }
        return true;
    }
    
    public TCComponentItemRevision[] getPreviousRevision(TCComponentItemRevision seletedRev)
    {
        int indx = 0;
        int iNoOfRevs = 0;
        
        Vector<TCComponentItemRevision> previousRevs = new Vector<TCComponentItemRevision>();
        try
        {
            TCComponent[] tcCompArray = getReferenceCompArray(seletedRev);
            List<TCComponent> tcCompList = Arrays.asList(tcCompArray); //TC 10.1 upgrade
            String sRevIds[][] = TCComponentType.getPropertiesSet(tcCompList, new String[] { "item_revision_id" });
            if (sRevIds != null)
            {
                iNoOfRevs = sRevIds.length;
                for (indx = 0; indx < iNoOfRevs; indx++)
                {
                    if (seletedRev.equals((TCComponentItemRevision) tcCompArray[indx]))
                    {
                        break;
                    }
                    previousRevs.add((TCComponentItemRevision) tcCompArray[indx]);
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return previousRevs.toArray(new TCComponentItemRevision[previousRevs.size()]);
    }
    
    public List<TCComponentItemRevision> getAheadRevisions(TCComponentItemRevision seletedRev)
    {
        int iNoOfRevs = 0;
        boolean flag = false;
        List<TCComponentItemRevision> revCompList = new ArrayList<TCComponentItemRevision>();
        try
        {
            TCComponent[] tcCompArray = getReferenceCompArray(seletedRev);
            List<TCComponent> tcCompList = Arrays.asList(tcCompArray); //TC 10.1 upgrade
           // String sRevIds[][] = TCComponentType.getPropertiesSet(tcCompArray, new String[] { "item_revision_id" });
            String sRevIds[][] = TCComponentType.getPropertiesSet(tcCompList, new String[] { "item_revision_id" });
            if (sRevIds != null)
            {
                iNoOfRevs = sRevIds.length;
                for (int indx = 0; indx < iNoOfRevs; indx++)
                {
                    if (seletedRev.equals((TCComponentItemRevision) tcCompArray[indx]))
                    {
                        flag = true;
                    }
                    if (flag)
                    {
                        revCompList.add((TCComponentItemRevision) tcCompArray[indx]);
                    }
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return revCompList;
    }
    
    private boolean isRevisionReleased(TCComponentItemRevision selectedRevision)
    {
        boolean isReleased = false;
        try
        {
            TCComponent[] relStatusList;
            relStatusList = (selectedRevision.getTCProperty("release_status_list").getReferenceValueArray());
            for (int index = 0; index < relStatusList.length; ++index)
            {
                if (relStatusList[index].getProperty("name").equalsIgnoreCase("Released"))
                {
                    isReleased = true;
                    break;
                }
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isReleased;
    }
    
    public boolean isLatestRevision(TCComponentItemRevision itemRevision)
    {
        boolean isLatest = false;
        try
        {
            TCComponentItem item = itemRevision.getItem();
            TCComponentItemRevision latestRevision = item.getLatestItemRevision();
            if (itemRevision.equals(latestRevision))
            {
                isLatest = true;
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return isLatest;
    }
    
    private TCComponent[] getReferenceCompArray(TCComponentItemRevision itemRevision)
    {
        TCComponent[] tcCompArray = null;
        try
        {
            TCComponentItem itemComp = itemRevision.getItem();
            TCProperty tcProperty = itemComp.getTCProperty("revision_list");
            tcCompArray = tcProperty.getReferenceValueArray();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return tcCompArray;
    }
}
