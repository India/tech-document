package com.nov.rac.utilities.wirelist.utils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.teamcenter.rac.util.Registry;

public class NOVXMLDataComparator
{
    private Registry m_registry = Registry.getRegistry(this);
    File m_xmlOld;
    File m_xmlNew;
    
    public NOVXMLDataComparator(File oldXMLDataset, File newXMLDataset)
    {
        // Fetch data from latest and prev revisions
        m_xmlOld = oldXMLDataset;
        m_xmlNew = newXMLDataset;
    }
    
    public HashMap<String, String[][]> fetchComparedData() throws ParserConfigurationException, SAXException,
            IOException
    {
        // HashMap to be returned contains added, revised and omitted wires data
        HashMap<String, String[][]> arrayMap = new HashMap<String, String[][]>();
        
        Vector<String[]> addedNodes = new Vector<String[]>();
        Vector<String[]> revisedNodes = new Vector<String[]>();
        Vector<String[]> omittedNodes = new Vector<String[]>();
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document oldXMLDoc = dBuilder.parse(m_xmlOld);
        Document newXMLDoc = dBuilder.parse(m_xmlNew);
        
        Node oldXMLRootNode = oldXMLDoc.getDocumentElement();
        Node newXMLRootNode = newXMLDoc.getDocumentElement();
        
        NodeList oldChildNodes = oldXMLRootNode.getChildNodes();
        NodeList newChildNodes = newXMLRootNode.getChildNodes();
        
        // MultiMap implemented as there are different revisions of a wire in a
        // single xml (using Wire as key)
        MultiMap oldXMLMultiMap = new MultiValueMap();
        
        // Used for creating a Multimap of child nodes from oldxml
        for (int index = 0; index < oldChildNodes.getLength(); index++)
        {
            if (oldChildNodes.item(index).getNodeType() == Node.ELEMENT_NODE)
            {
                Element tempOldNode = (Element) oldChildNodes.item(index);
                Node tempPropertyNode = tempOldNode.getElementsByTagName("WIRE").item(0);
                String multiMapKey = getPropertyNodeValue(tempPropertyNode);
                NOVWirelistNode multiMapValue = new NOVWirelistNode(oldChildNodes.item(index));
                oldXMLMultiMap.put(multiMapKey, multiMapValue);
            }
        }
        
        for (int i = 0; i < newChildNodes.getLength(); i++)
        {
            Element tempNewNode = null;
            
            if (newChildNodes.item(i).getNodeType() == Node.ELEMENT_NODE)
            {
                tempNewNode = (Element) newChildNodes.item(i);
            }
            else
            {
                continue;
            }
            
            Node newPropertyNodeWire = tempNewNode.getElementsByTagName("WIRE").item(0);
            Node newPropertyNodeRev = tempNewNode.getElementsByTagName("REV").item(0);
            
            String newWireValue = getPropertyNodeValue(newPropertyNodeWire);
            
            //Check if wire number is blank, skip that record
            if(newWireValue.isEmpty())
            {
            	continue;
            }
            
            String newRevValue = getPropertyNodeValue(newPropertyNodeRev);
            
            // Comparison logic starts here
            Collection<NOVWirelistNode> nodesWithSameWireKey = (Collection<NOVWirelistNode>) oldXMLMultiMap
                    .get(newWireValue);
            
            if (nodesWithSameWireKey != null)
            {
                String oldHighestRev = "00";
                Iterator<NOVWirelistNode> collectionIterator = nodesWithSameWireKey.iterator();
                NOVWirelistNode oldWirelistNodeWithHighestRev = null;
                while (collectionIterator.hasNext())
                {
                    NOVWirelistNode tempWirelistNode = collectionIterator.next();
                    tempWirelistNode.m_isOmitted = false;
                    
                    if ((Integer.parseInt(oldHighestRev)) < (Integer.parseInt(tempWirelistNode.m_revision)))
                    {
                        oldHighestRev = tempWirelistNode.m_revision;
                        oldWirelistNodeWithHighestRev = tempWirelistNode;
                    }
                }
                
                if (oldHighestRev.equalsIgnoreCase(newRevValue))
                {
                    // do nothing
                }
                else
                {
                    String[] allPropertyValues = putNodeValuesInStringArray(tempNewNode);
                    String[] allPropValuesWithRevisedFields = new String[allPropertyValues.length + 1];
                    
                    for (int index = 0; index < allPropertyValues.length; index++)
                    {
                        allPropValuesWithRevisedFields[index] = allPropertyValues[index];
                    }
                    
                    //Fetch revised property fields
                    String changedFields = getListOfChangedProperties(
                            (Element) oldWirelistNodeWithHighestRev.m_objNode, tempNewNode);
                                       
                    if(changedFields != null)
                    {
	                    allPropValuesWithRevisedFields[allPropertyValues.length] = changedFields;
	                    revisedNodes.add(allPropValuesWithRevisedFields);
                    }
                }
            }
            else
            {
                addedNodes.add(putNodeValuesInStringArray(tempNewNode));
            }
        }
        
        Collection<NOVWirelistNode> collectionOfValues = oldXMLMultiMap.values();
        Iterator collectionIterator = collectionOfValues.iterator();
        
        // Get all omitted nodes by checking IsOmitted boolean in
        // NOVWirelistNode
        while (collectionIterator.hasNext())
        {
            
            NOVWirelistNode tempNode = (NOVWirelistNode) collectionIterator.next();
            
            if (tempNode.m_isOmitted)
            {
                omittedNodes.add(putNodeValuesInStringArray((Element) tempNode.m_objNode));
            }
        }
        
        String[][] addedArray = addedNodes.toArray(new String[addedNodes.size()][]);
        String[][] revisedArray = revisedNodes.toArray(new String[revisedNodes.size()][]);
        String[][] omittedArray = omittedNodes.toArray(new String[omittedNodes.size()][]);
        
        // Collected in a HashMap and passed to Wirelist Revision Report
        // generation
        arrayMap.put("Added", addedArray);
        arrayMap.put("Revised", revisedArray);
        arrayMap.put("Omitted", omittedArray);
        
        return arrayMap;
    }
    
    public String getListOfChangedProperties(Element nodeOld, Element nodeNew)
    {
    	//Fetching list of property names to be checked for modifications
        String[] propertiesArray = getPropertyNamesList();

        String changedProps = null;
        
       	// OLD WIRE: LOC_F + COMP_F + TERM_F + LOC_T + COMP_T + TERM_T + SIZE
       	// NEW WIRE: LOC_T + COMP_T + TERM_T + LOC_F + COMP_F + TERM_F + SIZE
       	// If (OLD WIRE == NEW WIRE) as per above concatenation, then it is just swapped but NOT revised
        String oldLCTSPropertyValue = null;
        String newLCTSPropertyValue = null;
        boolean isLCTSwapped = false;
        
		oldLCTSPropertyValue = getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[0]).item(0)) +
							getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[1]).item(0)) +
							getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[2]).item(0)) +
							getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[3]).item(0)) +
							getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[4]).item(0)) +
							getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[5]).item(0)) +
							getPropertyNodeValue(nodeOld.getElementsByTagName(propertiesArray[6]).item(0));
	
		newLCTSPropertyValue = getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[3]).item(0)) +
							getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[4]).item(0)) +
							getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[5]).item(0)) +
							getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[0]).item(0)) +
							getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[1]).item(0)) +
							getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[2]).item(0)) +
							getPropertyNodeValue(nodeNew.getElementsByTagName(propertiesArray[6]).item(0));

        if (oldLCTSPropertyValue != null && newLCTSPropertyValue !=null && oldLCTSPropertyValue.equalsIgnoreCase(newLCTSPropertyValue))
        {
        	isLCTSwapped = true;
        }

        if(!isLCTSwapped) {
	        for (int i = 0; i < propertiesArray.length; i++)
	        {
	            String oldPropertyValue = null;
	            String newPropertyValue = null;
	            
	            Node oldPropertyNode = nodeOld.getElementsByTagName(propertiesArray[i]).item(0);
	            Node newPropertyNode = nodeNew.getElementsByTagName(propertiesArray[i]).item(0);
	            
	            oldPropertyValue = getPropertyNodeValue(oldPropertyNode);
	            newPropertyValue = getPropertyNodeValue(newPropertyNode);
	            
	            // Concatenate changed property names.
	            if (!oldPropertyValue.equalsIgnoreCase(newPropertyValue))
	            {
	            	if (changedProps == null)
	                {
	                    changedProps = propertiesArray[i];
	                }
	                else
	                {
	                    changedProps = changedProps + ", " + propertiesArray[i];
	                }
	            }
	        }
        }
        return changedProps;
    }
    
    private String getPropertyNodeValue(Node propertyNode)
    {
        String propertyValue = null;
        if (propertyNode.getFirstChild() != null)
        {
            propertyValue = propertyNode.getFirstChild().getNodeValue();
        }
        else
        {
            propertyValue = "";
        }
        
        return propertyValue;
    }
    
    public String[] putNodeValuesInStringArray(Element elementNode)
    {
        String[] propertyNames = getPropertyNamesArray();
        String[] propValues = new String[propertyNames.length];
        
        for (int i = 0; i < propertyNames.length; i++)
        {
            Node tempPropertyNode = null;
            tempPropertyNode = elementNode.getElementsByTagName(propertyNames[i]).item(0);
            propValues[i] = getPropertyNodeValue(tempPropertyNode);
        }
        return propValues;
    }
    
    public String[] getPropertyNamesArray()
    {
        String[] propertyNamesList = m_registry.getString("propertyNames.LIST").split(",");
        return propertyNamesList;
    }
    
    //Get list of property names to be checked for modifications
    public String[] getPropertyNamesList()
    {
        String[] propertiesToBeChecked = m_registry.getString("revisedPropertyNames.LIST").split(",");
        return propertiesToBeChecked;
    } 
}
