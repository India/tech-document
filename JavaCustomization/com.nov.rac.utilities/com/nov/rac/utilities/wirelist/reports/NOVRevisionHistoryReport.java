package com.nov.rac.utilities.wirelist.reports;

import java.util.Vector;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.P;

import com.nov.rac.utilities.wirelist.utils.NOVTableCreatorHelper;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.util.Registry;

public class NOVRevisionHistoryReport
{
    private WordprocessingMLPackage m_wordMLPackage;
    private TCComponentItemRevision[] m_previousItemRev = null;
    private Vector<String> m_revisionHistoryData = new Vector<String>();
    private Vector<String> m_changeDescriptionData = new Vector<String>();
    private Registry m_registry = Registry.getRegistry(this);
    
    public NOVRevisionHistoryReport(WordprocessingMLPackage wordMLPackage, TCComponentItemRevision[] previousRevisions)
    {
        m_wordMLPackage = wordMLPackage;
        m_previousItemRev = previousRevisions;
    }
    
    public void createRevisionHistoryReport() throws Exception
    {
        String[] revHistoryColumns = m_registry.getString("revisionReportHeaders.LIST").split(",");
        fetchRevisionHistory();
        
        // Revision History
        NOVTableCreatorHelper revHistoryTable = new NOVTableCreatorHelper(m_wordMLPackage, revHistoryColumns,
                m_revisionHistoryData);
        // Adding title
        P revHistoryTitle = m_wordMLPackage.getMainDocumentPart().createStyledParagraphOfText(
                m_registry.getString("titleType.Str"), m_registry.getString("revisionHistory.TITLE"));
        NOVWirelistWordDocument.insertObjectBeforeText(m_wordMLPackage, m_registry.getString("wiringNotesReference.Str"), revHistoryTitle);
        // Adding table
        revHistoryTable.createAndInsertTableForReport();
        // Leave a line in between two tables
        P leaveLine = m_wordMLPackage.getMainDocumentPart().createParagraphOfText(" ");
        NOVWirelistWordDocument.insertObjectBeforeText(m_wordMLPackage,  m_registry.getString("wiringNotesReference.Str"), leaveLine);
        
        m_wordMLPackage.getMainDocumentPart().addParagraphOfText(" ");
        
        // Change Description
        String[] changeDescColumns = m_registry.getString("changeDescriptionHeaders.LIST").split(",");
        
        // CR_Kiran: Define different NOVTableCreatorHelper variable for
        // revision history and change description data
        NOVTableCreatorHelper changeDescTable = new NOVTableCreatorHelper(m_wordMLPackage, changeDescColumns,
                m_changeDescriptionData);
        // Adding title
        P changeDescTitle = m_wordMLPackage.getMainDocumentPart().createStyledParagraphOfText(
                m_registry.getString("titleType.Str"), m_registry.getString("changeDescription.TITLE"));
        NOVWirelistWordDocument.insertObjectBeforeText(m_wordMLPackage, m_registry.getString("wiringNotesReference.Str"), changeDescTitle);
        // Adding table
        changeDescTable.createAndInsertTableForReport();
        
    }
    
    private void fetchRevisionHistory()
    {
        String[] propNames = m_registry.getString("ENFormPropertyNames.LIST").split(",");
        String itemRevId = null;
        
        try
        {
            for (int index = 0; index < m_previousItemRev.length; index++)
            {
                TCComponentForm ENForm = (TCComponentForm) m_previousItemRev[index].getRelatedComponent("RelatedECN");
				if(ENForm != null)
				{
					TCProperty[] formProps = ENForm.getFormTCProperties(propNames);
					itemRevId = m_previousItemRev[index].getProperty("current_revision_id");
					
					// Populating Revision History vector
					m_revisionHistoryData.add(itemRevId);
					
					for (int i = 0; i < formProps.length - 1; i++)
					{
						m_revisionHistoryData.add(formProps[i].toString());
					}
					
					// Populating Change Description vector
					m_changeDescriptionData.add(itemRevId);
					m_changeDescriptionData.add(formProps[formProps.length - 1].toString());
				}
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
}
