package com.nov.rac.utilities.wirelist.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.docx4j.TextUtils;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.wml.Body;
import org.docx4j.wml.Br;
import org.docx4j.wml.FldChar;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.STBrType;
import org.docx4j.wml.STFldCharType;
import org.docx4j.wml.Text;

import com.nov.rac.utilities.wirelist.utils.NOVXMLDataComparator;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;

public class NOVWirelistWordDocument
{
    private Registry m_registry = Registry.getRegistry(this);
    private String m_templateFileName;
    private String m_outputFileName;
    private String m_selXMLFileName;

    private static ObjectFactory m_factory = Context.getWmlObjectFactory();
    private File m_currentRevXMLFile;
    private File m_prevRevXMLFile;
    
    /************** creating word doc with the template file *************************/
    static WordprocessingMLPackage wordMLPackage = null;
    
    public NOVWirelistWordDocument(String templateName, String outputFile, String selXMLFileName) throws FileNotFoundException,
            Docx4JException
    {
        m_templateFileName = templateName;
        m_outputFileName = outputFile;
        m_selXMLFileName = selXMLFileName;
        wordMLPackage = this.getTemplate(m_templateFileName);
    }
    
    public void createDocument(String[][] xmlValues, String[][] xmlNumRepValues, TCComponentItemRevision selectedDocRev, TCComponentItemRevision previousRevisions[],
            boolean isXMLAttachedToPrevRev) throws Exception
    {
        
        // Cover Page
        NOVCoverPage coverPage = new NOVCoverPage(wordMLPackage);
        coverPage.updateCoverPage(selectedDocRev, m_outputFileName, m_selXMLFileName);
        // Append further reports i.e. take output file as template
        wordMLPackage = this.getTemplate(m_outputFileName);
        
        // Page break
        Br objBr = new Br();
        objBr.setType(STBrType.PAGE);
        wordMLPackage.getMainDocumentPart().addObject(objBr);
        
        // Revision History Report and Change Description information
        NOVRevisionHistoryReport revisionReport = new NOVRevisionHistoryReport(wordMLPackage, previousRevisions);
        revisionReport.createRevisionHistoryReport();
        wordMLPackage.getMainDocumentPart().addParagraphOfText("");
        
        // Page Break
        NOVWirelistWordDocument.insertObjectBeforeText(wordMLPackage, m_registry.getString("wiringNotesReference.Str"),
                objBr);
        // Table of contents
        MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
        // Add title
        P tableOfContentsTitle = wordMLPackage.getMainDocumentPart().createStyledParagraphOfText(
                m_registry.getString("titleType.Str"), m_registry.getString("tableOfContents.TITLE"));
        NOVWirelistWordDocument.insertObjectBeforeText(wordMLPackage, m_registry.getString("wiringNotesReference.Str"),
                tableOfContentsTitle);
        // Add TOC object
        addTableOfContent(documentPart);
        // Page break
        NOVWirelistWordDocument.insertObjectBeforeText(wordMLPackage, m_registry.getString("wiringNotesReference.Str"),
                objBr);
        
        // Numerical Report
        NOVNumericalReport numericalReport = new NOVNumericalReport(xmlNumRepValues);
        numericalReport.createNumericalReport(wordMLPackage);
        wordMLPackage.getMainDocumentPart().addObject(objBr);
        
        // Component Cross Reference Report
        NOVCompCrossRefReport compCrossRepo = new NOVCompCrossRefReport(xmlValues);
        compCrossRepo.createCompCrossRefReport(wordMLPackage);
        
        // Create Wirelist Revision report only if previous revision is present
        if (isXMLAttachedToPrevRev)
        {
            NOVXMLDataComparator compareXML = new NOVXMLDataComparator(m_prevRevXMLFile, m_currentRevXMLFile);
            HashMap<String, String[][]> arraysVector = compareXML.fetchComparedData();
            
            wordMLPackage.getMainDocumentPart().addObject(objBr);
            NOVWirelistRevisionReport wirelistRevReport = new NOVWirelistRevisionReport();
            wirelistRevReport.createWirelisRevReport(wordMLPackage, arraysVector);
        }
        
        /************** saving doc file *************************/
        writeDocxToStream(wordMLPackage, m_outputFileName);
    }
    
    // Insert before General Wiring Notes
    public static void insertObjectBeforeText(WordprocessingMLPackage pkg, String beforeText, Object objToAdd)
            throws Exception
    {
        Body b = pkg.getMainDocumentPart().getJaxbElement().getBody();
        int addPoint = -1;
        int count = 0;
        for (Object o : b.getEGBlockLevelElts())
        {
            if (o instanceof P && getElementText(o).startsWith(beforeText))
            {
                addPoint = count;
                break;
            }
            count++;
        }
        if (addPoint != -1)
        {
            b.getEGBlockLevelElts().add(addPoint, objToAdd);
        }
        else
        {
            // didn't find paragraph to insert after...
        }
    }
    
    static String getElementText(Object jaxbElem) throws Exception
    {
        StringWriter sw = new StringWriter();
        TextUtils.extractText(jaxbElem, sw);
        return sw.toString();
    }
    
    private void addTableOfContent(MainDocumentPart documentPart) throws Exception
    {
        P paragraph = m_factory.createP();
        
        addFieldBegin(paragraph);
        addTableOfContentField(paragraph);
        addFieldEnd(paragraph);
        
        insertObjectBeforeText(wordMLPackage, m_registry.getString("wiringNotesReference.Str"), paragraph);
    }
    
    private static void addTableOfContentField(P paragraph)
    {
        R run = m_factory.createR();
        Text txt = new Text();
        txt.setSpace("preserve");
        txt.setValue("TOC \\o \"1-3\" \\h \\z \\u");
        run.getContent().add(m_factory.createRInstrText(txt));
        paragraph.getContent().add(run);
    }
    
    private static void addFieldBegin(P paragraph)
    {
        R run = m_factory.createR();
        FldChar fldchar = m_factory.createFldChar();
        fldchar.setFldCharType(STFldCharType.BEGIN);
        fldchar.setDirty(true);
        run.getContent().add(getWrappedFldChar(fldchar));
        paragraph.getContent().add(run);
    }
    
    private static void addFieldEnd(P paragraph)
    {
        R run = m_factory.createR();
        FldChar fldcharend = m_factory.createFldChar();
        fldcharend.setFldCharType(STFldCharType.END);
        run.getContent().add(getWrappedFldChar(fldcharend));
        paragraph.getContent().add(run);
    }
    
    public static JAXBElement getWrappedFldChar(FldChar fldchar)
    {
        return new JAXBElement(new QName(Namespaces.NS_WORD12, "fldChar"), FldChar.class, fldchar);
    }
    
    private WordprocessingMLPackage getTemplate(String name) throws Docx4JException, FileNotFoundException
    {
        WordprocessingMLPackage template = WordprocessingMLPackage.load(new FileInputStream(new File(name)));
        return template;
    }
    
    private void writeDocxToStream(WordprocessingMLPackage template, String target) throws IOException, Docx4JException
    {
        File f = new File(target);
        template.save(f);
    }
    
    public void setXMLFilePaths(File currentXMLFile, File prevRevXMLFile)
    {
        m_currentRevXMLFile = currentXMLFile;
        m_prevRevXMLFile = prevRevXMLFile;
    }
}
