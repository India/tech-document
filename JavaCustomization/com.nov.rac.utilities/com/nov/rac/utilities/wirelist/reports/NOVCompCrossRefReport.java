package com.nov.rac.utilities.wirelist.reports;

import java.io.IOException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.xml.bind.JAXBException;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.nov.rac.utilities.wirelist.utils.NOVDataManipulator;
import com.nov.rac.utilities.wirelist.utils.NOVTableCreatorHelper;
import com.teamcenter.rac.util.Registry;

public class NOVCompCrossRefReport
{
    private Registry m_registry = Registry.getRegistry(this);
    String[][] m_xmlData = null;
    String[] m_columnNamesJTable;
    
    public NOVCompCrossRefReport(String[][] xmlVal)
    {
        m_xmlData = xmlVal;
    }
    
    public void createCompCrossRefReport(WordprocessingMLPackage wordMLPackage) throws IOException, Docx4JException
    {
        // Vector passed to NOVTableCreatorHelper to generate report in word
        // document
        Vector<String> newDataVector = null;
        
        // Getting the column names for Word Doc Report
        String[] columnHeaders = m_registry.getString("compCrossRefReportHeaders.LIST").split(",");
        
        // Getting the column names for JTable
        m_columnNamesJTable = m_registry.getString("reportHeaders.LIST").split(",");
        
        // Column names by which we need to sort
        String[] sortKeys = m_registry.getString("compCrossRefSoryKeys.LIST ").split(",");
        // Creating a JTable for sorting data
        JTable compCrossJTable = createJTable();
        
        // Pass xmlVal 2D array to JTable method for sorting and get a sorted
        // data vector
        NOVDataManipulator manipulator = new NOVDataManipulator();
        newDataVector = manipulator.getModifiedVector(compCrossJTable, m_columnNamesJTable, sortKeys);
        
        NOVTableCreatorHelper tableHelper = new NOVTableCreatorHelper(wordMLPackage, columnHeaders, newDataVector);
        
        // Report Title
        wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(m_registry.getString("titleType.Str"),
                m_registry.getString("compCrossRefReport.TITLE"));
        // Creating table for report
        try
        {
            tableHelper.createTableForReport();
        }
        catch (JAXBException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        wordMLPackage.getMainDocumentPart().addParagraphOfText(" ");
    }
    
    private JTable createJTable()
    {
        DefaultTableModel model = new DefaultTableModel(m_xmlData, m_columnNamesJTable);
        JTable jtab = new JTable(model);
        model.setRowCount(2 * (m_xmlData.length));
        
        addFlagColumn(jtab);
        sequencingColumns(jtab);
        
        // New Table used for storing swapped entries
        JTable jtableNew = new JTable(jtab.getModel());
        sequencingColumns(jtableNew);
        
        // Swapping FROM and TO columns
        swapColumns(jtableNew, jtableNew.getColumn("Location From").getModelIndex(), jtableNew.getColumn("Location To")
                .getModelIndex());
        swapColumns(jtableNew, jtableNew.getColumn("Component From").getModelIndex(),
                jtableNew.getColumn("Component To").getModelIndex());
        swapColumns(jtableNew, jtableNew.getColumn("Terminal To").getModelIndex(), jtableNew.getColumn("Terminal From")
                .getModelIndex());
        
        // Inserting in JTable cell by cell
        int rowSize = jtableNew.getRowCount();
        int colCount = jtableNew.getColumnCount();
        
        for (int rowi = 0; rowi < rowSize / 2; rowi++)
        {
            for (int coli = 0; coli < colCount - 1; coli++)
            {
                String value = (String) jtableNew.getValueAt(rowi, coli);
                
                jtab.setValueAt(value, (rowSize / 2) + rowi, coli);
            }
        }
        return jtab;
    }
    
    // Swapping any two columns
    private static void swapColumns(JTable jtableObj, int modelIndex1, int modelIndex2)
    {
        int actualIndex = jtableObj.convertColumnIndexToView(modelIndex1);
        int targetIndex = jtableObj.convertColumnIndexToView(modelIndex2);
        
        jtableObj.moveColumn(actualIndex, targetIndex);
        if (actualIndex < targetIndex)
        {
            jtableObj.moveColumn(targetIndex - 1, actualIndex);
        }
        else
        {
            jtableObj.moveColumn(targetIndex + 1, actualIndex);
        }
    }
    
    // Rearranging the columns to proper sequence
    private void sequencingColumns(JTable tblRef)
    {
        tblRef.moveColumn(tblRef.convertColumnIndexToView(tblRef.getColumn("Wire").getModelIndex()),
                tblRef.convertColumnIndexToView(tblRef.getColumn("Terminal To").getModelIndex()));
        tblRef.moveColumn(tblRef.convertColumnIndexToView(tblRef.getColumn("Rev").getModelIndex()),
                tblRef.convertColumnIndexToView(tblRef.getColumn("Wire").getModelIndex()));
    }
    
    // Adding a new FLAG column to JTable
    private void addFlagColumn(JTable jtbl)
    {
        Vector<String> star = new Vector<String>();
        
        for (int index = 0; index < jtbl.getRowCount() / 2; index++)
        {
            star.add("*");
        }
        for (int index = jtbl.getRowCount() / 2; index < jtbl.getRowCount(); index++)
        {
            star.add(" ");
        }
        
        TableColumn cols = new TableColumn(jtbl.getModel().getColumnCount());
        cols.setHeaderValue("FLAG");
        jtbl.addColumn(cols);
        ((DefaultTableModel) jtbl.getModel()).addColumn("FLAG", star);
    }
}
