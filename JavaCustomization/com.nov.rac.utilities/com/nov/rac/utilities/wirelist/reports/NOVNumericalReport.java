package com.nov.rac.utilities.wirelist.reports;

import java.io.IOException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.nov.rac.utilities.wirelist.utils.NOVDataManipulator;
import com.nov.rac.utilities.wirelist.utils.NOVTableCreatorHelper;
import com.teamcenter.rac.util.Registry;

public class NOVNumericalReport
{
    private Registry m_registry = Registry.getRegistry(this);
    String[][] m_xmlData = null;
    String[] m_columnHeaders = null;
    
    public NOVNumericalReport(String[][] xmlVal)
    {
        m_xmlData = xmlVal;
        // Getting the column names
        m_columnHeaders = m_registry.getString("reportHeaders.LIST").split(",");
    }
    
    public void createNumericalReport(WordprocessingMLPackage wordMLPackage) throws IOException, Docx4JException
    {
        // Vector passed to NOVTableCreatorHelper to generate report in word
        // document
        Vector<String> newDataVector = null;
        
        // Column names by which we need to sort
        String[] sortKeys = m_registry.getString("sortKeys.LIST").split(",");
        // Creating a JTable for sorting data
        JTable numericalJTable = createJTable();
        
        // Pass xmlVal 2D array to JTable method for sorting and get a sorted
        // data vector
        NOVDataManipulator manipulator = new NOVDataManipulator();
        newDataVector = manipulator.getModifiedVector(numericalJTable, m_columnHeaders, sortKeys);
        
        NOVTableCreatorHelper tableHelper = new NOVTableCreatorHelper(wordMLPackage, m_columnHeaders, newDataVector);
        // Report Title
        wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(m_registry.getString("titleType.Str"),
                m_registry.getString("numericalReport.TITLE"));
        // Creating table for report
        try
        {
            tableHelper.createTableForReport();
        }
        catch (JAXBException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        wordMLPackage.getMainDocumentPart().addParagraphOfText(" ");
    }
    
    private JTable createJTable()
    {
        DefaultTableModel model = new DefaultTableModel(m_xmlData, m_columnHeaders);
        JTable jtab = new JTable(model);
        return jtab;
    }
}
