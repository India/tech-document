package com.nov.rac.utilities.wirelist.reports;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.ContentAccessor;
import org.docx4j.wml.Text;

import com.nov.rac.utilities.wirelist.utils.NOVDocumentHeaderCreator;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class NOVCoverPage
{
    static WordprocessingMLPackage m_wordMLPackage;
    
    public NOVCoverPage(WordprocessingMLPackage wordMLPackage)
    {
        m_wordMLPackage = wordMLPackage; // CR_Kiran: Use constructor to assign
                                         // wordMLPackage to class member
                                         // variable so that it need not be
                                         // passed as parameter in
                                         // each function in this class.
    }
    
    public void updateCoverPage(TCComponentItemRevision selectedDocRev, String outputFileName, String selXMLFileName) throws IOException, Docx4JException, TCException
    {
        HashMap<String, String> docProps = (HashMap<String, String>) selectedDocRev.getProperties();
        String documentName = docProps.get("object_name");
        String documentNumber = docProps.get("item_id");
        String documentRev = docProps.get("item_revision_id");
      
        //Set the document header
        NOVDocumentHeaderCreator docHeader = new NOVDocumentHeaderCreator(m_wordMLPackage, documentNumber, documentRev);
        docHeader.createHeaderForDoc();

        // CR_Kiran: Pass data from the target document
        replacePlaceholder(selXMLFileName, "ReferenceName");
        replacePlaceholder(documentName, "DocumentName");
        replacePlaceholder(documentNumber, "DocumentID");
        replacePlaceholder(documentRev, "DocRev");
        
        writeDocxToStream(outputFileName);
    }
    
    private void writeDocxToStream(String target) throws IOException, Docx4JException
    {
        File file = new File(target);
        m_wordMLPackage.save(file);
    }
    
    private static void replacePlaceholder(String name, String placeholder)
    {
        List<Object> texts = getAllElementFromObject(m_wordMLPackage.getMainDocumentPart(), Text.class);
        
        for (Object text : texts)
        {
            Text textElement = (Text) text;
            if (textElement.getValue().equals(placeholder))
            {
                textElement.setValue(name);
            }
        }
    }
    
    private static List<Object> getAllElementFromObject(Object obj, Class<?> toSearch)
    {
        List<Object> result = new ArrayList<Object>();
        
        if (obj instanceof JAXBElement)
        {
            obj = ((JAXBElement<?>) obj).getValue();
        }
        if (obj.getClass().equals(toSearch))
        {
            result.add(obj);
        }
        
        else if (obj instanceof ContentAccessor)
        {
            List<?> children = ((ContentAccessor) obj).getContent();
            for (Object child : children)
            {
                result.addAll(getAllElementFromObject(child, toSearch));
            }
        }
        return result;
    }
}
