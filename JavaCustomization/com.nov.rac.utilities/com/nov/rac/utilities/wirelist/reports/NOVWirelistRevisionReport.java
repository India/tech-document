package com.nov.rac.utilities.wirelist.reports;

import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import com.nov.rac.utilities.wirelist.utils.NOVDataManipulator;
import com.nov.rac.utilities.wirelist.utils.NOVTableCreatorHelper;
import com.teamcenter.rac.util.Registry;

public class NOVWirelistRevisionReport
{
    private Registry m_registry = Registry.getRegistry(this);
    
    public void createWirelisRevReport(WordprocessingMLPackage wordMLPackage, HashMap<String, String[][]> arraysMap)
            throws IOException, Docx4JException
    {
        Vector<String> addedWiresVector = null;
        Vector<String> revisedWiresVector = null;
        Vector<String> omittedWiresVector = null;
        
        String[][] addedArray = arraysMap.get("Added");
        String[][] revisedArray = arraysMap.get("Revised");
        String[][] omittedArray = arraysMap.get("Omitted");
        
        String[] sortKeys = m_registry.getString("sortKeys.LIST").split(",");
        String[] columnHeaders = m_registry.getString("reportHeaders.LIST").split(",");
        String[] revisedHeaders = m_registry.getString("revisedReportHeaders.LIST").split(",");
        
        JTable addedJTable = createJTable(addedArray, columnHeaders);
        JTable revisedJTable = createJTable(revisedArray, revisedHeaders);
        JTable omittedJTable = createJTable(omittedArray, columnHeaders);
        
        // Report Title
        wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(m_registry.getString("titleType.Str"),
                m_registry.getString("wirelistRevReport.TITLE"));
        
        // Statistics
        wordMLPackage.getMainDocumentPart().addParagraphOfText(
                m_registry.getString("wiresAdded.Str") + addedArray.length);
        wordMLPackage.getMainDocumentPart().addParagraphOfText(
                m_registry.getString("wiresRevised.Str") + revisedArray.length);
        wordMLPackage.getMainDocumentPart().addParagraphOfText(
                m_registry.getString("wiresOmitted.Str") + omittedArray.length);
        wordMLPackage.getMainDocumentPart().addParagraphOfText(" ");
        
        // For Added
        NOVDataManipulator manipulatorForAdded = new NOVDataManipulator();
        addedWiresVector = manipulatorForAdded.getModifiedVector(addedJTable, columnHeaders, sortKeys);
        NOVTableCreatorHelper tableHelperForAdded = new NOVTableCreatorHelper(wordMLPackage, columnHeaders,
                addedWiresVector);
        wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(m_registry.getString("subtitleType.Str"),
                m_registry.getString("wiresAdded.TITLE"));
        try
        {
            tableHelperForAdded.createTableForReport();
        }
        catch (JAXBException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        wordMLPackage.getMainDocumentPart().addParagraphOfText(" ");
        
        // For Revised
        NOVDataManipulator manipulatorForRevised = new NOVDataManipulator();
        revisedWiresVector = manipulatorForRevised.getModifiedVector(revisedJTable, revisedHeaders, sortKeys);
        NOVTableCreatorHelper tableHelperForRevised = new NOVTableCreatorHelper(wordMLPackage, revisedHeaders,
                revisedWiresVector);
        wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(m_registry.getString("subtitleType.Str"),
                m_registry.getString("wiresRevised.TITLE"));
        try
        {
            tableHelperForRevised.createTableForReport();
        }
        catch (JAXBException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        wordMLPackage.getMainDocumentPart().addParagraphOfText(" ");
        
        // For Omitted
        NOVDataManipulator manipulatorForOmitted = new NOVDataManipulator();
        omittedWiresVector = manipulatorForOmitted.getModifiedVector(omittedJTable, columnHeaders, sortKeys);
        NOVTableCreatorHelper tableHelperForOmitted = new NOVTableCreatorHelper(wordMLPackage, columnHeaders,
                omittedWiresVector);
        wordMLPackage.getMainDocumentPart().addStyledParagraphOfText(m_registry.getString("subtitleType.Str"),
                m_registry.getString("wiresOmitted.TITLE"));
        
        try
        {
            tableHelperForOmitted.createTableForReport();
        }
        catch (JAXBException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private JTable createJTable(String[][] data, String[] columnHeaders)
    {
        DefaultTableModel model = new DefaultTableModel(data, columnHeaders);
        JTable jtab = new JTable(model);
        return jtab;
    }
}
