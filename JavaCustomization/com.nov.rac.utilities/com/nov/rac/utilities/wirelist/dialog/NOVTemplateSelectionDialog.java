package com.nov.rac.utilities.wirelist.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.nov.rac.utilities.wirelist.action.NOVWirelistReportAction;
import com.nov.rac.utilities.wirelist.utils.NOVWirelistReportValidator;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentQuery;
import com.teamcenter.rac.kernel.TCComponentQueryType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.dialog.AbstractSWTDialog;

public class NOVTemplateSelectionDialog extends AbstractSWTDialog implements SelectionListener
{
    private TCComponentItem m_selectedDocTemplate;
    private Registry m_reg = Registry.getRegistry(this);
    private Combo m_comboBox;
    private Button m_cancelButton;
    private Button m_okButton;
    private TCSession m_session;
    private List<TCComponentItem> m_groupTemplate = new ArrayList<TCComponentItem>();
    private NOVWirelistReportValidator m_validator;
    
    public NOVTemplateSelectionDialog(Shell shell, NOVWirelistReportValidator validator)
    {
        super(shell);
        m_validator = validator;
        m_session = (TCSession) AIFUtility.getDefaultSession();
        loadData();
    }
    
    @Override
    protected Control createDialogArea(Composite parent)
    {
        createUI(parent);
        return parent;
    }
    
    @Override
    protected Control createButtonBar(Composite parent)
    {
        return null;
    }
    
    private void createUI(Composite parent)
    {
        parent.setLayout(new GridLayout(1, true));
        parent.getShell().setText(m_reg.getString("SelectTemplate.TITLE"));
        parent.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(4, false));
        composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        
        Label label = new Label(composite, SWT.NONE);
        label.setText(m_reg.getString("SelTemplate.MSG"));
        label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        
        m_comboBox = new Combo(composite, SWT.DROP_DOWN | SWT.READ_ONLY | SWT.FLAT | SWT.BORDER);
        m_comboBox.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 3, 1));
        
        try {
	        for (int in = 0; in < m_groupTemplate.size(); in++)
	        {
				m_comboBox.add(((TCComponentItem)m_groupTemplate.get(in)).getTCProperty("object_name").toString());
	        }
		} catch (TCException e) {
			e.printStackTrace();
		}
        
        Composite buttonComposite = new Composite(parent, SWT.NONE);
        buttonComposite.setLayout(new FillLayout());
        buttonComposite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        
        m_okButton = new Button(buttonComposite, SWT.PUSH);
        m_okButton.setText(m_reg.getString("Ok.LBL"));
        m_okButton.addSelectionListener(this);
        
        m_cancelButton = new Button(buttonComposite, SWT.PUSH);
        m_cancelButton.setText(m_reg.getString("Cancel.LBL"));
        m_cancelButton.addSelectionListener(this);
        
    }
    
    private void loadData()
    {
        final List<TCComponentFolder> groupFolder = new ArrayList<TCComponentFolder>();
        final List<TCComponentItem> defaultTemplate = new ArrayList<TCComponentItem>();
        // Need to load template file list here
        AIFComponentContext[] qryResult;
        TCComponentQuery qry = null;
        try
        {
            TCComponentQueryType tc = (TCComponentQueryType) m_session.getTypeComponent(m_reg
                    .getString("queryType.NAME"));
            TCComponent[] queries = new TCComponent[1];
            queries[0] = tc.find(m_reg.getString("generalQuery.NAME"));
            if (queries[0] == null)
            {
                MessageBox.post(m_reg.getString("QryNotFound.ERROR"), m_reg.getString("Error.TITLE"), MessageBox.ERROR);
                return;
            }
            qry = (TCComponentQuery) queries[0];
            
            String[] sCriteriaName = new String[3];
            String[] sCriteriaVals = new String[3];
            sCriteriaName[0] = m_reg.getString("QueryCriteriaName1.NAME");
            sCriteriaName[1] = m_reg.getString("QueryCriteriaName2.NAME");
            sCriteriaName[2] = m_reg.getString("QueryCriteriaName3.NAME");
            
            sCriteriaVals[0] = m_reg.getString("QueryCriteriaVal1.NAME");
            sCriteriaVals[1] = m_reg.getString("QueryCriteriaVal2.NAME");
            sCriteriaVals[2] = m_reg.getString("QueryCriteriaVal3.NAME");
            
            String groupName = getLoggedInGroup();
            if (qry != null)
            {
                qryResult = qry.getExecuteResults(sCriteriaName, sCriteriaVals);
                TCComponentFolder parentFolder = (TCComponentFolder) qryResult[0].getComponent();
                AIFComponentContext[] childrenComp = parentFolder.getChildren();
                for (int inx = 0; inx < childrenComp.length; inx++)
                {
                    if (childrenComp[inx].getComponent() instanceof TCComponentFolder)
                    {
                        groupFolder.add((TCComponentFolder) childrenComp[inx].getComponent());
                    }
                    else if (childrenComp[inx].getComponent() instanceof TCComponentItem)
                    {
                        defaultTemplate.add((TCComponentItem) childrenComp[inx].getComponent());
                    }
                }
            }
            boolean flag = false;
            TCComponentFolder folderComp = null;
            for (int inx = 0; inx < groupFolder.size(); inx++)
            {
                folderComp = (TCComponentFolder) groupFolder.toArray()[inx];
                String folderName = folderComp.getTCProperty("object_name").toString();
                if (groupName.equalsIgnoreCase(folderName))
                {
                    flag = true;
                    break;
                }
            }
            if (flag)
            {
                AIFComponentContext[] templateComp = folderComp.getChildren();
                for (int inx = 0; inx < templateComp.length; inx++)
                {
                    if (templateComp[inx].getComponent() instanceof TCComponentItem)
                    {
                        m_groupTemplate.add((TCComponentItem) templateComp[inx].getComponent());
                    }
                }
            }
            else
            {
                m_groupTemplate.addAll(defaultTemplate);
            }
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private String getLoggedInGroup()
    {
        String currentGroup = null;
        try
        {
            currentGroup = m_session.getCurrentGroup().getGroupName();
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
        return currentGroup;
    }
    
    public TCComponentItem getSelectedTemplate()
    {
        return m_selectedDocTemplate;
    }
    
    @Override
    public void widgetSelected(SelectionEvent selectionevent)
    {
        if (selectionevent.getSource().equals(m_okButton))
        {
            if (m_comboBox.getSelectionIndex() != -1)
            {
                String selectedTemplate = m_comboBox.getItem(m_comboBox.getSelectionIndex());
                TCComponentItem templateComp = null;
                try
                {
                    for (int inx = 0; inx < m_groupTemplate.size(); inx++)
                    {
                    	templateComp = (TCComponentItem)m_groupTemplate.get(inx);
                        if (selectedTemplate.equalsIgnoreCase(templateComp.getTCProperty("object_name").toString()))
                        {
                            m_selectedDocTemplate = templateComp;
                            break;
                        }
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
                getShell().dispose();
                NOVWirelistReportAction action = new NOVWirelistReportAction(m_session, m_validator);
                boolean flag = action.doOperation(this.getShell(), templateComp);
                if (flag)
                {
                    MessageBox.post(m_reg.getString("SuccessGenWordDoc.MSG"), m_reg.getString("Info.TITLE"),
                            MessageBox.INFORMATION);
                }
            }
            else
            {
                MessageBox.post(m_reg.getString("TemplateSel.ERROR"), m_reg.getString("Error.TITLE"), MessageBox.ERROR);
            }
        }
        if (selectionevent.getSource().equals(m_cancelButton))
        {
            getShell().dispose();
            return;
        }
    }
    
    @Override
    public void widgetDefaultSelected(SelectionEvent selectionevent)
    {
        
    }
}
