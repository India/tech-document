package com.nov.rac.utilities.wirelist.csvreports;

import java.io.File;

import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinition;
import com.teamcenter.rac.kernel.TCComponentDatasetDefinitionType;
import com.teamcenter.rac.kernel.TCComponentDatasetType;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.Registry;

public class NOVCSVCreator
{
    private TCComponentItemRevision m_selectedDocRev;
    private TCSession m_session;
    private String[][] m_xmlVals;
    private File m_tempLabelCSV;
    private File m_heatLabelCSV;
    private File m_compLabelCSV;
    private File m_wireLabelCSV;
    private Registry m_reg;
    String m_batchCodes[];
    
    public NOVCSVCreator(TCComponentItemRevision itemRev, String[] batchCodes, String[][] xmlVals)
    {
        m_selectedDocRev = itemRev;
        m_batchCodes = batchCodes;
        m_xmlVals = xmlVals;
        m_session = (TCSession) AIFUtility.getDefaultSession();
        m_reg = Registry.getRegistry(this);
    }
    
    public void generateAllCSVFile(String sTempFolder)
    {
        NOVTempLabelCSV tempLblObj = new NOVTempLabelCSV(m_batchCodes, m_xmlVals);
        m_tempLabelCSV = tempLblObj.generateCSVFile(sTempFolder);
        
        NOVComponentLabelCSV compLblObj = new NOVComponentLabelCSV(m_batchCodes, m_xmlVals);
        m_compLabelCSV = compLblObj.generateCSVFile(sTempFolder);
        
        NOVHeatLabelCSV heatLblObj = new NOVHeatLabelCSV(m_batchCodes, m_xmlVals);
        m_heatLabelCSV = heatLblObj.generateCSVFile(sTempFolder);
        
        NOVWireLabelCSV wireLblObj = new NOVWireLabelCSV(m_batchCodes, m_xmlVals);
        m_wireLabelCSV = wireLblObj.generateCSVFile(sTempFolder);
        
        exportCSVFiles();
        deleteLocalCSVFiles();
    }
    
    private void exportCSVFiles()
    {
        String[] namedReferences = new String[1];
        namedReferences = getNamedRefOfDataSet(m_session, "Text");
        
        try
        {
            TCComponentDatasetType newDataset = (TCComponentDatasetType) m_session.getTypeService().getTypeComponent(
                    "Dataset");
            
            TCComponentDataset newDatasetTempLbl = newDataset.setFiles(m_reg.getString("tempLbl.NAME"), "", "Text",
                    new String[] { m_tempLabelCSV.getAbsolutePath() }, namedReferences);
            TCComponentDataset newDatasetHeatLbl = newDataset.setFiles(m_reg.getString("heatLbl.NAME"), "", "Text",
                    new String[] { m_heatLabelCSV.getAbsolutePath() }, namedReferences);
            TCComponentDataset newDatasetWireLbl = newDataset.setFiles(m_reg.getString("wireLbl.NAME"), "", "Text",
                    new String[] { m_wireLabelCSV.getAbsolutePath() }, namedReferences);
            TCComponentDataset newDatasetCompLbl = newDataset.setFiles(m_reg.getString("compLbl.NAME"), "", "Text",
                    new String[] { m_compLabelCSV.getAbsolutePath() }, namedReferences);
            
            m_selectedDocRev.add("IMAN_specification", newDatasetTempLbl);
            m_selectedDocRev.add("IMAN_specification", newDatasetHeatLbl);
            m_selectedDocRev.add("IMAN_specification", newDatasetWireLbl);
            m_selectedDocRev.add("IMAN_specification", newDatasetCompLbl);
        }
        catch (TCException e)
        {
            e.printStackTrace();
        }
    }
    
    private String[] getNamedRefOfDataSet(TCSession tcsession, String datasettype)
    {
        String namedRef[] = null;
        try
        {
            TCComponentDatasetDefinitionType tccompdatasetdefitype = (TCComponentDatasetDefinitionType) tcsession
                    .getTypeComponent("DatasetType");
            
            TCComponentDatasetDefinition datasetdef = tccompdatasetdefitype.find(datasettype);
            namedRef = datasetdef.getNamedReferences();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return namedRef;
    }
    
    private void deleteLocalCSVFiles()
    {
        if (m_tempLabelCSV.isFile())
        {
            m_tempLabelCSV.delete();
        }
        if (m_heatLabelCSV.isFile())
        {
            m_heatLabelCSV.delete();
        }
        if (m_wireLabelCSV.isFile())
        {
            m_wireLabelCSV.delete();
        }
        if (m_compLabelCSV.isFile())
        {
            m_compLabelCSV.delete();
        }
    }
    
}
