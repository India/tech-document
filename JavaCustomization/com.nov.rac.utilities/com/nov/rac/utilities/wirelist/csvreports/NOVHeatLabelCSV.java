package com.nov.rac.utilities.wirelist.csvreports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.teamcenter.rac.util.Registry;

public class NOVHeatLabelCSV
{
    private String[][] m_xmlVal;
    private String m_batchCodes[];
    private static String whiteSpaceStr = "     ";
    private Registry m_reg;
    
    public NOVHeatLabelCSV(String batchCodes[], String[][] xmlVals)
    {
        m_xmlVal = xmlVals;
        m_batchCodes = batchCodes;
        m_reg = Registry.getRegistry(this);
    }
    
    public File generateCSVFile(String sTempFolder)
    {
        String sFile = getFilePath(sTempFolder);
        File file = new File(sFile);
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append(m_batchCodes[0]);
            writer.append("\r\n");
            writer.append(m_batchCodes[1]);
            writer.append("\r\n");
            
            List<String> formattedVals = formatValues(m_xmlVal);
            for (int i = 0; i < formattedVals.size(); i++)
            {
                writer.append(formattedVals.get(i));
                if (i != formattedVals.size() - 1)
                {
                    writer.append("\r\n");
                }
            }
            writer.flush();
            writer.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return file;
    }
    
    private List<String> formatValues(String[][] xmlVal)
    {
        List<String> formattedVals = new ArrayList<String>();
        
        for (int i = 0; i < xmlVal.length; i++)
        {
            String sWireId = xmlVal[i][0];
            if (!sWireId.equals("") && sWireId != null)
            {
                StringBuffer firstCellData = new StringBuffer();
                firstCellData.append(sWireId);
                firstCellData.append(whiteSpaceStr);
                firstCellData.append(sWireId);
                formattedVals.add(firstCellData.toString());
            }
        }
        return formattedVals;
    }
    
    private String getFilePath(String folderName)
    {
        StringBuffer fileName = new StringBuffer(folderName);
        fileName.append("\\");
        fileName.append(m_reg.getString("heatLbl.NAME"));
        fileName.append(new SimpleDateFormat("HHmmss").format(new Date()));
        fileName.append(".txt");
        return fileName.toString();
    }
}
