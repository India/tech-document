package com.nov.rac.utilities.wirelist.csvreports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.teamcenter.rac.util.Registry;

public class NOVComponentLabelCSV
{
    private String[][] m_xmlVal;
    private String m_batchCodes[];
    private Registry m_reg;
    
    public NOVComponentLabelCSV(String batchCodes[], String[][] xmlVals)
    {
        m_xmlVal = xmlVals;
        m_batchCodes = batchCodes;
        m_reg = Registry.getRegistry(this);
    }
    
    public File generateCSVFile(String sTempFolder)
    {
        String sFile = getFilePath(sTempFolder);
        File file = new File(sFile);
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append(m_batchCodes[0]);
            writer.append("\r\n");
            writer.append(m_batchCodes[1]);
            writer.append("\r\n");
            
            List<String> formattedVals = formatValues(m_xmlVal);
            for (int i = 0; i < formattedVals.size(); i++)
            {
                writer.append(formattedVals.get(i));
                if (i != formattedVals.size() - 1)
                {
                    writer.append("\r\n");
                }
            }
            writer.flush();
            writer.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return file;
    }
    
    private List<String> formatValues(String[][] xmlVal)
    {
        Set<String> cmpValSet = new HashSet<String>();
        
        for (int i = 0; i < xmlVal.length; i++)
        {
        	String sComp_t = xmlVal[i][6];
            if (!sComp_t.equals("") && sComp_t != null)
            {
            	cmpValSet.add(sComp_t);
            }        	
            
            String sComp_f = xmlVal[i][3];
            if (!sComp_f.equals("") && sComp_f != null)
            {
            	cmpValSet.add(sComp_f);
            }  
        }
        
        List<String> formattedVals = new ArrayList<String>(cmpValSet);
        Collections.sort(formattedVals);
        
        return formattedVals;
    }
    
    private String getFilePath(String folderName)
    {
        StringBuffer fileName = new StringBuffer(folderName);
        fileName.append("\\");
        fileName.append(m_reg.getString("compLbl.NAME"));
        fileName.append(new SimpleDateFormat("HHmmss").format(new Date()));
        fileName.append(".txt");
        return fileName.toString();
    }
}
