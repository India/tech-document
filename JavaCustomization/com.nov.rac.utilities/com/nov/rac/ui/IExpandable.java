package com.nov.rac.ui;
/*
 * Interface: IExpandable
 * Description: To enable expanded or collapsed state of the panel.
 * This interface must me implemented by the panels.
 */
public interface IExpandable
{
    void setExpanded(boolean expanded);
}
