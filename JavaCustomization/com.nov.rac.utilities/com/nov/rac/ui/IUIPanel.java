/**
 * 
 */
package com.nov.rac.ui;

import com.teamcenter.rac.kernel.TCException;

/**
 * @author MundadaV
 *
 */
public interface IUIPanel
{
    public boolean createUI () throws TCException;
    public boolean reload () throws TCException;
    public Object getUIPanel();
    
   /* // TCDECREL-6756 For setting mandatory fields
    public void createUIPost() throws TCException;*/
    
    /**
     * reset():
     *   This function is a place holder wherein UI panels can reset their UI state.
     *   
     * @return
     */
    public boolean reset();
    
    public void dispose();
	public boolean createUIPost() throws TCException;
    
}
