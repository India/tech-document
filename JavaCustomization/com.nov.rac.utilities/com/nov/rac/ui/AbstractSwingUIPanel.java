/**
 * 
 */
package com.nov.rac.ui;

import javax.swing.JPanel;

import com.teamcenter.rac.kernel.TCException;

/**
 * @author viveksm
 *
 */
public abstract class AbstractSwingUIPanel extends JPanel implements IUIPanel 
{
	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see com.nov.rac.ui.IUIPanel#reload()
	 */
	@Override
	public boolean reload() throws TCException {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.nov.rac.ui.IUIPanel#getUIPanel()
	 */
	@Override
	public Object getUIPanel() 
	{
		return this;
	}
	
	/*// TCDECREL-6756 Red asterisk issue
    @Override
    public void createUIPost() throws TCException
    {
        // TODO Auto-generated method stub
    }    */

	@Override
	public boolean reset() 
	{
		return true;
	}
	
	@Override
	public void dispose() 
	{
		
	}
	
	@Override
	public boolean createUIPost() throws TCException {
		// TODO Auto-generated method stub
		return false;
	}

}
