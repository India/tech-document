//TC10.1 Upgrade
package com.nov.rac.ui.perspective;

import com.teamcenter.rac.aif.IPerspectiveDef;
import com.teamcenter.rac.aif.IPerspectiveDefService;
import com.teamcenter.rac.aifrcp.AifrcpPlugin;
import com.teamcenter.rac.services.IOpenService;
import com.teamcenter.rac.util.OSGIUtil;
import com.teamcenter.rac.util.Registry;

public class PerspectiveDefHelper
{
    public static IPerspectiveDefService getPerspectiveDefService()
    {
        IPerspectiveDefService iperspectivedefservice = (IPerspectiveDefService)OSGIUtil.getService(AifrcpPlugin.getDefault(), com.teamcenter.rac.aif.IPerspectiveDefService.class);

        return iperspectivedefservice;
    }

    public static IPerspectiveDef getPerspectiveDef(String perspectiveId)
    {
        IPerspectiveDefService iperspectivedefservice = getPerspectiveDefService();

        IPerspectiveDef perspectiveDef = null;

        if(iperspectivedefservice != null )
        {
            perspectiveDef = iperspectivedefservice.findByPerspectiveId(perspectiveId);
        }

        return perspectiveDef;
    }

    public static IPerspectiveDef getPerspectiveDefByLegacyAppID(String legacyAppId)
    {
        IPerspectiveDefService iperspectivedefservice = getPerspectiveDefService();

        IPerspectiveDef perspectiveDef = null;

        if(iperspectivedefservice != null )
        {
            perspectiveDef = iperspectivedefservice.findByLegacyAppId(legacyAppId);
        }

        return perspectiveDef;
    }

    public static IOpenService getOpenService(String perspectiveId)
    {
        IPerspectiveDef perspectiveDef = getPerspectiveDef(perspectiveId);

        IOpenService openService = null;

        if(perspectiveDef != null )
        {
            openService = perspectiveDef.getOpenService();
        }

        return openService;
    }
    
    public static IPerspectiveDef getPerspectiveDefByTitle(Registry registry,String name)
    {
        IPerspectiveDefService iperspectivedefservice = getPerspectiveDefService();

        IPerspectiveDef perspectiveDef = null;
        
        if(iperspectivedefservice != null )
        {
            perspectiveDef = iperspectivedefservice.createPerspectiveDefFromRegistry(registry, name);
        }
        
        return perspectiveDef;
    }    
}
