package com.nov.rac.ui.factory;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Instancer;
import com.teamcenter.rac.util.Registry;

public class UIPanelFactory
{
    private Registry m_registry = null;
    private static UIPanelFactory m_factory = null;
    private final String CLASS_NAME = "CLASS";
    
    private UIPanelFactory()
    {
        
    }
    
    // public UIPanelFactory (Registry registry,String... varargs)
    // {
    // m_registry = registry;
    //
    // }
    
    public Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry m_registry)
    {
        this.m_registry = m_registry;
    }
    
    public static UIPanelFactory getInstance()
    {
        if (m_factory == null)
        {
            m_factory = new UIPanelFactory();
        }
        
        return m_factory;
    }
        
    public ArrayList<IUIPanel> createPanels(Composite parent, String panelType,String theGroup)
            throws Exception
    {
        return createPanels(parent, panelType,theGroup, false);
    }
    
    public ArrayList<IUIPanel> createPanels(Composite parent, String panelType,String theGroup,
            boolean withSeparator) throws Exception
    {
        String[] panelNames;
        ArrayList<IUIPanel> panels = new ArrayList<IUIPanel>();
        IUIPanel panel = null;
        
        if (m_factory == null)
        {
            throw new Exception("Registry is not set.");
        }
        
        panelNames = RegistryUtils.getConfiguration(theGroup,panelType + "."
                + "PANELS_LIST", m_registry);
        
        // create panels from panel names
        Object[] params = new Object[2];
        params[0] = parent;
        params[1] = SWT.NONE;
        
        for (String panelName : panelNames)
        {
            panel = createPanel(theGroup,panelName, params);
            if (panel != null)
            {
                try
                {
                    panel.createUI();
                    if (withSeparator)
                    {
                        addSeparator(parent);
                    }
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
                
                panels.add(panel);
            }
        }
        
        return panels;
    }
    
    private IUIPanel createPanel(String theGroup,String panelName, Object[] params)
    {
        String[] configParams = RegistryUtils.getConfiguration(theGroup,panelName + "."
                + CLASS_NAME, m_registry);
        String className = configParams[0];
        IUIPanel panel = null;
        try
        {
            panel = (IUIPanel) Instancer.newInstanceEx(className, params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return panel;
    }
    
    private void addSeparator(Composite parent)
    {
        Label separator = new Label(parent, SWT.HORIZONTAL | SWT.SEPARATOR);
        separator.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    }
}
