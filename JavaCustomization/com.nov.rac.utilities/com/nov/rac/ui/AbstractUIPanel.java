package com.nov.rac.ui;

import org.eclipse.swt.widgets.Composite;

import com.teamcenter.rac.kernel.TCException;

public abstract class AbstractUIPanel implements IUIPanel
{
    public AbstractUIPanel(Composite parent, int style)
    {
//    	final ExpandableComposite theComposite = new ExpandableComposite(parent, SWT.NONE, ExpandableComposite.TWISTIE | ExpandableComposite.EXPANDED );
//    	theComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//    	theComposite.setLayout( new TableWrapLayout() );
//    	  	
//    	final Composite actualComposite = new Composite(theComposite, SWT.NONE);
//    	actualComposite.setLayout( new GridLayout(1, true));
//    	//actualComposite.setLayoutData( new TableWrapData(SWT.LEFT, SWT.TOP, 1, 1));
    	
        composite = createComposite(parent, style); //new Composite(actualComposite, style);
        
        //theComposite.setText("This is text");
//        theComposite.setSize(actualComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT) );
//        theComposite.setClient(actualComposite);    
//        theComposite.layout();
//        theComposite.getParent().layout();
//        
//        theComposite.addExpansionListener(new ExpansionAdapter() 
//        {
//            public void expansionStateChanged(ExpansionEvent e) 
//            {
//                // resizes the application window.
//            	//theComposite.getShell().pack(true);
//            	theComposite.setSize(actualComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT) );
//            	theComposite.layout();
//            	theComposite.getParent().layout();
//            }
//          });

        
    }
    
    public Composite getComposite()
    {
        return composite;
    }
    
    @Override
    public Object getUIPanel()
    {
        return getComposite();
    }
    
    /*// TCDECREL-6756 Red asterisk issue
    @Override
    public void createUIPost() throws TCException
    {
        // TODO Auto-generated method stub
    }    */
    
    public boolean reset()
    {
        return true;
    }
    
    @Override
    public void dispose()
    {
          if(!composite.isDisposed())
              composite.dispose();
    }
    
    protected Composite createComposite(Composite parent, int style)
    {
    	return new Composite(parent, style);
    }
    
    public abstract boolean createUI() throws TCException;
    
    public abstract boolean reload() throws TCException;
    
    protected Composite composite;

	public boolean createUIPost() throws TCException {
		// TODO Auto-generated method stub
		return false;
	}
}
