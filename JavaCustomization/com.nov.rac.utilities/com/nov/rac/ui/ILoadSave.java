/**
 * 
 */
package com.nov.rac.ui;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCException;

/**
 * @author MundadaV
 *
 */
public interface ILoadSave
{
    public boolean load(IPropertyMap propMap) throws TCException;
    
    public boolean save(IPropertyMap propMap) throws TCException;
    
    public boolean validate(IPropertyMap propMap) throws TCException;
}
