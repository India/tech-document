package com.nov.rac.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import com.teamcenter.rac.kernel.TCException;

public class AbstractExpandableUIPanel  extends AbstractUIPanel implements IExpandable
{

	public AbstractExpandableUIPanel(Composite parent, int style)
	{
		super(parent, style);
		
//    	final ExpandableComposite theComposite = new ExpandableComposite(parent, SWT.NONE, ExpandableComposite.TWISTIE | ExpandableComposite.EXPANDED );
//    	theComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//    	theComposite.setLayout( new TableWrapLayout() );
//    	  	
//    	final Composite actualComposite = new Composite(theComposite, SWT.NONE);
//    	actualComposite.setLayout( new GridLayout(1, true));
//    	//actualComposite.setLayoutData( new TableWrapData(SWT.LEFT, SWT.TOP, 1, 1));
//    	
//        composite = new Composite(actualComposite, style);
//        
//        //theComposite.setText("This is text");
//        theComposite.setSize(actualComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT) );
//        theComposite.setClient(actualComposite);    
//        theComposite.layout();
//        theComposite.getParent().layout();
//        
//        theComposite.addExpansionListener(new ExpansionAdapter() 
//        {
//            public void expansionStateChanged(ExpansionEvent e) 
//            {
//                // resizes the application window.
//            	//theComposite.getShell().pack(true);
//            	theComposite.setSize(actualComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT) );
//            	theComposite.layout();
//            	theComposite.getParent().layout();
//            }
//          });
	}

	@Override
	public boolean createUI() throws TCException 
	{	
		return false;
	}

	@Override
	public boolean reload() throws TCException 
	{	
		return false;
	}
	@Override
    protected Composite createComposite(Composite parent, int style)
    {
    	//m_exandableComposite = new ExpandableComposite(parent, SWT.BORDER, ExpandableComposite.TWISTIE | ExpandableComposite.EXPANDED );
    	//final ExpandableComposite theComposite = m_exandableComposite;
       	final ExpandableComposite theComposite = new ExpandableComposite(parent,SWT.BORDER, ExpandableComposite.TWISTIE | ExpandableComposite.EXPANDED);
       	
    	theComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
    	theComposite.setLayout( new TableWrapLayout() );
    
    	
    	final Composite actualComposite = new Composite(theComposite, SWT.NONE);
    	actualComposite.setLayout( new GridLayout(1, true));
    	//actualComposite.setLayoutData( new TableWrapData(SWT.LEFT, SWT.TOP, 1, 1));
    	
        composite = new Composite(actualComposite, style);
        
        m_exandableComposite =   theComposite;
        
        //theComposite.setText("This is text");
        theComposite.setSize(actualComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT) );
        theComposite.setClient(actualComposite);    
        theComposite.layout();
        theComposite.getParent().layout();
        
        theComposite.addExpansionListener(new ExpansionAdapter() 
        {
            public void expansionStateChanged(ExpansionEvent e) 
            {
                ExpandableComposite theComposite = (ExpandableComposite) e.getSource();
                theComposite.setSize(actualComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT) );
                theComposite.layout();
                theComposite.getParent().layout();
//              theComposite.getParent().redraw();
//              theComposite.getParent().update();
            
                ((ScrolledComposite)theComposite.getParent().getParent().getParent()).setMinSize(theComposite.getParent().getParent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
            }
          });
        
              

    	return composite;
    }
    
    protected ExpandableComposite getExpandableComposite()
    {
    	return m_exandableComposite;
    }

    /*
     * (non-Javadoc)
     * @see com.nov.rac.ui.IExpandable#setExpanded(boolean)
     * Description: This function set the state of the panel. State may be 
     * expanded or collapsed.
     */
    @Override
    public void setExpanded(boolean expanded)
    {
        m_exandableComposite.setExpanded(expanded);
    }
    
    private ExpandableComposite m_exandableComposite;


}
