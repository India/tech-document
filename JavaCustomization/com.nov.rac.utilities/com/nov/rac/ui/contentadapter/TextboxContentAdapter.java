package com.nov.rac.ui.contentadapter;

import java.util.List;

import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.widgets.Control;

public class TextboxContentAdapter extends TextContentAdapter
{
    private List<String> values;
    
    public TextboxContentAdapter(List<String> values)
    {
        super();
        this.values = values;
    }
    
    @Override
    public String getControlContents(Control control)
    {
        String cotrolText = super.getControlContents(control);
        if (!isValidPrefix(cotrolText))
        {
            setControlContents(control, "", 0);
            cotrolText = "";
        }
        return cotrolText;
    }
    
    boolean isValidPrefix(String prefix)
    {
        boolean isValid = false;
        if (prefix.equals(""))
        {
            isValid = true;
        }
        else
        {
            for (String string : values)
            {
                if (string.regionMatches(true, 0, prefix, 0, prefix.length()))
                {
                    isValid = true;
                    break;
                }
            }
        }
        return isValid;
    }
}
