package com.nov.rac.ui.fieldassist;

import org.eclipse.jface.fieldassist.ComboContentAdapter;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

public class CommonContentAdapter implements IControlContentAdapter
{
    private IControlContentAdapter controlContentAdapter;
    private String[] proposals;
    
    public String[] getProposals()
    {
        return proposals;
    }

    public void setProposals(String[] proposals)
    {
        this.proposals = proposals;
    }

    protected CommonContentAdapter(IControlContentAdapter controlContentAdapter,String[] proposals)
    {
        this.controlContentAdapter = controlContentAdapter;
        this.proposals = proposals;
    }
    
    public CommonContentAdapter(Text text ,String[] proposals)
    {
        this(new TextContentAdapter(),proposals);
    }
    
    public CommonContentAdapter(Combo combo ,String[] proposals)
    {
        this(new ComboContentAdapter(),proposals);
    }
    
    @Override
    public String getControlContents(Control control)
    {
        String cotrolText = controlContentAdapter.getControlContents(control);
        if (!isValidPrefix(cotrolText))
        {
            setControlContents(control, "", 0);
            cotrolText = "";
        }
        return cotrolText;
    }
    
    boolean isValidPrefix(String prefix)
    {
        boolean isValid = false;
        if (prefix.equals(""))
        {
            isValid = true;
        }
        else
        {
            for (String string : proposals)
            {
                if (string.regionMatches(true, 0, prefix, 0, prefix.length()))
                {
                    isValid = true;
                    break;
                }
            }
        }
        return isValid;
    }
    
    @Override
    public int getCursorPosition(Control control)
    {
        return controlContentAdapter.getCursorPosition(control);
    }
    
    @Override
    public Rectangle getInsertionBounds(Control control)
    {
        return controlContentAdapter.getInsertionBounds(control);
    }
    
    @Override
    public void insertControlContents(Control control, String s, int i)
    {
        controlContentAdapter.insertControlContents(control, s, i);
        
    }
    
    @Override
    public void setControlContents(Control control, String s, int i)
    {
        controlContentAdapter.setControlContents(control, s, i);
        
    }
    
    @Override
    public void setCursorPosition(Control control, int i)
    {
        controlContentAdapter.setCursorPosition(control, i);
        
    }
    
}
