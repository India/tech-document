package com.nov.rac.ui.fieldassist;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;


public class NovAutoCompleteField extends org.eclipse.jface.fieldassist.AutoCompleteField
{
    private CommonContentAdapter contentAdapter;
    
    protected NovAutoCompleteField(Control control,CommonContentAdapter controlContentAdapter)
    {
        super(control, controlContentAdapter, controlContentAdapter.getProposals());
        this.contentAdapter = controlContentAdapter;
    }
    
    public NovAutoCompleteField(Text text ,String[] proposals)
    {
        this(text,new CommonContentAdapter(text, proposals));
    }
    
    public NovAutoCompleteField(Combo combo ,String[] proposals)
    {
        this(combo,new CommonContentAdapter(combo, proposals));
        combo.setItems(proposals);
    }
    
    public String[] getProposals()
    {
        return contentAdapter.getProposals();
    }
    
    
    @Override
    public void setProposals(String[] proposals)
    {
        super.setProposals(proposals);
        contentAdapter.setProposals(proposals);
    }
    
}
