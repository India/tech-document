package com.nov.rac.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.common.UserSessionInfo;
import com.nov.rac.utilities.thread.utils.NOVCustomRunnable;
import com.nov.rac.utilities.utils.ExceptionStack;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractFormOperationDelegate implements
        IOperationDelegate
{
    protected ExceptionStack stack = new ExceptionStack();
    
    public AbstractFormOperationDelegate()
    {
    }
    
    @Override
    abstract public boolean preCondition() throws TCException;
    
    @Override
    abstract public boolean preAction() throws TCException;
    
    @Override
    abstract public boolean baseAction() throws TCException;
    
    @Override
    public boolean postAction() throws TCException
    {
        return true;
    }
    
    private void runWithProgress(final IProgressMonitor monitor)
            throws Exception
    {
        final Registry registry = Registry
                .getRegistry("com.nov.rac.operations.operations");
        monitor.beginTask(registry.getString("Operation.MESSAGE"), 10000);
        final NOVCustomRunnable runnable;
        
        Display.getDefault().syncExec(runnable = new NOVCustomRunnable()
        {
            @Override
            public void run()
            {
                try
                {
                    // monitor.subTask(registry.getString("Precondition.MESSAGE"));
                    success = preCondition();
                    if (success)
                    {
                        // monitor.subTask(registry.getString("CreateObject.MESSAGE"));
                        success = preAction();
                        
                        if (success)
                        {
                            // monitor.subTask("Executing baseAction");
                            success = baseAction();
                            if (success)
                            {
                                // monitor.subTask("Executing postAction");
                                success = postAction();
                            }
                        }
                    }
                    monitor.done();
                }
                catch (TCException exception)
                {
                    monitor.setCanceled(true);
                    monitor.done();
                    setException(exception);
                    success = false;
                }
                finally
                {
                    // refreshContainer();
                }
            }
        });
        
        if (!success && null != runnable.getException())
        {
            throw runnable.getException();
        }
    }
    
    @Override
    public boolean executeOperation() throws Exception
    {
        stack.clear(); // To clear the error stack captured in previous
                       // executeOperation() call
        
        final Display display = PlatformUI.getWorkbench().getDisplay();
        if (display != null)
        {
            final NOVCustomRunnable runnable;
            display.syncExec((runnable = new NOVCustomRunnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(
                                m_shell);
                        progressMonitorDialog.run(true, false,
                                new IRunnableWithProgress()
                                {
                                    @Override
                                    public void run(
                                            final IProgressMonitor monitor)
                                    {
                                        try
                                        {
                                            runWithProgress(monitor);
                                        }
                                        catch (Exception e)
                                        {
                                            setException(e);
                                        }
                                    }
                                });
                    }
                    catch (Exception ex)
                    {
                        setException(ex);
                    }
                }
            }));
            
            if (!success && null != runnable.getException())
            {
                throw runnable.getException();
            }
        }
        
        return success;
    }
    
    /**
     * Gets the filtered panels.
     * 
     * @param objectType
     *            the item type - by which given panels will be filtered
     * @param panels
     *            the panels
     * @return the filtered panels
     */
    // Nov4ObsoleteForm.OBJECTS_LIST=Form,Disposition
    // Nov4ObsoleteForm.Form.DATA_PANELS_LIST=Panel1,Panel2,Panel4
    // Nov4ObsoleteForm.Disposition.DATA_PANELS_LIST=Panel3
    
    // Nov4NewForm.Form.DATA_PANELS_LIST=Panel1,Panel2,Panel4, Panel8
    // Nov4NewForm.Disposition.DATA_PANELS_LIST=Panel3, Panel6
    
    // objectType = Nov4ObsoleteForm
    // contextInfo = Form OR Disposition
    //
    protected final List<IUIPanel> getFilteredPanels(String theGroup,
            String objectType, String contextInfo, List<IUIPanel> panels)
    {
        StringBuffer contextString = new StringBuffer();
        contextString.append(objectType).append(".").append(contextInfo)
                .append(".").append("DATA_PANELS_LIST");
        
        Registry registry = getRegistry();
        
        // String[] panelNames =
        // registry.getStringArray(contextString.toString(),
        // null);
        String[] panelNames = RegistryUtils.getConfiguration(theGroup,
                contextString.toString(), registry);
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        return panelList;
    }
    
    protected final List<IUIPanel> getFilteredPanels(String objectType, String contextInfo, List<IUIPanel> panels)
    {
        String currentGroup = UserSessionInfo.getCurrentGroup();
        
        return getFilteredPanels(currentGroup,objectType, contextInfo,panels);
        
    }
    
    protected Registry getRegistry()
    {
        Registry registry = Registry
                .getRegistry("com.nov.rac.processform.delegate.delegate");
        return registry;
    }
    
    /**
     * Fill panels data into given property map.
     * 
     * @param itemType
     *            the item type
     * @param type
     * @param panels
     *            the panels
     * @param propertyMap
     *            the property map
     */
    protected final void fillPanelsData(String theGroup,String itemType, String type,
            List<IUIPanel> panels, IPropertyMap propertyMap)
    {
        List<IUIPanel> filteredPanels = getFilteredPanels(theGroup,itemType, type,
                panels);
        
        for (IUIPanel panel : filteredPanels)
        {
            if (panel instanceof ILoadSave)
            {
                ILoadSave saveablepanel = (ILoadSave) panel;
                try
                {
                    saveablepanel.save(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    protected final void fillPanelsData(String itemType, String type,
            List<IUIPanel> panels, IPropertyMap propertyMap)
    {
        String currentGroup = UserSessionInfo.getCurrentGroup();
        
        fillPanelsData(currentGroup,itemType, type,panels, propertyMap);
        
    }
    
    @Override
    public void registerOperationInputProvider(List<IUIPanel> inputProviders)
    {
        m_OperationInputProviders = inputProviders;
    }
    
    /**
     * Gets the operation input providers.
     * 
     * @return the operation input providers
     */
    protected final List<IUIPanel> getOperationInputProviders()
    {
        return m_OperationInputProviders;
    }
    
    protected List<IUIPanel> m_OperationInputProviders = null;
    private Shell m_shell;
    private boolean success = false;
}
