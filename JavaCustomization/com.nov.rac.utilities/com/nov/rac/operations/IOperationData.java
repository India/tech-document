package com.nov.rac.operations;

import java.util.List;

import com.nov.rac.propertymap.IPropertyMap;

public interface IOperationData
{
    List<IPropertyMap> getTableValues(List<IPropertyMap> propertyMapList);
    List<IPropertyMap> getSelectedPartInfo(List<IPropertyMap> propertyMapList);
}
