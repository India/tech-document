package com.nov.rac.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.rac.interfaces.IFormLoadDelegate;
import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.common.UserSessionInfo;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractFormLoadDelegate implements IFormLoadDelegate
{
    protected List<IUIPanel> m_OperationInputProviders = null;
    protected Registry m_registry = null;
    private final String PANELS_LIST = "PANELS_LIST";
    
    private Registry getRegistry()
    {
        return m_registry;
    }
    
    public void loadPanelsData(String theGroup, String itemType,
            List<IUIPanel> panels, IPropertyMap propertyMap)
    {
        List<IUIPanel> filteredPanels = getFilteredPanels(theGroup, itemType,
                panels);
        
        for (IUIPanel panel : filteredPanels)
        {
            if (null != propertyMap && panel instanceof ILoadSave)
            {
                ILoadSave loadablepanel = (ILoadSave) panel;
                try
                {
                    loadablepanel.load(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    protected final List<IUIPanel> getFilteredPanels(String theGroup,
            String objectType, List<IUIPanel> panels)
    {
        StringBuffer contextString = new StringBuffer();
        contextString.append(objectType).append(".").append(PANELS_LIST);
        
        Registry registry = getRegistry();
        
        String[] panelNames = RegistryUtils.getConfiguration(theGroup,
                contextString.toString(), registry);
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        return panelList;
    }
    
    protected final List<IUIPanel> getFilteredPanels(String objectType,
            List<IUIPanel> panels)
    {
        String currentGroup = UserSessionInfo.getCurrentGroup();
        
        return getFilteredPanels(currentGroup, objectType, panels);
        
    }
    
    @Override
    public final List<IUIPanel> getInputProviders()
    {
        return m_OperationInputProviders;
    }
    
    @Override
    public void registerOperationInputProvider(List<IUIPanel> inputProviders)
    {
        m_OperationInputProviders = inputProviders;
    }
    
}
