package com.nov.rac.operations;

import com.nov.rac.propertymap.IPropertyMap;
import com.teamcenter.rac.kernel.TCComponent;

public interface IOperationInputProvider
{
 public IPropertyMap[] populateOperationInput( IPropertyMap[] helper);
    
    public TCComponent getComponent();
}
