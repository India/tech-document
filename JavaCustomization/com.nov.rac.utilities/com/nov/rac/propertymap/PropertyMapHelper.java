/**
 * 
 */
package com.nov.rac.propertymap;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.graphics.Image;

import com.teamcenter.rac.common.TCTypeRenderer;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCProperty;

/**
 * @author MundadaV
 * 
 */
public class PropertyMapHelper
{
    
    public static void componentToMap(TCComponent tcComponent,
            String[] propertyNames, IPropertyMap propertyMap)
            throws TCException
    {
        // For all property names, get a list of TCProperty[] objects.
        TCProperty[] tcPropertyObject = tcComponent
                .getTCProperties(propertyNames);
        
        // Copy the TCProperties to Property Map
        addPropertiesToMap(tcPropertyObject, propertyMap);
    }
    
    public static void addPropertiesToMap(TCProperty[] tcPropertyArray,
            IPropertyMap propertyMap)
    {
        for (TCProperty tcProperty : tcPropertyArray)
        {
            addPropertyToMap(tcProperty, propertyMap);
        }
    }
    
    public static void addPropertyToMap(TCProperty tcProperty,
            IPropertyMap propertyMap)
    {
        int propertyType = tcProperty.getPropertyType();
        boolean isArray = !tcProperty.isNotArray();
        String propertyName = tcProperty.getPropertyName();
        Object propertyValue = tcProperty.getPropertyValue();
        
        if (propertyType == TCProperty.PROP_typed_reference
                || propertyType == TCProperty.PROP_untyped_reference
                || propertyType == TCProperty.PROP_external_reference
                || propertyType == TCProperty.PROP_untyped_relation)
        {
            if (isArray)
            {
                propertyValue = tcProperty.getReferenceValueArray();
            }
            else
            {
                propertyValue = tcProperty.getReferenceValue();
            }
            
        }
        
        addPropertyValueToMap(propertyName, propertyValue, propertyType,
                isArray, propertyMap);
    }
    
    public static void addPropertyValueToMap(String propertyName,
            Object originalPropertyValue, IPropertyMap propertyMap)
    {
        int propertyType = TCProperty.PROP_untyped;
        boolean isArray = originalPropertyValue.getClass().isArray();
        
        Object propertyValue = originalPropertyValue;
        
        if (isArray)
        {
            propertyValue = ((Object[]) originalPropertyValue)[0];
        }
        
        if (propertyValue instanceof Date)
        {
            propertyType = TCProperty.PROP_date;
        }
        
        if (propertyValue instanceof Double)
        {
            propertyType = TCProperty.PROP_double;
        }
        
        if (propertyValue instanceof Float)
        {
            propertyType = TCProperty.PROP_float;
        }
        
        if (propertyValue instanceof Integer)
        {
            propertyType = TCProperty.PROP_int;
        }
        
        if (propertyValue instanceof Boolean)
        {
            propertyType = TCProperty.PROP_logical;
        }
        
        if (propertyValue instanceof String)
        {
            propertyType = TCProperty.PROP_string;
        }
        
        if (propertyValue instanceof TCComponent)
        {
            propertyType = TCProperty.PROP_typed_reference;
        }
        
        addPropertyValueToMap(propertyName, originalPropertyValue,
                propertyType, isArray, propertyMap);
        
    }
    
    public static void addPropertyValueToMap(String propertyName,
            Object propertyValue, int propertyType, boolean isArray,
            IPropertyMap propertyMap)
    {
        switch (propertyType)
        {
            case TCProperty.PROP_untyped: // Implement for this kind of property
                break;
            case TCProperty.PROP_date:
                if (isArray)
                {
                    propertyMap.setDateArray(propertyName,
                            (Date[]) propertyValue);
                }
                else
                {
                    propertyMap.setDate(propertyName, (Date) propertyValue);
                }
                break;
            case TCProperty.PROP_double:
                if (isArray)
                {
                    propertyMap.setDoubleArray(propertyName,
                            (Double[]) propertyValue);
                }
                else
                {
                    propertyMap.setDouble(propertyName, (Double) propertyValue);
                }
                break;
            case TCProperty.PROP_float:
                if (isArray)
                {
                    propertyMap.setFloatArray(propertyName,
                            (Float[]) propertyValue);
                }
                else
                {
                    propertyMap.setFloat(propertyName, (Float) propertyValue);
                }
                break;
            case TCProperty.PROP_int:
            case TCProperty.PROP_short:
                if (isArray)
                {
                    propertyMap.setIntegerArray(propertyName,
                            (Integer[]) propertyValue);
                }
                else
                {
                    propertyMap.setInteger(propertyName,
                            (Integer) propertyValue);
                }
                break;
            case TCProperty.PROP_logical:
                if (isArray)
                {
                    propertyMap.setLogicalArray(propertyName,
                            (Boolean[]) propertyValue);
                }
                else
                {
                    propertyMap.setLogical(propertyName,
                            (Boolean) propertyValue);
                }
                break;
            case TCProperty.PROP_string:
                if (isArray)
                {
                    propertyMap.setStringArray(propertyName,
                            (String[]) propertyValue);
                }
                else
                {
                    propertyMap.setString(propertyName, (String) propertyValue);
                }
                break;
            case TCProperty.PROP_typed_reference:
            case TCProperty.PROP_untyped_reference:
            case TCProperty.PROP_external_reference:
            case TCProperty.PROP_untyped_relation:
                if (isArray)
                {
                    // TCDECREL-4526 commented out
                    // propertyValue = tcProperty.getReferenceValueArray();
                    propertyMap.setTagArray(propertyName,
                            (TCComponent[]) propertyValue);
                }
                else
                {
                    // TCDECREL-4526 commented out
                    // propertyValue = tcProperty.getReferenceValue();
                    propertyMap.setTag(propertyName,
                            (TCComponent) propertyValue);
                }
                break;
        }
    }
    
    public static Object getPropertyValue(String propertyName,
            int propertyType, boolean isArray, IPropertyMap propertyMap)
    {
        
        Object propertyValue = null;
        
        switch (propertyType)
        {
            case TCProperty.PROP_untyped: // Implement for this kind of property
                break;
            case TCProperty.PROP_date:
                if (isArray)
                {
                    propertyValue = propertyMap.getDateArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getDate(propertyName);
                }
                break;
            case TCProperty.PROP_double:
                if (isArray)
                {
                    propertyValue = propertyMap.getDoubleArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getDouble(propertyName);
                }
                break;
            case TCProperty.PROP_float:
                if (isArray)
                {
                    propertyValue = propertyMap.getFloatArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getFloat(propertyName);
                }
                break;
            case TCProperty.PROP_int:
            case TCProperty.PROP_short:
                if (isArray)
                {
                    propertyValue = propertyMap.getIntegerArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getInteger(propertyName);
                }
                break;
            case TCProperty.PROP_logical:
                if (isArray)
                {
                    propertyValue = propertyMap.getLogicalArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getLogical(propertyName);
                }
                break;
            case TCProperty.PROP_string:
                if (isArray)
                {
                    propertyValue = propertyMap.getStringArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getString(propertyName);
                }
                break;
            case TCProperty.PROP_typed_reference:
            case TCProperty.PROP_untyped_reference:
            case TCProperty.PROP_external_reference:
                if (isArray)
                {
                    propertyValue = propertyMap.getTagArray(propertyName);
                }
                else
                {
                    propertyValue = propertyMap.getTag(propertyName);
                }
                break;
        }
        
        return propertyValue;
    }
    
    public static Object getPropertyValue(String propertyName,
            IPropertyMap propertyMap)
    {
        Object propertyValue = null;
        
        // Get STRING property names and check if given propertyName exists in
        // the list.
        Set<String> propertySet = propertyMap.getStringPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getString(propertyName);
        }
        
        // Get STRING ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getStringArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getStringArray(propertyName);
        }
        
        // Get LOGICAL property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getLogicalPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getLogical(propertyName);
        }
        
        // Get LOGICAL ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getLogicalArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getLogicalArray(propertyName);
        }
        
        // Get FLOAT property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getFloatPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getFloat(propertyName);
        }
        
        // Get FLOAT ARRAY property names and check if given propertyName exists
        // in the list.
        propertySet = propertyMap.getFloatArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getFloatArray(propertyName);
        }
        
        // Get INTEGER property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getIntegerPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getInteger(propertyName);
        }
        
        // Get INTEGER ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getIntegerArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getIntegerArray(propertyName);
        }
        
        // Get DATE property names and check if given propertyName exists in the
        // list.
        propertySet = propertyMap.getDatePropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getDate(propertyName);
        }
        
        // Get DATE ARRAY property names and check if given propertyName exists
        // in the list.
        propertySet = propertyMap.getDateArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getDateArray(propertyName);
        }
        
        // Get DOUBLE property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getDoublePropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getDouble(propertyName);
        }
        
        // Get DOUBLE ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getDoubleArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getDoubleArray(propertyName);
        }
        
        // Get TAG property names and check if given propertyName exists in the
        // list.
        propertySet = propertyMap.getTagPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getTag(propertyName);
        }
        
        // Get TAG ARRAY property names and check if given propertyName exists
        // in the list.
        propertySet = propertyMap.getTagArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propertyValue = propertyMap.getTagArray(propertyName);
        }
        
        return propertyValue;
    }
    
    public static String handleNull(Object propertyValue)
    {
        String returnValue = "";
        if (propertyValue != null)
        {
            
            returnValue = propertyValue.toString();
            
        }
        return returnValue;
    }
    
    public static String[] handleNull(Object[] propertyValue)
    {
        String[] returnValue = new String[propertyValue.length];
        
        for (int i = 0; i < propertyValue.length; ++i)
        {
            returnValue[i] = propertyValue[i].toString();
        }
        
        return returnValue;
    }
    
    public static String[] handleNullForArray(Object[] propertyValue)
    {
        String[] returnValue = new String[] { "" };
        if (propertyValue != null && propertyValue.length > 0)
        {
            returnValue = new String[propertyValue.length];
            
            for (int i = 0; i < propertyValue.length; ++i)
            {
                returnValue[i] = propertyValue[i].toString();
            }
            
        }
        
        return returnValue;
    }
    
    public static boolean isValidArgument(Object object)
    {
        if (null == object)
        {
            return false;
        }
        if (object instanceof String)
        {
            String new_String = (String) object;
            if (new_String.isEmpty())
            {
                return false;
            }
        }
        return true;
    }
    
    public static void setString(String prop_name, String prop_value,
            IPropertyMap propertyMap)
    {
        if (isValidArgument(prop_value))
        {
            propertyMap.setString(prop_name, prop_value);
        }
    }
    
    public static void setDouble(String prop_name, String prop_value, IPropertyMap propertyMap)
    {
        if (isValidArgument(prop_value))
        {
            Double doubleValue = Double.parseDouble(prop_value);
            propertyMap.setDouble(prop_name, doubleValue);
        }
        else
        {
        	Double defaultDouble = 0.0;
        	propertyMap.setDouble(prop_name, defaultDouble);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static boolean hasContent(Map map)
    {
        if (map.isEmpty())
            return false;
        return true;
    }
    
    @SuppressWarnings("unchecked")
    public static boolean isEmptyMaps(Collection<Map> maps)
    {
        boolean value = true;
        for (Map map : maps)
        {
            if (hasContent(map))
            {
                value = false;
                break;
            }
        }
        return value;
    }
    
    private static Boolean isArray(IPropertyMap propertyMap, String relationName)
    {
        Boolean isArray = null;
        Set<String> tagPropNames = propertyMap.getTagPropNames();
        Set<String> tagArrayPropNames = propertyMap.getTagArrayPropNames();
        relationName = relationName.trim();
        if (tagPropNames.contains(relationName))
        {
            isArray = false;
        }
        if (tagArrayPropNames.contains(relationName))
        {
            isArray = true;
        }
        return isArray;
        
    }
    
    public static boolean createCompoundPropMap(IPropertyMap propertyMap,
            String relationName) throws TCException
    {
        
        Object boType = propertyMap.getType();
        
        Boolean isArray = isArray(propertyMap, relationName);
        // ask about this check!
        if (isArray == null)
        {
            return false;
        }
        if (isArray)
        {
            TCComponent[] tCComponent = propertyMap.getTagArray(relationName);
            if (tCComponent != null)
            {
                // IPropertyMap[] compoundPropMaps = new
                // SimplePropertyMap[tCComponent.length];
                IPropertyMap[] compoundPropMaps = new IPropertyMap[tCComponent.length];
                for (int i = 0; i < tCComponent.length; i++)
                {
                    // compoundPropMaps[i] = new SimplePropertyMap();
                    compoundPropMaps[i] = propertyMap.createCompound(boType);
                    compoundPropMaps[i].setComponent(tCComponent[i]);
                }
                propertyMap.setCompoundArray(relationName, compoundPropMaps);
            }
            
        }
        else
        {
            
            TCComponent tCComponent = propertyMap.getTag(relationName);
            if (tCComponent != null)
            {
                // IPropertyMap compoundPropMap = new SimplePropertyMap();
                IPropertyMap compoundPropMap = propertyMap
                        .createCompound(boType);
                compoundPropMap.setComponent(tCComponent);
                propertyMap.setCompound(relationName, compoundPropMap);
            }
            
        }
        return true;
    }
    
    // public static void addCompound(String typeConstant, String
    // compoundRelation, IPropertyMap propertyMap)
    // {
    // String itemType = propertyMap.getType();
    // String compoundPropertyType =
    // ItemTypeHelper.getTypeConstantValue(itemType, typeConstant);
    // IPropertyMap compoundPropertyMap =
    // propertyMap.createCompound(compoundPropertyType);
    // propertyMap.setCompound(compoundRelation, compoundPropertyMap);
    // }
    
    /**
     * This function find whether the given property of given name is present in
     * the propery map or not. This also provied prop type and isArray value
     * 
     * @param propertyName
     *            : In
     * @param propertyMap
     *            : In
     * @param propType
     *            : Out param
     * @param isArray
     *            : Out param
     * @return boolean
     */
    
    public static boolean findProperty(String propertyName,
            IPropertyMap propertyMap, Integer propType, Boolean isArray)
    {
        // Get STRING property names and check if given propertyName exists in
        // the list.
        Set<String> propertySet = propertyMap.getStringPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_string;
            isArray = false;
            return true;
        }
        
        // Get STRING ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getStringArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_string;
            isArray = true;
            return true;
        }
        
        // Get LOGICAL property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getLogicalPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_logical;
            isArray = false;
            return true;
        }
        
        // Get LOGICAL ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getLogicalArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_logical;
            isArray = true;
            return true;
        }
        
        // Get FLOAT property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getFloatPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_float;
            isArray = false;
            return true;
        }
        
        // Get FLOAT ARRAY property names and check if given propertyName exists
        // in the list.
        propertySet = propertyMap.getFloatArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_float;
            isArray = true;
            return true;
        }
        
        // Get INTEGER property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getIntegerPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_int;
            isArray = false;
            return true;
        }
        
        // Get INTEGER ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getIntegerArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_int;
            isArray = true;
            return true;
        }
        
        // Get DATE property names and check if given propertyName exists in the
        // list.
        propertySet = propertyMap.getDatePropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_date;
            isArray = false;
            return true;
        }
        
        // Get DATE ARRAY property names and check if given propertyName exists
        // in the list.
        propertySet = propertyMap.getDateArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_date;
            isArray = true;
            return true;
        }
        
        // Get DOUBLE property names and check if given propertyName exists in
        // the list.
        propertySet = propertyMap.getDoublePropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_double;
            isArray = false;
            return true;
        }
        
        // Get DOUBLE ARRAY property names and check if given propertyName
        // exists in the list.
        propertySet = propertyMap.getDoubleArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_double;
            isArray = true;
            return true;
        }
        
        // Get TAG property names and check if given propertyName exists in the
        // list.
        propertySet = propertyMap.getTagPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_typed_reference;
            isArray = false;
            return true;
        }
        
        // Get TAG ARRAY property names and check if given propertyName exists
        // in the list.
        propertySet = propertyMap.getTagArrayPropNames();
        if (propertySet.contains(propertyName))
        {
            propType = TCProperty.PROP_typed_reference;
            isArray = true;
            return true;
        }
        
        return false;
    }
    
    public static Image getTypeImage(String propertyName, IPropertyMap propMap)
    {
        Image image = null;
        TCComponent propValue = propMap.getTag(propertyName);
        if (propValue != null)
        {
            image = TCTypeRenderer.getTypeImage(propValue.getTypeComponent(),
                    null);
        }
        
        return image;
    }
    
}
