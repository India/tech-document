package com.nov.rac.propertymap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.teamcenter.rac.kernel.TCComponent;

/**
 * The SimplePropertyMap can be used for data exchange purpose (data exchange
 * that does not involve creation of objects).
 */
public class SimplePropertyMap implements IPropertyMap
{
    public SimplePropertyMap()
    {
    }
    
    public SimplePropertyMap(String strBOTypeName)
    {
        m_boType = strBOTypeName;
    }
    
    public SimplePropertyMap(IPropertyMap propMap)
    {
        addAll(propMap);
    }
    
    public void setString(String prop_name, String prop_value)
    {
        m_stringPropsMap.put(prop_name, prop_value);
    }
    
    public String getString(String prop_name)
    {
        String propValue = m_stringPropsMap.get(prop_name);
        return propValue;
    }
    
    public void setStringArray(String prop_name, String[] prop_value)
    {
        m_stringArrayPropsMap.put(prop_name, prop_value);
    }
    
    public String[] getStringArray(String prop_name)
    {
        String[] propValueArr = m_stringArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setInteger(String prop_name, Integer prop_value)
    {
        m_intPropsMap.put(prop_name, prop_value);
    }
    
    public Integer getInteger(String prop_name)
    {
        Integer propValue = m_intPropsMap.get(prop_name);
        return propValue;
    }
    
    public void setIntegerArray(String prop_name, Integer[] prop_value)
    {
        m_intArrayPropsMap.put(prop_name, prop_value);
    }
    
    public Integer[] getIntegerArray(String prop_name)
    {
        Integer[] propValueArr = m_intArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setDouble(String prop_name, Double prop_value)
    {
        m_doublePropsMap.put(prop_name, prop_value);
    }
    
    public Double getDouble(String prop_name)
    {
        Double propValue = m_doublePropsMap.get(prop_name);
        return propValue;
    }
    
    public void setDoubleArray(String prop_name, Double[] prop_value)
    {
        m_doubleArrayPropsMap.put(prop_name, prop_value);
    }
    
    public Double[] getDoubleArray(String prop_name)
    {
        Double[] propValueArr = m_doubleArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setFloat(String prop_name, Float prop_value)
    {
        m_floatPropsMap.put(prop_name, prop_value);
    }
    
    public Float getFloat(String prop_name)
    {
        Float propValue = m_floatPropsMap.get(prop_name);
        return propValue;
    }
    
    public void setFloatArray(String prop_name, Float[] prop_value)
    {
        m_floatArrayPropsMap.put(prop_name, prop_value);
    }
    
    public Float[] getFloatArray(String prop_name)
    {
        Float[] propValueArr = m_floatArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setLogical(String prop_name, Boolean prop_value)
    {
        m_boolPropsMap.put(prop_name, prop_value);
    }
    
    public Boolean getLogical(String prop_name)
    {
        Boolean propValue = m_boolPropsMap.get(prop_name);
        return propValue;
    }
    
    public void setLogicalArray(String prop_name, Boolean[] prop_value)
    {
        m_boolArrayPropsMap.put(prop_name, prop_value);
    }
    
    public Boolean[] getLogicalArray(String prop_name)
    {
        Boolean[] propValueArr = m_boolArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setTag(String prop_name, TCComponent prop_value)
    {
        m_tagPropsMap.put(prop_name, prop_value);
    }
    
    public TCComponent getTag(String prop_name)
    {
        TCComponent propValue = m_tagPropsMap.get(prop_name);
        return propValue;
    }
    
    public void setTagArray(String prop_name, TCComponent[] prop_value)
    {
        m_tagArrayPropsMap.put(prop_name, prop_value);
    }
    
    public TCComponent[] getTagArray(String prop_name)
    {
        TCComponent[] propValueArr = m_tagArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setDate(String prop_name, Date prop_value)
    {
        m_datePropsMap.put(prop_name, prop_value);
    }
    
    public Date getDate(String prop_name)
    {
        Date propValue = m_datePropsMap.get(prop_name);
        return propValue;
    }
    
    public void setDateArray(String prop_name, Date[] prop_value)
    {
        m_dateArrayPropsMap.put(prop_name, prop_value);
    }
    
    public Date[] getDateArray(String prop_name)
    {
        Date[] propValueArr = m_dateArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public void setCompound(String prop_name, IPropertyMap prop_value)
    {
        m_compoundPropsMap.put(prop_name, prop_value);
    }
    
    public IPropertyMap getCompound(String prop_name)
    {
        IPropertyMap propValue = m_compoundPropsMap.get(prop_name);
        return propValue;
    }
    
    public void setCompoundArray(String prop_name, IPropertyMap[] prop_value)
    {
        m_compoundArrayPropsMap.put(prop_name, prop_value);
    }
    
    public IPropertyMap[] getCompoundArray(String prop_name)
    {
        IPropertyMap[] propValueArr = m_compoundArrayPropsMap.get(prop_name);
        return propValueArr;
    }
    
    public Set<String> getCompoundArrayPropNames()
    {
        Set<String> keysSet = m_compoundArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getCompoundPropNames()
    {
        Set<String> keysSet = m_compoundPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getDateArrayPropNames()
    {
        Set<String> keysSet = m_dateArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getDatePropNames()
    {
        Set<String> keysSet = m_datePropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getDoubleArrayPropNames()
    {
        Set<String> keysSet = m_doubleArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getDoublePropNames()
    {
        Set<String> keysSet = m_doublePropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getFloatArrayPropNames()
    {
        Set<String> keysSet = m_floatArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getFloatPropNames()
    {
        Set<String> keysSet = m_floatPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getIntegerArrayPropNames()
    {
        Set<String> keysSet = m_intArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getIntegerPropNames()
    {
        Set<String> keysSet = m_intPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getLogicalArrayPropNames()
    {
        Set<String> keysSet = m_boolArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getLogicalPropNames()
    {
        Set<String> keysSet = m_boolPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getStringArrayPropNames()
    {
        Set<String> keysSet = m_stringArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getStringPropNames()
    {
        Set<String> keysSet = m_stringPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getTagArrayPropNames()
    {
        Set<String> keysSet = m_tagArrayPropsMap.keySet();
        return keysSet;
    }
    
    public Set<String> getTagPropNames()
    {
        Set<String> keysSet = m_tagPropsMap.keySet();
        return keysSet;
    }
    
    @Override
    public IPropertyMap createCompound(Object args)
    {
        return new SimplePropertyMap();
    }
    
    public void addAll(IPropertyMap propMap)
    {
        if (propMap == null)
        {
            return;
        }
        
        SimplePropertyMap simplePropMap = this;
        
        Set<String> stringKeys = propMap.getStringPropNames();
        Iterator<String> itr = stringKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            String value = propMap.getString(key);
            simplePropMap.m_stringPropsMap.put(key, value);
        }
        
        Set<String> stringArrKeys = propMap.getStringArrayPropNames();
        itr = stringArrKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            String[] value = propMap.getStringArray(key);
            simplePropMap.m_stringArrayPropsMap.put(key, value);
        }
        
        Set<String> integerKeys = propMap.getIntegerPropNames();
        itr = integerKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            int value = propMap.getInteger(key).intValue();
            simplePropMap.m_intPropsMap.put(key, value);
        }
        
        Set<String> integerArrayKeys = propMap.getIntegerArrayPropNames();
        itr = integerArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Integer[] value = propMap.getIntegerArray(key);
            simplePropMap.m_intArrayPropsMap.put(key, value);
        }
        
        Set<String> doubleKeys = propMap.getDoublePropNames();
        itr = doubleKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Double value = propMap.getDouble(key);
            simplePropMap.m_doublePropsMap.put(key, value);
        }
        
        Set<String> doubleArrayKeys = propMap.getDoubleArrayPropNames();
        itr = doubleArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Double[] value = propMap.getDoubleArray(key);
            simplePropMap.m_doubleArrayPropsMap.put(key, value);
        }
        
        Set<String> floatKeys = propMap.getFloatPropNames();
        itr = floatKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Float value = propMap.getFloat(key);
            simplePropMap.m_floatPropsMap.put(key, value);
        }
        
        Set<String> floatArrayKeys = propMap.getFloatArrayPropNames();
        itr = floatArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Float[] value = propMap.getFloatArray(key);
            simplePropMap.m_floatArrayPropsMap.put(key, value);
        }
        
        Set<String> logicalKeys = propMap.getLogicalPropNames();
        itr = logicalKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Boolean value = propMap.getLogical(key);
            simplePropMap.m_boolPropsMap.put(key, value);
        }
        
        Set<String> logicalArrayKeys = propMap.getLogicalArrayPropNames();
        itr = logicalArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Boolean[] value = propMap.getLogicalArray(key);
            simplePropMap.m_boolArrayPropsMap.put(key, value);
        }
        
        Set<String> tagKeys = propMap.getTagPropNames();
        itr = tagKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            TCComponent value = propMap.getTag(key);
            simplePropMap.m_tagPropsMap.put(key, value);
        }
        
        Set<String> tagArrayKeys = propMap.getTagArrayPropNames();
        itr = tagArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            TCComponent[] value = propMap.getTagArray(key);
            simplePropMap.m_tagArrayPropsMap.put(key, value);
        }
        
        Set<String> compoundKeys = propMap.getCompoundPropNames();
        itr = compoundKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            IPropertyMap value = propMap.getCompound(key);
            simplePropMap.m_compoundPropsMap.put(key, new SimplePropertyMap(value));
        }
        
        Set<String> compoundArrayKeys = propMap.getCompoundArrayPropNames();
        itr = compoundArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            IPropertyMap[] value = propMap.getCompoundArray(key);
            SimplePropertyMap[] simpleValue = new SimplePropertyMap[value.length];
            
            for (int index = 0; index < value.length; ++index)
            {
                simpleValue[index] = new SimplePropertyMap(value[index]);
            }
            simplePropMap.m_compoundArrayPropsMap.put(key, simpleValue);
        }
    }
    
    @Override
    public String getType()
    { 	
    	TCComponent theComponent = getComponent();
    	
    	if(theComponent != null)
    	{
    		m_boType = theComponent.getType();
    	}
    	
    	return m_boType;
    }

    @Override
    public TCComponent getComponent()
    {
        return m_TCComponent;
    }

    @Override
    public void setComponent(TCComponent component)
    {
        m_TCComponent = component;        
    }
    
    /**
     * String Properties Map.
     */
    private java.util.Map<String, String> m_stringPropsMap = new java.util.HashMap<String, String>();
    
    /**
     * String Array Props Map.
     */
    private java.util.Map<String, String[]> m_stringArrayPropsMap = new java.util.HashMap<String, String[]>();
    
    /**
     * Double Properties Map.
     */
    private java.util.Map<String, Double> m_doublePropsMap = new java.util.HashMap<String, Double>();
    
    /**
     * Double Array Properties Map.
     */
    private java.util.Map<String, Double[]> m_doubleArrayPropsMap = new java.util.HashMap<String, Double[]>();
    
    /**
     * Float Properties Map.
     */
    private java.util.Map<String, Float> m_floatPropsMap = new java.util.HashMap<String, Float>();
    
    /**
     * Float Array Properties Map.
     */
    private java.util.Map<String, Float[]> m_floatArrayPropsMap = new java.util.HashMap<String, Float[]>();
    
    /**
     * Integer Properties Map.
     */
    private java.util.Map<String, Integer> m_intPropsMap = new java.util.HashMap<String, Integer>();
    
    /**
     * Integer Array Properties Map.
     */
    private java.util.Map<String, Integer[]> m_intArrayPropsMap = new java.util.HashMap<String, Integer[]>();
    
    /**
     * Boolean Properties Map.
     */
    private java.util.Map<String, Boolean> m_boolPropsMap = new java.util.HashMap<String, Boolean>();
    
    /**
     * Boolean Array Props Map.
     */
    private java.util.Map<String, Boolean[]> m_boolArrayPropsMap = new java.util.HashMap<String, Boolean[]>();
    
    /**
     * Date Properties Map.
     */
    private java.util.Map<String, Date> m_datePropsMap = new java.util.HashMap<String, Date>();
    
    /**
     * Date Array Properties Map.
     */
    private java.util.Map<String, Date[]> m_dateArrayPropsMap = new java.util.HashMap<String, Date[]>();
    
    /**
     * Tag Properties Map.
     */
    private java.util.Map<String, TCComponent> m_tagPropsMap = new java.util.HashMap<String, TCComponent>();
    
    /**
     * Tag Array Properties Map.
     */
    private java.util.Map<String, TCComponent[]> m_tagArrayPropsMap = new java.util.HashMap<String, TCComponent[]>();
    
    /**
     * Compound Properties Map.
     */
    private java.util.Map<String, IPropertyMap> m_compoundPropsMap = new java.util.HashMap<String, IPropertyMap>();
    
    /**
     * Compound Properties Map.
     */
    private java.util.Map<String, IPropertyMap[]> m_compoundArrayPropsMap = new java.util.HashMap<String, IPropertyMap[]>();
    
    private TCComponent m_TCComponent = null;
    
    private String m_boType = null;
    
    @Override
    public boolean isEmpty()
    {
        boolean value = true;
        Collection<Map> nonCompoundMaps = new ArrayList<Map>();
        
        nonCompoundMaps.add(m_tagPropsMap);
        nonCompoundMaps.add(m_tagArrayPropsMap);
        
        nonCompoundMaps.add(m_stringPropsMap);
        nonCompoundMaps.add(m_stringArrayPropsMap);
        
        nonCompoundMaps.add(m_doublePropsMap);
        nonCompoundMaps.add(m_doubleArrayPropsMap);
        
        nonCompoundMaps.add(m_floatPropsMap);
        nonCompoundMaps.add(m_floatArrayPropsMap);
        
        nonCompoundMaps.add(m_intPropsMap);
        nonCompoundMaps.add(m_intArrayPropsMap);
        
        nonCompoundMaps.add(m_boolPropsMap);
        nonCompoundMaps.add(m_boolArrayPropsMap);
        
        nonCompoundMaps.add(m_datePropsMap);
        nonCompoundMaps.add(m_dateArrayPropsMap);
        
        if (isEmptyNonCompoundMaps(nonCompoundMaps))
        {
            if (PropertyMapHelper.hasContent(m_compoundPropsMap))
            {
                for (IPropertyMap propertyMap : m_compoundPropsMap.values())
                {
                    if (!propertyMap.isEmpty())
                    {
                        value = false;
                        break;
                    }
                    ;
                }
            }
            
            else if (PropertyMapHelper.hasContent(m_compoundArrayPropsMap))
            {
                for (IPropertyMap[] propertyMaps : m_compoundArrayPropsMap.values())
                {
                    for (IPropertyMap propertyMap : propertyMaps)
                    {
                        if (!propertyMap.isEmpty())
                        {
                            value = false;
                            break;
                        }
                        ;
                    }
                }
            }
        }
        else
        {
            value = false;
        }
        
        return value;
        
    }
    
    private boolean isEmptyNonCompoundMaps(Collection<Map> nonCompoundMaps)
    {
        return PropertyMapHelper.isEmptyMaps(nonCompoundMaps);
    }

    @Override
    public void setType(String type)
    {
        m_boType = type;
        
    }
    
}
