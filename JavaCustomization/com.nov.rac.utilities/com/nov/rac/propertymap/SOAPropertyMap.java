package com.nov.rac.propertymap;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.nov4.services.rac.custom._2010_09.NOV4DataManagement.PropertyMap;
import com.teamcenter.rac.kernel.TCComponent;

public class SOAPropertyMap extends PropertyMap implements IPropertyMap
{
    private PropertyMap m_propertyMap = null;
    
    public SOAPropertyMap()
    {
        m_propertyMap = this;
    }
    
    public SOAPropertyMap(String boName)
    {
        this();
        m_propertyMap.boName = boName;
    }
    
    public SOAPropertyMap(IPropertyMap propMap)
    {
        this();
        
        m_propertyMap.boName = propMap.getType();
        m_propertyMap.targetObject = propMap.getComponent();
        
        addAll(propMap);
    }
    
    @Override
    public void setString(String prop_name, String prop_value)
    {       
        m_propertyMap.stringProps.put(prop_name, prop_value);
    }
    
    @Override
    public String getString(String prop_name)
    {
        String propValue = (String) m_propertyMap.stringProps.get(prop_name);
        return propValue;
    }
    
    @Override
    public Set<String> getStringPropNames()
    {
        Set<String> keysSet = m_propertyMap.stringProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setStringArray(String prop_name, String[] prop_value)
    {
        m_propertyMap.stringArrayProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public String[] getStringArray(String prop_name)
    {
        String[] propValueArr =  (String[]) m_propertyMap.stringArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getStringArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.stringArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setInteger(String prop_name, Integer prop_value)
    {
        m_propertyMap.intProps.put(prop_name, new BigInteger(prop_value.toString()));
        
    }
    
    @Override
    public Integer getInteger(String prop_name)
    {
        Integer propValue = Integer.parseInt( m_propertyMap.intProps.get(prop_name).toString());
        return propValue;
    }
    
    @Override
    public Set<String> getIntegerPropNames()
    {
        Set<String> keysSet = m_propertyMap.intProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setIntegerArray(String prop_name, Integer[] prop_value)
    {
        
        BigInteger[] valueObj = new BigInteger[prop_value.length];
        
        for (int iCount = 0; iCount < prop_value.length; iCount++)
        {
            Integer intObject = new Integer(prop_value[iCount]);
            valueObj[iCount] = new BigInteger(intObject.toString());
        }
        
        m_propertyMap.intArrayProps.put(prop_name, valueObj);
        
    }
    
    @Override
    public Integer[] getIntegerArray(String prop_name)
    {
        Integer[] propValueArr = (Integer[]) m_propertyMap.intArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getIntegerArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.intArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setDouble(String prop_name, Double prop_value)
    {
        m_propertyMap.doubleProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public Double getDouble(String prop_name)
    {
        Double propValue = (Double) m_propertyMap.doubleProps.get(prop_name);
        return propValue;
    }
    
    @Override
    public Set<String> getDoublePropNames()
    {
        Set<String> keysSet = m_propertyMap.doubleProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setDoubleArray(String prop_name, Double[] prop_value)
    {
        m_propertyMap.doubleArrayProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public Double[] getDoubleArray(String prop_name)
    {
        Double[] propValueArr = (Double[]) m_propertyMap.doubleArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getDoubleArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.doubleArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setFloat(String prop_name, Float prop_value)
    {
        m_propertyMap.floatProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public Float getFloat(String prop_name)
    {
        Float propValue = (Float) m_propertyMap.floatProps.get(prop_name);
        return propValue;
    }
    
    @Override
    public Set<String> getFloatPropNames()
    {
        Set<String> keysSet = m_propertyMap.floatProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setFloatArray(String prop_name, Float[] prop_value)
    {
        m_propertyMap.floatArrayProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public Float[] getFloatArray(String prop_name)
    {
        Float[] propValueArr = (Float[]) m_propertyMap.floatArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getFloatArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.floatArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setLogical(String prop_name, Boolean prop_value)
    {
        m_propertyMap.boolProps.put(prop_name, booleanToBigInteger(prop_value));
        
    }
    
    @Override
    public Boolean getLogical(String prop_name)
    {
        BigInteger propValue = (BigInteger) m_propertyMap.boolProps.get(prop_name);
        return bigIntegerToBoolean(propValue);
    }
    
    @Override
    public Set<String> getLogicalPropNames()
    {
        Set<String> keysSet = m_propertyMap.boolProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setLogicalArray(String prop_name, Boolean[] prop_value)
    {
        m_propertyMap.boolArrayProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public Boolean[] getLogicalArray(String prop_name)
    {
        Boolean[] propValueArr = (Boolean[]) m_propertyMap.boolArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getLogicalArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.boolArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setTag(String prop_name, TCComponent prop_value)
    {
        m_propertyMap.tagProps.put(prop_name, prop_value);
        
    }
    
    @Override
    public TCComponent getTag(String prop_name)
    {
        TCComponent propValue = (TCComponent) m_propertyMap.tagProps.get(prop_name);
        return propValue;
    }
    
    @Override
    public Set<String> getTagPropNames()
    {
        Set<String> keysSet = m_propertyMap.tagProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setTagArray(String prop_name, TCComponent[] prop_value)
    {
        m_propertyMap.tagArrayProps.put(prop_name, prop_value);
    }
    
    @Override
    public TCComponent[] getTagArray(String prop_name)
    {
        TCComponent[] propValueArr = (TCComponent[]) m_propertyMap.tagArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getTagArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.tagArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setDate(String prop_name, Date prop_value)
    {
        m_propertyMap.dateProps.put(prop_name, prop_value);
    }
    
    @Override
    public Date getDate(String prop_name)
    {
        Date propValue = (Date) m_propertyMap.dateProps.get(prop_name);
        return propValue;
    }
    
    @Override
    public Set<String> getDatePropNames()
    {
        Set<String> keysSet = m_propertyMap.dateProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setDateArray(String prop_name, Date[] prop_value)
    {
        m_propertyMap.dateArrayProps.put(prop_name, prop_value);
    }
    
    @Override
    public Date[] getDateArray(String prop_name)
    {
        Date[] propValueArr = (Date[]) m_propertyMap.dateArrayProps.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getDateArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.dateArrayProps.keySet();
        return keysSet;
    }
    
    @Override
    public void setCompound(String prop_name, IPropertyMap prop_value)
    {
        m_propertyMap.compoundPropInput.put(prop_name, new IPropertyMap[] {prop_value} );
    }
    
    @Override
    public IPropertyMap getCompound(String prop_name)
    {
        IPropertyMap propValue = null;
        
        IPropertyMap[] allPropValues = (IPropertyMap[]) m_propertyMap.compoundPropInput.get(prop_name);
        
        if(allPropValues != null && allPropValues.length > 0)
        {
            propValue = allPropValues[0]; 
        }
        
        return propValue;
    }
    
    @Override
    public Set<String> getCompoundPropNames()
    {
        Set<String> keysSet = m_propertyMap.compoundPropInput.keySet();
        return keysSet;
    }
    
    @Override
    public void setCompoundArray(String prop_name, IPropertyMap[] prop_value)
    {
        m_propertyMap.compoundPropInput.put(prop_name, prop_value);
    }
    
    @Override
    public IPropertyMap[] getCompoundArray(String prop_name)
    {
        IPropertyMap[] propValueArr = (IPropertyMap[]) m_propertyMap.compoundPropInput.get(prop_name);
        return propValueArr;
    }
    
    @Override
    public Set<String> getCompoundArrayPropNames()
    {
        Set<String> keysSet = m_propertyMap.compoundPropInput.keySet();
        return keysSet;
    }
    
    @Override
    public IPropertyMap createCompound(Object args)
    {
        SOAPropertyMap compoundObject = null;
        
        if (args != null && args instanceof String)
        {
            String boName = (String) args;
            
            compoundObject = new SOAPropertyMap(boName);
        }
        
        return compoundObject;
    }
    
    @Override
    public void addAll(IPropertyMap propMap)
    {
        if (propMap == null)
        {
            return;
        }
        
        SOAPropertyMap SOAPropMap = this;
        
        String type = propMap.getType();
        SOAPropMap.setType(type);
        
        Set<String> stringKeys = propMap.getStringPropNames();
        Iterator<String> itr = stringKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            String value = propMap.getString(key);
            SOAPropMap.setString(key, value);
        }
        
        Set<String> stringArrKeys = propMap.getStringArrayPropNames();
        itr = stringArrKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            String[] value = propMap.getStringArray(key);
            SOAPropMap.setStringArray(key, value);
        }
        
        Set<String> integerKeys = propMap.getIntegerPropNames();
        itr = integerKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            int value = propMap.getInteger(key).intValue();
            SOAPropMap.setInteger(key, value);
        }
        
        Set<String> integerArrayKeys = propMap.getIntegerArrayPropNames();
        itr = integerArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Integer[] value = propMap.getIntegerArray(key);
            SOAPropMap.setIntegerArray(key, value);
        }
        
        Set<String> doubleKeys = propMap.getDoublePropNames();
        itr = doubleKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Double value = propMap.getDouble(key);
            SOAPropMap.setDouble(key, value);
        }
        
        Set<String> doubleArrayKeys = propMap.getDoubleArrayPropNames();
        itr = doubleArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Double[] value = propMap.getDoubleArray(key);
            SOAPropMap.setDoubleArray(key, value);
        }
        
        Set<String> floatKeys = propMap.getFloatPropNames();
        itr = floatKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Float value = propMap.getFloat(key);
            SOAPropMap.setFloat(key, value);
        }
        
        Set<String> floatArrayKeys = propMap.getFloatArrayPropNames();
        itr = floatArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Float[] value = propMap.getFloatArray(key);
            SOAPropMap.setFloatArray(key, value);
        }
        
        Set<String> logicalKeys = propMap.getLogicalPropNames();
        itr = logicalKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Boolean value = propMap.getLogical(key);
            SOAPropMap.setLogical(key, value);
        }
        
        Set<String> logicalArrayKeys = propMap.getLogicalArrayPropNames();
        itr = logicalArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            Boolean[] value = propMap.getLogicalArray(key);
            SOAPropMap.setLogicalArray(key, value);
        }
        
        Set<String> tagKeys = propMap.getTagPropNames();
        itr = tagKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            TCComponent value = propMap.getTag(key);
            SOAPropMap.setTag(key, value);
        }
        
        Set<String> tagArrayKeys = propMap.getTagArrayPropNames();
        itr = tagArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            TCComponent[] value = propMap.getTagArray(key);
            SOAPropMap.setTagArray(key, value);
        }
        
//        Set<String> compoundKeys = propMap.getCompoundPropNames();
//        itr = compoundKeys.iterator();
//        while (itr.hasNext())
//        {
//            String key = itr.next();
//            IPropertyMap value = propMap.getCompound(key);
//            SOAPropMap.compoundPropInput.put(key, new SOAPropertyMap(value));
//        }
        
        Set<String> compoundArrayKeys = propMap.getCompoundArrayPropNames();
        itr = compoundArrayKeys.iterator();
        while (itr.hasNext())
        {
            String key = itr.next();
            IPropertyMap[] inputValue = propMap.getCompoundArray(key);
            IPropertyMap[] thisValue =  this.getCompoundArray(key);
            
            //Case 1. When inputValue has a map, but thisValue does not have a map
            // Then add all maps of inputValue into this Value.
            if( (thisValue == null || thisValue.length == 0) && (inputValue != null && inputValue.length > 0))
            {
                thisValue =  new SOAPropertyMap[inputValue.length];
                
                for(int inx=0; inx<inputValue.length ; inx++)
                {
                    if(inputValue[inx] != null)
                        thisValue[inx] = new SOAPropertyMap(inputValue[inx]);
                }
            }          
            else if( (thisValue != null &&  inputValue != null && thisValue.length == inputValue.length ))
            {
                //Case 2. When inputValue & thisValue both have equal number of maps then
                // Add inputValues into corresponding thisValue maps.
                
                for(int inx=0; inx<inputValue.length ; inx++)
                {
                    if(thisValue[inx] != null && inputValue[inx] != null)
                    {
                        thisValue[inx].addAll( inputValue[inx] );
                    }
                }
                
            }
            if(thisValue != null && thisValue.length > 0)
            {
                SOAPropMap.setCompoundArray(key, thisValue);    
            }
            
           /* String key = itr.next();
            IPropertyMap[] value = propMap.getCompoundArray(key);
            SOAPropertyMap[] simpleValue = new SOAPropertyMap[value.length];
            
            for (int index = 0; index < value.length; ++index)
            {
                if(value[index] != null)
                    simpleValue[index] = new SOAPropertyMap(value[index]);
           }
            
            SOAPropMap.setCompoundArray(key, simpleValue);*/
        }
        
    }
    
    @Override
    public String getType()
    {
        String boType = m_propertyMap.boName;
        TCComponent theComponent = getComponent();
        
        if(theComponent != null)
        {
            boType = theComponent.getType();
        }
        
        return boType;
    }
    
    @Override
    public TCComponent getComponent()
    {
        return m_propertyMap.targetObject;
    }
    
    @Override
    public void setComponent(TCComponent component)
    {
        m_propertyMap.targetObject = component;     
    }
    
    @Override
    public boolean isEmpty()
    {

        boolean value = true;
        Collection<Map> nonCompoundMaps = new ArrayList<Map>();
        
        nonCompoundMaps.add(m_propertyMap.tagProps);
        nonCompoundMaps.add(m_propertyMap.tagArrayProps);
        
        nonCompoundMaps.add(m_propertyMap.stringProps);
        nonCompoundMaps.add(m_propertyMap.stringArrayProps);
        
        nonCompoundMaps.add(m_propertyMap.doubleProps);
        nonCompoundMaps.add(m_propertyMap.doubleArrayProps);
        
        nonCompoundMaps.add(m_propertyMap.floatProps);
        nonCompoundMaps.add(m_propertyMap.floatArrayProps);
        
        nonCompoundMaps.add(m_propertyMap.intProps);
        nonCompoundMaps.add(m_propertyMap.intArrayProps);
        
        nonCompoundMaps.add(m_propertyMap.boolProps);
        nonCompoundMaps.add(m_propertyMap.boolArrayProps);
        
        nonCompoundMaps.add(m_propertyMap.dateProps);
        nonCompoundMaps.add(m_propertyMap.dateArrayProps);
        
        if (isEmptyNonCompoundMaps(nonCompoundMaps))
        {
            if (PropertyMapHelper.hasContent(m_propertyMap.compoundPropInput))
            {
                Map<String,IPropertyMap> compoundPropMap =  m_propertyMap.compoundPropInput;
                for (IPropertyMap propertyMap : compoundPropMap.values())
                {
                    if (!propertyMap.isEmpty())
                    {
                        value = false;
                        break;
                    }
                    ;
                }
            }
            
            else if (PropertyMapHelper.hasContent(m_propertyMap.compoundPropInput))
            {
                Map<String,IPropertyMap[]> compoundPropMap =  m_propertyMap.compoundPropInput;
                for (IPropertyMap[] propertyMaps : compoundPropMap.values())
                {
                    for (IPropertyMap propertyMap : propertyMaps)
                    {
                        if (!propertyMap.isEmpty())
                        {
                            value = false;
                            break;
                        }
                        ;
                    }
                }
            }
        }
        else
        {
            value = false;
        }
        
        return value;
        
    
    }
    
    private boolean isEmptyNonCompoundMaps(Collection<Map> nonCompoundMaps)
    {
        return PropertyMapHelper.isEmptyMaps(nonCompoundMaps);
    }

    @Override
    public void setType(String type)
    {
        m_propertyMap.boName = type;
    }
    
    /**
     * Convert Boolean to BigInteger.
     * 
     * @param value
     *            the value
     * @return the big integer
     */
    private BigInteger booleanToBigInteger(Boolean value)
    {
        if (value)
        {
            return BigInteger.ONE;
        }
        return BigInteger.ZERO;
    }
    
    /**
     * Convert BigInteger to Boolean.
     * 
     * @param bigInteger
     *            the big integer
     * @return the boolean
     */
    private Boolean bigIntegerToBoolean(BigInteger bigInteger)
    {
        if (bigInteger == null || BigInteger.ZERO.equals(bigInteger))
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    

}
