package com.nov.rac.propertymap;

import java.util.Date;
import java.util.Set;

import com.teamcenter.rac.kernel.TCComponent;

public interface IPropertyMap
{
    public void setString( String prop_name, String prop_value );
    public String getString( String prop_name );
    public Set<String> getStringPropNames(); 
    
    public void setStringArray( String prop_name, String[] prop_value );
    public String[] getStringArray( String prop_name );
    public Set<String> getStringArrayPropNames(); 
    
    public void setInteger( String prop_name, Integer prop_value );
    public Integer getInteger( String prop_name );
    public Set<String> getIntegerPropNames(); 
    
    public void setIntegerArray( String prop_name, Integer[] prop_valueArr );
    public Integer[] getIntegerArray( String prop_name );
    public Set<String> getIntegerArrayPropNames(); 
    
    public void setDouble( String prop_name, Double prop_value );
    public Double getDouble( String prop_name );
    public Set<String> getDoublePropNames(); 
    
    public void setDoubleArray( String prop_name, Double[] prop_valueArr );
    public Double[] getDoubleArray( String prop_name );
    public Set<String> getDoubleArrayPropNames(); 
    
    public void setFloat( String prop_name, Float prop_value );
    public Float getFloat( String prop_name );
    public Set<String> getFloatPropNames(); 

    public void setFloatArray( String prop_name, Float[] prop_valueArr );
    public Float[] getFloatArray( String prop_name );
    public Set<String> getFloatArrayPropNames(); 
    
    public void setLogical( String prop_name, Boolean prop_value );
    public Boolean getLogical( String prop_name );
    public Set<String> getLogicalPropNames(); 

    public void setLogicalArray( String prop_name, Boolean[] prop_valueArr );
    public Boolean[] getLogicalArray( String prop_name );
    public Set<String> getLogicalArrayPropNames(); 
    
    public void setTag( String prop_name, TCComponent prop_value );
    public TCComponent getTag( String prop_name );
    public Set<String> getTagPropNames(); 
    
    public void setTagArray( String prop_name, TCComponent[] prop_valueArr );
    public TCComponent[] getTagArray( String prop_name );
    public Set<String> getTagArrayPropNames(); 
    
    public void setDate( String prop_name, Date prop_value );
    public Date getDate( String prop_name );
    public Set<String> getDatePropNames(); 
    
    public void setDateArray( String prop_name, Date[] prop_value );
    public Date[] getDateArray( String prop_name );
    public Set<String> getDateArrayPropNames(); 
    
    public void setCompound( String prop_name, IPropertyMap prop_value );
    public IPropertyMap getCompound( String prop_name );
    public Set<String> getCompoundPropNames(); 

    public void setCompoundArray( String prop_name, IPropertyMap[] prop_value );
    public IPropertyMap[] getCompoundArray( String prop_name );
    public Set<String> getCompoundArrayPropNames();
    
    public IPropertyMap createCompound (Object args);
    
    public void addAll(IPropertyMap propMap);
    
    public String getType();
    
    public void setType(String type);
    
    public TCComponent getComponent();
    
    public void setComponent(TCComponent component);
    
    public boolean isEmpty();
}
