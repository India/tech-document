package com.nov.rac.propertymap;

import java.util.HashMap;
import java.util.Map;

import com.teamcenter.rac.kernel.TCComponent;

public class DeepCopyDataHelper
{
	public static final String RELATION_TYPE_NAME = "relationTypeName";
	public static final String NEW_NAME = "newName";
	public static final String IS_TARGET_PRIMARY = "isTargetPrimary";
	public static final String IS_REQUIRED = "isRequired";
	public static final String COPY_RELATIONS = "copyRelations";
	public static final String OPERATION_INPUT_TYPE_NAME = "operationInputTypeName";
	public static final String COPY_ACTION = "action";
	
	// Enum Copy Action
	public static final ENUM_COPY_ACTION COPYACTION_CopyAsObjectType = ENUM_COPY_ACTION.CopyAsObjectType;  
	public static final ENUM_COPY_ACTION COPYACTION_CopyAsObject = ENUM_COPY_ACTION.CopyAsObject;
	public static final ENUM_COPY_ACTION COPYACTION_CopyAsRefType = ENUM_COPY_ACTION.CopyAsRefType;
	public static final ENUM_COPY_ACTION COPYACTION_CopyAsReference = ENUM_COPY_ACTION.CopyAsReference;
	public static final ENUM_COPY_ACTION COPYACTION_DontCopyType = ENUM_COPY_ACTION.DontCopyType;
	public static final ENUM_COPY_ACTION COPYACTION_NoCopy = ENUM_COPY_ACTION.NoCopy;
	public static final ENUM_COPY_ACTION COPYACTION_RelateToLatest = ENUM_COPY_ACTION.RelateToLatest;
	public static final ENUM_COPY_ACTION COPYACTION_ReviseAndRelateToLatest = ENUM_COPY_ACTION.ReviseAndRelateToLatest;
	public static final ENUM_COPY_ACTION COPYACTION_Select = ENUM_COPY_ACTION.Select;
		
	public enum ENUM_PROPERTY_TYPE { Relation, Reference };
	
	private enum ENUM_COPY_ACTION 
	{ 				
		CopyAsObjectType (0),
		CopyAsObject (0),
		CopyAsRefType (1),
		CopyAsReference (1),
		DontCopyType  (2),
		NoCopy (2), 
		RelateToLatest (3), 
		ReviseAndRelateToLatest (4), 
		Select (4);
		
		private int m_actionCode;
		
		ENUM_COPY_ACTION(int actionCode)
		{
			m_actionCode = actionCode;
		}
		
		public int getActionCode()
		{
			return m_actionCode;
		}
	};
	
	//public enum ENUM_COPY_ACTION { CopyAsObject, CopyAsReference, NoCopy, RelateToLatest, ReviseAndRelateToLatest, Select };

	private TCComponent m_targetObject;
	private Map<String , Object> m_properties = new HashMap<String, Object>();

	public DeepCopyDataHelper(TCComponent attachedObject)
	{
		m_targetObject = attachedObject;
	}   

	public void setProperty(String propName, Object propValue)
	{
		m_properties.put( propName , propValue );
	}

	public Object getProperty(String propName)
	{
		return m_properties.get( propName );
	}

    public com.teamcenter.services.rac.core._2008_06.DataManagement.DeepCopyData getDeepCopyData_2008_06()
	{
		com.teamcenter.services.rac.core._2008_06.DataManagement.DeepCopyData newDeepCopyData = new com.teamcenter.services.rac.core._2008_06.DataManagement.DeepCopyData();

		// Copy property Values from internal structure into newDeepCopyData
		newDeepCopyData.otherSideObjectTag = m_targetObject;
		newDeepCopyData.relationTypeName = (String) m_properties.get(RELATION_TYPE_NAME);
		newDeepCopyData.newName = (String) m_properties.get( NEW_NAME );
		
		newDeepCopyData.action =  ((ENUM_COPY_ACTION) m_properties.get(COPY_ACTION)).getActionCode();
		newDeepCopyData.isTargetPrimary = (Boolean) m_properties.get(IS_TARGET_PRIMARY);
		newDeepCopyData.isRequired = (Boolean) m_properties.get(IS_REQUIRED);
		
		newDeepCopyData.copyRelations = (Boolean) m_properties.get(COPY_RELATIONS);
		
		return newDeepCopyData;
	}

// Placeholder for TC 9.1 DeepCopyData function	
//	/*package*/ com.teamcenter.services.rac.core._2011_06.DataManagement.DeepCopyData getDeepCopyData_2011_06()
//	{
//		com.teamcenter.services.rac.core._2011_06.DataManagement.DeepCopyData newDeepCopyData = new com.teamcenter.services.rac.core._2011_06.DataManagement.DeepCopyData();
//
//		// Copy property Values from internal structure into newDeepCopyData
//		newDeepCopyData.otherSideObjectTag = m_targetObject;
//		newDeepCopyData.relationTypeName = (String) m_properties.get(RELATION_TYPE_NAME);
//		newDeepCopyData.newName = (String) m_properties.get( NEW_NAME );
//		newDeepCopyData.action =  ((ENUM_COPY_ACTION) m_properties.get(COPY_ACTION)).getActionCode();
//		newDeepCopyData.isTargetPrimary = (Boolean) m_properties.get(IS_TARGET_PRIMARY);
//		newDeepCopyData.isRequired = (Boolean) m_properties.get(IS_REQUIRED);
//		newDeepCopyData.copyRelations = (Boolean) m_properties.get(COPY_RELATIONS);
//		
//		return newDeepCopyData;
//	}
	
// Placeholder for TC 10.1 DeepCopyData function	
//		/*package*/ com.teamcenter.services.rac.core._2013_05.DataManagement.DeepCopyData getDeepCopyData_2013_05()
//		{
//			com.teamcenter.services.rac.core._2013_05.DataManagement.DeepCopyData newDeepCopyData = new com.teamcenter.services.rac.core._2013_05.DataManagement.DeepCopyData();
//
//			// Copy property Values from internal structure into newDeepCopyData
//			newDeepCopyData.otherSideObjectTag = m_targetObject;
//			newDeepCopyData.relationTypeName = (String) m_properties.get(RELATION_TYPE_NAME);
//			newDeepCopyData.newName = (String) m_properties.get( NEW_NAME );
//			newDeepCopyData.action =  ((ENUM_COPY_ACTION) m_properties.get(COPY_ACTION)).getActionCode();
//			newDeepCopyData.isTargetPrimary = (Boolean) m_properties.get(IS_TARGET_PRIMARY);
//			newDeepCopyData.isRequired = (Boolean) m_properties.get(IS_REQUIRED);
//			newDeepCopyData.copyRelations = (Boolean) m_properties.get(COPY_RELATIONS);
//			
//			return newDeepCopyData;
//		}	

}