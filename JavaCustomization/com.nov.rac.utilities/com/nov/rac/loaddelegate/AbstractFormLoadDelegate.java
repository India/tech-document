package com.nov.rac.loaddelegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.util.Registry;

public abstract class AbstractFormLoadDelegate extends AbstractLoadDelegate
{
    private final String DATA_PANELS_LIST = "DATA_PANELS_LIST";
    
    public AbstractFormLoadDelegate(List<IUIPanel> inputProviders)
    {
        super(inputProviders);
    }
    
    
    @Override
    protected List<IUIPanel> getFilteredPanels(String theGroup,
            String objectType, List<IUIPanel> panels)
    {
        StringBuffer contextString = new StringBuffer();
        contextString.append(objectType).append(".")
                .append(DATA_PANELS_LIST);
        
        Registry registry = getRegistry();
        
        String[] panelNames = RegistryUtils.getConfiguration(theGroup,
                contextString.toString(), registry);
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        return panelList;
    }
    
}
