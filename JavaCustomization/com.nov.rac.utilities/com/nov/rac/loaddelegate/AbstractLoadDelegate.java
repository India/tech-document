package com.nov.rac.loaddelegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nov.rac.propertymap.IPropertyMap;
import com.nov.rac.ui.ILoadSave;
import com.nov.rac.ui.IUIPanel;
import com.nov.rac.utilities.common.RegistryUtils;
import com.nov.rac.utilities.common.UserSessionInfo;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.util.Registry;


public abstract class AbstractLoadDelegate implements ILoadDelegate
{
    private List<IUIPanel> m_inputProviders = null;
    private Registry m_registry = null;
    private static final String PANELS_LIST = "PANELS_LIST";
    
    public AbstractLoadDelegate(List<IUIPanel> inputProviders)
    {
        m_inputProviders = inputProviders;
    }
    
    protected Registry getRegistry()
    {
        return m_registry;
    }
    
    public void setRegistry(Registry registry)
    {
        m_registry = registry;
    }

    public void loadPanelsData(String theGroup, String itemType,
            List<IUIPanel> panels, IPropertyMap propertyMap)
    {
        List<IUIPanel> filteredPanels = getFilteredPanels(theGroup, itemType,
                panels);
        
        for (IUIPanel panel : filteredPanels)
        {
            if (null != propertyMap && panel instanceof ILoadSave)
            {
                ILoadSave loadablepanel = (ILoadSave) panel;
                try
                {
                    loadablepanel.load(propertyMap);
                }
                catch (TCException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    
    protected List<IUIPanel> getFilteredPanels(String theGroup,
            String objectType, List<IUIPanel> panels)
    {
        StringBuffer contextString = new StringBuffer();
        contextString.append(objectType).append(".").append(PANELS_LIST);
        
        Registry registry = getRegistry();
        
        String[] panelNames = RegistryUtils.getConfiguration(theGroup,
                contextString.toString(), registry);
        List<IUIPanel> panelList = new ArrayList<IUIPanel>();
        if (panelNames != null)
        {
            List<String> names = Arrays.asList(panelNames);
            for (IUIPanel panel : panels)
            {
                if (names.contains(panel.getClass().getSimpleName()))
                {
                    panelList.add(panel);
                }
            }
        }
        return panelList;
    }
    
    protected List<IUIPanel> getFilteredPanels(String objectType,
            List<IUIPanel> panels)
    {
        String currentGroup = UserSessionInfo.getCurrentGroup();
        
        return getFilteredPanels(currentGroup, objectType, panels);
        
    }
    
    public final List<IUIPanel> getInputProviders()
    {
        return m_inputProviders;
    }
    
}
