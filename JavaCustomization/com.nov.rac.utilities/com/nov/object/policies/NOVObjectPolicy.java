package com.nov.object.policies;

import com.teamcenter.soa.common.ObjectPropertyPolicy;
import com.teamcenter.soa.common.PolicyProperty;
import com.teamcenter.soa.common.PolicyType;

public class NOVObjectPolicy
{	
	private ObjectPropertyPolicy m_objectPolicy = null;
	
	public NOVObjectPolicy() 
	{
		m_objectPolicy = new ObjectPropertyPolicy();
	}

	public void addPropertiesPolicy(String policyType, NOVObjectPolicyProperty[] objectPolicyProps) 
	{		
		PolicyType objectPolicyType = new PolicyType(policyType);
		
		for(int iCnt = 0; iCnt < objectPolicyProps.length ; iCnt++ )
		{
			PolicyProperty policyProp = new PolicyProperty(objectPolicyProps[iCnt].getPropertyName());
			
			if(objectPolicyProps[iCnt].getPropertyModifier() != null)
			{
				policyProp.setModifier(objectPolicyProps[iCnt].getPropertyModifier(), true);
			}
			
			objectPolicyType.addProperty(policyProp);
		}		
		
		m_objectPolicy.addType(objectPolicyType);
		
	}

	public ObjectPropertyPolicy get_objectPolicy() 
	{
		return m_objectPolicy;
	}	
}
