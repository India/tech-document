package com.nov.object.policies;

import java.util.ArrayList;
import java.util.List;

import com.nov.rac.utilities.common.RegistryUtils;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.soa.common.PolicyProperty;

public class NOVObjectPolicyFactory
{
    
    //private TCComponent m_tcComponent = null;
	private String m_objectType = "";
    
    private final String PROPERTY_POLICY = "PROPERTY_POLICY";
    private final String PROPERTIES_AS_ATTRIBUTE = "PROPERTIES_AS_ATTRIBUTE";
    private final String PROPERTIES_WITH_PROPERTIES = "PROPERTIES_WITH_PROPERTIES";
    //private final String DELIMITER = ",";


    
    public NOVObjectPolicyFactory(TCComponent tcComponent)
    {
    	m_objectType = tcComponent.getType();
    }
    
    public NOVObjectPolicyFactory(String objectType)
    {
        m_objectType = objectType;
    }
        
    public NOVObjectPolicy createObjectPolicy(String theGroup,Registry registry) throws Exception
    {
        if (registry == null)
        {
            throw new Exception(
                    "Registry is not set:class NOVObjectPolicyFactory");
        }
        
        // String objectType = m_tcComponent.getType();
        //String emptyString = "";
        String[] propertiesAsAttribute = null;
        String[] propertiesWithProperties = null;
        
        NOVObjectPolicy l_objectPolicy = new NOVObjectPolicy();
        
        /*String[] propertyPolicies = registry.getStringArray(objectType + "."
                + PROPERTY_POLICY,DELIMITER, emptyString);*/
        String[] propertyPolicies = RegistryUtils.getConfiguration(theGroup,m_objectType + "."
                + PROPERTY_POLICY, registry);
        
        for (String propertyPolicy : propertyPolicies)
        {
            /*propertiesAsAttribute = registry.getStringArray(propertyPolicy
                    + "." + PROPERTIES_AS_ATTRIBUTE,DELIMITER, emptyString);
            
            propertiesWithProperties = registry.getStringArray(propertyPolicy
                    + "." + PROPERTIES_WITH_PROPERTIES,DELIMITER, emptyString);
            
            propertiesAsAttribute = propertiesAsAttribute!= null? propertiesAsAttribute : new String[]{""};
            propertiesWithProperties = propertiesWithProperties != null? propertiesWithProperties : new String[]{""};*/
            
            propertiesAsAttribute = RegistryUtils.getConfiguration(theGroup,propertyPolicy
                    + "." + PROPERTIES_AS_ATTRIBUTE, registry);
            propertiesWithProperties = RegistryUtils.getConfiguration(theGroup,propertyPolicy
                    + "." + PROPERTIES_WITH_PROPERTIES, registry);
            
            NOVObjectPolicyProperty policyProps[] = createNOVObjectPolicyProperty(
                    propertiesAsAttribute, propertiesWithProperties);
            
            l_objectPolicy.addPropertiesPolicy(propertyPolicy, policyProps);
        }
        
        return l_objectPolicy;
    }
    
    private NOVObjectPolicyProperty[] createNOVObjectPolicyProperty(
            String[] attributes, String[] properties)
    {
               
        List<NOVObjectPolicyProperty> policyProps = new ArrayList<NOVObjectPolicyProperty>();
        
        for (int inx = 0; inx < attributes.length; inx++)
        {
            
            policyProps.add(new NOVObjectPolicyProperty(attributes[inx],
                    PolicyProperty.AS_ATTRIBUTE));
        }
        
        for (int jnx = 0; jnx < properties.length; jnx++)
        {
            
            policyProps.add(new NOVObjectPolicyProperty(properties[jnx],
                    PolicyProperty.WITH_PROPERTIES));
        }
        NOVObjectPolicyProperty[] prop = new NOVObjectPolicyProperty[policyProps.size()];
        return  policyProps.toArray(prop);
    }
    
}
