package com.nov.object.policies;

import com.teamcenter.rac.kernel.ServiceData;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.rac.core.DataManagementService;
import com.teamcenter.services.rac.core.SessionService;
import com.teamcenter.soa.client.ObjectPropertyPolicyManager;

public class NOVDataManagementServiceHelper {
	
	private TCSession m_tcSession; 
	
	private String m_strCurrentSOAPolicy = null;
	
	public NOVDataManagementServiceHelper(TCComponent objectObject)
	{		
		m_tcSession = objectObject.getSession();				
	}
	
	public NOVDataManagementServiceHelper(TCSession session)
	{		
		m_tcSession = session;				
	}

	public TCComponent getObjectProperties(TCComponent compObject, String[] attrProperties,NOVObjectPolicy objectPolicy)
	{
		TCComponent[] inputObjs = new TCComponent[1];
		inputObjs[0] = compObject;
		
		TCComponent[] plainObject = getObjectsProperties(inputObjs, attrProperties, objectPolicy);
		return plainObject == null ? null : plainObject[0];
	}	
	
	public TCComponent[] getObjectsProperties(TCComponent[] inputObjs, String[] attrProperties, NOVObjectPolicy objectPolicy)
	{
		TCComponent[] plainObject = null;
		
		// Get Session SOA Policy
		getCurrentSOAPolicy();		
		
		SessionService service = SessionService.getService(m_tcSession);
		
		if(objectPolicy != null)
		{
			service.setObjectPropertyPolicy(objectPolicy.get_objectPolicy());
		}
		
		DataManagementService DMService = DataManagementService.getService(m_tcSession);
		
		ServiceData serviceData =  DMService.getProperties(inputObjs, attrProperties);
	
		if (serviceData.sizeOfPlainObjects() == 1)
		{
			plainObject = new TCComponent[serviceData.sizeOfPlainObjects()];

			for (int inx = 0; inx < serviceData.sizeOfPlainObjects() ; inx++)
			{
				plainObject[inx] = serviceData.getPlainObject(inx);
			}
		}
			
		setCurrentSOAPolicy();
		
		return plainObject;	
	}	

	private void setCurrentSOAPolicy() 
	{
		try 
		{
			if(m_strCurrentSOAPolicy != null){
			 // commented as part of TC10.1 upgrade
//				SessionService service = SessionService.getService(m_tcSession);
//				
//				boolean bStatus = service.setObjectPropertyPolicy(m_strCurrentSOAPolicy);
//				
//				System.out.println(bStatus);
			    
			    ObjectPropertyPolicyManager policyManager = m_tcSession.getSoaConnection().getObjectPropertyPolicyManager();
			    
			    policyManager.setPolicy(m_strCurrentSOAPolicy);
			}				
		} catch (ServiceException e) 
		{
			e.printStackTrace();
		}
	}

	private void getCurrentSOAPolicy() 
	{	  
	    ObjectPropertyPolicyManager policyManager = m_tcSession.getSoaConnection().getObjectPropertyPolicyManager();
	    
	    this.m_strCurrentSOAPolicy = policyManager.getCurrentPolicy();
	    
		//this.m_strCurrentSOAPolicy = m_tcSession.getSoaConnection().getCurrentObjectPropertyPolicy(); // depricated in TC10.1
	}
}
