package com.nov.object.policies;

public class NOVObjectPolicyProperty 
{	
	private String m_propertyName = null;	
	private String m_propertyModifier = null;
	
	public NOVObjectPolicyProperty (String name,String modifier)
	{		
		m_propertyName = name;		
		m_propertyModifier = modifier;
	}

	public String getPropertyName() 
	{
		return m_propertyName;
	}

	public String getPropertyModifier() 
	{
		return m_propertyModifier;
	}
}
